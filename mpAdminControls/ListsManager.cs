using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Data.OleDb;

namespace LMP.Administration.Controls
{
    public partial class ListsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *********************fields*********************

        //flag to prevent event code from running
        bool m_bLoadInProgress = false;

        //values for ValueSet on row entry
        string m_xValue1 = "";
        string m_xValue2 = "";

        //flag used to keep focus on invalid cell
        bool m_bListItemIsInvalid = false;
        //GLOG : 6592 : JSW
        //flag used to indicate an alpha sort is required
        bool m_bListNameIsEdited = false;

        //GLOG : 6891 : JSW
        //flag to indicate new list
        bool m_bListIsNew = false;

        BindingSource m_oQueriedSource;
        BindingSource m_oInternalSource;
        DataRow m_oLastExternalRow = null;
        DataRow m_oLastInternalRow = null;
        private const string DEF_INTERNAL_LISTNAME = "New List Name";
        private const string DEF_INTERNAL_SORTCOL = "1";
        private const string DEF_QUERIED_LISTNAME = "New Queried List Name";
        private const string DEF_QUERIED_SQLSTRING = "SELECT ";
        private const string DEF_QUERIED_DBCONNECTION = "LocalDBConnection";

        #endregion *********************fields*********************
        #region *********************constructors*********************
        public ListsManager()
        {
            try
            {
                m_bLoadInProgress = true;
                InitializeComponent();
                LoadInternalLists(false);
                LoadExternalLists();
                m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        #endregion *********************constructors*********************
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region *********************private functions*********************
        /// <summary>
        /// displays external list collection in grid
        /// </summary>
        private void FindMatchingRecord(string xSearchString)
        {
            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                int iMatchIndex = this.grdEnumLists.FindRecord
                    (xSearchString, "ListName", false);

                if (iMatchIndex > -1)
                    //Match found
                    this.grdEnumLists.CurrentCell = this.grdEnumLists.Rows[iMatchIndex].Cells["ListName"];
            }
        }
        private void LoadExternalLists()
        {
            try
            {
                //set to true to prevent event code from running
                m_bLoadInProgress = true;

                //get data set for Value Set items
                DataSet oDS = this.GetNonEditableListItems();

                //get datatable of all lists
                DataTable oDT = oDS.Tables[0];

                m_oQueriedSource = new BindingSource();

                m_oQueriedSource.DataSource = oDT;
                
                //subscribe to position changed event of data source - this will handle
                //updates to external list

                m_oQueriedSource.PositionChanged += new EventHandler(m_oExternalSource_PositionChanged); 

                //bind to grid
                this.grdQueriedLists.DataSource = m_oQueriedSource;

                //get grid columns collection
                DataGridViewColumnCollection oCols = this.grdQueriedLists.Columns;

                //hide all grid columns
                foreach (DataGridViewColumn oColumn in oCols)
                    oColumn.Visible = false;

                //bind (and unhide) only those columns that are needed
                oCols["ListName"].DataPropertyName = "ListName";
                oCols["ListName"].Visible = true;
                oCols["ListName"].DisplayIndex = 0;
                oCols["ListName"].HeaderText = "Name";
                oCols["SQL"].DataPropertyName = "SQL";
                oCols["SQL"].Visible = true;
                oCols["SQL"].DisplayIndex = 1;
                oCols["ConnectionString"].DataPropertyName = "ConnectionString";
                oCols["ConnectionString"].Visible = true;
                oCols["ConnectionString"].DisplayIndex = 2;
                oCols["ID"].DataPropertyName = "ID";
                oCols["ID"].Visible = false;

                if (oDT.Rows.Count > 0)
                {
                    //select first row
                    if (grdEnumLists.Rows.Count > 0)
                        this.grdEnumLists.Rows[0].Selected = true;
                    //populate value set grid 
                    UpdateValueSetDisplay();
                }

                //set flag to true so event code can run.
                m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        /// <summary>
        /// displays internal list collection in grid
        /// </summary>
        private void LoadInternalLists(bool bSortByID)
        {
            try
            {
                //set to true to prevent event code from running
                m_bLoadInProgress = true;
            
                //get Lists collection
                LMP.Data.Lists oLists = new LMP.Data.Lists(true);
                
                //get datatable of all lists
                DataTable oDT = oLists.ToDataSet().Tables[0];

                m_oInternalSource = new BindingSource();

                m_oInternalSource.DataSource = oDT;
                //bind to grid
                this.grdEnumLists.DataSource = m_oInternalSource;

                //get grid columns collection
                DataGridViewColumnCollection oCols = this.grdEnumLists.Columns;

                //hide all grid columns
                foreach (DataGridViewColumn oColumn in oCols)
                    oColumn.Visible = false;

                //bind (and unhide) only those columns that are needed
                oCols["ListName"].DataPropertyName = "ListName";
                oCols["ListName"].Visible = true;
                oCols["SortColumn"].DataPropertyName = "SortColumn";
                oCols["SortColumn"].Visible = false;
                oCols["ColumnCount"].DataPropertyName = "ColumnCount";
                oCols["ColumnCount"].Visible = false;
                oCols["ID"].DataPropertyName = "ID";
                oCols["SetID"].DataPropertyName = "SetID";

                if (oDT.Rows.Count > 0)
                {
                    if (bSortByID)
                    {
                        grdEnumLists.Sort(grdEnumLists.Columns["ID"], ListSortDirection.Ascending);
                        grdEnumLists.CurrentCell = grdEnumLists.Rows[grdEnumLists.Rows.Count - 1].Cells["ListName"];

                        //GLOG : 6592 : JSW
                        //function to resort alphabetically and select correct row
                        AlphaSortInternalListsAndSelectCurrentCell();
                    }
                    else
                    {
                        //select first row
                        this.grdEnumLists.Rows[0].Selected = true;
                        this.grdEnumLists.CurrentCell = grdEnumLists.Rows[0].Cells["ListName"];
                    }
                    //populate value set grid 
                    UpdateValueSetDisplay();
                }

                //subscribe to position changed event of data source - this will handle
                //updates to Internal list
                m_oInternalSource.PositionChanged += new EventHandler(m_oInternalSource_PositionChanged);

                //set flag to true so event code can run.
                m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        /// <summary>
        /// displays value set items for selected list item
        /// </summary>
        private void UpdateValueSetDisplay()
        {
            try
            {
                this.m_bLoadInProgress = true;
                
                //get ValueSetID for currently selected row
                string xSetID = this.GetInternalListGridValue("SetID");
                //get SortColumn
                string xSortColumn = this.GetInternalListGridValue("SortColumn");
                int iSortColumn = System.String.IsNullOrEmpty(xSortColumn) ? 0 : Int32.Parse(xSortColumn);

                //if there is a value, load value set items
                if (!System.String.IsNullOrEmpty(xSetID))
                {
                    //get data set for Value Set items
                    DataSet oDS = GetValueSet(xSetID);

                    //get data table
                    DataTable oTable = oDS.Tables[0];

                    //bind dataset to value set grid
                    this.grdListItems.DataSource = oTable.DefaultView;
 
                    //display column names 
                    this.grdListItems.Columns[0].HeaderText = "Column 1";
                    this.grdListItems.Columns[1].HeaderText = "Column 2";

                    //set column width
                    this.grdListItems.Columns[2].Visible = false;
                    this.grdListItems.Columns[0].SortMode = DataGridViewColumnSortMode.Programmatic;
                    this.grdListItems.Columns[1].SortMode = DataGridViewColumnSortMode.Programmatic;

                    //set multiline
                    this.grdListItems.Columns[0].DefaultCellStyle.WrapMode = 
                        DataGridViewTriState.True;
                    this.grdListItems.Columns[1].DefaultCellStyle.WrapMode = 
                        DataGridViewTriState.True;

                    DataGridViewColumn oColumn = new DataGridViewColumn();
                    if (iSortColumn == 2)
                        oColumn = this.grdListItems.Columns[1];
                    else
                        oColumn = this.grdListItems.Columns[0];

                    //sort according to sort column
                    this.grdListItems.Sort(oColumn, ListSortDirection.Ascending);
                }
                else
                {
                    //clear dataGrid
                    ClearValueSets();
                }
                this.m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }
        }
        /// <summary>
        /// inserts new valueset into db.
        /// </summary>
        /// <param name="xSetID"></param>
        /// <param name="xValue1"></param>
        /// <param name="xValue2"></param>
        private void AddValueSetItem(string xSetID, string xValue1, string xValue2)
        {
            try
            {
                DataGridViewRow oSelRow = this.GetSelectedValueSetGridRow();
                
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                //JTS 12/09/08: LastEditTime needs to be UTC Date
                //GLOG 6673:  Make sure Date in SQL string is US Format
                oCmd.CommandText = "Insert INTO ValueSets (SetID, Value1, Value2, LastEditTime) " +
                    "VALUES (" + xSetID + ", \"" + xValue1 + "\", \"" + xValue2 + "\", #" + 
                    LMP.Data.Application.GetCurrentEditTime(true) + "#);";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }

                this.m_bLoadInProgress = true;
                if (oSelRow != null)
                {
                    //update values in grid
                    this.SetValueSetGridValue("SetID", xSetID);
                    this.SetValueSetGridValue("Value1", xValue1);
                    this.SetValueSetGridValue("Value2", xValue2);
                }

                //if there are 2 columns, need to update column count for list
                if (!System.String.IsNullOrEmpty(xValue2))
                {
                    string xID = this.GetInternalListGridValue("ID");
                    LMP.Data.Lists oLists = new LMP.Data.Lists();
                    //get this list item
                    LMP.Data.List oL = (LMP.Data.List)oLists.ItemFromID(Convert.ToInt32(xID));
                    //set column count to 2
                    oL.ValueSetColumnCount = mpListColumns.Two;
                    oLists.Save(oL);
                    this.SetInternalListGridValue("ColumnCount", "2");
                }
                this.m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotAddListEntry"), oE);
            }
        }
        /// <summary>
        /// updates a valueset row into db.
        /// </summary>
        /// <param name="xSetID"></param>
        /// <param name="xValue1"></param>
        /// <param name="xValue2"></param>
        /// <param name="xEditedValue1"></param>
        /// <param name="xEditedValue2"></param>
        private void EditValueSetItem(string xSetID, string xValue1, string xValue2, string xEditedValue1, string xEditedValue2)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandType = CommandType.Text;
                //GLOG 3401: Because ValueSet items don't have a unique ID,
                //we need to update LastEditTime for all items in this set,
                //while adding a Deletions record so entire Set will get recreated in sync
                //GLOG 6673:  Make sure Date in SQL string is US Format
                oCmd.CommandText = "UPDATE ValueSets SET LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) +
                    "# WHERE SetID = " + xSetID + ";";
                oCmd.ExecuteNonQuery();
                SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, xSetID);

                //Create update SQL statement
                //JTS 12/09/08: LastEditTime needs to be UTC Date
                //GLOG 6673:  Make sure Date in SQL string is US Format
                string xSQL = "Update ValueSets SET Value1 = \"" + xEditedValue1 + "\", Value2 = \"" + xEditedValue2 +
                    "\", LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) + "# WHERE SetID = " + xSetID + " AND Value1 = \"" + xValue1 + "\" AND ";
                string xValue2Condition = "Value2 = \"" + xValue2 + "\";";
                if (System.String.IsNullOrEmpty(xValue2)) 
                    xValue2Condition = "(Value2 is null OR Value2 = '');";
                xSQL = xSQL + xValue2Condition;
                oCmd.CommandText = xSQL;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                //update value set grid cell values
                this.m_bLoadInProgress = true;
                this.SetValueSetGridValue("Value1", xEditedValue1);
                this.SetValueSetGridValue("Value2", xEditedValue2);
                this.m_bLoadInProgress = false;

                //need to update column count for list
                string xID = this.GetInternalListGridValue("ID");
                LMP.Data.Lists oLists = new LMP.Data.Lists();
                //get this list item
                LMP.Data.List oL = (LMP.Data.List)oLists.ItemFromID(Convert.ToInt32(xID));

                this.m_bLoadInProgress = true;
                if (!System.String.IsNullOrEmpty(xEditedValue2))
                {
                    //set column count to 2
                    oL.ValueSetColumnCount = mpListColumns.Two;
                    this.SetInternalListGridValue("ColumnCount", "2");

                }
                else
                {
                    //set column count to 1
                    oL.ValueSetColumnCount = mpListColumns.One;
                    this.SetInternalListGridValue("ColumnCount", "1");
                }
                this.m_bLoadInProgress = false;
                oLists.Save(oL);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotUpdateListEntry"), oE);
            }
        }
        /// <summary>
        /// deletes a valueset row into db.
        /// </summary>
        /// <param name="xSetID"></param>
        /// <param name="xValue1"></param>
        /// <param name="xValue2"></param>
        private void DeleteValueSetItem(string xSetID, string xValue1, string xValue2)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandType = CommandType.Text;
                //GLOG 3401: Because ValueSet items don't have a unique ID,
                //we need to update LastEditTime for all items in this set,
                //while adding a Deletions record so entire Set will get recreated in sync
                //GLOG 6673:  Make sure Date in SQL string is US Format
                oCmd.CommandText = "UPDATE ValueSets SET LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) +
                    "# WHERE SetID = " + xSetID + ";";
                oCmd.ExecuteNonQuery();
                SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, xSetID);
                //GLOG 3487:  Null value in DB may still result in empty string being passed to this function 
                if (string.IsNullOrEmpty(xValue2))
                    oCmd.CommandText = "Delete * FROM ValueSets WHERE  Value1 = \"" + xValue1 + "\" AND (Value2 = '' OR Value2 Is Null) " +
                        " AND SetID = " + xSetID + ";";
                else
                    oCmd.CommandText = "Delete * FROM ValueSets WHERE  Value1 = \"" + xValue1 + "\" AND Value2 = \"" + xValue2 +
                        "\" AND SetID = " + xSetID + ";";

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotDeleteListEntry"), oE);
            }
        }
        /// <summary>
        /// Deletes selected list item from grid
        /// </summary>
        /// <param name="xID"></param>
        private void DeleteListItem(string xID)
        {
            try
            {
                //get lists collection
                LMP.Data.Lists oLists = new LMP.Data.Lists();
                int iID = Convert.ToInt32(xID);
                //delete list item
                oLists.Delete(iID);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotDeleteListItem"), oE);
            }
        }
        /// <summary>
        /// Clears all value set items from grid
        /// </summary>
        private void ClearValueSets()
        {
            try
            {   //remove rows
                int iRowCount = this.grdListItems.Rows.Count;

                if (iRowCount == 0)
                    return;

                for (int i = (iRowCount - 1); i < this.grdListItems.Rows.Count && i >= 0; i--)
                {
                    if (this.grdListItems.Rows[i].Cells[0].Value != null)
                    {
                        this.grdListItems.Rows.Remove(this.grdListItems.Rows[i]);
                    }
                }
                //set sort column to 1
                DataGridViewColumn oColumn = new DataGridViewColumn();
                oColumn = this.grdListItems.Columns[0];
                this.grdListItems.Sort(oColumn, ListSortDirection.Ascending);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }

        } 
        /// <summary>
        /// Adds a list item to grid
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xSortColumn"></param>
        private void AddListItem(string xName, string xSortColumn, mpListColumns iNumColumns) //GLOG 8068
        {
            try
            {
                //get selected row
                DataGridViewRow oSelRow = grdEnumLists.CurrentRow;
                //get lists collection
                LMP.Data.Lists oLists = new LMP.Data.Lists();
                //create new list item
                LMP.Data.List oL = (LMP.Data.List)oLists.Create();
                mpListColumns iColumn = mpListColumns.One;
                //get SortColumn value 
                switch (xSortColumn)
                {
                    case "0":
                        iColumn = mpListColumns.NotApplicable;
                        break;
                    case "1":
                        iColumn = mpListColumns.One;
                        break;
                    case "2":
                        iColumn = mpListColumns.Two;
                        xSortColumn = "2";
                        break;
                    default:
                        iColumn = mpListColumns.NotApplicable;
                        break;
                }
                //assign required values for new list item
                oL.Name = xName;
                oL.ValueSetSortColumn = iColumn;
                oL.ValueSetID = GetNextID(true);
                oL.ValueSetColumnCount = iNumColumns; //GLOG 8068

                //save list item
                oLists.Save(oL);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateListItem"), oE);
            }
        }
        /// <summary>
        /// Updates edited list item
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xSortColumn"></param>
        /// <param name="xID"></param>
        private void EditListItem(string xName, string xSortColumn, string xID)
        {
            try
            {
                int iID = Convert.ToInt32(xID);
                LMP.Data.Lists oLists = new LMP.Data.Lists();
                LMP.Data.List oL = (LMP.Data.List)oLists.ItemFromID(iID);
                mpListColumns iColumn = mpListColumns.NotApplicable;

                switch (xSortColumn)
                {
                    case "0":
                        iColumn = mpListColumns.NotApplicable;
                        break;
                    case "1":
                        iColumn = mpListColumns.One;
                        break;
                    case "2":
                        iColumn = mpListColumns.Two;
                        xSortColumn = "2";
                        break;
                    default:
                        iColumn = mpListColumns.NotApplicable;
                        break;
                }

                oL.Name = xName;
                oL.ValueSetSortColumn = iColumn;
                //check number of valueset columns
                oL.ValueSetColumnCount = GetColumnCount(); 
                oLists.Save(oL);

                //Update properties in internal lists grid
                m_bLoadInProgress = true;

                DataGridViewRow oSelRow = grdEnumLists.CurrentRow;
                if (oSelRow != null)
                {
                    //update row info
                    this.SetInternalListGridValue("SortColumn", xSortColumn);
                    this.SetInternalListGridValue("ListName", oL.Name.ToString());
                    //this.SetInternalListGridValue("ColumnCount", oL.ValueSetColumnCount.ToString());
                    this.SetInternalListGridValue("SetID", oL.ValueSetID.ToString());
                }
                m_bLoadInProgress = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateListItem"), oE);
            }
        }
        /// <summary>
        /// sets the value from the specified column of the selected grid row
        /// </summary>
        /// <param name="xColumnName"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private void SetInternalListGridValue(string xColumnName, string xValue)
        {
            try
            {
                DataGridViewRow oRow = this.grdEnumLists.CurrentRow;
                if (oRow == null)
                    return;

                if (xColumnName == "ID")
                    oRow.Cells[xColumnName].Value = Convert.ToInt32(xValue);
                else
                    oRow.Cells[xColumnName].Value = xValue;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        /// <summary>
        /// sets the value from the specified column of the selected grid row
        /// </summary>
        /// <param name="xColumnName"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private void SetValueSetGridValue(string xColumnName, string xValue)
        {
            try
            {
                //get other value from grid
                DataGridViewRow oRow = this.GetSelectedValueSetGridRow();
                if (xColumnName == "SetID")
                    oRow.Cells[xColumnName].Value = Convert.ToInt32(xValue);
                else
                    oRow.Cells[xColumnName].Value = xValue;
            }
            catch
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"));
            }
        }
        /// <summary>
        /// Determines sorting behavior for selected List entry column
        /// </summary>
        private void UpdateQueriedLists(bool bAddNew)
        {
            try
            {
                if (m_oLastExternalRow != null && m_oLastExternalRow.RowState == DataRowState.Unchanged && !bAddNew)
                    return;

                LMP.Data.Lists oLists = new LMP.Data.Lists();
                LMP.Data.List oL = null;

                if (bAddNew)
                {
                    //The user is adding a record, create a new one
                    //use defaults for required values
                    oL = (List)oLists.Create();
                    oL.Name = GetNewQueriedListName();
                    oL.SQL = DEF_QUERIED_SQLSTRING;
                    oL.ConnectionString = DEF_QUERIED_DBCONNECTION;
                }
                else
                {
                    string xID = grdQueriedLists.CurrentRow.Cells["ID"].FormattedValue.ToString();
                    string xName = grdQueriedLists.CurrentRow.Cells["ListName"].FormattedValue.ToString();

                    //User is modifying an existing record, get the current record
                    if (xID != "")
                        oL = (LMP.Data.List)oLists.ItemFromID(Int32.Parse(xID));
                    else
                        oL = (LMP.Data.List)oLists.ItemFromName(xName);

                    //set properties
                    oL.Name = xName;
                    oL.ConnectionString = grdQueriedLists.CurrentRow.Cells["ConnectionString"].FormattedValue.ToString(); ;
                    oL.SQL = grdQueriedLists.CurrentRow.Cells["SQL"].FormattedValue.ToString(); ;
                }

                oL.ValueSetID = 0;
                //these values are required to save an external list
                oL.ValueSetColumnCount = mpListColumns.NotApplicable;
                oL.ValueSetSortColumn = mpListColumns.NotApplicable;

                oLists.Save(oL);

                //write ID to grid
                DataTable oDT = ((DataTable)m_oQueriedSource.DataSource);
                if (bAddNew)
                {
                    //refresh queried lists grid - select last item
                    LoadExternalLists();
                    grdQueriedLists.CurrentCell = grdQueriedLists.Rows[grdQueriedLists.Rows.Count - 1].Cells["ListName"];
                }
                else
                    //accept changes on modified row - not necessary to update 
                    //entire data source
                    m_oLastExternalRow.AcceptChanges();
            }
            catch (System.Exception oE)
            {
                if (oE.InnerException.GetType().Name == "OleDbException")
                {
                    MessageBox.Show(oE.InnerException.Message.ToString(),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.IsValid = false;
                }

                else

                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateListItem"), oE);
            }
        }
        /// <summary>
        /// adds new internal list to collection
        /// </summary>
        private void UpdateInternalLists(bool bAddNew)
        {
            try
            {
                if (m_oLastInternalRow != null && m_oLastInternalRow.RowState == DataRowState.Unchanged &&
                    !bAddNew)
                    return;

                LMP.Data.Lists oLists = new LMP.Data.Lists();
                string xID = null;
                string xName = "";
                //GLOG 6654: Default Sort order to first column, instead of value of currently-selected list
                string xSortColumn = "1"; // GetInternalListGridValue("SortColumn");

                if (bAddNew)
                {
                    //The user is adding a record, create a new one
                    xName = GetNewListName();
                    if (xName != "")
                    {
                        //GLOG 8068: Create list with 1 column initially
                        AddListItem(xName, xSortColumn, mpListColumns.One);
                    }
                    else
                        return;
                }
                else
                {
                    xID = grdEnumLists.CurrentRow.Cells["ID"].FormattedValue.ToString();
                    xName = grdEnumLists.CurrentRow.Cells["ListName"].FormattedValue.ToString();
                    xSortColumn = grdEnumLists.CurrentRow.Cells["SortColumn"].FormattedValue.ToString();
                    EditListItem(xName, xSortColumn, xID);
                }
                
                if (bAddNew)
                {
                    //reload lists grid if new item added; enter edit mode
                    LoadInternalLists(false);
                    FindMatchingRecord(xName);
                    UpdateValueSetDisplay();
                    //grdEnumLists.BeginEdit(true);
                }
                else
                    //accept changes on modified row 
                    m_oLastInternalRow.AcceptChanges();
            
            }
            catch (System.Exception oE)
            {
                if (oE.InnerException.GetType().Name == "OleDbException")
                {
                    MessageBox.Show(oE.InnerException.Message.ToString(),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.IsValid = false;
                }

                else

                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateListItem"), oE);
            }
        }
        /// <summary>
        /// Determines sorting behavior for selected List entry column
        /// </summary>
        private void SortValueSetColumn(int iColumn)
        {
            try
            {
                if (!m_bLoadInProgress)
                {
                    string xID = this.GetInternalListGridValue("ID");
                    string xSortColumn = "1";

                    //check if there's an ID
                    if (!System.String.IsNullOrEmpty(xID))
                    {
                        string xName = this.GetInternalListGridValue("ListName");
                        //get SortColumn value
                        //check if column is valid
                        if (this.grdListItems.Columns[iColumn] != null)
                        {
                            xSortColumn = iColumn.ToString();
                            if (xSortColumn == "2")
                            {    //make sure 2nd column isn't empty
                                if (this.GetInternalListGridValue("ColumnCount") != "2")
                                    return;
                                //make sure there is at least one row with valid column 1 value
                                if (this.grdListItems.Rows.Count == 1)
                                    if (System.String.IsNullOrEmpty(this.GetValueSetGridValue("Value1")))
                                        return;
                            }
                            //update SortColumn value for ValueSet item
                            EditListItem(xName, xSortColumn, xID);
                            this.UpdateValueSetDisplay();
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Msg_CouldNotSortListEntries"), oE);
            }
        }
        private void DeleteInternalValueSetItem()
        {
            //do nothing if user in new row and no edits made
            if (grdListItems.CurrentRow == null || grdListItems.CurrentRow.IsNewRow)
                return;

            if (grdListItems.CurrentRow.Selected == false)
                grdListItems.CurrentRow.Selected = true;

            //warn user they are about to delete a ValueSet item
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Message_DeleteEntry"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                //get values required for deletion query
                string xSetID = this.GetValueSetGridValue("SetID");
                string xValue1 = this.GetValueSetGridValue("Value1");
                string xValue2 = this.GetValueSetGridValue("Value2");

                //make sure required values are valid
                if (xSetID != "" && xValue1 != "")
                {
                    //delete Value Set item
                    this.DeleteValueSetItem(xSetID, xValue1, xValue2);
                    //remove row
                    this.grdListItems.Rows.RemoveAt(this.grdListItems.CurrentRow.Index);
                }
                else
                {
                    //user has started a new entry - simply cancel edit
                    grdListItems.CancelEdit();
                }
            }
        }
        private void DeleteInternalListRow()
        {
            //do nothing if user in new row and no edits made
            if (grdEnumLists.CurrentRow.IsNewRow)
                return;

            //warn that list will be deleted
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Msg_DeleteListItem"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                this.m_bLoadInProgress = true;
                //get List ID
                string xID = this.GetInternalListGridValue("ID");

                //make sure ID is valid
                //Remove the row from the group DataGridView.
                this.grdEnumLists.Rows.RemoveAt(this.grdEnumLists.CurrentRow.Index);

                if (grdQueriedLists.Rows.Count > 0)
                {
                    this.grdQueriedLists.CurrentCell.Selected = true;
                }
                

                if (!System.String.IsNullOrEmpty(xID))
                {
                    //Delete item from 
                    DeleteListItem(xID);
                    //refresh value set grid
                    UpdateValueSetDisplay();
                }
                
                this.m_bLoadInProgress = false;
            }
        }
        /// <summary>
        /// disables/enables non valuse set list controls
        /// </summary>
        /// <param name="bEnabled"></param>
        private void EnableValueSetControls(bool bEnabled)
        {
            this.grdQueriedLists.Enabled = bEnabled;
            this.tbtnDeleteQueriedList.Enabled = bEnabled;
            this.grdEnumLists.Enabled = bEnabled;
            this.tbtnDeleteEnumList.Enabled = bEnabled;
            this.tbtnTranslate.Enabled = bEnabled;
            this.tbtnNewEnumList.Enabled = bEnabled;
        }
        /// <summary>
        /// disables/enables non external list controls
        /// </summary>
        /// <param name="bEnabled"></param>
        private void EnableExternalListControls(bool bEnabled)
        {
            this.grdQueriedLists.Enabled = bEnabled;
            this.grdListItems.Enabled = bEnabled;
            this.tbtnDeleteQueriedList.Enabled = bEnabled;
            this.tbtnNewQueriedList.Enabled = bEnabled;
        }
        /// <summary>
        /// disables/reenables non internal list grids and menu items
        /// 
        /// </summary>
        /// <param name="bEnabled"></param>
        private void EnableInternalListControls(bool bEnabled)
        {
            this.grdEnumLists.Enabled = bEnabled;
            this.grdListItems.Enabled = bEnabled;
            this.tbtnDeleteEnumListItem.Enabled = bEnabled;
            this.tbtnNewEnumList.Enabled = bEnabled;
            this.tbtnTranslate.Enabled = bEnabled;
        }
        /// <summary>
        ///  Handles delete row event for External List
        /// </summary>
        private void DeleteExternalListRow()
        {
            //do nothing if user in new row
            if (this.grdQueriedLists.CurrentRow.IsNewRow)
                return;

            //warn that list will be deleted
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Msg_DeleteListItem"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                this.m_bLoadInProgress = true;
                //get List ID
                string xID = this.GetExternalListGridValue("ID");

                //make sure ID is valid
                if (!System.String.IsNullOrEmpty(xID))
                {
                    //Delete item from 
                    DeleteListItem(xID);
                }

                //Remove the row from the group DataGridView.
                this.grdQueriedLists.Rows.RemoveAt(this.grdQueriedLists.CurrentRow.Index);
                this.IsValid = true;
                this.EnableInternalListControls(true);
 
                this.m_bLoadInProgress = false;
            }
        }
        //GLOG : 6592 : JSW
        //function to alpha sort internal lists and reselect current cell
        /// <summary>
        ///  Sorts internal lists alphabetically 
        ///  when new list is entered or old list is modified
        /// </summary>
        private void AlphaSortInternalListsAndSelectCurrentCell()
        {

            try
            {
                string xCurrentCellName = grdEnumLists.CurrentCell.Value.ToString();
                string xCellName = "";
                int iRowCount = grdEnumLists.Rows.Count;
                int i = 0;

                m_bLoadInProgress = true;

                try
                {
                    grdEnumLists.Sort(grdEnumLists.Columns["ListName"], ListSortDirection.Ascending);
                }
                finally
                {
                    m_bLoadInProgress = false;
                }

                for (i = 0; i < iRowCount; i++)
                {
                    xCellName = grdEnumLists.Rows[i].Cells["ListName"].Value.ToString();
                    if (xCellName == xCurrentCellName)
                        break;
                }
                this.grdEnumLists.CurrentCell = grdEnumLists.Rows[i].Cells["ListName"];
            }
            catch { }
        }
        /// <summary>
        /// handles uncommitted changes on close of form
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //reset the last row variable - this ensures we'll catch edits
            //to current row before attempting to leave grid or add new list
            if (m_oInternalSource.Current != null && m_oInternalSource.Position > -1 )
                m_oLastInternalRow = ((DataRowView)m_oInternalSource.Current).Row;

            //reset the last row variable - this ensures we'll catch edits
            //to current row before attempting to leave grid or add new list
            if (m_oQueriedSource.Current != null && m_oQueriedSource.Position > -1 )
                m_oLastExternalRow = ((DataRowView)m_oQueriedSource.Current).Row;
            
            //user has deleted last last record - nothing to save
            //so just exit
            //handle external lists row
            if (m_oLastExternalRow != null)
            {
                //this event fires before cell or row validating events
                //make sure we have valid data in current row
                bool bNotValid = ValidateExternalListRow(true);

                if (bNotValid)
                {
                    this.IsValid = false;
                    return;
                }
                else
                {
                    this.IsValid = true;

                    //commit any edits to grid & binding source
                    //this will flip row state to added or modified
                    //don't save if we're in the last row
                    grdQueriedLists.EndEdit();
                    m_oQueriedSource.EndEdit();

                    //update db record
                    UpdateQueriedLists(false);
                }
            }
            
            
            //handle internal lists row
            if (m_oLastInternalRow != null)
            {
                //make sure we have valid data in current row
                bool bNotValid = ValidateInternalListRow(true);

                if (bNotValid)
                {
                    this.IsValid = false;
                    return;
                }
                else
                {
                    this.IsValid = true;

                    //commit any edits to grid & binding source
                    //this will flip row state to added or modified
                    grdEnumLists.EndEdit();
                    m_oInternalSource.EndEdit();

                    //update db record
                    UpdateInternalLists(false);
                }
            }
        }
        
        /// <summary>
        /// checks for duplicate values in value set 
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>bool</returns>
        private bool IsDuplicateValueSet(string xSetID, string xValue1)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "SELECT Count(*) FROM ValueSets WHERE Value1 =\"" +
                    xValue1 + "\" AND SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                DataTable oTable = oDS.Tables[0];
                DataRow oRow = oTable.Rows[0];
                int iCount = Convert.ToInt32(oRow[0].ToString());
                if (iCount > 0)
                    return true;
                else
                    return false;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        /// <summary>
        /// Determines if List should be edited or added. 
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>bool</returns>
        private bool ValidateExternalListRow(bool bSuppressMessage)
        {
            //don't validate if current row is null - form is loading
            if (m_oLastExternalRow == null)
                return false;

            string xID = this.GetExternalListGridValue("ID");
            string xName = this.GetExternalListGridValue("ListName");
            string xSQL = this.GetExternalListGridValue("SQL");
            string xConnection = GetExternalListGridValue("ConnectionString");
            string xMsg = "";
            Lists oLists = new Lists();
            
            bool bCancelEvent = true;
            m_bListItemIsInvalid = true;
            
            //Name, SQL string and Connection string must not be empty;
            if (System.String.IsNullOrEmpty(xName))
                //check if there's a value for ListName
                xMsg = LMP.Resources.GetLangString("Msg_ListNameRequired");
            else if (oLists.Exists(xName) &&
                oLists.ItemFromName(xName).ID.ToString() != xID)
                //if the name exists and the ID is different
                //from the current item ID, we have a duplicate
                //entry - otherwise its an edit to the name
                //which is okay
                xMsg = LMP.Resources.GetLangString("Msg_ListItemAlreadyExists") + xName;
            else if (System.String.IsNullOrEmpty(xSQL))
                //check if there's a value for SQL string
                xMsg = LMP.Resources.GetLangString("Msg_SQLStringRequired");
            else
            {
                bCancelEvent = false;
                m_bListItemIsInvalid = false;
            }

            if (m_bListItemIsInvalid && !bSuppressMessage)
            {
                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            this.IsValid = !bCancelEvent;
            return bCancelEvent;
        }
        /// <summary>
        /// Determines if List should be edited or added. 
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>bool</returns>
        private bool ValidateInternalListRow(bool bSuppressMessage)
        {
            //don't validate if we're in the last row
            if (grdEnumLists.CurrentRow.IsNewRow)
                return false;

            //don't validate if current row is null - form is loading
            if (m_oLastInternalRow == null)
                return false;

            string xID = this.GetInternalListGridValue("ID");
            string xName = this.GetInternalListGridValue("ListName");
            string xMsg = "";
            Lists oLists = new Lists();

            bool bCancelEvent = true;
            m_bListItemIsInvalid = true;

            //Name, SQL string and Connection string must not be empty;
            if (System.String.IsNullOrEmpty(xName))
                //check if there's a value for ListName
                xMsg = LMP.Resources.GetLangString("Msg_ListNameRequired");
            else if (oLists.Exists(xName) &&
                oLists.ItemFromName(xName).ID.ToString() != xID)
                //if the name exists and the ID is different
                //from the current item ID, we have a duplicate
                //entry - otherwise its an edit to the name
                //which is okay
                xMsg = LMP.Resources.GetLangString("Msg_ListItemAlreadyExists") + xName;
            else
            {
                bCancelEvent = false;
                m_bListItemIsInvalid = false;
            }

            if (m_bListItemIsInvalid && !bSuppressMessage)
            {
                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            this.IsValid = !bCancelEvent;
            return bCancelEvent;
        }
        private bool HandleValueSetUserEdit()
        {

            try
            {
                if (m_bLoadInProgress)
                    return false;

                //get ValueSetID from selected list item in internal list grid
                string xValueSetID = this.GetInternalListGridValue("SetID");
                bool bValueGridSelectionExists = this.grdListItems.SelectedCells.Count > 0;
                bool bCancelEvent = false;

                //check if List has a valid SetID
                if (xValueSetID != null && bValueGridSelectionExists)
                {
                    string xSetID = this.GetValueSetGridValue("SetID");
                    //get values for Value1 and Value2 fields for Value Set 
                    string xEditedValue1 = this.GetValueSetGridValue("Value1");
                    string xEditedValue2 = this.GetValueSetGridValue("Value2");

                    //if xSetID is null, this is a new record
                    if (System.String.IsNullOrEmpty(xSetID))
                    {
                        //value1 is required, can't be null
                        if (!System.String.IsNullOrEmpty(xEditedValue1))
                        {
                            //check if xEditedValue1 already exists in Value set for current List item
                            bool b_ValueSetAlreadyExists = this.IsDuplicateValueSet(xValueSetID, xEditedValue1);
                            if (b_ValueSetAlreadyExists)
                            {
                                //alert that value set already exists
                                MessageBox.Show(
                                LMP.Resources.GetLangString("Msg_ListEntryAlreadyExists") + xEditedValue1,
                                LMP.String.MacPacProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                                //cancel row leave event.
                                bCancelEvent = true;
                            }
                            else
                            {
                                //all is well, add to db.
                                AddValueSetItem(xValueSetID, xEditedValue1, xEditedValue2);
                            }
                        }
                        else
                        {
                            //check if Column 2 has a value
                            if (!System.String.IsNullOrEmpty(xEditedValue2))
                            {
                                //missing value for Column 1, inform user
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_ListEntryValue1Required"),
                                LMP.String.MacPacProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                                //cancel row leave event.
                                bCancelEvent = true;
                            }
                        }
                    }
                    //value set has been edited, validate before updating db entry.
                    else
                    {
                        //make sure original value for Value1 was not null
                        if (!System.String.IsNullOrEmpty(m_xValue1))
                        {
                            //check for blank values for Column 1
                            if (System.String.IsNullOrEmpty(xEditedValue1))
                            {
                                //inform user that blank values for Column 1 aren't allowed
                                DialogResult oChoice = MessageBox.Show(
                                    LMP.Resources.GetLangString("Msg_ListEntryValue1Required"),
                                    LMP.String.MacPacProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                                //cancel row leave event
                                bCancelEvent = true;
                            }
                            //if Cell's EditedFormattedValue doesn't match original Cell Value, update record
                            else
                            {
                                //check if EditedFormattedValue matched orginal value for cells
                                if (xEditedValue1 != m_xValue1 || xEditedValue2 != m_xValue2)
                                {
                                    //everything's ok, go ahead and edit
                                    EditValueSetItem(xSetID, m_xValue1, m_xValue2, xEditedValue1, xEditedValue2);
                                }
                            }
                        }
                    }
                }
                //associated List for this entry has no SetID, don't allow entry.
                else
                {
                    //if Column 1 value is null and List SetID is null, blank record, ignore.
                    if (System.String.IsNullOrEmpty(this.GetValueSetGridValue("Value1")))
                    {
                        return false;
                    }
                    //user has entered list entry with no list selected
                    else
                    {
                        //Show message that list must be selected
                        //before list entries can be made
                        MessageBox.Show(
                        LMP.Resources.GetLangString("Msg_NoListSelected"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                return bCancelEvent;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotDeleteListEntry"), oE);
            }
        }
        /// <summary>
        /// gets the value from the specified column of the selected grid row
        /// </summary>
        /// <param name="xColumnName"></param>
        /// <returns>string</returns>
        private string GetInternalListGridValue(string xColumnName)
        {
            try
            {
                string xValue = null;

                //get selected row in list grid
                int iRow = 0;
                if (m_oInternalSource != null)
                    iRow = m_oInternalSource.Position;
                else
                    iRow = grdEnumLists.CurrentRow.Index;

                DataGridViewRow oRow = grdEnumLists.Rows[iRow];

                if (oRow != null)
                    xValue = oRow.Cells[xColumnName].EditedFormattedValue.ToString();
                
                //return default sort column if column name is SortColumn and list has
                //no sort value
                if (xColumnName == "SortColumn")
                    xValue = xValue == null || xValue == "" ? DEF_INTERNAL_SORTCOL :
                        xValue;

                return xValue;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        private string GetInternalListGridValueOLD(string xColumnName)
        {
            try
            {
                if (xColumnName == "SortColumn")
                {
                    //get sort column from column that is sorted in 
                    //value sets grid
                    string xSortColumn = DEF_INTERNAL_SORTCOL;

                    if (this.grdListItems.SortedColumn != null)
                        xSortColumn = (grdListItems.SortedColumn.Index + 1).ToString();

                    return xSortColumn;
                }
                else
                {

                    //get selected row in list grid
                    int iRow = 0;
                    if (m_oInternalSource != null)
                        iRow = m_oInternalSource.Position;
                    else
                        iRow = grdEnumLists.CurrentRow.Index;

                    DataGridViewRow oRow = grdEnumLists.Rows[iRow];
                    if (oRow != null)
                        return oRow.Cells[xColumnName].EditedFormattedValue.ToString();
                    else
                        return null;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        /// <summary>
        /// gets the value from the specified column of the selected grid row
        /// </summary>
        /// <param name="xColumnName"></param>
        /// <returns>string</returns>
        private string GetExternalListGridValue(string xColumnName)
        {
            try
            {
                //get selected row in list grid
                DataGridViewRow oRow = grdQueriedLists.CurrentRow;

                if (oRow != null)
                    return oRow.Cells[xColumnName].EditedFormattedValue.ToString();
                else
                    return "";
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        /// <summary>
        /// gets the value from the specified column of the selected grid row
        /// </summary>
        /// <param name="xColumnName"></param>
        /// <returns></returns>
        private string GetValueSetGridValue(string xColumnName)
        {
            try
            {
                if (xColumnName == "SortColumn")
                {
                    //get sort column from column sorted in value grid
                    string xSortColumn = "1";

                    if (this.grdListItems.SortedColumn != null)
                        //there is a sort column
                        if (this.grdListItems.SortedColumn.Index == 1)
                            //sorted column is column 2
                            xSortColumn = "2";

                    return xSortColumn;
                }
                else
                {
                    //get other value from grid
                    DataGridViewRow oRow = this.GetSelectedValueSetGridRow();

                    if (oRow != null)
                        //get edited formatted value of specified column in selected row
                        return oRow.Cells[xColumnName].EditedFormattedValue.ToString();
                    else
                        return null;
                }
            }
            catch
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"));
            }
        }
        /// <summary>
        /// generates new List name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewListName()
        {
            //GLOG 6654: Prompt for new List Name to Add
            string xName = DEF_INTERNAL_LISTNAME;
            bool bValid = true;
            do
            {
                DialogResult oRet = Dialog.InputBox("New List", "Enter new List Name:", ref xName);
                if (oRet == DialogResult.OK)
                {
                    DataTable oDT = (DataTable)m_oInternalSource.DataSource;
                    DataRow[] oDR = oDT.Select("ListName = '" + xName + "'");

                    if (oDR.GetLength(0) == 0)
                    {
                        bValid = true;
                        return xName;
                    }
                    else
                    {
                        bValid = false;
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ItemNameAlreadyInUse"), "List"),
                            LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                    return "";
            } while (!bValid);
            return "";
        }
        /// <summary>
        /// generates new List name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewQueriedListName()
        {
            if (grdEnumLists.Rows.Count == 0)
                return DEF_QUERIED_LISTNAME;

            //search for existing default name
            DataTable oDT = (DataTable)m_oQueriedSource.DataSource;
            DataRow[] oDR = oDT.Select("ListName = '" + DEF_QUERIED_LISTNAME + "'");

            if (oDR.GetLength(0) == 0)
                return DEF_QUERIED_LISTNAME;
            else
            {
                return DEF_QUERIED_LISTNAME + "_" + GetNextID(false).ToString();
            }
        }
        ///<summary>
        /// gets next available SetID or ID number for creating a new value set.
        /// </summary>
        /// <param name="iRowIndex"></param>
        /// <returns>int</returns>
        private int GetNextID(bool bUseSetID)
        {
            try
            {
                string xCommandText = bUseSetID == true ?
                    "Select MAX(SetID) As MaxID FROM Lists;" :
                    "Select MAX(ID) As MaxID FROM Lists;";

                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = xCommandText;
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                DataTable oTable = oDS.Tables[0];
                DataRow oRow = oTable.Rows[0];

                int iMaxID = Convert.ToInt32(oRow[0].ToString());
                iMaxID++;

                return iMaxID;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        /// <summary>
        /// returns the selected List grid row
        /// </summary>
        /// <returns>DataGridViewRow</returns>
        private DataGridViewRow GetSelectedValueSetGridRow()
        {
            int iRow = -1;

            //get selected cells
            DataGridViewSelectedCellCollection oCells =
                this.grdListItems.SelectedCells;

            if (oCells.Count > 0)
            {
                //there are selected cells, get row
                iRow = oCells[0].RowIndex;

                //get value of specified column in selected row
                return this.grdListItems.Rows[iRow];
            }
            else
                return null;
        }
        /// <summary>
        /// returns the dataset with list items value set items
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>DataSet</returns>
        private DataSet GetValueSet(string xSetID)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "Select Value1, Value2, SetID FROM ValueSets WHERE SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                return oDS;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        /// <summary>
        /// returns the dataset of non-editable list items
        /// </summary>
        /// <returns>DataSet</returns>
        private DataSet GetNonEditableListItems()
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "Select * FROM Lists WHERE SetID = 0;";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                return oDS;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        /// <summary>
        /// gets the column count from Value Sets
        /// </summary>
        /// <returns>mpListColumns</returns>
        private mpListColumns GetColumnCount()
        {
            try
            {
                bool bHasOneColumn = false;
                bool bHasTwoColumns = false;

                for (int i = 0; i < grdListItems.Rows.Count; i++)
                {
                    if (grdListItems.Rows[i].Cells[0].Value != null && (grdListItems.Rows[i].Cells[0].Value.ToString() != ""))
                        bHasOneColumn = true;
                    if (grdListItems.Rows[i].Cells[1].Value != null && (grdListItems.Rows[i].Cells[1].Value.ToString() != ""))
                        bHasTwoColumns = true;
                    if (bHasTwoColumns)
                        break;
                }
                if (bHasTwoColumns)
                    return mpListColumns.Two;
                if (bHasOneColumn)
                    return mpListColumns.One;
                //default value must be One.
                //If value is NonApplicable, List Item won't be saved
                return mpListColumns.One;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"), oE);
            }
        }
        #endregion *********************private functions*********************
        #region *********************event handlers*********************

        private void tbtnDeleteEnumList_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteInternalListRow(); 
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDeleteEnumListItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteInternalValueSetItem(); 
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnNewQueriedList_Click(object sender, EventArgs e)
        {
            try
            {
                //commit any pending edits; save to db
                if (grdQueriedLists.Rows.Count > 0)
                {
                    grdQueriedLists.EndEdit();
                    m_oQueriedSource.EndEdit();
                    //generate validation message if any
                    this.IsValid = ValidateExternalListRow(false);
                    SaveCurrentRecord();
                    if (!this.IsValid)
                        return;
                }
                this.UpdateQueriedLists(true); 
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDeleteQueriedList_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteExternalListRow();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnNewEnumList_Click(object sender, EventArgs e)
        {
            try
            {
                grdEnumLists.EndEdit();
                m_oInternalSource.EndEdit();
                SaveCurrentRecord();
                this.UpdateInternalLists(true);
                //GLOG : 6891 : JSW
                m_bListIsNew = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdEnumLists_SelectionChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (!m_bLoadInProgress)
                {
                    if (this.GetInternalListGridValue("SetID") != null)
                    {
                        //GLOG : 6592 : JSW
                        //resort lists if list name has changed
                        if (m_bListNameIsEdited == true)
                        {
                            AlphaSortInternalListsAndSelectCurrentCell();
                            m_bListNameIsEdited = false;
                        }                        
                        //display list entries for this list
                        UpdateValueSetDisplay();
                    }
                    else
                    {
                        //clear value set display
                        ClearValueSets();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdEnumLists_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {

                if (this.m_bLoadInProgress)
                    return;

                bool bCancel = false;
                bCancel = this.ValidateInternalListRow(false);

                if (bCancel)
                    e.Cancel = true;

                this.IsValid = !e.Cancel;

                //disable controls if invalid
                this.EnableExternalListControls(!e.Cancel);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            
        }
        /// <summary>
        /// disable navigation while there is a pending edit in grdExternalLists
        /// this is due to bug where e.cancel = true in the RowValidating event
        /// handler causes app to hang if other grids are selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdEnumLists_CurrentCellDirtyStateChanged(object sender, System.EventArgs e)
        {
            try
            {
                EnableExternalListControls(false);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdEnumLists_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdListItems_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (this.m_bLoadInProgress)
                    return;

                bool bCancel = false;

                bCancel = this.HandleValueSetUserEdit();

                if (bCancel)
                {
                    e.Cancel = true;
                }
            
                //renable external list controls if valid
                this.EnableValueSetControls(!e.Cancel);

                //set valid property to prevent navigation 
                //to another Manager
                this.IsValid = !e.Cancel;
            
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdListItems_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //save  original row values
                m_xValue1 = this.GetValueSetGridValue("Value1");
                m_xValue2 = this.GetValueSetGridValue("Value2");

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdListItems_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int iColumn = e.ColumnIndex + 1;
                this.SortValueSetColumn(iColumn);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// disable navigation while there is a pending edit in grdInternalLists
        /// this is due to bug where e.cancel = true in the RowValidating event
        /// handler causes app to hang if other grids are selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdListItems_CurrentCellDirtyStateChanged(object sender, System.EventArgs e)
        {
            try
            {
                EnableValueSetControls(false);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdQueriedLists_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                if (this.m_bLoadInProgress)
                    return;

                bool bCancel = false;
                bCancel = this.ValidateExternalListRow(false);

                if (bCancel)
                    e.Cancel = true;
                
                this.IsValid = !e.Cancel;

                //disable controls if invalid
                this.EnableInternalListControls(!e.Cancel);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// disable navigation while there is a pending edit in grdExternalLists
        /// this is due to bug where e.cancel = true in the RowValidating event
        /// handler causes app to hang if other grids are selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdQueriedLists_CurrentCellDirtyStateChanged(object sender, System.EventArgs e)
        {
            try
            {
                EnableInternalListControls(false);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdQueriedLists_Leave(object sender, System.EventArgs e)
        {
            try
            {
                //this event fires before cell or row validating events
                //validate & attempt to save if appropriate
                SaveCurrentRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdEnumLists_Leave(object sender, System.EventArgs e)
        {
            try
            {
                //this event fires before cell or row validating events
                //validate & attempt to save if appropriate
                SaveCurrentRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdQueriedLists_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void m_oExternalSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //return if binding source current item is null --
                //this will be the case if all records have been deleted
                //and binding source is updating grid display
                if (((BindingSource)sender).Current == null)
                {
                    m_oLastExternalRow = null;
                    return;
                }

                //set last row variable if it was previously null
                if (m_oLastExternalRow == null)
                    m_oLastExternalRow = ((DataRowView)((BindingSource)sender).Current).Row;

                //update record if grid row has been modified
                if (m_oLastExternalRow.RowState == DataRowState.Modified)
                    UpdateQueriedLists(false);

                //store current row as last row variable
                if (((BindingSource)sender).Position == -1)
                    //data table has no rows - last row has been deleted
                    m_oLastExternalRow = null;
                else
                    m_oLastExternalRow = ((DataRowView)((BindingSource)sender).Current).Row;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void m_oInternalSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //return if binding source current item is null --
                //this will be the case if all records have been deleted
                //and binding source is updating grid display
                if (((BindingSource)sender).Current == null)
                {
                    m_oLastInternalRow = null;
                    return;
                }

                //set last row variable if it was previously null
                if (m_oLastInternalRow == null)
                    m_oLastInternalRow = ((DataRowView)m_oInternalSource.Current).Row;

                //update record if grid row has been modified
                if (m_oLastInternalRow.RowState == DataRowState.Modified)
                {
                    UpdateInternalLists(false);
					//GLOG : 6592 : JSW
                    m_bListNameIsEdited = true;
                }

                //store current row as last row variable
                if (((BindingSource)sender).Position == -1)
                    //data table has no rows - last row has been deleted
                    m_oLastInternalRow = null;
                else
                    m_oLastInternalRow = ((DataRowView)((BindingSource)sender).Current).Row;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion *********************event handlers*********************

        private void grdQueriedLists_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }

        }

        //GLOG : 6891 : JSW
        //tabbing moves from row to row for exising records
        //and to details grid on the left for new record
        private void grdEnumLists_TabPressed(object sender, LMP.Controls.TabPressedEventArgs e)
        {
            int iRowIndex;
            if (m_bListIsNew)
            {
                this.grdListItems.Focus();
                m_bListIsNew = false;
            }
            else
            {
                try
                {
                    iRowIndex = this.grdEnumLists.CurrentRow.Index;
                    if ((grdEnumLists.Rows.Count - 1) > iRowIndex)
                        this.grdEnumLists.CurrentCell = this.grdEnumLists.Rows[iRowIndex + 1].Cells[2];
                }
                catch { }
            }
        }

        private void tbtnTranslate_Click(object sender, EventArgs e)
        {
            //GLOG 8670
            MessageBox.Show(LMP.Resources.GetLangString("Msg_FeatureUnderConstruction"),
                LMP.ComponentProperties.ProductName,
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);

            return;

        }
    }
}

