﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LMP.Administration.Controls
{
    public partial class TemplateNameForm : Form
    {
        public TemplateNameForm()
        {
            InitializeComponent();
        }

        private void txtPattern_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)Keys.Back && e.KeyChar != '*' && e.KeyChar != '?' && Path.GetInvalidFileNameChars().Contains(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        public string NamePattern
        {
            get
            {
                return txtPattern.Text;
            }
            set
            {
                txtPattern.Text = value;
            }
        }
    }
}
