namespace LMP.Administration.Controls
{
    partial class LegacyDocumentsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.grdMappings = new System.Windows.Forms.DataGridView();
            this.LegacyDocID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LegacyDocDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SegmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SegmentDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treeSegments = new LMP.Controls.FolderTreeView(this.components);
            this.lblSegment = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeSegments)).BeginInit();
            this.SuspendLayout();
            // 
            // scMain
            // 
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.Location = new System.Drawing.Point(0, 0);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.Controls.Add(this.grdMappings);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.treeSegments);
            this.scMain.Panel2.Controls.Add(this.lblSegment);
            this.scMain.Size = new System.Drawing.Size(572, 443);
            this.scMain.SplitterDistance = 308;
            this.scMain.TabIndex = 5;
            // 
            // grdMappings
            // 
            this.grdMappings.AllowUserToAddRows = false;
            this.grdMappings.AllowUserToDeleteRows = false;
            this.grdMappings.AllowUserToResizeRows = false;
            this.grdMappings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdMappings.BackgroundColor = System.Drawing.Color.White;
            this.grdMappings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdMappings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LegacyDocID,
            this.LegacyDocDisplayName,
            this.SegmentID,
            this.SegmentDisplayName});
            this.grdMappings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMappings.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdMappings.GridColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.grdMappings.Location = new System.Drawing.Point(0, 0);
            this.grdMappings.MultiSelect = false;
            this.grdMappings.Name = "grdMappings";
            this.grdMappings.RowHeadersVisible = false;
            this.grdMappings.RowHeadersWidth = 20;
            this.grdMappings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdMappings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdMappings.Size = new System.Drawing.Size(304, 439);
            this.grdMappings.TabIndex = 4;
            this.grdMappings.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdMappings_ColumnAdded);
            this.grdMappings.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdMappings_KeyDown);
            // 
            // LegacyDocID
            // 
            this.LegacyDocID.HeaderText = "LegacyDocID";
            this.LegacyDocID.Name = "LegacyDocID";
            this.LegacyDocID.Visible = false;
            // 
            // LegacyDocDisplayName
            // 
            this.LegacyDocDisplayName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LegacyDocDisplayName.HeaderText = "Legacy Document Type";
            this.LegacyDocDisplayName.Name = "LegacyDocDisplayName";
            // 
            // SegmentID
            // 
            this.SegmentID.HeaderText = "Segment ID";
            this.SegmentID.Name = "SegmentID";
            this.SegmentID.Visible = false;
            // 
            // SegmentDisplayName
            // 
            this.SegmentDisplayName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SegmentDisplayName.HeaderText = "Segment";
            this.SegmentDisplayName.Name = "SegmentDisplayName";
            // 
            // treeSegments
            // 
            this.treeSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.White;
            this.treeSegments.Appearance = appearance1;
            this.treeSegments.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.treeSegments.DisableExcludedTypes = false;
            this.treeSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            this.treeSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.treeSegments.FilterText = "";
            this.treeSegments.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeSegments.HideSelection = false;
            this.treeSegments.IsDirty = false;
            this.treeSegments.Location = new System.Drawing.Point(0, 23);
            this.treeSegments.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.treeSegments.Name = "treeSegments";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.treeSegments.Override = _override1;
            this.treeSegments.OwnerID = 0;
            this.treeSegments.ShowCheckboxes = false;
            this.treeSegments.ShowFindPaths = true;
            this.treeSegments.Size = new System.Drawing.Size(255, 415);
            this.treeSegments.TabIndex = 1;
            this.treeSegments.TransparentBackground = false;
            this.treeSegments.DoubleClick += new System.EventHandler(this.treeSegments_DoubleClick);
            // 
            // lblSegment
            // 
            this.lblSegment.AutoSize = true;
            this.lblSegment.Location = new System.Drawing.Point(3, 5);
            this.lblSegment.Name = "lblSegment";
            this.lblSegment.Size = new System.Drawing.Size(47, 15);
            this.lblSegment.TabIndex = 2;
            this.lblSegment.Text = "&Map To:";
            // 
            // LegacyDocumentsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scMain);
            this.Name = "LegacyDocumentsManager";
            this.Size = new System.Drawing.Size(572, 443);
            this.Load += new System.EventHandler(this.LegacyDocumentsManager_Load);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel2.ResumeLayout(false);
            this.scMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdMappings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeSegments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblSegment;
        private System.Windows.Forms.DataGridView grdMappings;
        private System.Windows.Forms.SplitContainer scMain;
        private LMP.Controls.FolderTreeView treeSegments;
        private System.Windows.Forms.DataGridViewTextBoxColumn LegacyDocID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LegacyDocDisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SegmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SegmentDisplayName;
    }
}
