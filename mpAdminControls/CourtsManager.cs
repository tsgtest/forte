using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class CourtsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ***************fields*****************

        Addresses m_oAddresses = new Addresses();
        Courts m_oCourts = new Courts();
        Court m_oCourt = null;
        int m_iLevel0ID = 0;
        int m_iLevel1ID = 0;
        int m_iLevel2ID = 0;
        int m_iLevel3ID = 0;
        int m_iLevel4ID = 0;
        bool m_bGrdItemsEdited = false;
        bool m_bGrdItemsBound = false;
        bool m_bDeleted = false;

        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
                #endregion
        #region ***************constructors****************

        public CourtsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region **************methods*******************

        /// <summary>
        /// Display the court of the selected jurisdiction
        /// in grdItems
        /// </summary>
        private void DataBindGrdItems()
        {

            string xJudge = "";
            string xPreCity = "";
            string xCity = "";
            string xPostCity = "";
            string xCounty = "";
            string xCountry = "";
            string xState = "";
            string xStateAbbr = "";
            string xZip = "";
            string xLine1 = "";
            string xLine2 = "";
            string xLine3 = "";
            string xTemplate = "[Line1]|[Line2]|[Line3]|[City], [State]  [Zip]|[CountryIfForeign]";
            string xPhone1 = "";
            string xPhone2 = "";
            string xPhone3 = "";
            string xFax1 = "";
            string xFax2 = "";

            //reset level values
            m_iLevel0ID = 0;
            m_iLevel1ID = 0;
            m_iLevel2ID = 0;
            m_iLevel3ID = 0;
            m_iLevel4ID = 0;

            //retrieve court for Nodes with no children
            if (this.treeJurisdictions.SelectedNodes[0].HasExpansionIndicator == false)
            {
                this.grdItems.Enabled = true;
                m_bGrdItemsBound = true;

                if (m_bDeleted)
                {
                    //if this court has been deleted, set to null
                    m_oCourt = null;
                    m_bDeleted = false;
                }
                else
                {
                    //Get IDs from each level of jurisdiction
                    this.treeJurisdictions.GetJurisdictionIDs
                        (ref m_iLevel0ID, ref m_iLevel1ID, ref m_iLevel2ID, ref m_iLevel3ID, ref m_iLevel4ID);

                    //Get court from Jurisdiction ids
                    m_oCourt = this.GetCourt();
                }
            }
            else
            {
                //Clear grdItems
                grdItems.DataSource = null;
                grdItems.Rows.Clear();
                this.grdItems.Enabled = false;
                m_bGrdItemsBound = false;
                return;
            }
            if (m_oCourt != null)
            {
                xJudge = m_oCourt.JudgeName;
                try
                {
                    //get address values
                    Address oCourtAddress = (Address)m_oAddresses.
                        ItemFromID(m_oCourt.AddressID);

                    xPreCity = oCourtAddress.PreCity.ToString();
                    xCity = oCourtAddress.City.ToString();
                    xPostCity = oCourtAddress.PostCity.ToString();
                    xCounty = oCourtAddress.County.ToString();
                    xCountry = oCourtAddress.Country.ToString();
                    xState = oCourtAddress.State.ToString();
                    xStateAbbr = oCourtAddress.StateAbbr.ToString();
                    xZip = oCourtAddress.Zip.ToString();
                    xLine1 = oCourtAddress.Line1.ToString();
                    xLine2 = oCourtAddress.Line2.ToString();
                    xLine3 = oCourtAddress.Line3.ToString();
                    xTemplate = oCourtAddress.Template.ToString();
                    xPhone1 = oCourtAddress.Phone1.ToString();
                    xPhone2 = oCourtAddress.Phone2.ToString();
                    xPhone3 = oCourtAddress.Phone3.ToString();
                    xFax1 = oCourtAddress.Fax1.ToString();
                    xFax2 = oCourtAddress.Fax2.ToString();
                }
                catch
                {
                    //no address
                }
            }
            
            //Get the address object corresponding to the 
            //Create a new DataTable to use as the DataSource
            //for grdItems
            DataTable oPropertiesDT = new DataTable();

            //Add columns to the DataTable
            oPropertiesDT.Columns.Add("Property");
            oPropertiesDT.Columns.Add("Value");

            //An array for adding each new row to the DT
            object[] aRowValues = new object[2];

            //Add rows for each of the properties/values 
            //to the DataTable that is to be bound to 
            //properties

            //Courier Name
            aRowValues[0] = "Judge Name";
            aRowValues[1] = xJudge;
            oPropertiesDT.Rows.Add(aRowValues);

            //Pre City
            aRowValues[0] = "Pre City";
            aRowValues[1] = xPreCity;
            oPropertiesDT.Rows.Add(aRowValues);

            //City
            aRowValues[0] = "City";
            aRowValues[1] = xCity;
            oPropertiesDT.Rows.Add(aRowValues);

            //Post City
            aRowValues[0] = "Post City";
            aRowValues[1] = xPostCity;
            oPropertiesDT.Rows.Add(aRowValues);

            //County
            aRowValues[0] = "County";
            aRowValues[1] = xCounty;
            oPropertiesDT.Rows.Add(aRowValues);

            //State
            aRowValues[0] = "State";
            aRowValues[1] = xState;
            oPropertiesDT.Rows.Add(aRowValues);

            //State Abbreviation
            aRowValues[0] = "State Abbreviation";
            aRowValues[1] = xStateAbbr;
            oPropertiesDT.Rows.Add(aRowValues);

            //Country
            aRowValues[0] = "Country";
            aRowValues[1] = xCountry;
            oPropertiesDT.Rows.Add(aRowValues);

            //Zip Code
            aRowValues[0] = "Zip Code";
            aRowValues[1] = xZip;
            oPropertiesDT.Rows.Add(aRowValues);

            //Address Line 1
            aRowValues[0] = "Address Line 1";
            aRowValues[1] = xLine1;
            oPropertiesDT.Rows.Add(aRowValues);

            //Address Line 2
            aRowValues[0] = "Address Line 2";
            aRowValues[1] = xLine2;
            oPropertiesDT.Rows.Add(aRowValues);

            //Address Line 3
            aRowValues[0] = "Address Line 3";
            aRowValues[1] = xLine3;
            oPropertiesDT.Rows.Add(aRowValues);

            //Address Template
            aRowValues[0] = "Address Template";
            aRowValues[1] = xTemplate;
            oPropertiesDT.Rows.Add(aRowValues);

            //Phone Line 1
            aRowValues[0] = "Phone Line 1";
            aRowValues[1] = xPhone1;
            oPropertiesDT.Rows.Add(aRowValues);

            //Phone Line 2
            aRowValues[0] = "Phone Line 2";
            aRowValues[1] = xPhone2;
            oPropertiesDT.Rows.Add(aRowValues);

            //Phone Line 3
            aRowValues[0] = "Phone Line 3";
            aRowValues[1] = xPhone3;
            oPropertiesDT.Rows.Add(aRowValues);

            //Fax Line 1
            aRowValues[0] = "Fax Line 1";
            aRowValues[1] = xFax1;
            oPropertiesDT.Rows.Add(aRowValues);

            //Fax Line 2
            aRowValues[0] = "Fax Line 2";
            aRowValues[1] = xFax2;
            oPropertiesDT.Rows.Add(aRowValues);

            //Set grdItems' datasource to the newly created DT
            grdItems.DataSource = oPropertiesDT;

            grdItems.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdItems.Columns[0].FillWeight = 37;
            grdItems.Columns[0].ReadOnly = true;
            //grdItems.Columns[0].Selected = false;
            grdItems.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            
       }

        /// <summary>
        /// Gets the court according to Jurisdiction Level Ids.
        /// </summary>
        /// <returns>Court</returns>
        private Court GetCourt()
        {
            try
            {
                //Get the current courier record
                Court oCurCourt = (Court)m_oCourts.ItemFromLevels
                    (m_iLevel0ID, m_iLevel1ID, m_iLevel2ID, m_iLevel3ID, m_iLevel4ID);
                return oCurCourt;
            }
            catch
            {
                return null;
            }
        }

        ///<summary>
        /// Adds a new court to the DB 
        /// </summary>
        private void AddNewRecord()
        {
            m_oCourt = (Court)m_oCourts.Create();
            m_oCourt.JudgeName = this.grdItems.Rows[0].Cells[1].EditedFormattedValue.ToString();
            m_oCourt.L0 = this.m_iLevel0ID;
            m_oCourt.L1 = this.m_iLevel1ID;
            m_oCourt.L2 = this.m_iLevel2ID;
            m_oCourt.L3 = this.m_iLevel3ID;
            m_oCourt.L4 = this.m_iLevel4ID;
            Address oAddress = (Address)m_oAddresses.Create();
            oAddress.PreCity = this.grdItems.Rows[1].Cells[1].EditedFormattedValue.ToString();
            oAddress.City = this.grdItems.Rows[2].Cells[1].EditedFormattedValue.ToString();
            oAddress.PostCity = this.grdItems.Rows[3].Cells[1].EditedFormattedValue.ToString();
            oAddress.County = this.grdItems.Rows[4].Cells[1].EditedFormattedValue.ToString();
            oAddress.State = this.grdItems.Rows[5].Cells[1].EditedFormattedValue.ToString();
            oAddress.StateAbbr = this.grdItems.Rows[6].Cells[1].EditedFormattedValue.ToString();
            oAddress.Country = this.grdItems.Rows[7].Cells[1].EditedFormattedValue.ToString();
            oAddress.Zip = this.grdItems.Rows[8].Cells[1].EditedFormattedValue.ToString();
            oAddress.Line1 = this.grdItems.Rows[9].Cells[1].EditedFormattedValue.ToString();
            oAddress.Line2 = this.grdItems.Rows[10].Cells[1].EditedFormattedValue.ToString();
            oAddress.Line3 = this.grdItems.Rows[11].Cells[1].EditedFormattedValue.ToString();
            oAddress.Template = this.grdItems.Rows[12].Cells[1].EditedFormattedValue.ToString();
            oAddress.Phone1 = this.grdItems.Rows[13].Cells[1].EditedFormattedValue.ToString();
            oAddress.Phone2 = this.grdItems.Rows[14].Cells[1].EditedFormattedValue.ToString();
            oAddress.Phone3 = this.grdItems.Rows[15].Cells[1].EditedFormattedValue.ToString();
            oAddress.Fax1 = this.grdItems.Rows[16].Cells[1].EditedFormattedValue.ToString();
            oAddress.Fax2 = this.grdItems.Rows[17].Cells[1].EditedFormattedValue.ToString();
            try
            {
                //Save the current address
                m_oAddresses.Save(oAddress);
                m_oCourt.AddressID = oAddress.ID;
                //save the court
                m_oCourts.Save(m_oCourt);
            }
            catch (System.Exception oE)
            {
                //Update failed- 
                //alert
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Stores edits made to the currently displayed
        /// court in grdItems to the DB
        /// </summary>
        private void UpdateCourt()
        {
            //is this a new courier object?
            if (m_oCourt == null)
            {
                //add new record
                this.AddNewRecord();
            }
            else
            {
                try
                {
                    m_oCourt.JudgeName = this.grdItems.Rows[0].Cells[1].EditedFormattedValue.ToString();
                    m_oCourt.L0 = this.m_iLevel0ID;
                    m_oCourt.L1 = this.m_iLevel1ID;
                    m_oCourt.L2 = this.m_iLevel2ID;
                    m_oCourt.L3 = this.m_iLevel3ID;
                    m_oCourt.L4 = this.m_iLevel4ID;
                    Address oAddress = (Address)m_oAddresses.ItemFromID(m_oCourt.AddressID);
                    oAddress.PreCity = this.grdItems.Rows[1].Cells[1].EditedFormattedValue.ToString();
                    oAddress.City = this.grdItems.Rows[2].Cells[1].EditedFormattedValue.ToString();
                    oAddress.PostCity = this.grdItems.Rows[3].Cells[1].EditedFormattedValue.ToString();
                    oAddress.County = this.grdItems.Rows[4].Cells[1].EditedFormattedValue.ToString();
                    oAddress.State = this.grdItems.Rows[5].Cells[1].EditedFormattedValue.ToString();
                    oAddress.StateAbbr = this.grdItems.Rows[6].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Country = this.grdItems.Rows[7].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Zip = this.grdItems.Rows[8].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Line1 = this.grdItems.Rows[9].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Line2 = this.grdItems.Rows[10].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Line3 = this.grdItems.Rows[11].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Template = this.grdItems.Rows[12].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Phone1 = this.grdItems.Rows[13].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Phone2 = this.grdItems.Rows[14].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Phone3 = this.grdItems.Rows[15].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Fax1 = this.grdItems.Rows[16].Cells[1].EditedFormattedValue.ToString();
                    oAddress.Fax2 = this.grdItems.Rows[17].Cells[1].EditedFormattedValue.ToString();
                    //Save the current address
                    m_oAddresses.Save(oAddress);
                    //Save the current court
                    m_oCourts.Save(m_oCourt);
                }
                catch (System.Exception oE)
                {
                    //alert
                    throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                        "Error_CouldNotSaveChangesToDB"), oE);
                }
            }
        }


        /// <summary>
        /// Delete the currently selected court in grdItems from the DB
        /// </summary>
        private void DeleteCourt()
        {
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                if (m_oCourt != null)
                {
                    this.m_oCourts.Delete(m_oCourt.ID);
                    this.m_bDeleted = true;

                    // Glog: 2433
                    // Attempt to delete the address for the court.
                    try
                    {
                        this.m_oAddresses.Delete(m_oCourt.AddressID);
                    }
                    catch (LMP.Exceptions.StoredProcedureException)
                    {
                        // Allow the attempt to delete the address to fail silently
                        // if the failure is due to the record that is trying to be
                        // deleted being referenced by other records.
                    }
                }
            }
        }
        #endregion
        #region **************event handlers***************

        private void CourtsManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.treeJurisdictions.LevelDepth = 4;
                this.treeJurisdictions.Focus();
                UpdateView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private LMP.Data.Jurisdictions.Levels GetNodeJurisdictionLevel(UltraTreeNode oNode)
        {
            int iLevel = 0;
            string xNodeInfo = oNode.Key;
            //get level
            int iPos = xNodeInfo.IndexOf(".");
            string xLevel = xNodeInfo.Substring(0, iPos);
            iLevel = Int32.Parse(xLevel);

            switch (iLevel)
            {
                default:
                case 0:
                    return Jurisdictions.Levels.Zero;

                case 1:
                    return Jurisdictions.Levels.One;

                case 2:
                    return Jurisdictions.Levels.Two;

                case 3:
                    return Jurisdictions.Levels.Three;

                case 4:
                    return Jurisdictions.Levels.Four;
            }
        }
        
        private void UpdateView()
        {
            LMP.Data.Jurisdictions.Levels iLevel = LMP.Data.Jurisdictions.Levels.Four;
            if (treeJurisdictions.SelectedNodes.Count > 0)
            {
                iLevel = GetNodeJurisdictionLevel(treeJurisdictions.SelectedNodes[0]);
            }

            this.tbtnAdd.Enabled = (this.treeJurisdictions.SelectedNodes.Count > 0 &&
                                    iLevel < LMP.Data.Jurisdictions.Levels.Four);
            this.tbtnDelete.Enabled = (this.treeJurisdictions.SelectedNodes.Count > 0);
        }

        private void grdItems_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bGrdItemsEdited)
                {
                    //Save edits to grdItems to the DB
                    this.UpdateCourt();
                    
                    //Note that the changes have been made
                    m_bGrdItemsEdited = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeJurisdictions_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (this.treeJurisdictions.SelectedNodes.Count > 0)
                {
                    UpdateView();
                    this.DataBindGrdItems();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdItems_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bGrdItemsBound)
                    if (this.grdItems.CurrentCell.FormattedValue.ToString()
                       != this.grdItems.CurrentCell.EditedFormattedValue.ToString())
                            m_bGrdItemsEdited = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnClear_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteCourt();
                this.DataBindGrdItems();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void tbtnAdd_Click(object sender, EventArgs e)
        {
            AddJurisdictionForm oForm = new AddJurisdictionForm();

            DialogResult iChoice = oForm.ShowDialog();

            if (iChoice == DialogResult.OK)
            {
                AddNewJurisdictionRecord(oForm.JurisdictionName);
            }
        }

        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            // GLOG : 3217 : JAB
            // Use the newly introduced method that deletes nodes.
            try
            {
                if (treeJurisdictions.SelectedNodes.Count > 0)
                {
                    UltraTreeNode oCurNode = this.treeJurisdictions.SelectedNodes[0];
                    DeleteNode(oCurNode);
                }
            }
            catch(System.Exception oE)
            {
               LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Delete the specified node.
        /// </summary>
        /// <param name="oCurNode"></param>
        private void DeleteNode(UltraTreeNode oCurNode)
        {
            int iParentJurisdictionID = 0;

            if (oCurNode.Parent != null)
            {
                iParentJurisdictionID = (int)((object[])oCurNode.Parent.Tag)[0];
            }

            //Create a new Jurisdiction record
            Jurisdictions oJurisdictions = CreateJurisditionFromNode(oCurNode.Parent, iParentJurisdictionID);
            oJurisdictions.Delete((int)((object[])oCurNode.Tag)[0]);

            // Remove the expansion indicator if there are no more child items.
            if(oJurisdictions.Count == 0)
            {
                oCurNode.Parent.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            }

            // Set the next, previous or the parent node as the selected node.
            UltraTreeNode oSelectedNode = oCurNode.GetSibling(NodePosition.Next);
            
            if (oSelectedNode == null)
            {
                oSelectedNode = oCurNode.GetSibling(NodePosition.Previous);

                if (oSelectedNode == null)
                {
                    oSelectedNode = oCurNode.Parent;
                }
            }

            if (oSelectedNode != null)
            {
                oSelectedNode.Selected = true;
            }

            // Remove the node from the tree.
            oCurNode.Remove();
        }

        private Jurisdictions CreateJurisditionFromNode(UltraTreeNode oNode, int iParentJurisdictionID)
        {
            Jurisdictions oJurisdictions = null;

            switch (oNode.Level)
            {
                case 0:
                    oJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);
                    break;

                case 1:
                    oJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.One, iParentJurisdictionID);
                    break;

                case 2:
                    oJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Two, iParentJurisdictionID);
                    break;

                case 3:
                    oJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Three, iParentJurisdictionID);
                    break;

                case 4:
                    oJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Four, iParentJurisdictionID);
                    break;
            }

            return oJurisdictions;
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Provide an overload of this method for backward compatibility
        /// since a new parameter will be added to the original method
        /// which actually does all the work. Also, this method will return
        /// the newly added node via the original method which has been
        /// modified to return it.
        /// 
        /// Adds a new jurisdiction to the DB
        /// </summary>
        private UltraTreeNode AddNewJurisdictionRecord(string xNewJurisdictionName)
        {
            //Get the current node
            UltraTreeNode oCurNode = this.treeJurisdictions.SelectedNodes[0];
            return AddNewJurisdictionRecord(xNewJurisdictionName, oCurNode);
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Adding a parent node parameter to this method to facilitate copying
        /// and pasting of children nodes. Also, this method will return the newly
        /// added node to facilitate copying and pasting.
        /// 
        /// Adds a new jurisdiction to the DB
        /// </summary>
        private UltraTreeNode AddNewJurisdictionRecord(string xNewJurisdictionName, UltraTreeNode oParentNode)
        {
            // GLOG : 3217 : JAB
            // Initialize the new node value since it will be returned from this method.
            UltraTreeNode oNewNode = null;
            int iParentJurisdictionID = (int)((object[])oParentNode.Tag)[0];

            //Create a new Jurisdiction record
            Jurisdictions oJurisdictions = CreateJurisditionFromNode(oParentNode, iParentJurisdictionID);
            Jurisdiction oNewJurisdiction = (Jurisdiction)oJurisdictions.Create();

            //Set the properties of the new Jurisdiction
            oNewJurisdiction.ParentID = iParentJurisdictionID;
            oNewJurisdiction.Name = xNewJurisdictionName;

            try
            {
                //Save the new Jurisdiction to the DB
                oJurisdictions.Save(oNewJurisdiction);
            }
            catch (System.Exception oE)
            {
                //alert
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_CouldNotSaveChangesToDB"), oE);
            }

            //show expansion indicator
            oParentNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;

            oParentNode.Expanded = true;

            if ((bool)((object[])oParentNode.Tag)[1])
            {
                //The selected node has already been
                //expanded and bound- add a node for 
                //the new record

                //Create a node for the new record
                oNewNode = new UltraTreeNode("", oNewJurisdiction.Name);

                //Create the tag for the new node
                object[] aNodeIDandIsBound = new object[2];
                aNodeIDandIsBound[0] = oNewJurisdiction.ID;
                aNodeIDandIsBound[1] = false;

                //Set the tag for the new node
                oNewNode.Tag = aNodeIDandIsBound;

                //add key to node showing level and id
                oNewNode.Key = oParentNode.Level.ToString() + "." + oNewJurisdiction.ID;

                //Add the new node as a child of the selected node
                try
                {
                    oNewNode = oParentNode.Nodes[oNewNode.Key];
                    
                    if (oNewNode == null)
                    {
                        oParentNode.Nodes.Add(oNewNode);
                    }
                }
                catch
                {
                    oParentNode.Nodes.Add(oNewNode);
                }

                oNewNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;

                // Set the new node as the selected node.
                oNewNode.Selected = true;
            }

            return oNewNode;
        }

        // GLOG : 3217 : JAB
        // Store the node whose children will be copied to a targetted node.
        UltraTreeNode m_oSourceJurisdictionNode = null;

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Update the view of context menu prior to showing its content.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmsJurisdictionOps_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                UpdateContextMenuView();
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Set the display properties of context menu items.
        /// </summary>
        private void UpdateContextMenuView()
        {
            // Only enable the paste menu item if an item to copy exists.
            this.tsmiPaste.Enabled = (this.treeJurisdictions.SelectedNodes.Count > 0) && 
                                        (m_oSourceJurisdictionNode != null) && 
                                        (this.treeJurisdictions.SelectedNodes[0] != this.m_oSourceJurisdictionNode);

            // Only enable the copy menu item if an item is selected.
            this.tsmiCopy.Enabled = (this.treeJurisdictions.SelectedNodes.Count > 0);
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Handle clicking of an item in the context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmsJurisdictionOps_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                // Close the context menu. This is necessary since the dialog box that prompts the
                // user about overwritting existing nodes would display under the context menu.
                this.cmsJurisdictionOps.Close();

                if(e.ClickedItem == this.tsmiCopy)
                {
                    CopyJurisdictionChildren();
                }

                if(e.ClickedItem == this.tsmiPaste)
                {
                    PasteJurisdictionChildren(this.treeJurisdictions.SelectedNodes[0], this.m_oSourceJurisdictionNode);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Copy the children of the copied jurisdiction to the selected node.
        /// </summary>
        private void CopyJurisdictionChildren()
        {
            if (this.treeJurisdictions.SelectedNodes.Count > 0)
            {
                this.m_oSourceJurisdictionNode = this.treeJurisdictions.SelectedNodes[0];
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Paste the children of the selected jurisdiction.
        /// </summary>
        private void PasteJurisdictionChildren(UltraTreeNode oTargetNode, UltraTreeNode oSourceNode)
        {
            if (oTargetNode != null && oSourceNode != null)
            {
                // Populate the target node with its children nodes and the children's descendents since
                // we will need to check if nodes to be added already exist in the target node's nodes.
                this.treeJurisdictions.PopulateParentNode(oTargetNode);

                // Populate the selected node with its children nodes and the children's descendents.
                this.treeJurisdictions.PopulateParentNode(oSourceNode);
                                    
                UltraTreeNode oExistingNode = null;

                foreach (UltraTreeNode oSourceChildNode in oSourceNode.Nodes)
                {
                    bool bAddNode = true;

                    try
                    {
                        oExistingNode = GetNodeByName(oTargetNode, oSourceChildNode.Text);

                        if (oExistingNode != null)
                        {
                            DialogResult oChoice = MessageBox.Show("A jurisdiction named " + oSourceChildNode.Text + " already exists. Overwrite " + oExistingNode.FullPath + "?", LMP.String.MacPacProductName, MessageBoxButtons.YesNo);

                            if (oChoice == DialogResult.No)
                            {
                                bAddNode = false;
                            }
                            else if (oChoice == DialogResult.Yes)
                            {
                                // Delete the pre-existing target node before proceeding.
                                this.DeleteNode(oExistingNode);
                            }
                        }
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Error.Show(oE);
                    }

                    UltraTreeNode oTargetNodeChildNode;

                    if (bAddNode)
                    {
                        oTargetNodeChildNode = AddNewJurisdictionRecord(oSourceChildNode.Text, oTargetNode);
                    }
                    else
                    {
                        // We found a duplicate node and chose not to overwrite it.
                        // Set the oTargetNodeChildNode to the node that existed but not overwritten.
                        // This allows additions to be made to the existing node as opposed to
                        // the case where the existing node is deleted and replaced with the new
                        // nodes.
                        oTargetNodeChildNode = oExistingNode;
                    }

                    if (oTargetNodeChildNode != null)
                    {
                        PasteJurisdictionChildren(oTargetNodeChildNode, oSourceChildNode);
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Get the node in the specified node's node collection whose text property
        /// has the matching name.
        /// </summary>
        /// <param name="oTargetNode"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private UltraTreeNode GetNodeByName(UltraTreeNode oTargetNode, string xName)
        {
            // JAB TODO : This is a linear search. Discuss alternative with DF.
            for (int i = 0; i < oTargetNode.Nodes.Count; i++)
            {
                if (oTargetNode.Nodes[i].Text == xName)
                {
                    return oTargetNode.Nodes[i];
                }
            }

            return null;
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// If a right click is performed on a node, select it prior to the context menu opening.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeJurisdictions_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if ((e.Button & MouseButtons.Right) != 0)
                {
                    UltraTreeNode oNode = this.treeJurisdictions.GetNodeFromPoint(e.X, e.Y);
                    
                    if (oNode != null)
                    {
                        oNode.Selected = true;
                        this.cmsJurisdictionOps.Show(Control.MousePosition);
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}

