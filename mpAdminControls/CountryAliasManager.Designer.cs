namespace LMP.Administration.Controls
{
    partial class CountryAliasManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.grdCountries = new System.Windows.Forms.DataGridView();
            this.lblCountries = new System.Windows.Forms.Label();
            this.grdAliases = new LMP.Controls.DataGridView();
            this.tsStates = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAliasesDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lblAliases = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCountries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAliases)).BeginInit();
            this.tsStates.SuspendLayout();
            this.SuspendLayout();
            // 
            // scMain
            // 
            this.scMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scMain.Location = new System.Drawing.Point(-2, -2);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.Controls.Add(this.grdCountries);
            this.scMain.Panel1.Controls.Add(this.lblCountries);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.grdAliases);
            this.scMain.Panel2.Controls.Add(this.tsStates);
            this.scMain.Panel2.Controls.Add(this.lblAliases);
            this.scMain.Size = new System.Drawing.Size(697, 540);
            this.scMain.SplitterDistance = 314;
            this.scMain.TabIndex = 0;
            // 
            // grdCountries
            // 
            this.grdCountries.AllowUserToAddRows = false;
            this.grdCountries.AllowUserToDeleteRows = false;
            this.grdCountries.AllowUserToResizeRows = false;
            this.grdCountries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCountries.BackgroundColor = System.Drawing.Color.White;
            this.grdCountries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdCountries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCountries.ColumnHeadersVisible = false;
            this.grdCountries.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grdCountries.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdCountries.Location = new System.Drawing.Point(3, 30);
            this.grdCountries.MultiSelect = false;
            this.grdCountries.Name = "grdCountries";
            this.grdCountries.ReadOnly = true;
            this.grdCountries.RowHeadersVisible = false;
            this.grdCountries.RowHeadersWidth = 25;
            this.grdCountries.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCountries.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdCountries.Size = new System.Drawing.Size(310, 503);
            this.grdCountries.TabIndex = 0;
            this.grdCountries.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_CellEnter);
            this.grdCountries.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_RowEnter);
            this.grdCountries.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_RowLeave);
            this.grdCountries.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdCountries_KeyDown);
            // 
            // lblCountries
            // 
            this.lblCountries.AutoSize = true;
            this.lblCountries.BackColor = System.Drawing.Color.Transparent;
            this.lblCountries.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountries.Location = new System.Drawing.Point(6, 7);
            this.lblCountries.Name = "lblCountries";
            this.lblCountries.Size = new System.Drawing.Size(118, 17);
            this.lblCountries.TabIndex = 20;
            this.lblCountries.Text = "&Office Countries";
            // 
            // grdAliases
            // 
            this.grdAliases.AllowUserToResizeRows = false;
            this.grdAliases.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdAliases.BackgroundColor = System.Drawing.Color.White;
            this.grdAliases.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdAliases.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAliases.ColumnHeadersVisible = false;
            this.grdAliases.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdAliases.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdAliases.IsDirty = false;
            this.grdAliases.Location = new System.Drawing.Point(0, 30);
            this.grdAliases.MultiSelect = false;
            this.grdAliases.Name = "grdAliases";
            this.grdAliases.RowHeadersVisible = false;
            this.grdAliases.RowHeadersWidth = 25;
            this.grdAliases.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdAliases.Size = new System.Drawing.Size(373, 503);
            this.grdAliases.SupportingValues = "";
            this.grdAliases.TabIndex = 28;
            this.grdAliases.Tag2 = null;
            this.grdAliases.Value = null;
            this.grdAliases.ValueChanged += new LMP.Controls.ValueChangedHandler(this.grdAliases_ValueChanged);
            this.grdAliases.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdAliases_CellValidating);
            this.grdAliases.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdAliases_RowValidated);
            // 
            // tsStates
            // 
            this.tsStates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsStates.AutoSize = false;
            this.tsStates.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsStates.Dock = System.Windows.Forms.DockStyle.None;
            this.tsStates.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsStates.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.tbtnAliasesDelete,
            this.toolStripSeparator4});
            this.tsStates.Location = new System.Drawing.Point(85, -3);
            this.tsStates.Name = "tsStates";
            this.tsStates.Size = new System.Drawing.Size(288, 37);
            this.tsStates.TabIndex = 27;
            this.tsStates.Text = "toolStrip1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 37);
            // 
            // tbtnAliasesDelete
            // 
            this.tbtnAliasesDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAliasesDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAliasesDelete.Name = "tbtnAliasesDelete";
            this.tbtnAliasesDelete.Size = new System.Drawing.Size(66, 34);
            this.tbtnAliasesDelete.Text = "&Delete...";
            this.tbtnAliasesDelete.ToolTipText = "Delete selected alias";
            this.tbtnAliasesDelete.Click += new System.EventHandler(this.tbtnAliasesDelete_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 37);
            // 
            // lblAliases
            // 
            this.lblAliases.AutoSize = true;
            this.lblAliases.BackColor = System.Drawing.Color.Transparent;
            this.lblAliases.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAliases.Location = new System.Drawing.Point(4, 7);
            this.lblAliases.Name = "lblAliases";
            this.lblAliases.Size = new System.Drawing.Size(54, 17);
            this.lblAliases.TabIndex = 26;
            this.lblAliases.Text = "&Aliases";
            // 
            // CountryAliasManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scMain);
            this.Name = "CountryAliasManager";
            this.Size = new System.Drawing.Size(692, 537);
            this.Load += new System.EventHandler(this.CountryAliasManager_Load);
            this.Enter += new System.EventHandler(this.CountryAliasManager_Enter);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel1.PerformLayout();
            this.scMain.Panel2.ResumeLayout(false);
            this.scMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCountries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAliases)).EndInit();
            this.tsStates.ResumeLayout(false);
            this.tsStates.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.Label lblCountries;
        private System.Windows.Forms.DataGridView grdCountries;
        private System.Windows.Forms.ToolStrip tsStates;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnAliasesDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label lblAliases;
        private LMP.Controls.DataGridView grdAliases;
    }
}
