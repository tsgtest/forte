namespace LMP.Administration.Controls
{
    partial class ExternalConnectionsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdConnections = new System.Windows.Forms.DataGridView();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.grdConnections)).BeginInit();
            this.tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdConnections
            // 
            this.grdConnections.AllowUserToAddRows = false;
            this.grdConnections.AllowUserToDeleteRows = false;
            this.grdConnections.AllowUserToResizeRows = false;
            this.grdConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdConnections.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdConnections.BackgroundColor = System.Drawing.Color.White;
            this.grdConnections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdConnections.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdConnections.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdConnections.Location = new System.Drawing.Point(0, 29);
            this.grdConnections.MultiSelect = false;
            this.grdConnections.Name = "grdConnections";
            this.grdConnections.RowHeadersVisible = false;
            this.grdConnections.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdConnections.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdConnections.Size = new System.Drawing.Size(453, 372);
            this.grdConnections.TabIndex = 0;
            this.grdConnections.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdConnections_CellFormatting);
            this.grdConnections.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdConnections_CellValidating);
            this.grdConnections.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdConnections_ColumnAdded);
            this.grdConnections.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdConnections_DataError);
            this.grdConnections.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdConnections_RowValidating);
            this.grdConnections.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdConnections_KeyDown);
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.tsNew,
            this.toolStripSeparator2,
            this.tsDelete,
            this.toolStripSeparator3});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(454, 25);
            this.tsMain.TabIndex = 1;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.toolStripLabel1.Size = new System.Drawing.Size(150, 22);
            this.toolStripLabel1.Text = "External Connections";
            this.toolStripLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsNew
            // 
            this.tsNew.AutoSize = false;
            this.tsNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsNew.Name = "tsNew";
            this.tsNew.Size = new System.Drawing.Size(54, 22);
            this.tsNew.Text = "&New...";
            this.tsNew.Click += new System.EventHandler(this.tsNew_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsDelete
            // 
            this.tsDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsDelete.Name = "tsDelete";
            this.tsDelete.Size = new System.Drawing.Size(54, 22);
            this.tsDelete.Text = "&Delete...";
            this.tsDelete.Click += new System.EventHandler(this.tsDelete_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // ExternalConnectionsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.tsMain);
            this.Controls.Add(this.grdConnections);
            this.Name = "ExternalConnectionsManager";
            this.Load += new System.EventHandler(this.ExternalConnectionsManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdConnections)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.DataGridView grdConnections;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsDelete;
        private System.Windows.Forms.ToolStripButton tsNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}
