﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMP.Data;

namespace OxmlUtilities
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.tsslStatus.Text = "Conversion in progress.  Please wait...";
            this.Refresh();

            //connect to admin db
            LocalConnection.ConnectToDB(true);
            OleDbConnection oCn = LocalConnection.ConnectionObject;

            //fill datatable with segment xml
            using (OleDbDataAdapter oAdapter = new OleDbDataAdapter("Select ID, XML from Segments", oCn))
            {
                oAdapter.AcceptChangesDuringUpdate = true;
                OleDbCommandBuilder oBuilder = new OleDbCommandBuilder(oAdapter);
                oAdapter.UpdateCommand = oBuilder.GetUpdateCommand();

                DataTable oDT = new DataTable("SegmentXML");
                oAdapter.Fill(oDT);

                //cycle through each row, converting the xml to opc
                foreach (DataRow oDR in oDT.Rows)
                {
                    string xXml = oDR["XML"].ToString();

                    if (!string.IsNullOrEmpty(xXml))
                    {
                        try
                        {
                            string xOPC = LMP.Architect.Oxml.XmlSegment.GetOPCFromFlatXml(xXml);
                            oDR["XML"] = xOPC;
                        }
                        catch { }
                    }
                }

                //update table
                oAdapter.Update(oDT);
                LocalConnection.Close();

                try
                {
                    //compact db
                    LMP.Data.Application.CompactAdminDB();
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE, "Could not compact the database.");
                }

                this.tsslStatus.Text = "";
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.tsslStatus.Text = "Conversion in progress.  Please wait...";
            this.Refresh();

            //connect to admin db
            LocalConnection.ConnectToDB(true);
            OleDbConnection oCn = LocalConnection.ConnectionObject;

            //fill datatable with segment opc            
            using (OleDbDataAdapter oAdapter = new OleDbDataAdapter("Select ID, XML from Segments", oCn))
            {
                oAdapter.AcceptChangesDuringUpdate = true;
                OleDbCommandBuilder oBuilder = new OleDbCommandBuilder(oAdapter);
                oAdapter.UpdateCommand = oBuilder.GetUpdateCommand();

                DataTable oDT = new DataTable("SegmentOPC");
                oAdapter.Fill(oDT);

                //cycle through each row, converting the opc to flat xml
                foreach (DataRow oDR in oDT.Rows)
                {
                    string xOPC = oDR["XML"].ToString();

                    if (!string.IsNullOrEmpty(xOPC))
                    {
                        try
                        {
                            string xFlatXml = null;
                            if (LMP.String.IsBase64SegmentString(xOPC))
                                xFlatXml = LMP.Architect.Oxml.XmlSegment.GetFlatXmlFromBase64OpcString(xOPC);
                            else
                                xFlatXml = xOPC;

                            oDR["XML"] = xFlatXml;
                        }
                        catch { }
                    }
                }

                //update table
                oAdapter.Update(oDT);
                LocalConnection.Close();

                try
                {
                    //compact db
                    LMP.Data.Application.CompactAdminDB();
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE, "Could not compact database.");
                }

                this.tsslStatus.Text = "";
                this.Close();
            }

        }
    }
}
