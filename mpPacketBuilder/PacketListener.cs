﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public class PacketRequestListener
    {
        private Timer m_oTimer;
        private IPacketBuilderHelper m_oPacketBuilderBackend;

        public PacketRequestListener()
        {
            m_oTimer = new Timer();
            m_oTimer.Interval = 5000;
            m_oTimer.Tick += new EventHandler(m_oTimer_Tick);
        }

        void m_oTimer_Tick(object sender, EventArgs e)
        {
            string xClientMatterID = null;
            string xPacketID = null;
            string xPrefill = null;
            string xAdditionalDetail = null;

            //allow suspension of unattended mode
            if (!LMP.MacPac.MacPacImplementation.IsServer)
            {
                return;
            }

            m_oTimer.Stop();

            LMP.MacPac.IPacketBuilderHelper oHelper = GetPacketBuilderBackend();

            try
            {
                //get packet detail
                oHelper.GetPendingPacketInformation(ref xClientMatterID, ref xPacketID, ref  xPrefill, ref xAdditionalDetail);

                if (!string.IsNullOrEmpty(xClientMatterID))
                {
                    //insert packet
                    Segment[] aSegments = LMP.MacPac.Application.InsertSegmentPacket(xPacketID, xPrefill);

                    //save packet documents
                    foreach (Segment oSegment in aSegments)
                    {
                        oHelper.SaveFile(oSegment.MPDocument.WordDocument, xClientMatterID,
                            oSegment.DisplayName, oSegment.TypeID.ToString(), xAdditionalDetail);
                    }

                    //write back to db
                    oHelper.WriteBack(true, ref xAdditionalDetail);
                }
            }
            catch (System.Exception oE)
            {
                object oMissing = System.Reflection.Missing.Value;
                object oFalse = false;

                //close open documents
                LMP.MPCOMObjects.cWordApp.CurrentWordApplication()
                    .Documents.Close(ref oFalse, ref oMissing, ref oMissing);

                //write error message back to table
                xAdditionalDetail += "|" + oE.Message + "\v" + oE.StackTrace;
                oHelper.WriteBack(false, ref xAdditionalDetail);

                //write message through normal Forte channel - this will
                //send an e-mail to the administrator
                //if Forte is configure to do so
                LMP.Error.Show(oE, "Error in PacketRequestListener: ClientMatter/Case Number: " + 
                    xClientMatterID + "; PacketID: " + xPacketID);
            }
            finally
            {
                m_oTimer.Start();
            }
        }

        public void StartListening()
        {
            m_oTimer.Start();
        }

        public void StopListening()
        {
            m_oTimer.Stop();
        }

        private IPacketBuilderHelper GetPacketBuilderBackend()
        {
            if (m_oPacketBuilderBackend == null)
            {
                //retrieve assembly and class name of object to create
                string xAsmName = LMP.Registry.GetMacPac10Value("GrailAssemblyName");
                string xClassName = LMP.Registry.GetMacPac10Value("PacketBuilderClassName");

                if (string.IsNullOrEmpty(xAsmName))
                {
                    throw new LMP.Exceptions.RegistryKeyException(
                        LMP.Resources.GetLangString("Msg_MissingAsmNameRegKey"));
                }
                else if (string.IsNullOrEmpty(xClassName))
                {
                    throw new LMP.Exceptions.RegistryKeyException(
                        LMP.Resources.GetLangString("Msg_MissingClassNameRegKey"));
                }

                string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAsmName;

                Assembly oAsm = null;

                try
                {
                    //load assembly
                    oAsm = Assembly.LoadFrom(xAsmDllFullName);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
                }

                try
                {
                    //create class instance
                    m_oPacketBuilderBackend = (IPacketBuilderHelper)oAsm.CreateInstance(xClassName);

                    if (m_oPacketBuilderBackend == null)
                    {
                        throw new LMP.Exceptions.ResourceException(
                            LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                    }

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
                }
            }

            return m_oPacketBuilderBackend;
        }
    }
}
