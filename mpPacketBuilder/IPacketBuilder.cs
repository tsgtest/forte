﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public interface IPacketBuilderHelper
    {
        void GetPendingPacketInformation(ref string xClientMatterID, ref string xPacketID, ref string xPrefill, ref string xAdditionalDetail);

        void SaveFile(Word.Document oDoc, string xClientMatterID, string xDocName, string xDocType, string xAdditionalDetail);

        void WriteBack(bool bSuccess, ref string xAdditionalDetail);
    }
}
