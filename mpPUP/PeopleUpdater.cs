using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration
{
    static class PeopleUpdater
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] oArgs)
        {
            try
            {
                bool bUpdateLicenses = false;
                bool bPromptForSource = false;
                string xSourceServer = null;
                string xSourceDatabase = null;
                string xTargetServer = null;
                string xTargetDatabase = null;
                string xIISServer = null;
                string xServerApplicationName = null;
                bool bPublishAfterUpdate = false;

                foreach (string oArg in oArgs)
                {
                    if (oArg.ToUpper() == "-P")
                        bPromptForSource = true;
                }

                //GLOG 8220: Connect to ForteLocalPeople in Local mode
                LMP.Data.LocalConnection.ConnectToDB(true, LMP.MacPac.MacPacImplementation.IsFullLocal == true);

                //get pup options from db metadata
                string xOptions = LMP.Data.Application.GetMetadata("PUPOptions");

                if (xOptions == "" || bPromptForSource)
                {
                    System.Windows.Forms.Application.EnableVisualStyles();
                    System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

                    PUPForm oForm = new PUPForm();

                    if (xOptions != "")
                    {
                        string[] aOptions = xOptions.Split('|');

                        oForm.SourceServer = aOptions[0];
                        oForm.SourceDatabase = aOptions[1];
                        oForm.TargetServer = aOptions[2];
                        oForm.TargetDatabase = aOptions[3];
                        oForm.PublishAfterUpdate = Boolean.Parse(aOptions[4]);
                        oForm.UpdateLicenses = Boolean.Parse(aOptions[5]);

                        try
                        {
                            oForm.IISServer = aOptions[6];
                            oForm.ServerApplicationName = aOptions[7];
                        }
                        catch
                        {
                            //IIS server options not included -
                            //this may be the case with values stored
                            //in macpac versions that did not expose
                            //IIS server options -
                            //use reg value and "mp10SyncServer"
                            oForm.IISServer = LMP.Registry.GetLocalMachineValue(
                                LMP.Data.ForteConstants.mpSyncRegKey, "ServerIP");
                            oForm.ServerApplicationName = "ForteSyncServer";
                        }
                    }

                    DialogResult iRes = oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    if (iRes == DialogResult.Cancel)
                    {
                        return;
                    }
                    else
                    {
                        xSourceServer = oForm.SourceServer;
                        xSourceDatabase = oForm.SourceDatabase;
                        xTargetServer = oForm.TargetServer;
                        xTargetDatabase = oForm.TargetDatabase;
                        bPublishAfterUpdate = oForm.PublishAfterUpdate;
                        bUpdateLicenses = oForm.UpdateLicenses;
                        xIISServer = oForm.IISServer;
                        xServerApplicationName = oForm.ServerApplicationName;

                        //create options string
                        StringBuilder oSB = new StringBuilder();
                        oSB.AppendFormat("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}",
                            xSourceServer, xSourceDatabase, xTargetServer,
                            xTargetDatabase, bPublishAfterUpdate, bUpdateLicenses,
                            xIISServer, xServerApplicationName);

                        //store options as metadata
                        LMP.Data.Application.SetMetadata("PUPOptions", oSB.ToString());
                    }
                }
                else
                {
                    string[] aOptions = xOptions.Split('|');

                    xSourceServer = aOptions[0];
                    xSourceDatabase = aOptions[1];
                    xTargetServer = aOptions[2];
                    xTargetDatabase = aOptions[3];
                    bPublishAfterUpdate = Boolean.Parse(aOptions[4]);
                    bUpdateLicenses = Boolean.Parse(aOptions[5]);

                        try
                        {
                            xIISServer = aOptions[6];
                            xServerApplicationName = aOptions[7];
                        }
                        catch
                        {
                            //IIS server options not included -
                            //this may be the case with values stored
                            //in macpac versions that did not expose
                            //IIS server options -
                            //use reg value and "mp10SyncServer"
                            xIISServer = LMP.Registry.GetLocalMachineValue(
                                LMP.Data.ForteConstants.mpSyncRegKey, "ServerIP");
                            xServerApplicationName = "ForteSyncServer";
                        }
                }

                //execute PUP
                //GLOG 8149: Don't attempt publish if error occurs during queries
                bool bError = false;
                LMP.Data.PUP.Execute(bUpdateLicenses, xSourceServer, xSourceDatabase, out bError, false);

                if (!bError && bPublishAfterUpdate && !LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    try
                    {
                        //perform people sync with network db- this
                        //will deploy the latest people detail
                        LMP.MacPac.Sync.AdminSyncClient oSync = new LMP.MacPac.Sync.AdminSyncClient(
                            xIISServer, xServerApplicationName);

                        oSync.SyncPeopleUp(xTargetServer, xTargetDatabase);
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Error.Show(oE);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }
        }
   }
}