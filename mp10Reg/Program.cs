using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using Win32 = Microsoft.Win32;

namespace mp10Reg
{
    /// <summary>
    /// this application creates and updates the registry keys
    /// that allows Forte.dll to be used by COM
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] aArgs)
        {
            try
            {
                bool bMessage = true;
                //GLOG 8499: No longer COM components, so no need for command line arguments other than /s
                //string xArgs = string.Join(" ", aArgs).ToUpper();
                //if (xArgs.Contains("/T") && xArgs.Contains("/C"))
                //{
                //    RegisterForteTLB();
                //    RegisterMP10COMComponents();
                //}
                //else if (xArgs.Contains("/T"))
                //{
                    RegisterForteTLB();
                //}
                //else if (xArgs.Contains("/C"))
                //{
                //    RegisterMP10COMComponents();
                //}
                //else
                //{
                //    RegisterForteTLB();
                //    RegisterMP10COMComponents();
                //}

                //GLOG 4412
                foreach (string xArg in aArgs)
                {
                    if (xArg.ToUpper().Substring(0, 2) == "/S")
                    {
                        bMessage = false;
                    }
                }

                //message if necessary
                if (bMessage)
                {
                    //MessageBox.Show("All " + LMP.ComponentProperties.ProductName + " components were registered.",
                    //               LMP.ComponentProperties.ProductName);
                    MessageBox.Show("All components were registered.", "Forte");
                }
             }
            catch (System.Exception oE)
            {
                System.Windows.Forms.MessageBox.Show(oE.Message, 
                    "Registration Error", MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// writes all registry keys necessary to
        /// register Forte.tlb for use with COM
        /// </summary>
        public static void RegisterForteTLB()
        {
            Assembly oAsm = Assembly.GetExecutingAssembly();
            string xCodeBase = oAsm.CodeBase;
            xCodeBase = xCodeBase.Replace("ForteReg.exe", "Forte.dll");
            string xDLL = Assembly.GetExecutingAssembly().Location;
            xDLL = xDLL.Replace("ForteReg.exe", "Forte.dll");

            string xVersion = oAsm.GetName().Version.ToString();

            Win32.RegistryKey oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"LMP.MacPac.Application");
            oKey.SetValue("", "LMP.MacPac.Application");

            oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"LMP.MacPac.Application\CLSID");
            oKey.SetValue("", "{364CAAEC-1B64-4BA4-BD97-6DC34CC9BE0D}");

            oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"CLSID\{364CAAEC-1B64-4BA4-BD97-6DC34CC9BE0D}");
            oKey.SetValue("", "LMP.MacPac.Application");

            try
            {
                Win32.Registry.ClassesRoot.DeleteSubKeyTree(
                    @"CLSID\{364CAAEC-1B64-4BA4-BD97-6DC34CC9BE0D}\InprocServer32");
            }
            catch { }

            oKey = Win32.Registry.ClassesRoot.CreateSubKey(
                    @"CLSID\{364CAAEC-1B64-4BA4-BD97-6DC34CC9BE0D}\InprocServer32");
            oKey.SetValue("", "mscoree.dll");
            oKey.SetValue("Assembly", "MacPac10, Version=" + xVersion +
                ", Culture=neutral, PublicKeyToken=fea0311d95e483b6");
            oKey.SetValue("Class", "LMP.MacPac.Application");
            oKey.SetValue("CodeBase", xCodeBase);
            oKey.SetValue("RuntimeVersion", "v4.0.30319");
            oKey.SetValue("ThreadingModel", "Both");

            Win32.RegistryKey oSubKey = oKey.CreateSubKey(xVersion);
            oSubKey.SetValue("Assembly", "MacPac10, Version=" + xVersion +
                ", Culture=neutral, PublicKeyToken=fea0311d95e483b6");
            oSubKey.SetValue("Class", "LMP.MacPac.Application");
            oSubKey.SetValue("CodeBase", xCodeBase);
            oSubKey.SetValue("RuntimeVersion", "v4.0.30319");

            //register tlb file
            string xRoot = @"TypeLib\{F7549D0E-F9C7-3F91-9459-FFADD5BFD4E6}";
            
            oKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot);

            //split version into major and minor
            string xMajor = String.Format("{0:x}", oAsm.GetName().Version.Major);
            string xMinor = String.Format("{0:x}", oAsm.GetName().Version.Minor);
            string xHexVersion = xMajor + "." + xMinor;

            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + @"\" + xHexVersion);
            oSubKey.SetValue("", "MacPac10");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\0");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\0\\win32");
            string xTlb = xDLL.Replace("Forte.dll", "Forte.tlb");
            oSubKey.SetValue("", xTlb);
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\FLAGS");
            oSubKey.SetValue("", "0");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\HELPDIR");
            oSubKey.SetValue("", xDLL.Replace("\\Forte.dll", ""));
        }

        /// <summary>
        /// registers the COM components used by Forte
        /// </summary>
        public static void RegisterMP10COMComponents()
        {
            //GLOG 4837: Append to path, since Replace is case sensitive
            string[] xFiles = {};
            string xPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string xRegsvr = Environment.GetFolderPath(Environment.SpecialFolder.System) + "\\regsvr32.exe ";

            foreach (string xFile in xFiles)
            {
                string xDLL = xPath + "\\" + xFile;
                System.Diagnostics.Process.Start(xRegsvr, '"'.ToString() + xDLL + '"'.ToString() + " /s");
            }
        }
    }
}