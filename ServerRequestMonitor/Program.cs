﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

namespace ServerRequestMonitor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            while (true)
            {
                Process[] oWordProcesses = Process.GetProcessesByName("Winword");

                if (oWordProcesses.Length == 0)
                {
                    //give Word time to close, in case it is 
                    //currently in the process of closing
                    Thread.Sleep(10000);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = "winword.exe";
                    //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    Process.Start(startInfo);
                }

                Thread.Sleep(4000);
            }
        }
    }
}
