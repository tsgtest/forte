using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LMP.MacPac.Sync;
using System.IO;

namespace MPDesktopSyncProcess
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private const string mpLastSyncFailedRegValueName = "LastSyncFailed";
        private static UserSyncClient m_oSyncClient;
        private static int m_iFailedSyncAttempts = 0;

        [STAThread]
        static void Main(string[] aArgs)
        {
            bool bForceUpdate = false;
            bool bOnDemand = false;

            try
            {
                string xDBServer = "";
                string xDatabase = "";
                string xIISAddress = "";
                string xServerApplicationName = "";
                string xDateCreated = "";
                int iUserID = 0;
                bool bUpdateStructure = false;
                foreach (string xArg in aArgs)
                {
                    if (xArg.Length < 2)
                        continue;
                    //GLOG 2750: command line parameter to force update
                    if (xArg.ToUpper().Substring(0, 2) == "/F")
                    {
                        bForceUpdate = true;
                        LMP.Trace.WriteNameValuePairs("bForceUpdate", bForceUpdate);
                        continue;
                    }
                    //JTS 08/12/04: command line switches used for On-Demand sync
                    if (xArg.ToUpper().Substring(0, 2) == "/O")
                    {
                        //Execute On-demand sync
                        bOnDemand = true;
                        continue;
                    }

                    if (xArg.Length < 3)
                        continue;
                    if (xArg.ToUpper().Substring(0, 3) == "/U:")
                    {
                        //User ID
                        iUserID = Int32.Parse(xArg.Substring(3).Trim());
                        continue;
                    }
                    if (xArg.ToUpper().Substring(0, 3) == "/S:")
                    {
                        //SQL Server Address
                        xDBServer = xArg.Substring(3).Trim();
                        continue;
                    }
                    if (xArg.ToUpper().Substring(0, 3) == "/D:")
                    {
                        //SQL Server Database
                        xDatabase = xArg.Substring(3).Trim();
                        continue;
                    }
                    if (xArg.ToUpper().Substring(0, 3) == "/I:")
                    {
                        //IIS Server address
                        xIISAddress = xArg.Substring(3).Trim();
                        continue;
                    }
                    if (xArg.ToUpper().Substring(0, 3) == "/C:")
                    {
                        //Creation Date of User DB
                        xDateCreated = xArg.Substring(3).Trim();
                        continue;
                    }

                }
                //get User ID for sync
                bool bRet = false;
                if (iUserID == 0)
                {
                    try
                    {
                        LMP.Trace.WriteNameValuePairs("Environment.UserName", Environment.UserName);
                        LMP.Data.Application.WriteDBLog("DesktopSync: Login"); //GLOG 7546
                        bRet = (LMP.Data.Application.Login(Environment.UserName, false));
                        iUserID = LMP.Data.Application.User.ID;
                    }
                    catch (LMP.Exceptions.InvalidUserException)
                    {
                        //JTS 3/2812: Don't attempt repair if database is readable but there is no matching ID in People table
                        //Ignore invalid user error and default to Guest ID
                        iUserID = LMP.Data.ForteConstants.mpGuestPersonID;
                        bRet = true;
                    }
                    catch
                    {
                        //GLOG item #5638 - dcf - 8/12/11
                        if (m_iFailedSyncAttempts == 0) //JTS 12/28/11
                        {
                            LMP.Trace.WriteInfo("SyncFailed");
                            m_iFailedSyncAttempts++;

                            LMP.Data.Application.WriteDBLog("DesktopSync: Logout (error handler)"); //GLOG 7546
                            //GLOG 5857: Release Forte.mdb
                            LMP.Data.Application.Logout();

                            //GLOG : 8883 : jsw
                            if (!LMP.Registry.Is64Bit())
                            {
                                //attempt sync again after compacting the dbs
                                LMP.Data.Application.RepairUserDBs();
                            }

                            //run again
                            Main(aArgs);

                            return;
                        }

                        //Ignore invalid user error and default to Guest ID
                        bRet = true;
                        iUserID = LMP.Data.ForteConstants.mpGuestPersonID;
                    }
                }
                else
                    //User ID was passed on command line
                    bRet = true;

                LMP.Trace.WriteNameValuePairs("iUserID", iUserID, "bRet", bRet);

                if (bRet)
                {
                    if (iUserID == LMP.Data.ForteConstants.mpGuestPersonID && !bOnDemand)
                    {
                        //person is not in the local db -
                        //sync using the user's system login name
                        xIISAddress = LMP.Data.Application.GetMetadata("IISServerIPAddress");
                        xServerApplicationName = LMP.Data.Application.GetMetadata("ServerApplicationName");
                        xDBServer = LMP.Data.Application.GetMetadata("DefaultServer");
                        xDatabase = LMP.Data.Application.GetMetadata("DefaultDatabase");
                        xDateCreated = LMP.Data.Application.GetMetadata("DateCreated");
                        bUpdateStructure = LMP.Data.Application.LocalDBStructureUpdateRequired();

                        LMP.Trace.WriteNameValuePairs("bUpdateStructure", bUpdateStructure);

                        LMP.Data.Application.WriteDBLog("DesktopSync: Logout (pre Initial Sync)"); //GLOG 7546
                        LMP.Data.Application.Logout();

                        m_oSyncClient = new UserSyncClient(Environment.UserName,
                            xDBServer, xDatabase, xIISAddress, xServerApplicationName, xDateCreated);
                    }
                    else
                    {
                        if (!bOnDemand)
                        {
                            //get connection parameters
                            LMP.Data.FirmApplicationSettings oSettings =
                                new LMP.Data.FirmApplicationSettings(iUserID);
                            //JTS 08/12/04: Get any values not passed on command line
                            if (xDBServer == "")
                                xDBServer = oSettings.NetworkDatabaseServer;
                            if (xDatabase == "")
                                xDatabase = oSettings.NetworkDatabaseName;
                            if (xIISAddress == "")
                                xIISAddress = oSettings.IISServerIPAddress;
                            if (xServerApplicationName == "")
                                xServerApplicationName = oSettings.ServerApplicationName;
                            if (xDateCreated == "")
                                xDateCreated = LMP.Data.Application.GetMetadata("DateCreated");
                            oSettings = null;
                            bUpdateStructure = LMP.Data.Application.LocalDBStructureUpdateRequired();
                            LMP.Data.Application.WriteDBLog("DesktopSync: Logout (pre Sync)"); //GLOG 7546
                            LMP.Data.Application.Logout();
                        }

                        LMP.Trace.WriteNameValuePairs("bUpdateStructure", bUpdateStructure);

                        try
                        {
                            m_oSyncClient = new UserSyncClient(iUserID, xDBServer,
                                xDatabase, xIISAddress, xServerApplicationName, xDateCreated, bOnDemand);
                        }
                        catch (System.Exception oE)
                        {
                            //GLOG 4227: Ensure passing to correct error handler
                            throw oE;
                        }
                    }
                    if (bForceUpdate && LMP.Registry.GetCurrentUserValue(LMP.Data.ForteConstants.mpSyncRegKey, "Executing") == "1")
                    {
                        //Executing key may be incorrectly set, prompt before resetting
                        DialogResult iYesNo = MessageBox.Show(
                            LMP.Resources.GetLangString("Prompt_DesktopSyncConfirmUpdate"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, 
                            MessageBoxIcon.Question);

                        if (iYesNo == DialogResult.Yes)
                        {
                            //Reset registry key
                            LMP.Registry.SetCurrentUserValue(
                                LMP.Data.ForteConstants.mpSyncRegKey, "Executing", "0");
                        }
                    }

                    LMP.Trace.WriteNameValuePairs("bOnDemand", bOnDemand);

                    //run if scheduled time is now or has past
                    if (bOnDemand)
                    {                
                        m_oSyncClient.ExecuteOnDemandSync();
                    }
                    else if ((m_oSyncClient.NextScheduledSync <= DateTime.Now.ToUniversalTime()) || bForceUpdate)
                    {
                        try
                        {
                            //JTS 12/15/08: Use parameter indicating whether Local DB structure should be updated,
                            //to avoid extra login when not necessary
                            m_oSyncClient.ExecuteSync(bUpdateStructure);
                        }
                        catch (LMP.Exceptions.SyncRequirementsException oE)
                        {
                            throw oE;
                        }
                        catch (System.Net.WebException oE)
                        {
                            throw oE;
                        }
                        catch (System.UriFormatException oE)
                        {
                            throw oE;
                        }
                        catch (System.Exception oE)
                        {
							//GLOG 7546: Don't attempt repair after error at this point
                            string xMsg = "";
                            try
                            {
                                xMsg = oE.Message;
                            }
                            catch {}
                            LMP.Data.Application.WriteDBLog("DesktopSync Error: " + xMsg);
                            throw oE;
                        }
                    }

                    //GLOG 2895: Clear error flag if sync was successful
                    LMP.Registry.SetCurrentUserValue(LMP.Data.ForteConstants.mpSyncRegKey, 
                        mpLastSyncFailedRegValueName, "0");
                }
            }
            catch (LMP.Exceptions.SyncRequirementsException)
            {

            }
            catch (System.Net.WebException oE)
            {
                //GLOG 2895: If server is not available, display message one time only,
                //then set Registry flag to prevent subsequent displays
                //JTS 7/15/09: Always display message if /F switch was used
                //if (bForceUpdate || LMP.Registry.GetCurrentUserValue(
                //    LMP.Data.MP10Constants.mpSyncRegKey, mpSyncConnectErrorValueName) != "1")
                //9/09: Only display message if /F switch was used
                if (bForceUpdate)
                {
                    DialogResult iRes = MessageBox.Show(oE.Message + "\r\r" +
                        LMP.Resources.GetLangString("Error_SyncNetworkUnavailable"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Exclamation);

                    if (iRes == DialogResult.Yes)
                    {
                        LMP.Error.Show(oE);
                    }
                }

                LMP.Registry.SetCurrentUserValue(
                    LMP.Data.ForteConstants.mpSyncRegKey, mpLastSyncFailedRegValueName, "1");
            }
            catch (System.UriFormatException oE)
            {
                if (bOnDemand)
                    MessageBox.Show(LMP.Resources.GetLangString("Error_SyncNoConnection"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    //GLOG 6254
                    m_oSyncClient.WriteErrorToLog(LMP.Resources.GetLangString(
                        "Error_SyncNoConnection"), oE, m_oSyncClient.LastSuccessfulUserSync);

                    LMP.Registry.SetCurrentUserValue(
                        LMP.Data.ForteConstants.mpSyncRegKey, mpLastSyncFailedRegValueName, "1");
                }
            }
            catch (System.Exception oE)
            {
                if (bOnDemand)
                    //JTS 08/12/04: show different error message for On-Demand sync
                    LMP.Error.Show(oE, LMP.Resources.GetLangString(
                        "Error_OnDemandSyncCouldNotBeExecuted"));
                else
                {
                    if (m_oSyncClient != null)
                    {
                        //GLOG 6254
                        m_oSyncClient.WriteErrorToLog(LMP.Resources.GetLangString(
                            "Error_SyncCouldNotBeCompleted"), oE, m_oSyncClient.LastSuccessfulUserSync);
                    }

                    LMP.Registry.SetCurrentUserValue(
                        LMP.Data.ForteConstants.mpSyncRegKey, mpLastSyncFailedRegValueName, "1");
                }
            }
            finally
            {
                //JTS 9/5/12: Clean up objects before exiting
                if (m_oSyncClient != null)
                {
                    m_oSyncClient.Dispose();
                    m_oSyncClient = null;
                }
                //Application.Exit();
            }
        }
    }
}