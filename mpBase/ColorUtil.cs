using System;
using System.Collections.Generic;
using System.Text;

namespace LMP
{
    public static class ColorUtil
    {
        public static System.Drawing.Color ColorFromString(string xColorTextEntry, out string xInvalidColorEntry)
        {
            // This value can either be a known color name such as Blue, or BLUE, or BlUe 
            // or a RGB value such as 0, 0, 255.

            int iRed = 255;
            int iGreen = 255;
            int iBlue = 255;
            bool bColorCreationSucceeded = true;
            System.Drawing.Color oEnteredColor = System.Drawing.Color.White;

            try
            {

                if(xColorTextEntry != "")
                    oEnteredColor = System.Drawing.Color.FromName(xColorTextEntry);
            }
            catch
            {
                bColorCreationSucceeded = false;
            }

            xInvalidColorEntry = null;

            if (!bColorCreationSucceeded || !oEnteredColor.IsKnownColor)
            {
                // Color entry is probably an rgb value. Attempt to parse and create the color from the rgb values.
                const char cRGBSeparator = ',';

                string[] axRGBEntryValues = xColorTextEntry.Split(cRGBSeparator);

                // We should have 3 values, if not something is wrong and the default color will be used.
                if (axRGBEntryValues.Length == 3)
                {
                    // The values entered should be Int32 parseable. If not the default will be used.
                    try
                    {
                        iRed = Int32.Parse(axRGBEntryValues[0]);

                        if (!IsValidRGBValue(iRed))
                        {
                            xInvalidColorEntry = "The value " + axRGBEntryValues[0] + " for Red is invalid. Value must be in the range from 0 to 255.\n";
                        }
                    }
                    catch
                    {
                        xInvalidColorEntry = "The value " + axRGBEntryValues[0] + " for Red is invalid.\n";
                    }

                    try
                    {
                        iGreen = Int32.Parse(axRGBEntryValues[1]);

                        if (!IsValidRGBValue(iGreen))
                        {
                            xInvalidColorEntry += "The value " + axRGBEntryValues[1] + " for Green is invalid. Value must be in the range from 0 to 255\n.";
                        }
                    }
                    catch
                    {
                        xInvalidColorEntry += "The value " + axRGBEntryValues[1] + " for Green is invalid.\n";
                    }

                    try
                    {
                        iBlue = Int32.Parse(axRGBEntryValues[2]);

                        if (!IsValidRGBValue(iBlue))
                        {
                            xInvalidColorEntry += "The value " + axRGBEntryValues[2] + " for Blue is invalid. Value must be in the range from 0 to 255.\n";
                        }
                    }
                    catch
                    {
                        xInvalidColorEntry += "The value " + axRGBEntryValues[2] + " for Blue is invalid.\n";
                    }

                    if (xInvalidColorEntry == null)
                    {
                        oEnteredColor = System.Drawing.Color.FromArgb(iRed, iGreen, iBlue);
                    }
                }
                else
                {
                    xInvalidColorEntry = "The color " + xColorTextEntry + " is not a known color name.\n";
                }
            }

            return (oEnteredColor);
        }

        private static bool IsValidRGBValue(int iRGBValue)
        {
            return (iRGBValue >= 0 && iRGBValue <= 255);
        }

        private static int LimitRGBValue(int iRGBValue)
        {
            if (iRGBValue > 255)
            {
                return 255;
            }

            if (iRGBValue < 0)
            {
                return 0;
            }

            return (iRGBValue);
        }
    }
}
