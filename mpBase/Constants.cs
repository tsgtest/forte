﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMP.Data
{
    public class ForteConstants
    {
        public const int mpPublicPersonID2 = 0;
        public const int mpFirmRecordID = -999999999;
        public const string mpSyncRegKey = "SOFTWARE\\The Sackett Group\\Deca\\Sync";
        public const int mpGuestPersonID = 999999999;
        public const string mpFixedValueMarker = "~!!~";
        public const string mpInternalTagPrefixID = "SGI";
        public const string mpRequiredDBFields = "|ID1|ID2|OwnerID|UsageState|UserID|DisplayName|LinkedPersonID|Prefix|LastName|FirstName|MI|Suffix|FullName|ShortName|OfficeID|Initials|IsAttorney|IsAuthor|Title|Phone|Fax|EMail|AdmittedIn|LastEditTime|DefaultOfficeRecordID1|OfficeUsageState|LinkedExternally|s";
        public const string mpDataFileEntitySep = "@mp|mp@";
        public const string mpDataFileFieldSep = "@mp;mp@";
        public const string WordOpenXMLNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        public const string MacPacNamespace = "urn-legalmacpac-data/10";
        public const string WordNamespace = "http://schemas.microsoft.com/office/word/2003/wordml";
        public const string AmlNamespace = "http://schemas.microsoft.com/aml/2001/core";
    }
}
