using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace LMP
{
    public class WinAPI
    {
        [DllImport("User32.dll")]
        public static extern Int32 GetKeyState(int nVirtKey);

        [DllImport("User32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        public static extern int LockWindowUpdate(int hwndLock);

        [DllImport("User32.dll")]
        public static extern int FindWindow(string lpClassName, int lpWindowName);

        [DllImport("User32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern int GetFocus();

        [DllImport("user32.dll")]
        public static extern int SetFocus(int lpWindowHandle);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, System.Text.StringBuilder lpClassName, int nMaxCount);
    }
}
