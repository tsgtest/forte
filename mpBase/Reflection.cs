using System;
using System.Reflection;
using System.Diagnostics;
namespace LMP
{
	/// <summary>
	/// contains static members that invoke late binding calls to both COM and .NET -
	/// created by Daniel Fisherman on 12/7/04
	/// </summary>
	public static class Reflection
	{
		/// <summary>
		/// invokes the specified .NET method using the specified arguments -
		/// returns the return value of the invoked method
		/// </summary>
		/// <param name="oSourceObject">object containing method to invoke</param>
		/// <param name="xMethodName">name of method to invoke</param>
		/// <param name="xArgs">delimited string of required arguments</param>
		/// <param name="xArgDelimiter">delimiter used in argument string</param>
		public static object InvokeMethod(object oSourceObject, string xMethodName, 
			string xArgs, string xArgDelimiter)
		{

			if(oSourceObject == null)
				//source object not valid - alert
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_NullObjectArgument"));

			string[] aArgs = null;

			//split argument string into array
			if(xArgs != null && xArgs != "")
			{
				if (xArgDelimiter != null && xArgDelimiter != "")
					aArgs = xArgs.Split(xArgDelimiter.ToCharArray());
				else
					aArgs = new string[1]{xArgs};
			}

			System.Type oType  = oSourceObject.GetType();
			MethodInfo oMethod = oType.GetMethod(xMethodName);

			if(oMethod == null)
			{
				//there is no property with that name
				throw new LMP.Exceptions.MethodNameException(
					string.Concat(LMP.Resources.GetLangString("Error_MethodDoesNotExist"),
					oType.Name, ".", xMethodName));
			}

			ParameterInfo[] aParamInfos = oMethod.GetParameters();

			//create parameter array to handle strongly typed arguments
			object[] aTypedParams = new object[aParamInfos.Length];

			//cycle through method parameters, converting
			//the corresponding argument to the required type
			//and adding to parameter array
			int i = 0;

			try
			{
				foreach(ParameterInfo oParamInfo in aParamInfos)
				{
					if(oParamInfo.ParameterType.IsEnum)
					{
						//parameter is an enum - convert to enum value -
						//first convert argument string to underlying enum type
						System.Type oUnderlyingType = Enum.GetUnderlyingType(oParamInfo.ParameterType);
						
						aTypedParams[i] = Enum.ToObject(oParamInfo.ParameterType, 
							System.Convert.ChangeType(aArgs[i], oUnderlyingType));
					}
					else
						//convert to appropriate type and add to parameter array
						aTypedParams[i] = System.Convert.ChangeType(
							aArgs[i], oParamInfo.ParameterType);

					//move to next array index
					i++;
				}
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.ParameterException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_InvalidArgInParamString"), xArgs, " / ", xMethodName), oE);
			}

			//run method
			return oMethod.Invoke(oSourceObject,aTypedParams);
		}

		/// <summary>
		/// invokes the specified COM method with the specified arguments
		/// </summary>
		/// <param name="xServer"></param>
		/// <param name="xClass"></param>
		/// <param name="xMethodName"></param>
		/// <param name="xArgs"></param>
		/// <param name="xArgDelimiter"></param>
		public static void InvokeCOMMethod(string xServer, string xClass,  
			string xMethodName, string xArgs, string xArgDelimiter)
		{
			string[] aArgs = null;

			//split argument string into array
			if(xArgs != null && xArgs != "")
			{
				if (xArgDelimiter != null && xArgDelimiter != "")
					aArgs = xArgs.Split(xArgDelimiter.ToCharArray());
				else
					aArgs = new string[1]{xArgs};
			}

			//call method in mpCOM
			try
			{
				LMP.Forte.MSWord.GlobalMethods.CallMethod(xServer, xClass, xMethodName, aArgs);
			}
			catch(System.Exception oE)
			{
				//call method failed
				throw new LMP.Exceptions.MethodCallException( 
					string.Concat(LMP.Resources.GetLangString("Error_RunCOMMethod"), 
					xServer, ".", xClass, ".", xMethodName), oE);
			}
		}

		/// <summary>
		/// sets the property with the specified name (on the specified object) 
		/// to the specified value
		/// </summary>
		/// <param name="oSourceObject">object containing the property to set</param>
		/// <param name="xPropertyName">name of property to set</param>
		/// <param name="oPropValue">value of property</param>
		public static void SetPropertyValue(object oSourceObject, string xPropertyName, 
			object oPropValue)
		{
			object oVal = null;

			if(oSourceObject == null)
				//source object not valid - alert
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_NullObjectArgument"));

			//get the property of the specified object
			System.Type oType  = oSourceObject.GetType();

			PropertyInfo oPropInfo = oType.GetProperty(xPropertyName);

			if(oPropInfo == null)
			{
				//there is no property with that name
				throw new LMP.Exceptions.PropertyNameException(
					string.Concat(LMP.Resources.GetLangString("Error_PropertyDoesNotExist"),
					oType.Name, ".", xPropertyName));
			}

			//get the system type of the property
			System.Type oPropType = oPropInfo.PropertyType;

			try
			{
				if(oPropType.IsEnum)
				{
					//property type is an enum - convert to enum value -
					//first convert argument string to underlying enum type
					System.Type oUnderlyingType = Enum.GetUnderlyingType(oPropType);
							
					oVal = Convert.ChangeType(oPropValue, oUnderlyingType);
					oVal = Enum.ToObject(oPropType, oVal);
				}
				else
					//convert to appropriate type and add to parameter array
					oVal = System.Convert.ChangeType(oPropValue, oPropType);

				//set property value
				oPropInfo.SetValue(oSourceObject, oVal, null);
			}
			catch(System.Exception oE)
			{
				//couldn't set property value - alert
				throw new LMP.Exceptions.ValueException(
					string.Concat(LMP.Resources.GetLangString("Error_InvalidPropertyValue") + 
					oPropInfo.Name, "/", oPropValue.ToString()), oE);
			}
		}

        /// <summary>
        /// returns the value of the specified property for the specified object
        /// </summary>
        /// <param name="oSourceObject"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="bReturnUnderlyingType">if true, and if the property is an enum, 
        /// will return the value in the underlying type of the enum</param>
        /// <returns></returns>
		public static object GetPropertyValue(object oSourceObject, string xPropertyName, bool bReturnUnderlyingType)
		{
            if (oSourceObject == null)
                //source object not valid - alert
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_NullObjectArgument"));

            //get the property of the specified object
            System.Type oType = oSourceObject.GetType();
            PropertyInfo oPropInfo = oType.GetProperty(xPropertyName);

            if (oPropInfo == null)
            {
                //there is no property with that name
                throw new LMP.Exceptions.PropertyNameException(
                    string.Concat(LMP.Resources.GetLangString("Error_PropertyDoesNotExist"),
                    oType.Name, ".", xPropertyName));
            }

            //get the property value
            object oValue = oPropInfo.GetValue(oSourceObject, null);
            System.Type oPropType = oPropInfo.PropertyType;

            if (bReturnUnderlyingType && oPropType.IsEnum)
            {
                //property type is an enum - convert to underlying value
                //before converting to string
                System.Type oUnderlyingType = Enum.GetUnderlyingType(oPropType);
                object oUnderlyingValue = Convert.ChangeType(oValue, oUnderlyingType);
                return oUnderlyingValue;
            }
            else
                //convert to string
                return oValue;
        }

		public static string GetPropertyValueAsString(object oSourceObject, string xPropertyName, bool bReturnUnderlyingType)
		{
            object oValue = GetPropertyValue(oSourceObject, xPropertyName, bReturnUnderlyingType);

			//convert to string
			return oValue == null ? null : oValue.ToString();
		}
	}
}
