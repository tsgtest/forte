﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace LMP.Controls
{
    public static class Utilities
    {
        /// <summary>
        /// Extension method to return if the control is in design mode
        /// </summary>
        /// <param name="control">Control to examine</param>
        /// <returns>True if in design mode, otherwise false</returns>
        public static bool IsInDesignMode(this System.Windows.Forms.Control control)
        {
            return ResolveDesignMode(control);
        }


        /// <summary>
        /// Method to test if the control or it's parent is in design mode
        /// </summary>
        /// <param name="control">Control to examine</param>
        /// <returns>True if in design mode, otherwise false</returns>
        private static bool ResolveDesignMode(System.Windows.Forms.Control control)
        {
            // Test the parent if it exists
            if (control.Parent != null)
            {
                return ResolveDesignMode(control.Parent);
            }
            else
            {
                // Get the protected property
                System.Reflection.PropertyInfo designModeProperty = control.GetType().GetProperty(
                                        "DesignMode",
                                        System.Reflection.BindingFlags.Instance
                                        | System.Reflection.BindingFlags.NonPublic);

                // Get the controls DesignMode value
                bool designMode = (bool)designModeProperty.GetValue(control, null);

                return designMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime;
            }
        }
    }
}
