using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.Xsl;
using System.Collections.Generic;

namespace LMP
{
	/// <summary>
	/// Contains low level string functions
	/// </summary>
	public static class String
	{
        public const string mpPipeReplacementChars = "~`";
        public const string mpTagPrefixIDSeparator = "¯°¯";

        /// <summary>
        /// splits string ID in format xxxx.xxxxxx into two numeric IDs
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        public static void SplitID(string xID, out int iID1, out int iID2)
        {
		    const int mpFirmRecordID = -999999999;
            
            string[] aSplit = xID.Split('.');
            iID1 = int.Parse(aSplit[0]);
            if (aSplit.Length == 1)
                iID2 = mpFirmRecordID;
            else
            {
                if ((aSplit[1] == "0") || (aSplit[1] == ""))
                    iID2 = mpFirmRecordID;
                else
                    iID2 = Convert.ToInt32(aSplit[1]);
            }
        }

		/// <summary>
		/// returns the position of the specified instance of a substring -
		/// returns -1 if specified instance does not exist
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xSearch"></param>
		/// <param name="iInstance"></param>
		/// <returns></returns>
		public static int GetInstancePosition(string xSource, string xSearch, int iInstance)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "xSearch",
				xSearch, "iInstance", iInstance);

			//ensure that requested instance exists - if not, return -1
			int iChars = CountChrs(xSource, xSearch);
			if (iChars < iInstance)
				return -1;

			//cycle until requested instance is reached
			int i = 0;
			int iPos = xSource.IndexOf(xSearch);
			while (iPos != -1)
			{
				i++;
				if (i == iInstance)
					return iPos;
				iPos = xSource.IndexOf(xSearch,iPos + 1);
			}
			return iPos;
		}

		/// <summary>
		/// returns the number of instances of a substring
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xSearch"></param>
		/// <returns></returns>
		public static int CountChrs(string xSource, string xSearch)
		{
			//Don't write entire string to debug.log - it might be full document xml
            Trace.WriteNameValuePairs("xSource", xSource.Substring(0, Math.Min(xSource.Length, 100)), "xSearch", xSearch);

			if ((xSource == "") || (xSearch == ""))
				return 0;

			int i = 0;
			int iPos = xSource.IndexOf(xSearch);
			while (iPos != -1)
			{
				i++;
				iPos = xSource.IndexOf(xSearch,iPos + 1);
			}
			return i;
		}

		/// <summary>
		/// returns the number of instances of a character
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="cSearch"></param>
		/// <returns></returns>
		public static int CountChrs(string xSource, char cSearch)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "cSearch", cSearch);

			if (xSource == "")
				return 0;

			int i = 0;
			int iPos = xSource.IndexOf(cSearch);
			while (iPos != -1)
			{
				i++;
				iPos = xSource.IndexOf(cSearch,iPos + 1);
			}
			return i;
		}

		/// <summary>
		/// substitutes soft returns for hard returns
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string ReturnsToLFChange(string xSource)
		{
            return ReturnsToLFChange(xSource, false, false);
		}

		/// <summary>
		/// substitutes soft returns for hard returns -
		/// trims final return if specified
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="bTrimFinalLF"></param>
		/// <returns></returns>
		public static string ReturnsToLFChange(string xSource, bool bTrimFinalLF, bool bTrimStartingLF)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "bTrimFinalLF", bTrimFinalLF, "bTrimStartingLF", bTrimStartingLF);
			if (xSource == "")
				return xSource;
            string xText = xSource.Replace("\r\n", "\v");
            xText = xText.Replace("\r", "\v");
            if (bTrimStartingLF)
                xText = xText.TrimStart('\v');
            if (bTrimFinalLF)
                xText = xText.TrimEnd('\v');
            return xText;
		}

		/// <summary>
		/// replaces hard and soft returns with line feeds
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string CleanUpForDisplay(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);
			string xTemp = ReturnsToLFChange(xSource, true, true);
			return xTemp.Replace("\v", "\r\n");
		}

		/// <summary>
		/// splits source string in two at its middle return-linefeed combo
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xStr1"></param>
		/// <param name="xStr2"></param>
		public static void SplitString(string xSource, out string xStr1, out string xStr2)
		{
			Trace.WriteNameValuePairs("xSource", xSource);
			SplitString(xSource, out xStr1, out xStr2, "\r\n", 0); 
		}

		/// <summary>
		/// splits source string in two at middle instance of the specified separator
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xStr1"></param>
		/// <param name="xStr2"></param>
		/// <param name="xSep"></param>
		public static void SplitString(string xSource, out string xStr1, out string xStr2,
			string xSep)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "xSep", xSep);
			SplitString(xSource, out xStr1, out xStr2, xSep, 0); 
		}

		/// <summary>
		/// splits source string in two at specified instance of a return-linefeed combo
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xStr1"></param>
		/// <param name="xStr2"></param>
		/// <param name="iSeparatorInstance"></param>
		public static void SplitString(string xSource, out string xStr1, out string xStr2,
			int iSeparatorInstance)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "iSeparatorInstance",
				iSeparatorInstance);

            SplitString(xSource, out xStr1, out xStr2, "\r\n", iSeparatorInstance); 
		}

		/// <summary>
		/// splits source string in two at specified instance of the specified separator
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xStr1"></param>
		/// <param name="xStr2"></param>
		/// <param name="xSep"></param>
		/// <param name="iSeparatorInstance"></param>
		public static void SplitString(string xSource, out string xStr1, out string xStr2,
			string xSep, int iSeparatorInstance)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "xSep", xSep,
				"iSeparatorInstance", iSeparatorInstance);

			//trim starting and ending separators
			xSource = xSource.Trim(xSep.ToCharArray());

			//if no separators, put entire string in xStr1
			if (xSource.IndexOf(xSep)== -1)
			{
				xStr1=xSource;
				xStr2="";
				return;
			}

			//get separator to split at
			float fCount = CountChrs(xSource, xSep) + 1;

			if (iSeparatorInstance==0)
				//split at middle separator
			{
				if (fCount % 2 == 0)
					fCount = fCount/2;
				else
					fCount = (fCount/2) + 0.5f;
			}
			else
			{
				if (fCount<iSeparatorInstance)
					//if fewer than specified separators, put entire string in xStr1
				{
					xStr1=xSource;
					xStr2="";
					return;
				}
				fCount = iSeparatorInstance;
			}

			//split string
			int iPos = 0;
			for (int i=0;i<fCount;i++)
				iPos = xSource.IndexOf(xSep,iPos + 1);
	
			xStr1 = xSource.Substring(0, iPos);
			xStr2 = xSource.Substring(iPos + xSep.Length);
		}

		/// <summary>
		/// returns a blank line of specified length if source string is empty -
		/// otherwise, returns source string unchanged
		/// </summary>
		/// <param name="xText"></param>
		/// <param name="iLength"></param>
		/// <returns></returns>
		public static string LineIfEmpty(string xText, int iLength)
		{
			Trace.WriteNameValuePairs("xText", xText, "iLength", iLength);

			if (xText=="")
			{
				string xLine = new string('_', iLength);
				return xLine;
			}
			else
			{
				return xText;
			}
		}

		/// <summary>
		/// returns source string initial capped
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string InitialCap(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);

			//lower case xSource
			string xLower = xSource.ToLower();

			//cycle through lower case characters, appending to new string
			//as upper case if at start or following a space and otherwise
			//appending as lower case
			StringBuilder oSB = new StringBuilder();
			oSB.Append(Char.ToUpper(xLower[0]));
			bool bSpace=false;
			int iLength = xLower.Length;
			for (int i=1;i<iLength;i++)
			{
				if (xLower[i]==' ')
					bSpace=true;
				if ((xLower[i]!=' ') && (bSpace==true))
				{
					oSB.Append(Char.ToUpper(xLower[i]));
					bSpace=false;
				}
				else
				{
					oSB.Append(xLower[i]);
				}
			}
			return oSB.ToString();
		}

		/// <summary>
		/// trims all non-numeric characters from start and end of source string -
		/// designed for attorney bar id number field that contains abbreviated state info -
		/// note that returned value may contain embedded non-numeric characters
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string TrimNonNumeric(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);
			return TrimNonNumeric(xSource, false);
		}

		/// <summary>
		/// trims all non-numeric characters from start (and end if specified) of source string  -
		/// designed for attorney bar id number field that contains abbreviated state info -
		/// note that returned value may contain embedded non-numeric characters
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="bFromStartOnly"></param>
		/// <returns></returns>
		public static string TrimNonNumeric(string xSource, bool bFromStartOnly)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "bFromStartOnly", bFromStartOnly);

			char[] xSourceArray = xSource.ToCharArray();
			int iLength = xSourceArray.Length;
			int i=0;
			int j=0;

			//get first numeric character
			for (i=0;i<iLength;i++)
			{
				if (Char.IsNumber(xSourceArray[i]))
					break;
				else if (i == iLength - 1)
					//there are no numeric characters - return empty string
					return "";
			}

			//get first numeric character starting from end
			if (! bFromStartOnly)
			{
				for (j=0;j<iLength;j++)
				{
					if (Char.IsNumber(xSourceArray[iLength - j - 1]))
						break;
				}
			}

			//return numeric substring
			return xSource.Substring(i, iLength - i - j);
		}

		/// <summary>
		/// replaces specified instance of substring
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xSearch"></param>
		/// <param name="xReplace"></param>
		/// <param name="iInstance"></param>
		/// <returns></returns>
		public static string ReplaceInstance(string xSource, string xSearch, 
			string xReplace, int iInstance)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "xSearch", xSearch,
				"xReplace", xReplace, "iInstance", iInstance);

			if (CountChrs(xSource, xSearch) < iInstance)
				//specified instance does not exist
				return xSource;

			//find instance
			int iPos = GetInstancePosition(xSource, xSearch, iInstance);

			if(iPos > -1)
			{
				//instance was found - replace instance
				StringBuilder oSB = new StringBuilder(xSource);
				oSB.Replace(xSearch, xReplace, iPos, xSearch.Length);
				return oSB.ToString();
			}
			else
				return xSource;
		}

		/// <summary>
		/// moves text after last instance of specified separator to start of
		/// source string - useful for flipping name of person
		/// </summary>
		/// <param name="xSource"></param>
		/// <param name="xSep"></param>
		/// <returns></returns>
		public static string FlipString(string xSource, string xSep)
		{
			Trace.WriteNameValuePairs("xSource", xSource, "xSep", xSep);

			//find last instance of separator
			int iPos = xSource.LastIndexOf(xSep);
			if (iPos == -1)
				//no separators in source string
				return xSource;

			//get text before last separator
			string xSub = xSource.Substring(0, iPos);

			//initialize string builder with text after last separator
			iPos += xSep.Length;
			StringBuilder oSB = new StringBuilder(xSource, iPos, 
				xSource.Length - iPos, xSource.Length);

			//append space and text before last separator
			oSB.AppendFormat(" {0}", xSub);
			return oSB.ToString();
		}

        /// <summary>
        /// returns HotKey string
        /// </summary>
        /// <param name="xLabel"></param>
        /// <param name="xHotKey"></param>
        /// <returns></returns>
        public static string HotKeyString(string xLabel, string xHotKey)
        {
            string xUCaseLabel = xLabel.ToUpper();
            int iHotKeyLocation = xUCaseLabel.IndexOf(xHotKey.ToUpper());

            if (iHotKeyLocation >= 0)
            {
                string xHotKeyString = xLabel.Insert(iHotKeyLocation, "&");
                return xHotKeyString;
            }

            return xLabel;
        }

		/// <summary>
		/// upper cases with exception for "Mc"
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string UCaseName(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);
			string xUpper = xSource.ToUpper();
			xUpper = xUpper.Replace("MC", "Mc");
			return xUpper;
		}

		/// <summary>
		/// substitutes smart quotes for straight quotes
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string MakeQuotesSmart(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);

			//count straight quotes
			int iCount = CountChrs(xSource, '\u0022');

			//cycle through straight quotes replacing odd instances with
			//start quotes and even instances with end quotes
			StringBuilder oSB = new StringBuilder(xSource);
			char c;
			int iPos=0;
			for (int i=0;i<iCount;i++)
			{
				iPos = xSource.IndexOf('\u0022', iPos);
				if (i%2==0)
					//start quote
					c = '\u201C';
				else
					//end quote
					c = '\u201D';
				oSB.Replace('\u0022', c, iPos, 1);
				iPos++;
			}
			return oSB.ToString();
		}

		/// <summary>
		/// converts numeric month to "MMMM" or "MMM" format
		/// </summary>
		/// <param name="iMonth"></param>
		/// <param name="xFormat"></param>
		/// <returns></returns>
		public static string GetMonth(int iMonth, string xFormat)
		{
			Trace.WriteNameValuePairs("iMonth", iMonth, "xFormat", xFormat);

			if ((iMonth < 1) || (iMonth > 12))
				//month can only be 1-12
				throw new LMP.Exceptions.ParameterException(LMP.Resources.GetLangString
					("Error_Month_Index") + iMonth.ToString());
			if ((xFormat != "MMMM") && (xFormat != "MMM"))
				//only two formats supported
				throw new LMP.Exceptions.ParameterException(LMP.Resources.GetLangString
					("Error_Month_Format") + xFormat);
			DateTime dt = new DateTime(1,iMonth,1);
			return dt.ToString(xFormat);
		}

		/// <summary>
		/// returns the character specified by the supplied field code
		/// </summary>
		/// <param name="xToken"></param>
		/// <returns></returns>
		public static char GetCharFromFieldCode(string xCode)
		{
			Trace.WriteNameValuePairs("xCode", xCode);

			//validate field code
			Regex oRX = new Regex(@"(?i)\[Char_(\S+)\]");
			Match oM = oRX.Match(xCode);
			if (! oM.Success)
				throw new LMP.Exceptions.FieldCodeException(LMP.Resources.GetLangString
					("Error_FieldCode_Char") + xCode);

			//extract character code
			string xChar = xCode.Substring(6, xCode.Length - 7);
			int iChar;
			try
			{
				iChar = System.Convert.ToInt32(xChar);
			}
			catch (System.FormatException e)
			{
				//specified character code is non-numeric
				throw new LMP.Exceptions.FieldCodeException(LMP.Resources.GetLangString
					("Error_FieldCode_Char") + xCode, e);
			}
			return (char)iChar;
		}

		/// <summary>
		/// replaces all "Char_" field codes in source string with their values
		/// </summary>
		/// <param name="xSource"></param>
		/// <returns></returns>
		public static string ReplaceCharFieldCodes(string xSource)
		{
			Trace.WriteNameValuePairs("xSource", xSource);

			int iCount = CountChrs(xSource, "[Char_");
			StringBuilder oSB = new StringBuilder(xSource);
			int iPos=0;
			int iPos2;
			string xCode;

			//cycle through char field codes
			for (int i=0;i<iCount;i++)
			{
				//find start
				iPos = xSource.IndexOf("[Char_", iPos);
				//find end
				iPos2 = xSource.IndexOf("]", iPos);
				if (iPos2 == -1)
					//no end tag
					throw new LMP.Exceptions.FieldCodeException(LMP.Resources.GetLangString
						("Error_FieldCode_Char") + xSource.Substring(iPos));
				//evaluate and replace
				xCode = xSource.Substring(iPos, iPos2 - iPos + 1);
				oSB.Replace(xCode, GetCharFromFieldCode(xCode).ToString());
				iPos++;
			}
			return oSB.ToString();
		}

		/// <summary>
		/// returns true if xString can be converted to a double
		/// </summary>
		/// <param name="xString"></param>
		/// <returns></returns>
		public static bool IsNumericDouble(string xString)
		{
			try
			{
				double d = Convert.ToDouble(xString);
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// returns true if xString can be converted to an integer
		/// </summary>
		/// <param name="xString"></param>
		/// <returns></returns>
		public static bool IsNumericInt32(string xString)
		{
			try
			{
				int i = Convert.ToInt32(xString);
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// returns true if xString can be converted to a byte
		/// </summary>
		/// <param name="xString"></param>
		/// <returns></returns>
		public static bool IsNumericByte(string xString)
		{
			try
			{
				byte b = Convert.ToByte(xString);
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// appends a trailing slash to the specified string if necessary
		/// </summary>
		/// <param name="xString"></param>
		public static void EnsureTrailingSlash(ref string xString)
		{
			string xLastChar = xString.Substring(xString.Length - 1);

			if(xLastChar != @"\" && xLastChar != "/")
				xString = xString + @"\";
				
			return;
		}

		/// <summary>
		/// removes trailing slash from the specified string if necessary
		/// </summary>
		/// <param name="xString"></param>
		public static void EnsureNoTrailingSlash(ref string xString)
		{
			string xLastChar = xString.Substring(xString.Length - 1);

			if(xLastChar == @"\" || xLastChar == "/")
				xString = xString.Substring(0, xString.Length - 1);

			return;
		}

		/// <summary>
		/// returns for "0" and "false", else returns true.
		/// </summary>
		/// <param name="xString"></param>
		/// <returns></returns>
		public static bool ToBoolean(string xString)
		{
			try
			{
				//test if specified string is numeric
				double dblString = System.Convert.ToDouble(xString);

				//return true for all numeric values other than 0
				return dblString != 0;
			}
			catch
			{
				//string is not numeric - use system conversion to boolean
				return System.Convert.ToBoolean(xString);
			}
		}

		/// <summary>
		/// substitutes actual value for database keyword
		/// </summary>
		/// <param name="oValue"></param>
		/// <returns></returns>
		public static object ConvertDataTypeKeywords(string xValue)
		{
			if (xValue == "mpTrue")
				return true;
			else if (xValue == "mpFalse")
				return false;
			else
				return xValue;
		}

		/// <summary>
		/// returns the MacPac product name
		/// </summary>
		/// <returns></returns>
		public static string MacPacProductName
		{
			get
			{
				return LMP.ComponentProperties.ProductName;
			}
		}

        /// <summary>
        /// compresses the specified string
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        /// GLOG : 8257 : DF
        public static string Encrypt(string xText, string xPassword)
        {
            DateTime t0 = DateTime.Now;
            string xRet = LMP.Forte.MSWord.Application.Encrypt(xText, xPassword);
            LMP.Benchmarks.Print(t0);
            return xRet;
        }
        //public static string Compress(string xText, string xPassword)
        //{
        //    DateTime t0 = DateTime.Now;
            //MemoryStream oStream = new MemoryStream();
            //byte[] aBytes = Encoding.Unicode.GetBytes(xText);
            //System.IO.Compression.DeflateStream o = 
            //  new System.IO.Compression.DeflateStream(
            //  oStream, System.IO.Compression.CompressionMode.Compress);
            //o.Write(aBytes, 0, aBytes.Length);
            //LMP.Benchmarks.Print(t0);
            //return Encoding.Unicode.GetString(aBytes);
        //}

        /// <summary>
        /// decompresses the specified string
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string Decrypt(string xText)
        {
            string xPassword = "";
            return Decrypt(xText, ref xPassword);
        }
        public static string Decrypt(string xText, ref string xPassword)
        {

            if (IsEncrypted(xText))
            {
                DateTime t0 = DateTime.Now;

                try
                {
                    return LMP.Forte.MSWord.Application.Decrypt(xText, ref xPassword);
                }
                finally
                {
                    LMP.Benchmarks.Print(t0, xPassword);
                }
            }
            else
                return xText;
        }
        public static string DecryptMixedText(string xText, ref string xPassword)
        {
            const string MARKER = "^`~#mp!@";
            int iPos1 = 0;
            int iPos2 = 0;
            string xEncryptedText = null;
            string xDecryptedText = null;

            iPos1 = xText.IndexOf(MARKER);

            while (iPos1 > -1)
            {
                iPos2 = xText.IndexOf("\"", iPos1 + 1);
                xEncryptedText = xText.Substring(iPos1, iPos2 - iPos1 - 1 - 1);
                xDecryptedText = Decrypt(xEncryptedText, ref xPassword);

                xText = xText.Substring(0, iPos1) + xDecryptedText + xText.Substring(iPos2 - 1);

                iPos1 = xText.IndexOf(MARKER);
            }

            return xText;
        }
        /// <summary>
        /// returns the specified string after replacing reserved
        /// characters with escape sequences
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string ReplaceXMLChars(string xText)
        {
            return ReplaceXMLChars(xText, false);
        }
        /// <summary>
        /// returns the specified string after replacing reserved
        /// characters with escape sequences
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string ReplaceXMLChars(string xText, bool bIncludeQuotes)
        {

            //ensure that '&' is changed to '&amp;'
            xText = xText.Replace("&lt;", "<");
            xText = xText.Replace("&gt;", ">");
            xText = xText.Replace("&apos;", "'");
            xText = xText.Replace("&amp;", "&");
            xText = xText.Replace("&", "&amp;");

            //ensure that '<' is changed to '&lt;'
            xText = xText.Replace("<", "&lt;");

            //ensure that '>' is changed to '&gt;'
            xText = xText.Replace(">", "&gt;");

            //GLOG 6065: replace double quotes - not valid in XML Attributes
            if (bIncludeQuotes)
            {
                //GLOG item #6000 - dcf
                //remove double-quotes
                xText = xText.Replace("\"", "&quot;");
            }

            //ensure that ''' is changed to '&apos;'
            xText = xText.Replace("'", "&apos;");

            return xText;
        }

        /// <summary>
        /// returns the specified string after restoring
        /// escape sequences with reserved characters
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string RestoreXMLChars(string xText) //GLOG 8200
        {
            return RestoreXMLChars(xText, false);
        }
        public static string RestoreXMLChars(string xText, bool bIncludeQuotes) //GLOG 8200
        {
            //GLOG item #7574
            xText = xText.Replace("&amp;amp;", "&");
            xText = xText.Replace("&amp;", "&");
            xText = xText.Replace("&lt;", "<");
            xText = xText.Replace("&gt;", ">");
            xText = xText.Replace("&apos;", "'");
            //GLOG 8200
            if (bIncludeQuotes)
            {
                xText = xText.Replace("&quot;", "\"");
            }
            return xText;
        }
        /// <summary>
        /// returns the specified string after replacing 
        /// characters reserved for MacPac Fieldcodes with ASCII equivalents
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string ReplaceMPReservedChars(string xText)
        {
            if (xText == null)
                return null;

            xText = xText.Replace("[", "%5B");
            xText = xText.Replace("]", "%5D");
            xText = xText.Replace("^", "%5E");

            return xText;
        }
        /// <summary>
        /// returns the specified string after replacing ASCII 
        /// tokens with corresponding MacPac Fieldcode reserved characters
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string RestoreMPReservedChars(string xText)
        {
            if (xText == null)
                return null;

            xText = xText.Replace("%5B", "[");
            xText = xText.Replace("%5D", "]");
            xText = xText.Replace("%5E", "^");

            return xText;
        }
        /// <summary>
        /// returns the prefix assigned to the specified namespace
        /// in the specified xml - if namespace isn't found,
        /// returns an empty string
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xNamespace"></param>
        /// <returns></returns>
        public static string GetNamespacePrefix(string xXML, string xNamespace)
        {
            int iPos = xXML.IndexOf(xNamespace);
            if (iPos > -1)
            {
                int iPos2 = xXML.LastIndexOf("xmlns:", iPos);
                return xXML.Substring(iPos2 + 6, iPos - iPos2 - 8);
            }
            return "";
        }
        public static string GetDefaultXmlNamespacePrefix()
        {
            return "ns0";
        }

        /// <summary>
        /// Returns minimal base XML required for InsertXML to work correctly
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Returns minimal base XML required for InsertXML to work correctly
        /// </summary>
        /// <param name="bAsOpenXML"></param>
        /// <returns></returns>
        public static string GetMinimalOpeningXML(bool bAsOpenXML)
        {
            return GetMinimalOpeningXML("", bAsOpenXML);
        }
        /// <summary>
        /// Returns minimal base XML required for InsertXML to work correctly,
        /// including setting default style to be used
        /// </summary>
        /// <param name="xDefaultStyle"></param>
        /// <param name="bAsOpenXML"></param>
        /// <returns></returns>
        public static string GetMinimalOpeningXML(string xDefaultStyle, bool bAsOpenXML)
        {
            string xBase = "<?xml version=\"1.0\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?>";

            if (bAsOpenXML)
            {
                //open xml needs to be wrapped in a package node
                xBase = xBase + "<pkg:package xmlns:pkg=\"http://schemas.microsoft.com/office/2006/xmlPackage\"><pkg:part pkg:name=\"/_rels/.rels\" pkg:contentType=\"application/vnd.openxmlformats-package.relationships+xml\" " +
                    "pkg:padding=\"512\"><pkg:xmlData><Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\"><Relationship Id=\"rId1\" " +
                    "Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument\" Target=\"word/document.xml\"/></Relationships></pkg:xmlData></pkg:part>";
            }
            else
            {
                xBase = xBase + "<w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" " +
                    "xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" " +
                    "xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" " +
                    "xmlns:" + GetDefaultXmlNamespacePrefix() + "=\"urn-legalmacpac-data/10\" xml:space=\"preserve\">";
            }

            if (xDefaultStyle != "" && xDefaultStyle != null)
            {
                if (bAsOpenXML)
                {
                    //add styles part, along with relationship part linking styles and document parts
                    xBase = xBase + "<pkg:part pkg:name=\"/word/_rels/document.xml.rels\" pkg:contentType=\"application/vnd.openxmlformats-package.relationships+xml\" pkg:padding=\"256\"><pkg:xmlData><Relationships " +
                        "xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\"><Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles\" " +
                        "Target=\"styles.xml\"/></Relationships></pkg:xmlData></pkg:part>" +
                        "<pkg:part pkg:name=\"/word/styles.xml\" pkg:contentType=\"application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml\"><pkg:xmlData><w:styles xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" " +
                        "xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"><w:style w:type=\"paragraph\" w:default=\"on\"><w:name w:val=\"" + xDefaultStyle + "\"/></w:style></w:styles></pkg:xmlData></pkg:part>";
                }
                else
                {
                    xBase = xBase + "<w:styles><w:style w:type=\"paragraph\" w:default=\"on\"><w:name w:val=\""
                        + xDefaultStyle + "\"/></w:style></w:styles>";
                }
            }

            if (bAsOpenXML)
            {
                //add document part
                xBase = xBase + "<pkg:part pkg:name=\"/word/document.xml\" pkg:contentType=\"application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml\"><pkg:xmlData><w:document " +
                    "xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" " +
                    "xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" " +
                    "xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\">";
            }

            return xBase;
        }

        /// <summary>
        /// returns minimal base XML required for InsertXML to work correctly,
        /// </summary>
        /// <param name="bAsOpenXML"></param>
        /// <returns></returns>
        public static string GetMinimalClosingXML(bool bAsOpenXML)
        {
            if (bAsOpenXML)
                return "</w:document></pkg:xmlData></pkg:part></pkg:package>";
            else
                return "</w:wordDocument>";
        }
        /// <summary>
        /// Replaces references to old SegmentID and DisplayName in Tag Attributes with new values
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="oSourceDef"></param>
        /// <param name="oTargetDef"></param>
        /// <returns></returns>
        public static string ReplaceSegmentXML(string xSourceXML,  string xOldID, string xNewID, 
            string xOldDisplayName, string xNewDisplayName, string xOldNameOrTagID, string xNewName,
            string xOldTranslationID)
        {
            string xTempVal = xSourceXML;

            LMP.Trace.WriteNameValuePairs("xOldID", xOldID, "xNewID", xNewID, "xOldDisplayName", 
                xOldDisplayName, "xNewDisplayName", xNewDisplayName, "xOldNameOrTagID",
                xOldNameOrTagID, "xNewName", xNewName, "xOldTranslationID", xOldTranslationID);

            //get supplied name and/or tag id
            string xOldName = "";
            string xOldTagID = "";
            int iPos = xOldNameOrTagID.LastIndexOf('_');
            if (iPos != -1)
            {
                string xNumTest = xOldNameOrTagID.Substring(iPos + 1);
                //Check if last part of old name after underscore is integer or GUID
                //name may contain additional underscores before indexer
                if (String.IsNumericInt32(xNumTest) || xNumTest.StartsWith("{"))
                {
                    //old tag id supplied - parse out name
                    xOldName = xOldNameOrTagID.Substring(0, iPos);
                    xOldTagID = xOldNameOrTagID;
                }
                else
                {
                    //old name supplied - get old tag id from XML
                    xOldName = xOldNameOrTagID;
                    if (IsWordOpenXML(xSourceXML))
                    {
                        //Search XML for DocVar contaning TagID
                        //GLOG 5510: Escape any regex reserved characters to ensure match is found 
                        Match oMatch = Regex.Match(xSourceXML,
                            "<w:docVar[^<>]*w:val=\"" + Regex.Escape(xOldName) + "_.*?/>");
                        if (oMatch != null && oMatch.Value != "")
                        {
                            iPos = oMatch.Value.IndexOf("w:val=\"" + xOldName + "_") + 7;
                            int iPos2 = oMatch.Value.IndexOf("ÌÍ", iPos);
                            xOldTagID = oMatch.Value.Substring(iPos, iPos2 - iPos);
                        }
                    }
                    else
                    {
                        iPos = xTempVal.IndexOf("TagID=\"" + xOldName + "_") + 7;
                        int iPos2 = xTempVal.IndexOf("\"", iPos);
                        xOldTagID = xTempVal.Substring(iPos, iPos2 - iPos);
                    }
                }
            }
            else
            {
                //old name supplied - get old tag id from xml
                xOldName = xOldNameOrTagID;
                if (IsWordOpenXML(xSourceXML))
                {
                    //Search XML for DocVar contaning TagID
                    //GLOG 5510: Escape any regex reserved characters to ensure match is found 
                    Match oMatch = Regex.Match(xSourceXML,
                        "<w:docVar[^<>]*w:val=\"" + Regex.Escape(xOldName) + "_.*?/>");
                    if (oMatch != null && oMatch.Value != "")
                    {
                        iPos = oMatch.Value.IndexOf("w:val=\"" + xOldName + "_") + 7;
                        int iPos2 = oMatch.Value.IndexOf("ÌÍ", iPos);
                        xOldTagID = oMatch.Value.Substring(iPos, iPos2 - iPos);
                    }
                }
                else
                {
                    iPos = xTempVal.IndexOf("TagID=\"" + xOldName + "_") + 7;
                    int iPos2 = xTempVal.IndexOf("\"", iPos);
                    xOldTagID = xTempVal.Substring(iPos, iPos2 - iPos);
                }
            }

            // Replace DisplayName in ObjectData
            //GLOG 5419: Reserved XML Characters will appear as entity reference in XML
            xTempVal = xTempVal.Replace("|DisplayName=" + String.ReplaceXMLChars(xOldDisplayName) + "|",
                "|DisplayName=" + String.ReplaceXMLChars(xNewDisplayName) + "|");

            //GLOG 5419: Depending on how updated, property may contain "&amp;amp;"
            xTempVal = xTempVal.Replace("|DisplayName=" + String.ReplaceXMLChars(xOldDisplayName).Replace("&", "&amp;")  + "|",
                "|DisplayName=" + String.ReplaceXMLChars(xNewDisplayName) + "|");

            // Replace DialogCaption if default value used
            //GLOG 5419: Reserved XML Characters will appear as entity reference in XML
            xTempVal = xTempVal.Replace("|DialogCaption=" + String.ReplaceXMLChars(xOldDisplayName) + "|",
                "|DialogCaption=" + String.ReplaceXMLChars(xNewDisplayName) + "|");

            //GLOG 5419: Depending on how updated, property may contain "&amp;amp;"
            xTempVal = xTempVal.Replace("|DialogCaption=" + String.ReplaceXMLChars(xOldDisplayName).Replace("&", "&amp;") + "|",
                "|DialogCaption=" + String.ReplaceXMLChars(xNewDisplayName) + "|");

            // Replace Name in ObjectData
            xTempVal = xTempVal.Replace("|Name=" + xOldName + "|", "|Name=" + xNewName + "|");

            //10.2: Replace old TagID at start of Doc Variable value
            //12-16-10 (dm) - do regardless of whether this is open xml or WordML
            xTempVal = xTempVal.Replace("w:val=\"" + xOldTagID + "ÌÍ", "w:val=\"" + xNewName + "_1ÌÍ");
            // Update TagID in mDel ObjectData
            xTempVal = xTempVal.Replace("ÌÍObjectData=" + xOldTagID + "|", "ÌÍObjectData=" + xNewName + "_1|");

            if (!IsWordOpenXML(xSourceXML))
            {
                // Replace TagID
                xTempVal = xTempVal.Replace("TagID=\"" + xOldTagID, "TagID=\"" + xNewName + "_1");
                //GLOG 2946
                // Update TagID in mDel ObjectData
                xTempVal = xTempVal.Replace("ObjectData=\"" + xOldTagID + "|", "ObjectData=\"" + xNewName + "_1" + "|");

                //GLOG 3473
                // Update TagID in mDels within DeletedScopes attribute
                xTempVal = xTempVal.Replace("ObjectData=&quot;" + xOldTagID + "|", "ObjectData=&quot;" + xNewName + "_1" + "|");

            }

            // Replace occurrences of this Segment as ParentTagID
            xTempVal = xTempVal.Replace("|ParentTagID=" + xOldTagID + "|",
                   "|ParentTagID=" + xNewName + "_1|");

            // Replace SegmentID
            iPos = xOldID.IndexOf(".0");
            if (iPos != -1)
                xOldID = xOldID.Substring(0, iPos);
            iPos = xNewID.IndexOf(".0");
            if (iPos != -1)
                xNewID = xNewID.Substring(0, iPos);
            xTempVal = xTempVal.Replace("SegmentID=" + xOldID + "|", "SegmentID=" + xNewID + "|");

            //clear out old translation id
            xTempVal = xTempVal.Replace("|TranslationID=" + xOldTranslationID + "|",
                "|TranslationID=0|");
            //GLOG 5291: Also need to replace Segment ID that is part of ContentControl tag
            if (IsWordOpenXML(xSourceXML))
            {
                xOldID = xOldID.Replace('.', 'd');
                xOldID = xOldID.Replace('-', 'm');
                xOldID = xOldID.PadLeft(19, '0');
                xNewID = xNewID.Replace('.', 'd');
                xNewID = xNewID.Replace('-', 'm');
                xNewID = xNewID.PadLeft(19, '0');
                xTempVal = xTempVal.Replace(xOldID, xNewID);
            }
            return xTempVal;
        }
        /// <summary>
        /// evaluates date time format expression
        /// </summary>
        /// <param name="xDateFormat"></param>
        /// <param name="oDateTime"></param>
        /// <returns></returns>
        public static string EvaluateDateTimeValue(string xDateFormat, DateTime oDateTime)
        {
            return EvaluateDateTimeValue(xDateFormat, oDateTime, 1033);
        }
        /// <summary>
        /// evaluates date time format expression
        /// </summary>
        /// <param name="xDateFormat"></param>
        /// <param name="oDateTime"></param>
        /// <param name="iLCID"></param>
        /// <returns></returns>
        public static string EvaluateDateTimeValue(string xDateFormat, DateTime oDateTime, int iLCID)
        {
            Trace.WriteNameValuePairs(
                "xDateFormat", xDateFormat, "oDateTime", oDateTime, "iLCID", iLCID);

            System.Globalization.CultureInfo oCulture = new System.Globalization.CultureInfo(iLCID);

            //evaluate date format expression
            string xValue;
            bool bIsField = false;

            //strip and reserve field code if xDateFormat contains /F
            if (xDateFormat.Contains("/F"))
            {
                xDateFormat = xDateFormat.Replace("/F", "");
                bIsField = true;
            }

            if (xDateFormat.Contains("%") || xDateFormat.Contains("/J"))
            {
                //jurat type date formatting
                //parse formatting code, get current numeric day and 
                //add appropriate ordinal suffix
                xDateFormat = xDateFormat.Replace("/J", "");
                if (xDateFormat.IndexOf("%") > -1)
                {
                    string xDayFormat = xDateFormat.Substring(0, xDateFormat.IndexOf("%"));
                    xDayFormat = xDayFormat.Substring(xDayFormat.IndexOf(" ") + 1);

                    //get appropriate ordinal suffixed number for today's day
                    int iDisplayDay = Int32.Parse(oDateTime.Day.ToString(xDayFormat));
                    string xOrd = LMP.Number.GetOrdinal(iDisplayDay);
                    xDateFormat = xDateFormat.Replace(xDayFormat + "%", xOrd);
                }
                //convert rest of xDateFormat so Format function can use it
                xDateFormat = xDateFormat.Replace("MMMM", "{0:MMMM}");
                xDateFormat = xDateFormat.Replace("mmmm", "{0:MMMM}");
                xDateFormat = xDateFormat.Replace("YYYY", "{1:yyyy}");
                xDateFormat = xDateFormat.Replace("yyyy", "{1:yyyy}");

                //run resulting formating string - result will be completed Jurat date
                xValue = System.String.Format(oCulture.DateTimeFormat, xDateFormat, oDateTime, oDateTime);
            }
            else
            {
                //GLOG 8859 (dm) - format is different in VBA and .NET
                xDateFormat = xDateFormat.Replace("am/pm", "tt");

                xValue = oDateTime.ToString(xDateFormat, oCulture.DateTimeFormat);
            }

            if (bIsField)
                //append '(field)'
                xValue += LMP.Resources.GetLangString("Prompt_(field code)");

            //get rid of literal quotes
            xValue = xValue.Replace("'", "");

            return xValue;
        }
        /// <summary>
        /// Returns minimal Document XML with style nodes only that will work with Organizer
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string GetMinimalStyleDocumentXML(string xInput)
        {
            //TODO: If this function is to be used with WordOpenXML, it will need to be reworked.
            //Just including the <w:styles> information is not sufficient to get all of the default
            //font and paragraph format options
            const string xXMLStart = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
            "<?mso-application progid=\"Word.Document\"?>\r\n" +
            "<w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" " +
            "xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\">\r\n";

            const string xXMLEnd = "<w:body><w:p/></w:body><w:sectPr/></w:wordDocument>";

            const string xXMLStartOpenXML = "<?xml version=\"1.0\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?>" +
                    "<pkg:package xmlns:pkg=\"http://schemas.microsoft.com/office/2006/xmlPackage\"><pkg:part pkg:name=\"/_rels/.rels\" pkg:contentType=\"application/vnd.openxmlformats-package.relationships+xml\" " +
                    "pkg:padding=\"512\"><pkg:xmlData><Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\"><Relationship Id=\"rId1\" " +
                    "Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument\" Target=\"word/document.xml\"/></Relationships></pkg:xmlData></pkg:part>\r\n";

            const string xXMLEndOpenXML = "<pkg:part pkg:name=\"/word/document.xml\" pkg:contentType=\"application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml\"><pkg:xmlData><w:document " +
                    "xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" " +
                    "xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" " +
                    "xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\">" +
                    "</w:document></pkg:xmlData></pkg:part></pkg:package>";

            const string xStylesStartOpenXML = "<pkg:part pkg:name=\"/word/_rels/document.xml.rels\" pkg:contentType=\"application/vnd.openxmlformats-package.relationships+xml\" pkg:padding=\"256\"><pkg:xmlData><Relationships " +
                        "xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\"><Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles\" " +
                        "Target=\"styles.xml\"/></Relationships></pkg:xmlData></pkg:part>" +
                        "<pkg:part pkg:name=\"/word/styles.xml\" pkg:contentType=\"application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml\"><pkg:xmlData>";
            const string xStylesEndOpenXML = "</pkg:xmlData></pkg:part>";
            

            bool bIsWordOpenXML = String.IsWordOpenXML(xInput);

            //Extract style nodes from XML
            string xStyleXML = String.ExtractStyleXML(xInput);
            if (xStyleXML != "")
            {
                if (bIsWordOpenXML)
                    xStyleXML = xStylesStartOpenXML + xStyleXML + xStylesEndOpenXML;
            }
            string xListXML = "";
            //Check for style that use a ListTemplate
            if (xStyleXML.Contains("<w:ilfo"))
            {
                //TODO: This could probably by limited to just those ListTemplates used by existing styles
                //but first need to determine the relationship between <w:ilfo> tags in the style and the <w:ListDef> nodes
                xListXML = ExtractListXML(xInput);
            }
            if (bIsWordOpenXML)
                return xXMLStartOpenXML + xStyleXML + xXMLEndOpenXML;
            else
                return xXMLStart + xListXML + xStyleXML + xXMLEnd;
        }
        /// <summary>
        /// Extract contents of the w:lists node in Input XML
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string ExtractListXML(string xInput)
        {
            if (System.String.IsNullOrEmpty(xInput))
                return "";

            string xListXML = "";
            //GLOG 5072: List Templates configured differently in WordOpenXML
            if (LMP.String.IsWordOpenXML(xInput))
            {
                int iStart = xInput.IndexOf("<w:numbering");

                if (iStart > -1)
                {
                    //Input is full doc XML, return just contents of "<w:lists>" section
                    int iEnd = xInput.IndexOf("</w:numbering>", iStart);
                    if (iEnd > -1)
                    {
                        iEnd = iEnd + @"<w:numbering>".Length + 1;
                        xListXML = xInput.Substring(iStart, iEnd - iStart);
                    }
                }
            }
            else
            {
                int iStart = xInput.IndexOf("<w:lists");

                if (iStart > -1)
                {
                    //Input is full doc XML, return just contents of "<w:lists>" section
                    int iEnd = xInput.IndexOf("</w:lists>", iStart);
                    if (iEnd > -1)
                    {
                        iEnd = iEnd + @"<w:lists>".Length + 1;
                        xListXML = xInput.Substring(iStart, iEnd - iStart);
                    }
                }
            }
            return xListXML;

        }
        public static string ExtractStyleXML(string xInput)
        {
            int iStart = -1;
            int iEnd = -1;
            return ExtractStyleXML(xInput, ref iStart, ref iEnd);
        }
        /// <summary>
        /// Extract text of all w:style nodes from input XML
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string ExtractStyleXML(string xInput, ref int iStartXML, ref int iEndXML)
        {
            //GLOG 5353:  Added ref parameters for Start and End position,
            //to simplify replacing entire Styles node in XML
            if (System.String.IsNullOrEmpty(xInput))
                return "";

            string xStyleXML = "";
            iStartXML = -1;
            if (IsWordOpenXML(xInput))
            {
                //GLOG 5240: For Word Open XML, need to look for styles in proper XML package
                //GLOG 5310: Switched from regex to IndexOf - dcf
                //Match oPkg = Regex.Match(xInput, "<pkg:part pkg:name=\"/word/styles.xml\"" + ".*?" + "</pkg:part>");

                int iPkgStart = xInput.IndexOf("<pkg:part pkg:name=\"/word/styles.xml\"");

                //if (oPkg.Value != "")
                if(iPkgStart > -1)
                {
                    //iStart = xInput.IndexOf("<w:styles", oPkg.Index, oPkg.Length);
                    iStartXML = xInput.IndexOf("<w:styles", iPkgStart);
                }
                else
                    return "";
            }
            else
                iStartXML = xInput.IndexOf("<w:styles");

            if (iStartXML > -1)
            {
                //Input is full doc XML, return just contents of "<w:styles>" section
                //locate start of active styles XML
                //iStart = xInput.IndexOf("<w:style ", iStart + 9);
                //if (iStart > -1)
                //{
                    iEndXML = xInput.IndexOf("</w:styles>", iStartXML + 8) + 11;
                    xStyleXML = xInput.Substring(iStartXML, iEndXML - iStartXML);
                //}
            }
            else if (xInput.IndexOf("<w:style ") > -1)
                xStyleXML = "<w:styles>" + xInput + "</w:styles>";

            return xStyleXML;
        }
        /// <summary>
        /// Returns only the <w:body> node of Word document XML
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string ExtractBodyXML(string xInput)
        {
            if (System.String.IsNullOrEmpty(xInput))
                return "";
            string xBodyXML = "";
            int iStart = xInput.IndexOf("<w:body");

            if (iStart > -1)
            {
                //Input is full doc XML, return just contents of "<w:body>" section
                int iEnd = xInput.IndexOf("</w:body>", iStart);
                if (iEnd > -1)
                {
                    iEnd = iEnd + 9;
                    xBodyXML = xInput.Substring(iStart, iEnd - iStart);
                }
            }
            return xBodyXML;
        }
        /// <summary>
        /// Returns true if input string is Word Document XML
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static bool IsWordXML(string xText)
        {
            //TODO: Can this be refined?
            if (!xText.StartsWith("<?xml"))
                return false;
            else if (!xText.Contains("<w:wordDocument") || !xText.Contains("</w:wordDocument>"))
                return false;
            else
                return true;
        }
        /// <summary>
        /// Returns string with predefined delimiters removed from end
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string TrimDelimiters(string xInput)
        {
            return xInput.TrimEnd(new char[] { '\n', '\r', '\v', '\t', ',', ':', ';', '-', ' ' });
        }
        public static string RemoveIllegalNameChars(string xDisplayName)
        {
            //name is display name without spaces and special punctuation
            string xName = xDisplayName.Replace(" ", "");
            xName = xName.Replace("'", "");
            xName = xName.Replace(".", "_");
            xName = xName.Replace("<", "(");
            xName = xName.Replace(">", ")");
            xName = xName.Replace("&", "-");
            xName = xName.Replace("]", ")");
            xName = xName.Replace("[", "(");
            xName = xName.Replace("^", "-");
            xName = xName.Replace("\"", ""); //GLOG 8463
            return xName;
        }
        //GLOG : 8418 :  JSW
         /// <summary>
        /// Returns string after removing illegal element ID characters
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string RemoveIllegalIDChars(string xID)
        {
            string xName = xID.Replace(" ", "");
            xName = xName.Replace("'", "");
            xName = xName.Replace("!", "");
            xName = xName.Replace("<", "");
            xName = xName.Replace(">", "");
            xName = xName.Replace("&", "");
            xName = xName.Replace("]", "");
            xName = xName.Replace("[", "");
            xName = xName.Replace("(", "");
            xName = xName.Replace(")", "");
            xName = xName.Replace("@", "");
            xName = xName.Replace("#", "");
            xName = xName.Replace("$", "");
            xName = xName.Replace("*", "");
            xName = xName.Replace("$", "");
            xName = xName.Replace("+", "");
            xName = xName.Replace("=", "");
            xName = xName.Replace("?", "");
            xName = xName.Replace("{", "");
            xName = xName.Replace("}", "");
            xName = xName.Replace(";", "");
            xName = xName.Replace(",", "");
            xName = xName.Replace("%", "");
            xName = xName.Replace("^", "");
            xName = xName.Replace("\"", ""); //GLOG 8463
            return xName;
        } 
        /// <summary>
        /// Returns count of Top-level Nodes of specified Name in XML
        /// </summary>
        /// <param name="xXMLSource"></param>
        /// <param name="xTagName"></param>
        /// <returns></returns>
        public static int GetXMLEntityCount(string xXMLSource, string xTagName)
        {
            System.Xml.XmlNodeList oNodeList = null;
            //Extract just XML portion of string
            int iXMLStart = xXMLSource.IndexOf("<");
            int iXMLEnd = xXMLSource.LastIndexOf(">") + 1;

            //Input is not XML
            if (iXMLStart == -1 || iXMLEnd == 0)
                return 0;
            else
                xXMLSource = xXMLSource.Substring(iXMLStart, iXMLEnd - iXMLStart);

            //load specified XMLSource to XmlDocument
            System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
            xXMLSource = "<zzmpD>" + xXMLSource + "</zzmpD>";
            oXML.LoadXml(xXMLSource);


            if (string.IsNullOrEmpty(xTagName))
            {
                //GLOG 5745:  All details may not be included in every entity
                //if no Tag Name specified, determine count from number of difference Index values
                oNodeList = oXML.DocumentElement.SelectNodes("/zzmpD/*");

                if (oNodeList.Count > 0)
                {
                    System.Collections.ArrayList aIndices = new System.Collections.ArrayList();
                    for (int i = 0; i < oNodeList.Count; i++)
                    {
                        string xCurItem = oNodeList.Item(i).Attributes.GetNamedItem("Index").Value;
                        if (!aIndices.Contains(xCurItem))
                        {
                            aIndices.Add(xCurItem);
                        }
                    }
                    return aIndices.Count;
                }
                else
                    return 0;
            }

            StringBuilder oSB = new StringBuilder();
            //Get number of top-level nodes matching name
            oNodeList = oXML.DocumentElement.SelectNodes(
                   (oSB.AppendFormat("(/zzmpD/{0}|/zzmpD/{1})", xTagName, xTagName.ToUpper())).ToString());
            return oNodeList.Count;
        }
        public static string NumberToText(int iNum)
        {
            return NumberToText(iNum, true).Trim();
        }
        /// <summary>
        /// Returns English language text representation of specified number
        /// </summary>
        /// <param name="iNum"></param>
        /// <returns></returns>
        private static string NumberToText(int iNum, bool bZeroText)
        {
            //Largest number represented is 2,147,483,647
            if (iNum < 0)
                return "Minus " + NumberToText(-iNum, false);
            else if (iNum == 0)
                if (bZeroText)
                    return "Zero";
                else
                    return "";
            else if (iNum <= 19)
                return new string[] {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", 
         "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", 
         "Seventeen", "Eighteen", "Nineteen"}[iNum - 1] + " ";
            else if (iNum <= 99)
                return new string[] {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", 
         "Eighty", "Ninety"}[iNum / 10 - 2] + ((iNum % 10) > 0 ? "-" : " ")  + NumberToText(iNum % 10, false);
            else if (iNum <= 999)
                return NumberToText(iNum / 100, false) + "Hundred " + NumberToText(iNum % 100, false);
            else if (iNum <= 999999)
                return NumberToText(iNum / 1000, false) + "Thousand " + NumberToText(iNum % 1000, false);
            else if (iNum <= 999999999)
                return NumberToText(iNum / 1000000, false) + "Million " + NumberToText(iNum % 1000000, false);
            else
                return NumberToText(iNum / 1000000000, false) + "Billion " + NumberToText(iNum % 1000000000, false);
        }
        public static string GetUSPSFormat(string xInput)
        {
            //Default character set
            return GetUSPSFormat(xInput, new char[] {'#', ',', '.', '\"', ':', ';', '\''});
        }
        /// <summary>
        /// Returns xInput converted to uppercase, with any characters in cChars removed
        /// </summary>
        /// <param name="xInput"></param>
        /// <param name="cChars"></param>
        /// <returns></returns>
        public static string GetUSPSFormat(string xInput, char[] cChars)
        {
            string xTemp = xInput;
            foreach (char c in cChars)
            {
                xTemp = xTemp.Replace(c.ToString(), "");
            }
            xTemp = xTemp.ToUpper();
            return xTemp;
        }

        /// <summary>
        /// compares the specified strings -
        /// case sensitive except for boolean values
        /// </summary>
        /// <param name="xString1"></param>
        /// <param name="xString2"></param>
        /// <returns></returns>
        public static bool CompareBooleanCaseInsensitive(string xString1, string xString2)
        {
            //ensure that boolean comparison is case insensitive
            string xLower = xString1.ToLower();
            if ((xLower == "false") || (xLower == "true"))
                xString1 = xLower;
            xLower = xString2.ToLower();
            if ((xLower == "false") || (xLower == "true"))
                xString2 = xLower;

            return (xString1 == xString2);
        }

        //TODO: AppendToDPhrase
		//TODO: All functions that involved arrays in VB6

        /// <summary>
        /// Returns Source XML with the <w:styles> nodes of the TargetXML
        /// with styles unique to the Source only appended
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="xTargetXML"></param>
        /// <param name="xDefaultStyle"></param>
        /// <returns></returns>
        public static string MergeStylesXML(string xSourceXML, string xTargetXML, string xDefaultStyle)
        {
            return MergeStylesXML(xSourceXML, xTargetXML, xDefaultStyle, false);
        }

        /// <summary>
        /// Returns Source XML with the <w:styles> nodes of the TargetXML
        /// with styles unique to the Source only appended if specified
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="xTargetXML"></param>
        /// <param name="xDefaultStyle"></param>
        /// <param name="bOmitSourceStyles"></param>
        /// <returns></returns>
        public static string MergeStylesXML(string xSourceXML, string xTargetXML, string xDefaultStyle,
            bool bOmitSourceStyles)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //Return Source XML unchanged if either string is in WordOpenXML format
                if (IsWordOpenXML(xSourceXML) || IsWordOpenXML(xTargetXML))
                    return xSourceXML;

                string xOldStylesXML = "";
                string xNewStylesXML = "";
                string xAddedStylesXML = "";
                int iTargetStylesStart = xTargetXML.IndexOf("<w:styles>");
                int iTargetStylesEnd = xTargetXML.IndexOf("</w:styles>");
                int iSourceStylesStart = xSourceXML.IndexOf("<w:styles>");
                int iSourceStylesEnd = xSourceXML.IndexOf("</w:styles>");
                if (iTargetStylesStart > -1 && iTargetStylesEnd > -1 &&
                    iSourceStylesStart > -1 && iSourceStylesEnd > -1)
                {
                    xNewStylesXML = xTargetXML.Substring(iTargetStylesStart,
                        (iTargetStylesEnd + @"</w;styles>".Length) - iTargetStylesStart);
                }
                else
                    //return unchanged
                    return xSourceXML;

                if (!bOmitSourceStyles) //GLOG 8248 (dm)
                {
                    xOldStylesXML = ExtractStyleXML(xSourceXML);
                    string[] aOldStyles = xOldStylesXML.Split(
                        new string[] { "<w:style " }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string xStyle in aOldStyles)
                    {
                        int iNameStart = xStyle.IndexOf("<w:name");
                        int iNameEnd = xStyle.IndexOf("/>", iNameStart + 7);
                        if (iNameStart > -1 && iNameEnd > -1)
                        {
                            //Check if like-named style exists in Target XML
                            //GLOG 5514: Closing tags may be preceded by extra
                            //space if processed by LoadXML, so Trim search string
                            int iTargetStylePos = xNewStylesXML.IndexOf(
                                xStyle.Substring(iNameStart, iNameEnd - iNameStart).Trim());

                            if (iTargetStylePos == -1)
                            {
                                //If not, append to XML
                                xAddedStylesXML = xAddedStylesXML + "<w:style " + xStyle;
                            }
                        }
                    }
                }

                if (xAddedStylesXML != "")
                {
                    if (xAddedStylesXML.EndsWith("</w:styles>"))
                        xAddedStylesXML = xAddedStylesXML.Substring(0, xAddedStylesXML.Length - "</w:styles>".Length);

                    //Insert added styles at end of w:styles section
                    xNewStylesXML = xNewStylesXML.Insert(
                        xNewStylesXML.IndexOf("</w:styles>"), xAddedStylesXML);
                }

                //Set default style for Normal paragraphs in XML
                if (!string.IsNullOrEmpty(xDefaultStyle) && xNewStylesXML.IndexOf("\"" + xDefaultStyle + "\"") > -1)
                {
                    //Clear any existing default
                    xNewStylesXML = xNewStylesXML.Replace(" w:default=\"on\"", "");
                    //Find default style definition
                    int iDefStart = xNewStylesXML.IndexOf("<w:name w:val=\"" + xDefaultStyle + "\"");
                    if (iDefStart > -1)
                    {
                         xNewStylesXML = xNewStylesXML.Insert(iDefStart - 1, " w:default=\"on\"");
                    }
                }
                xNewStylesXML = xSourceXML.Substring(0, iSourceStylesStart) +
                    xNewStylesXML + xSourceXML.Substring(iSourceStylesEnd + @"</w:styles>".Length);

                //Also replace defaultfonts node
                int iTargetDefaultFontsStart = xTargetXML.IndexOf("<w:defaultFonts");
                int iTargetDefaultFontsEnd = xTargetXML.IndexOf("/>", iTargetDefaultFontsStart) + 2;
                int iSourceDefaultFontsStart = xNewStylesXML.IndexOf("<w:defaultFonts");
                int iSourceDefaultFontsEnd = xNewStylesXML.IndexOf("/>", iSourceDefaultFontsStart) + 2;
                if (iTargetDefaultFontsStart > -1 && iTargetDefaultFontsEnd > -1 &&
                    iSourceDefaultFontsStart > -1 && iSourceDefaultFontsEnd > -1)
                {
                    string xDefaultFonts = xTargetXML.Substring(iTargetDefaultFontsStart, iTargetDefaultFontsEnd - iTargetDefaultFontsStart);
                    xNewStylesXML = xNewStylesXML.Substring(0, iSourceDefaultFontsStart) + xDefaultFonts +
                        xNewStylesXML.Substring(iSourceDefaultFontsEnd);
                }
                LMP.Benchmarks.Print(t0);
                return xNewStylesXML;
            }
            catch
            {
                //If an error occurs here, just return the Source XML unchanged
                return xSourceXML;
            }
        }
        /// <summary>
        /// Returns Source XML with the <w:styles> nodes of the TargetXML
        /// with styles unique to the Source only appended
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="xTargetXML"></param>
        /// <param name="xDefaultStyle"></param>
        /// <returns></returns>
        public static string MergeStylesWordOpenXML(string xSourceXML, string xTargetXML, string xDefaultStyle)
        {
            return MergeStylesWordOpenXML(xSourceXML, xTargetXML, xDefaultStyle, false);
        }

        /// <summary>
        /// Returns Source XML with the <w:styles> nodes of the TargetXML
        /// and with styles unique to the Source only appended if specified
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="xTargetXML"></param>
        /// <param name="xDefaultStyle"></param>
        /// <param name="bOmitSourceStyles"></param>
        /// <returns></returns>
        public static string MergeStylesWordOpenXML(string xSourceXML, string xTargetXML, string xDefaultStyle,
            bool bOmitSourceStyles)
        {
            //GLOG 5128
            DateTime t0 = DateTime.Now;
            try
            {
                //Return Source XML unchanged if both strings aren't in WordOpenXML format
                if (!IsWordOpenXML(xSourceXML) || !IsWordOpenXML(xTargetXML))
                    return xSourceXML;

                string xSourceStylesXML = "";
                string xTargetStylesXML = "";
                string xNewStylesXML = "";
                string xAddedStylesXML = "";

                int iTargetStylesStart = -1;
                int iTargetStylesEnd = -1;
                int iSourceStylesStart = -1;
                int iSourceStylesEnd = -1;

                //GLOG 5353: Use ExtractStyleXML to ensure we're getting XML from proper pkg: node
                xTargetStylesXML = ExtractStyleXML(xTargetXML, ref iTargetStylesStart, ref iTargetStylesEnd);
                xSourceStylesXML = ExtractStyleXML(xSourceXML, ref iSourceStylesStart, ref iSourceStylesEnd);

                if (iTargetStylesStart == -1 || iTargetStylesEnd == -1 ||
                    iSourceStylesStart == -1 || iSourceStylesEnd == -1)
                    return xSourceXML;

                if (!bOmitSourceStyles) //GLOG 8248
                {
                    string[] aOldStyles = xSourceStylesXML.Split(
                        new string[] { "<w:style " }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string xStyle in aOldStyles)
                    {
                        int iNameStart = xStyle.IndexOf("<w:name");
                        int iNameEnd = xStyle.IndexOf("/>", iNameStart + 7);
                        if (iNameStart > -1 && iNameEnd > -1)
                        {
                            //Check if like-named style exists in Target XML
                            //GLOG 5514: Check for style in Target XML - closing tags may be preceded by extra
                            //space if processed by LoadXML, so Trim search string
                            int iTargetStylePos = xTargetStylesXML.IndexOf(
                                xStyle.Substring(iNameStart, iNameEnd - iNameStart).Trim());

                            if (iTargetStylePos == -1)
                            {
                                //If not, append to XML
                                xAddedStylesXML = xAddedStylesXML + "<w:style " + xStyle;
                            }
                        }
                    }
                }

                xNewStylesXML = xTargetStylesXML;

                if (xAddedStylesXML != "")
                {
                    if (xAddedStylesXML.EndsWith("</w:styles>"))
                        xAddedStylesXML = xAddedStylesXML.Substring(0, xAddedStylesXML.Length - "</w:styles>".Length);

                    //Insert added styles at end of w:styles section
                    xNewStylesXML = xNewStylesXML.Insert(
                        xNewStylesXML.IndexOf("</w:styles>"), xAddedStylesXML);
                }

                //Set default style for Normal paragraphs in XML
                if (!string.IsNullOrEmpty(xDefaultStyle) && xNewStylesXML.IndexOf("\"" + xDefaultStyle + "\"") > -1)
                {
                    //Clear any existing default
                    xNewStylesXML = xNewStylesXML.Replace(" w:default=\"1\"", "");
                    //Find default style definition
                    int iDefStart = xNewStylesXML.IndexOf("<w:name w:val=\"" + xDefaultStyle + "\"");
                    if (iDefStart > -1)
                    {
                        //Insert new text before preceding closing tag
                        xNewStylesXML = xNewStylesXML.Insert(iDefStart - 1, " w:default=\"1\"");
                    }
                }
                //Also replace defaultfonts node
                //GLOG 5353: Make sure the rFonts being replaced is in the proper pkg: node
                int iTargetDefaultFontsStart = xTargetStylesXML.IndexOf("<w:rFonts ");
                int iTargetDefaultFontsEnd = xTargetStylesXML.IndexOf("/>", iTargetDefaultFontsStart) + 2;
                int iSourceDefaultFontsStart = xNewStylesXML.IndexOf("<w:rFonts ");
                int iSourceDefaultFontsEnd = -1;
                if (iSourceDefaultFontsStart > -1)
                    iSourceDefaultFontsEnd = xNewStylesXML.IndexOf("/>", iSourceDefaultFontsStart) + 2;
                if (iTargetDefaultFontsStart > -1 && iTargetDefaultFontsEnd > -1 &&
                    iSourceDefaultFontsStart > -1 && iSourceDefaultFontsEnd > -1)
                {
                    string xDefaultFonts = xTargetStylesXML.Substring(iTargetDefaultFontsStart, iTargetDefaultFontsEnd - iTargetDefaultFontsStart);
                    xNewStylesXML = xNewStylesXML.Substring(0, iSourceDefaultFontsStart) + xDefaultFonts +
                        xNewStylesXML.Substring(iSourceDefaultFontsEnd);
                }

                xNewStylesXML = xSourceXML.Substring(0, iSourceStylesStart) +
                    xNewStylesXML + xSourceXML.Substring(iSourceStylesEnd);

                LMP.Benchmarks.Print(t0);
                return xNewStylesXML;
            }
            catch
            {
                //If an error occurs here, just return the Source XML unchanged
                return xSourceXML;
            }
        }

        /// <summary>
        /// Returns the Source XML with the Paragraph Style of the TargetXML applied
        /// </summary>
        /// <param name="xSourceXML"></param>
        /// <param name="xTargetXML"></param>
        /// <returns></returns>
        public static string ApplyTargetParagraphFormattingToXML(string xSourceXML, string xTargetXML,
            string xDefaultStyle, bool bStripNumberingTrailingCharacters)
        {
            //GLOG 5128
            DateTime t0 = DateTime.Now;
            try
            {
                string xNewXML = xSourceXML;
                //Merge Styles XML and set Default style for Normal paragraphs
                //GLOG 8248 (dm) - omit source styles
                if (String.IsWordOpenXML(xSourceXML))
                    xNewXML = MergeStylesWordOpenXML(xSourceXML, xTargetXML, xDefaultStyle, true);
                else
                    xNewXML = MergeStylesXML(xSourceXML, xTargetXML, xDefaultStyle, true);
                
                string xSourceBodyXml = ExtractBodyXML(xNewXML);
                if (xSourceBodyXml == "")
                    return xSourceXML;

                string xNewBodyXml = xSourceBodyXml;
                
                string xTargetBodyXml = ExtractBodyXML(xTargetXML);

                string xParaProps = "";
                int iParaPropStart = xTargetBodyXml.IndexOf("<w:pPr>");
                int iParaPropEnd = -1;
                if (iParaPropStart > -1)
                {
                    //Get Paragraph Properties from target Paragraph
                    iParaPropEnd = xTargetBodyXml.IndexOf("</w:pPr>", iParaPropStart);
                    if (iParaPropEnd > -1)
                        xParaProps = xTargetBodyXml.Substring(iParaPropStart, (iParaPropEnd + @"</w:pPr>".Length) - iParaPropStart);
                }
                iParaPropStart =   xNewBodyXml.IndexOf("<w:pPr>");
                //First remove all existing <w:pPr> nodes - not all paragraphs may have one
                while (iParaPropStart > -1)
                {
                    iParaPropEnd = xNewBodyXml.IndexOf("</w:pPr>", iParaPropStart);
                    if (iParaPropEnd == -1)
                        break;
                    xNewBodyXml = xNewBodyXml.Substring(0, iParaPropStart) + xNewBodyXml.Substring(iParaPropEnd + @"</w:pPr>".Length);
                    iParaPropStart = xNewBodyXml.IndexOf("<w:pPr>", iParaPropStart + 1);
                }
                //Now insert Target Paragraph Properties
                int iParaStart = xNewBodyXml.IndexOf("<w:p>");
                //First remove all existing <w:pPr> nodes - not all paragraphs may have one
                while (iParaStart > -1)
                {
                    xNewBodyXml = xNewBodyXml.Substring(0, iParaStart + @"<w:p>".Length) + xParaProps + xNewBodyXml.Substring(iParaStart + @"<w:p>".Length);
                    iParaStart = xNewBodyXml.IndexOf("<w:p>", iParaStart + 1);
                }

                //GLOG 8343 (dm) - segments created before 11.2.0.40 will contain w:rsid attributes
                //if they were never opened and saved in the designer
                iParaStart = xNewBodyXml.IndexOf("<w:p w:rsid");
                //First remove all existing <w:pPr> nodes - not all paragraphs may have one
                while (iParaStart > -1)
                {
                    int iEndTagPos = xNewBodyXml.IndexOf(">", iParaStart);
                    xNewBodyXml = xNewBodyXml.Substring(0, iEndTagPos + 1) + xParaProps +
                        xNewBodyXml.Substring(iEndTagPos + 1);
                    iParaStart = xNewBodyXml.IndexOf("<w:p w:rsid", iParaStart + 1);
                }

                if (xNewBodyXml != xSourceBodyXml)
                {
                    xNewXML = xNewXML.Replace(xSourceBodyXml, xNewBodyXml);
                }

                //GLOG 8343 (dm) - remove orphaned trailing characters from formerly numbered paragraphs
                if (bStripNumberingTrailingCharacters)
                {
                    //shift-returns
                    int iPos = xNewXML.IndexOf("<w:br/>");
                    while (iPos > -1)
                    {
                        int iPos2 = xNewXML.LastIndexOf("<w:p>", iPos);
                        int iPos3 = xNewXML.LastIndexOf("<w:p w:rsid", iPos);
                        iPos2 = Math.Max(iPos2, iPos3);
                        iPos3 = xNewXML.LastIndexOf("<w:t>", iPos);
                        if (iPos2 > iPos3)
                            xNewXML = xNewXML.Substring(0, iPos) + xNewXML.Substring(iPos + 7);
                        else
                            iPos += 7;
                        iPos = xNewXML.IndexOf("<w:br/>", iPos);
                    }

                    //double spaces
                    iPos = xNewXML.IndexOf("<w:t xml:space=\"preserve\">  </w:t>");
                    while (iPos > -1)
                    {
                        int iPos2 = xNewXML.LastIndexOf("<w:p>", iPos);
                        int iPos3 = xNewXML.LastIndexOf("<w:p w:rsid", iPos);
                        iPos2 = Math.Max(iPos2, iPos3);
                        iPos3 = xNewXML.LastIndexOf("<w:t>", iPos);
                        if (iPos2 > iPos3)
                            xNewXML = xNewXML.Substring(0, iPos) + xNewXML.Substring(iPos + 34);
                        else
                            iPos += 34;
                        iPos = xNewXML.IndexOf("<w:t xml:space=\"preserve\">  ", iPos);
                    }
                }

                LMP.Benchmarks.Print(t0);
                return xNewXML;
            }
            catch
            {
                //If an error occurs here, just return the Source XML unchanged
                return xSourceXML;
            }
        }

        /// <summary>
        /// returns true if string contains only spaces
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static bool ContainsOnlySpaces(string xInput)
        {
            if (string.IsNullOrEmpty(xInput))
                return false;

            for (int i = 0; i < xInput.Length; i++)
            {
                //Non-space character found
                if (char.GetUnicodeCategory(xInput, i) != System.Globalization.UnicodeCategory.SpaceSeparator)
                    return false;
            }
            //No non-space characters found
            return true;
        }
        /// <summary>
        /// Converts Date string to ISO8601 format - yyyy-MM-ddThh:mm:ssZ
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string ConvertDateToISO8601(string xInput)
        {

            string xTemp = xInput;
            int iPos = xTemp.IndexOf("+");
            if (iPos > -1)
                xTemp = xTemp.Substring(0, iPos - 1);
            DateTime oDt;
            try
            {
                //GLOG 4310: Don't assume U.S. English culture, since some regional date formats 
                //may not be valid (e.g., "dd/MM/yyyy")
                oDt = DateTime.Parse(xTemp);
            }
            catch
            {
                return xInput;
            }
            return XmlConvert.ToString(oDt, XmlDateTimeSerializationMode.Utc);
        }

        /// <summary>
        /// returns the tag prefix id
        /// </summary>
        /// <param name="xObjectData"></param>
        /// <returns></returns>
        public static string GetTagPrefixID(string xObjectData)
        {
            int iPos = xObjectData.IndexOf(mpTagPrefixIDSeparator);
            if (iPos > -1)
                return xObjectData.Substring(0, iPos);
            else
                return "";
        }
        /// <summary>
        /// Removes any <w:pict> tags with content corresponding to the Document Stamp types specified from XML
        /// </summary>
        /// <param name="xInput"></param>
        /// <param name="bRemoveTrailers"></param>
        /// <param name="bRemoveDraftStamps"></param>
        /// <returns></returns>
        public static string RemoveDocumentStampXML(string xInput, string xNSPrefix, bool bRemoveTrailers, bool bRemoveDraftStamps)
        {
            const string xPictStartTag = "<w:pict>";
            const string xPictEndTag = "</w:pict>";
            int iStartPict = 0;
            int iEndPict = 0;
            int iCursorPos = 0;
            string xOutput = "";
            iStartPict = xInput.IndexOf(xPictStartTag);

            while (iStartPict > -1)
            {
                iEndPict = xInput.IndexOf(xPictEndTag, iStartPict);
                if (iEndPict > -1)
                {
                    int iStartTest = iStartPict + xPictStartTag.Length;
                    //Shapes may be nested, so move past end tag of contained shapes
                    while (xInput.Substring(iStartTest, iEndPict - iStartTest).Contains(xPictStartTag))
                    {
                        iStartTest = iEndPict + xPictEndTag.Length;
                        iEndPict = xInput.IndexOf(xPictEndTag, iEndPict + xPictEndTag.Length);
                    }

                    iEndPict = iEndPict + xPictEndTag.Length;
                    //Check if shape object contains mSEG tag with ObjectTypeID matching Stamp type to be removed
                    //GLOG 5739:  Trailer textbox will no longer contain XML tags, so check for presence of MacPac Trailer style as well
                    string xPictContent = xInput.Substring(iStartPict, iEndPict - iStartPict);
                    if (xPictContent.Contains("<" + xNSPrefix + ":mSEG") &&
                        (bRemoveTrailers && xPictContent.Contains("|ObjectTypeID=401|")) ||
                        (bRemoveDraftStamps && xPictContent.Contains("|ObjectTypeID=510|")) ||
                        (bRemoveTrailers && xPictContent.Contains("<w:pStyle w:val=\"MacPacTrailer\"/>")))
                    {
                        //Matches type to be removed - don't include shape xml in output
                        xOutput = xOutput + xInput.Substring(iCursorPos, iStartPict - iCursorPos);
                    }
                    else
                    {
                        //No match, include this shape in output
                        xOutput = xOutput + xInput.Substring(iCursorPos, iEndPict - iCursorPos);
                    }
                }
                else
                {
                    //Should never end up here, but if we do, the input XML is malformed - return untouched
                    return xInput;
                }
                iCursorPos = iEndPict;
                //Search for additional shapes
                iStartPict = xInput.IndexOf("<w:pict>", iCursorPos);
            }
            //Append remainder of XML
            xOutput = xOutput + xInput.Substring(iCursorPos);
            return xOutput;
        }
        public static string RemoveDocumentStampXML_CC(string xInput, bool bRemoveTrailers, bool bRemoveDraftStamps)
        {
            const string xPictStartTag = "<w:pict>";
            const string xPictEndTag = "</w:pict>";
            int iStartPict = 0;
            int iEndPict = 0;
            int iCursorPos = 0;
            string xOutput = "";

            iStartPict = xInput.IndexOf(xPictStartTag);

            while (iStartPict > -1)
            {
                iEndPict = xInput.IndexOf(xPictEndTag, iStartPict);
                if (iEndPict > -1)
                {
                    int iStartTest = iStartPict + xPictStartTag.Length;
                    //Shapes may be nested, so move past end tag of contained shapes
                    while (xInput.Substring(iStartTest, iEndPict - iStartTest).Contains(xPictStartTag))
                    {
                        iStartTest = iEndPict + xPictEndTag.Length;
                        iEndPict = xInput.IndexOf(xPictEndTag, iEndPict + xPictEndTag.Length);
                    }

                    iEndPict = iEndPict + xPictEndTag.Length;
                    //Check if shape object contains mSEG tag with ObjectTypeID matching Stamp type to be removed
                    string xPictContent = xInput.Substring(iStartPict, iEndPict - iStartPict);

                    int iPos = xPictContent.IndexOf("<w:tag w:val=\"mps");
                    if (iPos > -1)
                    {
                        iPos = iPos + "<w:tag w:val=\"mps".Length;

                        string xID = "mps" + xPictContent.Substring(iPos, 8); ;

                        xID = xID.Replace("mps", "mpo");

                        string xObjData = GetObjectData_CC(xInput, xID);

                        if ((bRemoveTrailers && xObjData.Contains("|ObjectTypeID=401|")) ||
                            (bRemoveDraftStamps && xObjData.Contains("|ObjectTypeID=510|")))
                        {
                            //Matches type to be removed - don't include shape xml in output
                            xOutput = xOutput + xInput.Substring(iCursorPos, iStartPict - iCursorPos);
                        }
                        else
                        {
                            //No match, include this shape in output
                            xOutput = xOutput + xInput.Substring(iCursorPos, iEndPict - iCursorPos);
                        }
                    }
                    else
                    {
                        //GLOG 5739: Trailer textbox no longer contains Content Controls
                        //Also check for presence of MacPac Trailer style within the Textbox to identify Trailer
                        if (bRemoveTrailers && xPictContent.Contains("<w:pStyle w:val=\"MacPacTrailer\"/>"))
                        {
                            xOutput = xOutput + xInput.Substring(iCursorPos, iStartPict - iCursorPos);
                        }
                        else
                            //GLOG 4913: Shape doesn't contain any ContentControls - include in output
                            xOutput = xOutput + xInput.Substring(iCursorPos, iEndPict - iCursorPos);
                    }
                }
                else
                {
                    //Should never end up here, but if we do, the input XML is malformed - return untouched
                    return xInput;
                }
                iCursorPos = iEndPict;
                //Search for additional shapes
                iStartPict = xInput.IndexOf("<w:pict>", iCursorPos);
            }
            //Append remainder of XML
            xOutput = xOutput + xInput.Substring(iCursorPos);
            return xOutput;
        }

        public static string GetObjectData_CC(string xDocXML, string xDocVarID)
        {
            XmlNamespaceManager oNS = null;

            XmlDocument oDocXML = new XmlDocument();
            oDocXML.LoadXml(xDocXML);
            oNS = new XmlNamespaceManager(oDocXML.NameTable);
            oNS.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            XmlNode oNode = oDocXML.SelectSingleNode("//w:docVar[@w:name='" + xDocVarID + "']", oNS);
            string xDocVar = oNode.Attributes["w:val"].Value;
            string xObjData = xDocVar.Substring(xDocVar.IndexOf("ÌÍ") + 2);
            return String.Decrypt(xObjData);
        }

        /// <summary>
        /// Replaces contents of all mSEG nodes matching TagID in XML with corresponding Array contents
        /// </summary>
        /// <param name="xInput"></param>
        /// <param name="iIndex"></param>
        /// <param name="xNSPrefix"></param>
        /// <param name="xTagID"></param>
        /// <param name="xReplacement"></param>
        /// <returns></returns>
        public static bool SubstituteTrailerXML(ref string xInput, int iIndex, string xNSPrefix, string xTagID, 
            string[] xReplacement)
        {
            try
            {
                string xOldIndex = "";
                string xBuffer = xInput;
                string xSearch = ":mSEG TagID=\"" + xTagID + "_";
                int iPos = xInput.IndexOf(xSearch);
                if (iPos > -1)
                {
                    iPos = iPos + xSearch.Length;
                    int iPos2 = xInput.IndexOf("\"", iPos);
                    xOldIndex = xInput.Substring(iPos, iPos2 - iPos);
                    if (xOldIndex != iIndex.ToString())
                        xBuffer = xInput.Replace(xSearch + xOldIndex, xSearch + iIndex.ToString());
                }
                else
                    return false;
                //Replace any reserved Regex characters with Escape codes
                string xTagIDRegex = Regex.Escape(xTagID);
                //Collect all matching mSEGs
                Regex oRegex = new Regex("<" + xNSPrefix + ":mSEG TagID=\\\"" + xTagIDRegex + "_.*?</" + xNSPrefix + ":mSEG>");
                MatchCollection oMatches = oRegex.Matches(xBuffer);
                if (oMatches.Count == 0 || oMatches.Count != xReplacement.Length)
                    return false;
                string xNewXML = "";
                int iCounter = 0;
                for (int i = 0; i < oMatches.Count; i++)
                {
                    Match oMatch = oMatches[i];
                    string xTag = oMatch.Value;
                    int iStart = xTag.IndexOf(">") + 1;
                    int iEnd = xTag.IndexOf("</" + xNSPrefix + ":mSEG>");
                    xNewXML = xNewXML + xBuffer.Substring(iCounter, (oMatch.Index + iStart) - iCounter)
                        + xReplacement[i] + xBuffer.Substring(oMatch.Index + iEnd, oMatch.Length - iEnd);
                    iCounter = oMatch.Index + iEnd + (@"</" + xNSPrefix + ":mSEG>").Length;
                }
                xNewXML = xNewXML + xBuffer.Substring(iCounter);
                xInput = xNewXML;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool ContentContainsLinkedStories(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            //GLOG 5472: Support Content Controls or XML Tags
            //GLOG 4243: Returns true if w:ftr or w:hdr node corresponding to one 
            //of the linked stories contains mSEG tag or Content Control
            if (LMP.String.IsWordOpenXML(xXML))
                return ContentContainsLinkedStoriesCC(xXML, bIncludePrimaryFooter, bIncludePrimaryHeader, bIncludeFirstFooter,
                    bIncludeFirstHeader, bIncludeEvenFooter, bIncludeEvenHeader);
            else
                return ContentContainsLinkedStoriesXML(xXML, bIncludePrimaryFooter, bIncludePrimaryHeader, bIncludeFirstFooter,
                    bIncludeFirstHeader, bIncludeEvenFooter, bIncludeEvenHeader);
        }
        public static bool ContentContainsUnlinkedStories(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            //GLOG 5472: Returns true if w:ftr or w:hdr node corresponding to one 
            //of the unlinked stories contains mSEG tag or Content Control
            if (LMP.String.IsWordOpenXML(xXML))
                return ContentContainsUnlinkedStoriesCC(xXML, bIncludePrimaryFooter, bIncludePrimaryHeader, bIncludeFirstFooter,
                    bIncludeFirstHeader, bIncludeEvenFooter, bIncludeEvenHeader);
            else
                return ContentContainsUnlinkedStoriesXML(xXML, bIncludePrimaryFooter, bIncludePrimaryHeader, bIncludeFirstFooter,
                    bIncludeFirstHeader, bIncludeEvenFooter, bIncludeEvenHeader);
        }
        private static bool ContentContainsLinkedStoriesXML(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            Match oMatch;
            string xNameSpacePrefix = GetNamespacePrefix(xXML, "urn-legalmacpac-data/10");
            string xMatchTemplate = "<w:{0} w:type=\"{1}\".*?</w:{0}>";
            string xMatch;
            if (bIncludePrimaryFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] {"ftr", "odd"});
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (bIncludePrimaryHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "odd" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (bIncludeFirstFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "ftr", "first" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (bIncludeFirstHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "first" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (bIncludeEvenFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "ftr", "even" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (bIncludeEvenHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "even" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            return false;
        }
        private static bool ContentContainsLinkedStoriesCC(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            //GLOG 4243: Returns true if w:footerReference or w:headerReference node corresponding to one 
            //of the linked stories contains mSEG Content Control
            string xStory;
            if (bIncludePrimaryFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "default");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (bIncludePrimaryHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "default");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (bIncludeFirstFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "first");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (bIncludeFirstHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "first");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (bIncludeEvenFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "even");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (bIncludeEvenHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "even");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            return false;
        }
        private static bool ContentContainsUnlinkedStoriesCC(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            //GLOG 4243: Returns true if w:footerReference or w:headerReference node corresponding to one 
            //of the unlinked stories contains mSEG Content Control
            string xStory;
            if (!bIncludePrimaryFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "default");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (!bIncludePrimaryHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "default");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (!bIncludeFirstFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "first");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (!bIncludeFirstHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "first");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (!bIncludeEvenFooter)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "footer", "even");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            if (!bIncludeEvenHeader)
            {
                xStory = LMP.String.ExtractHeaderFooterXML(xXML, "header", "even");
                if (xStory.Contains("<w:sdt><w:sdtPr><w:tag w:val=\"mps"))
                    return true;
            }
            return false;
        }
        private static bool ContentContainsUnlinkedStoriesXML(string xXML, bool bIncludePrimaryFooter, bool bIncludePrimaryHeader,
            bool bIncludeFirstFooter, bool bIncludeFirstHeader, bool bIncludeEvenFooter, bool bIncludeEvenHeader)
        {
            //GLOG 4243: Returns true if w:ftr or w:hdr node corresponding to one 
            //of the linked stories contains mSEG tag
            Match oMatch;
            string xNameSpacePrefix = GetNamespacePrefix(xXML, "urn-legalmacpac-data/10");
            string xMatchTemplate = "<w:{0} w:type=\"{1}\".*?</w:{0}>";
            string xMatch;
            if (!bIncludePrimaryFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "ftr", "odd" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (!bIncludePrimaryHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "odd" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (!bIncludeFirstFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "ftr", "first" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (!bIncludeFirstHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "first" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (!bIncludeEvenFooter)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "ftr", "even" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            if (!bIncludeEvenHeader)
            {
                xMatch = string.Format(xMatchTemplate, new object[] { "hdr", "even" });
                oMatch = Regex.Match(xXML, xMatch);
                if (oMatch.Value != "" && oMatch.Value.Contains("<" + xNameSpacePrefix + ":mSEG "))
                    return true;
            }
            return true;
        }
        /// <summary>
        /// Returns XML of w:ftr or w:hdr node corresponding to specified story
        /// </summary>
        /// <param name="xXML">Content XML</param>
        /// <param name="xType">"header" or "footer"</param>
        /// <param name="xItem">"default", "first" or "even"</param>
        /// <returns></returns>
        private static string ExtractHeaderFooterXML(string xXML, string xType, string xItem)
        {
            Match oMatch;
            Match oRel;
            Match oPkg;
            string xMatchTemplate;
            string xRelID = "";
            string xElement = "";
            string xTarget = "";
            int iPos;
            int iPos2;
            if (xType == "header")
                xElement = "hdr";
            else
                xElement = "ftr";

	        xMatchTemplate = "<w:" + xType + "Reference w:type=\"" + xItem + "\".*?>";
            oMatch = Regex.Match(xXML, xMatchTemplate);
            if (oMatch.Value != "")
            {
                iPos = oMatch.Value.IndexOf("r:id=") + 6;
                iPos2 = oMatch.Value.IndexOf("\"", iPos);
                xRelID = oMatch.Value.Substring(iPos, iPos2 - iPos);
                oRel = Regex.Match(xXML, "<Relationship Id=\"" + xRelID + "\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/" +
                    xType + "\".*?>");
                if (oRel.Value != "")
                {
                    iPos = oRel.Value.IndexOf("Target=");
                    if (iPos > -1)
                    {
                        iPos = iPos + 8;
                        iPos2 = oRel.Value.IndexOf("\"", iPos);
                        xTarget = oRel.Value.Substring(iPos, iPos2-iPos);
                    }
                    //GLOG 8651: Relationships added by Oxml may use full path for the Target
                    xTarget = xTarget.Replace("/word/", "");
                    oPkg = Regex.Match(xXML, "<pkg:part pkg:name=\"/word/" + xTarget + "\".*?</pkg:part>");
                    if (oPkg.Value != "")
                    {
                        iPos = oPkg.Value.IndexOf("<w:" + xElement);
                        if (iPos > -1)
                        {
                            iPos = oPkg.Value.IndexOf(">", iPos);
                            if (iPos > -1)
                            {
                                iPos = iPos + 1;
                                iPos2 = oPkg.Value.IndexOf("</w:" + xElement + ">", iPos);
                                if (iPos2 > -1)
                                    return oPkg.Value.Substring(iPos, iPos2 - iPos);
                            }
                        }
                    }
                }
            }
            return "";
        }
        /// <summary>
        /// returns true if input string is Word Open XML
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        public static bool IsWordOpenXML(string xXML)
        {
            return (xXML.IndexOf("<pkg:package ") != -1);
        }

        /// <summary>
        /// returns the MacPac schema base name embedded in the specified tag
        /// </summary>
        /// <param name="xTag"></param>
        /// <returns></returns>
        public static string GetBoundingObjectBaseName(string xTag)
        {
            if (System.String.IsNullOrEmpty(xTag) || (xTag.Length < 3) ||
                (xTag.Substring(0, 2) != "mp"))
                return "";

            switch (xTag.Substring(2,1))
            {
                case "s":
                    return "mSEG";
                case "v":
                    return "mVar";
                case "b":
                    return "mBlock";
                case "d":
                    return "mDel";
                case "u":
                    return "mSubVar";
                case "p":
                    return "mDocProps";
                case "c":
                    return "mSecProps";
                default:
                    return "";
            }
        }

        /// <summary>
        /// returns the id of the document variables associated with the
        /// specified tag
        /// </summary>
        /// <param name="xTag"></param>
        /// <returns></returns>
        public static string GetDocVarIDFromTag(string xTag)
        {
            return xTag.Substring(3, 8);
        }
        /// <summary>
        /// Returns input string with any characters not valid for a Windows filename removed
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string RemoveInvalidFileNameCharacters(string xInput)
        {
            //GLOG 4844
            char[] aC = Path.GetInvalidFileNameChars();
            foreach (char c in aC)
            {
                xInput = xInput.Replace(c.ToString(), "");
            }
            return xInput;
        }

        public static bool IsEncrypted(string xText)
        {
            //GLOG : 8239 : ceh
            if (System.String.IsNullOrEmpty(xText))
                return false;

            if (xText.Length < 3)
                return false;
            if (xText.Substring(0, 3) == "^`~")
                //new encryption marker
                return true;
            else if (xText.Substring(0, 1) == '\u2514'.ToString())
                //old encryption markers
                return ((xText.Substring(1,1)  == '\u2524'.ToString()) ||
                    (xText.Substring(1,1)  == '\u2518'.ToString()));
            else
                return false;
        }
        /// <summary>
        /// Check if XML contains an mBlock with IsBody=true
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        public static bool XMLContainsBodyBlock(string xXML)
        {
            try
            {
                if (LMP.String.IsWordOpenXML(xXML))
                {
                    XmlDocument oXMLDoc = new XmlDocument();
                    XmlNamespaceManager oNSMan = new XmlNamespaceManager(oXMLDoc.NameTable);
                    oXMLDoc.LoadXml(xXML);

                    oNSMan.AddNamespace("w", LMP.Data.ForteConstants.WordOpenXMLNamespace);

                    //cycle through all content controls
                    //whose tag indicates a block
                    XmlNodeList oNodes = oXMLDoc.SelectNodes("//w:sdt/w:sdtPr/w:tag[starts-with(@w:val,\"mpb\")]", oNSMan);

                    foreach (XmlNode oNode in oNodes)
                    {
                        //check corresponding doc var
                        //for body=true
                        XmlAttribute oAttr = oNode.Attributes["w:val"];
                        string xTag = oAttr.Value;
                        string xDocVarID = "mpo" + xTag.Substring(3, 8);

                        XmlNode oDocVarNode = oXMLDoc.SelectSingleNode("//w:docVar[@w:name=\"" + xDocVarID + "\"]", oNSMan);
                        oAttr = oDocVarNode.Attributes["w:val"];
                        string xData = oAttr.Value;

                        int iSepPos = 0;

                        //IsBody is fifth field of ObjectData attribute
                        for (int i = 1; i <= 4; i++)
                        {
                            iSepPos = xData.IndexOf("|", iSepPos + 1);
                        }
                        if (iSepPos > -1)
                        {
                            if (xData.Substring(iSepPos + 1, 4).ToUpper() == "TRUE")
                                return true;
                        }
                    }

                    return false;
                }
                else
                {
                    string xData = "";
                    int iEnd = 0;
                    // Find next mBlock tag
                    int iStart = xXML.IndexOf("<" + String.GetNamespacePrefix(xXML,
                        LMP.Data.ForteConstants.MacPacNamespace) + ":mBlock ");
                    while (iStart > -1)
                    {
                        // Get closing bracket of tag
                        iEnd = xXML.IndexOf(">", iStart + 11);
                        // Work with text of ObjectData property
                        int iObjData = xXML.IndexOf("ObjectData=", iStart, iEnd - iStart);
                        if (iObjData > -1)
                        {
                            xData = xXML.Substring(iObjData, iEnd - iObjData);
                            int iSepPos = 0;
                            //IsBody is fifth field of ObjectData attribute
                            for (int i = 1; i <= 4; i++)
                            {
                                iSepPos = xData.IndexOf("|", iSepPos + 1);
                            }
                            if (iSepPos > -1)
                            {
                                if (xData.Substring(iSepPos + 1, 4).ToUpper() == "TRUE")
                                    return true;
                            }
                        }
                        // Look for additional mBlocks
                        iStart = xXML.IndexOf("<" + String.GetNamespacePrefix(xXML,
                            LMP.Data.ForteConstants.MacPacNamespace) + ":mBlock ", iStart + 11);
                    }
                    // If we got here, none were found
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public static string ReplaceQuotesWithSmartQuotes(string xInput)
        {
            int i;
            string xTemp;
            xTemp = xInput;
            //GLOG 6065: Adjusted to deal with &quot; and &amp;quot; in string
            xTemp = xTemp.Replace("&amp;", "&"); //Replace any XML tokens first
            xTemp = xTemp.Replace("&quot;", "\"");
            xTemp = xTemp.Replace("&apos;", "'");
            xTemp = xTemp.Replace("&", "&amp;"); //restore any standalone &amp; tokens
            do
            {
                i = xTemp.IndexOf((char)34);
                switch (i)
                {
                    case -1:
                        break;
                    case 0:
                        xTemp = LMP.String.ReplaceInstance(xTemp, ((char)34).ToString(), ((char)8220).ToString(), 1);
                        break;
                    default:
                        switch (xTemp[i - 1])
                        {
                            case '-':
                            case ' ':
                            case '(':
                            case '{':
                            case '[':
                            case (char)13: //Paragraph mark
                            case (char)11: //Line break
                            case (char)10: //New line
                            case (char)12: //Page break
                            case (char)9: //tab
                            case (char)30: //hyphen
                            case (char)150: //em-dash
                            case (char)151: //en-dash
                            case (char)160: //Required space
                                xTemp = LMP.String.ReplaceInstance(xTemp, ((char)34).ToString(), ((char)8220).ToString(), 1);
                                break;
                            default:
                                xTemp = LMP.String.ReplaceInstance(xTemp, ((char)34).ToString(), ((char)8221).ToString(), 1);
                                break;
                        }
                        break;
                }
            }
            while (i > -1);
            do
            {
                i = xTemp.IndexOf((char)39);
                switch (i)
                {
                    case -1:
                        break;
                    case 0:
                        xTemp = LMP.String.ReplaceInstance(xTemp, ((char)39).ToString(), ((char)8216).ToString(), 1);
                        break;
                    default:
                        switch (xTemp[i - 1])
                        {
                            case '-':
                            case ' ':
                            case '(':
                            case '{':
                            case '[':
                            case (char)13: //Paragraph mark
                            case (char)11: //Line break
                            case (char)10: //New line
                            case (char)12: //Page break
                            case (char)9: //tab
                            case (char)30: //hyphen
                            case (char)150: //em-dash
                            case (char)151: //en-dash
                            case (char)160: //Required space
                                xTemp = LMP.String.ReplaceInstance(xTemp, ((char)39).ToString(), ((char)8216).ToString(), 1);
                                break;
                            default:
                                xTemp = LMP.String.ReplaceInstance(xTemp, ((char)39).ToString(), ((char)8217).ToString(), 1);
                                break;
                        }
                        break;
                }
            }
            while (i > -1);
            return xTemp;
        }

        public static string AddNumberingRelationship(string xWordOpenXML)
        {
            //get document relationships part
            int iPos = xWordOpenXML.IndexOf("<pkg:part pkg:name=\"/word/_rels/document.xml.rels\"");
            if (iPos == -1)
                return xWordOpenXML;
            int iPos2 = xWordOpenXML.IndexOf("</pkg:part>", iPos);
            string xPart = xWordOpenXML.Substring(iPos, iPos2 - iPos);

            //check whether relationship already exists
            if (xPart.IndexOf("Target=\"numbering.xml\"") > -1)
                return xWordOpenXML;

            //get next available id
            int iIndex = 1;
            while (xPart.IndexOf("<Relationship Id=\"rId" + iIndex.ToString() + "\"") > -1)
                iIndex++;

            //add relationship
            iPos2 = xWordOpenXML.IndexOf("</Relationships>", iPos);
            return xWordOpenXML.Substring(0, iPos2) + "<Relationship Id=\"rId" + iIndex.ToString() +
                "\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering\" Target=\"numbering.xml\"/>" +
                xWordOpenXML.Substring(iPos2);
        }

        /// <summary>
        /// parses the specified definition string into segment xml and prepended tags
        /// </summary>
        /// <param name="xDefString"></param>
        /// <param name="xSegmentXml"></param>
        /// <param name="xTags"></param>
        public static void ParseSegmentDefString(string xDefString, out string xSegmentXml, out string xTags)
        {
            //parse out "true" xml - the field
            //may begin with prepended mp tags like <mAuthorPrefs>
            int iPos = xDefString.IndexOf("<?xml");

            if (iPos > -1)
            {
                xSegmentXml = xDefString.Substring(iPos);
                xTags = xDefString.Substring(0, iPos);
            }
            else
            {
                iPos = xDefString.IndexOf("<?mso-application progid=\"Word.Document\"?>");

                if (iPos > -1)
                {
                    xSegmentXml = xDefString.Substring(iPos);
                    xTags = xDefString.Substring(0, iPos);
                }
                else
                {
                    //test for start of Base64 string
                    iPos = xDefString.IndexOf(">UEsDBBQABgAIA");
                    if (iPos > -1)
                    {
                        xSegmentXml = xDefString.Substring(iPos + 1);
                        xTags = xDefString.Substring(0, iPos + 1);
                    }
                    else
                    {
                        xSegmentXml = xDefString;
                        xTags = "";
                    }
                }
            }
        }

        /// <summary>
        /// returns the segment xml portion of the segment definition string
        /// </summary>
        /// <param name="xDefString"></param>
        /// <returns></returns>
        public static string GetSegmentXmlFromDefString(string xDefString)
        {
            string xSegmentXml = null;
            string xTags = null;

            ParseSegmentDefString(xDefString, out xSegmentXml, out xTags);

            return xSegmentXml;
        }

        public static bool IsBase64SegmentString(string xSegmentString)
        {
            if (xSegmentString.StartsWith("<mAuthorPrefs>"))
            {
                //trim author prefs tag
                xSegmentString = xSegmentString.Substring(14);
            }

            return xSegmentString.StartsWith("UEsDBBQABgAIA");
        }

        /// <summary>
        /// returns the prepended tags porition of the segment definition string
        /// </summary>
        /// <param name="xDefString"></param>
        /// <returns></returns>
        public static string GetSegmentTagsFromDefString(string xDefString)
        {
            string xSegmentXml = null;
            string xTags = null;

            ParseSegmentDefString(xDefString, out xSegmentXml, out xTags);

            return xTags;
        }
        //GLOG 8213
        public static string DeleteOrphanedDocVarsInXml(string xInput)
        {
            DateTime t0 = DateTime.Now;

            int iCount = 0;
            string xOutput = xInput;
            string xDeletedTags = "";
            Match oDocVars = Regex.Match(xInput, "<w:docVars>.*?</w:docVars>");
            string xDocVarXml = oDocVars.Value;
            if (xDocVarXml != "")
            {
                string xNewDocVarXml = xDocVarXml;
                DateTime t1 = DateTime.Now;
                //Append all variable values to single string - text being searched may be split between two doc variables
                //Parens delimit two groups: 1. 8-digit numeric tag; 2. Variable value
                Match oDelVar = Regex.Match(xDocVarXml, "<w:docVar w:name=\"mpd(\\d{8})01.*? w:val=\"(.*?)\".*?/>");
                while (oDelVar.Value != "")
                {
                    string xDelTag = oDelVar.Groups[1].Value;
                    int iNextIndex = 1;
                    Match oNextDel = oDelVar;
                    while (oNextDel.Value != "")
                    {
                        string xDel = oNextDel.Groups[2].Value;
                        xDeletedTags += xDel;
                        iNextIndex++;
                        oNextDel = Regex.Match(xDocVarXml, "<w:docVar w:name=\"mpd(" + xDelTag + ")" + iNextIndex.ToString().PadLeft(2, '0') + ".*? w:val=\"(.*?)\".*?/>");
                    }
                    if (!xDeletedTags.EndsWith("|"))
                        xDeletedTags = xDeletedTags + "|";
                    oDelVar = oDelVar.NextMatch();
                }
                LMP.Benchmarks.Print(t1, "Get DeletedTags");
                t1 = DateTime.Now;
                Match oDocVar = Regex.Match(xDocVarXml, "<w:docVar w:name=\"mpo(\\d{8}).*?/>");
                while (oDocVar.Value != "")
                {
                    string xTag = oDocVar.Groups[1].Value;
                    //JTS 11/23/15:  Tag may appear in these contexts:
                    //1. Content Control Tag: w:val="mpv12345678000...
                    //2. XML Tag: Reserved="mpv12345678000...
                    //3. Content Control Tag in DeletedScope: w:val=&quot;mpv12345678000...
                    //4. XML Tag in DeletedScope: Reserved=&quot;mpv12345678000...
                    //if (!Regex.IsMatch(xInput, "(Reserved|w:val)=(&quot;|\")mp[svbudpc]" + xTag + "000"))
                    //JTS 11/23/15: Using multiple Contains tests takes less than a third of the time of the complex Regex above
                    //GLOG 8376: Previous comparison was not finding match if ID1 of UserSegment had more than 6 digits
                    if ((!xInput.Contains("w:val=\"mpv" + xTag)) &&
                        (!xInput.Contains("w:val=\"mps" + xTag)) &&
                        (!xInput.Contains("w:val=\"mpb" + xTag)) &&
                        (!xInput.Contains("w:val=\"mpd" + xTag)) &&
                        (!xInput.Contains("w:val=\"mpu" + xTag)) &&
                        (!xInput.Contains("w:val=\"mpp" + xTag)) &&
                        (!xInput.Contains("w:val=\"mpc" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpv" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mps" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpb" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpd" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpu" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpp" + xTag)) &&
                        (!xDeletedTags.Contains("w:val=&quot;mpc" + xTag))  &&
                        (!xInput.Contains("Reserved=\"mpv" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mps" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mpb" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mpd" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mpu" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mpp" + xTag)) &&
                        (!xInput.Contains("Reserved=\"mpc" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpv" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mps" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpb" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpd" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpu" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpp" + xTag)) &&
                        (!xDeletedTags.Contains("Reserved=&quot;mpc" + xTag)))
                    {
                        //Remove individual <w:docVar> node from <w:docVars>
                        xNewDocVarXml = xNewDocVarXml.Replace(oDocVar.Value, "");
                        iCount++;
                    }
                    oDocVar = oDocVar.NextMatch();
                }
                LMP.Benchmarks.Print(t1, "Update DocVars XML");
                if (iCount > 0)
                {
                    t1 = DateTime.Now;
                    //Replace original <w:docVars> node with updated string
                    //JTS 11/23/15: Using Remove and Insert is slightly faster than xOutput.Replace
                    xOutput = xOutput.Remove(oDocVars.Index, oDocVars.Length);
                    xOutput = xOutput.Insert(oDocVars.Index, xNewDocVarXml);
                    LMP.Benchmarks.Print(t1, "Replace DocVars XML");
                }
            }
            LMP.Benchmarks.Print(t0, iCount + " variables removed");
            return xOutput;
        }
        //GLOG 8248
        public static bool ContentUsesNumberingScheme(string xSourceXML)
        {
            try
            {

                //Get Body XML 
                string xBodyXML = ExtractBodyXML(xSourceXML);
                //Get collection of all Styles used in paragraphs in body
                MatchCollection oMatches = Regex.Matches(xBodyXML, "<w:pStyle.*?>");
                foreach (Match oMatch in oMatches)
                {
                    int iPos = oMatch.Value.IndexOf("w:val=\"");
                    if (iPos > -1)
                    {
                        iPos += "w:val=\"".Length;
                        int iPos2 = oMatch.Value.IndexOf("\"", iPos);
                        if (iPos2 > -1)
                        {
                            string xStyleId = oMatch.Value.Substring(iPos, iPos2 - iPos);
                            //Check styles that match "Heading[1-9]" or end with "L[1-9]")
                            if ((xStyleId.ToLower().StartsWith("heading") || xStyleId.ToLower().Substring(xStyleId.Length-2, 1) == "l") && 
                                (String.IsNumericInt32(xStyleId.Substring(xStyleId.Length - 1)) && Int32.Parse(xStyleId.Substring(xStyleId.Length-1)) > 0))
                            {
                                //Test if specified style is associated with a List Template
                                string xListXML = ExtractListXML(xSourceXML);
                                if (xListXML.IndexOf("<w:pStyle w:val=\"" + xStyleId + "\"") > -1)
                                    return true;
                            }
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return false;
        }
        public static string RemoveBodyXmlNamespace(string xXml)
        {
            int iBodyStart = xXml.IndexOf("<w:body");
            if (iBodyStart > -1)
            {
                int iBodyEnd = xXml.IndexOf("</w:body");
                if (iBodyEnd > -1)
                {
                    string xBodyXML = xXml.Substring(iBodyStart, iBodyEnd - iBodyStart);
                    xBodyXML = xBodyXML.Replace(" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"", "");
                    xBodyXML = xBodyXML.Replace(" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"", "");
                    xXml = xXml.Substring(0, iBodyStart) + xBodyXML + xXml.Substring(iBodyEnd);
                }
            }
            return xXml;
        }
        public static string RemoveXMLWhiteSpace(string xXml)
        {
            XmlDocument oDoc = new XmlDocument();
            oDoc.PreserveWhitespace = false;
            oDoc.LoadXml(xXml);
            //remove extra spaces before closing tag -
            //some code does not expect this
            return oDoc.OuterXml.Replace(" />", "/>");
        }
        public static int XMLComplexity(string xXML)
        {
            DateTime t0 = DateTime.Now;
            int iValue = 0;
            try
            {
                iValue += String.CountChrs(xXML, "<w:docVar");
                iValue += String.CountChrs(xXML, "<w:sdt>");
            }
            catch { }
            LMP.Trace.WriteNameValuePairs("iValue", iValue);
            LMP.Benchmarks.Print(t0);
            return iValue;
        }
        public static bool ContainsWordObjectFieldCode(string xInput)
        {
            xInput = xInput.ToLower();
            return xInput.IndexOf("[style") > -1
                || xInput.IndexOf("[document_") > -1
                || xInput.IndexOf("[pagesetup") > -1
                || xInput.IndexOf("[wordapplication") > -1;
        }
        //GLOG 15838: Moved function to Base so it can be shared between API and Oxml
        /// <summary>
        /// returns value string from designated XML source for designated XML tag
        /// </summary>
        /// <param name="xXMLSource"></param>
        /// <param name="xTagName"></param>
        /// <returns></returns>
        public static string GetXMLExtract(string xXMLSource, string xTagName, int iIndex)
        {
            //GLOG 6508 - Added parameter to specify Index
            StringBuilder oSB = new StringBuilder();
            string xText = "";

            //Extract just XML portion of string
            int iXMLStart = xXMLSource.IndexOf("<");
            int iXMLEnd = xXMLSource.LastIndexOf(">") + 1;
            //Input is not XML
            if (iXMLStart == -1 || iXMLEnd == 0)
                //return input text
                return xXMLSource;
            else
                xXMLSource = xXMLSource.Substring(iXMLStart, iXMLEnd - iXMLStart);

            //load specified XMLSource to XmlDocument
            System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
            oXML.LoadXml("<zzmpD>" + xXMLSource + "</zzmpD>");

            //Get list of nodes for designated tag name
            //GLOG 8003: If single index is specified, include only the node with matching Index attribute
            System.Xml.XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes(
                (oSB.AppendFormat("(/zzmpD/{0}|/zzmpD/{1}){2}", xTagName, xTagName.ToUpper(),
                ((iIndex > 0) && xXMLSource.Contains(" Index=")) ? "[@Index=\"" + iIndex.ToString() + "\"]" : "")).ToString());

            oSB = new StringBuilder();
            //GLOG 6508
            int iStartIndex = 0;
            int iEndIndex = oNodeList.Count - 1;
            if (iIndex > 0)
            {
                //GLOG 8003: If Index specified and XML has Index attribute, NodeList will have at most one item
                if (xXMLSource.Contains(" Index="))
                {
                    iStartIndex = 0;
                    iEndIndex = 0;
                }
                else
                {
                    //Convert for zero-based array
                    iStartIndex = iIndex - 1;
                    iEndIndex = iIndex - 1;
                }
            }
            //foreach (System.Xml.XmlNode oNode in oNodeList)
            for (int i = iStartIndex; i <= iEndIndex && i <= oNodeList.Count - 1; i++)
            {
                //oNode inner text will suffice for parent node
                //these will be attribute values for child
                foreach (System.Xml.XmlNode oChildNode in oNodeList[i].ChildNodes)
                {
                    switch (oChildNode.NodeType)
                    {
                        case XmlNodeType.Text:
                            // Node is the Text value of base node
                            xText = oChildNode.Value;
                            if (xText != "")
                                oSB.AppendFormat("{0}; ", String.RestoreXMLChars(xText));
                            break;
                        case XmlNodeType.Element:
                            // Node is a child element of base node
                            xText = oChildNode.InnerText;
                            oSB.AppendFormat("{0}; ", String.RestoreXMLChars(xText));
                            break;
                    }
                }
            }

            //trim trailing delimiters
            string xTrim = oSB.ToString().TrimEnd();
            xTrim = xTrim.TrimEnd(';');
            return xTrim.TrimEnd(',');
        }
        public static string GetFormattedXml(string xXMLSource, string xTemplate, string xSep, string xLastSep, int iIndex, bool bSkipEmptyValues)
        {
            //GLOG 15838
            string xFormattedText = "";
            int iStart = 0;
            int iEnd = 0;
            List<string> aTags = new List<string>();
            //Parse field names from Template
            int iPos = xTemplate.IndexOf("<");
            while (iPos > -1)
            {
                    int iPos2 = xTemplate.IndexOf(">", iPos);
                    if (iPos2 > -1)
                    {
                        string xTag = xTemplate.Substring(iPos + 1, iPos2 - (iPos + 1));
                        aTags.Add(xTag);
                        iPos = xTemplate.IndexOf("<", iPos2);
                    }
                    else
                        iPos = -1;
            }
            if (iIndex > 0)
            {
                iStart = iIndex;
                iEnd = iIndex;
            }
            else
            {
                iStart = 1;
                iEnd = GetXMLEntityCount(xXMLSource, null);

            }
            List<string> aItems = new List<string>();
            for (int i = iStart; i <= iEnd; i++)
            {
                string xItem = xTemplate;
                for (int t = 0; t < aTags.Count; t++)
                {
                    string xTag = aTags[t];
                    string xText = GetXMLExtract(xXMLSource, xTag, i);
                    if (!string.IsNullOrEmpty(xText) && bSkipEmptyValues)
                    {
                        //If any field is empty, omit entire item
                        xItem = "";
                        continue;
                    }
                    else
                    {
                        xItem = xItem.Replace("<" + xTag + ">", xText);
                    }
                }
                if (xItem != "")
                {
                    aItems.Add(xItem);
                }
            }
            for (int i = 0; i < aItems.Count; i++)
            {
                xFormattedText += aItems[i];
                if (i == aItems.Count - 2 && !string.IsNullOrEmpty(xLastSep))
                {
                    //Use different separator for last item
                    xFormattedText += xLastSep;
                }
                else if (i < aItems.Count - 1)
                {
                    xFormattedText += xSep;
                }
            }
            return xFormattedText.Trim();
        }
    }
}
