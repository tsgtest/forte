using System;

namespace LMP.MacPac
{
	/// <summary>
	/// Summary description for Session.
	/// </summary>
	public sealed class Session
	{
		static Word.Application m_oWordApp = null;
		static bool m_bAdminMode = false;
		static bool m_bInitialized = false;

		//prevent instance creation
		private Session(){}

		public static bool IsInitialized
		{
			get{return m_bInitialized;}
		}
		public static void Initialize(int iCulture, bool bAdminMode)
		{
			LMP.Resources.SetCulture(iCulture);
			Session.AdminMode = bAdminMode;
		}

		public static void Initialize(bool AdminMode)
		{
			Initialize(1033,AdminMode);
		}

		public static Word.Application CurrentWordApp
		{
			get{return m_oWordApp;}

			set
			{
				m_oWordApp = value;
				LMP.Architect.Application.CurrentWordApp = m_oWordApp;
			}
		}

		public static LMP.Data.User CurrentUser
		{
			get{return LMP.Data.Application.User;}
		}
		public static bool AdminMode
		{
			get{return m_bAdminMode;}
			set{m_bAdminMode = value;}
		}
	}
}
