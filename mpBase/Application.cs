using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;

namespace LMP
{
    public static class Utility
    {
        public static bool InDevelopmentMode
        {
            //GLOG 8220
            get { return InDevelopmentModeState(1); }
        }

        //GLOG 8220
        public static bool InDevelopmentModeState(int i)
        {
            //Uses bitfield values
            //1=General
            //2=Oxml Development
            //4=Forte Local
            //8=Use XML Config file for fDMS
            string xVal = Registry.GetMacPac10Value("DevelopmentMode");
            int iVal = 0;
            if (Int32.TryParse(xVal, out iVal))
            {
                return (iVal & i) == i;
            }
            else
                return false;
        }
    }


    public static class ComponentProperties
    {
        public static string ProductName;
        public static string Copyright;
        public static string CompanyName;
        public static string Version;
        static ComponentProperties()
        {
            Assembly oAsm = Assembly.GetExecutingAssembly();

            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                ProductName = "Forte Tools";
            }
            else
            {
                ProductName = "Forte";
            }

            CompanyName = ((AssemblyCompanyAttribute) Attribute.GetCustomAttribute(
                oAsm, typeof(AssemblyCompanyAttribute))).Company;

            Copyright = ((AssemblyCopyrightAttribute) Attribute.GetCustomAttribute(
                oAsm, typeof(AssemblyCopyrightAttribute))).Copyright;
            
            //Version = Attribute.GetCustomAttribute(
            //    oAsm, typeof(AssemblyVersionAttribute)).ToString();
        }
    }
}