using System;

namespace LMP
{
	public enum mpTriState
	{
		Undefined = -777777,
		False = 0,
		True = 1,
	}

	/// <summary>
	/// Contains low level number functions
	/// </summary>
	public static class Number
	{
		/// <summary>
		/// returns the ordinal suffix of an integer
		/// </summary>
		/// <param name="iNum"></param>
		/// <returns></returns>
		public static string GetOrdinalSuffix(int iNum)
		{
			//TODO: Add support for languages other than English
			Trace.WriteNameValuePairs("iNum", iNum);
			string xRet;
			string xLang = System.Threading.Thread.CurrentThread
				.CurrentCulture.TwoLetterISOLanguageName;
			switch (xLang)
			{
				case "en":
					xRet = GetOrdinalSuffixEnglish(iNum);
					break;
				default:
					xRet = GetOrdinalSuffixEnglish(iNum);
					break;
			}
			return xRet;
		}

		private static string GetOrdinalSuffixEnglish(int iNum)
		{
			Trace.WriteNameValuePairs("iNum", iNum);
			string xNum = iNum.ToString();
			if (((iNum>=4) && (iNum<=20)) || ((iNum<=-4) && (iNum>=-20)))
				return "th";
			else if (xNum.EndsWith("1"))
				return "st";
			else if (xNum.EndsWith("2"))
				return "nd";
			else if (xNum.EndsWith("3"))
				return "rd";
			else
				return "th";
		}

		/// <summary>
		/// returns the ordinal of an integer
		/// </summary>
		/// <param name="iNum"></param>
		/// <returns></returns>
		public static string GetOrdinal(int iNum)
		{
			//TODO: Add support for languages other than English
			Trace.WriteNameValuePairs("iNum", iNum);
			string xRet;
			string xLang = System.Threading.Thread.CurrentThread
				.CurrentCulture.TwoLetterISOLanguageName;
			switch (xLang)
			{
				case "en":
					xRet = System.String.Concat(iNum.ToString(),
						GetOrdinalSuffixEnglish(iNum));
					break;
				default:
					xRet = System.String.Concat(iNum.ToString(),
						GetOrdinalSuffixEnglish(iNum));
					break;
			}
			return xRet;
		}

		/// <summary>
		/// substitutes the ordinal suffix for the ordinal
		/// variable %o% in a date format string
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="xFormat"></param>
		/// <returns></returns>
		public static string GetOrdinalFormat(DateTime dt, string xFormat)
		{
			Trace.WriteNameValuePairs("dt", dt, "xFormat", xFormat);
			string xSuffix =GetOrdinalSuffix(dt.Day);
			return xFormat.Replace("%o%", xSuffix);
		}
        /// <summary>
        /// returns true if input number is odd
        /// </summary>
        /// <param name="iInput"></param>
        /// <returns></returns>
        public static bool IsOdd(int iInput)
        {
            return (iInput % 2 == 1);
        }

		/// <summary>
		/// fills supplied array with an integer or ordinal sequence,
		/// starting with a blank line if specified
		/// </summary>
		/// <param name="aSequence"></param>
		/// <param name="iFirst"></param>
		/// <param name="iLast"></param>
		/// <param name="bIncludeBlankLine"></param>
		/// <param name="iLength"></param>
		/// <param name="bAsOrdinal"></param>
		private static void GetSequence(out string[] aSequence, int iFirst,int iLast,
			bool bIncludeBlankLine, int iLength, bool bAsOrdinal)
		{
			Trace.WriteNameValuePairs("iFirst", iFirst, "iLast", iLast,
				"bIncludeBlankLine", bIncludeBlankLine, "iLength", iLength,
				"bAsOrdinal", bAsOrdinal);

			int iCount=0;
			string xNum;

			//calculate length and declare array
			int iElements = Math.Abs(iLast - iFirst) +
				System.Convert.ToInt32(bIncludeBlankLine) + 1;
			aSequence = new string[iElements];

			//add blank line if specified
			if (bIncludeBlankLine)
				aSequence[0] = new string('_', iLength);

			//fill rest of array
			if (iLast >= iFirst)
			{
				//increment from first to last
				for (int i = iFirst; i < iLast + 1; i++)
				{
					if (bAsOrdinal)
						xNum = GetOrdinal(i);
					else
						xNum = i.ToString();

					if (bIncludeBlankLine)
						aSequence[iCount + 1] = xNum;
					else
						aSequence[iCount] = xNum;

					iCount ++;
				}
			}
			else
			{
				//decrement from first to last
				for (int i = iFirst; i > iLast - 1; i--)
				{
					if (bAsOrdinal)
						xNum = GetOrdinal(i);
					else
						xNum = i.ToString();

					if (bIncludeBlankLine)
						aSequence[iCount + 1] = xNum;
					else
						aSequence[iCount] = xNum;

					iCount ++;
				}
			}
		}

		/// <summary>
		/// fills supplied array with a string integer sequence
		/// </summary>
		/// <param name="aSequence"></param>
		/// <param name="iFirst"></param>
		/// <param name="iLast"></param>
		public static void GetIntegerSequence(out string[] aSequence, int iFirst,int iLast)
		{
			GetSequence(out aSequence, iFirst, iLast, false, 0, false);
		}

		/// <summary>
		/// fills supplied array with a string integer sequence -
		/// optionally includes blank line of specified length as
		/// first element
		/// </summary>
		/// <param name="aSequence"></param>
		/// <param name="iFirst"></param>
		/// <param name="iLast"></param>
		/// <param name="bIncludeBlankLine"></param>
		/// <param name="iLength"></param>
		public static void GetIntegerSequence(out string[] aSequence, int iFirst,int iLast,
			bool bIncludeBlankLine, int iLength)
		{
			GetSequence(out aSequence, iFirst, iLast, bIncludeBlankLine, iLength, false);
		}

		/// <summary>
		/// fills supplied array with a string ordinal sequence
		/// </summary>
		/// <param name="aSequence"></param>
		/// <param name="iFirst"></param>
		/// <param name="iLast"></param>
		public static void GetOrdinalSequence(out string[] aSequence, int iFirst,int iLast)
		{
			GetSequence(out aSequence, iFirst, iLast, false, 0, true);
		}

		/// <summary>
		/// fills supplied array with a string ordinal sequence -
		/// optionally includes blank line of specified length as
		/// first element
		/// </summary>
		/// <param name="aSequence"></param>
		/// <param name="iFirst"></param>
		/// <param name="iLast"></param>
		/// <param name="bIncludeBlankLine"></param>
		/// <param name="iLength"></param>
		public static void GetOrdinalSequence(out string[] aSequence, int iFirst,int iLast,
			bool bIncludeBlankLine, int iLength)
		{
			GetSequence(out aSequence, iFirst, iLast, bIncludeBlankLine, iLength, true);
		}
	}
}
