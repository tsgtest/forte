using System;
using Diag = System.Diagnostics;
using System.Reflection;
using System.Text;

namespace LMP
{
    public enum TraceMessageTypes
    {
        Warning = 1,
        ERROR = 2,
        Info = 3
    }

    /// <summary>
    /// contains the methods that write to a trace file,
    /// which is located in the same directory as the executing code -
    /// the LMPTraceSwitch needs to be set in machine.config
    /// to an integer between 0 and 4,
    /// as defined by the TraceLevel enum:
    /// 	<system.diagnostics>
    ///			<switches>
    ///				<add name="LMPTraceSwitch" value="0"/>
    ///			</switches>
    ///			<trace autoflush="false" indentsize="4"/>
    ///		</system.diagnostics>

    /// </summary>
    public sealed class Trace : System.IDisposable
    {
        private static Diag.TraceSwitch m_oAppSwitch;
        private static Diag.TraceListener m_oListener;

        static Trace()
        {
            //GLOG 15949: Write deubg.log to WritableDataDirectory if defined
            //get path from registry
            string xPath = Registry.GetLocalMachineValue(
                LMP.Registry.MacPac10RegistryRoot, "WritableDataDirectory");

            if (System.String.IsNullOrEmpty(xPath))
            {
                //JTS 3/28/13: Write Debug.log to DataDirectory if defined
                //get path from registry
                xPath = Registry.GetLocalMachineValue(
                    LMP.Registry.MacPac10RegistryRoot, "DataDirectory");
            }

            if (System.String.IsNullOrEmpty(xPath))
            {
                //no registry path was specified - get from root directory
                xPath = Registry.GetLocalMachineValue(
                    LMP.Registry.MacPac10RegistryRoot, "RootDirectory");
                if (!System.String.IsNullOrEmpty(xPath))
                    xPath = xPath + @"\data";
            }
            if (!System.String.IsNullOrEmpty(xPath))
                xPath = LMP.OS.EvaluateEnvironmentVariables(xPath);

            if (System.String.IsNullOrEmpty(xPath) || !System.IO.Directory.Exists(xPath))
            {
                //JTS 3/28/13: If Data directory not found, use MacPac\bin
                System.Reflection.Assembly oAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                string xFullName = oAssembly.CodeBase.Substring(8);
                int iPos = xFullName.LastIndexOf(@"/");
                xPath = xFullName.Substring(0, iPos);
            }
            //set full name of debug.log
            string xDebugLog = xPath  + "\\debug_" + Environment.UserName + ".log"; //JTS 3/28/13: Include UserName in filename

            //get the trace switch for LMP - this is used to 
            //determine what trace data will be output
            m_oAppSwitch = new Diag.TraceSwitch(
                "LMPTraceSwitch", "Switch for entire application");

            //add a listener that will write trace output to the debug.log
            m_oListener = new Diag.TextWriterTraceListener(xDebugLog);
            Diag.Trace.Listeners.Add(m_oListener);
        }

        /// <summary>
        /// returns the current trace level for LMP
        /// </summary>
        public static Diag.TraceLevel Level
        {
            get
            {
                return m_oAppSwitch.Level;
            }
        }

        /// <summary>
        /// writes to trace if not off
        /// </summary>
        /// <param name="xMsg"></param>
        /// <param name="oMsgType"></param>
        private static void Write(string xMsg, LMP.TraceMessageTypes oMsgType)
        {
            if (m_oAppSwitch.Level != Diag.TraceLevel.Off)
            {
                //remove passwords from text
                xMsg = xMsg.Replace("fish4mill", "");
                xMsg = xMsg.Replace("fish4wade", "");
                xMsg = xMsg.Replace("Sackett@4437", "");

                //Get StackFrame of function that called
                //the trace - always has index of 2
                Diag.StackTrace oST = new System.Diagnostics.StackTrace();
                Diag.StackFrame oStackFrame = oST.GetFrame(2);

                //get StackFrame method
                MethodBase oTraceCaller = oStackFrame.GetMethod();

                //get method detail
                StringBuilder oSB = new StringBuilder();
                oSB.AppendFormat("{0}\t{1}\r\n\t{2}.{3}\r\n\t{4}\r\n{5}",
                    System.DateTime.Now,
                    oMsgType.ToString(),
                    oTraceCaller.ReflectedType.FullName,
                    oTraceCaller.Name,
                    xMsg,
                    new string('_', 100));

                string xText = oSB.ToString();

                //blank out passwords
                xText = xText.Replace("Fish4Mill", "********");
                xText = xText.Replace("Fish4Wade", "********");

                Diag.Trace.WriteLine(xText);
                Diag.Trace.Flush();
            }
        }

        /// <summary>
        /// writes info message to trace if level is info or verbose
        /// </summary>
        /// <param name="xMsg"></param>
        public static void WriteInfo(string xMsg)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceInfo)
            {
                Write(xMsg, LMP.TraceMessageTypes.Info);
            }
        }

        /// <summary>
        /// writes parameter values to trace if level is info or verbose
        /// </summary>
        /// <param name="NameValuePairs">array must be of the form
        /// VarName1,VarValue1,VarName2,VarValue2,VarName3,VarValue3, etc...
        /// </param>
        public static void WriteNameValuePairs(params object[] oNameValuePairs)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceInfo)
            {
                StringBuilder oSB = new StringBuilder();

                for (int i = 0; i < oNameValuePairs.Length; i += 2)
                {
                    try
                    {
                        //first of two is the name
                        string xName = oNameValuePairs[i].ToString();

                        //convert object value to string
                        string xValue = oNameValuePairs[i + 1].ToString();

                        //append name/value pair to string
                        oSB.Append(string.Concat(xName, "=", xValue));

                        //append separator
                        if (i < oNameValuePairs.Length - 2)
                            oSB.Append("; ");
                    }

                    //ignore if name/value pair can't be added to string - 
                    //we're tracing because we're examining a different error
                    catch { }
                }

                Write(oSB.ToString(), LMP.TraceMessageTypes.Info);
            }
        }

        /// <summary>
        /// writes values to trace if level is info or verbose
        /// </summary>
        /// <param name="Values">list of Values to write</param>
        public static void WriteValues(params object[] oValues)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceInfo)
            {
                if (oValues == null)
                    return;

                StringBuilder oSB = new StringBuilder();
                string Value;

                foreach (object o in oValues)
                {
                    try
                    {
                        Value = o.ToString();

                        //append value to string
                        oSB.AppendFormat("{0};", Value);
                    }

                        //ignore if name/value pair can't be added to string - 
                    //we're tracing because we're examining a different error
                    catch { }
                }

                Write(oSB.ToString(), LMP.TraceMessageTypes.Info);
            }
        }

        /// <summary>
        /// writes parameter values to trace if level is info or verbose -
        /// begins with the word "Parameters: "
        /// </summary>
        /// <param name="Values">list of Values to write</param>
        public static void WriteParameters(params object[] oParameters)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceInfo)
            {
                if (oParameters == null)
                    return;

                StringBuilder oSB = new StringBuilder();

                //prepend the word parameters to the parameter array
                oSB.Append("Parameters: ");

                string Value;

                foreach (object o in oParameters)
                {
                    try
                    {
                        Value = o.ToString();

                        //append value to string
                        oSB.AppendFormat("{0};", Value);
                    }

                        //ignore if name/value pair can't be added to string - 
                    //we're tracing because we're examining a different error
                    catch { }
                }

                Write(oSB.ToString(), LMP.TraceMessageTypes.Info);
            }
        }

        /// <summary>
        /// writes warning message to tract if level is warning, info or verbose
        /// </summary>
        /// <param name="xMsg"></param>
        public static void WriteWarning(string xMsg)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceWarning)
            {
                Write(xMsg, LMP.TraceMessageTypes.Warning);
            }
        }

        /// <summary>
        /// writes error message to trace if trace is not off
        /// </summary>
        /// <param name="xMsg"></param>
        public static void WriteError(string xMsg)
        {
            //trace only if trace level is Info or Verbose
            if (m_oAppSwitch.TraceError)
            {
                Write(xMsg, LMP.TraceMessageTypes.ERROR);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            m_oListener.Dispose();
        }

        #endregion
    }
}
