using System;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Interop = System.Runtime.InteropServices;
using LMP.Data;

namespace LMP
{
	/// <summary>
	/// defines an LMP exception - derived from System.Exception
	/// </summary>
	namespace Exceptions
	{
		public class LMPException : Exception
		{
			/// <summary>
			/// constructor for the LMPException class -
			/// derived from System.Exception
			/// </summary>
			/// <param name="xMsg"></param>
			/// <param name="oInnerException"></param>
			public LMPException(
				string xMsg, Exception oInnerException):base(xMsg, oInnerException){}

			/// <summary>
			/// constructor for the LMPException class -
			/// derived from System.Exception
			/// </summary>
			/// <param name="xMsg"></param>
			public LMPException(string xMsg):base(xMsg, null){}

			/// <summary>
			/// shows this LMPException, with the specified into message
			/// </summary>
			/// <param name="xIntroMsg"></param>
			public void Show(string xIntroMsg)
			{
				LMP.Error.Show(this,xIntroMsg);
			}

			/// <summary>
			/// shows this LMPException
			/// </summary>
			public void Show()
			{
				LMP.Error.Show(this);
			}
		}

        public class InvalidUserException : LMP.Exceptions.LMPException
        {
            public InvalidUserException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public InvalidUserException(string xMsg) : base(xMsg, null) { }
        }

        public class SyncRequirementsException : LMP.Exceptions.LMPException
        {
            public SyncRequirementsException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public SyncRequirementsException(string xMsg) : base(xMsg, null) { }
        }

        public class SyncDBException : LMP.Exceptions.LMPException
        {
            public SyncDBException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public SyncDBException(string xMsg) : base(xMsg, null) { }
        }

        public class GroupsException : LMP.Exceptions.LMPException
        {
            public GroupsException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public GroupsException(string xMsg) : base(xMsg, null) { }
        }

        public class ControlActionException : LMP.Exceptions.LMPException
        {
            public ControlActionException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public ControlActionException(string xMsg) : base(xMsg, null) { }
        }

        public class WordIntegrationException : LMP.Exceptions.LMPException
        {
            public WordIntegrationException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public WordIntegrationException(string xMsg) : base(xMsg, null) { }
        }

        public class LicenseException : LMP.Exceptions.LMPException
        {
            public LicenseException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public LicenseException(string xMsg) : base(xMsg, null) { }
        }

        public class PendingActionException : LMP.Exceptions.LMPException
        {
            public PendingActionException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public PendingActionException(string xMsg) : base(xMsg, null) { }
        }

        public class UIException : LMP.Exceptions.LMPException
        {
            public UIException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public UIException(string xMsg) : base(xMsg, null) { }
        }

		public class UINodeException: LMP.Exceptions.LMPException
		{
			public UINodeException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UINodeException(string xMsg):base(xMsg,null){}
		}

		public class VariableException: LMP.Exceptions.LMPException
		{
			public VariableException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public VariableException(string xMsg):base(xMsg,null){}
		}

		public class SegmentDefinitionException: LMP.Exceptions.LMPException
		{
			public SegmentDefinitionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SegmentDefinitionException(string xMsg):base(xMsg,null){}
		}

		public class ValueSourceException: LMP.Exceptions.LMPException
		{
			public ValueSourceException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ValueSourceException(string xMsg):base(xMsg,null){}
		}

		public class SessionException: LMP.Exceptions.LMPException
		{
			public SessionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SessionException(string xMsg):base(xMsg,null){}
		}

		public class SegmentException: LMP.Exceptions.LMPException
		{
			public SegmentException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SegmentException(string xMsg):base(xMsg,null){}
		}

        public class SegmentInsertionException : LMP.Exceptions.LMPException
        {
            public SegmentInsertionException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public SegmentInsertionException(string xMsg) : base(xMsg, null) { }
        }
        
        public class MacPacDocumentIDException : LMP.Exceptions.LMPException
		{
			public MacPacDocumentIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MacPacDocumentIDException(string xMsg):base(xMsg,null){}
		}

		public class WordTagException: LMP.Exceptions.LMPException
		{
			public WordTagException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordTagException(string xMsg):base(xMsg,null){}
		}

		public class UNIDException: LMP.Exceptions.LMPException
		{
			public UNIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UNIDException(string xMsg):base(xMsg,null){}
		}

		public class ControlEventException: LMP.Exceptions.LMPException
		{
			public ControlEventException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlEventException(string xMsg):base(xMsg,null){}
		}

		public class COMEventException: LMP.Exceptions.LMPException
		{
			public COMEventException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public COMEventException(string xMsg):base(xMsg,null){}
		}

		public class ControlTagException: LMP.Exceptions.LMPException
		{
			public ControlTagException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlTagException(string xMsg):base(xMsg,null){}
		}

		public class MethodNameException: LMP.Exceptions.LMPException
		{
			public MethodNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MethodNameException(string xMsg):base(xMsg,null){}
		}

		public class CompressionException: LMP.Exceptions.LMPException
		{
			public CompressionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public CompressionException(string xMsg):base(xMsg,null){}
		}

		public class NullReferenceException: LMP.Exceptions.LMPException
		{
			public NullReferenceException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public NullReferenceException(string xMsg):base(xMsg,null){}
		}

		public class NetworkException: LMP.Exceptions.LMPException
		{
			public NetworkException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public NetworkException(string xMsg):base(xMsg,null){}
		}

		public class ServerException: LMP.Exceptions.LMPException
		{
			public ServerException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ServerException(string xMsg):base(xMsg,null){}
		}

		public class ClassInitializationException: LMP.Exceptions.LMPException
		{
			public ClassInitializationException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ClassInitializationException(string xMsg):base(xMsg,null){}
		}

		public class RegistryKeyException: LMP.Exceptions.LMPException
		{
			public RegistryKeyException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public RegistryKeyException(string xMsg):base(xMsg,null){}
		}

		public class ParameterException: LMPException
		{
			public ParameterException(string xMsg, 
				System.Exception xInnerException):base(xMsg,xInnerException){}
			public ParameterException(string xMsg):base(xMsg,null){}
		}

		public class ResourceException: LMP.Exceptions.LMPException
		{
			public ResourceException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ResourceException(string xMsg):base(xMsg,null){}
		}

		public class NotImplementedException: LMP.Exceptions.LMPException
		{
			public NotImplementedException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public NotImplementedException(string xMsg):base(xMsg,null){}
		}

        public class ConnectionParametersException : LMP.Exceptions.LMPException
        {
            public ConnectionParametersException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public ConnectionParametersException(string xMsg) : base(xMsg, null) { }
        }

        public class DBConnectionException : LMP.Exceptions.LMPException
        {
            public DBConnectionException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public DBConnectionException(string xMsg) : base(xMsg, null) { }
        }

		public class FieldCodeException: LMP.Exceptions.LMPException
		{
			public FieldCodeException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public FieldCodeException(string xMsg):base(xMsg,null){}
		}

		public class StoredProcedureException: LMP.Exceptions.LMPException
		{
			public StoredProcedureException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StoredProcedureException(string xMsg):base(xMsg,null){}
		}

		public class SQLStatementException: LMP.Exceptions.LMPException
		{
			public SQLStatementException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SQLStatementException(string xMsg):base(xMsg,null){}
		}

		public class NotInCollectionException: LMP.Exceptions.LMPException
		{
			public NotInCollectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public NotInCollectionException(string xMsg):base(xMsg,null){}
		}

		public class DataException: LMP.Exceptions.LMPException
		{
			public DataException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DataException(string xMsg):base(xMsg,null){}
		}

		public class RecordUpdateException: LMP.Exceptions.LMPException
		{
			public RecordUpdateException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public RecordUpdateException(string xMsg):base(xMsg,null){}
		}

		public class ObjectIDException: LMP.Exceptions.LMPException
		{
			public ObjectIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectIDException(string xMsg):base(xMsg,null){}
		}

		public class ObjectIDRequiredException: LMP.Exceptions.LMPException
		{
			public ObjectIDRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectIDRequiredException(string xMsg):base(xMsg,null){}
		}

		public class UserNameRequiredException: LMP.Exceptions.LMPException
		{
			public UserNameRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UserNameRequiredException(string xMsg):base(xMsg,null){}
		}

		public class StringArrayElementException: LMP.Exceptions.LMPException
		{
			public StringArrayElementException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StringArrayElementException(string xMsg):base(xMsg,null){}
		}

		public class StringArrayRecordException: LMP.Exceptions.LMPException
		{
			public StringArrayRecordException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StringArrayRecordException(string xMsg):base(xMsg,null){}
		}

		public class StringArrayFieldException: LMP.Exceptions.LMPException
		{
			public StringArrayFieldException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StringArrayFieldException(string xMsg):base(xMsg,null){}
		}

		public class StringArrayTextException: LMP.Exceptions.LMPException
		{
			public StringArrayTextException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StringArrayTextException(string xMsg):base(xMsg,null){}
		}

		public class StringArraySeparatorsException: LMP.Exceptions.LMPException
		{
			public StringArraySeparatorsException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public StringArraySeparatorsException(string xMsg):base(xMsg,null){}
		}

		public class ParameterCountException: LMP.Exceptions.LMPException
		{
			public ParameterCountException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ParameterCountException(string xMsg):base(xMsg,null){}
		}

		public class AlreadyInCollectionException: LMP.Exceptions.LMPException
		{
			public AlreadyInCollectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public AlreadyInCollectionException(string xMsg):base(xMsg,null){}
		}

		public class ItemKeyException: LMP.Exceptions.LMPException
		{
			public ItemKeyException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ItemKeyException(string xMsg):base(xMsg,null){}
		}

		public class ValueException: LMP.Exceptions.LMPException
		{
			public ValueException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ValueException(string xMsg):base(xMsg,null){}
		}

		public class TableKeyException: LMP.Exceptions.LMPException
		{
			public TableKeyException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public TableKeyException(string xMsg):base(xMsg,null){}
		}

		public class OwnerRequiredException: LMP.Exceptions.LMPException
		{
			public OwnerRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public OwnerRequiredException(string xMsg):base(xMsg,null){}
		}

		public class PersonIDException: LMP.Exceptions.LMPException
		{
			public PersonIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PersonIDException(string xMsg):base(xMsg,null){}
		}

		public class UserException: LMP.Exceptions.LMPException
		{
			public UserException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UserException(string xMsg):base(xMsg,null){}
		}

		public class TypeMismatchException: LMP.Exceptions.LMPException
		{
			public TypeMismatchException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public TypeMismatchException(string xMsg):base(xMsg,null){}
		}

		public class ObjectTypeIDException: LMP.Exceptions.LMPException
		{
			public ObjectTypeIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectTypeIDException(string xMsg):base(xMsg,null){}
		}

		public class DocPropertyIDRequiredException: LMP.Exceptions.LMPException
		{
			public DocPropertyIDRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DocPropertyIDRequiredException(string xMsg):base(xMsg,null){}
		}

		public class DocPropertyIDException: LMP.Exceptions.LMPException
		{
			public DocPropertyIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DocPropertyIDException(string xMsg):base(xMsg,null){}
		}

		public class ControlIDRequiredException: LMP.Exceptions.LMPException
		{
			public ControlIDRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlIDRequiredException(string xMsg):base(xMsg,null){}
		}

		public class ControlIDException: LMP.Exceptions.LMPException
		{
			public ControlIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlIDException(string xMsg):base(xMsg,null){}
		}

		public class CultureIDException: LMP.Exceptions.LMPException
		{
			public CultureIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public CultureIDException(string xMsg):base(xMsg,null){}
		}

		public class ItemEditException: LMP.Exceptions.LMPException
		{
			public ItemEditException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ItemEditException(string xMsg):base(xMsg,null){}
		}

        public class SaveRequiredException : LMP.Exceptions.LMPException
        {
            public SaveRequiredException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public SaveRequiredException(string xMsg) : base(xMsg, null) { }
        }

        public class MailException : LMP.Exceptions.LMPException
        {
            public MailException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public MailException(string xMsg) : base(xMsg, null) { }
        }

		public class ControlTypeIDException: LMP.Exceptions.LMPException
		{
			public ControlTypeIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlTypeIDException(string xMsg):base(xMsg,null){}
		}

		public class ItemDeleteException: LMP.Exceptions.LMPException
		{
			public ItemDeleteException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ItemDeleteException(string xMsg):base(xMsg,null){}
		}

		public class ExistingAssignmentException: LMP.Exceptions.LMPException
		{
			public ExistingAssignmentException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ExistingAssignmentException(string xMsg):base(xMsg,null){}
		}

		public class MissingAssignmentException: LMP.Exceptions.LMPException
		{
			public MissingAssignmentException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MissingAssignmentException(string xMsg):base(xMsg,null){}
		}

		public class PermissionException: LMP.Exceptions.LMPException
		{
			public PermissionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PermissionException(string xMsg):base(xMsg,null){}
		}

		public class PublicPersonRequiredException: LMP.Exceptions.LMPException
		{
			public PublicPersonRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PublicPersonRequiredException(string xMsg):base(xMsg,null){}
		}

		public class MethodCallException: LMP.Exceptions.LMPException
		{
			public MethodCallException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MethodCallException(string xMsg):base(xMsg,null){}
		}

		public class ActionException: LMP.Exceptions.LMPException
		{
			public ActionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ActionException(string xMsg):base(xMsg,null){}
		}

		public class EntityIDException: LMP.Exceptions.LMPException
		{
			public EntityIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public EntityIDException(string xMsg):base(xMsg,null){}
		}

		public class KeySetException: LMP.Exceptions.LMPException
		{
			public KeySetException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public KeySetException(string xMsg):base(xMsg,null){}
		}

		public class DefinitionObjectException: LMP.Exceptions.LMPException
		{
			public DefinitionObjectException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DefinitionObjectException(string xMsg):base(xMsg,null){}
		}

		public class AddressIDException: LMP.Exceptions.LMPException
		{
			public AddressIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public AddressIDException(string xMsg):base(xMsg,null){}
		}

		public class FieldException: LMP.Exceptions.LMPException
		{
			public FieldException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public FieldException(string xMsg):base(xMsg,null){}
		}

		public class MissingTranslationException: LMP.Exceptions.LMPException
		{
			public MissingTranslationException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MissingTranslationException(string xMsg):base(xMsg,null){}
		}

		public class ApplicationInitializationException: LMP.Exceptions.LMPException
		{
			public ApplicationInitializationException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ApplicationInitializationException(string xMsg):base(xMsg,null){}
		}

		public class ApplicationNotRunningException: LMP.Exceptions.LMPException
		{
			public ApplicationNotRunningException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ApplicationNotRunningException(string xMsg):base(xMsg,null){}
		}

		public class WordStyleException: LMP.Exceptions.LMPException
		{
			public WordStyleException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordStyleException(string xMsg):base(xMsg,null){}
		}

		public class WordBookmarkNameException: LMP.Exceptions.LMPException
		{
			public WordBookmarkNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordBookmarkNameException(string xMsg):base(xMsg,null){}
		}

		public class AuthorsRequiredException: LMP.Exceptions.LMPException
		{
			public AuthorsRequiredException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public AuthorsRequiredException(string xMsg):base(xMsg,null){}
		}

		public class ObjectPropertyNameException: LMP.Exceptions.LMPException
		{
			public ObjectPropertyNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectPropertyNameException(string xMsg):base(xMsg,null){}
		}

		public class ExpressionException: LMP.Exceptions.LMPException
		{
			public ExpressionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ExpressionException(string xMsg):base(xMsg,null){}
		}

		public class ArgumentException: LMP.Exceptions.LMPException
		{
			public ArgumentException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ArgumentException(string xMsg):base(xMsg,null){}
		}

		public class PropertyNameException: LMP.Exceptions.LMPException
		{
			public PropertyNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PropertyNameException(string xMsg):base(xMsg,null){}
		}

		public class ObjectParameterException: LMP.Exceptions.LMPException
		{
			public ObjectParameterException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectParameterException(string xMsg):base(xMsg,null){}
		}

		public class WordDocException: LMP.Exceptions.LMPException
		{
			public WordDocException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordDocException(string xMsg):base(xMsg,null){}
		}

		public class WordPropertyReferenceException: LMP.Exceptions.LMPException
		{
			public WordPropertyReferenceException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordPropertyReferenceException(string xMsg):base(xMsg,null){}
		}

		public class WordObjectReferenceException: LMP.Exceptions.LMPException
		{
			public WordObjectReferenceException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordObjectReferenceException(string xMsg):base(xMsg,null){}
		}

		public class CallByNameException: LMP.Exceptions.LMPException
		{
			public CallByNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public CallByNameException(string xMsg):base(xMsg,null){}
		}

		public class ErrorDetailException: LMP.Exceptions.LMPException
		{
			public ErrorDetailException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ErrorDetailException(string xMsg):base(xMsg,null){}
		}

		public class EXECodeException: LMP.Exceptions.LMPException
		{
			public EXECodeException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public EXECodeException(string xMsg):base(xMsg,null){}
		}

		public class FileException: LMP.Exceptions.LMPException
		{
			public FileException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public FileException(string xMsg):base(xMsg,null){}
		}

		public class FileAccessException: LMP.Exceptions.LMPException
		{
			public FileAccessException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public FileAccessException(string xMsg):base(xMsg,null){}
		}

		public class MacroException: LMP.Exceptions.LMPException
		{
			public MacroException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MacroException(string xMsg):base(xMsg,null){}
		}

		public class DirectoryException: LMP.Exceptions.LMPException
		{
			public DirectoryException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DirectoryException(string xMsg):base(xMsg,null){}
		}

		public class BookmarkException: LMP.Exceptions.LMPException
		{
			public BookmarkException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public BookmarkException(string xMsg):base(xMsg,null){}
		}

		public class COMObjectCreationException: LMP.Exceptions.LMPException
		{
			public COMObjectCreationException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public COMObjectCreationException(string xMsg):base(xMsg,null){}
		}

		public class MethodExecutionException: LMP.Exceptions.LMPException
		{
			public MethodExecutionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MethodExecutionException(string xMsg):base(xMsg,null){}
		}

		public class DocumentSectionException: LMP.Exceptions.LMPException
		{
			public DocumentSectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DocumentSectionException(string xMsg):base(xMsg,null){}
		}

		public class PropertyCountMismatchException: LMP.Exceptions.LMPException
		{
			public PropertyCountMismatchException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PropertyCountMismatchException(string xMsg):base(xMsg,null){}
		}

		public class ObjectPropertiesException: LMP.Exceptions.LMPException
		{
			public ObjectPropertiesException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ObjectPropertiesException(string xMsg):base(xMsg,null){}
		}

		public class LimitExceededException: LMP.Exceptions.LMPException
		{
			public LimitExceededException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public LimitExceededException(string xMsg):base(xMsg,null){}
		}

		public class TypeIDException: LMP.Exceptions.LMPException
		{
			public TypeIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public TypeIDException(string xMsg):base(xMsg,null){}
		}

		public class ControlValueException: LMP.Exceptions.LMPException
		{
			public ControlValueException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlValueException(string xMsg):base(xMsg,null){}
		}

		public class MissingParentException: LMP.Exceptions.LMPException
		{
			public MissingParentException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public MissingParentException(string xMsg):base(xMsg,null){}
		}

		public class ControlCaptionException: LMP.Exceptions.LMPException
		{
			public ControlCaptionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ControlCaptionException(string xMsg):base(xMsg,null){}
		}

		public class UnsupportedPropertyException: LMP.Exceptions.LMPException
		{
			public UnsupportedPropertyException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UnsupportedPropertyException(string xMsg):base(xMsg,null){}
		}

		public class UnsupportedActionException: LMP.Exceptions.LMPException
		{
			public UnsupportedActionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public UnsupportedActionException(string xMsg):base(xMsg,null){}
		}

		public class ListException: LMP.Exceptions.LMPException
		{
			public ListException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public ListException(string xMsg):base(xMsg,null){}
		}

		public class PasswordException: LMP.Exceptions.LMPException
		{
			public PasswordException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PasswordException(string xMsg):base(xMsg,null){}
		}

		public class CaptionsCollectionException: LMP.Exceptions.LMPException
		{
			public CaptionsCollectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public CaptionsCollectionException(string xMsg):base(xMsg,null){}
		}

		public class CounselsCollectionException: LMP.Exceptions.LMPException
		{
			public CounselsCollectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public CounselsCollectionException(string xMsg):base(xMsg,null){}
		}

		public class SignaturesCollectionException: LMP.Exceptions.LMPException
		{
			public SignaturesCollectionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SignaturesCollectionException(string xMsg):base(xMsg,null){}
		}

		public class XMLException: LMP.Exceptions.LMPException
		{
			public XMLException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public XMLException(string xMsg):base(xMsg,null){}
		}

		public class XMLNodeException: LMP.Exceptions.LMPException
		{
			public XMLNodeException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public XMLNodeException(string xMsg):base(xMsg,null){}
		}

		public class COMProgIDException: LMP.Exceptions.LMPException
		{
			public COMProgIDException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public COMProgIDException(string xMsg):base(xMsg,null){}
		}

		public class AuthorIndexException: LMP.Exceptions.LMPException
		{
			public AuthorIndexException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public AuthorIndexException(string xMsg):base(xMsg,null){}
		}

		public class SubObjectException: LMP.Exceptions.LMPException
		{
			public SubObjectException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public SubObjectException(string xMsg):base(xMsg,null){}
		}

		public class DocPropertyNameException: LMP.Exceptions.LMPException
		{
			public DocPropertyNameException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public DocPropertyNameException(string xMsg):base(xMsg,null){}
		}

		public class KeywordException: LMP.Exceptions.LMPException
		{
			public KeywordException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public KeywordException(string xMsg):base(xMsg,null){}
		}

		public class PropertyException: LMP.Exceptions.LMPException
		{
			public PropertyException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public PropertyException(string xMsg):base(xMsg,null){}
		}

		public class TablePositionException: LMP.Exceptions.LMPException
		{
			public TablePositionException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public TablePositionException(string xMsg):base(xMsg,null){}
		}

		public class WordXmlNodeException: LMP.Exceptions.LMPException
		{
			public WordXmlNodeException(string xMsg, 
				Exception xInnerException):base(xMsg,xInnerException){}
			public WordXmlNodeException(string xMsg):base(xMsg,null){}
		}
        public class CIException : LMP.Exceptions.LMPException
        {
            public CIException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public CIException(string xMsg) : base(xMsg, null) { }
        }
        public class SegmentInsertionCancelledException : LMP.Exceptions.LMPException
        {
            public SegmentInsertionCancelledException(string xMsg,
                Exception xInnerException)
                : base(xMsg, xInnerException) { }
            public SegmentInsertionCancelledException(string xMsg) : base(xMsg, null) { }
        }
	}

	/// <summary>
	/// manages exceptions and debugging
	/// </summary>
	public static class Error
	{
        public static MailServerParameters MailServerParameters;
        public static string CurrentUser;
        public static string AdminEMailAddress;

        public static bool SendErrorsToAdmin
        {
            get { return !string.IsNullOrEmpty(MailServerParameters.Host) &&
                !string.IsNullOrEmpty(MailServerParameters.UserID); }
        }

		/// <summary>
		/// shows the specified exception - substitutes the
		/// specified intro message for the default message
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xIntroMsg"></param>
		public static void Show(Exception e, string xIntroMsg)
		{		
			string xErrNameLbl=null;
			string xErrDescLbl=null;
			string xErrSourceLbl=null;
			string xErrInternalDescLbl=null;

			//get the message - we do this so that we can modify
			//the message while keeping the rest of the exception's
			//data intact - see the modifications below
			string xMsg = e.Message;
			
			//COM Exceptions (coming from mpWord) contain the name of the
			//resource string to display in the message - get message
			if(e.GetType().Name == "COMException" && xMsg != null)
				xMsg = EvaluateMessageTag(xMsg);

			try
			{
				//get localized strings
				xErrNameLbl=LMP.Resources.GetLangString("Msg_LMP.Error.Show_Name");
				xErrDescLbl=LMP.Resources.GetLangString("Msg_LMP.Error.Show_Description");
				xErrSourceLbl=LMP.Resources.GetLangString("Msg_LMP.Error.Show_Source");
				xErrInternalDescLbl=LMP.Resources.GetLangString(
					"Msg_LMP.Error.Show_InternalDescription");
			}
			catch
			{
				//could not get localized strings - show error in English only dialog
				ShowInEnglish(e,xMsg);
			}

			try
			{
                string xPromptForFurtherAction = null;

                if (Error.SendErrorsToAdmin)
                {
                    xPromptForFurtherAction = LMP.Resources.GetLangString(
                        "Msg_LMP.Error.Show_PromptToSendEmailToAdmin");
                }
                else
                {
                    xPromptForFurtherAction = LMP.Resources.GetLangString(
                        "Msg_LMP.Error.Show_PromptToViewMoreDetail");
                }

                //use default intro message if none provided
				if(xIntroMsg==null)
					xIntroMsg = LMP.Resources.GetLangString("Msg_LMP.Error.Show_DefaultIntro");
				
				//build error message
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}\n\n{1}  {2}\n{3}  {4}\n\n{5}",
					xIntroMsg,xErrNameLbl,e.GetType().Name,
					xErrDescLbl,xMsg,xPromptForFurtherAction);

				//display error detail to user - prompt to copy to clipboard
                //string xProductName = LMP.ComponentProperties.ProductName;
                string xProductName = LMP.ComponentProperties.ProductName;
                string xErrorDetail = GetErrorDetail(e);

				try
				{
					LMP.Trace.WriteError(xErrorDetail);
				}

				catch{}

                if (LMP.MacPac.MacPacImplementation.IsServer)
                {
                    if (Error.SendErrorsToAdmin)
                    {
                        LMP.OS.SendMessage(Error.CurrentUser, LMP.ComponentProperties.ProductName +
                            " Error on " + Environment.MachineName, xErrorDetail,
                            Error.MailServerParameters, null, Error.AdminEMailAddress);
                    }

                    #if DEBUG
                        DialogResult oChoice = MessageBox.Show(oSB.ToString(), xProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    #endif


                    WriteErrorToEventLog(xErrorDetail);
                }
                else
                {
                    //not in server mode - display error message
                    DialogResult oChoice = MessageBox.Show(oSB.ToString(), xProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (oChoice == DialogResult.Yes)
                    {
                        //copy detail to clipboard
                        Clipboard.SetDataObject(xErrorDetail, true);

                        if (Error.SendErrorsToAdmin)
                        {
                            LMP.OS.SendMessage(Error.CurrentUser, LMP.ComponentProperties.ProductName +
                                " Error on " + Environment.MachineName, xErrorDetail,
                                Error.MailServerParameters, null, Error.AdminEMailAddress);
                        }
                        else
                        {
                            //GLOG : 2762 : CEH 
                            //GLOG : 3106 : CEH 
                            //launch program associated with .txt files and paste error automatically

                            //Get the temp directory.
                            string xFile = System.IO.Path.GetTempPath() + "\\Forte Error.txt"; //GLOG 7920 (dm)

                            //create new text file in user documents location
                            StreamWriter oSW = new StreamWriter(xFile);
                            oSW.Write(xErrorDetail);
                            oSW.Close();

                            //launch new text file
                            Process oProcess = new Process();
                            oProcess.StartInfo.FileName = xFile;
                            oProcess.Start();

                            oProcess.WaitForInputIdle();
                        }
                    }
                }
			}
			catch (System.Exception e2)
			{
				//get new intro message
				xIntroMsg = Resources.GetLangString("Error_IntroMsgForErrorInShowError");

				//get error detail in string
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}\n\n{1}:  {2}\n{3}:  {4}",
					xIntroMsg,xErrNameLbl,e2.GetType().ToString(),xErrDescLbl,e2.Message);

				MessageBox.Show(oSB.ToString(),null,MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

        /// <summary>
        /// writes the specified error to the application log
        /// </summary>
        /// <param name="oE"></param>
        private static void WriteErrorToEventLog(string xErrorDetail)
        {
            try
            {
                EventLogPermission eventLogPermission = new EventLogPermission(EventLogPermissionAccess.Administer, ".");
                eventLogPermission.PermitOnly();
                //using arbitrary value of 1000 for Event ID
                //With mscoree.dll as the EventMessageFile, this ensures that only
                //the specified text is logged, without "Description for Event ID ... cannot be found"
                EventLog.WriteEntry(LMP.ComponentProperties.ProductName + " Server", xErrorDetail, EventLogEntryType.Error, 1000);
            }
            catch (System.Exception oLogOE)
            {
                if (oLogOE.Message == "The event log is full")
                { }
                else if (!LMP.MacPac.MacPacImplementation.IsServer)
                {
                    throw oLogOE;
                }
            }
        }

		/// <summary>
		/// build error detail- include detail from every inner exception
		/// </summary>
		/// <param name="e"></param>
		/// <returns></returns>
		public static string GetErrorDetail(System.Exception e)
		{
			StringBuilder oSB = new StringBuilder();
			System.Exception oCurExc = e;
					
			//loop through nested inner exceptions
			while (oCurExc != null)
			{
				if (oSB.Length > 0)
					//there's already detail - add blank line to separate
					oSB.AppendFormat("\r\n\r\n{0}\r\n", new string('_',100));

				//add detail for current exception
				oSB.AppendFormat("{0}  {1}\r\n{2}  {3}\r\n{4}{5}",
					"Name:",oCurExc.GetType().Name,"Description:",
					EvaluateMessageTag(oCurExc.Message), "Stack:",oCurExc.StackTrace);

				//get nested exception
				oCurExc = oCurExc.InnerException;
			}

            //replace all references to "MacPac" with "Forte"
            oSB = oSB.Replace("MacPac102007", "ForteAddIn");
            oSB = oSB.Replace("LMP", "TSG");
            oSB = oSB.Replace("MacPac", "Forte");

			return oSB.ToString();
		}


		/// <summary>
		/// returns the resource tag in the specified message
		/// </summary>
		/// <param name="xMsg"></param>
		/// <returns></returns>
		private static string EvaluateMessageTag(string xMsg)
		{
			string xTag = xMsg;

			//parse out tag that refers to a particular resource
			int iPos1 = xTag.IndexOf("<");
			int iPos2 = xTag.IndexOf(">");

            if (iPos1 > -1 && iPos2 > -1 && iPos2 > iPos1)
            {
				//a resource tag exists - get it
				xTag = xTag.Substring(iPos1 + 1, iPos2 - iPos1 -1);

				if (xTag !="")
				{
					//get resource from tag
					string xRes = LMP.Resources.GetLangString(xTag);
						
					if(xRes==null)
					{
						//no resource was found for tag -
						//get message to display below
						xMsg= LMP.Resources.GetLangString("Error_InvalidResourceName") + xTag;
					}
					else
					{
						//replace tag with resource - 
						//we'll display message below
						string xTagPortion = xMsg.Substring(iPos1, iPos2-iPos1+1);
						xMsg = xMsg.Replace(xTagPortion,xRes);
					}
				}
			}

			return xMsg;
		}


		/// <summary>
		/// shows the specified exception
		/// </summary>
		/// <param name="e"></param>
		public static void Show(Exception e)
		{			
			Show(e,null);
		}

		
		/// <summary>
		/// shows the specified exception in English- substitutes the
		///specified intro message for the default message - displays
		///the specified message instead of the actual error message -
		///we need this method for errors that occur in the LMP.Resource class,
		///when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xNewErrorMsg"></param>
		/// <param name="xIntroMsg"></param>
		public static void ShowInEnglish(Exception e, string xNewErrorMsg, string xIntroMsg)
		{
			StringBuilder oSB = new StringBuilder();

			if (xIntroMsg==null)
				xIntroMsg="The following unexpected error occurred:";

			if (xNewErrorMsg==null)
				xNewErrorMsg=e.Message;

			oSB.AppendFormat("{0}\n\n{1}  {2}\n{3}  {4}\n\n{5}",
				xIntroMsg,"Name:",e.GetType().Name ,
				"Description:",xNewErrorMsg,
				"Would you like to copy this information to the clipboard?");

			//display error detail to user - prompt to copy to clipboard
			string xProductName = Application.ProductName;

			try
			{
				LMP.Trace.WriteError(xNewErrorMsg);
			}

			catch{}
		
			//prompt
			DialogResult oChoice = MessageBox.Show(oSB.ToString(),xProductName, 
				MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

			if (oChoice == DialogResult.Yes)
			{
				//build error detail with stack trace
				oSB.Remove(0,oSB.Length);

				if(e.InnerException == null)
					oSB.AppendFormat("{0}\r\n{1}  {2}\r\n{3}  {4}\r\n{5}{6}\r\n{7}{8}",
						xIntroMsg,"Name:",e.GetType().Name,
						"Description:",xNewErrorMsg, 
						"Internal Description:", "NONE",
						"Source",e.StackTrace);
				else
					oSB.AppendFormat("{0}\r\n{1}  {2}\r\n{3}  {4}\r\n{5}{6}\r\n{7}{8}",
						xIntroMsg,"Name:",e.GetType().Name,
						"Description:",xNewErrorMsg, 
						"Internal Description:", e.InnerException.Message,
						"Source",e.StackTrace);

				//copy to clipboard
				Clipboard.SetDataObject(oSB.ToString(),true);
			}
		}
		

		/// <summary>
		/// shows the specified exception in English- displays
		/// the specified message instead of the actual error message -
		/// we need this method for errors that occur in the LMP.Resource class,
		/// when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xNewErrorMsg"></param>
		public static void ShowInEnglish(Exception e, string xNewErrorMsg)
		{
			ShowInEnglish(e,xNewErrorMsg,null);
		}
		

		/// <summary>
		/// shows the specified exception in English-
		/// we need this method for errors that occur in the LMP.Resource class,
		/// when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		public static void ShowInEnglish(Exception e)
		{
			ShowInEnglish(e,null,null);
		}
	}
}
