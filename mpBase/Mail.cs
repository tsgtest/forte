﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMP.Data;

namespace LMP
{
    public struct MailServerParameters
    {
        public string Host;
        public bool UseSsl;
        public string UserID;
        public string Password;
        public bool UseNetworkCredentialsToLogin;
        public int Port;
    }

    public class Mail
    {
        private MailServerParameters m_oParams;

        public Mail(MailServerParameters oParams)
        {
            this.m_oParams = oParams;
        }

        //public void SendMessage(string xFrom, string xMsg, params string[] xTo)
        //{
        //    if (m_oParams.MailHost != "" && (m_oParams.MailUseNetworkCredentialsToLogin ||
        //            (m_oParams.MailUserID != "" && m_oParams.MailPassword != "")))
        //    {
        //        throw new LMP.Exceptions.MailException("Error_MailNotSetup");
        //    }

        //    if (xTo.Length == 0 || string.IsNullOrEmpty(xTo[0]))
        //    {
        //        throw new LMP.Exceptions.MailException(
        //            LMP.Resources.GetLangString("Error_MissingMailRecipient"));
        //    }

        //    if (string.IsNullOrEmpty(xFrom))
        //    {
        //        throw new LMP.Exceptions.MailException(
        //            LMP.Resources.GetLangString("Error_MissingMailSender"));
        //    }

        //    try
        //    {
        //        LMP.OS.SendMessage(xFrom, xMsg, m_oParams, xTo);
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.MailException(
        //            LMP.Resources.GetLangString("Error_CouldNotSendMailMessage"));
        //    }
        //}
    }
}
