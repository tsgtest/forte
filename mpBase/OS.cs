using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Runtime.InteropServices;
using System.Text;

namespace LMP
{
	/// <summary>
	/// contains methods and properties relating to the operating system
	/// and general environment created by Daniel Fisherman - 10/04
	/// </summary>
	public static class OS
	{
        private static List<string> m_oFilesForDeletion = new List<string>();
		/// <summary>
		/// returns the location of the COM dll, ocx, or exe that contains
		/// the specified class
		/// </summary>
		/// <param name="xProgID"></param>
		/// <returns></returns>
		public static string GetDLLOCXPath(string xProgID)
		{
			//get guid from prog id
			string xClsid = Registry.GetClassesRootValue(System.String.Concat(xProgID, @"\Clsid"));

			//get location from guid
			string xPath = Registry.GetClassesRootValue(System.String.Concat(@"Clsid\", xClsid,
				@"\InprocServer32"));

			if (xPath==null)
				return "";

			//trim file name
			int iPos = xPath.LastIndexOf('\\');
			return xPath.Substring(0, iPos + 1);
		}

        ///// <summary>
        ///// returns the major version number of Word
        ///// </summary>
        ///// <returns></returns>
        //public static byte GetWinwordVersion()
        //{
        //    try
        //    {
        //        return (byte)LMP.Forte.MSWord.WordApp.Version;
        //    }

        //    catch
        //    {
        //        try
        //        {
        //            //Word might be closed - get from registry
        //            return GetWinwordVersionFromRegistry();
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.FileException(
        //                LMP.Resources.GetLangString("Error_CouldNotRetrieveVersion"), oE);
        //        }
        //    }
        //}

        /// <summary>
        /// returns the major version number of Word
        /// </summary>
        /// <returns></returns>
        public static byte GetWinwordVersionFromRegistry()
        {
            string xWinEXEPath = Registry.GetLocalMachineValue(
                @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe", "");

            try
            {
                System.Diagnostics.FileVersionInfo oVersionInfo =
                    System.Diagnostics.FileVersionInfo.GetVersionInfo(xWinEXEPath);

                return (byte)oVersionInfo.ProductMajorPart;
            }

            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.FileException(
                    LMP.Resources.GetLangString("Error_CouldNotRetrieveVersion"), oE);
            }
        }

        /// <summary>
        /// returns the major version number of Outlook
        /// </summary>
        /// <returns></returns>
		//GLOG : 7469 : ceh
        public static byte GetOutlookVersionFromRegistry()
        {
            string xWinEXEPath = Registry.GetLocalMachineValue(
                @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\OUTLOOK.EXE", "");

            if (xWinEXEPath != "")
            {
                try
                {
                    System.Diagnostics.FileVersionInfo oVersionInfo =
                        System.Diagnostics.FileVersionInfo.GetVersionInfo(xWinEXEPath);

                    return (byte)oVersionInfo.ProductMajorPart;
                }

                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.FileException(
                        LMP.Resources.GetLangString("Error_CouldNotRetrieveVersion"), oE);
                }
            }
            else
                return 0;
        }

        /// <summary>
        /// returns the printer bin name for the specified bin ID
        /// </summary>
        /// <param name="iBinID"></param>
        /// <returns></returns>
        public static string GetPrinterBinName(int iBinID)
        {
            return LMP.Forte.MSWord.Application.GetPrinterBinName(iBinID);
        }

        /// <summary>
        /// sends the specified message to the specified address
        /// </summary>
        /// <param name="xEmailAddress"></param>
        /// <param name="xSubject"></param>
        /// <param name="xBody"></param>
        public static void SendMessage(string xFrom, string xSubject,
            string xBody, MailServerParameters oClientParams, Attachment[] aAttachments, params string[] aRecipients)
        {
            MailMessage oMsg = new MailMessage();
            oMsg.From = new MailAddress(xFrom);

            foreach (string xRecipient in aRecipients)
            {
                oMsg.To.Add(new MailAddress(xRecipient));
            }

            oMsg.Subject = xSubject;
            oMsg.Body = xBody;
            oMsg.IsBodyHtml = true;

            if (aAttachments != null)
            {
                foreach (Attachment oAttachment in aAttachments)
                {
                    oMsg.Attachments.Add(oAttachment);
                }
            }

            SmtpClient oClient = new SmtpClient();

            if (oClientParams.UseNetworkCredentialsToLogin)
                oClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            else
            {
                System.Net.NetworkCredential oCredential = new System.Net.NetworkCredential(
                    oClientParams.UserID, oClientParams.Password);
                oClient.Credentials = oCredential;
            }

            oClient.Host = oClientParams.Host;
            oClient.EnableSsl = oClientParams.UseSsl;
            oClient.Port = oClientParams.Port;
            oClient.Send(oMsg);
        }
        /// <summary>
        /// returns True iff the specified message indicates
        /// that the tab key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="ShiftPressed">returns True iff the shift key was pressed along with the tab key</param>
        /// <returns></returns>
        public static bool TabKeyPressed(Message oMessage, out bool ShiftPressed)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_TAB = 9;
            const int KEY_SHIFT = 16;

            if (oMessage.Msg == WM_KEYDOWN && oMessage.WParam.ToInt32() == KEY_TAB)
            {
                //tab key was pressed - get state of shift key
                int iKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                ShiftPressed = !(iKeyState == 1 || iKeyState == 0);

                return true;
            }
            else
            {
                //tab key was not pressed
                ShiftPressed = false;
                return false;
            }
        }
        /// <summary>
        /// returns True if the specified message indicates
        /// that the Enter key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="bNoKeyQualifiers"</param>
        /// <returns></returns>
        public static bool EnterKeyPressed(Message oMessage, out int iQualifierKeys)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_ENTER = 13;
            const int KEY_SHIFT = 16;
            const int KEY_CTRL = 17;
            const int KEY_ALTL = 164;
            const int KEY_ALTR = 165;

            iQualifierKeys = 0;
            if (oMessage.Msg == WM_KEYDOWN && oMessage.WParam.ToInt32() == KEY_ENTER)
            {
                int iKeyState = LMP.WinAPI.GetKeyState(KEY_CTRL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_CTRL;

                iKeyState = LMP.WinAPI.GetKeyState(KEY_ALTL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_ALTL;

                iKeyState = LMP.WinAPI.GetKeyState(KEY_ALTR);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_ALTR;

                iKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_SHIFT;


                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// returns True if the specified message indicates
        /// that the Enter key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="bNoKeyQualifiers"</param>
        /// <returns></returns>
        public static bool AltKeyPressed(Message oMessage, out int iQualifierKeys)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_ALT = 18;
            const int KEY_SHIFT = 16;
            const int KEY_CTRL = 17;

            iQualifierKeys = 0;
            if ((oMessage.Msg == 260 || oMessage.Msg == WM_KEYDOWN) && oMessage.WParam.ToInt32() == KEY_ALT)
            {
                int iKeyState = LMP.WinAPI.GetKeyState(KEY_CTRL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_CTRL;

                iKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_SHIFT;

                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Track temp files that we were unable to delete during session
        /// </summary>
        /// <param name="xNewFile"></param>
        public static void AddFileToDeletionList(string xNewFile)
        {
            if (!m_oFilesForDeletion.Contains(xNewFile))
            {
                m_oFilesForDeletion.Add(xNewFile);
            }
        }
        /// <summary>
        /// Delete files in list that could not be deleted during session
        /// </summary>
        public static void DeleteTemporaryFiles()
        {
            foreach (string xFile in m_oFilesForDeletion)
            {
                try
                {
                    File.Delete(xFile);
                }
                catch 
                {
                }
            }
        }

        /// <summary>
        /// Returns true if xProcess is running
        /// </summary>
        /// <returns></returns>
		//GLOG : 7469 : ceh
        public static bool ProcessIsRunning(string xProcess)
        {
            return (System.Diagnostics.Process.GetProcessesByName(xProcess).Length != 0);
        }

        /// <summary>
        /// Returns Window handle of running Word session
        /// </summary>
        /// <returns></returns>
        public static IntPtr GetWordHandle()
        {
            //GLOG 3505: Check Session ID, since GetProcessesByName might return
            //processes running in a different Terminal Server session
            //SessionID will be 0 in non-TS environment
            int iSession = System.Diagnostics.Process.GetCurrentProcess().SessionId;
            System.Diagnostics.Process[] aProcesses = System.Diagnostics.Process.GetProcessesByName("Winword");
            for (int i = 0; i < aProcesses.Length; i++)
            {
                if (aProcesses[i].SessionId == iSession)
                {
                    return aProcesses[i].MainWindowHandle;
                }
            }
            //No instances found
            return IntPtr.Zero;
        }

        public static IWin32Window GetWordWindow()
        {
            IntPtr iHandle = GetWordHandle();
            return new WindowWrapper(iHandle);
        }

        private class WindowWrapper : System.Windows.Forms.IWin32Window
        {
            private IntPtr hwnd;

            public WindowWrapper(IntPtr handle)
            {
                hwnd = handle;
            }

            public IntPtr Handle
            {
                get { return hwnd; }
            }
        }

        /// <summary>
        /// returns the specified string after evaluating
        /// its environment variables
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        public static string EvaluateEnvironmentVariables(string xText)
        {

            //evaluate path for environment variables
            int iPos = xText.IndexOf('%');
            while (iPos > -1)
            {
                int iPos2 = xText.IndexOf('%', iPos + 1);

                if (iPos2 == -1)
                {
                    //something's wrong
                    throw new LMP.Exceptions.DirectoryException(
                        LMP.Resources.GetLangString("Error_InvalidDirectory") + xText);
                }

                string xEnvVar = xText.Substring(iPos + 1, iPos2 - iPos - 1);

                //evaluate
                xEnvVar = System.Environment.GetEnvironmentVariable(xEnvVar);

                xText = xText.Substring(0, iPos) + xEnvVar + xText.Substring(iPos2 + 1);

                iPos = xText.IndexOf('%', iPos + 1);
            }

            return xText;
        }
        [DllImport("mpr.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int WNetGetConnection(
            [MarshalAs(UnmanagedType.LPTStr)] string localName,
            [MarshalAs(UnmanagedType.LPTStr)] StringBuilder remoteName,
            ref int length);

        public static string GetUNCPath(string xOriginalPath)
        {
            StringBuilder sb = new StringBuilder(512);
            int iSize = sb.Capacity;

            // look for the {LETTER}: combination ...
            if (xOriginalPath.Length > 2 && xOriginalPath[1] == ':')
            {
                // don't use char.IsLetter here - as that can be misleading
                // the only valid drive letters are a-z && A-Z.
                char c = xOriginalPath[0];
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    int iError = WNetGetConnection(xOriginalPath.Substring(0, 2),
                        sb, ref iSize);
                    if (iError == 0)
                    {
                        DirectoryInfo dir = new DirectoryInfo(xOriginalPath);

                        string xPath = Path.GetFullPath(xOriginalPath)
                            .Substring(Path.GetPathRoot(xOriginalPath).Length);
                        return Path.Combine(sb.ToString().TrimEnd(), xPath);
                    }
                }
            }
            return xOriginalPath;
        }
        /// <summary>
        /// Searches child window hierarchy of Parent Window looking for match for specified Window Class and Title
        /// </summary>
        /// <param name="iParentWindow"></param>
        /// <param name="xClass"></param>
        /// <param name="xTitle"></param>
        /// <returns></returns>
        public static IntPtr FindChildWindow(IntPtr iParentWindow, string xClass, string xTitle)
        {
            // Search for match in top-level children
            IntPtr hwnd = LMP.WinAPI.FindWindowEx(iParentWindow, IntPtr.Zero, xClass, xTitle);
            if (hwnd == IntPtr.Zero)
            {
                // No match at top level, search children
                IntPtr hwndChild = LMP.WinAPI.FindWindowEx((IntPtr)iParentWindow, IntPtr.Zero, null, null);
                while (hwndChild != IntPtr.Zero && hwnd == IntPtr.Zero)
                {
                    //Search child windows of each child until match is found
                    hwnd = FindChildWindow(hwndChild, xClass, xTitle);
                    if (hwnd == IntPtr.Zero)
                    {
                        // If we didn't find it yet, check the next child.
                        hwndChild = LMP.WinAPI.FindWindowEx((IntPtr)iParentWindow, hwndChild, null, null);
                    }
                }
            }
            return hwnd;
        }

        //returns the screen scaling factor
        public static float GetScalingFactor()
        {
            System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
            return g.DpiX/96;
        }
    }
    /// <summary>
    /// Used to get IWin32Window object for MessageBox owner
    /// </summary>
    public class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        private IntPtr iHwnd;
        public WindowWrapper(IntPtr handle)
        {
            iHwnd = handle;
        }

        public IntPtr Handle
        {
            get { return iHwnd; }
        }
    }
}
