using System;
using System.IO;
using System.Text;
using System.Configuration;
using Diag = System.Diagnostics;
using System.Reflection;

namespace LMP
{
	[Flags]
	public enum mpBenchmarkModes: int
	{
		Off = 0,
		PrintToConsole = 1,
		PrintToFile = 2
	}

	/// <summary>
	/// manages benchmarks - prints to file, shows file, clears file, etc.
	/// </summary>
	public static class Benchmarks
	{
		private static string m_xBenchFile = null;
		private static mpBenchmarkModes m_iBenchMode = 0;
		private static long m_dtInitialStart = 0;

		/// <summary>
		/// private constructor - prevent instance creation
		/// </summary>
		static Benchmarks()
		{
            string xBenchMode = Registry.GetLocalMachineValue(
                LMP.Registry.MacPac10RegistryRoot, "BenchmarkMode");

            try
            {
                //ignore if couldn't get a valid value - value will
                //be set to 0, which is benchmarks off
                m_iBenchMode = (mpBenchmarkModes)System.Convert.ToInt32(xBenchMode);
            }
            catch { };

            //GLOG 15949: Write Bench.log to WritableDataDirectory if defined
            //get path from registry
            string xPath = Registry.GetLocalMachineValue(
                LMP.Registry.MacPac10RegistryRoot, "WritableDataDirectory");

            if (System.String.IsNullOrEmpty(xPath))
            {
                //JTS 3/28/13: Write Bench.log to DataDirectory if defined
                //get path from registry
                xPath = Registry.GetLocalMachineValue(
                    LMP.Registry.MacPac10RegistryRoot, "DataDirectory");
            }

            if (System.String.IsNullOrEmpty(xPath))
            {
                //no registry path was specified - get from root directory
                xPath = Registry.GetLocalMachineValue(
                    LMP.Registry.MacPac10RegistryRoot, "RootDirectory");
                if (!System.String.IsNullOrEmpty(xPath))
                    xPath = xPath + @"\data";
            }
            
            if (!System.String.IsNullOrEmpty(xPath))
                xPath = LMP.OS.EvaluateEnvironmentVariables(xPath);

            if (System.String.IsNullOrEmpty(xPath) || !System.IO.Directory.Exists(xPath))
            {
                //JTS 3/28/13: If Data directory not found, use MacPac\bin
                //get directory running this code
                System.Reflection.Assembly oAssembly =
                    System.Reflection.Assembly.GetExecutingAssembly();

                string xFullName = oAssembly.CodeBase.Substring(8);
                int iPos = xFullName.LastIndexOf(@"/");
                xPath = xFullName.Substring(0, iPos);
            }
			//set full name of bench.log
            m_xBenchFile = xPath + "\\bench_" + Environment.UserName + ".log"; //JTS 3/28/13: Include System UserName as part of file name
		}

		/// <summary>
		/// returns whether we're in benchmark mode - if
		/// true, all calls to PrintBenchmark will print
		/// benchmarks to the bench.log
		/// </summary>
		public static mpBenchmarkModes BenchmarkMode
		{
			get{return m_iBenchMode;}
		}

		public static string ElapsedTime(DateTime dtStart)
		{
			//get end time
			DateTime dtEnd = DateTime.Now;

			//get time xElapsedTime
			TimeSpan oElapsedTime = 
				new TimeSpan(dtEnd.Ticks - dtStart.Ticks);
			return oElapsedTime.TotalSeconds.ToString("0.00");
		}


		/// <summary>
		/// prints the running time only
		/// </summary>
		/// <param name="xLabels"></param>
		public static void Print(string xLabel)
		{
            Print(DateTime.Parse("1/1/2000", LMP.Culture.USEnglishCulture), xLabel);
		}

		/// <summary>
		/// prints the time elapsed since the specified start date-
		/// prints the specified labels next to the time elapsed
		/// </summary>
		/// <param name="dtStart"></param>
		/// <param name="xLabels"></param>
		public static void Print(DateTime dtStart,params string[] xLabels)
		{
			if(BenchmarkMode==mpBenchmarkModes.Off)
				return;
				
			//get end time
			DateTime dtEnd = DateTime.Now;

			string xElapsedTime = "";

			if(dtStart != DateTime.Parse("1/1/2000", LMP.Culture.USEnglishCulture))
			{
				//get time xElapsedTime
				TimeSpan oElapsedTime = 
					new TimeSpan(dtEnd.Ticks - dtStart.Ticks);

				//JTS 08/28/2009: Make sure decimal separator is '.'
                if(oElapsedTime.TotalSeconds < .04)
					xElapsedTime = "  " + oElapsedTime.TotalSeconds.ToString("0.000", LMP.Culture.USEnglishCulture);
				else
					xElapsedTime = "* " + oElapsedTime.TotalSeconds.ToString("0.000", LMP.Culture.USEnglishCulture);
			}
			else
				//write a '-' for elapsed time
				xElapsedTime = "-";

			//get total xRunning time
			string xRunning;
			if(m_dtInitialStart == 0)
			{
				//there is no previous time recorded
				xRunning = xElapsedTime;

				//set last end time for next iteration
				m_dtInitialStart = dtStart.Ticks;
			}
			else
			{
				//get time xElapsedTime since last recorded time
				TimeSpan xRunningTime =new TimeSpan(dtEnd.Ticks - m_dtInitialStart);
                //GLOG : 6302 : CEH
                xRunning = xRunningTime.TotalSeconds.ToString("0.000", LMP.Culture.USEnglishCulture);
			}

			//pad to force decimal alignment
			int iPos = xElapsedTime.IndexOf(".");
			string xPadBefore = new string(' ',6 - iPos + 1);
			string xPadAfter = new string(' ',6 - (xElapsedTime.Length - iPos));

			iPos = xRunning.IndexOf(".");
			string xRunningPadBefore = new string(' ',6 - iPos + 1);
			string xRunningPadAfter = new string(' ',6 - (xRunning.Length - iPos));

			//get xLabels, separate with ";" -
			//first label is the member name - 
			//set StackFrame of function that called this method - 
			//always has index of 1
			Diag.StackTrace oST = new Diag.StackTrace();
			Diag.StackFrame oStackFrame = oST.GetFrame(1);

			//get StackFrame method
			MethodBase oCaller = oStackFrame.GetMethod();

			if(oCaller.ReflectedType.FullName == "LMP.Benchmarks")
			{
				//called from an overload of this method - 
				//get stack previous to caller
				oStackFrame = oST.GetFrame(2);
				oCaller = oStackFrame.GetMethod();
			}

			//add member name
			StringBuilder oSB = new StringBuilder();
			oSB.AppendFormat("{0}.{1};",
				oCaller.ReflectedType.FullName, oCaller.Name);

			if(xLabels != null)
			{
				//add each label to stringbuilder
				foreach(string s in xLabels)
				{
					oSB.AppendFormat(" {0};",s);
				}
			}

			string xLabelString = oSB.ToString();
			xLabelString = xLabelString.TrimEnd(';');

			//build text to write
			oSB.Remove(0,oSB.Length);
			oSB.AppendFormat("{0}{1}{2}\t{3}{4}{5}\t{6}", 
				xPadBefore, xElapsedTime, xPadAfter,
				xRunningPadBefore, xRunning, xRunningPadAfter, xLabelString);

            if ((BenchmarkMode & mpBenchmarkModes.PrintToConsole) == mpBenchmarkModes.PrintToConsole)
            {
                //write to output
                System.Diagnostics.Debug.WriteLine(oSB.ToString());
            }
			
			if((BenchmarkMode & mpBenchmarkModes.PrintToFile) > 0)
			{
				//write to bench.log file
				FileInfo oBenchFile = new FileInfo(m_xBenchFile);

				FileStream oFS;
				StreamWriter oSW;
				if(oBenchFile.Exists)
					oSW = oBenchFile.AppendText();
				else
				{
					try
					{
						//create new file
						oFS = oBenchFile.Create();
					}
					catch(System.Exception e)
					{
						throw new LMP.Exceptions.FileException(
							Resources.GetLangString("Error_CouldNotCreateFile") + 
							oBenchFile.FullName,e);
					}

					//open write stream
					oSW = new StreamWriter(oFS);
				}
				
				//write to file
				oSW.WriteLine(oSB.ToString());
				oSW.Close();
			}
		}

		public static void Print(DateTime dtStart)
		{
			Print(dtStart, null);
		}
	
		/// <summary>
		/// resets the start of the running time counter
		/// </summary>
		public static void ResetStartTime()
		{
			m_dtInitialStart = 0;
		}
		
		/// <summary>
		/// clears the bench.log
		/// </summary>
		public static void Clear()
		{
			//delete bench.log
			try
			{
				File.Delete(m_xBenchFile);
				m_dtInitialStart = 0;
			}
			catch (System.Exception e)
			{
				//couldn't delete file - alert
				string xMsg = string.Concat(Resources.GetLangString(
					"Error_Benchmarks_Show")," ", e.Message);

				Exception oE= new Exception(xMsg);
				throw oE;
			}
		}

		/// <summary>
		/// opens the bench.log in notepad
		/// </summary>
		public static void Show()
		{
			if (File.Exists(m_xBenchFile))
			{
				try
				{
					//open file in notepad
					System.Diagnostics.Process.Start("notepad.exe", m_xBenchFile);
				}
				catch (System.Exception e)
				{
					//couldn't open file - alert
					string xMsg = string.Concat(Resources.GetLangString(
						"Error_Benchmarks_Show"), " ", e.Message);
					Exception oE= new Exception(xMsg);
					throw oE;
				}
			}
		}
	}
}
