﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using Word=Microsoft.Office.Interop.Word;

namespace LMP
{
    public class Conversion
    {
        public const string MacPacNamespace = "urn-legalmacpac-data/10";
        public const string WordNamespace = "http://schemas.microsoft.com/office/word/2003/wordml";
        public const string WordOpenXMLNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        public const string AmlNamespace = "http://schemas.microsoft.com/aml/2001/core";
        public static Random m_oRandom = null;

        public static bool IsPreconvertedXML(string xXML)
        {
            return String.IsWordOpenXML(xXML) ||
                xXML.Contains("w:type=\"Word.Bookmark.Start\" w:name=\"_mp");
        }

        /// <summary>
        ///adds v10.2 bookmarks and doc vars to tagged XML document
        /// </summary>
        /// <param name="xDocXML"></param>
        /// <param name="bForConversionToContentControls"></param>
        /// <returns></returns>
        public static string PreconvertIfNecessary(string xDocXML,
            bool bForConversionToContentControls)
        {
            return PreconvertIfNecessary(xDocXML, bForConversionToContentControls, "");
        }

        /// <summary>
        /// dds v10.2 bookmarks and doc vars to tagged XML document
        /// </summary>
        /// <param name="xDocXML"></param>
        /// <param name="bForConversionToContentControls"></param>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public static string PreconvertIfNecessary(string xDocXML,
            bool bForConversionToContentControls, string xSegmentID)
        {
            if (IsPreconvertedXML(xDocXML))
                return xDocXML;

            int iIndex = 0;

            //GLOG item #5057 - dcf -
            //remove sgi tag prefix - labels segments seem to have this
            //text in the source segment - it shouldn't be there, and
            //it is causing errors in cc support
            xDocXML = xDocXML.Replace("SGI¯°¯", "");

            string xPrefix = LMP.String.GetNamespacePrefix(xDocXML, MacPacNamespace);

            XmlDocument oDocXML = new XmlDocument();
            oDocXML.LoadXml(xDocXML);

            //add namespaces for XPath navigation
            XmlNamespaceManager oNSMan = new XmlNamespaceManager(oDocXML.NameTable);
            oNSMan.AddNamespace(xPrefix, MacPacNamespace);
            oNSMan.AddNamespace("w", WordNamespace);
            oNSMan.AddNamespace("aml", AmlNamespace);

            //get doc vars element
            XmlNode oDocVarsNode = oDocXML.SelectSingleNode(@"//w:docVars", oNSMan);

            //set counter for bookmark ID -
            //we set the counter high, then
            //decrement, as bookmark order is
            //determined by ID when start position
            //is the same
            int iBmkID = 800;

            if (oDocVarsNode == null)
            {
                //no doc vars in xml - add doc vars node
                XmlElement oDocVarsElement = oDocXML.CreateElement("w:docVars", WordNamespace);

                XmlNode oParentNode = oDocXML.SelectSingleNode("/w:wordDocument/w:docPr", oNSMan);
                oParentNode.AppendChild(oDocVarsElement);

                oDocVarsNode = oDocVarsElement;
            }

            //get all macpac segment tags
            XmlNodeList oMacPacNodes = oDocXML.SelectNodes(@"//" + xPrefix + ":mSEG", oNSMan);

            foreach (XmlNode oNode in oMacPacNodes)
            {
                XmlNode oParentMPNode = oNode.SelectSingleNode(
                    "ancestor::" + xPrefix + ":" + oNode.LocalName, oNSMan);

                if (oParentMPNode == null)
                {
                    AddBookmarkAndDocVarsForNode(oNode, oDocXML, oNSMan,
                        xPrefix, oDocVarsNode, ref iIndex, ref iBmkID,
                        bForConversionToContentControls);
                }
            }

            //remove the displacedBySDT attribute from the NewSegment
            //bookmark.start element - it causes the bookmark to be
            //deleted from a document created or opened in 2007 - this
            //has no effect on the bookmark in Word 2010
            XmlNode oBmkNode = oDocXML.SelectSingleNode(
                "//aml:annotation[@w:name=\"mpNewSegment\"]", oNSMan);
            if (oBmkNode != null)
            {
                XmlAttribute oAttr = oBmkNode.Attributes["w:displacedBySDT"];
                XmlElement oBmkElement = null;
                
                if (oAttr != null)
                {
                    //attribute found
                    oBmkElement = oAttr.OwnerElement;
                    oBmkElement.RemoveAttribute("w:displacedBySDT");
                }

                //12-21-10 (dm) - the displacedBySDT attribute also needs to be
                //removed from the bookmark.end element of the three built-in
                //collection segments with negative ids
                if ((xSegmentID != "") && (System.Int32.Parse(xSegmentID) < 0))
                {
                    oAttr = oBmkNode.Attributes["aml:id"];
                    if (oAttr != null)
                    {
                        oBmkNode = oDocXML.SelectSingleNode(
                            "//aml:annotation[@aml:id=\"" + oAttr.Value + "\" and " +
                            "@w:type=\"Word.Bookmark.End\"]", oNSMan);
                        if (oBmkNode != null)
                        {
                            oAttr = oBmkNode.Attributes["w:displacedBySDT"];
                            if (oAttr != null)
                            {
                                //attribute found
                                oBmkElement = oAttr.OwnerElement;
                                oBmkElement.RemoveAttribute("w:displacedBySDT");
                            }
                        }
                    }
                }
            }

            //release Random number generator
            m_oRandom = null;
            return oDocXML.OuterXml;
        }

        public static string RemoveSchemaNodes(string xXML)
        {
            string xPrefix = LMP.String.GetNamespacePrefix(xXML, MacPacNamespace);

            int iPosStart = xXML.IndexOf("<" + xPrefix + ":");

            while (iPosStart > -1)
            {
                int iPosEnd = xXML.IndexOf(">", iPosStart);

                xXML = xXML.Substring(0, iPosStart) + xXML.Substring(iPosEnd + 1);

                iPosStart = xXML.IndexOf("<" + xPrefix + ":");
            }

            iPosStart = xXML.IndexOf("<\\" + xPrefix + ":");

            while (iPosStart > -1)
            {
                int iPosEnd = xXML.IndexOf(">", iPosStart);

                xXML = xXML.Substring(0, iPosStart) + xXML.Substring(iPosEnd + 1);

                iPosStart = xXML.IndexOf("<\\" + xPrefix + ":");
            }

            return xXML;
        }

        private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode, XmlDocument oDocXML,
            XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode, ref int iIndex,
            ref int iBmkID)
        {
            AddBookmarkAndDocVarsForNode(oMPNode, oDocXML, oNSMan, xPrefix, oDocVarsNode,
               ref iIndex, ref iBmkID, null, false);
        }
        private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode, XmlDocument oDocXML,
            XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode, ref int iIndex,
            ref int iBmkID, bool bForConversionToContentControls)
        {
            AddBookmarkAndDocVarsForNode(oMPNode, oDocXML, oNSMan, xPrefix, oDocVarsNode,
                ref iIndex, ref iBmkID, null, bForConversionToContentControls);
        }

        private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode, XmlDocument oDocXML,
            XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode, ref int iIndex,
            ref int iBmkID, XmlDocument oEmbeddedXML, bool bForConversionToContentControls)
        {
            try
            {
                XmlDocument oTargetXML = null;
                if (oEmbeddedXML != null)
                    oTargetXML = oEmbeddedXML;
                else
                    oTargetXML = oDocXML;

                //do children first
                XmlNodeList oChildNodes = oMPNode.SelectNodes("descendant::" + xPrefix + ":*", oNSMan);
                foreach (XmlNode oChildNode in oChildNodes)
                {
                    XmlNodeList oParentMPNodes = oChildNode.SelectNodes(
                        "ancestor::" + xPrefix + ":*", oNSMan);

                    XmlNode oParentMPNode = null;

                    if (oParentMPNodes.Count > 0)
                    {
                        oParentMPNode = oParentMPNodes[oParentMPNodes.Count - 1];
                    }

                    if (oParentMPNode == oMPNode)
                    {
                        //this is a child of
                        AddBookmarkAndDocVarsForNode(oChildNode, oDocXML, oNSMan,
                            xPrefix, oDocVarsNode, ref iIndex, ref iBmkID, oEmbeddedXML,
                            bForConversionToContentControls);
                    }
                }

                //get basename
                string xBaseName = oMPNode.Name.Replace(xPrefix + ":", "");
                string xObjectData = oMPNode.SelectSingleNode("@ObjectData").Value;

                //construct new tag id - get basename prefix
                string xBaseNamePrefix = null;

                switch (xBaseName)
                {
                    case "mSEG":
                        xBaseNamePrefix = "mps";
                        break;
                    case "mVar":
                        xBaseNamePrefix = "mpv";
                        break;
                    case "mBlock":
                        xBaseNamePrefix = "mpb";
                        break;
                    case "mDel":
                        xBaseNamePrefix = "mpd";
                        break;
                    case "mSubVar":
                        xBaseNamePrefix = "mpu";
                        break;
                    case "mDocProps":
                        xBaseNamePrefix = "mpp";
                        break;
                    case "mSecProps":
                        xBaseNamePrefix = "mpc";
                        break;
                }

                string xDocVarID = GenerateDocVarID();
                string xBookmarkName = xBaseNamePrefix + xDocVarID;
                string xObjectDBID = null;

                //object db id
                if (xBaseName == "mSEG")
                {
                    xObjectDBID = GetmSEGObjectDataValue(xObjectData, "SegmentID");
                    if (xObjectDBID.Length < 19)
                    {
                        xObjectDBID = xObjectDBID.PadLeft(19, '0');
                    }
                }
                else
                {
                    //NOTE: iIndex is a dummy value since existing variables and blocks
                    //do not yet contain an ObjectDatabaseID property
                    if (xBaseName == "mVar" || xBaseName == "mBlock" || xBaseName == "mDel")
                    {
                        iIndex++;
                        xObjectDBID = iIndex.ToString();
                    }
                    else
                    {
                        xObjectDBID = "0";
                    }

                    //object db id for non-segments is a max 6 digits
                    if (xObjectDBID.Length < 6)
                    {
                        xObjectDBID = xObjectDBID.PadLeft(6, '0');
                    }
                }

                xBookmarkName += xObjectDBID;

                //bookmark names can only contain alphanumeric characters
                xBookmarkName = xBookmarkName.Replace('-', 'm');
                xBookmarkName = xBookmarkName.Replace('.', 'd');

                //pad to 40 characters
                xBookmarkName = "_" + xBookmarkName.PadRight(39, '0');

                //get mpo doc var name
                string xMPOVar = "mpo" + xDocVarID;

                //test for conflicts with existing bookmarks 
                //and doc vars - regenerate ID if necessary
                bool bDocVarExists = oDocXML.SelectSingleNode(
                    @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
                bool bBookmarkExists = oDocXML.SelectSingleNode(
                    @"//aml:annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;

                while (bBookmarkExists || bDocVarExists)
                {
                    //conflict exists, regenerate id
                    xDocVarID = GenerateDocVarID();
                    //include beginning '_'
                    xBookmarkName = xBookmarkName.Substring(0, 4) + xDocVarID + xBookmarkName.Substring(12);
                    xMPOVar = xMPOVar.Substring(0, 3) + xDocVarID;
                    xDocVarID = null;

                    //test again with new ID
                    bDocVarExists = oDocXML.SelectSingleNode(
                        @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
                    bBookmarkExists = oDocXML.SelectSingleNode(
                        @"//aml.annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;
                }

                //06/22/11: Set XMLNode Reserved Attribute to Bookmark Name without first character
                string xReserved = xBookmarkName.Substring(1);
                if (oMPNode.SelectSingleNode("@Reserved") == null)
                {
                    XmlAttribute oMPReserved = oTargetXML.CreateAttribute("Reserved");
                    oMPNode.Attributes.Append(oMPReserved);
                }
                oMPNode.SelectSingleNode("@Reserved").Value = xReserved;

                //11-29-10 (dm) - use TempID attribute to index nested adjacent tags
                XmlAttribute oTempIDAttr = oMPNode.Attributes["TempID"];
                if (oTempIDAttr != null)
                    oMPNode.Attributes.Remove(oTempIDAttr);
                if (HasAdjacentChild(oMPNode.InnerXml))
                {
                    int iTempID = 1;
                    XmlNode oChildNode = oMPNode.SelectSingleNode(
                        @".//" + xPrefix + ":*", oNSMan);
                    XmlNode oChildNodeTempID = oChildNode.SelectSingleNode("@TempID");
                    if (oChildNodeTempID == null)
                    {
                        //append TempID attribute to child's mpo doc var
                        xReserved = oChildNode.SelectSingleNode("@Reserved").Value;
                        string xChildDocVar = "mpo" + xReserved.Substring(3, 8);
                        XmlNode oChildDocVar = oDocXML.SelectSingleNode(
                            @"//w:docVar[@w:name='" + xChildDocVar + "']", oNSMan);
                        XmlAttribute oChildDocVarValue = oChildDocVar.Attributes["w:val"];
                        oChildDocVarValue.Value = oChildDocVarValue.Value + "ÌÍTempID=1";
                    }
                    else
                        iTempID = System.Int32.Parse(oChildNodeTempID.Value);
                    iTempID++;
                    oTempIDAttr = oTargetXML.CreateAttribute("TempID");
                    oTempIDAttr.Value = iTempID.ToString();
                    oMPNode.Attributes.Append(oTempIDAttr);
                }

                //create mpo docvar
                string xTagID = null;
                if (xBaseName == "mSubVar")
                {
                    xTagID = oMPNode.SelectSingleNode("@Name").Value;
                }
                else if ((xBaseName == "mDocProps") || (xBaseName == "mSecProps"))
                    //GLOG 6480 (dm) - mDocProps and mSecProps don't have a tag id or name
                    xTagID = "";
                else
                {
                    xTagID = oMPNode.SelectSingleNode("@TagID").Value;
                }

                //the rest of the value is encrypted
                string xAttrName = null;
                string xAttrs = null;
                string xPwd = "";

                foreach (XmlAttribute oAttr in oMPNode.Attributes)
                {
                    xAttrName = oAttr.Name;

                    if (xAttrName != "TagID" && xAttrName != "Name" && xAttrName != "DeletedScopes")
                    {
                        xAttrs += xAttrName + "=" + LMP.Forte.MSWord.Application.Decrypt(oAttr.Value, ref xPwd) + "ÌÍ";
                    }
                }

                //trim trailing separator
                xAttrs = xAttrs.TrimEnd('Ì', 'Í');

                //encrypt
                //added IsEncrypted condition (dm 2/11/11)
                if (xBaseName != "mDel" && xBaseName != "mSubVar" &&
                    LMP.String.IsEncrypted(xObjectData))
                    xAttrs = LMP.Forte.MSWord.Application.Encrypt(xAttrs, "");

                //add variable
                string xValue = xTagID + "ÌÍ" + xAttrs;

                //create doc var element
                XmlElement oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

                //create name & value attributes for element
                XmlAttribute oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
                oVarAttr.Value = xMPOVar;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
                oVarAttr.Value = xValue;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                //add element to doc vars element
                oDocVarsNode.AppendChild(oNewDocVarElement);

                //create mpd variable
                if (xBaseName == "mSEG")
                {
                    XmlNode oDeletedScopes = oMPNode.SelectSingleNode("@DeletedScopes");
                    if (oDeletedScopes != null && !string.IsNullOrEmpty(oDeletedScopes.Value))
                    {
                        string xDeletedScopes = oDeletedScopes.Value;
                        xDeletedScopes = LMP.Forte.MSWord.Application.Decrypt(xDeletedScopes, ref xPwd);

                        //add bookmarks and doc vars for tags in deleted scopes
                        AddBookmarksAndDocVarsForDeletedScopes(oDocXML, oNSMan, xPrefix,
                            oDocVarsNode, ref iIndex, ref iBmkID, ref xDeletedScopes, bForConversionToContentControls);

                        //update deleted scopes attribute
                        oDeletedScopes.Value = xDeletedScopes;

                        int iDelPart = 0;
                        //create multiple mpd variables if necessary
                        do
                        {
                            iDelPart++;
                            string xMPDVar = "mpd" + xDocVarID + iDelPart.ToString().PadLeft(2, '0');

                            //create doc var element
                            oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

                            //create name & value attributes for element
                            oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
                            oVarAttr.Value = xMPDVar;
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
                            oVarAttr.Value = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oDocVarsNode.AppendChild(oNewDocVarElement);
                            if (xDeletedScopes.Length > 65000)
                                xDeletedScopes = xDeletedScopes.Substring(65000);
                            else
                                break;
                        } while (xDeletedScopes != "");
                    }
                }

                //create bookmark start element
                XmlElement oBookmarkStart = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

                //create name & value attributes for element
                oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkStart.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
                oVarAttr.Value = "Word.Bookmark.Start";
                oBookmarkStart.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
                oVarAttr.Value = xBookmarkName;
                oBookmarkStart.Attributes.Append(oVarAttr);

                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                bool bIsInline = (oMPNode.SelectSingleNode(
                        "descendant::w:p", oNSMan) == null);
                if (!bIsInline)
                {
                    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
                    oVarAttr.Value = "prev";
                    oBookmarkStart.Attributes.Append(oVarAttr);
                }

                //create bookmark end element
                XmlElement oBookmarkEnd = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

                //create name & value attributes for element
                oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkEnd.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
                oVarAttr.Value = "Word.Bookmark.End";
                oBookmarkEnd.Attributes.Append(oVarAttr);

                //oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
                //oVarAttr.Value = xBookmarkName;
                //oBookmarkEnd.Attributes.Append(oVarAttr);

                //GLOG 5268 (dm) - not adding w:displacedBySDT was problematic
                //in the fax recipients table
                //if (oTargetNode.InnerXml.Contains("<w:p "))
                //{
                    //Omit this attribute if using alternate method above
                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                if (!bIsInline)
                {
                    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
                    oVarAttr.Value = "next";
                    oBookmarkEnd.Attributes.Append(oVarAttr);
                }

                //11-29-10 (dm) - add bookmark start and end tags
                bool bIsEmpty = (oMPNode.ChildNodes.Count == 0);
                if (bIsEmpty)
                {
                    oMPNode.PrependChild(oBookmarkEnd);
                    oMPNode.PrependChild(oBookmarkStart);
                }
                else
                {
                    oMPNode.PrependChild(oBookmarkStart);
                    oMPNode.AppendChild(oBookmarkEnd);
                }

                //set next aml id
                iBmkID--;
            }
            catch (System.Exception oE)
            {
                throw new System.Exception(oE.Message, oE);
            }
        }
        //private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode, XmlDocument oDocXML,
        //   XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode, ref int iIndex,
        //   ref int iBmkID, XmlDocument oEmbeddedXML, bool bForConversionToContentControls)
        //{
        //    XmlDocument oTargetXML = null;
        //    if (oEmbeddedXML != null)
        //        oTargetXML = oEmbeddedXML;
        //    else
        //        oTargetXML = oDocXML;

        //    //do children first
        //    XmlNodeList oChildNodes = oMPNode.SelectNodes("descendant::" + xPrefix + ":*", oNSMan);
        //    foreach (XmlNode oChildNode in oChildNodes)
        //    {
        //        XmlNodeList oParentMPNodes = oChildNode.SelectNodes(
        //            "ancestor::" + xPrefix + ":*", oNSMan);

        //        XmlNode oParentMPNode = null;

        //        if (oParentMPNodes.Count > 0)
        //        {
        //            oParentMPNode = oParentMPNodes[oParentMPNodes.Count - 1];
        //        }

        //        if (oParentMPNode == oMPNode)
        //        {
        //            //this is a child of
        //            AddBookmarkAndDocVarsForNode(oChildNode, oDocXML, oNSMan,
        //                xPrefix, oDocVarsNode, ref iIndex, ref iBmkID, oEmbeddedXML,
        //                bForConversionToContentControls);
        //        }
        //    }

        //    //get basename
        //    string xBaseName = oMPNode.Name.Replace(xPrefix + ":", "");

        //    string xObjectData = oMPNode.SelectSingleNode("@ObjectData").Value;

        //    //construct new tag id - get basename prefix
        //    string xBaseNamePrefix = null;

        //    switch (xBaseName)
        //    {
        //        case "mSEG":
        //            xBaseNamePrefix = "mps";
        //            break;
        //        case "mVar":
        //            xBaseNamePrefix = "mpv";
        //            break;
        //        case "mBlock":
        //            xBaseNamePrefix = "mpb";
        //            break;
        //        case "mDel":
        //            xBaseNamePrefix = "mpd";
        //            break;
        //        case "mSubVar":
        //            xBaseNamePrefix = "mpu";
        //            break;
        //        case "mDocProps":
        //            xBaseNamePrefix = "mpp";
        //            break;
        //        case "mSecProps":
        //            xBaseNamePrefix = "mpc";
        //            break;
        //    }

        //    string xDocVarID = GenerateDocVarID();
        //    string xBookmarkName = xBaseNamePrefix + xDocVarID;
        //    string xObjectDBID = null;

        //    //object db id
        //    if (xBaseName == "mSEG")
        //    {
        //        xObjectDBID = GetmSEGObjectDataValue(xObjectData, "SegmentID");
        //        if (xObjectDBID.Length < 19)
        //        {
        //            xObjectDBID = xObjectDBID.PadLeft(19, '0');
        //        }
        //    }
        //    else
        //    {
        //        //NOTE: iIndex is a dummy value since existing variables and blocks
        //        //do not yet contain an ObjectDatabaseID property
        //        if (xBaseName == "mVar" || xBaseName == "mBlock" || xBaseName == "mDel")
        //        {
        //            iIndex++;
        //            xObjectDBID = iIndex.ToString();
        //        }
        //        else
        //        {
        //            xObjectDBID = "0";
        //        }

        //        //object db id for non-segments is a max 6 digits
        //        if (xObjectDBID.Length < 6)
        //        {
        //            xObjectDBID = xObjectDBID.PadLeft(6, '0');
        //        }
        //    }

        //    xBookmarkName += xObjectDBID;

        //    //bookmark names can only contain alphanumeric characters
        //    xBookmarkName = xBookmarkName.Replace('-', 'm');
        //    xBookmarkName = xBookmarkName.Replace('.', 'd');

        //    //pad to 40 characters
        //    xBookmarkName = "_" + xBookmarkName.PadRight(39, '0');

        //    //get mpo doc var name
        //    string xMPOVar = "mpo" + xDocVarID;

        //    //test for conflicts with existing bookmarks 
        //    //and doc vars - regenerate ID if necessary
        //    bool bDocVarExists = oDocXML.SelectSingleNode(
        //        @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
        //    bool bBookmarkExists = oDocXML.SelectSingleNode(
        //        @"//aml:annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;

        //    while (bBookmarkExists || bDocVarExists)
        //    {
        //        //conflict exists, regenerate id
        //        xDocVarID = GenerateDocVarID();
        //        //include beginning '_'
        //        xBookmarkName = xBookmarkName.Substring(0, 4) + xDocVarID + xBookmarkName.Substring(12);
        //        xMPOVar = xMPOVar.Substring(0, 3) + xDocVarID;
        //        xDocVarID = null;

        //        //test again with new ID
        //        bDocVarExists = oDocXML.SelectSingleNode(
        //            @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
        //        bBookmarkExists = oDocXML.SelectSingleNode(
        //            @"//aml.annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;
        //    }

        //    //06/22/11: Set XMLNode Reserved Attribute to Bookmark Name without first character
        //    string xReserved = xBookmarkName.Substring(1);
        //    if (oMPNode.SelectSingleNode("@Reserved") == null)
        //    {
        //        XmlAttribute oMPReserved = oTargetXML.CreateAttribute("Reserved");
        //        oMPNode.Attributes.Append(oMPReserved);
        //    }
        //    oMPNode.SelectSingleNode("@Reserved").Value = xReserved;

        //    //create mpo docvar
        //    string xTagID = null;
        //    if (xBaseName == "mSubVar")
        //    {
        //        xTagID = oMPNode.SelectSingleNode("@Name").Value;
        //    }
        //    else
        //    {
        //        xTagID = oMPNode.SelectSingleNode("@TagID").Value;
        //    }

        //    LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();

        //    //the rest of the value is encrypted
        //    string xAttrName = null;
        //    string xAttrs = null;
        //    string xPwd = "";

        //    foreach (XmlAttribute oAttr in oMPNode.Attributes)
        //    {
        //        xAttrName = oAttr.Name;

        //        if (xAttrName != "TagID" && xAttrName != "Name" && xAttrName != "DeletedScopes")
        //        {
        //            xAttrs += xAttrName + "=" + oCOM.Decrypt(oAttr.Value, ref xPwd) + "ÌÍ";
        //        }
        //    }

        //    //trim trailing separator
        //    xAttrs = xAttrs.TrimEnd('Ì', 'Í');

        //    //encrypt
        //    if (xBaseName != "mDel" && xBaseName != "mSubVar")
        //        xAttrs = oCOM.Encrypt(xAttrs, "");

        //    //add variable
        //    string xValue = xTagID + "ÌÍ" + xAttrs;

        //    //create doc var element
        //    XmlElement oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

        //    //create name & value attributes for element
        //    XmlAttribute oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xMPOVar;
        //    oNewDocVarElement.Attributes.Append(oVarAttr);

        //    oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
        //    oVarAttr.Value = xValue;
        //    oNewDocVarElement.Attributes.Append(oVarAttr);

        //    //add element to doc vars element
        //    oDocVarsNode.AppendChild(oNewDocVarElement);

        //    //create mpd variable
        //    if (xBaseName == "mSEG")
        //    {
        //        XmlNode oDeletedScopes = oMPNode.SelectSingleNode("@DeletedScopes");
        //        if (oDeletedScopes != null && !string.IsNullOrEmpty(oDeletedScopes.Value))
        //        {

        //            //TODO: we'll have to do the following in Word, when the 
        //            //doc is opened for final conversion to content controls
        //            //object xDeletedScopes = oCOM.ConvertDeletedScopesToOpenXML(oDeletedScopes.Value, oWordDoc);
        //            string xDeletedScopes = oDeletedScopes.Value;
        //            xDeletedScopes = oCOM.Decrypt(xDeletedScopes, ref xPwd);

        //            //add bookmarks and doc vars for tags in deleted scopes
        //            AddBookmarksAndDocVarsForDeletedScopes(oDocXML, oNSMan, xPrefix,
        //                oDocVarsNode, ref iIndex, ref iBmkID, ref xDeletedScopes, bForConversionToContentControls);

        //            //update deleted scopes attribute
        //            oDeletedScopes.Value = xDeletedScopes;

        //            int iDelPart = 0;
        //            //create multiple mpd variables if necessary
        //            do
        //            {
        //                iDelPart++;
        //                string xMPDVar = "mpd" + xDocVarID + iDelPart.ToString().PadLeft(2, '0');

        //                //create doc var element
        //                oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

        //                //create name & value attributes for element
        //                oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
        //                oVarAttr.Value = xMPDVar;
        //                oNewDocVarElement.Attributes.Append(oVarAttr);

        //                oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
        //                oVarAttr.Value = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
        //                oNewDocVarElement.Attributes.Append(oVarAttr);

        //                oDocVarsNode.AppendChild(oNewDocVarElement);
        //                if (xDeletedScopes.Length > 65000)
        //                    xDeletedScopes = xDeletedScopes.Substring(65000);
        //                else
        //                    break;
        //            } while (xDeletedScopes != "");
        //        }
        //    }

        //    XmlNode oTargetNode = null;
        //    oTargetNode = oMPNode;

        //    //create bookmark start element
        //    XmlElement oBookmarkStart = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

        //    //create name & value attributes for element
        //    oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
        //    oVarAttr.Value = iBmkID.ToString();
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
        //    oVarAttr.Value = "Word.Bookmark.Start";
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xBookmarkName;
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    //if (bForConversionToContentControls)
        //    //{
        //    //Omit this attribute if using alternate method below
        //    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
        //    //oVarAttr.Value = oTargetNode.LocalName != "mDel" ? "prev" : "next";
        //    oVarAttr.Value = "prev";
        //    oBookmarkStart.Attributes.Append(oVarAttr);
        //    //}

        //    //create bookmark end element
        //    XmlElement oBookmarkEnd = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

        //    //create name & value attributes for element
        //    oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
        //    oVarAttr.Value = iBmkID.ToString();
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
        //    oVarAttr.Value = "Word.Bookmark.End";
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xBookmarkName;
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    //if (bForConversionToContentControls)
        //    //{
        //    //Omit this attribute if using alternate method above
        //    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
        //    //oVarAttr.Value = oTargetNode.LocalName != "mDel" ? "next" : "prev";
        //    oVarAttr.Value = "next";
        //    oBookmarkEnd.Attributes.Append(oVarAttr);
        //    //}

        //    if (oTargetNode.LocalName == "mDel")
        //    {
        //        //add closing bookmark tag right below
        //        oTargetNode.PrependChild(oBookmarkEnd);

        //        //add opening bookmark tag
        //        oTargetNode.PrependChild(oBookmarkStart);
        //    }
        //    else
        //    {
        //        //add opening bookmark tag
        //        oTargetNode.PrependChild(oBookmarkStart);

        //        //add closing bookmark tag at end of tag
        //        oTargetNode.AppendChild(oBookmarkEnd);
        //    }

        //    //set next aml id
        //    iBmkID--;
        //}
        //private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode, XmlDocument oDocXML,
        //            XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode, ref int iIndex,
        //            ref int iBmkID, XmlDocument oEmbeddedXML, bool bForConversionToContentControls)
        //{
        //    XmlDocument oTargetXML = null;
        //    if (oEmbeddedXML != null)
        //        oTargetXML = oEmbeddedXML;
        //    else
        //        oTargetXML = oDocXML;

        //    //do children first
        //    XmlNodeList oChildNodes = oMPNode.SelectNodes("descendant::" + xPrefix + ":*", oNSMan);
        //    foreach (XmlNode oChildNode in oChildNodes)
        //    {
        //        XmlNodeList oParentMPNodes = oChildNode.SelectNodes(
        //            "ancestor::" + xPrefix + ":*", oNSMan);

        //        XmlNode oParentMPNode = null;

        //        if (oParentMPNodes.Count > 0)
        //        {
        //            oParentMPNode = oParentMPNodes[oParentMPNodes.Count - 1];
        //        }

        //        if (oParentMPNode == oMPNode)
        //        {
        //            //this is a child of
        //            AddBookmarkAndDocVarsForNode(oChildNode, oDocXML, oNSMan,
        //                xPrefix, oDocVarsNode, ref iIndex, ref iBmkID, oEmbeddedXML,
        //                bForConversionToContentControls);
        //        }
        //    }

        //    //get basename
        //    string xBaseName = oMPNode.Name.Replace(xPrefix + ":", "");

        //    string xObjectData = oMPNode.SelectSingleNode("@ObjectData").Value;

        //    //construct new tag id - get basename prefix
        //    string xBaseNamePrefix = null;

        //    switch (xBaseName)
        //    {
        //        case "mSEG":
        //            xBaseNamePrefix = "mps";
        //            break;
        //        case "mVar":
        //            xBaseNamePrefix = "mpv";
        //            break;
        //        case "mBlock":
        //            xBaseNamePrefix = "mpb";
        //            break;
        //        case "mDel":
        //            xBaseNamePrefix = "mpd";
        //            break;
        //        case "mSubVar":
        //            xBaseNamePrefix = "mpu";
        //            break;
        //        case "mDocProps":
        //            xBaseNamePrefix = "mpp";
        //            break;
        //        case "mSecProps":
        //            xBaseNamePrefix = "mpc";
        //            break;
        //    }

        //    string xDocVarID = GenerateDocVarID();
        //    string xBookmarkName = xBaseNamePrefix + xDocVarID;
        //    string xObjectDBID = null;

        //    //object db id
        //    if (xBaseName == "mSEG")
        //    {
        //        xObjectDBID = GetmSEGObjectDataValue(xObjectData, "SegmentID");
        //        if (xObjectDBID.Length < 19)
        //        {
        //            xObjectDBID = xObjectDBID.PadLeft(19, '0');
        //        }
        //    }
        //    else
        //    {
        //        //NOTE: iIndex is a dummy value since existing variables and blocks
        //        //do not yet contain an ObjectDatabaseID property
        //        if (xBaseName == "mVar" || xBaseName == "mBlock" || xBaseName == "mDel")
        //        {
        //            iIndex++;
        //            xObjectDBID = iIndex.ToString();
        //        }
        //        else
        //        {
        //            xObjectDBID = "0";
        //        }

        //        //object db id for non-segments is a max 6 digits
        //        if (xObjectDBID.Length < 6)
        //        {
        //            xObjectDBID = xObjectDBID.PadLeft(6, '0');
        //        }
        //    }

        //    xBookmarkName += xObjectDBID;

        //    //bookmark names can only contain alphanumeric characters
        //    xBookmarkName = xBookmarkName.Replace('-', 'm');
        //    xBookmarkName = xBookmarkName.Replace('.', 'd');

        //    //pad to 40 characters
        //    xBookmarkName = "_" + xBookmarkName.PadRight(39, '0');

        //    //get mpo doc var name
        //    string xMPOVar = "mpo" + xDocVarID;

        //    //test for conflicts with existing bookmarks 
        //    //and doc vars - regenerate ID if necessary
        //    bool bDocVarExists = oDocXML.SelectSingleNode(
        //        @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
        //    bool bBookmarkExists = oDocXML.SelectSingleNode(
        //        @"//aml:annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;

        //    while (bBookmarkExists || bDocVarExists)
        //    {
        //        //conflict exists, regenerate id
        //        xDocVarID = GenerateDocVarID();
        //        //include beginning '_'
        //        xBookmarkName = xBookmarkName.Substring(0, 4) + xDocVarID + xBookmarkName.Substring(12);
        //        xMPOVar = xMPOVar.Substring(0, 3) + xDocVarID;
        //        xDocVarID = null;

        //        //test again with new ID
        //        bDocVarExists = oDocXML.SelectSingleNode(
        //            @"//w:docVar[@w:name='" + xMPOVar + "']", oNSMan) != null;
        //        bBookmarkExists = oDocXML.SelectSingleNode(
        //            @"//aml.annotation[@w:name='" + xBookmarkName + "']", oNSMan) != null;
        //    }

        //    //06/22/11: Set XMLNode Reserved Attribute to Bookmark Name without first character
        //    string xReserved = xBookmarkName.Substring(1);
        //    if (oMPNode.SelectSingleNode("@Reserved") == null)
        //    {
        //        XmlAttribute oMPReserved = oTargetXML.CreateAttribute("Reserved");
        //        oMPNode.Attributes.Append(oMPReserved);
        //    }
        //    oMPNode.SelectSingleNode("@Reserved").Value = xReserved;

        //    //create mpo docvar
        //    string xTagID = null;
        //    if (xBaseName == "mSubVar")
        //    {
        //        xTagID = oMPNode.SelectSingleNode("@Name").Value;
        //    }
        //    else
        //    {
        //        xTagID = oMPNode.SelectSingleNode("@TagID").Value;
        //    }

        //    LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();

        //    //the rest of the value is encrypted
        //    string xAttrName = null;
        //    string xAttrs = null;
        //    string xPwd = "";

        //    foreach (XmlAttribute oAttr in oMPNode.Attributes)
        //    {
        //        xAttrName = oAttr.Name;

        //        if (xAttrName != "TagID" && xAttrName != "Name" && xAttrName != "DeletedScopes")
        //        {
        //            xAttrs += xAttrName + "=" + oCOM.Decrypt(oAttr.Value, ref xPwd) + "ÌÍ";
        //        }
        //    }

        //    //trim trailing separator
        //    xAttrs = xAttrs.TrimEnd('Ì', 'Í');

        //    //encrypt
        //    if (xBaseName != "mDel" && xBaseName != "mSubVar")
        //        xAttrs = oCOM.Encrypt(xAttrs, "");

        //    //add variable
        //    string xValue = xTagID + "ÌÍ" + xAttrs;

        //    //create doc var element
        //    XmlElement oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

        //    //create name & value attributes for element
        //    XmlAttribute oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xMPOVar;
        //    oNewDocVarElement.Attributes.Append(oVarAttr);

        //    oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
        //    oVarAttr.Value = xValue;
        //    oNewDocVarElement.Attributes.Append(oVarAttr);

        //    //add element to doc vars element
        //    oDocVarsNode.AppendChild(oNewDocVarElement);

        //    //create mpd variable
        //    if (xBaseName == "mSEG")
        //    {
        //        XmlNode oDeletedScopes = oMPNode.SelectSingleNode("@DeletedScopes");
        //        if (oDeletedScopes != null && !string.IsNullOrEmpty(oDeletedScopes.Value))
        //        {

        //            //TODO: we'll have to do the following in Word, when the 
        //            //doc is opened for final conversion to content controls
        //            //object xDeletedScopes = oCOM.ConvertDeletedScopesToOpenXML(oDeletedScopes.Value, oWordDoc);
        //            string xDeletedScopes = oDeletedScopes.Value;
        //            xDeletedScopes = oCOM.Decrypt(xDeletedScopes, ref xPwd);

        //            //add bookmarks and doc vars for tags in deleted scopes
        //            AddBookmarksAndDocVarsForDeletedScopes(oDocXML, oNSMan, xPrefix,
        //                oDocVarsNode, ref iIndex, ref iBmkID, ref xDeletedScopes, bForConversionToContentControls);

        //            //update deleted scopes attribute
        //            oDeletedScopes.Value = xDeletedScopes;

        //            int iDelPart = 0;
        //            //create multiple mpd variables if necessary
        //            do
        //            {
        //                iDelPart++;
        //                string xMPDVar = "mpd" + xDocVarID + iDelPart.ToString().PadLeft(2, '0');

        //                //create doc var element
        //                oNewDocVarElement = oDocXML.CreateElement("w:docVar", WordNamespace);

        //                //create name & value attributes for element
        //                oVarAttr = oDocXML.CreateAttribute("w:name", WordNamespace);
        //                oVarAttr.Value = xMPDVar;
        //                oNewDocVarElement.Attributes.Append(oVarAttr);

        //                oVarAttr = oDocXML.CreateAttribute("w:val", WordNamespace);
        //                oVarAttr.Value = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
        //                oNewDocVarElement.Attributes.Append(oVarAttr);

        //                oDocVarsNode.AppendChild(oNewDocVarElement);
        //                if (xDeletedScopes.Length > 65000)
        //                    xDeletedScopes = xDeletedScopes.Substring(65000);
        //                else
        //                    break;
        //            } while (xDeletedScopes != "");
        //        }
        //    }

        //    //create bookmark start element
        //    XmlElement oBookmarkStart = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

        //    //create name & value attributes for element
        //    oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
        //    oVarAttr.Value = iBmkID.ToString();
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
        //    oVarAttr.Value = "Word.Bookmark.Start";
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xBookmarkName;
        //    oBookmarkStart.Attributes.Append(oVarAttr);

        //    if (bForConversionToContentControls)
        //    {
        //        //Omit this attribute if using alternate method below
        //        oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
        //        oVarAttr.Value = "prev";
        //        oBookmarkStart.Attributes.Append(oVarAttr);
        //    }

        //    XmlNode oTargetNode = null;
        //    oTargetNode = oMPNode;

        //    oTargetNode.PrependChild(oBookmarkStart);

        //    //create bookmark end element
        //    XmlElement oBookmarkEnd = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

        //    //create name & value attributes for element
        //    oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
        //    oVarAttr.Value = iBmkID.ToString();
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
        //    oVarAttr.Value = "Word.Bookmark.End";
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
        //    oVarAttr.Value = xBookmarkName;
        //    oBookmarkEnd.Attributes.Append(oVarAttr);

        //    if (bForConversionToContentControls)
        //    {
        //        //Omit this attribute if using alternate method above
        //        oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
        //        oVarAttr.Value = "next";
        //        oBookmarkEnd.Attributes.Append(oVarAttr);
        //    }

        //    oTargetNode.AppendChild(oBookmarkEnd);

        //    //set next aml id
        //    iBmkID++;
        //}
        private static void AddBookmarksAndDocVarsForDeletedScopes(XmlDocument oDocXML,
            XmlNamespaceManager oNSMan, string xPrefix, XmlNode oDocVarsNode,
            ref int iIndex, ref int iBmkID, ref string xDeletedScopes, bool bForConversionToContentControls)
        {
            //create new xml document
            XmlDocument oEmbeddedXML = new XmlDocument();

            //add namespaces for XPath navigation
            XmlNamespaceManager oEmbeddedNSMan = new XmlNamespaceManager(oEmbeddedXML.NameTable);
            oEmbeddedNSMan.AddNamespace(xPrefix, MacPacNamespace);
            oEmbeddedNSMan.AddNamespace("w", WordNamespace);
            oEmbeddedNSMan.AddNamespace("aml", AmlNamespace);

            //cycle though delete scopes
            int iPos = xDeletedScopes.IndexOf("¬w:wordDocument ");
            while (iPos > -1)
            {
                //get body node
                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>", iPos) + 9;
                string xBody = xDeletedScopes.Substring(iPos, iPos2 - iPos);

                //restore XML character
                xBody = xBody.Replace("¬", "<");

                //append w:wordDocument closing tag
                xBody = xBody + "</w:wordDocument>";

                //load xml
                oEmbeddedXML.LoadXml(xBody);

                //cycle through MacPac tags, adding bookmarks and doc vars -
                //bookmark and reserved attribute get added to oEmbeddedXML,
                //while doc vars get added to oDocXML
                //get all macpac tags
                XmlNodeList oMacPacNodes = oEmbeddedXML.SelectNodes(@"//" +
                    xPrefix + ":*", oEmbeddedNSMan);

                foreach (XmlNode oNode in oMacPacNodes)
                {
                    XmlNode oParentMPNode = oNode.SelectSingleNode("ancestor::" +
                        xPrefix + ":*", oEmbeddedNSMan);

                    if (oParentMPNode == null)
                    {
                        AddBookmarkAndDocVarsForNode(oNode, oDocXML, oNSMan, xPrefix,
                            oDocVarsNode, ref iIndex, ref iBmkID, oEmbeddedXML,
                            bForConversionToContentControls);
                    }
                }

                //replace body node, removing w:wordDocument closing tag
                xBody = oEmbeddedXML.OuterXml;
                xDeletedScopes = xDeletedScopes.Substring(0, iPos) + xBody.Substring(0,
                    xBody.Length - 17) + xDeletedScopes.Substring(iPos2);

                //search for next scope
                iPos = xDeletedScopes.IndexOf("¬w:wordDocument ");
            }

            //replace XML character
            xDeletedScopes = xDeletedScopes.Replace("<", "¬");
        }

        /// <summary>
        /// returns a new docvar id
        /// </summary>
        /// <returns></returns>
        public static string GenerateDocVarID()
        {
            //hold in static variable - otherwise if clock has not advanced enough from last pass
            //number generated by NextDouble may be a duplicate
            if (m_oRandom == null)
                m_oRandom = new Random();
            string xDocVarID = m_oRandom.NextDouble().ToString();
            xDocVarID = xDocVarID.Substring(2, 8);
            return xDocVarID;
        }

        /// <summary>
        /// gets the value of the specified segment property
        /// </summary>
        /// <param name="xObjectData"></param>
        /// <param name="xProperty"></param>
        /// <returns></returns>
        public static string GetmSEGObjectDataValue(string xObjectData, string xProperty)
        {
            string xPwd = "";

            xObjectData = LMP.Forte.MSWord.Application.Decrypt(xObjectData, ref xPwd);

            int iPos1 = xObjectData.IndexOf(xProperty + "=");

            if (iPos1 > -1)
            {
                //property found - parse from object data
                iPos1 += xProperty.Length + 1;

                //get delimiting pipe - it may or may not exist
                int iPos2 = xObjectData.IndexOf("|", iPos1);

                string xValue = null;

                if (iPos2 > -1)
                {
                    //pipe exists - parse to pipe
                    xValue = xObjectData.Substring(iPos1, iPos2 - iPos1);
                }
                else
                {
                    //pipe doesn't exist - parse to end of string
                    xValue = xObjectData.Substring(iPos1);
                }

                return xValue;
            }

            return "";
        }

        public static string RemoveCustomXMLTags(string xXML)
        {
            if (String.IsWordOpenXML(xXML))
            {
                XmlDocument oDocXML = new XmlDocument();
                oDocXML.LoadXml(xXML);

                XmlNamespaceManager oNSMan = new XmlNamespaceManager(oDocXML.NameTable);
                oNSMan.AddNamespace("w", WordOpenXMLNamespace);

                XmlNodeList oNodes = oDocXML.SelectNodes("descendant::w:customXmlPr", oNSMan);
                foreach (XmlNode oNode in oNodes)
                {
                    oNode.RemoveAll();
                }

                xXML = oDocXML.OuterXml;

                StringBuilder oSB = new StringBuilder();

                int iPos1 = 0;
                int iPos2 = xXML.IndexOf("<w:customXml ");

                if (iPos2 == -1)
                    return xXML;

                while (iPos2 > -1)
                {
                    oSB.Append(xXML.Substring(iPos1, iPos2 - iPos1));
                    iPos1 = xXML.IndexOf(">", iPos2) + 1;
                    iPos2 = xXML.IndexOf("<w:customXml ", iPos1);
                }

                oSB.Append(xXML.Substring(iPos1));

                oSB = oSB.Replace("</w:customXml>", "");
                oSB = oSB.Replace("<w:customXmlPr></w:customXmlPr>", "");

                return oSB.ToString();
            }
            else
            {
                //JTS 6/11/10: Strips all <ns0:*> tags from XML
                //We may want to force this in environments where
                //Custom XML is not supported, if we find that Word's
                //handling of bookmarks for stripped Tags is not consistent
                const string MacPacNamespace = "urn-legalmacpac-data/10";

                string xPrefix = LMP.String.GetNamespacePrefix(xXML, MacPacNamespace);

                Regex oBodyRegex = new Regex("<w:body.*</w:body>");
                Match oBody = oBodyRegex.Match(xXML, 0);

                Regex oRegex = new Regex("<" + xPrefix + ":.*?>|</" + xPrefix + ":.*?>");
                string xTempXML = oRegex.Replace(oBody.Value, "");

                xXML = xXML.Substring(0, oBody.Index) + xTempXML + xXML.Substring(oBody.Index + oBody.Length);

                xXML = xXML.Replace(" w:displacedBySDT=\"prev\"", "");
                xXML = xXML.Replace(" w:displacedBySDT=\"next\"", "");

                return xXML;
            }
        }

        /// <summary>
        /// links MacPac numbering styles to list template levels if necessary
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        public static string RelinkNumberingSchemes(string xXML)
        {
            if (String.IsWordOpenXML(xXML))
            {
                XmlDocument oDocXML = new XmlDocument();
                oDocXML.LoadXml(xXML);

                XmlNamespaceManager oNSMan = new XmlNamespaceManager(oDocXML.NameTable);
                oNSMan.AddNamespace("w", WordOpenXMLNamespace);

                //search for MacPac list templates
                XmlNodeList oNodes = oDocXML.SelectNodes(
                    @"//w:abstractNum/w:name[starts-with(@w:val, 'zzmp')]", oNSMan);
                if (oNodes.Count > 0)
                {
                    foreach (XmlNode oNode in oNodes)
                    {
                        //get style root
                        string xLT = oNode.Attributes["w:val"].Value;
                        int iPos = xLT.IndexOf('|');
                        string xStyleRoot = xLT.Substring(4, iPos - 4);

                        //cycle through list template levels, adding linked
                        //style if necessary
                        XmlNode oLTNode = oNode.ParentNode;
                        for (int i = 1; i <= 9; i++)
                        {
                            //check whether style for this level exists
                            string xStyle = xStyleRoot + "_L" + i.ToString();
                            if (xXML.IndexOf(xStyle) < 0)
                                break;

                            //get level node
                            XmlNode oLevelNode = oLTNode.SelectSingleNode(
                                "child::w:lvl[@w:ilvl='" + (i - 1).ToString() +
                                "']", oNSMan);
                            if (oLevelNode == null)
                                break;

                            //add style node if necessary
                            XmlNode oStyleNode = oLevelNode.SelectSingleNode(
                                "child::w:pStyle", oNSMan);
                            if (oStyleNode == null)
                            {
                                XmlElement oElement = oDocXML.CreateElement(
                                    "w:pStyle", WordOpenXMLNamespace);
                                XmlAttribute oAttr = oDocXML.CreateAttribute("w:val",
                                    WordOpenXMLNamespace);
                                oAttr.Value = xStyleRoot + "L" + i.ToString();
                                oElement.Attributes.Append(oAttr);
                                oLevelNode.AppendChild(oElement);
                            }
                        }
                    }
                    return oDocXML.OuterXml;
                }
                else
                    //segment contains no macpac numbering schemes
                    return xXML;
            }
            else
            {
                //not implemented for wordml
                return xXML;
            }
        }

        private static bool HasAdjacentChild(string xInnerXML)
        {
            string xPrefix = LMP.String.GetNamespacePrefix(xInnerXML,
                MacPacNamespace);
            if (xPrefix == "")
                return false;

            //prefix is sometimes returned with extra characters
            int iPos = xPrefix.IndexOf('=');
            if (iPos > -1)
                xPrefix = xPrefix.Substring(0, iPos);

            //search for first child tag
            iPos = xInnerXML.IndexOf('<' + xPrefix + ':');
            if (iPos == -1)
                return false;

            //if run or end of paragraph occurs before child,
            //child is not adjacent
            int iPos2 = xInnerXML.IndexOf("<w:r>");
            int iPos3 = xInnerXML.IndexOf("<w:p />");
            int iPos4 = xInnerXML.IndexOf("</w:p>");
            if (((iPos2 > -1) && (iPos2 < iPos)) ||
                ((iPos3 > -1) && (iPos3 < iPos)) ||
                ((iPos4 > -1) && (iPos4 < iPos)))
                return false;

            //child is adjacent
            return true;
        }
    }
}
