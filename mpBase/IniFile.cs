﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;

namespace LMP
{
    /// <summary>
    /// read/writes to an ini file
    /// </summary>

    //GLOG - 3450 - CEH

    public static class IniFile
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
          string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
          string key, string def, StringBuilder retVal,
          int size, string filePath);

        public static void IniWriteValue(string xSection, string xKey, string xValue, string xPath)
        {
            WritePrivateProfileString(xSection, xKey, xValue, xPath);
        }

        public static string IniReadValue(string xSection, string xKey, string xPath)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(xSection, xKey, "", temp, 255, xPath);
            return temp.ToString();
        }
    }
}
