﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMP.MacPac
{
    public static class MacPacImplementation
    {
        private static bool m_bIsAdminMode = false; //GLOG 7898 (dm)

        /// <summary>
        /// returns true iff the current implementation 
        /// of MacPac is toolbar only
        /// </summary>
        public static bool IsToolkit
        {
            get
            {
                //GLOG 7898 (dm) - treat as full implementation when logged in as admin
                if (!m_bIsAdminMode)
                    return LMP.Registry.GetMacPac10Value("RibbonOnly") == "1";
                else
                    return false;
            }
        }
        /// <summary>
        /// returns true iff the current implementation 
        /// of MacPac is the fully functional local version
        /// </summary>
        public static bool IsFullLocal
        {
            //GLOG 8220 (Removed from Development Mode)
            get { return (LMP.Registry.GetMacPac10Value("FullyLocal") == "1"); }
        }
        /// <summary>
        /// returns true iff the current implementation 
        /// of MacPac is the fully functional version with network support
        /// </summary>
        public static bool IsFullWithNetworkSupport
        {
            get { return !IsFullLocal && !IsToolkit; }
        }

        /// <summary>
        /// returns true iff Forte is configured as a server
        /// </summary>
        public static bool IsServer
        {
            get
            {
                return LMP.Registry.GetMacPac10Value("UnattendedMode") == "1";
            }
        }

        /// <summary>
        /// added for GLOG 7898 (dm)
        /// </summary>
        public static bool IsAdminMode
        {
            get { return m_bIsAdminMode; }
            set { m_bIsAdminMode = value; }
        }

		//GLOG : 7998 : ceh
        /// <summary>
        /// returns true if Forte is configured as toolkit in Admin mode
        /// </summary>
        public static bool IsToolsAdministrator 
        {
            get
            {
                return (LMP.Registry.GetMacPac10Value("RibbonOnly") == "1" && IsAdminMode);
            }
        }
    }
}
