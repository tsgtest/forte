using System;
using System.Text;
using System.Collections;

namespace LMP
{
	/// <summary>
	/// Contains functions for accessing delimited strings as an array
	/// </summary>
	public class StringArray
	{
		public const char mpEndOfElement = '�';		// ASCII 222
		public const char mpEndOfRecord = '�';		// ASCII 164
		public const char mpEndOfField = '|';		// ASCII 124
		public const char mpEndOfValue = '�';		// ASCII 166
		public const char mpEndOfSubField = '�';	// ASCII 135
		public const char mpEndOfSubValue = '�';	// ASCII 134
		private char m_xEndOfElementChar = mpEndOfElement;
		private char m_xEndOfRecordChar = mpEndOfRecord;
		private char m_xEndOfFieldChar = mpEndOfField;
		private char m_xEndOfValueChar = mpEndOfValue;
		private char m_xEndOfSubValueChar = mpEndOfSubValue;
		private string m_xTextString = null;
		private bool m_bIsDirty = false;
		private bool m_bAutoExpand = true;
		private ArrayList m_aMatrix = null;

		#region Constructors
		/// <summary>
		/// Public constructor
		/// </summary>
		public StringArray()
		{
			this.TextString = null;
		}
		/// <summary>
		/// Public constructor
		/// </summary>
		/// <param name="xText">Delimited string</param>
		public StringArray(string xText)
		{
			this.TextString = xText;
		}
		#endregion
		
		#region Indexers
		/// <summary>
		/// Returns value based on indices of all 5 dimensions
		/// </summary>
		public string this[int iRecord, int iElement, int iField, int iValue, int iSubValue]
		{
			get
			{
				if (WithinBounds(iElement, iRecord, iField, iValue, iSubValue) == false)
				{
					throw new LMP.Exceptions.NotInCollectionException(
						string.Concat(LMP.Resources.GetLangString("Error_ItemNotInCollection"),
						" (",iElement, ",", iRecord, ",", iField, ",", iValue, ",", iSubValue, ")"));
				}
				ArrayList aRec = (ArrayList) m_aMatrix[iElement];
				ArrayList aFld = (ArrayList) aRec[iRecord];
				ArrayList aVal = (ArrayList) aFld[iField];
				ArrayList aSub = (ArrayList) aVal[iValue];
				return (string) aSub[iSubValue];
			}
			set
			{
				if (WithinBounds(iElement, iRecord, iField, iValue, iSubValue) == false && m_bAutoExpand == false)
				{
					throw new LMP.Exceptions.NotInCollectionException(
						string.Concat(LMP.Resources.GetLangString("Error_ItemNotInCollection"),
						" (",iElement, ",", iRecord, ",", iField, ",", iValue, ",", iSubValue, ")"));
				}
				else
				{
					// Expand array to required number of items if necessary-
					//set specified indices to specified values
					if (iElement >= m_aMatrix.Count) 
						for (int i = m_aMatrix.Count; i<=iElement; i++)
							InsertElement(i);
					ArrayList aRec = (ArrayList) m_aMatrix[iElement];
					if (iRecord >= aRec.Count) 
						for (int i = aRec.Count; i<=iRecord; i++)
							InsertRecord(iElement, i);
					ArrayList aFld = (ArrayList) aRec[iRecord];
					if (iField >= aFld.Count) 
						for (int i = aFld.Count; i <= iField; i++)
							InsertField(iElement, iRecord, i);
					ArrayList aVal = (ArrayList) aFld[iField];
					if (iValue >= aVal.Count)
						for (int i = aVal.Count; i <= iValue; i++)
							InsertValue(iElement, iRecord, iField, i);
					ArrayList aSub = (ArrayList) aVal[iValue];
					if (iSubValue >= aSub.Count) 
						for (int i = aSub.Count; i <= iSubValue; i++)
							InsertSubValue(iElement, iRecord, iField, iValue, i);

					aSub[iSubValue] = value;
					m_bIsDirty = true;
				}
			}
		}
		/// <summary>
		/// Returns value based on Field and Value indices
		/// Index of 0 is assumed for other dimensions
		/// </summary>
		public string this[int iField,int iValue]
		{
			get
			{
				return this[0, 0, iField, iValue, 0];

			}
			set
			{
				this[0, 0, iField, iValue, 0] = value;
			}
		}
		/// <summary>
		/// Returns value based on SubValue index
		/// Index of 0 is assumed for all other dimensions
		/// </summary>
		public string this[int iSubValue]
		{
			get
			{
				return this[0, 0, 0, 0, iSubValue];

			}
			set
			{
				this[0, 0, 0, 0, iSubValue] = value;
			}
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// Get/Set Delimited text string
		/// </summary>
		public string TextString
		{
			get
			{
				if (m_bIsDirty==true)
				{
					BuildString();
				}
				return m_xTextString;
			}

			set
			{
				if (value != m_xTextString)
				{
					//trim any delimiters from end of string -
                    //include only delimiters of units that cannot be empty
					m_xTextString = value.TrimEnd( EndOfElementChar, EndOfRecordChar, EndOfFieldChar  ) ;
					LoadArray();
					m_bIsDirty = false;
				}
			}		
		}
		/// <summary>
		/// Specifies if Array is automatically expanded
		/// to accommodate out-of-bound indices
		/// </summary>
		public bool AutoExpand
		{
			get {return m_bAutoExpand;}
			set {m_bAutoExpand = value;}
		}
		/// <summary>
		/// char to use as end-of-element marker
		/// </summary>
		public char EndOfElementChar
		{
			get {return m_xEndOfElementChar;}
		}
		/// <summary>
		/// char to use as end-of-record marker
		/// </summary>
		public char EndOfRecordChar
		{
			get {return m_xEndOfRecordChar;}
		}
		/// <summary>
		/// char to use as end-of-field marker
		/// </summary>
		public char EndOfFieldChar
		{
			get {return m_xEndOfFieldChar;}
		}
		/// <summary>
		/// char to use as end-of-value marker
		/// </summary>
		public char EndOfValueChar
		{
			get {return m_xEndOfValueChar;}
		}
		/// <summary>
		/// char to use as end-of-subvalue marker
		/// </summary>
		public char EndOfSubValueChar
		{
			get {return m_xEndOfSubValueChar;}
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Return 2-dimensional array containing Fields and Values
		/// for the specified Record
		/// </summary>
		/// <param name="iElement">index of Element</param>
		/// <param name="iRecord">index of Record</param>
		/// <returns>string []</returns>
		public string [,] ToArray(int iElement, int iRecord)
		{
			int iMaxValues = 0;
			int iNumFields = this.FieldCount(iElement, iRecord);

			//cycle through each field, getting the max number of values
			for (int i = 0; i < iNumFields; i++)
				if (this.ValueCount(iElement, iRecord, i) > iMaxValues)
					iMaxValues = this.ValueCount(iElement, iRecord, i);

			//create enough space to hold all values
			string[,] aValues  = new string[iNumFields, iMaxValues];

			//populate array
			for (int i = 0; i < iNumFields; i++)
			{
				int iNumValues = this.ValueCount(iElement, iRecord, i);
				for (int j = 0; j < iNumValues; j++)
					aValues[i, j] = this[iElement, iRecord, i, j, 0];
			}
			return aValues;
		}

		/// <summary>
		/// Return 2-dimensional array containing Fields and Values
		/// (Assumes Element and Record Index of 0)
		/// </summary>
		/// <returns>string []</returns>
		public string [,] ToArray()
		{
			return ToArray(0, 0);
		}
		/// <summary>
		/// Inserts a new delmited Element at the specified position
		/// </summary>
		/// <param name="iIndex">Index of inserted Element</param>
		public void InsertElement(int iIndex)
		{
			if (iIndex > this.ElementCount())
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (",iIndex, ")"));
			}

			m_aMatrix.Insert(iIndex, new ArrayList(0));
			// Initialize empty Record
			InsertRecord(iIndex, 0);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Inserts a new delimited Record at the specified position
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iIndex">Index of inserted Record</param>
		public void InsertRecord(int iElement, int iIndex)
		{
			if (iElement >= this.ElementCount() || iIndex > this.RecordCount(iElement) )
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iIndex, ")"));
			}
			((ArrayList) m_aMatrix[iElement]).Insert(iIndex, new ArrayList(0));
			// Initialize empty Field
			InsertField(iElement, iIndex, 0);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Inserts a new delimited Field at the specified position
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iIndex">Index of inserted Field</param>
		public void InsertField(int iElement, int iRecord, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iIndex > this.FieldCount(iElement, iRecord))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iRecord, ",", iIndex, ")"));
			}
			((ArrayList)((ArrayList) m_aMatrix[iElement])[iRecord]).Insert(iIndex, new ArrayList(0));
			// Initialize empty Value
			InsertValue(iElement, iRecord, iIndex, 0);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Inserts a new delimited Field at the specified position in the first Element and Record 
		/// </summary>
		/// <param name="iIndex">Index of inserted Field</param>
		public void InsertField(int iIndex)
		{
			InsertField(0, 0, iIndex);
		}
		/// <summary>
		/// Inserts a new delimited Field at the end of the first Element and Record
		/// </summary>
		public void InsertField()
		{
			InsertField(0, 0, this.FieldCount());
		}
		/// <summary>
		/// Inserts a new delimited Value at the specified position
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iIndex">Index of inserted Value</param>
		public void InsertValue(int iElement, int iRecord, int iField, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iField >= this.FieldCount(iElement, iRecord) || iIndex > this.ValueCount(iElement, iRecord, iField))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString("Error_ItemNotInCollection"),
					" (", iElement, ",", iRecord, ",", iField, ",", iIndex, ")"));
			}

			((ArrayList)(((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]))[iField]).Insert(iIndex, new ArrayList(0));
			// Initialize empty Subvalue
			InsertSubValue(iElement, iRecord, iField, iIndex, 0);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Inserts a new delimited Value at the specified position within the Field specified in the first Element and Record
		/// </summary>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iIndex">Index of inserted Value</param>
		public void InsertValue(int iField, int iIndex)
		{
			InsertValue(0, 0, iField, iIndex);
		}
		/// <summary>
		/// Inserts a new delmited Subvalue at the specified position
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iValue">Index of parent Value</param>
		/// <param name="iIndex">Index of inserted Subvalue</param>
		public void InsertSubValue(int iElement, int iRecord, int iField, int iValue, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iField >= this.FieldCount(iElement, iRecord) 
				|| iValue > this.ValueCount(iElement, iRecord, iField)
				|| iIndex > this.SubValueCount(iElement, iRecord, iField, iValue))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString("Error_ItemNotInCollection"),
					" (", iElement, ",", iRecord, ",", iField, ",", iValue, ",", iIndex, ")"));
			}

			((ArrayList)((ArrayList)(((ArrayList)(((ArrayList) m_aMatrix[iElement]))
				[iRecord]))[iField])[iValue]).Insert(iIndex, new ArrayList(0));

			m_bIsDirty = true;
		}
		/// <summary>
		/// Inserts a new delimited Subvalue at the specified position within the first Element, Record, Field and Value
		/// </summary>
		/// <param name="iIndex">Index of inserted Subvalue</param>
		public void InsertSubValue(int iIndex)
		{
			InsertSubValue(0, 0, 0, 0, iIndex);
		}

		/// <summary>
		/// Removes the Element at the specified index
		/// </summary>
		/// <param name="iIndex">index of Element to delete</param>
		public void DeleteElement(int iIndex)
		{
			if (iIndex >= this.ElementCount())
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iIndex, ")"));
			}
			m_aMatrix.RemoveAt(iIndex);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Removes the Record at the specified index within the Element
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iIndex">Index of Record to delete</param>
		public void DeleteRecord(int iElement, int iIndex)
		{
			if (iElement >= this.ElementCount() || iIndex >= this.RecordCount(iElement) )
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iIndex, ")"));
			}
			((ArrayList) m_aMatrix[iElement]).RemoveAt(iIndex);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Removes the Field at the specfied index within the Record
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iIndex">Index of Field to delete</param>
		public void DeleteField(int iElement, int iRecord, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iIndex >= this.FieldCount(iElement, iRecord))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iRecord, ",", iIndex, ")"));
			}
			((ArrayList)((ArrayList) m_aMatrix[iElement])[iRecord]).RemoveAt(iIndex);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Removes the Field at the specfied index
		/// (assumes Element and Record index of 0)
		/// </summary>
		/// <param name="iIndex">Index of Field to delete</param>
		public void DeleteField(int iIndex)
		{
			DeleteField(0, 0, iIndex);
		}
		/// <summary>
		/// Deletes Value at the specified index in the Field
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iIndex">Index of Value to delete</param>
		public void DeleteValue(int iElement, int iRecord, int iField, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iField >= this.FieldCount(iElement, iRecord) || iIndex >= this.ValueCount(iElement, iRecord, iField))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iRecord, ",", iField, ",", iIndex, ")"));
			}
			((ArrayList)(((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]))[iField]).RemoveAt(iIndex);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Deletes Value at the specified index in the Field
		/// (assumes Element and Record index of 0)
		/// </summary>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iIndex">Index of Value to delete</param>
		public void DeleteValue(int iField, int iIndex)
		{
			DeleteValue(0, 0, iField, iIndex);
		}
		/// <summary>
		/// Deletes Subvalue at the specified index in the Value
		/// </summary>
		/// <param name="iElement">Index of parent Element</param>
		/// <param name="iRecord">Index of parent Record</param>
		/// <param name="iField">Index of parent Field</param>
		/// <param name="iValue">Index of parent Value</param>
		/// <param name="iIndex">Index of Subvalue to delete</param>
		public void DeleteSubValue(int iElement, int iRecord, int iField, int iValue, int iIndex)
		{
			if (iElement >= this.ElementCount() || iRecord >= this.RecordCount(iElement) 
				|| iField >= this.FieldCount(iElement, iRecord) || iValue > this.ValueCount(iElement, iRecord, iField)
				|| iIndex > this.SubValueCount(iElement, iRecord, iField, iValue))
			{
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (", iElement, ",", iRecord, ",", iField, ",", iValue, ",", iIndex, ")"));
			}
			((ArrayList)((ArrayList)(((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]))[iField])[iValue]).RemoveAt(iIndex);
			m_bIsDirty = true;
		}
		/// <summary>
		/// Deletes Subvalue at the specified index in the Value
		/// (assumes Element, Record, Field and Value index of 0)
		/// </summary>
		/// <param name="iIndex">Index of Subvalue to delete</param>
		public void DeleteSubValue(int iIndex)
		{
			DeleteSubValue(0, 0, 0, 0, iIndex);
		}

		/// <summary>
		/// Count of Elements in delimited string
		/// </summary>
		/// <returns>int</returns>
		public int ElementCount()
		{
			return m_aMatrix.Count;
		}
		/// <summary>
		/// Count of Records in specified Element
		/// </summary>
		/// <param name="iElement">Index of Element</param>
		/// <returns>int</returns>
		public int RecordCount(int iElement)
		{
			return ((ArrayList) m_aMatrix[iElement]).Count;
		}
		public int FieldCount(int iElement, int iRecord)
		{
			return ((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]).Count;
		}
		/// <summary>
		/// Count of Fields (assumes Element 0 and Record 0)
		/// </summary>
		/// <returns>int</returns>
		public int FieldCount()
		{
			return ((ArrayList)(((ArrayList) m_aMatrix[0]))[0]).Count;
		}
		/// <summary>
		/// Count of Values in specified Field
		/// </summary>
		/// <param name="iElement">index of Element</param>
		/// <param name="iRecord">index of Record</param>
		/// <param name="iField">index of Field</param>
		/// <returns>int</returns>
		public int ValueCount(int iElement, int iRecord, int iField)
		{
			return ((ArrayList)(((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]))[iField]).Count;
		}
		/// <summary>
		/// Count of Values in specified Field
		/// (assumes Element 0 and Record 0)
		/// </summary>
		/// <param name="iField">index of Field</param>
		/// <returns>int</returns>
		public int ValueCount(int iField)
		{
			return ((ArrayList) (((ArrayList)(((ArrayList) m_aMatrix[0]))[0]))[iField]).Count;
		}
		/// <summary>
		/// Count of Subvalues in specified Value
		/// </summary>
		/// <param name="iElement">index of Element</param>
		/// <param name="iRecord">index of Record</param>
		/// <param name="iField">index of Field</param>
		/// <param name="iValue">index of Value</param>
		/// <returns>int</returns>
		public int SubValueCount(int iElement, int iRecord, int iField, int iValue)
		{
			return ((ArrayList)((ArrayList) (((ArrayList)(((ArrayList) m_aMatrix[iElement]))[iRecord]))[iField])[iValue]).Count;
		}
		/// <summary>
		/// Count of Subvalues in specified Value
		/// (assumes Element, Record, Field and Value index of 0)
		/// </summary>
		/// <returns>int</returns>
		public int SubValueCount()
		{
			return ((ArrayList)((ArrayList) (((ArrayList)(((ArrayList) m_aMatrix[0]))[0]))[0])[0]).Count;
		}
		/// <summary>
		/// Specifies alernate delimiters to use in parsing and writing the text string
		/// </summary>
		/// <param name="xElement">End-of-Element character</param>
		/// <param name="xRecord">End-of-Record character</param>
		/// <param name="xField">End-of-Field character</param>
		/// <param name="xValue">End-of-Value character</param>
		/// <param name="xSubValue">End-of-Subvalue character</param>
		public void SetDelimiters(char xElement, char xRecord, char xField, char xValue, char xSubValue)
		{
			// make sure all delimiters are different
			if (xElement == xRecord || xElement == xField || xElement == xValue || xElement == xSubValue
				|| xRecord == xField || xRecord == xValue || xRecord == xSubValue || xField == xValue
				|| xField == xSubValue || xValue == xSubValue)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_StringArrayDelimitersNotUnique")," (", xElement, ",", xRecord, ",", xField, ",", xValue, ",", xSubValue, ")"));
			m_xEndOfElementChar = xElement;
			m_xEndOfRecordChar = xRecord;
			m_xEndOfFieldChar = xField;
			m_xEndOfValueChar = xValue;
			m_xEndOfSubValueChar = xSubValue;
		}

		#endregion
		
		#region Internal Methods
		/// <summary>
		/// Cycle through array to build delimited string
		/// </summary>
		internal void BuildString()
		{
			ArrayList aRec;
			ArrayList aFld;
			ArrayList aVal;
			ArrayList aSub;
			if (m_aMatrix.Count == 0) return;
			m_xTextString = null;
			for (int e=0; e < m_aMatrix.Count; e++)
			{
				aRec = (ArrayList) m_aMatrix[e];
				for (int r=0; r < aRec.Count; r++)
				{
					aFld = (ArrayList) aRec[r];
					for (int f=0; f < aFld.Count; f++)
					{
						aVal = (ArrayList) aFld[f];
						for (int v=0; v < aVal.Count; v++)
						{
							aSub = (ArrayList) aVal[v];
							for (int s=0; s < aSub.Count; s++)
							{
								//Append item
								m_xTextString = System.String.Concat(m_xTextString, aSub[s]);

								//Append end-of-subvalue delimiter if not the first or last item
								if ((m_xTextString != "") && (s < aSub.Count - 1))
								{
									m_xTextString = System.String.Concat(m_xTextString, mpEndOfSubValue);
								}
							}
							// Append end-of-value delimiter if not the first or last item
							if ((m_xTextString != "") && (v < aVal.Count -1))
							{
								m_xTextString = System.String.Concat(m_xTextString, mpEndOfValue);
							}
						}
						// Append end-of-field delimiter if not the first or last item
						if ((m_xTextString != "") && (f < aFld.Count - 1))
						{
							m_xTextString = System.String.Concat(m_xTextString, mpEndOfField);
						}
					}
					// Append end-of-record delimiter if not the first or last item
					if ((m_xTextString != "") && (r < aRec.Count - 1))
					{
						m_xTextString = System.String.Concat(m_xTextString, mpEndOfRecord);
					}
				}
				// Append end-of-element delimiter if not the first or last item
				if ((m_xTextString != "") && (e < m_aMatrix.Count - 1))
				{
					m_xTextString = System.String.Concat(m_xTextString, mpEndOfElement);
				}
			}
			m_bIsDirty = false;
		}

		/// <summary>
		/// Parse delimited string to fill Array
		/// </summary>
		internal void LoadArray()
		{
			if (m_xTextString == null) return;
			ArrayList aRec;
			ArrayList aFld;
			ArrayList aVal;
			ArrayList aSub;
			m_aMatrix = new ArrayList(0);
			foreach (string xElement in m_xTextString.Split(EndOfElementChar))
			{
				aRec = new ArrayList(0);
				foreach (string xRecord in xElement.Split(EndOfRecordChar))
				{
					aFld = new ArrayList(0);
					foreach (string xField in xRecord.Split(EndOfFieldChar))
					{
						aVal = new ArrayList(0);
						foreach (string xValue in xField.Split(EndOfValueChar))
						{
							aSub = new ArrayList(0);
							foreach (string xSubValue in xValue.Split(EndOfSubValueChar))
							{
								aSub.Add(xSubValue);
							}
							aVal.Add(aSub);
						}
						aFld.Add(aVal);
					}
					aRec.Add(aFld);
				}
				m_aMatrix.Add(aRec);
			}
		}

		/// <summary>
		/// Are specified indices all within capacity of current ArrayLists?
		/// </summary>
		/// <param name="iElement">index of Element</param>
		/// <param name="iRecord">index of Record</param>
		/// <param name="iField">index of Field</param>
		/// <param name="iValue">index of Value</param>
		/// <param name="iSubValue">index of SubValue</param>
		/// <returns></returns>
		internal bool WithinBounds(int iElement, int iRecord, int iField, int iValue, int iSubValue)
		{
			ArrayList aRec;
			ArrayList aFld;
			ArrayList aVal;
			ArrayList aSub;
			if (iElement < m_aMatrix.Count) 
			{
				aRec = (ArrayList) m_aMatrix[iElement];
				if (iRecord < aRec.Count)
				{
					aFld = (ArrayList) aRec[iRecord];
					if (iField < aFld.Count)
					{
						aVal = (ArrayList) aFld[iField];
						if (iValue < aVal.Count)
						{
							aSub = (ArrayList) aVal[iValue];
							if (iSubValue <= aSub.Count)
								return true;
						}
					}
				}
			}
			return false;
		}
		#endregion

	}

}
