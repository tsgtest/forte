﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Data.OleDb;
using System.Net.Sockets;

namespace LMP
{
    public class Network
    {
        /// <summary>
        /// retrieves the IP address and port of the
        /// specified connection
        /// </summary>
        /// <param name="xAddress"></param>
        /// <param name="iPort"></param>
        public static void GetConnectionParameters(OleDbConnection oCnn, out string xAddress, out int iPort)
        {
            Trace.WriteNameValuePairs("oCnn.DataSource", oCnn.DataSource);

            //GLOG 4250: Port should be separated from Server Name by comma for OLEDB connection string
            string[] axConnParams = oCnn.DataSource.Split(',');
            xAddress = axConnParams[0];

            if (axConnParams.Length > 1)
            {
                // Get the specified port.
                iPort = int.Parse(axConnParams[1]);
            }
            else
            {
                iPort = 1433; // This is the default port for sql server.
            }
        }

        /// <summary>
        /// GLOG 2728 : JAB
        /// Check if the specified server is available by simply connecting to it's listening socket.
        /// </summary>
        /// <param name="xHost"></param>
        /// <param name="iPort"></param>
        /// <param name="iTimeoutInMilliSec"></param>
        /// <returns></returns>
        public static bool IsServerConnectionAvailable(string xHost, int iPort, int iTimeoutInMilliSec)
        {
            IPAddress[] aoHostIP;
            IPAddress oHostIP;

            Trace.WriteNameValuePairs("xHost", xHost,
                "iPort", iPort, "iTimeoutInMilliSec", iTimeoutInMilliSec);

            //GLOG 4250: If Host includes an instance name, use just the Server name portion
            if (xHost.Contains("\\"))
            {
                xHost = xHost.Substring(0, xHost.IndexOf("\\"));
            }
            // Determine if the data source is an IP or a hostname by creating an
            // IPAddress object.
            if (xHost.ToUpper() == Dns.GetHostName().ToUpper())
                //this machine is the host - it's available
                return true;

            try
            {
                // If parsing the host as an IP does not yield an exception, we have an IP.
                oHostIP = IPAddress.Parse(xHost);
            }
            catch
            {
                // The attempt to parse the host as an IP failed indicating that 
                // the host is not an IP but a hostname.
                aoHostIP = Dns.GetHostAddresses(xHost);

                if (aoHostIP.Length > 0)
                {
                    // Use the first resolved ip address.
                    oHostIP = aoHostIP[0];
                }
                else
                {
                    //We could not resolve the host's ip address -
                    //we can't determine if the connection is available,
                    //so let it go through - the timeout will catch it
                    return true;
                }
            }

            return IsServerConnectionAvailable(oHostIP, iPort, iTimeoutInMilliSec);
        }

        public static bool IsServerConnectionAvailable(IPAddress oHostAddress, int iPort, int iTimeoutInMilliSec)
        {
            Trace.WriteNameValuePairs("oHostAddress.ToString()", oHostAddress.ToString(), 
                "iPort", iPort, "iTimeoutInMilliSec", iTimeoutInMilliSec);

            if (oHostAddress.ToString() == "127.0.0.1")
                //this is a connection to the local machine
                return true;

            System.Net.IPEndPoint oHostEndPoint = new System.Net.IPEndPoint(oHostAddress, iPort);
            //GLOG 5515: IPAddress may be IPv4 or IPv6 - use appropriate AddressFamily when creating Socket
            Socket oSocket = new Socket(oHostAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                // Set the socket to non-blocking so that the connect attempt returns immediately.
                oSocket.Blocking = false;

                // Attempt to connect;
                oSocket.Connect(oHostEndPoint);

                // We have successfully connected which indicates that the server is available.
                oSocket.Close();
                return true;
            }
            catch (System.Net.Sockets.SocketException oSocketException)
            {
                // The attempt to connect caused an exception.
                // We expect that the exception error indicates that we would block.
                if (oSocketException.ErrorCode == 10035)
                {
                    if (iTimeoutInMilliSec > 0)
                    {
                        int iTimeoutInMicroSecs = iTimeoutInMilliSec * 1000;

                        // The attempt to connect would have blocked, so we'll attempt to connect
                        // timing out after the amount of time specified.
                        if (oSocket.Poll(iTimeoutInMicroSecs, System.Net.Sockets.SelectMode.SelectWrite))
                        {
                            // We've timed out.
                            oSocket.Close();
                            return true;
                        }
                    }

                    oSocket.Close();

                    // Either the specified timeout was <= 0 or the specified timeout expired
                    // and no connection was established.
                    return false;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

            finally
            {
                oSocket.Close();
            }

            return false;
        }
    }
}
