﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace LMP
{
    public class Culture
    {
        //GLOG 8203:  Make sure default U.S. English formats are used, even if these have been customized
        private static CultureInfo m_oUSEnCulture = new CultureInfo(1033, false);

        public static CultureInfo USEnglishCulture
        {
            get
            {
                return m_oUSEnCulture;
            }
        }
    }
}
