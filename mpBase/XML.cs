using System;
using System.IO;
using XML= System.Xml;

namespace LMP
{
	/// <summary>
	/// contains methods to read/write to/from XML Files.
	/// </summary>
	public class XMLFile
	{
		string m_xFullName;
		XML.XmlDocument m_oXML;

		/// <summary>
		/// public constructor
		/// </summary>
		/// <param name="FullName">full name of the XMLFile to load</param>
		public XMLFile(string FullName)
		{
			m_xFullName = FullName;
		}

		/// <summary>
		/// gets/sets the full name of the XML File
		/// </summary>
		internal string FullName
		{
			get{return m_xFullName;}
			set{m_xFullName = value;}
		}

		/// <summary>
		/// gets value of element referred to by specified path (XPath) as a string
		/// </summary>
		/// <param name="xPath">XML XPath string</param>
		public string GetValue(string XPath)
		{
			XML.XmlNode oNode;
			
			LMP.Trace.WriteNameValuePairs("XPath", XPath);

			oNode = GetNode(XPath);

			if(oNode != null)
				return oNode.InnerText;
			else
				return null;
		}

		/// <summary>
		/// gets value of element referred to by specified path (XPath) as a boolean
		/// </summary>
		/// <param name="xPath">XML XPath string</param>
		public bool GetBooleanValue(string XPath)
		{
			string xValue = GetValue(XPath);
			if (xValue == null)
				return false;
			else
			{
				try
				{
					return System.Convert.ToBoolean(xValue);
				}
				catch(System.Exception e) 
				{
					throw new LMP.Exceptions.TypeMismatchException(
						LMP.Resources.GetLangString("Error_CouldNotConvertXMLElementToBoolean") + XPath,
						e.InnerException);
				}
			}
		}

		/// <summary>
		/// gets value of element referred to by specified path (XPath) as an integer
		/// </summary>
		/// <param name="xPath">XML XPath string</param>
		public int GetIntegerValue(string XPath)
		{
			string xValue = GetValue(XPath);
			if (xValue == null)
				return 0;
			else
			{
				try
				{
					return System.Convert.ToInt32(xValue);
				}
				catch(System.Exception e) 
				{
					throw new LMP.Exceptions.TypeMismatchException(
						LMP.Resources.GetLangString("Error_CouldNotConvertXMLElementToInteger") + XPath,
						e.InnerException);
				}
			}
		}

		/// <summary>
		/// gets value of element referred to by specified path (XPath) as a datetime
		/// </summary>
		/// <param name="xPath">XML XPath string</param>
		public DateTime GetDateTimeValue(string XPath)
		{
			string xValue = GetValue(XPath);
			if (xValue == null)
				return new DateTime(1,1,1);
			else
			{
				try
				{
					return System.Convert.ToDateTime(xValue);
				}
				catch(System.Exception e) 
				{
					throw new LMP.Exceptions.TypeMismatchException(
						LMP.Resources.GetLangString("Error_CouldNotConvertXMLElementToDateTime") + XPath,
						e.InnerException);
				}
			}
		}

		/// <summary>
		/// gets value of element referred to by specified path (XPath) as a float
		/// </summary>
		/// <param name="xPath">XML XPath string</param>
		public float GetFloatValue(string XPath)
		{
			string xValue = GetValue(XPath);
			if (xValue == null)
				return 0;
			else
			{
				try
				{
					return System.Convert.ToSingle(xValue);
				}
				catch(System.Exception e) 
				{
					throw new LMP.Exceptions.TypeMismatchException(
						LMP.Resources.GetLangString("Error_CouldNotConvertXMLElementToFloat") + XPath,
						e.InnerException);
				}
			}
		}

		/// <summary>
		/// sets value of element referred to by specified path (XPath)
		/// </summary>
		/// <param name="xPath"></param>
		public void SetValue(string XPath, string Value)
		{
			Trace.WriteNameValuePairs("XPath", XPath, "Value", Value);
			XML.XmlNode oNode = GetNode(XPath);
			oNode.InnerText = Value;
		}

		/// <summary>
		/// returns the xml node referred to by the specified XPath
		/// </summary>
		/// <param name="xXPath"></param>
		/// <returns></returns>
		private XML.XmlNode GetNode(string xXPath)
		{
			XML.XmlNode oNode=null;
			OpenIfNecessary();
			
			try
			{
				LMP.Trace.WriteNameValuePairs("xXPath",xXPath);

				//get node referred to by path
				oNode = m_oXML.DocumentElement.SelectSingleNode(xXPath);
			}
			catch{}

			return oNode;
		}
		/// <summary>
		/// loads XML in specified file if not already loaded
		/// </summary>
		private void OpenIfNecessary()
		{
			
			if(m_oXML ==null)
			//xml has not yet been loaded
			{
				try
				{
					LMP.Trace.WriteNameValuePairs("m_xFullName",m_xFullName);

					//load
					m_oXML= new System.Xml.XmlDocument();
					m_oXML.Load(m_xFullName);
				}

				catch(System.Exception e)
				{
					//could not load - alert
					throw new LMP.Exceptions.FileAccessException(
						LMP.Resources.GetLangString("Error_CouldNotLoadXML") + m_xFullName,e);
				}
			}
		}
	}
}
