using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using Microsoft.InformationBridge.Framework.Interfaces;
using Word=Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Text.RegularExpressions;

namespace LMP.MP10.Controls
{
	/// <summary>
	/// Summary description for DocumentRegion.
	/// </summary>
	public class DocumentRegion : System.Windows.Forms.UserControl, IRegion
	{
		private const string IMAGE_DIR = @"D:\work\macpac\10\images\";
		private System.Xml.XmlNode m_oNode;
		private IRegionFrameProxy m_oFrameProxy;
		private IVisualStyles m_oVisualStyle;
		private FrameType m_oFrameType;
		private Word.ApplicationClass wordApplication;
		private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
		private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
		private System.Windows.Forms.Panel panel2;
		private LMP.MP10.Controls.MPTree ultraTree1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label2;
		private Infragistics.Win.Misc.UltraButton ultraButton1;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFind;
		private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSource1;

		private Control m_oCurEditCtl;
		private Infragistics.Win.UltraWinTree.UltraTreeNode m_oCurNode;
		private Control m_oCurMacroFieldsEditCtl;
		private Infragistics.Win.UltraWinTree.UltraTreeNode m_oCurMacroFieldsNode;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenu mnuSegment;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel4;
		private Infragistics.Win.Misc.UltraButton ultraButton2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.ContextMenu mnuDocument;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.ContextMenu mnuTOC;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ContextMenu mnuMacPac;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem menuItem16;
		private System.Windows.Forms.MenuItem menuItem17;
		private System.Windows.Forms.MenuItem menuItem18;
		private System.Windows.Forms.MenuItem menuItem19;
		private System.Windows.Forms.MenuItem menuItem20;
		private System.Windows.Forms.ContextMenu mnuContent;
		private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl2;
		private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPreview;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Label label1;
		private Infragistics.Win.UltraWinGrid.UltraCombo cmbVariableSets;
		private LMP.MP10.Controls.MPTree ultraTree2;
		private System.Windows.Forms.Label label3;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
		private System.Windows.Forms.MenuItem menuItem24;
		private System.Windows.Forms.MenuItem menuItem25;
		private Infragistics.Win.Misc.UltraDropDownButton dbtnInsert;
		private Infragistics.Win.Misc.UltraPopupControlContainer ultraPopupControlContainer1;
		private System.Windows.Forms.MenuItem mnuEditVariables;
		private System.Windows.Forms.MenuItem menuItem27;
		private System.Windows.Forms.MenuItem menuItem26;
		private LMP.MP10.Controls.MPTree utrEditVariables;
		private Infragistics.Win.Misc.UltraButton btnMappings;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlMappings;
		private Infragistics.Win.Misc.UltraButton btnExecutionCondition;
		private Infragistics.Win.Misc.UltraButton btnDefaultValue;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlExecutionCondition;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlDelimiterReplacement;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlXMlTag;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlDefaultValue;
		private Infragistics.Win.UltraWinGrid.UltraCombo ctlControlType;
		private Infragistics.Win.UltraWinGrid.UltraCombo ctlVisibleInTree;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlUnderlineLength;
		private Infragistics.Win.Misc.UltraButton btnExpression;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlExpression;
		private Infragistics.Win.UltraWinGrid.UltraCombo ctlDeleteScope;
		private System.Windows.Forms.ContextMenu mnuVariablesEdit;
		private System.Windows.Forms.MenuItem mitemCreateVariable;
		private System.Windows.Forms.MenuItem mitemDeleteVariable;
		private System.Windows.Forms.MenuItem mitemRenameVariable;
		private System.Windows.Forms.MenuItem menuItem29;
		private System.Windows.Forms.MenuItem menuItem30;
		private System.Windows.Forms.MenuItem menuItem31;
		private System.Windows.Forms.MenuItem menuItem32;
		private System.Windows.Forms.MenuItem mitemCreateAction;
		private System.Windows.Forms.MenuItem menuItem33;
		private System.Windows.Forms.MenuItem menuItem34;
		private System.Windows.Forms.MenuItem menuItem35;
		private System.Windows.Forms.MenuItem menuItem36;
		private System.Windows.Forms.MenuItem menuItem37;
		private System.Windows.Forms.MenuItem menuItem38;
		private System.Windows.Forms.MenuItem menuItem39;
		private System.Windows.Forms.MenuItem menuItem40;
		private System.Windows.Forms.MenuItem menuItem41;
		private System.Windows.Forms.MenuItem menuItem42;
		private System.Windows.Forms.MenuItem menuItem43;
		private System.Windows.Forms.MenuItem menuItem44;
		private System.Windows.Forms.MenuItem menuItem45;
		private System.Windows.Forms.MenuItem mitemDeleteAction;
		private System.Windows.Forms.MenuItem menuItem47;
		private System.Windows.Forms.MenuItem menuItem48;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlAssociatedMVar;
		private Infragistics.Win.Misc.UltraButton btnAssociatedMVar;
		private Infragistics.Win.Misc.UltraButton btnValidateExecutionCondition;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlValidateExecutionCondition;
		private Infragistics.Win.UltraWinGrid.UltraCombo ctlValidateTargetVariable;
		private Infragistics.Win.Misc.UltraButton btnValidateValidationCondition;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ctlValidateValidationCondition;
		private System.Windows.Forms.MenuItem mitemSetProperty;
		private System.Windows.Forms.MenuItem menuItem28;
		private System.Windows.Forms.MenuItem menuItem46;
		private System.Windows.Forms.MenuItem menuItem49;
		private System.Windows.Forms.MenuItem menuItem50;
		private System.Windows.Forms.MenuItem menuItem51;
		private System.Windows.Forms.MenuItem menuItem52;
		private System.Windows.Forms.MenuItem menuItem21;
		private Infragistics.Win.UltraWinTree.UltraTree ultraTree3;
		private System.Windows.Forms.MenuItem menuItem22;
		private System.ComponentModel.IContainer components;

		public DocumentRegion()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(DocumentRegion));
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTree.UltraTreeColumnSet ultraTreeColumnSet1 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode1 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode2 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode3 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode4 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode5 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode6 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode7 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode8 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode9 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode10 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode11 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode12 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode13 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode14 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode15 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode16 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode17 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode18 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode19 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode20 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode21 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode22 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode23 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode24 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode25 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode26 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode27 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode28 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode29 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode30 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode31 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode32 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode33 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode34 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode35 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode36 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode37 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode38 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode39 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode40 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode41 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode42 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode43 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode44 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode45 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode46 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode47 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode48 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode49 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode50 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode51 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode52 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode53 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTree.UltraTreeColumnSet ultraTreeColumnSet2 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode54 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode55 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode56 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTree.UltraTreeColumnSet ultraTreeColumnSet3 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode57 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode58 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode59 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode60 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override3 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode61 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override4 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode62 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode63 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode64 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override5 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode65 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override6 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode66 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override7 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode67 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override8 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode68 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override9 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode69 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override10 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode70 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override11 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.Override _override12 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode71 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode72 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode73 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode74 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode75 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode76 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override13 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode77 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode78 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode79 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override14 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode80 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override15 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode81 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override16 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode82 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode83 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode84 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override17 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode85 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override18 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode86 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override19 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.Override _override20 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.Override _override21 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode87 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override22 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode88 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override23 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode89 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override24 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode90 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override25 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.Override _override26 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode91 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode92 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode93 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode94 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode95 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode96 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode97 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode98 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode99 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode100 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode101 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode102 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode103 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode104 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode105 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode106 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode107 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode108 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode109 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode110 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode111 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode112 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode113 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode114 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode115 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override27 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.UltraWinTree.UltraTreeColumnSet ultraTreeColumnSet4 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode116 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode117 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode118 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode119 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode120 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode121 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode122 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode123 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode124 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode125 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode126 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode127 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode128 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode129 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode130 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode131 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode132 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode133 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode134 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode135 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode136 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode137 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode138 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode139 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode140 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode141 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode142 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.UltraTreeNode ultraTreeNode143 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
			Infragistics.Win.UltraWinTree.Override _override28 = new Infragistics.Win.UltraWinTree.Override();
			Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinDataSource.UltraDataBand ultraDataBand1 = new Infragistics.Win.UltraWinDataSource.UltraDataBand("Name");
			Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn1 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Value");
			Infragistics.Win.UltraWinDataSource.UltraDataBand ultraDataBand2 = new Infragistics.Win.UltraWinDataSource.UltraDataBand("Value");
			Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn2 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Column 0");
			this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.txtPreview = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.txtFind = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
			this.ultraTree1 = new LMP.MP10.Controls.MPTree();
			this.ultraTree3 = new Infragistics.Win.UltraWinTree.UltraTree();
			this.dbtnInsert = new Infragistics.Win.Misc.UltraDropDownButton();
			this.ultraPopupControlContainer1 = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
			this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel3 = new System.Windows.Forms.Panel();
			this.ultraTabControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
			this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
			this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.btnValidateValidationCondition = new Infragistics.Win.Misc.UltraButton();
			this.ctlValidateValidationCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlValidateTargetVariable = new Infragistics.Win.UltraWinGrid.UltraCombo();
			this.btnValidateExecutionCondition = new Infragistics.Win.Misc.UltraButton();
			this.ctlValidateExecutionCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnAssociatedMVar = new Infragistics.Win.Misc.UltraButton();
			this.ctlAssociatedMVar = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnMappings = new Infragistics.Win.Misc.UltraButton();
			this.ctlMappings = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnExecutionCondition = new Infragistics.Win.Misc.UltraButton();
			this.btnDefaultValue = new Infragistics.Win.Misc.UltraButton();
			this.ctlExecutionCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnExpression = new Infragistics.Win.Misc.UltraButton();
			this.ctlExpression = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlDeleteScope = new Infragistics.Win.UltraWinGrid.UltraCombo();
			this.ctlUnderlineLength = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlDelimiterReplacement = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlXMlTag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlDefaultValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.ctlControlType = new Infragistics.Win.UltraWinGrid.UltraCombo();
			this.ctlVisibleInTree = new Infragistics.Win.UltraWinGrid.UltraCombo();
			this.label1 = new System.Windows.Forms.Label();
			this.mnuDocument = new System.Windows.Forms.ContextMenu();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.utrEditVariables = new LMP.MP10.Controls.MPTree();
			this.ultraTree2 = new LMP.MP10.Controls.MPTree();
			this.cmbVariableSets = new Infragistics.Win.UltraWinGrid.UltraCombo();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.panel5 = new System.Windows.Forms.Panel();
			this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.label3 = new System.Windows.Forms.Label();
			this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
			this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
			this.ultraDataSource1 = new Infragistics.Win.UltraWinDataSource.UltraDataSource();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.mnuSegment = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem19 = new System.Windows.Forms.MenuItem();
			this.menuItem25 = new System.Windows.Forms.MenuItem();
			this.menuItem20 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.mnuTOC = new System.Windows.Forms.ContextMenu();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.mnuMacPac = new System.Windows.Forms.ContextMenu();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuItem24 = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.menuItem27 = new System.Windows.Forms.MenuItem();
			this.menuItem26 = new System.Windows.Forms.MenuItem();
			this.mnuEditVariables = new System.Windows.Forms.MenuItem();
			this.menuItem15 = new System.Windows.Forms.MenuItem();
			this.menuItem16 = new System.Windows.Forms.MenuItem();
			this.menuItem17 = new System.Windows.Forms.MenuItem();
			this.menuItem18 = new System.Windows.Forms.MenuItem();
			this.mnuContent = new System.Windows.Forms.ContextMenu();
			this.menuItem22 = new System.Windows.Forms.MenuItem();
			this.menuItem49 = new System.Windows.Forms.MenuItem();
			this.menuItem50 = new System.Windows.Forms.MenuItem();
			this.menuItem51 = new System.Windows.Forms.MenuItem();
			this.menuItem52 = new System.Windows.Forms.MenuItem();
			this.menuItem21 = new System.Windows.Forms.MenuItem();
			this.mnuVariablesEdit = new System.Windows.Forms.ContextMenu();
			this.mitemCreateVariable = new System.Windows.Forms.MenuItem();
			this.mitemDeleteVariable = new System.Windows.Forms.MenuItem();
			this.mitemRenameVariable = new System.Windows.Forms.MenuItem();
			this.mitemCreateAction = new System.Windows.Forms.MenuItem();
			this.menuItem29 = new System.Windows.Forms.MenuItem();
			this.menuItem30 = new System.Windows.Forms.MenuItem();
			this.menuItem41 = new System.Windows.Forms.MenuItem();
			this.menuItem42 = new System.Windows.Forms.MenuItem();
			this.menuItem44 = new System.Windows.Forms.MenuItem();
			this.menuItem37 = new System.Windows.Forms.MenuItem();
			this.menuItem47 = new System.Windows.Forms.MenuItem();
			this.menuItem31 = new System.Windows.Forms.MenuItem();
			this.menuItem32 = new System.Windows.Forms.MenuItem();
			this.menuItem33 = new System.Windows.Forms.MenuItem();
			this.menuItem34 = new System.Windows.Forms.MenuItem();
			this.menuItem35 = new System.Windows.Forms.MenuItem();
			this.menuItem48 = new System.Windows.Forms.MenuItem();
			this.menuItem36 = new System.Windows.Forms.MenuItem();
			this.menuItem39 = new System.Windows.Forms.MenuItem();
			this.menuItem40 = new System.Windows.Forms.MenuItem();
			this.menuItem38 = new System.Windows.Forms.MenuItem();
			this.menuItem43 = new System.Windows.Forms.MenuItem();
			this.menuItem45 = new System.Windows.Forms.MenuItem();
			this.mitemDeleteAction = new System.Windows.Forms.MenuItem();
			this.mitemSetProperty = new System.Windows.Forms.MenuItem();
			this.menuItem28 = new System.Windows.Forms.MenuItem();
			this.menuItem46 = new System.Windows.Forms.MenuItem();
			this.ultraTabPageControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPreview)).BeginInit();
			this.ultraTabPageControl5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
			this.ultraTabPageControl1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtFind)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree3)).BeginInit();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).BeginInit();
			this.ultraTabControl2.SuspendLayout();
			this.ultraTabPageControl2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateValidationCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateTargetVariable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateExecutionCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlAssociatedMVar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlMappings)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlExecutionCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlExpression)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDeleteScope)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlUnderlineLength)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDelimiterReplacement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlXMlTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDefaultValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlControlType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlVisibleInTree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.utrEditVariables)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbVariableSets)).BeginInit();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
			this.ultraTabControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).BeginInit();
			this.SuspendLayout();
			// 
			// ultraTabPageControl3
			// 
			this.ultraTabPageControl3.Controls.Add(this.txtPreview);
			this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabPageControl3.Name = "ultraTabPageControl3";
			this.ultraTabPageControl3.Size = new System.Drawing.Size(318, 149);
			// 
			// txtPreview
			// 
			this.txtPreview.AllowDrop = true;
			this.txtPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtPreview.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.txtPreview.Location = new System.Drawing.Point(1, 2);
			this.txtPreview.Multiline = true;
			this.txtPreview.Name = "txtPreview";
			this.txtPreview.Size = new System.Drawing.Size(315, 153);
			this.txtPreview.TabIndex = 20;
			// 
			// ultraTabPageControl5
			// 
			this.ultraTabPageControl5.Controls.Add(this.ultraTextEditor1);
			this.ultraTabPageControl5.Location = new System.Drawing.Point(0, 20);
			this.ultraTabPageControl5.Name = "ultraTabPageControl5";
			this.ultraTabPageControl5.Size = new System.Drawing.Size(318, 149);
			// 
			// ultraTextEditor1
			// 
			this.ultraTextEditor1.AllowDrop = true;
			this.ultraTextEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ultraTextEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.ultraTextEditor1.Location = new System.Drawing.Point(1, 2);
			this.ultraTextEditor1.Multiline = true;
			this.ultraTextEditor1.Name = "ultraTextEditor1";
			this.ultraTextEditor1.Size = new System.Drawing.Size(321, 153);
			this.ultraTextEditor1.TabIndex = 22;
			// 
			// ultraTabPageControl1
			// 
			this.ultraTabPageControl1.Controls.Add(this.panel2);
			this.ultraTabPageControl1.Location = new System.Drawing.Point(0, 22);
			this.ultraTabPageControl1.Name = "ultraTabPageControl1";
			this.ultraTabPageControl1.Size = new System.Drawing.Size(322, 665);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.splitter1);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(322, 665);
			this.panel2.TabIndex = 1;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel4.Controls.Add(this.label2);
			this.panel4.Controls.Add(this.txtFind);
			this.panel4.Controls.Add(this.ultraButton1);
			this.panel4.Controls.Add(this.ultraTree1);
			this.panel4.Controls.Add(this.ultraTree3);
			this.panel4.Controls.Add(this.dbtnInsert);
			this.panel4.Controls.Add(this.ultraButton2);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(322, 483);
			this.panel4.TabIndex = 19;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 11);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 15);
			this.label2.TabIndex = 13;
			this.label2.Text = "Find:";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// txtFind
			// 
			this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFind.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.txtFind.Location = new System.Drawing.Point(43, 8);
			this.txtFind.Name = "txtFind";
			this.txtFind.Size = new System.Drawing.Size(253, 19);
			this.txtFind.TabIndex = 12;
			this.txtFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownHandler);
			// 
			// ultraButton1
			// 
			this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
			appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
			this.ultraButton1.Appearance = appearance1;
			this.ultraButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.PopupBorderless;
			this.ultraButton1.ImageSize = new System.Drawing.Size(15, 13);
			this.ultraButton1.Location = new System.Drawing.Point(295, 6);
			this.ultraButton1.Name = "ultraButton1";
			this.ultraButton1.Size = new System.Drawing.Size(24, 23);
			this.ultraButton1.TabIndex = 11;
			this.ultraButton1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownHandler);
			// 
			// ultraTree1
			// 
			this.ultraTree1.AllowDrop = true;
			this.ultraTree1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			appearance2.BackColor = System.Drawing.Color.White;
			appearance2.BorderColor = System.Drawing.Color.Transparent;
			this.ultraTree1.Appearance = appearance2;
			this.ultraTree1.AutoDragExpandDelay = 1000;
			this.ultraTree1.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.ultraTree1.ColumnSettings.RootColumnSet = ultraTreeColumnSet1;
			this.ultraTree1.FullRowSelect = true;
			this.ultraTree1.HideSelection = false;
			this.ultraTree1.Location = new System.Drawing.Point(4, 38);
			this.ultraTree1.Name = "ultraTree1";
			ultraTreeNode1.LeftImages.Add(((object)(resources.GetObject("resource"))));
			ultraTreeNode2.LeftImages.Add(((object)(resources.GetObject("resource1"))));
			ultraTreeNode3.LeftImages.Add(((object)(resources.GetObject("resource2"))));
			ultraTreeNode3.Text = "English Letter";
			ultraTreeNode4.LeftImages.Add(((object)(resources.GetObject("resource3"))));
			ultraTreeNode4.Text = "French Letter";
			ultraTreeNode5.LeftImages.Add(((object)(resources.GetObject("resource4"))));
			ultraTreeNode5.Text = "German Letter";
			ultraTreeNode6.LeftImages.Add(((object)(resources.GetObject("resource5"))));
			ultraTreeNode6.Text = "Spanish Letter";
			ultraTreeNode2.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								ultraTreeNode3,
																								ultraTreeNode4,
																								ultraTreeNode5,
																								ultraTreeNode6});
			ultraTreeNode2.Text = "Letters";
			ultraTreeNode1.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								ultraTreeNode2});
			ultraTreeNode1.Text = "Correspondence";
			ultraTreeNode7.LeftImages.Add(((object)(resources.GetObject("resource6"))));
			ultraTreeNode8.LeftImages.Add(((object)(resources.GetObject("resource7"))));
			ultraTreeNode10.Text = "Alabama";
			ultraTreeNode11.Text = "Alaska";
			ultraTreeNode14.Text = "San Francisco";
			ultraTreeNode15.Text = "Marin";
			ultraTreeNode16.Text = "Sonoma";
			ultraTreeNode17.Text = "Yuba";
			ultraTreeNode13.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode14,
																								 ultraTreeNode15,
																								 ultraTreeNode16,
																								 ultraTreeNode17});
			ultraTreeNode13.Text = "Superior";
			ultraTreeNode18.Text = "Supreme";
			ultraTreeNode19.Text = "Appeals";
			ultraTreeNode12.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode13,
																								 ultraTreeNode18,
																								 ultraTreeNode19});
			ultraTreeNode12.Text = "California";
			ultraTreeNode21.Text = "Civil";
			ultraTreeNode22.Text = "Supreme";
			ultraTreeNode23.Text = "Family";
			ultraTreeNode20.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode21,
																								 ultraTreeNode22,
																								 ultraTreeNode23});
			ultraTreeNode20.Text = "New York";
			ultraTreeNode24.Text = "Illinois";
			ultraTreeNode25.Text = "Texas";
			ultraTreeNode26.Text = "Florida";
			ultraTreeNode27.Text = "Federal";
			ultraTreeNode9.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								ultraTreeNode10,
																								ultraTreeNode11,
																								ultraTreeNode12,
																								ultraTreeNode20,
																								ultraTreeNode24,
																								ultraTreeNode25,
																								ultraTreeNode26,
																								ultraTreeNode27});
			ultraTreeNode9.Text = "United States";
			ultraTreeNode28.Text = "United Kingdom";
			ultraTreeNode29.Text = "Germany";
			ultraTreeNode30.Text = "France";
			ultraTreeNode31.Text = "China";
			ultraTreeNode32.Text = "Japan";
			ultraTreeNode8.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								ultraTreeNode9,
																								ultraTreeNode28,
																								ultraTreeNode29,
																								ultraTreeNode30,
																								ultraTreeNode31,
																								ultraTreeNode32});
			ultraTreeNode8.Text = "Pleadings";
			ultraTreeNode33.LeftImages.Add(((object)(resources.GetObject("resource8"))));
			ultraTreeNode33.Text = "Service Documents";
			ultraTreeNode34.LeftImages.Add(((object)(resources.GetObject("resource9"))));
			ultraTreeNode34.Text = "Notary Documents";
			ultraTreeNode7.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								ultraTreeNode8,
																								ultraTreeNode33,
																								ultraTreeNode34});
			ultraTreeNode7.Text = "Litigation";
			ultraTreeNode35.LeftImages.Add(((object)(resources.GetObject("resource10"))));
			ultraTreeNode36.LeftImages.Add(((object)(resources.GetObject("resource11"))));
			ultraTreeNode37.LeftImages.Add(((object)(resources.GetObject("resource12"))));
			ultraTreeNode37.Text = "Articles";
			ultraTreeNode36.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode37});
			ultraTreeNode36.Text = "Snippets";
			ultraTreeNode35.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode36});
			_override1.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
			ultraTreeNode35.Override = _override1;
			ultraTreeNode35.Text = "Personal Content";
			ultraTreeNode38.LeftImages.Add(((object)(resources.GetObject("resource13"))));
			ultraTreeNode38.Text = "Transactions";
			ultraTreeNode39.LeftImages.Add(((object)(resources.GetObject("resource14"))));
			ultraTreeNode40.LeftImages.Add(((object)(resources.GetObject("resource15"))));
			ultraTreeNode40.Text = "San Francisco Superior Pleading";
			ultraTreeNode41.LeftImages.Add(((object)(resources.GetObject("resource16"))));
			ultraTreeNode41.Text = "Doug Miller Letter";
			ultraTreeNode42.LeftImages.Add(((object)(resources.GetObject("resource17"))));
			ultraTreeNode42.Text = "Dan Fisherman Letter";
			ultraTreeNode43.LeftImages.Add(((object)(resources.GetObject("resource18"))));
			ultraTreeNode43.Text = "Firm Labels";
			ultraTreeNode39.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode40,
																								 ultraTreeNode41,
																								 ultraTreeNode42,
																								 ultraTreeNode43});
			ultraTreeNode39.Text = "Favorites";
			ultraTreeNode44.Key = "Labels";
			ultraTreeNode44.LeftImages.Add(((object)(resources.GetObject("resource19"))));
			ultraTreeNode45.LeftImages.Add(((object)(resources.GetObject("resource20"))));
			ultraTreeNode45.Text = "Avery 5164 - Firm Labels (Word-Generated)";
			ultraTreeNode46.LeftImages.Add(((object)(resources.GetObject("resource21"))));
			ultraTreeNode46.Text = "Avery 5262 - Address Labels";
			ultraTreeNode47.LeftImages.Add(((object)(resources.GetObject("resource22"))));
			ultraTreeNode47.Text = "Avery 5266 - File Folder Labels";
			ultraTreeNode48.LeftImages.Add(((object)(resources.GetObject("resource23"))));
			ultraTreeNode48.Text = "Avery 5267 - Return Address Labels";
			ultraTreeNode49.LeftImages.Add(((object)(resources.GetObject("resource24"))));
			ultraTreeNode49.Text = "Avery 5660 - Address Labels";
			ultraTreeNode44.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode45,
																								 ultraTreeNode46,
																								 ultraTreeNode47,
																								 ultraTreeNode48,
																								 ultraTreeNode49});
			ultraTreeNode44.Text = "Labels...";
			ultraTreeNode50.Key = "Envelopes";
			ultraTreeNode50.LeftImages.Add(((object)(resources.GetObject("resource25"))));
			ultraTreeNode50.Text = "Envelopes...";
			ultraTreeNode51.Key = "DraftStamps";
			ultraTreeNode51.LeftImages.Add(((object)(resources.GetObject("resource26"))));
			ultraTreeNode51.Text = "Draft Stamps...";
			ultraTreeNode52.Key = "Trailer";
			ultraTreeNode52.LeftImages.Add(((object)(resources.GetObject("resource27"))));
			ultraTreeNode52.Text = "Trailer: Insert Doc ID...";
			ultraTreeNode53.LeftImages.Add(((object)(resources.GetObject("resource28"))));
			ultraTreeNode53.Text = "Page Numbers...";
			this.ultraTree1.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode1,
																								 ultraTreeNode7,
																								 ultraTreeNode35,
																								 ultraTreeNode38,
																								 ultraTreeNode39,
																								 ultraTreeNode44,
																								 ultraTreeNode50,
																								 ultraTreeNode51,
																								 ultraTreeNode52,
																								 ultraTreeNode53});
			_override2.SelectionType = Infragistics.Win.UltraWinTree.SelectType.Single;
			this.ultraTree1.Override = _override2;
			this.ultraTree1.Size = new System.Drawing.Size(314, 442);
			this.ultraTree1.TabIndex = 1;
			this.ultraTree1.DragOver += new System.Windows.Forms.DragEventHandler(this.ultraTree1_DragOver);
			this.ultraTree1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultraTree1_MouseMove);
			this.ultraTree1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ultraTree1_MouseDown);
			this.ultraTree1.DragLeave += new System.EventHandler(this.ultraTree1_DragLeave);
			this.ultraTree1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownHandler);
			this.ultraTree1.DragEnter += new System.Windows.Forms.DragEventHandler(this.ultraTree1_DragEnter);
			this.ultraTree1.DragDrop += new System.Windows.Forms.DragEventHandler(this.ultraTree1_DragDrop);
			// 
			// ultraTree3
			// 
			appearance3.BackColor = System.Drawing.Color.Gainsboro;
			this.ultraTree3.Appearance = appearance3;
			this.ultraTree3.BorderStyle = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
			this.ultraTree3.ColumnSettings.RootColumnSet = ultraTreeColumnSet2;
			this.ultraTree3.FullRowSelect = true;
			this.ultraTree3.Indent = 35;
			this.ultraTree3.Location = new System.Drawing.Point(150, 439);
			this.ultraTree3.Name = "ultraTree3";
			ultraTreeNode54.Text = "Insert At Selection";
			ultraTreeNode55.Text = "Insert At Start Of Document";
			ultraTreeNode56.Text = "Insert At End Of Document";
			this.ultraTree3.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode54,
																								 ultraTreeNode55,
																								 ultraTreeNode56});
			this.ultraTree3.ShowLines = false;
			this.ultraTree3.ShowRootLines = false;
			this.ultraTree3.Size = new System.Drawing.Size(53, 37);
			this.ultraTree3.TabIndex = 22;
			this.ultraTree3.Visible = false;
			// 
			// dbtnInsert
			// 
			this.dbtnInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dbtnInsert.BackColor = System.Drawing.Color.Gainsboro;
			this.dbtnInsert.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
			this.dbtnInsert.Location = new System.Drawing.Point(162, 458);
			this.dbtnInsert.Name = "dbtnInsert";
			this.dbtnInsert.PopupItemKey = "ultraTree3";
			this.dbtnInsert.PopupItemProvider = this.ultraPopupControlContainer1;
			this.dbtnInsert.Size = new System.Drawing.Size(153, 21);
			this.dbtnInsert.Style = Infragistics.Win.Misc.SplitButtonDisplayStyle.DropDownButtonOnly;
			this.dbtnInsert.TabIndex = 21;
			this.dbtnInsert.TabStop = false;
			this.dbtnInsert.Text = "&Insert At Selection";
			this.dbtnInsert.Visible = false;
			// 
			// ultraPopupControlContainer1
			// 
			this.ultraPopupControlContainer1.PopupControl = this.ultraTree3;
			// 
			// ultraButton2
			// 
			this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ultraButton2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.ultraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
			this.ultraButton2.Location = new System.Drawing.Point(-10, 458);
			this.ultraButton2.Name = "ultraButton2";
			this.ultraButton2.ShowFocusRect = false;
			this.ultraButton2.Size = new System.Drawing.Size(63, 22);
			this.ultraButton2.TabIndex = 18;
			this.ultraButton2.Text = "&Insert";
			this.ultraButton2.Visible = false;
			this.ultraButton2.WrapText = false;
			this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click_1);
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.Color.Gray;
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter1.Location = new System.Drawing.Point(0, 483);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(322, 1);
			this.splitter1.TabIndex = 18;
			this.splitter1.TabStop = false;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel3.Controls.Add(this.ultraTabControl2);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel3.Location = new System.Drawing.Point(0, 484);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(322, 181);
			this.panel3.TabIndex = 17;
			this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
			// 
			// ultraTabControl2
			// 
			this.ultraTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ultraTabControl2.Controls.Add(this.ultraTabSharedControlsPage2);
			this.ultraTabControl2.Controls.Add(this.ultraTabPageControl3);
			this.ultraTabControl2.Controls.Add(this.ultraTabPageControl5);
			this.ultraTabControl2.Location = new System.Drawing.Point(1, 8);
			this.ultraTabControl2.Name = "ultraTabControl2";
			this.ultraTabControl2.SharedControlsPage = this.ultraTabSharedControlsPage2;
			this.ultraTabControl2.ShowButtonSeparators = true;
			this.ultraTabControl2.Size = new System.Drawing.Size(318, 169);
			this.ultraTabControl2.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.StateButtons;
			this.ultraTabControl2.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
			this.ultraTabControl2.TabIndex = 21;
			ultraTab1.TabPage = this.ultraTabPageControl5;
			ultraTab1.Text = "Description";
			ultraTab2.TabPage = this.ultraTabPageControl3;
			ultraTab2.Text = "Preview";
			this.ultraTabControl2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
																									   ultraTab1,
																									   ultraTab2});
			// 
			// ultraTabSharedControlsPage2
			// 
			this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
			this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(318, 149);
			// 
			// ultraTabPageControl2
			// 
			this.ultraTabPageControl2.Controls.Add(this.panel1);
			this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabPageControl2.Name = "ultraTabPageControl2";
			this.ultraTabPageControl2.Size = new System.Drawing.Size(322, 665);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel1.Controls.Add(this.panel6);
			this.panel1.Controls.Add(this.splitter2);
			this.panel1.Controls.Add(this.panel5);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(322, 665);
			this.panel1.TabIndex = 18;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.btnValidateValidationCondition);
			this.panel6.Controls.Add(this.ctlValidateValidationCondition);
			this.panel6.Controls.Add(this.ctlValidateTargetVariable);
			this.panel6.Controls.Add(this.btnValidateExecutionCondition);
			this.panel6.Controls.Add(this.ctlValidateExecutionCondition);
			this.panel6.Controls.Add(this.btnAssociatedMVar);
			this.panel6.Controls.Add(this.ctlAssociatedMVar);
			this.panel6.Controls.Add(this.btnMappings);
			this.panel6.Controls.Add(this.ctlMappings);
			this.panel6.Controls.Add(this.btnExecutionCondition);
			this.panel6.Controls.Add(this.btnDefaultValue);
			this.panel6.Controls.Add(this.ctlExecutionCondition);
			this.panel6.Controls.Add(this.btnExpression);
			this.panel6.Controls.Add(this.ctlExpression);
			this.panel6.Controls.Add(this.ctlDeleteScope);
			this.panel6.Controls.Add(this.ctlUnderlineLength);
			this.panel6.Controls.Add(this.ctlDelimiterReplacement);
			this.panel6.Controls.Add(this.ctlXMlTag);
			this.panel6.Controls.Add(this.ctlDefaultValue);
			this.panel6.Controls.Add(this.ctlControlType);
			this.panel6.Controls.Add(this.ctlVisibleInTree);
			this.panel6.Controls.Add(this.label1);
			this.panel6.Controls.Add(this.utrEditVariables);
			this.panel6.Controls.Add(this.ultraTree2);
			this.panel6.Controls.Add(this.cmbVariableSets);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(0, 0);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(322, 554);
			this.panel6.TabIndex = 36;
			// 
			// btnValidateValidationCondition
			// 
			this.btnValidateValidationCondition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance4.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
			appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance4.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance4.TextHAlign = Infragistics.Win.HAlign.Left;
			this.btnValidateValidationCondition.Appearance = appearance4;
			this.btnValidateValidationCondition.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnValidateValidationCondition.ImageSize = new System.Drawing.Size(5, 2);
			this.btnValidateValidationCondition.Location = new System.Drawing.Point(283, 540);
			this.btnValidateValidationCondition.Name = "btnValidateValidationCondition";
			this.btnValidateValidationCondition.Size = new System.Drawing.Size(16, 19);
			this.btnValidateValidationCondition.TabIndex = 60;
			this.btnValidateValidationCondition.Visible = false;
			// 
			// ctlValidateValidationCondition
			// 
			this.ctlValidateValidationCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlValidateValidationCondition.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlValidateValidationCondition.Location = new System.Drawing.Point(212, 540);
			this.ctlValidateValidationCondition.Name = "ctlValidateValidationCondition";
			this.ctlValidateValidationCondition.Size = new System.Drawing.Size(71, 19);
			this.ctlValidateValidationCondition.TabIndex = 59;
			this.ctlValidateValidationCondition.Visible = false;
			// 
			// ctlValidateTargetVariable
			// 
			this.ctlValidateTargetVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlValidateTargetVariable.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlValidateTargetVariable.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			appearance5.BackColor = System.Drawing.SystemColors.Window;
			appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.ctlValidateTargetVariable.DisplayLayout.Appearance = appearance5;
			this.ctlValidateTargetVariable.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ctlValidateTargetVariable.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance6.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlValidateTargetVariable.DisplayLayout.GroupByBox.Appearance = appearance6;
			appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlValidateTargetVariable.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
			this.ctlValidateTargetVariable.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance8.BackColor2 = System.Drawing.SystemColors.Control;
			appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlValidateTargetVariable.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
			this.ctlValidateTargetVariable.DisplayLayout.MaxColScrollRegions = 1;
			this.ctlValidateTargetVariable.DisplayLayout.MaxRowScrollRegions = 1;
			appearance9.BackColor = System.Drawing.SystemColors.Window;
			appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ctlValidateTargetVariable.DisplayLayout.Override.ActiveCellAppearance = appearance9;
			appearance10.BackColor = System.Drawing.SystemColors.Highlight;
			appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.ctlValidateTargetVariable.DisplayLayout.Override.ActiveRowAppearance = appearance10;
			this.ctlValidateTargetVariable.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlValidateTargetVariable.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance11.BackColor = System.Drawing.SystemColors.Window;
			this.ctlValidateTargetVariable.DisplayLayout.Override.CardAreaAppearance = appearance11;
			appearance12.BorderColor = System.Drawing.Color.Silver;
			appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.ctlValidateTargetVariable.DisplayLayout.Override.CellAppearance = appearance12;
			this.ctlValidateTargetVariable.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.ctlValidateTargetVariable.DisplayLayout.Override.CellPadding = 0;
			appearance13.BackColor = System.Drawing.SystemColors.Control;
			appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance13.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlValidateTargetVariable.DisplayLayout.Override.GroupByRowAppearance = appearance13;
			appearance14.TextHAlign = Infragistics.Win.HAlign.Left;
			this.ctlValidateTargetVariable.DisplayLayout.Override.HeaderAppearance = appearance14;
			this.ctlValidateTargetVariable.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.ctlValidateTargetVariable.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance15.BackColor = System.Drawing.SystemColors.Window;
			appearance15.BorderColor = System.Drawing.Color.Silver;
			this.ctlValidateTargetVariable.DisplayLayout.Override.RowAppearance = appearance15;
			this.ctlValidateTargetVariable.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ctlValidateTargetVariable.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
			this.ctlValidateTargetVariable.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.ctlValidateTargetVariable.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.ctlValidateTargetVariable.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.ctlValidateTargetVariable.DisplayMember = "";
			this.ctlValidateTargetVariable.Location = new System.Drawing.Point(212, 522);
			this.ctlValidateTargetVariable.Name = "ctlValidateTargetVariable";
			this.ctlValidateTargetVariable.Size = new System.Drawing.Size(87, 19);
			this.ctlValidateTargetVariable.TabIndex = 58;
			this.ctlValidateTargetVariable.Text = "Yes";
			this.ctlValidateTargetVariable.ValueMember = "";
			this.ctlValidateTargetVariable.Visible = false;
			// 
			// btnValidateExecutionCondition
			// 
			this.btnValidateExecutionCondition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance17.Image = ((object)(resources.GetObject("appearance17.Image")));
			appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance17.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance17.TextHAlign = Infragistics.Win.HAlign.Left;
			this.btnValidateExecutionCondition.Appearance = appearance17;
			this.btnValidateExecutionCondition.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnValidateExecutionCondition.ImageSize = new System.Drawing.Size(5, 2);
			this.btnValidateExecutionCondition.Location = new System.Drawing.Point(283, 504);
			this.btnValidateExecutionCondition.Name = "btnValidateExecutionCondition";
			this.btnValidateExecutionCondition.Size = new System.Drawing.Size(16, 19);
			this.btnValidateExecutionCondition.TabIndex = 57;
			this.btnValidateExecutionCondition.Visible = false;
			// 
			// ctlValidateExecutionCondition
			// 
			this.ctlValidateExecutionCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlValidateExecutionCondition.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlValidateExecutionCondition.Location = new System.Drawing.Point(212, 504);
			this.ctlValidateExecutionCondition.Name = "ctlValidateExecutionCondition";
			this.ctlValidateExecutionCondition.Size = new System.Drawing.Size(71, 19);
			this.ctlValidateExecutionCondition.TabIndex = 56;
			this.ctlValidateExecutionCondition.Visible = false;
			// 
			// btnAssociatedMVar
			// 
			this.btnAssociatedMVar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance18.Image = ((object)(resources.GetObject("appearance18.Image")));
			appearance18.ImageHAlign = Infragistics.Win.HAlign.Left;
			appearance18.ImageVAlign = Infragistics.Win.VAlign.Middle;
			appearance18.TextHAlign = Infragistics.Win.HAlign.Left;
			appearance18.TextVAlign = Infragistics.Win.VAlign.Middle;
			this.btnAssociatedMVar.Appearance = appearance18;
			this.btnAssociatedMVar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnAssociatedMVar.ImageSize = new System.Drawing.Size(7, 7);
			this.btnAssociatedMVar.Location = new System.Drawing.Point(282, 113);
			this.btnAssociatedMVar.Name = "btnAssociatedMVar";
			this.btnAssociatedMVar.Size = new System.Drawing.Size(17, 18);
			this.btnAssociatedMVar.TabIndex = 55;
			this.btnAssociatedMVar.Visible = false;
			// 
			// ctlAssociatedMVar
			// 
			this.ctlAssociatedMVar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlAssociatedMVar.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlAssociatedMVar.Location = new System.Drawing.Point(212, 112);
			this.ctlAssociatedMVar.Name = "ctlAssociatedMVar";
			this.ctlAssociatedMVar.Size = new System.Drawing.Size(71, 19);
			this.ctlAssociatedMVar.TabIndex = 54;
			this.ctlAssociatedMVar.Visible = false;
			// 
			// btnMappings
			// 
			this.btnMappings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance19.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance19.Image = ((object)(resources.GetObject("appearance19.Image")));
			appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance19.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance19.TextHAlign = Infragistics.Win.HAlign.Left;
			this.btnMappings.Appearance = appearance19;
			this.btnMappings.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnMappings.ImageSize = new System.Drawing.Size(5, 2);
			this.btnMappings.Location = new System.Drawing.Point(283, 188);
			this.btnMappings.Name = "btnMappings";
			this.btnMappings.Size = new System.Drawing.Size(16, 19);
			this.btnMappings.TabIndex = 53;
			this.btnMappings.Visible = false;
			// 
			// ctlMappings
			// 
			this.ctlMappings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlMappings.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlMappings.Location = new System.Drawing.Point(212, 188);
			this.ctlMappings.Name = "ctlMappings";
			this.ctlMappings.Size = new System.Drawing.Size(71, 19);
			this.ctlMappings.TabIndex = 52;
			this.ctlMappings.Visible = false;
			// 
			// btnExecutionCondition
			// 
			this.btnExecutionCondition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance20.Image = ((object)(resources.GetObject("appearance20.Image")));
			appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance20.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance20.TextHAlign = Infragistics.Win.HAlign.Left;
			this.btnExecutionCondition.Appearance = appearance20;
			this.btnExecutionCondition.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnExecutionCondition.ImageSize = new System.Drawing.Size(5, 2);
			this.btnExecutionCondition.Location = new System.Drawing.Point(283, 170);
			this.btnExecutionCondition.Name = "btnExecutionCondition";
			this.btnExecutionCondition.Size = new System.Drawing.Size(16, 19);
			this.btnExecutionCondition.TabIndex = 51;
			this.btnExecutionCondition.Visible = false;
			// 
			// btnDefaultValue
			// 
			this.btnDefaultValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance21.Image = ((object)(resources.GetObject("appearance21.Image")));
			appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance21.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance21.TextHAlign = Infragistics.Win.HAlign.Left;
			appearance21.TextVAlign = Infragistics.Win.VAlign.Middle;
			this.btnDefaultValue.Appearance = appearance21;
			this.btnDefaultValue.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnDefaultValue.ImageSize = new System.Drawing.Size(5, 2);
			this.btnDefaultValue.Location = new System.Drawing.Point(282, 95);
			this.btnDefaultValue.Name = "btnDefaultValue";
			this.btnDefaultValue.Size = new System.Drawing.Size(17, 18);
			this.btnDefaultValue.TabIndex = 50;
			this.btnDefaultValue.Visible = false;
			// 
			// ctlExecutionCondition
			// 
			this.ctlExecutionCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlExecutionCondition.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlExecutionCondition.Location = new System.Drawing.Point(212, 170);
			this.ctlExecutionCondition.Name = "ctlExecutionCondition";
			this.ctlExecutionCondition.Size = new System.Drawing.Size(71, 19);
			this.ctlExecutionCondition.TabIndex = 48;
			this.ctlExecutionCondition.Visible = false;
			// 
			// btnExpression
			// 
			this.btnExpression.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			appearance22.BackColor = System.Drawing.SystemColors.ControlLight;
			appearance22.Image = ((object)(resources.GetObject("appearance22.Image")));
			appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
			appearance22.ImageVAlign = Infragistics.Win.VAlign.Bottom;
			appearance22.TextHAlign = Infragistics.Win.HAlign.Left;
			this.btnExpression.Appearance = appearance22;
			this.btnExpression.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
			this.btnExpression.ImageSize = new System.Drawing.Size(5, 2);
			this.btnExpression.Location = new System.Drawing.Point(282, 278);
			this.btnExpression.Name = "btnExpression";
			this.btnExpression.Size = new System.Drawing.Size(16, 18);
			this.btnExpression.TabIndex = 47;
			this.btnExpression.Visible = false;
			// 
			// ctlExpression
			// 
			this.ctlExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlExpression.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlExpression.Location = new System.Drawing.Point(212, 277);
			this.ctlExpression.Name = "ctlExpression";
			this.ctlExpression.Size = new System.Drawing.Size(71, 19);
			this.ctlExpression.TabIndex = 46;
			this.ctlExpression.Visible = false;
			// 
			// ctlDeleteScope
			// 
			this.ctlDeleteScope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlDeleteScope.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlDeleteScope.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			appearance23.BackColor = System.Drawing.SystemColors.Window;
			appearance23.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.ctlDeleteScope.DisplayLayout.Appearance = appearance23;
			this.ctlDeleteScope.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ctlDeleteScope.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance24.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance24.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlDeleteScope.DisplayLayout.GroupByBox.Appearance = appearance24;
			appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlDeleteScope.DisplayLayout.GroupByBox.BandLabelAppearance = appearance25;
			this.ctlDeleteScope.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance26.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance26.BackColor2 = System.Drawing.SystemColors.Control;
			appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlDeleteScope.DisplayLayout.GroupByBox.PromptAppearance = appearance26;
			this.ctlDeleteScope.DisplayLayout.MaxColScrollRegions = 1;
			this.ctlDeleteScope.DisplayLayout.MaxRowScrollRegions = 1;
			appearance27.BackColor = System.Drawing.SystemColors.Window;
			appearance27.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ctlDeleteScope.DisplayLayout.Override.ActiveCellAppearance = appearance27;
			appearance28.BackColor = System.Drawing.SystemColors.Highlight;
			appearance28.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.ctlDeleteScope.DisplayLayout.Override.ActiveRowAppearance = appearance28;
			this.ctlDeleteScope.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlDeleteScope.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance29.BackColor = System.Drawing.SystemColors.Window;
			this.ctlDeleteScope.DisplayLayout.Override.CardAreaAppearance = appearance29;
			appearance30.BorderColor = System.Drawing.Color.Silver;
			appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.ctlDeleteScope.DisplayLayout.Override.CellAppearance = appearance30;
			this.ctlDeleteScope.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.ctlDeleteScope.DisplayLayout.Override.CellPadding = 0;
			appearance31.BackColor = System.Drawing.SystemColors.Control;
			appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance31.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance31.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlDeleteScope.DisplayLayout.Override.GroupByRowAppearance = appearance31;
			appearance32.TextHAlign = Infragistics.Win.HAlign.Left;
			this.ctlDeleteScope.DisplayLayout.Override.HeaderAppearance = appearance32;
			this.ctlDeleteScope.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.ctlDeleteScope.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance33.BackColor = System.Drawing.SystemColors.Window;
			appearance33.BorderColor = System.Drawing.Color.Silver;
			this.ctlDeleteScope.DisplayLayout.Override.RowAppearance = appearance33;
			this.ctlDeleteScope.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ctlDeleteScope.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
			this.ctlDeleteScope.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.ctlDeleteScope.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.ctlDeleteScope.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.ctlDeleteScope.DisplayMember = "";
			this.ctlDeleteScope.Location = new System.Drawing.Point(212, 259);
			this.ctlDeleteScope.Name = "ctlDeleteScope";
			this.ctlDeleteScope.Size = new System.Drawing.Size(87, 19);
			this.ctlDeleteScope.TabIndex = 45;
			this.ctlDeleteScope.Text = "Node";
			this.ctlDeleteScope.ValueMember = "";
			this.ctlDeleteScope.Visible = false;
			// 
			// ctlUnderlineLength
			// 
			this.ctlUnderlineLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlUnderlineLength.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlUnderlineLength.Location = new System.Drawing.Point(212, 241);
			this.ctlUnderlineLength.Name = "ctlUnderlineLength";
			this.ctlUnderlineLength.Size = new System.Drawing.Size(87, 19);
			this.ctlUnderlineLength.TabIndex = 44;
			this.ctlUnderlineLength.Visible = false;
			// 
			// ctlDelimiterReplacement
			// 
			this.ctlDelimiterReplacement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlDelimiterReplacement.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlDelimiterReplacement.Location = new System.Drawing.Point(212, 223);
			this.ctlDelimiterReplacement.Name = "ctlDelimiterReplacement";
			this.ctlDelimiterReplacement.Size = new System.Drawing.Size(87, 19);
			this.ctlDelimiterReplacement.TabIndex = 43;
			this.ctlDelimiterReplacement.Visible = false;
			// 
			// ctlXMlTag
			// 
			this.ctlXMlTag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlXMlTag.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlXMlTag.Location = new System.Drawing.Point(212, 206);
			this.ctlXMlTag.Name = "ctlXMlTag";
			this.ctlXMlTag.Size = new System.Drawing.Size(87, 19);
			this.ctlXMlTag.TabIndex = 42;
			this.ctlXMlTag.Visible = false;
			// 
			// ctlDefaultValue
			// 
			this.ctlDefaultValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlDefaultValue.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlDefaultValue.Location = new System.Drawing.Point(212, 94);
			this.ctlDefaultValue.Name = "ctlDefaultValue";
			this.ctlDefaultValue.Size = new System.Drawing.Size(71, 19);
			this.ctlDefaultValue.TabIndex = 40;
			this.ctlDefaultValue.Visible = false;
			// 
			// ctlControlType
			// 
			this.ctlControlType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlControlType.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlControlType.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			appearance35.BackColor = System.Drawing.SystemColors.Window;
			appearance35.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.ctlControlType.DisplayLayout.Appearance = appearance35;
			this.ctlControlType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ctlControlType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance36.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlControlType.DisplayLayout.GroupByBox.Appearance = appearance36;
			appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlControlType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
			this.ctlControlType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance38.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance38.BackColor2 = System.Drawing.SystemColors.Control;
			appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlControlType.DisplayLayout.GroupByBox.PromptAppearance = appearance38;
			this.ctlControlType.DisplayLayout.MaxColScrollRegions = 1;
			this.ctlControlType.DisplayLayout.MaxRowScrollRegions = 1;
			appearance39.BackColor = System.Drawing.SystemColors.Window;
			appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ctlControlType.DisplayLayout.Override.ActiveCellAppearance = appearance39;
			appearance40.BackColor = System.Drawing.SystemColors.Highlight;
			appearance40.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.ctlControlType.DisplayLayout.Override.ActiveRowAppearance = appearance40;
			this.ctlControlType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlControlType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance41.BackColor = System.Drawing.SystemColors.Window;
			this.ctlControlType.DisplayLayout.Override.CardAreaAppearance = appearance41;
			appearance42.BorderColor = System.Drawing.Color.Silver;
			appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.ctlControlType.DisplayLayout.Override.CellAppearance = appearance42;
			this.ctlControlType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.ctlControlType.DisplayLayout.Override.CellPadding = 0;
			appearance43.BackColor = System.Drawing.SystemColors.Control;
			appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance43.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlControlType.DisplayLayout.Override.GroupByRowAppearance = appearance43;
			appearance44.TextHAlign = Infragistics.Win.HAlign.Left;
			this.ctlControlType.DisplayLayout.Override.HeaderAppearance = appearance44;
			this.ctlControlType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.ctlControlType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance45.BackColor = System.Drawing.SystemColors.Window;
			appearance45.BorderColor = System.Drawing.Color.Silver;
			this.ctlControlType.DisplayLayout.Override.RowAppearance = appearance45;
			this.ctlControlType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ctlControlType.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
			this.ctlControlType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.ctlControlType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.ctlControlType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.ctlControlType.DisplayMember = "";
			this.ctlControlType.Location = new System.Drawing.Point(212, 318);
			this.ctlControlType.Name = "ctlControlType";
			this.ctlControlType.Size = new System.Drawing.Size(87, 19);
			this.ctlControlType.TabIndex = 39;
			this.ctlControlType.Text = "Multiline ComboBox";
			this.ctlControlType.ValueMember = "";
			this.ctlControlType.Visible = false;
			// 
			// ctlVisibleInTree
			// 
			this.ctlVisibleInTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ctlVisibleInTree.BorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlVisibleInTree.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			appearance47.BackColor = System.Drawing.SystemColors.Window;
			appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.ctlVisibleInTree.DisplayLayout.Appearance = appearance47;
			this.ctlVisibleInTree.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ctlVisibleInTree.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance48.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance48.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlVisibleInTree.DisplayLayout.GroupByBox.Appearance = appearance48;
			appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlVisibleInTree.DisplayLayout.GroupByBox.BandLabelAppearance = appearance49;
			this.ctlVisibleInTree.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance50.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance50.BackColor2 = System.Drawing.SystemColors.Control;
			appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ctlVisibleInTree.DisplayLayout.GroupByBox.PromptAppearance = appearance50;
			this.ctlVisibleInTree.DisplayLayout.MaxColScrollRegions = 1;
			this.ctlVisibleInTree.DisplayLayout.MaxRowScrollRegions = 1;
			appearance51.BackColor = System.Drawing.SystemColors.Window;
			appearance51.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ctlVisibleInTree.DisplayLayout.Override.ActiveCellAppearance = appearance51;
			appearance52.BackColor = System.Drawing.SystemColors.Highlight;
			appearance52.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.ctlVisibleInTree.DisplayLayout.Override.ActiveRowAppearance = appearance52;
			this.ctlVisibleInTree.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ctlVisibleInTree.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance53.BackColor = System.Drawing.SystemColors.Window;
			this.ctlVisibleInTree.DisplayLayout.Override.CardAreaAppearance = appearance53;
			appearance54.BorderColor = System.Drawing.Color.Silver;
			appearance54.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.ctlVisibleInTree.DisplayLayout.Override.CellAppearance = appearance54;
			this.ctlVisibleInTree.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.ctlVisibleInTree.DisplayLayout.Override.CellPadding = 0;
			appearance55.BackColor = System.Drawing.SystemColors.Control;
			appearance55.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance55.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance55.BorderColor = System.Drawing.SystemColors.Window;
			this.ctlVisibleInTree.DisplayLayout.Override.GroupByRowAppearance = appearance55;
			appearance56.TextHAlign = Infragistics.Win.HAlign.Left;
			this.ctlVisibleInTree.DisplayLayout.Override.HeaderAppearance = appearance56;
			this.ctlVisibleInTree.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.ctlVisibleInTree.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance57.BackColor = System.Drawing.SystemColors.Window;
			appearance57.BorderColor = System.Drawing.Color.Silver;
			this.ctlVisibleInTree.DisplayLayout.Override.RowAppearance = appearance57;
			this.ctlVisibleInTree.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance58.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ctlVisibleInTree.DisplayLayout.Override.TemplateAddRowAppearance = appearance58;
			this.ctlVisibleInTree.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.ctlVisibleInTree.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.ctlVisibleInTree.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.ctlVisibleInTree.DisplayMember = "";
			this.ctlVisibleInTree.Location = new System.Drawing.Point(212, 76);
			this.ctlVisibleInTree.Name = "ctlVisibleInTree";
			this.ctlVisibleInTree.Size = new System.Drawing.Size(87, 19);
			this.ctlVisibleInTree.TabIndex = 38;
			this.ctlVisibleInTree.Text = "Yes";
			this.ctlVisibleInTree.ValueMember = "";
			this.ctlVisibleInTree.Visible = false;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.ContextMenu = this.mnuDocument;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label1.Location = new System.Drawing.Point(5, 5);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(161, 17);
			this.label1.TabIndex = 36;
			this.label1.Text = "Contents of this Word File:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuDocument
			// 
			this.mnuDocument.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.menuItem5,
																						this.menuItem6,
																						this.menuItem7});
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 0;
			this.menuItem5.Text = "Doc&ument Font...";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 1;
			this.menuItem6.Text = "Show &XML Tags";
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 2;
			this.menuItem7.Text = "&E-File Document...";
			// 
			// utrEditVariables
			// 
			this.utrEditVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.utrEditVariables.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.utrEditVariables.ColumnSettings.RootColumnSet = ultraTreeColumnSet3;
			this.utrEditVariables.FullRowSelect = true;
			this.utrEditVariables.HideSelection = false;
			this.utrEditVariables.Indent = 15;
			this.utrEditVariables.Location = new System.Drawing.Point(4, 24);
			this.utrEditVariables.Name = "utrEditVariables";
			this.utrEditVariables.NodeConnectorColor = System.Drawing.SystemColors.ControlLight;
			this.utrEditVariables.NodeConnectorStyle = Infragistics.Win.UltraWinTree.NodeConnectorStyle.Solid;
			ultraTreeNode57.Key = "segLetter";
			ultraTreeNode57.LeftImages.Add(((object)(resources.GetObject("resource29"))));
			ultraTreeNode58.Key = "segLetter.varAuthors";
			ultraTreeNode58.LeftImages.Add(((object)(resources.GetObject("resource30"))));
			ultraTreeNode58.Text = "Authors";
			ultraTreeNode59.Key = "segLetter.varDeliveryPhrases";
			ultraTreeNode59.LeftImages.Add(((object)(resources.GetObject("resource31"))));
			ultraTreeNode60.Key = "VisibleInTree";
			_override3.NodeSpacingAfter = 3;
			ultraTreeNode60.Override = _override3;
			ultraTreeNode60.Text = "Visible In Tree:";
			ultraTreeNode61.Key = "DefaultValue";
			_override4.NodeSpacingAfter = 3;
			ultraTreeNode61.Override = _override4;
			ultraTreeNode61.Text = "Default Value:";
			ultraTreeNode62.Key = "Actions";
			ultraTreeNode63.Key = "segLetter.varDeliveryPhrases.actionInsertText";
			ultraTreeNode64.Key = "ExecutionCondition";
			_override5.NodeSpacingAfter = 3;
			ultraTreeNode64.Override = _override5;
			ultraTreeNode64.Text = "Execution Condition:";
			ultraTreeNode65.Key = "Mappings";
			_override6.NodeSpacingAfter = 3;
			ultraTreeNode65.Override = _override6;
			ultraTreeNode65.Text = "Mappings:";
			ultraTreeNode66.Key = "XMLTag";
			_override7.NodeSpacingAfter = 3;
			ultraTreeNode66.Override = _override7;
			ultraTreeNode66.Text = "XML Tag:";
			ultraTreeNode67.Key = "UnderlineLength";
			_override8.NodeSpacingAfter = 3;
			ultraTreeNode67.Override = _override8;
			ultraTreeNode67.Text = "Delimiter Replacement:";
			ultraTreeNode68.Key = "DeleteScope";
			_override9.NodeSpacingAfter = 3;
			ultraTreeNode68.Override = _override9;
			ultraTreeNode68.Text = "Underline Length:";
			ultraTreeNode69.Key = "Expression";
			_override10.NodeSpacingAfter = 3;
			ultraTreeNode69.Override = _override10;
			ultraTreeNode69.Text = "Delete Scope:";
			_override11.NodeSpacingAfter = 6;
			ultraTreeNode70.Override = _override11;
			ultraTreeNode70.Text = "Expression:";
			ultraTreeNode63.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode64,
																								 ultraTreeNode65,
																								 ultraTreeNode66,
																								 ultraTreeNode67,
																								 ultraTreeNode68,
																								 ultraTreeNode69,
																								 ultraTreeNode70});
			appearance59.BackColor = System.Drawing.Color.MediumAquamarine;
			appearance59.BackColor2 = System.Drawing.Color.Aqua;
			appearance59.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
			appearance59.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
			appearance59.BorderAlpha = Infragistics.Win.Alpha.Opaque;
			appearance59.BorderColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(64)), ((System.Byte)(0)));
			_override12.ActiveNodeAppearance = appearance59;
			_override12.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
			_override12.NodeSpacingAfter = 5;
			ultraTreeNode63.Override = _override12;
			ultraTreeNode63.Text = "Insert Text";
			ultraTreeNode72.Text = "Execution Condition:";
			ultraTreeNode73.Text = "Mappings:";
			ultraTreeNode71.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode72,
																								 ultraTreeNode73});
			ultraTreeNode71.Text = "Execute On Document";
			ultraTreeNode62.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode63,
																								 ultraTreeNode71});
			ultraTreeNode62.Text = "Actions:";
			ultraTreeNode75.Key = "segLetter.varDeliveryPhrases.Control.Properties";
			_override13.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
			_override13.ShowColumns = Infragistics.Win.DefaultableBoolean.True;
			_override13.TipStyleNode = Infragistics.Win.UltraWinTree.TipStyleNode.Show;
			ultraTreeNode76.Override = _override13;
			ultraTreeNode76.Text = "NumberOfLines = 3";
			ultraTreeNode77.Text = "List=DeliveryPhrases";
			ultraTreeNode75.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode76,
																								 ultraTreeNode77});
			ultraTreeNode75.Text = "Properties:";
			_override14.NodeSpacingAfter = 4;
			ultraTreeNode79.Override = _override14;
			ultraTreeNode79.Text = "On First Display";
			_override15.NodeSpacingAfter = 4;
			ultraTreeNode80.Override = _override15;
			ultraTreeNode80.Text = "On Got Focus";
			_override16.NodeSpacingAfter = 4;
			ultraTreeNode81.Override = _override16;
			ultraTreeNode81.Text = "On Lost Focus";
			ultraTreeNode83.Key = "segLetter.varDeliveryPhrases.Control.Action.Validate";
			_override17.NodeSpacingAfter = 4;
			ultraTreeNode84.Override = _override17;
			ultraTreeNode84.Text = "Execution Condition:";
			_override18.NodeSpacingAfter = 4;
			ultraTreeNode85.Override = _override18;
			ultraTreeNode85.Text = "Target Variable:";
			_override19.NodeSpacingAfter = 4;
			ultraTreeNode86.Override = _override19;
			ultraTreeNode86.Text = "Validation Condition:";
			ultraTreeNode83.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode84,
																								 ultraTreeNode85,
																								 ultraTreeNode86});
			_override20.NodeSpacingAfter = 5;
			ultraTreeNode83.Override = _override20;
			ultraTreeNode83.Text = "Validate";
			ultraTreeNode82.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode83});
			_override21.NodeSpacingAfter = 4;
			ultraTreeNode82.Override = _override21;
			ultraTreeNode82.Text = "On Value Changed";
			_override22.NodeSpacingAfter = 4;
			ultraTreeNode87.Override = _override22;
			ultraTreeNode87.Text = "On Click";
			_override23.NodeSpacingAfter = 4;
			ultraTreeNode88.Override = _override23;
			ultraTreeNode88.Text = "On Double Click";
			_override24.NodeSpacingAfter = 4;
			ultraTreeNode89.Override = _override24;
			ultraTreeNode89.Text = "Before Item Add";
			_override25.NodeSpacingAfter = 6;
			ultraTreeNode90.Override = _override25;
			ultraTreeNode90.Text = "On Validate";
			ultraTreeNode78.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode79,
																								 ultraTreeNode80,
																								 ultraTreeNode81,
																								 ultraTreeNode82,
																								 ultraTreeNode87,
																								 ultraTreeNode88,
																								 ultraTreeNode89,
																								 ultraTreeNode90});
			ultraTreeNode78.Text = "Actions:";
			ultraTreeNode74.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode75,
																								 ultraTreeNode78});
			ultraTreeNode74.Text = "Control:";
			ultraTreeNode59.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode60,
																								 ultraTreeNode61,
																								 ultraTreeNode62,
																								 ultraTreeNode74});
			_override26.NodeSpacingAfter = 5;
			_override26.ShowEditorButtons = Infragistics.Win.UltraWinTree.ShowEditorButtons.Always;
			_override26.TipStyleNode = Infragistics.Win.UltraWinTree.TipStyleNode.Show;
			ultraTreeNode59.Override = _override26;
			ultraTreeNode59.Text = "Delivery Phrases";
			ultraTreeNode91.Key = "segLetter.varRecipients";
			ultraTreeNode91.LeftImages.Add(((object)(resources.GetObject("resource32"))));
			ultraTreeNode91.Text = "Recipients";
			ultraTreeNode92.Key = "segLetter.varSalutation";
			ultraTreeNode92.LeftImages.Add(((object)(resources.GetObject("resource33"))));
			ultraTreeNode92.Text = "Salutation";
			ultraTreeNode93.Key = "segLetter.varReline";
			ultraTreeNode93.LeftImages.Add(((object)(resources.GetObject("resource34"))));
			ultraTreeNode93.Text = "Reline";
			ultraTreeNode94.Key = "segLetter.segLetterhead";
			ultraTreeNode94.LeftImages.Add(((object)(resources.GetObject("resource35"))));
			ultraTreeNode95.Text = "Type";
			ultraTreeNode96.Text = "Include Phone";
			ultraTreeNode97.Text = "Include Fax";
			ultraTreeNode98.Text = "Include Title";
			ultraTreeNode94.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode95,
																								 ultraTreeNode96,
																								 ultraTreeNode97,
																								 ultraTreeNode98});
			ultraTreeNode94.Text = "Letterhead";
			ultraTreeNode99.LeftImages.Add(((object)(resources.GetObject("resource36"))));
			ultraTreeNode100.Text = "Closing Phrase";
			ultraTreeNode101.Text = "Include Firm Name";
			ultraTreeNode102.Text = "Include Title";
			ultraTreeNode99.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode100,
																								 ultraTreeNode101,
																								 ultraTreeNode102});
			ultraTreeNode99.Text = "Signature";
			ultraTreeNode57.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode58,
																								 ultraTreeNode59,
																								 ultraTreeNode91,
																								 ultraTreeNode92,
																								 ultraTreeNode93,
																								 ultraTreeNode94,
																								 ultraTreeNode99});
			ultraTreeNode57.Text = "Letter";
			ultraTreeNode103.LeftImages.Add(((object)(resources.GetObject("resource37"))));
			ultraTreeNode104.Text = "Authors";
			ultraTreeNode105.LeftImages.Add(((object)(resources.GetObject("resource38"))));
			ultraTreeNode106.Text = "Type";
			ultraTreeNode107.Text = "Include Phone";
			ultraTreeNode108.Text = "Include Fax";
			ultraTreeNode109.Text = "Include Title";
			ultraTreeNode105.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode106,
																								  ultraTreeNode107,
																								  ultraTreeNode108,
																								  ultraTreeNode109});
			ultraTreeNode105.Text = "Letterhead";
			ultraTreeNode103.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode104,
																								  ultraTreeNode105});
			ultraTreeNode103.Text = "Fax";
			ultraTreeNode110.LeftImages.Add(((object)(resources.GetObject("resource39"))));
			ultraTreeNode111.Text = "Type";
			ultraTreeNode112.Text = "Include Draft";
			ultraTreeNode113.Text = "Include Date";
			ultraTreeNode114.Text = "Include Time";
			ultraTreeNode110.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode111,
																								  ultraTreeNode112,
																								  ultraTreeNode113,
																								  ultraTreeNode114});
			ultraTreeNode110.Text = "Trailer";
			ultraTreeNode115.LeftImages.Add(((object)(resources.GetObject("resource40"))));
			ultraTreeNode115.Text = "Table Of Contents";
			this.utrEditVariables.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																									   ultraTreeNode57,
																									   ultraTreeNode103,
																									   ultraTreeNode110,
																									   ultraTreeNode115});
			_override27.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
			_override27.Multiline = Infragistics.Win.DefaultableBoolean.True;
			_override27.NodeStyle = Infragistics.Win.UltraWinTree.NodeStyle.Standard;
			_override27.SelectionType = Infragistics.Win.UltraWinTree.SelectType.Single;
			_override27.ShowEditorButtons = Infragistics.Win.UltraWinTree.ShowEditorButtons.Always;
			_override27.UseEditor = Infragistics.Win.DefaultableBoolean.True;
			this.utrEditVariables.Override = _override27;
			this.utrEditVariables.Size = new System.Drawing.Size(313, 526);
			this.utrEditVariables.TabIndex = 2;
			this.utrEditVariables.AfterCollapse += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.utrEditVariables_AfterCollapse);
			this.utrEditVariables.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.utrEditVariables_BeforeExpand);
			this.utrEditVariables.MouseDown += new System.Windows.Forms.MouseEventHandler(this.utrEditVariables_MouseDown);
			this.utrEditVariables.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.utrEditVariables_AfterSelect);
			this.utrEditVariables.AfterExpand += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.utrEditVariables_AfterExpand);
			this.utrEditVariables.BeforeLabelEdit += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.utrEditVariables_BeforeLabelEdit);
			// 
			// ultraTree2
			// 
			this.ultraTree2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ultraTree2.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.ultraTree2.ColumnSettings.RootColumnSet = ultraTreeColumnSet4;
			this.ultraTree2.FullRowSelect = true;
			this.ultraTree2.HideSelection = false;
			this.ultraTree2.Indent = 15;
			this.ultraTree2.Location = new System.Drawing.Point(4, 24);
			this.ultraTree2.Name = "ultraTree2";
			this.ultraTree2.NodeConnectorStyle = Infragistics.Win.UltraWinTree.NodeConnectorStyle.Inset;
			ultraTreeNode116.LeftImages.Add(((object)(resources.GetObject("resource41"))));
			ultraTreeNode117.Text = "Authors";
			ultraTreeNode118.Text = "Delivery Phrases";
			ultraTreeNode119.Text = "Recipients";
			ultraTreeNode120.Text = "Salutation";
			ultraTreeNode121.Text = "Reline";
			ultraTreeNode122.LeftImages.Add(((object)(resources.GetObject("resource42"))));
			ultraTreeNode123.Text = "Type";
			ultraTreeNode124.Text = "Include Phone";
			ultraTreeNode125.Text = "Include Fax";
			ultraTreeNode126.Text = "Include Title";
			ultraTreeNode122.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode123,
																								  ultraTreeNode124,
																								  ultraTreeNode125,
																								  ultraTreeNode126});
			ultraTreeNode122.Text = "Letterhead";
			ultraTreeNode127.LeftImages.Add(((object)(resources.GetObject("resource43"))));
			ultraTreeNode128.Text = "Closing Phrase";
			ultraTreeNode129.Text = "Include Firm Name";
			ultraTreeNode130.Text = "Include Title";
			ultraTreeNode127.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode128,
																								  ultraTreeNode129,
																								  ultraTreeNode130});
			ultraTreeNode127.Text = "Signature";
			ultraTreeNode116.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode117,
																								  ultraTreeNode118,
																								  ultraTreeNode119,
																								  ultraTreeNode120,
																								  ultraTreeNode121,
																								  ultraTreeNode122,
																								  ultraTreeNode127});
			ultraTreeNode116.Text = "Letter";
			ultraTreeNode131.LeftImages.Add(((object)(resources.GetObject("resource44"))));
			ultraTreeNode132.Text = "Authors";
			ultraTreeNode133.LeftImages.Add(((object)(resources.GetObject("resource45"))));
			ultraTreeNode134.Text = "Type";
			ultraTreeNode135.Text = "Include Phone";
			ultraTreeNode136.Text = "Include Fax";
			ultraTreeNode137.Text = "Include Title";
			ultraTreeNode133.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode134,
																								  ultraTreeNode135,
																								  ultraTreeNode136,
																								  ultraTreeNode137});
			ultraTreeNode133.Text = "Letterhead";
			ultraTreeNode131.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode132,
																								  ultraTreeNode133});
			ultraTreeNode131.Text = "Fax";
			ultraTreeNode138.LeftImages.Add(((object)(resources.GetObject("resource46"))));
			ultraTreeNode139.Text = "Type";
			ultraTreeNode140.Text = "Include Draft";
			ultraTreeNode141.Text = "Include Date";
			ultraTreeNode142.Text = "Include Time";
			ultraTreeNode138.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								  ultraTreeNode139,
																								  ultraTreeNode140,
																								  ultraTreeNode141,
																								  ultraTreeNode142});
			ultraTreeNode138.Text = "Trailer";
			ultraTreeNode143.LeftImages.Add(((object)(resources.GetObject("resource47"))));
			ultraTreeNode143.Text = "Table Of Contents";
			this.ultraTree2.Nodes.AddRange(new Infragistics.Win.UltraWinTree.UltraTreeNode[] {
																								 ultraTreeNode116,
																								 ultraTreeNode131,
																								 ultraTreeNode138,
																								 ultraTreeNode143});
			_override28.Multiline = Infragistics.Win.DefaultableBoolean.True;
			_override28.NodeStyle = Infragistics.Win.UltraWinTree.NodeStyle.Standard;
			_override28.SelectionType = Infragistics.Win.UltraWinTree.SelectType.Single;
			this.ultraTree2.Override = _override28;
			this.ultraTree2.ShowLines = false;
			this.ultraTree2.Size = new System.Drawing.Size(314, 526);
			this.ultraTree2.TabIndex = 34;
			this.ultraTree2.BeforeSelect += new Infragistics.Win.UltraWinTree.BeforeNodeSelectEventHandler(this.UltraTree2_BeforeSelect);
			// 
			// cmbVariableSets
			// 
			this.cmbVariableSets.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.cmbVariableSets.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			appearance60.BackColor = System.Drawing.SystemColors.Window;
			appearance60.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.cmbVariableSets.DisplayLayout.Appearance = appearance60;
			this.cmbVariableSets.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.cmbVariableSets.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance61.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance61.BorderColor = System.Drawing.SystemColors.Window;
			this.cmbVariableSets.DisplayLayout.GroupByBox.Appearance = appearance61;
			appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
			this.cmbVariableSets.DisplayLayout.GroupByBox.BandLabelAppearance = appearance62;
			this.cmbVariableSets.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance63.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance63.BackColor2 = System.Drawing.SystemColors.Control;
			appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
			this.cmbVariableSets.DisplayLayout.GroupByBox.PromptAppearance = appearance63;
			this.cmbVariableSets.DisplayLayout.MaxColScrollRegions = 1;
			this.cmbVariableSets.DisplayLayout.MaxRowScrollRegions = 1;
			appearance64.BackColor = System.Drawing.SystemColors.Window;
			appearance64.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmbVariableSets.DisplayLayout.Override.ActiveCellAppearance = appearance64;
			appearance65.BackColor = System.Drawing.SystemColors.Highlight;
			appearance65.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.cmbVariableSets.DisplayLayout.Override.ActiveRowAppearance = appearance65;
			this.cmbVariableSets.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.cmbVariableSets.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance66.BackColor = System.Drawing.SystemColors.Window;
			this.cmbVariableSets.DisplayLayout.Override.CardAreaAppearance = appearance66;
			appearance67.BorderColor = System.Drawing.Color.Silver;
			appearance67.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.cmbVariableSets.DisplayLayout.Override.CellAppearance = appearance67;
			this.cmbVariableSets.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.cmbVariableSets.DisplayLayout.Override.CellPadding = 0;
			appearance68.BackColor = System.Drawing.SystemColors.Control;
			appearance68.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance68.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance68.BorderColor = System.Drawing.SystemColors.Window;
			this.cmbVariableSets.DisplayLayout.Override.GroupByRowAppearance = appearance68;
			appearance69.TextHAlign = Infragistics.Win.HAlign.Left;
			this.cmbVariableSets.DisplayLayout.Override.HeaderAppearance = appearance69;
			this.cmbVariableSets.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.cmbVariableSets.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance70.BackColor = System.Drawing.SystemColors.Window;
			appearance70.BorderColor = System.Drawing.Color.Silver;
			this.cmbVariableSets.DisplayLayout.Override.RowAppearance = appearance70;
			this.cmbVariableSets.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance71.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cmbVariableSets.DisplayLayout.Override.TemplateAddRowAppearance = appearance71;
			this.cmbVariableSets.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.cmbVariableSets.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.cmbVariableSets.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.cmbVariableSets.DisplayMember = "";
			this.cmbVariableSets.Location = new System.Drawing.Point(20, 389);
			this.cmbVariableSets.Name = "cmbVariableSets";
			this.cmbVariableSets.Size = new System.Drawing.Size(307, 19);
			this.cmbVariableSets.TabIndex = 35;
			this.cmbVariableSets.Text = "Select a variable set to prefill variables";
			this.cmbVariableSets.ValueMember = "";
			this.cmbVariableSets.Visible = false;
			// 
			// splitter2
			// 
			this.splitter2.BackColor = System.Drawing.Color.Gray;
			this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter2.Location = new System.Drawing.Point(0, 554);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(322, 1);
			this.splitter2.TabIndex = 35;
			this.splitter2.TabStop = false;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.ultraTextEditor2);
			this.panel5.Controls.Add(this.label3);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel5.Location = new System.Drawing.Point(0, 555);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(322, 110);
			this.panel5.TabIndex = 34;
			// 
			// ultraTextEditor2
			// 
			this.ultraTextEditor2.AllowDrop = true;
			this.ultraTextEditor2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ultraTextEditor2.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
			this.ultraTextEditor2.Location = new System.Drawing.Point(3, 20);
			this.ultraTextEditor2.Multiline = true;
			this.ultraTextEditor2.Name = "ultraTextEditor2";
			this.ultraTextEditor2.Size = new System.Drawing.Size(316, 87);
			this.ultraTextEditor2.TabIndex = 21;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(6, 5);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 0;
			this.label3.Text = "&Description:";
			// 
			// ultraTabPageControl4
			// 
			this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabPageControl4.Name = "ultraTabPageControl4";
			this.ultraTabPageControl4.Size = new System.Drawing.Size(322, 665);
			// 
			// ultraTabControl1
			// 
			this.ultraTabControl1.AllowDrop = true;
			appearance72.BackColor = System.Drawing.Color.Gainsboro;
			appearance72.BackColor2 = System.Drawing.Color.White;
			appearance72.BackColorAlpha = Infragistics.Win.Alpha.Opaque;
			appearance72.BorderColor3DBase = System.Drawing.Color.White;
			this.ultraTabControl1.Appearance = appearance72;
			this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
			this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
			this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
			this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
			this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraTabControl1.Location = new System.Drawing.Point(5, 5);
			this.ultraTabControl1.Name = "ultraTabControl1";
			this.ultraTabControl1.ScrollArrowStyle = Infragistics.Win.UltraWinTabs.ScrollArrowStyle.VisualStudio;
			this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
			this.ultraTabControl1.ShowButtonSeparators = true;
			this.ultraTabControl1.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
			this.ultraTabControl1.Size = new System.Drawing.Size(322, 687);
			this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.StateButtons;
			this.ultraTabControl1.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
			this.ultraTabControl1.TabIndex = 0;
			appearance73.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			appearance73.BackColor2 = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(128)));
			this.ultraTabControl1.TabListButtonAppearance = appearance73;
			this.ultraTabControl1.TabPadding = new System.Drawing.Size(2, 2);
			ultraTab3.TabPage = this.ultraTabPageControl1;
			ultraTab3.Text = "&Content";
			ultraTab4.TabPage = this.ultraTabPageControl2;
			ultraTab4.Text = "&Variables";
			ultraTab5.TabPage = this.ultraTabPageControl4;
			ultraTab5.Text = "&Numbering";
			this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
																									   ultraTab3,
																									   ultraTab4,
																									   ultraTab5});
			this.ultraTabControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownHandler);
			// 
			// ultraTabSharedControlsPage1
			// 
			this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
			this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(322, 665);
			// 
			// ultraDataSource1
			// 
			ultraDataBand1.Columns.AddRange(new object[] {
															 ultraDataColumn1});
			this.ultraDataSource1.Band.ChildBands.AddRange(new object[] {
																			ultraDataBand1,
																			ultraDataBand2});
			this.ultraDataSource1.Band.Columns.AddRange(new object[] {
																		 ultraDataColumn2});
			this.ultraDataSource1.Band.Key = "Properties";
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// mnuSegment
			// 
			this.mnuSegment.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem1,
																					   this.menuItem2,
																					   this.menuItem3,
																					   this.menuItem4,
																					   this.menuItem19,
																					   this.menuItem25,
																					   this.menuItem20,
																					   this.menuItem8,
																					   this.menuItem9});
			this.mnuSegment.Popup += new System.EventHandler(this.mnuSegment_Popup);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "&Copy";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "&Delete";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "-";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 3;
			this.menuItem4.Text = "&Save Variables...";
			// 
			// menuItem19
			// 
			this.menuItem19.Index = 4;
			this.menuItem19.Text = "Save Variables As &Default";
			// 
			// menuItem25
			// 
			this.menuItem25.Index = 5;
			this.menuItem25.Text = "&Manage Variable Sets...";
			// 
			// menuItem20
			// 
			this.menuItem20.Index = 6;
			this.menuItem20.Text = "-";
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 7;
			this.menuItem8.Text = "Edit Author &Preferences...";
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 8;
			this.menuItem9.Text = "&Reset Styles";
			// 
			// mnuTOC
			// 
			this.mnuTOC.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				   this.menuItem10,
																				   this.menuItem11,
																				   this.menuItem12});
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 0;
			this.menuItem10.Text = "&Update";
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 1;
			this.menuItem11.Text = "&Delete";
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 2;
			this.menuItem12.Text = "&Change Tabs";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(314, 10);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(11, 11);
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			// 
			// mnuMacPac
			// 
			this.mnuMacPac.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem13,
																					  this.menuItem24,
																					  this.menuItem14,
																					  this.menuItem27,
																					  this.menuItem26,
																					  this.mnuEditVariables,
																					  this.menuItem15,
																					  this.menuItem16,
																					  this.menuItem17,
																					  this.menuItem18});
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 0;
			this.menuItem13.Text = "&Manage Authors...";
			// 
			// menuItem24
			// 
			this.menuItem24.Index = 1;
			this.menuItem24.Text = "Manage &Variable Sets...";
			// 
			// menuItem14
			// 
			this.menuItem14.Index = 2;
			this.menuItem14.Text = "&Edit Author Preferences...";
			// 
			// menuItem27
			// 
			this.menuItem27.Index = 3;
			this.menuItem27.Text = "-";
			// 
			// menuItem26
			// 
			this.menuItem26.Index = 4;
			this.menuItem26.Text = "Create &User Variable (simple vars)";
			this.menuItem26.Click += new System.EventHandler(this.menuItem26_Click);
			// 
			// mnuEditVariables
			// 
			this.mnuEditVariables.Index = 5;
			this.mnuEditVariables.Text = "Edit &Variables (visible to Admin only)";
			this.mnuEditVariables.Click += new System.EventHandler(this.mnuEditVariables_Click);
			// 
			// menuItem15
			// 
			this.menuItem15.Index = 6;
			this.menuItem15.Text = "-";
			// 
			// menuItem16
			// 
			this.menuItem16.Index = 7;
			this.menuItem16.Text = "Document &Font...";
			// 
			// menuItem17
			// 
			this.menuItem17.Index = 8;
			this.menuItem17.Text = "Show &XML Tags";
			// 
			// menuItem18
			// 
			this.menuItem18.Index = 9;
			this.menuItem18.Text = "&E-File Document...";
			// 
			// mnuContent
			// 
			this.mnuContent.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem22,
																					   this.menuItem49,
																					   this.menuItem21});
			// 
			// menuItem22
			// 
			this.menuItem22.Index = 0;
			this.menuItem22.Text = "Create New Document";
			// 
			// menuItem49
			// 
			this.menuItem49.Index = 1;
			this.menuItem49.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem50,
																					   this.menuItem51,
																					   this.menuItem52});
			this.menuItem49.Text = "Insert At";
			// 
			// menuItem50
			// 
			this.menuItem50.Index = 0;
			this.menuItem50.Text = "Selection";
			// 
			// menuItem51
			// 
			this.menuItem51.Index = 1;
			this.menuItem51.Text = "Start of Document";
			// 
			// menuItem52
			// 
			this.menuItem52.Index = 2;
			this.menuItem52.Text = "End of Document";
			// 
			// menuItem21
			// 
			this.menuItem21.Index = 2;
			this.menuItem21.Text = "Advanced Insert...";
			// 
			// mnuVariablesEdit
			// 
			this.mnuVariablesEdit.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.mitemCreateVariable,
																							 this.mitemDeleteVariable,
																							 this.mitemRenameVariable,
																							 this.mitemCreateAction,
																							 this.mitemDeleteAction,
																							 this.mitemSetProperty,
																							 this.menuItem28,
																							 this.menuItem46});
			// 
			// mitemCreateVariable
			// 
			this.mitemCreateVariable.Index = 0;
			this.mitemCreateVariable.Text = "Create Variable";
			// 
			// mitemDeleteVariable
			// 
			this.mitemDeleteVariable.Index = 1;
			this.mitemDeleteVariable.Text = "Delete Selected Variable";
			// 
			// mitemRenameVariable
			// 
			this.mitemRenameVariable.Index = 2;
			this.mitemRenameVariable.Text = "Rename Selected Variable";
			// 
			// mitemCreateAction
			// 
			this.mitemCreateAction.Index = 3;
			this.mitemCreateAction.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							  this.menuItem29,
																							  this.menuItem30,
																							  this.menuItem41,
																							  this.menuItem42,
																							  this.menuItem44,
																							  this.menuItem37,
																							  this.menuItem47,
																							  this.menuItem31,
																							  this.menuItem32,
																							  this.menuItem33,
																							  this.menuItem34,
																							  this.menuItem35,
																							  this.menuItem48,
																							  this.menuItem36,
																							  this.menuItem39,
																							  this.menuItem40,
																							  this.menuItem38,
																							  this.menuItem43,
																							  this.menuItem45});
			this.mitemCreateAction.Text = "Create Action";
			// 
			// menuItem29
			// 
			this.menuItem29.Index = 0;
			this.menuItem29.Text = "Insert Text";
			// 
			// menuItem30
			// 
			this.menuItem30.Index = 1;
			this.menuItem30.Text = "Insert Text As Table";
			// 
			// menuItem41
			// 
			this.menuItem41.Index = 2;
			this.menuItem41.Text = "Insert Boilerplate At Tag";
			// 
			// menuItem42
			// 
			this.menuItem42.Index = 3;
			this.menuItem42.Text = "Insert Boilerplate At Location";
			// 
			// menuItem44
			// 
			this.menuItem44.Index = 4;
			this.menuItem44.Text = "Insert Date Time";
			// 
			// menuItem37
			// 
			this.menuItem37.Index = 5;
			this.menuItem37.Text = "Insert Checkbox";
			// 
			// menuItem47
			// 
			this.menuItem47.Index = 6;
			this.menuItem47.Text = "-";
			// 
			// menuItem31
			// 
			this.menuItem31.Index = 7;
			this.menuItem31.Text = "Execute On Bookmark";
			this.menuItem31.Click += new System.EventHandler(this.menuItem31_Click);
			// 
			// menuItem32
			// 
			this.menuItem32.Index = 8;
			this.menuItem32.Text = "Execute On Document";
			// 
			// menuItem33
			// 
			this.menuItem33.Index = 9;
			this.menuItem33.Text = "Execute On Application";
			// 
			// menuItem34
			// 
			this.menuItem34.Index = 10;
			this.menuItem34.Text = "Execute On Style";
			// 
			// menuItem35
			// 
			this.menuItem35.Index = 11;
			this.menuItem35.Text = "Execute On Page Setup";
			// 
			// menuItem48
			// 
			this.menuItem48.Index = 12;
			this.menuItem48.Text = "-";
			// 
			// menuItem36
			// 
			this.menuItem36.Index = 13;
			this.menuItem36.Text = "Include/Exclude Text";
			// 
			// menuItem39
			// 
			this.menuItem39.Index = 14;
			this.menuItem39.Text = "Run Macro";
			// 
			// menuItem40
			// 
			this.menuItem40.Index = 15;
			this.menuItem40.Text = "Run Method";
			// 
			// menuItem38
			// 
			this.menuItem38.Index = 16;
			this.menuItem38.Text = "Set Variable Value";
			// 
			// menuItem43
			// 
			this.menuItem43.Index = 17;
			this.menuItem43.Text = "Set Document Variable Value";
			// 
			// menuItem45
			// 
			this.menuItem45.Index = 18;
			this.menuItem45.Text = "End Execution";
			// 
			// mitemDeleteAction
			// 
			this.mitemDeleteAction.Index = 4;
			this.mitemDeleteAction.Shortcut = System.Windows.Forms.Shortcut.Del;
			this.mitemDeleteAction.Text = "Delete Selected Action";
			// 
			// mitemSetProperty
			// 
			this.mitemSetProperty.Index = 5;
			this.mitemSetProperty.Text = "Set Property";
			// 
			// menuItem28
			// 
			this.menuItem28.Index = 6;
			this.menuItem28.Text = "Delete Property";
			// 
			// menuItem46
			// 
			this.menuItem46.Index = 7;
			this.menuItem46.Text = "Edit Property";
			// 
			// DocumentRegion
			// 
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.ultraTabControl1);
			this.DockPadding.All = 5;
			this.Name = "DocumentRegion";
			this.Size = new System.Drawing.Size(332, 697);
			this.Resize += new System.EventHandler(this.DocumentRegion_Resize);
			this.Load += new System.EventHandler(this.DocumentRegion_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DocumentRegion_KeyDown);
			this.ultraTabPageControl3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtPreview)).EndInit();
			this.ultraTabPageControl5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
			this.ultraTabPageControl1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtFind)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree3)).EndInit();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).EndInit();
			this.ultraTabControl2.ResumeLayout(false);
			this.ultraTabPageControl2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateValidationCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateTargetVariable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlValidateExecutionCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlAssociatedMVar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlMappings)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlExecutionCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlExpression)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDeleteScope)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlUnderlineLength)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDelimiterReplacement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlXMlTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlDefaultValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlControlType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ctlVisibleInTree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.utrEditVariables)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbVariableSets)).EndInit();
			this.panel5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
			this.ultraTabControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraDataSource1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		
		#region IRegion Members

		public Microsoft.InformationBridge.Framework.Interfaces.IRegionFrameProxy HostProxy
		{
			set
			{
				this.m_oFrameProxy = value;
			}
		}

		public System.Xml.XmlNode Data
		{
			set
			{
				this.m_oNode = value;
			}
		}

		public Microsoft.InformationBridge.Framework.Interfaces.FrameType HostType
		{
			set
			{
				this.m_oFrameType = value; 
			}
		}

		public Microsoft.InformationBridge.Framework.Interfaces.IVisualStyles VisualStyle
		{
			set
			{
				this.m_oVisualStyle = value;
			}
		}

		#endregion

		private void tvVariables_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
		
		}

		private void DocumentRegion_Load(object sender, System.EventArgs e)
		{
			IMediator mediator = this.m_oFrameProxy.Host.Mediator;

			System.Type ias = typeof(IApplicationService);
			object o = mediator.GetService(ias);

			IApplicationService iasApp = (IApplicationService)o;

			System.Object iasType = iasApp;

			try
			{
				wordApplication = (Word.ApplicationClass)iasApp.Application;
				wordApplication.XMLSelectionChange += new Microsoft.Office.Interop.Word.ApplicationEvents4_XMLSelectionChangeEventHandler(wordApplication_XMLSelectionChange);
			}
			catch{}

			this.m_oFrameProxy.Host.RegionMenu.AddMenuItem(
				"About MacPac", new EventHandler(ShowAboutMacPac));

			//MessageBox.Show("Loading");
			this.Height = wordApplication.ActiveWindow.Height - 25;

			for(int i = 0; i < this.ultraTree2.Nodes.Count; i++)
			{
				AddImages(this.ultraTree2.Nodes[i]);
			}

			DataSet oDS = new DataSet();

			DataTable oDT = oDS.Tables.Add();
			oDT.Columns.Add("Name");
			oDT.Columns.Add("Owner");
			oDT.Rows.Add(new object[]{"Current Letter", "Doug Miller"});
			oDT.Rows.Add(new object[]{"Current Fax", "Doug Miller"});
			oDT.Rows.Add(new object[]{"Dan's Letter", "Dan Fisherman"});
			oDT.Rows.Add(new object[]{"Jones v. Smith", "Linda Sackett"});

			this.cmbVariableSets.DataSource = oDS;

			oDT = new DataTable();
			oDT.Columns.Add();
			oDT.Rows.Add(new object[] {"Insert At Start of Document"});
			oDT.Rows.Add(new object[] {"Insert At End of Document"});
			oDT.Rows.Add(new object[] {"Insert At Cursor"});
			 
			//			this.label1.Text = wordApplication.ActiveDocument.Name;
			this.ultraTree1.ExpandAll();
			this.ultraTree2.CollapseAll();

			oDT = new DataTable();
			oDT.Columns.Add();
			oDT.Rows.Add(new object[] {"NumberOfLines=3"});
			oDT.Rows.Add(new object[] {"List=DeliveryPhrases"});

//			this.ugControlProperties.DataSource = oDT;

			Infragistics.Win.UltraWinTree.UltraTreeNode oNode = this.utrEditVariables.GetNodeByKey("segLetter.varDeliveryPhrases.Control.Properties");
			oNode.Override.ColumnSet = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();
			oNode.Override.ColumnSet.Columns.Add("Name");
			oNode.Override.ColumnSet.Columns.Add("Value");
			oNode.Override.ShowColumns = Infragistics.Win.DefaultableBoolean.True;

			oNode = this.utrEditVariables.GetNodeByKey(
				"segLetter.varDeliveryPhrases").Nodes.Insert(2,"AssociatedMVar", "Associated Tag:");

			oNode.Visible = true;
			oNode.Override.NodeSpacingAfter = 4;
			this.utrEditVariables.Indent = 8;
		}

		private void AddImages(Infragistics.Win.UltraWinTree.UltraTreeNode oParentNode)
		{
			if(oParentNode.HasNodes)
			{
				for(int i = 0; i < oParentNode.Nodes.Count; i++)
				{
					AddImages(oParentNode.Nodes[i]);
				}
			}
			else
			{
				if(oParentNode.LeftImages.Count == 0)
					oParentNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
						IMAGE_DIR + "VarEmpty.bmp"));
				oParentNode.LeftImagesAlignment = Infragistics.Win.VAlign.Top;
			}
		}

		private void wordApplication_XMLSelectionChange(Microsoft.Office.Interop.Word.Selection Sel, Microsoft.Office.Interop.Word.XMLNode OldXMLNode, Microsoft.Office.Interop.Word.XMLNode NewXMLNode, ref int Reason)
		{
		}
		private void ShowAboutMacPac(object sender, System.EventArgs e)
		{
			MessageBox.Show("This is the about MacPac form.");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Move Next");
		}

		private void DocumentRegion_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			MessageBox.Show(e.KeyCode.ToString());
		}

		private void tvVariables_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			MessageBox.Show("tvVariables " + e.KeyCode.ToString());
		}

		/// <summary>
		/// handles all key down events - used for hotkeys -
		/// all controls need to call this method on key down
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void KeyDownHandler(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.Control && e.Alt)
			{
				switch(e.KeyCode)
				{
					case Keys.C:
						this.ultraTabControl1.Tabs[0].Selected =true;
						break;
					case Keys.V:
						this.ultraTabControl1.Tabs[1].Selected =true;
						break;
					case Keys.D:
						this.ultraTabControl1.Tabs[2].Selected =true;
						break;
					case Keys.N:
						this.ultraTabControl1.Tabs[3].Selected =true;
						break;
					case Keys.T:
						this.ultraTabControl1.Tabs[4].Selected =true;
						break;
				}
			}
		}

		private void ultraTree1_DragLeave(object sender, System.EventArgs e)
		{
		
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			//			Infragistics.Win.UltraWinTree.UltraTreeNode oNode = new Infragistics.Win.UltraWinTree.UltraTreeNode("SampleVar", "Sample Variable");
			//			oNode.UIElement = new Infragistics.Win.UltraWinTree.TreeNodeUIElement(
			//			this.treeVariables.Nodes.Add
		}

		//		private void ultraTree1_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
		//		{
		//			this.utMacroFields.Nodes.Clear();
		//
		//			switch (this.ultraTree1.SelectedNodes[0].Key)
		//			{
		//				case "Labels":
		//				{
		//					//load node data for macro content
		//					string[] aNodeNames = new string[15];
		//					aNodeNames[0] = "Select a Variable Set:";
		//					aNodeNames[1] = "Recipients";
		//					aNodeNames[2] = "Bar Code";
		//					aNodeNames[3] = "Include Return Address";
		//					aNodeNames[4] = "Include USPS Standards";
		//					aNodeNames[5] = "Return Address";
		//					aNodeNames[6] = "Envelope Type";
		//					aNodeNames[7] = "Output Envelopes To";
		//					aNodeNames[8] = "Client Matter No.";
		//					aNodeNames[9] = "Author Name/Initials";
		//					aNodeNames[10] = "Delivery Phrases";
		//					aNodeNames[11] = "Font Name";
		//					aNodeNames[12] = "Font Size";
		//					aNodeNames[13] = "Left Indent";
		//					aNodeNames[14] = "Top Margin";
		////
		////					Infragistics.Win.UltraWinTree.UltraTreeNode oNode = this.utMacroFields.Nodes
		////						.Add(aNodeNames[0], aNodeNames[0]);
		////
		////					oNode.Override.NodeAppearance.ForeColor = Color.BlueViolet;
		////					oNode.Override.NodeSpacingAfter = 6;
		////					oNode.Override.NodeSpacingBefore = 2;
		////
		//
		//					for(int i = 1; i < aNodeNames.Length; i++)
		//					{
		//						Infragistics.Win.UltraWinTree.UltraTreeNode oNode = this.utMacroFields.Nodes
		//							.Add(aNodeNames[i], aNodeNames[i]);
		//						oNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
		//							@"D:\Work\MacPac\Images\VarEmpty.bmp"));
		//					}
		//
		//					this.utMacroFields.Nodes[0].Override.NodeSpacingBefore = 2;
		//					this.ultraTabControl2.SelectedTab = this.ultraTabControl2.Tabs[1];
		//					break;
		//				}
		//
		//				default:
		//					this.ultraTabControl2.SelectedTab = this.ultraTabControl2.Tabs[0];
		//					break;
		//			}
		//		}
		//
		private void UltraTree2_BeforeLabelEdit(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
		{
		}

		private void UltraTree2_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
		{
			int iLostSpace = 0;
			int iNewSelectionTop = e.NewSelections[0].Bounds.Y;

			//close previous node
			if(this.m_oCurNode != null)
			{
				//account for space lost when closing the current node
				if(this.m_oCurNode.UIElement != null && 
					e.NewSelections[0].UIElement.Rect.Top > this.m_oCurNode.UIElement.Rect.Top)
				{
					if(e.NewSelections[0].IsRootLevelNode)
					{
						//get next sibling node
						Infragistics.Win.UltraWinTree.UltraTreeNode oCurParent = null;
						if (m_oCurNode.IsRootLevelNode)
							oCurParent = m_oCurNode;
						else
							oCurParent = m_oCurNode.Parent;
						Infragistics.Win.UltraWinTree.UltraTreeNode oNextSibling =
							oCurParent.GetSibling(Infragistics.Win.UltraWinTree.NodePosition.Next);
						iLostSpace = oNextSibling.Bounds.Y - oCurParent.Bounds.Y -
							this.m_oCurNode.Override.NodeSpacingAfter + 5;
					}
					else
						iLostSpace = this.m_oCurNode.Override.NodeSpacingAfter;
				}

				//close current node
				this.m_oCurNode.Override.NodeSpacingAfter = 2;

				if(this.m_oCurEditCtl != null && this.m_oCurEditCtl != this.cmbVariableSets)
				{
					//store control value in NodeData
					NodeData oND = ((NodeData) this.m_oCurNode.Tag);
					oND.Value = this.m_oCurEditCtl.Text;

					//modify node display text to display new value
					this.m_oCurNode.Text = oND.PropertyName + ":  " + oND.Value;
					this.m_oCurNode.LeftImages.RemoveAt(0);
					this.m_oCurNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
						IMAGE_DIR + "VarFilled.bmp"));
				}
			}

			if(this.m_oCurEditCtl != null && this.m_oCurEditCtl != this.cmbVariableSets)
				this.ultraTabPageControl2.Controls.Remove(this.m_oCurEditCtl);

			//   Get the size and location of the cell
			Rectangle oRect = e.NewSelections[0].Bounds;

			//   Get the size and location of the node
			int left = oRect.X + this.ultraTree2.Location.X;
			int top = iNewSelectionTop + this.ultraTree2.Location.Y - iLostSpace;
			int width = this.ultraTree2.Width - left - 15;
			int height = oRect.Height;

			this.cmbVariableSets.Visible = false;

			if(e.NewSelections[0].IsRootLevelNode)
			{			
				this.ultraTree2.CollapseAll();
				e.NewSelections[0].Expanded = true;
				this.m_oCurEditCtl = this.cmbVariableSets;
				this.cmbVariableSets.Left = left + 20;
				this.cmbVariableSets.Top = top + 20;
				this.cmbVariableSets.Width = width - 20;
				this.cmbVariableSets.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
				try
				{
					this.cmbVariableSets.Text = "Select a variable set to prefill variables";
				}
				catch (System.Exception eE)
				{
				}
				this.cmbVariableSets.Visible = true;
			
				//add space below node
				e.NewSelections[0].Override.NodeSpacingAfter = 
					m_oCurEditCtl.Height + 5;
				this.m_oCurNode = e.NewSelections[0];

				//add node data struct to Tag if necessary -
				//NodeData struct stores info about node
				if(e.NewSelections[0].Tag == null)
				{
					NodeData oND = new NodeData();
					e.NewSelections[0].Tag = oND;

					//set node data property name to node text -
					//this will have to be changed when we do
					//the real thing
					oND.PropertyName = e.NewSelections[0].Text;
				}

				//assign appropriate context menu
				if (e.NewSelections[0].Text == "Table Of Contents")
					this.ultraTree2.ContextMenu = mnuTOC;
				else
					this.ultraTree2.ContextMenu = mnuSegment;
			}
			else 
			{
				if(System.Text.RegularExpressions.Regex.IsMatch(e.NewSelections[0].Text, 
					"Delivery Phrases.*"))
				{
					LMP.MP10.Controls.TreeMultiLineCombo oMLC = 
						new LMP.MP10.Controls.TreeMultiLineCombo(e.NewSelections[0]);

					oMLC.Height = 45;
					m_oCurEditCtl = oMLC;
				}
				else if(System.Text.RegularExpressions.Regex.IsMatch(e.NewSelections[0].Text, 
					"Recipients.*"))
				{
					//				LMP.MP10.Controls.RecipientList oRD = new LMP.MP10.Controls.RecipientList();
					LMP.MP10.Controls.RecipientDetail oD = new RecipientDetail();
					oD.ConfigString = "1�Name�&Name:�<DISPLAYNAME>��1�Address�&Address:�<Address>�5�3�DeliveryPhrases�&DeliveryPhrases:��";
					//				oRD.LabelColumnWidth = 0;
					oD.LabelSpacerWidth = 2;
					oD.Height = 160;
					m_oCurEditCtl = oD;
				}
				else
				{
					//create new control to add below node
					LMP.MP10.Controls.MPTextEditor oTB = 
						new LMP.MP10.Controls.MPTextEditor(e.NewSelections[0]);
					m_oCurEditCtl = oTB;
				}

				this.ultraTabPageControl2.Controls.Add(m_oCurEditCtl);

				//display control below node
				m_oCurEditCtl.Left = left + 20;
				m_oCurEditCtl.Top = top + 20;
				m_oCurEditCtl.Width = width - 20;
				m_oCurEditCtl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
				//oTB.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;

				m_oCurEditCtl.Visible = true;
				m_oCurEditCtl.BringToFront();
				m_oCurEditCtl.Focus();

				//add space below node
				e.NewSelections[0].Override.NodeSpacingAfter = 
					m_oCurEditCtl.Height + 5;
				this.m_oCurNode = e.NewSelections[0];

				//add node data struct to Tag if necessary -
				//NodeData struct stores info about node
				if(e.NewSelections[0].Tag == null)
				{
					NodeData oND = new NodeData();
					e.NewSelections[0].Tag = oND;

					//set node data property name to node text -
					//this will have to be changed when we do
					//the real thing
					oND.PropertyName = e.NewSelections[0].Text;
				}
				else
					//get value from existing NodeData
					this.m_oCurEditCtl.Text = ((NodeData)e.NewSelections[0].Tag).Value;

				//remove context menu
				this.ultraTree2.ContextMenu = null;
			}
		}

		private void UltraTree2_Scroll(object sender, Infragistics.Win.UltraWinTree.TreeScrollEventArgs e)
		{
			if(this.m_oCurNode.UIElement != null)
			{
				this.m_oCurEditCtl.Top = this.m_oCurNode.UIElement.Rect.Top + 
					this.ultraTree2.Location.Y + 20;
				this.m_oCurEditCtl.Visible = true;
			}
			else
				this.m_oCurEditCtl.Visible = false;
		}

		private void ultraButton2_Click(object sender, System.EventArgs e)
		{
			Form1 oF = new Form1();
			oF.ShowDialog();
		}

		private void ultraTree2_DragLeave(object sender, System.EventArgs e)
		{
		}

		private void ultraTree2_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
		}

		private void ultraTree2_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			MessageBox.Show("About to drop.");
		}

		private void ultraTree2_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				Infragistics.Win.UltraWinTree.UltraTreeNode oNode = 
					this.ultraTree2.GetNodeFromPoint(new Point(e.X, e.Y));

				oNode.Selected = true;

				if(oNode.IsRootLevelNode)
				{
					Point p = new Point(oNode.Bounds.Left, 
						oNode.Bounds.Top + oNode.Bounds.Height);

					this.mnuSegment.Show(this.ultraTree2,p);
				}
			}
		}

		private void ultraTree2_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		}

		private void ultraTree1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if((e.Button == MouseButtons.Left) && (this.ultraTree1.SelectedNodes.Count > 0))
			{
				
				DragDropEffects oEffect = this.ultraTree1.DoDragDrop(
					" ",DragDropEffects.Copy);

				if(oEffect == DragDropEffects.Copy)
				{
					InsertDocPart();
				}
			}
		}

		private void InsertDocPart()
		{
			wordApplication.Selection.InsertAfter("Appropriate boilerplate inserted here.");

			NodeData oND = (NodeData) this.ultraTree1.SelectedNodes[0].Tag;

			if(this.ultraTree1.SelectedNodes[0].Parent.Key == "Labels")
			{
				//add new node to Variables tree
				Infragistics.Win.UltraWinTree.UltraTreeNode oRootNode = 
					this.ultraTree2.Nodes.Add(this.ultraTree1.SelectedNodes[0].Text, 
					this.ultraTree1.SelectedNodes[0].Text);

				oRootNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
					IMAGE_DIR + "DocumentFragment.bmp"));

				//load node data for macro content
				string[] aNodeNames = new string[15];
				aNodeNames[0] = "Select a Variable Set:";
				aNodeNames[1] = "Recipients";
				aNodeNames[2] = "Bar Code";
				aNodeNames[3] = "Include Return Address";
				aNodeNames[4] = "Include USPS Standards";
				aNodeNames[5] = "Return Address";
				aNodeNames[6] = "Envelope Type";
				aNodeNames[7] = "Output Envelopes To";
				aNodeNames[8] = "Client Matter No.";
				aNodeNames[9] = "Author Name/Initials";
				aNodeNames[10] = "Delivery Phrases";
				aNodeNames[11] = "Font Name";
				aNodeNames[12] = "Font Size";
				aNodeNames[13] = "Left Indent";
				aNodeNames[14] = "Top Margin";

				for(int i = 1; i < aNodeNames.Length; i++)
				{
					Infragistics.Win.UltraWinTree.UltraTreeNode oNode = oRootNode.Nodes
						.Add(aNodeNames[i], aNodeNames[i]);
					oNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
						IMAGE_DIR + "VarEmpty.bmp"));
				}

				this.ultraTree2.Nodes[0].Override.NodeSpacingBefore = 2;
				this.ultraTabControl1.SelectedTab = this.ultraTabControl1.Tabs[1];
				this.ultraTree2.CollapseAll();
				this.ultraTree2.Nodes[this.ultraTree1.SelectedNodes[0].Text].Expanded = true;
						
				this.ultraTree2.Select();
				oRootNode.Selected = true;
				SendKeys.Send("{F10}(^{Tab})");
				this.ultraTree2.SelectedNodes[0].BeginEdit();
			}
			else
			{
				object oMissing = Type.Missing;
				if(oND != null)
					wordApplication.Selection.InsertXML(oND.Value, ref oMissing);
			}		
		}

		private void ultraTree1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string x = this.wordApplication.Selection.get_XML(false);

			Point p = this.ultraTree1.PointToClient(new Point(e.X,e.Y))	;
			Infragistics.Win.UltraWinTree.UltraTreeNode oSelNode = 
				this.ultraTree1.GetNodeFromPoint(p);
			Infragistics.Win.UltraWinTree.UltraTreeNode oNewNode = 
				oSelNode.Nodes.Add();

			oNewNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
			oNewNode.Override.NodeDoubleClickAction = Infragistics.Win.UltraWinTree
				.NodeDoubleClickAction.ToggleExpansionWhenExpansionIndicatorVisible;
			oNewNode.Text = "New Snippet";
			NodeData oND = new NodeData();
			oND.Value = x;
			oNewNode.Tag = oND;
			
			SendKeys.Send("{F10}(^{Tab})");

			for(int i = 1; i < 1000000; i++){}

			this.ultraTree1.Select();
			oNewNode.BeginEdit();
		}

		private void txtPreview_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
		}

		private void ultraTree1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			//this.ultraTree1.DoDragDrop("Dan Entering", DragDropEffects.Copy);
		}

		private void ultraTree1_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
		}

		//		private void btnMenu_Click(object sender, System.EventArgs e)
		//		{
		//			Point p = new Point(this.btnMenu.Left, this.btnMenu.Top + this.btnMenu.Height);
		//			this.mnuSegment.Show(this.btnMenu,p);
		//		}

		private void ultraTree2_AfterCollapse(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
		{
			this.ultraTabPageControl2.Controls.Remove(this.m_oCurEditCtl);
		}

		//		private void utMacroFields_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
		//		{
		//			int iLostSpace = 0;
		//
		//			//close previous node
		//			if(this.m_oCurMacroFieldsNode != null)
		//			{
		//				//account for space lost when closing the current node
		//				if(this.m_oCurMacroFieldsNode.UIElement != null && 
		//					e.NewSelections[0].UIElement.Rect.Top > this.m_oCurMacroFieldsNode.UIElement.Rect.Top)
		//				{
		//					iLostSpace = this.m_oCurMacroFieldsNode.Override.NodeSpacingAfter;
		//				}
		//
		//				//close current node
		//				this.m_oCurMacroFieldsNode.Override.NodeSpacingAfter = 2;
		//
		//				if(this.m_oCurMacroFieldsEditCtl != null)
		//				{
		//					//store control value in NodeData
		//					NodeData oND = ((NodeData) this.m_oCurMacroFieldsNode.Tag);
		//					oND.Value = this.m_oCurMacroFieldsEditCtl.Text;
		//
		//					//modify node display text to display new value
		//					this.m_oCurMacroFieldsNode.Text = oND.PropertyName + ":  " + oND.Value;
		//					this.m_oCurMacroFieldsNode.LeftImages.RemoveAt(0);
		//					this.m_oCurMacroFieldsNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
		//						@"D:\Work\MacPac\Images\VarFilled.bmp"));
		//				}
		//			}
		//
		//			if(this.m_oCurMacroFieldsEditCtl != null)
		//				this.utMacroFields.Controls.Remove(this.m_oCurMacroFieldsEditCtl);
		//
		//			//   Get the UIElement associated with the active cell, which we will
		//			//   need so we can get the size and location of the cell
		//			Rectangle oRect = e.NewSelections[0].UIElement.Rect;
		//			//oRect = utMacroFields.RectangleToScreen(oRect);
		//
		//			//   Get the size and location of the node
		//			int left = oRect.X + this.utMacroFields.Location.X;
		//			int top = oRect.Y + this.utMacroFields.Location.Y - iLostSpace;
		//			int width = this.utMacroFields.Width - left - 40;
		//			int height = oRect.Height;
		//
		//			if(System.Text.RegularExpressions.Regex.IsMatch(e.NewSelections[0].Text, 
		//				"Delivery Phrases.*"))
		//			{
		//				LMP.MP10.Controls.TreeMultiLineCombo oMLC = 
		//					new LMP.MP10.Controls.TreeMultiLineCombo(e.NewSelections[0]);
		//
		//				oMLC.Height = 45;
		//				m_oCurMacroFieldsEditCtl = oMLC;
		//			}
		//			else if(System.Text.RegularExpressions.Regex.IsMatch(e.NewSelections[0].Text, 
		//				"Recipients.*"))
		//			{
		//				LMP.MP10.Controls.RecipientDetail oD = new RecipientDetail();
		//				oD.ConfigString = "1�Name�&Name:�<DISPLAYNAME>��1�Address�&Address:�<Address>�5�3�DeliveryPhrases�&DeliveryPhrases:��";
		//				oD.LabelSpacerWidth = 2;
		//				oD.Height = 160;
		//				m_oCurMacroFieldsEditCtl = oD;
		//			}
		//			else
		//			{
		//				//create new control to add below node
		//				LMP.MP10.Controls.MPTextEditor oTB = 
		//					new LMP.MP10.Controls.MPTextEditor(e.NewSelections[0]);
		//				m_oCurMacroFieldsEditCtl = oTB;
		//			}
		//
		//			this.utMacroFields.Controls.Add(m_oCurMacroFieldsEditCtl);
		//
		//			//display control below node
		//			m_oCurMacroFieldsEditCtl.Left = left + 20;
		//			m_oCurMacroFieldsEditCtl.Top = top - 10;
		//			m_oCurMacroFieldsEditCtl.Width = width;
		//			m_oCurMacroFieldsEditCtl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
		//			//oTB.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
		//
		//			m_oCurMacroFieldsEditCtl.Visible = true;
		//			m_oCurMacroFieldsEditCtl.BringToFront();
		//			m_oCurMacroFieldsEditCtl.Focus();
		//
		//			//add space below node
		//			e.NewSelections[0].Override.NodeSpacingAfter = 
		//				m_oCurMacroFieldsEditCtl.Height + 5;
		//			this.m_oCurMacroFieldsNode = e.NewSelections[0];
		//
		//			//add node data struct to Tag if necessary -
		//			//NodeData struct stores info about node
		//			if(e.NewSelections[0].Tag == null)
		//			{
		//				NodeData oND = new NodeData();
		//				e.NewSelections[0].Tag = oND;
		//
		//				//set node data property name to node text -
		//				//this will have to be changed when we do
		//				//the real thing
		//				oND.PropertyName = e.NewSelections[0].Text;
		//			}
		//			else
		//				//get value from existing NodeData
		//				this.m_oCurMacroFieldsEditCtl.Text = ((NodeData)e.NewSelections[0].Tag).Value;
		//		}
		//
		private void label2_Click(object sender, System.EventArgs e)
		{
		
		}

		private void DocumentRegion_Resize(object sender, System.EventArgs e)
		{
			//MessageBox.Show("Resizing");
		}

		private void panel3_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void mnuSegment_Popup(object sender, System.EventArgs e)
		{
		
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
			MessageBox.Show(this.ultraTree2.SelectedNodes[0].Bounds.Y.ToString());
		}

		private void cmbVariables_ValueChanged(object sender, System.EventArgs e)
		{
			if (this.m_oCurNode.Text == "Letter")
			{
				switch (cmbVariableSets.SelectedRow.Cells[0].Text)
				{
					case "Dan's Letter":
						SetNodeText(this.m_oCurNode.Nodes[0], "Authors", "Dan Fisherman");
						SetNodeText(this.m_oCurNode.Nodes[1], "Delivery Phrases", "Confidential");
						SetNodeText(this.m_oCurNode.Nodes[2], "Recipients", "Charlie Homo");
						SetNodeText(this.m_oCurNode.Nodes[3], "Salutation", "Dear Charlie:");
						SetNodeText(this.m_oCurNode.Nodes[4], "Reline", "This is Dan's Letter");
						break;
					case "Jones v. Smith":
						SetNodeText(this.m_oCurNode.Nodes[0], "Authors", "Linda Sackett");
						SetNodeText(this.m_oCurNode.Nodes[1], "Delivery Phrases", "Privileged");
						SetNodeText(this.m_oCurNode.Nodes[2], "Recipients", "Valerie Melville");
						SetNodeText(this.m_oCurNode.Nodes[3], "Salutation", "Dear Valerie:");
						SetNodeText(this.m_oCurNode.Nodes[4], "Reline", "Jones v. Smith");
						break;
					default:
						break;
				}
			}
			//			else if (this.m_oCurNode.Text == "Fax")
			//			{
			//			}		
		}

		private void SetNodeText(Infragistics.Win.UltraWinTree.UltraTreeNode oNode,
			string xPropName, string xValue)
		{
			NodeData oND = new NodeData();
			oNode.Tag = oND;
			oND.PropertyName = xPropName;
			oND.Value = xValue;
			oNode.Text = oND.PropertyName + ":  " + oND.Value;
			oNode.LeftImages.RemoveAt(0);
			oNode.LeftImages.Add(System.Drawing.Bitmap.FromFile(
				IMAGE_DIR + "VarFilled.bmp"));
		}

		private void ultraButton2_Click_1(object sender, System.EventArgs e)
		{
			InsertDocPart();
		}

		private void pictureBox1_Click(object sender, System.EventArgs e)
		{
			Point p = new Point(this.pictureBox1.Left - 290, 
				this.pictureBox1.Top + this.pictureBox1.Height);
			this.mnuMacPac.Show(this.pictureBox1,p);
		}

		private void ultraTree1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				Infragistics.Win.UltraWinTree.UltraTreeNode oNode = 
					this.ultraTree1.GetNodeFromPoint(new Point(e.X, e.Y));
				oNode.Selected = true;
				this.mnuContent.Show(this.ultraTree1, new Point(e.X, e.Y));
			}	
		}

		private void menuItem26_Click(object sender, System.EventArgs e)
		{
		
		}

		private void mnuEditVariables_Click(object sender, System.EventArgs e)
		{
			this.mnuEditVariables.Checked = !this.mnuEditVariables.Checked;
			this.utrEditVariables.Visible = this.mnuEditVariables.Checked;
			this.ultraTree2.Visible = !this.mnuEditVariables.Checked;
		}

		private void utrEditVariables_BeforeLabelEdit(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
		{
			//MessageBox.Show("Before Label Edit");
		}

		private void utrEditVariables_BeforeExpand(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
		{
		}

		private void utrEditVariables_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
		{
			e.NewSelections[0].Expanded = true;
		}

		private void utrEditVariables_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				Infragistics.Win.UltraWinTree.UltraTreeNode oNode = this.utrEditVariables.GetNodeFromPoint(
					new Point(e.X,e.Y));

				//get last item in key
				string xKey = this.utrEditVariables.SelectedNodes[0].Key;
				int iPos = xKey.LastIndexOf('.');
				string xName = xKey.Substring(iPos + 1);
				if(Regex.IsMatch(this.utrEditVariables.SelectedNodes[0].Key,"seg.*"))
				{
					this.mitemCreateAction.Visible = false;
					this.mitemCreateVariable.Visible = true;
					this.mitemDeleteAction.Visible = false;
					this.mitemRenameVariable.Visible = true;
					this.mitemDeleteVariable.Visible = true;
				}
				else if(this.utrEditVariables.SelectedNodes[0].Key == "Actions")
				{
					this.mitemCreateAction.Visible = true;
					this.mitemCreateVariable.Visible = false;
					this.mitemDeleteAction.Visible = true;
					this.mitemRenameVariable.Visible = false;
					this.mitemDeleteVariable.Visible = false;
				}

				this.mnuVariablesEdit.Show(this.utrEditVariables,new Point(e.X, e.Y));
			}	
		}

		private void contextMenu1_Popup(object sender, System.EventArgs e)
		{
		
		}

		private void menuItem31_Click(object sender, System.EventArgs e)
		{
		
		}

		private void utrEditVariables_AfterExpand(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
		{
			this.SetControlVisibility();
		}

		private void utrEditVariables_AfterCollapse(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
		{
			this.SetControlVisibility();
		}

		private void SetControlVisibility()
		{
			bool bExpanded = this.utrEditVariables.GetNodeByKey("segLetter.varDeliveryPhrases").Expanded;

			this.ctlControlType.Visible = bExpanded;
			this.ctlDefaultValue.Visible = bExpanded;
			this.ctlVisibleInTree.Visible = bExpanded;
			this.btnDefaultValue.Visible = bExpanded;
			this.ctlAssociatedMVar.Visible = bExpanded;
			this.btnAssociatedMVar.Visible = bExpanded;

			bExpanded = this.utrEditVariables.GetNodeByKey("segLetter.varDeliveryPhrases.actionInsertText").Expanded;

			this.ctlDeleteScope.Visible = bExpanded;
			this.ctlDelimiterReplacement.Visible = bExpanded;
			this.ctlExecutionCondition.Visible = bExpanded;
			this.ctlExpression.Visible = bExpanded;
			this.ctlMappings.Visible = bExpanded;
			this.ctlUnderlineLength.Visible = bExpanded;
			this.ctlXMlTag.Visible = bExpanded;
			this.btnExecutionCondition.Visible = bExpanded;
			this.btnExpression.Visible = bExpanded;
			this.btnMappings.Visible = bExpanded;

			bExpanded = this.utrEditVariables.GetNodeByKey("segLetter.varDeliveryPhrases.Control.Action.Validate").Expanded;
			this.ctlValidateExecutionCondition.Visible = bExpanded;
			this.btnValidateExecutionCondition.Visible = bExpanded;
			this.ctlValidateTargetVariable.Visible = bExpanded;
			this.ctlValidateValidationCondition.Visible = bExpanded;
			this.btnValidateValidationCondition.Visible = bExpanded;
		}
	}
}
