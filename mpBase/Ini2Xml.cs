﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Xml;
using System.IO;

namespace LMP
{
    /// <summary>
    /// Convert an INI file into an XML file
    ///     Adapted from Ini2Xml by Loki
    ///     https://www.codeproject.com/Articles/2033/Convert-INI-file-to-XML
    ///     Changes:
    ///     -   Strip quotes from around Ini values
    ///     -   Format xml with linebreaks and indents
    /// </summary>
    public class INI2XML
	{
		/// <summary>
		/// Get all the section names from an INI file
		/// </summary>
		[ DllImport("kernel32.dll", EntryPoint="GetPrivateProfileSectionNamesA") ]
		public extern static int GetPrivateProfileSectionNames(
			[MarshalAs(UnmanagedType.LPArray)] byte[] lpReturnedString,
			int nSize,
			string lpFileName);

		/// <summary>
		/// Get all the settings from a section in a INI file
		/// </summary>
		[ DllImport("kernel32.dll", EntryPoint="GetPrivateProfileSectionA") ]
		public extern static int GetPrivateProfileSection(
			string lpAppName,
			[MarshalAs(UnmanagedType.LPArray)] byte[] lpReturnedString,
			int nSize,
			string lpFileName);
		
		/// <summary>
		/// Initial size of the buffer used when calling the Win32 API functions
		/// </summary>
		const int INITIAL_BUFFER_SIZE = 1024;

		/// <summary>
		/// Converts an INI file into an XML file.
		/// Output XML file has the following structure...
		///   <?xml version="1.0"?>
		///   <configuration>
		///       <section name="Main">
		///           <setting name="Timeout" value="90"/>
		///           <setting name="Mode" value="Live"/>
		///      </section>
		///   </configuration>
		/// Example:
		///	if (Loki.INI2XML.Convert( txtIniFileName.Text ))
		///		Console.WriteLine( "Successfully converted \"" + txtIniFileName.Text + "\" to xml" );
		///	else
		///		Console.WriteLine( "Problem converting \"" + txtIniFileName.Text + "\" to xml" );
		/// If an exception is raised, it is passed on to the caller.
		/// </summary>
		/// <param name="strINIFileName">File name of the INI file to convert</param>
		/// <returns>True if successfuly, or False if a problem</returns>
		public static bool Convert( string strINIFileName )
		{
			return Convert( strINIFileName, "" );
		}

		/// <summary>
		/// Converts an INI file into an XML file.
		/// Output XML file has the following structure...
		///   <?xml version="1.0"?>
		///   <configuration>
		///       <section name="Main">
		///           <setting name="Timeout" value="90"/>
		///           <setting name="Mode" value="Live"/>
		///      </section>
		///   </configuration>
		/// Example:
		///	if (Loki.INI2XML.Convert( txtIniFileName.Text, txtXMLFileName.Text ))
		///		Console.WriteLine( "Successfully converted \"" + txtIniFileName.Text + "\" to \"" + txtXMLFileName.Text + "\"" );
		///	else
		///		Console.WriteLine( "Problem converting \"" + txtIniFileName.Text + "\" to \"" + txtXMLFileName.Text + "\"" );
		/// If an exception is raised, it is passed on to the caller.
		/// </summary>
		/// <param name="strINIFileName">File name of the INI file to convert</param>
		/// <param name="strXMLFileName">File name of the XML file that is created</param>
		/// <returns>True if successfuly, or False if a problem</returns>
		public static bool Convert( string strINIFileName, string strXMLFileName )
		{
			char[] charEquals = {'='};
			string lpSections;
			int nSize;
			int nMaxSize;
			string strSection;
			int intSection;
			int intNameValue;
			string strName;
			string strValue;
			string strNameValue;
			string lpNameValues;
			byte[] str = new byte[1];
			XmlTextWriter oWriter = null;
			bool ok = false;

            try
            {
                if (strXMLFileName.Length == 0)
                {
                    //If no filename specified, use Ini name with .xml extension
                    strXMLFileName = Path.Combine(Path.GetDirectoryName(strINIFileName), string.Format("{0}.xml", Path.GetFileNameWithoutExtension(strINIFileName)));
                }

                // Get all sections names
                // Making sure allocate enough space for data returned
                // Thanks to Peter for his fix to this loop.
                // Peter G Jones, Microsoft .Net MVP, University of Canterbury, NZ
                nMaxSize = INITIAL_BUFFER_SIZE;
                while (true)
                {
                    str = new byte[nMaxSize];
                    nSize = GetPrivateProfileSectionNames(str, nMaxSize, strINIFileName);
                    if ((nSize != 0) && (nSize == (nMaxSize - 2)))
                    {
                        nMaxSize *= 2;
                    }
                    else
                    {
                        break;
                    }
                }

                // convert the byte array into a .NET string
                lpSections = Encoding.ASCII.GetString(str);

                // Use this for Unicode
                // lpSections = Encoding.Unicode.GetString( str );

                // Create XML File
                /*
                StringBuilder sb = new StringBuilder();
                xw = new XmlTextWriter( new StringWriter( sb ) );
                */

                oWriter = new XmlTextWriter(strXMLFileName, Encoding.UTF8);
                oWriter.Formatting = Formatting.Indented;

                // Write the opening xml
                oWriter.WriteStartDocument();
                oWriter.WriteStartElement("configuration");

                // Loop through each section
                char[] charNull = { '\0' };
                for (intSection = 0,
                    strSection = GetToken(lpSections, charNull, intSection);
                    strSection.Length > 0;
                    strSection = GetToken(lpSections, charNull, ++intSection))
                {
                    // Write a Node for the Section
                    oWriter.WriteStartElement("section");
                    oWriter.WriteAttributeString("name", strSection);

                    // Get all values in this section, making sure to allocate enough space
                    for (nMaxSize = INITIAL_BUFFER_SIZE,
                        nSize = nMaxSize;
                        nSize != 0 && nSize >= (nMaxSize - 2);
                        nMaxSize *= 2)
                    {
                        str = new Byte[nMaxSize];
                        nSize = GetPrivateProfileSection(strSection, str, nMaxSize, strINIFileName);
                    }

                    // convert the byte array into a .NET string
                    lpNameValues = Encoding.ASCII.GetString(str);

                    // Use this for Unicode
                    // lpNameValues = Encoding.Unicode.GetString( str );

                    // Loop through each Name/Value pair
                    for (intNameValue = 0,
                        strNameValue = GetToken(lpNameValues, charNull, intNameValue);
                        strNameValue.Length > 0;
                        strNameValue = GetToken(lpNameValues, charNull, ++intNameValue))
                    {
                        // Get the name and value from the entire null separated string of name/value pairs
                        // Also escape out the special characters, (ie. &"<> )
                        strName = GetToken(strNameValue, charEquals, 0);
                        if (strNameValue.Length > (strName.Length + 1))
                        {
                            strValue = strNameValue.Substring(strName.Length + 1);
                            strValue = strValue.TrimEnd('\"').TrimStart('\"');
                        }
                        else
                        {
                            strValue = "";
                        }

                        // Write the XML Name/Value Node to the xml file
                        oWriter.WriteStartElement("setting");
                        oWriter.WriteAttributeString("name", strName);
                        oWriter.WriteAttributeString("value", strValue);
                        oWriter.WriteEndElement();
                    }

                    // Close the section node
                    oWriter.WriteEndElement();
                }

                // Thats it
                oWriter.WriteEndElement();
                oWriter.WriteEndDocument();

                ok = true;
            }
            catch
            {
                ok = false;
            }
			finally
			{
				if ( oWriter != null )
				{
					oWriter.Close();
				}
			}

			return ok;
		} // Convert
	
		/// <summary>
		/// Get a token from a delimited string, eg.
		///   intSection = 0
		///   strSection = GetToken(lpSections, charNull, intSection)
		/// </summary>
		/// <param name="strText">Text that is delimited</param>
		/// <param name="delimiter">The delimiter, eg. ","</param>
		/// <param name="intIndex">The index of the token to return, NB. first token is index 0.</param>
		/// <returns>Returns the nth token from a string.</returns>
		private static string GetToken( string strText, char[] delimiter, int intIndex )
		{
			string strTokenRet = "";

			string[] strTokens = strText.Split( delimiter );

			if ( strTokens.GetUpperBound(0) >= intIndex )
				strTokenRet = strTokens[intIndex];

			return strTokenRet;
		} // GetToken
	} // class INI2XML
}
