﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LMP.Architect.Base;

namespace LMP.Architect.Oxml.Server
{
    public class XmlSegmentInsertion
    {
        /// <summary>
        /// inserts the specified segment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the segment definition</param>
        /// <param name="oLocation">the location to insert the segment</param>
        /// <param name="iInsertionOptions">options available for segment insertion</param>
        /// <param name="oMPDocument">MacPac document representing the Word document into which the segment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill segment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        /// <returns></returns>
        public static XmlSegment Create(string xID, XmlSegment oParent, XmlPrefill oPrefill)
        {
            return Create(xID, oParent, oPrefill, null, null, null, false, false);
        }

        public static XmlSegment Create(string xID, XmlSegment oParent, XmlPrefill oPrefill, string xBodyText, CollectionTableStructure oChildStructure)
        {
            return Create(xID, oParent, oPrefill, oChildStructure, xBodyText, null, false, false);
        }

        /// <summary>
        /// inserts the specified XmlSegment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the XmlSegment definition</param>
        /// <param name="oLocation">the location to insert the XmlSegment</param>
        /// <param name="iInsertionOptions">options available for XmlSegment insertion</param>
        /// <param name="oForteDocument">MacPac document representing the Word document into which the XmlSegment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill XmlSegment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="InsertingInHeaderFooter">Set to true by ContentManager when inserting in a Header/Footer</param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the XmlSegment definition is used</param>
        /// <returns></returns>
        public static XmlSegment Create(string xID, XmlSegment oParent, XmlPrefill oPrefill, CollectionTableStructure oChildStructure, string xBodyText, string xTemplate, bool bNewDocument, bool bDesign)
        {
            DateTime t0 = DateTime.Now;
            XmlForteDocument oForteDocument = new XmlForteDocument();
            XmlSegment.OxmlCreationMode iMode = XmlSegment.OxmlCreationMode.Default;
            //GLOG 15864
            if (bDesign)
            {
                iMode = XmlSegment.OxmlCreationMode.Design;
            }
            else if (bNewDocument)
            {
                iMode = XmlSegment.OxmlCreationMode.Finalize;
            }
            XmlSegment oSegment = LMP.Architect.Oxml.XmlSegment.CreateOxmlSegment(xID, oParent, oForteDocument, oPrefill, xBodyText, xTemplate, oChildStructure, iMode, 0, false);
            if (bNewDocument)
            {
                oSegment.WriteToSaveDirectory();
            }

            LMP.Benchmarks.Print(t0);
            return oSegment;
        }
    }
}
