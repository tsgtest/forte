using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using Win32 = Microsoft.Win32;

namespace mpReg
{
    /// <summary>
    /// this application registers CI, Numbering & Forte
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        //GLOG 4412
        static void Main()
        {
            string xTarget = "";
            try
            {
                Assembly oAsm = Assembly.GetExecutingAssembly();
                string xExe = Assembly.GetExecutingAssembly().Location;
                Process oProcess = new Process();

                //register MacPac10
                //GLOG 4837: Replace is case sensitive, so instead
                //get path from xExe and append to that
                string xPath = System.IO.Path.GetDirectoryName(xExe);
                xTarget = xPath + "\\Forte\\bin\\ForteReg.exe";
                oProcess.StartInfo.FileName = xTarget;
                oProcess.StartInfo.Arguments = "/s";
                oProcess.Start();

                //GLOG 8582: CI no longer has COM Components.
                //Numbering 10 has only tlb to register.

                ////register CI - needs the silent switch as it is not the default
                //xTarget = xPath + "\\CI\\App\\CIReg.exe";
                //oProcess.StartInfo.FileName = xTarget;
                //oProcess.StartInfo.Arguments = "/s";
                //oProcess.Start();

                //register Numbering
                xTarget = xPath + "\\Numbering\\bin\\tsgNumReg.exe";
                //xTarget = xPath + "\\Numbering\\App\\mpNumReg.exe";
                //GLOG : 8008 : JSW
                //check if .exe file is present
                if (System.IO.File.Exists(xTarget))
                {
                    oProcess.StartInfo.FileName = xTarget;
                    oProcess.StartInfo.Arguments = "/s";
                    oProcess.Start();
                }
             }
            catch (System.Exception oE)
            {
                System.Windows.Forms.MessageBox.Show(oE.Message + " '" + xTarget + "'", 
                    "Registration Error", MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
        }
    }
}