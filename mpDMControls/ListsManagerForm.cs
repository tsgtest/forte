using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using LMP.Data;


namespace LMP.Controls
{
    public partial class ListsManagerForm : Form
    {
        public ListsManagerForm()
        {
            InitializeComponent();

            //add listsmanager control
            Control oCtl;
            //late bind to Administration Controls
            //Assembly oA = Assembly.LoadWithPartialName("mpAdmin");
            Assembly oA = Assembly.Load("fAdmin"); //GLOG 7931 (dm)

            oCtl = (Control)oA.CreateInstance("LMP.Administration.Controls." +
                    "ListsManager");

            panel1.Controls.Add(oCtl);

            //fill panel with control
            oCtl.Dock = DockStyle.Fill;

        }
    }
}