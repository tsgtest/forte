﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using LMP.Controls;
using System.Collections;
using System.Windows.Forms;
using LMP.Data;
using Infragistics.Win.UltraWinTree;

namespace LMP.Controls
{
    public delegate void AfterSelectEventHandler(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e);

    public partial class SearchableFolderTreeView : UserControl
    {
        public event AfterSelectEventHandler AfterSelect;
        public event TreeDoubleClickEventHandler TreeDoubleClick;
        //GLOG : 6257 : CEH
        public event TreeKeyDownEventHandler TreeKeyDown;
        //GLOG : 15743 : jsw
        public Point m_oNodePoint;
        private bool m_bAllowSearching = false;
        //GLOG : 7276 : JSW
        private bool m_bAllowFavoritesMenu = false;
        private ArrayList m_aFindResults = null;
        private bool bInitialized = false;
        private bool m_bAdvancedSearchRunning = false; //GLOG 8499

        public SearchableFolderTreeView()
        {
            InitializeComponent();
            if (!this.DesignMode)
            {
                this.ftvMain.TreeDoubleClick += new TreeDoubleClickEventHandler(ftvMain_TreeDoubleClick);
                //GLOG : 6257 : CEH
                this.ftvMain.TreeKeyDown += new TreeKeyDownEventHandler(ftvMain_TreeKeyDown);
                //GLOG : 7276 : JSW
                mnuSegment_AddToFavorites.Text = LMP.Resources.GetLangString("Menu_Content_Segment_AddToFavorites");
                mnuSegment_RemoveFromFavorites.Text = LMP.Resources.GetLangString("Menu_Content_Segment_RemoveFromFavorites");
            }
        }

        //GLOG : 15743 : jsw
        public Point NodePoint
        {
            get
            {
                return m_oNodePoint;
            }
        }

        void ftvMain_TreeDoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (TreeDoubleClick != null)
                {
                    TreeDoubleClick(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6257 : CEH
        void ftvMain_TreeKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (TreeKeyDown != null)
                {
                    TreeKeyDown(this, new KeyEventArgs(e.KeyCode));
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #region *****************Properties******************
        public bool AllowSearching
        {
            get { return m_bAllowSearching; }
            set
            {
                if (bInitialized && value != m_bAllowSearching)
                {
                    m_bAllowSearching = value && ((this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.Segments) == FolderTreeView.mpFolderTreeViewOptions.Segments ||
                        (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.UserSegments) == FolderTreeView.mpFolderTreeViewOptions.UserSegments ||
                        (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.Prefills) == FolderTreeView.mpFolderTreeViewOptions.Prefills ||
                        (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly) == FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly ||
                        (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.MasterData) == FolderTreeView.mpFolderTreeViewOptions.MasterData); //GLOG 8163
                    this.pnlFind.Visible = m_bAllowSearching;
                    this.splitter1.Visible = m_bAllowSearching;
                }
                else
                {
                    m_bAllowSearching = value;
                }
            }
        }
        //GLOG : 7276 : JSW
        public bool AllowFavoritesMenu
        {
            get {return m_bAllowFavoritesMenu;}
            set
            {
                m_bAllowFavoritesMenu = value;
            }
        }

        /// <summary>
        /// Get/Set OwnerID for displayed objects
        /// </summary>
        public int OwnerID
        {
            get { return ftvMain.OwnerID; }
            set
            {
                ftvMain.OwnerID  = value;
            }
        }
        /// <summary>
        /// Get/Set which types of objects to display in the tree
        /// </summary>
        public FolderTreeView.mpFolderTreeViewOptions DisplayOptions
        {
            get { return ftvMain.DisplayOptions; }
            set
            {
                ftvMain.DisplayOptions  = value;
            }
        }
        public bool DisableExcludedTypes
        {
            get { return ftvMain.DisableExcludedTypes; }
            set
            {
                ftvMain.DisableExcludedTypes = value;
            }
        }
        //GLOG : 8380 : jsw
        public bool ExcludeNonMacPacTypes
        {
            get { return ftvMain.ExcludeNonMacPacTypes; }
            set
            {
                ftvMain.ExcludeNonMacPacTypes = value;
            }
        }

        /// <summary>
        /// Get/Set whether to display checkboxes for nodes
        /// </summary>
        public bool ShowCheckboxes
        {
            get { return ftvMain.ShowCheckboxes; }
            set
            {
                ftvMain.ShowCheckboxes = value;
            }

        }
        /// <summary>
        /// Get/Set which nodes will be expanded initially
        /// </summary>
        public FolderTreeView.mpFolderTreeViewExpansion Expansion
        {
            get { return ftvMain.Expansion; }
            set { ftvMain.Expansion = value; }
        }
        /// <summary>
        /// Get ArrayList of ArrayLists containing Folder ID, Name and Type
        /// </summary>
        public ArrayList FolderList
        {
            get
            {
                return ftvMain.FolderList;
            }
        }
        /// <summary>
        /// Get ArrayList of ArrayLists containing FolderMember ID, Name and ObjectType
        /// </summary>
        public ArrayList MemberList
        {
            get
            {
                return ftvMain.MemberList;
            }
        }
        /// <summary>
        /// Get delimited list of Segment or Folder IDs
        /// </summary>
        public string Value
        {
            get
            {
                return ftvMain.Value;
            }
        }
        public Infragistics.Win.UltraWinTree.UltraTree Tree
        {
            get 
            {
                if (treeResults.Visible)
                    return treeResults;
                else
                    return ftvMain;
            }
        }
        public FolderTreeView.mpFolderTreeViewModes Mode
        {
            get { return ftvMain.Mode; }
        }
        #endregion
        #region *****************Methods******************
        public void ExecuteFinalSetup()
        {
            m_bAllowSearching = m_bAllowSearching && ((this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.Segments) == FolderTreeView.mpFolderTreeViewOptions.Segments ||
                (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.UserSegments) == FolderTreeView.mpFolderTreeViewOptions.UserSegments ||
                (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.Prefills) == FolderTreeView.mpFolderTreeViewOptions.Prefills ||
                        (this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly) == FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly);
            this.pnlFind.Visible = m_bAllowSearching;
            this.splitter1.Visible = m_bAllowSearching;

            if (!m_bAllowSearching)
            {
                treeResults.Visible = false;
                ftvMain.Visible = true;
            }
            this.ftvMain.ExecuteFinalSetup();
        }

        // GLOG : 7276 : jsw
        /// <summary>
        /// Checks if Segment is Favorite
        /// </summary>
        /// <param name="oNode"></param>
        private bool IsFavorite(string xDisplayName)
        {
            try
            {

                // check if already in the list
                for (int i = 0; i < FavoriteSegments.Count; i++)
                {
                    string xName = FavoriteSegments.SegmentDisplayName(i);

                    if (xName == xDisplayName)
                        return true;
                }

            }
            catch { }

            //not in Favorites list
            return false;
        }
        //GLOG : 7276 : JSW
        /// <summary>
        /// Adds Segment to Favorites
        /// </summary>
        /// <param name="oNode"></param>
        private void AddToFavorites()
        {
            try
            {

                UltraTreeNode oNode = this.ftvMain.ActiveNode;

                if (FavoriteSegments.Count < FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES)
                {
                    //GLOG 6295 (dm) - saved data was supplying segment id and name
                    //instead of its own - use following three variables throughout this
                    //method rather than assuming segment def
                    string xSegName;
                    string xID;
                    string xDisplayName;
                    //GLOG : 6303 : jsw
                    //add intended use icon to favorites 
                    mpSegmentIntendedUses tIntendedUse;
                    if (TreeHelper.IsVariableSetNode(oNode))
                    {
                        VariableSetDef oDef = TreeHelper.GetVariableSetFromNode(oNode);
                        xSegName = oDef.Name;
                        xID = oDef.ID;
                        xDisplayName = oDef.Name;
                        tIntendedUse = mpSegmentIntendedUses.AsDocument;
                    }
                    else
                    {
                        ISegmentDef oDef = TreeHelper.GetSegmentDefFromNode(oNode);
                        xSegName = oDef.Name;
                        xID = TreeHelper.GetSegmentIDFromNode(oNode);
                        xDisplayName = oDef.DisplayName;
                        tIntendedUse = oDef.IntendedUse;
                    }

                    // GLOG : 7276 : jsw
                    // check if already in the list
                    if (IsFavorite(xDisplayName))
                        return;

                    // favorite not in list, ok to add
                    // get segment type
                    mpFolderMemberTypes iType = 0;

                    if (TreeHelper.IsUserSegmentNode(oNode))
                    {
                        iType = mpFolderMemberTypes.UserSegment;
                    }
                    else if (TreeHelper.IsVariableSetNode(oNode))
                    {
                        iType = mpFolderMemberTypes.VariableSet;
                    }
                    else
                    {
                        iType = mpFolderMemberTypes.AdminSegment;
                    }

                    // Add segment to Favorites
                    FavoriteSegments.Add(xSegName, xID, iType, xDisplayName, tIntendedUse);

                    // Save Favorites list
                    FavoriteSegments.Save();

                }
                else
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FavoritesLimitReached"),
                        FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        #endregion
        #region *****************Event Handlers******************
        private void txtFindContent_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ftvMain_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }

        private void ftvMain_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            if (AfterSelect != null)
            {
                AfterSelect(this, e);
                m_oNodePoint = Cursor.Position;
            }
        }
        /// <summary>
        /// When Enter is pressed in Find Textbox, toggle Find checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFindContent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //GLOG 8163: KeyCode was not matching here
                if (e.KeyValue  == (int)Keys.Return)
                {
                    e.SuppressKeyPress = true;
                    if (txtFindContent.Text != "")
                    {
                        chkFindContent.Checked = !chkFindContent.Checked;
                    }
                    else if (chkFindContent.Checked)
                        chkFindContent.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clear find results if text is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFindContent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkFindContent.Checked == true)
                {
                    this.chkFindContent.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Toggle between Find Results and Content trees
        /// Check if there are matching results before display find results tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkFindContent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 8499 : JSW
                if (m_bAdvancedSearchRunning == true)
                    return;

                if (this.txtFindContent.Text == "")
                {
                    chkFindContent.Checked = false;
                    //treeResults.Visible = false;
                    //ftvMain.Visible = true;
                    m_aFindResults = null;
                    ftvMain.FilterText = "";
                    ftvMain.Mode = FolderTreeView.mpFolderTreeViewModes.FolderTree;
                }
                else if (chkFindContent.Checked)
                {
                    if (ftvMain.FilterText != this.txtFindContent.Text)
                        this.ftvMain.FilterText = this.txtFindContent.Text;
                    else
                    {
                        //GLOG 8163
                        if (m_aFindResults == null)
                        {
                            this.ftvMain.FilterText = this.txtFindContent.Text;
                        }
                        else
                            this.ftvMain.Mode = FolderTreeView.mpFolderTreeViewModes.FindResults;
                    }

                    if (ftvMain.Mode != FolderTreeView.mpFolderTreeViewModes.FindResults)
                    {
                        //No Matches found
                        // The find string is appended a carriage return line feed. 
                        //Remove it to fix situation where subseqent
                        // search contains this find string.
                        //GLOG 6031: Show different message if Preference Segments are being displayed
                        string xMsg = LMP.Resources.GetLangString("Msg_No_Matching_Segments");
                        if ((this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly) == FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly)
                            xMsg = LMP.Resources.GetLangString("Msg_No_Matching_Preference_Segments");
                        MessageBox.Show(xMsg,
                            LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);

                        string xCRLF = "\r\n";

                        if (this.txtFindContent.Text.EndsWith(xCRLF))
                        {
                            this.txtFindContent.Text = this.txtFindContent.Text.Remove(
                                this.txtFindContent.Text.IndexOf(xCRLF));
                        }

                        chkFindContent.Checked = false;
                        txtFindContent.Focus();
                        //ftvMain.Visible = true;
                        //treeResults.Visible = false;
                    }
                }
                else
                {
                    //GLOG 8163
                    m_aFindResults = null;
                    //Return to folder tree view
                    ftvMain.Mode = FolderTreeView.mpFolderTreeViewModes.FolderTree;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {

                if (chkFindContent.Checked)
                    this.chkFindContent.BackColor = Color.SlateGray;
                else
                {
                    if (!m_bAdvancedSearchRunning) //GLOG 8499
                        m_aFindResults = null;
                    this.chkFindContent.BackColor = Color.Transparent;
                }
            }
        }
        private void treeResults_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            if (AfterSelect != null)
                AfterSelect(this, e);
        }

        private void treeResults_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }
        private void mnuContext_Clear_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.txtFindContent.Text = "";
                this.txtFindContent.Focus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeResults_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (TreeDoubleClick != null)
                {
                    TreeDoubleClick(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6244 : CEH
        private void chkFindContent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Return)
                {
                    e.SuppressKeyPress = true;
                    if (txtFindContent.Text != "")
                    {
                        chkFindContent.Checked = !chkFindContent.Checked;
                    }
                    else if (chkFindContent.Checked)
                        chkFindContent.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void ftvMain_Scroll(object sender, TreeScrollEventArgs e)
        {
            //GLOG item #7003 - dcf
            this.ftvMain.Refresh();
        }

        //GLOG : 7276 : JSW
        private void mnuSegment_AddToFavorites_Click(object sender, EventArgs e)
        {

           AddToFavorites();
        }

        //GLOG : 7276 : JSW
        private void ftvMain_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    
                    UltraTreeNode oNode = this.ftvMain.GetNodeFromPoint(e.X, e.Y);

                    if (oNode != null)
                    {
                        oNode.Selected = true;
                        this.ftvMain.ActiveNode = oNode;

                    }
                    
                    if (this.AllowFavoritesMenu && oNode.Tag is FolderMember)
                    {
                        //check if already in favorites and display suitable option
                        if (IsFavorite(oNode.Text))
                        {
                            this.mnuSegment_AddToFavorites.Visible = false;
                            this.mnuSegment_RemoveFromFavorites.Visible = true;
                        }
                        else
                        {
                            this.mnuSegment_AddToFavorites.Visible = true;
                            this.mnuSegment_RemoveFromFavorites.Visible = false;
                        }

                        this.mnuSegment.Show(oNode.Control, e.X, e.Y);
                    }
                }
            }
            catch { }
        }
        //GLOG : 7276 : JSW
        private void mnuSegment_RemoveFromFavorites_Click(object sender, EventArgs e)
        {
            string xSegName = this.ftvMain.ActiveNode.Text;
            FavoriteSegments.Remove(xSegName);
            FavoriteSegments.Save();

        }
        //GLOG : 8348 : jsw
        private void btnFindContentAdvanced_Click(object sender, EventArgs e)
        {
            try
            {
                ContentSearchForm oForm = new ContentSearchForm();
                if (this.txtFindContent.Text != "")
                    oForm.SearchText = this.txtFindContent.Text;
                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    if (oForm.SearchText == "" && oForm.SearchFolder == ContentSearchForm.mpSearchLocations.AllFolders &&
                        oForm.OwnerID == 0 && oForm.ContentType == ContentSearchForm.mpSearchContentType.AllContent)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_NoSearchCriteriaSelected"),
                            LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    string xSearchText = oForm.SearchText;
                    if (xSearchText == "")
                        xSearchText = " "; //Set Search Text to non-empty value to use other search criteria
                    int iOwnerID = oForm.OwnerID;
                    string xFolderIDList = "";
                    bool bIncludeAdminContent = true;
                    bool bIncludeUserContent = true;
                    m_bAdvancedSearchRunning = true;

                    ftvMain.SearchContentType = (FolderTreeView.mpFolderTreeSearchContentTypes)oForm.ContentType; 
                    ftvMain.SearchFields = oForm.SearchField;
                    ftvMain.SearchUserID = iOwnerID;

                    //get folder list
                    if (oForm.SearchFolder != LMP.Controls.ContentSearchForm.mpSearchLocations.AllFolders)
                    {
                        UltraTreeNode oNode = this.ftvMain.ActiveNode;
                        if (oNode.Key == "0") //search results node
                            return;
                        if (oNode != null)
                        {
                            TreeHelper.LoadNodeReference(oNode);
                            //If My Folders or Public node is selected, include all user or admin content
                            if (TreeHelper.IsPublicFolderNode(oNode))
                            {
                                bIncludeUserContent = false;
                            }
                            else if (TreeHelper.IsMyFolderNode(oNode))
                            {
                                //Search all User Folders
                                xFolderIDList = this.GetSubFolderIDs(oNode);
                            }
                            else if (oForm.SearchFolder == LMP.Controls.ContentSearchForm.mpSearchLocations.SelectedFolder)
                            {
                                if (TreeHelper.IsUserFolderNode(oNode))
                                {
                                    xFolderIDList = ((UserFolder)oNode.Tag).ID;
                                    bIncludeAdminContent = false;
                                }
                                else if (TreeHelper.IsAdminFolderNode(oNode))
                                {
                                    xFolderIDList = ((Folder)oNode.Tag).ID.ToString();
                                    bIncludeUserContent = false;
                                }
                                else if (oNode.Tag is FolderMember || oNode.Tag is UserSegmentDef || oNode.Tag is AdminSegmentDef || oNode.Tag is VariableSetDef)
                                {
                                    oNode = oNode.Parent;
                                    if (oNode.Tag is Folder)
                                    {
                                        xFolderIDList = ((Folder)oNode.Tag).ID.ToString();
                                        bIncludeUserContent = false;
                                    }
                                    else if (oNode.Tag is UserFolder)
                                    {
                                        xFolderIDList = ((UserFolder)oNode.Tag).ID;
                                        bIncludeAdminContent = false;
                                    }
                                }
                            }
                            else if (oForm.SearchFolder == LMP.Controls.ContentSearchForm.mpSearchLocations.SelectedFolderAndSubfolders)
                            {
                                xFolderIDList = this.GetSubFolderIDs(oNode);
                                if (xFolderIDList.Contains("."))
                                    bIncludeAdminContent = false;
                                else
                                    bIncludeUserContent = false;
                            }
                        }
                    }

                    this.ftvMain.SearchFolderList = xFolderIDList;
                    this.ftvMain.IncludeAdminContentInSearch = bIncludeAdminContent;
                    this.ftvMain.IncludeUserContentInSearch = bIncludeUserContent;

                    this.ftvMain.AdvancedFilterText = xSearchText;

                    if (ftvMain.Mode != FolderTreeView.mpFolderTreeViewModes.AdvancedFindResults)
                    {
                        //No Matches found
                        // The find string is appended a carriage return line feed. 
                        //Remove it to fix situation where subseqent
                        // search contains this find string.
                        //GLOG 6031: Show different message if Preference Segments are being displayed
                        string xMsg = LMP.Resources.GetLangString("Msg_No_Matching_Segments");
                        if ((this.DisplayOptions & FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly) == FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly)
                            xMsg = LMP.Resources.GetLangString("Msg_No_Matching_Preference_Segments");
                        MessageBox.Show(xMsg,
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // The find string is appended a carriage return line feed. 
                        //Remove it to fix situation where subseqent
                        // search contains this find string.
                        string xCRLF = "\r\n";

                        if (this.txtFindContent.Text.EndsWith(xCRLF))
                        {
                            this.txtFindContent.Text = this.txtFindContent.Text.Remove(
                                this.txtFindContent.Text.IndexOf(xCRLF));
                        }

                        txtFindContent.Focus();
                        this.chkFindContent.Checked = false;
                    }
                    else
                    {
                        this.txtFindContent.Text = oForm.SearchText;
                        this.chkFindContent.Checked = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bAdvancedSearchRunning = false;
            }

        }
        private string GetSubFolderIDs(UltraTreeNode oNode)
        {
            string xFolderList = "";
            if (TreeHelper.IsMyFolderNode(oNode))
            {
                UserFolders oChildren = new UserFolders(LMP.Data.Application.User.ID);

                for (int i = 1; i <= oChildren.Count; i++)
                {
                    UserFolder oChild = (UserFolder)oChildren.ItemFromIndex(i);
                    GetUserSubFolderList(oChild, ref xFolderList);
                }
            }
            else
            {
                if (oNode.Tag is FolderMember || oNode.Tag is UserSegmentDef || oNode.Tag is AdminSegmentDef || oNode.Tag is VariableSetDef)
                {
                    oNode = oNode.Parent;
                }
                if (TreeHelper.IsUserFolderNode(oNode))
                {
                    UserFolder oParent = (UserFolder)oNode.Tag;
                    GetUserSubFolderList(oParent, ref xFolderList);
                }
                else if (TreeHelper.IsAdminFolderNode(oNode))
                {
                    Folder oParent = (Folder)oNode.Tag;
                    GetAdminSubFolderList(oParent, ref xFolderList);
                }
            }
            xFolderList = xFolderList.TrimEnd(',');
            return xFolderList;
        }
        private void GetUserSubFolderList(UserFolder oFolder, ref string xFolderList)
        {
            xFolderList = xFolderList + oFolder.ID + ",";
            string xParentID = oFolder.ID;
            int iID1 = 0;
            int iID2 = 0;
            LMP.Data.Application.SplitID(xParentID, out iID1, out iID2);
            UserFolders oFolders = new UserFolders(LMP.Data.Application.User.ID, iID1, iID2);
            for (int i = 1; i <= oFolders.Count; i++)
            {
                GetUserSubFolderList((UserFolder)oFolders.ItemFromIndex(i), ref xFolderList);
            }
        }
        private void GetAdminSubFolderList(Folder oFolder, ref string xFolderList)
        {
            xFolderList = xFolderList + oFolder.ID.ToString() + ",";
            Folders oFolders = new Folders(0, oFolder.ID);
            for (int i = 1; i <= oFolders.Count; i++)
            {
                GetAdminSubFolderList((Folder)oFolders.ItemFromIndex(i), ref xFolderList);
            }
        }
        //private void ShowFindResults()
        //{
        //    if (m_aFindResults == null)
        //        return;
        //    treeResults.SuspendLayout();
        //    if (treeResults.Nodes.Count == 0)
        //    {
        //        //Add top-level Search Results node
        //        UltraTreeNode oTopNode = this.treeResults.Nodes.Add(
        //            "0", LMP.Resources.GetLangString("Prompt_SearchResults"));
        //        // oTopNode.Override.ExpandedNodeAppearance.Image = Images.SearchResults;
        //        oTopNode.Expanded = true;
        //        oTopNode.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
        //        oTopNode.Override.NodeSpacingBefore = 5;
        //        oTopNode.Override.SelectionType = SelectType.None;
        //        oTopNode.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;
        //    }
        //    else
        //    {
        //        treeResults.Nodes[0].Nodes.Clear();
        //    }
        //    for (int i = 0; i < m_aFindResults.Count; i++)
        //    {
        //        // Use DisplayName for Node Text, ID for Key and Tag
        //        object[] aFindResult = (object[])m_aFindResults[i];
        //        string xName = aFindResult[0].ToString();
        //        string xID = @aFindResult[1].ToString();
        //        mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)(int.Parse(aFindResult[2].ToString()));
        //        mpSegmentIntendedUses iUse = (mpSegmentIntendedUses)(int.Parse(aFindResult[3].ToString()));  //GLOG 8037

        //        //add node to tree
        //        UltraTreeNode oTreeNode = null;
        //        switch (shMemberType)
        //        {
        //            case mpFolderMemberTypes.UserSegment:
        //                //UserSegment
        //                oTreeNode = treeResults.Nodes[0].Nodes.Add("U" + xID, xName);
        //                oTreeNode.Tag = (object)string.Concat("U", xID);
        //                //oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.UserSegment, oTreeNode); //GLOG 8376
        //                //oTreeNode.Override.NodeAppearance.Image = FolderTreeView.GetImage
        //                //oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.UserSegment, oTreeNode); //GLOG 8376
        //                break;
        //            case mpFolderMemberTypes.VariableSet:
        //                //VariableSet/Prefill
        //                oTreeNode = treeResults.Nodes[0].Nodes.Add("P" + xID, xName);
        //                oTreeNode.Tag = (object)string.Concat("P", xID);
        //                //oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.VariableSet, false); //GLOG 8376
        //                //oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.VariableSet, false); //GLOG 8376
        //                break;
        //            default:
        //                //AdminSegment
        //                oTreeNode = treeResults.Nodes[0].Nodes.Add("A" + xID, xName);
        //                oTreeNode.Tag = (object)string.Concat("A", xID);
        //                //oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.AdminSegment, oTreeNode);
        //                //oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.AdminSegment, oTreeNode);
        //                break;
        //        }
        //        oTreeNode.Override.CellClickAction = CellClickAction.Default;
        //        //GLOG : 8308 : JSW
        //        //if (Session.CurrentUser.UserSettings.ExpandFindResults)
        //        //    ExpandNodeShowSegmentPath(oTreeNode);
        //    }
        //    treeResults.ResumeLayout();
        //}
        ///// <summary>
        /// returns an image for the specified intended use
        /// </summary>
        /// <param name="iIntendedUse"></param>
        /// <returns></returns>
        //private Images GetImage(mpSegmentIntendedUses iIntendedUse, mpFolderMemberTypes iMemberType, bool bIsDesignerSegment) //GLOG 8376
        //{

        //    //get icon based on intended use
        //    if (iMemberType == mpFolderMemberTypes.VariableSet)
        //    {
        //        //VariableSet may reference Data Interview or Standalone Segment
        //        if (iIntendedUse == mpSegmentIntendedUses.AsAnswerFile)
        //            // GLOG : 3041 : JAB
        //            // User answer files are VariableSets with intended use AsAnswerFile.
        //            // Use the icon for user answer file segment.
        //            return Images.UserSegmentPacket;
        //        else
        //            return Images.Prefill;
        //    }
        //    else
        //    {
        //        switch (iIntendedUse)
        //        {
        //            //GLOG 8376: Show Admin content icons for Designer Segments
        //            case mpSegmentIntendedUses.AsDocument:
        //                return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserDocument : Images.DocumentSegment;
        //            case mpSegmentIntendedUses.AsDocumentComponent:
        //                return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserComponentSegment : Images.ComponentSegment;
        //            case mpSegmentIntendedUses.AsParagraphText:
        //                return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserParagraphText : Images.ParagraphTextSegment;
        //            case mpSegmentIntendedUses.AsSentenceText:
        //                return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserSentenceText : Images.SentenceTextSegment;
        //            case mpSegmentIntendedUses.AsStyleSheet:
        //                // GLOG : 3041 : JAB
        //                // Return image index for user styles sheet segments.
        //                return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserStyleSheetSegment : Images.StyleSheetSegment;
        //            case mpSegmentIntendedUses.AsAnswerFile:
        //                // GLOG : 3041 : JAB
        //                // Return image index for answer file segments.
        //                switch (iMemberType)
        //                {
        //                    case mpFolderMemberTypes.UserSegment:
        //                        return Images.UserSegmentPacket;
        //                    case mpFolderMemberTypes.UserSegmentPacket:
        //                        return Images.UserSegmentPacket;
        //                    default:
        //                        return Images.SegmentPacket;
        //                }
        //            case mpSegmentIntendedUses.AsMasterDataForm:
        //                return Images.MasterDataForm;
        //            default:
        //                return Images.DocumentSegment;
        //        }
        //    }
        //}
    }
}
