using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;

namespace LMP.Controls
{
    public class IntendedUsesComboBox: LMP.Controls.ComboBox
    {
        #region *********************fields*********************
        private static object[,] m_aIntendedUses;
        private static object[,] m_aUserSegmentIntendedUses;
        private static bool m_bAllowStyleSheet = true;
        #endregion
        #region *********************constructors*********************
        static IntendedUsesComboBox()
        {
            //get array of insertion locations
            m_aIntendedUses = new object[,]{{"As Document",1}, 
                {"As Document Component",2}, 
                {"As Paragraph Text",3}, 
                {"As Sentence Text",6},
                {"As Style Sheet",4}};

            m_aUserSegmentIntendedUses = new object[,]{{"As Document",1}, 
                {"As Paragraph Text",3}, 
                {"As Sentence Text",6},
                {"As Style Sheet",4}};
        }

        public IntendedUsesComboBox() : this(false)
        {
        }

        public IntendedUsesComboBox(bool bIsForUserSegment)
        {
            //GLOG 3413: Prevent blanking control
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            if (bIsForUserSegment)
            {
                this.SetList(m_aUserSegmentIntendedUses);
            }
            else
            {
                this.SetList(m_aIntendedUses);
            }
        }
        #endregion
        #region *********************properties***************************
        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public static object[,] List
        {
            get
            {
                return m_aIntendedUses;
            }
        }
        public bool AllowStyleSheet
        {
            get { return m_bAllowStyleSheet; }
            set 
            { 
                m_bAllowStyleSheet = value;

                if (!m_bAllowStyleSheet)
                {
                    //Don't display StyleSheet option
                    object[,] aShortList = new object[,]{{"As Document",1}, 
                        {"As Document Component",2}, 
                        {"As Paragraph Text",3},
                        {"As Sentence Text",6}};
                    this.SetList(aShortList);
                }
                else
                {
                    this.SetList(m_aIntendedUses);
                }
            }
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // IntendedUsesComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.Name = "IntendedUsesComboBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #region *********************protected members*********************
        #endregion
    }
}
