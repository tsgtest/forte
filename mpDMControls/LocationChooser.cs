﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class LocationChooser : UserControl, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private bool m_bInitialized = false;
        private mpLocationChooserMode m_iMode = mpLocationChooserMode.County;
        private string m_xSupportingValues = "";

        #endregion
        #region *********************enums*********************
        public enum mpLocationChooserMode
        {
            Country = 1,
            State = 2,
            County = 3
        }
        #endregion
        #region *********************constructors*********************
        public LocationChooser()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************methods*********************
        private void SetLevelDisplay()
        {
            if (m_iMode == mpLocationChooserMode.Country)
            {
                lblState.Visible = false;
                lddState.Visible = false;
                lblCounty.Visible = false;
                lddCounty.Visible = false;
                this.Height = lddCountry.Top + lddCountry.Height + 3;
                lddCountry.ExecuteFinalSetup();
            }
            else if (m_iMode == mpLocationChooserMode.State)
            {
                lblState.Visible = true;
                lddState.Visible = true;
                lblCounty.Visible = false;
                lddCounty.Visible = false;
                this.Height = lddState.Top + lddState.Height + 3;
                lddCountry.ExecuteFinalSetup();
                lddState.ExecuteFinalSetup();
            }
            else
            {
                lblState.Visible = true;
                lddState.Visible = true;
                lblCounty.Visible = true;
                lddCounty.Visible = true;
                this.Height = lddCounty.Top + lddCounty.Height + 3;
                lddCountry.ExecuteFinalSetup();
                lddState.ExecuteFinalSetup();
                lddCounty.ExecuteFinalSetup();
            }
        }
        private void SetLocations(string xLastLevelID)
        {
            try
            {
                if (m_iMode == mpLocationChooserMode.Country)
                {
                    lddCountry.Value = xLastLevelID;
                }
                else if (m_iMode == mpLocationChooserMode.State)
                {
                    string xSQL = "SELECT CountryID FROM States WHERE ID=" + xLastLevelID + ";";
                    object oRet = LMP.Data.SimpleDataCollection.GetScalar(xSQL);
                    if (oRet != null)
                    {
                        lddCountry.Value = oRet.ToString();
                        lddState.Value = xLastLevelID;
                    }
                }
                else
                {
                    //Get Parent State and Country from County ID
                    string xSQL = "SELECT StateID FROM Counties WHERE ID=" + xLastLevelID + ";";
                    object oRet = LMP.Data.SimpleDataCollection.GetScalar(xSQL);
                    if (oRet != null)
                    {
                        string xStateID = "";
                        xStateID = oRet.ToString();
                        xSQL = "SELECT CountryID FROM States WHERE ID=" + xStateID + ";";
                        object oRet2 = LMP.Data.SimpleDataCollection.GetScalar(xSQL);
                        if (oRet2 != null)
                        {
                            lddCountry.Value = oRet2.ToString();
                            lddState.Value = xStateID;
                            lddCounty.Value = xLastLevelID;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        #endregion
        #region *********************properties*********************
        public mpLocationChooserMode Mode
        {
            get { return m_iMode; }
            set
            {
                m_iMode = value;
                if (m_bInitialized)
                {
                    SetLevelDisplay();
                }
            }
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
            SetLevelDisplay();
            m_bInitialized = true;
        }
        /// <summary>
        /// value is set in form of delimited string
        /// returns delimited string
        /// </summary>
        public string Value
        {
            get
            {
                //Return numeric ID of last configured level
                switch (m_iMode)
                {
                    case mpLocationChooserMode.Country:
                        return lddCountry.Value;
                    case mpLocationChooserMode.State:
                        return lddState.Value;
                    default:
                        return lddCounty.Value;
                }
            }
            set
            {
                //Only apply value if numeric
                if (String.IsNumericInt32(value))
                {
                    SetLocations(value);
                    this.IsDirty = false;
                }
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************event handlers*********************
        private void lddCountry_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_iMode > mpLocationChooserMode.Country)
                {
                    //Update State list to match selected Country
                    lddState.Value = "";
                    if (!string.IsNullOrEmpty(lddCountry.Value))
                        lddState.LocationParentID = Int32.Parse(lddCountry.Value);
                    else
                        lddState.LocationParentID = 0;
                }
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lddState_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_iMode > mpLocationChooserMode.Country)
                {
                    if (m_iMode > mpLocationChooserMode.State)
                    {
                        //Update County list to match selected State
                        lddCounty.Value = "";
                        if (!string.IsNullOrEmpty(lddState.Value))
                            lddCounty.LocationParentID = Int32.Parse(lddState.Value);
                        else
                            lddCounty.LocationParentID = 0;
                    }
                    this.IsDirty = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void lddCounty_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Mode == mpLocationChooserMode.County)
                    this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

    }
}
