﻿namespace LMP.Controls
{
    partial class LocationChooser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCountry = new System.Windows.Forms.Label();
            this.lddCountry = new LMP.Controls.LocationsDropdown();
            this.lddState = new LMP.Controls.LocationsDropdown();
            this.lblState = new System.Windows.Forms.Label();
            this.lddCounty = new LMP.Controls.LocationsDropdown();
            this.lblCounty = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(3, 7);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 0;
            this.lblCountry.Text = "&Country:";
            // 
            // lddCountry
            // 
            this.lddCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lddCountry.BackColor = System.Drawing.Color.White;
            this.lddCountry.DisplayMember = "ID";
            this.lddCountry.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lddCountry.FormattingEnabled = true;
            this.lddCountry.IsDirty = true;
            this.lddCountry.Location = new System.Drawing.Point(55, 3);
            this.lddCountry.LocationParentID = 0;
            this.lddCountry.LocationType = LMP.Data.mpLocationTypes.Countries;
            this.lddCountry.Name = "lddCountry";
            this.lddCountry.Size = new System.Drawing.Size(244, 23);
            this.lddCountry.SupportingValues = "";
            this.lddCountry.TabIndex = 1;
            this.lddCountry.Tag2 = null;
            this.lddCountry.Value = "0";
            this.lddCountry.ValueField = LMP.Controls.LocationsDropdown.mpLocationFields.ID;
            this.lddCountry.ValueMember = "ID";
            this.lddCountry.ValueChanged += new LMP.Controls.ValueChangedHandler(this.lddCountry_ValueChanged);
            // 
            // lddState
            // 
            this.lddState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lddState.BackColor = System.Drawing.Color.White;
            this.lddState.DisplayMember = "ID";
            this.lddState.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lddState.FormattingEnabled = true;
            this.lddState.IsDirty = true;
            this.lddState.Location = new System.Drawing.Point(55, 32);
            this.lddState.LocationParentID = 0;
            this.lddState.LocationType = LMP.Data.mpLocationTypes.States;
            this.lddState.Name = "lddState";
            this.lddState.Size = new System.Drawing.Size(244, 23);
            this.lddState.SupportingValues = "";
            this.lddState.TabIndex = 3;
            this.lddState.Tag2 = null;
            this.lddState.Value = "0";
            this.lddState.ValueField = LMP.Controls.LocationsDropdown.mpLocationFields.ID;
            this.lddState.ValueMember = "ID";
            this.lddState.ValueChanged += new LMP.Controls.ValueChangedHandler(this.lddState_ValueChanged);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(3, 36);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 2;
            this.lblState.Text = "&State:";
            // 
            // lddCounty
            // 
            this.lddCounty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lddCounty.BackColor = System.Drawing.Color.White;
            this.lddCounty.DisplayMember = "ID";
            this.lddCounty.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lddCounty.FormattingEnabled = true;
            this.lddCounty.IsDirty = true;
            this.lddCounty.Location = new System.Drawing.Point(55, 62);
            this.lddCounty.LocationParentID = 0;
            this.lddCounty.LocationType = LMP.Data.mpLocationTypes.Counties;
            this.lddCounty.Name = "lddCounty";
            this.lddCounty.Size = new System.Drawing.Size(244, 23);
            this.lddCounty.SupportingValues = "";
            this.lddCounty.TabIndex = 5;
            this.lddCounty.Tag2 = null;
            this.lddCounty.Value = "0";
            this.lddCounty.ValueField = LMP.Controls.LocationsDropdown.mpLocationFields.ID;
            this.lddCounty.ValueMember = "ID";
            this.lddCounty.ValueChanged += new LMP.Controls.ValueChangedHandler(this.lddCounty_ValueChanged);
            // 
            // lblCounty
            // 
            this.lblCounty.AutoSize = true;
            this.lblCounty.Location = new System.Drawing.Point(3, 66);
            this.lblCounty.Name = "lblCounty";
            this.lblCounty.Size = new System.Drawing.Size(43, 13);
            this.lblCounty.TabIndex = 4;
            this.lblCounty.Text = "&County:";
            // 
            // LocationChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lddCounty);
            this.Controls.Add(this.lblCounty);
            this.Controls.Add(this.lddState);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lddCountry);
            this.Controls.Add(this.lblCountry);
            this.Name = "LocationChooser";
            this.Size = new System.Drawing.Size(302, 90);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCountry;
        private LocationsDropdown lddCountry;
        private LocationsDropdown lddState;
        private System.Windows.Forms.Label lblState;
        private LocationsDropdown lddCounty;
        private System.Windows.Forms.Label lblCounty;
    }
}
