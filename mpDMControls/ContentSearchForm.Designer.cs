﻿namespace LMP.Controls
{
    partial class ContentSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSearchText = new System.Windows.Forms.TextBox();
            this.lblSearchText = new System.Windows.Forms.Label();
            this.lblSearchField = new System.Windows.Forms.Label();
            this.lblContentType = new System.Windows.Forms.Label();
            this.lblOwnedBy = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblSearchLocation = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmbSearchLocation = new LMP.Controls.ComboBox();
            this.cmbOwnedBy = new LMP.Controls.ComboBox();
            this.cmbSearchField = new LMP.Controls.ComboBox();
            this.cmbContentType = new LMP.Controls.ComboBox();
            this.SuspendLayout();
            // 
            // txtSearchText
            // 
            this.txtSearchText.Location = new System.Drawing.Point(103, 15);
            this.txtSearchText.Name = "txtSearchText";
            this.txtSearchText.Size = new System.Drawing.Size(221, 20);
            this.txtSearchText.TabIndex = 1;
            // 
            // lblSearchText
            // 
            this.lblSearchText.AutoSize = true;
            this.lblSearchText.Location = new System.Drawing.Point(24, 17);
            this.lblSearchText.Name = "lblSearchText";
            this.lblSearchText.Size = new System.Drawing.Size(62, 13);
            this.lblSearchText.TabIndex = 0;
            this.lblSearchText.Text = "Search &For:";
            // 
            // lblSearchField
            // 
            this.lblSearchField.AutoSize = true;
            this.lblSearchField.Location = new System.Drawing.Point(24, 55);
            this.lblSearchField.Name = "lblSearchField";
            this.lblSearchField.Size = new System.Drawing.Size(44, 13);
            this.lblSearchField.TabIndex = 2;
            this.lblSearchField.Text = "&Search:";
            // 
            // lblContentType
            // 
            this.lblContentType.AutoSize = true;
            this.lblContentType.Location = new System.Drawing.Point(24, 132);
            this.lblContentType.Name = "lblContentType";
            this.lblContentType.Size = new System.Drawing.Size(74, 13);
            this.lblContentType.TabIndex = 6;
            this.lblContentType.Text = "Content &Type:";
            // 
            // lblOwnedBy
            // 
            this.lblOwnedBy.AutoSize = true;
            this.lblOwnedBy.Location = new System.Drawing.Point(24, 170);
            this.lblOwnedBy.Name = "lblOwnedBy";
            this.lblOwnedBy.Size = new System.Drawing.Size(59, 13);
            this.lblOwnedBy.TabIndex = 8;
            this.lblOwnedBy.Text = "&Owned By:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(249, 214);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(168, 214);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblSearchLocation
            // 
            this.lblSearchLocation.AutoSize = true;
            this.lblSearchLocation.Location = new System.Drawing.Point(24, 94);
            this.lblSearchLocation.Name = "lblSearchLocation";
            this.lblSearchLocation.Size = new System.Drawing.Size(56, 13);
            this.lblSearchLocation.TabIndex = 4;
            this.lblSearchLocation.Text = "Search &In:";
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.AutoSize = true;
            this.btnReset.Location = new System.Drawing.Point(11, 214);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 27);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmbSearchLocation
            // 
            this.cmbSearchLocation.AllowEmptyValue = false;
            this.cmbSearchLocation.AutoSize = true;
            this.cmbSearchLocation.Borderless = false;
            this.cmbSearchLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbSearchLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSearchLocation.IsDirty = false;
            this.cmbSearchLocation.LimitToList = true;
            this.cmbSearchLocation.ListName = "";
            this.cmbSearchLocation.Location = new System.Drawing.Point(103, 91);
            this.cmbSearchLocation.MaxDropDownItems = 8;
            this.cmbSearchLocation.Name = "cmbSearchLocation";
            this.cmbSearchLocation.SelectedIndex = -1;
            this.cmbSearchLocation.SelectedValue = null;
            this.cmbSearchLocation.SelectionLength = 0;
            this.cmbSearchLocation.SelectionStart = 0;
            this.cmbSearchLocation.Size = new System.Drawing.Size(221, 26);
            this.cmbSearchLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbSearchLocation.SupportingValues = "";
            this.cmbSearchLocation.TabIndex = 5;
            this.cmbSearchLocation.Tag2 = null;
            this.cmbSearchLocation.Value = "";
            // 
            // cmbOwnedBy
            // 
            this.cmbOwnedBy.AllowEmptyValue = false;
            this.cmbOwnedBy.AutoSize = true;
            this.cmbOwnedBy.Borderless = false;
            this.cmbOwnedBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbOwnedBy.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOwnedBy.IsDirty = false;
            this.cmbOwnedBy.LimitToList = true;
            this.cmbOwnedBy.ListName = "";
            this.cmbOwnedBy.Location = new System.Drawing.Point(103, 167);
            this.cmbOwnedBy.MaxDropDownItems = 8;
            this.cmbOwnedBy.Name = "cmbOwnedBy";
            this.cmbOwnedBy.SelectedIndex = -1;
            this.cmbOwnedBy.SelectedValue = null;
            this.cmbOwnedBy.SelectionLength = 0;
            this.cmbOwnedBy.SelectionStart = 0;
            this.cmbOwnedBy.Size = new System.Drawing.Size(221, 26);
            this.cmbOwnedBy.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbOwnedBy.SupportingValues = "";
            this.cmbOwnedBy.TabIndex = 9;
            this.cmbOwnedBy.Tag2 = null;
            this.cmbOwnedBy.Value = "";
            // 
            // cmbSearchField
            // 
            this.cmbSearchField.AllowEmptyValue = false;
            this.cmbSearchField.AutoSize = true;
            this.cmbSearchField.Borderless = false;
            this.cmbSearchField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbSearchField.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSearchField.IsDirty = false;
            this.cmbSearchField.LimitToList = true;
            this.cmbSearchField.ListName = "";
            this.cmbSearchField.Location = new System.Drawing.Point(103, 52);
            this.cmbSearchField.MaxDropDownItems = 8;
            this.cmbSearchField.Name = "cmbSearchField";
            this.cmbSearchField.SelectedIndex = -1;
            this.cmbSearchField.SelectedValue = null;
            this.cmbSearchField.SelectionLength = 0;
            this.cmbSearchField.SelectionStart = 0;
            this.cmbSearchField.Size = new System.Drawing.Size(221, 26);
            this.cmbSearchField.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbSearchField.SupportingValues = "";
            this.cmbSearchField.TabIndex = 3;
            this.cmbSearchField.Tag2 = null;
            this.cmbSearchField.Value = "";
            // 
            // cmbContentType
            // 
            this.cmbContentType.AllowEmptyValue = false;
            this.cmbContentType.AutoSize = true;
            this.cmbContentType.Borderless = false;
            this.cmbContentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbContentType.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContentType.IsDirty = false;
            this.cmbContentType.LimitToList = true;
            this.cmbContentType.ListName = "";
            this.cmbContentType.Location = new System.Drawing.Point(103, 129);
            this.cmbContentType.MaxDropDownItems = 8;
            this.cmbContentType.Name = "cmbContentType";
            this.cmbContentType.SelectedIndex = -1;
            this.cmbContentType.SelectedValue = null;
            this.cmbContentType.SelectionLength = 0;
            this.cmbContentType.SelectionStart = 0;
            this.cmbContentType.Size = new System.Drawing.Size(221, 26);
            this.cmbContentType.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbContentType.SupportingValues = "";
            this.cmbContentType.TabIndex = 7;
            this.cmbContentType.Tag2 = null;
            this.cmbContentType.Value = "";
            // 
            // ContentSearchForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(347, 253);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lblSearchLocation);
            this.Controls.Add(this.cmbSearchLocation);
            this.Controls.Add(this.cmbOwnedBy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblOwnedBy);
            this.Controls.Add(this.lblContentType);
            this.Controls.Add(this.lblSearchField);
            this.Controls.Add(this.cmbSearchField);
            this.Controls.Add(this.cmbContentType);
            this.Controls.Add(this.lblSearchText);
            this.Controls.Add(this.txtSearchText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContentSearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Advanced Find Options";
            this.Load += new System.EventHandler(this.ContentSearchForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSearchText;
        private System.Windows.Forms.Label lblSearchText;
        private LMP.Controls.ComboBox cmbContentType;
        private System.Windows.Forms.Label lblSearchField;
        private LMP.Controls.ComboBox cmbSearchField;
        private System.Windows.Forms.Label lblContentType;
        private System.Windows.Forms.Label lblOwnedBy;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private LMP.Controls.ComboBox cmbOwnedBy;
        private System.Windows.Forms.Label lblSearchLocation;
        private LMP.Controls.ComboBox cmbSearchLocation;
        private System.Windows.Forms.Button btnReset;
    }
}