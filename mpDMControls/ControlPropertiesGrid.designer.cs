namespace LMP.Controls
{
    partial class ControlPropertiesGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_DeleteItem = new System.Windows.Forms.MenuItem();
            this.mnuContext_ReloadDefaults = new System.Windows.Forms.MenuItem();
            this.grdProperties = new LMP.Controls.DataGridView();
            this.mnuGrid = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuGrid_DeleteItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGrid_ReloadDefaults = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.mnuGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_DeleteItem,
            this.mnuContext_ReloadDefaults});
            // 
            // mnuContext_DeleteItem
            // 
            this.mnuContext_DeleteItem.Index = 0;
            this.mnuContext_DeleteItem.Text = "#Delete Item#";
            this.mnuContext_DeleteItem.Click += new System.EventHandler(this.mnuGrid_DeleteItem_Click);
            // 
            // mnuContext_ReloadDefaults
            // 
            this.mnuContext_ReloadDefaults.Index = 1;
            this.mnuContext_ReloadDefaults.Text = "#Reload Defaults#";
            this.mnuContext_ReloadDefaults.Click += new System.EventHandler(this.mnuGrid_ReloadDefaults_Click);
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProperties.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.IsDirty = false;
            this.grdProperties.Location = new System.Drawing.Point(0, 0);
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowHeadersWidth = 20;
            this.grdProperties.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdProperties.RowTemplate.Height = 18;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(209, 150);
            this.grdProperties.SupportingValues = "";
            this.grdProperties.TabIndex = 0;
            this.grdProperties.Tag2 = null;
            this.grdProperties.Value = null;
            this.grdProperties.TabPressed += new LMP.Controls.TabPressedHandler(this.grdProperties_TabPressed);
            this.grdProperties.EnterKeyPressed += new LMP.Controls.EnterKeyPressedHandler(this.grdProperties_EnterKeyPressed);
            this.grdProperties.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellContentClick);
            this.grdProperties.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdProperties_CellMouseDown);
            this.grdProperties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdProperties_CellValidating);
            this.grdProperties.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdProperties_CurrentCellDirtyStateChanged);
            this.grdProperties.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdProperties_EditingControlShowing);
            this.grdProperties.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_RowLeave);
            this.grdProperties.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.grdProperties_RowsAdded);
            this.grdProperties.Scroll += new System.Windows.Forms.ScrollEventHandler(this.grdProperties_Scroll);
            this.grdProperties.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grdProperties_KeyUp);
            this.grdProperties.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ControlPropertiesGrid_PreviewKeyDown);
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuGrid_DeleteItem,
            this.mnuGrid_ReloadDefaults});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(151, 48);
            this.mnuGrid.TimerInterval = 500;
            this.mnuGrid.UseTimer = true;
            // 
            // mnuGrid_DeleteItem
            // 
            this.mnuGrid_DeleteItem.Name = "mnuGrid_DeleteItem";
            this.mnuGrid_DeleteItem.Size = new System.Drawing.Size(150, 22);
            this.mnuGrid_DeleteItem.Text = "#Delete Item#";
            this.mnuGrid_DeleteItem.Click += new System.EventHandler(this.mnuGrid_DeleteItem_Click);
            // 
            // mnuGrid_ReloadDefaults
            // 
            this.mnuGrid_ReloadDefaults.Name = "mnuGrid_ReloadDefaults";
            this.mnuGrid_ReloadDefaults.Size = new System.Drawing.Size(150, 22);
            this.mnuGrid_ReloadDefaults.Text = "#Reload Defaults#";
            this.mnuGrid_ReloadDefaults.Click += new System.EventHandler(this.mnuGrid_ReloadDefaults_Click);
            // 
            // ControlPropertiesGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.grdProperties);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ControlPropertiesGrid";
            this.Size = new System.Drawing.Size(209, 150);
            this.SizeChanged += new System.EventHandler(this.ControlPropertiesGrid_SizeChanged);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ControlPropertiesGrid_PreviewKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private LMP.Controls.DataGridView grdProperties;
        private TimedContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuGrid_DeleteItem;
        private System.Windows.Forms.ToolStripMenuItem mnuGrid_ReloadDefaults;
        private System.Windows.Forms.ContextMenu mnuContext;
        private System.Windows.Forms.MenuItem mnuContext_DeleteItem;
        private System.Windows.Forms.MenuItem mnuContext_ReloadDefaults;
    }
}
