using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using System.Collections;
using System.Reflection;

namespace LMP.Controls
{
    internal partial class ExpressionBuilder : Form
    {
        /// <summary>
        /// contains the methods and properties that define a MacPac Expression Builder
        /// created by Jenny Wade - 03/06
        /// </summary>

        #region *********************fields*********************
        private ParameterPanel m_oPanel = null;
        private List<string> m_oExpressionList = new List<string>();
        private bool m_bTextChanged = false;
        private int m_iExpressionCounter = 0;
        #endregion *********************fields*********************

        //TODO: need to softcode author field codes. 
        //TODO: need to use meaningful error strings for the LMP.Execptions statements

        #region *********************constants*********************
        //string contains the values for the Expression Builder field code nodes.
        //First portion of field is the node display name, 2nd portion is the field code.
        //the "|" character separates each field
        //the "+" character designates a new child node
        //the "-" character designates a new parent level node

        //GLOG : 3688 : CEH
        //Authors top node
        private const string xFieldCodes = "Authors=|+Bar ID=Authors__BarID|" +
                                                     "Count=AuthorsCount|" +
                                                     "Display Name=Authors__DisplayName|" +
                                                     "EMail=Authors__Email|" +
                                                     "Fax=Authors__Fax|" + 
                                                     "First Name=Authors__FirstName|" +
                                                     "Full Name=Authors__FullName|" +
                                                     "ID=Authors__ID|" +
                                                     "Initials=Authors__Initials|" +
                                                     "Is Alias=Authors__IsAlias|" +
                                                     "Is Attorney=Authors__IsAttorney|" +
                                                     "Is Manual=Authors__IsManual|" +
                                                     "Last Name=Authors__LastName|" +
                                                     "Middle Initial=Authors__MiddleInitial|" +
                                                     "Phone=Authors__Phone|" +
                                                     "Prefix=Authors__Prefix|" +
                                                     "Short Name=Authors__ShortName|" +
                                                     "Suffix=Authors__Suffix|" +
                                                     "Title=Authors__Title|" +
                                                     "User ID=Authors__UserID|" +
            //sub node
            "Office=|+Abbreviation=AuthorsOffice__Abbr|" +
                     "Custom Field 1=AuthorsOffice__CustomField|" +
                     "Custom Field 2=AuthorsOffice__CustomField2|" +
                     "Display Name=AuthorsOffice__DisplayName|" +
                     "Firm ID=AuthorsOffice__FirmID|" +
                     "Firm Name=AuthorsOffice__FirmName|" +
                     "Firm Name Upper Case=AuthorsOffice__FirmNameUpperCase|" +
                     "Name=AuthorsOffice__Name|" + 
                     "Slogan=AuthorsOffice__Slogan|" +
            //sub node
            "Address=|+City=AuthorsOffice__City|" +
                      "Country=AuthorsOffice__Country|" +
                      "County=AuthorsOffice__County|" +
                      "Fax 1=AuthorsOffice__Fax1|" +
                      "Fax 2=AuthorsOffice__Fax2|" +
                      "Formatted Address=AuthorsOfficeAddress|" +
                      "Line 1=AuthorsOffice__Line1|" +
                      "Line 2=AuthorsOffice__Line2|" +
                      "Line 3=AuthorsOffice__Line3|" +
                      "Phone 1=AuthorsOffice__Phone1|" +
                      "Phone 2=AuthorsOffice__Phone2|" +
                      "Phone 3=AuthorsOffice__Phone3|" +
                      "Post City=AuthorsOffice__PostCity|" +
                      "Pre City=AuthorsOffice__PreCity|" +
                      "State=AuthorsOffice__State|" +
                      "State Abbreviation=AuthorsOffice__StateAbbr|" +
                      "Zip=AuthorsOffice__Zip|" +

        //Author top node
         "----Author=|+Bar ID=Author__BarID|" +
                      "Display Name=Author__DisplayName|" +
                      "EMail=Author__EMail|" +
                      "Fax=Author__Fax|" +
                      "First Name=Author__FirstName|" +
                      "Full Name=Author__FullName|" +
                      "ID=Author__ID|" +
                      "Initials=Author__Initials|" +
                      "Is Alias=Author__IsAlias|" +
                      "Is Attorney=Author__IsAttorney|" +
                      "Is Manual=Author__IsManual|" +
                      "Last Name=Author__LastName|" +
                      "Middle Initial=Author__MiddleInitial|" +
                      "Office ID=Author__OfficeID|" +
                      "Parent Preference=AuthorParentPreference|" +
                      "Phone=Author__Phone|" +
                      "Preference=AuthorPreference|" +
                      "Prefix=Author__Prefix|" +
                      "Short Name=Author__ShortName|" +
                      "Suffix=Author__Suffix|" +
                      "Title=Author__Title|" +
                      "User ID=Author__UserID|" +
            //sub node
            "Office=|+Abbreviation=AuthorOffice__Abbr|" +
                     "Custom Field 1=AuthorOffice__CustomField|" +
                     "Custom Field 2=AuthorOffice__CustomField2|" +
                     "Display Name=AuthorOffice__DisplayName|" +
                     "Firm ID=AuthorOffice__FirmID|" +
                     "Firm Name=AuthorOffice__FirmName|" +
                     "Firm Name Upper Case=AuthorOffice__FirmNameUpperCase|" +
                     "Name=AuthorOffice__Name|" +
                     "Slogan=AuthorOffice__Slogan|" +
            //sub node
            "Address=|+City=AuthorOffice__City|" +
                      "Country=AuthorOffice__Country|" +
                      "County=AuthorOffice__County|" +
                      "Fax 1=AuthorOffice__Fax1|" +
                      "Fax 2=AuthorOffice__Fax2|" +
                      "Formatted Address=AuthorOfficeAddress|" +
                      "Line 1=AuthorOffice__Line1|" +
                      "Line 2=AuthorOffice__Line2|" +
                      "Line 3=AuthorOffice__Line3|" +
                      "Phone 1=AuthorOffice__Phone1|" +
                      "Phone 2=AuthorOffice__Phone2|" +
                      "Phone 3=AuthorOffice__Phone3|" +
                      "Post City=AuthorOffice__PostCity|" +
                      "Pre City=AuthorOffice__PreCity|" +
                      "State=AuthorOffice__State|" +
                      "State Abbreviation=AuthorOffice__StateAbbr|" +
                      "Zip=AuthorOffice__Zip|" +

        //Lead Author top node
         "----Lead Author=|+Bar ID=LeadAuthor__BarID|" +
                           "Display Name=LeadAuthor__DisplayName|" +
                           "EMail=LeadAuthor__EMail|" +
                           "Fax=LeadAuthor__Fax|" +
                           "First Name=LeadAuthor__FirstName|" +
                           "Full Name=LeadAuthor__FullName|" +
                           "ID=LeadAuthor__ID|" +
                           "Initials=LeadAuthor__Initials|" +
                           "Is Alias=LeadAuthor__IsAlias|" +
                           "Is Attorney=LeadAuthor__IsAttorney|" +
                           "Is Manual=LeadAuthor__IsManual|" +
                           "Last Name=LeadAuthor__LastName|" +
                           "Middle Initial=LeadAuthor__MiddleInitial|" +
                           "Office ID=LeadAuthor__OfficeID|" +
                           "Parent Preference=LeadAuthorParentPreference|" +
                           "Phone=LeadAuthor__Phone|" +
                           "Preference=LeadAuthorPreference|" +
                           "Prefix=LeadAuthor__Prefix|" +
                           "Short Name=LeadAuthor__ShortName|" +
                           "Suffix=LeadAuthor__Suffix|" +
                           "Title=LeadAuthor__Title|" +
                           "User ID=LeadAuthor__UserID|" +

            //sub node
            "Office=|+Abbreviation=LeadAuthorOffice__Abbr|" +
                     "Custom Field 1=LeadAuthorOffice__CustomField|" +
                     "Custom Field 2=LeadAuthorOffice__CustomField2|" +
                     "Display Name=LeadAuthorOffice__DisplayName|" +
                     "Firm ID=LeadAuthorOffice__FirmID|" +
                     "Firm Name=LeadAuthorOffice__FirmName|" +
                     "Firm Name Upper Case=LeadAuthorOffice__FirmNameUpperCase|" +
                     "Name=LeadAuthorOffice__Name|" +
                     "Slogan=LeadAuthorOffice__Slogan|" +
            //sub node
            "Address=|+City=LeadAuthorOffice__City|" +
                      "Country=LeadAuthorOffice__Country|" +
                      "County=LeadAuthorOffice__County|" +
                      "Fax 1=LeadAuthorOffice__Fax1|" +
                      "Fax 2=LeadAuthorOffice__Fax2|" +
                      "Formatted Address=LeadAuthorOfficeAddress|" +
                      "Line 1=LeadAuthorOffice__Line1|" +
                      "Line 2=LeadAuthorOffice__Line2|" +
                      "Line 3=LeadAuthorOffice__Line3|" +
                      "Phone 1=LeadAuthorOffice__Phone1|" +
                      "Phone 2=LeadAuthorOffice__Phone2|" +
                      "Phone 3=LeadAuthorOffice__Phone3|" +
                      "Post City=LeadAuthorOffice__PostCity|" +
                      "Pre City=LeadAuthorOffice__PreCity|" +
                      "State=LeadAuthorOffice__State|" +
                      "State Abbreviation=LeadAuthorOffice__StateAbbr|" +
                      "Zip=LeadAuthorOffice__Zip|" +


        //Additional Authors top node
        "----Additional Authors=|+Bar ID=AdditionalAuthors__BarID|" +
                                 "Count=AdditionalAuthorsCount|" +
                                 "Display Name=AdditionalAuthors__DisplayName|" +
                                 "EMail=AdditionalAuthors__Email|" +
                                 "Fax=AdditionalAuthors__Fax|" + 
                                 "First Name=AdditionalAuthors__FirstName|" +
                                 "Full Name=AdditionalAuthors__FullName|" +
                                 "ID=AdditionalAuthors__ID|" +
                                 "Initials=AdditionalAuthors__Initials|" +
                                 "Is Alias=AdditionalAuthors__IsAlias|" +
                                 "Is Attorney=AdditionalAuthors__IsAttorney|" +
                                 "Is Manual=AdditionalAuthors__IsManual|" +
                                 "Last Name=AdditionalAuthors__LastName|" +
                                 "Middle Initial=AdditionalAuthors__MiddleInitial|" +
                                 "Phone=AdditionalAuthors__Phone|" +
                                 "Prefix=AdditionalAuthors__Prefix|" +
                                 "Short Name=AdditionalAuthors__ShortName|" +
                                 "Suffix=AdditionalAuthors__Suffix|" +
                                 "Title=AdditionalAuthors__Title|" +
                                 "User ID=AdditionalAuthors__UserID|" +

            //sub node
            "Office=|+Abbreviation=AdditionalAuthorsOffice__Abbr|" +
                     "Custom Field 1=AdditionalAuthorsOffice__CustomField|" +
                     "Custom Field 2=AdditionalAuthorsOffice__CustomField2|" +
                     "Display Name=AdditionalAuthorsOffice__DisplayName|" +
                     "Firm ID=AdditionalAuthorsOffice__FirmID|" +
                     "Firm Name=AdditionalAuthorsOffice__FirmName|" +
                     "Firm Name Upper Case=AdditionalAuthorsOffice__FirmNameUpperCase|" +
                     "Name=AdditionalAuthorsOffice__Name|" +
                     "Slogan=AdditionalAuthorsOffice__Slogan|" +
            //sub node
            "Address=|+City=AdditionalAuthorsOffice__City|" +
                      "Country=AdditionalAuthorsOffice__Country|" +
                      "County=AdditionalAuthorsOffice__County|" +
                      "Fax 1=AdditionalAuthorsOffice__Fax1|" +
                      "Fax 2=AdditionalAuthorsOffice__Fax2|" +
                      "Formatted Address=AdditionalAuthorsOfficeAddress|" +
                      "Line 1=AdditionalAuthorsOffice__Line1|" +
                      "Line 2=AdditionalAuthorsOffice__Line2|" +
                      "Line 3=AdditionalAuthorsOffice__Line3|" +
                      "Phone 1=AdditionalAuthorsOffice__Phone1|" +
                      "Phone 2=AdditionalAuthorsOffice__Phone2|" +
                      "Phone 3=AdditionalAuthorsOffice__Phone3|" +
                      "Post City=AdditionalAuthorsOffice__PostCity|" +
                      "Pre City=AdditionalAuthorsOffice__PreCity|" +
                      "State=AdditionalAuthorsOffice__State|" +
                      "State Abbreviation=AdditionalAuthorsOffice__StateAbbr|" +
                      "Zip=AdditionalAuthorsOffice__Zip|" +


        //Previous Lead Author top node
        "----Previous Lead Author=|+Bar ID=PreviousLeadAuthor__BarID|" +
                                   "Display Name=PreviousLeadAuthor__DisplayName|" +
                                   "EMail=PreviousLeadAuthor__EMail|" +
                                   "Fax=PreviousLeadAuthor__Fax|" +
                                   "First Name=PreviousLeadAuthor__FirstName|" +
                                   "Full Name=PreviousLeadAuthor__FullName|" +
                                   "ID=PreviousLeadAuthor__ID|" +
                                   "Initials=PreviousLeadAuthor__Initials|" +
                                   "Is Alias=PreviousLeadAuthor__IsAlias|" +
                                   "Is Attorney=PreviousLeadAuthor__IsAttorney|" +
                                   "Is Manual=PreviousLeadAuthor__IsManual|" +
                                   "Last Name=PreviousLeadAuthor__LastName|" +
                                   "Middle Initial=PreviousLeadAuthor__MiddleInitial|" +
                                   "Office ID=PreviousLeadAuthor__OfficeID|" +
                                   "Parent Preference=PreviousLeadAuthorParentPreference|" +
                                   "Phone=PreviousLeadAuthor__Phone|" +
                                   "Preference=PreviousLeadAuthorPreference|" +
                                   "Prefix=PreviousLeadAuthor__Prefix|" +
                                   "Short Name=PreviousLeadAuthor__ShortName|" +
                                   "Suffix=PreviousLeadAuthor__Suffix|" +
                                   "Title=PreviousLeadAuthor__Title|" +
                                   "User ID=PreviousLeadAuthor__UserID|" +

            //sub node
            "Office=|+Abbreviation=PreviousLeadAuthorOffice__Abbr|" +
                     "Custom Field 1=PreviousLeadAuthorOffice__CustomField|" +
                     "Custom Field 2=PreviousLeadAuthorOffice__CustomField2|" +
                     "Display Name=PreviousLeadAuthorOffice__DisplayName|" +
                     "Firm ID=PreviousLeadAuthorOffice__FirmID|" +
                     "Firm Name=PreviousLeadAuthorOffice__FirmName|" +
                     "Firm Name Upper Case=PreviousLeadAuthorOffice__FirmNameUpperCase|" +
                     "Name=PreviousLeadAuthorOffice__Name|" +
                     "Slogan=PreviousLeadAuthorOffice__Slogan|" +
            //sub node
            "Address=|+City=PreviousLeadAuthorOffice__City|" +
                      "Country=PreviousLeadAuthorOffice__Country|" +
                      "County=PreviousLeadAuthorOffice__County|" +
                      "Fax 1=PreviousLeadAuthorOffice__Fax1|" +
                      "Fax 2=PreviousLeadAuthorOffice__Fax2|" +
                      "Formatted Address=PreviousLeadAuthorOfficeAddress|" +
                      "Line 1=PreviousLeadAuthorOffice__Line1|" +
                      "Line 2=PreviousLeadAuthorOffice__Line2|" +
                      "Line 3=PreviousLeadAuthorOffice__Line3|" +
                      "Phone 1=PreviousLeadAuthorOffice__Phone1|" +
                      "Phone 2=PreviousLeadAuthorOffice__Phone2|" +
                      "Phone 3=PreviousLeadAuthorOffice__Phone3|" +
                      "Post City=PreviousLeadAuthorOffice__PostCity|" +
                      "Pre City=PreviousLeadAuthorOffice__PreCity|" +
                      "State=PreviousLeadAuthorOffice__State|" +
                      "State Abbreviation=PreviousLeadAuthorOffice__StateAbbr|" +
                      "Zip=PreviousLeadAuthorOffice__Zip|" +

        //Previous Authors top node
        "----Previous Authors=|+Bar ID=PreviousAuthors__BarID|" +
                                 "Count=PreviousAuthorsCount|" +
                                 "Display Name=PreviousAuthors__DisplayName|" +
                                 "EMail=PreviousAuthors__Email|" +
                                 "Fax=PreviousAuthors__Fax|" +
                                 "First Name=PreviousAuthors__FirstName|" +
                                 "Full Name=PreviousAuthors__FullName|" +
                                 "ID=PreviousAuthors__ID|" +
                                 "Initials=PreviousAuthors__Initials|" +
                                 "Is Alias=PreviousAuthors__IsAlias|" +
                                 "Is Attorney=PreviousAuthors__IsAttorney|" +
                                 "Is Manual=PreviousAuthors__IsManual|" +
                                 "Last Name=PreviousAuthors__LastName|" +
                                 "Middle Initial=PreviousAuthors__MiddleInitial|" +
                                 "Phone=PreviousAuthors__Phone|" +
                                 "Prefix=PreviousAuthors__Prefix|" +
                                 "Short Name=PreviousAuthors__ShortName|" +
                                 "Suffix=PreviousAuthors__Suffix|" +
                                 "Title=PreviousAuthors__Title|" +
                                 "User ID=PreviousAuthors__UserID|" +

            //sub node
            "Office=|+Abbreviation=PreviousAuthorsOffice__Abbr|" +
                     "Custom Field 1=PreviousAuthorsOffice__CustomField|" +
                     "Custom Field 2=PreviousAuthorsOffice__CustomField2|" +
                     "Display Name=PreviousAuthorsOffice__DisplayName|" +
                     "Firm ID=PreviousAuthorsOffice__FirmID|" +
                     "Firm Name=PreviousAuthorsOffice__FirmName|" +
                     "Firm Name Upper Case=PreviousAuthorsOffice__FirmNameUpperCase|" +
                     "Name=PreviousAuthorsOffice__Name|" +
                     "Slogan=PreviousAuthorsOffice__Slogan|" +
            //sub node
            "Address=|+City=PreviousAuthorsOffice__City|" +
                      "Country=PreviousAuthorsOffice__Country|" +
                      "County=PreviousAuthorsOffice__County|" +
                      "Fax 1=PreviousAuthorsOffice__Fax1|" +
                      "Fax 2=PreviousAuthorsOffice__Fax2|" +
                      "Formatted Address=PreviousAuthorsOfficeAddress|" +
                      "Line 1=PreviousAuthorsOffice__Line1|" +
                      "Line 2=PreviousAuthorsOffice__Line2|" +
                      "Line 3=PreviousAuthorsOffice__Line3|" +
                      "Phone 1=PreviousAuthorsOffice__Phone1|" +
                      "Phone 2=PreviousAuthorsOffice__Phone2|" +
                      "Phone 3=PreviousAuthorsOffice__Phone3|" +
                      "Post City=PreviousAuthorsOffice__PostCity|" +
                      "Pre City=PreviousAuthorsOffice__PreCity|" +
                      "State=PreviousAuthorsOffice__State|" +
                      "State Abbreviation=PreviousAuthorsOffice__StateAbbr|" +
                      "Zip=PreviousAuthorsOffice__Zip|" +


        //Person top node
        "----Person=|+Bar ID=Person__BarID|" +
                     "Display Name=Person__DisplayName|" +
                     "EMail=Person__EMail|" +
                     "Fax=Person__Fax|" +
                     "First Name=Person__FirstName|" +
                     "Full Name=Person__FullName|" +
                     "ID=Person__ID|" +
                     "Initials=Person__Initials|" +
                     "Is Alias=Person__IsAlias|" +
                     "Is Attorney=Person__IsAttorney|" +
                     "Last Name=Person__LastName|" +
                     "Middle Initial=Person__MiddleInitial|" +
                     "Office ID=Person__OfficeID|" +
                     "Phone=Person__Phone|" +
                     "Prefix=Person__Prefix|" +
                     "Short Name=Person__ShortName|" +
                     "Suffix=Person__Suffix|" +
                     "Title=Person__Title|" +
                     "User ID=Person__UserID|" +

            //sub node
            "Office=|+Abbreviation=PersonOffice__Abbr|" +
                     "Custom Field 1=PersonOffice__CustomField|" +
                     "Custom Field 2=PersonOffice__CustomField2|" +
                     "Display Name=PersonOffice__DisplayName|" +
                     "Firm ID=PersonOffice__FirmID|" +
                     "Firm Name=PersonOffice__FirmName|" +
                     "Firm Name Upper Case=PersonOffice__FirmNameUpperCase|" +
                     "Name=PersonOffice__Name|" +
                     "Slogan=PersonOffice__Slogan|" +
            //sub node
            "Address=|+City=PersonOffice__City|" +
                      "Country=PersonOffice__Country|" +
                      "County=PersonOffice__County|" +
                      "Fax 1=PersonOffice__Fax1|" +
                      "Fax 2=PersonOffice__Fax2|" +
                      "Formatted Address=PersonOfficeAddress|" +
                      "Line 1=PersonOffice__Line1|" +
                      "Line 2=PersonOffice__Line2|" +
                      "Line 3=PersonOffice__Line3|" +
                      "Phone 1=PersonOffice__Phone1|" +
                      "Phone 2=PersonOffice__Phone2|" +
                      "Phone 3=PersonOffice__Phone3|" +
                      "Post City=PersonOffice__PostCity|" +
                      "Pre City=PersonOffice__PreCity|" +
                      "State=PersonOffice__State|" +
                      "State Abbreviation=PersonOffice__StateAbbr|" +
                      "Zip=PersonOffice__Zip|" +

        //User top node
         "----User=|+Bar ID=User__BarID|" +
                    "Display Name=User__DisplayName|" +
                    "EMail=User__EMail|" +
                    "Fax=User__Fax|" +
                    "First Name=User__FirstName|" +
                    "Full Name=User__FullName|" +
                    "ID=User__ID|" +
                    "Initials=User__Initials|" +
                    "Is Alias=User__IsAlias|" +
                    "Is Attorney=User__IsAttorney|" +
                    "Last Name=User__LastName|" +
                    "Middle Initial=User__MiddleInitial|" +
                    "Office ID=User__OfficeID|" +
                    "Prefix=User__Prefix|" +
                    "Short Name=User__ShortName|" +
                    "Suffix=User__Suffix|" +
                    "Title=User__Title|Phone=User__Phone|" +
                    "User ID=User__UserID|" +

            //sub node
            "Application Settings=|+Firm Application Setting=FirmSetting|" +
                                    "User Application Setting=UserSetting|" +

            //sub node
            "-Office=|+Abbreviation=UserOffice__Abbr|" +
                      "Custom Field 1=UserOffice__CustomField1|" +
                      "Custom Field 2=UserOffice__CustomField2|" +
                      "Display Name=UserOffice__DisplayName|" +
                      "Firm ID=UserOffice__FirmID|" +
                      "Firm Name=UserOffice__FirmName|" +
                      "Firm Name Upper Case=UserOffice__FirmNameUpperCase|" +
                      "Name=UserOffice__Name|" +
                      "Slogan=UserOffice__Slogan|" +
            //sub node
            "Address=|+City=UserOffice__City|" +
                      "Country=UserOffice__Country|" +
                      "County=UserOffice__County|" +
                      "Fax 1=UserOffice__Fax1|" +
                      "Fax 2=UserOffice__Fax2|" +
                      "Formatted Address=UserOfficeAddress|" +
                      "Line 1=UserOffice__Line1|" +
                      "Line 2=UserOffice__Line2|" +
                      "Line 3=UserOffice__Line3|" +
                      "Phone 1=UserOffice__Phone1|" +
                      "Phone 2=UserOffice__Phone2|" +
                      "Phone 3=UserOffice__Phone3|" +
                      "Post City=UserOffice__PostCity|" +
                      "Pre City=UserOffice__PreCity|" +
                      "State=UserOffice__State|" +
                      "State Abbreviation=UserOffice__StateAbbr|" +
                      "Zip=UserOffice__Zip|" +
            //sub node
            "--Preference=|+Application Preference=ApplicationPreference|" +
                         "Object Preference=ObjectPreference|" +
                         "Type Preference=TypePreference|" +

        //Contacts top node
        "-----Contacts=|+Additional Information=CIDetail__AdditionalInformation|" +
                        "Address ID=CIDetail__AddressID|" +
                        "Address Type Name=CIDetail__AddressTypeName|" +
                        "City=CIDetail__City|Company=CIDetail__Company|" +
                        "Company (if business address)=CIDetail__CompanyIfBusiness|" +
                        "Core Address=CIDetail__CoreAddress|" +
                        "Country=CIDetail__Country|" +
                        "Custom 1=CIDetail__Custom1|" +
                        "Custom 2=CIDetail__Custom2|" +
                        "Custom 3=CIDetail__Custom3|" +
                        "Custom 4=CIDetail__Custom4|" +
                        "Department=CIDetail__Department|" +
                        "Display Name=CIDetail__DisplayName|" +
                        "EMail Address=CIDetail__EMailAddress|" +
                        "Fax=CIDetail__Fax|" +
                        "Full Detail=CIDetail__FullDetail|" + 
                        "First Name=CIDetail__FirstName|" +
                        "Full Name=CIDetail__FullName|" +
                        "Full Name (with Prefix)=CIDetail__FullNameWithPrefix|" +
                        "Full Name (with Suffix)=CIDetail__FullNameWithSuffix|" +
                        "Full Name (with Prefix/Suffix)=CIDetail__FullNameWithPrefixAndSuffix|" +
                        "Initials=CIDetail__Initials|" +
                        "Last Name=CIDetail__LastName|" +
                        "Middle Name=CIDetail__MiddleName|" +
                        "Goes By=CIDetail__GoesBy|" +   //GLOG : 15877 : ceh
                        "Phone=CIDetail__Phone|" +
                        "Phone Extension=CIDetail__PhoneExtension|" +
                        "Prefix=CIDetail__Prefix|" +
                        "Salutation=CIDetail__Salutation|" +
                        "State=CIDetail__State|" +
                        "Street 1=CIDetail__Street1|" +
                        "Street 2=CIDetail__Street2|" +
                        "Street 3=CIDetail__Street3|" +
                        "Suffix=CIDetail__Suffix|" +
                        "Tag=CIDetail__Tag|" +
                        "Title=CIDetail__Title|" +
                        "Title (if business address)=CIDetail__TitleIfBusiness|" +
                        "Zip=CIDetail__Zip|" +
                        "Zip Code=CIDetail__ZipCode|" +

        //Office top node
        "--Office=|+Abbreviation=Office__Abbr|" +
                   "Custom Field 1=Office__CustomField1|" +
                   "Custom Field 2=Office__CustomField2|" +
                   "Display Name=Office__DisplayName|" +
                   "Firm ID=Office__FirmID|" +
                   "Firm Name=Office__FirmName|" +
                   "Firm Name Upper Case=Office__FirmNameUpperCase|" +
                   "ID=Office__ID|Name=Office__Name|" +
                   "Slogan=Office__Slogan|" +
                   "URL=Office__URL|" + 

             //sub node
            "Address=|+City=Office__City|" +
                      "Country=Office__Country|" +
                      "County=Office__County|" +
                      "Fax 1=Office__Fax1|" +
                      "Fax 2=Office__Fax2|" +
                      "Formatted Address=OfficeAddress|" +
                      "Line 1=Office__Line1|" +
                      "Line 2=Office__Line2|" +
                      "Line 3=Office__Line3|" +
                      "Post City=Office__PostCity|" +
                      "Pre City=Office__PreCity|" +
                      "State=Office__State|" +
                      "State Abbreviation=Office__StateAbbr|" +
                      "Phone 1=Office__Phone1|" +
                      "Phone 2=Office__Phone2|" +
                      "Phone 3=Office__Phone3|" +
                      "Zip=Office__Zip|" +

        //Court top node
        "---Court=|+Court=CourtProperty|" +
                   "ID=Court__ID|" +
                   "Judge Name=Court__JudgeName|" +
                   "Jurisdiction Level 0=Court__L0|" +
                   "Jurisdiction Level 1=Court__L1|" +
                   "Jurisdiction Level 2=Court__L2|" +
                   "Jurisdiction Level 3=Court__L3|" +
                   "Jurisdiction Level 4=Court__L4|" +

             //sub node
             "Address=|+City=Court__City|" +
                       "Country=Court__Country|" +
                       "County=Court__County|" +
                       "Fax 1=Court__Fax1|" +
                       "Fax 2=Court__Fax2|" +
                       "Formatted Address=CourtAddress|" +
                       "Line 1=Court__Line1|" +
                       "Line 2=Court__Line2|" +
                       "Line 3=Court__Line3|" +
                       "Phone 1=Court__Phone1|" +
                       "Phone 2=Court__Phone2|" +
                       "Phone 3=Court__Phone3|" +
                       "Post City=Court__PostCity|" +
                       "Pre City=Court__PreCity|" +
                       "State=Court__State|" +
                       "State Abbreviation=Court__StateAbbr|" +
                       "Zip=Court__Zip|" +

        //Courier top node
        "---Courier=|+ID=Courier__ID|" +
                     "Name=Courier__Name|" +

             //sub node
             "Address=Courier__Address|+City=Courier__City|" +
                                       "Country=Courier__Country|" +
                                       "County=Courier__County|" +
                                       "Fax 1=Courier__Fax1|" +
                                       "Fax 2=Courier__Fax2|" +
                                       "Line 1=Courier__Line1|" +
                                       "Line 2=Courier__Line2|" +
                                       "Line 3=Courier__Line3|" +
                                       "Phone 1=Courier__Phone1|" +
                                       "Phone 2=Courier__Phone2|" +
                                       "Phone 3=Courier__Phone3|" +
                                       "Post City=Courier__PostCity|" +
                                       "Pre City=Courier__PreCity|" +
                                       "State=Courier__State|" +
                                       "State Abbreviation=Courier__StateAbbr|" +
                                       "Zip=Courier__Zip|" +


        //DMS top node
        "---DMS=|+Author ID=DMS__AuthorID|" +
                 "Author Name=DMS__AuthorName|" +
                 "Client ID=DMS__ClientID|" +
                 "Client Name=DMS__ClientName|" +
                 "Comment/Abstract=DMS__Abstract|" +
                 "Creation Date=DMS__CreationDate|" +
                 "Custom 1=DMS__Custom1|" +
                 "Custom 2=DMS__Custom2|" +
                 "Custom 3=DMS__Custom3|" +
                 "Doc Library=DMS__Library|" +
                 "Doc Number=DMS__DocNumber|" +
                 "Doc Type Description=DMS__DocTypeDescription|" +
                 "Doc Type ID=DMS__DocTypeID|" +
                 "Doc Version=DMS__Version|" +
                 "File Name=DMS__FileName|" +
                 "File Path=DMS__Path|" +
                 "Is Profiled?=DMS__IsProfiled|" +
                 "Matter ID=DMS__MatterID|" +
                 "Matter Name=DMS__MatterName|" +
                 "Title/Description=DMS__DocName|" +
                 "Typist ID=DMS__TypistID|" +
                 "Typist Name=DMS__TypistName|" +
                 "Revision Date=DMS__RevisionDate|" +

        //MS Word top node
        "---MS Word=|+Active Document Object Property=Document|" +
                     "Boilerplate Range=Boilerplate|" +
                     "Bookmark Text=Bookmark|" +
                     "Built In Document Property=WordBuiltInDocProperty|" +
                     "Custom Document Property=WordCustomDocProperty|" +
                     "Document Variable=DocumentVariable|" +
                     "Page Setup=PageSetup|" +
                     "Style=Style|" +
                     "Word Application Object Property=WordApplication|" +

        //External Data top node
        "--External Data=|+Client Matter Related Value=CM|" +
                          "External Database Data=ExternalData|" +
                          "Function Return Value=Function|" +

        //Segment Data top node
        "--Segment=|+Child Segment ID=ChildSegmentID|" +
                    "My Tag Value=MyTagValue|" +
                    "Detail Value=DetailValue|" +
                    "Segment ID=SegmentID|" +
                    "Segment Index=SegmentIndex|" +
                    "Segment Insertion Complete=SegmentInsertionComplete|" + 
                    "Segment Level=SegmentLevel|" +
                    "Segment Name=SegmentName|" +
                    "Sub Variable=SubVar|" +
                    "Tag Value=TagValue|" +
                    "Top Segment ID=TopSegmentID|" +
                    "Top Segment Name=TopSegmentName|" +
                    "Variable Value=Variable|" +

        //Word VBA top node
        "--Word VBA=|+Exec=EXEC|" +

        //Text top node
        "--Text=|+Character=Char|" +
                 "Count=Count|" +
                 "Count Entities=CountEntities|" +
                 "Ends With=EndsWith|" +
                 "Extract XML=XMLExtract|" + 
                 "Format XML Fields=XMLFormat|" +
                 "Literal Underscore=Char__95|" +
                 "Lower Case=LowerCase|" +
                 "Replace=Replace|" +
                 "Substring After=SubstringAfter|" +
                 "Substring After Last=SubstringAfterLast|" +
                 "Substring Before=SubstringBefore|" +
                 "Substring Before Last=SubstringBeforeLast|" +
                 "Starts With=StartsWith|" +
                 "Truncate=Truncate|" +
                 "Unicode Character=UChar|" +
                 "Upper Case=UpperCase|" +

        //Number top node
        "--Number=|+Addition=Add|" +
                   "Convert=Convert|" +
                   "Division=Divide|" +
                   "Is Numeric=IsNumeric|" +
                   "Modulus=Modulus|" +
                   "Multiplication=Multiply|" +
                   "Number Text=NumberText|" +
                   "Ordinal suffix=OrdinalSuffix|" +
                   "Subtraction=Subtract|" +

        //Mapping top node
        "--Mapping=|+Choose=Choose|" +
                    "List Lookup=ListLookup|" +

        //Other top node
        "--Other=|+Jurisdiction Text=Level|" +
                  "Location ID=LocationID|" +
                  "US State Indefinite Article=USStateIndefiniteArticle|" +

        //Date Time top node
        //GLOG : 8060 : jsw : remove 'Today's Date'
        //GLOG : 8374 : jsw 
        //add new date calcuation choices
        "--Date Time=|+Date Format=DateFormat|" +
                      "Format Date/Time=FormatDateTime|" +
                      "Date Difference=DateDiff|" +
                      "Date Add=DateAdd|" +
                      "Date Subtract=DateSubtract| +";

        #endregion *********************constants*********************

        #region *********************constructors*********************
        public ExpressionBuilder()
        {
            InitializeComponent();
            try
            {
                RefreshNodes();
                //this.ShowDialog();
                //GLOG : 8546 : ceh
                this.txtExpression.Height = (int)(68 * LMP.OS.GetScalingFactor());
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.FileException(LMP.Resources.GetLangString("Error_FileDoesNotExist"), oE);
            }
        }

        #endregion *********************constructors*********************

        #region *********************properties*********************
        /// <summary>
        /// get/sets the expression that is displayed in the form
        /// </summary>
        public string Expression
        {
            get { return this.txtExpression.Text; }
            set { this.txtExpression.Text = value; }
        }

        #endregion *********************properties*********************

        #region *********************methods*********************

        public void RefreshNodes()
        {
            /// <summary>
            /// populates the field code nodes with values from the xFieldCodes string.
            /// </summary>
            /// 

            UltraTreeNode oNode = null;
            string xDisplayName = "";
            string xFieldCode = "";
            string xOperator = "";

            //clear tree
            this.treeFieldCodes.Nodes.Clear();

            //split constant into an array
            string[] aFieldCodes = xFieldCodes.Split('|');

            //cycle through array, adding each array item to string
            for (int i = 0; i < aFieldCodes.Length; i++)
            {
                //get left and right sides of array item
                int iPos = aFieldCodes[i].IndexOf("=");
                if (iPos == -1)
                    continue;
                string xLeftSide = aFieldCodes[i].Substring(0, iPos);
                xFieldCode = aFieldCodes[i].Substring(iPos + 1);

                //split left side into display name and operator
                int iOperatorPos = xLeftSide.LastIndexOfAny(new char[] { '+', '-' });
                xDisplayName = xLeftSide.Substring(iOperatorPos + 1);

                //get operator
                xOperator = "";
                if (iOperatorPos > -1)
                    xOperator = xLeftSide.Substring(0, iOperatorPos + 1);

                //add node
                oNode = AddNode(xDisplayName, xFieldCode, xOperator, oNode);
            }
        }

        private UltraTreeNode AddNode(string xDisplayName, string xFieldCode, string xOperator, UltraTreeNode oNode)
        {
            TreeNodesCollection oNodes = this.treeFieldCodes.Nodes;
            UltraTreeNode oTargetNode, oNewNode;
            bool bAddRootNode = false;
            string xPrefix;

            if (xOperator.Length > 0)
                xPrefix = xOperator.Substring(0, 1);
            else
                xPrefix = "";
            switch (xPrefix)
            {
                case "+":
                    //new child level
                    //xDisplayName = xDisplayName.Substring(1);
                    try
                    {
                        if (oNode != null)
                        {
                            UltraTreeNode oChildNode = oNode.Nodes.Add(xFieldCode, xDisplayName);
                            oNode = oChildNode;
                        }
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(LMP.Resources.GetLangString("Error_ArrayBoundsDontMatch"), oE);
                    }
                    break;
                case "-":
                    //cycle through string and get each "-"

                    if (oNode != null)
                    {
                        oTargetNode = oNode;

                        int i = 0;

                        while (i <= xOperator.Length)
                        {
                            if (oTargetNode.IsRootLevelNode == false)
                                oTargetNode = oTargetNode.Parent;
                            else
                                bAddRootNode = true;
                            i++;
                        }
                        //xDisplayName = xDisplayName.Substring(i);
                        if (bAddRootNode)
                            oNewNode = this.treeFieldCodes.Nodes.Add(xFieldCode, xDisplayName);
                        else
                            oNewNode = oTargetNode.Nodes.Add(xFieldCode, xDisplayName);
                        oNode = oNewNode;
                    }
                    bAddRootNode = false;
                    break;
                default:
                    try
                    {
                        if (oNode != null)
                        {
                            //add node on same level
                            UltraTreeNode oTreeNode = oNode.Parent.Nodes.Add(xFieldCode, xDisplayName);
                            oNode = oTreeNode;
                        }
                        else
                        {
                            UltraTreeNode oTreeNode = this.treeFieldCodes.Nodes.Add(
                            xFieldCode, xDisplayName);
                            oNode = oTreeNode;
                        }
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(LMP.Resources.GetLangString("Error_ArrayBoundsDontMatch"), oE);
                    }
                    break;

            }
            return oNode;
        }

        #endregion *********************methods*********************

        #region *********************private functions*********************
        
        //captures node click
        private void treeFieldCodes_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (e.NewSelections.Count > 0)
                {
                    UltraTreeNode oSelNode = e.NewSelections[0];

                    this.SelectNode(oSelNode);
                }
            }
            catch (System.Exception oE)
            {
                //throw new LMP.Exceptions.NotInCollectionException(LMP.Resources.GetLangString("Error_ArrayBoundsDontMatch"), oE);
                LMP.Error.Show(oE);
            }
        }

        //appends node tag to m_xExpression
        //displays appropriate panel for field code
        private void SelectNode(UltraTreeNode oNode)
        {
            try
            {
                string xTag;
                string xFieldCode;

                xTag = oNode.Key;

                int iPos = xTag.IndexOf("__");
                if (iPos != -1 && xTag != "Char_95")
                    xFieldCode = xTag.Substring(0, iPos);
                else
                    xFieldCode = xTag;

                DisplayDescription(oNode);

                ParameterPanel oNewPanel = m_oPanel;

                switch (xFieldCode)
                {
                    case "Author":
                    case "AuthorOffice":
                    case "AuthorOfficeAddress":
                    case "PreviousAuthor":
                    case "PreviousAuthorOffice":
                    case "PreviousAuthorOfficeAddress":
                        //spinner for index, combo for ref Qualifier
                        oNewPanel = ((ParameterPanel)new AuthorDetailPanel(xTag));
                        break;
                    case "Authors":
                    case "PreviousAuthors":
                    case "AdditionalAuthors":
                    case "AuthorsCount":
                    case "PreviousAuthorsCount":
                    case "AdditionalAuthorsCount":
                    case "AuthorsOffice":
                    case "PreviousAuthorsOffice":
                    case "AdditionalAuthorsOffice":
                    case "LeadAuthor":
                    case "LeadAuthorOffice":
                    case "PreviousLeadAuthor":
                    case "PreviousLeadAuthorOffice":
                    case "AuthorsOfficeAddress":
                    case "PreviousAuthorsOfficeAddress":
                    case "AdditionalAuthorsOfficeAddress":
                    case "UserOfficeAddress":
                    case "LeadAuthorOfficeAddress":
                    case "PreviousLeadAuthorOfficeAddress":
                    case "CourtAddress":
                    case "SegmentIndex":
                        //combo for ref qualifiers
                        oNewPanel = ((ParameterPanel)new AuthorsDetailPanel(xTag));
                        break;
                    case "AuthorPreference":
                    case "AuthorParentPreference":
                    case "PreviousAuthorPreference":
                    case "PreviousAuthorParentPreference":
                        //spinner for index, text box for keyname, combo for ref Qualifier
                        oNewPanel = ((ParameterPanel)new AuthorPreferencePanel(xTag));
                        break;
                    case "Style":
                    case "Boilerplate":
                    case "Level":
                    case "PageSetup":
                    case "FormatDateTime":
                    case "Count":
                    case "StartsWith":
                    case "EndsWith":
                    case "SubstringAfter":
                    case "SubstringAfterLast":
                    case "SubstringBefore":
                    case "SubstringBeforeLast":
                        //textbox for name, textbox for parameters
                        oNewPanel = ((ParameterPanel)new WordObjectMultipleDetailPanel(xTag));
                        break;
                    case "XMLExtract":
                        //multiline textbox for xml, textbox for tag name
                        oNewPanel = ((ParameterPanel)new XMLExtractPanel(xTag));
                        break;
                    case "XMLFormat":
                        //multiline textbox for xml, textbox for tag name
                        oNewPanel = ((ParameterPanel)new XMLFormatPanel(xTag));
                        break;
                    case "TypePreference":
                    case "ObjectPreference":
                    case "ApplicationPreference":
                    case "UserSetting":
                    case "FirmSetting":
                    case "SegmentID":
                    case "DateFormat":
                        //textbox for name or id
                        oNewPanel = ((ParameterPanel)new PreferenceDetailPanel(xTag));
                        break;
                    case "Person":
                    case "PersonOffice":
                    case "Courier":
                    case "Variable":
                    case "ChildSegmentID":
                    case "Office":
                    case "PersonOfficeAddress":
                    case "OfficeAddress":
                    case "LeadAuthorPreference":
                    case "LeadAuthorParentPreference":
                    case "PreviousLeadAuthorPreference":
                    case "PreviousLeadAuthorParentPreference":
                        //textbox for name or id, combo for ref qualifiers
                        oNewPanel = ((ParameterPanel)new OfficeDetailPanel(xTag));
                        break;
                    case "Bookmark":
                    case "DocumentVariable":
                    case "Document":
                    case "WordApplication":
                    case "CM":
                    case "EXEC":
                    case "Char":
                    case "UChar":
                    case "WordBuiltInDocProperty":
                    case "WordCustomDocProperty":
                    case "SubVar":
                    case "LowerCase":
                    case "UpperCase":
                    case "SegmentName":
                    case "TagValue":
                    case "CountEntities":
                    case "CreationDate":
                    case "RevisionDate":
                    case "IsNumeric":  //GLOG : 8375 : JSW
                        //textbox for parameters
                        oNewPanel = ((ParameterPanel)new WordObjectSingleDetailPanel(xTag));
                        break;
                    case "Replace":
                        //3 parameter textboxes
                        oNewPanel = ((ParameterPanel)new ReplaceDetailPanel(xTag));
                        break;
                    case "Function":
                        //3 name textboxes, 1 parameter textbox
                        oNewPanel = ((ParameterPanel)new FunctionDetailPanel(xTag));
                        break;
                    case "ExternalData":
                        //4 textboxes
                        oNewPanel = ((ParameterPanel)new ExternalConnectionDetailPanel(xTag));
                        break;
                    case "Truncate":
                        //textbox, spinner for index
                        oNewPanel = ((ParameterPanel)new TruncatePanel(xTag));
                        break;
                    case "SegmentLevel":
                    case "NumberText":
                        //spinner for index
                        oNewPanel = ((ParameterPanel)new SegmentLevelPanel(xTag));
                        break;
                    case "CourtProperty":
                        //court property dropdown
                        oNewPanel = ((ParameterPanel)new CourtPropertyPanel(xTag));
                        break;
                    case "Convert":
                        //number convert property dropdown
                        oNewPanel = ((ParameterPanel)new ConvertPropertyPanel(xTag));
                        break;
                    case "Choose":
                        //number convert property dropdown
                        oNewPanel = ((ParameterPanel)new ChooseDetailPanel(xTag));
                        break;
                    case "LocationID":
                        //number convert property dropdown
                        oNewPanel = ((ParameterPanel)new LocationPropertyPanel(xTag));
                        break;
                    case "Add":
                    case "Subtract":
                    case "Multiply":
                    case "Divide":
                    case "Modulus": //GLOG 4452
                        oNewPanel = ((ParameterPanel)new MathPanel(xTag));
                        break;
                    case "ListLookup":
                        oNewPanel = ((ParameterPanel)new ListLookupPanel(xTag));
                        break;
                    //GLOG : 8374 : JSW
                    case "DateDiff":
                        oNewPanel = ((ParameterPanel)new DateDiffPropertyPanel(xTag));
                        break;
                    case "DateAdd":
                    case "DateSubtract":
                        oNewPanel = ((ParameterPanel)new DateAddPropertyPanel(xTag));
                        break;
                    default:
                        oNewPanel = ((ParameterPanel)new BlankPanel(xTag));
                        break;
                }
                if (oNewPanel != null)
                {
                    this.gBoxParameters.Controls.Add(oNewPanel);
                    oNewPanel.Dock = DockStyle.Fill;
                    //if (oNewPanel.Controls.Count > 0)
                        //oNewPanel.Controls[1].Select();

                    //remove previous panel if necessary
                    if (m_oPanel != null)
                        this.gBoxParameters.Controls.Remove(m_oPanel);

                    m_oPanel = oNewPanel;
                }
                oNode.Selected = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Displays field description 
        /// </summary>
        private void DisplayDescription(UltraTreeNode oNode)
        {
            string xDescription = "";

            if (oNode.Key == "")
            {
                xDescription = oNode.Text;
            }
            else
            {
                xDescription = oNode.Parent.Text + ":  " +  oNode.Text;
            }

            this.lblDescription.Text = xDescription;
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            try
            {
                //GLOG - 3286 - CEH
                if (m_oPanel != null)
                {
                    string xExpression = m_oPanel.Value;

                    //get selection start and length
                    int iSelStart = this.txtExpression.SelectionStart;
                    int iSelLen = this.txtExpression.SelectionLength;

                    string xLeft = this.txtExpression.Text.Substring(0, iSelStart);
                    string xRight = this.txtExpression.Text.Substring(iSelStart + iSelLen);

                    this.txtExpression.Focus();

                    if (xExpression.Length > 0)
                    {
                        //add space before if necessary
                        if (xLeft.Length > 0 && xLeft.Substring(xLeft.Length - 1, 1) != " ")
                        {
                            xExpression = " " + xExpression;
                        }
                        //add space after if necessary
                        if (xRight.Length == 0 || xRight.Substring(0, 1) != " ")
                        {
                            xExpression = xExpression + " ";
                        }
                    }
                    this.txtExpression.Text = xLeft + xExpression + xRight;
                    this.txtExpression.SelectionStart = iSelStart + xExpression.Length;

                    //save current expression in list
                    m_oExpressionList.Add(this.txtExpression.Text);
                    m_bTextChanged = false;
                }
            }
            catch (System.Exception oE)
            {
                //throw new LMP.Exceptions.UINodeException(
                //    LMP.Resources.GetLangString("Error_ControlActionExecutionFailed"), oE);
                LMP.Error.Show(oE);
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_bTextChanged)
                {
                    this.txtExpression.Undo();
                }
                else
                {
                    m_iExpressionCounter = m_oExpressionList.Count;
                    string xExpression;
                    if (m_iExpressionCounter > 1)
                    {
                        this.txtExpression.Text = m_oExpressionList[m_iExpressionCounter - 2].ToString();
                    }
                    else
                    {
                        this.txtExpression.Text = "";

                    }
                    if (m_iExpressionCounter > 0)
                    {
                        xExpression = m_oExpressionList[m_iExpressionCounter - 1].ToString();
                        m_oExpressionList.Remove(xExpression);
                    }
                    m_bTextChanged = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                int iPos;
                LMP.Architect.Api.Expression.SyntaxErrors bytErrType;
                string xMsg = "";

                this.txtExpression.Text = this.txtExpression.Text.Trim();

                if (!LMP.Architect.Api.Expression.SyntaxIsValid(
                    this.txtExpression.Text, out iPos, out bytErrType, false))
                {
                    switch (bytErrType)
                    {
                        case LMP.Architect.Api.Expression.SyntaxErrors.InvalidFieldCodeSyntax:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidFieldCodeSyntax");
                            break;
                        case LMP.Architect.Api.Expression.SyntaxErrors.InvalidOperands:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionOperands");
                            break;
                        case LMP.Architect.Api.Expression.SyntaxErrors.InvalidOperator:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionOperator");
                            break;
                        case LMP.Architect.Api.Expression.SyntaxErrors.MissingSpace:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionMissingSpace");
                            break;
                        case LMP.Architect.Api.Expression.SyntaxErrors.UnmatchedParen:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionUnmatchedParenthesis");
                            break;
                        case LMP.Architect.Api.Expression.SyntaxErrors.InvalidPreferenceExpression:
                            xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionPreferenceCodesNotAlone");
                            break;
                    }

                    txtExpression.Select(iPos, 1);

                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    this.DialogResult = DialogResult.None;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnBrackets_Click(object sender, EventArgs e)
        {
            try
            {
                string xExpression;
                string xSelectedText;
                int iPos;
                int iLen;

                xExpression = txtExpression.Text;
                iPos = txtExpression.SelectionStart;
                iLen = txtExpression.SelectionLength;
                xSelectedText = txtExpression.SelectedText;
                //nothing selected, add to end

                if (iPos == 0 && iLen == 0)
                    txtExpression.Text = xExpression + "{}";
                //insert at cursor position.
                else
                {
                    if (iLen == 0)
                    {
                        txtExpression.Text = xExpression.Substring(0, iPos) +
                            "{}" + xExpression.Substring(iPos);
                    }
                    else
                    {
                        txtExpression.Text = xExpression.Substring(0, iPos) +
                            "{" + xSelectedText + "}" + xExpression.Substring(iPos + iLen);
                    }
                }
                //save current expression in list
                m_oExpressionList.Add(this.txtExpression.Text);
                m_bTextChanged = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnAnd_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^AND"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnOr_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^OR"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnLike_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^LIKE"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnIf_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^?"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void btnElse_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^:"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^="); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnNotEquals_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^!="); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnGreater_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^GT"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnLess_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^LT"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnGreaterEquals_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^GTE"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnLesserEquals_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("^LTE"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
 
        private void btnMyValue_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("[MyValue]"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("[Empty]"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnNull_Click(object sender, EventArgs e)
        {
            try { AddButtonValue("[Null]"); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtExpression_TextChanged(object sender, EventArgs e)
        {
            try { m_bTextChanged = true; }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtExpression_LostFocus(object sender, EventArgs e)
        {
            try
            {
                if (m_bTextChanged == true)
                {
                    this.m_oExpressionList.Add(this.txtExpression.Text);
                    m_bTextChanged = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AddButtonValue(string xOperator )
        {
            try
            {
                string xExpression = this.txtExpression.Text;
                int iSelStart = this.txtExpression.SelectionStart;
                int iSelLen = this.txtExpression.SelectionLength;
                string xSelectedText = this.txtExpression.SelectedText;

                string xLeft = xExpression.Substring(0, iSelStart);
                string xRight = xExpression.Substring(iSelStart + iSelLen);

                this.txtExpression.Focus(); 
                
                if (xLeft.Length > 0 && xLeft.Substring(xLeft.Length - 1, 1) != " ")
                {
                    //add a preceding space to operator
                    xOperator = " " + xOperator;
                }
                if (xRight.Length == 0 || xRight.Substring(0, 1) != " ")
                {
                    //add a following space to operator
                    xOperator = xOperator + " ";
                }
 
                txtExpression.Text = xLeft +
                        xOperator + xRight;
                    txtExpression.SelectionStart = iSelStart + xOperator.Length;

                //save current expression in list
                    m_oExpressionList.Add(this.txtExpression.Text);
                    m_bTextChanged = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try { this.Close(); }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }           
        }

        #endregion *********************private functions*********************

        #region *********************helper classes*********************

        private class AuthorsDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.ComboBox oAddressFormats = new LMP.Controls.ComboBox();

            public AuthorsDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //display ref qualifier controls
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Target:", oLabel1);
                this.Controls.Add(oLabel1);
                
                //combo box for Reference Qualifier
                LMP.Controls.ComboBox oCmbRefQualifier = new LMP.Controls.ComboBox();
                SetUpCombo(2, "RefQualifier", oCmbRefQualifier);
                this.Controls.Add(oCmbRefQualifier);

                if (m_xFieldCodeBase.IndexOf("OfficeAddress") > 0)
                {
                    //label for address format

                    System.Windows.Forms.Label oLabel2 = new Label();
                    SetUpLabel(3, "&Address Formats:", oLabel2);
                    this.Controls.Add(oLabel2);

                    //dropdown for address format and ellipsis button
                    //oAddressFormats = new LMP.Controls.ComboBox();
                    SetUpCombo(4, "AddressFormat", oAddressFormats);
                    this.Controls.Add(oAddressFormats);

                    //button for adding address format
                    System.Windows.Forms.Button oButton = new Button();
                    SetUpButton(4, "...", oButton);
                    this.Controls.Add(oButton);
                }
				// GLOG : 3438 : CEH
                if (m_xFieldCodeBase.IndexOf("CourtAddress") == 0)
                {
                    //label for address format

                    System.Windows.Forms.Label oLabel2 = new Label();
                    SetUpLabel(3, "&Address Formats:", oLabel2);
                    this.Controls.Add(oLabel2);

                    //dropdown for address format and ellipsis button
                    //oAddressFormats = new LMP.Controls.ComboBox();
                    SetUpCombo(4, "AddressFormat", oAddressFormats);
                    this.Controls.Add(oAddressFormats);

                    //button for adding address format
                    System.Windows.Forms.Button oButton = new Button();
                    SetUpButton(4, "...", oButton);
                    this.Controls.Add(oButton);
                }

                if (m_xFieldCodeBase.IndexOf("BarID") > 0)
                {
                    //label for name
                    System.Windows.Forms.Label oLabel3 = new Label();
                    SetUpLabel(3, "&Bar ID Index or Description:", oLabel3);
                    this.Controls.Add(oLabel3);

                    //textbox for name
                    LMP.Controls.TextBox oTxtBarID = new LMP.Controls.TextBox();
                    SetUpTextBox(4, "BarID", oTxtBarID);
                    this.Controls.Add(oTxtBarID);
                }
            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xRefQualifier = "";
                string xBarIDParam = "";
                string xField = "";
                string xParam = "";

                //clear out previous Expression string
                m_xExpression = "";

                LMP.Controls.ComboBox oRefQualifier = (LMP.Controls.ComboBox) this.Controls["RefQualifier"];

                if (this.Controls["BarID"] != null)
                    xBarIDParam = this.Controls["BarID"].Text;
                
                if (oRefQualifier != null)
                {
                    xRefQualifier = oRefQualifier.Value;
                    if (xRefQualifier != "None")
                    {
                        xRefQualifier += "::";
                    }
                }
                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                if (xField.IndexOf("OfficeAddress") != -1 || xField.IndexOf("CourtAddress") != -1)
                {
                    //m_xExpression = xField + "__" + this.Controls["AddressFormat"].Text;
                    int iListIndex = oAddressFormats.SelectedIndex;
                    m_xExpression = xField + "__" + oAddressFormats.ListArray[iListIndex, 1];
                }
                else
                {
                    if (xParam.Length > 0)
                    {
                        m_xExpression = xField + "__" + xParam;
                    }
                    else
                    {
                        m_xExpression = xField;
                    }
                
                }
                if (xRefQualifier != "None")
                {
                    m_xExpression = xRefQualifier +
                        m_xExpression;
                }

                if (xBarIDParam != "")
                {
                    m_xExpression = m_xExpression + "__" + xBarIDParam;
                }

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";

            }
        }
        private class AuthorDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.ComboBox oAddressFormats = new LMP.Controls.ComboBox();
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public AuthorDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for ref qualifiers
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(1, "&Target:", oLabel2);
                this.Controls.Add(oLabel2);

                //combo box for Reference Qualifier
                LMP.Controls.ComboBox oCmbRefQualifier = new LMP.Controls.ComboBox();
                SetUpCombo(2, "RefQualifier", oCmbRefQualifier);
                this.Controls.Add(oCmbRefQualifier);

                //label for index
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(3, "&Index:", oLabel1);
                this.Controls.Add(oLabel1);

                //spinner control for index
                SetUpSpinner(4, "Index", oSpinnerIndex);
                this.Controls.Add(oSpinnerIndex);
               
                if (m_xFieldCodeBase.IndexOf("OfficeAddress") > 0)
                {
                    //display address format controls

                    //label for address format
                    System.Windows.Forms.Label oLabel3 = new Label();
                    SetUpLabel(5, "&Address Formats:", oLabel3);
                    this.Controls.Add(oLabel3);

                    //dropdown for address format and ellipsis button
                    SetUpCombo(6, "AddressFormat", oAddressFormats);
                    this.Controls.Add(oAddressFormats);

                    //button for adding address format
                    System.Windows.Forms.Button oButton = new Button();
                    SetUpButton(6, "...", oButton);
                    this.Controls.Add(oButton);

                    ////position controls for index
                    //oLabel1.SetBounds(16, 16, 150, 13);
                    //oSpinnerIndex.SetBounds(16, 32, 150, 20);
                }
                if (m_xFieldCodeBase.IndexOf("BarID") > 0)
                {
                    //label for name
                    System.Windows.Forms.Label oLabel4 = new Label();
                    SetUpLabel(5, "&Bar ID Index or Description:", oLabel4);
                    this.Controls.Add(oLabel4);

                    //textbox for name
                    LMP.Controls.TextBox oTxtBarID = new LMP.Controls.TextBox();
                    SetUpTextBox(6, "BarID", oTxtBarID);
                    this.Controls.Add(oTxtBarID);
                }
            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xIndex = oSpinnerIndex.Value;
                string xRefQualifier = ""; 
                string xField = "";
                string xParam = "";
                string xBarIDParam = "";

                LMP.Controls.ComboBox oRefQualifier = (LMP.Controls.ComboBox)this.Controls["RefQualifier"];

                if (this.Controls["BarID"] != null)
                    xBarIDParam = this.Controls["BarID"].Text;

                {
                    xRefQualifier = oRefQualifier.Value;
                    if (xRefQualifier != "None")
                    {
                        xRefQualifier += "::";
                    }
                }

                //clear out previous expression
                m_xExpression = "";
               
                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = xField + xIndex;
                
                if (xField.IndexOf("OfficeAddress") != -1)
                {
                    int iListIndex = oAddressFormats.SelectedIndex;
                    m_xExpression = m_xExpression + "__" + oAddressFormats.ListArray[iListIndex, 1];
                }
                else
                {
                    if (xParam.Length > 0)
                        m_xExpression += "__" + xParam;
                }
                if (xRefQualifier != "None")
                    m_xExpression = xRefQualifier +
                        m_xExpression;

                if (xBarIDParam != "")
                    m_xExpression = m_xExpression + "__" + xBarIDParam;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class AuthorPreferencePanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public AuthorPreferencePanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for Reference Qualifier
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Target:", oLabel1);
                this.Controls.Add(oLabel1);

                //combo box for Reference Qualifier
                LMP.Controls.ComboBox oCmbRefQualifier = new LMP.Controls.ComboBox();
                SetUpCombo(2, "RefQualifier", oCmbRefQualifier);
                this.Controls.Add(oCmbRefQualifier);

                //label for index
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Index:", oLabel2);
                this.Controls.Add(oLabel2);

                //spinner control for index
                SetUpSpinner(4, "Index", oSpinnerIndex);
                this.Controls.Add(oSpinnerIndex);

                //label for name
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(5, "&Key Name:", oLabel3);
                this.Controls.Add(oLabel3);

                //textbox for name
                LMP.Controls.TextBox oTxtName = new LMP.Controls.TextBox();
                SetUpTextBox(6, "KeyName", oTxtName);
                this.Controls.Add(oTxtName);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xRefQualifier = "";
                string xIndex = oSpinnerIndex.Value;
                string xKeyName = this.Controls["KeyName"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                LMP.Controls.ComboBox oRefQualifier = (LMP.Controls.ComboBox)this.Controls["RefQualifier"];

                if (oRefQualifier != null)
                {
                    xRefQualifier = oRefQualifier.Value;
                    if (xRefQualifier != "None")
                    {
                        xRefQualifier += "::";
                    }
                }

                xField = m_xFieldCodeBase;

                m_xExpression = xField + xIndex +
                    "__" + xKeyName;

                if (xRefQualifier != "None")
                {
                    m_xExpression = xRefQualifier +
                        m_xExpression;
                }

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class OfficeDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.ComboBox oAddressFormats = new LMP.Controls.ComboBox();

            public OfficeDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;
                string xField = "";

                System.Windows.Forms.Label oLabel1 = new Label();
                LMP.Controls.TextBox oTxtName = new LMP.Controls.TextBox();
                
                if (m_xFieldCodeBase.StartsWith("Office"))
                {
                    //label for Name textbox
                    SetUpLabel(1, "&Office:", oLabel1);
                    this.Controls.Add(oLabel1);

                    //combo box for Offices
                    LMP.Controls.ComboBox oCmbOffices = new LMP.Controls.ComboBox();
                    SetUpCombo(2, "Office", oCmbOffices);
                    this.Controls.Add(oCmbOffices);
                }
                else
                {
                    //display ref qualifer controls

                    //label for Reference Qualifier
                    System.Windows.Forms.Label oLabel2 = new Label();
                    SetUpLabel(1, "&Target:", oLabel2);
                    this.Controls.Add(oLabel2);

                    //combo box for Reference Qualifier
                    LMP.Controls.ComboBox oCmbRefQualifier = new LMP.Controls.ComboBox();
                    SetUpCombo(2, "RefQualifier", oCmbRefQualifier);
                    this.Controls.Add(oCmbRefQualifier);

                    //label for Name textbox
                    SetUpLabel(3, "&Name:", oLabel1);
                    this.Controls.Add(oLabel1);
                    //textbox for name
                    SetUpTextBox(4, "Name", oTxtName);
                    this.Controls.Add(oTxtName);
                }
                if (m_xFieldCodeBase.IndexOf("OfficeAddress") > -1)
                {
                    //Display address format controls
                    
                    //label for address format
                    int iControlPos;
                    if (m_xFieldCodeBase == "OfficeAddress")
                        iControlPos = 3;
                    else
                    {
                        iControlPos = 4;
                    }
                    System.Windows.Forms.Label oLabel3 = new Label();
                    SetUpLabel(iControlPos, "&Address Formats:", oLabel3);
                    this.Controls.Add(oLabel3);

                    //dropdown for address format and ellipsis button
                    SetUpCombo(iControlPos + 1, "AddressFormat", oAddressFormats);
                    this.Controls.Add(oAddressFormats);

                    //button for adding address format
                    System.Windows.Forms.Button oButton = new Button();
                    SetUpButton(iControlPos + 1, "...", oButton);
                    this.Controls.Add(oButton);
                }

                if (m_xFieldCodeBase.IndexOf("BarID") > 0)
                {
                    //label for name
                    System.Windows.Forms.Label oLabel4 = new Label();
                    SetUpLabel(5, "&Bar ID Index or Description:", oLabel4);
                    this.Controls.Add(oLabel4);

                    //textbox for name
                    LMP.Controls.TextBox oTxtBarID = new LMP.Controls.TextBox();
                    SetUpTextBox(6, "BarID", oTxtBarID);
                    this.Controls.Add(oTxtBarID);
                }
                
                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                }
                else
                {
                    xField = xFieldCodeBase;
                }
                switch (xField)
                {
                    case "LeadAuthorPreference":
                    case "LeadAuthorParentPreference": 
                    case "PreviousLeadAuthorPreference":
                    case "PreviousLeadAuthorParentPreference":
                        oLabel1.Text = "&Key Name:";
                        break;
                    case "Courier":
                        oLabel1.Text = "&Courier ID:";
                        break;
                    case "Person": 
                    case "PersonOffice":
                    case "PersonOfficeAddress":
                        oLabel1.Text = "&Person ID:";
                        break;
                    case "Variable": 
                        oLabel1.Text = "&Variable Name:";
                        break;
                    case "ChildSegmentID":
                        oLabel1.Text = "&Child Segment ID:";
                        break;
                }
            }

          public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xName = "";
                
                string xRefQualifier = "";
                string xOffice = "";
                string xField = "";
                string xParam = "";
                string xBarIDParam = "";

                //clear out previous expression
                m_xExpression = "";

                LMP.Controls.ComboBox oRefQualifier = (LMP.Controls.ComboBox)this.Controls["RefQualifier"];
                LMP.Controls.ComboBox oOffices = (LMP.Controls.ComboBox)this.Controls["Office"];

                if (this.Controls["Name"] != null)
                    xName = this.Controls["Name"].Text;

                if (this.Controls["BarID"] != null)
                    xBarIDParam = this.Controls["BarID"].Text;
                
                if (oRefQualifier != null)
                {
                    xRefQualifier = oRefQualifier.Value;
                    if (xRefQualifier != "None")
                    {
                        xRefQualifier += "::";
                    }
                }

                if (oOffices != null)
                    xOffice = oOffices.Value;

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = m_xExpression + xField;

                switch (xField)
                {
                    case "Variable":
                    case "ChildSegmentID":
                    case "LeadAuthorPreference":
                    case "LeadAuthorParentPreference":
                    case "PreviousLeadAuthorPreference":
                    case "PreviousLeadAuthorParentPreference":
                        m_xExpression = m_xExpression + "__" + xName;
                        break;
                    case "Office":
                    case "OfficeAddress":
                        m_xExpression = m_xExpression + xOffice;
                        break;
                    default:
                         m_xExpression = m_xExpression + xName;
                         break;
                }

                if (xField.IndexOf("OfficeAddress") != -1)
                {
                    int iListIndex = oAddressFormats.SelectedIndex;
                    m_xExpression = m_xExpression + "__" + oAddressFormats.ListArray[iListIndex, 1];
                }
                else
                {
                    if (xParam.Length > 0)
                        m_xExpression = m_xExpression + "__" + xParam;
                }
                if (xRefQualifier != "None")
                    m_xExpression = xRefQualifier +
                        m_xExpression;

                if (xBarIDParam != "")
                    m_xExpression = m_xExpression + "__" + xBarIDParam;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";
            }

        }
        private class PreferenceDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.ComboBox oAddressFormats = new LMP.Controls.ComboBox();

            public PreferenceDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;
                string xField = "";
                string xLabel = "";

                switch (m_xFieldCodeBase)
                {
                    case "SegmentID":
                        xLabel = "&Segment Name:";
                        break;
                    case "DateFormat":
                        xLabel = "&Date Format:";
                        break;
                    default:
                        xLabel = "&Key Name:";
                        break;
                }


                //label for name
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, xLabel, oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for name
                LMP.Controls.TextBox oTxtName = new LMP.Controls.TextBox();
                SetUpTextBox(2, "Name", oTxtName);
                this.Controls.Add(oTxtName);

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                }
                else
                {
                    xField = xFieldCodeBase;
                }
                if (m_xFieldCodeBase.IndexOf("OfficeAddress") != -1)
                {
                    //label for address format
                    System.Windows.Forms.Label oLabel3 = new Label();
                    SetUpLabel(5, "&Address Formats:", oLabel3);
                    this.Controls.Add(oLabel3);

                    //dropdown for address format and ellipsis button
                    SetUpCombo(6, "AddressFormat", oAddressFormats);
                    this.Controls.Add(oAddressFormats);

                    //button for adding address format
                    System.Windows.Forms.Button oButton = new Button();
                    SetUpButton(6, "...", oButton);
                    this.Controls.Add(oButton);
                }
            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xName = this.Controls["Name"].Text;
                string xField = "";
                string xParam = "";

                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                if (xName.Length > 0)
                    m_xExpression = m_xExpression + xField + "__" + xName;

                if (xField.IndexOf("OfficeAddress") != -1)
                {
                    int iListIndex = oAddressFormats.SelectedIndex;
                    m_xExpression = xField + "__" + oAddressFormats.ListArray[iListIndex, 1];
                }
                else
                {
                    if (xParam.Length > 0)
                        m_xExpression = m_xExpression + "__" + xParam;
                }

                m_xExpression = "[" + m_xExpression + "]";
            }

        }
        private class WordObjectMultipleDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();
            private LMP.Controls.TextBox oTxtBox = new LMP.Controls.TextBox();

            public WordObjectMultipleDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;
                
                //label for name
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Name:", oLabel1);
                this.Controls.Add(oLabel1);

                if (xFieldCodeBase == "PageSetup" || xFieldCodeBase == "Level")
                {
                    //spinner for page setup
                    SetUpSpinner(2, "Name", oSpinnerIndex);
                    if (xFieldCodeBase == "Level")
                        oSpinnerIndex.Maximum = 4;
                    this.Controls.Add(oSpinnerIndex);
                }
                else
                {
                    //textbox for name for everything else.
                    SetUpTextBox(2, "Name", oTxtBox);
                    this.Controls.Add(oTxtBox);
                }


                //label for parameter
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Parameter:", oLabel2);
                this.Controls.Add(oLabel2);

                //textbox for parameter
                LMP.Controls.TextBox oTxtParameter = new LMP.Controls.TextBox();
                SetUpTextBox(4, "Parameter", oTxtParameter);
                this.Controls.Add(oTxtParameter);

                //create label captions according to field code selected
                switch (xFieldCodeBase)
                {
                    case "Style": //style name, parameters
                        oLabel1.Text = "&Style Name:";
                        oLabel2.Text = "&Property String:";
                        break;
                    case "Boilerplate": //file name, bookmark
                        oLabel1.Text = "&File Name:";
                        oLabel2.Text = "&Bookmark Name:";
                        break;
                    case "Level": //Level Number, ID
                        oLabel1.Text = "&Jurisdiction Level:";
                        oLabel2.Text = "&ID:";
                        break;
                    case "PageSetup": //index, parameters
                        oLabel1.Text = "&Section Index:";
                        oLabel2.Text = "&Property Name:";
                        break;
                    case "FormatDateTime": //date format, date string
                        oLabel1.Text = "&Date Format:";
                        oLabel2.Text = "&Date String:";
                        break;
                    case "Count": //search text, source text
                        oLabel1.Text = "&Search Text:";
                        oLabel2.Text = "Source &Text:";
                        break;
                    case "StartsWith":
                    case "EndsWith":
                    case "SubstringAfter":
                    case "SubstringAfterLast":
                    case "SubstringBefore":
                    case "SubstringBeforeLast":
                        oLabel1.Text = "&Source Text:";
                        oLabel2.Text = "Search &Text:";
                        break;
                }   
           }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xName = "";
                string xParam = this.Controls["Parameter"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                switch (xField)
                {
                    case "Boilerplate":
                    case "FormatDateTime":
                    case "Count":
                    case "StartsWith":
                    case "EndsWith":
                    case "SubstringAfter":
                    case "SubstringAfterLast":
                    case "SubstringBefore":
                    case "SubstringBeforeLast":
                        m_xExpression = xField + "__"; 
                        break;
                    default:
                        m_xExpression = xField; 
                        break;
                }

                if (xField == "PageSetup" || xField == "Level")
                {
                    xName = oSpinnerIndex.Value;
                }
                else
                {
                    xName = oTxtBox.Text;
                }

                m_xExpression = m_xExpression + xName +
                    "__" + xParam;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class WordObjectSingleDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public WordObjectSingleDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {

                m_xFieldCodeBase = xFieldCodeBase;

                //label for parameter
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Parameter:", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for parameter
                LMP.Controls.TextBox oTxtParameter = new LMP.Controls.TextBox();
                SetUpTextBox(2, "Parameter", oTxtParameter);
                this.Controls.Add(oTxtParameter);

                //create label captions according to field code selected
                switch (xFieldCodeBase)
                {
                    case "Bookmark": //Bookmark name
                        oLabel1.Text = "&Bookmark Name:";
                        break;
                    case "DocumentVariable": //variable name
                        oLabel1.Text = "&Variable Name:";
                        break;
                    case "Document": //Property reference (e.g. ActiveWindow.Caption)
                        oLabel1.Text = "&Property Reference ";
                        Create2ndLabel("(e.g. ActiveWindow.Caption):");
                        oTxtParameter.SetBounds(8, 39, 174, 20);
                        break;
                    case "WordApplication":  //Property reference (e.g. ActiveDocument.Content.Text)
                        oLabel1.Text = "&Property reference (e.g. ";
                        Create2ndLabel("ActiveDocument.Content.Text):");
                        oTxtParameter.SetBounds(8, 39, 174, 20);
                        break;
                    case "WordBuiltInDocProperty": //property name
                    case "WordCustomDocProperty": //property name
                        oLabel1.Text = "&Property Name:";
                        break;
                    case "CM": //Related Value Index
                        oLabel1.Text = "&Related Value Index:";
                        break;
                    case "ExternalData":
                        oLabel1.Text = "&Record ID:";
                        break;
                    case "EXEC":
                        oLabel1.Text = "&Instructions (separate ";
                        Create2ndLabel("statements with a semicolon):");
                        oTxtParameter.SetBounds(8, 39, 174, 20);
                        break;
                    case "Char": //ASCII Number
                        oLabel1.Text = "&ASCII Number:";
                        break;
                    case "UChar": //Unicode number
                        oLabel1.Text = "&Unicode Number:";
                        break;
                    case "SubVar": //Sub Variable
                        oLabel1.Text = "&Sub Variable:";
                        break;
                    case "LowerCase":
                    case "UpperCase":
                        oLabel1.Text = "&Text:";
                        break;
                    case "SegmentName":
                        oLabel1.Text = "&Segment ID:";
                        break;
                    case "TagValue":
                        oLabel1.Text = "&Variable Name:";
                        break;
                    case "CountEntities":
                        oLabel1.Text = "&XML";
                        break;
                    //GLOG : 8375 : JSW
                    case "IsNumeric":
                        oLabel1.Text = "&Value";
                        oTxtParameter.Width = 134;
                        break;
                }
            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xParam = this.Controls["Parameter"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                if (xParam.Length > 0)
                {
                    if (xField == "EXEC")
                        //space instead of underscore
                        m_xExpression = xField + " " + xParam;
                    else
                        m_xExpression =  xField + "__" + xParam;
                }
                else
                {
                    m_xExpression = xField;
                }

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";

            }
            private void Create2ndLabel(string xCaption)
            {
                //label for parameter
                System.Windows.Forms.Label oLabel2 = new Label();
                oLabel2.SetBounds(8, 21, 250, 13);
                oLabel2.Text = xCaption;
                //SetUpLabel(1, xCaption, ref oLabel2);
                this.Controls.Add(oLabel2);
            }
        }
        private class XMLExtractPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner(); //GLOG 6508

            public XMLExtractPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {

                m_xFieldCodeBase = xFieldCodeBase;

                //label for parameter
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "Source &XML:", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for parameter
                LMP.Controls.MultilineTextBox oTxtParameter = new LMP.Controls.MultilineTextBox();
                SetUpTextBox(2, "Parameter", oTxtParameter);

                oTxtParameter.Text = "[MyValue]";
                this.Controls.Add(oTxtParameter);

                //label for name
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Tag Name:", oLabel2);
                this.Controls.Add(oLabel2);

                //textbox for name
                LMP.Controls.TextBox oTxtName = new LMP.Controls.TextBox();
                SetUpTextBox(4, "Tag Name", oTxtName);
                this.Controls.Add(oTxtName);

                //GLOG 6508
                //label for index
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(5, "&Item Number (Optional):", oLabel3);
                this.Controls.Add(oLabel3);

                //spinner control for index
                SetUpSpinner(6, "ItemIndex", oSpinner);
                oSpinner.Minimum = 0;
                oSpinner.Maximum = 9999;
                oSpinner.Value = "0";
                this.Controls.Add(oSpinner);
            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xParam = this.Controls["Parameter"].Text;
                string xTag = this.Controls["Tag Name"].Text;
                string xIndex = oSpinner.Value; //GLOG 6508
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                xField = xField + "__" + xParam;

                if (xTag.Length > 0)
                    xField = xField + "__" + xTag;

                //GLOG 6508: Append Index parameter if non-zero
                if (xIndex.Length > 0 && xIndex != "0")
                    xField = xField + "__" + xIndex;

                m_xExpression = xField;
                
                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";

            }
        }
        //GLOG 15838
        private class XMLFormatPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();

            public XMLFormatPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {

                m_xFieldCodeBase = xFieldCodeBase;
                //Need to adjust controls up to fit in space available
                int iOffSet = -10;

                //label for parameter
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "Source &XML:", oLabel1, iOffSet);
                this.Controls.Add(oLabel1);

                //textbox for parameter
                LMP.Controls.MultilineTextBox oTxtParameter = new LMP.Controls.MultilineTextBox();
                SetUpTextBox(2, "Parameter", oTxtParameter, iOffSet);

                oTxtParameter.Text = "[MyValue]";
                this.Controls.Add(oTxtParameter);

                iOffSet -= 5;
                //label for name
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Template (Tag names in <...>):", oLabel2, iOffSet);
                this.Controls.Add(oLabel2);

                //textbox for name
                LMP.Controls.TextBox oTemplate = new LMP.Controls.TextBox();
                oTemplate.Height = 2 * oTxtParameter.Height;
                SetUpTextBox(4, "Template", oTemplate, iOffSet);
                this.Controls.Add(oTemplate);

                iOffSet -= 5;
                //label for separator
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(5, "Item &Separator:", oLabel3, iOffSet);
                this.Controls.Add(oLabel3);

                //textbox for separator
                LMP.Controls.TextBox oSep = new LMP.Controls.TextBox();
                SetUpTextBox(6, "Separator", oSep, iOffSet);
                this.Controls.Add(oSep);

                iOffSet -= 5;
                //label for last separator
                System.Windows.Forms.Label oLabel4 = new Label();
                SetUpLabel(7, "&Last Item Separator (Optional):", oLabel4, iOffSet);
                this.Controls.Add(oLabel4);

                //textbox for separator
                LMP.Controls.TextBox oLastSep = new LMP.Controls.TextBox();
                SetUpTextBox(8, "LastSeparator", oLastSep, iOffSet);
                this.Controls.Add(oLastSep);

                iOffSet -= 5;
                //label for index
                System.Windows.Forms.Label oLabel5 = new Label();
                SetUpLabel(9, "&Item Number (Optional):", oLabel5, iOffSet);
                this.Controls.Add(oLabel5);

                //spinner control for index
                SetUpSpinner(10, "ItemIndex", oSpinner, iOffSet);
                oSpinner.Minimum = 0;
                oSpinner.Maximum = 9999;
                oSpinner.Value = "0";
                this.Controls.Add(oSpinner);
            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xParam = this.Controls["Parameter"].Text;
                string xTemplate = this.Controls["Template"].Text;
                string xSep = this.Controls["Separator"].Text;
                string xLastSep = this.Controls["LastSeparator"].Text;
                string xIndex = oSpinner.Value; //GLOG 6508
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                xField = xField + "__" + xParam;

                xField = xField + "__" + xTemplate;

                xField = xField + "__" + xSep;

                if (xLastSep != "")
                {
                    xField = xField + "__" + xLastSep;
                }

                //GLOG 6508: Append Index parameter if non-zero
                if (xIndex.Length > 0 && xIndex != "0")
                    xField = xField + "__" + xIndex;

                m_xExpression = xField;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";

            }
        }
        private class FunctionDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public FunctionDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for server
                System.Windows.Forms.Label oLabel1 = new Label();
                oLabel1.SetBounds(16, 5, 150, 13);
                oLabel1.Text = "&Server Name:";
                this.Controls.Add(oLabel1);

                //textbox for server
                LMP.Controls.TextBox oTxtServer = new LMP.Controls.TextBox();
                oTxtServer.SetBounds(16, 20, 150, 20);
                oTxtServer.Name = "Server";
                this.Controls.Add(oTxtServer);

                //label for class
                System.Windows.Forms.Label oLabel2 = new Label();
                oLabel2.SetBounds(16, 45, 150, 13);
                oLabel2.Text = "&Class Name:";
                this.Controls.Add(oLabel2);

                //textbox for class
                LMP.Controls.TextBox oTxtClass = new LMP.Controls.TextBox();
                oTxtClass.SetBounds(16, 60, 150, 20);
                oTxtClass.Name = "Class";
                this.Controls.Add(oTxtClass);

                //label for method
                System.Windows.Forms.Label oLabel3 = new Label();
                oLabel3.SetBounds(16, 85, 150, 13);
                oLabel3.Text = "&Method Name:";
                this.Controls.Add(oLabel3);

                //textbox for method
                LMP.Controls.TextBox oTxtMethod = new LMP.Controls.TextBox();
                oTxtMethod.SetBounds(16, 100, 150, 20);
                oTxtMethod.Name = "Method";
                this.Controls.Add(oTxtMethod);

                //label for parameters
                System.Windows.Forms.Label oLabel4 = new Label();
                oLabel4.SetBounds(16, 125, 150, 13);
                oLabel4.Text = "&Argument List:";
                this.Controls.Add(oLabel4);

                //textbox for parameters
                LMP.Controls.TextBox oTxtParameter = new LMP.Controls.TextBox();
                oTxtParameter.SetBounds(16, 140, 150, 20);
                oTxtParameter.Name = "Parameter";
                this.Controls.Add(oTxtParameter);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xServer = this.Controls["Server"].Text;
                string xClass = this.Controls["Class"].Text;
                string xMethod = this.Controls["Method"].Text;
                string xParameter = this.Controls["Parameter"].Text;
                string xField = m_xFieldCodeBase;

                //clear out previous expression
                m_xExpression = "";

                m_xExpression = xField + "__" + xServer +
                    "__" + xClass + "__" + xMethod + 
                    "__" + xParameter;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class ExternalConnectionDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public ExternalConnectionDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for server
                System.Windows.Forms.Label oLabel1 = new Label();
                oLabel1.SetBounds(16, 5, 150, 13);
                oLabel1.Text = "&External Connection Record:";
                this.Controls.Add(oLabel1);

                //textbox for server
                LMP.Controls.TextBox oTxtServer = new LMP.Controls.TextBox();
                oTxtServer.SetBounds(16, 20, 150, 20);
                oTxtServer.Name = "RecordName";
                this.Controls.Add(oTxtServer);

                //label for class
                System.Windows.Forms.Label oLabel2 = new Label();
                oLabel2.SetBounds(16, 45, 150, 13);
                oLabel2.Text = "&Query Name:";
                this.Controls.Add(oLabel2);

                //textbox for class
                LMP.Controls.TextBox oTxtClass = new LMP.Controls.TextBox();
                oTxtClass.SetBounds(16, 60, 150, 20);
                oTxtClass.Name = "QueryName";
                this.Controls.Add(oTxtClass);

                //label for method
                System.Windows.Forms.Label oLabel3 = new Label();
                oLabel3.SetBounds(16, 85, 150, 13);
                oLabel3.Text = "Query &Parameters:";
                this.Controls.Add(oLabel3);

                //textbox for method
                LMP.Controls.TextBox oTxtMethod = new LMP.Controls.TextBox();
                oTxtMethod.SetBounds(16, 100, 150, 20);
                oTxtMethod.Name = "QueryParameters";
                this.Controls.Add(oTxtMethod);

                //label for parameters
                System.Windows.Forms.Label oLabel4 = new Label();
                oLabel4.SetBounds(16, 125, 150, 13);
                oLabel4.Text = "&Return Value Column Name:";
                this.Controls.Add(oLabel4);

                //textbox for parameters
                LMP.Controls.TextBox oTxtParameter = new LMP.Controls.TextBox();
                oTxtParameter.SetBounds(16, 140, 150, 20);
                oTxtParameter.Name = "ReturnValueColumnName";
                this.Controls.Add(oTxtParameter);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xRecordName = this.Controls["RecordName"].Text;
                string xQueryName = this.Controls["QueryName"].Text;
                string xQueryParameters = this.Controls["QueryParameters"].Text;
                string xReturnValueColumnName = this.Controls["ReturnValueColumnName"].Text;
                string xField = m_xFieldCodeBase;

                //clear out previous expression
                m_xExpression = "";

                m_xExpression = xField + xRecordName + "__" + xQueryName +
                    "__" + xQueryParameters + "__" + xReturnValueColumnName;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class ReplaceDetailPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public ReplaceDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for server
                System.Windows.Forms.Label oLabel1 = new Label();
                oLabel1.SetBounds(16, 5, 150, 13);
                oLabel1.Text = "&Source Text:";
                this.Controls.Add(oLabel1);

                //textbox for server
                LMP.Controls.TextBox oTxtServer = new LMP.Controls.TextBox();
                oTxtServer.SetBounds(16, 20, 150, 20);
                oTxtServer.Name = "SourceText";
                this.Controls.Add(oTxtServer);

                //label for class
                System.Windows.Forms.Label oLabel2 = new Label();
                oLabel2.SetBounds(16, 45, 150, 13);
                oLabel2.Text = "&Old Text:";
                this.Controls.Add(oLabel2);

                //textbox for class
                LMP.Controls.TextBox oTxtClass = new LMP.Controls.TextBox();
                oTxtClass.SetBounds(16, 60, 150, 20);
                oTxtClass.Name = "OldText";
                this.Controls.Add(oTxtClass);

                //label for method
                System.Windows.Forms.Label oLabel3 = new Label();
                oLabel3.SetBounds(16, 85, 150, 13);
                oLabel3.Text = "&New Text:";
                this.Controls.Add(oLabel3);

                //textbox for method
                LMP.Controls.TextBox oTxtMethod = new LMP.Controls.TextBox();
                oTxtMethod.SetBounds(16, 100, 150, 20);
                oTxtMethod.Name = "NewText";
                this.Controls.Add(oTxtMethod);

                //GLOG 3561: Allow replacing last instance of old text with different value
                System.Windows.Forms.Label oLabel4 = new Label();
                oLabel4.SetBounds(16, 125, 150, 28);
                oLabel4.Text = "Different Text for &Last Match\r\n(Optional):";
                this.Controls.Add(oLabel4);

                //textbox for method
                LMP.Controls.TextBox oTxtOptional = new LMP.Controls.TextBox();
                oTxtOptional.SetBounds(16, 155, 150, 20);
                oTxtOptional.Name = "LastNewText";
                this.Controls.Add(oTxtOptional);
            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xSourceText = this.Controls["SourceText"].Text;
                string xOldText = this.Controls["OldText"].Text;
                string xNewText = this.Controls["NewText"].Text;
                string xLastNewText = this.Controls["LastNewText"].Text;
                string xField = m_xFieldCodeBase;

                //clear out previous expression
                m_xExpression = "";

                //GLOG 3561: Optional fourth parameter used
                if (xLastNewText != "")
                    m_xExpression = xField + "__" + xSourceText + "__" + xOldText +
                        "__" + xNewText + "__" + xLastNewText;
                else
                    m_xExpression = xField + "__" + xSourceText + "__" + xOldText +
                        "__" + xNewText;
                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class MathPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public MathPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                string xLabel1 = "";
                string xLabel2 = "";
                switch (xFieldCodeBase)
                {
                    case "Add":
                        xLabel1 = "Value 1:";
                        xLabel2 = "Plus Value 2:";
                        break;
                    case "Subtract":
                        xLabel1 = "Value 1:";
                        xLabel2 = "Minus Value 2:";
                        break;
                    case "Divide":
                        xLabel1 = "Value 1:";
                        xLabel2 = "Divided By Value 2:";
                        break;
                    case "IsNumeric": //GLOG : 8375 : JSW
                        xLabel1 = "Value:";
                        break;
                    case "Multiply":
                        xLabel1 = "Value 1:";
                        xLabel2 = "Times Value 2:";
                        break;
                    case "Modulus": //GLOG 4452
                        xLabel1 = "Value 1:";
                        xLabel2 = "Modulus Value 2:";
                        break;
                }

                //label for first Number
                System.Windows.Forms.Label oLabel1 = new Label();
                oLabel1.SetBounds(16, 6, 134, 13);
                oLabel1.Text = xLabel1;
                this.Controls.Add(oLabel1);

                //textbox for first Number
                LMP.Controls.TextBox oTxtServer = new LMP.Controls.TextBox();
                oTxtServer.SetBounds(16, 22, 134, 20);
                oTxtServer.Name = "Number1";
                this.Controls.Add(oTxtServer);

                //label for second Number
                System.Windows.Forms.Label oLabel2 = new Label();
                oLabel2.SetBounds(16, 54, 134, 13);
                oLabel2.Text = xLabel2;
                this.Controls.Add(oLabel2);

                //textbox for second Number
                LMP.Controls.TextBox oTxtClass = new LMP.Controls.TextBox();
                oTxtClass.SetBounds(16, 70, 134, 20);
                oTxtClass.Name = "Number2";
                this.Controls.Add(oTxtClass);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xNumber1 = this.Controls["Number1"].Text;
                string xNumber2 = this.Controls["Number2"].Text;
                string xField = m_xFieldCodeBase;

                //clear out previous expression
                m_xExpression = "";

                m_xExpression = xField + "__" + xNumber1 + "__" + xNumber2;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class TruncatePanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
            LMP.Controls.TextBox oTxt = new LMP.Controls.TextBox();

            public TruncatePanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for Reference Qualifier
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Source:", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for name
                SetUpTextBox(2, "Source", oTxt);
                this.Controls.Add(oTxt);

                //label for index
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Length:", oLabel2);
                this.Controls.Add(oLabel2);

                //spinner control for index
                SetUpSpinner(4, "Length", oSpinner);
                oSpinner.Maximum = 999999999;
                this.Controls.Add(oSpinner);

            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xLength = oSpinner.Value;
                string xSource = oTxt.Value;
                string xField = "";
                string xParam = "";

                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xSource + "__" + xLength;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class SubstringPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
            LMP.Controls.TextBox oTxt = new LMP.Controls.TextBox();

            public SubstringPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for Reference Qualifier
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Source:", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for name
                SetUpTextBox(2, "Source", oTxt);
                this.Controls.Add(oTxt);

                //label for index
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "&Length:", oLabel2);
                this.Controls.Add(oLabel2);

                //spinner control for index
                SetUpSpinner(4, "Length", oSpinner);
                oSpinner.Maximum = 999999999;
                this.Controls.Add(oSpinner);

            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xLength = oSpinner.Value;
                string xSource = oTxt.Value;
                string xField = "";
                string xParam = "";

                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xSource + "__" + xLength;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class SegmentLevelPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();

            public SegmentLevelPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for index
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Level:", oLabel1);
                this.Controls.Add(oLabel1);

                //spinner control for index
                SetUpSpinner(2, "Level", oSpinner);
                oSpinner.Minimum = 0;
                this.Controls.Add(oSpinner);

                if (xFieldCodeBase == "NumberText")
                {
                    oLabel1.Text = "&Number";
                    oSpinner.Maximum = 999999;
                }
                else
                {
                    oLabel1.Text = "Level";
                    oSpinner.Maximum = 4;
                }
            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xLevel = oSpinner.Value;
                string xField = "";
                string xParam = "";

                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xLevel;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class CourtPropertyPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;

            public CourtPropertyPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;
                if (m_xFieldCodeBase == "CourtProperty")
                    m_xFieldCodeBase = "Court";

                //display ref qualifier controls
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "&Court Property:", oLabel1);
                this.Controls.Add(oLabel1);

                //combo box for Reference Qualifier
                LMP.Controls.ComboBox oCmbRefQualifier = new LMP.Controls.ComboBox();
                SetUpCombo(2, "CourtProperty", oCmbRefQualifier);
                this.Controls.Add(oCmbRefQualifier);
            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xCourtProperty = "";
                string xField = "";
                string xParam = "";

                //clear out previous Expression string
                m_xExpression = "";

                LMP.Controls.ComboBox oCourtProperty = (LMP.Controls.ComboBox)this.Controls["CourtProperty"];

                if (oCourtProperty != null)
                {
                    xCourtProperty = oCourtProperty.Value;
                }
                //clear out previous expression
                m_xExpression = "";

                //parse out info for building the expression
                int iPos = m_xFieldCodeBase.IndexOf("__");
                if (iPos != -1)
                {
                    xField = m_xFieldCodeBase.Substring(0, iPos);
                    xParam = m_xFieldCodeBase.Substring(iPos + 2);
                }
                else
                    xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xCourtProperty;

                if (m_xExpression.Length > 0)
                    m_xExpression = "[" + m_xExpression + "]";

            }
        }
        private class ConvertPropertyPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public ConvertPropertyPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label 
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(1, "&Value:", oLabel3);
                this.Controls.Add(oLabel3);

                //textbox for value
                LMP.Controls.TextBox oTxtValue = new LMP.Controls.TextBox();
                SetUpTextBox(2, "Value", oTxtValue);
                this.Controls.Add(oTxtValue);
                
                //label for name
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(3, "&From:", oLabel1);
                this.Controls.Add(oLabel1);

                //combo box for measurement unit
                LMP.Controls.ComboBox oComboMeasurement1 = new LMP.Controls.ComboBox();
                SetUpCombo(4, "MeasurementUnit", oComboMeasurement1);
                oComboMeasurement1.Name = "Source";
                oComboMeasurement1.SelectedIndex = 1;
                this.Controls.Add(oComboMeasurement1);

                //label for name
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(5, "&To:", oLabel2);
                this.Controls.Add(oLabel2);

                //combo box for measurement unit
                LMP.Controls.ComboBox oComboMeasurement2 = new LMP.Controls.ComboBox();
                SetUpCombo(6, "MeasurementUnit", oComboMeasurement2);
                oComboMeasurement2.Name = "Target";
                oComboMeasurement2.SelectedIndex = 4;
                this.Controls.Add(oComboMeasurement2);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xSource = this.Controls["Source"].Text;
                string xTarget = this.Controls["Target"].Text;
                string xValue = this.Controls["Value"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                m_xExpression = xField + xSource +
                    "__" + xTarget + "__" + xValue;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class ListLookupPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();
            private LMP.Controls.Spinner oSpinnerIndex2 = new LMP.Controls.Spinner();

            public ListLookupPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for list name
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(1, "List Name", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for list name
                LMP.Controls.TextBox oListName = new LMP.Controls.TextBox();
                this.SetUpTextBox(2, "ListName", oListName);
                this.Controls.Add(oListName);

                //label for value
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(3, "Search Value", oLabel2);
                this.Controls.Add(oLabel2);

                //textbox for value
                LMP.Controls.TextBox oTxtValue = new LMP.Controls.TextBox();
                SetUpTextBox(4, "Value", oTxtValue);
                this.Controls.Add(oTxtValue);

                //label for search col 1
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(5, "Search Column 1", oLabel3);
                this.Controls.Add(oLabel3);

                //spinner for search col 1
                SetUpSpinner(6, "Index", oSpinnerIndex);
                oSpinnerIndex.Maximum = 2;
                this.Controls.Add(oSpinnerIndex);

                //label for search col 2
                System.Windows.Forms.Label oLabel4 = new Label();
                SetUpLabel(7, "Search Column 2", oLabel4);
                this.Controls.Add(oLabel4);

                //spinner for search col 2
                SetUpSpinner(8, "Index1", oSpinnerIndex2);
                oSpinnerIndex2.Maximum = 2;
                this.Controls.Add(oSpinnerIndex2);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xListName = this.Controls["ListName"].Text;
                string xValue = this.Controls["Value"].Text;
                string xIndex = oSpinnerIndex.Value;
                string xIndex1 = oSpinnerIndex2.Value;

                //clear out previous expression

                string xField = m_xFieldCodeBase;
                    
                m_xExpression = "";

                m_xExpression = xField + xListName + "__" + xValue +
                    "__" + xIndex + "__" + xIndex1;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class ChooseDetailPanel : ParameterPanel
        {


            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            string xField = "";
            DataGridView grdValues = new DataGridView();

            public ChooseDetailPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for match expression
                System.Windows.Forms.Label oLabel1 = new Label();
                oLabel1.SetBounds(15, -2, 150, 13);
                oLabel1.Text = "&Match Expression:";
                this.Controls.Add(oLabel1);

                //textbox for match expression
                LMP.Controls.TextBox oTxt1 = new LMP.Controls.TextBox();
                oTxt1.SetBounds(15, 13, 175, 20);
                oTxt1.Name = "MatchEx";
                this.Controls.Add(oTxt1);

                //2 column grid for Match Value and return value
                grdValues.Left = 15;
                grdValues.Top = 43;
                grdValues.Width = 175;
                grdValues.Height = 90;
                DataGridViewTextBoxColumn oMatchValCol = new DataGridViewTextBoxColumn();
                DataGridViewTextBoxColumn oReturnValue = new DataGridViewTextBoxColumn();
                oMatchValCol.Name = "Match Value";
                oReturnValue.Name = "Return Value";
                grdValues.Columns.Add(oMatchValCol);
                grdValues.Columns.Add(oReturnValue);
                grdValues.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdValues.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdValues.Columns[0].Width = 80;
                grdValues.Columns[1].Width = 80;
                grdValues.Name = "Values";
                grdValues.BorderStyle = BorderStyle.None;
                grdValues.RowHeadersWidth = 10;
                grdValues.BackgroundColor = System.Drawing.Color.White;
                grdValues.ColumnHeadersHeight = 20;
                grdValues.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                grdValues.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                this.Controls.Add(grdValues);

                grdValues.RowsAdded += new DataGridViewRowsAddedEventHandler(grdValues_RowsAdded);
                grdValues.RowsRemoved += new DataGridViewRowsRemovedEventHandler(grdValues_RowsRemoved);

                //label for default value
                System.Windows.Forms.Label oLabel8 = new Label();
                oLabel8.SetBounds(15, 145, 170, 13);
                oLabel8.Text = "Default Value:";
                this.Controls.Add(oLabel8);

                //textbox for default value
                LMP.Controls.TextBox oTxt8 = new LMP.Controls.TextBox();
                oTxt8.SetBounds(15, 159, 175, 20);
                oTxt8.Name = "DefaultVal";
                this.Controls.Add(oTxt8);
            }

            void grdValues_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
            {
                if (grdValues.Rows.Count < 4)
                {
                    //no vertical scroll present, increase column width
                    grdValues.Columns[0].Width = 81;
                    grdValues.Columns[1].Width = 81;
                }
            }

            void grdValues_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
            {
                if (grdValues.Rows.Count > 3)
                {
                    //vertical scroll present, decrease column width
                    grdValues.Columns[0].Width = 72;
                    grdValues.Columns[1].Width = 72;
                }
            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;
                }
            }
            private void BuildExpression()
            {
                string xMatchExpression = this.Controls["MatchEx"].Text;
                string xDefaultVal = this.Controls["DefaultVal"].Text;
                LMP.Controls.DataGridView oGrd = (DataGridView)this.Controls["Values"];

                int iRowCount = oGrd.Rows.Count;
                string[] xMatchVals = new string[iRowCount];
                string[] xReturnVals = new string[iRowCount];

                xField = m_xFieldCodeBase;

                //clear out previous expression
                m_xExpression = "";

                m_xExpression = xField + "__" + xMatchExpression;             
                for (int i = 0; i < (oGrd.RowCount - 1); i++)
                {
                    xMatchVals[i] = oGrd.Rows[i].Cells[0].FormattedValue.ToString();
                    xReturnVals[i] = oGrd.Rows[i].Cells[1].FormattedValue.ToString();
                    m_xExpression = m_xExpression + "__" + xMatchVals[i] + "__" + xReturnVals[i];
                }

                m_xExpression = m_xExpression + "__" + xDefaultVal;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class LocationPropertyPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public LocationPropertyPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label 
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(1, "&Type:", oLabel3);
                this.Controls.Add(oLabel3);

                //combo box for measurement unit
                LMP.Controls.ComboBox oType = new LMP.Controls.ComboBox();
                SetUpCombo(2, "Location", oType);
                oType.Name = "Type";
                oType.SelectedIndex = 0;
                this.Controls.Add(oType);

                //label 
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(3, "&Parent ID:", oLabel1);
                this.Controls.Add(oLabel1);

                //textbox for value
                LMP.Controls.TextBox oTxtParentID = new LMP.Controls.TextBox();
                SetUpTextBox(4, "ParentID", oTxtParentID);
                this.Controls.Add(oTxtParentID);

                //label
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(5, "&Location Name:", oLabel2);
                this.Controls.Add(oLabel2);

                //textbox for value
                LMP.Controls.TextBox oTxtLocation = new LMP.Controls.TextBox();
                SetUpTextBox(6, "Location", oTxtLocation);
                this.Controls.Add(oTxtLocation);

            }
            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xType = "";
                string xParentID = this.Controls["ParentID"].Text;
                string xLocation = this.Controls["Location"].Text;
                string xField = "";

                LMP.Controls.ComboBox oTypeProperty = (LMP.Controls.ComboBox)this.Controls["Type"];

                if (oTypeProperty != null)
                {
                    xType = oTypeProperty.SelectedValue.ToString();
                }
                
                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xType +
                    "__" + xParentID + "__" + xLocation;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        //GLOG : 8374 : JSW
        //panels for new date calculations
        private class DateDiffPropertyPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            //private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public DateDiffPropertyPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for date picker 1
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(1, "&Date 1:", oLabel3);
                this.Controls.Add(oLabel3);

                //date picker for date 1
                //LMP.Data.mpControlTypes.Calendar oDPValue1 = new LMP.Data.mpControlTypes.Calendar();
                //LMP.Controls.DatePicker oDPValue1 = new LMP.Controls.DatePicker();
                //System.Windows.Forms.MonthCalendar oDPValue1 = new System.Windows.Forms.MonthCalendar();
                //SetUpCalendar(2, "Value", oDPValue1);
                DateTimePicker oDPValue1 = new DateTimePicker();       
                oDPValue1.Format = DateTimePickerFormat.Short;
                //oDPValue1.CustomFormat = "MMMM dd, yyyy";
                SetUpDatePicker(2, "Date1", oDPValue1);
                this.Controls.Add(oDPValue1);

                //label for date picker 2
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(3, "D&ate 2:", oLabel1);
                this.Controls.Add(oLabel1);

                //date picker for date 2
                DateTimePicker oDPValue2 = new DateTimePicker();
                oDPValue2.Format = DateTimePickerFormat.Short;
                //oDPValue2.CustomFormat = "MMMM dd, yyyy";
                SetUpDatePicker(4, "Date2", oDPValue2);
                this.Controls.Add(oDPValue2);

                //label for units to return
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(5, "&Date unit to return:", oLabel2);
                this.Controls.Add(oLabel2);

                //combo box for measurement unit
                LMP.Controls.ComboBox oComboDateUnit = new LMP.Controls.ComboBox();
                SetUpCombo(6, "DateUnit", oComboDateUnit);
                oComboDateUnit.Width = 134;
                this.Controls.Add(oComboDateUnit);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xDate1 = this.Controls["Date1"].Text;
                string xDate2 = this.Controls["Date2"].Text;
                string xDateUnit = this.Controls["DateUnit"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xDate1 +
                    "__" + xDate2 + "__" + xDateUnit;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class DateAddPropertyPanel : ParameterPanel
        {
            private string m_xExpression = "";
            private string m_xFieldCodeBase;
            private LMP.Controls.Spinner oSpinnerIndex = new LMP.Controls.Spinner();

            public DateAddPropertyPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                m_xFieldCodeBase = xFieldCodeBase;

                //label for date picker 1
                System.Windows.Forms.Label oLabel3 = new Label();
                SetUpLabel(1, "&Start Date:", oLabel3);
                this.Controls.Add(oLabel3);

                //date picker for date 1
                //LMP.Data.mpControlTypes.Calendar oDPValue1 = new LMP.Data.mpControlTypes.Calendar();
                DateTimePicker oDPValue1 = new DateTimePicker();
                oDPValue1.Format = DateTimePickerFormat.Short;
                //oDPValue1.CustomFormat = "MM dd, yyyy";
                SetUpDatePicker(2, "StartDate", oDPValue1);
                this.Controls.Add(oDPValue1);

                //label for name
                System.Windows.Forms.Label oLabel1 = new Label();
                SetUpLabel(3, "Date &Unit:", oLabel1);
                this.Controls.Add(oLabel1);

                //combo box for measurement unit
                LMP.Controls.ComboBox oComboDateUnit = new LMP.Controls.ComboBox();
                SetUpCombo(4, "DateUnit", oComboDateUnit);
                //oComboDateUnit.Name = "DateUnit";
                //oComboDateUnit.SelectedIndex = 1;
                oComboDateUnit.Width = 134;
                this.Controls.Add(oComboDateUnit);

                //label for name
                System.Windows.Forms.Label oLabel2 = new Label();
                SetUpLabel(5, "&Number:", oLabel2);
                this.Controls.Add(oLabel2);

                //text box for number 
                //textbox for list name
                LMP.Controls.TextBox oListName = new LMP.Controls.TextBox();
                this.SetUpTextBox(6, "NumValue", oListName);
                oListName.Width = 134;
                this.Controls.Add(oListName);

            }

            public override string Value
            {
                get
                {
                    BuildExpression();
                    return m_xExpression;

                }
            }
            private void BuildExpression()
            {
                string xStartDate = this.Controls["StartDate"].Text;
                string xDateUnit = this.Controls["DateUnit"].Text;
                string xIndex = this.Controls["NumValue"].Text;
                string xField = "";

                //clear out previous expression
                m_xExpression = "";

                xField = m_xFieldCodeBase;

                m_xExpression = xField + "__" + xStartDate +
                    "__" + xDateUnit + "__" + xIndex;

                m_xExpression = "[" + m_xExpression + "]";
            }
        }
        private class BlankPanel : ParameterPanel
        {
            private string m_xExpression;

            public BlankPanel(string xFieldCodeBase)
                : base(xFieldCodeBase)
            {
                if (xFieldCodeBase.Length > 0)
                    m_xExpression = "[" + xFieldCodeBase + "]";
                else
                    m_xExpression = xFieldCodeBase;
            }

            public override string Value
            {
                get
                {
                    return m_xExpression;
                }
            }
        }
        abstract private class ParameterPanel : Panel
        {
            protected string FieldCodeBase;
            protected static string[,] ReferenceQualifier;
            protected static string[,] CourtProperty;
            protected static string [,] MeasurementUnit;
            protected static string[,] Location;
            protected static string[,] DateUnit;
            const int FIRST_POS = 6;
		    const int SECOND_POS = 22; 
            const int THIRD_POS = 54;
            const int FOURTH_POS = 70;
            const int FIFTH_POS = 102;
            const int SIXTH_POS = 118;
            const int SEVENTH_POS = 150;
            const int EIGHTH_POS = 166;
            //GLOG 15838
            const int NINTH_POS = 198;
            const int TENTH_POS = 214;

            public ParameterPanel(string xFieldCodeBase)
            {
               //this.AutoScroll = true;
                
                FieldCodeBase = xFieldCodeBase;

                if (ReferenceQualifier == null)
                {
                    ReferenceQualifier = new string[,] {{ "This Segment", "None" }, 
                                                        { "Host Segment", "Host" }, 
                                                        { "Parent Segment", "Parent" }, 
                                                        { "Top Parent", "TopParent" }, 
                                                        { "Child", "Child" }, 
                                                        { "Children", "Children" }, 
                                                        { "Sibling", "Sibling" }, 
                                                        { "Siblings", "Siblings" }, 
                                                        { "Segment", "Segment" }, 
                                                        { "Segments", "Segments" }, 
                                                        { "Letter Child", "Segment1" },
                                                        { "Memo Child", "Segment2" },
                                                        { "Fax Child", "Segment3" },
                                                        { "Pleading Child", "Segment5" },
                                                        { "Service Child", "Segment6" },
                                                        { "Notary Child", "Segment7" },
                                                        { "Verification Child", "Segment8" },
                                                        { "Depo Summary Child", "Segment9" },
                                                        { "Bates Labels Child", "Segment10" },
                                                        { "Agreement Child", "Segment11" },
                                                        { "Agreement Title Page Child", "Segment515" },
                                                        { "Document Stamp Child", "Segment401" },
                                                        { "Envelope Child ", "Segment402" },
                                                        { "Label Child", "Segment403" },
                                                        { "Letterhead Child", "Segment404" },
                                                        { "Pleading Caption Child", "Segment500" },
                                                        { "Pleading Counsel Child", "Segment501" },
                                                        { "Pleading Signature Child", "Segment502" },
                                                        { "Pleading Paper Child", "Segment503" },
                                                        { "Pleading Cover Page Child", "Segment505" },
                                                        { "Agreement Signatures Child", "Segment528" },
                                                        { "Letter Signatures Collection", "Segment527" },
                                                        { "Pleading Captions Child", "Segment506" },
                                                        { "Pleading Counsels Child", "Segment507" },
                                                        { "Pleading Signatures Collection", "Segment508" },
                                                        { "Service List Child", "Segment512" },  //GLOG 6094
                                                        { "Service List Separate Page Child", "Segment642" }, 
                                                        { "Disclaimer Sidebar Child", "Segment646" }, //GLOG 15915
                                                        { "Disclaimer Text Child", "Segment647" }, 
                                                        { "Logo Sidebar Child", "Segment648" } };
                }
                if (CourtProperty == null)
                {
                    CourtProperty = new string[,] {{ "Address ID", "AddressID" }, 
                                                        { "Judge Name", "JudgeName" }, 
                                                        { "Level 0", "L0" }, 
                                                        { "Level 1", "L1" }, 
                                                        { "Level 2", "L2" }, 
                                                        { "Level 3", "L3" }, 
                                                        { "Level 4", "L4" }};
                }
                if (MeasurementUnit == null)
                {
                    MeasurementUnit = new string[,] {{ "Centimeters", "Centimeters" }, 
                                                        { "Inches", "Inches" }, 
                                                        { "Millimeters", "Millimeters" }, 
                                                        { "Picas", "Picas" }, 
                                                        { "Points", "Points" }};
                }
                if (Location == null)
                {
                    Location = new string[,] {{ "Country", "1" }, 
                                                        { "State", "2" }, 
                                                        { "County", "3" }};
                }
                if (DateUnit == null)
                {
                    DateUnit = new string[,] {{ "Days", "Days" }, 
                                                        { "Months", "Months" }, 
                                                        { "Years", "Years" }};
                }

            }

            protected void SetUpLabel(int iPosition, string xCaption, System.Windows.Forms.Label oLabel)
            {
                SetUpLabel(iPosition, xCaption, oLabel, 0);
            }
            //GLOG : 8546 : ceh
            //creates label
            protected void SetUpLabel(int iPosition, string xCaption, System.Windows.Forms.Label oLabel, int iOffset)
            {
                //setting for vertical position 
                int iVPos = FIRST_POS;
                //GLOG : 8546 : ceh
                int iWidth = 174;
                int iHeight = 13;       //GLOG : 8546 : ceh

                oLabel.Text = xCaption;
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS + iOffset;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                    case 7:
                        iVPos = SEVENTH_POS;
                        break;
                    case 8:
                        iVPos = EIGHTH_POS;
                        break;
                    case 9:
                        iVPos = NINTH_POS;
                        break;
                }
                iVPos += iOffset;
                if (xCaption == "&Address Formats:")
                {
                    iWidth = 140;
                }

                oLabel.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                    (int)(iWidth * LMP.OS.GetScalingFactor()),
                                    (int)(iHeight * LMP.OS.GetScalingFactor()));

            }
            protected void SetUpCombo(int iPosition, string xName, LMP.Controls.ComboBox oCombo)
            {
                //setting for vertical position 
                int iVPos = FIRST_POS;
                int iWidth = 174;
                int iHeight = 21;     //GLOG : 8546 : ceh

                oCombo.Name = xName;
                oCombo.LimitToList = false;
                switch (xName)
                {
                    case "RefQualifier":
                        oCombo.SetList(ReferenceQualifier);
                        oCombo.LimitToList = true;
                        iWidth = 174;
                        break;
                    case "CourtProperty":
                        oCombo.SetList(CourtProperty);
                        oCombo.LimitToList = true;
                        iWidth = 174;
                        break;
                    case "AddressFormat":
                        LMP.Data.AddressFormats oAFs = new LMP.Data.AddressFormats();
                        oCombo.SetList(oAFs.ToArrayList());
                        iWidth = 144;
                        break;
                    case "Office":
                        LMP.Data.Offices oOffices = new LMP.Data.Offices();
                        int[] aNameAndIDIndexes = new int[2];
                        aNameAndIDIndexes[0] = 0;
                        aNameAndIDIndexes[1] = 4;

                        //Get an Array of the names and IDs of all Offices
                        ArrayList oAllOfficeNamesAndIDsArray = oOffices.ToArrayList(aNameAndIDIndexes);

                        //bind the cbox
                        oCombo.SetList(oAllOfficeNamesAndIDsArray);
                        oCombo.LimitToList = true;
                        iWidth = 174;
                        break;
                    case "MeasurementUnit":
                        oCombo.SetList(MeasurementUnit);
                        iWidth = 174;
                        break;
                    case "Location":
                        oCombo.SetList(Location);
                        iWidth = 174;
                        break;
                    //GLOG : 8374 : JSW
                    case "DateUnit":
                        oCombo.SetList(DateUnit);
                        iWidth = 174;
                        break;
                }

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                }

                oCombo.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()), 
                                    (int)(iWidth * LMP.OS.GetScalingFactor()), 
                                    (int)(iHeight * LMP.OS.GetScalingFactor()));
            }
            protected void SetUpDatePicker(int iPosition, string xName, DateTimePicker oDP)
            {
                //setting for vertical position 
                int iVPos = FIRST_POS;
                int iWidth = 134;
                int iHeight = 21;     //GLOG : 8546 : ceh

                oDP.Name = xName;
                //oDP.LimitToList = false;

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                }

                oDP.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                 (int)(iWidth * LMP.OS.GetScalingFactor()),
                                 (int)(iHeight * LMP.OS.GetScalingFactor()));
            }
            protected void SetUpCalendar(int iPosition, string xName, System.Windows.Forms.MonthCalendar oDP)
            {
                //setting for vertical position 
                int iVPos = FIRST_POS;
                int iWidth = 174;
                int iHeight = 21;       //GLOG : 8546 : ceh

                oDP.Name = xName;
                //oDP.LimitToList = false;

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                }

                oDP.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                 (int)(iWidth * LMP.OS.GetScalingFactor()),
                                 (int)(iHeight * LMP.OS.GetScalingFactor()));
            }
            protected void SetUpSpinner(int iPosition, string xName, LMP.Controls.Spinner oSpinner)
            {
                SetUpSpinner(iPosition, xName, oSpinner, 0);
            }
            protected void SetUpSpinner(int iPosition, string xName, LMP.Controls.Spinner oSpinner, int iOffSet)
            {
                int iVPos = FIRST_POS;
                //GLOG : 8546 : ceh
                int iWidth = 174;
                int iHeight = 23;

                oSpinner.Name = xName;
                oSpinner.Minimum = 1;

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                    case 7:
                        iVPos = SEVENTH_POS;
                        break;
                    case 8:
                        iVPos = EIGHTH_POS;
                        break;
                    case 9:
                        iVPos = NINTH_POS;
                        break;
                    case 10:
                        iVPos = TENTH_POS;
                        break;
                }
                iVPos += iOffSet;
                oSpinner.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                      (int)(iWidth * LMP.OS.GetScalingFactor()),
                                      (int)(iHeight * LMP.OS.GetScalingFactor()));
            }

            protected void SetUpButton(int iPosition, string xCaption, System.Windows.Forms.Button oButton)
            {
                int iVPos = FIRST_POS;
                int iHPos = 155;
                //GLOG : 8546 : ceh
                int iWidth = 27;    
                int iHeight = 21;

                oButton.Text = xCaption;
                oButton.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, 
                    GraphicsUnit.Point, ((byte)(0)));

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                }

                oButton.SetBounds(iHPos, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                         (int)(iWidth * LMP.OS.GetScalingFactor()),
                                         (int)(iHeight * LMP.OS.GetScalingFactor()));

                oButton.Click += new System.EventHandler(oButton_Click);
            }

            protected void oButton_Click(object sender, System.EventArgs e)
            {
                string xMessage;
                try
                {
                    xMessage = "This feature is under construction.";
                    MessageBox.Show(xMessage, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    //Ask Dan
                    //this.DialogResult = DialogResult.None;

                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
            protected void SetUpTextBox(int iPosition, string xName, LMP.Controls.TextBox oTextBox)
            {
                SetUpTextBox(iPosition, xName, oTextBox, 0);
            }
            
            protected void SetUpTextBox(int iPosition, string xName, LMP.Controls.TextBox oTextBox, int iOffSet)
            {
                int iVPos = FIRST_POS;
                //GLOG : 8546 : ceh
                int iWidth = 174; 
                int iHeight = 20;

                oTextBox.Name = xName;

                //GLOG : 8546 : ceh
                switch (iPosition)
                {
                    case 1:
                        iVPos = FIRST_POS;
                        break;
                    case 2:
                        iVPos = SECOND_POS;
                        break;
                    case 3:
                        iVPos = THIRD_POS;
                        break;
                    case 4:
                        iVPos = FOURTH_POS;
                        break;
                    case 5:
                        iVPos = FIFTH_POS;
                        break;
                    case 6:
                        iVPos = SIXTH_POS;
                        break;
                    case 7:
                        iVPos = SEVENTH_POS;
                        break;
                    case 8:
                        iVPos = EIGHTH_POS;
                        break;
                    case 9:
                        iVPos = NINTH_POS;
                        break;
                    case 10:
                        iVPos = TENTH_POS;
                        break;
                }
                iVPos += iOffSet;
                oTextBox.SetBounds(8, (int)(iVPos * LMP.OS.GetScalingFactor()),
                                      (int)(iWidth * LMP.OS.GetScalingFactor()),
                                      (int)(iHeight * LMP.OS.GetScalingFactor()));
                oTextBox.Name = xName;
           
            }
            
            abstract public string Value { get; }

        }
        #endregion *********************helper classes*********************
    }
}
