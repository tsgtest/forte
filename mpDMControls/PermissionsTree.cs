using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using LMP.Data;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Win.UltraWinTree;

namespace LMP.Controls
{
    public partial class PermissionsTree : Infragistics.Win.UltraWinTree.UltraTree, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private string m_xValue = "";
        private int m_iObjectID = 0;
        private bool m_bInitialized = false;
        private mpObjectTypes m_iObjectTypeID = 0;
        private string m_xSupportingValues = "";
        private int m_iParentFolderID = 0;
        private bool m_bFilterSet = false;
        private ArrayList m_oPermissions = null;
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
            PopulateTree();
            this.IsDirty = false;
            m_bInitialized = true;
        }
        public string Value
        {
            get { return m_xValue; }
            set
            {
                m_xValue = value;
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************constructors*********************
        public PermissionsTree(int iParentFolderID)
        {
            InitializeComponent();
            m_iParentFolderID = iParentFolderID;

            //get permissions of parent folder
            Folders oFolders = new Folders();
            Folder oFolder = null;
            try
            {
                oFolder = oFolders.ItemFromID(iParentFolderID) as Folder;
            }
            catch { }

            if (oFolder != null)
            {
                m_oPermissions = oFolder.Permissions();
            }

            this.AfterCheck += new AfterNodeChangedEventHandler(PermissionsTree_AfterCheck);
        }
        #endregion
        #region *********************private methods*********************
        /// <summary>
        /// Populates tree with list of Offices and Groups
        /// </summary>
        private void PopulateTree()
        {

            this.Nodes.Clear();
            this.Override.NodeSpacingAfter = 3;
            this.ShowRootLines = false;
            this.ShowLines = false;
            //Create a new new node
            UltraTreeNode oNewNode = new UltraTreeNode(@"-999999999", "Everyone");
            oNewNode.Override.NodeStyle = NodeStyle.CheckBox;
            oNewNode.Override.NodeAppearance.Image = imageList1.Images["Everyone"];
            //Add the new node to the tree
            this.Nodes.Add(oNewNode);
            oNewNode = new UltraTreeNode("Offices", "Offices");
            oNewNode.Override.NodeAppearance.Image = imageList1.Images["Offices"];
            this.Nodes.Add(oNewNode);

            //Add checkbox nodes for each office
            Offices oOffices = new Offices();
            ArrayList aDisplay = oOffices.ToArrayList(new int[] { 0, 4 });
            for (int i = 0; i < aDisplay.Count; i++)
            {
                UltraTreeNode oOfficeNode = new UltraTreeNode(((object[])aDisplay[i])[0].ToString(),
                    ((object[])aDisplay[i])[1].ToString());
                oNewNode.Nodes.Add(oOfficeNode);
            }
            oNewNode.Nodes.Override.NodeStyle = NodeStyle.CheckBox;
            oNewNode.Expanded = true;

            oNewNode = new UltraTreeNode("Groups", "Groups");
            oNewNode.Override.NodeAppearance.Image = imageList1.Images["Groups"];
            this.Nodes.Add(oNewNode);
            //Add checkbox nodes for each group
            PeopleGroups oGroups = new PeopleGroups();
            aDisplay = oGroups.ToArrayList(new int[] { 0, 1 });
            for (int i = 0; i < aDisplay.Count; i++)
            {
                UltraTreeNode oGroupNode = new UltraTreeNode(((object[])aDisplay[i])[0].ToString(),
                    ((object[])aDisplay[i])[1].ToString());
                oNewNode.Nodes.Add(oGroupNode);
            }
            oNewNode.Nodes.Override.NodeStyle = NodeStyle.CheckBox;
            oNewNode.Expanded = true;
        }

        /// <summary>
        /// Uncheck all nodes in tree
        /// </summary>
        private void UnCheckTreeNodes()
        {
            UnCheckTreeNodes(null);
        }
        /// <summary>
        /// Uncheck the specified node and all children
        /// </summary>
        /// <param name="oNode"></param>
        private void UnCheckTreeNodes(UltraTreeNode oNode)
        {
            if (oNode == null)
            {
                foreach (UltraTreeNode oTopLevelNode in this.Nodes)
                {
                    UnCheckTreeNodes(oTopLevelNode);
                }
            }
            else 
            {
                if (oNode.NodeStyleResolved == NodeStyle.CheckBox)
                {
                    oNode.CheckedState  = CheckState.Unchecked;
                }
                foreach (UltraTreeNode oChild in oNode.Nodes)
                {
                    UnCheckTreeNodes(oChild);
                }
            }
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Checked state of a node has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PermissionsTree_AfterCheck(object sender, NodeEventArgs e)
        {
            try
            {
                if (e.TreeNode.Key == "-999999999" && e.TreeNode.CheckedState == CheckState.Checked)
                {
                    //If Everyone node has been checked, uncheck all others
                    UnCheckTreeNodes(this.Nodes["Offices"]);
                    UnCheckTreeNodes(this.Nodes["Groups"]);
                }
                else if (e.TreeNode.CheckedState == CheckState.Checked)
                {
                    //Otherwise uncheck Everyone if individual group node has been checked
                    UnCheckTreeNodes(this.Nodes["-999999999"]);
                }
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        #endregion
        #region *********************public methods*********************
        /// <summary>
        /// Write selected permissions to database
        /// </summary>
        public void SavePermissions()
        {
            //Clear existing permissions
            Data.AdminPermissions.RevokePermission(m_iObjectTypeID, m_iObjectID, 0);
            
            if (this.Nodes["-999999999"].CheckedState == CheckState.Checked)
            {
                //Assign permission to entire firm
                Data.AdminPermissions.GrantPermission(m_iObjectTypeID, m_iObjectID, -999999999);
                return;
            }
            else
            {
                foreach (UltraTreeNode oNode in this.Nodes["Offices"].Nodes)
                {
                    if (oNode.CheckedState == CheckState.Checked)
                    {
                        Data.AdminPermissions.GrantPermission(m_iObjectTypeID, m_iObjectID, Int32.Parse(oNode.Key));
                    }
                }
                foreach (UltraTreeNode oNode in this.Nodes["Groups"].Nodes)
                {
                    if (oNode.CheckedState == CheckState.Checked)
                    {
                        Data.AdminPermissions.GrantPermission(m_iObjectTypeID, m_iObjectID, Int32.Parse(oNode.Key));
                    }
                }
            }
        }
        /// <summary>
        /// Sets Checked state of Group nodes based on Object Type and ID specified
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="iObjectID"></param>
        public void SetObjectFilter(mpObjectTypes iObjectTypeID, int iObjectID)
        {
            if (!m_bInitialized)
                ExecuteFinalSetup();

            m_iObjectTypeID = iObjectTypeID;
            m_iObjectID = iObjectID;
            UnCheckTreeNodes();
            if (Data.AdminPermissions.IsEveryonePermitted(m_iObjectTypeID, m_iObjectID))
            {
                //Assigned to Everyone, only check the first node
                this.Nodes["-999999999"].CheckedState = CheckState.Checked;
            }
            else
            {
                //If not assigned to Everyone, get individual Group and Office assignments
                ArrayList aOffices = Data.AdminPermissions.GetPermittedOffices(m_iObjectTypeID, m_iObjectID);
                for (int i = 0; i <= aOffices.Count - 1; i++)
                {
                    string xID = ((object[])aOffices[i])[0].ToString();
                    UltraTreeNode oNode = this.GetNodeByKey(xID);
                    if (oNode != null)
                    {
                        oNode.CheckedState = CheckState.Checked;
                    }
                }
                ArrayList aGroups = Data.AdminPermissions.GetPermittedGroups(m_iObjectTypeID, m_iObjectID);
                for (int i = 0; i <= aGroups.Count - 1; i++)
                {
                    string xID = ((object[])aGroups[i])[0].ToString();
                    UltraTreeNode oNode = this.GetNodeByKey(xID);
                    if (oNode != null)
                    {
                        oNode.CheckedState = CheckState.Checked;
                    }
                }
            }
            m_bFilterSet = true;
            this.IsDirty = false;
        }
        #endregion

        private void PermissionsTree_BeforeCheck(object sender, BeforeCheckEventArgs e)
        {
            try
            {
                //GLOG item #5984 - dcf
                if (m_bFilterSet && m_iParentFolderID > 0 && e.TreeNode.CheckedState == CheckState.Unchecked)
                {
                    bool bExists = false;

                    foreach (object[] oPermission in m_oPermissions)
                    {
                        if ((int)oPermission[0] == -999999999)
                        {
                            e.Cancel = false;
                            return;
                        }
                    }

                    //prevent adding permissions to sub folders
                    foreach (object[] oPermission in m_oPermissions)
                    {
                        if (int.Parse(e.TreeNode.Key) == (int)oPermission[0])
                        {
                            bExists = true;
                            break;
                        }
                    }

                    if (bExists)
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_CantAddPermission"), e.TreeNode.Text),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

    }
}
