﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class CustomMessageBox : Form
    {
        private MessageBoxIcon m_iIcon = MessageBoxIcon.Information;
        private MessageBoxButtons m_iButtons = MessageBoxButtons.OK;
        private MessageBoxDefaultButton m_iDefault = MessageBoxDefaultButton.Button1;
        public CustomMessageBox()
        {
            InitializeComponent();
        }
        public DialogResult Show(string xMessage, string xTitle, MessageBoxButtons iButtons, MessageBoxIcon iIcon, MessageBoxDefaultButton iDefault)
        {
            this.Message = xMessage;
            this.Title = xTitle;
            m_iIcon = iIcon;
            m_iButtons = iButtons;
            m_iDefault = iDefault;
            return this.ShowDialog();
        }

        public DialogResult Show(string xMessage, string xTitle, MessageBoxButtons iButtons, MessageBoxIcon iIcon)
        {
            return this.Show(xMessage, LMP.ComponentProperties.ProductName, iButtons, iIcon, m_iDefault);
        }
        public DialogResult Show(string xMessage)
        {
            return this.Show(xMessage, LMP.ComponentProperties.ProductName, m_iButtons, m_iIcon, m_iDefault);
        }
        public DialogResult Show(string xMessage, string xTitle)
        {
            return this.Show(xMessage, xTitle, m_iButtons, m_iIcon, m_iDefault);
        }
        private string Title
        {
            set { this.Text = value;}
        }
        private string Message
        {
            set { this.lblMessage.Text = value; }
        }
        public string OKText
        {
            set { this.btnOK.Text = value; }
        }
        public string CancelText
        {
            set { this.btnCancel.Text = value; }
        }
        public string YesText
        {
            set { this.btnYes.Text = value; }
        }
        public string NoText
        {
            set { this.btnNo.Text = value; }
        }

        private void CustomMessageBox_Load(object sender, EventArgs e)
        {
            int iFarRight = btnCancel.Left + btnCancel.Width;

            if (m_iButtons == MessageBoxButtons.OKCancel || m_iButtons == MessageBoxButtons.YesNoCancel)
            {
                this.CancelButton = this.btnCancel;
                iFarRight = btnCancel.Left - 8;
            }
            else
                btnCancel.Visible = false;

            if (m_iButtons == MessageBoxButtons.OK || m_iButtons == MessageBoxButtons.OKCancel)
            {
                btnOK.Left = iFarRight - btnOK.Width;
                iFarRight = btnOK.Left - 8;
                if (m_iDefault == MessageBoxDefaultButton.Button2 && this.btnCancel.Visible)
                {
                    this.AcceptButton = this.btnCancel;
                    this.ActiveControl = this.btnCancel;
                }
                else
                {
                    this.AcceptButton = this.btnOK;
                    this.ActiveControl = this.btnOK;
                }
            }
            else
                btnOK.Visible = false;
            
            if (m_iButtons == MessageBoxButtons.YesNoCancel || m_iButtons == MessageBoxButtons.YesNo)
            {
                btnNo.Left = iFarRight - btnNo.Width;
                iFarRight = btnNo.Left - 8;
                btnYes.Left = iFarRight - btnYes.Width;
                if (btnCancel.Visible)
                {
                    this.CancelButton = this.btnCancel;
                }
                else
                {
                    this.CancelButton = this.btnNo;
                }
                if (m_iDefault == MessageBoxDefaultButton.Button3 && this.btnCancel.Visible)
                {
                    this.AcceptButton = btnCancel;
                    this.ActiveControl = btnCancel;
                }
                else if (m_iDefault == MessageBoxDefaultButton.Button2 && this.btnNo.Visible)
                {
                    this.AcceptButton = btnNo;
                    this.ActiveControl = btnNo;
                }
                else
                {
                    this.AcceptButton = btnYes;
                    this.ActiveControl = btnYes;
                }
            }
            else
            {
                btnNo.Visible = false;
                btnYes.Visible = false;
            }
            Icon oUseIcon = null;
            switch (m_iIcon)
            {
                case MessageBoxIcon.Information:
                    //Same as Asterisk
                    oUseIcon = SystemIcons.Information;
                    break;
                case MessageBoxIcon.Error:
                    //Same as Stop, Hand
                    oUseIcon = SystemIcons.Error;
                    break;
                case MessageBoxIcon.Exclamation:
                    //Same as Warning
                    oUseIcon = SystemIcons.Exclamation;
                    break;
                case MessageBoxIcon.Question:
                    oUseIcon = SystemIcons.Question;
                    break;
                case MessageBoxIcon.None:
                    break;

            }
            if (oUseIcon != null)
            {
                pbIcon.Image = oUseIcon.ToBitmap();
            }
        }


    }
}
