using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class RelineFormatOptionsList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aOptions;
        #endregion
        #region *********************constructors*********************
        static RelineFormatOptionsList()
        {
            //get array of insertion locations
            m_aOptions = new object[,]{{"Reline Bold", 1}, 
                                        {"Reline Italic", 2}, 
                                        {"Reline Underline", 4}, 
                                        {"Reline Underline Last Line", 8}, 
                                        {"Special Bold", 16}, 
                                        {"Special Italic", 32}, 
                                        {"Special Underline", 64},
                                        {"Special Underline Last Line", 128}};
        }

        public RelineFormatOptionsList()
            : base(m_aOptions)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aOptions; }
        }
        #endregion
    }
}
