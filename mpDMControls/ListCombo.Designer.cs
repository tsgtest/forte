namespace LMP.Controls
{
    partial class ListCombo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEllipsis = new System.Windows.Forms.Button();
            this.comboBox1 = new LMP.Controls.ComboBox();
            this.SuspendLayout();
            // 
            // btnEllipsis
            // 
            this.btnEllipsis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEllipsis.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEllipsis.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEllipsis.Location = new System.Drawing.Point(234, -1);
            this.btnEllipsis.Name = "btnEllipsis";
            this.btnEllipsis.Size = new System.Drawing.Size(23, 25);
            this.btnEllipsis.TabIndex = 1;
            this.btnEllipsis.Text = "...";
            this.btnEllipsis.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEllipsis.UseVisualStyleBackColor = false;
            this.btnEllipsis.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.AllowEmptyValue = false;
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.AutoSize = true;
            this.comboBox1.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox1.Borderless = false;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.comboBox1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.IsDirty = false;
            this.comboBox1.LimitToList = true;
            this.comboBox1.ListName = "";
            this.comboBox1.Location = new System.Drawing.Point(0, 0);
            this.comboBox1.MaxDropDownItems = 8;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndex = -1;
            this.comboBox1.SelectedValue = null;
            this.comboBox1.SelectionLength = 0;
            this.comboBox1.SelectionStart = 0;
            this.comboBox1.Size = new System.Drawing.Size(234, 26);
            this.comboBox1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.comboBox1.SupportingValues = "";
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Tag2 = null;
            this.comboBox1.Value = "";
            this.comboBox1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.comboBox1_ValueChanged);
            this.comboBox1.Enter += new System.EventHandler(this.comboBox1_Enter);
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            // 
            // ListCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnEllipsis);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ListCombo";
            this.Size = new System.Drawing.Size(256, 23);
            this.Resize += new System.EventHandler(this.ListCombo_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Button btnEllipsis;
        private ComboBox comboBox1;
    }
}
