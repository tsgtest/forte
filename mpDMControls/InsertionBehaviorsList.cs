using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class InsertionBehaviorsList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aBehaviors;
        #endregion
        #region *********************constructors*********************
        //GLOG - 3590 - CEH
        static InsertionBehaviorsList()
        {
            m_aBehaviors = new object[,]{
                {"Insert In Separate Next Page Section",1}, 
                {"Keep Existing Headers Footers",2}, 
                {"Restart Page Numbering",4},
                {"Insert In Separate Continuous Section",8}};
        }

        public InsertionBehaviorsList()
            : base(m_aBehaviors)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aBehaviors; }
        }
        #endregion

        #region *********************protected members*********************
        protected override void OnClick(EventArgs e)
        {
            //if section option already checked, uncheck it
            UncheckPreviousSectionOptions();
            base.OnClick(e);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            //if section option already checked, uncheck it
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
                UncheckPreviousSectionOptions();

            base.OnKeyDown(e);
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// uncheck existing section option
        /// </summary>
        private void UncheckPreviousSectionOptions()
        {
            string xSelectedText = this.SelectedItem.ToString();
            if (xSelectedText.ToUpper().Contains("SECTION"))
            {
                for (int i = 0; i < this.CheckedItems.Count; i++)
                {
                    
                    if (this.CheckedItems[i].ToString().ToUpper().Contains("SECTION"))
                    {
                        if (this.SelectedItem != this.CheckedItems[i])
                        {
                            this.SetItemChecked(this.CheckedIndices[i], false);
                            break;
                        }
                    }
                }

            }
        }
        #endregion

    }
}
