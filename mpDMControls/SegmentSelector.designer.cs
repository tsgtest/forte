namespace LMP.Controls
{
    partial class SegmentSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEllipse = new System.Windows.Forms.Button();
            this.txtDisplayName = new LMP.Controls.TextBox();
            this.SuspendLayout();
            // 
            // btnEllipse
            // 
            this.btnEllipse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEllipse.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEllipse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEllipse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEllipse.Location = new System.Drawing.Point(192, -1);
            this.btnEllipse.Name = "btnEllipse";
            this.btnEllipse.Size = new System.Drawing.Size(23, 22);
            this.btnEllipse.TabIndex = 1;
            this.btnEllipse.TabStop = false;
            this.btnEllipse.Text = "...";
            this.btnEllipse.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEllipse.UseVisualStyleBackColor = false;
            this.btnEllipse.Click += new System.EventHandler(this.btnEllipse_Click);
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisplayName.IsDirty = true;
            this.txtDisplayName.Location = new System.Drawing.Point(0, 0);
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtDisplayName.Size = new System.Drawing.Size(192, 20);
            this.txtDisplayName.SupportingValues = "";
            this.txtDisplayName.TabIndex = 0;
            this.txtDisplayName.Tag2 = null;
            this.txtDisplayName.Value = "";
            this.txtDisplayName.TabPressed += new LMP.Controls.TabPressedHandler(this.txtDisplayName_TabPressed);
            this.txtDisplayName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDisplayName_KeyDown);
            // 
            // SegmentSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.btnEllipse);
            this.Controls.Add(this.txtDisplayName);
            this.Name = "SegmentSelector";
            this.Size = new System.Drawing.Size(214, 20);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private TextBox txtDisplayName;
        private System.Windows.Forms.Button btnEllipse;
    }
}
