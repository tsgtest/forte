using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{

    public delegate void ButtonPressedHandler(object sender, EventArgs e);

    public partial class ExpressionTextBox : System.Windows.Forms.UserControl, LMP.Controls.IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************

        public ExpressionTextBox()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************methods**************************
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ButtonPressedHandler ButtonPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get { return this.txtExpression.Value; }
            set { this.txtExpression.Value = value; }
        }
        public bool IsDirty
        {
            set 
            { 
                m_bIsDirty = value;
                this.txtExpression.IsDirty = value;
            }

            //TODO: this needs to be implemented
            get
            {
                m_bIsDirty = this.txtExpression.IsDirty; 
                return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            this.txtExpression.SelectAll();
            base.OnGotFocus(e);
        }
        protected override void OnTextChanged(EventArgs e)
        {
            //mark as edited
            m_bIsDirty = true;
            base.OnTextChanged(e);

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }

        #endregion
        #region *********************event handlers*********************
        private void btnEllipse_Click(object sender, EventArgs e)
        {
            ShowExpressionBuilder();
        }

        private void ShowExpressionBuilder()
        {
            ExpressionBuilder oForm = new ExpressionBuilder();

            oForm.Expression = this.txtExpression.Text;

            if (oForm.ShowDialog() == DialogResult.OK)
            {
                this.Value = oForm.Expression;
            }

            this.txtExpression.Focus();
        }

        /// <summary>
        /// broadcast that tab was pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtExpression_TabPressed(object sender, TabPressedEventArgs e)
        {
            //notify subscribers tab was pressed
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }

        private void txtExpression_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ShowExpressionBuilder();
            }
            else
            {
                e.Handled = false;
            }
        }
        #endregion
    }
}
