using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class SupportedLanguagesList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aLanguages;
        #endregion
        #region *********************constructors*********************
        static SupportedLanguagesList()
        {
            //get array of supported languages
            string xLanguages = LMP.Data.Application.SupportedLanguagesString;
            LMP.StringArray oLanguages = new LMP.StringArray(xLanguages);
            string[,] aLanguages = oLanguages.ToArray();
            m_aLanguages = aLanguages;
        }

        public SupportedLanguagesList()
            : base(m_aLanguages, LMP.StringArray.mpEndOfValue)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aLanguages; }
        }
        #endregion
    }
}
