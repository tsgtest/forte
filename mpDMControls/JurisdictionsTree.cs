using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using LMP.Data;

namespace LMP.Controls
{
    public partial class JurisdictionsTree: Infragistics.Win.UltraWinTree.UltraTree, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private string m_xValue;
        private int m_iLevelDepth;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************IControl members*********************
        public event CIRequestedHandler CIRequested;
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        public string Value
        {
            get { return m_xValue; }
            set
            {
                m_xValue = value;
                this.SelectNode(value, false);
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion *********************IControl members*********************
        #region ***************constructors*****************
        public JurisdictionsTree()
        {
            try
            {
                InitializeComponent();
                this.Nodes.Clear();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region **************properties*******************
        /// <summary>
        /// Returns full path of selected node
        /// </summary>
        /// <returns>string</returns>
        public string Path
        {
            get { return this.SelectedNodes[0].FullPath.Replace(@"\\", @"\"); }
        }
        /// <summary>
        /// Specifies the number of jurisdiction levels to show
        /// </summary>
        /// <returns>int</returns>
        public int LevelDepth
        {
            get {return this.m_iLevelDepth;}
            
            set
            {
                this.m_iLevelDepth = value;
                this.BuildTree(value);
            }
        }

        #endregion
        #region **************methods*******************

        /// <summary>
        /// determines ID for each juridiction node in current node path
        /// </summary>
        /// <param name="iLevel0ID"></param>
        /// <param name="iLevel1ID"></param>
        /// <param name="iLevel2ID"></param>
        /// <param name="iLevel3ID"></param>
        /// <param name="iLevel4ID"></param>
        /// <returns>void</returns>
        public void GetJurisdictionIDs(ref int iLevel0ID, ref int iLevel1ID, ref int iLevel2ID, ref int iLevel3ID, ref int iLevel4ID)
        {
            if (this.DesignMode || (this.Site != null && this.Site.DesignMode))
                return;
            if (this.SelectedNodes.Count > 0)
            {
                UltraTreeNode oNode = this.SelectedNodes[0];
                string xNodeInfo = oNode.Key;
                //get id and level
                int iPos = xNodeInfo.IndexOf(".");
                string xLevel = xNodeInfo.Substring(0, iPos);
                string xID = xNodeInfo.Substring(iPos + 1);

                try
                {
                    switch (xLevel)
                    {
                        case "0":
                            iLevel0ID = Convert.ToInt32(xID);
                            break;
                        case "1":
                            iLevel1ID = Convert.ToInt32(xID);

                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel0ID = Convert.ToInt32(xID);
                            break;
                        case "2":
                            iLevel2ID = Convert.ToInt32(xID);

                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel1ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("1." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel0ID = Convert.ToInt32(xID);

                            break;
                        case "3":
                            iLevel3ID = Convert.ToInt32(xID);

                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel2ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("2." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel1ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("1." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel0ID = Convert.ToInt32(xID);
                            break;
                        case "4":
                            iLevel4ID = Convert.ToInt32(xID);

                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel3ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("3." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel2ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("2." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel1ID = Convert.ToInt32(xID);

                            oNode = this.GetNodeByKey("1." + xID);
                            xNodeInfo = oNode.Parent.Key;
                            iPos = xNodeInfo.IndexOf(".");
                            xID = xNodeInfo.Substring(iPos + 1);
                            iLevel0ID = Convert.ToInt32(xID);
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString("Error_JurisdictionDoesNotExist") + xNodeInfo, oE);
                }
            }
        }

        /// <summary>
        /// GLOG : 3217 : JAB
        /// Populate the specified nodes with all its children to
        /// facilitate copy and paste functionality.
        /// </summary>
        /// <param name="oCurNode"></param>
        public void PopulateParentNode(UltraTreeNode oParentNode)
        {
            if (this.DesignMode || (this.Site != null && this.Site.DesignMode))
                return;
            if ((bool)((object[])oParentNode.Tag)[1] == false)
            {
                //The expanding node has not already had children
                //set to it- get and bind the relevant child nodes

                Jurisdictions oCurJurisdictions = null;

                if (oParentNode.Level <= this.LevelDepth)
                {
                    //Only attempt to bind child nodes to the 
                    //expanding node if there are more levels of 
                    //jurisdictions below the expanding one

                    switch (oParentNode.Level)
                    {
                        //Get a Jurisdictions object representative of the 
                        //children of the expanding jurisdiction- the cases
                        //specify which level of jurisdictions should be returned

                        case 0:
                            oCurJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);
                            break;

                        case 1:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.One, (int)((object[])oParentNode.Tag)[0]);
                            break;

                        case 2:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Two, (int)((object[])oParentNode.Tag)[0]);
                            break;

                        case 3:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Three, (int)((object[])oParentNode.Tag)[0]);
                            break;

                        case 4:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Four, (int)((object[])oParentNode.Tag)[0]);
                            break;
                    }


                    //Create an array of UltraTreeNodes big with size
                    //equal to the number of child jurisdictions
                    UltraTreeNode[] aChildNodes = new UltraTreeNode[oCurJurisdictions.Count];

                    object[] aNodeIDandIsBound;

                    UltraTreeNode oNodeToAdd;

                    ////Get an array of the child nodes
                    object[] aChildJurisdictions = (object[])oCurJurisdictions.ToArray();

                    for (int i = 0; i < aChildJurisdictions.Length; i++)
                    {
                        //Initialize a new array to be used as the tag for 
                        //the child node being created
                        aNodeIDandIsBound = new object[2];

                        //Store the ID and the fact that this node has not yet
                        //been expanded in the array that will be the tag
                        aNodeIDandIsBound[0] = ((object[])aChildJurisdictions[i])[0];
                        aNodeIDandIsBound[1] = false;

                        int iLevel = oParentNode.Level;
                        string xID = ((object[])aChildJurisdictions[i])[0].ToString();

                        //Create the new node and set its tag
                        oNodeToAdd = new UltraTreeNode("", ((object[])aChildJurisdictions[i])[1].ToString());
                        oNodeToAdd.Tag = aNodeIDandIsBound;
                        //add key to node showing level and id
                        oNodeToAdd.Key = iLevel.ToString() + "." + xID;
                        //show expansion indicator or not
                        if (oParentNode.Level == this.LevelDepth)
                            oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                        else
                        {
                            if (oParentNode.Level == (this.LevelDepth - 1))
                            {
                                if (this.JurisdictionHasChildren(oNodeToAdd, iLevel))
                                    oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                                else
                                    oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            }
                            else
                            {
                                oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                            }
                        }
                        //Store the new node in an array
                        aChildNodes[i] = oNodeToAdd;
                    }

                    //Add the new child nodes to treeJurisdictions
                    //as children of the expanding node
                    oParentNode.Nodes.AddRange(aChildNodes);
                }

                //Store the expanding node has been expanded
                ((object[])oParentNode.Tag)[1] = true;
            }

            // Recursively populate each child node with its children.
            foreach (UltraTreeNode oChildNode in oParentNode.Nodes)
            {
                PopulateParentNode(oChildNode);
            }
        }

        #endregion
        #region **************private methods*******************
        /// <summary>
        /// When the user expands a node in the tree
        /// this gets the child jurisdictions for the 
        /// jurisdiction being expanded and adds them to 
        /// treeJurisdictions as children of the expanding node
        /// </summary>
        /// <param name="oCurNode"></param>
        private void BindNodesToCurExpandingNode(UltraTreeNode oCurNode)
        {
            if (this.DesignMode || (this.Site != null && this.Site.DesignMode))
                return;
            if ((bool)((object[])oCurNode.Tag)[1] == false)
            {
                //The expanding node has not already had children
                //set to it- get and bind the relevant child nodes

                Jurisdictions oCurJurisdictions = null;

                if (oCurNode.Level <= this.LevelDepth)
                {
                    //Only attempt to bind child nodes to the 
                    //expanding node if there are more levels of 
                    //jurisdictions below the expanding one

                    switch (oCurNode.Level)
                    {
                        //Get a Jurisdictions object representative of the 
                        //children of the expanding jurisdiction- the cases
                        //specify which level of jurisdictions should be returned

                        case 0:
                            oCurJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);
                            break;

                        case 1:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.One, (int)((object[])oCurNode.Tag)[0]);
                            break;

                        case 2:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Two, (int)((object[])oCurNode.Tag)[0]);
                            break;

                        case 3:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Three, (int)((object[])oCurNode.Tag)[0]);
                            break;

                        case 4:
                            oCurJurisdictions = new Jurisdictions
                                 (Jurisdictions.Levels.Four, (int)((object[])oCurNode.Tag)[0]);
                            break;
                    }


                    //Create an array of UltraTreeNodes big with size
                    //equal to the number of child jurisdictions
                    UltraTreeNode[] aChildNodes = new UltraTreeNode[oCurJurisdictions.Count];

                    object[] aNodeIDandIsBound;

                    UltraTreeNode oNodeToAdd;

                    ////Get an array of the child nodes
                    object[] aChildJurisdictions = (object[])oCurJurisdictions.ToArray();

                    for (int i = 0; i < aChildJurisdictions.Length; i++)
                    {
                        //Initialize a new array to be used as the tag for 
                        //the child node being created
                        aNodeIDandIsBound = new object[2];

                        //Store the ID and the fact that this node has not yet
                        //been expanded in the array that will be the tag
                        aNodeIDandIsBound[0] = ((object[])aChildJurisdictions[i])[0];
                        aNodeIDandIsBound[1] = false;

                        int iLevel = oCurNode.Level;
                        string xID = ((object[])aChildJurisdictions[i])[0].ToString();

                        //Create the new node and set its tag
                        oNodeToAdd = new UltraTreeNode("", ((object[])aChildJurisdictions[i])[1].ToString());
                        oNodeToAdd.Tag = aNodeIDandIsBound;
                        //add key to node showing level and id
                        oNodeToAdd.Key = iLevel.ToString() + "." + xID;
                        //show expansion indicator or not
                        if (oCurNode.Level == this.LevelDepth)
                            oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                        else
                        {
                            if (oCurNode.Level == (this.LevelDepth - 1))
                            {
                                if (this.JurisdictionHasChildren(oNodeToAdd, iLevel))
                                    oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                                else
                                    oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            }
                            else
                            {
                                oNodeToAdd.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                            }
                        }
                        //Store the new node in an array
                        aChildNodes[i] = oNodeToAdd;
                    }

                    //Add the new child nodes to treeJurisdictions
                    //as children of the expanding node
                    oCurNode.Nodes.AddRange(aChildNodes);
                }

                //Store the expanding node has been expanded
                ((object[])oCurNode.Tag)[1] = true;
            }
        }
        /// <summary>
        /// Builds the jurisdiction tree 
        /// to the level specified
        /// </summary>
        /// <param name="iLevel"></param>
        private void BuildTree(int iLevel)
        {
            if (this.DesignMode || (this.Site != null && this.Site.DesignMode))
                return;

            if (iLevel < 0)
                return;

            //create Jurisdictions collection
            Jurisdictions oCurJurisdictions = null;

            //Create a new new node
            UltraTreeNode oFirstNode = new UltraTreeNode();

            //Set the displayed text of the new node
            oFirstNode.Text = "The World";

            //Create a an array to be used as the Tag
            object[] aNodeIDandIsBound = new object[2];
            aNodeIDandIsBound[0] = 0;
            aNodeIDandIsBound[1] = false;

            //Set the tag
            oFirstNode.Tag = aNodeIDandIsBound;

            //Set the key
            oFirstNode.Key = "-1.0";

            //Add the new node to the tree
            this.Nodes.Add(oFirstNode);

            //set to 0 level of Jurisdictions
            oCurJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);

            //loop through first level
            this.BindNodesToCurExpandingNode(oFirstNode);
            //Select "the USA" node
            this.Value = "0.1";
            //glog 6253: Make sure "World" node is visible if there are no scrollbars
            oFirstNode.BringIntoView();

        }
        /// <summary>
        /// Selects the node according to level & id
        /// </summary>
        /// <param name="xNodeInfo"></param>
        private void SelectNode(string xNodeInfo, bool IsParent)
        {
            if (this.DesignMode || (this.Site != null && this.Site.DesignMode))
                return;
            if (xNodeInfo == null)
                return;

            UltraTreeNode oNode = this.GetNodeByKey(xNodeInfo);
            if (oNode != null)
            {
                oNode.Selected = true;
                if (IsParent)
                    oNode.Expanded = true;
                else
                    this.ActiveNode = oNode;
            }
            else
            {
                //node is not available,
                //get id and level
                int iPos = xNodeInfo.IndexOf(".");
                string xLevel = xNodeInfo.Substring(0, iPos);
                string xID = xNodeInfo.Substring(iPos + 1);
                string xParentKey = "";
                Jurisdictions oJurisdictions = new Jurisdictions(Jurisdictions.Levels.One);
                Jurisdiction oJurisdiction = (Jurisdiction)oJurisdictions.ItemFromIndex(1);

                try
                {

                    switch (xLevel)
                    {
                        case "0":
                            //this level is already expanded, node does not exist
                            break;
                        case "1":
                            xParentKey = "0.";
                            oJurisdiction = (Jurisdiction)oJurisdictions.ItemFromID(Int32.Parse(xID));
                            break;
                        case "2":
                            oJurisdictions = new Jurisdictions(Jurisdictions.Levels.Two);
                            oJurisdiction = (Jurisdiction)oJurisdictions.ItemFromID(Int32.Parse(xID));
                            xParentKey = "1.";
                            break;
                        case "3":
                            oJurisdictions = new Jurisdictions(Jurisdictions.Levels.Three);
                            oJurisdiction = (Jurisdiction)oJurisdictions.ItemFromID(Int32.Parse(xID));
                            xParentKey = "2.";
                            break;
                        case "4":
                            oJurisdictions = new Jurisdictions(Jurisdictions.Levels.Four);
                            oJurisdiction = (Jurisdiction)oJurisdictions.ItemFromID(Int32.Parse(xID));
                            xParentKey = "3.";
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString("Error_JurisdictionDoesNotExist") + xNodeInfo, oE);
                }
                if (oJurisdiction == null)
                    return;
                else
                {
                    xParentKey = xParentKey + oJurisdiction.ParentID.ToString();
                    //if (!NodeExpanded(xParentKey))
                    //{
                    SelectNode(xParentKey, true);
                    if (Convert.ToInt32(xLevel) > this.LevelDepth)
                        return;
                    else
                        SelectNode(xNodeInfo, false);
                    //}
                }
            }
        }
        /// <summary>
        /// Determines if node is expanded
        /// </summary>
        /// <param name="xNodeInfo"></param>
        /// <returns>bool</returns>
        private bool NodeExpanded(string xNodeInfo)
        {
            UltraTreeNode oNode = this.GetNodeByKey(xNodeInfo);
            if (oNode != null)
            {
                if (oNode.Expanded)
                    return true;
                else
                {
                    oNode.Expanded = true;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Determines if Jurisdiction has children
        /// </summary>
        /// <param name="iLevel"></param>
        /// <param name="xID"></param>
        /// <returns>bool</returns>
        private bool JurisdictionHasChildren(UltraTreeNode oChildNode, int iLevel)
        {
            Jurisdictions oCurJurisdictions = null;
            bool bHasChildren = false;

            switch (iLevel + 1)
            {
                //Get a Jurisdictions object representative of the 
                //children of the expanding jurisdiction- the cases
                //specify which level of jurisdictions should be returned

                case 0:
                    oCurJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);
                    break;

                case 1:
                    oCurJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.One, (int)((object[])oChildNode.Tag)[0]);
                    break;

                case 2:
                    oCurJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Two, (int)((object[])oChildNode.Tag)[0]);
                    break;

                case 3:
                    oCurJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Three, (int)((object[])oChildNode.Tag)[0]);
                    break;

                case 4:
                    oCurJurisdictions = new Jurisdictions
                         (Jurisdictions.Levels.Four, (int)((object[])oChildNode.Tag)[0]);
                    break;
            }
            if (oCurJurisdictions.Count > 0)
                bHasChildren = true;

            return bHasChildren;
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue == 18)
                    e.SuppressKeyPress = true;
                //else if (e.KeyCode == Keys.F2)
                //{
                //    //request CI if there are subscribers
                //    if (this.CIRequested != null)
                //        this.CIRequested(this, new EventArgs());
                //}
                else if (e.KeyCode == Keys.Tab)
                {
                    if (this.TabPressed != null)
                        this.TabPressed(this, new TabPressedEventArgs(e.Shift));
                }
                else
                    base.OnKeyDown(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		/// <summary>
		/// ensures that a processed tab (processed in ProcessKeyMessage)
		/// won't continue to be processed
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		protected override bool ProcessKeyEventArgs(ref Message m)
		{
			if(m.WParam.ToInt32() == (int) Keys.Tab)
				return true;
			else
				return base.ProcessKeyEventArgs (ref m);
		}
		/// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion
        #region *********************event handlers*********************
        private void JurisdictionTreeView_AfterSelect(object sender, SelectEventArgs e)
        {
            if (this.SelectedNodes.Count > 0)
                this.m_xValue = this.SelectedNodes[0].Key;
        }
        private void JurisdictionsTree_AfterExpand(object sender, NodeEventArgs e)
        {
            if(!this.DesignMode && (this.Site == null || !this.Site.DesignMode))
                this.BindNodesToCurExpandingNode(e.TreeNode);
        }
        #endregion *********************event handlers*********************

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // JurisdictionsTree
            // 
            this.HideSelection = false;
            this.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.JurisdictionTreeView_AfterSelect);
            this.AfterExpand += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.JurisdictionsTree_AfterExpand);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

    }
}
