using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win.UltraWinTree;

namespace LMP.Controls
{
    public partial class SegmentTreeview : Form
    {
        #region *********************constants**************************
        private const string mpPathSeparator = "\\\\";
        #endregion
        #region *********************fields**************************
        //TODO: settle on an ITEM_SEP - these are set to work with ComboBox.SetList(string)
        public const char ITEM_SEP = '~';
        public const char NAME_VALUE_SEP = ';';
        private string m_xSegmentID = "";
        private bool m_bMultipleSelection = false;
        //GLOG : 8380 : jsw
        private bool m_bExludeNonMacPacTypes = false;

        #endregion
        #region *********************constructors**************************
        /// <summary>
        /// Contructor
        /// </summary>
        public SegmentTreeview()
        {
            InitializeComponent();
            InitializeControls();
        }
        public SegmentTreeview(bool bMultipleSelection, string xSegmentsList)
        {
            InitializeComponent();
            m_bMultipleSelection = bMultipleSelection;
            InitializeControls();
            SetupSegmentList(xSegmentsList);
        }
        #endregion
        #region *********************methods**************************
        private void InitializeControls()
        {
            if (!DesignMode)
            {
                try
                {
                    //GLOG : 7276 : JSW
                    this.ftvSegment.AllowFavoritesMenu = true;
                    ////GLOG : 8380 : jsw
                    //this.ftvSegment.ExcludeNonMacPacSegments = this.ExcludeNonMacPacSegments;
                    this.ftvSegment.OwnerID = LMP.Data.Application.User.ID;
                    this.ftvSegment.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
                    this.ftvSegment.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
                    DataTable oDT = new DataTable();
                    if (m_bMultipleSelection)
                    {
                        oDT.Columns.Add("DisplayName");
                        oDT.Columns.Add("ID");
                        oDT.PrimaryKey = new DataColumn[] { oDT.Columns["ID"] };
                        this.lstSelected.DataSource = oDT;
                        this.lstSelected.DisplayMember = "DisplayName";
                        this.lstSelected.ValueMember = "ID";
                        this.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_Title_Multi");
                    }
                    else
                    {
                        this.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_Title_Single");
                    }
                    this.lblSegments.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_TreeCaption");
                    this.lblSelected.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_SelectedCaption");
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }
        /// <summary>
        /// Populate listbox from delimited list of Segment IDs and Display Names
        /// </summary>
        /// <param name="xInputString"></param>
        public void SetupSegmentList(string xInputString)
        {
            try
            {
                if (lstSelected.Items.Count > 0)
                    lstSelected.Items.Clear();
                if (xInputString != "")
                {
                    string[] xItemArray = xInputString.Split(ITEM_SEP);
                    DataTable oDT = (DataTable)lstSelected.DataSource;
                    for (int i = 0; i < xItemArray.Length; i++)
                    {
                        string[] xDetail = xItemArray[i].Split(NAME_VALUE_SEP);
                        oDT.Rows.Add(xDetail[0], xDetail[1]);
                    }
                    lstSelected.Refresh();
                }
            }
            catch
            {
                MessageBox.Show(LMP.Resources.GetLangString("Error_InvalidInputString") + xInputString, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Return listbox contents as delimited list of Segment IDs and Display Names
        /// </summary>
        /// <returns></returns>
        private string BuildSegmentList()
        {
            string xTemp = "";
            DataTable oDT = (DataTable)lstSelected.DataSource;
            for (int i = 0; i < oDT.Rows.Count; i++)
            {
                xTemp = xTemp + oDT.Rows[i]["DisplayName"] + NAME_VALUE_SEP.ToString() + oDT.Rows[i]["ID"];
                if (i < oDT.Rows.Count - 1)
                    xTemp = xTemp + ITEM_SEP.ToString();
            }
            return xTemp;
        }
        
        /// <summary>
        /// Hide optional controls and resize
        /// </summary>
        private void SetupControl()
        {
            if (!m_bMultipleSelection)
            {
                this.lblSelected.Visible = false;
                this.lstSelected.Visible = false;
                this.btnAdd.Visible = false;
                this.btnDelete.Visible = false;
                this.Width = btnAdd.Left - 10;
                this.ftvSegment.Width = this.Width - 30;
                this.ftvSegment.Height = this.btnOK.Top - (this.ftvSegment.Top + 10);
            }
            this.ftvSegment.ExecuteFinalSetup();

			//GLOG : 7698 : ceh
            string xDefaultFolder = "";

            try
            {
                xDefaultFolder = LMP.Data.Application.User.GetPreferenceSet(
                    mpPreferenceSetTypes.UserApp, "").GetValue("DefaultContentFolder");
            }
            catch { }

            if (!string.IsNullOrEmpty(xDefaultFolder))
            {
                SelectFolderNode(xDefaultFolder);
            }
        }
        /// <summary>
        /// Select Folder node corresponding to Key
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private void SelectFolderNode(string xKey)
        {
            UltraTreeNode oNode = LoadNodeIfNecessary(xKey);
            if (oNode != null)
            {
                oNode.Selected = true;
                this.ftvSegment.Tree.ActiveNode = oNode;
                this.ftvSegment.Tree.ActiveNode.Expanded = true;
            }
        }
        /// <summary>
        /// loads the specified folder into the tree if necessary
        /// </summary>
        /// <param name="xFolderPath"></param>
        private UltraTreeNode LoadNodeIfNecessary(string xNodeKey)
        {
            try
            {
                UltraTreeNode oNode = this.ftvSegment.Tree.GetNodeByKey(xNodeKey);
                if (oNode != null)
                    //node already exists in tree
                    return oNode;

                //node does not exist in tree - get node
                UltraTreeNode oNewNode = null;
                string xParentKey, xNewKey;
                string[] xSep = new string[] { mpPathSeparator };
                string[] xPath = xNodeKey.Split(xSep, StringSplitOptions.None);
                for (int i = xPath.GetLowerBound(0); i <= xPath.GetUpperBound(0); i++)
                {
                    if (oNewNode != null)
                        xParentKey = oNewNode.Key + mpPathSeparator;
                    else
                        xParentKey = "";
                    xNewKey = xParentKey + xPath[i];
                    LMP.Trace.WriteNameValuePairs("Level " + i.ToString(), xNewKey);
                    oNewNode = null;
                    oNewNode = this.ftvSegment.Tree.GetNodeByKey(xPath[i]);
                    if (oNewNode != null)
                    {
                        //GLOG item #6670 - dcf
                        if (!oNewNode.Expanded)
                            oNewNode.Expanded = true;
                    }
                    else
                    {
                        // Default path doesn't exist
                        return null;
                    }
                }
                return oNewNode;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region *********************properties**************************
        /// <summary>
        /// Returns ID of Segment selected in Treeview, or delimited list of IDs and Names for Multiple selection
        /// </summary>
        public string Value
        {
            get
            {
                if (!m_bMultipleSelection)
                    return m_xSegmentID;
                else
                    return this.BuildSegmentList();
            }
        }
        public FolderTreeView.mpFolderTreeViewOptions DisplayOptions
        {
            get { return this.ftvSegment.DisplayOptions; }
            set { this.ftvSegment.DisplayOptions = value; }
        }
        public UltraTreeNode SelectedNode
        {
            get { return this.ftvSegment.Tree.ActiveNode; }
        }
        //GLOG : 8380 : jsw
        /// <summary>
        /// Get/Set ExcludeNonMacPacSegments for displayed objects
        /// </summary>
        public bool ExcludeNonMacPacTypes
        {
            get { return ftvSegment.ExcludeNonMacPacTypes; }
            set
            {
                ftvSegment.ExcludeNonMacPacTypes = value;
            }
        }

        #endregion
        #region *********************event handlers**************************
        /// <summary>
        /// True if Node corresponds to an VariableSet/Prefill object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsVariableSetNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if ((oNode.Tag is string && ((string)oNode.Tag).StartsWith("P")) ||
                (oNode.Tag is FolderMember && ((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.VariableSet) ||
                (oNode.Tag is VariableSetDef))
                return true;
            else
                return false;
        }
        #endregion
        #region *********************event handlers**************************
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //GLOG : 8353 : ceh - set dialog result in code
            //return if no selection
            try
            {
                if (ftvSegment.MemberList.Count > 0)
                    m_xSegmentID = ((ArrayList)ftvSegment.MemberList[0])[0].ToString();
            }
            catch { }

            if (m_xSegmentID == "" && !m_bMultipleSelection)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_NoSegmentSelected"), LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.ftvSegment.Focus();
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_xSegmentID = "";
            this.Close();
        }
        /// <summary>
        /// Set options first time form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SegmentTreeview_Load(object sender, EventArgs e)
        {
            SetupControl();
        }
        /// <summary>
        /// Delete selected segment from list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SegmentDeleteEvent(object sender, EventArgs e)
        {
            try
            {
                if (this.lstSelected.SelectedItems.Count == 0)
                    return;
                DataTable oDT = (DataTable)this.lstSelected.DataSource;
                ListBox.SelectedObjectCollection oCol = this.lstSelected.SelectedItems;
                for (int i = 0; i < oCol.Count; i++)
                {
                    DataRowView oRow = (DataRowView)oCol[i];
                    oDT.Rows.Remove(oRow.Row);
                }
                lstSelected.Update();
                this.lstSelected.ClearSelected();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Add selected segment to list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SegmentAddEvent(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 3159 : CEH
                //double-click event = OK for non-multiple selection
                if (!m_bMultipleSelection)
                {
                    if (ftvSegment.MemberList.Count > 0)
                    m_xSegmentID = ((ArrayList)ftvSegment.MemberList[0])[0].ToString();

                    if (m_xSegmentID == "")
                        return;

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    string xSegmentID = "";
                    string xSegmentName = "";
                    if (ftvSegment.MemberList.Count > 0)
                    {
                        xSegmentID = ((ArrayList)ftvSegment.MemberList[0])[0].ToString();
                        //GLOG 6966: Make sure string can be parsed as a Double even if
                        //the Decimal separator is something other than '.' in Regional Settings
                        if (xSegmentID.EndsWith(".0"))
                        {
                            xSegmentID = xSegmentID.Replace(".0", "");
                        }
                        //GLOG item #4605 - dcf
                        //xSegmentName = ((ArrayList)ftvSegment.MemberList[0])[1].ToString();
                        double dID = double.Parse(xSegmentID);
                        int iID = Convert.ToInt32(dID);
                        xSegmentName = ((AdminSegmentDef)(new AdminSegmentDefs()).ItemFromID(iID)).DisplayName;
                    }
                    if (xSegmentID != "")
                    {
                        DataTable oDT = (DataTable)lstSelected.DataSource;
                        if (oDT.Rows.Contains(xSegmentID))
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_SegmentAlreadySelected"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            oDT.Rows.Add(xSegmentName, xSegmentID);
                            lstSelected.SelectedIndex = lstSelected.Items.Count - 1;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion

    }
}