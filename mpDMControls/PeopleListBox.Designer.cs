using System.Windows.Forms;

namespace LMP.Controls
{
    partial class PeopleListBox : UserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPeople = new System.Windows.Forms.ListBox();
            this.clbPeople = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // lbPeople
            // 
            this.lbPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPeople.FormattingEnabled = true;
            this.lbPeople.Location = new System.Drawing.Point(0, 0);
            this.lbPeople.Name = "lbPeople";
            this.lbPeople.Size = new System.Drawing.Size(187, 244);
            this.lbPeople.TabIndex = 0;
            this.lbPeople.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PeopleListBox_KeyPress);
            // 
            // clbPeople
            // 
            this.clbPeople.CheckOnClick = true;
            this.clbPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clbPeople.FormattingEnabled = true;
            this.clbPeople.Location = new System.Drawing.Point(0, 0);
            this.clbPeople.Name = "clbPeople";
            this.clbPeople.Size = new System.Drawing.Size(187, 244);
            this.clbPeople.TabIndex = 1;
            this.clbPeople.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PeopleListBox_KeyPress);
            // 
            // PeopleListBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.clbPeople);
            this.Controls.Add(this.lbPeople);
            this.Name = "PeopleListBox";
            this.Size = new System.Drawing.Size(187, 244);
            this.Load += new System.EventHandler(this.PeopleListBox_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PeopleListBox_KeyPress);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbPeople;
        private System.Windows.Forms.CheckedListBox clbPeople;
    }
}
