using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;

namespace LMP.Controls
{
    public class LanguagesComboBox: LMP.Controls.ComboBox
    {
        #region *********************fields*********************
        private static string[,] m_aSupportedLanguages;
        private static string[,] m_aLanguages;
        #endregion
        #region *********************constructors*********************
        static LanguagesComboBox()
        {
            //get array of supported languages
            string xLanguages = LMP.Data.Application.SupportedLanguagesString;
            LMP.StringArray oLanguages = new LMP.StringArray(xLanguages);
            m_aSupportedLanguages = oLanguages.ToArray();
        }

        /// <summary>
        /// creates LanguageComboBox populated with all supported languages
        /// </summary>
        public LanguagesComboBox()
        {
            //GLOG 3413: Prevent blanking control
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            //include all
            m_aLanguages = m_aSupportedLanguages;
            this.SetList(m_aLanguages);
        }
        /// <summary>
        /// creates LanguageComboBox populated with languages specified in
        /// delimited list of ids
        /// </summary>
        /// <param name="xLanguages"></param>
        /// <param name="xLanguages"></param>
        public LanguagesComboBox(string xLanguages, char cDelimiter)
        {
            //GLOG 3413: Prevent blanking control
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            string[] aLanguages = xLanguages.Split(cDelimiter);
            m_aLanguages = new string[aLanguages.Length, 2];
            for (int i = 0; i < aLanguages.Length; i++)
            {
                m_aLanguages[i, 0] = LMP.Data.Application
                    .GetLanguagesDisplayValue(aLanguages[i]);
                m_aLanguages[i, 1] = aLanguages[i];
            }
            this.SetList(m_aLanguages);
        }
        #endregion
        #region *********************properties***************************
        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        public static object[,] List
        {
            get
            {
                return m_aLanguages;
            }
        }

        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LanguagesComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.Name = "LanguagesComboBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
