using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data; 

namespace LMP.Controls
{
    /// <summary>
    /// ListCombo class.  Defines a MacPac ListCombo box
    /// Self-loads list of lists defined in tblLists
    /// Created by Michael Conner 3/2006
    /// </summary>
    public partial class ListCombo : System.Windows.Forms.UserControl, LMP.Controls.IControl
    {
        private string m_xList = "";
        private bool m_bBorderless = false;
        private bool m_bLimitToList = true;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bValueSetListsOnly = false;
        private bool m_bButtonPressed = false;
        private bool m_bAllowEmptyValue = false;
        private string m_xSupportingValues = "";

        #region *********************constructors**************************
        public ListCombo()
        {
            InitializeComponent();

        }
        #endregion
        #region *********************properties*********************
        [DescriptionAttribute("Gets/Sets the name of the list that displays in the dropdown portion of the control.")]
        public string ListName
        {
            get { return m_xList; }
            set
            {
                m_xList = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the text of the control.")]
        public override string Text
        {
            get { return this.comboBox1.Text; }
            set
            {
                this.comboBox1.Text = value;

            }
        }
        [DescriptionAttribute("Gets/sets whether empty value is allowed in Limit-to-list combo.")]
        public bool AllowEmptyValue
        {
            get { return m_bAllowEmptyValue; }
            set 
            { 
                m_bAllowEmptyValue = value;
                this.comboBox1.AllowEmptyValue = value;
            }
        }
        /// <summary>
        /// gets/sets the list items of the control as a string
        /// </summary>
        [BrowsableAttribute(false)]
        public string ListString
        {
            get
            {
                return this.comboBox1.ListString;
            }
        }

        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public string[,] ListArray
        {
            get
            {
                return this.comboBox1.ListArray; 
            }
        }
        [DescriptionAttribute("Gets/Sets whether user must select an item from the list.")]
        public bool LimitToList
        {
            get { return m_bLimitToList; }
            set 
            { 
                m_bLimitToList = value;
                this.comboBox1.LimitToList = value;
            }
        }
        [DescriptionAttribute("Gets/Sets whether control displays value set lists only")]
        public bool ValueSetListsOnly
        {
            get { return m_bValueSetListsOnly; }
            set 
            { 
                m_bValueSetListsOnly = value;
                //load lists list here to ensure filtering
                RefreshList() ;
            }
        }
        [DescriptionAttribute("Gets/Sets the backcolor of the control.")]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                this.comboBox1.BackColor = value;
            }
        }

        [DescriptionAttribute("Gets/Sets the font of the control.")]
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                this.comboBox1.Font = value;
            }
        }

        [DescriptionAttribute("Gets/Sets whether or not the control displays borders.")]
        public bool Borderless
        {
            get { return m_bBorderless; }
            set
            {
                m_bBorderless = value;
                this.Invalidate();
            }
        }
        [DescriptionAttribute("Gets/Sets the number of dropdown rows displayed.")]
        public int MaxDropDownItems
        {
            get { return this.comboBox1.MaxDropDownItems; }
            set { this.comboBox1.MaxDropDownItems = value; }
        }
        [DescriptionAttribute("Gets/Sets the index of the selected value.")]
        public int SelectedIndex
        {
            get { return this.comboBox1.SelectedIndex; }
            set
            {
                this.comboBox1.SelectedIndex = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the length of the selected text.")]
        public int SelectionLength
        {
            get { return this.comboBox1.SelectionLength; }
            set { this.comboBox1.SelectionLength = value; }
        }
        [DescriptionAttribute("Gets/Sets the first selected character.")]
        public int SelectionStart
        {
            get { return this.comboBox1.SelectionStart; }
            set { this.comboBox1.SelectionStart = value; }
        }

        #endregion
        #region *********************methods*********************
        /// <summary>
        /// loads combo with list of lists in tblLists
        /// </summary>
        private void LoadListsList()
        {
            try
            {
                //load list of lists
                Lists oLists = LMP.Data.Application.GetLists(this.ValueSetListsOnly);
                string[,] aLists = new string[oLists.Count, 2];

                for (int i = 1; i <= oLists.Count; i++)
                {
                    List oList = (List)oLists[i];
                    aLists[i - 1, 0] = oList.Name;
                    aLists[i - 1, 1] = oList.ID.ToString();
                }

                this.SetList(aLists);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);                
            }
        }
       
        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        private void SetList(string[,] aListItems)
        {
            this.comboBox1.SetList(aListItems);
            this.Invalidate();
        }

        /// <summary>
        /// 
        /// </summary>
        public void RefreshList()
        {
            LoadListsList(); 
        }

        #endregion
        #region *********************event handlers*********************
        private void button1_Click(object sender, EventArgs e)
        {
            ShowListManagerForm();
        }

        private void ShowListManagerForm()
        {
            try
            {
                m_bButtonPressed = true;
                Form oForm = new ListsManagerForm();
                oForm.StartPosition = FormStartPosition.CenterScreen;
                oForm.ShowDialog(this);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// notify subscribers if combobox value has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_ValueChanged(object sender, EventArgs e)
        {
            //notify that value has been changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }

        /// <summary>
        /// refreshes list if ellipse button has been pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_Enter(object sender, EventArgs e)
        {
            try
            {
                if (m_bButtonPressed)
                {
                    this.RefreshList();
                    m_bButtonPressed = false;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F12)
            {
                ShowListManagerForm();
            }
            else
            {
                e.Handled = false;
            }
        }

        private void ListCombo_Resize(object sender, EventArgs e)
        {
            //GLOG 3732: Make sure height doesn't change when
            //added to TaskPane
            this.Height = 23;
        }

        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// Determine if the key pressed is recognized for input by the control
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosting
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            //line below causes last character of selected text to be hidden
            //this.comboBox1.SelectAll();
            base.OnGotFocus(e);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;
        //public event ButtonPressedHandler ButtonPressed;

        public void ExecuteFinalSetup()
        {
            this.comboBox1.ExecuteFinalSetup();
        }

        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get { return this.comboBox1.Value;}
            set
            {
                this.comboBox1.Value = value;
            }
        }

        public bool IsDirty
        {
            set 
            {
                m_bIsDirty = value;
                this.comboBox1.IsDirty = value;
            }

            get { return this.comboBox1.IsDirty; }
        }

        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion

    }
}
