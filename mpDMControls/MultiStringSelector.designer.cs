namespace LMP.Controls
{
    partial class MultiStringSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEllipse = new System.Windows.Forms.Button();
            this.txtSelected = new LMP.Controls.TextBox();
            this.SuspendLayout();
            // 
            // btnEllipse
            // 
            this.btnEllipse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEllipse.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEllipse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEllipse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEllipse.Location = new System.Drawing.Point(192, -1);
            this.btnEllipse.Name = "btnEllipse";
            this.btnEllipse.Size = new System.Drawing.Size(23, 24);
            this.btnEllipse.TabIndex = 1;
            this.btnEllipse.TabStop = false;
            this.btnEllipse.Text = "...";
            this.btnEllipse.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEllipse.UseVisualStyleBackColor = false;
            this.btnEllipse.Click += new System.EventHandler(this.btnEllipse_Click);
            // 
            // txtSelected
            // 
            this.txtSelected.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSelected.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSelected.IsDirty = true;
            this.txtSelected.Location = new System.Drawing.Point(0, 0);
            this.txtSelected.Name = "txtSelected";
            this.txtSelected.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtSelected.Size = new System.Drawing.Size(192, 22);
            this.txtSelected.SupportingValues = "";
            this.txtSelected.TabIndex = 0;
            this.txtSelected.Tag2 = null;
            this.txtSelected.Value = "";
            this.txtSelected.TabPressed += new LMP.Controls.TabPressedHandler(this.txtSelected_TabPressed);
            this.txtSelected.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSelected_KeyDown);
            // 
            // MultiStringSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnEllipse);
            this.Controls.Add(this.txtSelected);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MultiStringSelector";
            this.Size = new System.Drawing.Size(214, 22);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private TextBox txtSelected;
        private System.Windows.Forms.Button btnEllipse;
    }
}
