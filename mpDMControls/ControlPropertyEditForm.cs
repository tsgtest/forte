using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Controls;
using Infragistics.Win.UltraWinGrid;


namespace LMP.Controls
{
    public partial class ControlPropertyEditForm : Form
    {
        #region ***************************constants*********************************
        private const int CTL_TOP = 33;
        private const int CTL_LEFT = 15;
        private const AnchorStyles CTL_ANCHORS = AnchorStyles.Top | AnchorStyles.Left;
        private const int FORM_HEIGHT_LARGE = 220;
        private const int FORM_HEIGHT_MED = 175;
        private const int FORM_HEIGHT_SMALL = 120;
        private const int FORM_WIDTH_WIDE = 700;
        #endregion
        #region ***************************enumerations*******************************
        private enum ControlWidths
        {
            Default = 285,
            Spinner = 285,
            SalutationSchemaGrid = 200,
            DetailConfigGrid = 650
        }
        private enum ControlHeights
        {
            MultilineTextBox = 60,
            Spinner = 20,
            TextBox = 20,
            DetailConfigGrid = 95,
            CheckedListBox = 120,
            CheckedListBoxTall = 130,
            SalutationSchema = 77,
            DateComboFormatsGrid = 120
        }
        private enum ConfigurationControlTypes
        {
            mpControl = 0,
            RelineGridConfigStringTextBoxes = 1,
            ListCombo = 2,
            DetailGridConfigStringGrid = 3,
            CheckedListBox = 4,
            DropdownList = 5,
            BooleanCombo = 6,
            AuthorSelector = 7,
            SalutationSchemaGrid = 8,
            ExpressionTextBox = 9,
            DateComboFormatsGrid = 10,
            LocationChooser = 11

        }

        private enum DetailGridFieldTypes
        {
            None = 0,
            TextBox = 1,
            ComboBox = 2,
            ListBox = 3,
            MultiLineCombo = 4,
            BooleanCombo = 5
        }

        #endregion
        #region ***************************fields************************************
        private Control m_oCtl;
        private string m_xValue;
        private DataTable m_oDataTable;
        private System.Windows.Forms.ComboBox m_oCombo;
        private bool m_bShiftPressed = false;
        private bool m_bInitializing = false;
        private bool m_bIgnoreComboSelIndex = false;
        private string m_xRelatedPropertyName;
        private string m_xRelatedValue;
        #endregion
        #region ***************************constructors******************************
        public ControlPropertyEditForm(LMP.Data.mpControlTypes iType, string xPropertyName, string xValue)
        {
            InitializeComponent();

            this.Value = xValue;
            
            //set form caption
            if (!System.String.IsNullOrEmpty(xPropertyName))
                this.Text = this.Text + " - " + xPropertyName;
            
            //set up control to edit the specified property
            m_bInitializing = true;
            SetupEditingControls(iType, xPropertyName);
        }
        /// <summary>
        /// this constructor provides related property name and value for those
        /// properties whose configuration control setup depends on the supplied
        /// related value
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xValue"></param>
        /// <param name="xRelatedPropertyName"></param>
        /// <param name="xRelatedValue"></param>
        public ControlPropertyEditForm(LMP.Data.mpControlTypes iType, 
            string xPropertyName, string xValue, string xRelatedPropertyName, string xRelatedValue)
        {
            InitializeComponent();

            this.Value = xValue;
            
            //set related prop name, related value
            m_xRelatedPropertyName = xRelatedPropertyName;
            m_xRelatedValue = xRelatedValue;

            //set form caption
            if (!System.String.IsNullOrEmpty(xPropertyName))
                this.Text = this.Text + " - " + xPropertyName;
            
            //set up control to edit the specified property
            m_bInitializing = true;
            SetupEditingControls(iType, xPropertyName, xRelatedValue);
        }       
        #endregion
        #region ***************************properties********************************
        [DescriptionAttribute("Gets the selected value.")]
        public string Value
        {
            set { m_xValue = value; }
            get 
            {
                m_xValue = this.GetValue();
                return m_xValue;
            } 
        }
        #endregion
        #region ***************************internal procedures***********************
        /// <summary>
        /// overloaded method
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xPropertyName"></param>
        private void SetupEditingControls(mpControlTypes iType, string xPropertyName)
        {
            SetupEditingControls(iType, xPropertyName, string.Empty);
        }
        
        /// <summary>
        /// creates the necessary configuration controls for each task pane control type
        /// </summary>
        /// <param name="iType">Task pane control type</param>
        /// <param name="xPropertyName">Property to be configured for each control type</param>
        private void SetupEditingControls(mpControlTypes iType, string xPropertyName, 
            string xRelatedValue)
        {
            Control oCtl = null;
            string xLabelTextID = null;
            switch (iType)
            {
                case mpControlTypes.LocationsDropdown:
                    switch (xPropertyName)
                    {
                        case "LocationType":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyLocationsLocationType";
                            break;
                        case "LocationParentID":
                            //GLOG 3563: Use LocationChooser for this
                            oCtl = CreateLocationChooser(xPropertyName, xRelatedValue);
                            xLabelTextID = "Dialog_ControlPropertyLocationsLocationParentID";
                            break;
                        case "ValueField":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyLocationsValueField";
                            break;
                        case "MaxDropDownItems":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 20);
                            xLabelTextID = "Dialog_ControlPropertyMaxDropDownItems";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.AuthorSelector:
                    switch (xPropertyName)
                    {
                        case "ListType":
                            oCtl = CreateComboBox("AuthorListType", true);
                            xLabelTextID = "Dialog_ControlPropertyAuthorListType";
                            break;
                        case "AllowManualInput":
                        case "AllowManualInputLeadAuthor":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            //set label text
                            xLabelTextID = xPropertyName == "AllowManualInput" ? 
                                "Dialog_ControlPropertyAuthorAllowManualInput" :
                                    "Dialog_ControlPropertyAuthorAllowManualInputLeadAuthor";
                            break;
                        case "DropDownRows":
                        case "DisplayRows":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 10);
                            xLabelTextID = xPropertyName == "DropDownRows" ?
                                "Dialog_ControlPropertyAuthorDropdownRows" :
                                    "Dialog_ControlPropertyAuthorDisplayRows";
                            break;
                        case "ShowColumns":
                            oCtl = CreateCheckedListBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyAuthorShowColumns";
                            break;
                        case "ListWhereClause":
                            oCtl = CreateMultiLineTextBox(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyAuthorListWhereClause";
                            break;
                        default:
                            break;
                    }
                    break;
                
                case mpControlTypes.MultilineCombo:
                    switch (xPropertyName)
                    {
                        case "ForceCaps":
                            oCtl = CreateBooleanCombo(xPropertyName); 
                            xLabelTextID = "Dialog_ControlPropertyMultiLineComboForceCaps";
                            break;
                        case "ListName":
                            oCtl = CreateListCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyListName";
                            break;
                        case "ListRows":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 20);
                            xLabelTextID = "Dialog_ControlPropertyMaxDropDownItems";
                            break;
                        case "Separator":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyMultiLineComboSeparator";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.DropdownList: 
                case mpControlTypes.Combo:
                    switch (xPropertyName)
                    {
                        case "ListName":
                            oCtl = CreateListCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyListName";
                            break;
                        case "MaxDropDownItems":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 10);
                            xLabelTextID = "Dialog_ControlPropertyMaxDropDownItems";
                            break;
                        case "AllowEmptyValue":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyAllowEmptyValue";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.PeopleDropdown:
                    switch (xPropertyName)
                    {
                        case "WhereClause":
                            oCtl = CreateExpressionTextBox(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyPeopleDropdownWhereClause";
                            break;
                        case "MaxDropDownItems":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 10);
                            xLabelTextID = "Dialog_ControlPropertyMaxDropDownItems";
                            break;
                        case "FilterOptions":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyFilterOptions";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.Calendar:
                    switch (xPropertyName)
                    {
                        case "CustomDateFormat":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyDatePickerCustomFormat";
                            break;
                        case "ShowPickerUpDown":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyDatePickerShowUpDown";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.DetailGrid:
                    switch (xPropertyName)
                    {
                        case "ConfigString":
                            oCtl =  CreateGrid(xPropertyName, 
                                    ConfigurationControlTypes.DetailGridConfigStringGrid);
                            xLabelTextID = "Dialog_ControlPropertyDetailGridConfigString";
                            break;
                        case "AutoSize":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyDetailAutosize";
                            break;
                        case "CounterScheme":
                            oCtl = CreateComboBox(xPropertyName, false);
                            xLabelTextID = "Dialog_ControlPropertyDetailCounterScheme";
                            break;
                        case "LabelColumnWidth":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 50, 150);
                            xLabelTextID = "Dialog_ControlPropertyDetailColumnLabelWidth";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.DetailList:
                    switch (xPropertyName)
                    {
                        case "CIToken":
                            oCtl = CreateExpressionTextBox(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyDetailListCIToken";
                            break;
                        case "AutoSize":
                        case "ShowGridlines":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = xPropertyName == "AutoSize" ?
                                "Dialog_ControlPropertyDetailListAutosize" :
                                    "Dialog_ControlPropertyDetailListShowGridlines";
                            break;
                        case "VisibleRows":
                        case "RowsPerItem":
                            oCtl = CreateSpinner(xPropertyName);
                            decimal dMax = xPropertyName == "VisibleRows" ? 10 : 4;
                            SetSpinner(oCtl, 1, 1, dMax);
                            xLabelTextID = xPropertyName == "VisibleRows" ?
                                "Dialog_ControlPropertyDetailListVisibleRows" :
                                    "Dialog_ControlPropertyDetailListRowsPerItem";
                            break;
                        case "CounterScheme":
                            oCtl = CreateComboBox(xPropertyName, false);
                            xLabelTextID = "Dialog_ControlPropertyDetailCounterScheme";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.RelineGrid:
                    switch (xPropertyName)
                    {
                        case "CaptionString":
                            //we will use 2 textboxes for this string;
                            //create one here so we can pass it to the configuration
                            //routine
                            oCtl = CreateTextBox(xPropertyName, "txtRelineConfig1", true, 
                                    ConfigurationControlTypes.RelineGridConfigStringTextBoxes,
                                    mpControlTypes.None, true);
                            xLabelTextID = "Dialog_ControlPropertyConfigStringTab1";
                            break;
                        case "FormatOptions":
                        case "FormatValues":
                            oCtl = CreateCheckedListBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyReline" + xPropertyName;
                            break;
                        case "Style":
                        case "DefaultTab":
                            oCtl = CreateComboBox(xPropertyName, true);    
                            xLabelTextID = xPropertyName == "Style" ?
                                "Dialog_ControlPropertyRelineStyle" :
                                    "Dialog_ControlPropertyRelineDefaultTab";
                            break;
                        default:
                            break;
                    }
                    break;
                
                case mpControlTypes.SalutationCombo:
                    switch (xPropertyName)
                    {
                        case "Schema":
                            oCtl = CreateGrid(xPropertyName, ConfigurationControlTypes.SalutationSchemaGrid); 
                            xLabelTextID = "Dialog_ControlPropertySalutationSchema";

                            break;
                        case "AllowDropdown":
                        case "CanRequestCI":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = xPropertyName == "AllowDropdown" ?
                                "Dialog_ControlPropertySalutationAllowDropdown" :
                                    "Dialog_ControlPropertySalutationCanRequestCI";
                            break;
                        case "SelectionType":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertySalutationSelectionType";
                            break;
                        case "DefaultListIndex":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 0, 10);
                            xLabelTextID = "Dialog_ControlPropertySalutationDefaultListIndex";
                            break;
                        default:
                            break;
                    }
                    break;
                
                case mpControlTypes.DateCombo:
                    switch (xPropertyName)
                    {
                        case  "Formats":
                            oCtl = CreateCheckedListBox(xPropertyName, false); 
                            xLabelTextID = "Dialog_ControlPropertyDateComboFormats";
                            break;
                        case "MaxDropDownItems":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 10);
                            xLabelTextID = "Dialog_ControlPropertyMaxDropDownItems";
                            break;
                        case "Mode":
                            oCtl = CreateComboBox(xPropertyName, false);
                            xLabelTextID = "Dialog_ControlPropertyDateComboMode";
                            break;
                        case "ListName":
                            oCtl = CreateListCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyListName";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.MultilineTextbox:
                    switch (xPropertyName)
                    {
                        case "NumberOfLines":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 10);
                            xLabelTextID = "Dialog_ControlPropertyMultiLineTextBoxNumLines";
                            break;
                        case "SelectionMode": //GLOG 6235
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyTextBoxSelectionMode";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.Textbox:  //GLOG 6235
                    switch (xPropertyName)
                    {
                        case "SelectionMode":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyTextBoxSelectionMode";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.Spinner:
                    switch (xPropertyName)
                    {
                        case "AppendSymbol":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertySpinnerAppendSymbol";
                            break;
                        case "DisplayUnit":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertySpinnerDisplayUnit";
                            break;
                        case "DecimalPlaces":
                        case "Increment":
                            oCtl = CreateSpinner(xPropertyName);
                            decimal dMax = xPropertyName == "DecimalPlaces" ? 3 : 10;
                            decimal dInc = xPropertyName == "Increment" ? (decimal).25 : 1;
                            decimal dMin = 0;
                            SetSpinner(oCtl, dInc, dMin, dMax);
                            xLabelTextID = xPropertyName == "DecimalPlaces" ?
                               "Dialog_ControlPropertySpinnerDecimalPlaces" :
                                    "Dialog_ControlPropertySpinnerIncrement";
                            break;
                        case "Maximum":
                        case "Minimum":
                            oCtl = CreateTextBox(xPropertyName, null, true, null, mpControlTypes.Textbox, false);
                            xLabelTextID = xPropertyName == "Minimum" ?
                               "Dialog_ControlPropertySpinnerMinimum" :
                                    "Dialog_ControlPropertySpinnerMaximum";
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.SegmentChooser:
                    switch (xPropertyName)
                    {
                        case "IncludeNoneOption":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyChooserIncludeNone";
                            break;
                        case "AssignedObjectType":
                        default:
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyAssignedObjectType";
                            break;
                    }
                    break;
                case mpControlTypes.FontList:
                    switch (xPropertyName)
                    {
                        case "OnNotInList":
                        default:
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyFontListOnNotInList";
                            break;
                    }
                    break;
                case mpControlTypes.JurisdictionChooser:
                    switch (xPropertyName)
                    {
                        case "FirstLevel":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 0, 4);
                            xLabelTextID = "Dialog_ControlPropertyJurisdictionFirstLevel";
                            break;
                        case "LastLevel":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 0, 4);
                            xLabelTextID = "Dialog_ControlPropertyJurisdictionLastLevel";
                            break;
                        case "HideDisabledLevels":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyJurisdictionHideDisabledLevels";
                            break;
                        case "Level0Label":
                        case "Level1Label":
                        case "Level2Label":
                        case "Level3Label":
                        case "Level4Label":
                            oCtl = CreateTextBox(xPropertyName, null, true, null, mpControlTypes.Textbox, false);
                            xLabelTextID = "Dialog_ControlPropertyJurisdiction" + xPropertyName;
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.NameValuePairGrid:
                    switch (xPropertyName)
                    {
                        case "AllowAddRow":
                        case "AllowDeleteRow":
                        case "AllowEmptyRow":
                        case "RemoveEmptyValues":
                        case "WrapCellText":
                        case "AllowColumnResize":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlPropertyNameValueGrid" + xPropertyName;
                            break;
                        case "TabStopColumns":
                            oCtl = CreateCheckedListBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyNameValueGrid" + xPropertyName;
                            break;
                        case "VisibleRows":
                            oCtl = CreateSpinner(xPropertyName);
                            SetSpinner(oCtl, 1, 1, 20);
                            xLabelTextID = "Dialog_ControlPropertyNameValueGrid" + xPropertyName;
                            break;
                        case "NameValueSeparator": //GLOG 4559
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyNameValueGrid" + xPropertyName;
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.StartingLabelSelector:
                    switch (xPropertyName)
                    {
                        case "LabelType":
                            oCtl = CreateComboBox(xPropertyName, true);
                            xLabelTextID = "Dialog_ControlPropertyLabelType";
                            break;
                        case "AlternatingRowBackColor":
                            oCtl = CreateBooleanCombo(xPropertyName);
                            xLabelTextID = "Dialog_ControlProperty" + xPropertyName;
                            break;
                        case "Height":
                            oCtl = CreateTextBox(xPropertyName, null, false, null, mpControlTypes.Textbox, false);
                            xLabelTextID = "Dialog_ControlPropertyStartingLabelSelectorHeight";
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            if (oCtl == null)
            {
                //no control set either because of no control type
                //or no property name - default to text box
                oCtl = CreateTextBox(xPropertyName, null, true, null, mpControlTypes.Textbox, false );
                xLabelTextID = "Dialog_ControlPropertyEnterPropertyValue";
            }
            
            //add control to form; set module level control var & label text
            //There is an instance of the grid on the form;
            //There is also an instance of the textbox - we don't have to add either
            if (oCtl.Name != "grdProperties" && oCtl.Name != "txtRelineConfig1")
                this.Controls.Add(oCtl);
            m_oCtl = oCtl;
            
            //set the label text for the form
            this.lblPropertyName.Text = LMP.Resources.GetLangString(xLabelTextID);

            //JTS 1/30/09: Anchor was not being properly set previously, since & was being used instead of |
            oCtl.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            oCtl.Top = lblPropertyName.Top + lblPropertyName.Height;
            oCtl.Left = lblPropertyName.Left - 1;
            //resize form - this will also force the buttons to reposition
            this.Height = oCtl.Top + oCtl.Height + 92;
        }
        /// <summary>
        /// creates and configures a multiline combo
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <param name="xSeparator"></param>
        /// <param name="iListRows"></param>
        /// <param name="bAllowReservedCharacters"></param>
        /// <returns></returns>
        private Control CreateMultilineCombo(string xPropertyName, string xSeparator, 
                                                int iListRows, bool bAllowReservedCharacters)
        {
            //create instance of multiline combobox
            Control oCtl = new MultilineCombo(this);
            oCtl.Tag = mpControlTypes.MultilineCombo;
            //load lookup
            ((LMP.Controls.MultilineCombo)oCtl).ListArray = GetPropertySettingsList(xPropertyName);
            ConfigureEditingControls(oCtl, mpControlTypes.MultilineCombo, xPropertyName);
            MultilineCombo oMLC = (MultilineCombo)oCtl;
            oMLC.Separator = xSeparator;
            oMLC.ListRows = iListRows;
            oMLC.AllowReservedCharacterInput = bAllowReservedCharacters;
            InitializeEditingControls(oCtl, mpControlTypes.MultilineCombo, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and configures a textbox
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        private Control CreateTextBox(  string xPropertyName, string xName,
                                        bool bAllowRestrictedCharacters, object oTag,
                                        mpControlTypes oType, bool bAddControl)
        {
            Control oCtl = new Controls.TextBox(bAllowRestrictedCharacters);
            //set the tag either to Controls.Type or ConfigurationControls.Type
            if (oTag != null)
                oCtl.Tag = oTag;
            else
                oCtl.Tag = oType;
            //set name for the first of the reline config string textbox pair
            //name must be set before control is added on the fly
            if (xName != null)
                oCtl.Name = xName;
            if (bAddControl)
                this.Controls.Add(oCtl);
            ConfigureEditingControls(oCtl, oType, xPropertyName);
            InitializeEditingControls(oCtl, oType, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and configures an expression textbox
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        private Control CreateExpressionTextBox(string xPropertyName)
        {
            Control oCtl = new Controls.ExpressionTextBox();
            oCtl.Tag = ConfigurationControlTypes.ExpressionTextBox;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a multiline textbox
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateMultiLineTextBox(string xPropertyName)
        {
            Control oCtl = new Controls.MultilineTextBox(true);
            oCtl.Tag = mpControlTypes.MultilineTextbox;
            ConfigureEditingControls(oCtl, mpControlTypes.MultilineTextbox, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.MultilineTextbox, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a checked listbox control
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateCheckedListBox(string xPropertyName, bool bUseBitwiseList)
        {
            //create instance of CheckedListbox control
            //use either a bitwise list or string array list
            Control oCtl = null;
            if (bUseBitwiseList)
                oCtl = new CheckedListBox(GetBitwiseList(xPropertyName));
            else
                oCtl = new CheckedListBox(GetPropertySettingsList(xPropertyName), StringArray.mpEndOfSubField);

            oCtl.BackColor = Color.FromKnownColor(KnownColor.ControlLight);
            oCtl.Tag = ConfigurationControlTypes.CheckedListBox;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a boolean combobox control
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateBooleanCombo(string xPropertyName)
        {
            Control oCtl = new LMP.Controls.BooleanComboBox();
            oCtl.Tag = ConfigurationControlTypes.BooleanCombo;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// configures and initializes grid
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        private Control CreateGrid(string xPropertyName, object oTag)
        {
            Control oCtl = this.grdProperties;
            //ConfigureDataGridView();
            oCtl.Tag = oTag;// ConfigurationControlTypes.DetailGridConfigStringGrid;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// configures and initializes location chooser
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        private Control CreateLocationChooser(string xPropertyName, string xRelatedValue)
        {
            //GLOG 3563
            Control oCtl = new LMP.Controls.LocationChooser();
            oCtl.Tag = ConfigurationControlTypes.LocationChooser;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName, xRelatedValue);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a combobox control
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateComboBox(string xPropertyName, bool bLimitToList)
        {
            Control oCtl = new LMP.Controls.ComboBox();
            oCtl.Tag = mpControlTypes.Combo;
            //load lookup
            LMP.Controls.ComboBox oCombo = (LMP.Controls.ComboBox)oCtl;
            oCombo.SetList(GetPropertySettingsList(xPropertyName));
            oCombo.LimitToList = bLimitToList;
            ConfigureEditingControls(oCtl, mpControlTypes.Combo, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.Combo, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a spinner control
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateSpinner(string xPropertyName)
        {
            Control oCtl = new LMP.Controls.Spinner();
            oCtl.Tag = ConfigurationControlTypes.mpControl;
            ConfigureEditingControls(oCtl, mpControlTypes.Spinner, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.Spinner, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// creates and initializes a listcombo control
        /// </summary>
        /// <param name="xPropertyName"></param>
        private Control CreateListCombo(string xPropertyName)
        {
            Control oCtl = new LMP.Controls.ListCombo();
            oCtl.Tag = ConfigurationControlTypes.ListCombo;
            ConfigureEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            InitializeEditingControls(oCtl, mpControlTypes.None, xPropertyName);
            return oCtl;
        }
        /// <summary>
        /// configures common spinner settings.
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="dIncrement"></param>
        /// <param name="dMinimum"></param>
        /// <param name="dMaximum"></param>
        private void SetSpinner(Control oCtl, decimal dIncrement, decimal dMinimum, decimal dMaximum)
        {
            LMP.Controls.Spinner oSpin = (LMP.Controls.Spinner)oCtl;
            if (dIncrement < 1)
                oSpin.DecimalPlaces = 2;
            oSpin.Increment = dIncrement;
            oSpin.Minimum = dMinimum;
            oSpin.Maximum = dMaximum;
        }
        /// <summary>
        /// overloaded method
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="iEditCtlType"></param>
        /// <param name="xPropertyName"></param>
        private void ConfigureEditingControls(Control oCtl, mpControlTypes iEditCtlType,
           string xPropertyName)
        {
            ConfigureEditingControls(oCtl, iEditCtlType, xPropertyName, string.Empty);  
        }
             
        /// <summary>
        /// configures position, control dimensions
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="iEditCtlType"></param>
        /// <param name="xPropertyName"></param>
        private void ConfigureEditingControls(Control oCtl, mpControlTypes iEditCtlType, 
            string xPropertyName, string xRelatedPropertyValue)
        {
            //set standard anchor, position, tab index
            oCtl.Anchor = CTL_ANCHORS;
            oCtl.Top = CTL_TOP;
            oCtl.Left = CTL_LEFT;
            oCtl.TabIndex = 1;

            //set control widths according to type
            switch (iEditCtlType)
            {
                case mpControlTypes.None:
                    //this case supports non LMP.Controls such as dataGrid

                    switch ((ConfigurationControlTypes)oCtl.Tag)
                    {
                        case ConfigurationControlTypes.LocationChooser:
                            //GLOG 3563
                            oCtl.Width = (int)ControlWidths.Default;

                            //right now we are displaying only two parent levels
                            if (xRelatedPropertyValue == "2")
                            {
                                //Only Country is displayed
                                ((LocationChooser)oCtl).Mode = LocationChooser.mpLocationChooserMode.Country;
                                ((IControl)oCtl).ExecuteFinalSetup();
                            }
                            else
                            {
                                //Both Country and State are displayed
                                ((LocationChooser)oCtl).Mode = LocationChooser.mpLocationChooserMode.State;
                                ((IControl)oCtl).ExecuteFinalSetup();
                            }
                            break;
                        case ConfigurationControlTypes.RelineGridConfigStringTextBoxes:
                            //we will use 2 textboxes to obtain these values
                            //configure existing text box passed into method

                            LMP.Controls.TextBox oTB = (LMP.Controls.TextBox)oCtl;
                            oTB.Height = (int)ControlHeights.TextBox;
                            oTB.Width = (int)ControlWidths.Default / 2;

                            //configure & add new second textbox
                            LMP.Controls.TextBox oTB2 = new LMP.Controls.TextBox(true);
                            oTB2.Top = oTB.Top;
                            oTB2.Width = oTB.Width;
                            oTB2.Anchor = oTB.Anchor;
                            oTB2.Left = oTB.Left + oTB.Width + 10;
                            oTB2.Height = (int)ControlHeights.TextBox;
                            oTB2.Tag = ConfigurationControlTypes.RelineGridConfigStringTextBoxes;
                            oTB2.Name = "txtRelineConfig2";
                            oTB2.TabIndex = 3;

                            this.Controls.Add(oTB2);

                            //configure & add new label for second textbox
                            Label oLabel2 = new Label();
                            oLabel2.Top = this.lblPropertyName.Top;
                            oLabel2.Left = oTB2.Left + 1;
                            oLabel2.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                            oLabel2.Text = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyConfigStringTab2");
                            oLabel2.TabIndex = 2;
                            this.Controls.Add(oLabel2);
                            //ensure it's in front of the label
                            oTB2.BringToFront();

                            //reset tab index of remaining controls
                            this.btnOK.TabIndex = 4;
                            this.btnCancel.TabIndex = 5;
                            break;
                        
                        case ConfigurationControlTypes.DetailGridConfigStringGrid:
                            //resize form to contain large grid
                            this.Width = FORM_WIDTH_WIDE;
                            this.Height = FORM_HEIGHT_LARGE;
                            DataGridView oGrid = (DataGridView)oCtl;
                            oGrid.Visible = true;
                            oGrid.Width = (int)ControlWidths.DetailConfigGrid;
                            oGrid.Height = (int)ControlHeights.DetailConfigGrid;

                            //reset top/left since we have resized form
                            oCtl.Top = CTL_TOP;
                            oCtl.Left = CTL_LEFT;

                            //build datatable 
                            m_oDataTable = new DataTable();
                            m_oDataTable.Columns.Add("ControlType");
                            m_oDataTable.Columns.Add("DetailFieldName");
                            m_oDataTable.Columns.Add("DetailDisplayName");
                            m_oDataTable.Columns.Add("CIEnabled");
                            m_oDataTable.Columns.Add("ListName");
                            m_oDataTable.Columns.Add("LinesPerField");
                            m_oDataTable.Rows.Add("", "", "", "", "", "");

                            //rebind datatable to grid
                            oGrid.DataSource = m_oDataTable;

                            //set column header text for textbox cells
                            oGrid.Columns[1].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringFieldName");
                            oGrid.Columns[2].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringDisplayName");

                            //replace columns with combobox columns for Control Type, CI Enabled and Lines Per Field
                            //set column header text for these cells
                            oGrid.Columns.Remove("ControlType");
                            oGrid.Columns.Insert(0, CreateComboBoxColumn("ControlType", oGrid.Columns[2].Width, 6)); 
                            oGrid.Columns[0].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringControlType");


                            oGrid.Columns.Remove("CIEnabled");
                            oGrid.Columns.Insert(3, CreateComboBoxColumn("CIEnabled", oGrid.Columns[0].Width, 2));
                            oGrid.Columns[3].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringCIEnabled");


                            oGrid.Columns.Remove("ListName");
                            oGrid.Columns.Insert(4, CreateComboBoxColumn("ListName", oGrid.Columns[0].Width, 10));
                            oGrid.Columns[4].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringListName");


                            oGrid.Columns.Remove("LinesPerField");
                            oGrid.Columns.Insert(5, CreateComboBoxColumn("LinesPerField", oGrid.Columns[0].Width, 10));
                            oGrid.Columns[5].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertyDetailConfigStringLinesPerField");

                            //set column size to fit control width
                            for (int i = 0; i < oGrid.ColumnCount; i++)
                            {
                                oGrid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                            
                            //complete configuration
                            oGrid.AllowUserToDeleteRows = false;
                            oGrid.AllowUserToAddRows = true;
                            oGrid.AllowUserToResizeColumns = false;
                            oGrid.AllowUserToResizeRows = false;
                            oGrid.ScrollBars = ScrollBars.Vertical;
                            
                            //subscribe to various events
                            grdProperties.EditingControlShowing +=
                                    new DataGridViewEditingControlShowingEventHandler
                                    (grdProperties_EditingControlShowing);
                            grdProperties.CellLeave += 
                                    new DataGridViewCellEventHandler(grdProperties_CellLeave);
                            grdProperties.DefaultValuesNeeded += new DataGridViewRowEventHandler(grdProperties_DefaultValuesNeeded);
                            //key handlers deal with tabbing across read only DataGridView cells
                            grdProperties.KeyPress += new KeyPressEventHandler(grdProperties_KeyPress);
                            //cast to DataGridView -- we don't want to use the LMP TabPressed event handler
                            ((DataGridView)grdProperties).TabPressed += 
                                    new TabPressedHandler(grdProperties_TabPressed);
                            break;
                        case ConfigurationControlTypes.SalutationSchemaGrid:
                            this.Height = FORM_HEIGHT_LARGE;
                            oCtl.Width = (int)ControlWidths.Default;
                            oCtl.Height = (int)ControlHeights.SalutationSchema;
                            oCtl.Top = CTL_TOP; 

                            oGrid = (DataGridView)oCtl;
                            oGrid.Visible = true;
                            oGrid.AllowUserToAddRows = true;

                            //build datatable 
                            m_oDataTable = new DataTable();
                            m_oDataTable.Columns.Add("SchemaItem");
                            m_oDataTable.Rows.Add("");
                            m_oDataTable.Rows.Add("");

                            //rebind datatable to grid
                            oGrid.DataSource = m_oDataTable;

                            //set column header text for textbox cells
                            oGrid.Columns[0].HeaderText = LMP.Resources.GetLangString
                                        ("Dialog_ControlPropertySalutationSchemaItem");

                            oGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                            //add DataGridViewButtonColumn
                            CreateButtonColumn();

                            //subscribe to grid click event - this will handle button click
                            this.grdProperties.CellContentClick += new DataGridViewCellEventHandler(grdProperties_CellContentClick);

                            //subscribe to editing control showing - handler will assign 
                            //context menu to override native context menu
                            this.grdProperties.EditingControlShowing +=
                                            new DataGridViewEditingControlShowingEventHandler
                                            (grdProperties_EditingControlShowing);
                            
                            //subscribe to LMP version of enterkeypressed --
                            //handler will call expression builder if focus is on
                            //grid button column
                            ((LMP.Controls.DataGridView)grdProperties).EnterKeyPressed += 
                                            new EnterKeyPressedHandler(grdProperties_EnterKeyPressed);
                            break;
                            
                        case ConfigurationControlTypes.CheckedListBox:
                            CheckedListBox oLstbox = (CheckedListBox) oCtl;
                            oCtl.BackColor = Color.White;
                            oLstbox.BorderStyle = BorderStyle.Fixed3D;

                            if (xPropertyName == "FormatOptions" || xPropertyName == "FormatValues")
                            {
                                this.Height = FORM_HEIGHT_LARGE;
                                oCtl.Height = (int)ControlHeights.CheckedListBoxTall;
                                oCtl.BackColor = Color.FromKnownColor(KnownColor.Control); 
                            }
                            else if (xPropertyName == "ShowColumns")
                            {
                                this.Height = FORM_HEIGHT_MED;
                                oCtl.Height = (int)ControlHeights.MultilineTextBox;
                                oCtl.BackColor = Color.FromKnownColor(KnownColor.Control);
                            }
                            else if (xPropertyName == "Formats")
                            {
                                this.Height = FORM_HEIGHT_LARGE;
                                oCtl.Height = (int)ControlHeights.CheckedListBoxTall + 22;
                                oCtl.BackColor = Color.FromKnownColor(KnownColor.Control);
                            }
                            oCtl.Width = (int)ControlWidths.Default;
                            break;
                        case ConfigurationControlTypes.BooleanCombo:
                        case ConfigurationControlTypes.DropdownList:
                            oCtl.Width = (int)ControlWidths.Default;
                            ((LMP.Controls.ComboBox)oCtl).LimitToList = true;
                            break;
                        case ConfigurationControlTypes.ListCombo:
                            oCtl.Width = (int)ControlWidths.Default;
                            ListCombo oCombo = (ListCombo)oCtl;
                            oCombo.LimitToList = false;
                            oCombo.ValueSetListsOnly = false;
                            break;
                        case ConfigurationControlTypes.ExpressionTextBox:
                            oCtl.Width = (int)ControlWidths.Default;
                            break;
                        default:
                            break;
                    }
                    break;
                case mpControlTypes.Spinner:
                    oCtl.Width = (int)ControlWidths.Spinner;
                    break;
                case mpControlTypes.MultilineCombo:
                    this.Height = FORM_HEIGHT_LARGE;
                    oCtl.Width = (int)ControlWidths.Default;
                    oCtl.Height = (int)ControlHeights.MultilineTextBox;
                    break;
                case mpControlTypes.MultilineTextbox:
                    this.Height = FORM_HEIGHT_MED; 
                    oCtl.Width = (int)ControlWidths.Default;
                    oCtl.Height = (int)ControlHeights.MultilineTextBox;
                    break;
                default:
                    oCtl.Width = (int)ControlWidths.Default; 
                    break;
            }
        }

        /// <summary>
        /// set value of designated control
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="iEditCtlType"></param>
        private void InitializeEditingControls( Control oCtl, mpControlTypes iEditCtlType, 
                                                string xPropertyName)
        {
            //load existing grid data table with parsed value string

            char cSep = LMP.StringArray.mpEndOfSubField;

            switch (iEditCtlType)
            {
                case mpControlTypes.AuthorSelector:
                case mpControlTypes.DetailGrid:
                case mpControlTypes.ClientMatterSelector:
                case mpControlTypes.SalutationCombo:
                case mpControlTypes.RelineGrid:
                case mpControlTypes.Checkbox:
                case mpControlTypes.List:
                case mpControlTypes.DetailList:
                case mpControlTypes.SegmentChooser:
                case mpControlTypes.DateCombo:
                    break;
                case mpControlTypes.MultilineCombo:
                case mpControlTypes.MultilineTextbox:
                case mpControlTypes.Spinner:
                case mpControlTypes.Textbox:
                case mpControlTypes.JurisdictionChooser:
                    ((IControl)oCtl).Value = m_xValue;  
                    break;
                case mpControlTypes.Combo:
                    LMP.Controls.ComboBox oCombo = (LMP.Controls.ComboBox)oCtl;
                    //user may have entered manual input

                    if (xPropertyName != "LabelType")
                    {
                        if (oCombo.LimitToList)
                            ((IControl)oCtl).Value = m_xValue;
                        else
                        {
                            ((IControl)oCtl).Value = m_xValue;
                            oCombo.Text = m_xValue;
                        }
                    }
                    else
                    {
                        ((IControl)oCtl).Value = m_xValue;
                        oCombo.LimitToList = false;
                        oCombo.Text = m_xValue;
                        //reset limit to list
                        oCombo.LimitToList = true;
                    }
                    break;
                case mpControlTypes.None:
                    //handle non LMP.Controls
                    switch ((ConfigurationControlTypes)oCtl.Tag)
                    {
                        case ConfigurationControlTypes.LocationChooser:
                            //JTS 1/30/09: Only set value if it's a numeric (not an expression)
                            if (String.IsNumericInt32(m_xValue))
                                ((IControl)oCtl).Value = m_xValue;  
                            break;
                        case ConfigurationControlTypes.DetailGridConfigStringGrid:
                            //load existing grid data table with parsed value string

                            //validate the input string -- if it contains wrong
                            //number of fields, message user
                            bool bValid = ValidatePropertyValue
                                (ConfigurationControlTypes.DetailGridConfigStringGrid);

                            //user has chosen to return to the control property grid
                            //cancel the form
                            if (!bValid)
                            {
                                this.CancelButton = this.btnCancel;
                                SendKeys.Send("{ESC}");
                                return;
                            }
                            
                            DataGridView oGrid = (DataGridView)oCtl;
                            DataTable oDT = (DataTable)oGrid.DataSource;
                            oDT.Rows.Clear();

                            //add default row for editing if initial value == ""
                            if (m_xValue == "")
                            {
                                AddDefaultRow(oDT);
                                break;
                            }

                            //since this config string doesn't have a record separator, we'll iterate
                            //through the array based on column count and an offset incremented at
                            //each set of n == columns.count - 1.  note there is an extra column in 
                            //the grid - aValueItems[4] can either be assigned to column[4] or column[5]
                            //depending on value of iType
                            int iC = 0;
                            DetailGridFieldTypes iType = DetailGridFieldTypes.None;
                            string[] xValueItems = m_xValue.Split(cSep);

                            for (int i = 0; i < Math.Min(xValueItems.Length / (oDT.Columns.Count - 1), 12); i++) //GLOG 5134
                            {
                                string xList = "";
                                string xLines = "";
                                iType = (DetailGridFieldTypes)Int32.Parse(xValueItems[0 + iC]);
                                switch (iType)
                                {
                                    case DetailGridFieldTypes.TextBox:
                                        xLines = xValueItems[4 + iC];
                                        break;
                                    case DetailGridFieldTypes.ComboBox:
                                    case DetailGridFieldTypes.ListBox:
                                    case DetailGridFieldTypes.MultiLineCombo:
                                        xList = xValueItems[4 + iC];
                                        break;
                                    default:
                                        break;
                                }
                                oDT.Rows.Add(xValueItems[0 + iC], ConvertFromXML(xValueItems[1 + iC]),
                                    ConvertFromXML(xValueItems[2 + iC]), xValueItems[3 + iC], xList, xLines);
                                
                                iC = iC + (oDT.Columns.Count - 1);
                            }
                            //GLOG 5134: Prevent addition of more properties than supported by DetailGrid control
                            oGrid.AllowUserToAddRows = oGrid.Rows.Count <= 12;
                            break;
                        case ConfigurationControlTypes.RelineGridConfigStringTextBoxes:
                            //load two text boxes with parsed value string
                            xValueItems = m_xValue.Split(cSep);

                            //load the value items into the two textboxes
                            Control oTB1 = this.Controls["txtRelineConfig1"];
                            Control oTB2 = this.Controls["txtRelineConfig2"];
                            oTB1.Text = ConvertFromXML(xValueItems[0]);
                            oTB2.Text = ConvertFromXML(xValueItems[1]);  
                            break;
                        case ConfigurationControlTypes.SalutationSchemaGrid:
                            //load existing grid data table with parsed value string
                            xValueItems = m_xValue.Split(cSep);

                            oGrid = (DataGridView)oCtl;
                            oDT = (DataTable)oGrid.DataSource;
                            oDT.Rows.Clear();
                            //load grid with parsed values
                                for (int i = 0; i < xValueItems.Length; i++)
                                {
                                    oDT.Rows.Add(xValueItems[i]);
                                }
                            break;
                        case ConfigurationControlTypes.BooleanCombo:
                        case ConfigurationControlTypes.DropdownList:
                        case ConfigurationControlTypes.CheckedListBox:
                        case ConfigurationControlTypes.ListCombo:
                        case ConfigurationControlTypes.ExpressionTextBox:
                            ((IControl)oCtl).Value = m_xValue;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// validates input value for designated configuration control type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        private bool ValidatePropertyValue(ConfigurationControlTypes iType)
        {
            bool bRet = false;

            switch (iType)
            {
                case ConfigurationControlTypes.DetailGridConfigStringGrid:
                    if (m_xValue == "")
                    {
                        bRet = true;
                        break;
                    }

                    string[] xValueItems = m_xValue.Split(LMP.StringArray.mpEndOfSubField);

                    if (xValueItems.Length % (grdProperties.Columns.Count - 1) != 0)
                    {
                        string xMsg = LMP.Resources.GetLangString("Dialog_ControlPropertyDetailGridConfigStringInvalid");
                        DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                        switch (oDR)
                        {
                            case DialogResult.Cancel:
                            case DialogResult.No:
                                break;
                            case DialogResult.Yes:
                                bRet = true;
                                m_xValue = "";
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        bRet = true;
                    break;
                default:
                    break;
            }
            return bRet;
        }

        /// <summary>
        /// sets up grid for editing if initial value is empty or
        /// rows have been cleared via right click menu
        /// </summary>
        /// <param name="oDT"></param>
        private void AddDefaultRow(DataTable oDT)
        {
            if (oDT.Columns.Count == 1)
            {
                oDT.Rows.Add();
            }
            else
            {
                oDT.Rows.Add("1", "", "", "True", "", "1");
            }
            oDT.AcceptChanges();
        }
        /// <summary>
        /// clears values, sets specified cells read only prop
        /// for specified iType
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="oListCell"></param>
        /// <param name="oLinesCell"></param>
        /// <param name="oCICell"></param>
        private void ConfigureComboCells(DetailGridFieldTypes iType, DataGridViewComboBoxCell oListCell, 
                            DataGridViewComboBoxCell oLinesCell, DataGridViewComboBoxCell oCICell)
        {
            switch (iType)
            {
                case DetailGridFieldTypes.TextBox:
                    //textbox - clear lists field lookup
                    oListCell.Value = "";
                    if (oCICell.Value == null || oCICell.Value.ToString() == "")
                        oCICell.Value = "True";
                    if (oLinesCell.Value == null || oLinesCell.Value.ToString() == "")
                        oLinesCell.Value = "1";
                    oLinesCell.ReadOnly = false;
                    oListCell.ReadOnly = true;
                    oCICell.ReadOnly = false;
                    break;
                case DetailGridFieldTypes.ListBox:
                case DetailGridFieldTypes.ComboBox:
                case DetailGridFieldTypes.MultiLineCombo:
                    //listboxes - clear lines field lookup
                    oLinesCell.Value = "";
                    if (oCICell.Value == null || oCICell.Value.ToString() == "")
                        oCICell.Value = "True";
                    oLinesCell.ReadOnly = true;
                    oListCell.ReadOnly = false;
                    oCICell.ReadOnly = false;
                    break;
                case DetailGridFieldTypes.BooleanCombo:
                    //boolean combo - clear three lookups
                    oLinesCell.Value = "";
                    oListCell.Value = "";
                    oCICell.Value = "";
                    oLinesCell.ReadOnly = true;
                    oListCell.ReadOnly = true;
                    oCICell.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// creates a DataGridViewComboBoxColumn object for insertion into grid
        /// </summary>
        /// <param name="xDataPropName"></param>
        /// <param name="iWidth"></param>
        /// <param name="iMaxDropdownItems"></param>
        /// <returns></returns>
        private DataGridViewComboBoxColumn CreateComboBoxColumn(string xDataPropName, 
                            int iWidth, int iMaxDropdownItems)
        {
            DataGridViewComboBoxColumn oCol = new DataGridViewComboBoxColumn();

            oCol.DataPropertyName = xDataPropName;
            oCol.Width = iWidth;
            oCol.MaxDropDownItems = iMaxDropdownItems;
            oCol.FlatStyle = FlatStyle.Flat;
            oCol.DropDownWidth = iWidth + 60;
            oCol.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
            LoadDataGridViewCombo(oCol, null, xDataPropName);
            return oCol;
        }
        /// <summary>
        /// loads designated lookup combo with items
        /// </summary>
        /// <param name="oCol"></param>
        /// <param name="xDataPropName"></param>
        private void LoadDataGridViewCombo(DataGridViewComboBoxColumn oCol, DataGridViewComboBoxCell oCell,  string xDataPropName)
        {
            DataTable oDT = new DataTable();

            switch (xDataPropName)
            {
                case "ControlType":
                    oDT.Columns.Add("ControlName");
                    oDT.Columns.Add("ControlID");
                    string[,] aProps = GetPropertySettingsList(xDataPropName);
                    for (int i = 0; i <= aProps.GetUpperBound(0); i++)
                    {
                        oDT.Rows.Add(aProps[i, 0], aProps[i, 1]);
                    }
                    oCol.DataSource = oDT;
                    oCol.ValueMember = "ControlID";
                    oCol.DisplayMember = "ControlName";
                    break;
                case "CIEnabled":
                case "LinesPerField":
                case "ListName":
                    //boolean lookup for DetailGrid ConfigString
                    oDT.Columns.Add("Values");
                    aProps = GetPropertySettingsList(xDataPropName);
                    for (int i = 0; i <= aProps.GetUpperBound(0); i++)
                    {
                        oDT.Rows.Add(aProps[i, 0]);
                    }

                    if (oCell != null)
                    {
                        oCell.DataSource = oDT;
                        oCell.ValueMember = "Values";
                        oCell.DisplayMember = "Values";
                    }
                    else
                    { 
                        oCol.DataSource = oDT;
                        oCol.ValueMember = "Values";
                        oCol.DisplayMember = "Values";
                    }
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// constructs valid value strings for various configuration control types
        /// </summary>
        /// <returns></returns>
        private string GetValue()
        {
            string xSep = LMP.StringArray.mpEndOfSubField.ToString();
            StringBuilder oSB = new StringBuilder();

            //the tag can either be an LMP.Data.ControlType or 
            //this.ConfigurationControlType
            switch ((mpControlTypes)m_oCtl.Tag)
            {
                case mpControlTypes.None:
                case mpControlTypes.AuthorSelector:
                case mpControlTypes.DetailGrid:
                case mpControlTypes.ClientMatterSelector:
                case mpControlTypes.SalutationCombo:
                case mpControlTypes.RelineGrid:
                case mpControlTypes.Checkbox:
                case mpControlTypes.List:
                case mpControlTypes.DetailList:
                case mpControlTypes.DropdownList:
                case mpControlTypes.SegmentChooser:
                case mpControlTypes.DateCombo:
                    break;
                case mpControlTypes.MultilineTextbox:
                case mpControlTypes.Textbox:
                case mpControlTypes.Spinner:
                case mpControlTypes.Combo:
                case mpControlTypes.MultilineCombo:
                    return ((IControl)m_oCtl).Value;
                default:
                    break;
            }

            switch ((ConfigurationControlTypes)m_oCtl.Tag)
            {
                case ConfigurationControlTypes.LocationChooser:
                    return ((LocationChooser)m_oCtl).Value;  
                case ConfigurationControlTypes.ListCombo:
                    return ((ListCombo)m_oCtl).Text;
                case ConfigurationControlTypes.SalutationSchemaGrid:
                case ConfigurationControlTypes.DateComboFormatsGrid:
                    DataTable oDT = (DataTable)grdProperties.DataSource;
                    for (int i = 0; i < oDT.Rows.Count; i++)
                    {
                        for (int j = 0; j < oDT.Rows[i].ItemArray.Length; j++)
                        {
                            //replace hotkey indicator w/ detail XML version
                            string xValue = oDT.Rows[i].ItemArray.GetValue(j).ToString();
                            oSB.AppendFormat("{0}{1}", xValue, xSep);
                        }
                    }
                    //trim new prop string
                    return oSB.ToString(0, oSB.Length - 1);
                case ConfigurationControlTypes.DetailGridConfigStringGrid:
                    oDT = (DataTable)grdProperties.DataSource;
                    for (int i = 0; i < oDT.Rows.Count; i++)
                    {
                        for (int j = 0; j < oDT.Rows[i].ItemArray.Length; j++)
                        {
                            string xValue = oDT.Rows[i].ItemArray.GetValue(j).ToString();
                            
                            //config string field 4 contains either grid field 4 or 5
                            //depending on the detail grid field type selected

                            DetailGridFieldTypes  iType =  
                                    (DetailGridFieldTypes) Int32.Parse(oDT.Rows[i].
                                    ItemArray.GetValue(0).ToString());
                            
                            bool bSkip = false;

                            //load values according to control type
                            switch (iType)
                            {
                                case DetailGridFieldTypes.TextBox:
                                    //textbox - skip listname field
                                    if (j == 4)
                                        bSkip = true;
                                    break;
                                case DetailGridFieldTypes.BooleanCombo:
                                    //boolean combo - field 4 & 5 always empty
                                    if (j == 4)
                                        bSkip = true;
                                    else if (j == 5)
                                        xValue = "";
                                    break;
                                case DetailGridFieldTypes.ComboBox:
                                case DetailGridFieldTypes.ListBox:
                                case DetailGridFieldTypes.MultiLineCombo:
                                    //remaining controls receive list name from
                                    //field 4
                                    if (j == 5)
                                        bSkip = true;
                                    break;
                                default:
                                    break;
                            }

                            if (!bSkip)
                            {
                                if (xValue.IndexOf("&") != -1)
                                {
                                    if (xValue.IndexOf("&amp;") == -1)
                                        xValue = xValue.Replace("&", "&amp;");
                                }

                                oSB.AppendFormat("{0}{1}", xValue, xSep);
                            }
                        }
                    }

                    //trim new prop string
                    return oSB.ToString(0, Math.Max(oSB.Length - 1, 0));
                case ConfigurationControlTypes.RelineGridConfigStringTextBoxes:
                    string xTab1 = ConvertToXML(this.Controls["txtRelineConfig1"].Text);
                    string xTab2 = ConvertToXML(this.Controls["txtRelineConfig2"].Text);
                    oSB.AppendFormat("{0}{1}{2}", xTab1, xSep, xTab2);
                    return oSB.ToString();
                case ConfigurationControlTypes.CheckedListBox:
                case ConfigurationControlTypes.DropdownList: 
                case ConfigurationControlTypes.mpControl:
                case ConfigurationControlTypes.BooleanCombo:
                case ConfigurationControlTypes.ExpressionTextBox:
                    return ((IControl)m_oCtl).Value;
                default:
                    return null;
            }
        }
        /// <summary>
        /// adds DataGridViewButtons to grid
        /// </summary>
        private void CreateButtonColumn()
        {
            DataGridViewButtonColumn oButtons = new DataGridViewButtonColumn();
            {
                oButtons.HeaderText = "";
                oButtons.UseColumnTextForButtonValue = false;
                oButtons.AutoSizeMode =
                        DataGridViewAutoSizeColumnMode.NotSet;
                oButtons.FlatStyle = FlatStyle.Standard;
                oButtons.CellTemplate.Style.BackColor =
                        Color.FromKnownColor(KnownColor.ButtonFace);
                oButtons.Text = "...";
                oButtons.UseColumnTextForButtonValue = true;
            }
            //set button width
            this.grdProperties.Columns.Insert(1, oButtons);
            this.grdProperties.Columns[1].Width = 23;
        }
        /// <summary>
        /// provides storage for DataGridViewCombo selections
        /// </summary>
        /// <param name="xDataPropName"></param>
        /// <returns></returns>
        private string[,] GetPropertySettingsList(string xDataPropName)
        {
            switch (xDataPropName)
            {
                case "AssignedObjectType":
                    //this corresponds to the enum contained in DetailGrid control
                    return new string[,] {  {"Bates Labels", "10"},
                                            {"Agreement", "11"},
                                            {"Agreement Title Page", "515"},
                                            {"Depo Summary", "9"},
                                            {"Trailer", "401"},
                                            {"Envelope", "402"},
                                            {"Fax", "3"},
                                            {"Labels", "403"},
                                            {"Letter", "1"},
                                            {"Letterhead", "404"},
                                            {"Letter Signature", "509"},
                                            {"Litigation Back","643"},    //GLOG 7154
                                            {"Memo", "2"},
                                            {"Notary", "7"},
                                            {"Pleading", "5"},
                                            {"Pleading Caption", "500"},
                                            {"Pleading Caption Border Set", "504"},
                                            {"Pleading Captions", "506"},
                                            {"Pleading Counsel", "501"},
                                            {"Pleading Counsels", "507"},
                                            {"Pleading Cover Page", "505"},
                                            {"Pleading Paper", "503"},
                                            {"Pleading Signature", "502"},
                                            {"Pleading Signatures", "508"},
                                            {"Service", "6"},
                                            {"Verification", "8"}}; 

                case "ControlType":
                    //this corresponds to the enum contained in DetailGrid control
                    return new string[,] {  {"Text Box", "1"},
                                            {"Combo Box", "2"},
                                            {"List Box", "3"}, 
                                            {"Multiline Combo", "4"},
                                            {"Boolean Combo", "5"}};
                case "ValueField":
                    //this corresponds to the mpLocationFields enum contained in LocationsDropdown control
                    return new string[,] {  {"Name", "1"},
                                            {"ID", "2"}};
                case "LocationType":
                    //this corresponds to the mpLocationTypes enum contained in LocationsDropdown control
                    return new string[,] {  {"Countries", "1"},
                                            {"States", "2"},
                                            {"Counties", "3"}};
                case "CIEnabled":
                    return new string[,] {  {"True", ""},
                                            {"False", ""}};

                case "DefaultTab":
                    return new string[,] {  {"Tab 1", "0"},
                                            {"Tab 2", "1"}};

                case "Style":
                    return new string[,] {  {"Show Reline Only", "0"},
                                            {"Show Special Only", "1"},
                                            {"Show Both", "2"}};
                
                case "CounterScheme":

                    return new string[,] {  {"Item %0 of %1", "Item %0 of %1"},
                                            {"Item %0", "Item %0"},
                                            {"Total Items: %1", "Total Items: %1"},
                                            {"%1 Items", "%1 Items"},
                                            {"Editing %0", "Editing %0"}};

                case "Separator":
                    return new string[,] {  {"Return", "1310"},
                                            {"Soft Return", "11"},
                                            {"Tab", "9"}};

                case "OnNotInList":
                    return new string[,] {  {"Raise Error", "1"},
                                            {"Select First In List", "2"},
                                            {"Do Not Select Font", "3"}};

                case "SelectionType":
                    return new string[,] {  {"Content", "0"},
                                            {"End of Salutation", "1"},
                                            {"Start of Salutation", "2"}};

                case "Formats":
                    string[] aFormats = LMP.Data.Application.GetDateFormats();

                    //convert formats array to 2 col array w/ formatted date, format string
                    return CreateDateList(aFormats, true, true);

                case "CustomDateFormat":
                    //formats available for configuration of date picker
                    aFormats = LMP.Data.Application.GetDateFormats();

                    //convert formats array to 2 col array w/ formatted date, format string
                    return CreateDateList(aFormats, false, false);

                case "Mode":
                    return new string[,] {  {"Literal", "Literal"},
                                            {"Format", "Format"}};

                case "FilterOptions":
                    return new string[,] {  {"All People", "0"},
                                            {"Authors", "1"},
                                            {"Attorneys", "2"},
                                            {"Users", "3"}};

               case "DisplayUnit":
                    return new string[,] {  {"Inches", "Inches"},
                                            {"Centimeters", "Centimeters"},
                                            {"Millimeters", "Millimeters"},
                                            {"Points", "Points"},
                                            {"Picas", "Picas"}};

                case "LinesPerField":
                    string[,] aNums = new string[10, 2];
                    for (int i = 1; i <= 10; i++)
			        {
                        aNums[i - 1, 0] = i.ToString();
                        aNums[i - 1, 1] = "";
			        }
                    return aNums;
                case "ListName":
                    return GetLMPListsList();
                case "AuthorListType":
                    return new string[,] {  {"All Authors", "0"},
                                            {"Attorneys", "1"},
                                            {"Non-Attorneys", "2"}};
                case "DisplayMode":
                    return new string[,] { {"Jurisdictions", "0"},
                                           {"Locations", "1"}};
                case "LabelType":
                    string xSep = LMP.StringArray.mpEndOfSubField.ToString();

                    return new string[,] { {"Avery 5160/5260/etc. (3 columns x 10 rows, 1 x 2.63)", "Avery 5160" + xSep + "3" + xSep + "10"},
                                           {"Avery 5161/5261/etc. (2 columns x 10 rows, 1 x 4)", "Avery 5161" + xSep + "2"+ xSep + "10"},
                                           {"Avery 5162/5262/etc. (2 columns x 7 rows, 1.33 x 4)", "Avery 5162" + xSep + "2"+ xSep + "7"},
                                           {"Avery 5163/5263/etc. (2 columns x 5 rows, 2 x 4)", "Avery 5163" + xSep + "2"+ xSep + "5"},
                                           {"Avery 5164/5264/etc. (2 columns x 3 rows, 3.33 x 4)", "Avery 5164" + xSep + "2"+ xSep + "3"},
                                           {"Avery 5162/5262/etc. (2 columns x 7 rows, 1.33 x 4)", "Avery 5162" + xSep + "2"+ xSep + "7"},
                                           {"Avery 5066/5266/etc. (2 columns x 15 rows, .5 x 3)", "Avery 5266" + xSep + "2"+ xSep + "15"},
                                           {"Avery 5167/5267/etc. (4 columns x 20 rows, .5 x 1.75)", "Avery 5167" + xSep + "4"+ xSep + "20"},
                                           {"Avery 5160/5260/etc. (3 columns x 10 rows, 1 x 2.63)", "Avery 5160" + xSep + "3"+ xSep + "10"},
                                           {"Avery 5662 (2 columns x 7 rows, 1.33 x 4.25)", "Avery 5662" + xSep + "2"+ xSep + "7"},
                                           {"Avery 5663 (2 columns x 5 rows, 2 x 4.25)", "Avery 5663" + xSep + "2"+ xSep + "5"},
                                           {"Other (1 column x 1 row)", "Other" + xSep +"1" + xSep + "1"}};
                case "NameValueSeparator":
                    return new string[,] { {"Tab", "0"},
                                           {"Spaces", "1"}};
                case "SelectionMode": //GLOG 6235
                    return new string[,] {  {"Select All", "0"},
                                            {"Select End of Text", "1"},
                                            {"Select Start of Text", "2"}};

                default:
                    return new string[0, 0];
            }
        }
        /// <summary>
        /// returns 2 column array, display column = current date in formats specified in
        /// designated array of date formats
        /// </summary>
        /// <param name="aList"></param>
        private string[,] CreateDateList(string[] aFormats, bool bIncludeFieldFormats, bool bIncludeJuratFormats)
        {
            string[,] aDateFormats = new string[aFormats.Length, 2];
            ArrayList aUniqueKeys = new ArrayList();
            ArrayList aDupeKeys = new ArrayList();
            DateTime oDT = DateTime.Now;
            int i = 0;

            //GLOG 6450:  If two or more formats resolve to the same text, 
            //add format to the List text to differentiate
            foreach(string xFormat in aFormats)
            {
                string xDateText = LMP.String.EvaluateDateTimeValue(xFormat, oDT);
                if (!aUniqueKeys.Contains(xDateText))
                {
                    aUniqueKeys.Add(xDateText);
                }
                else if (!aDupeKeys.Contains(xDateText))
                {
                    aDupeKeys.Add(xDateText);
                }
            }
            foreach(string xFormat in aFormats)
            {
                //skip jurat formats and field code formats if specified
                if ((!bIncludeFieldFormats && xFormat.Contains("/F")) ||
                    !bIncludeJuratFormats && xFormat.Contains("/J"))
                    continue;
                string xDateText = LMP.String.EvaluateDateTimeValue(xFormat, oDT);
                if (aDupeKeys.Contains(xDateText))
                {
                    xDateText = xDateText + "  [" + xFormat + "]";
                }
                aDateFormats[i, 0] = xDateText;
                aDateFormats[i, 1] = xFormat;
                i++;
            }
            return aDateFormats;
        }
        /// <summary>
        /// provides storage for CheckedListBox control
        /// </summary>
        /// <param name="xDataPropertyName"></param>
        /// <returns></returns>
        private object[,] GetBitwiseList(string xDataPropertyName)
        {
            switch (xDataPropertyName)
            {
                case "FormatOptions":
                case "FormatValues":
                    return new object[,] {  {"Reline Bold", 1}, 
                                            {"Reline Italic", 2}, 
                                            {"Reline Underline", 4}, 
                                            {"Reline Underline Last Line", 8}, 
                                            {"Special Bold", 16}, 
                                            {"Special Italic", 32}, 
                                            {"Special Underline", 64},
                                            {"Special Underline Last Line", 128}}; 
           
                case "ShowColumns":
                    return new object[,] {  {"Signer", 1}, 
                                            {"Display Name", 2}, 
                                            {"License/Bar ID", 4}}; 

                case "TabStopColumns":
                    return new object[,] {  {"Grid Column 1", 1},
                                            {"Grid Column 2", 2}};
                default:
                    return new object[0, 0];
            }
        }
        /// <summary>
        /// returns string array of LMP List names, IDs
        /// </summary>
        /// <returns></returns>
        private string[,] GetLMPListsList()
        {
            //load list of value set lists lists
            Lists oLists = LMP.Data.Application.GetLists(true);
            string[,] aLists = new string[oLists.Count, 2];

            for (int i = 1; i <= oLists.Count; i++)
            {
                List oList = (List)oLists[i];
                aLists[i - 1, 0] = oList.Name;
                aLists[i - 1, 1] = oList.ID.ToString();
            }
            return aLists;
        }
        /// <summary>
        /// handle escaped version of ampersand
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        private string ConvertFromXML(string xInput)
        {
            if (xInput.IndexOf("&") != -1)
            {
                if (xInput.IndexOf("&amp;") != -1)
                    xInput = xInput.Replace("&amp;", "&");
            }
            return xInput;
        }
        /// <summary>
        /// converts ampersand entered in control to escaped version
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        private string ConvertToXML(string xInput)
        {
            if (xInput.IndexOf("&") != -1)
            {
                if (xInput.IndexOf("&amp;") == -1)
                    xInput = xInput.Replace("&", "&amp;");
            }
            return xInput;
        }
        #endregion
        #region ***************************event handlers************************************
        /// <summary>
        /// handle data errors from DataGridView when when combo cell values don't match
        /// combo list contents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
        /// <summary>
        /// displays context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                    this.mnuPopup.Show(grdProperties, new Point(e.X, e.Y));
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// sets flag for detail config grid tab key processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_TabPressed(object sender, TabPressedEventArgs e)
        {
            try
            {
                m_bShiftPressed = e.ShiftPressed;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// handles pressing enter on button column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_EnterKeyPressed(object sender, EnterKeyPressedEventArgs e)
        {
            try
            {
                if (grdProperties.CurrentCellAddress.X == 0)
                {   //enter key pressed with focus on button -- call expression builder
                    DataGridViewCell oTargetCell = this.grdProperties.Rows
                                [grdProperties.CurrentRow.Index].Cells[1];
                    UpdateCellWithExpression(oTargetCell);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// allow tabbing through read only cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        private void grdProperties_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!grdProperties.CurrentCell.ReadOnly)
                    return;

                if ((int)e.KeyChar == 9)
                {
                    Point oCurAddress = (Point)grdProperties.CurrentCellAddress;
                    DataGridViewRow oCurRow = grdProperties.CurrentRow;
                    DataGridViewCell oNewCell = null;
                    int iInc = 1;

                    //shift pressed - navigate left
                    if (m_bShiftPressed)
                    {
                        iInc = -1;
                        m_bShiftPressed = false;
                        oNewCell = oCurRow.Cells[oCurAddress.X + iInc];
                    }
                    else
                    {
                        //check if we are at the last cell in the row
                        if (oCurAddress.X == oCurRow.Cells.Count - 1)
                        {
                            if (oCurRow.Index == 11)
                                return;
                            if (oCurRow.Index + 1 == grdProperties.Rows.Count)
                            {
                                oNewCell = grdProperties.Rows[grdProperties.Rows.Count - 1].Cells[0];
                            }
                            else
                            {
                                oNewCell = grdProperties.Rows[oCurRow.Index + 1].Cells[0];
                            }
                        }
                        else
                        {
                            oNewCell = oCurRow.Cells[oCurAddress.X + iInc];
                        }
                    }
                    //set new current cell
                    grdProperties.CurrentCell = oNewCell;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles value setting when navigating through grid with
        /// combobox cells - only subscribed to by detail config string grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bInitializing)
                    return;
                
                if (grdProperties.CurrentCellAddress.X > 0)
                    return;

                //toggle selected index to force combo value to refresh - this will
                //automatically add a row when leaving the control type cell 
                System.Windows.Forms.ComboBox oCombo = 
                    (System.Windows.Forms.ComboBox)grdProperties.EditingControl;
                int iCurIndex = oCombo.SelectedIndex;
                //set flag -- we don't want combo selected index changed handler to run twice
                m_bIgnoreComboSelIndex = true;
                oCombo.SelectedIndex = 1;
                m_bIgnoreComboSelIndex = false;
                oCombo.SelectedIndex = iCurIndex;
                grdProperties.AllowUserToAddRows = grdProperties.Rows.Count <=12; //GLOG 5134: Heading row is index 0
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// subscribe to combo selected index changed event
        /// if column 1 value changes
        /// disable lookup cells according to field type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_EditingControlShowing(object sender,
                        DataGridViewEditingControlShowingEventArgs e)
        {

            DetailGridFieldTypes  iFieldType = DetailGridFieldTypes.None;
            DataGridViewCell oCell = null;
            int iColIndex = -1;
            try
            {
                //ensure we only display our context menu - this will
                //suppress native context menu in text box cells of grid
                e.Control.ContextMenuStrip = this.mnuPopup;

                //return if only two columns showing - grid configured
                //for Salutation Combo schema property in that case
                if (grdProperties.Columns.Count == 2)
                    return;

                //initialize module level combo variable when event fires on initialization
                //note that event fires when form is shown by caller; therefore
                //we have to reset the initialization flag here, rather than in the 
                //constructor
                if (m_bInitializing)
                {
                    m_oCombo = (System.Windows.Forms.ComboBox)e.Control;
                    m_bInitializing = false;
                }
                //attempt to gather various indices
                iFieldType = (DetailGridFieldTypes)Int32.Parse
                            (grdProperties.CurrentRow.Cells[0].Value.ToString());
                oCell = grdProperties.CurrentCell;
                iColIndex = oCell.ColumnIndex;
            }
            catch { return; }
            try
            {
                //set cell to read only depending on the detail field type
                switch (iFieldType)
                {
                    case DetailGridFieldTypes.TextBox:
                        if (iColIndex == 4)
                            oCell.ReadOnly = true;
                        else if (iColIndex == 1 && grdProperties.Columns[1].Name == "DetailFieldName")
                            //Force all caps for Field Name to match CI format
                            ((DataGridViewTextBoxEditingControl)oCell.DataGridView.EditingControl).CharacterCasing = CharacterCasing.Upper;
                        else
                            try
                            {
                                ((DataGridViewTextBoxEditingControl)oCell.DataGridView.EditingControl).CharacterCasing = CharacterCasing.Normal;
                            }
                            catch { }
                        break;
                    case DetailGridFieldTypes.ComboBox:
                    case DetailGridFieldTypes.ListBox: 
                    case DetailGridFieldTypes.MultiLineCombo:
                        //list boxes
                        if (iColIndex == 5)
                            oCell.ReadOnly = true;
                        break;
                    case DetailGridFieldTypes.BooleanCombo:
                        //boolean combo
                        if (iColIndex > 2)
                            oCell.ReadOnly = true;
                        break;
                    default:
                        break;
                }
                
                //subscribe to combo.SelectedIndexChanged only if column 0 combo selected
                if (iColIndex == 0)
                {
                    m_oCombo = (System.Windows.Forms.ComboBox)e.Control;
                    //make sure we don't create multiple subscriptions
                    m_oCombo.SelectedIndexChanged -= grdPropertiesComboBox_SelectedIndexChanged;
                    m_oCombo.SelectedIndexChanged += new EventHandler(grdPropertiesComboBox_SelectedIndexChanged);
                }
                else
                {
                    m_oCombo.SelectedIndexChanged -= grdPropertiesComboBox_SelectedIndexChanged;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles combo selected index changed event -- subscribed to only if
        /// column 0 entered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdPropertiesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (grdProperties.CurrentCellAddress.X > 0 || m_bIgnoreComboSelIndex == true)
                    return;

                m_bIgnoreComboSelIndex = true;
                //get dropdown value and current row index for grid
                System.Windows.Forms.ComboBox oCombo = ((System.Windows.Forms.ComboBox)sender);

                DataTable oDT = (DataTable)oCombo.DataSource;
                if (oDT == null)
                    return;

                int iComboIndex = oCombo.SelectedIndex;

                if (iComboIndex == -1)
                    return;

                DetailGridFieldTypes iType = (DetailGridFieldTypes)Int32.Parse(oDT.Rows[iComboIndex]
                                                .ItemArray.GetValue(1).ToString());

                //configure remaining cells according to control type
                DataGridViewComboBoxCell oListCell = (DataGridViewComboBoxCell)
                                                        grdProperties.CurrentRow.Cells[4];
                DataGridViewComboBoxCell oLinesCell = (DataGridViewComboBoxCell)
                                                         grdProperties.CurrentRow.Cells[5];
                DataGridViewComboBoxCell oCICell = (DataGridViewComboBoxCell)
                                                         grdProperties.CurrentRow.Cells[3];

                ConfigureComboCells(iType, oListCell, oLinesCell, oCICell);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bIgnoreComboSelIndex = false;
            }
        }
        /// <summary>
        /// subscribed to only if data grid view button created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    DataGridViewCell oTargetCell = this.grdProperties.Rows[e.RowIndex].Cells[1];
                    UpdateCellWithExpression(oTargetCell);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// calls expression builder, passing in cell contents, updates cell
        /// with new expression
        /// </summary>
        /// <param name="oTargetCell"></param>
        private void UpdateCellWithExpression(DataGridViewCell oTargetCell)
        {
            //show expression builder; update if appropriate
            ExpressionBuilder oEB = new ExpressionBuilder();
            oEB.Expression = oTargetCell.Value.ToString();

            if (oEB.ShowDialog(this) == DialogResult.OK)
            {
                DataTable oDT = (DataTable)grdProperties.DataSource;
                if (oDT.Rows.Count == oTargetCell.RowIndex)
                {
                    //expression builder called from bottom row -- add new row to DataTable
                    //reset current cell, then delete "extra" row
                    oDT.Rows.Add(oEB.Expression);
                    grdProperties.CurrentCell = oTargetCell;
                    oDT.Rows.RemoveAt(oDT.Rows.Count - 1);
                }
                else
                {
                    //expression builded called from existing row
                    oTargetCell.Value = oEB.Expression;
                    grdProperties.CurrentCell = oTargetCell;
                }
                //position cursor after text
                System.Windows.Forms.TextBox oTB = (System.Windows.Forms.TextBox)grdProperties.EditingControl;
                oTB.SelectionLength = 0;
                oTB.SelectionStart = oTB.Text.Length;
            }
        }
        /// <summary>
        /// provides default values for new grid row -- subscribed to only for
        /// Detail config string version of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            //these are default values for new row
            e.Row.Cells[0].Value = "1";
            e.Row.Cells[3].Value = "True";
            e.Row.Cells[5].Value = "1";
        }
        /// <summary>
        /// sets menu captions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPopup_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                this.mnuPopup_DeleteAll.Text = LMP.Resources.
                                        GetLangString("Dialog_ControlPropertyPopupDeleteAll");
                this.mnuPopup_DeleteRow.Text = LMP.Resources.
                                        GetLangString("Dialog_ControlPropertyPopupDeleteRow");
                this.mnuPopup_MoveUp.Text = LMP.Resources.
                                        GetLangString("Menu_AuthorSelector_MoveUp");
                this.mnuPopup_MoveDown.Text = LMP.Resources.
                                        GetLangString("Menu_AuthorSelector_MoveDown");
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// deletes a single row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPopup_DeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdProperties.CurrentRow == null)
                    return;
                DataTable oDT = (DataTable)grdProperties.DataSource;
                if (oDT.Rows.Count >= grdProperties.CurrentRow.Index)
                {
                    oDT.Rows.RemoveAt(grdProperties.CurrentRow.Index);
                    //reset current cell to give focus to grid if last row has been deleted
                    if (oDT.Rows.Count == 0)
                    {
                        AddDefaultRow(oDT);
                        grdProperties.CurrentCell = grdProperties.Rows[0].Cells[0];
                    }
                }
            }
            catch (System.IndexOutOfRangeException)
            {
                //handle deletion of row that has not yet been added to data table
                //ie, items added to last row of grid -- send esc twice if text box
                //has contents
                string xEsc = "{ESC}";
                if (grdProperties.EditingControl.GetType().Name == "DataGridViewTextBoxEditingControl" &&
                        grdProperties.CurrentCell.Value.ToString() != "")
                    xEsc = xEsc + xEsc;

                SendKeys.Send(xEsc);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// deletes all rows from grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPopup_DeleteAll_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable oDT = (DataTable)grdProperties.DataSource;
                oDT.Rows.Clear();

                //add a default row for new editing
                AddDefaultRow(oDT);
                
                //set current cell so grid has focus -- grid responds only
                //if current cell is set and then moved to col 0
                if (oDT.Columns.Count > 1)
                    grdProperties.CurrentCell = grdProperties.Rows[0].Cells[1];
                grdProperties.CurrentCell = grdProperties.Rows[0].Cells[0];
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// moves record down in grdProperties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPopup_MoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdProperties.CurrentRow == null)
                    return;

                //commit any changes to grid
                grdProperties.EndEdit();

                DataTable oDT = (DataTable)grdProperties.DataSource;

                int iInd = grdProperties.CurrentRow.Index;

                //create new row for copying
                DataRow oDR = oDT.NewRow();

                if (oDT.Rows.Count > 1 && iInd < oDT.Rows.Count - 1)
                {
                    //copy the new row
                    foreach (DataColumn oCol in oDT.Columns)
                        oDR[oCol.ColumnName] = oDT.Rows[iInd][oCol.ColumnName];

                    //remove the copied row
                    oDT.Rows.RemoveAt(iInd);

                    //insert the copy at the new index
                    oDT.Rows.InsertAt(oDR, iInd + 1);

                    //reset the current row
                    grdProperties.CurrentCell = grdProperties.Rows[iInd + 1].Cells[0];
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// moves record up in grdProperties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuPopup_MoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdProperties.CurrentRow == null)
                    return;
                
                //commit any changes to grid
                grdProperties.EndEdit();
                
                DataTable oDT = (DataTable)grdProperties.DataSource;

                int iInd = grdProperties.CurrentRow.Index;

                //create a new row for copying
                DataRow oDR = oDT.NewRow();

                if (oDT.Rows.Count > 1 && iInd > 0)
                {
                    //copy the current row
                    foreach (DataColumn oCol in oDT.Columns)
                        oDR[oCol.ColumnName] = oDT.Rows[iInd][oCol.ColumnName]; 
                    
                    //remove the copied row
                    oDT.Rows.RemoveAt(iInd);

                    //insert the copy at the new index
                    oDT.Rows.InsertAt(oDR, iInd - 1);
                    
                    //reset the current row
                    grdProperties.CurrentCell = grdProperties.Rows[iInd - 1].Cells[0];
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region ***************************helper classes************************************
        /// <summary>
        /// helper class for access to abstract checkedlistbox class
        /// </summary>
        public class CheckedListBox : LMP.Controls.CheckedListBox
        {
        #region *********************constructors*********************
            public CheckedListBox(object[,] aBitwiseList) : base(aBitwiseList)
            {
            }
            public CheckedListBox(object[,] xList, char cSep) : base(xList, cSep)
            {
            }
        #endregion
        }
        #endregion
    }
}