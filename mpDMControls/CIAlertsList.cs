using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class CIAlertsList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aAlerts;
        #endregion
        #region *********************constructors*********************
        static CIAlertsList()
        {
            m_aAlerts = new object[,]{  {"All Alerts", 1},
                                        {"Multiple Phone Numbers", 2}, 
                                        {"No Phone Numbers", 4},
                                        {"Multiple Fax Numbers", 8},
                                        {"No Fax Numbers", 16},
                                        {"Multiple Email Addresses", 32},
                                        {"No EMail Addresses", 64},
                                        {"No Addresses", 128}};
        }

        public CIAlertsList()
            : base(m_aAlerts)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aAlerts; }
        }
        #endregion
    }
}
