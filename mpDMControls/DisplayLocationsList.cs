using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class DisplayLocationsList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aLocations;
        #endregion
        #region *********************constructors*********************
        static DisplayLocationsList()
        {
            //get array of insertion locations
            m_aLocations = new object[,]{
                {"Document Editor", 1},
                {"Author Preference Manager", 2}, 
                {"Wizard", 4}};
        }

        public DisplayLocationsList()
            : base(m_aLocations)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aLocations; }
        }
        #endregion
    }
}
