using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;
using System.Collections;
using LMP.Architect.Api;

namespace LMP.Controls
{
    public class DefaultLocationComboBox: LMP.Controls.ComboBox
    {
        #region *********************fields*********************
        private static object[,] m_aDefaultLocations;
        #endregion
        #region *********************constructors*********************
        static DefaultLocationComboBox()
        {
            //get array of insertion locations
            m_aDefaultLocations = new object[,]{
                {"Start of Current Section",1}, 
                {"End of Current Section",2}, 
                {"Start of All Sections",4}, 
                {"End of All Sections",8}, 
                {"Selection",16}, 
                {"Start of Document",32}, 
                {"End of Document",64},
                {"New Document",128},
                {"Default Location", 0}};
        }

        // GLOG : 2475 : JAB
        // Populate the combo box with the possible insertion locations for the selected segment.
        public void SetInsertionValues(short sMenuInsertionOptions, bool bStyleSheetOnly, bool bAnswerFileOnly, bool bIsExternalContent)
        {
            ArrayList alLocations = new ArrayList();

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtStartOfCurrentSection) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "Start of Current Section", 1 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtEndOfCurrentSection) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "End of Current Section", 2 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtStartOfAllSections) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "Start of All Sections", 4 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtEndOfAllSections) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "End of All Sections", 8 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtSelection) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                // GLOG : 2475 : JAB
                // Insert at selection should be "At insertion point".
                alLocations.Add(new object[] { LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelection"), 16 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtStartOfDocument) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "Start of Document", 32 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertAtEndOfDocument) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "End of Document", 64 });
            }

            if ((sMenuInsertionOptions & (short)Segment.InsertionLocations.InsertInNewDocument) > 0 &&
                !bStyleSheetOnly &&
                !bAnswerFileOnly &&
                !bIsExternalContent)
            {
                alLocations.Add(new object[] { "New Document", 128 });
            }

            alLocations.Add(new object[] { "Default Location", 0 });

            object[,] aLocations = new object[alLocations.Count, 2];

            for (int i = 0; i < alLocations.Count; i++)
            {
                aLocations[i, 0] = ((object[])alLocations[i])[0];
                aLocations[i, 1] = ((object[])alLocations[i])[1];
            }
            this.SetList(aLocations);
        }

        public DefaultLocationComboBox()
        {
            //GLOG 3413: Prevent blanking control
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.SetList(m_aDefaultLocations);
        }
        #endregion
        #region *********************properties***************************
        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public static object[,] List
        {
            get
            {
                return m_aDefaultLocations;
            }
        }
        #endregion
        #region *********************protected members*********************
        private new int SelectionStart
        {
            get { return this.SelectionStart; }
        }

        private new int SelectionLength
        {
            get { return this.SelectionLength; }
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            // 
            // DefaultLocationComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Margin = new System.Windows.Forms.Padding(48, 19, 48, 19);
            this.Name = "DefaultLocationComboBox";
            this.Size = new System.Drawing.Size(288, 42);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
