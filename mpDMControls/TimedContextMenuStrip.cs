using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;

namespace LMP.Controls
{

   /// <summary>
   /// TimedContextMenuStrip class
   /// Defines a ContextMenuStrip with a corresponding timer to 
   /// automatically close menu when it loses focus
   /// </summary>
    public partial class TimedContextMenuStrip : ContextMenuStrip
    {
        #region *********************fields*********************
        // Windows API call to get active window
        [DllImport("user32.dll")]
        static extern int GetForegroundWindow();
        static Hashtable m_oOpenMenus = null;
        static System.Windows.Forms.Timer m_oTimer;
        static int m_oTimerInterval;
        static ContextMenuStrip m_oCurrentMenu = null;
        static int m_iParentHandle = 0;
        public bool m_bUseTimer = true;
        #endregion
        #region *******************constructors*****************
        static TimedContextMenuStrip()
        {
            m_oOpenMenus = new Hashtable();
            m_oTimer = new System.Windows.Forms.Timer();
            m_oTimer.Tick += new EventHandler(m_oTimer_Tick);
            //GLOG 3113: Set initial timer interval to large value
            //so menu won't flash off immediately if not initially under mouse cursor
            m_oTimer.Interval = 3000;
            m_oTimer.Stop();
        }
        public TimedContextMenuStrip()
        {
            InitializeComponent();
            ExecuteFinalSetup();
        }
        public TimedContextMenuStrip(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            ExecuteFinalSetup();
        }
        #endregion
        #region *******************properties********************
        /// <summary>
        /// Interval in milliseconds between Ticks of timer
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets menu timeout interval in milliseconds.")]
        public int TimerInterval
        {
            get { return m_oTimerInterval; }
            set { m_oTimerInterval = value; }
        }
        /// <summary>
        /// gets/sets whether to use the timer for this menu-
        /// if not, the only way to close the menu is to press "Esc"
        /// </summary>
        public bool UseTimer
        {
            get { return m_bUseTimer; }
            set { m_bUseTimer = value; }
        }
        #endregion
        #region ********************methods*******************
        /// <summary>
        /// Setup default properties and events
        /// </summary>
        private void ExecuteFinalSetup()
        {
            if (!this.DesignMode)
            {
                this.Opened += new EventHandler(TimedContextMenuStrip_Opened);
                this.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(TimedContextMenuStrip_Closed);
            }
        }

        /// <summary>
        /// Handles Cursor navigation keys in menu
        /// </summary>
        /// <param name="e"></param>
        public void HandleKeyDown(KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    CloseActiveMenu(m_oCurrentMenu);
                }
                else if (e.KeyCode == Keys.Down)
                {
                    ToolStripItem oSelItem = null;
                    foreach (ToolStripItem oItem in this.Items)
                    {
                        if (oSelItem != null && oItem.CanSelect && oItem.Visible && oItem.Enabled)
                        {
                            //select this item
                            oItem.Select();

                            if (oItem is ToolStripMenuItem)
                                ((ToolStripMenuItem)oItem).ShowDropDown();

                            return;
                        }
                        else if (oSelItem == null && oItem.Selected && oItem.Visible && oItem.Enabled)
                        {
                            oSelItem = oItem;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Up)
                {
                    ToolStripItem oSelItem = null;
                    ToolStripItem oPrevItem = null;

                    foreach (ToolStripItem oItem in this.Items)
                    {
                        if (oSelItem == null && oItem.Selected && oItem.Visible && oItem.Enabled)
                        {
                            //this is the selected item
                            oSelItem = oItem;

                            if (oPrevItem != null)
                            {
                                oPrevItem.Select();

                                if (oPrevItem is ToolStripMenuItem)
                                    ((ToolStripMenuItem)oPrevItem).ShowDropDown();

                                return;
                            }
                        }
                        else if (oItem.CanSelect && oItem.Visible && oItem.Enabled)
                        {
                            //get this item as the previous item
                            oPrevItem = oItem;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
                {
                    foreach (ToolStripItem oItem in this.Items)
                    {
                        if (oItem.Selected && oItem.Visible && oItem is ToolStripMenuItem)
                        {
                            //selected item is a dropdown - show dropdown menu
                            ToolStripMenuItem oMenu = (ToolStripMenuItem)oItem;
                            oMenu.ShowDropDown();
                            oMenu.DropDown.Select();
                            oMenu.DropDown.Focus();
                            if (!this.DesignMode)
                            {
                                AddToOpenMenusCollection();
                            }

                            foreach (ToolStripItem oSubItem in oMenu.DropDownItems)
                            {
                                //select first item in dropdown
                                if (oSubItem.Enabled && oSubItem.Visible)
                                {
                                    oSubItem.Select();
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (e.KeyCode == Keys.Return)
                {
                    foreach (ToolStripItem oItem in this.Items)
                    {
                        if (oItem.Selected && oItem.Visible)
                        {
                            if (((ToolStripMenuItem)oItem).DropDown.Items.Count > 0)
                                ((ToolStripMenuItem)oItem).ShowDropDown();
                            else
                                oItem.PerformClick();
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Returns false if cursor is not over the specific menu or its parent,
        /// or if focus has switched to a different application
        /// </summary>
        /// <param name="oMenu"></param>
        /// <returns></returns>
        static bool bCursorIsOverMenu()
        {
            if (m_iParentHandle > 0 && (m_iParentHandle != GetForegroundWindow()))
            {
                // Originating application is no longer in the foreground
                return false;
            }
            Point oPt = Cursor.Position;
            bool bCursorInMenu = false;
            //Check each menu in HashTable to see if cursor is within its area
            foreach (DictionaryEntry de in m_oOpenMenus)
            {
                ContextMenuStrip oMenu = (ContextMenuStrip)de.Value;
                //Point oMenuTopLeft = oMenu.PointToScreen(new Point(oMenu.Left, 
                //    oMenu.Top));
                Rectangle oRect = oMenu.ClientRectangle;
                //Rectangle oRect = new Rectangle(oMenuTopLeft.X, 
                //    oMenuTopLeft.Y, oMenu.Width, oMenu.Height);
                // Add some padding
                //GLOG 3113: Allow extra space in case context menu is below node
                oRect.Inflate(40, 40);

                if (oRect.Contains(oMenu.PointToClient(oPt)))
                {
                    //No need to check further
                    bCursorInMenu = true;
                    break;
                }
                else
                {
                    LMP.Trace.WriteNameValuePairs("oMenu", oMenu.Name, "oRect.X", oRect.X,
                        "oRect.Y", oRect.Y, "oRect.Width", oRect.Width, "oRect.Height", oRect.Height,
                        "oPt.X", oPt.X, "oPt.Y", oPt.Y);
                }

            }
            return bCursorInMenu;
        }
        /// <summary>
        /// Closes the specified menu or top-level menu if current object is submenu
        /// </summary>
        /// <param name="oMenu"></param>
        static void CloseActiveMenu(ContextMenuStrip oMenu)
        {
            // Find top-level menu
            while (oMenu.OwnerItem != null)
            {
                oMenu = (ContextMenuStrip)oMenu.OwnerItem.Owner;
            }

            //GLOG item #5740 - dcf
            //oMenu.Close(ToolStripDropDownCloseReason.CloseCalled);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            try
            {
                HandleKeyDown(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
        }
        #endregion
        #region *********************events*********************
        /// <summary>
        /// Menu has been closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TimedContextMenuStrip_Closed(object sender, System.Windows.Forms.ToolStripDropDownClosedEventArgs e)
        {
            if (!this.DesignMode)
            {
                try
                {
                    LMP.Trace.WriteNameValuePairs("Name", this.Name);
                    if (m_oOpenMenus.ContainsKey(this.Name))
                        m_oOpenMenus.Remove(this.Name);

                    // If menu is submenu, make parent menu the current object
                    if (((ContextMenuStrip)sender).OwnerItem != null)
                    {
                        m_oCurrentMenu = (ContextMenuStrip)((ContextMenuStrip)sender).OwnerItem.Owner;
                        //Start timer for parent menu
                        m_oTimer.Interval = ((TimedContextMenuStrip)m_oCurrentMenu).TimerInterval;
                        
                        if(this.UseTimer)
                            m_oTimer.Start();
                    }
                    else
                    {
                        m_oCurrentMenu = null;
                        m_iParentHandle = 0;
                    }
                }
                catch { }
            }
        }
        /// <summary>
        /// Menu has just been opened
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TimedContextMenuStrip_Opened(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                AddToOpenMenusCollection();
            }
        }

        private void AddToOpenMenusCollection()
        {
            try
            {
                LMP.Trace.WriteNameValuePairs("Name", this.Name);
                if (!m_oOpenMenus.ContainsKey(this.Name))
                    m_oOpenMenus.Add(this.Name, this);

                //restart timer while menu is displayed
                m_oTimer.Stop();
                m_iParentHandle = GetForegroundWindow();
                m_oCurrentMenu = this;
                if (this.UseTimer)
                    m_oTimer.Start();
            }
            catch { };
        }
        /// <summary>
        /// Specified timer interval has passed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void m_oTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (m_oCurrentMenu == null)
                {
                    // Timer should not be running in this case
                    m_oTimer.Stop();
                    return;
                }
                else if (!bCursorIsOverMenu())
                {
                    // If cursor is not currently over a menu item, close the menu
                    m_oTimer.Stop();

                    CloseActiveMenu(m_oCurrentMenu);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void TimedContextMenuStrip_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                //GLOG 5526:  Don't immediately close menu if mouse is still within
                //the boundaries of a rectangle slightly larger than the menu.
                //Menu won't close until Timer expires and cursor is outside rectangle.
                if (m_oCurrentMenu == this)
                {
                    if (!bCursorIsOverMenu())
                    {
                        m_oTimer.Stop();
                        this.Close();
                    }
                }
                else
                {
                    m_oTimer.Stop();
                    m_oTimer.Interval = this.TimerInterval;
                    if (this.UseTimer)
                        m_oTimer.Start();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        ///// <summary>
        ///// processes tab and shift-tab key messages -
        ///// this method will be executed only when the
        ///// message pump is broken, ie when the hosing
        ///// form's ProcessDialogKey method is not run.
        ///// this seems to happen when when tabbing from
        ///// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        ///// </summary>
        ///// <param name="m"></param>
        ///// <returns></returns>
        //protected override bool ProcessKeyMessage(ref Message m)
        //{
        //    int iQualifierKeys;
        //    bool bShiftPressed;
        //    bool bEnterKeyPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);
        //    bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

        //    if (bTabPressed)
        //    {
        //        //return that key was processed
        //        return true;
        //    }
        //    else if (bEnterKeyPressed)
        //    {
        //        return true;
        //    }
        //    else
        //        //process key normally
        //        return base.ProcessKeyMessage(ref m);
        //}
        #endregion

    }
}
