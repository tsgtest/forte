namespace LMP.Controls
{
    partial class ExpressionBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtExpression = new System.Windows.Forms.TextBox();
            this.treeFieldCodes = new Infragistics.Win.UltraWinTree.UltraTree();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAnd = new System.Windows.Forms.Button();
            this.btnOr = new System.Windows.Forms.Button();
            this.btnLike = new System.Windows.Forms.Button();
            this.btnEquals = new System.Windows.Forms.Button();
            this.btnNotEquals = new System.Windows.Forms.Button();
            this.btnGreater = new System.Windows.Forms.Button();
            this.btnLess = new System.Windows.Forms.Button();
            this.btnGreaterEquals = new System.Windows.Forms.Button();
            this.btnLesserEquals = new System.Windows.Forms.Button();
            this.btnBrackets = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gBoxParameters = new System.Windows.Forms.GroupBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnElse = new System.Windows.Forms.Button();
            this.btnIf = new System.Windows.Forms.Button();
            this.btnPaste = new System.Windows.Forms.Button();
            this.btnMyValue = new System.Windows.Forms.Button();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.btnNull = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDescription = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.treeFieldCodes)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtExpression
            // 
            this.txtExpression.HideSelection = false;
            this.txtExpression.Location = new System.Drawing.Point(7, 22);
            this.txtExpression.Multiline = true;
            this.txtExpression.Name = "txtExpression";
            this.txtExpression.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtExpression.Size = new System.Drawing.Size(504, 68);
            this.txtExpression.TabIndex = 0;
            this.txtExpression.TextChanged += new System.EventHandler(this.txtExpression_TextChanged);
            this.txtExpression.Leave += new System.EventHandler(this.txtExpression_LostFocus);
            // 
            // treeFieldCodes
            // 
            this.treeFieldCodes.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.treeFieldCodes.Indent = 15;
            this.treeFieldCodes.Location = new System.Drawing.Point(7, 146);
            this.treeFieldCodes.Name = "treeFieldCodes";
            this.treeFieldCodes.Size = new System.Drawing.Size(200, 226);
            this.treeFieldCodes.TabIndex = 15;
            this.treeFieldCodes.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeFieldCodes_AfterSelect);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(7, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 21;
            this.label1.Text = "&Field Codes:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(8, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "&Expression:";
            // 
            // btnAnd
            // 
            this.btnAnd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAnd.Location = new System.Drawing.Point(8, 97);
            this.btnAnd.Name = "btnAnd";
            this.btnAnd.Size = new System.Drawing.Size(34, 25);
            this.btnAnd.TabIndex = 1;
            this.btnAnd.Text = "And";
            this.btnAnd.UseVisualStyleBackColor = true;
            this.btnAnd.Click += new System.EventHandler(this.btnAnd_Click);
            // 
            // btnOr
            // 
            this.btnOr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOr.Location = new System.Drawing.Point(41, 97);
            this.btnOr.Name = "btnOr";
            this.btnOr.Size = new System.Drawing.Size(28, 25);
            this.btnOr.TabIndex = 2;
            this.btnOr.Text = "Or";
            this.btnOr.UseVisualStyleBackColor = true;
            this.btnOr.Click += new System.EventHandler(this.btnOr_Click);
            // 
            // btnLike
            // 
            this.btnLike.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLike.Location = new System.Drawing.Point(68, 97);
            this.btnLike.Name = "btnLike";
            this.btnLike.Size = new System.Drawing.Size(35, 25);
            this.btnLike.TabIndex = 3;
            this.btnLike.Text = "Like";
            this.btnLike.UseVisualStyleBackColor = true;
            this.btnLike.Click += new System.EventHandler(this.btnLike_Click);
            // 
            // btnEquals
            // 
            this.btnEquals.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEquals.Location = new System.Drawing.Point(338, 97);
            this.btnEquals.Name = "btnEquals";
            this.btnEquals.Size = new System.Drawing.Size(25, 25);
            this.btnEquals.TabIndex = 8;
            this.btnEquals.Text = "=";
            this.btnEquals.UseVisualStyleBackColor = true;
            this.btnEquals.Click += new System.EventHandler(this.btnEquals_Click);
            // 
            // btnNotEquals
            // 
            this.btnNotEquals.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNotEquals.Location = new System.Drawing.Point(362, 97);
            this.btnNotEquals.Name = "btnNotEquals";
            this.btnNotEquals.Size = new System.Drawing.Size(23, 25);
            this.btnNotEquals.TabIndex = 9;
            this.btnNotEquals.Text = "!=";
            this.btnNotEquals.UseVisualStyleBackColor = true;
            this.btnNotEquals.Click += new System.EventHandler(this.btnNotEquals_Click);
            // 
            // btnGreater
            // 
            this.btnGreater.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGreater.Location = new System.Drawing.Point(384, 97);
            this.btnGreater.Name = "btnGreater";
            this.btnGreater.Size = new System.Drawing.Size(24, 25);
            this.btnGreater.TabIndex = 10;
            this.btnGreater.Text = ">";
            this.btnGreater.UseVisualStyleBackColor = true;
            this.btnGreater.Click += new System.EventHandler(this.btnGreater_Click);
            // 
            // btnLess
            // 
            this.btnLess.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLess.Location = new System.Drawing.Point(407, 97);
            this.btnLess.Name = "btnLess";
            this.btnLess.Size = new System.Drawing.Size(24, 25);
            this.btnLess.TabIndex = 11;
            this.btnLess.Text = "<";
            this.btnLess.UseVisualStyleBackColor = true;
            this.btnLess.Click += new System.EventHandler(this.btnLess_Click);
            // 
            // btnGreaterEquals
            // 
            this.btnGreaterEquals.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGreaterEquals.Location = new System.Drawing.Point(430, 97);
            this.btnGreaterEquals.Name = "btnGreaterEquals";
            this.btnGreaterEquals.Size = new System.Drawing.Size(27, 25);
            this.btnGreaterEquals.TabIndex = 12;
            this.btnGreaterEquals.Text = ">=";
            this.btnGreaterEquals.UseVisualStyleBackColor = true;
            this.btnGreaterEquals.Click += new System.EventHandler(this.btnGreaterEquals_Click);
            // 
            // btnLesserEquals
            // 
            this.btnLesserEquals.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLesserEquals.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLesserEquals.Location = new System.Drawing.Point(456, 97);
            this.btnLesserEquals.Name = "btnLesserEquals";
            this.btnLesserEquals.Size = new System.Drawing.Size(27, 25);
            this.btnLesserEquals.TabIndex = 13;
            this.btnLesserEquals.Text = "<=";
            this.btnLesserEquals.UseVisualStyleBackColor = true;
            this.btnLesserEquals.Click += new System.EventHandler(this.btnLesserEquals_Click);
            // 
            // btnBrackets
            // 
            this.btnBrackets.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBrackets.Location = new System.Drawing.Point(482, 97);
            this.btnBrackets.Name = "btnBrackets";
            this.btnBrackets.Size = new System.Drawing.Size(27, 25);
            this.btnBrackets.TabIndex = 14;
            this.btnBrackets.Text = "{}";
            this.btnBrackets.UseVisualStyleBackColor = true;
            this.btnBrackets.Click += new System.EventHandler(this.btnBrackets_Click);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(436, 352);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 27);
            this.btnOk.TabIndex = 19;
            this.btnOk.Text = "O&K";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(436, 386);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(220, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 22;
            this.label3.Text = "&Parameters:";
            // 
            // gBoxParameters
            // 
            this.gBoxParameters.Location = new System.Drawing.Point(219, 139);
            this.gBoxParameters.Name = "gBoxParameters";
            this.gBoxParameters.Size = new System.Drawing.Size(209, 234);
            this.gBoxParameters.TabIndex = 16;
            this.gBoxParameters.TabStop = false;
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(434, 180);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(75, 25);
            this.btnUndo.TabIndex = 18;
            this.btnUndo.Text = "&Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnElse
            // 
            this.btnElse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnElse.Location = new System.Drawing.Point(154, 97);
            this.btnElse.Name = "btnElse";
            this.btnElse.Size = new System.Drawing.Size(35, 25);
            this.btnElse.TabIndex = 5;
            this.btnElse.Text = "Else";
            this.btnElse.UseVisualStyleBackColor = true;
            this.btnElse.Click += new System.EventHandler(this.btnElse_Click);
            // 
            // btnIf
            // 
            this.btnIf.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIf.Location = new System.Drawing.Point(106, 97);
            this.btnIf.Name = "btnIf";
            this.btnIf.Size = new System.Drawing.Size(49, 25);
            this.btnIf.TabIndex = 4;
            this.btnIf.Text = "If Then";
            this.btnIf.UseVisualStyleBackColor = true;
            this.btnIf.Click += new System.EventHandler(this.btnIf_Click);
            // 
            // btnPaste
            // 
            this.btnPaste.Location = new System.Drawing.Point(436, 148);
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.Size = new System.Drawing.Size(73, 25);
            this.btnPaste.TabIndex = 17;
            this.btnPaste.Text = "&Paste";
            this.btnPaste.UseVisualStyleBackColor = true;
            this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
            // 
            // btnMyValue
            // 
            this.btnMyValue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMyValue.Location = new System.Drawing.Point(192, 97);
            this.btnMyValue.Name = "btnMyValue";
            this.btnMyValue.Size = new System.Drawing.Size(59, 25);
            this.btnMyValue.TabIndex = 6;
            this.btnMyValue.Text = "My Value";
            this.btnMyValue.UseVisualStyleBackColor = true;
            this.btnMyValue.Click += new System.EventHandler(this.btnMyValue_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEmpty.Location = new System.Drawing.Point(254, 97);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(44, 25);
            this.btnEmpty.TabIndex = 7;
            this.btnEmpty.Text = "Empty";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnNull
            // 
            this.btnNull.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNull.Location = new System.Drawing.Point(301, 97);
            this.btnNull.Name = "btnNull";
            this.btnNull.Size = new System.Drawing.Size(34, 25);
            this.btnNull.TabIndex = 24;
            this.btnNull.Text = "Null";
            this.btnNull.UseVisualStyleBackColor = true;
            this.btnNull.Click += new System.EventHandler(this.btnNull_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblDescription);
            this.groupBox1.Location = new System.Drawing.Point(7, 379);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 33);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(7, 13);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(0, 15);
            this.lblDescription.TabIndex = 28;
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ExpressionBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(523, 424);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.treeFieldCodes);
            this.Controls.Add(this.btnNull);
            this.Controls.Add(this.btnEmpty);
            this.Controls.Add(this.btnPaste);
            this.Controls.Add(this.btnElse);
            this.Controls.Add(this.btnMyValue);
            this.Controls.Add(this.btnIf);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnBrackets);
            this.Controls.Add(this.btnLesserEquals);
            this.Controls.Add(this.btnGreaterEquals);
            this.Controls.Add(this.btnLess);
            this.Controls.Add(this.btnGreater);
            this.Controls.Add(this.btnNotEquals);
            this.Controls.Add(this.btnEquals);
            this.Controls.Add(this.btnLike);
            this.Controls.Add(this.btnOr);
            this.Controls.Add(this.btnAnd);
            this.Controls.Add(this.txtExpression);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gBoxParameters);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExpressionBuilder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forte Expression Builder";
            ((System.ComponentModel.ISupportInitialize)(this.treeFieldCodes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtExpression;
        private Infragistics.Win.UltraWinTree.UltraTree treeFieldCodes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAnd;
        private System.Windows.Forms.Button btnOr;
        private System.Windows.Forms.Button btnLike;
        private System.Windows.Forms.Button btnEquals;
        private System.Windows.Forms.Button btnNotEquals;
        private System.Windows.Forms.Button btnGreater;
        private System.Windows.Forms.Button btnLess;
        private System.Windows.Forms.Button btnGreaterEquals;
        private System.Windows.Forms.Button btnLesserEquals;
        private System.Windows.Forms.Button btnBrackets;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gBoxParameters;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnElse;
        private System.Windows.Forms.Button btnIf;
        private System.Windows.Forms.Button btnPaste;
        private System.Windows.Forms.Button btnMyValue;
        private System.Windows.Forms.Button btnEmpty;
        private System.Windows.Forms.Button btnNull;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDescription;

    
    
    }

}
