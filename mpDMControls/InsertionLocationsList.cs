using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class InsertionLocationsList : LMP.Controls.CheckedListBox
    {
        #region *********************fields*********************
        private static object[,] m_aLocations;
        #endregion
        #region *********************constructors*********************
        static InsertionLocationsList()
        {
            //get array of insertion locations
            m_aLocations = new object[,]{
                {"Start of Current Section",1}, 
                {"End of Current Section", 2},
                {"Start of All Sections", 4}, 
                {"End of All Sections", 8},
                {"Selection", 16},
                {"Start of Document", 32},
                {"End of Document", 64},
                {"New Document", 128},
                {"Default Location", 256}};
        }

        public InsertionLocationsList()
            : base(m_aLocations)
        {
        }
        #endregion
        #region *********************properties*********************
        public static object[,] List
        {
            get { return m_aLocations; }
        }
        #endregion
    }
}
