namespace LMP.Controls
{
    partial class ControlPropertyEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPropertyName = new System.Windows.Forms.Label();
            this.mnuPopup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuPopup_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPopup_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPopup_DeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPopup_DeleteAll = new System.Windows.Forms.ToolStripMenuItem();
            this.grdProperties = new LMP.Controls.DataGridView();
            this.mnuPopup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(138, 95);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(82, 25);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(223, 95);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 25);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblPropertyName
            // 
            this.lblPropertyName.AutoSize = true;
            this.lblPropertyName.BackColor = System.Drawing.Color.Transparent;
            this.lblPropertyName.Location = new System.Drawing.Point(12, 16);
            this.lblPropertyName.Name = "lblPropertyName";
            this.lblPropertyName.Size = new System.Drawing.Size(37, 14);
            this.lblPropertyName.TabIndex = 0;
            this.lblPropertyName.Text = "&Value:";
            // 
            // mnuPopup
            // 
            this.mnuPopup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPopup_MoveUp,
            this.mnuPopup_MoveDown,
            this.mnuPopup_DeleteRow,
            this.mnuPopup_DeleteAll});
            this.mnuPopup.Name = "mnuPopup";
            this.mnuPopup.ShowImageMargin = false;
            this.mnuPopup.Size = new System.Drawing.Size(119, 92);
            this.mnuPopup.Opening += new System.ComponentModel.CancelEventHandler(this.mnuPopup_Opening);
            // 
            // mnuPopup_MoveUp
            // 
            this.mnuPopup_MoveUp.Name = "mnuPopup_MoveUp";
            this.mnuPopup_MoveUp.Size = new System.Drawing.Size(118, 22);
            this.mnuPopup_MoveUp.Text = "[MoveUp]";
            this.mnuPopup_MoveUp.Click += new System.EventHandler(this.mnuPopup_MoveUp_Click);
            // 
            // mnuPopup_MoveDown
            // 
            this.mnuPopup_MoveDown.Name = "mnuPopup_MoveDown";
            this.mnuPopup_MoveDown.Size = new System.Drawing.Size(118, 22);
            this.mnuPopup_MoveDown.Text = "[MoveDown]";
            this.mnuPopup_MoveDown.Click += new System.EventHandler(this.mnuPopup_MoveDown_Click);
            // 
            // mnuPopup_DeleteRow
            // 
            this.mnuPopup_DeleteRow.Name = "mnuPopup_DeleteRow";
            this.mnuPopup_DeleteRow.Size = new System.Drawing.Size(118, 22);
            this.mnuPopup_DeleteRow.Text = "[DeleteRow]";
            this.mnuPopup_DeleteRow.Click += new System.EventHandler(this.mnuPopup_DeleteRow_Click);
            // 
            // mnuPopup_DeleteAll
            // 
            this.mnuPopup_DeleteAll.Name = "mnuPopup_DeleteAll";
            this.mnuPopup_DeleteAll.Size = new System.Drawing.Size(118, 22);
            this.mnuPopup_DeleteAll.Text = "[DeleteAll]";
            this.mnuPopup_DeleteAll.Click += new System.EventHandler(this.mnuPopup_DeleteAll_Click);
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProperties.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.IsDirty = false;
            this.grdProperties.Location = new System.Drawing.Point(15, 33);
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowHeadersWidth = 20;
            this.grdProperties.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdProperties.RowTemplate.Height = 18;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(290, 44);
            this.grdProperties.SupportingValues = "";
            this.grdProperties.TabIndex = 4;
            this.grdProperties.Tag2 = null;
            this.grdProperties.Value = null;
            this.grdProperties.Visible = false;
            this.grdProperties.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdProperties_DataError);
            this.grdProperties.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grdProperties_KeyPress);
            this.grdProperties.MouseDown += new System.Windows.Forms.MouseEventHandler(this.grdProperties_MouseDown);
            // 
            // ControlPropertyEditForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(317, 132);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grdProperties);
            this.Controls.Add(this.lblPropertyName);
            this.Controls.Add(this.btnOK);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ControlPropertyEditForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Control Property";
            this.mnuPopup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Label lblPropertyName;
        private DataGridView grdProperties;
        private System.Windows.Forms.ContextMenuStrip mnuPopup;
        private System.Windows.Forms.ToolStripMenuItem mnuPopup_DeleteRow;
        private System.Windows.Forms.ToolStripMenuItem mnuPopup_DeleteAll;
        private System.Windows.Forms.ToolStripMenuItem mnuPopup_MoveUp;
        private System.Windows.Forms.ToolStripMenuItem mnuPopup_MoveDown;
    }
}