using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;
using System.Collections;
using LMP.Architect.Api;

namespace LMP.Controls
{
    public class TrailerDisplayOptionsComboBox : LMP.Controls.ComboBox
    {

        #region ***************************enumerations*******************************
        #endregion
        #region *********************fields*********************
        private static object[,] m_aTrailerDisplayOptions;
        #endregion
        #region *********************constructors*********************
        static TrailerDisplayOptionsComboBox()
        {
            //get array of insertion DisplayOptions
            m_aTrailerDisplayOptions = new object[,]{
                {"Manual Insertion",1}, 
                {"Insert Without Dialog Prompt",2}, 
                {"Display Dialog",3}};
        }

        public TrailerDisplayOptionsComboBox()
        {
            this.SetList(m_aTrailerDisplayOptions);
        }
        #endregion
        #region *********************properties***************************
        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public static object[,] List
        {
            get
            {
                return m_aTrailerDisplayOptions;
            }
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TrailerDisplayOptionsComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.Name = "TrailerDisplayOptionsComboBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #region *********************protected members*********************
        #endregion
    }
}
