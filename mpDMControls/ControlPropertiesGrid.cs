using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class ControlPropertiesGrid : UserControl, IControl
    {
        #region *********************enumerations**************
        #endregion
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private DataTable m_oDataTable;
        private int m_iLastSelectedRowIndex = -1;
        private int m_iScrollOffset = 0;
        private LMP.Data.mpControlTypes m_iCtlType; 
        PreviewKeyDownEventHandler m_oPreviewKeyDownEventHandler = null;
        private string m_xSupportingValues = "";

        #endregion
        #region *********************constructors*********************
        public ControlPropertiesGrid()
        {
            InitializeComponent();

            //subscribe to DataGridView's TabPressed event
            //this.grdProperties.TabPressed += new TabPressedHandler(grdProperties_TabPressed);
            this.mnuGrid_DeleteItem.Text = LMP.Resources.GetLangString("Menu_PropertyDeleteItem");
            this.mnuGrid_ReloadDefaults.Text = LMP.Resources.GetLangString("Menu_PropertyReloadDefaults");

            //build datatable - include column for ellipse button
            m_oDataTable = new DataTable();
            m_oDataTable.Columns.Add("Name");
            m_oDataTable.Columns.Add("Value");

            //rebind datatable to grid
            this.grdProperties.DataSource = m_oDataTable;

            //set column properties
            this.grdProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.grdProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //set relative width of two display columns
            this.grdProperties.Columns[0].FillWeight = 100;
            this.grdProperties.Columns[1].FillWeight = 100;

            //don't need to be able to sort the rows by clicking on headers
            this.grdProperties.Columns[0].SortMode = DataGridViewColumnSortMode.Programmatic;
            this.grdProperties.Columns[1].SortMode = DataGridViewColumnSortMode.Programmatic;

            //GLOG : 8546 : ceh
            this.grdProperties.RowTemplate.Height = (int)(18 * LMP.OS.GetScalingFactor());
            
            //add button column
            this.AddButtonColumn();
            this.ContextMenuStrip = mnuGrid;
        }
        #endregion
        #region *********************properties***********************
        [DescriptionAttribute("Gets/Sets the an LMP.Data.ControlType.")]
        public LMP.Data.mpControlTypes ControlType
        {
            set { m_iCtlType = value; }
            get { return m_iCtlType; }
        }
         #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }

        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get { return GetValue(); }
            set{ SetValue(value); }
        }
        public bool IsDirty
        {
            set
            {
                m_bIsDirty = value;
            }

            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// Determine if the key pressed is recognized for input by the control
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                HandleTabPressed(bShiftPressed);

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// populates the datatable for this 
        /// control with the specified properties
        /// </summary>
        /// <param name="xControlProperties"></param>
        private void SetValue(string xControlProperties)
        {
            try
            {
                m_iLastSelectedRowIndex = -1;

                //clear any existing values
                m_oDataTable.Rows.Clear();

                if (xControlProperties == null || xControlProperties == "")
                    return;

                //split string into a property array
                string[] aProps = xControlProperties.Split('�');

                foreach (string xProp in aProps)
                {
                    //parse name/value
                    int iPos = xProp.IndexOf('=');

                    if (iPos == -1)
                        throw new LMP.Exceptions.DataException(
                            LMP.Resources.GetLangString(
                            "Error_InvalidControlPropertiesString") + xControlProperties);

                    //add row to datatable
                    m_oDataTable.Rows.Add(xProp.Substring(0, iPos),
                        xProp.Substring(iPos + 1));
                }

                this.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlPropertiesGridValue"), oE);
            }
        }
        private void HandleTabPressed(bool bShiftPressed)
        {
            try
            {
                if ((grdProperties.CurrentCell.ColumnIndex == 0 &&
                    grdProperties.CurrentCell.RowIndex == grdProperties.Rows.Count - 1 && !bShiftPressed) ||
                    (grdProperties.CurrentCell.RowIndex == 0 && grdProperties.CurrentCell.ColumnIndex == 1 && bShiftPressed))
                {
                    //tabbing out of last cell or shift-tabbing out of first cell
                    //notify subscribers that tab was pressed
                    if (this.TabPressed != null)
                        this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));
                }
                else if (grdProperties.CurrentCell.ColumnIndex == 0)
                {
                    //Tabbing out of button cell
                    if (bShiftPressed)
                    {
                        grdProperties.CurrentCell = grdProperties.Rows[grdProperties.CurrentCell.RowIndex].Cells[2];
                    }
                    else
                    {
                        grdProperties.CurrentCell = grdProperties.Rows[grdProperties.CurrentCell.RowIndex + 1].Cells[1];
                    }
                }
                else
                {
                    //tabbing within grid
                    return;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// returns the value of the grid as a delimited string
        /// </summary>
        /// <returns></returns>
        private string GetValue()
        {
            StringBuilder oSB = new StringBuilder();

            if (m_oDataTable.Rows.Count == 0)
                return "";

            //if grid is dirty, ensure pending edits are committed
            //before building value string
            if (this.IsDirty && ((DataRowView)grdProperties.
                    BindingContext[m_oDataTable].Current).IsEdit)
            {
                grdProperties.EndEdit();
                grdProperties.BindingContext[m_oDataTable].EndCurrentEdit();
            }
            
            //cycle through rows, adding name/value pairs to stringbuilder
            foreach (DataRow oRow in m_oDataTable.Rows)
            {
                if (oRow.ItemArray[0].ToString() != "")
                {
                    oSB.AppendFormat("{0}={1}�", oRow.ItemArray[0].ToString(),
                        oRow.ItemArray[1].ToString());
                }
            }

            //remove trailing delimiter
            oSB.Remove(oSB.Length - 1, 1);

            string xValue = oSB.ToString();

            //return string
            return xValue;
        }
        #endregion
        #region *********************methods*****************************
        /// <summary>
        /// loads grid with preset values appropriate to designated
        /// control type
        /// </summary>
        /// <param name="oType"></param>
        public void LoadDefaultValues(LMP.Data.mpControlTypes iType)
        {
            string[,] aValues = null;
            //build storage for prop name value string
            //clear data table for control types whose default props are
            //not configured
            StringBuilder oSB = new StringBuilder();

            switch (iType)
            {
                case LMP.Data.mpControlTypes.None:
                    m_oDataTable.Rows.Clear();
                    break;
                case LMP.Data.mpControlTypes.AuthorSelector:
                    aValues = new string[,]{{"AllowManualInput","false"},
                                            {"AllowManualInputLeadAuthor","false"},
                                            {"DropDownRows","8"},
                                            {"DisplayRows", "4"},
                                            {"ShowColumns","3"},
                                            {"ListType", "0"}, 
                                            {"ListWhereClause",""}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.MultilineCombo:
                    aValues = new string[,]{{"ForceCaps","False"},
                                            {"ListName",""},
                                            {"ListRows","6"},
                                            {"Separator", "1310"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.DetailGrid:
                    aValues = new string[,]{{"ConfigString",""},
                                            {"AutoSize","True"},
                                            {"CounterScheme", "Item %0 of %1"},
                                            {"LabelColumnWidth", "85"},
                                            {"MaxEntities", "0"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.DetailList:
                    aValues = new string[,]{{"CIToken","<DISPLAYNAME>"},
                                            {"VisibleRows", "4"},
                                            {"RowsPerItem", "1"},
                                            {"ShowGridlines", "True"},
                                            {"AutoSize", "False"},
                                            {"CounterScheme", "Item %0 of %1"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.ClientMatterSelector:
                    m_oDataTable.Rows.Clear();
                    break;
                case LMP.Data.mpControlTypes.SegmentChooser:
                    aValues = new string[,]{{"AssignedObjectType","4"},
                                                {"SegmentsList", ""}, 
                                                {"IncludeNoneOption", "False"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.SalutationCombo:
                    aValues = new string[,]{{"AllowDropdown","False"},
                                                {"SelectionType", "0"},
                                                {"DefaultListIndex", "0"},
                                                {"CanRequestCI", "False"},
                                                {"Schema", "Dear [CIDetail_FirstName]:�Dear [CIDetail_Prefix] [CIDetail_LastName]:"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.RelineGrid:
                    aValues = new string[,]{{"CaptionString","Reline�Special"},
                                            {"DefaultTab","0"},
                                            {"Style", "0"},
                                            {"RemoveEmptyValues", "true"}, //GLOG 4220
                                            {"FormatOptions","0"},
                                            {"FormatValues","0"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.Textbox:
                    aValues = new string[,] {{"SelectionMode", "0" }};   //GLOG 6235
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.Checkbox:
                    aValues = new string[,]{{"TrueString","Yes"},
                                            {"FalseString","No"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.Combo:
                    aValues = new string[,]{{"ListName",""},
                                            {"MaxDropDownItems", "6"}};

                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.LocationsDropdown:
                    aValues = new string[,]{{"LocationType","1"},
                                              {"LocationParentID","0"},
                                              {"ValueField","1"},
                                              {"MaxDropDownItems", "6"}};

                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.List:
                    m_oDataTable.Rows.Clear();
                    break;
                case LMP.Data.mpControlTypes.MultilineTextbox:
                    aValues = new string[,] {{ "NumberOfLines", "2" }, 
                                             { "SelectionMode", "0"}};  //GLOG 6235
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.DropdownList:
                    aValues = new string[,]{{"ListName",""},
                                            {"MaxDropDownItems", "6"},
                                            {"AllowEmptyValue", "false"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.DateCombo:
                    aValues = new string[,]{{"Formats", "MMMM d, yyyy /F�MMMM d, yyyy�MMM d, yyyy�MM/dd/yy�this d% day of MMMM, yyyy"},
                                            {"MaxDropDownItems", "6"},
                                            {"Mode", "Format"},
                                            {"ListName", ""},
                                            {"LCID", "[SegmentLanguageID]"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.Spinner:
                    aValues = new string[,]{{"AppendSymbol", "True"},
                                            {"DecimalPlaces", "0"},
                                            {"DisplayUnit", "Inches"},
                                            {"Increment", "1"},
                                            {"Maximum", "1000"},
                                            {"Minimum", "0"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.PeopleDropdown:
                    aValues = new string[,]{{"FilterOptions","0"},
                                            {"MaxDropDownItems", "6"},
                                            {"WhereClause", ""}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.Calendar:
                    aValues = new string[,]{{"CustomDateFormat","MMMM d, yyyy"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.FontList:
                    aValues = new string[,]{{ "OnNotInList", "2" } };
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.NameValuePairGrid:
                    aValues = new string[,] { { "AllowAddRow", "true" },
                                              { "AllowDeleteRow", "true" },
                                              { "AllowEmptyRow", "true" },
                                              { "RemoveEmptyValues", "true"},
                                              { "InitialContent", "" },
                                              { "VisibleRows", "4" }, 
                                              { "NameValueSeparator", "0" }, //GLOG 4559
                                              { "TabStopColumns", "2"},
                                              { "WrapCellText", "false"},
                                              { "LabelColumnWidth", "0"},
                                              { "AllowColumnResize", "true"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.JurisdictionChooser:
                    aValues = new string[,] { { "FirstLevel", "0" },
                                              { "LastLevel", "4"},
                                              { "HideDisabledLevels", "true"},
                                              { "Level0Label", ""},
                                              { "Level1Label", ""},
                                              { "Level2Label", ""},
                                              { "Level3Label", ""},
                                              { "Level4Label", ""}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.StartingLabelSelector:
                    aValues = new string[,] { { "LabelType", "Other" },
                                              { "Columns", "1"},
                                              { "Rows", "1"},
                                              { "Height", "94"},
                                              { "AlternatingRowBackColor", "false"}};
                    SetDefaultValues(aValues);
                    break;
                case LMP.Data.mpControlTypes.PaperTraySelector:
                    break;
                default:
                    break;
            }
        }
        /// returns delimited string of default values for designated
        /// control type
        /// </summary>
        /// <param name="oType"></param>
        /// <returns></returns>
        public string GetDefaultValues(LMP.Data.mpControlTypes iType)
        {
            LoadDefaultValues(iType);
            m_iCtlType = iType;
            return GetValue();
        }
        /// <summary>
        /// creates delimited string, calls SetValue
        /// </summary>
        /// <param name="aValues"></param>
        private void SetDefaultValues(string[,] aValues)
        {
            StringBuilder oSB = new StringBuilder();

            //create delimited string
            for (int i = 0; i <= aValues.GetUpperBound(0); i++)
            {
                oSB.AppendFormat("{0}={1}�", aValues[i, 0].ToString(),
                    aValues[i, 1].ToString());
            }
            //remove trailing sep
            oSB.Remove(oSB.Length - 1, 1);
            this.SetValue(oSB.ToString());
        }
        /// <summary>
        /// displays input form for prop values - reads
        /// tag property of button instance to determine which
        /// grid value to change
        /// </summary>
        private void ShowPropertyEditForm(int iIndex)
        {
            DataRow oRow = null;
            string xPropValue = null;
            string xPropName = null;
            string[] xValues;

            try
            {
                oRow = m_oDataTable.Rows[iIndex];
            }
            catch { }

            if (oRow != null)
            {
                xPropValue = oRow.ItemArray[1].ToString();
                xPropName = oRow.ItemArray[0].ToString();
            }
            else
            {
                try
                {
                    xPropName = grdProperties.Rows[iIndex].Cells[1].Value.ToString();
                }
                catch {}  
            }
            DialogResult oDR;
            string xValue = "";
            if (m_iCtlType == LMP.Data.mpControlTypes.SegmentChooser && xPropName == "SegmentsList")
            {
                SegmentTreeview oSegForm = new SegmentTreeview(true, xPropValue);
                oDR = oSegForm.ShowDialog();
                xValue = oSegForm.Value;
            }
            else if (m_iCtlType == LMP.Data.mpControlTypes.LocationsDropdown && xPropName == "LocationParentID")
            {
                
                string xRelatedPropName = "LocationType";
                string xRelatedValue = "";
                
                //find the related value for the LocationType prop in the grid DT
                DataTable oDT = (DataTable)this.grdProperties.DataSource;
                DataRow[] oDRow = oDT.Select("Name='LocationType'");
                if (oDRow.Length > 0)
                    xRelatedValue = oDRow[0].ItemArray.GetValue(1).ToString(); 
                
                //message user and exit if the control prop requires a related value
                //and none has been supplied

                if (System.String.IsNullOrEmpty(xRelatedValue))
                {
                    string xMsg = LMP.Resources.
                        GetLangString("Dialog_ControlPropertyLocationsLocationTypeMissing");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    oDR = DialogResult.None;
                }
                else if (xRelatedValue == "1")
                    {
                        string xMsg = LMP.Resources.
                            GetLangString("Dialog_ControlPropertyLocationsNoParentForLocationType");
                        MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        oDR = DialogResult.None;
                    }
                else
                {
                    //show property editing form, passing in the control type belonging to the variable or
                    //segment property, the property name and its value, and the related property name and
                    //its value
                    ControlPropertyEditForm oForm = new ControlPropertyEditForm(m_iCtlType, xPropName,
                        xPropValue, xRelatedPropName, xRelatedValue);
                    oDR = oForm.ShowDialog();
                    xValue = oForm.Value;
                }
            }
            else
            {
                //show property editing form, passing in the control type belonging to the variable or
                //segment property, the property name and its value
                ControlPropertyEditForm oForm = new ControlPropertyEditForm(m_iCtlType, xPropName, xPropValue);
                oDR = oForm.ShowDialog();
                xValue = oForm.Value;
            }
            if (oDR == DialogResult.OK)
            {
                //edit data table row & update properties grid - 
                //this ensures values update without having to tab
                //out of grid cell

                //for StartingLabelSelector, if label type selected, also fill in
                //Rows and Columns property
                if (m_iCtlType == LMP.Data.mpControlTypes.StartingLabelSelector)
                {
                    //further parse xValue and assign value to remaining rows
                    if (iIndex == 0)
                    {
                        xValues = xValue.Split(LMP.StringArray.mpEndOfSubField);
                        for (int i = 0; i < xValues.GetLength(0) ; i++)
                        {
                            m_oDataTable.Rows[i].BeginEdit();
                            m_oDataTable.Rows[i].ItemArray[1] = xValues[i];
                            m_oDataTable.Rows[i].AcceptChanges();
                        }
                    }
                }
                else if (oRow != null)
                {
                    oRow.BeginEdit();
                    oRow.ItemArray[1] = xValue;
                    oRow.AcceptChanges();
                    
                }

                this.grdProperties.Update();

                if (m_iCtlType == LMP.Data.mpControlTypes.StartingLabelSelector)
                {
                    xValues = xValue.Split(LMP.StringArray.mpEndOfSubField);
                    //set related values if label type has been selected
                    if (xValues.GetLength(0) > 1) 
                        for (int i = 0; i < xValues.GetLength(0); i++)
                            this.grdProperties.Rows[i].Cells[2].Value = xValues[i];
                    else
                        this.grdProperties.Rows[iIndex].Cells[2].Value = xValue;
                }
                else
                    //cell 0 belongs to the button column - value col is therefore
                    //cell 2
                    this.grdProperties.Rows[iIndex].Cells[2].Value = xValue;

                
                this.IsDirty = true;
                    
                //notify that value has changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
        }
        /// <summary>
        /// adds DataGridViewButtons to grid
        /// </summary>
        private void AddButtonColumn()
        {
            DataGridViewButtonColumn oButtons = new DataGridViewButtonColumn();
            {
                oButtons.HeaderText = "";
                oButtons.UseColumnTextForButtonValue = false;
                oButtons.AutoSizeMode =
                    DataGridViewAutoSizeColumnMode.NotSet;
                oButtons.FlatStyle = FlatStyle.Standard;
                oButtons.CellTemplate.Style.BackColor = Color.FromKnownColor(KnownColor.ButtonFace);
                oButtons.Text = "...";
                oButtons.UseColumnTextForButtonValue = true;
            }
            //set button width
            this.grdProperties.Columns.Insert(2, oButtons);
            this.grdProperties.Columns[2].Width = 23;
        }
        /// <summary>
        /// Check for any active context menu and close if necessary
        /// </summary>
        private void DismissActiveMenu()
        {
            if (mnuGrid.Visible)
                mnuGrid.Close(ToolStripDropDownCloseReason.AppFocusChange);
        }

        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// handles pressing enter on button column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_EnterKeyPressed(object sender, EnterKeyPressedEventArgs e)
        {
            //GLOG 6450: Add error handling
            try
            {
                if (grdProperties.CurrentCellAddress.X == 0)
                    //button clicked via enter key - display property edit form
                    ShowPropertyEditForm(grdProperties.CurrentCellAddress.Y);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Contents of current cell are being validated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                int iCol = e.ColumnIndex;
                int iRow = e.RowIndex;
                // Verify property name is not blank
                if (iCol == 1 && e.FormattedValue.ToString() == "")
                {
                    if (iRow > m_oDataTable.Rows.Count - 1)
                    {
                        // if we're adding a new row, blank out Value cell as well
                        if (this.grdProperties.Rows[iRow].Cells["Value"].Value.ToString() != "")
                            grdProperties.Rows[iRow].Cells["Value"].Value = null;
                    }
                    else
                    {

                        DialogResult iAnswer = MessageBox.Show(LMP.Resources.GetLangString("Prompt_PropertyDeleteEmptyItem"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (iAnswer == DialogResult.Yes)
                        {
                            m_oDataTable.Rows[iRow].Delete();
                            this.IsDirty = true;
                        }
                        else
                        {
                            // Restore previous cell contents
                            e.Cancel = true;
                            grdProperties.CancelEdit();
                        }
                    }
                }
                else if (iCol == 2 && e.FormattedValue.ToString() != "" &&
                    grdProperties.Rows[iRow].Cells["Name"].Value.ToString() == "")
                {
                    // Can't have a property value without a name
                    // This should only occur if user has cursored into the 2nd column of the add-new row from above
                    if (iRow > m_oDataTable.Rows.Count - 1)
                    {
                        grdProperties.CancelEdit();
                    }
                    else
                    {
                        m_oDataTable.Rows[iRow].Delete();
                        this.IsDirty = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Clicked on menu item to delete property row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuGrid_DeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iRow = grdProperties.CurrentRow.Index;
                if (iRow > -1 && iRow < m_oDataTable.Rows.Count)
                {
                    string xPropName = grdProperties.Rows[iRow].Cells["Name"].Value.ToString();
                    DialogResult iResult = MessageBox.Show(LMP.Resources.GetLangString("Prompt_DeleteControlProperty") + xPropName,
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iResult == DialogResult.Yes)
                    {
                        m_oDataTable.Rows[iRow].Delete();
                        grdProperties.CurrentCell = grdProperties.Rows[Math.Max(iRow, m_oDataTable.Rows.Count - 1)].Cells[1];
                        this.IsDirty = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Click menu item to reload all default values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuGrid_ReloadDefaults_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult iResult = MessageBox.Show(LMP.Resources.GetLangString("Prompt_ReloadControlPropertyDefaults"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iResult == DialogResult.Yes)
                {
                    LoadDefaultValues(m_iCtlType);
                    grdProperties.CurrentCell = grdProperties.Rows[0].Cells["Name"];
                    this.IsDirty = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
        }
        private void grdProperties_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            m_iLastSelectedRowIndex = e.RowIndex;

            if (this.IsDirty && this.ValueChanged != null)
                //notify that value has changed
                ValueChanged(this, new EventArgs());
        }

        /// <summary>
        /// positions ellipse buttons when control is resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlPropertiesGrid_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                //AlignEllipseButtons();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// add ellipse buttons when rows are added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                //AddEllipseButton(this.grdProperties.Rows.Count);
            }

            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// capture scroll offset, used for interpreting ellipse 
        /// button click events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
                    return;

                if (e.Type == ScrollEventType.SmallIncrement)
                    m_iScrollOffset++;
                else if (e.Type == ScrollEventType.SmallDecrement)
                    m_iScrollOffset--;
            }

            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// broadcast that tab was pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdProperties_TabPressed(object sender, TabPressedEventArgs e)
        {
            HandleTabPressed(e.ShiftPressed);
        }
        private void grdProperties_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);
        }
        private void grdProperties_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        /// <summary>
        /// handles ellipse button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //GLOG 6450: Add error handling
            try
            {
                if (e.ColumnIndex == 0)
                    //button pushed - display property edit form
                    ShowPropertyEditForm(e.RowIndex);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Replace default context menu of Editing control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.ContextMenuStrip = this.ContextMenuStrip;
                // Enables cancelling context menu with Esc

                if (m_oPreviewKeyDownEventHandler == null)
                {
                    m_oPreviewKeyDownEventHandler = new PreviewKeyDownEventHandler(ControlPropertiesGrid_PreviewKeyDown);
                    e.Control.PreviewKeyDown += m_oPreviewKeyDownEventHandler;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Mouse has been clicked within a Cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DismissActiveMenu();
                if (e.Button == MouseButtons.Right)
                {
                    // Only display Delete Item if current cell is in a data row
                    if (e.RowIndex > -1 && e.ColumnIndex > -1)
                        this.mnuGrid_DeleteItem.Visible = true;
                    else
                        this.mnuGrid_DeleteItem.Visible = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check for Esc to close context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlPropertiesGrid_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                switch(e.KeyCode)
                {
                    case Keys.Escape:
                        if(mnuGrid.Visible)
                        {
                            DismissActiveMenu();
                        }
                    break;

                    case Keys.F12:
                            ShowPropertyEditForm(this.grdProperties.CurrentRow.Index);
                        break;
                }
                
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

    }
}
