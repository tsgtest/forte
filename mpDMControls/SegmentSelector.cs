using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{

    public partial class SegmentSelector : System.Windows.Forms.UserControl, LMP.Controls.IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private AdminSegmentDef m_oSegDef = null;
        private string m_xSegmentID = "";
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************

        public SegmentSelector()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************methods**************************
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ButtonPressedHandler ButtonPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get 
            {
                if (m_oSegDef != null)
                    return m_oSegDef.ID.ToString();
                else
                    return "";
            }
            set 
            { 
                m_xSegmentID = value;
                if (m_xSegmentID != "" && m_xSegmentID != "0")
                {
                    int iSegmentID1 = 0;
                    int iSegmentID2 = 0;
                    LMP.String.SplitID(m_xSegmentID, out iSegmentID1, out iSegmentID2);
                    AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                    try
                    {
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iSegmentID1);
                        txtDisplayName.Text = oDef.DisplayName;
                        m_oSegDef = oDef;
                    }
                    catch (System.Exception oE)
                    {
                        m_xSegmentID = "";
                        txtDisplayName.Text = "";
                        m_oSegDef = null;
                        LMP.Error.Show(oE, LMP.Resources.GetLangString("Error_InvalidSegmentID" + m_xSegmentID));
                    }
                }
                else
                {
                    txtDisplayName.Text = "";
                    m_xSegmentID = "";
                    m_oSegDef = null;
                }
            }
        }
        public bool IsDirty
        {
            set 
            { 
                m_bIsDirty = value;
            }
            get
            {
                return m_bIsDirty; 
            }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            this.txtDisplayName.SelectAll();
            base.OnGotFocus(e);
        }
        protected override void OnTextChanged(EventArgs e)
        {
            //mark as edited
            m_bIsDirty = true;
            base.OnTextChanged(e);

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        /// <summary>
        /// Disallow typing directly into textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDisplayName_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.F12)
            {
                ShowSegmentTreeViewForm();
            }
        }

        #endregion
        #region *********************event handlers*********************
        private void btnEllipse_Click(object sender, EventArgs e)
        {
            ShowSegmentTreeViewForm();
        }

        public void ShowSegmentTreeViewForm()
        {
            SegmentTreeview oForm = new SegmentTreeview();
            oForm.ShowDialog();
            if (oForm.Value != "" && this.Value != oForm.Value)
            {
                this.Value = oForm.Value;
                this.IsDirty = true;
            }
            this.txtDisplayName.Focus();
        }

        /// <summary>
        /// broadcast that tab was pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDisplayName_TabPressed(object sender, TabPressedEventArgs e)
        {
            //notify subscribers tab was pressed
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }
        #endregion

    }
}
