﻿namespace LMP.Controls
{
    partial class SearchableFolderTreeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchableFolderTreeView));
            this.pnlFind = new System.Windows.Forms.Panel();
            this.chkFindContent = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtFindContent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_Clear = new System.Windows.Forms.MenuItem();
            this.lblFindContent = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.treeResults = new Infragistics.Win.UltraWinTree.UltraTree();
            this.ftvMain = new LMP.Controls.FolderTreeView(this.components);
            this.mnuSegment = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuSegment_AddToFavorites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_RemoveFromFavorites = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFindContentAdvanced = new System.Windows.Forms.Button();
            this.pnlFind.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFindContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ftvMain)).BeginInit();
            this.mnuSegment.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFind
            // 
            this.pnlFind.BackColor = System.Drawing.Color.Transparent;
            this.pnlFind.Controls.Add(this.btnFindContentAdvanced);
            this.pnlFind.Controls.Add(this.chkFindContent);
            this.pnlFind.Controls.Add(this.txtFindContent);
            this.pnlFind.Controls.Add(this.lblFindContent);
            this.pnlFind.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFind.Location = new System.Drawing.Point(0, 0);
            this.pnlFind.Name = "pnlFind";
            this.pnlFind.Size = new System.Drawing.Size(276, 31);
            this.pnlFind.TabIndex = 1;
            // 
            // chkFindContent
            // 
            this.chkFindContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.chkFindContent.Appearance = appearance1;
            this.chkFindContent.BackColor = System.Drawing.Color.Transparent;
            this.chkFindContent.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D;
            this.chkFindContent.ImageTransparentColor = System.Drawing.Color.Black;
            this.chkFindContent.Location = new System.Drawing.Point(229, 4);
            this.chkFindContent.Name = "chkFindContent";
            this.chkFindContent.Size = new System.Drawing.Size(17, 21);
            this.chkFindContent.Style = Infragistics.Win.EditCheckStyle.Custom;
            this.chkFindContent.TabIndex = 6;
            this.chkFindContent.CheckedChanged += new System.EventHandler(this.chkFindContent_CheckedChanged);
            this.chkFindContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkFindContent_KeyDown);
            // 
            // txtFindContent
            // 
            this.txtFindContent.AcceptsReturn = true;
            this.txtFindContent.AlwaysInEditMode = true;
            this.txtFindContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.TextVAlignAsString = "Middle";
            this.txtFindContent.Appearance = appearance2;
            this.txtFindContent.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtFindContent.ContextMenu = this.mnuContext;
            this.txtFindContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFindContent.Location = new System.Drawing.Point(33, 5);
            this.txtFindContent.Multiline = true;
            this.txtFindContent.Name = "txtFindContent";
            this.txtFindContent.Size = new System.Drawing.Size(188, 19);
            this.txtFindContent.TabIndex = 5;
            this.txtFindContent.TextChanged += new System.EventHandler(this.txtFindContent_TextChanged);
            this.txtFindContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFindContent_KeyDown);
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_Clear});
            // 
            // mnuContext_Clear
            // 
            this.mnuContext_Clear.DefaultItem = true;
            this.mnuContext_Clear.Index = 0;
            this.mnuContext_Clear.Text = "&Clear";
            this.mnuContext_Clear.Click += new System.EventHandler(this.mnuContext_Clear_Click);
            // 
            // lblFindContent
            // 
            this.lblFindContent.BackColor = System.Drawing.Color.Transparent;
            this.lblFindContent.Location = new System.Drawing.Point(3, 8);
            this.lblFindContent.Name = "lblFindContent";
            this.lblFindContent.Size = new System.Drawing.Size(32, 15);
            this.lblFindContent.TabIndex = 4;
            this.lblFindContent.Text = "&Find:";
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 31);
            this.splitter1.Margin = new System.Windows.Forms.Padding(0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(276, 1);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // treeResults
            // 
            appearance3.BackColor = System.Drawing.Color.White;
            this.treeResults.Appearance = appearance3;
            this.treeResults.AutoDragExpandDelay = 1000;
            this.treeResults.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.treeResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeResults.HideSelection = false;
            this.treeResults.LeftImagesSize = new System.Drawing.Size(32, 16);
            this.treeResults.Location = new System.Drawing.Point(0, 32);
            this.treeResults.Name = "treeResults";
            appearance4.Image = 1;
            _override1.ExpandedNodeAppearance = appearance4;
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.ItemHeight = 16;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance5.Image = 0;
            _override1.NodeAppearance = appearance5;
            _override1.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.None;
            _override1.NodeSpacingAfter = 2;
            _override1.SelectionType = Infragistics.Win.UltraWinTree.SelectType.SingleAutoDrag;
            this.treeResults.Override = _override1;
            this.treeResults.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeResults.ShowLines = false;
            this.treeResults.ShowRootLines = false;
            this.treeResults.Size = new System.Drawing.Size(276, 471);
            this.treeResults.TabIndex = 3;
            this.treeResults.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.Standard;
            this.treeResults.Visible = false;
            this.treeResults.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeResults_AfterSelect);
            this.treeResults.DoubleClick += new System.EventHandler(this.treeResults_DoubleClick);
            this.treeResults.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeResults_MouseDoubleClick);
            // 
            // ftvMain
            // 
            appearance6.BackColor = System.Drawing.Color.White;
            this.ftvMain.Appearance = appearance6;
            this.ftvMain.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.ftvMain.DisableExcludedTypes = false;
            this.ftvMain.DisplayOptions = ((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions)((((((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.StyleSheets) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets)));
            this.ftvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ftvMain.ExcludeNonMacPacTypes = false;
            this.ftvMain.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvMain.FilterText = "";
            this.ftvMain.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvMain.HideSelection = false;
            this.ftvMain.IsDirty = false;
            this.ftvMain.Location = new System.Drawing.Point(0, 31);
            this.ftvMain.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.ftvMain.Name = "ftvMain";
            _override2.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override2.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override2.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.ftvMain.Override = _override2;
            this.ftvMain.OwnerID = 0;
            this.ftvMain.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.ftvMain.ShowCheckboxes = false;
            this.ftvMain.ShowFindPaths = true;
            this.ftvMain.ShowTopUserFolderNode = false;
            this.ftvMain.Size = new System.Drawing.Size(276, 472);
            this.ftvMain.TabIndex = 2;
            this.ftvMain.TransparentBackground = false;
            this.ftvMain.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.ftvMain_AfterSelect);
            this.ftvMain.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.ftvMain_Scroll);
            this.ftvMain.DoubleClick += new System.EventHandler(this.ftvMain_DoubleClick);
            this.ftvMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ftvMain_MouseDown);
            // 
            // mnuSegment
            // 
            this.mnuSegment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSegment_AddToFavorites,
            this.mnuSegment_RemoveFromFavorites});
            this.mnuSegment.Name = "mnuSegment";
            this.mnuSegment.Size = new System.Drawing.Size(211, 48);
            this.mnuSegment.TimerInterval = 500;
            this.mnuSegment.UseTimer = true;
            // 
            // mnuSegment_AddToFavorites
            // 
            this.mnuSegment_AddToFavorites.Name = "mnuSegment_AddToFavorites";
            this.mnuSegment_AddToFavorites.Size = new System.Drawing.Size(210, 22);
            this.mnuSegment_AddToFavorites.Text = "#Add to Favorites#";
            this.mnuSegment_AddToFavorites.Click += new System.EventHandler(this.mnuSegment_AddToFavorites_Click);
            // 
            // mnuSegment_RemoveFromFavorites
            // 
            this.mnuSegment_RemoveFromFavorites.Name = "mnuSegment_RemoveFromFavorites";
            this.mnuSegment_RemoveFromFavorites.Size = new System.Drawing.Size(210, 22);
            this.mnuSegment_RemoveFromFavorites.Text = "#Remove from Favorites#";
            this.mnuSegment_RemoveFromFavorites.Click += new System.EventHandler(this.mnuSegment_RemoveFromFavorites_Click);
            // 
            // btnFindContentAdvanced
            // 
            this.btnFindContentAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindContentAdvanced.FlatAppearance.BorderSize = 0;
            this.btnFindContentAdvanced.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindContentAdvanced.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindContentAdvanced.Image = ((System.Drawing.Image)(resources.GetObject("btnFindContentAdvanced.Image")));
            this.btnFindContentAdvanced.Location = new System.Drawing.Point(251, 3);
            this.btnFindContentAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.btnFindContentAdvanced.Name = "btnFindContentAdvanced";
            this.btnFindContentAdvanced.Size = new System.Drawing.Size(21, 23);
            this.btnFindContentAdvanced.TabIndex = 8;
            this.btnFindContentAdvanced.Text = "…";
            this.btnFindContentAdvanced.UseVisualStyleBackColor = true;
            this.btnFindContentAdvanced.Click += new System.EventHandler(this.btnFindContentAdvanced_Click);
            // 
            // SearchableFolderTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.Controls.Add(this.treeResults);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.ftvMain);
            this.Controls.Add(this.pnlFind);
            this.Name = "SearchableFolderTreeView";
            this.Size = new System.Drawing.Size(276, 503);
            this.pnlFind.ResumeLayout(false);
            this.pnlFind.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFindContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ftvMain)).EndInit();
            this.mnuSegment.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFind;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkFindContent;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFindContent;
        private System.Windows.Forms.Label lblFindContent;
        private FolderTreeView ftvMain;
        private System.Windows.Forms.Splitter splitter1;
        private Infragistics.Win.UltraWinTree.UltraTree treeResults;
        private System.Windows.Forms.ContextMenu mnuContext;
        private System.Windows.Forms.MenuItem mnuContext_Clear;
        private TimedContextMenuStrip mnuSegment;
        private System.Windows.Forms.ToolStripMenuItem mnuSegment_AddToFavorites;
        private System.Windows.Forms.ToolStripMenuItem mnuSegment_RemoveFromFavorites;
        private System.Windows.Forms.Button btnFindContentAdvanced;
    }
}
