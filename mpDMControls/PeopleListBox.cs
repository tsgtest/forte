using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Data;
using System.Net.NetworkInformation;

namespace LMP.Controls
{
    public partial class PeopleListBox : UserControl
    {
        #region *********************Enumerations*********************
        public enum mpPeopleListSources
        {
            Local = 0,
            Network = 1
        }
        public enum mpPeopleFilterOptions
        {
            Users = 0,
            Authors = 1,
            Attorneys = 2
        }
        #endregion
        #region *********************Fields*********************
        mpPeopleListSources m_iListSource = mpPeopleListSources.Local;
        DataTable m_oNetworkPeople = null;
        LocalPersons m_oLocalPeople = null;
        System.Windows.Forms.ListBox m_oCurList = null;
        bool m_bMultiSelect = false;
        mpPeopleFilterOptions m_iFilterOption = mpPeopleFilterOptions.Users;
        string m_xWhereClause = "";
        string m_xSearchString = "";
        Timer m_oSearchTimer = new Timer();

        #endregion
        #region Constructors
        public PeopleListBox()
        {
            InitializeComponent();
        }

        public PeopleListBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion
        #region *********************Properties*********************
        /// <summary>
        /// Get/Set Source for People List
        /// </summary>
        public mpPeopleListSources ListSource
        {
            get { return m_iListSource; }
            set 
            { 
                if (m_iListSource != value)
                {
                    if (!this.DesignMode && value == mpPeopleListSources.Network && 
                        !NetworkInterface.GetIsNetworkAvailable())
                    {
                        throw new LMP.Exceptions.NetworkException(LMP.Resources.GetLangString("Msg_NoNetworkConnectionAvailable"));
                    }
                    m_iListSource = value;
                    SetControlOptions();
                }
            }
        }
        /// <summary>
        /// Get/Set whether multiple People can be selected
        /// </summary>
        public bool MultipleSelection
        {
            get { return m_bMultiSelect;}
            set
            {
                if (m_bMultiSelect != value)
                {
                    m_bMultiSelect = value;
                    SetControlOptions();
                }
            }
        }
        /// <summary>
        /// Get/Set People Filter Options
        /// </summary>
        public mpPeopleFilterOptions FilterOption
        {
            get { return m_iFilterOption; }
            set
            {
                if (m_iFilterOption != value)
                {
                    m_iFilterOption = value;
                    m_oLocalPeople = null;
                    m_oNetworkPeople = null;
                    SetControlOptions();
                }
            }
        }
        /// <summary>
        /// Get/Set additional SQL Where clause to be used
        /// </summary>
        public string WhereClause
        {
            get { return m_xWhereClause; }
            set
            {
                if (m_xWhereClause != value)
                {
                    m_xWhereClause = value;
                    m_oLocalPeople = null;
                    m_oNetworkPeople = null;
                    SetControlOptions();
                }
            }
        }
        /// <summary>
        /// Get/Set Control Value
        /// </summary>
        public string Value
        {
            get
            {
                if (!this.DesignMode)
                    return GetSelectedPeopleIDs();
                else
                    return "";
            }
            set
            {
                if (!this.DesignMode)
                    SetSelectedPeople(value);
            }
        }
        #endregion
        #region *********************Methods*********************
        private void SetSelectedPeople(string xValue)
        {
            if (m_bMultiSelect)
            {
                string[] aPeople = xValue.Split('|');
                List<string> aList = new List<string>(aPeople);
                // GLOG : 2797 : JAB
                // Provide a collection to store the indices of the checked items.
                // We'll use this to later order the list such that selected items 
                // appear at the top of the list.
                System.Collections.ArrayList alSelectedIndices = new System.Collections.ArrayList();

                for (int i = 0; i < clbPeople.Items.Count; i++)
                {
                    DataRowView oRow = (DataRowView)clbPeople.Items[i];
                    string xID = oRow[1].ToString();

                    if (aList.Contains(xID))
                    {
                        clbPeople.SetItemChecked(i, true);

                        // GLOG : 2797 : JAB
                        // Store the index of this checked item.
                        alSelectedIndices.Add(oRow.Row);

                        aList.Remove(xID);

                        if (aList.Count == 0)
                            break;
                    }
                    //else
                    //{
                    //    clbPeople.SetItemChecked(i, false);
                    //}
                }
                if (clbPeople.CheckedItems.Count > 0)
                {
                    //Ensure first checked item is visible
                    clbPeople.SetSelected(clbPeople.CheckedIndices[0], true);

                    // GLOG : 2797 : JAB
                    // Change the order of the checked people such that they appear first
                    // in the list of people.

                    DataTable oTable = null;

                    for (int i = alSelectedIndices.Count - 1; i >= 0; i--)
                    {
                        DataRow oRow = (DataRow)alSelectedIndices[i];

                        if (oTable == null)
                        {
                            oTable = oRow.Table;
                        }
                        
                        // We'll need the values of the row to restore 
                        // GLOG : 2797 : JAB
                        // The schema of the table varies depending on if the source 
                        // is network or local based.
                        object [] aoRowValues = oRow.ItemArray;

                        // Remove the row and reinsert it at the first row.
                        oTable.Rows.Remove(oRow);
                        oTable.Rows.InsertAt(oRow, 0);

                        // The values must be reset in the rows after they are readded.
                        // GLOG : 2797 : JAB
                        // Use the respective values associated with the appropriate 
                        // table's schema to restore the row values.
                        oRow.ItemArray = aoRowValues;
                    }

                    // Set each item that was moved to the top of the list as checked.
                    for (int i = 0; i < alSelectedIndices.Count; i++)
                    {
                        clbPeople.SetItemChecked(i, true);
                    }

                    //select first item in list
                    clbPeople.SelectedIndex = 0;
                }
            }
            else
                lbPeople.SelectedValue = xValue;
        }
        private void SetControlOptions()
        {
            clbPeople.Visible = m_bMultiSelect;
            lbPeople.Visible = !m_bMultiSelect;
            
            //Display checkboxes if Multi-select
            if (!m_bMultiSelect)
                m_oCurList = lbPeople;
            else
                m_oCurList = (System.Windows.Forms.ListBox)clbPeople;

            if (!this.DesignMode)
            {
                m_oCurList.SuspendLayout();
                if (m_iListSource == mpPeopleListSources.Network)
                {
                    //populate people from network db, if necessary
                    if (m_oNetworkPeople == null)
                        m_oNetworkPeople = LMP.Data.User.GetNetworkUserList();
                    m_oCurList.DataSource = m_oNetworkPeople;
                    m_oCurList.DisplayMember = "DisplayName";
                    m_oCurList.ValueMember = "ID1";
                }
                else
                {
                    if (m_oLocalPeople == null)
                    {
                        if (m_iFilterOption == mpPeopleFilterOptions.Users)
                        {
                            m_oLocalPeople = new LocalPersons(mpPeopleListTypes.Users,
                                LMP.Data.Application.User.ID);
                        }
                        else
                        {
                            mpTriState iAuthor = mpTriState.Undefined;
                            mpTriState iAttorney = mpTriState.Undefined;

                            if (m_iFilterOption == mpPeopleFilterOptions.Authors)
                                iAuthor = mpTriState.True;
                            else if (m_iFilterOption == mpPeopleFilterOptions.Attorneys)
                                iAttorney = mpTriState.True;

                            m_oLocalPeople = new LocalPersons(mpPeopleListTypes.AllPeople, LMP.Data.Application.User.ID,
                                iAuthor, iAttorney, 0, m_xWhereClause);
                        }
                    }
                    DataTable oPeopleDT = m_oLocalPeople.ToDataSet(false).Tables[0];
                    m_oCurList.DataSource = oPeopleDT;
                    m_oCurList.DisplayMember = "DisplayName";
                    m_oCurList.ValueMember = "ID1";
                }
                m_oCurList.ResumeLayout();
                m_oCurList.Refresh();
            }
        }
        private string GetSelectedPeopleIDs()
        {
            if (m_bMultiSelect)
            {
                //Return selected IDs in pipe-delimited string 
                string xTemp = "";
                for (int i = 0; i < clbPeople.CheckedIndices.Count; i++ )
                {
                    DataRowView oRow = (DataRowView)clbPeople.Items[clbPeople.CheckedIndices[i]];
                    xTemp = xTemp + oRow["ID1"].ToString();
                    if (i < clbPeople.CheckedItems.Count - 1)
                        xTemp = xTemp + "|";
                }
                return xTemp;
            }
            else
                //Return single ID
                return this.lbPeople.SelectedValue.ToString();
        }

        /// <summary>
        /// GLOG : 2797 : JAB
        /// Get a pipe-delimited string of all the IDs.
        /// </summary>
        /// <returns></returns>
        public string AllPeopleIDs
        {
            get
            {
                //Return all the IDs in a pipe-delimited string 
                DataTable oTable = (DataTable)m_oCurList.DataSource;
                StringBuilder sbIds = new StringBuilder();

                foreach (DataRow oRow in oTable.Rows)
                {
                    if (sbIds.Length > 0)
                    {
                        sbIds.Append("|");
                    }

                    sbIds.Append(oRow["ID1"].ToString());
                }

                return sbIds.ToString();
            }
        }

        #endregion
        #region *********************Events*********************
        private void PeopleListBox_Load(object sender, EventArgs e)
        {
            try
            {
                SetControlOptions();
                m_oSearchTimer.Tick += m_oSearchTimer_Tick;
                m_oSearchTimer.Interval = 300;
                m_oSearchTimer.Stop();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void m_oSearchTimer_Tick(object sender, EventArgs e)
        {
            //Clear incremental search string after time out
            m_xSearchString = "";
            m_oSearchTimer.Stop();
        }
        #endregion

        private void PeopleListBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //Perform incremental search
                m_oSearchTimer.Stop();
                m_oSearchTimer.Start();
                m_xSearchString += e.KeyChar.ToString();
                int iMatch = m_oCurList.FindString(m_xSearchString);
                if (iMatch > -1)
                {
                    m_oCurList.SelectedIndex = iMatch;
                }
                e.Handled = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

    }
}
