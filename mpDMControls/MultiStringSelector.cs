using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{

    public partial class MultiStringSelector : System.Windows.Forms.UserControl, LMP.Controls.IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private string m_xTitle = "";
        private string[] m_aList = null;
        private string m_xDescription = "";
        private string m_xSeparator = ",";
        private string m_xValue = "";
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************

        public MultiStringSelector(string [] aList, string xTitle, string xDescription)
        {
            m_aList = aList;
            m_xTitle = xTitle;
            m_xDescription = xDescription;
            InitializeComponent();
        }
        #endregion
        #region *********************properties**************************
        public string Separator
        {
            get { return m_xSeparator; }
            set { m_xSeparator = value; }
        }
        #endregion
        #region *********************methods**************************
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ButtonPressedHandler ButtonPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get 
            {
                if (m_xValue != null)
                    return m_xValue;
                else
                    return "";
            }
            set 
            {
                if (m_xValue != value && value != null)
                {
                    m_xValue = value;
                    this.txtSelected.Text = value;
                }
            }
        }
        public bool IsDirty
        {
            set 
            { 
                m_bIsDirty = value;
            }
            get
            {
                return m_bIsDirty; 
            }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            this.txtSelected.SelectAll();
            base.OnGotFocus(e);
        }
        protected override void OnTextChanged(EventArgs e)
        {
            //mark as edited
            m_bIsDirty = true;
            base.OnTextChanged(e);

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        /// <summary>
        /// Disallow typing directly into textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSelected_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ShowMultiStringListBox();
            }
            e.SuppressKeyPress = true;
        }

        #endregion
        #region *********************event handlers*********************
        private void btnEllipse_Click(object sender, EventArgs e)
        {
            ShowMultiStringListBox();
        }

        private void ShowMultiStringListBox()
        {
            MultiStringListBox oForm = new MultiStringListBox(m_aList, m_xTitle, m_xDescription);
            oForm.Value = this.Value;
            oForm.Separator = this.Separator;
            oForm.ShowDialog();
            if (oForm.DialogResult == DialogResult.OK && (oForm.Value != this.Value))
            {
                this.Value = oForm.Value;
                this.IsDirty = true;
            }
            this.txtSelected.Focus();
        }

        /// <summary>
        /// broadcast that tab was pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSelected_TabPressed(object sender, TabPressedEventArgs e)
        {
            //notify subscribers tab was pressed
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }
        #endregion

    }
}
