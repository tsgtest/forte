using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class MultiStringListBox : Form
    {
        private string m_xValue = "";
        private string m_xSeparator = ",";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aListItems"></param>
        /// <param name="xTitle"></param>
        /// <param name="xDescription"></param>
        public MultiStringListBox(string[] aListItems, string xTitle, string xDescription)
        {
            InitializeComponent();
            if (aListItems != null)
            {
                for (int i = 0; i < aListItems.GetUpperBound(0); i++)
                {
                    lbItems.Items.Add(aListItems[i]);
                }
            }
            //Set titlebar
            this.Text = xTitle;
            //Set descriptive label
            this.lblDescription.Text = xDescription;
        }
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            m_xValue = "";
            //Build string of all checked items separated by m_xSeparator
            for (int i = 0; i < lbItems.CheckedItems.Count; i++)
            {
                m_xValue += lbItems.CheckedItems[i].ToString();
                if (i < lbItems.CheckedItems.Count - 1)
                    m_xValue += this.Separator;
            }
            this.DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// Get/Set value of control
        /// </summary>
        public string Value
        {
            get { return m_xValue; }
            set 
            {
                if (m_xValue != value)
                {
                    m_xValue = value;
                    if (m_xValue != null)
                    {
                        string[] aList = m_xValue.Split(this.Separator.ToCharArray());
                        //Check list items includes in delimited string
                        for (int i = 0; i <= aList.GetUpperBound(0); i++)
                        {
                            int iIndex = lbItems.FindStringExact(aList[i]);
                            if (iIndex > -1)
                            {
                                lbItems.SetItemChecked(iIndex, true);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Get/Set the separator used in the return value
        /// </summary>
        public string Separator
        {
            get { return m_xSeparator; }
            set { m_xSeparator = value; }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}