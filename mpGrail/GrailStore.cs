﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Reflection;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Grail
{
    public static class GrailStore
    {
        private static GrailBackend m_oGrailBackend;
        /// <summary>
        /// Return true of BackEnd object is successfully created
        /// </summary>
        public static bool IsConfigured
        {
            get
            {
                //retrieve assembly and class name of object to create
                string xAsmName = LMP.Registry.GetMacPac10Value("GrailAssemblyName");
                string xClassName = LMP.Registry.GetMacPac10Value("GrailClassName");

                if (string.IsNullOrEmpty(xAsmName) && string.IsNullOrEmpty(xClassName))
                {
                    return false;
                }
                else
                {
                    return LoadBackendIfNecessary(true);
                }
            }
        }
        /// <summary>
        /// Instantiate Grail Backend object using Registry values,
        /// optionally suppressing errors about missing values
        /// </summary>
        /// <param name="bShowErrors"></param>
        /// <returns></returns>
        private static bool LoadBackendIfNecessary(bool bShowErrors)
        {
            try
            {
                if (m_oGrailBackend == null)
                {
                    //retrieve assembly and class name of object to create
                    string xAsmName = LMP.Registry.GetMacPac10Value("GrailAssemblyName");
                    string xClassName = LMP.Registry.GetMacPac10Value("GrailClassName");

                    if (string.IsNullOrEmpty(xAsmName))
                    {
                        throw new LMP.Exceptions.RegistryKeyException(
                            LMP.Resources.GetLangString("Msg_MissingAsmNameRegKey"));
                    }
                    else if (string.IsNullOrEmpty(xClassName))
                    {
                        throw new LMP.Exceptions.RegistryKeyException(
                            LMP.Resources.GetLangString("Msg_MissingClassNameRegKey"));
                    }

                    string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAsmName;

                    Assembly oAsm = null;

                    try
                    {
                        //load assembly
                        oAsm = Assembly.LoadFrom(xAsmDllFullName);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ResourceException(
                            LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
                    }

                    try
                    {
                        //create class instance
                        m_oGrailBackend = (GrailBackend)oAsm.CreateInstance(xClassName);

                        if (m_oGrailBackend == null)
                        {
                            throw new LMP.Exceptions.ResourceException(
                                LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                        }
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ResourceException(
                            LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
                    }
                }
            }
            catch (System.Exception oE)
            {
                if (bShowErrors)
                    throw oE;
            }
            return m_oGrailBackend != null;
        }
        public static LMP.Grail.GrailBackend Backend
        {
            get
            {
                if (LoadBackendIfNecessary(true))
                    return m_oGrailBackend;
                else
                    return null;
            }
        }
    }
}
