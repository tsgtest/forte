﻿namespace LMP.Grail
{
    partial class ClientMatterLookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientMatterLookup));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkRestartNumbering = new System.Windows.Forms.CheckBox();
            this.chkKeepExisting = new System.Windows.Forms.CheckBox();
            this.chkSeparateSection = new System.Windows.Forms.CheckBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.cmbLocation = new System.Windows.Forms.ComboBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblClients = new System.Windows.Forms.Label();
            this.lblFindContent = new System.Windows.Forms.Label();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdClients = new System.Windows.Forms.DataGridView();
            this.lblMatters = new System.Windows.Forms.Label();
            this.MatterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MatterNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdMatters = new System.Windows.Forms.DataGridView();
            this.btnFind = new System.Windows.Forms.Button();
            this.grpOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMatters)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(469, 340);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(388, 340);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "Search");
            // 
            // grpOptions
            // 
            this.grpOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOptions.Controls.Add(this.chkRestartNumbering);
            this.grpOptions.Controls.Add(this.chkKeepExisting);
            this.grpOptions.Controls.Add(this.chkSeparateSection);
            this.grpOptions.Location = new System.Drawing.Point(334, 84);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(215, 97);
            this.grpOptions.TabIndex = 10;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Insertion Options";
            // 
            // chkRestartNumbering
            // 
            this.chkRestartNumbering.AutoSize = true;
            this.chkRestartNumbering.Location = new System.Drawing.Point(16, 68);
            this.chkRestartNumbering.Name = "chkRestartNumbering";
            this.chkRestartNumbering.Size = new System.Drawing.Size(142, 17);
            this.chkRestartNumbering.TabIndex = 6;
            this.chkRestartNumbering.Text = "&Restart Page Numbering";
            this.chkRestartNumbering.UseVisualStyleBackColor = true;
            // 
            // chkKeepExisting
            // 
            this.chkKeepExisting.AutoSize = true;
            this.chkKeepExisting.Location = new System.Drawing.Point(16, 44);
            this.chkKeepExisting.Name = "chkKeepExisting";
            this.chkKeepExisting.Size = new System.Drawing.Size(173, 17);
            this.chkKeepExisting.TabIndex = 5;
            this.chkKeepExisting.Text = "Keep &Existing Headers/Footers";
            this.chkKeepExisting.UseVisualStyleBackColor = true;
            // 
            // chkSeparateSection
            // 
            this.chkSeparateSection.AutoSize = true;
            this.chkSeparateSection.Location = new System.Drawing.Point(16, 22);
            this.chkSeparateSection.Name = "chkSeparateSection";
            this.chkSeparateSection.Size = new System.Drawing.Size(148, 17);
            this.chkSeparateSection.TabIndex = 4;
            this.chkSeparateSection.Text = "Insert in &Separate Section";
            this.chkSeparateSection.UseVisualStyleBackColor = true;
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(332, 42);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(94, 13);
            this.lblLocation.TabIndex = 9;
            this.lblLocation.Text = "Insertion &Location:";
            // 
            // cmbLocation
            // 
            this.cmbLocation.FormattingEnabled = true;
            this.cmbLocation.Location = new System.Drawing.Point(334, 58);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(215, 21);
            this.cmbLocation.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Number";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 155;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Number";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // lblClients
            // 
            this.lblClients.BackColor = System.Drawing.Color.Transparent;
            this.lblClients.Location = new System.Drawing.Point(9, 42);
            this.lblClients.Name = "lblClients";
            this.lblClients.Size = new System.Drawing.Size(312, 13);
            this.lblClients.TabIndex = 3;
            this.lblClients.Text = "&Clients:";
            // 
            // lblFindContent
            // 
            this.lblFindContent.BackColor = System.Drawing.Color.Transparent;
            this.lblFindContent.Location = new System.Drawing.Point(10, 16);
            this.lblFindContent.Name = "lblFindContent";
            this.lblFindContent.Size = new System.Drawing.Size(32, 15);
            this.lblFindContent.TabIndex = 0;
            this.lblFindContent.Text = "&Find:";
            // 
            // txtFind
            // 
            this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFind.Location = new System.Drawing.Point(42, 13);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(239, 20);
            this.txtFind.TabIndex = 1;
            this.txtFind.Leave += new System.EventHandler(this.txtFind_Leave);
            this.txtFind.Enter += new System.EventHandler(this.txtFind_Enter);
            // 
            // ClientName
            // 
            this.ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ClientName.HeaderText = "ClientName";
            this.ClientName.Name = "ClientName";
            this.ClientName.ReadOnly = true;
            // 
            // Number
            // 
            this.Number.HeaderText = "Number";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            // 
            // grdClients
            // 
            this.grdClients.AllowUserToAddRows = false;
            this.grdClients.AllowUserToDeleteRows = false;
            this.grdClients.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grdClients.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdClients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdClients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdClients.BackgroundColor = System.Drawing.Color.White;
            this.grdClients.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdClients.CausesValidation = false;
            this.grdClients.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.grdClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdClients.ColumnHeadersVisible = false;
            this.grdClients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.ClientName});
            this.grdClients.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdClients.GridColor = System.Drawing.Color.LightGray;
            this.grdClients.Location = new System.Drawing.Point(8, 58);
            this.grdClients.MultiSelect = false;
            this.grdClients.Name = "grdClients";
            this.grdClients.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdClients.RowHeadersVisible = false;
            this.grdClients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdClients.ShowCellToolTips = false;
            this.grdClients.ShowEditingIcon = false;
            this.grdClients.Size = new System.Drawing.Size(313, 123);
            this.grdClients.StandardTab = true;
            this.grdClients.TabIndex = 4;
            this.grdClients.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdClients_RowEnter);
            // 
            // lblMatters
            // 
            this.lblMatters.BackColor = System.Drawing.Color.Transparent;
            this.lblMatters.Location = new System.Drawing.Point(9, 186);
            this.lblMatters.Name = "lblMatters";
            this.lblMatters.Size = new System.Drawing.Size(312, 13);
            this.lblMatters.TabIndex = 5;
            this.lblMatters.Text = "&Matters:";
            // 
            // MatterName
            // 
            this.MatterName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MatterName.HeaderText = "Name";
            this.MatterName.Name = "MatterName";
            this.MatterName.ReadOnly = true;
            // 
            // MatterNumber
            // 
            this.MatterNumber.HeaderText = "Number";
            this.MatterNumber.Name = "MatterNumber";
            this.MatterNumber.ReadOnly = true;
            // 
            // grdMatters
            // 
            this.grdMatters.AllowUserToAddRows = false;
            this.grdMatters.AllowUserToDeleteRows = false;
            this.grdMatters.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grdMatters.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdMatters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMatters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdMatters.BackgroundColor = System.Drawing.Color.White;
            this.grdMatters.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdMatters.CausesValidation = false;
            this.grdMatters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMatters.ColumnHeadersVisible = false;
            this.grdMatters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MatterNumber,
            this.MatterName});
            this.grdMatters.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdMatters.GridColor = System.Drawing.Color.LightGray;
            this.grdMatters.Location = new System.Drawing.Point(8, 202);
            this.grdMatters.Name = "grdMatters";
            this.grdMatters.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdMatters.RowHeadersVisible = false;
            this.grdMatters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMatters.ShowCellToolTips = false;
            this.grdMatters.ShowEditingIcon = false;
            this.grdMatters.Size = new System.Drawing.Size(313, 123);
            this.grdMatters.StandardTab = true;
            this.grdMatters.TabIndex = 6;
            this.grdMatters.DoubleClick += new System.EventHandler(this.grdMatters_DoubleClick);
            this.grdMatters.SelectionChanged += new System.EventHandler(this.grdMatters_SelectionChanged);
            // 
            // btnFind
            // 
            this.btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFind.ImageIndex = 0;
            this.btnFind.ImageList = this.ilImages;
            this.btnFind.Location = new System.Drawing.Point(288, 11);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(34, 24);
            this.btnFind.TabIndex = 2;
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // ClientMatterLookup
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(557, 377);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.grdMatters);
            this.Controls.Add(this.lblMatters);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.grdClients);
            this.Controls.Add(this.txtFind);
            this.Controls.Add(this.lblFindContent);
            this.Controls.Add(this.lblClients);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClientMatterLookup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lookup Client Matter";
            this.Load += new System.EventHandler(this.ClientMatterLookup_Load);
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMatters)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNameCol;
        private System.Windows.Forms.ImageList ilImages;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.CheckBox chkRestartNumbering;
        private System.Windows.Forms.CheckBox chkKeepExisting;
        private System.Windows.Forms.CheckBox chkSeparateSection;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.ComboBox cmbLocation;
        private System.Windows.Forms.Label lblClients;
        private System.Windows.Forms.Label lblFindContent;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridView grdClients;
        private System.Windows.Forms.Label lblMatters;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNumber;
        private System.Windows.Forms.DataGridView grdMatters;
        private System.Windows.Forms.Button btnFind;
    }
}