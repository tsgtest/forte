﻿namespace LMP.Grail
{
    partial class ClientMatterLookupControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientMatterLookupControl));
            this.lblFindContent = new System.Windows.Forms.Label();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.grdClients = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblClients = new System.Windows.Forms.Label();
            this.lblMatters = new System.Windows.Forms.Label();
            this.grdMatters = new System.Windows.Forms.DataGridView();
            this.MatterNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MatterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.btnFind = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMatters)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFindContent
            // 
            this.lblFindContent.BackColor = System.Drawing.Color.Transparent;
            this.lblFindContent.Location = new System.Drawing.Point(15, 25);
            this.lblFindContent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFindContent.Name = "lblFindContent";
            this.lblFindContent.Size = new System.Drawing.Size(48, 23);
            this.lblFindContent.TabIndex = 0;
            this.lblFindContent.Text = "&Find:";
            // 
            // txtFind
            // 
            this.txtFind.Location = new System.Drawing.Point(63, 20);
            this.txtFind.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(356, 26);
            this.txtFind.TabIndex = 1;
            this.txtFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFind_KeyDown);
            // 
            // grdClients
            // 
            this.grdClients.AllowUserToAddRows = false;
            this.grdClients.AllowUserToDeleteRows = false;
            this.grdClients.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grdClients.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.grdClients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdClients.BackgroundColor = System.Drawing.Color.White;
            this.grdClients.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdClients.CausesValidation = false;
            this.grdClients.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.grdClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdClients.ColumnHeadersVisible = false;
            this.grdClients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.ClientName});
            this.grdClients.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdClients.GridColor = System.Drawing.Color.LightGray;
            this.grdClients.Location = new System.Drawing.Point(12, 89);
            this.grdClients.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grdClients.MultiSelect = false;
            this.grdClients.Name = "grdClients";
            this.grdClients.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdClients.RowHeadersVisible = false;
            this.grdClients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdClients.ShowCellToolTips = false;
            this.grdClients.ShowEditingIcon = false;
            this.grdClients.Size = new System.Drawing.Size(470, 189);
            this.grdClients.StandardTab = true;
            this.grdClients.TabIndex = 4;
            this.grdClients.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdClients_RowEnter);
            // 
            // Number
            // 
            this.Number.HeaderText = "Number";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            // 
            // ClientName
            // 
            this.ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ClientName.HeaderText = "ClientName";
            this.ClientName.Name = "ClientName";
            this.ClientName.ReadOnly = true;
            // 
            // lblClients
            // 
            this.lblClients.BackColor = System.Drawing.Color.Transparent;
            this.lblClients.Location = new System.Drawing.Point(14, 65);
            this.lblClients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClients.Name = "lblClients";
            this.lblClients.Size = new System.Drawing.Size(468, 20);
            this.lblClients.TabIndex = 3;
            this.lblClients.Text = "&Clients:";
            // 
            // lblMatters
            // 
            this.lblMatters.BackColor = System.Drawing.Color.Transparent;
            this.lblMatters.Location = new System.Drawing.Point(14, 286);
            this.lblMatters.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMatters.Name = "lblMatters";
            this.lblMatters.Size = new System.Drawing.Size(468, 20);
            this.lblMatters.TabIndex = 5;
            this.lblMatters.Text = "&Matters:";
            // 
            // grdMatters
            // 
            this.grdMatters.AllowUserToAddRows = false;
            this.grdMatters.AllowUserToDeleteRows = false;
            this.grdMatters.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grdMatters.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.grdMatters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdMatters.BackgroundColor = System.Drawing.Color.White;
            this.grdMatters.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdMatters.CausesValidation = false;
            this.grdMatters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMatters.ColumnHeadersVisible = false;
            this.grdMatters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MatterNumber,
            this.MatterName});
            this.grdMatters.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdMatters.GridColor = System.Drawing.Color.LightGray;
            this.grdMatters.Location = new System.Drawing.Point(12, 311);
            this.grdMatters.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grdMatters.Name = "grdMatters";
            this.grdMatters.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdMatters.RowHeadersVisible = false;
            this.grdMatters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMatters.ShowCellToolTips = false;
            this.grdMatters.ShowEditingIcon = false;
            this.grdMatters.Size = new System.Drawing.Size(470, 189);
            this.grdMatters.StandardTab = true;
            this.grdMatters.TabIndex = 6;
            this.grdMatters.SelectionChanged += new System.EventHandler(this.grdMatters_SelectionChanged);
            this.grdMatters.DoubleClick += new System.EventHandler(this.grdMatters_DoubleClick);
            // 
            // MatterNumber
            // 
            this.MatterNumber.HeaderText = "Number";
            this.MatterNumber.Name = "MatterNumber";
            this.MatterNumber.ReadOnly = true;
            // 
            // MatterName
            // 
            this.MatterName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MatterName.HeaderText = "Name";
            this.MatterName.Name = "MatterName";
            this.MatterName.ReadOnly = true;
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "Search");
            // 
            // btnFind
            // 
            this.btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFind.ImageIndex = 0;
            this.btnFind.ImageList = this.ilImages;
            this.btnFind.Location = new System.Drawing.Point(430, 15);
            this.btnFind.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(51, 37);
            this.btnFind.TabIndex = 2;
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Number";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 155;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Number";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // ClientMatterLookupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grdClients);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.grdMatters);
            this.Controls.Add(this.lblMatters);
            this.Controls.Add(this.txtFind);
            this.Controls.Add(this.lblFindContent);
            this.Controls.Add(this.lblClients);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ClientMatterLookupControl";
            this.Size = new System.Drawing.Size(500, 525);
            this.Load += new System.EventHandler(this.ClientMatterLookup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMatters)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFindContent;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.DataGridView grdClients;
        private System.Windows.Forms.Label lblClients;
        private System.Windows.Forms.Label lblMatters;
        //private System.Windows.Forms.DataGridViewTextBoxColumn NumberCol;
        //private System.Windows.Forms.DataGridViewTextBoxColumn ClientNameCol;
        private System.Windows.Forms.DataGridView grdMatters;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        //private System.Windows.Forms.DataGridViewTextBoxColumn MatterNumberCol;
        //private System.Windows.Forms.DataGridViewTextBoxColumn MatterNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterName;
        private System.Windows.Forms.ImageList ilImages;
        private System.Windows.Forms.Button btnFind;
    }
}