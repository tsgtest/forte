﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;

namespace LMP.Grail
{
    public delegate void MatterDoubleClickedHandler(object sender, EventArgs e);
    public delegate void ClientMatterChangedHandler(object sender, ClientMatterChangedEventArgs e);
 
    public partial class ClientMatterLookupControl : UserControl
    {
        #region***********************************enumerations***********************************
        public enum mpClientMatterLookupFormat
        {
            ClientMatter,
            CaseNumber
        }
        #endregion
        #region***********************************fields***********************************
        public event MatterDoubleClickedHandler MatterDoubleClicked;
        public event ClientMatterChangedHandler ClientMatterChanged;

        private LMP.Grail.GrailBackend m_oBackend;
        private Clients oClients;
        private Matters oMatters;
        private string m_xFilter;
        #endregion
        #region***********************************constructors***********************************
        public ClientMatterLookupControl()
        {
            InitializeComponent();

            if (!LMP.Controls.Utilities.IsInDesignMode(this))
            {
                this.grdClients.AutoGenerateColumns = false;
                this.grdMatters.AutoGenerateColumns = false;

                try
                {
                    m_oBackend = GrailStore.Backend;
                    m_oBackend.Connect(null);
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE, "Could not connect to grail backend store.");
                    return;
                }
            }
        }
        #endregion
        #region ***********************************properties***********************************
        public Client Client
        {
            get
            {
                if (this.grdClients.SelectedRows.Count > 0)
                {
                    return oClients[this.grdClients.SelectedRows[0].Index];
                }
                else
                {
                    return null;
                }
            }
        }
        public Matter Matter
        {
            get
            {
                if (this.grdMatters.SelectedRows.Count > 0)
                {
                    return oMatters[this.grdMatters.SelectedRows[0].Index];
                }
                else
                {
                    return null;
                }
            }
        }
        private string ClientFilter
        {
            get
            {
                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //returns what's to the left of the '.' -
                    //if '.' does not exist, returns entire filter
                    if (this.Filter.Contains('.'))
                    {
                        int iPos = this.Filter.IndexOf('.');
                        return this.Filter.Substring(0, iPos);
                    }
                    else
                    {
                        return this.Filter;
                    }
                }
                else
                {
                    return "";
                }
            }
        }
        private string MatterFilter
        {
            get
            {
                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //returns the substring to the right of the '.' 
                    //if it exists - else returns null
                    if (this.Filter.Contains('.'))
                    {
                        int iPos = this.Filter.IndexOf('.');
                        return this.Filter.Substring(iPos + 1);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    //in case number format, filter on matter number
                    return this.Filter;
                }
            }
        }
        public string Filter
        {
            get { return m_xFilter; }

            set
            {
                m_xFilter = value;
            }
        }
        public GrailBackend.mpGrailClientMatterDisplayFormat DisplayFormat
        {
            get { return m_oBackend.DisplayFormat; }
        }
        /// <summary>
        /// true iff matters were searched
        /// </summary>
        private bool MattersSearched { get; set; }
        #endregion
        #region************************************methods***********************************
        /// <summary>
        /// loads clients into the clients grid
        /// </summary>
        /// <returns></returns>
        private int LoadClients()
        {
            oClients = m_oBackend.GetClients(this.ClientFilter, SortField.Name);
            DataTable oDT = oClients.ToDataTable();
            this.grdClients.DataSource = oDT;
            this.grdClients.Columns[0].DataPropertyName = "Number";

            if (!this.m_oBackend.ShowNameColumn)
            {
                this.grdClients.Columns[1].Visible = false;
            }
            else
            {
                this.grdClients.Columns[1].DataPropertyName = "ClientName";
            }

            if (oClients.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oClients.Count +
                    " - Max displayed):";
            }
            else
            {
                this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oClients.Count + "):";
            }

            return oClients.Count;
        }
        /// <summary>
        /// loads matters of the specified client into the matters grid
        /// </summary>
        /// <param name="xMatterFilter"></param>
        /// <param name="oClient"></param>
        /// <returns></returns>
        private int LoadMatters(string xMatterFilter, Client oClient)
        {
            oMatters = oClient.GetMatters(xMatterFilter);
            DataTable oDT = oMatters.ToDataTable();
            this.grdMatters.DataSource = oDT;
            this.grdMatters.Columns[0].DataPropertyName = "Number";

            if (!this.m_oBackend.ShowNameColumn)
            {
                this.grdMatters.Columns[1].Visible = false;
            }
            else
            {
                this.grdMatters.Columns[1].DataPropertyName = "MatterName";
            }

            if (oMatters.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count +
                    " - Max displayed):";
            }
            else
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + "):";
            }

            return oMatters.Count;
        }
        /// <summary>
        /// loads matters into the matters grid -
        /// independent of client
        /// </summary>
        /// <param name="xMatterFilter"></param>
        /// <returns></returns>
        private int LoadMatters(string xMatterFilter)
        {
            oMatters = m_oBackend.GetMatters(xMatterFilter, SortField.Name);
            DataTable oDT = oMatters.ToDataTable();
            this.grdMatters.DataSource = oDT;
            this.grdMatters.Columns[0].DataPropertyName = "Number";
            this.grdMatters.Columns[1].DataPropertyName = "MatterName";

            if (oMatters.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + " - Maximum displayed):";
            }
            else
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + "):";
            }

            return oMatters.Count;
        }
        /// <summary>
        /// Filters grids based on specified filter string -
        /// supports filtering on both Clients and Matters
        /// </summary>
        private void ExecuteFilter()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.Filter = this.txtFind.Text;

                int iClients = 0;

                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //load clients based on filter
                    iClients = LoadClients();
                }

                //search matters if filter is not numeric
                //and no clients were found
                if (iClients == 0 && !String.IsNumericInt32(this.ClientFilter))
                {
                    MattersSearched = true;

                    //no clients were returned by a client filter that was text -
                    //search for matters that match the filter
                    int iMatters = LoadMatters(this.MatterFilter);

                    this.grdMatters.Select();
                }
                else
                {
                    this.grdClients.Select();
                    MattersSearched = false;
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion
        #region ***********************************event handlers***********************************
        private void ClientMatterLookup_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_oBackend != null)
                {
                    if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.CaseNumber)
                    {
                        //position matters grid to span both client and matters grids
                        this.grdMatters.Height = (this.grdMatters.Top + this.grdMatters.Height) - this.grdClients.Top;
                        this.grdMatters.Top = this.grdClients.Top;
                        this.lblMatters.Top = this.lblClients.Top;

                        //hide clients controls
                        this.grdClients.Visible = false;
                        this.lblClients.Visible = false;

                        //rename clients list to case number
                        this.lblMatters.Text = m_oBackend.MatterLabelText + ":";

                        this.Text = LMP.Resources.GetLangString("Dlg_ClientMatterLookupCaseNumberFormatText");
                    }
                    else
                    {
                        this.Text = LMP.Resources.GetLangString("Dlg_ClientMatterLookupClientMatterFormatText");
                    }

                    this.txtFind.Text = this.Filter;

                    this.grdMatters.Columns[0].Visible = this.m_oBackend.ShowIDColumn;
                    this.grdMatters.Columns[0].DataPropertyName = "ClientNumber";
                    this.grdMatters.Columns[1].Visible = this.m_oBackend.ShowNameColumn;
                    this.grdMatters.Columns[1].DataPropertyName = "ClientName";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdClients_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((m_oBackend != null) && !MattersSearched)
                {
                    //load matters for the selected client
                    int iMatters = LoadMatters(this.MatterFilter, oClients[e.RowIndex]);

                    //broadcast matters loaded;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                ExecuteFilter();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdMatters_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    Clients oTempClients = null;

                    DataGridViewSelectedRowCollection oSelRows = this.grdMatters.SelectedRows;

                    //display client of selected matter if matters were searched
                    if (MattersSearched && oSelRows.Count > 0)
                    {
                        oTempClients = m_oBackend.GetClients(oMatters[oSelRows[0].Index].ClientID, SortField.Name);
                        DataTable oDT = oTempClients.ToDataTable();
                        this.grdClients.DataSource = oDT;
                        this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oTempClients.Count + "):";
                    }
                }

                if (this.ClientMatterChanged != null)
                {
                    //broadcast client matter changed
                    this.ClientMatterChanged(this, new ClientMatterChangedEventArgs(this.Client, this.Matter));
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdMatters_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //broadcast matters double-clicked
                this.MatterDoubleClicked(this, new EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ExecuteFilter();
                    e.Handled = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }

    public class ClientMatterChangedEventArgs
    {
        public Client Client { get; set; }
        public Matter Matter { get; set; }
        public ClientMatterChangedEventArgs(Client Client, Matter Matter)
        {
            this.Client = Client;
            this.Matter = Matter;
        }
    }

}
