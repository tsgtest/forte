﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMP.Grail
{
    public enum SortField
    {
        ID = 1,
        Name = 2
    }

    public interface IGrailStore
    {
        //connects to the backend data store -
        //connection data array can be any data
        //required by the connection for the store -
        //such data is store-specific
        void Connect(string[] aConnectionData);
        void Disconnect();
        bool IsConnected { get; }
        bool RemoveAutomationAfterInsertion { get; }

        int MaxRecordsRetrievable { get; }
        int ClientsCount(string xClientNameFilter, string xAdditionalFilter);
        Clients GetClients(string xClientNameFilter, string xAdditionalFilter, SortField iSortBy);
        Clients GetClients(string xClientNameFilter, SortField iSortBy);
        Clients GetClients(SortField iSortBy);

        int MattersCount(Client oClient, string xMatterFilter, string xAdditionalFilter); 
        Matters GetMatters(Client oClient, string xMatterFilter, string xAdditionalFilter, SortField iSortBy);
        Matters GetMatters(Client oClient, string xMatterFilter, SortField iSortBy);
        Matters GetMatters(Client oClient, SortField iSortBy);
        Matters GetMatters(string xMatterFilter, SortField iSortBy);

        bool IsValidClientMatterNumber(string xClientID, string xMatterID);

        //returns the source value of the specified MacPac variable
        //for the specified ID
        string GetData(string xID, params string[] xMacPacVariableName);
        string GetData(int iID, params string[] xMacPacVariableName);

        //returns an xml document string of the format 
        //<GrailDatum name='MacPacVariableName' value='Value'/>
        //for all mapped variables for the specified ID - 
        //e.g. ID can be any identifier for data - Client/Matter number
        string GetData(string xID);
        string GetData(int iID);
    }
}
