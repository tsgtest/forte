﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using LMP.Data;

namespace LMP.Grail
{
    public class TestGrailStore: IGrailStore
    {
        OleDbConnection m_oCn;
        string[,] m_aMappings;

        public TestGrailStore()
        {
            //populate array of variable/field mappings
            string[] aSourceFieldNames = ((IGrailStore)this).GetSourceDataFieldNames();
            string[] aMacPacFieldNames = ((IGrailStore)this).GetMacPacVariableNames();

            m_aMappings = new string[aSourceFieldNames.Length, 2];

            for (int i = 0; i < aSourceFieldNames.Length; i++)
            {
                m_aMappings[i, 0] = aSourceFieldNames[i];
                m_aMappings[i, 1] = aMacPacFieldNames[i];
            }
        }
        #region IGrailStore Members

        void IGrailStore.Connect(string[] aConnectionData)
        {
            m_oCn = new OleDbConnection(
                @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                LMP.Data.Application.PublicDataDirectory + 
                @"\TestGrailStore.accdb" + ";Jet OLEDB:System database=" +
                LMP.Data.Application.PublicDataDirectory + @"\mp10.mdw;User ID=MacPacPersonnel;Password=fish4mill");

            //connect to admin db
            m_oCn.Open();
        }

        bool IGrailStore.IsConnected
        {
            get
            {
                if (m_oCn == null)
                    return false;
                else
                    return (m_oCn.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open;
            }
        }

        int IGrailStore.MaxRecordsRetrievable
        {
            get { return 5000; }
        }

        Clients IGrailStore.GetClients(string xClientNameFilter, string xAdditionalFilter, SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientName FROM Clients ";

            if (!string.IsNullOrEmpty(xClientNameFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (!string.IsNullOrEmpty(xClientNameFilter))
            {
                if (LMP.String.IsNumericInt32(xClientNameFilter))
                    xSQL += "ID=" + xClientNameFilter + " ";
                else
                    xSQL += "ClientName LIKE '%" + xClientNameFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                xSQL += xAdditionalFilter;
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientName";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            
            Clients oClients = new Clients();

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i > ((IGrailStore)this).MaxRecordsRetrievable)
                    return oClients;

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
                oClients.Add(oValues[1].ToString(), oValues[0].ToString(), null, this);
            }

            return oClients;
        }

        Clients IGrailStore.GetClients(string xClientNameFilter, SortField iSortBy)
        {
            return ((IGrailStore)this).GetClients(xClientNameFilter, null, iSortBy);
        }

        Clients IGrailStore.GetClients(SortField iSortBy)
        {
            return ((IGrailStore)this).GetClients(null, null, iSortBy);
        }

        Matters IGrailStore.GetMatters(Client oClient, string xMatterFilter, string xAdditionalFilter, SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (oClient != null || !string.IsNullOrEmpty(xMatterFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (oClient != null)
            {
                xSQL += "ClientID=" + oClient.Number + " ";
            }

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                if (xSQL.Contains("WHERE ClientID="))
                    xSQL += " AND ";

                if (LMP.String.IsNumericInt32(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                if (xSQL.Contains("WHERE ClientID=") || xSQL.Contains("WHERE ID=") || xSQL.Contains("WHERE Description LIKE"))
                    xSQL += " AND ";

                xSQL += xAdditionalFilter;
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientID,Description;";
            }
            else
            {
                xSQL += "ORDER BY ClientID;";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            Matters oMatters = new Matters(oClient);

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i >= ((IGrailStore)this).MaxRecordsRetrievable)
                {
                    return oMatters;
                }

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
               oMatters.Add(oValues[2].ToString(), oValues[1].ToString() + "." + oValues[0].ToString(),
                   oClient.Number);
            }

            return oMatters;
        }

        Matters IGrailStore.GetMatters(Client oClient, string xMatterFilter, SortField iSortBy)
        {
            return ((IGrailStore)this).GetMatters(oClient, xMatterFilter, null, iSortBy);
        }

        Matters IGrailStore.GetMatters(Client oClient, SortField iSortBy)
        {
            return ((IGrailStore)this).GetMatters(oClient, null, null, iSortBy);
        }

        bool IGrailStore.IsValidClientMatterNumber(string xClientID, string xMatterID)
        {
            throw new NotImplementedException();
        }

        string[] IGrailStore.GetMacPacVariableNames()
        {
            return new string[2] { "Test1", "Test2" };
        }

        string[] IGrailStore.GetSourceDataFieldNames()
        {
            return new string[2] { "ClientAddress", "Judge" };
        }

        string IGrailStore.GetData(string xID, params string[] xMacPacVariableName)
        {
            throw new NotImplementedException();
        }

        string IGrailStore.GetData(int iID, params string[] xMacPacVariableName)
        {
            throw new NotImplementedException();
        }

        string IGrailStore.GetData(string xID)
        {
            int iPos = xID.IndexOf(".");
            string xClientNumber = xID.Substring(0, iPos);
            string xMatterNumber = xID.Substring(iPos + 1);

            //query for client matter detail
            string xSQL = "SELECT m.ID as MatterNumber, c.ID as ClientNumber, * FROM Matters m INNER JOIN Clients c ON m.ClientID=c.ID WHERE m.ID=" + 
                xMatterNumber + " AND m.ClientID=" + xClientNumber;

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);

            if (oReader.Read())
            {
                //get field names
                DataTable oDT = oReader.GetSchemaTable();

                //get values
                object[] aValues = new object[oReader.FieldCount];
                oReader.GetValues(aValues);

                StringBuilder oSB = new StringBuilder();

                //cycle through values, creating field/value pair string
                for (int i = 0; i < aValues.Length; i++)
                {
                    oSB.AppendFormat("{0}¤{1}Þ", oDT.Rows[i][0], aValues[i]);
                }

                //add starting delimiter
                string xPrefillString = "Þ" + oSB.ToString();

                for (int i = 0; i < m_aMappings.GetLength(0); i++)
                {
                    xPrefillString = xPrefillString.Replace(
                        "Þ" + m_aMappings[i, 0] + "¤", "Þ" + m_aMappings[i, 1] + "¤");
                }

                return xPrefillString;
            }
            else
            {
                return null;
            }
        }

        string IGrailStore.GetData(int iID)
        {
            throw new NotImplementedException();
        }

        int IGrailStore.ClientsCount(string xClientNameFilter, string xAdditionalFilter)
        {
            string xSQL = "SELECT COUNT(ID) FROM Clients ";

            if (!string.IsNullOrEmpty(xClientNameFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (!string.IsNullOrEmpty(xClientNameFilter))
            {
                if (LMP.String.IsNumericInt32(xClientNameFilter))
                    xSQL += "ID=" + xClientNameFilter + " ";
                else
                    xSQL += "ClientName LIKE '%" + xClientNameFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                xSQL += xAdditionalFilter;
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            object oCount = oCmd.ExecuteScalar();

            return (int)oCount;
        }

        int IGrailStore.MattersCount(Client oClient, string xMatterFilter, string xAdditionalFilter)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (oClient != null || !string.IsNullOrEmpty(xMatterFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (oClient != null)
            {
                xSQL += "ClientID=" + oClient.Number + " ";
            }

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                if (xSQL.Contains("WHERE ClientID="))
                    xSQL += " AND ";

                if (LMP.String.IsNumericInt32(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                if (xSQL.Contains("WHERE ClientID=") || xSQL.Contains("WHERE ID=") || xSQL.Contains("WHERE Description LIKE"))
                    xSQL += " AND ";

                xSQL += xAdditionalFilter;
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            object oCount = oCmd.ExecuteScalar();

            return (int)oCount;
        }

        Matters IGrailStore.GetMatters(string xMatterFilter, SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                xSQL += "WHERE ";

                if (LMP.String.IsNumericInt32(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientID,Description;";
            }
            else
            {
                xSQL += "ORDER BY ClientID;";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            Matters oMatters = new Matters(this);

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i > ((IGrailStore)this).MaxRecordsRetrievable)
                {
                    return oMatters;
                }

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
                oMatters.Add(oValues[2].ToString(), oValues[1].ToString() + "." + oValues[0].ToString(), 
                    oValues[1].ToString());
            }

            return oMatters;
        }
        #endregion
    }
}
