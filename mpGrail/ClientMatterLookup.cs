﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;

namespace LMP.Grail
{
    public partial class ClientMatterLookup : Form
    {
        #region***********************************enumerations***********************************
        public enum mpClientMatterLookupFormat
        {
            ClientMatter,
            CaseNumber
        }
        #endregion
        #region***********************************fields***********************************
        private LMP.Grail.GrailBackend m_oBackend;
        private Clients oClients;
        private Matters oMatters;
        private AdminSegmentDef m_oCurDef;
        #endregion
        #region***********************************constructors***********************************
        //public ClientMatterLookup(string xFilter, AdminSegmentDef oDef)
        public ClientMatterLookup(string xFilter, int iInsertLocations, int iDefInsertionLocation, int iDefInsertionBehavior)
        {
            InitializeComponent();

            this.Filter = xFilter;
            this.grdClients.AutoGenerateColumns = false;
            this.grdMatters.AutoGenerateColumns = false;

            m_oBackend = GrailStore.Backend;
            m_oBackend.Connect(null);

            //m_oCurDef = oDef;

            if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.CaseNumber)
            {
                //hide matters controls
                this.grdClients.Visible = false;
                this.lblClients.Visible = false;

                this.grdMatters.Top = this.grdClients.Top;
                this.lblMatters.Top = this.lblClients.Top;

                //rename clients list to case number
                this.lblMatters.Text = m_oBackend.MatterLabelText + ":";

                this.grdMatters.Height = 180;
                //resize
                //this.Width = 368;
                this.Height = 310;
            }

            RefreshInsertionLocationDropdown(iInsertLocations);
        }
        #endregion
        #region ***********************************properties***********************************
        public Client Client
        {
            get { return oClients[this.grdClients.SelectedRows[0].Index]; }
        }
        public Matter Matter
        {
            get { return oMatters[this.grdMatters.SelectedRows[0].Index]; }
        }
        private string ClientFilter
        {
            get
            {
                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //returns what's to the left of the '.' -
                    //if '.' does not exist, returns entire filter
                    if (this.Filter.Contains('.'))
                    {
                        int iPos = this.Filter.IndexOf('.');
                        return this.Filter.Substring(0, iPos);
                    }
                    else
                    {
                        return this.Filter;
                    }
                }
                else
                {
                    return "";
                }
            }
        }
        private string MatterFilter
        {
            get
            {
                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //returns the substring to the right of the '.' 
                    //if it exists - else returns null
                    if (this.Filter.Contains('.'))
                    {
                        int iPos = this.Filter.IndexOf('.');
                        return this.Filter.Substring(iPos + 1);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    //in case number format, filter on matter number
                    return this.Filter;
                }
            }
        }
        public string Filter { get; set; }
        /// <summary>
        /// true iff matters were searched
        /// </summary>
        private bool MattersSearched {get; set; }
        public int InsertionLocation
        {
            get { return (int)this.cmbLocation.SelectedItem; }
            set { this.cmbLocation.SelectedItem = value; }
        }
        public int InsertBehaviors
        {
            get
            {
                int iBeh = 0;
                if (this.chkKeepExisting.Checked)
                    iBeh = 1;

                if (this.chkRestartNumbering.Checked)
                    iBeh += 2;

                if (this.chkSeparateSection.Checked)
                    iBeh += 4;

                return iBeh;
            }
            set
            {
                this.chkKeepExisting.Checked = (value & 1) == 1;
                this.chkRestartNumbering.Checked = (value & 2) == 2;
                this.chkSeparateSection.Checked = (value & 4) == 4;
            }
        }

        #endregion
        #region************************************methods***********************************
        /// <summary>
        /// loads clients into the clients grid
        /// </summary>
        /// <returns></returns>
        private int LoadClients()
        {
            oClients = m_oBackend.GetClients(this.ClientFilter, SortField.Name);
            DataTable oDT = oClients.ToDataTable();
            this.grdClients.DataSource = oDT;
            this.grdClients.Columns[0].DataPropertyName = "Number";

            if (!this.m_oBackend.ShowNameColumn)
            {
                this.grdClients.Columns[1].Visible = false;
            }
            else
            {
                this.grdClients.Columns[1].DataPropertyName = "ClientName";
            }

            if (oClients.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oClients.Count + 
                    " - Max displayed):";
            }
            else
            {
                this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oClients.Count + "):";
            }

            return oClients.Count;
        }
        /// <summary>
        /// loads matters of the specified client into the matters grid
        /// </summary>
        /// <param name="xMatterFilter"></param>
        /// <param name="oClient"></param>
        /// <returns></returns>
        private int LoadMatters(string xMatterFilter, Client oClient)
        {
            oMatters = oClient.GetMatters(xMatterFilter);
            DataTable oDT = oMatters.ToDataTable();
            this.grdMatters.DataSource = oDT;
            this.grdMatters.Columns[0].DataPropertyName = "Number";

            if (!this.m_oBackend.ShowNameColumn)
            {
                this.grdMatters.Columns[1].Visible = false;
            }
            else
            {
                this.grdMatters.Columns[1].DataPropertyName = "MatterName";
            }

            if (oMatters.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + 
                    " - Max displayed):";
            }
            else
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + "):";
            }

            return oMatters.Count;
        }
        /// <summary>
        /// loads matters into the matters grid -
        /// independent of client
        /// </summary>
        /// <param name="xMatterFilter"></param>
        /// <returns></returns>
        private int LoadMatters(string xMatterFilter)
        {
            oMatters = m_oBackend.GetMatters(xMatterFilter, SortField.Name);
            DataTable oDT = oMatters.ToDataTable();
            this.grdMatters.DataSource = oDT;
            this.grdMatters.Columns[0].DataPropertyName = "Number";
            this.grdMatters.Columns[1].DataPropertyName = "MatterName";

            if (oMatters.Count == m_oBackend.MaxRecordsRetrievable)
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + " - Maximum displayed):";
            }
            else
            {
                this.lblMatters.Text = m_oBackend.MatterLabelText + " (" + oMatters.Count + "):";
            }

            return oMatters.Count;
        }
        /// <summary>
        /// Filters grids based on specified filter string -
        /// supports filtering on both Clients and Matters
        /// </summary>
        private void ExecuteFilter(){
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.Filter = this.txtFind.Text;

                int iClients = 0;

                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
                {
                    //load clients based on filter
                    iClients = LoadClients();
                }

                //search matters if filter is not numeric
                //and no clients were found
                if (iClients == 0 && !String.IsNumericInt32(this.ClientFilter))
                {
                    MattersSearched = true;

                    //no clients were returned by a client filter that was text -
                    //search for matters that match the filter
                    int iMatters = LoadMatters(this.MatterFilter);

                    this.grdMatters.Select();
                    this.btnOK.Enabled = iMatters > 0;
                }
                else
                {
                    this.grdClients.Select();
                    MattersSearched = false;
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        ///// <summary>
        ///// Setup Display options for current Segment
        ///// </summary>
        //private void LoadSegmentOptions()
        //{
        //    //GLOG 3983: Save currently selected location so it can be preserved when Segment changes
        //    short shCurLocation = 0;
        //    if (this.cmbLocation.SelectedValue != null)
        //        shCurLocation = short.Parse(this.cmbLocation.SelectedValue.ToString());
        //    ArrayList oListItems = new ArrayList();
        //    object[,] aDefaultLocations = null;
        //    object[,] aBehaviors = null;
        //    if ((false && m_oCurDef.TypeID == mpObjectTypes.Labels || m_oCurDef.TypeID == mpObjectTypes.Envelopes) &&
        //        LMP.MacPac.MacPacImplementation.IsToolbarOnly && LMP.Utility.InDevelopmentMode) //GLOG 7127
        //    {
        //        aDefaultLocations = new object[,]{
        //        {"New Document",128}};
        //    }
        //    else
        //    {
        //        //get array of insertion locations
        //        aDefaultLocations = new object[,]{
        //        {"Start of Current Section",1}, 
        //        {"End of Current Section",2}, 
        //        {"Start of All Sections",4}, 
        //        {"End of All Sections",8}, 
        //        {"Insertion Point",16}, 
        //        {"Start of Document",32}, 
        //        {"End of Document",64},
        //        {"New Document",128},
        //        {"Default Location", 256}};
        //    }

        //    aBehaviors = new object[,]{
        //    {"Insert In Separate Section",1}, 
        //    {"Insert In Separate Continuous Section",8}, 
        //    {"Keep Existing Headers Footers",2}, 
        //    {"Restart Page Numbering",4}};

        //    for (int i = aDefaultLocations.GetUpperBound(0); i >= 0; i--)
        //    {
        //        if ((m_oCurDef.MenuInsertionOptions &
        //            (short)(int)aDefaultLocations[i, 1]) == (short)(int)aDefaultLocations[i, 1])
        //        {
        //            oListItems.Add(new object[] { aDefaultLocations[i, 1], aDefaultLocations[i, 0] });
        //        }
        //    }

        //    if (oListItems.Count == 0)
        //    {
        //        cmbLocation.SetList(aDefaultLocations);
        //    }
        //    else
        //    {
        //        mpCOM.cWordDocument oDoc = LMP.MPCOMObjects.cWordDoc;
        //        //GLOG 5723: Add options for Below TOC and Above TOA if appropriate
        //        if (m_oCurDef.TypeID == mpObjectTypes.TOA || m_oCurDef.TypeID == mpObjectTypes.PleadingExhibit)
        //        {
        //            if (m_oCurDef.TypeID != mpObjectTypes.TOA)
        //            {
        //                if (oDoc.TOAExists(LMP.MPCOMObjects.cWordApp.ActiveDocument()))
        //                    oListItems.Add(new object[] { "1024", "Above TOA Section" });
        //            }
        //            if (oDoc.TOCExists(LMP.MPCOMObjects.cWordApp.ActiveDocument()))
        //            {
        //                oListItems.Add(new object[] { "512", "Below TOC Section" });
        //                if (m_oCurDef.TypeID == mpObjectTypes.TOA)
        //                    //Use as default location for TOA
        //                    m_oCurDef.DefaultDoubleClickLocation = 512;
        //            }

        //        }
        //        cmbLocation.SetList(oListItems);
        //    }

        //    //GLOG 3983: Retain previously selected location after initial display
        //    if (!m_bFirstSegmentShown)
        //    {
        //        // Select default double-click item in list
        //        cmbLocation.SelectedValue = m_oCurDef.DefaultDoubleClickLocation;
        //        m_bFirstSegmentShown = true;
        //    }
        //    else
        //    {
        //        //Reloading List changed cmbLocation value, so restore saved Last location
        //        m_shLastLocation = shCurLocation;
        //        cmbLocation.SelectedValue = shCurLocation;
        //    }
        //    if (cmbLocation.SelectedIndex == -1)
        //        cmbLocation.SelectedIndex = 0;
        //}
        //private void EnableBehaviorOptions()
        //{
        //    if (this.cmbLocation.SelectedValue == null)
        //        return;

        //    if (short.Parse(this.cmbLocation.SelectedValue.ToString()) != 128)
        //    {
        //        this.chkSeparateSection.Checked = (m_oCurDef.IntendedUse == mpSegmentIntendedUses.AsDocument);
        //        this.chkKeepExisting.Checked = (m_oCurDef.IntendedUse != mpSegmentIntendedUses.AsDocument);
        //        this.chkRestartNumbering.Checked = (m_oCurDef.IntendedUse == mpSegmentIntendedUses.AsDocument);
        //    }
        //    else
        //    {
        //        //GLOG 3983: These items disabled for 'New Document' location
        //        this.chkSeparateSection.Enabled = false;
        //        this.chkRestartNumbering.Enabled = false;
        //        this.chkKeepExisting.Enabled = false;
        //    }

        //    if (m_oCurDef.TypeID == mpObjectTypes.BusinessTitlePage)
        //    {
        //        this.chkSeparateSection.Enabled = false;
        //        this.chkKeepExisting.Enabled = false;
        //        this.chkRestartNumbering.Enabled = true;
        //    }
        //    if (m_oCurDef.TypeID == mpObjectTypes.PleadingExhibit ||
        //        m_oCurDef.TypeID == mpObjectTypes.BusinessExhibit)
        //    {
        //        //GLOG item #6720 - dcf - 04/22/13
        //        this.chkSeparateSection.Checked = true; //GLOG 6355
        //        this.chkKeepExisting.Checked = false;
        //        this.chkRestartNumbering.Checked = true;
        //    }
        //    else if (m_oCurDef.TypeID == mpObjectTypes.TOA)
        //    {
        //        this.chkKeepExisting.Enabled = false;
        //        this.chkSeparateSection.Checked = true; //GLOG 6355
        //        this.chkRestartNumbering.Enabled = true;
        //    }
        //    else
        //    {
        //        //GLOG 3983: Enable options appropriate for current definition
        //        this.chkSeparateSection.Enabled = (((m_oCurDef.DefaultMenuInsertionBehavior &
        //            (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ==
        //            (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ||
        //            ((m_oCurDef.DefaultMenuInsertionBehavior &
        //            (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection) ==
        //            (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection));

        //        //GLOG 3983: Set Separate Section checkbox only if changing from New Document location
        //        //Otherwise, maintain previously selected value
        //        if (this.chkSeparateSection.Enabled && m_shLastLocation == 128)
        //        {
        //            this.chkSeparateSection.Checked = true;
        //        }

        //        this.chkRestartNumbering.Enabled = ((m_oCurDef.DefaultMenuInsertionBehavior &
        //            (short)Segment.InsertionBehaviors.RestartPageNumbering) ==
        //            (short)Segment.InsertionBehaviors.RestartPageNumbering);

        //        this.chkKeepExisting.Enabled = ((m_oCurDef.DefaultMenuInsertionBehavior &
        //            (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters) ==
        //            (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters);
        //    }
        //}

        /// <summary>
        /// refreshes the dropdown items listed in the insertion locations dropdown
        /// </summary>
        /// <param name="iLocations"></param>
        private void RefreshInsertionLocationDropdown(int iLocations)
        {
            string[] oItems = GetInsertionOptionsFromBitwiseInt(iLocations);
            this.cmbLocation.Items.Clear();

            foreach (string oItem in oItems)
            {
                this.cmbLocation.Items.Add(oItem);
            }
        }
        /// <summary>
        /// returns a string array of the specified available locations
        /// </summary>
        /// <param name="iOptions"></param>
        /// <returns></returns>
        private string[] GetInsertionOptionsFromBitwiseInt(int iOptions)
        {
            string xOptions = "";

            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection)
            //    xOptions = "Start of Current Section|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection)
            //    xOptions = xOptions + "End of Current Section|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfAllSections) == (int)Segment.InsertionLocations.InsertAtStartOfAllSections)
            //    xOptions = xOptions + "Start of All Sections|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfAllSections) == (int)Segment.InsertionLocations.InsertAtEndOfAllSections)
            //    xOptions = xOptions + "End of All Sections|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtSelection) == (int)Segment.InsertionLocations.InsertAtSelection)
            //    xOptions = xOptions + "Insertion Point|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfDocument) == (int)Segment.InsertionLocations.InsertAtStartOfDocument)
            //    xOptions = xOptions + "Start of Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfDocument) == (int)Segment.InsertionLocations.InsertAtEndOfDocument)
            //    xOptions = xOptions + "End of Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertInNewDocument) == (int)Segment.InsertionLocations.InsertInNewDocument)
            //    xOptions = xOptions + "New Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.Default) == (int)Segment.InsertionLocations.Default)
            //    xOptions = xOptions + "Default|";

            return xOptions.TrimEnd('|').Split('|');
        }
        #endregion
        #region ***********************************event handlers***********************************
        private void ClientMatterLookup_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.CaseNumber)
                {
                    this.Text = LMP.Resources.GetLangString("Dlg_ClientMatterLookupCaseNumberFormatText");
                }
                else
                {
                    this.Text = LMP.Resources.GetLangString("Dlg_ClientMatterLookupClientMatterFormatText");
                }

                this.txtFind.Text = this.Filter;

                this.grdMatters.Columns[0].Visible = this.m_oBackend.ShowIDColumn;
                this.grdMatters.Columns[0].DataPropertyName = "ClientNumber";
                this.grdMatters.Columns[1].Visible = this.m_oBackend.ShowNameColumn;
                this.grdMatters.Columns[1].DataPropertyName = "ClientName";
                //ExecuteFilter();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdClients_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!MattersSearched)
                {
                    //load matters for the selected client
                    int iMatters = LoadMatters(this.MatterFilter, oClients[e.RowIndex]);

                    this.btnOK.Enabled = iMatters > 0;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                ExecuteFilter();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdMatters_SelectionChanged(object sender, EventArgs e)
        {
            if (m_oBackend.DisplayFormat == GrailBackend.mpGrailClientMatterDisplayFormat.ClientMatter)
            {
                Clients oTempClients = null;

                DataGridViewSelectedRowCollection oSelRows = this.grdMatters.SelectedRows;

                //display client of selected matter if matters were searched
                if (MattersSearched && oSelRows.Count > 0)
                {
                    oTempClients = m_oBackend.GetClients(oMatters[oSelRows[0].Index].ClientID, SortField.Name);
                    DataTable oDT = oTempClients.ToDataTable();
                    this.grdClients.DataSource = oDT;
                    this.lblClients.Text = m_oBackend.ClientLabelText + " (" + oTempClients.Count + "):";
                }

                this.btnOK.Enabled = oSelRows.Count > 0;
            }
        }
        private void txtFind_Enter(object sender, EventArgs e)
        {
            try
            {
                this.AcceptButton = this.btnFind;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtFind_Leave(object sender, EventArgs e)
        {
            try
            {
                this.AcceptButton = this.btnOK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdMatters_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}