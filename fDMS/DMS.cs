﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMP.fDMS
{
    public static class DMS
    {
        static DMSBackend m_oBackend = null;
        static DMS()
        {
            DMSBackendType iDMS = DMSBackendType.Windows;
            //Get DMS type from Firm Application Settings
            LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
            try
            {
                int iTest = oSettings.DMSType;
                iDMS = (DMSBackendType)iTest;
            }
            catch
            {
                iDMS = DMSBackendType.Windows;
            }
            LMP.Trace.WriteNameValuePairs("iDMS", iDMS);
            try
            {
                switch (iDMS)
                {
                    case DMSBackendType.IManageWork:
                        m_oBackend = new IManageBackend();
                        break;
                    case DMSBackendType.eDocsDM:
                        m_oBackend = new eDocsDMBackend();
                        break;
                    case DMSBackendType.ndOffice:
                        m_oBackend = new NdOfficeBackend();
                        break;
                    case DMSBackendType.IManageWork10:
                        m_oBackend = new IManWork10Backend();
                        break;
                    //TODO: Fill in other backend types
                    default:
                        m_oBackend = new WindowsBackend();
                        break;
                }
            }
            catch (System.Exception oE)
            {
                m_oBackend = new WindowsBackend();
            }
        }
        public static DMSBackend ProfileObject
        {
            get { return m_oBackend; }
        }
        /// <summary>
        /// Returns true of activedocument is profiled in DMS
        /// </summary>
        public static bool IsProfiled
        {
            //JTS 12/16/10: If not Profiled, GetProfileValue might return null instead of ""
            get
            {
                return m_oBackend.DMSType != DMSBackendType.Windows
                    && !string.IsNullOrEmpty(GetProfileValue("DocNumber"));
            }
        }
        /// <summary>
        /// Return 
        /// </summary>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        public static string GetProfileValue(string xPropName)
        {
            return GetProfileValue(xPropName, "");
        }
        public static string GetProfileValue(string xPropName, string xDateFormat)
        {
            string xValue = "";
            try
            {
                switch (xPropName.ToUpper())
                {
                    case "DOCNUMBER":
                        xValue = m_oBackend.DocNumber;
                        break;
                    case "VERSION":
                        xValue = m_oBackend.Version;
                        break;
                    case "LIBRARY":
                        xValue = m_oBackend.Library;
                        break;
                    case "CLIENTID":
                        xValue = m_oBackend.ClientID;
                        break;
                    case "CLIENTNAME":
                        xValue = m_oBackend.ClientName;
                        break;
                    case "MATTERID":
                        xValue = m_oBackend.MatterID;
                        break;
                    case "MATTERNAME":
                        xValue = m_oBackend.MatterName;
                        break;
                    case "AUTHORID":
                        xValue = m_oBackend.AuthorID;
                        break;
                    case "AUTHORNAME":
                        xValue = m_oBackend.AuthorName;
                        break;
                    case "TYPISTID":
                        xValue = m_oBackend.TypistID;
                        break;
                    case "TYPISTNAME":
                        xValue = m_oBackend.TypistName;
                        break;
                    case "ABSTRACT":
                        xValue = m_oBackend.Abstract;
                        break;
                    case "DOCNAME":
                        xValue = m_oBackend.DocName;
                        break;
                    case "DOCTYPEDESCRIPTION":
                        xValue = m_oBackend.DocTypeDescription;
                        break;
                    case "DOCTYPEID":
                        xValue = m_oBackend.DocTypeID;
                        break;
                    case "FILENAME":
                        xValue = m_oBackend.FileName;
                        break;
                    case "GROUPNAME":
                        xValue = m_oBackend.GroupName;
                        break;
                    case "PATH":
                        xValue = m_oBackend.Path;
                        break;
                    case "REVISIONDATE":
                        xValue = m_oBackend.RevisionDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CREATIONDATE":
                        xValue = m_oBackend.CreationDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CUSTOM1":
                        xValue = m_oBackend.Custom1;
                        break;
                    case "CUSTOM2":
                        xValue = m_oBackend.Custom2;
                        break;
                    case "CUSTOM3":
                        xValue = m_oBackend.Custom3;
                        break;
                    default:
                        //Get any other profile property using DMS-specific field ID
                        xValue = m_oBackend.GetProfileInfo(xPropName);
                        break;
                }
            }
            catch
            {
            }
            return xValue;
        }
    }
}
