﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.IO; //GLOG 15924

namespace LMP.fDMS
{

    public delegate void FileSavedHandler();
    public enum OfficeAppTypes
    {
        MSWord = 0,
        MSExcel = 1,
        MSPowerPoint = 2
    }
    public enum DMSBackendType : short
    {
        Windows = 0,
        PCDOCS = 1,
        IManageWork = 3,
        Fusion = 5,
        NetDocuments = 6,
        eDocsDM = 7,
        ndOffice = 10,
        IManageWork10 = 12
    }
    public abstract class DMSBackend
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern int GetPrivateProfileString(
            string xAppName, string xKeyName, string xDefault, StringBuilder xReturnedString,
            int iSize, string xFileName); 
        
        public FileSavedHandler FileSaved;
        protected DMSBackendType m_iDMSType = DMSBackendType.Windows;
        protected string m_xSection = "";
        protected string[] m_aCustomFields = new string[3];
        internal OfficeIntegrator m_oIntegrator = new OfficeIntegrator(OfficeAppTypes.MSWord);

        protected DMSBackend()
        {
        }
        protected DMSBackend(OfficeAppTypes iAppType)
        {
            this.AppType = iAppType;
        }
        public OfficeAppTypes AppType
        {
            get
            {
                if (m_oIntegrator != null)
                {
                    return m_oIntegrator.AppType;
                }
                else
                {
                    return OfficeAppTypes.MSWord;
                }
            }
            set
            {
                if (m_oIntegrator != null && value != m_oIntegrator.AppType)
                {
                    m_oIntegrator = new OfficeIntegrator(value);
                }
            }

        }
        public DMSBackendType DMSType
        {
            get {return m_iDMSType;}
        }
        internal void LoadCustomFields()
        {
            if (m_xSection != "")
            {
                for (int i = 1; i <=3; i++)
                {
                    m_aCustomFields[i-1] = GetSetting(m_xSection, "Custom" + i.ToString());
                }
            }
            else
            {
                m_aCustomFields[0] = "";
                m_aCustomFields[1] = "";
                m_aCustomFields[2] = "";
            }
        }
        internal string GetSetting(string xSection, string xSetting)
        {
            const string xINIFILE = "\\ForteDMS.ini";
            const string xXMLFILE = "\\ForteDMSConfig.xml";
            //JTS 10/26/16: Settings will be read from Ini for now.
            //Using Win API instead of Word.Application.System.PrivateProfileString, since also need to support Excel and PowerPoint
            try
            {
                if (LMP.Utility.InDevelopmentModeState(8)) //Enable for 11.4
                {
                    //GLOG 15937: Convert existing ForteDMS.ini if present and not current xml
                    if (!File.Exists(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xXMLFILE) &&
                        File.Exists(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xINIFILE))
                    {
                        if (INI2XML.Convert(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xINIFILE, LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xXMLFILE))
                        {
                            try
                            {
                                File.Delete(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xINIFILE);
                            }
                            catch { }
                        };
                    }
                }

                //GLOG 15937
                if (File.Exists(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xXMLFILE ))
                {
                    XmlDocument oDoc = new XmlDocument();
                    XmlNode oNode = null;
                    try
                    {
                        //If XML file doesn't exist, return empty value
                        oDoc.Load(LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xXMLFILE);
                        oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']//setting[@name='" + xSetting + "']");
                        if (oNode != null)
                            return oNode.Attributes["value"].Value;
                        else
                            return "";
                    }
                    catch
                    {
                        return "";
                    }
                }
                else
                {
                    string xIni = LMP.Data.Application.GetDirectory(Data.mpDirectories.DB) + xINIFILE;
                    StringBuilder xValue = new StringBuilder(512);
                    int iRet = 0;
                    iRet = GetPrivateProfileString(xSection, xSetting, "", xValue, 512, xIni);
                    if (iRet > 0)
                    {
                        return xValue.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        abstract public string GetProfileInfo(string xItem);
        abstract public string DocNumber {get; }
        abstract public string Version {get; }
        abstract public string DocName { get; }
        abstract public string DocTypeID { get; }
        abstract public string DocTypeDescription { get; }
        abstract public string Library { get; }
        abstract public string AuthorID { get; }
        abstract public string TypistID { get; }
        abstract public string AuthorName { get; }
        abstract public string TypistName { get; }
        abstract public string ClientID { get; }
        abstract public string ClientName { get; }
        abstract public string MatterID { get; }
        abstract public string MatterName { get; }
        abstract public string Abstract { get; }
        abstract public string GroupName { get; }
        abstract public string CreationDate { get; }
        abstract public string RevisionDate { get; }
        abstract public string Path { get; }
        abstract public string FileName { get; }
        abstract public string Custom1 { get; }
        abstract public string Custom2 { get; }
        abstract public string Custom3 { get; }
        abstract public void FileSave();
        abstract public void FileClose();
        abstract public void FileSaveAs();
        abstract public void DocClose();
    }
    public class WindowsBackend : DMSBackend
    {
        private const string NO_OPEN_DOCUMENT = "This command is not available because no document is open.";
        public WindowsBackend() : this(OfficeAppTypes.MSWord)
        {
        }
        public WindowsBackend(OfficeAppTypes iAppType)
            : base(iAppType)
        {
            m_iDMSType = DMSBackendType.Windows;
            m_xSection = "Native Word";
            LoadCustomFields();
        }
        public override string GetProfileInfo(string xItem)
        {
            //Return any defined doc property by name
            return m_oIntegrator.GetDocPropValue(xItem);
        }
        public override string DocNumber
        {
            get { return ""; }
        }

        public override string Version
        {
            get { return ""; }
        }

        public override string DocName
        {
            get
            {
                return m_oIntegrator.GetDocPropValue("Title");
            }
        }
        public override string DocTypeID
        {
            get { return ""; }
        }

        public override string DocTypeDescription
        {
            get { return ""; }
        }

        public override string Library
        {
            get { return ""; }
        }

        public override string AuthorID 
        {
            get { return m_oIntegrator.GetDocPropValue("Author Initials"); }
        }
 
        public override string TypistID
        {
            get { return m_oIntegrator.AppUserInitials; }
        }

        public override string AuthorName
        {
            get { return m_oIntegrator.GetDocPropValue("Author"); }
        }
        public override string TypistName 
        { 
            get  { return m_oIntegrator.AppUserName; }
        }
        public override string ClientID
        {
            get { return ""; }
        }
        public override string ClientName
        {
            get { return ""; }
        }

        public override string MatterID
        {
            get { return ""; }
        }

        public override string MatterName
        {
            get { return ""; }
        }

        public override string Abstract        
        {
            get { return m_oIntegrator.GetDocPropValue("Comments"); }
        }

        public override string GroupName
        {
            get { return ""; }
        }

        public override string CreationDate
        {
            get { return m_oIntegrator.GetDocPropValue("Creation Date"); }
        }

        public override string RevisionDate
        {
            get { return m_oIntegrator.GetDocPropValue("Last save time"); }
        }
        public override string Path
        {
            get
            {
                return m_oIntegrator.TargetAppPath;
            }
        }
        public override string FileName
        {
            get
            {
                return m_oIntegrator.TargetAppFullName;
            }
        }
        public override string Custom1
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 0 && !string.IsNullOrEmpty(m_aCustomFields[0]))
                    return GetProfileInfo(m_aCustomFields[0]);
                else
                    return "";
            }
        }

        public override string Custom2
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 1 && !string.IsNullOrEmpty(m_aCustomFields[1]))
                    return GetProfileInfo(m_aCustomFields[1]);
                else
                    return "";
            }
        }

        public override string Custom3
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 2 && !string.IsNullOrEmpty(m_aCustomFields[2]))
                    return GetProfileInfo(m_aCustomFields[2]);
                else
                    return "";
            }
        }

        public override void FileSave()
        {
            try
            {
                switch (m_oIntegrator.AppType)
                {
                    case OfficeAppTypes.MSExcel:
                        FileSaveExcel();
                        break;
                    case OfficeAppTypes.MSPowerPoint:
                        FileSavePowerPoint();
                        break;
                    default:
                        FileSaveWord();
                        break;
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override void FileClose()
        {
            try
            {
                switch (m_oIntegrator.AppType)
                {
                    case OfficeAppTypes.MSWord:
                        FileCloseWord();
                        break;
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void FileCloseWord() 
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                Word.Document oDoc = m_oIntegrator.WordApp.ActiveDocument;
                if (!oDoc.Saved)
                {
                    DialogResult iRet = MessageBox.Show("Do you want to save the changes you made to " + oDoc.Name + "?", LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    switch (iRet)
                    {
                        case DialogResult.Yes:
                            this.FileSave();
                            if (oDoc.Saved)
                            {
                                oDoc.Close();
                            }
                            break;
                        case DialogResult.No:
                            oDoc.Saved = true;
                            oDoc.Close();
                            break;
                        default:
                            return;
                    }
                }
                else
                    oDoc.Close();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override void FileSaveAs()
        {
            try
            {
                switch (m_oIntegrator.AppType)
                {
                    case OfficeAppTypes.MSExcel:
                        FileSaveAsExcel();
                        break;
                    case OfficeAppTypes.MSPowerPoint:
                        FileSaveAsPowerPoint();
                        break;
                    default:
                        FileSaveAsWord();
                        break;
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override void DocClose()
        {
            switch (m_oIntegrator.AppType)
            {
                case OfficeAppTypes.MSWord:
                    DocCloseWord();
                    break;
            }
        }
        private void DocCloseWord()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                return;
            }
            if (m_oIntegrator.WordApp.Windows.Count > 1)
                if (m_oIntegrator.WordApp.ActiveDocument.ActiveWindow.View.Type == Word.WdViewType.wdPrintPreview)
                    m_oIntegrator.WordApp.ActiveDocument.ClosePrintPreview();
                else
                    m_oIntegrator.WordApp.ActiveDocument.ActiveWindow.Close();
            else
                this.FileClose();

        }
        internal void FileSaveWord()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                Word.Document oDoc = m_oIntegrator.WordApp.ActiveDocument;
                if (oDoc == null)
                    return;

                if (oDoc.ReadOnly || oDoc.Path == "")
                {
                    FileSaveAsWord();
                    return;
                }

                int iFormat = oDoc.SaveFormat;
                if (iFormat == 0 || iFormat == 1 || (iFormat > 11 && iFormat < 17))
                {
                    oDoc.Save();
                }
                else
                {
                    if (!oDoc.Saved)
                    {
                        Word.WdSaveFormat iWordFormat = (Word.WdSaveFormat)iFormat;
                        string xFormat = "";
                        switch (iWordFormat)
                        {
                            case Word.WdSaveFormat.wdFormatText:
                                xFormat = "Text Only";
                                break;
                            case Word.WdSaveFormat.wdFormatTextLineBreaks:
                                xFormat = "Text Only with Line Breaks";
                                break;
                            case Word.WdSaveFormat.wdFormatRTF:
                                xFormat = "Rich Text Format";
                                break;
                            case Word.WdSaveFormat.wdFormatUnicodeText:
                                xFormat = "Unicode Text";
                                break;

                            case Word.WdSaveFormat.wdFormatDOSText:
                                xFormat = "MS-DOS Text";
                                break;
                            case Word.WdSaveFormat.wdFormatDOSTextLineBreaks:
                                xFormat = "MS-DOS Text with Line Breaks";
                                break;
                            default:
                                foreach (Word.FileConverter oConv in m_oIntegrator.WordApp.FileConverters)
                                {
                                    if (oConv.CanSave)
                                    {
                                        if (oConv.SaveFormat == iFormat)
                                        {
                                            xFormat = oConv.FormatName;
                                            break;
                                        }
                                    }
                                }
                                break;
                        }
                        if (xFormat == "")
                        {
                            xFormat = "unknown";
                        }
                        if (iFormat > 1 && (iFormat < 12 || iFormat > 16))
                        {
                            string xMsg = "This document may contain formatting which will be lost upon conversion to " + xFormat +
                                   " format.  Click No to save the document in Word format and preserve the current formatting.\r\n\r\n" +
                                   "Continue with save in " + xFormat + " format?";

                            DialogResult iRet = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                            switch (iRet)
                            {
                                case DialogResult.Yes:
                                    oDoc.Save();
                                    break;

                                case DialogResult.No:
                                    //convert to Word Format when saving

                                    string xTempFile = System.IO.Path.GetTempPath() + "tempsave.docx";

                                    string xCurFile = oDoc.FullName;

                                    //save to temp file in Word format
                                    oDoc.SaveAs2(xTempFile, 0);

                                    //save back to original filename
                                    oDoc.SaveAs2(xCurFile, 0);

                                    try
                                    {
                                        System.IO.File.Delete(xTempFile);
                                    }
                                    catch { }
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            oDoc.Save();
                        }
                    }
                }
                if (oDoc.Saved)
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        internal void FileSaveAsWord()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                int iRet = 0;
                string xOrig = m_oIntegrator.WordApp.ActiveDocument.FullName;
                Word.Dialog oDlg = m_oIntegrator.WordApp.Dialogs[Word.WdWordDialog.wdDialogFileSaveAs];
            ShowDialog:
                try
                {
                    iRet = oDlg.Show();
                }
                catch (System.Exception oE)
                {
                    switch (oE.HResult)
                    {
                        case -2146823135: //VBA error 5155
                            // This error will be generated even if filename is corrected and document saved
                            if (m_oIntegrator.WordApp.ActiveDocument.Saved && (xOrig != m_oIntegrator.WordApp.ActiveDocument.FullName))
                            {
                                iRet = -1;
                            }
                            break;
                        //Not sure of .NET equivalent error code, or even how to reproduce this with current Word version
                        //case 5153:
                        //    GLOG 3777: Can't save using same path and name as another open document
                        //    Display Word's internal error message text, then display dialog again
                        //    MessageBox.Show(oE.Message, m_xProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //    goto ShowDialog;
                        default:
                            throw oE;
                    }
                }
                int iFormat = m_oIntegrator.WordApp.ActiveDocument.SaveFormat;
                if (iRet == -1 && (iFormat < 2 || (iFormat > 11 && iFormat < 17)))
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void FileSaveExcel()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                Excel.Workbook oDoc = m_oIntegrator.ExcelApp.ActiveWorkbook;
                if (oDoc == null)
                    return;

                if (oDoc.ReadOnly || oDoc.Path == "")
                {
                    FileSaveAsExcel();
                    return;
                }

                oDoc.Save();
                if (oDoc.Saved)
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
    
        }
        private void FileSaveAsExcel()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                bool bRet = false;
                Excel.Dialog oDlg = m_oIntegrator.ExcelApp.Dialogs[Excel.XlBuiltInDialog.xlDialogSaveAs];
                    bRet = oDlg.Show();
                if (bRet)
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void FileSavePowerPoint()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                PowerPoint.Presentation oDoc = m_oIntegrator.PowerPointApp.ActivePresentation;
                if (oDoc == null)
                    return;

                if (oDoc.ReadOnly == Microsoft.Office.Core.MsoTriState.msoTrue || oDoc.Path == "")
                {
                    FileSaveAsPowerPoint();
                    return;
                }

                oDoc.Save();
                if (oDoc.Saved == Microsoft.Office.Core.MsoTriState.msoTrue)
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void FileSaveAsPowerPoint()
        {
            if (m_oIntegrator.TargetAppFileCount == 0)
            {
                MessageBox.Show(NO_OPEN_DOCUMENT, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                int iRet = 0;
                Microsoft.Office.Core.FileDialog oDlg = m_oIntegrator.PowerPointApp.FileDialog[Microsoft.Office.Core.MsoFileDialogType.msoFileDialogSaveAs];
                iRet = oDlg.Show();
                if (iRet == -1)
                {
                    if (FileSaved != null)
                        FileSaved();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }

        }
    }
}
