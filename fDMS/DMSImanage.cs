﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;
using Com.Interwoven.WorkSite.iManage;
using Com.Interwoven.Worksite.iManExt;
using Com.iManage.WorkSiteAddinInterfacesEx;

namespace LMP.fDMS
{
    public class IManageBackend : DMSBackend
    {
        const int IMAN_PROFILE_DOCNUMBER = 1;  // nrDocNum
        const int IMAN_PROFILE_DOCNAME = 3; // nrDescription
        const int IMAN_PROFILE_DOCTYPEID = 8; // nrClass   ; 
        const int IMAN_PROFILE_DOCTYPEDESCRIPTION = 77; // nrClassDescription  ;
        const int IMAN_PROFILE_LIBRARY = 0; // nrDatabase
        const int IMAN_PROFILE_AUTHORUSERID = 5; // nrAuthor
        const int IMAN_PROFILE_AUTHORFULLNAME = 74; // nrAuthorDescription  ;
        const int IMAN_PROFILE_TYPISTUSERID = 18; // nrLastUser
        const int IMAN_PROFILE_TYPISTFULLNAME = 79; // nrLastUserDescription
        const int IMAN_PROFILE_CREATIONDATE = 11; // nrCreateDate
        const int IMAN_PROFILE_LASTEDITDATE = 10; // nrEditDate
        const int IMAN_PROFILE_COMMENT = 24; // nrComment
        const int IMAN_PROFILE_CLIENTID = 25; // nrCustom1
        const int IMAN_PROFILE_CLIENTNAME = 60; // nrCustom1Description
        const int IMAN_PROFILE_MATTERID = 26; // nrCustom2
        const int IMAN_PROFILE_MATTERNAME = 61; // nrCustom2Description
        const int IMAN_PROFILE_VERSION = 2; // nrVersion

        iManageExtensibility m_oExt = null;


        public IManageBackend() : this(OfficeAppTypes.MSWord)
        {
        }
        public IManageBackend(OfficeAppTypes iAppType)
            : base(iAppType)
        {
            m_iDMSType = DMSBackendType.IManageWork;
            m_xSection = "iManage Work";
            switch (iAppType)
            {
                case OfficeAppTypes.MSExcel:
                    InitializeExcel();
                    break;
                case OfficeAppTypes.MSPowerPoint:
                    InitializePowerPoint();
                    break;
                default:
                    InitializeWord();
                    break;
            }
            LoadCustomFields();
        }
        private void InitializeWord()
        {
            try
            {
                //already initialized
                if (m_oExt != null)
                    return;

                //Addin Name for Worksite 8.5 SP3 Update 2 and later
                object oAddin = "WorkSiteOffice2007Addins.Connect";
                try
                {
                    m_oExt = (iManageExtensibility)m_oIntegrator.WordApp.COMAddIns.Item(ref oAddin).Object;
                }
                catch { }
                //Addin Name for Worksite 8.5 SP2 or earlier
                oAddin = "oUTR02K.Connect";
                if (m_oExt == null)
                {
                    try
                    {
                        m_oExt = (iManageExtensibility)m_oIntegrator.WordApp.COMAddIns.Item(ref oAddin).Object;
                    }
                    catch { }
                }
                if (m_oExt == null)
                {
                    throw new LMP.Exceptions.NotInCollectionException("IManage COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void InitializeExcel()
        {
            try
            {
                //already initialized
                if (m_oExt != null)
                    return;

                //Addin Name for Worksite 8.5 SP3 Update 2 and later
                object oAddin = "WorkSiteOffice2007Addins.Connect";
                try
                {
                    m_oExt = (iManageExtensibility)m_oIntegrator.ExcelApp.COMAddIns.Item(ref oAddin).Object;
                }
                catch { }
                //Addin Name for Worksite 8.5 SP2 or earlier
                oAddin = "oUTR02K.Connect";
                if (m_oExt == null)
                {
                    try
                    {
                        m_oExt = (iManageExtensibility)m_oIntegrator.ExcelApp.COMAddIns.Item(ref oAddin).Object;
                    }
                    catch { }
                }
                if (m_oExt == null)
                {
                    throw new LMP.Exceptions.NotInCollectionException("IManage COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void InitializePowerPoint()
        {
            try
            {
                //already initialized
                if (m_oExt != null)
                    return;

                //Addin Name for Worksite 8.5 SP3 Update 2 and later
                object oAddin = "WorkSiteOffice2007Addins.Connect";
                try
                {
                    m_oExt = (iManageExtensibility)m_oIntegrator.PowerPointApp.COMAddIns.Item(ref oAddin).Object;
                }
                catch { }
                //Addin Name for Worksite 8.5 SP2 or earlier
                oAddin = "oUTR02K.Connect";
                if (m_oExt == null)
                {
                    try
                    {
                        m_oExt = (iManageExtensibility)m_oIntegrator.PowerPointApp.COMAddIns.Item(ref oAddin).Object;
                    }
                    catch { }
                }
                if (m_oExt == null)
                {
                    throw new LMP.Exceptions.NotInCollectionException("IManage COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override string GetProfileInfo(string xItem)
        {
            NRTDocument oDoc = null;
            string xVar = m_oIntegrator.GetDocVarValue("DMS_Temp");

            if (!string.IsNullOrEmpty(xVar))
            {
                //we've written profile info to a doc var in the document-
                //use that, as the most current profile info is not yet
                //available through iManage
                return GetVarAttributeByID(xItem);
            }
            else
            {
                try
                {
                    oDoc = (NRTDocument)m_oExt.GetDocumentFromPath(m_oIntegrator.TargetAppFullName);
                }
                catch { }


                if (oDoc != null)
                {
                    int iAttribute = 0;
                    if (Int32.TryParse(xItem, out iAttribute))
                    {
                        AttributeID oItem = (AttributeID)iAttribute;
                        switch (oItem)
                        {
                            case AttributeID.nrDocNum:
                                return oDoc.Number.ToString();
                            case AttributeID.nrVersion:
                                return oDoc.Version.ToString();
                            case AttributeID.nrDatabase:
                                return oDoc.Database.Name;
                            case AttributeID.nrClass:
                                return oDoc.Class.Name;
                            case AttributeID.nrClassDescription:
                                return oDoc.Class.Description;
                            case AttributeID.nrCustom1:
                                return (string)oDoc.CustomAttributes.Item(1).Name;
                            case AttributeID.nrCustom2:
                                return (string)oDoc.CustomAttributes.Item(2).Name;
                            case AttributeID.nrCustom1Description:
                                return oDoc.CustomAttributes.Item(1).Description;
                            case AttributeID.nrCustom2Description:
                                return oDoc.CustomAttributes.Item(2).Description;
                            case AttributeID.nrAuthor:
                                return oDoc.Author.Name;
                            case AttributeID.nrAuthorDescription:
                                return oDoc.Author.FullName;
                            case AttributeID.nrLastUser:
                                return oDoc.LastUser.Name;
                            case AttributeID.nrLastUserDescription:
                                return oDoc.LastUser.FullName;
                            case AttributeID.nrComment:
                                return oDoc.Comment;
                            case AttributeID.nrCreateDate:
                                return oDoc.CreationDate.ToString();
                            case AttributeID.nrEditDate:
                                return oDoc.EditDate.ToString();
                            default:
                                return oDoc.GetAttributeByID(oItem).ToString();

                        }
                    }
                }
            }
            return "";
        }
        public override string DocNumber
        {
            get { return GetProfileInfo(IMAN_PROFILE_DOCNUMBER.ToString()); }
        }

        public override string Version
        {
            get { return GetProfileInfo(IMAN_PROFILE_VERSION.ToString()); }
        }

        public override string DocName
        {
            get { return GetProfileInfo(IMAN_PROFILE_DOCNAME.ToString()); }
        }
        public override string DocTypeID
        {
            get { return GetProfileInfo(IMAN_PROFILE_DOCTYPEID.ToString()); }
        }

        public override string DocTypeDescription
        {
            get { return GetProfileInfo(IMAN_PROFILE_DOCTYPEDESCRIPTION.ToString()); }
        }

        public override string Library
        {
            get { return GetProfileInfo(IMAN_PROFILE_LIBRARY.ToString()); }
        }

        public override string AuthorID
        {
            get { return GetProfileInfo(IMAN_PROFILE_AUTHORUSERID.ToString()); }
        }

        public override string TypistID
        {
            get { return GetProfileInfo(IMAN_PROFILE_TYPISTUSERID.ToString()); }
        }

        public override string AuthorName
        {
            get { return GetProfileInfo(IMAN_PROFILE_AUTHORFULLNAME.ToString()); }
        }
        public override string TypistName
        {
            get { return GetProfileInfo(IMAN_PROFILE_TYPISTFULLNAME.ToString()); }
        }
        public override string ClientID
        {
            get { return GetProfileInfo(IMAN_PROFILE_CLIENTID.ToString()); }
        }
        public override string ClientName
        {
            get { return GetProfileInfo(IMAN_PROFILE_CLIENTNAME.ToString()); }
        }

        public override string MatterID
        {
            get { return GetProfileInfo(IMAN_PROFILE_MATTERID.ToString()); }
        }

        public override string MatterName
        {
            get { return GetProfileInfo(IMAN_PROFILE_MATTERNAME.ToString()); }
        }

        public override string Abstract
        {
            get { return GetProfileInfo(IMAN_PROFILE_COMMENT.ToString()); }
        }

        public override string GroupName
        {
            get { return ""; }
        }

        public override string CreationDate
        {
            get 
            {
                try
                {
                    string xUseDocProp = GetSetting(m_xSection, "CreationDateFromDocProps");
                    if (xUseDocProp.ToUpper() == "TRUE")
                    {
                        string[] aDate = m_oIntegrator.GetDocPropValue("Creation Date").Split(' ');
                        return aDate[0];
                    }
                    else
                        return GetProfileInfo(IMAN_PROFILE_CREATIONDATE.ToString());
                }
                catch { }
                return "";
            }
        }

        public override string RevisionDate
        {
            get
            {
                try
                {
                    string xDocPropsDate = GetSetting(m_xSection, "EditDateFromDocProps");
                    string xSystemDate = GetSetting(m_xSection, "EditDateFromSystem");
                    string xFormat = GetSetting(m_xSection, "EditDateFromSystemFormat");
                    if (xDocPropsDate.ToUpper() == "TRUE")
                    {
                        string[] aDate = m_oIntegrator.GetDocPropValue("Last Save Time").Split(' ');
                        return aDate[0];
                    }
                    else if (xSystemDate.ToUpper() == "TRUE")
                    {
                        if (xFormat == "")
                            xFormat = "MM/d/yy";
                        return DateTime.Now.ToString(xFormat);
                    }
                    else
                        return GetProfileInfo(IMAN_PROFILE_CREATIONDATE.ToString());
                }
                catch { }
                return "";
            }
        }
        public override string Path
        {
            get
            {
                return m_oIntegrator.TargetAppPath;
            }
        }
        public override string FileName
        {
            get
            {
                return m_oIntegrator.TargetAppFileName;
            }
        }
        public override string Custom1
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 0 && !string.IsNullOrEmpty(m_aCustomFields[0]))
                    return GetProfileInfo(m_aCustomFields[0]);
                else
                    return "";
            }
        }

        public override string Custom2
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 1 && !string.IsNullOrEmpty(m_aCustomFields[1]))
                    return GetProfileInfo(m_aCustomFields[1]);
                else
                    return "";
            }
        }

        public override string Custom3
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 2 && !string.IsNullOrEmpty(m_aCustomFields[2]))
                    return GetProfileInfo(m_aCustomFields[2]);
                else
                    return "";
            }
        }
        public override void FileSave()
        {
            //Implemented for Word only
            switch (this.AppType)
            {
                case OfficeAppTypes.MSWord:
                    FileSaveWord();
                    break;
            }
        }
        private void FileSaveWord()
        {
            CommandBars oCBS = m_oIntegrator.WordApp.CommandBars;
            oCBS.ExecuteMso("FileSave");
        }
        public override void FileClose()
        {
            //Not implemented for IManage
        }
        public override void FileSaveAs()
        {
            //Not implemented for IManage
        }
        public override void DocClose()
        {
            //Not implemented for IManage
        }
        private string GetVarAttributeByID(string xItem)
        {
            //returns the value of the specified data
            //from the active document's doc var
            string xVar;
            int iPos;
            int iStart;
            int iEnd;

            try
            {

                xVar = m_oIntegrator.GetDocVarValue("DMS_Temp");
                if (xVar != "")
                {
                    //this data is available - test for field
                    iPos = xVar.IndexOf(xItem + "~");

                    if (iPos > -1)
                    {
                        //field exists - get start and end points of field data
                        iStart = iPos + xItem.Length + 1;
                        iEnd = xVar.IndexOf("||", iStart);
                        if (iEnd > 0)
                        {
                            //field delimiter exists
                            return xVar.Substring(iStart, iEnd - iStart);
                        }
                        else
                        {
                            return xVar.Substring(iStart);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }

            return "";
        }
    }
}