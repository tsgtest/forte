﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

using Hummingbird.DM.Server.Interop.PCDClient;
using Hummingbird.DM.Extensions.Interop.DOCSObjects;

namespace LMP.fDMS
{
    public class eDocsDMBackend : DMSBackend
    {
        const string DM_PROFILE_DOCNUMBER = "DOCNUM"; 
        const string DM_PROFILE_DOCNAME = "DOCNAME"; 
        const string DM_PROFILE_DOCTYPEID = "TYPE_ID";
        const string DM_PROFILE_DOCTYPEDESCRIPTION = "DOCUMENTTYPE"; 
        const string DM_PROFILE_LIBRARY = "LIBNAME";
        const string DM_PROFILE_AUTHORUSERID = "AUTHOR_ID";
        const string DM_PROFILE_AUTHORFULLNAME = "AUTHOR_FULL_NAME";
        const string DM_PROFILE_TYPISTUSERID = "TYPIST_ID"; 
        const string DM_PROFILE_CREATIONDATE = "CREATION_DATE";
        const string DM_PROFILE_LASTEDITDATE = "LASTEDITDATE";
        const string DM_PROFILE_COMMENT = "ABSTRACT";
        const string DM_PROFILE_CLIENTID = "CLIENT_ID";
        const string DM_PROFILE_MATTERID = "MATTER_ID";

        public eDocsDMBackend() : this(OfficeAppTypes.MSWord)
        {
        }
        public eDocsDMBackend(OfficeAppTypes iAppType)
            : base(iAppType)
        {
            m_iDMSType = DMSBackendType.eDocsDM;
            m_xSection = "eDocs DM";
            switch (iAppType)
            {
                case OfficeAppTypes.MSExcel:
                    break;
                case OfficeAppTypes.MSPowerPoint:
                    break;
                default:
                    InitializeWord();
                    break;
            }
            LoadCustomFields();
        }
        private void InitializeWord()
        {
            try
            {
                bool bAddinLoaded = false;
                foreach (COMAddIn oAddin in m_oIntegrator.WordApp.COMAddIns)
                {
                    if (oAddin.ProgId.ToUpper().StartsWith("DMWORD"))
                    {
                        bAddinLoaded = true;
                        break;
                    }
                }
                LMP.Trace.WriteNameValuePairs("bAddinLoaded", bAddinLoaded);
                if (!bAddinLoaded)
                {
                    throw new LMP.Exceptions.NotInCollectionException("DM COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override string GetProfileInfo(string xItem)
        {
            PCDDocObject oPCDDoc = null;
            Hummingbird.DM.Extensions.Interop.DOCSObjects.Application oDMApp = null;
            string xDocNum = "";
            string xLibrary = "";
            string xVersion = "";
            string xDST = "";
            string xForm = "";
            string xField = "";

            if (!GetDocNumVersionLibrary(m_oIntegrator.TargetAppFileName, ref xDocNum, ref xVersion, ref xLibrary))
                return "";

            oDMApp = new Hummingbird.DM.Extensions.Interop.DOCSObjects.Application();
            xDST = oDMApp.DST;
            LMP.Trace.WriteNameValuePairs("DST", xDST);
            switch (xItem.ToUpper())
            {
                case "CLIENT_ID":
                case "MATTER_ID":
                //These don't exist in v_defprof, use default profile form instead
                try
                {
                    xForm = oDMApp.CurrentLibrary. SelectProfileForm(0, null).FormName;
                }
                    catch
                {
                        xForm = "LAWPROF";
                    }
                    break;
                default:
                //Use internal form for other fields
                xForm = "v_defprof";
                    break;
            }
            LMP.Trace.WriteNameValuePairs("Form", xForm);
    
            //Gather profile information using DM API
            oPCDDoc = new PCDDocObject();
            oPCDDoc.SetDST(xDST);
            oPCDDoc.SetObjectType(xForm);
            //Set library incase the document is from a remote library
            oPCDDoc.SetProperty("%TARGET_LIBRARY", xLibrary);
            oPCDDoc.SetProperty("%OBJECT_IDENTIFIER", xDocNum);
            oPCDDoc.Fetch();
    
            try
            {
                xField = (string)oPCDDoc.GetReturnProperty(xItem);
            }
            catch {}
            LMP.Trace.WriteNameValuePairs("xItem", xItem, "xField", xField);

            return xField;
        }
        private bool GetDocNumVersionLibrary(string xFileName, ref string xDocNum, ref string xVersion, ref string xLibrary)
        {
            xDocNum = "";
            xVersion = "";
            xLibrary = "";

            if (xFileName.StartsWith(@"\\"))
                return false;

            string[] xElements = xFileName.Split('-');
            if (xElements.GetUpperBound(0) < 3)
            {
                return false;
            }
            else
            {
                xLibrary = xElements[0];
                LMP.Trace.WriteNameValuePairs("Library", xLibrary);
                if (xLibrary.Contains("_"))
                {
                    Hummingbird.DM.Extensions.Interop.DOCSObjects.Application oDMApp = new Hummingbird.DM.Extensions.Interop.DOCSObjects.Application();
                    string[] xLibraries = new string[oDMApp.Libraries.Count];
                    for (int i = 1; i <= oDMApp.Libraries.Count; i++)
                    {
                        LMP.Trace.WriteNameValuePairs("Library " + i.ToString(), oDMApp.Libraries[i].Name);
                        if (xLibrary.ToUpper() == oDMApp.Libraries[i].Name.ToUpper())
                            break;
                        else if (xLibrary.ToUpper().Replace("_", " ") == oDMApp.Libraries[i].Name.ToUpper())
                        {
                            xLibrary = oDMApp.Libraries[i].Name;
                            break;
                        }
                    }
                    LMP.Trace.WriteNameValuePairs("Library", xLibrary);
                }
                xDocNum = xElements[1];
                if (xDocNum.StartsWith("#"))
                    xDocNum = xDocNum.Substring(1);
                LMP.Trace.WriteNameValuePairs("DocNum", xDocNum);
                xVersion = xElements[2];
                if (xVersion.StartsWith("v", StringComparison.CurrentCultureIgnoreCase))
                    xVersion = xVersion.Substring(1);
                LMP.Trace.WriteNameValuePairs("Version", xVersion);
                return true;
            }
        }
        public override string DocNumber
        {
            get 
            {
                string xDocNum = "";
                string xVersion = "";
                string xLib = "";
                if (GetDocNumVersionLibrary(m_oIntegrator.TargetAppFileName, ref xDocNum, ref xVersion, ref xLib))
                    return xDocNum;
                else
                    return "";
            }
        }

        public override string Version
        {
            get
            {
                string xDocNum = "";
                string xVersion = "";
                string xLib = "";
                if (GetDocNumVersionLibrary(m_oIntegrator.TargetAppFileName, ref xDocNum, ref xVersion, ref xLib))
                    return xVersion;
                else
                    return "";
            }
        }

        public override string DocName
        {
            get { return GetProfileInfo(DM_PROFILE_DOCNAME); }
        }
        public override string DocTypeID
        {
            get { return GetProfileInfo(DM_PROFILE_DOCTYPEID); }
        }

        public override string DocTypeDescription
        {
            get { return GetProfileInfo(DM_PROFILE_DOCTYPEDESCRIPTION); }
        }

        public override string Library
        {
            get 
            {
                string xDocNum = "";
                string xVersion = "";
                string xLib = "";
                if (GetDocNumVersionLibrary(m_oIntegrator.TargetAppFileName, ref xDocNum, ref xVersion, ref xLib))
                    return xLib;
                else
                    return "";
            }
        }

        public override string AuthorID
        {
            get { return GetProfileInfo(DM_PROFILE_AUTHORUSERID); }
        }

        public override string TypistID
        {
            get { return GetProfileInfo(DM_PROFILE_TYPISTUSERID); }
        }

        public override string AuthorName
        {
            get { return GetProfileInfo(DM_PROFILE_AUTHORFULLNAME); }
        }
        public override string TypistName
        {
            get { return ""; }
        }
        public override string ClientID
        {
            get { return GetProfileInfo(DM_PROFILE_CLIENTID); }
        }
        public override string ClientName
        {
            get { return ""; }
        }

        public override string MatterID
        {
            get { return GetProfileInfo(DM_PROFILE_MATTERID); }
        }

        public override string MatterName
        {
            get { return ""; }
        }

        public override string Abstract
        {
            get { return GetProfileInfo(DM_PROFILE_COMMENT); }
        }

        public override string GroupName
        {
            get { return ""; }
        }

        public override string CreationDate
        {
            get { return GetProfileInfo(DM_PROFILE_CREATIONDATE); }
        }

        public override string RevisionDate
        {
            get { return GetProfileInfo(DM_PROFILE_LASTEDITDATE); }
        }
        public override string Path
        {
            get
            {
                return m_oIntegrator.TargetAppPath;
            }
        }
        public override string FileName
        {
            get
            {
                return m_oIntegrator.TargetAppFileName;
            }
        }
        public override string Custom1
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 0 && !string.IsNullOrEmpty(m_aCustomFields[0]))
                    return GetProfileInfo(m_aCustomFields[0]);
                else
                    return "";
            }
        }

        public override string Custom2
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 1 && !string.IsNullOrEmpty(m_aCustomFields[1]))
                    return GetProfileInfo(m_aCustomFields[1]);
                else
                    return "";
            }
        }

        public override string Custom3
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 2 && !string.IsNullOrEmpty(m_aCustomFields[2]))
                    return GetProfileInfo(m_aCustomFields[2]);
                else
                    return "";
            }
        }
        public override void FileSave()
        {
            //Implemented for Word only
            switch (this.AppType)
            {
                case OfficeAppTypes.MSWord:
                    FileSaveWord();
                    break;
            }
        }
        private void FileSaveWord()
        {
            LMP.Trace.WriteInfo("FileSaveWord");
            CommandBars oCBS = m_oIntegrator.WordApp.CommandBars;
            oCBS.ExecuteMso("FileSave");
        }
        public override void FileClose()
        {
            //Not implemented for DM
        }
        public override void FileSaveAs()
        {
            //Not implemented for DM
        }
        public override void DocClose()
        {
            //Not implemented for DM
        }
    }
}