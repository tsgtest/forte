﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDocuments.Client.COM.Interfaces;
using NetDocuments.Client.COM.Models;
using Microsoft.Office.Core;

namespace LMP.fDMS
{
    class NdOfficeBackend : DMSBackend
    {
        public NdOfficeBackend() : this(OfficeAppTypes.MSWord)
        {
        }
        public NdOfficeBackend(OfficeAppTypes iAppType)
            : base(iAppType)
        {
            m_iDMSType = DMSBackendType.ndOffice;
            m_xSection = "ndOffice";
            switch (iAppType)
            {
                case OfficeAppTypes.MSExcel:
                    InitializeExcel();
                    break;
                case OfficeAppTypes.MSPowerPoint:
                    InitializePowerPoint();
                    break;
                default:
                    InitializeWord();
                    break;
            }
            LoadCustomFields();
        }
        private void InitializeWord()
        {
            try
            {
                Microsoft.Office.Core.COMAddIn oAddin = null;
                object oName = "NetDocuments.Client.WordAddin";
                try
                {
                    oAddin = m_oIntegrator.WordApp.COMAddIns.Item(ref oName);
                }
                catch { }
                if (oAddin == null)
                {
                    LMP.Trace.WriteInfo("NdOffice Addin not loaded");
                    throw new LMP.Exceptions.NotInCollectionException("ndOffice COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void InitializeExcel()
        {
            try
            {
                Microsoft.Office.Core.COMAddIn oAddin = null;
                object oName = "NetDocuments.Client.ExcelAddin";
                try
                {
                    oAddin = m_oIntegrator.ExcelApp.COMAddIns.Item(ref oName);
                }
                catch { }
                if (oAddin == null)
                {
                    throw new LMP.Exceptions.NotInCollectionException("ndOffice COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void InitializePowerPoint()
        {
            try
            {
                Microsoft.Office.Core.COMAddIn oAddin = null;
                object oName = "NetDocuments.Client.PowerPointAddin";
                try
                {
                    oAddin = m_oIntegrator.PowerPointApp.COMAddIns.Item(ref oName);
                }
                catch { }
                if (oAddin == null)
                {
                    throw new LMP.Exceptions.NotInCollectionException("ndOffice COM Addin is not loaded.");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public override string GetProfileInfo(string xItem)
        {
            if (xItem == "")
                return "";

            string xVar = m_oIntegrator.GetDocVarValue("DMS_Temp");
            if (!string.IsNullOrEmpty(xVar))
            {
                LMP.Trace.WriteNameValuePairs("DMS_Temp", xVar);
                //we've written profile info to a doc var in the document-
                //use that, as the most current profile info is not yet
                //available through iManage
                return GetVarAttributeByID(xItem);
            }
            else
            {
                NdDocumentInfo oInfo = DocumentInfo;
                switch (xItem.ToUpper())
                {
                    case "ID":
                        return oInfo.Identifier;
                    case "VERSION":
                        return oInfo.Version.ToString();
                    case "FILENAME":
                        return oInfo.FileName;
                    case "CABINET":
                        return oInfo.Cabinet;
                    case "LASTMODIFIED":
                        return oInfo.LastModified.ToString();
                    default:
                        foreach (NdProfileAttribute oAttr in oInfo.ProfileAttributes)
                        {
                            if (oAttr.Name.ToUpper() == xItem.ToUpper())
                            {
                                return oAttr.Value.ToString();
                            }
                        }
                        break;
                }
            }
            return "";
        }
        private NdDocumentInfo DocumentInfo
        {
            get
            {
                Type type = Type.GetTypeFromProgID("ndOffice.EchoingDataService");
                try
                {
                    IEchoingDataService oEcho = (IEchoingDataService)Activator.CreateInstance(type);
                    if (oEcho != null)
                    {
                        return oEcho.GetDocumentInfo(m_oIntegrator.TargetAppFullName);
                    }
                }
                catch 
                {
                }
                return null;
            }
        }
        public override string DocNumber
        {
            get { return GetProfileInfo("ID"); }
        }
        private bool IsProfiled
        {
            get
            {
                //NetDocuments ID Pattern "????-????-????"
                string xNum = this.DocNumber;
                return xNum.Length == 14 && xNum.Substring(4, 1) == "-" && xNum.Substring(9, 1) == "-";
            }
        }
        private string GetAttributeNameFromXML(string xProp)
        {
            return GetSetting(m_xSection, xProp);
        }
        public override string Version
        {
            get { return GetProfileInfo("Version"); }
        }

        public override string DocName
        {
            get { return GetProfileInfo("DocName"); }
        }
        public override string DocTypeID
        {
            get 
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("DocTypeIDAttribute"));
                }
                else
                    return "";
            }
        }

        public override string DocTypeDescription
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("DocTypeDescriptionAttribute"));
                }
                else
                    return "";
            }
        }

        public override string Library
        {
            get { return GetProfileInfo("Cabinet"); }
        }

        public override string AuthorID
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("AuthorIDAttribute"));
                }
                else
                    return "";
            }
        }

        public override string TypistID
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("TypistIDAttribute"));
                }
                else
                    return m_oIntegrator.AppUserInitials;
            }
        }

        public override string AuthorName
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("AuthorNameAttribute"));
                }
                else
                    return m_oIntegrator.GetDocPropValue("Author");
            }
        }
        public override string TypistName
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("TypistNameAttribute"));
                }
                else
                    return m_oIntegrator.AppUserName;
            }
        }
        public override string ClientID
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("ClientIDAttribute"));
                }
                else
                    return "";
            }
        }
        public override string ClientName
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("ClientNameAttribute"));
                }
                else
                    return "";
            }
        }

        public override string MatterID
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("MatterIDAttribute"));
                }
                else
                    return "";
            }
        }

        public override string MatterName
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("MatterNameAttribute"));
                }
                else
                    return "";
            }
        }

        public override string Abstract
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("AbstractAttribute"));
                }
                else
                    return "";
            }
        }

        public override string GroupName
        {
            get { return ""; }
        }

        public override string CreationDate
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo(GetAttributeNameFromXML("CreationDateAttribute"));
                }
                else
                    return m_oIntegrator.GetDocPropValue("Creation Date");
            }
        }

        public override string RevisionDate
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo("LastModified");
                }
                else
                    return m_oIntegrator.GetDocPropValue("Last Save Time");
            }
        }
        public override string Path
        {
            get
            {
                return m_oIntegrator.TargetAppPath;
            }
        }
        public override string FileName
        {
            get
            {
                if (IsProfiled)
                {
                    return GetProfileInfo("FileName");
                }
                else
                    return m_oIntegrator.TargetAppFileName;
            }
        }
        public override string Custom1
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 0 && !string.IsNullOrEmpty(m_aCustomFields[0]))
                    return GetProfileInfo(m_aCustomFields[0]);
                else
                    return "";
            }
        }

        public override string Custom2
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 1 && !string.IsNullOrEmpty(m_aCustomFields[1]))
                    return GetProfileInfo(m_aCustomFields[1]);
                else
                    return "";
            }
        }

        public override string Custom3
        {
            get
            {
                if (m_aCustomFields.GetLength(0) > 2 && !string.IsNullOrEmpty(m_aCustomFields[2]))
                    return GetProfileInfo(m_aCustomFields[2]);
                else
                    return "";
            }
        }
        public override void FileSave()
        {
            //Implemented for Word only
            switch (this.AppType)
            {
                case OfficeAppTypes.MSWord:
                    FileSaveWord();
                    break;
            }
        }
        private void FileSaveWord()
        {
            CommandBars oCBS = m_oIntegrator.WordApp.CommandBars;
            oCBS.ExecuteMso("FileSave");
        }
        public override void FileClose()
        {
            //Not implemented for ndOffice
        }
        public override void FileSaveAs()
        {
            //Not implemented for ndOffice
        }
        public override void DocClose()
        {
            //Not implemented for ndOffice
        }
        private string GetVarAttributeByID(string xItem)
        {
            //returns the value of the specified data
            //from the active document's doc var
            string xVar;
            int iPos;
            int iStart;
            int iEnd;

            try
            {

                xVar = m_oIntegrator.GetDocVarValue("DMS_Temp");
                if (xVar != "" && xVar.ToUpper().StartsWith("NDOFFICE"))
                {
                    //this data is available - test for field
                    iPos = xVar.IndexOf(xItem + "~");

                    if (iPos > -1)
                    {
                        //field exists - get start and end points of field data
                        iStart = iPos + xItem.Length + 1;
                        iEnd = xVar.IndexOf("||", iStart);
                        if (iEnd > 0)
                        {
                            //field delimiter exists
                            return xVar.Substring(iStart, iEnd - iStart);
                        }
                        else
                        {
                            return xVar.Substring(iStart);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }

            return "";
        }
       }
}