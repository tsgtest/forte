﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Windows.Forms;

namespace LMP.fDMS
{
    internal class OfficeIntegrator
    {

        private OfficeAppTypes m_iAppType = OfficeAppTypes.MSWord;
        private Word.Application m_oWordApp;
        private Excel.Application m_oExcelApp;
        private PowerPoint.Application m_oPowerPointApp;

        internal OfficeIntegrator(OfficeAppTypes iType)
        {
            this.AppType = iType;
        }
        internal OfficeAppTypes AppType
        {
            get { return m_iAppType; }
            set { m_iAppType = value; }
        }
        internal string TargetAppPath
        {
            get
            {
                switch (m_iAppType)
                {
                    case OfficeAppTypes.MSExcel:
                        return this.ExcelApp.ActiveWorkbook.Path;
                    case OfficeAppTypes.MSPowerPoint:
                        return this.PowerPointApp.ActivePresentation.Path;
                    default:
                        return this.WordApp.ActiveDocument.Path;
                }
            }
        }
        internal string GetDocPropValue(string xPropName)
        {
            try
            {
                object oPropName = xPropName;
                switch (m_iAppType)
                {
                    case OfficeAppTypes.MSWord:
                        return LMP.Forte.MSWord.WordDoc.GetDocPropValue(m_oWordApp.ActiveDocument, xPropName);
                    case OfficeAppTypes.MSExcel:
                        try
                        {
                            //try as built-in property first
                            return ((DocumentProperties)m_oExcelApp.ActiveWorkbook.BuiltinDocumentProperties)[oPropName].Value.ToString();
                        }
                        catch
                        {
                            return ((DocumentProperties)m_oExcelApp.ActiveWorkbook.CustomDocumentProperties)[oPropName].Value.ToString();
                        }
                    case OfficeAppTypes.MSPowerPoint:
                        try
                        {
                            //try as built-in property first
                            return ((DocumentProperties)m_oPowerPointApp.ActivePresentation.BuiltInDocumentProperties)[oPropName].Value.ToString();
                        }
                        catch
                        {
                            return ((DocumentProperties)m_oPowerPointApp.ActivePresentation.CustomDocumentProperties)[oPropName].Value.ToString();
                        }
                }
            }
            catch { }
            return "";
        }
        internal string GetDocVarValue(string xPropName)
        {
            try
            {
                switch (m_iAppType)
                {
                    //No Doc Variables in Excel or PowerPoint, use Doc Props instead
                    case OfficeAppTypes.MSExcel:
                    case OfficeAppTypes.MSPowerPoint:
                        return GetDocPropValue(xPropName);
                    default:
                        return LMP.Forte.MSWord.WordDoc.GetDocumentVariableValue(m_oWordApp.ActiveDocument, xPropName, false);
                }
            }
            catch { }
            return "";
        }
        internal string TargetAppFileName
        {
            get
            {
                switch (m_iAppType)
                {
                    case OfficeAppTypes.MSExcel:
                        return this.ExcelApp.ActiveWorkbook.Name;
                    case OfficeAppTypes.MSPowerPoint:
                        return this.PowerPointApp.ActivePresentation.Name;
                    default:
                        return this.WordApp.ActiveDocument.Name;
                }
            }
        }
        internal string TargetAppFullName
        {
            get
            {
                switch (m_iAppType)
                {
                    case OfficeAppTypes.MSExcel:
                        return this.ExcelApp.ActiveWorkbook.FullName;
                    case OfficeAppTypes.MSPowerPoint:
                        return this.PowerPointApp.ActivePresentation.FullName;
                    default:
                        return this.WordApp.ActiveDocument.FullName;
                }
            }
        }
        internal Word.Application WordApp
        {
            get
            {
                if (m_oWordApp == null)
                {
                    m_oWordApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                }
                return m_oWordApp;
            }
        }
        internal Excel.Application ExcelApp
        {
            get
            {
                if (m_oExcelApp == null)
                {
                    m_oExcelApp = new Excel.Application();
                }
                return m_oExcelApp;
            }
        }
        internal int TargetAppFileCount
        {
            get
            {
                switch (this.AppType)
                {
                    case OfficeAppTypes.MSExcel:
                        if (this.ExcelApp.ActiveWorkbook == null)
                            return 0;
                        else
                            return this.ExcelApp.Workbooks.Count;
                    case OfficeAppTypes.MSPowerPoint:
                        if (this.PowerPointApp.ActivePresentation == null)
                            return 0;
                        else
                            return this.PowerPointApp.Presentations.Count;
                    default:
                        if (this.WordApp.ActiveDocument == null)
                            return 0;
                        else
                            return this.WordApp.Documents.Count;
                }
            }
        }
        internal PowerPoint.Application PowerPointApp
        {
            get
            {
                if (m_oPowerPointApp == null)
                {
                    m_oPowerPointApp = new PowerPoint.Application();
                }
                return m_oPowerPointApp;
            }
        }
        internal string AppUserName
        {
            get
            {
                switch (this.AppType)
                {
                    case OfficeAppTypes.MSExcel:
                        return ExcelApp.UserName;
                    case OfficeAppTypes.MSPowerPoint:
                        return "";
                    default:
                        return WordApp.UserName;
                }
            }
        }
        internal string AppUserInitials
        {
            get
            {
                switch (this.AppType)
                {
                    case OfficeAppTypes.MSExcel:
                    case OfficeAppTypes.MSPowerPoint:
                        return "";
                    default:
                        return WordApp.UserInitials;
                }
            }
        }
    }
}
