using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using Infragistics.Win.UltraWinTree;
using LMP.Administration.Controls;
using LMP.Data;
namespace LMP.MacPac10.Administration
{
    public partial class MainForm : Form
    {
        #region *********************static members*********************
        [STAThread]
        static void Main()
        {
            try
            {
                System.Windows.Forms.Application.EnableVisualStyles();
                System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                System.Windows.Forms.Application.Run(new MainForm());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************constructors*********************
        public MainForm()
        {
            InitializeComponent();
            try
            {
                //login to the data component
                bool bRet = LMP.Data.Application.Login(System.Environment.UserName, true);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DBConnectionException(
                    LMP.Resources.GetLangString("Error_CouldNotLoginToAdminDB"), oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategories();
                UltraTreeNode oTreeNode = this.treeCategories.Nodes[0];
                oTreeNode.Selected = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// ensure current usercontrol is valid before permitting navigation to another control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void treeCategories_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
        {
            try
            {
                if (this.treeCategories.SelectedNodes.Count == 0)
                    return;
                UltraTreeNode oNode = this.treeCategories.SelectedNodes[0];
                if (oNode.Tag is UserControl)
                {
                    if (!((LMP.Administration.Controls.AdminManagerBase)oNode.Tag).IsValid)
                        e.Cancel = true;
                    else
                        //notify admin base class control will be unloaded
                        ((AdminManagerBase)(Control)oNode.Tag)
                                .OnBeforeControlUnloaded(this.treeCategories, new System.EventArgs());
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeCategories_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                Control oCtl;
                //get currently selected node
                UltraTreeNode oNode = e.NewSelections[0];
                //freeze the window
                LMP.WinAPI.LockWindowUpdate(this.Handle.ToInt32());
                //remove the previous control, if any
                this.scPanels.Panel2.Controls.Clear();
                if (oNode.Tag is UserControl)
                    //the control has already been created - just show
                    oCtl = (Control)oNode.Tag;
                else
                {
                    //control has not yet been created - 
                    //use reflection to create instance
                    string xClassName = oNode.Tag.ToString();
                    Assembly oA = Assembly.GetAssembly(
                        typeof(LMP.Administration.Controls.GroupsManager));
                    oCtl = (Control)oA.CreateInstance("LMP.Administration.Controls." +
                        xClassName);
                    //add control to node tag for future use
                    oNode.Tag = oCtl;
                }
                //add control to collection
                this.scPanels.Panel2.Controls.Add(oCtl);
                //fill panel with control
                oCtl.Dock = DockStyle.Fill;
                //notify admin base class control has been loaded
                ((AdminManagerBase)oCtl)
                        .OnAfterControlLoaded(oCtl, new System.EventArgs());
                //select the loaded control
                oCtl.Focus();
                //unfreeze window
                LMP.WinAPI.LockWindowUpdate(0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //GLOG 8316
                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    SaveCurrentRecord();
                }
                else
                {

                    DialogResult oDR = MessageBox.Show("Save changes?", LMP.String.MacPacProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                    {
                        SaveCurrentRecord();
                        CopyDB();
                    }
                }
                return;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.Exit();
                //CopyDB();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************private methods*********************
        private void LoadCategories()
        {
            TreeNodesCollection oNodes = this.treeCategories.Nodes;
            UltraTreeNode oNode;
            oNode = oNodes.Add("  People");
            oNode.Tag = "PeopleManager";
        }
        private void SaveCurrentRecord()
        {
            if (this.scPanels.Panel2.Controls.Count > 0)
            {
                //ensure that current record is saved if necessary
                Control oCtl = this.scPanels.Panel2.Controls[0];
                ((LMP.Administration.Controls.AdminManagerBase)oCtl).SaveCurrentRecord();
            }
        }
        private static void CopyDB()
        {
            try
            {
                //get admin db full name
                string xAdminDB = LMP.Data.Application.GetDirectory(mpDirectories.WritableDB) + @"\" + LMP.Data.Application.AdminDBName;
                string xUserDB = LMP.Data.Application.GetDirectory(mpDirectories.WritableDB) + @"\" + LMP.Data.Application.UserDBName;

                //copy admin db to Forte.mdb and overwrite if this file exists.
                File.Copy(xAdminDB, xUserDB, true);
                //open connection to local db
                using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xUserDB +
                    ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                    @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"))
                {
                    oLocalDBCnn.Open();
                    using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                    {
                        int iNumRowsAffected = 0;
                        //try
                        //{
                        //    //Clear existing records except for TagPrefixID and EncryptionPassword
                        //    oCommand.CommandText = "DELETE * FROM Metadata WHERE [Name] <> 'TagPrefixID' AND [Name] <> 'EncryptionPassword' AND [Name] <> 'DBStructureUpdateIDs';";
                        //    oCommand.CommandType = CommandType.Text;
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //}
                        //catch { }
                        try
                        {
                            oCommand.CommandText = "UPDATE Metadata SET [Value] = 'false' WHERE [Name] = 'IsAdminDb';";
                            oCommand.CommandType = CommandType.Text;
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                            if (iNumRowsAffected == 0)
                            {
                                // There was no record to update. Add a record.
                                oCommand.CommandText = "Insert into Metadata([Name], [Value]) VALUES('IsAdminDb', 'false')";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        //try
                        //{
                        //    //set DefaultServer key in DB Metadata table
                        //    oCommand.CommandText = "UPDATE Metadata SET [Value]='" + oDlg.Server + "' WHERE [Name]='DefaultServer'";
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    if (iNumRowsAffected == 0)
                        //    {
                        //        // There was no record to update. Add a record.
                        //        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DefaultServer', '" + oDlg.Server + "')";
                        //        iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    LMP.Error.Show(ex);
                        //}
                        ////set DefaultDatabase key in DB Metadata table
                        //try
                        //{
                        //    oCommand.CommandText = "UPDATE Metadata SET [Value] = '" + oDlg.Database + "' WHERE [Name] = 'DefaultDatabase'";
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    if (iNumRowsAffected == 0)
                        //    {
                        //        // There was no record to update. Add a record.
                        //        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DefaultDatabase', '" + oDlg.Database + "')";
                        //        iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    LMP.Error.Show(ex);
                        //}
                        //try
                        //{
                        //    //set sync server uri key in DB Metadata table
                        //    oCommand.CommandText = "UPDATE Metadata SET [Value]='" + oDlg.IISServerIPAddress + "' WHERE [Name]='IISServerIPAddress'";
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    if (iNumRowsAffected == 0)
                        //    {
                        //        // There was no record to update. Add a record.
                        //        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('IISServerIPAddress', '" + oDlg.IISServerIPAddress + "')";
                        //        iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    LMP.Error.Show(ex);
                        //}
                        //try
                        //{
                        //    //set date created
                        //    oCommand.CommandText = "UPDATE Metadata SET [Value]='" +
                        //        String.ConvertDateToISO8601(LMP.Data.Application.GetCurrentEditTime())
                        //        + "' WHERE [Name]='DateCreated'";
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    if (iNumRowsAffected == 0)
                        //    {
                        //        // There was no record to update. Add a record.
                        //        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DateCreated', '"
                        //            + String.ConvertDateToISO8601(LMP.Data.Application.GetCurrentEditTime()) + "')";
                        //        iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    LMP.Error.Show(ex);
                        //}
                        //try
                        //{
                        //    //clear out people table -
                        //    //related records will be cleared automatically
                        //    //due to referential integrity
                        //    oCommand.CommandText = "DELETE * FROM People";
                        //    iNumRowsAffected = oCommand.ExecuteNonQuery();
                        //}
                        //catch (Exception ex)
                        //{
                        //    LMP.Error.Show(ex);
                        //}
                        ////clear out UserSegments, VariableSets, FavoritePeople, Deletions and Permissions, 
                        //since these aren't set for referential integrity
                        try
                        {
                            oCommand.CommandText = "DELETE * FROM VariableSets";
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        try
                        {
                            oCommand.CommandText = "DELETE * FROM UserSegments";
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        try
                        {
                            oCommand.CommandText = "DELETE * FROM Permissions";
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        try
                        {
                            //GLOG : 6628 : JSW
							//Add favorite people to each new user created by admin

							//create an array of each id over 9999
                            string xSQL = "SELECT ID1 FROM People WHERE ID1 > 9999";
                            ArrayList aNewUserIDArray = SimpleDataCollection.GetArray(xSQL);
                       
                            //create an array of each generic jazz person 
                            xSQL = "SELECT ID1 FROM People WHERE UserID = 'HerbertHancock' OR UserID = 'SidneyBechet' OR " +
                                "UserID = 'JohnColtrane' OR UserID = 'HerbertHancock' OR UserID = 'SidneyBechet' OR " +
                                 "UserID = 'EdwardEllington' OR UserID = 'JohnGillespie' OR UserID = 'BranfordMarsalis' OR " +
                                 "UserID = 'CharlesRouse' OR UserID = 'NicholasLaRocca' OR UserID = 'RosemaryClooney' OR " +
                                 "UserID = 'JosephineBaker'";

                            ArrayList aJazzPeople = SimpleDataCollection.GetArray(xSQL);

                            try
                            {
                                //for each new user assign generic favorite people
                                for (int i = 0; i < aNewUserIDArray.Count; i++)
                                {

                                    for (int y = 0; y < aJazzPeople.Count; y++)
                                    {
                                        object[] aNewUserItem = (object[])aNewUserIDArray[i];
                                        object[] aJazzPeopleItem = (object[])aJazzPeople[y];

                                        xSQL = "INSERT INTO FavoritePeople (OwnerID1, OwnerID2, PersonID1, PersonID2, LastEditTime) " +
                                            "VALUES (" + aNewUserItem[0].ToString() + ", 0, " + aJazzPeopleItem[0].ToString() + ", 0, Now())";

                                        oCommand.CommandText = xSQL;
                                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    }
                                }
                            }
                            catch
                            { }

                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        try
                        {
                            oCommand.CommandText = "DELETE * FROM Deletions";
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                        try
                        {
                            //Remove user-related Keyset records
                            oCommand.CommandText = "DELETE * FROM KeySets WHERE OwnerID1 > 0";
                            iNumRowsAffected = oCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            LMP.Error.Show(ex);
                        }
                    }

                    oLocalDBCnn.Close();
                }
                //MessageBox.Show("The user database '" + xUserDB + "' can now be deployed.",
                //    LMP.ComponentProperties.ProductName,
                //    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}
