using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data; //GLOG 6983

namespace LMP.Architect.Api
{
    public class Trailer : AdminSegment, LMP.Architect.Base.IDocumentStamp, ISingleInstanceSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //GLOG 3069: Check if XML contains an mSEG in Main story and insertion point is inside a table
                if (Segment.TargetIsBody(xXML) && oLocation.Characters.Last.Tables.Count > 0)
                {
                    //GLOG 2908: Textbox cannot be anchored inside a table - need to relocate insertion
                    int iStartPage = (int)oLocation.get_Information(Word.WdInformation.wdActiveEndPageNumber);

                    object oTable = Word.WdUnits.wdTable;
                    object oPara = Word.WdUnits.wdParagraph;
                    object oCount = 2;
                    object oMove = Word.WdMovementType.wdMove;
                    //Move to end of table
                    oLocation.EndOf(ref oTable, ref oMove);
                    //Move past end of row
                    oLocation.Move(ref oPara, ref oCount);
                    //GLOG 3103: if stamp would end up on a different page, prompt to continue
                    int iEndPage = (int)oLocation.get_Information(Word.WdInformation.wdActiveEndPageNumber);
                    if (iStartPage < iEndPage)
                    {
                        DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_CantInsertContentInTable"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (iRet != DialogResult.OK)
                            throw new LMP.Exceptions.SegmentInsertionCancelledException("Insertion cancelled");
                    }
                }
                //if this is end-of document trailer, check for existence of BCC mVar
                //and insert before that if found
                //GLOG 6498 (dm) - check whether at end of targeted section - we were
                //previously skipping this block when not at end of doc
                if (oLocation.StoryType == Microsoft.Office.Interop.Word.WdStoryType.wdMainTextStory 
                    && (oLocation.Start + 1 >= oLocation.Sections[1].Range.End))
                {
                    Variable oVar = null;
                    for (int i = this.ForteDocument.Segments.Count - 1; i >= 0; i--)
                    {
                        oVar = this.FindLocationVariable(this.ForteDocument.Segments[i]);
                        if (oVar != null)
                        {
                            if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            {
                                //Only reposition if Variable is in main story and not in deleted scope
                                if (oVar.TagType == ForteDocument.TagTypes.Variable &&
                                    oVar.AssociatedWordTags[0].Range.StoryType == Word.WdStoryType.wdMainTextStory)
                                {
                                    Word.Range oBCCRange = oVar.AssociatedWordTags[0].Range.Paragraphs[1].Range;
                                    //GLOG 6494 (dm) - don't reposition if BCC is in a different section
                                    if (oLocation.Sections[1].Index == oBCCRange.Sections[1].Index)
                                        oLocation.SetRange(oBCCRange.Start - 1, oBCCRange.Start - 1);
                                }
                            }
                            else
                            {
                                //Only reposition if Variable is in main story and not in deleted scope
                                if (oVar.TagType == ForteDocument.TagTypes.Variable &&
                                    oVar.AssociatedContentControls[0].Range.StoryType == Word.WdStoryType.wdMainTextStory)
                                {
                                    Word.Range oBCCRange = oVar.AssociatedContentControls[0].Range.Paragraphs[1].Range;
                                    //GLOG 6494 (dm) - don't reposition if BCC is in a different section
                                    if (oLocation.Sections[1].Index == oBCCRange.Sections[1].Index)
                                        oLocation.SetRange(oBCCRange.Start - 1, oBCCRange.Start - 1);
                                }
                            }
                            break;
                        }
                    }
                }
                if (!this.ForteDocument.IsSectionMarkedForNoTrailer(oLocation.Sections[1].Index) && this.ForteDocument.AllowDocumentTrailer)
                {
                    //insert new Trailer
                    //GLOG 4243
                    Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                        this.ForteDocument.Mode == ForteDocument.Modes.Design ? 
                        LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty
                        : LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False,
                        true, iIntendedUse, mpObjectTypes.Trailer, this.ForteDocument); //GLOG 6983
                }
                LMP.Benchmarks.Print(t0);
            }
            catch (LMP.Exceptions.SegmentInsertionCancelledException oC)
            {
                throw oC;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        public void DeleteExistingInstance(Word.Section oSection)
        {
            //get existing segment of this type
            Segment oExistingTrailer = null;
            Segments oSegs = null;

            //get from location
            oSegs = GetExistingSegments(oSection);

            if (oSegs != null && oSegs.Count > 0)
            {
                oExistingTrailer = oSegs[0];
            }

            //delete existing Trailer
            if (oExistingTrailer != null)
            {
                try
                {
                    this.ForteDocument.DeleteSegment(oExistingTrailer);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentException(
                        LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
                }
            }
        }
        public Segments GetExistingSegments(Word.Section oSection)
        {
            //get existing segment of this type
            Segments oSegs = null;

            ////GLOG - 2476 - ceh
            try
            {
                //get from location
                oSegs = Segment.GetSegments(this.ForteDocument,
                    oSection, this.Definition.TypeID, 1);
            }
            catch { }

            return oSegs;
        }
        /// <summary>
        /// Check for existence of predefined variable name to position trailer above
        /// </summary>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        private Variable FindLocationVariable(Segment oSeg)
        {
            Variable oVar = null;
            try
            {
                oVar = oSeg.Variables.ItemFromName("BCC");
            }
            catch {}
            if (oVar == null)
            {
                try
                {
                    oVar = oSeg.Variables.ItemFromName("bcc");
                }
                catch { }
            }
            if (oVar == null)
            {
                //Check child segments also
                for (int i = oSeg.Segments.Count - 1; i >= 0; i--)
                {
                    oVar = FindLocationVariable(oSeg.Segments[i]);
                    if (oVar != null)
                        break;
                }
            }
            return oVar;
        }
    }
}
