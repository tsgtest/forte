using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Api
{
    public class DraftStamp : AdminSegment, LMP.Architect.Base.IDocumentStamp //, ISingleInstanceSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //GLOG 3069: Check if XML contains an mSEG in Main story and insertion point is inside a table
                if (Segment.TargetIsBody(xXML) && oLocation.Characters.Last.Tables.Count > 0)
                {
                    //GLOG 2908: Textbox cannot be anchored inside a table - need to relocate insertion
                    int iStartPage = (int)oLocation.get_Information(Word.WdInformation.wdActiveEndPageNumber);

                    object oTable = Word.WdUnits.wdTable;
                    object oPara = Word.WdUnits.wdParagraph;
                    object oCount = 2;
                    object oMove = Word.WdMovementType.wdMove;
                    //Move to end of table
                    oLocation.EndOf(ref oTable, ref oMove);
                    //Move past end of row
                    oLocation.Move(ref oPara, ref oCount);
                    //GLOG 3103: if stamp would end up on a different page, prompt to continue
                    int iEndPage = (int)oLocation.get_Information(Word.WdInformation.wdActiveEndPageNumber);
                    if (iStartPage < iEndPage)
                    {
                        DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_CantInsertContentInTable"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (iRet != DialogResult.OK)
                            throw new LMP.Exceptions.SegmentInsertionCancelledException("Insertion cancelled");
                    }
                }
                //NOTE: draft stamp is no longer a single instance
                //segment - this mimics mp9 functionality - df

                ////get existing segment of this type
                //Segment oExistingDraftStamp = null;
                ////get from locat  ion
                //Segments oSegs = Segment.GetSegments(this.ForteDocument,
                //    oLocation.Sections.First, this.Definition.TypeID, 1);
                //if (oSegs.Count == 1)
                //{
                //    oExistingDraftStamp = oSegs[0];
                //}

                ////delete existing DraftStamp
                //if (oExistingDraftStamp != null)
                //{
                //    try
                //    {
                //        this.ForteDocument.DeleteSegment(oExistingDraftStamp);
                //    }
                //    catch (System.Exception oE)
                //    {
                //        throw new LMP.Exceptions.SegmentException(
                //            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
                //    }
                //}

                //insert new DraftStamp
                //GLOG 4526: Always use Append, since this isn't a Single Instance type
                Segment.InsertXML(xXML, oLocation, LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Append,
                    this.ForteDocument.Mode == ForteDocument.Modes.Design ? 
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty
                    : LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, true,
                    iIntendedUse, mpObjectTypes.DraftStamp, this.ForteDocument); //GLOG 6983

                LMP.Benchmarks.Print(t0);
            }
            catch (LMP.Exceptions.SegmentInsertionCancelledException oC)
            {
                throw oC;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
