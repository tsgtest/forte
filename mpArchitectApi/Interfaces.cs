using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Api
{
    public interface ISingleInstanceSegment : LMP.Architect.Base.ISingleInstanceSegment
    {
        Segments GetExistingSegments(Word.Section oSection);
    }
}
