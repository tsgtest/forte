using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Architect : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                LMP.Forte.MSWord.mpSectionOnePageSetupOptions iPageSetup = Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty;
                //GLOG 8836: Don't replace page setup if Paragraph or Sentence Text segment is being inserted in existing Segment, even if body is empty
                if (oParent != null && (iIntendedUse == mpSegmentIntendedUses.AsParagraphText || iIntendedUse == mpSegmentIntendedUses.AsSentenceText))
                {
                    iPageSetup = Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False;
                }
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    iPageSetup, bTargeted,
                    iIntendedUse, mpObjectTypes.Architect, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
