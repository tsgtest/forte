﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class PleadingExhibit : AdminSegment, LMP.Architect.Base.ILitigationAddOnSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.PleadingExhibit, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
    public class AgreementExhibit : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.AgreementExhibit, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
