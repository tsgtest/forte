﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.Architect.Api
{
    public partial class PaperTrayForm : Form
    {
        private string[] m_arTrays;
        private int[] m_arIDs;
        public PaperTrayForm(string xSegmentTypeName)
        {
            InitializeComponent();
            //GLOG 4752: Use Base function to retrieve bin information
            LMP.Forte.MSWord.Application.GetPrinterBinNamesAndIDs(ref m_arTrays, ref m_arIDs);
            //setup the paper tray combo
            foreach (string xTray in m_arTrays)
            {
                this.lstPaperTray.Items.Add(xTray);
            }

            if (m_arTrays.GetUpperBound(0) >= 0)
            {
                this.lstPaperTray.SelectedIndex = 0;
            }

            this.lblPaperTray.Text = "Select Paper Tray for " + xSegmentTypeName + ":";
        }

        /// <summary>
        /// returns the selected paper tray
        /// </summary>
        public int PaperTray
        {
            get
            {
                return m_arIDs[this.lstPaperTray.SelectedIndex];
            }
        }
    }
}
