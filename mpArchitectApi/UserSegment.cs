using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Api
{
    public class SavedContent : UserSegment
    {
        public override void InsertXML(string xXML, Segment oParent, 
            Word.Range oLocation, LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, 
            bool bTargeted, mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    mpSegmentIntendedUses.AsDocument, mpObjectTypes.SavedContent, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
    public class UserSegment : Segment
    {
        #region *********************fields*********************
        #endregion
		#region *********************constructors*********************
        public UserSegment(): base() { }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the definition of the segment
        /// </summary>
        public new UserSegmentDef Definition
        {
            get { return (UserSegmentDef)base.Definition; } //GLOG 8496
            internal set { base.Definition = value; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// inserts the user segment xml at the specified location
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oLocation"></param>
        /// <returns>returns the unindexed TagID of the inserted segment</returns>
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.UserSegment, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
        protected override void Initialize(string xTagID, Segment oParent)
        {
            base.Initialize(xTagID, oParent);

            //get segment properties as list dictionary
            ListDictionary oLD = this.Nodes
                .GetItemObjectDataListDictionary(this.FullTagID);

            //get segment properties -
            //get segment ID first
            string xID = oLD["SegmentID"].ToString();

            int iID1;
            int iID2;

            //parse ID into integer components
            LMP.Data.Application.SplitID(xID, out iID1, out iID2);

            this.ID1 = iID1;
            this.ID2 = iID2;

            this.TypeID = (mpObjectTypes)Int32.Parse(oLD["ObjectTypeID"].ToString());
            this.Name = oLD["Name"].ToString();
            this.DisplayName = oLD["DisplayName"].ToString();
            if (oLD["HelpText"] != null)
                this.HelpText = oLD["HelpText"].ToString();
            if (oLD["IntendedUse"] != null)
                this.IntendedUse = (mpSegmentIntendedUses)Int32.Parse(oLD["IntendedUse"].ToString());

            //these are optional values - ignore missing values

            //these are optional values - ignore missing values
            if (PropertyValueExists(oLD, "ShowChooser"))
                this.ShowChooser = Boolean.Parse(oLD["ShowChooser"].ToString());
            if (PropertyValueExists(oLD, "MaxAuthors"))
                this.MaxAuthors = Int32.Parse(oLD["MaxAuthors"].ToString());
            if (PropertyValueExists(oLD, "AuthorNodeUILabel"))
            {
                //GLOG 6653
                string xAuthorNodeUILabel = oLD["AuthorNodeUILabel"].ToString();
                if (xAuthorNodeUILabel == "1" || string.IsNullOrEmpty(xAuthorNodeUILabel))
                {
                    this.AuthorsNodeUILabel = "Author";
                }
                else if (xAuthorNodeUILabel == "2")
                {
                    this.AuthorsNodeUILabel = "Attorney";
                }
                else
                {
                    this.AuthorsNodeUILabel = xAuthorNodeUILabel;
                }
            }
            if (PropertyValueExists(oLD, "AuthorsHelpText"))
                this.AuthorsHelpText = oLD["AuthorsHelpText"].ToString();

            if (PropertyValueExists(oLD, "AuthorControlProperties"))
                this.AuthorControlProperties = oLD["AuthorControlProperties"].ToString();
            if (PropertyValueExists(oLD, "TranslationID"))
                this.TranslationID = Int32.Parse(oLD["TranslationID"].ToString());
            if (PropertyValueExists(oLD, "DefaultWrapperID"))
                this.DefaultWrapperID = Int32.Parse(oLD["DefaultWrapperID"].ToString());
            if (PropertyValueExists(oLD, "IsTransparentDef"))
                this.IsTransparentDef = bool.Parse(oLD["IsTransparentDef"].ToString());
            if (PropertyValueExists(oLD, "DisplayWizard"))
                this.DisplayWizard = bool.Parse(oLD["DisplayWizard"].ToString());

            if (PropertyValueExists(oLD, "WordTemplate"))
                this.WordTemplate = oLD["WordTemplate"].ToString();
            if (PropertyValueExists(oLD, "RequiredStyles"))
                this.RequiredStyles = oLD["RequiredStyles"].ToString();

            //transparent if defined as such, not top-level, and the only sibling of its type
            if (!this.IsTopLevel)
            {
                if (this.IsOnlyInstanceOfType)
                    this.IsTransparent = this.IsTransparentDef;
                else
                {
                    //ensure that no siblings of the same type are transparent
                    Segments oSiblings = this.Parent.Segments;
                    for (int i = 0; i < oSiblings.Count; i++)
                    {
                        Segment oSibling = oSiblings[i];
                        if ((oSibling.TypeID == this.TypeID) &&
                            (oSibling.FullTagID != this.FullTagID))
                            oSibling.IsTransparent = false;
                    }
                }
            }

            //get LinkAuthorsToParent properties - definition and runtime
            if (PropertyValueExists(oLD, "LinkAuthorsToParentDef"))
                this.LinkAuthorsToParentDef = (LinkAuthorsToParentOptions)
                    Int32.Parse(oLD["LinkAuthorsToParentDef"].ToString());

            this.LinkAuthorsToParent = ((this.LinkAuthorsToParentDef == LinkAuthorsToParentOptions.Always) ||
                ((this.LinkAuthorsToParentDef == LinkAuthorsToParentOptions.FirstChildOfType) &&
                this.IsOnlyInstanceOfType));

            //get author control properties
            if (PropertyValueExists(oLD, "AuthorControlActions"))
                this.Authors.ControlActions.SetFromString(oLD["AuthorControlActions"].ToString());

            if (PropertyValueExists(oLD, "L0"))
            {
                //if L0 exists, L1-L4 also exist
                if (oLD["L0"] != null && oLD["L1"] != null && oLD["L2"] != null
                && oLD["L3"] != null && oLD["L4"] != null)
                {
                    //all levels exist - get levels
                    this.L0 = Int32.Parse(oLD["L0"].ToString());
                    this.L1 = Int32.Parse(oLD["L1"].ToString());
                    this.L2 = Int32.Parse(oLD["L2"].ToString());
                    this.L3 = Int32.Parse(oLD["L3"].ToString());
                    this.L4 = Int32.Parse(oLD["L4"].ToString());
                }
                else if (oLD["L0"] != null || oLD["L1"] != null || oLD["L2"] != null
                || oLD["L3"] != null || oLD["L4"] != null)
                    //some levels exist, while others don't - alert
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString("Error_MissingSegmentLevelsProperty"));
            }

            //GLOG 3413: Test for valid integer value in ObjectData before attempting to set these
            int iTest = 0;
            if (PropertyValueExists(oLD, "MenuInsertionOptions") && Int32.TryParse(oLD["MenuInsertionOptions"].ToString(), out iTest))
                this.MenuInsertionOptions = (InsertionLocations)iTest;
            if (PropertyValueExists(oLD, "DefaultMenuInsertionBehavior") && Int32.TryParse(oLD["DefaultMenuInsertionBehavior"].ToString(), out iTest))
                this.DefaultMenuInsertionBehavior = (InsertionBehaviors)iTest;
            if (PropertyValueExists(oLD, "DefaultDragLocation") && Int32.TryParse(oLD["DefaultDragLocation"].ToString(), out iTest))
                this.DefaultDragLocation = (InsertionLocations)iTest;
            if (PropertyValueExists(oLD, "DefaultDragBehavior") && Int32.TryParse(oLD["DefaultDragBehavior"].ToString(), out iTest))
                this.DefaultDragBehavior = (InsertionBehaviors)iTest;
            if (PropertyValueExists(oLD, "DefaultDoubleClickLocation") && Int32.TryParse(oLD["DefaultDoubleClickLocation"].ToString(), out iTest))
                this.DefaultDoubleClickLocation = (InsertionLocations)iTest;
            if (PropertyValueExists(oLD, "DefaultDoubleClickBehavior") && Int32.TryParse(oLD["DefaultDoubleClickBehavior"].ToString(), out iTest))
                this.DefaultDoubleClickBehavior = (InsertionBehaviors)iTest;

            //get jurisdiction control properties
            if (PropertyValueExists(oLD, "ShowCourtChooser"))
                this.ShowCourtChooser = Boolean.Parse(oLD["ShowCourtChooser"].ToString());
            if (PropertyValueExists(oLD, "CourtChooserUILabel"))
                this.CourtChooserUILabel = oLD["CourtChooserUILabel"].ToString();
            if (PropertyValueExists(oLD, "CourtChooserHelpText"))
                this.CourtChooserHelpText = oLD["CourtChooserHelpText"].ToString();
            if (PropertyValueExists(oLD, "CourtChooserControlProperties"))
                this.CourtChooserControlProperties = oLD["CourtChooserControlProperties"].ToString();
            if (PropertyValueExists(oLD, "CourtChooserControlActions"))
            {
                this.CourtChooserControlActions.SetFromString(oLD["CourtChooserControlActions"].ToString());
            }
            //get dialog options
            if (PropertyValueExists(oLD, "ShowSegmentDialog"))
                this.ShowSegmentDialog = Boolean.Parse(oLD["ShowSegmentDialog"].ToString());
            if (PropertyValueExists(oLD, "DialogCaption"))
                this.DialogCaption = oLD["DialogCaption"].ToString();
            if (PropertyValueExists(oLD, "DialogTabCaptions"))
                this.DialogTabCaptions = oLD["DialogTabCaptions"].ToString();
            if (this.Definition == null)
            {
                try
                {
                    //get the segment def for this segment - one may not exist, e.g. when a segment has not yet been saved
                    this.Definition = (UserSegmentDef)Segment.GetDefFromID(this.ID);
                }

                catch { }
            }
        }
        #endregion
    }
}
