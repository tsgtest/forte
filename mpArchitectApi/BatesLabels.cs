using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class BatesLabels : AdminSegment, LMP.Architect.Base.IStaticCreationSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.BatesLabels, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        public override bool PrepareForFinish()
        {
            string xInvalidVarDisplayName = null;

            //GLOG item #6377 - dcf
            string xBasePath = this.TagID + ".";

            Prefill oPrefill = new Prefill(this);
            
            //validate data
            if (!String.IsNumericInt32(oPrefill[xBasePath + "FontSize"]))
            {
                xInvalidVarDisplayName = "Font Size";
            }
            else if (!String.IsNumericInt32(oPrefill[xBasePath + "NumberOfLabels"]))
            {
                xInvalidVarDisplayName = "Number of Labels";
            }
            else if (!String.IsNumericInt32(oPrefill[xBasePath + "StartingNumber"]))
            {
                xInvalidVarDisplayName = "Starting Number";
            }

            //GLOG : 6377 : CEH
            if (xInvalidVarDisplayName != null)
            {
                MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_InvalidVariableValueNumeric"),
                    xInvalidVarDisplayName),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);

                return false;
            }

            LMP.Forte.MSWord.Bates oBates = new LMP.Forte.MSWord.Bates();

            //GLOG : 5611 : ceh - fill direction
            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "FillDirection"]))
            {
                oBates.SideBySide = oPrefill[xBasePath + "FillDirection"] == "Vertically" ? true : false;
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "AllCaps"]))
            {
                oBates.AllCaps = bool.Parse(oPrefill[xBasePath + "AllCaps"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "Bold"]))
            {
                oBates.Bold = bool.Parse(oPrefill[xBasePath + "Bold"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "ConfidentialPhrases"]))
            {
                oBates.ConfidentialPhrases = oPrefill[xBasePath + "ConfidentialPhrases"];
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "NumberOfDigits"]))
            {
                oBates.Digits = short.Parse(oPrefill[xBasePath + "NumberOfDigits"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "FontName"]))
            {
                oBates.FontName = oPrefill[xBasePath + "FontName"];
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "FontSize"]))
            {
                oBates.FontSize = short.Parse(oPrefill[xBasePath + "FontSize"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "HorizontalLabelAlignment"]))
            {
                oBates.HorizontalAlignment = (Word.WdParagraphAlignment)(int.Parse(oPrefill[xBasePath + "HorizontalLabelAlignment"]));
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "NumberOfLabels"]))
            {
                oBates.NumLabels = Int32.Parse(oPrefill[xBasePath + "NumberOfLabels"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "ConfidentialPhrasePosition"]))
            {
                oBates.PositionPhrasesAbove = oPrefill[xBasePath + "ConfidentialPhrasePosition"] == "Above" ? true : false;
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "Prefix"]))
            {
                oBates.Prefix = oPrefill[xBasePath + "Prefix"];
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "SmallCaps"]))
            {
                oBates.SmallCaps = bool.Parse(oPrefill[xBasePath + "SmallCaps"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingColumn"]))
            {
                oBates.StartColumn = short.Parse(oPrefill[xBasePath + "StartingColumn"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingRow"]))
            {
                oBates.StartRow = short.Parse(oPrefill[xBasePath + "StartingRow"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingNumber"]))
            {
                oBates.StartNumber = int.Parse(oPrefill[xBasePath + "StartingNumber"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "Suffix"]))
            {
                oBates.Suffix = oPrefill[xBasePath + "Suffix"];
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "LabelSheetTopMargin"]))
            {
                //GLOG 6966: Make sure value is interpreted using "." as Decimal separator
                oBates.TopMargin = double.Parse(oPrefill[xBasePath + "LabelSheetTopMargin"], LMP.Culture.USEnglishCulture);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "Underline"]))
            {
                oBates.Underline = bool.Parse(oPrefill[xBasePath + "Underline"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "VerticalLabelAlignment"]))
            {
                oBates.VerticalAlignment = (Word.WdCellVerticalAlignment)int.Parse(oPrefill[xBasePath + "VerticalLabelAlignment"]);
            }

            oBates.WriteLabels(false);

            return true;
        }

        protected override void InitializeValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride, bool bChildStructureAvailable)
        {
            base.InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, bChildStructureAvailable);
        }
    }
}
