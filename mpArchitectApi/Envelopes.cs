using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Envelopes : AdminSegment, LMP.Architect.Base.IStaticDistributedSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.Envelopes, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        //public override bool PrepareForFinish()
        //{
        //    //get recipients value here, as variables will be deleted
        //    string xValue = "";
        //    Variable oRecips = null;
			
        //    //GLOG : 8104 : ceh - Recipients variable may not exist
        //    try
        //    {
        //        oRecips = this.Variables.ItemFromName("Recipients");
        //    }
        //    catch { }

        //    //exit if no Recipients variable exists
        //    if (oRecips == null)
        //        return base.PrepareForFinish();
				
        //    //GLOG 7901: Get value from Detail control to ensure Details in Saved Data
        //    //that are not defined for this variable are not included in XML

        //    if (oRecips.ControlType == mpControlTypes.DetailGrid || oRecips.ControlType == mpControlTypes.DetailList)
        //    {
        //        //Use only XML Details defined for this variable
        //        Controls.IControl oCtl;
        //        if (oRecips.AssociatedControl != null)
        //        {
        //            oCtl = oRecips.AssociatedControl;
        //        }
        //        else
        //            oCtl = oRecips.CreateAssociatedControl(null);

        //        oCtl.Value = oRecips.Value;
        //        xValue = oCtl.Value;
        //        oCtl = null;
        //    }
        //    else
        //    {
        //        xValue = oRecips.Value;
        //    }
        //    //GLOG 7879 : get USPS value
        //    bool bUSPS = false;

        //    try
        //    {
        //        bUSPS = (this.Variables.ItemFromName("USPSStandards").Value.ToLower() == "true");
        //    }
        //    catch { }

        //    string[] aSubVariables = null;

        //    //GLOG : 8104 : ceh - get array of subvar names
        //    if (oRecips.HasDistributedSegmentVariableActions())
        //    {
        //        //get names of subvariables from RunVariableActions parameter
        //        for (int i = 0; i < oRecips.VariableActions.Count; i++)
        //        {
        //            VariableAction oAction = oRecips.VariableActions[i];
        //            if (oAction.Type == VariableActions.Types.RunVariableActions)
        //            {
        //                string[] aRVParams = oAction.Parameters.Split(
        //                    LMP.StringArray.mpEndOfSubField);
        //                aSubVariables = aRVParams[0].Split(',');
        //                break;
        //            }
        //        }
        //    }
        //    //GLOG 8126: If no Distributed variables specified, default to first variable with InsertDetail action
        //    if (aSubVariables == null)
        //    {
        //        Variable oSubVar = this.FirstInsertDetailVariable;
        //        if (oSubVar != null)
        //            aSubVariables = new string[] { this.FirstInsertDetailVariable.Name };
        //        else
        //            return base.PrepareForFinish();
        //    }

        //    string xTemplate = "";

        //    //GLOG : 8104 : ceh - get range for each associated var & bookmark appropriately
        //    for (int j = 0; j < aSubVariables.Length; j++)
        //    {
        //        Word.Range oRecipsLoc = null;
        //        string xSubVar = aSubVariables[j];

        //        Variable oVar = this.Variables.ItemFromName(xSubVar);

        //        for (int i = 0; i < oVar.VariableActions.Count; i++)
        //        {
        //            VariableAction oAction = oVar.VariableActions[i];
        //            if (oAction.Type == VariableActions.Types.InsertDetail)
        //            {
        //                string[] aParams = oAction.Parameters.Split('�');
        //                xTemplate += aParams[2].ToString() + "�";
        //                break;
        //            }
        //        }

        //        //any detail variable that is in a deleted state need to be reinstated - no

        //        //in code that populates (later) - does Word merge fields.


        //        //get associated range
        //        oRecipsLoc = oVar.FirstAssociatedWordRange;

        //        //above gives the range that's contained by the content control

        //        //GLOG 7830: If Variable tag is empty, insert bookmark at start of Paragraph,
        //        //so that it won't get deleted with bounding object
        //        if (oRecipsLoc.Text == null)
        //        {
        //            //needs to move before the content control that's bounding the variable

        //            //object oPara = Word.WdUnits.wdParagraph;
        //            //object oMissing = System.Reflection.Missing.Value;
        //            //oRecipsLoc.StartOf(ref oPara, ref oMissing);

        //            object oMissing = System.Reflection.Missing.Value;
        //            oRecipsLoc.StartOf(ref oMissing, ref oMissing);

        //            //GLOG 8226: Don't move range when using XML Tags
        //            if (LMP.Forte.MSWord.WordApp.ActiveDocument().SaveFormat != 0)
        //            {
        //                //GLOG : 8104 : ceh - backup one character which will move past the start of the CC
        //                object oUnit = Word.WdUnits.wdCharacter;
        //                object oCount = -1;
        //                oRecipsLoc.Move(ref oUnit, ref oCount);
        //            }
        //        }

        //        object oRng = oRecipsLoc;
        //        if (oRng != null)
        //        {
        //            oRecipsLoc.Bookmarks.Add("RecipientsLocation_" + (j + 1).ToString(), ref oRng);
        //        }
        //    }

        //    xTemplate = xTemplate.TrimEnd('�');
        //    //GLOG 8158: Conform all fieldcodes to single underscore format
        //    xTemplate = xTemplate.Replace("[SubVar__", "[SubVar_");
			
        //    //write recips to temp file - to be used in ExecutePostFinish
        //    File.WriteAllText(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt", bUSPS.ToString() + "|" + xTemplate + "|" + xValue); //GLOG 7901

        //    return base.PrepareForFinish();
        //}

        //public override bool ExecutePostFinish()
        //{
        //    //get bookmark of segment
        //    Word.Bookmark oSegmentBmk = this.PrimaryBookmark;

        //    //determine if the envelope is supposed to be in a new doc - 
        //    //if so we modify the call to merge below to improve performance
        //    int iStartRngIndex = oSegmentBmk.Range.Start;
        //    int iEndRngIndex = oSegmentBmk.Range.End;
        //    bool bInNewDoc = (iStartRngIndex == 0 && iEndRngIndex == this.ForteDocument.WordDocument.Content.End - 1);

        //    StringBuilder oSB = new StringBuilder();

        //    string xText = null;
        //    //convert xml to address -
        //    //replace xml end tags with char 11

        //    //GLOG : 8104 : ceh
        //    try
        //    {
        //        xText = File.ReadAllText(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt");
        //    }
        //    catch { }

        //    //exit if no text file exists
        //    if (xText == null)
        //        return base.ExecutePostFinish();

        //    //GLOG 7879 : Parse USPSFormat and Recipients
        //    int iPos1 = xText.IndexOf("|");
        //    int iPos2 = xText.IndexOf("|", iPos1 + 1);

        //    string[] xSubVars = null;
           
        //    //get array of SubVar template parameters
        //    xSubVars = xText.Substring(iPos1 + 1, iPos2 - (iPos1 + 1)).Split(LMP.StringArray.mpEndOfSubField);
        //    string xRecips = xText.Substring(iPos2 + 1);
        //    bool bUSPS = xText.Substring(0, iPos1).ToLower() == "true";

        //    bool bFound = false;

        //    //cycle through SubVar array, removing all but CI token string
        //    for (int k = 0; k <= xSubVars.GetUpperBound(0); k++)
        //    {
        //        iPos1 = 0;
        //        iPos2 = 0;
        //        do
        //        {
        //            bFound = false;
        //            iPos1 = xSubVars[k].IndexOf("[SubVar_", iPos2);
        //            if (iPos1 > -1)
        //            {
        //                iPos1 = iPos1 + 8;
        //                iPos2 = xSubVars[k].IndexOf("]", iPos1);
        //                oSB.AppendFormat("{0}�", xSubVars[k].Substring(iPos1, iPos2 - iPos1));
        //                bFound = true;
        //            }
        //        } while (bFound);
        //        xSubVars[k] = oSB.ToString().TrimEnd('�');
        //        oSB.Clear();
        //    }
        //    File.Delete(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt");
        //    string xRecipsMod = xRecips.Replace("\r\n", "\v");
        //    xRecipsMod = xRecipsMod.Replace("\r", "\v");

        //    //reset StringBuilder
        //    oSB.Clear();
        //    //cycle through Index attributes, pulling out each address xml string
        //    int i = 0;
        //    string[] xTokens = null;

        //    //iPos1 = 0; //GLOG 7824
        //    //iPos2 = 0; //GLOG 7824

        //    do
        //    {
        //        //GLOG 8126: Separate handling depending on whether InsertDetail Template parameter includes SubVar tokens
        //        i++;
        //        bFound = false;

        //        iPos1 = 0;
        //        iPos2 = 0;

        //        //cycle through SubVars
        //        for (int j = 0; j <= xSubVars.GetUpperBound(0); j++)
        //        {
        //            //GLOG 8126
        //            if (xSubVars[j] != "")
        //            {
        //                xTokens = xSubVars[j].Split('�');
        //                if (j > 0)
        //                    oSB.Append("\r\n");

        //                //cycle through tokens
        //                for (int l = 0; l <= xTokens.GetUpperBound(0); l++)
        //                {
        //                    iPos1 = xRecipsMod.IndexOf(xTokens[l].ToString() + " Index='" + i + "'", iPos2);

        //                    if (iPos1 == -1)
        //                    {
        //                        iPos1 = xRecipsMod.IndexOf(xTokens[l].ToString() + " Index=\"" + i + "\"", iPos2);
        //                    }

        //                    if (iPos1 > -1)
        //                    {
        //                        iPos1 = xRecipsMod.IndexOf(">", iPos1) + 1;
        //                        iPos2 = xRecipsMod.IndexOf("<", iPos1);
        //                        oSB.AppendFormat("{0}\v", xRecipsMod.Substring(iPos1, iPos2 - iPos1));
        //                        bFound = true;
        //                    }
        //                }
        //                //GLOG 8126: remove last separator for current SubVar
        //                if (oSB.ToString().EndsWith("\v"))
        //                    oSB.Remove(oSB.Length - 1, 1);
        //            }
        //            else
        //            {
        //                //GLOG 8126: If no Template specified, include all fields
        //                do
        //                {
        //                    iPos1 = xRecipsMod.IndexOf("Index='" + i + "'", iPos2);

        //                    if (iPos1 == -1)
        //                    {
        //                        iPos1 = xRecipsMod.IndexOf("Index=\"" + i + "\"", iPos2);
        //                    }

        //                    if (iPos1 > -1)
        //                    {
        //                        iPos1 = xRecipsMod.IndexOf(">", iPos1) + 1;
        //                        iPos2 = xRecipsMod.IndexOf("<", iPos1);
        //                        oSB.AppendFormat("{0}\v", xRecipsMod.Substring(iPos1, iPos2 - iPos1));
        //                        bFound = true;
        //                    }
        //                } while (iPos1 > -1);
        //                //GLOG 8126: remove last separator for current SubVar
        //                if (oSB.ToString().EndsWith("\v"))
        //                    oSB.Remove(oSB.Length - 1, 1);
        //            }
        //        }
        //        //GLOG 8126: Moved above
        //        ////remove last separator for current SubVar
        //        //if (oSB.Length != 0)
        //        //    oSB.Remove(oSB.Length - 1, 1);
        //        oSB.Append("\r\n");
        //    } while (bFound);

        //    //GLOG 8126: Trim extra blank lines from end
        //    ////GLOG : 7614 : CEH
        //    //if (oSB.Length != 0)
        //    //    oSB.Remove(oSB.Length - 1, 1);
        //    xRecips = oSB.ToString().TrimEnd(new char[] { '\r', '\n', '\v' });

        //    xRecips = LMP.String.RestoreXMLChars(xRecips);

        //    //trim trailing hard-return
        //    //if (!string.IsNullOrEmpty(xRecips))
        //        //xRecips = xRecips.Substring(0, xRecips.Length - 2);

        //    //GLOG : 7918 : ceh - Only delete bookmarks within the envelope mseg.
        //    //Alternatively, we could keep the 'if' block and add an argument to 
        //    //not include property tags in DeleteBookmarksAndDocVars.  This will
        //    //involve bookmarks starting with mpp & mpc prefixes, as well as their 
        //    //corresponding variables (mpo + id)

        //    //GLOG 7874:  Make sure no mSEG bookmarks remain
        //    //if (bInNewDoc)
        //    //{
        //    //    //JTS: If new document, clear out all variables and bookmarks
        //    //    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
        //    //    oConvert.DeleteBookmarksAndDocVars(this.ForteDocument.WordDocument);
        //    //}
        //    //else
        //    //{
        //        //JTS: If not new document, just delete bookmarks for Envelope
        //        foreach (Word.Bookmark oBmk in this.Bookmarks)
        //        {
        //            //GLOG 7916 (dm) - delete doc vars as well as booknmarks
        //            LMP.Forte.MSWord.WordDoc.DeleteAssociatedDocVars_Bmk(oBmk.Range, null, null, null);
        //            object oRef = oBmk;
        //            this.ForteDocument.WordDocument.Bookmarks[ref oRef].Delete();
        //        }
        //    //}

        //    //GLOG 7879 : apply USPS formatting if necessary
        //    if (bUSPS)
        //        xRecips = LMP.String.GetUSPSFormat(xRecips);

        //    int iSubVarsCount = xSubVars.GetUpperBound(0) + 1;

        //    //create range array using SubVar array
        //    object[] oRangeArray = new object[iSubVarsCount];

        //    //fill range array
        //    for (int j = 1; j <= iSubVarsCount; j++)
        //    {
        //        object oBmkName = "RecipientsLocation_" + j.ToString();

        //        Word.Range oBmkRng = null;

        //        if (this.ForteDocument.WordDocument.Bookmarks.Exists(oBmkName.ToString()))
        //        {
        //            oBmkRng = this.ForteDocument.WordDocument.Bookmarks.get_Item(ref oBmkName).Range;
        //        }

        //        if (oBmkRng == null)
        //        {
        //            object oRngIndex = 0;
        //            oBmkRng = this.ForteDocument.WordDocument.Range(ref oRngIndex, ref oRngIndex);
        //        }

        //        oRangeArray[j - 1] = oBmkRng;
        //    }

        //    LMP.Forte.MSWord.WordDoc.MergeEnvelopeRecipients(
        //        oRangeArray, xRecips, bInNewDoc);

        //    return base.ExecutePostFinish();
        //}
    }
}
