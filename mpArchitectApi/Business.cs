using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Agreement : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.Agreement, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class AgreementSignatures : CollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.AgreementSignature; }
        }
    }

    public class AgreementSignature : CollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.AgreementSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.AgreementSignature; }
        }
    }

    public class AgreementSignatureNonTable : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, false,
                    iIntendedUse, mpObjectTypes.AgreementSignatureNonTable, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class AgreementTitlePage : AdminSegment, ISingleInstanceSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, bTargeted,
                    iIntendedUse, mpObjectTypes.AgreementTitlePage, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        //public void DeleteExistingInstance(Word.Section oSection)
        //{
        //    //get existing segment of this type
        //    Segment oExistingTitlePage = GetExistingSegments(oSection)[0];

        //    //delete existing Trailer
        //    if (oExistingTitlePage != null)
        //    {
        //        try
        //        {
        //            this.ForteDocument.DeleteSegment(oExistingTitlePage);
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.SegmentException(
        //                LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
        //        }
        //    }
        //}
        public Segments GetExistingSegments(Word.Section oSection)
        {
            //get from location - notice that we ignore oSection -
            //that's because title page is document-specific
            return Segment.GetSegments(this.ForteDocument, 
                LMP.Data.mpObjectTypes.AgreementTitlePage, 1);
        }
    }
}
