using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Service : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.Service, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
    public class ServiceList : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.ServiceList, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
    public class ServiceListSeparatePage : AdminSegment //GLOG 6094
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.ServiceListSeparatePage, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
