using System;
using System.Text;
using System.Text.RegularExpressions;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;

namespace LMP.Architect.Api
{
    public delegate string FieldCodeExternalEvaluationRequiredHandler(string xExpression);

	/// <summary>
	/// contains methods for processing field codes
	/// created by Doug Miller - 10/14/04
	/// </summary>
	public class FieldCode
	{
        #region *********************constants*********************
        internal const char mpFieldCodeStartTag = '[';
        internal const char mpFieldCodeEndTag = ']';
        internal const string mpFieldCodeParamSep = "__";
        internal const string mpFieldCodeParamSepOLD = "_";
        #endregion
        #region *********************fields*********************
		private static LMP.Data.Person m_oPerson;
		private static LMP.Data.AddressFormat m_oAddressFormat;
		private static LMP.Data.KeySet m_oLeadAuthorPrefs;
		private static LMP.Data.KeySet m_oTypePrefs;
		private static LMP.Data.KeySet m_oObjectPrefs;
		private static LMP.Data.KeySet m_oAppPrefs;
		private static LMP.Data.Court m_oCourt;
		private static LMP.Data.Office m_oOffice;
		private static LMP.Data.Courier m_oCourier;
        #endregion
        #region *********************constructors*********************
        static FieldCode()
		{
			SubscribeToEvents();
		}
		#endregion
        #region *********************public methods*********************
        //GLOG item #5982 - dcf
        /// <summary>
        /// returns the specified expression with all instances
        /// of the MyValue field code or keyword evaluated -
        /// </summary>
        /// <param name="xExpression"></param>
        /// <param name="xMyValue"></param>
        /// <returns></returns>
        public static string EvaluateMyValue(string xExpression, string xMyValue)
        {
            xExpression = Regex.Replace(xExpression, "\\[MyValue\\]", 
                xMyValue, RegexOptions.IgnoreCase);

            //replace MyValue in parameters string with value -
            //this may be specified inside of a field code, e.g. [OfficeMyValue_City]
            xExpression = xExpression.Replace("MyValue", xMyValue);

            return xExpression;
        }
        /// <summary>
        /// returns true iff the specified field code
        /// requires a segment to evaluate
        /// </summary>
        /// <param name="xFieldCodeName"></param>
        /// <returns></returns>
        public static bool RequiresSegment(string xFieldCodeName)
        {
            string xName = xFieldCodeName.ToUpper();
            //return !Expression.mpNonSegmentFieldCodes.Contains("|" + xFieldCodeName.ToUpper() + "|");
            return !(xName.StartsWith("USERSETTING") || xName.StartsWith("FIRMSETTING") ||
                xName.StartsWith("OFFICE") || xName.StartsWith("COURIER") ||
                xName.StartsWith("LEVEL") || xName.StartsWith("CHAR") ||
                xName.StartsWith("UCHAR") || xName.StartsWith("TODAY") ||
                xName.StartsWith("ORDINALSUFFIX") || xName.StartsWith("CM") ||
                xName.StartsWith("USEROFFICEADDRESS") || xName.StartsWith("OFFICEADDRESS") ||
                xName.StartsWith("PERSONOFFICE") || xName.StartsWith("PERSONOFFICEADDRESS") ||
                xName.StartsWith("SUBSTRING") || xName.StartsWith("EXTERNALDATA") ||
                xName.StartsWith("LISTLOOKUP") || xName.StartsWith("EMPTY") ||
                xName.StartsWith("XMLEXTRACT") || xName.StartsWith("REPLACE") ||
                xName.StartsWith("CONVERTTOXML") || xName.StartsWith("SEGMENTNAME") ||
                xName.StartsWith("UPPERCASE") || xName.StartsWith("LOWERCASE") ||
                xName.StartsWith("COUNT") || xName.StartsWith("NULL") ||
                xName.StartsWith("TRUNCATE") || xName.StartsWith("STARTSWITH") ||
                xName.StartsWith("ENDSWITH") || xName.StartsWith("COUNTENTITIES") ||
                xName.StartsWith("NUMBERTEXT") || xName.StartsWith("CONVERT") ||
                xName.StartsWith("USPSFORMAT") || xName.StartsWith("EXTRACTENTITIES") ||
                xName.StartsWith("CHOOSE") || xName.StartsWith("COUNTRYNAME") ||
                xName.StartsWith("STATENAME") || xName.StartsWith("COUNTYNAME") ||
                xName.StartsWith("LOCATIONID") || xName.StartsWith("ADD") ||
                xName.StartsWith("SUBTRACT") || xName.StartsWith("MULTIPLY") ||
                xName.StartsWith("DIVIDE") || xName.StartsWith("MODULUS") || xName.StartsWith("ISNUMERIC") || //GLOG : 8375 : JSW
                xName.StartsWith("FORMATDATETIME") || xName.StartsWith("SEGMENTID") ||
                xName.StartsWith("DATEDIFF") || xName.StartsWith("DATEADD") || xName.StartsWith("DATESUBTRACT") || //GLOG : 8374 : JSW
                xName.StartsWith("XMLFORMAT")); //GLOG 15838

            //return !Expression.mpNonSegmentFieldCodes.Contains("|" + xFieldCodeName.ToUpper());
        }

        /// <summary>
        /// returns the value of the specified field code in the
        /// context of the specified mpObject
        /// </summary>
        /// <param name="xFieldCode"></param>
        /// <param name="oSegment">the segment to use for evaluation</param>
        /// <param name="oMPDocument">the macpac document to use for evaluation</param>
        /// <returns></returns>
        public static string GetValue(string xFieldCode, Segment oSegment, ForteDocument oMPDocument)
        {
            return GetValue(xFieldCode, oSegment, oMPDocument, false, null);
        }
        public static string GetValue(string xFieldCode, Segment oSegment, ForteDocument oMPDocument, bool bAllowOldFormat){
            return GetValue(xFieldCode, oSegment, oMPDocument, bAllowOldFormat, null);
        }
        public static string GetValue(string xFieldCode, Segment oSegment, ForteDocument oMPDocument, bool bAllowOldFormat,
            FieldCodeExternalEvaluationRequiredHandler oExternalEvalRequiredDelegate)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xFieldCode", xFieldCode);

            //evaluate the [EMPTY] and [NULL] field codes here for better
            //performance - we don't have to execute all the code below
            if (xFieldCode.ToUpper() == "[EMPTY]")
                return "";
            else if (xFieldCode.ToUpper() == "[NULL]")
                return "null";

            string xName = "";
            string xParam = "";
            string[] axParams = null;
            bool bIsUnsupported = false;
            Segment oTarget = null;

            //determine whether this is design mode
            bool bIsDesign = oMPDocument != null ? (oMPDocument.Mode == ForteDocument.Modes.Design) : false;

            //get the document associated with the specified object-
            Word.Document oDoc = oMPDocument != null ? oMPDocument.WordDocument : null;

            //remove start and end tags if necessary
            string xFieldCodeMod = xFieldCode.TrimStart(mpFieldCodeStartTag);
            if (xFieldCodeMod.Substring(xFieldCodeMod.Length - 1) == mpFieldCodeEndTag.ToString())
                xFieldCodeMod = xFieldCodeMod.Substring(0, xFieldCodeMod.Length - 1);

            string xParamSep = mpFieldCodeParamSep;
            //Check if field code uses old format
            if (bAllowOldFormat && !Expression.FieldCodeIsSupported(xFieldCodeMod.ToUpper()) 
                && Expression.FieldCodeIsSupportedOLD(xFieldCodeMod.ToUpper()))
                xParamSep = mpFieldCodeParamSepOLD;

            //spaces are represented by %20
            xFieldCodeMod = xFieldCodeMod.Replace("%20", " ");

            //find parameter start
            int iPos = xFieldCodeMod.IndexOf(xParamSep);

            if (iPos == -1)
            {
                //handle field codes with no "_"
                if (xFieldCodeMod.ToUpper().EndsWith("AUTHORSCOUNT") ||
                    xFieldCodeMod.ToUpper().EndsWith("SEGMENTINDEX") ||
                    xFieldCodeMod.ToUpper().EndsWith("SEGMENTINSERTIONCOMPLETE") ||
                    xFieldCodeMod.ToUpper().EndsWith("SEGMENTLANGUAGEID") ||
                    xFieldCodeMod.ToUpper().EndsWith("SEGMENTLANGUAGENAME") ||
                    xFieldCodeMod.ToUpper().EndsWith("TOPSEGMENTNAME") ||
                    xFieldCodeMod.ToUpper().EndsWith("TOPSEGMENTID") ||
                    xFieldCodeMod.ToUpper() == "EMPTY")
                {
                    xName = xFieldCodeMod;
                    axParams = new string[1];
                }
                else
                {
                    //GLOG item #4216 - dcf -
                    //field code is not supported -
                    //return as text
                    return xFieldCode;
                    throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                        .GetLangString("Error_InvalidFieldCode") + xFieldCode);
                }
            }
            else
            {
                xName = xFieldCodeMod.Substring(0, iPos);
                xParam = xFieldCodeMod.Substring(iPos + xParamSep.Length);
                ////field codes NO LONGER support alternating value|separator protocol -
                axParams = new string[1] { xParam };
            }

            if (oSegment != null)
            {
                //get target object - field code may reference a parent, child, or sibling
                oTarget = oSegment.GetReferenceTarget(ref xName);
            }

            if (oTarget == null && RequiresSegment(xName))
                //target segment doesn't exist - return empty value
                return "";

            string xNameUpper = xName.ToUpper();

            //control value is a valid field code, but is only handled in control actions
            if (xNameUpper == "CONTROLVALUE")
            {
                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                    .GetLangString("Error_FieldCodeOutOfContext") + xFieldCode);
            }

            //store current xml event handling status
            ForteDocument.WordXMLEvents bIgnoreWordXMLEvents =
                oMPDocument != null ? ForteDocument.IgnoreWordXMLEvents : ForteDocument.WordXMLEvents.None;

            StringBuilder oSB = new StringBuilder();

            //get values for each item in collection, separated by "\r\n;
            //parameters alternate btwn values and separators
            try
            {
                for (int i = 0; i < axParams.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        //append value
                        if (xNameUpper == "AUTHORSCOUNT")
                        {
                            if (!bIsDesign)
                                oSB.Append(oTarget.Authors.Count.ToString());
                            else
                                oSB.Append("0");
                        }
                        else if (xNameUpper == "LEADAUTHOR")
                        {
                            //get value of lead author
                            if (!bIsDesign)
                                oSB.Append(GetLeadAuthorValue(oTarget.Authors, axParams[i]));
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "PREVIOUSLEADAUTHOR")
                        {
                            //get value of previously-set lead author
                            if (!bIsDesign)
                            {
                                if (oTarget.PreviousAuthors != null && oTarget.PreviousAuthors.Count > 0)
                                    oSB.Append(GetLeadAuthorValue(oTarget.PreviousAuthors, axParams[i]));
                                else
                                    oSB.Append("");
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        // GLOG : 3441 : JAB
                        // Handle [PreviousAuthors]
                        else if (xNameUpper == "PREVIOUSAUTHORS")
                        {
                            if (!bIsDesign)
                            {
                                if (axParams[i].Contains("[" + xParamSep))
                                {
                                    string xFullValue = "";
                                    //field code parameter is an expression -
                                    //evaluate for each author and append to output string
                                    for (short j = 1; j <= oTarget.PreviousAuthors.Count; j++)
                                    {
                                        //modify expression so that it can be evaluated for each author
                                        string xExpMod = axParams[i].Replace("[" + xParamSep, "[Author" + j.ToString() + xParamSep);
                                        string xEvalValue = Expression.Evaluate(xExpMod, oTarget, oMPDocument);
                                        //Remove any delimiters from last item
                                        if (j == oTarget.Authors.Count)
                                            xEvalValue = LMP.String.TrimDelimiters(xEvalValue);
                                        xFullValue = xFullValue + xEvalValue;
                                    }
                                    //Trim any delimiters at end of string
                                    xFullValue = LMP.String.TrimDelimiters(xFullValue);
                                    oSB.Append(xFullValue);
                                }
                                else
                                {
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                        //get value for each author in collection
                                        oSB.Append(GetAuthorValue(oTarget.Authors, axParams[i], j) + "\r\n");

                                    if (oSB.Length > 1)
                                        oSB.Remove(oSB.Length - 2, 2);
                                }
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "LEADAUTHOROFFICEADDRESS")
                        {
                            if (!bIsDesign)
                            {
                                int iFormat = System.Convert.ToInt32(axParams[i]);
                                oSB.AppendFormat(GetLeadAuthorOfficeAddress(oTarget.Authors, iFormat));
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "LEADAUTHOROFFICE")
                        {
                            if (!bIsDesign)
                                oSB.Append(GetLeadAuthorOfficeValue(oTarget.Authors, axParams[i]));
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "LEADAUTHORPREFERENCE")
                        {
                            string xPref = "null";
                            if (oTarget is AdminSegment)
                            {
                                if (!bIsDesign)
                                {
                                    //get preference value of lead author
                                    if(oTarget.ID2 != 0)
                                    {
                                        //target is content designer segment -
                                        //content designer segments are stored as
                                        //user segments, hence they have two IDs-
                                        //get lead author pref using both IDs
                                        xPref = GetLeadAuthorPreference(oTarget.Authors,
                                            oTarget.ID, axParams[i]);
                                    }
                                    else
                                    {
                                        xPref = GetLeadAuthorPreference(oTarget.Authors,
                                            oTarget.ID1.ToString(), axParams[i]);
                                    }

                                    //GLOG : 6957 : CEH - if no lead author
									//set to firm-wide preference value
                                    if (xPref == "null")
                                    {
                                        //get firm-wide preference value
                                        xPref = GetFirmWideAuthorPreference(
                                            oTarget, axParams[i]);
                                    }
                                }
                                else
                                {
                                    //get firm-wide preference value
                                    xPref = GetFirmWideAuthorPreference(
                                        oTarget, axParams[i]);
                                }
                            }
                            oSB.Append(xPref);
                        }
                        else if (xNameUpper == "LEADAUTHORPARENTPREFERENCE")
                        {
                            string xPref = "null";

                            //get top level parent object
                            Segment oParent = oTarget.Parent;

                            if (oParent != null)
                                while (oParent.Parent != null)
                                    oParent = oParent.Parent;

                            if (oParent != null)
                            {
                                //get preference value of lead author
                                //for top level parent object
                                if (oParent is AdminSegment)
                                {
                                    if (!bIsDesign)
                                        xPref = GetLeadAuthorPreference(oTarget.Authors,
                                            oParent.ID1.ToString(), axParams[i]);
                                    else
                                        //get firm-wide preference value
                                        xPref = GetFirmWideAuthorPreference(
                                            oParent, axParams[i]);
                                }
                            }

                            if (xPref == "null")
                            {
                                //no parent pref - get preference value of
                                //lead author for target object
                                if (oTarget is AdminSegment)
                                {
                                    if (!bIsDesign)
                                        xPref = GetLeadAuthorPreference(oTarget.Authors,
                                            oTarget.ID1.ToString(), axParams[i]);
                                    else
                                        //get firm-wide preference value
                                        xPref = GetFirmWideAuthorPreference(
                                            oTarget, axParams[i]);
                                }
                            }

                            oSB.Append(xPref);
                        }
                        else if (xNameUpper == "ADDITIONALAUTHORS")
                        {
                            if (!bIsDesign)
                            {
                                if (axParams[i].Contains("[" + xParamSep))
                                {
                                    //expression contains field codes that
                                    //reference the parent code -
                                    //modify the expression to create 
                                    //something that we can evaluate
                                    string xFullValue = "";
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        if (oTarget.Authors.GetLeadAuthor().ID != oTarget.Authors.ItemFromIndex(j).ID)
                                        {
                                            //modify expression so that it can be evaluated for each author
                                            string xExpMod = axParams[i].Replace("[" + xParamSep, "[Author" + j.ToString() + xParamSep);
                                            string xEvalValue = Expression.Evaluate(xExpMod, oTarget, oMPDocument);
                                            xFullValue = xFullValue + xEvalValue;
                                        }
                                    }
                                    //Trim any delimiters at end of string
                                    xFullValue = LMP.String.TrimDelimiters(xFullValue);
                                    oSB.Append(xFullValue);
                                }
                                else
                                {
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        if (oTarget.Authors.GetLeadAuthor().ID != oTarget.Authors.ItemFromIndex(j).ID)
                                            //get value for each author in collection, except lead author
                                            oSB.Append(GetAuthorValue(oTarget.Authors, axParams[i], j) + "\r\n");
                                    }

                                    if (oSB.Length > 1)
                                        oSB.Remove(oSB.Length - 2, 2);
                                }
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "ADDITIONALAUTHORSOFFICEADDRESS")
                        {
                            if (!bIsDesign)
                            {
                                int iFormat = System.Convert.ToInt32(axParams[i]);

                                for (short j = 1; j <= oTarget.Authors.Count; j++)
                                {
                                    //get value for each author in collection except lead author
                                    if (oTarget.Authors.GetLeadAuthor().ID != oTarget.Authors.ItemFromIndex(j).ID)
                                        oSB.AppendFormat(GetAuthorOfficeAddress(oTarget.Authors, iFormat, j) + "\r\n");
                                }

                                if (oSB.Length > 1) //GLOG 7561 (dm)
                                    oSB.Remove(oSB.Length - 2, 2);
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "ADDITIONALAUTHORSOFFICE")
                        {
                            if (!bIsDesign)
                            {
                                if (axParams[i].Contains("[" + xParamSep))
                                {
                                    string xFullValue = "";
                                    //field code parameter is an expression -
                                    //evaluate for each author and append to output string
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        if (oTarget.Authors.GetLeadAuthor().ID != oTarget.Authors.ItemFromIndex(j).ID)
                                        {
                                            //modify expression so that it can be evaluated for each author
                                            string xExpMod = axParams[i].Replace("[" + xParamSep, "[AuthorOffice" + j.ToString() + xParamSep);
                                            string xEvalValue = Expression.Evaluate(xExpMod, oTarget, oMPDocument);
                                            xFullValue = xFullValue + xEvalValue;
                                        }
                                    }
                                    //Trim any delimiters at end of string
                                    xFullValue = LMP.String.TrimDelimiters(xFullValue);
                                    oSB.Append(xFullValue);
                                }
                                else
                                {
                                    //get value for each author in collection
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        //get value for each author in collection except lead author
                                        if (oTarget.Authors.GetLeadAuthor().ID != oTarget.Authors.ItemFromIndex(j).ID)
                                            oSB.Append(GetAuthorOfficeValue(oTarget.Authors, axParams[i], j) + "\r\n");
                                    }

                                    if (oSB.Length > 1) //GLOG 7561 (dm)
                                        oSB.Remove(oSB.Length - 2, 2);
                                }
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "AUTHORS")
                        {
                            if (!bIsDesign)
                            {
                                if (axParams[i].Contains("[" + xParamSep))
                                {
                                    string xFullValue = "";
                                    //field code parameter is an expression -
                                    //evaluate for each author and append to output string
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        //modify expression so that it can be evaluated for each author
                                        string xExpMod = axParams[i].Replace(
                                            "[" + xParamSep, "[Author" + j.ToString() + xParamSep);
                                        string xEvalValue = Expression.Evaluate(xExpMod, oTarget, oMPDocument);
                                        //Remove any delimiters from last item
                                        if (j == oTarget.Authors.Count)
                                            xEvalValue = LMP.String.TrimDelimiters(xEvalValue);
                                        xFullValue = xFullValue + xEvalValue;
                                    }
                                    //Trim any delimiters at end of string
                                    xFullValue = LMP.String.TrimDelimiters(xFullValue);
                                    oSB.Append(xFullValue);
                                }
                                else
                                {
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                        //get value for each author in collection
                                        oSB.Append(GetAuthorValue(oTarget.Authors, axParams[i], j) + "\r\n");

                                    if (oSB.Length > 1)
                                        oSB.Remove(oSB.Length - 2, 2);
                                }
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "AUTHORSOFFICEADDRESS")
                        {
                            if (!bIsDesign)
                            {
                                int iFormat = System.Convert.ToInt32(axParams[i]);

                                for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    //get value for each author in collection
                                    oSB.AppendFormat(GetAuthorOfficeAddress(oTarget.Authors, iFormat, j) + "\r\n");

                                oSB.Remove(oSB.Length - 2, 2);
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "AUTHORSOFFICE")
                        {
                            if (!bIsDesign)
                            {
                                if (axParams[i].Contains("[" + xParamSep))
                                {
                                    string xFullValue = "";
                                    //field code parameter is an expression -
                                    //evaluate for each author and append to output string
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                    {
                                        //modify expression so that it can be evaluated for each author
                                        string xExpMod = axParams[i].Replace("[" + xParamSep, "[AuthorOffice" + j.ToString() + xParamSep);
                                        string xEvalValue = Expression.Evaluate(xExpMod, oTarget, oMPDocument);
                                        //Remove any delimiters from last item
                                        if (j == oTarget.Authors.Count)
                                            xEvalValue = LMP.String.TrimDelimiters(xEvalValue);
                                        xFullValue = xFullValue + xEvalValue;
                                    }
                                    //Trim any delimiters at end of string
                                    xFullValue = LMP.String.TrimDelimiters(xFullValue);
                                    oSB.Append(xFullValue);
                                }
                                else
                                {
                                    //get value for each author in collection
                                    for (short j = 1; j <= oTarget.Authors.Count; j++)
                                        oSB.Append(GetAuthorOfficeValue(oTarget.Authors, axParams[i], j) + "\r\n");

                                    oSB.Remove(oSB.Length - 2, 2);
                                }
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper == "USER")
                        {
                            //get property of user
                            if (!bIsDesign)
                                //GLOG 4838: Use DefaultAuthor
                                oSB.Append(LMP.Data.Application.User.DefaultAuthor
                                    .GetPropertyValue(axParams[i]));
                            else
                                oSB.Append("UserData");
                        }
                        else if (xNameUpper == "USEROFFICE")
                        {
                            //get office property of user
                            if (!bIsDesign)
                                //GLOG 4838: Use DefaultAuthor
                                oSB.Append(LMP.Data.Application.User.DefaultAuthor
                                    .GetOffice().GetProperty(axParams[i]));
                            else
                                oSB.Append("UserData");
                        }
                        else if (xNameUpper == "USEROFFICEADDRESS")
                        {
                            //get office address of user
                            if (!bIsDesign)
                            {
                                int iFormatID = System.Convert.ToInt32(axParams[i]);
                                string xFormat = GetAddressTokenString(iFormatID);
                                string xDelimReplacement = GetAddressDelimiterReplacement(iFormatID);
                                //GLOG 4838: Use DefaultAuthor
                                oSB.Append(LMP.Data.Application.User.DefaultAuthor
                                    .GetOffice().GetAddress().ToString(xFormat, xDelimReplacement));
                            }
                            else
                                oSB.Append("UserData");
                        }
                        else if (xNameUpper == "TYPEPREFERENCE")
                        {
                            //get user type preference
                            oSB.Append(GetTypePreferenceValue(oTarget, axParams[i]));
                        }
                        else if (xNameUpper == "TAGVALUE")
                        {
                            // The TAGVALUE field code has 2 forms:
                            //      1: TAGVALUE - this form is not evaluated in this method.
                            //      2: TAGVALUE_X where X is the name of another variable in this segment.
                            //         We will return the value of the first tag associated with this variable named X.

                            if (axParams.Length > 0)
                            {
                                // A source variable has been specified. Get the source variables name in
                                // order to get its tag value.
                                string xSourceVariableName = axParams[i];

                                Variable oValueSourceVariable = oTarget.Variables.ItemFromName(xSourceVariableName);

                                if (oValueSourceVariable != null)
                                {
                                    string xPref = "";
                                    if (oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                    {
                                        Word.XMLNode oWordNode = oValueSourceVariable.AssociatedWordTags[0];
                                        xPref = oWordNode.Text;
                                    }
                                    else
                                    {
                                        Word.ContentControl oCC = oValueSourceVariable.AssociatedContentControls[0];
                                        xPref = oCC.Range.Text;
                                    }
                                    oSB.Append(xPref);
                                }
                            }
                        }
                        else if (xNameUpper == "OBJECTPREFERENCE")
                        {
                            string xPref = "null";
                            if (oTarget is AdminSegment)
                            {
                                //get user object preference
                                xPref = GetObjectPreferenceValue(oTarget, axParams[i]);
                            }
                            oSB.Append(xPref);
                        }
                        else if (xNameUpper == "APPLICATIONPREFERENCE")
                        {
                            //get user application preference
                            oSB.Append(GetAppPreferenceValue(axParams[i], oTarget.Culture));
                        }
                        else if (xNameUpper == "USERSETTING")
                        {
                            //get user application setting
                            oSB.Append(LMP.Data.Application.User
                                .GetUserAppKeys().GetValue(axParams[i]));
                        }
                        else if (xNameUpper == "FIRMSETTING")
                        {
                            //get firm application setting
                            oSB.Append(LMP.Data.Application.User
                                .GetFirmAppKeys().GetValue(axParams[i]));
                        }
                        else if (xNameUpper == "COURT")
                        {
                            if (oTarget is AdminSegment)
                            {
                                string xCourtVal = "";
                                try
                                {
                                    //get court value
                                    xCourtVal = GetCourtValue((AdminSegment)oTarget, axParams[i]);
                                }
                                catch { }
                                oSB.Append(xCourtVal);
                            }
                            else
                                //the [Court] field code can be used
                                //only with admin segments
                                throw new LMP.Exceptions.FieldCodeException(
                                    LMP.Resources.GetLangString("Error_UserSegmentCantUseFieldCode") + "[Court]");
                        }
						// GLOG : 3438 : CEH
                        else if (xNameUpper == "COURTADDRESS")
                        {
                            if (!bIsDesign)
                            {
                                int iFormatID = System.Convert.ToInt32(axParams[i]);
                                string xFormat = GetAddressTokenString(iFormatID);
                                string xDelimReplacement = GetAddressDelimiterReplacement(iFormatID);
                                string xCourtAddress = "";

				                //retrieve court
				                LMP.Data.Courts oCourts = new Courts();
                                try
                                {
                                    m_oCourt = (Court)oCourts.ItemFromLevels(oSegment.L0,
                                        oSegment.L1, oSegment.L2, oSegment.L3,
                                        oSegment.L4);
                                    xCourtAddress = m_oCourt.GetAddress().ToString(xFormat, xDelimReplacement);
                                }
                                catch { }
                                oSB.Append(xCourtAddress);
                            }
                            else
                                oSB.Append("CourtAddressData");
                        }
                        else if (xNameUpper == "DOCUMENTVARIABLE")
                        {
                            //get Word document variable
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetDocumentVariableValue(oDoc, axParams[i], false));
                        }
                        else if (xNameUpper == "BOOKMARK")
                        {
                            //get Word bookmark text
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetBookmarkText(oDoc, axParams[i], false));
                        }
                        else if (xNameUpper == "VARIABLE")
                        {
                            //if (oSegment.IsFinished && (oExternalEvalRequiredDelegate != null))
                            //{
                            //    oExternalEvalRequiredDelegate(oSegment, new EventArgs());
                            //}

                            //get variable value
                            Variable oVar = oTarget.GetVariable(axParams[i]);
                            if (oVar != null && oVar.Value != null)
                            {
                                ////if segment is deactivated, review data (5/20/11 dm)
                                //if (!oTarget.Activated)
                                //    oTarget.ReviewData(axParams[i]);

                                oSB.Append(oVar.Value);
                            }
                            else if (Snapshot.Exists(oTarget))
                            {
                                //get variable value from snapshot,
                                //as the document is in a Finished state
                                oSB.Append(oTarget.Snapshot.GetValue(axParams[i]));
                            }
                        }
                        else if (xNameUpper == "CHAR")
                        {
                            oSB.Append(GetMacPacChar(axParams[i]));
                        }
                        else if (xNameUpper == "UCHAR")
                        {
                            //no processing is required for unicode character - the
                            //parameter needs to include the escape character(\u) 
                            //because there doesn't seem to be any way to append it in code
                            oSB.Append(axParams[i]);
                        }
                        else if (xNameUpper == "DOCUMENT")
                        {
                            //get property of Word document object
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetDocumentValue(oDoc, axParams[i]));
                        }
                        else if (xNameUpper == "WORDAPPLICATION")
                        {
                            //get property of Word application object
                            oSB.Append(LMP.Forte.MSWord.WordApp.GetWordApplicationValue(axParams[i]));
                        }
                        else if (xNameUpper == "WORDBUILTINDOCPROPERTY")
                        {
                            //get built-in Word document property
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetWordDocPropertyValue(oDoc, axParams[i], false));
                        }
                        else if (xNameUpper == "WORDCUSTOMDOCPROPERTY")
                        {
                            //get custom Word document property
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetWordCustomDocPropertyValue(oDoc, axParams[i], false));
                        }
                        else if (xNameUpper == "FUNCTION")
                        {
                            //get value from COM function
                            oSB.Append(GetFunctionValue(axParams[i]));
                        }
                        else if (xNameUpper.IndexOf("AUTHOROFFICEADDRESS") == 0)
                        {
                            if (!bIsDesign)
                            {
                                //get office value for author with specified index-
                                string xIndex = xNameUpper.Substring(19);

                                //get the author whose detail we should retrieve -
                                //parameter represents the ID of the address format to be used
                                short shIndex = short.Parse(xIndex);
                                int iFormat = System.Convert.ToInt32(axParams[i]);

                                //GLOG 7561 (dm) - avoid error when specified author doesn't exist
                                if (shIndex <= oTarget.Authors.Count)
                                    oSB.Append(GetAuthorOfficeAddress(oTarget.Authors, iFormat, shIndex));
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper.IndexOf("AUTHOROFFICE") == 0)
                        {
                            if (!bIsDesign)
                            {
                                //get office value for author with specified index-
                                string xIndex = xNameUpper.Substring(12);
                                //get the author whose detail we should retrieve
                                short shIndex = short.Parse(xIndex);

                                //GLOG 7561 (dm) - avoid error when specified author doesn't exist
                                if (shIndex <= oTarget.Authors.Count)
                                    oSB.Append(GetAuthorOfficeValue(oTarget.Authors, axParams[i], shIndex));
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper.IndexOf("AUTHORPREFERENCE") == 0)
                        {
                            string xPref = "null";
                            if (oTarget is AdminSegment)
                            {
                                if (!bIsDesign)
                                {
                                    //get author preference value for author with specified index-
                                    //get the index of the author whose detail we should retrieve
                                    string xIndex = xNameUpper.Substring(16);

                                    //get the author whose detail we should retrieve
                                    short shIndex = short.Parse(xIndex);

                                    xPref = GetAuthorPreferenceValue(oTarget.Authors,
                                        oTarget.ID1.ToString(), axParams[i], shIndex);
                                }
                                else
                                    //get firm-wide preference value
                                    xPref = GetFirmWideAuthorPreference(
                                        oTarget, axParams[i]);
                            }
                            oSB.Append(xPref);
                        }
                        else if (xNameUpper.IndexOf("AUTHORPARENTPREFERENCE") == 0)
                        {
                            string xPref = "null";

                            //get the index of the author whose detail we should retrieve
                            string xIndex = xNameUpper.Substring(22);

                            //get the author whose detail we should retrieve
                            short shIndex = short.Parse(xIndex);

                            //get top level parent object
                            Segment oParent = oTarget.Parent;

                            if (oParent != null)
                            {
                                while (oParent.Parent != null)
                                    oParent = oParent.Parent;
                            }

                            if (oParent != null)
                            {
                                if (oParent is AdminSegment)
                                {
                                    if (!bIsDesign)
                                        //get preference value of specified author 
                                        //for top level parent object
                                        xPref = GetAuthorPreferenceValue(oTarget.Authors,
                                             oParent.ID1.ToString(), axParams[i], shIndex);
                                    else
                                        //get firm-wide preference value
                                        xPref = GetFirmWideAuthorPreference(
                                            oParent, axParams[i]);
                                }
                            }

                            if (xPref == "null")
                            {
                                if (oTarget is AdminSegment)
                                {
                                    if (!bIsDesign)
                                        //get preference value of specified author for object
                                        xPref = GetAuthorPreferenceValue(oTarget.Authors,
                                            oTarget.ID1.ToString(), axParams[i], shIndex);
                                    else
                                        //get firm-wide preference value
                                        xPref = GetFirmWideAuthorPreference(
                                            oTarget, axParams[i]);
                                }
                            }

                            oSB.Append(xPref);
                        }
                        else if (xNameUpper.IndexOf("AUTHOR") == 0)
                        {
                            if (!bIsDesign)
                            {
                                //get value for author with specified index-
                                string xIndex = xNameUpper.Substring(6);
                                short shIndex = short.Parse(xIndex);
                                oSB.Append(GetAuthorValue(oTarget.Authors, axParams[i], shIndex));
                            }
                            else
                                oSB.Append("AuthorData");
                        }
                        else if (xNameUpper.IndexOf("PERSONOFFICEADDRESS") == 0)
                        {
                            //get office address for person with the specified ID;
                            //parameter is address format;
                            //parse person ID from field code category
                            string xID = xName.Substring(19);
                            int iFormatID = System.Convert.ToInt32(axParams[i]);
                            oSB.Append(GetPersonOfficeAddress(iFormatID, xID));
                        }
                        else if (xNameUpper.IndexOf("PERSONOFFICE") == 0)
                        {
                            //get office property of person with the specified ID;
                            //parse person ID from field code category
                            string xID = xName.Substring(12);
                            oSB.Append(GetPerson(xID).GetOffice().GetProperty(axParams[i]));
                        }
                        else if (xNameUpper.IndexOf("PERSON") == 0)
                        {
                            //get property of person with the specified ID;
                            //parse person ID from field code category
                            string xID = xName.Substring(6);
                            oSB.Append(GetPerson(xID).GetPropertyValue(axParams[i]));
                        }
                        else if (xNameUpper.IndexOf("OFFICEADDRESS") == 0)
                        {
                            //get address of office with the specified ID;
                            //parse office ID from field code category;
                            //parameter is ID of address format
                            int iID = System.Convert.ToInt32(xName.Substring(13));
                            int iFormatID = System.Convert.ToInt32(axParams[i]);
                            string xFormat = GetAddressTokenString(iFormatID);
                            string xDelimReplacement = GetAddressDelimiterReplacement(iFormatID);
                            oSB.Append(GetOffice(iID).GetAddress().ToString(xFormat, xDelimReplacement));
                        }
                        else if (xNameUpper.IndexOf("OFFICE") == 0)
                        {
                            //get property of office with the specified ID;
                            //parse office ID from field code category
                            int iID = System.Convert.ToInt32(xName.Substring(6));
                            oSB.Append(GetOffice(iID).GetProperty(axParams[i]));
                        }
                        else if (xNameUpper.IndexOf("COURIER") == 0)
                        {
                            //get property of courier with the specified ID;
                            //parse courier ID from field code category
                            int iID = System.Convert.ToInt32(xName.Substring(7));
                            oSB.Append(GetCourierValue(iID, axParams[i]));
                        }
                        else if (xNameUpper.IndexOf("STYLE") == 0)
                        {
                            //get style name from name;
                            //parameter is name of property of style object
                            string xStyle = xName.Substring(5);
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetStyleValue(oDoc, xStyle, axParams[i], false));
                        }
                        else if (xNameUpper.IndexOf("PAGESETUP") == 0)
                        {
                            //parse section index from field code category;
                            //parameter is name of property of page setup object
                            string xIndex = xName.Substring(9);
                            short shIndex = short.Parse(xIndex);
                            oSB.Append(LMP.Forte.MSWord.WordDoc.GetPageSetupValue(oDoc, shIndex, axParams[i], false));
                        }
                        else if (xNameUpper == "SEGMENTINDEX")
                        {
                            //get position of Segment in Segments collection
                            Segments oSegments = null;
                            if (oTarget.IsTopLevel)
                                oSegments = oTarget.ForteDocument.Segments;
                            else
                                oSegments = oTarget.Parent.Segments;
                            for (int iSeg = 1; iSeg <= oSegments.Count; iSeg++) //GLOG 1878
                            {
                                if (oSegments[iSeg - 1] == oTarget)
                                {
                                    oSB.Append(iSeg.ToString());
                                    break;
                                }
                            }
                        }
                        else if (xNameUpper == "SEGMENTLEVEL")
                        {
                            //Get Target Segment Level value
                            switch (axParams[i])
                            {
                                case "0":
                                    oSB.Append(oTarget.L0);
                                    break;
                                case "1":
                                    oSB.Append(oTarget.L1);
                                    break;
                                case "2":
                                    oSB.Append(oTarget.L2);
                                    break;
                                case "3":
                                    oSB.Append(oTarget.L3);
                                    break;
                                case "4":
                                    oSB.Append(oTarget.L4);
                                    break;
                                default:
                                    //No other values allowed
                                    throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                                        .GetLangString("Error_InvalidFieldCode") + xFieldCode);
                            }
                        }
                        else if (xNameUpper.IndexOf("XMLEXTRACT") == 0)
                        {
                            //extract the text enclosed by the specified
                            //tag in the specified XML source
                            string xXMLSource = "";
                            string xTagName = "";
                            int iIndex = 0;
                            string[] axExtractParams;
                            if (xParamSep == mpFieldCodeParamSepOLD)
                            {
                                //Old format
                                xXMLSource = xName.Substring(10);
                                axExtractParams = axParams[i].Split(new string[] { xParamSep }, StringSplitOptions.None);
                                xTagName = axExtractParams[0];
                                //GLOG 6508 - additional parameter to specify item index
                                if (axExtractParams.GetLength(0) > 1 && !string.IsNullOrEmpty(axExtractParams[1]))
                                {
                                    try
                                    {
                                        iIndex = Int32.Parse(axExtractParams[1]);
                                    }
                                    catch {}
                                }
                            }
                            else
                            {
                                //New format with additional separator
                                axExtractParams = axParams[i].Split(new string[] { xParamSep }, StringSplitOptions.None);
                                xXMLSource = axExtractParams[0];
                                xTagName = axExtractParams[1];
                                //GLOG 6508 - additional parameter to specify item index
                                if (axExtractParams.GetLength(0) > 2 && !string.IsNullOrEmpty(axExtractParams[2]))
                                {
                                    try
                                    {
                                        iIndex = Int32.Parse(axExtractParams[2]);
                                    }
                                    catch { }
                                }
                            }
                            oSB.Append(String.GetXMLExtract(xXMLSource, xTagName, iIndex));
                        }
                        else if (xNameUpper.IndexOf("XMLFORMAT") == 0)
                        {
                            //GLOG 18538
                            //extract the text enclosed by the specified tags
                            //in the specified XML source, and return in specified format
                            string xXMLSource = "";
                            string xTemplate = "";
                            string xSep = "";
                            string xLastSep = "";
                            int iIndex = 0;
                            string[] axExtractParams;

                            axExtractParams = axParams[i].Split(new string[] { xParamSep }, StringSplitOptions.None);
                            xXMLSource = axExtractParams[0];
                            xTemplate = axExtractParams[1];
                            //restore and evaluate nested char codes
                            xTemplate = xTemplate.Replace("[Char~", "[Char__");
                            xTemplate = Expression.EvaluateCharacterCodes(xTemplate, bAllowOldFormat);

                            xSep = axExtractParams[2];
                            //restore and evaluate nested char codes
                            xSep = xSep.Replace("[Char~", "[Char__");
                            xSep = Expression.EvaluateCharacterCodes(xSep, bAllowOldFormat);

                            //GLOG 6508 - additional parameter to specify item index
                            if (axExtractParams.GetLength(0) > 3 && !string.IsNullOrEmpty(axExtractParams[3]))
                            {
                                int iLastSepIndex = 3;
                                int iIndexIndex = 4;
                                if (axExtractParams.GetLength(0) == 4)
                                {
                                    //Last two parameters are optional - if there are exactly 4 parameters
                                    //and the last parameter is numeric, assume it is the Index
                                    if (String.IsNumericInt32(axExtractParams[3]))
                                    {
                                        iLastSepIndex = -1;
                                        iIndexIndex = 3;
                                    }
                                }
                                if (iLastSepIndex > 0)
                                {
                                    xLastSep = axExtractParams[iLastSepIndex];
                                    //restore and evaluate nested char codes
                                    xLastSep = xLastSep.Replace("[Char~", "[Char__");
                                    xLastSep = Expression.EvaluateCharacterCodes(xLastSep, bAllowOldFormat);
                                }
                                if (axExtractParams.GetUpperBound(0) >= iIndexIndex)
                                {
                                    try
                                    {
                                        iIndex = Int32.Parse(axExtractParams[iIndexIndex]);
                                    }
                                    catch { }
                                }
                            }
                            oSB.Append(String.GetFormattedXml(xXMLSource, xTemplate, xSep, xLastSep, iIndex, false));
                        }
                        else if (xNameUpper.IndexOf("LEVEL") == 0)
                        {
                            //get text of specified pleading chooser level;
                            //parse level from field code category
                            try
                            {
                                byte bytLevel = System.Convert.ToByte(xName.Substring(5));
                                int iID = System.Convert.ToInt32(axParams[i]);
                                oSB.Append(LMP.Data.Application
                                    .GetPleadingCourtLevelName(bytLevel, iID));
                            }
                            catch { }
                        }
                        else if (xNameUpper.IndexOf("CHILDSEGMENTID") == 0)
                        {
                            //get id of first child segment of specified type
                            int iTypeID = Int32.Parse(axParams[i]);
                            oSB.Append(GetChildSegmentID(oTarget, iTypeID));
                        }
                        else if (xNameUpper.IndexOf("SUBVAR") == 0)
                        {
                            // Don't do anything - this is handled by mpCOM
                            oSB.Append(xFieldCode);
                        }
                        else if (xNameUpper.IndexOf("CIDETAIL") == 0)
                        {
                            oSB.Append(string.Concat("<", axParams[i].ToUpper(), ">"));
                            ////don't do anything - this is handled by CI-related functionality
                            //oSB.Append(string.Concat("[", xParams[i], "]"));
                        }
                        else if (xNameUpper.IndexOf("CONVERT") == 0 && xNameUpper.IndexOf("CONVERTTOXML") == -1)
                        {
                            //convert measurement units are return 
                            //appropriate value
                            string xBaseUnit = xName.Replace("Convert", "");
                            string[] xValueUnits = xParam.Split(new string[] {xParamSep}, StringSplitOptions.None);
                            string xValueUnit = xValueUnits[0].ToString();
                            decimal decValue = decimal.Parse(xValueUnits[1].ToString(),
                                LMP.Culture.USEnglishCulture);

                            oSB.Append(ConvertMeasurementUnits(xBaseUnit, xValueUnit, decValue));
                        }
                        else if (xNameUpper.IndexOf("LISTLOOKUP") == 0)
                        {
                            //lookup the specified column in specified list
                            //return value of specified column
                            string[] xLookupParams = xParam.Split(new string[] {xParamSep}, StringSplitOptions.None);

                            //parse out the necessary param values
                            int iSearchColumn = Int32.Parse(xLookupParams[1]);
                            int iReturnColumn = Int32.Parse(xLookupParams[2]);
                            string xSearchValue = xLookupParams[0].ToString();
                            string xListName = xName.Replace("ListLookup", "");

                            //lookup value
                            oSB.Append(LookupInList(xListName, xSearchValue, iSearchColumn, iReturnColumn));
                        }
                        else if (xNameUpper.IndexOf("REPLACE") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <SourceString>_<SearchStrings>_<ReplaceWithString>
                            string[] xReplaceParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            string xSourceString = xReplaceParams[0];
                            if (xSourceString != "" && xReplaceParams[1] != "")
                            {
                                //get search strings
                                string[] aSearchStrings = xReplaceParams[1].Split(new string[] {"!!"}, StringSplitOptions.None);

                                foreach (string xSearch in aSearchStrings)
                                {
                                    //replace each search string
                                    //xSourceString = xSourceString.Replace(xSearch, xReplaceParams[2]);

                                    //replace standard wildcard token with regular expression equivalent
                                    string xSearchMod = xSearch.Replace("*", ".*?");

                                    //GLOG 3561: Optional 3rd paramater added to allow specifying
                                    //different text to be used to replace last instance of matching text
                                    if (xReplaceParams.GetUpperBound(0) > 2)
                                    {
                                        System.Text.RegularExpressions.Regex oRegex = new System.Text.RegularExpressions.Regex(xSearchMod);
                                        System.Text.RegularExpressions.MatchCollection oMatches = oRegex.Matches(xSourceString);
                                        for (int m = 0; m < oMatches.Count; m++)
                                        {
                                            string xReplaceText = xReplaceParams[2];
                                            if (m == oMatches.Count - 1)
                                                xReplaceText = xReplaceParams[3];
                                            xSourceString = xSourceString.Substring(0, oMatches[m].Index) +
                                                xReplaceText + xSourceString.Substring(oMatches[m].Index + oMatches[m].Length);
                                        }
                                    }
                                    else
                                    {
                                        //replace using regular expressions-
                                        //escape reserved characters - [\^$.|?*+(){}
                                        xSearchMod = xSearchMod.Replace("[", @"\[");
                                        xSearchMod = xSearchMod.Replace("^", @"\^");
                                        xSearchMod = xSearchMod.Replace("$", @"\$");
                                        xSearchMod = xSearchMod.Replace(".", @"\.");
                                        xSearchMod = xSearchMod.Replace("|", @"\|");
                                        xSearchMod = xSearchMod.Replace("?", @"\?");
                                        xSearchMod = xSearchMod.Replace("*", @"\*");
                                        xSearchMod = xSearchMod.Replace("+", @"\+");
                                        xSearchMod = xSearchMod.Replace("(", @"\(");
                                        xSearchMod = xSearchMod.Replace(")", @"\)");
                                        xSearchMod = xSearchMod.Replace("{", @"\{");
                                        xSearchMod = xSearchMod.Replace("}", @"\}");
                                        xSourceString = System.Text.RegularExpressions.Regex.Replace(xSourceString, xSearchMod, xReplaceParams[2]);
                                    }
                                }
                            }
                            oSB.Append(xSourceString);
                        }
                        else if (xNameUpper.IndexOf("TRUNCATE") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <SourceString>_<Length>
                            string[] xTruncParams = xParam.Split(new string[] {xParamSep}, StringSplitOptions.None);
                            if (Int32.Parse(xTruncParams[1]) < xTruncParams[0].Length)
                                oSB.Append(xTruncParams[0].Substring(0, Int32.Parse(xTruncParams[1])));
                            else
                                //Return entire string if shorter than specified length
                                oSB.Append(xTruncParams[0]);
                        }
                        else if (xNameUpper.IndexOf("SUBSTRINGAFTER") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <TEXT>_<STRING_TO_FIND>
                            string[] xParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iPosFound = xParams[0].IndexOf(xParams[1]);

                            if (iPosFound > -1)
                                oSB.Append(xParams[0].Substring(iPosFound + xParams[1].Length));
                            else
                                oSB.Append(xParams[0]);
                        }
                        else if (xNameUpper.IndexOf("SUBSTRINGAFTERLAST") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <TEXT>_<STRING_TO_FIND>
                            string[] xParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iPosFound = xParams[0].LastIndexOf(xParams[1]);

                            if (iPosFound > -1)
                                oSB.Append(xParams[0].Substring(iPosFound + xParams[1].Length));
                            else
                                oSB.Append(xParams[0]);
                        }
                        else if (xNameUpper.IndexOf("SUBSTRINGBEFORE") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <TEXT>_<STRING_TO_FIND>
                            string[] xParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iPosFound = xParams[0].IndexOf(xParams[1]);

                            if (iPosFound > -1)
                                oSB.Append(xParams[0].Substring(0, iPosFound + xParams[1].Length));
                            else
                                oSB.Append(xParams[0]);
                        }
                        else if (xNameUpper.IndexOf("SUBSTRINGBEFORELAST") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <TEXT>_<STRING_TO_FIND>
                            string[] xParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iPosFound = xParams[0].LastIndexOf(xParams[1]);

                            if (iPosFound > -1)
                                oSB.Append(xParams[0].Substring(0, iPosFound + xParams[1].Length));
                            else
                                oSB.Append(xParams[0]);
                        }
                        else if (xNameUpper.IndexOf("STARTSWITH") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <SourceString>_<SearchString>
                            string[] xStartsWithParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            if (xStartsWithParams[0].StartsWith(xStartsWithParams[1]))
                                oSB.Append("true");
                            else
                                oSB.Append("false");
                        }
                        else if (xNameUpper.IndexOf("ENDSWITH") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            //parameter format is <SourceString>_<SearchString>
                            string[] xEndsWithParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            xEndsWithParams[0] = xEndsWithParams[0].TrimEnd();
                            if (xEndsWithParams[0].EndsWith(xEndsWithParams[1]))
                                oSB.Append("true");
                            else
                                oSB.Append("false");
                        }

                        else if (xNameUpper.IndexOf("FORMATDATETIME") == 0)
                        {
                            //Parameter format is <DateTimeFormatString>_<DateString>
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            string xFormatString = aParams[0];

                            DateTime oDT;

                            //if second parameter does not exist, we will pass oDT
                            //otherwise perform DateTime parse on string value
                            if (aParams.Length == 2 && aParams[1] != "")
                                oDT = DateTime.Parse(aParams[1]);
                            else
                                oDT = DateTime.Now;

                            //GLOG 5384: Don't require Segment - use English default if not assigned
                            if (oSegment != null)
                                oSB.Append(LMP.String.EvaluateDateTimeValue(xFormatString, oDT, oSegment.Culture));
                            else
                                oSB.Append(LMP.String.EvaluateDateTimeValue(xFormatString, oDT));
                        }
                        else if (xNameUpper.IndexOf("CONVERTTOXML") == 0)
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            string xSource = aParams[0].ToString();

                            //obtain delimiter
                            string xDelimiter = "";
                            if (aParams.Length == 3)
                                xDelimiter = aParams[2];

                            //obtain tag name
                            string xXMLTemplate = "Default";
                            if (aParams.Length > 1)
                                xXMLTemplate = aParams[1].ToString();

                            //evaluate delimifter [Char] field code if necessary
                            if (xDelimiter.IndexOf("[") != -1)
                            {
                                //restore underscore character and evaluate
                                xDelimiter = xDelimiter.Replace("[Char~", "[Char__");
                                xDelimiter = Expression.EvaluateCharacterCodes(xDelimiter, bAllowOldFormat);
                                xDelimiter = xDelimiter.TrimEnd();
                                xDelimiter = xDelimiter.TrimStart();
                            }

                            //convert to detail type XML string
                            oSB.Append(ConvertToDetailXML(xSource, xXMLTemplate, xDelimiter));
                        }
                        else if (xNameUpper.IndexOf("EXTERNALDATA") == 0)
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            // first item is External Data Record ID
                            int iRecordID = Int32.Parse(aParams[0]);
                            // second item is SProc Name
                            string xSProcName = aParams[1].ToString();
                            // last item is Column ID
                            string xColName = aParams[aParams.GetUpperBound(0)].ToString();
                            // Check if column name is unspecified
                            if (xColName.ToUpper() == "NULL")
                                xColName = "";

                            object[] aSProcParams = null;
                            if (aParams.GetUpperBound(0) > 2)
                                aSProcParams = new object[aParams.GetUpperBound(0) - 2];
                            // intermediate items are SProc parameters
                            for (int iParam = 2; iParam < aParams.GetUpperBound(0); iParam++)
                            {
                                aSProcParams[iParam - 2] = aParams[iParam].ToString();
                            }
                            ExternalConnections oCons = new ExternalConnections();
                            ExternalConnection oCon = (ExternalConnection)oCons.ItemFromID(iRecordID);

                            System.Data.DataSet oDS = oCon.GetDataSet(xSProcName, aSProcParams);
                            // Get first row of dataset
                            System.Data.DataRow oRow = oDS.Tables[0].Rows[0];

                            if (xColName == "")
                            {
                                oSB.Append(oRow[0].ToString());
                            }
                            else
                            {
                                oSB.Append(oRow[xColName].ToString());
                            }

                        }
                        else if (xNameUpper == "SEGMENTID")
                        {
                            //Get ID of AdminSegment with matching name
                            int iID = LMP.Data.Application.GetSegmentIDFromName(axParams[i]);
                            if (iID > 0)
                                oSB.Append(iID.ToString());
                            else
                                throw new LMP.Exceptions.FieldCodeException(
                                    LMP.Resources.GetLangString(
                                    "Error_NamedSegmentDoesNotExist") + axParams[i]);
                        }
                        else if (xNameUpper == "TOPSEGMENTID")
                        {
                            //Get ID of top segment
                            try
                            {
                                string xID = GetTopSegmentID(oSegment);
                                oSB.Append(xID.ToString());
                            }
                            catch
                            {
                                throw new LMP.Exceptions.FieldCodeException(
                                    LMP.Resources.GetLangString(
                                    "Error_NamedSegmentDoesNotExist") + oSegment.Name);
                            }
                        }
                        else if (xNameUpper == "TOPSEGMENTNAME")
                        {
                            try
                            {
                                oSB.Append(GetTopSegmentName(oSegment));
                            }
                            catch
                            {
                                throw new LMP.Exceptions.FieldCodeException(
                                    LMP.Resources.GetLangString(
                                    "Error_NamedSegmentDoesNotExist") + oSegment.Name);
                            }
                        }
                        else if (xNameUpper == "SEGMENTNAME")
                        {
                            string xID = axParams[0];
                            //GLOG 6966: Make sure string can be parsed as a Double even if
                            //the Decimal separator is something other than '.' in Regional Settings
                            if (xID.EndsWith(".0"))
                            {
                                xID = xID.Replace(".0", "");
                            }

                            int iID = (int)double.Parse(xID);

                            try
                            {
                                string xSegmentName = LMP.Data.Application.GetSegmentNameFromID(iID);
                                oSB.Append(xSegmentName);
                            }
                            catch
                            {
                                throw new LMP.Exceptions.FieldCodeException(
                                    LMP.Resources.GetLangString(
                                    "Error_NamedSegmentDoesNotExist") + xID);
                            }
                        }
                        else if (xNameUpper == "UPPERCASE")
                        {
                            oSB.Append(axParams[i].ToUpper());
                        }
                        else if (xNameUpper == "LOWERCASE")
                        {
                            oSB.Append(axParams[i].ToLower());
                        }
                        else if (xNameUpper == "DATEFORMAT")
                        {
                            DateTime oDT = DateTime.MinValue;
                            string xDateFormat = axParams[i];
                            string xFieldCodeSwitch = "";

                            if (xDateFormat.EndsWith(" /F"))
                            {
                                //format is a field code
                                xFieldCodeSwitch = " /F";

                                //remove tag for date parsing below
                                xDateFormat = xDateFormat.Substring(0, xDateFormat.Length - 3);
                            }
                            else if (xDateFormat.EndsWith("/F"))
                            {
                                //format is a field code
                                xFieldCodeSwitch = "/F";

                                //remove tag for date parsing below
                                xDateFormat = xDateFormat.Substring(0, xDateFormat.Length - 2);
                            }

                            if (DateTime.TryParse(xDateFormat, out oDT))
                            {
                                //If input is a date, but not current date, treat as literal text
                                if (oDT.Date == DateTime.Now.Date)
                                    xDateFormat = LMP.Architect.Base.Application.
                                        GetDateFormatPattern(xDateFormat);
                            }
                            else if (xDateFormat.Contains("__"))
                            {
                                //GLOG 4959: Test dates including underscores for match with configured formats
                                string xTestFormat = LMP.Architect.Base.Application.GetDateFormatPattern(xDateFormat);
                                if (xTestFormat != "")
                                    xDateFormat = xTestFormat;
                            }
                            if (xFieldCodeSwitch != "")
                            {
                                xDateFormat += xFieldCodeSwitch;
                            }

                            oSB.Append(xDateFormat);
                        }
                        else if (xNameUpper == "COUNTENTITIES")
                        {
                            string xTagName = "";
                            string xXML = "";
                            // format is COUNTENTITIES__X__Y
                            // returns count of nodes with Name 'Y' in XML 'X'
                            int iSep = xParam.IndexOf(xParamSep);
                            if (iSep > -1)
                            {
                                xXML = xParam.Substring(0, iSep);
                                xTagName = xParam.Substring(iSep + 1);
                            }
                            else
                                xXML = xParam;
                            oSB.Append(String.GetXMLEntityCount(xXML, xTagName));
                        }
                        else if (xNameUpper == "COUNT")
                        {
                            // format is COUNT__X__Y
                            // returns count of instances of 'X' in 'Y'
                            int iSep = xParam.IndexOf(xParamSep);
                            if (iSep == -1)
                            {
                                //Must be 2 paramaters
                                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                                       .GetLangString("Error_InvalidFieldCode") + xFieldCode);
                            }
                            else
                            {
                                string xSearch = xParam.Substring(0, iSep);
                                string xSource = xParam.Substring(iSep + 1);
                                if (!string.IsNullOrEmpty(xSource))
                                    oSB.Append(String.CountChrs(xSource.ToUpper(), xSearch.ToUpper()).ToString());
                            }

                        }
                        else if (xNameUpper == "NUMBERTEXT")
                        {
                            int iNum;
                            try
                            {
                                iNum = Int32.Parse(xParam);
                            }
                            catch
                            {
                                //Parameter must be Numeric
                                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                                    .GetLangString("Error_InvalidFieldCode") + xFieldCode);

                            }
                            oSB.Append(String.NumberToText(iNum));
                        }
                        else if (xNameUpper == "USPSFORMAT")
                        {
                            // format is USPSFORMAT__X or USPSFORMAT__X__Y
                            // returns 'X' converted to uppercase with any characters in 'Y' stripped
                            string xInput = "";
                            string xChars = null;
                            int iSep = xParam.IndexOf(xParamSep);
                            if (iSep > 0)
                            {
                                xInput = xParam.Substring(0, iSep);
                                xChars = xParam.Substring(iSep + 1);
                            }
                            else
                            {
                                //Use default settings
                                xInput = xParam;
                            }
                            oSB.Append(ConvertToUSPSFormat(xInput, xChars));
                        }
                        else if (xNameUpper == "EXTRACTENTITIES")
                        {
                            string xSource = "";
                            int iCount = 0;
                            int iStart = 1;
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);

                            try
                            {
                                xSource = aParams[0];
                                iCount = Int32.Parse(aParams[1]);
                                if (aParams.GetUpperBound(0) > 1)
                                    iStart = Int32.Parse(aParams[2]);
                            }
                            catch
                            {
                                //Parameter must be Numeric
                                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                                    .GetLangString("Error_InvalidFieldCode") + xFieldCode);

                            }
                            if (xSource == "" || iCount == -1)
                                //Return value unchanged
                                oSB.Append(xSource);
                            else
                                oSB.Append(ExtractXMLEntities(xSource, iCount, iStart));
                        }
                        else if (xNameUpper.IndexOf("CHOOSE") == 0)
                        {
                            //restore and evaluate nested char codes
                            xParam = xParam.Replace("[Char~", "[Char__");
                            xParam = Expression.EvaluateCharacterCodes(xParam, bAllowOldFormat);

                            string xAppend = null;
                            //parameter format is <SourceString>__<SearchString>__<ReplaceWithString>
                            string[] xChooseParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            if (xChooseParams[0] != "")
                            {
                                //first param is the Match Expression
                                string xExpression = xChooseParams[0].Trim();

                                //compare every odd numbered paramater to the
                                //Match Expression -- these are the Match Values
                                for (int y = 1; y < xChooseParams.Length; y += 2)
                                {
                                    if (xExpression == xChooseParams[y].Trim())
                                    {
                                        //append Return Value
                                        //GLOG 6847
                                        if (y >= xChooseParams.Length - 1)
                                            //Value matches Default
                                            xAppend = xChooseParams[xChooseParams.Length - 1];
                                        else
                                            xAppend = xChooseParams[y + 1];
                                        break;
                                    }
                                }

                                if (xAppend == null)
                                    //append default value
                                    xAppend = xChooseParams[xChooseParams.Length - 1];

                            }
                            oSB.Append(xAppend);
                        }
                        else if (xNameUpper == "COUNTRYNAME")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iID = int.Parse(aParams[0]);
                            if (aParams[0] != "")
                            {
                                oSB.Append(GetCountryName(iID));
                            }
                        }
                        else if (xNameUpper == "STATENAME")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            if (aParams[0] != "")
                            {
                                int iID = int.Parse(aParams[0]);
                                int iCountryID = int.Parse(aParams[1]);
                                oSB.Append(GetStateName(iID, iCountryID));
                            }
                        }
                        else if (xNameUpper == "COUNTYNAME")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            int iID = int.Parse(aParams[0]);
                            if (aParams[0] != "")
                            {
                                int iStateID = int.Parse(aParams[1]);
                                oSB.Append(GetCountyName(iID, iStateID));
                            }
                        }
                        else if (xNameUpper == "DETAILVALUE")
                        {
                            //we support the DETAILVALUE field code with or without
                            //a variable name parameter - in the latter case, the name
                            //is implied by the context, which we don't have here
                            if (axParams.Length > 0)
                            {
                                string xVariableName = axParams[i];
                                Variable oVar = oTarget.Variables.ItemFromName(xVariableName);
                                if (oVar != null)
                                    oSB.Append(LMP.Architect.Api.Variable.GetDetailValue(oVar));
                            }
                        }
                        else if (xNameUpper == "RELINEVALUE")
                        {
                            //we support the RELINEVALUE field code with or without
                            //a variable name parameter - in the latter case, the name
                            //is implied by the context, which we don't have here
                            if (axParams.Length > 0)
                            {
                                string xVariableName = axParams[i];
                                Variable oVar = oTarget.Variables.ItemFromName(xVariableName);
                                if (oVar != null)
                                {
                                    //GLOG 4887 (dm) - turn off event handling - using Range.Information to
                                    //get position info triggers the content control enter and exit events
                                    ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                    oSB.Append(GetRelineValue(oVar));
                                    ForteDocument.IgnoreWordXMLEvents = iEvents;
                                }
                            }
                        }
                        else if (xNameUpper == "LOCATIONID")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            mpLocationTypes iType = (mpLocationTypes)(int.Parse(aParams[0]));
                            int iParentID = int.Parse(aParams[1]);
                            string xLocationName = aParams[2];

                            oSB.Append(GetLocationID(iType, iParentID, xLocationName));
                        }
                        else if (xNameUpper == "DMS")
                        {
                            if (xParam.ToUpper() == "ISPROFILED")
                            {
                                string xTF = "";
                                if (fDMS.DMS.IsProfiled)
                                    xTF = "true";
                                else
                                    xTF = "false";
                                oSB.Append(xTF);
                            }
                            else
                            {
                                //May be a single parameter with DMS Profile property
                                //Or two parameters for Date Property and Date Format
                                string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                                string xProp = aParams[0];
                                string xDateFormat = "";
                                if (aParams.Length > 1)
                                    xDateFormat = aParams[1];
                                oSB.Append(fDMS.DMS.GetProfileValue(xProp, xDateFormat));
                            }
                        }
                        else if (xNameUpper == "TAGEXPANDEDVALUE")
                        {
                            //turn off word xml event handling - this is necessary due
                            //to a weird native bug whereby Range.Find.Execute triggers
                            //the XMLSelectionChange event under certain circumstances -
                            //we're seeing this when inserting side-by-side pleading table items
                            ForteDocument.IgnoreWordXMLEvents =
                                LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                            string xStartDelimiter = "";
                            string xEndDelimiter = "";
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            string xVariableName = aParams[0];
                            Variable oVar = oTarget.Variables.ItemFromName(xVariableName);
                            if (oVar != null)
                            {
                                if (oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                {
                                    if (aParams.Length > 1)
                                        xStartDelimiter = aParams[1];
                                    if (aParams.Length > 2)
                                        xEndDelimiter = aParams[2];
                                    //GLOG 8407: Only check revisions if Segment is fully created
                                    oSB.Append(LMP.Forte.MSWord.WordDoc.GetExpandedTagValue(
                                        oVar.AssociatedWordTags[0], xStartDelimiter, xEndDelimiter, oVar.Segment.CreationStatus == Base.Segment.Status.Finished));
                                }
                                else
                                {
                                    //just return range of content control as no expanded tag value is necessary
                                    //GLOG 8407: Only check revisions if Segment is fully created
                                    string xTagValue = LMP.Forte.MSWord.WordDoc
                                        .GetRangeTextIfAllCapped(oVar.ContainingContentControls[0].Range, oVar.Segment.CreationStatus == Base.Segment.Status.Finished);
                                    oSB.Append(xTagValue);
                                }
                            }
                        }
                        else if (xNameUpper == "SEGMENTLANGUAGEID")
                        {
                            oSB.Append(oSegment.Culture.ToString());
                        }
                        else if (xNameUpper == "SEGMENTLANGUAGENAME")
                        {
                            string xCulture = LMP.Data.Application
                                .GetLanguagesDisplayValue(oSegment.Culture.ToString());
                            oSB.Append(xCulture);
                        }
                        else if (xNameUpper == "ADD")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            try
                            {
                                //GLOG 7400:  Handle empty/invalid values same as zero
                                decimal dResult = 0;
                                for (int iNum = 0; iNum < aParams.Length; iNum++)
                                {
                                    string xNum = aParams[iNum];
                                    if (xNum.StartsWith("(") && xNum.EndsWith(")"))
                                        //If Number is in parens, treat as negative
                                        xNum = "-" + xNum.Substring(1, xNum.Length - 2);
                                    //Attempt to parse as Decimal
                                    decimal dParam = 0;
                                    if (decimal.TryParse(xNum, out dParam))
                                        dResult = dResult + dParam;
                                }
                                oSB.Append(dResult.ToString());
                            }
                            catch
                            {
                                oSB.Append("");
                            }
                        }
                        else if (xNameUpper == "SUBTRACT")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            try
                            {
                                //GLOG 7400:  Handle empty/invalid values same as zero
                                decimal dResult = 0;
                                decimal dParam1 = 0;
                                decimal dParam2 = 0;
                                string xNum1 = aParams[0];
                                if (xNum1.StartsWith("(") && xNum1.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum1 = "-" + xNum1.Substring(1, xNum1.Length - 2);
                                if (!decimal.TryParse(xNum1, out dParam1))
                                    dParam1 = 0;

                                string xNum2 = aParams[1];
                                if (xNum2.StartsWith("(") && xNum2.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum2 = "-" + xNum2.Substring(1, xNum2.Length - 2);
                                if (!decimal.TryParse(xNum2, out dParam2))
                                    dParam2 = 0;
                                dResult = dParam1 - dParam2;
                                oSB.Append(dResult.ToString());
                            }
                            catch
                            {
                                oSB.Append("");
                            }
                        }
                        else if (xNameUpper == "MULTIPLY")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            try
                            {
                                //GLOG 7400:  Handle empty/invalid values same as zero
                                decimal dResult = 1; //Multiple first paramter by 1
                                for (int iNum = 0; iNum < aParams.Length; iNum++)
                                {
                                    string xNum = aParams[iNum];
                                    if (xNum.StartsWith("(") && xNum.EndsWith(")"))
                                        //If Number is in parens, treat as negative
                                        xNum = "-" + xNum.Substring(1, xNum.Length - 2);
                                    //Attempt to parse as Decimal
                                    decimal dParam = 0;
                                    if (decimal.TryParse(xNum, out dParam))
                                        dResult = dResult * dParam;
                                    else
                                        //If parameter is not valid number, treat as zero
                                        dResult = 0;
                                }
                                oSB.Append(dResult.ToString());
                            }
                            catch
                            {
                                oSB.Append("");
                            }
                        }
                        else if (xNameUpper == "DIVIDE")
                        {
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            try
                            {
                                //GLOG 7400:  Handle empty/invalid values same as zero
                                decimal dResult = 0;
                                decimal dParam1 = 0;
                                decimal dParam2 = 0;
                                string xNum1 = aParams[0];
                                if (xNum1.StartsWith("(") && xNum1.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum1 = "-" + xNum1.Substring(1, xNum1.Length - 2);
                                if (!decimal.TryParse(xNum1, out dParam1))
                                    dParam1 = 0;

                                string xNum2 = aParams[1];
                                if (xNum2.StartsWith("(") && xNum2.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum2 = "-" + xNum2.Substring(1, xNum2.Length - 2);
                                if (!decimal.TryParse(xNum2, out dParam2))
                                    dParam2 = 0;

                                if (dParam1 == 0 || dParam2 == 0)
                                {
                                    //Division by 0 not allowed
                                    dResult = 0;
                                }
                                else
                                {
                                    dResult = dParam1 / dParam2;
                                }
                                oSB.Append(dResult.ToString());
                            }
                            catch
                            {
                                oSB.Append("");
                            }
                        }
                        else if (xNameUpper == "MODULUS")
                        {
                            //GLOG 4452
                            string[] aParams = xParam.Split(new string[] { xParamSep },
                                StringSplitOptions.None);
                            try
                            {
                                //GLOG 7400:  Handle empty/invalid values same as zero
                                int iResult = 0;
                                int iParam1 = 0;
                                int iParam2 = 0;
                                string xNum1 = aParams[0];
                                if (xNum1.StartsWith("(") && xNum1.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum1 = "-" + xNum1.Substring(1, xNum1.Length - 2);
                                if (!Int32.TryParse(xNum1, out iParam1))
                                    iParam1 = 0;

                                string xNum2 = aParams[1];
                                if (xNum2.StartsWith("(") && xNum2.EndsWith(")"))
                                    //If Number is in parens, treat as negative
                                    xNum2 = "-" + xNum2.Substring(1, xNum2.Length - 2);
                                if (!Int32.TryParse(xNum2, out iParam2))
                                    iParam2 = 0;

                                if (iParam2 == 0)
                                {
                                    //Modulus 0 not valid
                                    iResult = 0;
                                }
                                else
                                    iResult = iParam1 % iParam2;

                                oSB.Append(iResult.ToString());
                            }
                            catch
                            {
                                oSB.Append("");
                            }
                        }
                        else if (xNameUpper == "SEGMENTINSERTIONCOMPLETE")
                        {
                            oSB.Append((oSegment.CreationStatus == Segment.Status.Finished).ToString());

                        }
                        //GLOG : 8375 : JSW
                        else if (xNameUpper == "ISNUMERIC")
                        {
                            oSB.Append(String.IsNumericInt32(axParams[i]));
                        }

                        //GLOG : 8374 : JSW
                        else if (xNameUpper.IndexOf("DATEDIFF") == 0)
                        {
                            //Parameter format is <DateTimeFormatString>_<DateString>
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            
                            DateTime ODTStart = DateTime.Parse(aParams[0]);

                            DateTime oDTEnd = DateTime.Parse(aParams[1]);

                            string xDateUnit = aParams[2];

                            int iDateDiff = GetDateDuration(ODTStart, oDTEnd, xDateUnit);

                            oSB.Append(iDateDiff.ToString());
                        }
                        else if (xNameUpper.IndexOf("DATEADD") == 0)
                        {
                            //Parameter format is <DateTimeFormatString>_<DateString>
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            string xFormatString = "M/dd/yyyy";

                            DateTime oDT = DateTime.Parse(aParams[0]);
                            string xDateUnit = aParams[1];
                            int iCount = Int32.Parse(aParams[2]);

                            DateTime oNewDT;
                            
                            switch (xDateUnit)
                            {
                                case ("Days"):
                                    oNewDT = oDT.AddDays((double) iCount);
                                    break;
                                case ("Months"):
                                    oNewDT = oDT.AddMonths(iCount);
                                    break;
                                case ("Years"):
                                    oNewDT = oDT.AddYears(iCount);
                                    break;
                                default:
                                   oNewDT = oDT;
                                   break;
                            }
                                oSB.Append(LMP.String.EvaluateDateTimeValue(xFormatString, oNewDT));
                        }
                        else if (xNameUpper.IndexOf("DATESUBTRACT") == 0)
                        {
                            //Parameter format is <DateTimeFormatString>_<DateString>
                            string[] aParams = xParam.Split(new string[] { xParamSep }, StringSplitOptions.None);
                            string xFormatString = "M/dd/yyyy";

                            DateTime oDT = DateTime.Parse(aParams[0]);
                            string xDateUnit = aParams[1];
                            int iCount = Int32.Parse(aParams[2]);

                            DateTime oNewDT;

                            switch (xDateUnit)
                            {
                                case ("Days"):
                                    oNewDT = oDT.AddDays((double)-iCount);
                                    break;
                                case ("Months"):
                                    oNewDT = oDT.AddMonths(-iCount);
                                    break;
                                case ("Years"):
                                    oNewDT = oDT.AddYears(-iCount);
                                    break;
                                default:
                                    oNewDT = oDT;
                                    break;
                            }
                            oSB.Append(LMP.String.EvaluateDateTimeValue(xFormatString, oNewDT));
                        }
                        else
                        {
                            //field code is not recognized
                            bIsUnsupported = true;
                        }
                    }
                    else
                    {
                        //append value separator
                        if (LMP.String.IsNumericInt32(axParams[i]))
                            //ascii or MacPac shortcut
                            oSB.Append(GetMacPacChar(axParams[i]));
                        else
                            //literal
                            oSB.Append(axParams[i]);
                    }
                }

                //append item separator; "\r\n" is used for compatibility
                //with InsertText doc prop action
                oSB.Append("\r\n");
            }

            catch (System.Exception e)
            {
                //error was caused by parameter
                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                    .GetLangString("Error_CouldNotEvaluateFieldCode") + xFieldCode, e);
            }
            finally
            {
                if(oMPDocument != null)
                    ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
            }

            if (bIsUnsupported)
            {
                //GLOG #4216 - dcf -
                //field code was not recognized - 
                //return as text
                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                    .GetLangString("Error_InvalidFieldCode") + xFieldCode);
            }

            //trim trailing separator
            oSB.Remove(oSB.Length - 2, 2);

            string xValue = oSB.ToString();
            //GLOG 2151: Replace any character codes in the evaluated value
            xValue = Expression.EvaluateCharacterCodes(xValue, bAllowOldFormat);

            LMP.Benchmarks.Print(t0, xFieldCode);

            //return value in case in which field code was specified
            if (xName == xNameUpper)
                return xValue.ToUpper();
            else if (xName == xName.ToLower())
                return xValue.ToLower();
            else
                return xValue;
        }

		/// <summary>
		/// returns the character with the specified ascii or
		/// MacPac shortcut representing a set of characters
		/// </summary>
		/// <param name="xASCII"></param>
		/// <returns></returns>
		public static string GetMacPacChar(string xASCII)
		{
			Trace.WriteNameValuePairs("xASCII", xASCII);

			string xFieldCode = System.String.Concat(mpFieldCodeStartTag,
				"Char__", xASCII, mpFieldCodeEndTag.ToString());

			if (xASCII == "")
			{
				throw new LMP.Exceptions.FieldCodeException(LMP.Resources
					.GetLangString("Error_InvalidFieldCode") + xFieldCode);
			}

			//check for supported shortcuts
			switch (xASCII)
			{
				case "1310":
					return "\r\n";
				case "1313":
					return "\r\r";
				case "1111":
					return "\v\v";
				case "3232":
					return "  ";
				case "4432":
					return ", ";
				default:
					break;
			}

			//return character with specified ascii value
			try
			{
				char cChar = (char)System.Convert.ToByte(xASCII);
				return cChar.ToString();
			}
			catch
			{
				//value is not between 0 and 255
				throw new LMP.Exceptions.FieldCodeException(LMP.Resources
					.GetLangString("Error_InvalidFieldCodeParameter") + xFieldCode);
			}
		}
		#endregion
		#region *********************private methods*********************
        /// <summary>
        /// returns the name of the country with the specified ID
        /// </summary>
        /// <param name="iID"></param>
        /// <returns></returns>
        internal static string GetCountryName(int iID)
        {
            object[] aParams = { iID };
            object oName = LMP.Data.SimpleDataCollection.GetScalar("spCountryName", aParams);

            if (oName != null)
                return oName.ToString();
            else
                return "";
        }
        internal static string GetStateName(int iID, int iCountryID)
        {
            object[] aParams = { iID, iCountryID };
            object oName = LMP.Data.SimpleDataCollection.GetScalar("spStateName", aParams);

            if (oName != null)
                return oName.ToString();
            else
                return "";
        }
        internal static string GetCountyName(int iID, int iStateID)
        {
            object[] aParams = { iID, iStateID };
            object oName = LMP.Data.SimpleDataCollection.GetScalar("spCountyName", aParams);

            if (oName != null)
                return oName.ToString();
            else
                return "";
        }

        /// <summary>
        /// returns the ID of the root segment
        /// of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private static string GetTopSegmentID(Segment oSegment)
        {
            Segment oParent = oSegment.Parent;
            Segment oTopParent = oSegment;

            while (oParent != null)
            {
                oTopParent = oParent;
                oParent = oParent.Parent;
            }

            return oTopParent.ID;
        }

        /// <summary>
        /// returns the name of the root segment
        /// of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private static string GetTopSegmentName(Segment oSegment)
        {
            Segment oParent = oSegment.Parent;
            Segment oTopParent = oSegment;

            while (oParent != null)
            {
                oTopParent = oParent;
                oParent = oParent.Parent;
            }

            return oTopParent.Name;
        }

        /// <summary>
        /// converts designated input value from base unit value to value unit value
        /// </summary>
        /// <param name="xBaseUnit"></param>
        /// <param name="xValueUnit"></param>
        /// <param name="decValue"></param>
        /// <returns></returns>
        internal static string ConvertMeasurementUnits(string xBaseUnit, string xValueUnit, decimal decValue)
        {
            decimal decValueUnits = UnitsPerBase(xValueUnit);
            decimal decBaseUnits = UnitsPerBase(xBaseUnit);
            return (decValue * (decValueUnits / decBaseUnits)).ToString();
        }
        /// <summary>
        /// converts designated decimal value to ValueUnits,
        /// ie. 1 inch to points = 72
        /// </summary>
        /// <param name="decValueToConvert"></param>
        /// <returns></returns>
        internal static decimal UnitsPerBase(string xUnit)
        {
            decimal decFactor = 1;

            switch (xUnit)
            {
                case "Inches":
                    decFactor = 1;
                    break;
                case "Centimeters":
                    decFactor = 2.54M;
                    break;
                case "Millimeters":
                    decFactor = 25.4M;
                    break;
                case "Points":
                    decFactor = 72;
                    break;
                case "Picas":
                    decFactor = 6;
                    break;
                default:
                    break;
            }

            return decFactor;
        }

        /// <summary>
        /// converts delimited source string to XML string whose tags are defined by
        /// xXMLTemplate string
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xDelimiter"></param>
        /// <param name="xXMLTemplate"></param>
        /// <returns></returns>
        internal static string ConvertToDetailXML(string xSource, string xXMLTemplate, string xDelimiter)
        {
            //use mpEndOfField as default if item is empty
            if (System.String.IsNullOrEmpty(xDelimiter))
                xDelimiter = StringArray.mpEndOfField.ToString();  
            
            //replace escaped returns/linefeed with delimiter
            xSource = xSource.Replace("\r\n", xDelimiter);
            
            //delimit paired XML tags in template string so we can split
            xXMLTemplate = xXMLTemplate.Replace("><", ">" + xDelimiter + "<"); 
            
            //create arrays of source items and template tags
            char cDelim = char.Parse(xDelimiter);
            string[] aSource = xSource.Split(cDelim);
            string[] aTemplateElements = xXMLTemplate.Split(cDelim);

            //cycle through source array and construct XML string
            //in same format as that produced by Detail type controls
            int iElement = 0;
            int iIndex = 1;

            StringBuilder oSB = new StringBuilder(); 
            for (int i = 0; i < aSource.Length; i++)
            {
                iElement = iElement < aTemplateElements.Length ? iElement: 0 ;
                
                //create opening and closing tags for each element
                string xOpenTag = aTemplateElements[iElement];
                xOpenTag = xOpenTag.Replace(">", "");
                string xClosingTag = aTemplateElements[iElement].Replace("<", "</");

                iIndex = i > 0 && iElement == aTemplateElements.GetLowerBound(0) ? iIndex + 1 : iIndex;
                
                oSB.AppendFormat(@"{0} Index=""{1}"" UNID=""{2}"">{3}{4}",
                    xOpenTag, iIndex.ToString(), "0", aSource[i], xClosingTag);

                iElement++;
            }
            
            return oSB.ToString();
        }
        
        /// <summary>
		/// returns the address in the specified format
		/// for the person with the specified ID
		/// </summary>
		/// <param name="iFormatID"></param>
		/// <param name="xPersonID"></param>
		/// <returns></returns>
		internal static string GetPersonOfficeAddress(int iFormatID, string xPersonID)
		{
			Trace.WriteNameValuePairs("iFormatID", iFormatID,
				"xPersonID", xPersonID);

			//get address format token string
			string xFormat = GetAddressTokenString(iFormatID);
            string xDelimReplacement = GetAddressDelimiterReplacement(iFormatID);

			//get address
			return GetPerson(xPersonID).GetOffice().GetAddress().ToString(xFormat, xDelimReplacement);
		}

		/// <summary>
		/// returns Person with specified id, using existing object if it matches
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		private static LMP.Data.Person GetPerson(string xID)
		{
			//use existing object if possible
			bool bDo = false;
			if (m_oPerson == null)
				bDo = true;
			else if (m_oPerson.ID != xID)
				bDo = true;

			if (bDo)
			{
				//retrieve specified person
				LocalPersons oPersons = new LocalPersons();
				m_oPerson = oPersons.ItemFromID(xID);
			}

			return m_oPerson;
		}

		private static LMP.Data.Office GetOffice(int iID)
		{
			//use existing object if possible
			bool bDo = false;
			if (m_oOffice == null)
				bDo = true;
			else if (m_oOffice.ID != iID)
				bDo = true;

			if (bDo)
			{
				//retrieve specified office
				LMP.Data.Offices oOffices = new LMP.Data.Offices();
				m_oOffice = (LMP.Data.Office) oOffices.ItemFromID(iID);
			}

			return m_oOffice;
		}

        /// <summary>
        /// returns the ID of the location of the
        /// specified type with the specified name
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="iParentID"></param>
        /// <param name="xLocationName"></param>
        /// <returns></returns>
        private static string GetLocationID(mpLocationTypes iType, int iParentID, string xLocationName)
        {
            string xSQL = "";

            if (string.IsNullOrEmpty(xLocationName))
                return "0";

            switch (iType)
            {
                case mpLocationTypes.Counties:
                    {
                        xSQL = "SELECT ID FROM Counties WHERE StateID =" + 
                            iParentID.ToString() + " AND Name='" + xLocationName + "'";
                        break;
                    }
                case mpLocationTypes.Countries:
                    {
                        xSQL = "SELECT ID FROM Countries WHERE Name='" + xLocationName + 
                            "' OR ISONum='" + xLocationName + "'";
                        break;
                    }
                case mpLocationTypes.States:
                    {
                        xSQL = "SELECT ID FROM States WHERE CountryID =" +
                           iParentID.ToString() + " AND Name='" + xLocationName + "'";
                        break;
                    }
            }

            return LMP.Data.SimpleDataCollection.GetScalar(xSQL).ToString();
        }
        /// <summary>
        /// returns token string for address format with specified id,
        /// using existing address format object if it matches
        /// </summary>
        /// <param name="iFormatID"></param>
        /// <returns></returns>
        private static string GetAddressTokenString(int iFormatID)
        {
            //use existing object if possible
            bool bDo = false;
            if (m_oAddressFormat == null)
                bDo = true;
            else if (m_oAddressFormat.ID != iFormatID)
                bDo = true;

            if (bDo)
            {
                //retrieve address format
                AddressFormats oFormats = new AddressFormats();
                m_oAddressFormat = (AddressFormat)oFormats.ItemFromID(iFormatID);
            }

            return m_oAddressFormat.AddressTokenString;
        }

        /// <summary>
        /// returns token string for address format with specified id,
        /// using existing address format object if it matches
        /// </summary>
        /// <param name="iFormatID"></param>
        /// <returns></returns>
        private static string GetAddressDelimiterReplacement(int iFormatID)
        {
            //use existing object if possible
            bool bDo = false;
            if (m_oAddressFormat == null)
                bDo = true;
            else if (m_oAddressFormat.ID != iFormatID)
                bDo = true;

            if (bDo)
            {
                //retrieve address format
                AddressFormats oFormats = new AddressFormats();
                m_oAddressFormat = (AddressFormat)oFormats.ItemFromID(iFormatID);
            }

            return m_oAddressFormat.DelimiterReplacement;
        }

		/// <summary>
		/// returns specified user application preference
		/// </summary>
        /// <param name="xPropertyName"></param>
        /// <param name="iCulture"></param>
        /// <returns></returns>
		internal static string GetAppPreferenceValue(string xPropertyName, int iCulture)
		{
			//use existing object if possible
			string xEntityID = LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);
			bool bDo = false;
			if (m_oAppPrefs == null)
				bDo = true;
			else if (m_oAppPrefs.EntityID != xEntityID || m_oAppPrefs.Culture != iCulture)
				bDo = true;

			if (bDo)
			{
				try
				{
					//retrieve user application preferences
                    m_oAppPrefs = KeySet.GetPreferenceSet(mpPreferenceSetTypes.UserApp,
                        "", xEntityID, 0, iCulture);
                }
				catch{}
				if(m_oAppPrefs == null)
					return "null";
			}

			string xValue = null;
			try{xValue = m_oAppPrefs.GetValue(xPropertyName);}
			catch{}

            if (xValue != null)
                return xValue;
            else
                return "null";
		}

		private static void SubscribeToEvents()
		{
			LMP.Data.KeySet.KeySetChanged += new LMP.Data
				.KeySetChangeHandler(KeySetChanged);
			LMP.Data.LocalPersons.PrivatePersonChanged += new LMP.Data
				.PersonChangeHandler(PrivatePersonChanged);
		}

		/// <summary>
		/// updates static preference sets
		/// </summary>
		/// <param name="oKS"></param>
		/// <param name="e"></param>
		private static void KeySetChanged(object oKS, LMP.Data.KeySetChangeEventArgs e)
		{
			switch (e.KeySetObject.KeySetType)
			{
				case mpKeySetTypes.UserAppPref:
					m_oAppPrefs = e.KeySetObject;
					break;
				case mpKeySetTypes.UserObjectPref:
					m_oObjectPrefs = e.KeySetObject;
					break;
				case mpKeySetTypes.UserTypePref:
					m_oTypePrefs = e.KeySetObject;
					break;
				case mpKeySetTypes.AuthorPref:
					m_oLeadAuthorPrefs = e.KeySetObject;
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// updates static private person following user edit
		/// </summary>
		/// <param name="oPersons"></param>
		/// <param name="e"></param>
		private static void PrivatePersonChanged(object oPersons,
			LMP.Data.PersonChangeEventArgs e)
		{
			if (m_oPerson != null && (e.PersonObject.ID == m_oPerson.ID))
				m_oPerson = e.PersonObject;
		}

		/// <summary>
		/// returns the specified property value of the author with the specified index
		/// </summary>
		/// <param name="oAuthors"></param>
		/// <param name="xPropertyName"></param>
		/// <param name="shIndex"></param>
		/// <returns></returns>
		internal static string GetAuthorValue(Authors oAuthors, string xPropertyName, short shIndex)
		{
			Author oAuthor = null;

			Trace.WriteNameValuePairs("xPropertyName", xPropertyName, "shIndex", shIndex);

			//get field code value for author with specified index
			try
			{
				oAuthor = oAuthors.ItemFromIndex(shIndex);
			}
			catch(System.ArgumentOutOfRangeException)
			{
				//author at specified index does not exist -
                //return empty string
                return "";
			}

            if (oAuthor != null)
            {
                //there is an author with this
                //index in the collection of authors
                string xPropValue = "";
                switch (xPropertyName.ToUpper())
                {
                    case "FULLNAME":
                        xPropValue = oAuthor.FullName;
                        break;
                    case "BARID":
                        xPropValue = oAuthor.BarID;
                        break;
                    case "ID":
                        xPropValue = oAuthor.ID;
                        break;
                    case "ISMANUAL":
                        if (oAuthor.PersonObject == null)
                            xPropValue = "True";
                        else
                            xPropValue = "False";
                        break;
                    default:
                        //For other than defined Author properties, use Person object
                        if (oAuthor.PersonObject != null)
                            xPropValue = oAuthor.PersonObject.GetPropertyValue(xPropertyName).ToString();
                        else
                            return "";
                        break;
                }
                //return value matching field case
                if (xPropertyName.ToUpper() == xPropertyName)
                    return xPropValue.ToUpper();
                else if (xPropertyName.ToLower() == xPropertyName)
                    return xPropValue.ToLower();
                else
                    return xPropValue;
            }
            else
                return "";
		}


		/// <summary>
		/// returns the specified office value of the specified author
		/// </summary>
		/// <param name="oAuthors"></param>
		/// <param name="xPropertyName"></param>
		/// <param name="shIndex"></param>
		/// <returns></returns>
		internal static string GetAuthorOfficeValue(Authors oAuthors, string xPropertyName, short shIndex)
		{
			Author oAuthor = null;

			Trace.WriteNameValuePairs("xPropertyName", xPropertyName, "shIndex", shIndex);

			//get field code value for author with specified index
			try
			{
				oAuthor = oAuthors.ItemFromIndex(shIndex);
			}
			catch(System.ArgumentOutOfRangeException oE)
			{
				//author at specified index does not exist
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
					shIndex.ToString(),oE);
			}

            if (oAuthor != null)
            {
                //there is an author with this
                //index in the collection of authors
                string xPropValue = "";
                if (oAuthor.PersonObject != null)
                    xPropValue = oAuthor.PersonObject.GetOffice().GetProperty(xPropertyName).ToString();
                else
                    //For manual author, return current user's office information
                    xPropValue = LMP.Data.Application.User.PersonObject.GetOffice().GetProperty(xPropertyName).ToString();
                //Return value matching Field case
                if (xPropertyName.ToUpper() == xPropertyName)
                    return xPropValue.ToUpper();
                else if (xPropertyName.ToLower() == xPropertyName)
                    return xPropValue.ToLower();
                else
                    return xPropValue;
            }
            else
                return "";
		}

		/// <summary>
		/// returns the specified author's office address in the specified address format
		/// </summary>
		/// <param name="oAuthors">authors collection</param>
		/// <param name="iAddressFormat">address format</param>
		/// <param name="shIndex">index of author in the collection</param>
		/// <returns></returns>
		internal static string GetAuthorOfficeAddress(Authors oAuthors, int iAddressFormat, short shIndex)
		{
			Author oAuthor = null;

			Trace.WriteNameValuePairs("iAddressFormat", iAddressFormat, "shIndex", shIndex);

			//get field code value for author with specified index
			try
			{
				oAuthor = oAuthors.ItemFromIndex(shIndex);
			}
			catch(System.ArgumentOutOfRangeException oE)
			{
				//author at specified index does not exist
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
					shIndex.ToString(),oE);
			}

            if (oAuthor != null)
            {
                //there is an author with this
                //index in the collection of authors
                if (oAuthor.PersonObject != null)
                    return oAuthor.PersonObject.GetOffice().GetAddress()
                        .ToString(GetAddressTokenString(iAddressFormat),
                        GetAddressDelimiterReplacement(iAddressFormat));
                else
                    //For Manual author - return Current User's Office information
                    return LMP.Data.Application.User.PersonObject.GetOffice().GetAddress()
                        .ToString(GetAddressTokenString(iAddressFormat),
                        GetAddressDelimiterReplacement(iAddressFormat));
            }
            else
                return "";
		}

		/// <summary>
		/// returns the specified value of the lead author
		/// </summary>
		/// <param name="oAuthors"></param>
		/// <param name="xPropertyName">property of lead author whose value should be returned</param>
		/// <returns></returns>
		internal static string GetLeadAuthorValue(Authors oAuthors, string xPropertyName)
		{
            switch (xPropertyName.ToUpper())
            {
                //GLOG 3634: FullName, ID and BarID supported for Manual Authors
                case "FULLNAME":
                    return oAuthors.GetLeadAuthor().FullName;
                case "ID":
                    return oAuthors.GetLeadAuthor().ID;
                case "BARID":
                    //GLOG 3048: Support BarID property for LeadAuthor
                    return oAuthors.GetLeadAuthor().BarID;
                case "ISMANUAL":
                    if (oAuthors.GetLeadAuthor().PersonObject == null)
                        return "True";
                    else
                        return "False";
                default:
                    if (oAuthors != null && oAuthors.Count > 0 && oAuthors.GetLeadAuthor().PersonObject != null)
                        return oAuthors.GetLeadAuthor().PersonObject.GetPropertyValue(xPropertyName).ToString();
                    else
                        return "";
            }
		}

		/// <summary>
		/// returns the specified value of the lead author's office
		/// </summary>
		/// <param name="oAuthors"></param>
		/// <param name="xPropertyName"></param>
		/// <returns></returns>
		internal static string GetLeadAuthorOfficeValue(Authors oAuthors, string xPropertyName)
		{

            if (oAuthors != null && oAuthors.Count > 0)
            {
                if (oAuthors.GetLeadAuthor().PersonObject != null)
                    return oAuthors.GetLeadAuthor().PersonObject.GetOffice().GetProperty(xPropertyName).ToString();
                else
                    return LMP.Data.Application.User.PersonObject.GetOffice().GetProperty(xPropertyName).ToString();
            }
            else
                return "";
		}

		/// <summary>
		/// returns the lead author's office address in the specified address format
		/// </summary>
		/// <param name="oAuthors"></param>
		/// <param name="iAddressFormat"></param>
		/// <returns></returns>
        internal static string GetLeadAuthorOfficeAddress(Authors oAuthors, int iAddressFormat)
        {
            Trace.WriteNameValuePairs("iAddressFormat", iAddressFormat);

            if (oAuthors != null && oAuthors.Count > 0)
            {
                try
                {
                    if (oAuthors.GetLeadAuthor().PersonObject != null)
                        return oAuthors.GetLeadAuthor().PersonObject.GetOffice().GetAddress()
                            .ToString(GetAddressTokenString(iAddressFormat),
                            GetAddressDelimiterReplacement(iAddressFormat));
                    else
                        //For manual author, return current user's office information
                        return LMP.Data.Application.User.PersonObject.GetOffice().GetAddress()
                            .ToString(GetAddressTokenString(iAddressFormat),
                            GetAddressDelimiterReplacement(iAddressFormat));
                }
                catch
                {
                    //GLOG 2952 - Return empty string if non-existent Address format
                    return "";
                }
            }
            else
                return "";
        }

        /// <summary>
        /// returns the specified author preference value for the lead author
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <param name="oObjectTypeDef"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        internal static string GetLeadAuthorPreference(Authors oAuthors,
            string xSegmentID, string xPropertyName)
        {
            //we will cache the last used author
            //preference keyset to improve performance

            Author oLeadAuthor = oAuthors.GetLeadAuthor();

            if (oLeadAuthor == null)
                //no lead author - author is required - alert
                throw new LMP.Exceptions.AuthorsRequiredException(
                    LMP.Resources.GetLangString("Error_FieldCodeRequiresAuthor") +
                        "LeadAuthorPreference_" + xPropertyName);

            Trace.WriteNameValuePairs("xSegmentID",
                xSegmentID, "xPropertyName", xPropertyName);

            //Manual Authors don't have preferences
            if (oLeadAuthor.IsManualAuthor)
                return "null";

            int iCulture = oAuthors.Segment.Culture;

            if ((m_oLeadAuthorPrefs == null) || xSegmentID != m_oLeadAuthorPrefs.Scope ||
                oLeadAuthor.ID != m_oLeadAuthorPrefs.EntityID ||
                iCulture != m_oLeadAuthorPrefs.Culture)
            {
                //either there is no cached author pref keyset or requested 
                //keyset is not the one that we've cached - get new one
                try
                {
                    m_oLeadAuthorPrefs = new KeySet(mpPreferenceSetTypes.Author,
                        xSegmentID, oLeadAuthor.ID, 0, iCulture);
                }
                catch { }
            }

            //get preference
            string xValue = null;
            if (m_oLeadAuthorPrefs != null)
            {
                try
                {
                    xValue = m_oLeadAuthorPrefs.GetValue(xPropertyName);
                }
                catch { }
            }
            else
            {
                //no keyset found - alert that there is no firm keyset
                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                    .GetLangString("Error_InvalidFirmRecord") + oAuthors.Segment.DisplayName);
            }

            if (xValue != null)
                return xValue;
            else
                return "null";
        }

        /// <summary>
        /// gets the specified preference value for the specified author
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <param name="xSegmentID"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="shIndex"></param>
        /// <returns></returns>
        internal static string GetAuthorPreferenceValue(Authors oAuthors,
            string xSegmentID, string xPropertyName, short shIndex)
        {
            //returns the specified author preference for the specified author-
            //returns empty value if collection of authors does not have a member with that index

            Author oAuthor = null;

            Trace.WriteNameValuePairs("xSegmentID", xSegmentID,
                "xPropertyName", xPropertyName, "shIndex", shIndex);

            //get field code value for author with specified index
            try
            {
                oAuthor = oAuthors.ItemFromIndex(shIndex);
            }
            catch (System.ArgumentOutOfRangeException oE)
            {
                //author at specified index does not exist
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") +
                    shIndex.ToString(), oE);
            }

            string xValue = null;
            if (oAuthor != null && oAuthor.PersonObject != null)
            {
                //there is an author with this
                //index in the collection of authors
                LMP.Data.KeySet oPrefs = null;
                try
                {
                    oPrefs = new KeySet(mpPreferenceSetTypes.Author,
                        xSegmentID, oAuthor.ID, 0, oAuthors.Segment.Culture);
                }
                catch { }

                if (oPrefs != null)
                {
                    try
                    {
                        xValue = oPrefs.GetValue(xPropertyName);
                    }
                    catch { }
                }
                else
                {
                    //no keyset found - alert that there is no firm keyset
                    throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                        .GetLangString("Error_InvalidFirmRecord") + oAuthors.Segment.DisplayName);
                }
            }

            if (xValue != null)
                return xValue;
            else
                return "null";
        }

		/// <summary>
		/// returns specified user type preference
		/// </summary>
		/// <param name="oSegment"></param>
		/// <param name="xPropertyName"></param>
		/// <returns></returns>
		internal static string GetTypePreferenceValue(Segment oSegment, string xPropertyName)
		{
            LMP.Data.mpObjectTypes oType = oSegment.TypeID;
			int iTypeID = (int) oType;
			//use existing object if possible
			string xEntityID = LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);
            int iCulture = oSegment.Culture;
			bool bDo = false;
			if (m_oTypePrefs == null)
				bDo = true;
			else if ((m_oTypePrefs.EntityID != xEntityID) || 
				(m_oTypePrefs.Scope != iTypeID.ToString()) ||
                (m_oTypePrefs.Culture != iCulture))
				bDo = true;

			if (bDo)
			{
				//retrieve user type preferences - return empty string if no keyset
				try
				{
					m_oTypePrefs = KeySet.GetPreferenceSet(mpPreferenceSetTypes.UserType,
						iTypeID.ToString(), xEntityID, 0, iCulture);
				}
				catch{}

				if(m_oTypePrefs == null)
					return "null";
			}
	
			string xValue = null;
			try{xValue = m_oTypePrefs.GetValue(xPropertyName);}
			catch{}

            if (xValue != null)
                return xValue;
            else
                return "null";
		}

        /// <summary>
        /// returns specified user object preference
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        internal static string GetObjectPreferenceValue(Segment oSegment,
            string xPropertyName)
        {
            //use existing object if possible
            string xSegmentID = oSegment.ID1.ToString();
            int iCulture = oSegment.Culture;
            string xEntityID = LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);
            bool bDo = false;
            if (m_oObjectPrefs == null)
                bDo = true;
            else if ((m_oObjectPrefs.EntityID != xEntityID) ||
                    (m_oObjectPrefs.Scope != xSegmentID) ||
                    (m_oObjectPrefs.Culture != iCulture))
                bDo = true;

            if (bDo)
            {
                //retrieve user type preferences - return empty if none set
                try
                {
                    m_oObjectPrefs = KeySet.GetPreferenceSet(mpPreferenceSetTypes.UserObject,
                        xSegmentID, xEntityID, 0, iCulture);
                }
                catch { }

                if (m_oObjectPrefs == null)
                    return "null";
            }

            string xValue = null;

            try { xValue = m_oObjectPrefs.GetValue(xPropertyName); }
            catch { }

            if (xValue != null)
                return xValue;
            else
                return "null";
        }

		internal static string GetCourtValue(AdminSegment oSegment, string xPropertyName)
		{
			bool bDo = false;
			if (m_oCourt == null)
				bDo = true;
			else if ((m_oCourt.L0 != oSegment.L0) ||
				(m_oCourt.L1 != oSegment.L1) ||
				(m_oCourt.L2 != oSegment.L2) ||
				(m_oCourt.L3 != oSegment.L3) ||
				(m_oCourt.L4 != oSegment.L4))
				bDo = true;

			if (bDo)
			{
				//retrieve court
				LMP.Data.Courts oCourts = new Courts();
				m_oCourt = (Court) oCourts.ItemFromLevels(oSegment.L0,
					oSegment.L1, oSegment.L2, oSegment.L3,
					oSegment.L4);
			}

			return m_oCourt.GetPropertyValue(xPropertyName);
		}

		internal static string GetCourierValue(int iCourierID, string xPropertyName)
		{
			bool bDo = false;
			if (m_oCourier == null)
				bDo = true;
			else if (m_oCourier.ID != iCourierID)
				bDo = true;

			if (bDo)
			{
				//retrieve court
				Couriers oCouriers = new Couriers();
				m_oCourier = (Courier) oCouriers.ItemFromID(iCourierID);
			}

			return m_oCourier.GetPropertyValue(xPropertyName);
		}

		/// <summary>
		/// gets value from COM function -
		/// expects parameter in the following format: Server_Class_Function_Arguments
		/// </summary>
		/// <param name="xParams"></param>
		/// <returns></returns>
		private static string GetFunctionValue(string xParams)
		{
            //Replace fieldcode delimiter with single underscore
            xParams = xParams.Replace("__", "_");
			string[] vParams = xParams.Split('_');

			if (vParams.Length < 3)
			{
				//insufficient function declaration
				throw new LMP.Exceptions.COMObjectCreationException(
					LMP.Resources.GetLangString("Error_InvalidCOMProcedure" + xParams));
			}
			else if (vParams.Length == 3)
			{
				//function has no arguments
				object oMissing = System.Reflection.Missing.Value;

				//call COM function
                return LMP.Forte.MSWord.Application.GetFunctionValue(vParams[0], vParams[1], vParams[2], oMissing);
			}
			else
			{
				//get function arguments
				object[] vArgs = new object[vParams.Length - 3];
				for (int i=0; i<vArgs.Length; i++)
					vArgs[i] = vParams[i + 3];

				//call COM function
                return LMP.Forte.MSWord.Application.GetFunctionValue(vParams[0], vParams[1], vParams[2], vArgs);
			}
		}

        /// <summary>
        /// gets the id of the first child segment of the specified type
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="iTypeID"></param>
        /// <returns></returns>
        internal static string GetChildSegmentID(Segment oSegment, int iTypeID)
        {
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];
                if (oChild.TypeID == (mpObjectTypes)iTypeID)
                    return oChild.ID;
            }
            return "";
        }


        /// <summary>
        /// returns value string from specified return column, 
        /// from specified search col from specified list
        /// </summary>
        /// <param name="xListName"></param>
        /// <param name="xValue"></param>
        /// <param name="iSearchColumn"></param>
        /// <param name="iReturnColumn"></param>
        /// <returns></returns>
        internal static string LookupInList(string xListName, string xValue, int iSearchColumn, int iReturnColumn)
        {
            //get value set as a list
            Lists oLists = LMP.Data.Application.GetLists(false);

            List oList = (List)oLists.ItemFromName(xListName);

            if (oList == null)
                return "";
            //look up & match designated search col value and return return col value
            xValue = oList.Lookup(xValue, (byte)iSearchColumn, (byte)iReturnColumn);
            return xValue;
        }

        /// <summary>
        /// returns firmwide author preference for specified segment and property;
        /// if no firmwide preference, returns "null"
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        internal static string GetFirmWideAuthorPreference(Segment oSegment,
            string xPropertyName)
        {
            LMP.Data.KeySet oPrefs = null;
            string xValue = null;

            try
            {
                oPrefs = new KeySet(mpPreferenceSetTypes.Author, oSegment.ID1.ToString(),
                    LMP.Data.ForteConstants.mpFirmRecordID.ToString(), 0, oSegment.Culture);
            }
            catch { }

            if (oPrefs != null)
            {
                try
                {
                    xValue = oPrefs.GetValue(xPropertyName);
                }
                catch { }
            }
            else
            {
                //no keyset found - alert that there is no firm keyset
                throw new LMP.Exceptions.FieldCodeException(LMP.Resources
                    .GetLangString("Error_InvalidFirmRecord") + oSegment.DisplayName);
            }

            if (xValue != null)
                return xValue;
            else
                return "null";
        }
        internal static string ConvertToUSPSFormat(string xInput, string xRemove)
        {
            return ConvertToUSPSFormat(xInput, xRemove, true);
        }
        /// <summary>
        /// Returns string converted to USPS Format
        /// If input is XML, only Node text is converted
        /// </summary>
        /// <param name="xInput"></param>
        /// <param name="xChars"></param>
        /// <returns></returns>
        internal static string ConvertToUSPSFormat(string xInput, string xRemove, bool bCheckForXML)
        {
            string xText = "";
            string xXMLText = "";
            int iXMLStart = -1;
            int iXMLEnd = 0;

            //GLOG 4525: Don't text for XML if parameter is false
            if (bCheckForXML)
            {
                //Check for XML portion of string
                iXMLStart = xInput.IndexOf("<");
                iXMLEnd = xInput.LastIndexOf(">") + 1;
            }
            //Input is not XML
            if (iXMLStart == -1 || iXMLEnd == 0)
            {
                if (xRemove != null)
                    return String.GetUSPSFormat(xInput, xRemove.ToCharArray());
                else
                    return String.GetUSPSFormat(xInput);
            }
            else
            {
                xText = xInput.Substring(0, iXMLStart);
                xXMLText = xInput.Substring(iXMLStart, iXMLEnd - iXMLStart);
            }

            if (xXMLText != "")
            {
                //load specified XMLSource to XmlDocument
                System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
                oXML.LoadXml("<zzmpD>" + xXMLText + "</zzmpD>");

                //Get list of nodes
                System.Xml.XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes("*");

                foreach (System.Xml.XmlNode oNode in oNodeList)
                {
                    //Reformat only text nodes
                    foreach (System.Xml.XmlNode oChildNode in oNode.ChildNodes)
                    {
                        if (oChildNode.NodeType == XmlNodeType.Text)
                        {
                            //GLOG 4525:  Make sure any XML tokens are replaced before converting
                            string xValue = LMP.String.RestoreXMLChars(oChildNode.Value);
                            xValue = ConvertToUSPSFormat(xValue, xRemove, false);
                            //Replace reserved XML Characters again 
                            oChildNode.Value = LMP.String.ReplaceXMLChars(xValue);
                        }
                    }
                }
                xXMLText = oXML.DocumentElement.InnerXml;
            }
            xText = xText + xXMLText;
            if (iXMLEnd < xInput.Length - 1)
            {
                xText = xText + xInput.Substring(iXMLEnd + 1);
            }
            return xText;
        }
        /// <summary>
        /// Returns substring of detail XML text containing specified number of entities
        /// </summary>
        /// <param name="xXMLSource"></param>
        /// <param name="iCount"></param>
        /// <param name="iStart"></param>
        /// <returns></returns>
        internal static string ExtractXMLEntities(string xXMLSource, int iCount, int iStart)
        {
            System.Xml.XmlNodeList oNodeList = null;
            //Extract just XML portion of string
            int iXMLStart = xXMLSource.IndexOf("<");
            int iXMLEnd = xXMLSource.LastIndexOf(">") + 1;

            //Input is not XML
            if (iXMLStart == -1 || iXMLEnd == 0)
                return "";
            else
                xXMLSource = xXMLSource.Substring(iXMLStart, iXMLEnd - iXMLStart);

            //load specified XMLSource to XmlDocument
            System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
            oXML.LoadXml("<zzmpD>" + xXMLSource + "</zzmpD>");


            //If no name specified, use name of first XML node
            oNodeList = oXML.DocumentElement.SelectNodes("/zzmpD/*");

            string xRefTagName = "";
            if (oNodeList.Count > 0)
                xRefTagName = oNodeList.Item(0).Name;
            else
                return "";

            StringBuilder oSB = new StringBuilder();
            int iTrack = 0;
            foreach (System.Xml.XmlNode oNode in oNodeList)
            {
                //Append XML of each node within the specified range
                if (oNode.Name == xRefTagName)
                    //Repeating name indicates start of new entity
                    iTrack++;
                if (iTrack > (iStart - 1) + iCount)
                    break;
                else if (iTrack >= iStart)
                    oSB.Append(oNode.OuterXml);
            }
            return oSB.ToString();
        }

        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertReline action
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        internal static string GetRelineValue(Variable oVar)
        {
            //get InsertReline parameters
            string xParameters = "";
            for (int i = 0; i < oVar.VariableActions.Count; i++)
            {
                VariableAction oAction = oVar.VariableActions[i];
                if (oAction.Type == VariableActions.Types.InsertReline)
                {
                    xParameters = oAction.Parameters;
                    break;
                }
            }

            //exit if there's no InsertReline action or if the control isn't a RelineGrid
            if ((xParameters == "") || (oVar.ControlType != mpControlTypes.RelineGrid))
                return "";


            //get parameters for retrieving value
            Segment oSegment = oVar.Segment;
            ForteDocument oMPDocument = oSegment.ForteDocument;
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
            string xLabelText = Expression.Evaluate(aParams[0], oSegment, oMPDocument);
            string xStandardDelimiter = Expression.Evaluate(aParams[1], oSegment, oMPDocument);
            string xSpecialSepararator = Expression.Evaluate(aParams[2], oSegment, oMPDocument);
            string xControlProps = oVar.ControlProperties;
            int iPos = xControlProps.IndexOf("Style=") + 6;
            LMP.Forte.MSWord.mpRelineStyles iStyle = (LMP.Forte.MSWord.mpRelineStyles)System.Int32.Parse(
                xControlProps.Substring(iPos, 1));

            object oTag;
            if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                oTag = oVar.AssociatedContentControls[0];
            else
                oTag = oVar.AssociatedWordTags[0];

            //get value
            return LMP.Forte.MSWord.WordDoc.GetRelineValue(oTag,
                xLabelText, iStyle, xStandardDelimiter, xSpecialSepararator,
                oMPDocument.WordDocument);
        }
        //GLOG : 8374 : JSW
        /// <summary>
        /// returns an int representing the value of the duration between dates,
        ///  in days, months or years
        /// </summary>
        /// <param name="oDTStart"></param>
        /// <param name="oDTEnd"></param>
        /// <param name="xDateUnit"></param>
        /// <returns></returns>
        public static int GetDateDuration(DateTime oDTStart, DateTime oDTEnd, string xDateUnit)
        {
            try
            {

                int iYears;
                int iMonths;
                int iDays;

                iDays = (int)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Day, oDTStart, oDTEnd);
                iYears = (int)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Year, oDTStart, oDTEnd);
                iMonths = (int)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, oDTStart, oDTEnd);

                switch (xDateUnit)
                {
                    case ("Days"):
                        return iDays;
                    case ("Months"):
                        return iMonths;
                    case ("Years"):
                        return iYears;
                    default:
                        return 0;
                } 
            }
            catch 
            {

                return 0;
            }
        }
        #endregion
        #region *********************events*********************
        public event FieldCodeExternalEvaluationRequiredHandler FieldCodeExternalEvaluationRequired;
        #endregion
    }
}
