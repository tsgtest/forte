﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Sidebar : AdminSegment, LMP.Architect.Base.IDocumentStamp, ISingleInstanceSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //get existing segment of this type
                Segment oExistingSidebar = null;
                Segments oSegs = null;
                //GLOG 5394: If current Range is ane existing Sidebar, reset to start of Section,
                //otherwise Range will become invalid once existing Sidebar is deleted
                if (oLocation.StoryType == Microsoft.Office.Interop.Word.WdStoryType.wdTextFrameStory)
                {
                    oLocation = oLocation.Sections[1].Range;
                    object oStart = Word.WdCollapseDirection.wdCollapseStart;
                    oLocation.Collapse(ref oStart);
                }
                if (oParent == null)
                {
                    //get from location
                    oSegs = Segment.GetSegments(this.ForteDocument,
                        oLocation.Sections.First, this.Definition.TypeID, 1);
                    if (oSegs.Count == 1)
                    {
                        oExistingSidebar = oSegs[0];

                        //get parent of existing paper
                        oParent = oExistingSidebar.Parent;

                    }
                    if (oExistingSidebar == null)
                    {
                        //Look for existing Pleading Paper as parent
                        oSegs = Segment.GetSegments(this.ForteDocument,
                            oLocation.Sections.First, mpObjectTypes.PleadingPaper, 1);
                        if (oSegs.Count == 1)
                        {
                            oParent = oSegs[0];
                        }
                        else if (oSegs.Count == 0)
                        {
                            // GLOG 6519: Also allow Letterhead as parent
                            oSegs = Segment.GetSegments(this.ForteDocument,
                                oLocation.Sections.First, mpObjectTypes.Letterhead, 1);
                            if (oSegs.Count == 1)
                            {
                                oParent = oSegs[0];
                            }
                        }
                    }
                    //add parent id to object data of new paper
                    if (oParent != null)
                        Segment.AddParentIDToXML(ref xXML, oParent);
                }
                else
                {
                    //get from parent
                    for (int i = 0; i < oParent.Segments.Count; i++)
                    {
                        Segment oSegment = oParent.Segments[i];
                        if (oSegment.TypeID == this.Definition.TypeID)
                        {
                            oExistingSidebar = oSegment;
                            break;
                        }
                    }
                    if (oExistingSidebar == null)
                    {
                        //get from location
                        oSegs = Segment.GetSegments(this.ForteDocument,
                            oLocation.Sections.First, this.Definition.TypeID, 1);
                        if (oSegs.Count == 1)
                        {
                            oExistingSidebar = oSegs[0];

                            //get parent of existing paper
                            oParent = oExistingSidebar.Parent;
                        }
                        if (oExistingSidebar == null)
                        {
                            //Look for existing Pleading Paper as parent
                            oSegs = Segment.GetSegments(this.ForteDocument,
                                oLocation.Sections.First, mpObjectTypes.PleadingPaper, 1);
                            if (oSegs.Count == 1)
                            {
                                oParent = oSegs[0];
                            }
                            else if (oSegs.Count == 0)
                            {
                                // GLOG 6519: Also allow Letterhead as parent
                                oSegs = Segment.GetSegments(this.ForteDocument,
                                    oLocation.Sections.First, mpObjectTypes.Letterhead, 1);
                                if (oSegs.Count == 1)
                                {
                                    oParent = oSegs[0];
                                }
                            }
                        }
                        Segment.AddParentIDToXML(ref xXML, oParent);
                    }
                }
                //delete existing Sidebar
                if (oExistingSidebar != null)
                {
                    try
                    {
                        this.ForteDocument.DeleteSegment(oExistingSidebar);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
                    }
                }

                //GLOG 8541: Copy variables, since ObjectData may have been changed by adding Parent ID
                LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(
                    this.ForteDocument.WordDocument, xXML, true);

                //insert new Sidebar
                Segment.InsertXML(xXML, oLocation, LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Append,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, true,
                    iIntendedUse, this.TypeID, this.ForteDocument); //GLOG 6983
                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        public void DeleteExistingInstance(Word.Section oSection)
        {
        }
        public Segments GetExistingSegments(Word.Section oSection)
        {
            return null;
        }
    }
    public class DisclaimerSidebar : Sidebar
    {
    }
    public class LogoSidebar : Sidebar
    {
    }
    public class DisclaimerText : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
        LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType, Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, true,
                    iIntendedUse, mpObjectTypes.DisclaimerText, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
