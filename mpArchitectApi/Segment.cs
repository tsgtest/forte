using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;
using System.Collections;
using LMP.Controls;
using LMP.Architect.Base;
using System.Linq; //GLOG 7848
using TSG.CI; //GLOG : 8819 : ceh

namespace LMP.Architect.Api
{
	//TODO: create derivations of EventArgs for appropriate delegates below
	public delegate void AfterDefaultAuthorSetHandler(object sender, EventArgs e);
	public delegate void AfterDefaultValuesSetHandler(object sender, EventArgs e);
	public delegate void BeforeSegmentSetupHandler(object sender, EventArgs e);
	public delegate void SegmentGeneratedHandler(object sender, EventArgs e);
    public delegate void SegmentBeforeFinishHandler(object sender, EventArgs e); //GLOG 8512
    public delegate void AfterAuthorUpdatedHandler(object sender, EventArgs e);
    public delegate void SegmentDeletedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentAddedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void ContactsAddedHandler(object sender, EventArgs e);
    public delegate void AfterAllVariablesVisitedHandler(object sender, EventArgs e);
    public delegate void CIDialogReleasedHandler(object sender, EventArgs e);
    public delegate void BeforeSegmentReplacedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void PaperTrayRequestedHandler(object sender, PaperTrayEventArgs e);
    public delegate void ActivationStateChangedHandler(object sender, ActivationStateChangedEventArgs e);
    public delegate void BeforeCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);
    public delegate void AfterCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);

    public class PaperTrayEventArgs : System.EventArgs
    {
        public int PaperTrayID;
        public PaperTrayEventArgs(int PaperTrayID)
        {
            PaperTrayID = this.PaperTrayID;
        }
    }
	public class SegmentValidationChangeEventArgs
	{
		bool SegmentValid;
        bool SegmentComplete;

		public SegmentValidationChangeEventArgs(bool bSegmentValid, bool bSegmentComplete)
		{
			this.SegmentValid = bSegmentValid;
            this.SegmentComplete = bSegmentComplete;
		}
	}
    public class ActivationStateChangedEventArgs{
        public bool Activated;

        public ActivationStateChangedEventArgs(bool bActivated){
            this.Activated = bActivated;
        }
    }

    public class CurrentSegmentSwitchedEventArgs : EventArgs
    {
        public Segment OldSegment;
        public Segment NewSegment;

        public CurrentSegmentSwitchedEventArgs(Segment oOldSegment, Segment oNewSegment)
        {
            this.OldSegment = oOldSegment;
            this.NewSegment = oNewSegment;
        }
    }

    /// <summary>
    /// contains properties that define the arguments for the
    /// SegmentDeleted and SegmentAdded events
    /// </summary>
    public class SegmentEventArgs
    {
        private Segment m_oSeg;

        public SegmentEventArgs(Segment oSeg)
        {
            m_oSeg = oSeg;
        }

        public Segment Segment
        {
            get { return m_oSeg; }
            set { m_oSeg = value; }
        }
    }

    /// <summary>
	/// contains the members that define a MacPac segment
	/// </summary>
	abstract public class Segment : LMP.Architect.Base.Segment
	{
		#region *********************events*********************
		public event AfterDefaultAuthorSetHandler AfterDefaultAuthorSet;
		public event AfterDefaultValuesSetHandler AfterDefaultValuesSet;
		public event BeforeSegmentSetupHandler BeforeSetup;
		public event SegmentGeneratedHandler Generated;
        public event SegmentBeforeFinishHandler BeforeFinish; //GLOG 8512
		public event LMP.Architect.Base.ValidationChangedHandler ValidationChange;
        public event AfterAuthorUpdatedHandler AfterAuthorUpdated;
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        public event ContactsAddedHandler ContactsAdded;
        public event AfterAllVariablesVisitedHandler AfterAllVariablesVisited;
        public event CIDialogReleasedHandler CIDialogReleased;
        public event ActivationStateChangedHandler ActivationStateChanged;
        public static event PaperTrayRequestedHandler PaperTrayRequested;
        #endregion
		#region *********************fields*********************
		internal const byte MAX_VARIABLE_ACTIONS = 50;
		private Variables m_oVariables;
        private Blocks m_oBlocks;
		private SegmentActions m_oSegmentActions;
		private ForteDocument m_oMPDocument;
		private NodeStore m_oNodes;
		private Segments m_oChildSegments;
		private Segment m_oParentSegment;
        private Authors m_oAuthors;
        private Authors m_oPrevAuthors;
		private bool m_bSegmentPreviouslyValid;
		private bool m_bShowChooser;
		private int m_iMaxAuthors = 1;
        private mpObjectTypes m_iTypeID;
        private ciRetrieveData m_iRetrieveDataTo = 0;
        private ciRetrieveData m_iRetrieveDataFrom = 0;
        private ciRetrieveData m_iRetrieveDataCC = 0;
        private ciRetrieveData m_iRetrieveDataBCC = 0;

        private string m_xToLabel = "";
        private string m_xFromLabel = "";
        private string m_xCCLabel = "";
        private string m_xBCCLabel = "";
        private int m_iMax = 0;
        private int m_iToMax = -1;
        private int m_iFromMax = -1;
        private int m_iCCMax = -1;
        private int m_iBCCMax = -1;
        private int m_iOtherMax = -1;

        private object[,] m_aCustomEntities = null;
        private ciAlerts m_iCIAlerts = 0;
        //protected ISegmentDef m_oDef; //GLOG 8496
        private ICContacts m_oCIContacts = null;
        internal bool m_bSkipAuthorActions = false;
        internal bool m_bAllowUpdateForAuthor = true;
        private ControlActions m_oCourtChooserControlActions;
        private int m_iCulture = LMP.Data.Language.mpUSEnglishCultureID;
        private ControlActions m_oLanguageControlActions;
        private bool m_bIsPrimary = false;
        //GLOG 3689
        private string m_xInvalidPreferenceVariables = "";
        private int m_iNextObjectDatabaseID = 1;
        private bool m_bActivated = true;
        private Snapshot m_oSnapshot = null;
        private List<string> m_oMissingContactVariables = new List<string>();
        #endregion
		#region *********************constructors*********************
        internal Segment()
        {
            //setup authors-
            //create authors collection
            this.PreviousAuthors = new Authors(this);
            this.Authors = new Authors(this);
            m_oCourtChooserControlActions = new ControlActions(this);
            m_oLanguageControlActions = new ControlActions(this);
        }
        #endregion
		#region *********************properties*********************
        public bool Activated
        {
            get { return m_bActivated; }
            set
            {
                if (value)
                {
                    //5-30-11 (dm) - target the highest-level parent that's deactivated
                    Segment oTarget = this;
                    while ((oTarget.Parent != null) && (oTarget.Parent.Activated == false))
                        oTarget = oTarget.Parent;

                    //if (oTarget.Activatable)
                    //{
                    //    //no existing fixed value marker - present any subsequent changes
                    //    //for review - activate only if all tag values are still valid 
                    //    if (oTarget.ReviewData())
                    //    {
                    //        Activate(oTarget);
                    //        if (oTarget.ActivationStateChanged != null)
                    //        {
                    //            oTarget.ActivationStateChanged(oTarget,
                    //                new ActivationStateChangedEventArgs(true));
                    //        }
                    //    }
                    //}
                }
                else
                {
                    //GLOG 5540 (dm) - if transparent, target the nearest
                    //non-transparent ancestor
                    Segment oTarget = this;
                    while (oTarget.IsTransparent)
                        oTarget = oTarget.Parent;

                    //deactivate segment - create deactivation docvar
                    Deactivate(oTarget);
                    if (oTarget.ActivationStateChanged != null)
                    {
                        oTarget.ActivationStateChanged(oTarget,
                            new ActivationStateChangedEventArgs(false));
                    }
                }
            }
        }
        
        /// <summary>
        /// returns true iff the segment is finished
        /// </summary>
        public bool IsFinished
        {
            get
            {
                bool bHasVariables = this.HasVariables(false);
                bool bHasBlocks = this.HasBlocks(false);
                bool bSnapshotExists = Snapshot.Exists(this);

                return (!bHasVariables && !bHasBlocks && bSnapshotExists);
            }
        }
        /// <summary>
        /// returns true iff the segment can be activated
        /// </summary>
        /// <returns></returns>
        public bool Activatable
        {
            get
            {
                //a non-activatable child prevents activation of parent
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (!this.Segments[i].Activatable)
                        return false;
                }

                //get deactivation doc var
                object oName = "Deactivation_" + this.TagID;
                string xVarValue = null;
                Word.Variable oVar = null;
                try
                {
                    oVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);
                    xVarValue = oVar.Value;
                }
                catch { }

                //activatable if deactivation doc var doesn't contain a fixed value
                //marker, i.e. previous user confirmation of a bad tag value
                return ((xVarValue == null) ||
                    (LMP.String.Decrypt(xVarValue).IndexOf(
                    ForteConstants.mpFixedValueMarker) == -1));
            }
        }
        //returns the value snapshot
        //taken on Finish
        public Snapshot Snapshot
        {
            get
            {
                try
                {
                    if (m_oSnapshot == null)
                        m_oSnapshot = new Snapshot(this);

                    return m_oSnapshot;
                }
                catch
                {
                    return null;
                }
            }
        }
        /// <summary>
		/// returns the variables of this segment
		/// </summary>
		public Variables Variables
		{
			get
			{
				if(m_oVariables == null)
				{
					m_oVariables = new Variables(this);

					//subscribe to notifications that variables have been validated
					m_oVariables.ValidationChanged += new LMP.Architect.Base.ValidationChangedHandler(
						OnVariablesValidationChanged);
                    m_oVariables.BeforeContactsUpdateEvent += new BeforeContactsUpdateHandler(m_oVariables_BeforeContactsUpdateEvent);
                    m_oVariables.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
				}
				return m_oVariables;
			}
            private set {this.m_oVariables = value;}
		}
        /// <summary>
        /// returns true iff the segment has a body block
        /// </summary>
        /// <returns></returns>
        public bool HasBody()
        {
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                if (this.Blocks[i].IsBody)
                {
                    return true;
                }
            }

            return false;
        }
        internal List<string> MissingContactVariables
        {
            get { return m_oMissingContactVariables; }
            set { m_oMissingContactVariables = value; }
        }
        /// <summary>
        /// returns a collection of either basic or advanced variables
        /// </summary>
        /// <param name="oLevel"></param>
        /// <returns></returns>
        public System.Collections.Generic.List<Variable> GetVariablesSubset(Variable.Levels oDisplayLevel)
        {
            System.Collections.Generic.List<Variable> oList = new System.Collections.Generic.List<Variable>();

            for (int i = 0; i < this.Variables.Count; i++)
            {
                Variable oVar = this.Variables[i];
                if (oVar.DisplayLevel == oDisplayLevel)
                    oList.Add(oVar);
            }
            return oList;
        }
        /// <summary>
        /// returns a collection of all variables configured to display in the Wizard
        /// </summary>
        /// <param name="oLevel"></param>
        /// <returns></returns>
        public System.Collections.Generic.List<Variable> GetWizardVariables(bool bIncludeChildren)
        {
            System.Collections.Generic.List<Variable> oList = new System.Collections.Generic.List<Variable>();

            for (int i = 0; i < this.Variables.Count; i++)
            {
                Variable oVar = this.Variables[i];
                if ((oVar.DisplayIn & Variable.ControlHosts.Wizard) == Variable.ControlHosts.Wizard)
                    oList.Add(oVar);
            }
            if (bIncludeChildren)
            {
                // Also include variables from child segments
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].DisplayWizard)
                    {
                        System.Collections.Generic.List<Variable> oChildVars = this.Segments[i].GetWizardVariables(bIncludeChildren);
                        for (int c = 0; c < oChildVars.Count ; c++)
                        {
                            oList.Add(oChildVars[c]);
                        }
                    }
                }
            }

            return oList;
        }
        /// <summary>
        /// returns the blocks of this segment
        /// </summary>
        public Blocks Blocks
        {
            get
            {
                if (m_oBlocks == null)
                    m_oBlocks = new Blocks(this);
                return m_oBlocks;
            }
            private set { this.m_oBlocks = value; }
        }
        /// <summary>
		/// returns the actions of this segment
		/// </summary>
		public SegmentActions Actions
		{
			get
			{
				if(m_oSegmentActions == null)
					m_oSegmentActions = new SegmentActions(this);

				return m_oSegmentActions;
			}
		}
		/// <summary>
		/// returns the parent segment of the this segment
		/// </summary>
		public Segment Parent
		{
			get
			{
				if(m_oParentSegment == null)
				{
					//TODO: get parent segment from document
					string xTagID = this.FullTagID;
					int iPos1 = xTagID.LastIndexOf(".");

					string xParentTagID = "";

					if(iPos1 > -1)
					{
						//parent tag precedes the '.'
						xParentTagID = xTagID.Substring(0, iPos1);

						//find parent in segments collection
						m_oParentSegment = Segment.FindSegment(xParentTagID, this.ForteDocument.Segments);
					}
					else
						m_oParentSegment = null;
				}
				
				return m_oParentSegment;
			}
            protected set { m_oParentSegment = value; }
		}
        public string ParentVariableID
        {
            get 
            {
                return this.Nodes.GetItemAssociatedParentVariable(this.FullTagID);
            }
            set 
            {
                this.Nodes.SetItemAssociatedParentVariable(this.FullTagID, value);
            }
        }
        /// <summary>
        /// Parses path to child segment in form x.y.z and returns corresponding Segment object
        /// </summary>
        /// <param name="xPathToSegment"></param>
        /// <returns></returns>
        public Segment GetChildSegmentFromPath(string xPathToSegment)
        {
            Segment oTargetSegment = null;

            // Parse full path into separate elements
            string[] xSegments = xPathToSegment.Split('.');
            Segment oParentSegment = this;
            for (int i = 0; i <= xSegments.GetUpperBound(0); i++)
            {
                try
                {
                    oTargetSegment = null;
                    for (int j = 0; j < oParentSegment.Segments.Count; j++)
                    {
                        // Look for child segment with matching DisplayName
                        if (String.RemoveIllegalNameChars(oParentSegment.Segments.ItemFromIndex(j).DisplayName) ==  xSegments[i])
                        {
                            oTargetSegment = oParentSegment.Segments.ItemFromIndex(j);
                            break;
                        }
                    }
                    if (oTargetSegment == null)
                    {
                        return null;
                    }
                    else
                    {
                        oParentSegment = oTargetSegment;
                    }
                }
                catch
                {
                    // No matching Segment found
                    return null;
                }
            }
            return oTargetSegment;
        }
        /// <summary>
        /// Parses path to child segment in form x.y.z and returns corresponding Segment object
        /// </summary>
        /// <param name="xPathToSegment"></param>
        /// <returns></returns>
        public Segment GetChildSegmentFromFullTagID(string xFullTagID)
        {
            Segment oTargetSegment = null;

            // Parse full path into separate elements
            string[] xSegments = xFullTagID.Split('.');
            Segment oParentSegment = this;
            for (int i = 0; i <= xSegments.GetUpperBound(0); i++)
            {
                try
                {
                    if (xSegments[i] == oParentSegment.TagID)
                        continue;

                    oTargetSegment = null;
                    for (int j = 0; j < oParentSegment.Segments.Count; j++)
                    {
                        // Look for child segment with matching DisplayName
                        if (String.RemoveIllegalNameChars(oParentSegment.Segments.ItemFromIndex(j).TagID) == xSegments[i])
                        {
                            oTargetSegment = oParentSegment.Segments.ItemFromIndex(j);
                            break;
                        }
                    }
                    if (oTargetSegment == null)
                    {
                        return null;
                    }
                    else
                    {
                        oParentSegment = oTargetSegment;
                    }
                }
                catch
                {
                    // No matching Segment found
                    return null;
                }
            }
            return oTargetSegment;
        }
        /// <summary>
        /// returns the child segments of this segment
        /// </summary>
        public Segments Segments
		{
			get
			{
				//get child segments if not already retrieved
                if (m_oChildSegments == null)
					GetChildSegments();

				return m_oChildSegments;
			}
            private set { m_oChildSegments = value; }
		}

        /// <summary>
		/// returns the collection of Nodes associated with this segment - read only
		/// </summary>
		public NodeStore Nodes
		{
			get
			{
				if(m_oNodes == null || m_oNodes.XML == "<Nodes></Nodes>")
				{
                    DateTime t0 = DateTime.Now;

					//get node store containing only those nodes for the segment
					m_oNodes = new NodeStore(this.m_oMPDocument.Tags, this.FullTagID,
						mpNodeStoreMatchTypes.Equals);
                    
					if(m_oNodes.Count == 0)
						//something is wrong - there needs to be
						//at least one mSEG tag in the segment xml
						throw new LMP.Exceptions.SegmentException(LMP.Resources
							.GetLangString("Error_NoNodesFoundForSegment") + this.FullTagID);

                    LMP.Benchmarks.Print(t0, this.FullTagID);
				}

				return m_oNodes;
			}
            private set {m_oNodes = value;}
		}
        /// <summary>
		/// returns the MacPac document containing this segment
		/// </summary>
		public ForteDocument ForteDocument
		{
			get{return this.m_oMPDocument;}
            protected set {m_oMPDocument = value;}
		}
		/// <summary>
		/// returns the Authors collection for this segment
		/// </summary>
		public Authors Authors
		{
            get{return m_oAuthors;}
            set
            {
                if ((m_oAuthors == null && value != null) || (m_oAuthors.ToString() != value.ToString()))
                {
                    //authors have changed -
                    //set previous authors
                    if (m_oAuthors != null)
                        m_oPrevAuthors.SetAuthors(m_oAuthors);

                    //set new authors
                    m_oAuthors = value;
                }
            }
		}
        /// <summary>
        /// Stores XML of previous Authors collection
        /// </summary>
        public Authors PreviousAuthors
        {
            get { return m_oPrevAuthors; }
            set { m_oPrevAuthors = value; }
        }
		/// <summary>
		/// returns true if all child segments are valid
		/// and all variables are valid
		/// </summary>
		public bool IsValid
		{
			get
			{
                bool bRet = true;

                DateTime t0 = DateTime.Now;

                //segment can't be valid if the default values
                //have yet to be set
                if (!(this.CreationStatus == Status.Finished ||
                    this.CreationStatus == Status.VariableDefaultValuesSet))
                    return false;

				if(!this.Segments.AreValid)
					//at least one segment is invalid
					bRet = false;
                else if(!this.Variables.ValuesAreValid)
					//at least one variable is invalid
					bRet =  false;

                LMP.Benchmarks.Print(t0, this.Name);

				return bRet;
			}
		}
        /// <summary>
        /// gets the CI Contacts used by this segment
        /// </summary>
        public ICContacts CIContacts
        {
            get
            {
                return m_oCIContacts;
            }
        }

        public bool ContentIsMissing
        {
            get
            {
                if (this.Segments.ContentIsMissing)
                    return true;
                else if (LMP.Forte.MSWord.WordDoc.BookmarkIsMissing((object)this.Bookmarks))
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// returns true if 'Must Visit' variables of this segment
        /// and all child segments have been visited
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                DateTime t0 = DateTime.Now;

                //segment can't be valid if the default values
                //have yet to be set
                if (!(this.CreationStatus == Status.Finished ||
                    this.CreationStatus == Status.VariableDefaultValuesSet))
                    return false;

                if (!this.Segments.AllRequiredValuesVisited)
                    return false;

                if (!this.Variables.AllRequiredValuesVisited)
                    return false;

                LMP.Benchmarks.Print(t0, this.Name);
                return true;
            }
        }
        /// <summary>
        /// returns true iff this segment is an external child segment
        /// </summary>
        public bool IsExternalChild
        {
            get
            {
                if (this.Parent == null)
                    return false;
                else
                {
                    string xObjectData = null;

                    if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary && this.PrimaryWordTag != null)
                    {
                        //get primary Word tag of segment
                        Word.XMLNode oNode = this.PrimaryWordTag;

                        //search for existence of ParentTagID key in 
                        //the object data - existence of such a key indicates 
                        //that this segment is an external child
                        Word.XMLNode oObjectData = oNode.SelectSingleNode("@ObjectData", "", true);
                        xObjectData = oObjectData.Text;
                    }
                    else if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML && this.PrimaryContentControl != null)
                    {
                        xObjectData = LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(
                            this.PrimaryContentControl, "ObjectData");
                    }
                    else
                    {
                        //use bookmark
                        xObjectData = LMP.Forte.MSWord.WordDoc.GetAttributeValue_Bmk(
                            this.PrimaryBookmark, "ObjectData");
                    }

                    //GLOG 4441: Make sure ObjectData is decrypted if necessary
                    xObjectData = String.Decrypt(xObjectData);

                    //true only if ObjectData includes ParentTagID, but value is not empty
                    return xObjectData.Contains("ParentTagID=") && 
                        !xObjectData.Contains("ParentTagID=|") && 
                        !xObjectData.EndsWith("ParentTagID=");
                }
            }
        }
		/// <summary>
		/// returns true iff the segment is a top level segment
		/// </summary>
		public bool IsTopLevel
		{
			get{return this.Parent == null;}
		}
		/// <summary>
		/// sets/gets the maximum number of authors allowed for this segment
		/// </summary>
        /// <summary>
        /// returns the primary Word tag for the segment
        /// </summary>
        public Word.XMLNode PrimaryWordTag
        {
            get
            {
                Word.XMLNode oNode = null;

                try
                {
                    oNode = Nodes.GetWordTag(this.FullTagID, 1);
                }
                catch { }

                return oNode;
            }
        }
        /// <summary>
        /// returns the first Word tag for the segment -
        /// this is the tag recognized as the first one
        /// by Word - it might not be the Part 1 tag
        /// </summary>
        public Word.XMLNode FirstWordTag
        {
            get
            {
                //GLOG 6928 (dm) - segment won't have any tags in 10.5
                Word.XMLNode[] aTags = this.WordTags;

                if (aTags.Length > 0)
                {
                    return this.WordTags[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// returns the Word tags for the segment
        /// </summary>
        public Word.XMLNode[] WordTags
        {
            get
            {
                return Nodes.GetWordTags(this.FullTagID);
            }
        }

        /// <summary>
        /// returns the primary content control for the segment
        /// </summary>
        public Word.ContentControl PrimaryContentControl
        {
            get
            {
                Word.ContentControl oCtl = null;
                try
                {
                    oCtl = Nodes.GetContentControl(this.FullTagID, 1);
                }
                catch { }

                return oCtl;
            }
        }

        /// <summary>
        /// returns the primary content control for the segment
        /// </summary>
        public Word.Bookmark PrimaryBookmark
        {
            get
            {
                Word.Bookmark oBmk = null;
                try
                {
                    oBmk = Nodes.GetBookmark(this.FullTagID, 1);
                }
                catch { }

                return oBmk;
            }
        }

        /// <summary>
        /// returns the range of this segment's primary word tag or content control
        /// </summary>
        public Word.Range PrimaryRange
        {
            get
            {
                Word.XMLNode oTag = this.PrimaryWordTag;

                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary && oTag != null)
                {
                    return oTag.Range;
                }

                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    Word.ContentControl oCC = this.PrimaryContentControl;
                    if (oCC != null)
                    {
                        return oCC.Range;
                    }
                }

                Word.Bookmark oBmk = this.PrimaryBookmark;

                if (oBmk != null)
                {
                    return oBmk.Range;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// returns the start range of the primary bounding object of the segment
        /// </summary>
        public Word.Range StartOfPrimaryRange
        {
            get
            {
                Word.Range oRng = this.PrimaryRange;
                object oDir = Word.WdCollapseDirection.wdCollapseStart;
                object oUnit = Word.WdUnits.wdCharacter;
                object oCount = 1;
                object oCountRev = -1;

                oRng.Collapse(ref oDir);

                if (m_oMPDocument.FileFormat == mpFileFormats.Binary)
                {
                    if (oRng.XMLParentNode != this.PrimaryWordTag)
                    {
                        //move one character forward, then back -
                        //this ensures that the range is inside the tag
                        oRng.Move(ref oUnit, ref oCount);
                        oRng.Move(ref oUnit, ref oCountRev);
                    }
                }
                else
                {
                    if (oRng.ParentContentControl != this.PrimaryContentControl)
                    {
                        //move one character forward, then back -
                        //this ensures that the range is inside the cc
                        oRng.Move(ref oUnit, ref oCount);
                        oRng.Move(ref oUnit, ref oCountRev);
                    }
                }

                return oRng;
            }
        }
        /// <summary>
        /// returns the first content control for the segment - 
        /// this is the tag recognized as the first one by Word -
        /// it might not be the Part 1 tag
        /// </summary>
        public Word.Bookmark FirstBookmark
        {
            get
            {
                return this.Bookmarks[0];
            }
        }

        /// <summary>
        /// returns the first content control for the segment -
        /// this is the tag recognized as the first one
        /// by Word - it might not be the Part 1 tag
        /// </summary>
        public Word.ContentControl FirstContentControl
        {
            get
            {
                Word.ContentControl[] aCCs = this.ContentControls;

                if (aCCs.Length > 0)
                {
                    return this.ContentControls[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public Word.Bookmark[] Bookmarks
        {
            get
            {
                return Nodes.GetBookmarks(this.FullTagID);
            }
        }

        /// <summary>
        /// returns the content controls for the segment
        /// </summary>
        public Word.ContentControl[] ContentControls
        {
            get
            {
                return Nodes.GetContentControls(this.FullTagID);
            }
        }

        /// <summary>
        /// if segment is top-level, returns true if there are no other top-level
        /// segments of the same object type - otherwise, returns true if there are
        /// no siblings of the same object type
        /// </summary>
        public bool IsOnlyInstanceOfType
        {
            get
            {
                //get target segments collection
                Segments oSegments = null;
                if (this.IsTopLevel)
                    oSegments = this.ForteDocument.Segments;
                else
                    oSegments = this.Parent.Segments;

                //look for segment of same type
                for (int i = 0; i < oSegments.Count; i++)
                {
                    Segment oSegment = oSegments[i];
                    if ((oSegment.TypeID == this.TypeID) &&
                        (oSegment.FullTagID != this.FullTagID))
                        return false;
                }

                //no segments of same type found
                return true;
            }
        }
        public string ChildSegmentIDs(bool bIncludeChildren)
        {
            if (this.IntendedUse == mpSegmentIntendedUses.AsStyleSheet ||
                this.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                return "";

            System.Collections.Generic.List<string> aList =
                this.ChildSegmentList(bIncludeChildren);
            string xTemp = "";
            //Return as delimited string
            foreach (string xItem in aList)
            {
                if (xItem != this.ID)
                    xTemp = xTemp + xItem + "|";
            }
            //clean up trailing delimiter
            xTemp = xTemp.TrimEnd('|');
            return xTemp;
        }

        public int Culture
        {
            get { return m_iCulture; }
            set
            {
                if (this.SupportedLanguages.Contains(value + "�") ||
                    this.SupportedLanguages.Contains("�" + value) ||
                    this.SupportedLanguages == value.ToString())
                {
                    m_iCulture = value;
                }
                else if (this is CollectionTable)
                {
                    //GLOG item #4219 - dcf -
                    //segment is a collection, and collections
                    //don't have supported languages - so just
                    //set the language to allow collection items
                    //to be set to the language of thier parent,
                    //i.e. the collection
                    m_iCulture = value;
                }
            }
        }
        public ControlActions CourtChooserControlActions
        {
            get { return m_oCourtChooserControlActions; }
        }
        public ControlActions LanguageControlActions
        {
            get { return m_oLanguageControlActions; }
        }
        /// <summary>
        /// gets/sets whether this is the primary item among siblings of this type
        /// for purposes of maintaining author linkage and position on the tree
        /// </summary>
        public bool IsPrimary
        {
            get { return m_bIsPrimary; }
            set
            {
                DateTime t0 = DateTime.Now;

                m_bIsPrimary = value;

                if (m_bIsPrimary)
                {

                    //ensure that all siblings are non-primary
                    Segments oSiblings = null;
                    if (!this.IsTopLevel)
                        oSiblings = this.Parent.Segments;
                    else
                        oSiblings = this.ForteDocument.Segments;
                    for (int i = 0; i < oSiblings.Count; i++)
                    {
                        Segment oSibling = oSiblings[i];
                        if ((oSibling.TypeID == this.TypeID) &&
                            (oSibling.FullTagID != this.FullTagID) && oSibling.IsPrimary)
                        {
                            oSibling.IsPrimary = false;
                            if (this.ForteDocument.Mode != ForteDocument.Modes.Design)
                            {
                                oSibling.Nodes.SetItemObjectDataValue(oSibling.FullTagID,
                                    "IsPrimary", "false");
                            }
                            if (oSibling.LinkAuthorsToParentDef ==
                                LinkAuthorsToParentOptions.FirstChildOfType)
                                oSibling.LinkAuthorsToParent = false;
                        }
                    }
                }

                //write runtime value to object data
                if (this.ForteDocument.Mode != ForteDocument.Modes.Design)
                {
                    this.Nodes.SetItemObjectDataValue(this.FullTagID, "IsPrimary",
                        this.IsPrimary.ToString().ToLower());
                }

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// Return true if Segment has any nodes in the Document Editor
        /// </summary>
        public bool HasVisibleUIElements
        {
            //GLOG 15907
            get
            {
                if (this.IsTopLevel && (this.MaxAuthors > 0 || this.SupportedLanguages.Contains(StringArray.mpEndOfValue) || this.ShowChooser || this.ShowCourtChooser == true))
                {
                    //return true if Author, Language, Court or Segment Chooser controls should be displayed
                    return true;
                }
                else
                {
                    //return true if there are any visible variables or blocks
                    for (int i = 0; i < this.Variables.Count; i++)
                    {
                        if ((this.Variables[i].DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                            return true;
                    }
                    for (int i = 0; i < this.Blocks.Count; i++)
                    {
                        if (this.Blocks[i].ShowInTree == true)
                            return true;
                    }
                    for (int i = 0; i < this.Segments.Count; i++)
                    {
                        if (!this.Segments[i].IsTransparent)
                            return true;
                    }
                }
                return false;
            }
        }
        #endregion
        #region *********************methods*********************
        public void ApplyPrefill(Prefill oPrefill)
        {
            //GLOG : 6987 : CEH
            //reset status to pre-Finished & force initialization of Prefill authors
            //GLOG 7826
            Segment.Status iStatus = this.CreationStatus;
            this.CreationStatus = Status.BoilerplateInserted;
            this.InitializeValues(oPrefill, false, true);
            //GLOG 7826: Restore original creation status if greater than current value
            if (iStatus > this.CreationStatus)
                this.CreationStatus = iStatus;
        }
        /// <summary>
        /// gets a pipe-delimited list of names
        /// of deactivated variables
        /// </summary>
        /// <returns></returns>
        public string GetMismatchedDeactivatedVariables()
        {
            return GetMismatchedDeactivatedVariables(true).TrimEnd(StringArray.mpEndOfRecord);
        }
        private string GetMismatchedDeactivatedVariables(bool bTopLevel)
        {
            StringBuilder oSB = new StringBuilder();

            //cycle through segment variables, getting mismatches
            for (int i = 0; i < this.Variables.Count; i++)
            {
                Variable oVar = this.Variables[i];

                //skip detail subcomponent variables (6/27/11, dm)
                if (oVar.IsDetailSubComponent)
                    continue;

                //get value at the time of deactivation
                string xDeactivatedValue = oVar.DeactivationValue;
                if (xDeactivatedValue == null)
                    xDeactivatedValue = "";

                //get value in document
                string xDocumentValue = oVar.GetValueFromSource();
                string xDisplayValue = Expression.Evaluate(oVar.DisplayValue, oVar.Segment, oVar.ForteDocument, true);

                //remmed out (6/28/11 dm)
                ////GLOG item #5554 - dcf -
                ////this is not an ideal solution, but seems to be the best we have -
                ////clearly if the value source expression contains the DateFormat
                ////field code, but does not start with it, the problem will resurface
                //if (oVar.ValueSourceExpression.StartsWith("[DateFormat_"))
                //{
                //    xDocumentValue = LMP.Architect.Base.Application
                //        .GetDateFormatPattern(xDocumentValue);
                //}

                if (xDeactivatedValue.StartsWith(LMP.Data.ForteConstants.mpFixedValueMarker))
                {
                    //deactivated value has been updated by user -
                    //this indicates that the tag value should never be used -
                    //therefore, we consider there to be no mismatch, as there
                    //is no doubt as to which value should be used
                    continue;
                }
                else if (xDocumentValue != xDeactivatedValue)
                {
                    oSB.AppendFormat("{0}" + StringArray.mpEndOfElement + "{1}" +
                        StringArray.mpEndOfElement + "{2}" + StringArray.mpEndOfElement + "{3}" +
                        StringArray.mpEndOfElement + "{4}" +  StringArray.mpEndOfRecord, 
                        oVar.Segment.TypeID, oVar.Segment.FullTagID,
                        oVar.Name, xDocumentValue, GetVariableUIValue(oVar, xDocumentValue));
                }
            }

            //cycle recursively through children, getting mismatches
            for (int i = 0; i < this.Segments.Count; i++)
            {
                //GLOG 5532 (dm) - compare only if child is deactivated
                if (!this.Segments[i].Activated)
                {
                    oSB.AppendFormat("{0}", this.Segments[i]
                        .GetMismatchedDeactivatedVariables(false));
                }
            }

            return oSB.ToString();
        }

        private string GetMismatchedDeactivatedVariable(string xVarName)
        {
            Variable oVar = this.Variables.ItemFromName(xVarName);

            //get value at the time of deactivation
            string xDeactivatedValue = oVar.DeactivationValue;
            if (xDeactivatedValue == null)
                xDeactivatedValue = "";

            if (xDeactivatedValue.StartsWith(LMP.Data.ForteConstants.mpFixedValueMarker))
            {
                return null;
            }

            //get value in document
            string xDocumentValue = oVar.GetValueFromSource();

            if (xDeactivatedValue.StartsWith(LMP.Data.ForteConstants.mpFixedValueMarker))
            {
                //deactivated value has been updated by user -
                //this indicates that the tag value should never be used -
                //therefore, we consider there to be no mismatch, as there
                //is no doubt as to which value should be used
                return null;
            }
            else if (xDocumentValue != xDeactivatedValue)
            {
                return string.Format("{0}" + StringArray.mpEndOfElement + "{1}" +
                    StringArray.mpEndOfElement + "{2}" + StringArray.mpEndOfElement + "{3}",
                    oVar.Segment.TypeID, oVar.Segment.FullTagID,
                    oVar.Name, xDocumentValue);
            }

            return null;
        }
        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue(Variable oVar, string xDocumentValue)
        {
            DateTime t0 = DateTime.Now;

            string m_xEmptyDisplayValue = LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(oVar.DisplayValue))
            {
                xValue = xDocumentValue;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = Expression.MarkLiteralReservedCharacters(xValue);
                xValue = Expression.Evaluate(xValue, oVar.Segment, oVar.ForteDocument);

                if (oVar.ControlType == mpControlTypes.Checkbox)
                {
                    if (!oVar.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = (BooleanComboBox)oVar.AssociatedControl;
                        if (oBC == null)
                        {
                            DateTime t1 = DateTime.Now;
                            oBC = (BooleanComboBox)oVar.CreateAssociatedControl(null);
                            LMP.Benchmarks.Print(t1, "Create Combobox");
                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)oVar.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)oVar.CreateAssociatedControl(null);
                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                else if (oVar.ControlType == mpControlTypes.PaperTraySelector)
                {
                    //GLOG 4752: Access Printer bin information via Base wrapper functions
                    if (!string.IsNullOrEmpty(oVar.Value))
                        xValue = LMP.Forte.MSWord.Application.GetPrinterBinName(short.Parse(oVar.Value));
                }

                if (oVar.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue(oVar, xDocumentValue);

                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                            oVar.AssociatedWordTags[0].Range.Fields.Count > 0)
                            xValue = xValue + " (field code)";
                    }
                    else
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                            oVar.AssociatedContentControls[0].Range.Fields.Count > 0)
                            xValue = xValue + " (field code)";
                    }
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = m_xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (oVar.Value == "") ? m_xEmptyDisplayValue : xValue;
            }

            ////get available value node text width
            ////TODO: we may want to enhance this method
            ////to determine how much text to enter into
            ////the node, given how much width is available
            //if (xValue.Length > 35)
            //    //trim string and append ellipse if necessary
            //    xValue = xValue.Substring(0, 35) +
            //        (xValue.EndsWith("...") ? "" : "...");

            LMP.Benchmarks.Print(t0, oVar.DisplayName);

            return xValue;
        }
        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string EvaluateDisplayValue(Variable oVar, string xDocumentValue)
        {
            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = xDocumentValue;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = FieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        Word.XMLNode oWordNode = oVar.AssociatedWordTags[0];
                        string xTagValue = oWordNode.Text;
                        //GLOG 3318: Get text using original input case if necessary
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, oVar.Segment.CreationStatus == Status.Finished);
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oWordNode.Range.Fields.Count > 0))
                            xTagValue = oWordNode.Range.Fields[1].Result.Text;

                        xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    }
                    else
                    {
                        Word.ContentControl oCC = oVar.AssociatedContentControls[0];
                        string xTagValue = oCC.Range.Text;

                        //GLOG 3318: Get text using original input case if necessary
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range, oVar.Segment.CreationStatus == Status.Finished);
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oCC.Range.Fields.Count > 0))
                            xTagValue = oCC.Range.Fields[1].Result.Text;

                        xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    }
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }
            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = FieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return Expression.Evaluate(xFieldCode, oVar.Segment, oVar.ForteDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {

                        xTemp = oVar.AssociatedWordTags[0].Text;
                    }
                    else
                    {
                        xTemp = oVar.AssociatedContentControls[0].Range.Text;
                    }
                }
                else
                {
                    xTemp = Expression.Evaluate(xTemp, oVar.Segment, oVar.ForteDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", "");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            return xValue;
        }
        ///// <summary>
        ///// prompts the user to change any data that might
        ///// have been corrupted during document editing - stores
        ///// edited data to appropriate deactivation variable -
        ///// returns true iff user did not indicate any bad tag values
        ///// </summary>
        ///// <returns></returns>
        //public bool ReviewData()
        //{
        //    return ReviewData(null);
        //}

        //public bool ReviewData(string xVarName)
        //{
        //    if(string.IsNullOrEmpty(this.GetDeactivationData()))
        //        return true;

        //    string xMismatchedData = null;

        //    if (!string.IsNullOrEmpty(xVarName))
        //    {
        //        //return variable detail if variable value is mismatched -
        //        //i.e. if the value is different from the last
        //        //deactivation snapshot
        //        xMismatchedData = this.GetMismatchedDeactivatedVariable(xVarName);
        //    }
        //    else
        //    {
        //        //get mismatched variables -
        //        //these are those variables could possibly
        //        //have been corrupted since the last
        //        //deactivation snapshot
        //        xMismatchedData = this.GetMismatchedDeactivatedVariables();
        //    }

        //    //exit if there is no possibly corrupted data
        //    if (string.IsNullOrEmpty(xMismatchedData))
        //        return true;

        //    //prompt user - display only the mismatched data
        //    DataReviewForm oForm = new DataReviewForm(xMismatchedData, this);

        //    oForm.ShowDialog();

        //    //user has not canceled operation -
        //    //get the reviewed data
        //    string xReviewedData = oForm.ReviewedData;
        //    string[] aReviewedData = xReviewedData.Split(StringArray.mpEndOfRecord);

        //    //cycle through each reviewed variable -
        //    //writing the value to the deactivation doc var
        //    foreach (string xReviewedVar in aReviewedData)
        //    {
        //        //get detail of each reviewed variable
        //        string[] aReviewedVar = xReviewedVar.Split(StringArray.mpEndOfElement);

        //        //get the deactivation document variable that
        //        //contains the data for this reviewed variable
        //        string xSegmentID = aReviewedVar[1];
        //        int iPos = xSegmentID.LastIndexOf('.');
        //        if (iPos > -1)
        //            xSegmentID = xSegmentID.Substring(iPos + 1);
        //        object oName = "Deactivation_" + xSegmentID;
        //        Word.Variable oDocVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);

        //        //get the value of the document variable
        //        string xValue = StringArray.mpEndOfRecord +
        //            LMP.String.Decrypt(oDocVar.Value) + StringArray.mpEndOfRecord;

        //        //search for the deactivated name/value pair
        //        iPos = xValue.IndexOf(StringArray.mpEndOfRecord +
        //            aReviewedVar[2] + StringArray.mpEndOfElement);

        //        if (iPos > -1)
        //        {
        //            //pair was found - find the end of the old name/value pair
        //            int iPos2 = xValue.IndexOf(StringArray.mpEndOfRecord, iPos + 1);

        //            //reconstruct the new name/value pair
        //            string xNewValue = StringArray.mpEndOfRecord + aReviewedVar[2] +
        //                StringArray.mpEndOfElement + aReviewedVar[3];

        //            //replace the existing value with the reviewed value
        //            string xNewVarValue = xValue.Substring(0, iPos) +
        //                xNewValue + xValue.Substring(iPos2);

        //            xNewVarValue = xNewVarValue.Trim(StringArray.mpEndOfRecord);

        //            //set document variable to the new value
        //            oDocVar.Value = LMP.Forte.MSWord.Application.Encrypt(xNewVarValue,
        //                LMP.Data.Application.EncryptionPassword);

        //            //force variable value to be updated on next request (5/19/11 dm)
        //            Segment oSegment;
        //            if (aReviewedVar[1] != this.FullTagID)
        //                //variable belongs to a child of this segment
        //                oSegment = FindSegment(aReviewedVar[1], this.Segments);
        //            else
        //                oSegment = this;
        //            Variable oVar = oSegment.Variables.ItemFromName(aReviewedVar[2]);
        //            oVar.ForceValueRefresh();
        //        }
        //    }

        //    //return true if reviewed data contains no fixed value markers
        //    return (xReviewedData.IndexOf(ForteConstants.mpFixedValueMarker) == -1);
        //}

        /// <summary>
        /// returns true iff the specified segment
        /// property has a non-empty value
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        protected bool PropertyValueExists(ListDictionary oLD, string xPropertyName)
        {
            return oLD[xPropertyName] != null && oLD[xPropertyName].ToString() != "";
        }
        /// <summary>
        /// returns true if and only if the segment or one of
        /// its direct children are of the specified type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public bool Contains(LMP.Data.mpObjectTypes iType)
        {
            if (this.TypeID == iType)
                return true;
            else
            {
                int iCount = this.Segments.Count;
                for (int i = 0; i < iCount; i++)
                {
                    if (this.Segments[i].TypeID == iType)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// selects this segment in the document
        /// </summary>
        public void Select()
        {
            Select(false);
        }

         /// <summary>
        /// selects this segment in the document -
        /// selects the start of the segment if specified
        /// </summary>
        public void Select(bool bSelectStart)
{
			try
			{
				//TODO: reimplement this method to use a transform -
				//that's the only way to get the entire segment, as
				//a segment can be distributed across numerous tags
                if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
                {
                    Word.XMLNode[] aTags = this.Nodes.GetWordTags(this.FullTagID);

                    if (aTags.Length == 0)
                    {
                        this.Nodes.GetBookmarks(this.FullTagID)[0].Range.Select();
                    }
                    else
                    {
                        aTags[0].Range.Select();
                    }
                }
                else
                {
                    Word.ContentControl[] aCtls = this.Nodes.GetContentControls(this.FullTagID);

                    if (aCtls.Length == 0)
                    {
                        this.Nodes.GetBookmarks(this.FullTagID)[0].Range.Select();
                    }
                    else
                    {
                        aCtls[0].Range.Select();
                    }
                }

                if (bSelectStart)
                {
                    //select the start of the segment
                    object oDir = Word.WdCollapseDirection.wdCollapseStart;

                    this.ForteDocument.WordDocument.Application.Selection.Collapse(ref oDir);
                }
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.WordTagException(
					LMP.Resources.GetLangString("Error_CouldNotSelectSegment") + this.FullTagID, oE);
			}
		}
		/// <summary>
		/// copies this segment to the clipboard
		/// </summary>
		public void Copy()
		{
			try
			{
				//TODO: reimplement this method to use a transform -
				//that's the only way to get the entire segment, as
				//a segment can be distributed across numerous tags
				this.Nodes.GetWordTags(this.FullTagID)[0].Range.Copy();
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.WordTagException(
					LMP.Resources.GetLangString("Error_CouldNotCopySegment") + this.FullTagID, oE);
			}
		}
		/// <summary>
		/// looks for qualifier in xRefName;
		/// returns target object and unqualified xRefName
		/// </summary>
		/// <param name="xRefName"></param>
		/// <returns></returns>
		internal Segment GetReferenceTarget(ref string xRefName)
		{
            Trace.WriteNameValuePairs("xRefName", xRefName);

			//check for parent or defined sub-object keyword
			int iPos = xRefName.IndexOf("::");
			if(iPos == -1)
                //no qualifier
				return this;

            //parse out qualifier
            string xQualifier = xRefName.Substring(0, iPos).ToUpper();
            xRefName = xRefName.Substring(iPos + 2);

            //get top-level parent
            Segment oTopParent = this.Parent;
            if (oTopParent != null)
            {
                while (oTopParent.Parent != null)
                    oTopParent = oTopParent.Parent;
            }

            int iStartIndex;
            string xTypeID;
            mpObjectTypes iObjectType;

            if (xQualifier == "PARENT")
                return this.Parent;
            else if (xQualifier == "TOPPARENT")
                return oTopParent;
            else if (xQualifier.IndexOf("CHILD") > -1)
            {
                //search this segment's children for a segment of the specified type
                if (xQualifier.IndexOf("CHILDREN") > -1)
                    iStartIndex = 8;
                else
                    iStartIndex = 5;
                xTypeID = xQualifier.Substring(iStartIndex);
                iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                return FindSegment(iObjectType, this.Segments);
            }
            else if (xQualifier.IndexOf("SIBLING") > -1)
            {
                //search all descendants of this segment's top parent for a segment
                //of the specified type, excluding this segment and its ancestors
                if (oTopParent != null)
                {
                    if (xQualifier.IndexOf("SIBLINGS") > -1)
                        iStartIndex = 8;
                    else
                        iStartIndex = 7;
                    xTypeID = xQualifier.Substring(iStartIndex);
                    iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                    Segment[] oSegments = FindSegments(iObjectType, oTopParent.Segments);
                    foreach (Segment oSegment in oSegments)
                    {
                        string xTagID = oSegment.FullTagID;
                        if ((this.FullTagID != xTagID) &&
                            ((this.FullTagID.Length < xTagID.Length + 1) ||
                            (this.FullTagID.Substring(0, xTagID.Length + 1) != xTagID + ".")))
                            return oSegment;
                    }
                    return null;
                }
                else if (this.ForteDocument.Segments.Count > 1)
                {
                    //this is a top level segment - a sibling is also a top level segment
                    if (this.ForteDocument.Segments[0] == this)
                    {
                        //return the second segment in the collection -
                        //that's the first sibling
                        return this.ForteDocument.Segments[1];
                    }
                    else
                    {
                        //return the first segment in the collection -
                        //that's the first sibling
                        return this.ForteDocument.Segments[0];
                    }
                }
                else
                    return null;
            }
            else if (xQualifier.IndexOf("SEGMENT") > -1)
            {
                //search entire document for a segment of the specified type
                if (xQualifier.IndexOf("SEGMENTS") > -1)
                    iStartIndex = 8;
                else
                    iStartIndex = 7;
                xTypeID = xQualifier.Substring(iStartIndex);
                iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                return FindSegment(iObjectType, this.ForteDocument.Segments);
            }
            else
            {
                //unsupported keyword - raise error
                throw new LMP.Exceptions.KeywordException(
                    LMP.Resources.GetLangString("Error_InvalidKeyword") + xQualifier);
            }
        }
        /// <summary>
        /// returns true iif the current selection is in the 
        /// body of this segment
        /// </summary>
        /// <returns></returns>
        public bool SelectionIsInBody()
        {
            Word.Range oBody = this.GetBody();
            return LMP.Forte.MSWord.WordApp.SelectionIsInRange(oBody);
        }
        /// <summary>
        /// looks for qualifier in xRefName;
        /// returns target objects and unqualified xRefName
        /// </summary>
        /// <param name="xRefName"></param>
        /// <returns></returns>
        public Segment[] GetReferenceTargets(ref string xRefName)
        {
            Segment[] oSegments = null;

            Trace.WriteNameValuePairs("xRefName", xRefName);

            //check for parent or defined sub-object keyword
            int iPos = xRefName.IndexOf("::");
            if (iPos == -1)
                //no qualifier - return this segment
                return new Segment[1] { this };

            //parse out qualifier
            string xQualifier = xRefName.Substring(0, iPos).ToUpper();

            if (xQualifier.IndexOf("CHILDREN") + xQualifier.IndexOf("SIBLINGS") +
                xQualifier.IndexOf("SEGMENTS") == -3)
            {
                //perhaps this is one of the singular keywords
                Segment oTarget = GetReferenceTarget(ref xRefName);
                if (oTarget != null)
                    return new Segment[1] { oTarget };
                else
                    return null;
            }

            //parse out type id
            string xTypeID = xQualifier.Substring(8);
            mpObjectTypes iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);

            if (xQualifier.IndexOf("CHILDREN") > -1)
            {
                //search this segment's children for segments of the specified type
                oSegments = FindSegments(iObjectType, this.Segments);
            }
            else if (xQualifier.IndexOf("SIBLINGS") > -1)
            {
                //search all descendants of this segment's top parent for segments
                //of the specified type, excluding this segment's ancestors
                Segment oTopParent = this.Parent;
                if (oTopParent != null)
                {
                    //get top-level parent
                    while (oTopParent.Parent != null)
                        oTopParent = oTopParent.Parent;
                    oSegments = FindSegments(iObjectType, oTopParent.Segments);

                    //add found segments to array list
                    ArrayList oSegArrayList = new ArrayList();
                    foreach (Segment oSegment in oSegments)
                    {
                        //do not include direct ancestors
                        string xTagID = oSegment.FullTagID;
                        //GLOG 4237: Avoid using Substring comparison on ancestors or same Segment
                        if ((this.FullTagID.Length < xTagID.Length + 1) ||
                            (this.FullTagID.Substring(0, xTagID.Length + 1) != xTagID + "."))
                            oSegArrayList.Add(oSegment);
                    }

                    //copy segments to array
                    oSegments = new Segment[oSegArrayList.Count];
                    for (int i = 0; i < oSegArrayList.Count; i++)
                        oSegments[i] = (Segment)oSegArrayList[i];
                }
            }
            else if (xQualifier.IndexOf("SEGMENTS") > -1)
            {
                //search all segments for segments of the specified type
                oSegments = FindSegments(iObjectType, this.ForteDocument.Segments);
            }

            //parse out remainder of code
            xRefName = xRefName.Substring(iPos + 2);

            return oSegments;
        }
        /// <summary>
        /// returns the first variable that has 
        /// an InsertDetailInTable variable action
        /// </summary>
        public Variable FirstInsertDetailVariable
        {
            get
            {
                for (int i = 0; i < this.Variables.Count; i++ )
                {
                    Variable oVar = this.Variables.ItemFromIndex(i);

                    if (oVar.HasDetailTableAction())
                        return oVar;
                }

                return null;
            }
        }
		/// <summary>
		/// returns the doc property referred to by xDocPropReference
		/// can be 
		/// </summary>
		/// <param name="oMPObject"></param>
		/// <param name="xVariableRef">either the name of the variable, or a fully qualified name, which
		/// is of the form A::B, where A is the parent or child segment containing the variable,
		/// and B is the name of the variable</param>
		/// <returns></returns>
		public Variable GetVariable(string xVariableRef)
		{
			Variable oVar = null;;

    		Trace.WriteNameValuePairs("xVariableRef", xVariableRef);
			
			if(xVariableRef != null && xVariableRef != "")
			{
				//get target object
				Segment oTarget = this.GetReferenceTarget(ref xVariableRef);
				
				//get target property
				try
				{
					//TODO: rework this line - needs to be found by name
					oVar  = oTarget.Variables.ItemFromName(xVariableRef);
				}
				catch{}

				return oVar;
			}
			else
				return null;
		}
        /// <summary>
        /// refreshes this segment's node store
        /// </summary>
        public void RefreshNodes()
        {
            RefreshNodes(false);
        }
        /// <summary>
		/// refreshes this segment's node store
		/// </summary>
		public void RefreshNodes(bool bIncludeChildren)
		{
            DateTime t0 = DateTime.Now;

			m_oNodes = new NodeStore(this.m_oMPDocument.Tags, this.FullTagID,
				mpNodeStoreMatchTypes.Equals);

			if(m_oNodes.Count == 0)
				//something is wrong - there needs to be
				//at least one mSEG tag in the segment xml
				throw new LMP.Exceptions.SegmentException(LMP.Resources
					.GetLangString("Error_NoNodesFoundForSegment") + this.FullTagID);

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                    this.Segments[i].RefreshNodes(bIncludeChildren);
            }

            LMP.Benchmarks.Print(t0);
		}
        /// <summary>
        /// refreshes this segment
        /// </summary>
        public void Refresh()
        {
            this.Refresh(Segment.RefreshTypes.All);
        }
        /// <summary>
        /// refreshes this segment's node store, variables, blocks, and,
        /// if specified, child segments
        /// </summary>
        /// <param name="oType"></param>
        public void Refresh(Segment.RefreshTypes oType)
        {
            DateTime t0 = DateTime.Now;

            //refresh node store
            RefreshNodes(oType != RefreshTypes.NoChildSegments);

            //refresh variables, blocks and, if specified, child segments
            RefreshMembers(oType);

            LMP.Benchmarks.Print(t0);
        }

        internal void RefreshBlocks()
        {
            m_oBlocks = new Blocks(this);
        }

        /// <summary>
        /// returns a prefill object populated with 
        /// the values of the segment's variables
        /// </summary>
        /// <returns></returns>
        public Prefill CreatePrefill()
        {
            return new Prefill(this);
        }

        /// <summary>
        /// returns the data stored during deactivation
        /// </summary>
        /// <returns></returns>
        public string GetDeactivationData()
        {
            object oName = "Deactivation_" + this.TagID;

            try
            {
                return this.ForteDocument.WordDocument.Variables.get_Item(ref oName).Value;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// calls CI
        /// </summary>
        public void GetContacts()
        {
            //GLOG : 7469 : ceh
            if (!LMP.Data.Application.AllowCIWithoutOutlookRunning())
                return;

            Data.Application.StartCIIfNecessary();

            //GLOG 3461: Reset these each time, as list of CI-enabled variables might have changed
            m_iRetrieveDataFrom = 0;
            m_iRetrieveDataTo = 0;
            m_iRetrieveDataCC = 0;
            m_iRetrieveDataBCC = 0;
            m_aCustomEntities = null;
            m_iCIAlerts = 0;

            GetCIFormat(ref m_iRetrieveDataTo, ref m_iRetrieveDataFrom, ref m_iRetrieveDataCC,
                ref m_iRetrieveDataBCC, ref m_aCustomEntities, ref m_iCIAlerts);
            bool bCustomEntitiesExist = (m_aCustomEntities != null && m_aCustomEntities.Length > 0);

            // No CI-enabled variabled
            if (m_iRetrieveDataTo == 0 && m_iRetrieveDataFrom == 0 &&
                m_iRetrieveDataCC == 0 && m_iRetrieveDataBCC == 0 &&
                !bCustomEntitiesExist)
                return;

            //GLOG - 3503 - ceh 
            //GLOG - 3218 - ceh 
            //GLOG - 3631 - ceh 
            //Get Max Contacts information
            GetCIMaxContacts(ref m_iMax, ref m_iToMax, ref m_iFromMax, ref m_iCCMax, ref m_iBCCMax, ref m_iOtherMax);

            //check if maximum contacts has been reached
            if ((m_iRetrieveDataTo != 0 || m_iRetrieveDataFrom != 0 ||
                m_iRetrieveDataCC != 0 || m_iRetrieveDataBCC != 0 ) &&
                (m_iToMax == -1 && m_iFromMax == -1 && m_iCCMax == -1 && m_iBCCMax == -1 && m_iOtherMax == -1))
            {
                System.Windows.Forms.MessageBox.Show(LMP.Resources.GetLangString("Msg_MaxNumberOfContactsAllFields"),
                                                LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
                                                System.Windows.Forms.MessageBoxIcon.Exclamation); 
                return;
            }
            
            //Get Display Name information
            GetCIDisplayName(ref m_xToLabel, ref m_xFromLabel, ref m_xCCLabel, ref m_xBCCLabel);

            //Save state
            Environment oEnv = new Environment(this.ForteDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                string xSep = "|";

                //if custom entities exist, show the custom CI UI -
                //TODO: we have to, at some point, support custom entities
                //when there are standard entities as well, but for now,
                //when custom entities exist, no standard entities are displayed -
                //clearly, segments can't request both
                ICContacts oContacts = null;

                if (bCustomEntitiesExist)
                {
                    Array oArray = (Array)m_aCustomEntities;
                    oContacts = oCI.GetContactsFromArray(ref oArray, xSep);
                }
                else
                {
                    //#3218 - ceh 
                    //use variable display name as the label for the right side list box in the CI dialog
                    oContacts = oCI.GetContactsEx(m_iRetrieveDataTo,
                        m_iRetrieveDataFrom, m_iRetrieveDataCC, m_iRetrieveDataBCC,
                        ICI.ciSelectionlists.ciSelectionList_To,0, m_iCIAlerts,
                        m_xToLabel, m_xFromLabel, m_xCCLabel, m_xBCCLabel,
                        m_iToMax, m_iFromMax, m_iCCMax, m_iBCCMax);
                }

                //alert that CI dialog has been released
                if (CIDialogReleased != null)
                    CIDialogReleased(this, new EventArgs());

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                if ((oContacts != null) && (oContacts.Count() > 0))
                {
                    //add contact detail to ci-enabled variables
                    SetCIEnabledVariables(oContacts);

                    //alert that contacts have been added
                    if (this.ContactsAdded != null)
                        this.ContactsAdded(this, new EventArgs());
                }
                //if (oContacts != null)
                //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

                oContacts = null;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.CIException(
                LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
            }
            finally
            {
                //Restore state
                //3-1-12 (dm) - disable event handlers while restoring state
                ForteDocument.WordXMLEvents iEvents = LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents;
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oEnv.RestoreState();
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = iEvents;
            }

        }
        /// <summary>
        /// calls CI to populate a single variable
        /// </summary>
        public void GetContacts(Variable oVar)
        {
            string xToLabel = "";
            string xFromLabel = "";
            string xCCLabel = "";
            string xBCCLabel = "";
            int iMax = 0;
            int iToMax = -1;
            int iFromMax = -1;
            int iCCMax = -1;
            int iBCCMax = -1;
            int iOtherMax = -1;

            //GLOG : 7469 : ceh
            if (!Data.Application.AllowCIWithoutOutlookRunning())
                return;

            if (oVar.CIContactType == 0)
                return;

            Data.Application.StartCIIfNecessary();

            ciRetrieveData iRetrieveDataTo = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataFrom = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataCC = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataBCC = ciRetrieveData.ciRetrieveData_None;
            ciAlerts iAlerts = ciAlerts.ciAlert_None;
            ICI.ciSelectionlists iDefList = 0;
            
            List<Variable> oVarList = new List<Variable>();
            oVarList.Add(oVar);
            object[,] aCustomEntity = null;

            Contacts.GetCIFormat(oVarList, ref iRetrieveDataTo, ref iRetrieveDataFrom, ref iRetrieveDataCC,
                ref iRetrieveDataBCC, ref aCustomEntity, ref iAlerts);

            // Variable not ci-enabled
            if (iRetrieveDataTo == 0 && iRetrieveDataFrom == 0 &&
                iRetrieveDataCC == 0 && iRetrieveDataBCC == 0 && 
                (aCustomEntity == null || aCustomEntity[0,0] == null))
                return;

            //GLOG - 3503 - ceh 
            //GLOG - 3218 - ceh 
            //GLOG - 3631 - ceh 
            //GLOG - 3635 - ceh
            //Get Max Contacts information
            this.GetCIVariableInfo(oVarList, ref iMax, ref iToMax,
                                   ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);

            //check if maximum contacts has been reached
            if ((iRetrieveDataTo != 0 || iRetrieveDataFrom != 0 ||
                iRetrieveDataCC != 0 || iRetrieveDataBCC != 0) &&
                (iToMax == -1 && iFromMax == -1 && iCCMax == -1 && iBCCMax == -1 && iOtherMax == -1))
            {
                //GLOG : 8009 : ceh - use variable display name instead of generic 'contacts' string
                System.Windows.Forms.MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_MaxNumberOfContacts"), oVar.DisplayName),
                                                LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
                                                System.Windows.Forms.MessageBoxIcon.Exclamation);
                return;
            }

            //Get Display Name information
            this.GetCIVariableInfo(oVarList, ref xToLabel,
                                   ref xFromLabel, ref xCCLabel, ref xBCCLabel);
            
            //Save state
            Environment oEnv = new Environment(this.ForteDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                ICContacts oContacts =  null;

                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                if (aCustomEntity != null && aCustomEntity.Length != 0)
                {
                    string xSep = "|";
                    System.Array oArray = (Array)aCustomEntity;
                    oContacts = Data.Application.CISession
                        .GetContactsFromArray(ref oArray, xSep);
                }
                else
                {
                    //#3218 - ceh 
                    //use variable display name as the label for the right side list box in the CI dialog
                    oContacts = Data.Application.CISession
                        .GetContactsEx(iRetrieveDataTo, iRetrieveDataFrom,
                        iRetrieveDataCC, iRetrieveDataBCC,
                        iDefList, 0, iAlerts, 
                        xToLabel, xFromLabel, xCCLabel, xBCCLabel,
                        iToMax, iFromMax, iCCMax, iBCCMax);
                }

                //alert that CI dialog has been released
                if (CIDialogReleased != null)
                    CIDialogReleased(this, new EventArgs());

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                if ((oContacts != null) && (oContacts.Count() > 0))
                {
                    //add contact detail to ci-enabled variables
                    SetCIEnabledVariables(oVarList, oContacts, true);

                    //alert that contacts have been added
                    if (this.ContactsAdded != null)
                        this.ContactsAdded(this, new EventArgs());

                    //if(oContacts != null)
                    //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

                    oContacts = null;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.CIException(
                LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
            }
            finally
            {
                //Restore state
                //3-1-12 (dm) - disable event handlers while restoring state
                ForteDocument.WordXMLEvents iEvents = LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents;
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oEnv.RestoreState();
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = iEvents;
            }
        }
        /// <summary>
        /// returns the child segments of this segment
        /// that are of the specified type
        /// </summary>
        /// <param name="iChildObjectType"></param>
        /// <returns></returns>
        public Segment[] FindChildren(mpObjectTypes iChildObjectType)
        {
            return Segment.FindSegments(iChildObjectType, this.Segments);
        }

        /// <summary>
        /// returns true iff the specified 
        /// segment is contained in this segment
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public bool Contains(string xTagID)
        {
            return FindSegment(xTagID, this.Segments) != null;
        }

        /// <summary>
        /// restores all deleted scopes in the segment
        /// </summary>
        public void RestoreDeletedScopesForDesign()
        {
            //GLOG 1407 - set flag to avoid execution indexes from being adjusted
            //when variables are added with delete scopes
            m_oMPDocument.DeleteScopesUpdatingInDesign = true;

            //GLOG 5315 (dm) - ensure that deleted scopes nested in restored blocks
            //are themselves restored
            bool bHasConditionalBlock = false;

            //cycle through variables - execute action, leaving expanded scope, if variable
            //has an IncludeExcludeBlock action or is in its deleted state
            for (int i = 0; i < this.Variables.Count; i++)
            {
                Variable oVar = this.Variables[i];
                for (int j = 0; j < oVar.VariableActions.Count; j++)
                {
                    VariableAction oAction = oVar.VariableActions[j];
                    VariableActions.Types iType = oAction.Type;
                    //GLOG - 3395 - CEH
					if ((iType == VariableActions.Types.IncludeExcludeBlocks) ||
                        (oVar.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (oVar.TagType == ForteDocument.TagTypes.Mixed))
                    {
                        //restore deleted scope
                        oAction.Execute(true);

                        //GLOG 5315 (dm) - flag presence of conditional block
                        if (iType == VariableActions.Types.IncludeExcludeBlocks)
                            bHasConditionalBlock = true;

                        //break;
                    }
                }
            }

            //GLOG 5315 (dm) - check for any remaining mDels
            if (bHasConditionalBlock)
            {
                for (int i = 0; i < this.Variables.Count; i++)
                {
                    Variable oVar = this.Variables[i];
                    if ((oVar.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (oVar.TagType == ForteDocument.TagTypes.Mixed))
                        oVar.VariableActions.Execute(true);
                }
            }

            //GLOG 6328 (dm) - we're no longer persisting deleted scopes xml in design -
            //any value that remains is obsolete - clear out to prevent conflicts in the
            //event that a variable of the same name is imported
            if (this.Nodes.GetItemDeletedScopes(this.FullTagID).Length > 2)
                this.Nodes.ClearItemDeletedScopes(this.FullTagID);

            //child segments
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].RestoreDeletedScopesForDesign();

            //GLOG 1407 - reset flag
            m_oMPDocument.DeleteScopesUpdatingInDesign = false;
        }

        /// <summary>
        /// sets/removes the TagExpandedValue field code as the ValueSourceExpression
        /// of all variables with an InsertText action, unless a different
        /// ValueSourceExpression has been specified - we do this to pick up adjacent
        /// manual edits that are inadvertently outside of the tag
        /// </summary>
        /// <param name="bExpanded"></param>
        public void RefreshExpandedTagValueCodes(bool bExpanded)
        {
            //if the segment is using content controls, don't do anything
            if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                return;

            //this segment
            for (int i = 0; i < this.Variables.Count; i++)
            {
                bool bDo = false;
                Variable oVar = this.Variables[i];
                string xExpression = oVar.ValueSourceExpression;
                if (((bExpanded && System.String.IsNullOrEmpty(xExpression)) ||
                    (xExpression.ToLower().IndexOf("[tagexpandedvalue") > -1)) &&
                    (((oVar.AssociatedWordTags.Length == 1) && !oVar.IsMultiValue) ||
                    !bExpanded))
                {
                    VariableActions oActions = oVar.VariableActions;
                    for (int j = 0; j < oActions.Count; j++)
                    {
                        VariableActions.Types iType = oActions[j].Type;
                        if (iType == VariableActions.Types.InsertText)
                        {
                            bDo = true;
                            break;
                        }
                    }
                }

                if (bDo)
                {
                    if (bExpanded)
                    {
                        //set
                        oVar.ValueSourceExpression = LMP.Forte.MSWord.WordDoc
                            .GetExpandedTagCode(oVar.AssociatedWordTags[0]);
                    }
                    else
                    {
                        //remove
                        oVar.ValueSourceExpression = "";
                    }

                    //save to document
                    this.Variables.Save(oVar);
                }
            }

            //child segments
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].RefreshExpandedTagValueCodes(bExpanded);
        }
        /// <summary>
        /// inserts the xml of the segment at the specified location
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oLocation"></param>
        /// <returns>returns the unindexed TagID of the inserted segment</returns>
        abstract public void InsertXML(string xXML, Segment oParent, Word.Range oLocation, 
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted, 
            mpSegmentIntendedUses iIntendedUse);
        /// <summary>
        /// Returns List of Unique IDs of child segments
        /// </summary>
        /// <returns></returns>
        internal System.Collections.Generic.List<string> ChildSegmentList(bool bIncludeChildren)
        {
            System.Collections.Generic.List<string> aList = new System.Collections.Generic.List<string>();
            for (int i = 0; i < this.Segments.Count; i++)
            {
                if (!aList.Contains(this.Segments[i].ID))
                    aList.Add(this.Segments[i].ID);

                if (bIncludeChildren)
                {
                    //Append IDs of Children of Children
                    System.Collections.Generic.List<string> aChildList = this.Segments[i].ChildSegmentList(bIncludeChildren);
                    foreach (string xItem in aChildList)
                    {
                        if (!aList.Contains(xItem))
                            aList.Add(xItem);
                    }
                }
            }
            return aList;
        }

        /// <summary>
        /// sets authors of all child segments whose authors are
        /// not linked to this segment's authors
        /// </summary>
        public void SetChildAuthorsFromDocument(bool bExecuteActions)
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oChild = this.Segments[i];
                //do only if not linked to this segment's authors
                if (!oChild.LinkAuthorsToParent)
                {
                    oChild.SetAuthorsFromDocument(bExecuteActions);
                    oChild.SetChildAuthorsFromDocument(bExecuteActions);
                }
            }
        }
        /// <summary>
        /// returns the body of the segment
        /// if it exists - else returns null
        /// </summary>
        /// <returns></returns>
        public Word.Range GetBody()
        {
            Block oBlock = null;
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                //GLOG 3260: Make sure oBlock is never set to non-body block
                if (this.Blocks[i].IsBody)
                {
                    oBlock = this.Blocks[i];
                    break;
                }
            }

            if (oBlock == null)
            {
                // The segment has no body. Use message variable as body if it exists.
                if (this.Variables.VariableExists("Message"))
                {
                    Variable oVar = this.Variables.ItemFromName("Message");

                    if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        if (oVar.AssociatedWordTags.Length > 0)
                            return oVar.AssociatedWordTags[0].Range;
                        else
                            return null;
                    }
                    else
                    {
                        if (oVar.AssociatedContentControls.Length > 0)
                            return oVar.AssociatedContentControls[0].Range;
                        else
                            return null;
                    }
                }
                else
                    return null;
            }
            else if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
            {
                return oBlock.AssociatedWordTag.Range;
            }
            else
                return oBlock.AssociatedContentControl.Range;
        }
        /// <summary>
        /// builds jurisdictional domain string from segment def L0-L4
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        public string GetJurisdictionText()
        {
            return GetJurisdictionText(true, true, true, true, true);
        }
        /// <summary>
        /// builds jurisdictional domain string from segment def L0-L4
        /// </summary>
        /// <param name="bIncludeL0"></param>
        /// <param name="bIncludeL1"></param>
        /// <param name="bIncludeL2"></param>
        /// <param name="bIncludeL3"></param>
        /// <param name="bIncludeL4"></param>
        /// <returns></returns>
        public string GetJurisdictionText(bool bIncludeL0, bool bIncludeL1, bool bIncludeL2, 
            bool bIncludeL3, bool bIncludeL4)
        {
            if (!(this is AdminSegment))
                return "";

            AdminSegment oSegment = (AdminSegment)this;
            //build display string from jurisdiction levels 0 - 4
            StringBuilder oSB = new StringBuilder();
            //GLOG 3248: Handle invalid/non-existent levels, so that Design will load successfully
            try
            {
                Jurisdictions oJurs = null;
                Jurisdiction oJur;
                int iL0 = 0;
                int iL1 = 0;
                int iL2 = 0;
                int iL3 = 0;
                int iL4 = 0;

                if (bIncludeL0)
                {
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Zero);

                    if (oJurs.Count != 0)
                    {
                        try
                        { iL0 = oSegment.L0; }

                        catch { }
                        if (iL0 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL0));
                            }
                            catch (System.Exception oE)
                            {
                                //Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }
                if (bIncludeL1)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.One);
                    if (oJurs != null)
                    {
                        try
                        { iL1 = oSegment.L1; }

                        catch { }
                        if (iL1 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL1));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL2)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Two);
                    if (oJurs != null)
                    {
                        try
                        { iL2 = oSegment.L2; }

                        catch { }
                        if (iL2 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL2));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL3)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Three);
                    if (oJurs != null)
                    {
                        try
                        { iL3 = oSegment.L3; }

                        catch { }
                        if (iL3 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL3));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL4)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Four);
                    if (oJurs != null)
                    {
                        try
                        { iL4 = oSegment.L4; }
                        catch { }
                        if (iL4 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL4));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                        }
                    }
                }
            }
            catch {}
            string xTemp = oSB.ToString().TrimEnd('\n', '\r');
            return oSB.ToString();
        }
        /// <summary>
        /// returns true if segment design requires level chooser control
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        public bool LevelChooserRequired()
        {
            if (!(this is  AdminSegment))
                return false;

            switch (this.TypeID)
            {
                case mpObjectTypes.Pleading:
                    return true;
                case mpObjectTypes.Service:
                    return true;
                case mpObjectTypes.Notary:
                    return true;
                case mpObjectTypes.Verification:
                    return true;
                case mpObjectTypes.PleadingCaption:
                    return true;
                case mpObjectTypes.PleadingCounsel:
                    return true;
                case mpObjectTypes.PleadingSignature:
                    return true;
                case mpObjectTypes.PleadingPaper:
                    return true;
                case mpObjectTypes.PleadingCaptionBorderSet:
                    return true;
                case mpObjectTypes.PleadingCoverPage:
                    return true;
                case mpObjectTypes.PleadingCaptions:
                    return true;
                case mpObjectTypes.PleadingCounsels:
                    return true;
                case mpObjectTypes.PleadingSignatures:
                    return true;
                case mpObjectTypes.PleadingSignatureNonTable:
                    return true;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Builds delmited string formatted for use with JurisdictionChooser control
        /// </summary>
        /// <returns></returns>
        public string GetJurisdictionLevels()
        {
            if (!(this is AdminSegment))
                return "";

            AdminSegment oSeg = (AdminSegment)this;
            string xSep = StringArray.mpEndOfRecord.ToString();
            string xFormat = "{0}" + xSep + "{1}" + xSep + "{2}" + xSep + "{3}" + xSep + "{4}";
            return string.Format(xFormat, oSeg.L0.ToString(), oSeg.L1.ToString(), oSeg.L2.ToString(), oSeg.L3.ToString(), oSeg.L4.ToString());
        }
        public bool UpdateContactDetail()
        {
            //GLOG 15812
            return UpdateContactDetail(true);
        }
        /// <summary>
        /// updates the contact detail of all
        /// variables in the segment
        /// </summary>
        public bool UpdateContactDetail(bool bNotifyMissing)
        {
            try
            {
                m_oMissingContactVariables = new List<string>();
                bool bDetailUpdated = false;
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].UpdateContactDetail(false))
                    {
                        bDetailUpdated = true;
                    }
                    //Include variables from child segments in missing list
                    if (this.Segments[i].MissingContactVariables != null)
                    {
                        this.MissingContactVariables.AddRange(this.Segments[i].MissingContactVariables);
                    }
                }

                for (int i = 0; i < this.Variables.Count; i++)
                {
                    if (this.Variables[i].UpdateContactDetail())
                    {
                        bDetailUpdated = true;
                    }
                }
                if (bNotifyMissing && this.MissingContactVariables != null && this.MissingContactVariables.Count > 0)
                {
                    int iCount = this.MissingContactVariables.Count;
                    string xMissingContacts = "";
                    for (int i = 0; i < iCount; i++)
                    {
                        xMissingContacts += this.MissingContactVariables[i];
                        if (i < this.MissingContactVariables.Count - 1)
                        {
                            xMissingContacts += ", ";
                        }
                    }
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenUpdating = true;
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_CouldNotFindContact_UseExistingDetails"), iCount > 1 ? "variables" : "variable", xMissingContacts),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                return bDetailUpdated;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotUpdateSegmentContactDetail"), oE);
            }
        }
        /// <summary>
        /// Clear AssociatedControls to release any objects
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        internal void UnloadVariableControls(bool bIncludeChildren)
        {
            for (int i = 0; i < m_oVariables.Count; i++)
            {
                //Need to explicitly release ICContacts object
                if (m_oVariables[i].AssociatedControl is LMP.Controls.SalutationCombo)
                    ((LMP.Controls.SalutationCombo)m_oVariables[i].AssociatedControl).ClearContacts();
                m_oVariables[i].AssociatedControl = null;
            }
            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    this.Segments[i].UnloadVariableControls(bIncludeChildren);
                }
            }
        }

        /// <summary>
        /// returns TRUE if the specified language is supported by this segment
        /// </summary>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public bool LanguageIsSupported(int iCulture)
        {
            string xSupportedCultures = '�' + this.SupportedLanguages + '�';
            string xCulture = '�' + iCulture.ToString() + '�';
            return (xSupportedCultures.IndexOf(xCulture) > -1);
        }

        /// <summary>
        /// sets creation status of all child segments
        /// </summary>
        /// <param name="iStatus"></param>
        internal void SetChildSegmentsStatus(LMP.Architect.Api.Segment.Status iStatus)
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oChild = this.Segments[i];
                oChild.CreationStatus = iStatus;
                oChild.SetChildSegmentsStatus(iStatus);
            }
        }
        public void UpdateTableBorders()
        {
            try
            {
                //GLOG 4282: Adjust length of center special character border
                //TODO: BOOKMARKS - RESTORE THIS FUNCTIONALITY - dcf
                return;
                if (!(this is CollectionTableItem))
                    //Currently only appropriate for collection table items
                    return;
                
                //JTS 4/16/10: Content Control support
                bool bContentControls = this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;

                Environment oEnv = new Environment(this.ForteDocument.WordDocument);
                Word.Range oRng = null;
                Object oNodesArray = null;
                if (bContentControls)
                {
                    oRng = this.FirstContentControl.Range;
                    oNodesArray = this.ContentControls;
                }
                else
                {
                    oRng = this.FirstWordTag.Range;
                    oNodesArray = this.WordTags;
                }

                //GLOG 3144: Update length special character border if it exists
                if (oRng.Tables.Count > 0 &&
                    oRng.Tables[1].Columns.Count > 2)
                {
                    oEnv.SaveState();
                    int iRowIndex = 0;
                    iRowIndex = oRng.Cells[1].RowIndex;
                    //Check that there is a narrow border column
                    if (oRng.Tables[1].Cell(iRowIndex, 2).Width < 72)
                    {
                        DateTime t0 = DateTime.Now;
                        ForteDocument.WordXMLEvents iIgnore = LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents;
                        //Ignore XML events during update
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        LMP.Forte.MSWord.WordDoc.UpdateSpecialCaptionBorders(ref oNodesArray, "", "", "");
                        //Restore original state
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = iIgnore;
                        LMP.Benchmarks.Print(t0);
                    }
                    oEnv.RestoreState(true, false, false);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }

        /// <summary>
        /// returns a new object database id and increments tracker
        /// </summary>
        /// <returns></returns>
        public int GetNewObjectDatabaseID()
        {
            int iID = m_iNextObjectDatabaseID;
            m_iNextObjectDatabaseID++;
            return iID;
        }

        /// <summary>
        /// activates this segment and children if necessary
        /// </summary>
        /// <param name="bFailed"></param>
        public void ActivateIfNecessary(ref bool bFailed)
        {
            //activate children
            for (int i = 0; i < this.Segments.Count; i++)
            {
                this.Segments[i].ActivateIfNecessary(ref bFailed);
                if (bFailed)
                    return;
            }

            //activate this segment
            if (this.Activated == false)
            {
                this.Activated = true;
                bFailed = (this.Activated == false);
            }
        }

        /// <summary>
        /// deletes all variables belonging to this segment and its children
        /// </summary>
        public void DeleteVariables()
        {
            DeleteVariables(false);
        }
        /// <summary>
        /// deletes all variables belonging to this segment and its children
        /// </summary>
        public void DeleteVariables(bool bTaglessOnly)
        {
            //delete variables of child segments
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].DeleteVariables(bTaglessOnly);

            //delete variables of this segment
            for (int i = this.Variables.Count - 1; i >= 0; i--)
                if (!bTaglessOnly || this.Variables[i].IsTagless)
                    this.Variables.Delete(i);

            //GLOG 7361 (dm) - remove deleted scope doc vars
            Word.Bookmark[] aBmks = this.Bookmarks;
            object oBmks = (object)aBmks;
            LMP.Forte.MSWord.WordDoc.RemoveDeletedScopeDocVars(oBmks);
        }

        /// <summary>
        /// returns whether the segment has variables without invoking its
        /// variables property and thereby populating the collection -
        /// 8-18-11 (dm) - changed from a property to a method in order to add parameter
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasVariables(bool bIncludeChildren)
        {
            //GLOG 6708: Access this.Variables instead of m_oVariables 
            //to ensure collection is initialized if necessary
            if ((this.Variables != null) && (this.Variables.Count > 0))
                return true;

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasVariables(true))
                        return true;
                }
            }

            return false;
        }
        /// <summary>
        /// returns true iff the segment has variables that
        /// display in the task pane
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasDisplayVariables(bool bIncludeChildren)
        {
            Variable oVar = null;

            int iNumVariables = this.Variables.Count;

            for(int i=0; i<iNumVariables; i++)
            {
                if ((this.Variables[i].DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                    Variable.ControlHosts.DocumentEditor)
                {
                    return true;
                }
            }

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasDisplayVariables(true))
                        return true;
                }
            }

            return false;
        }
        /// <summary>
        /// returns whether the segment has blocks without invoking its
        /// blocks property and thereby populating the collection -
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasBlocks(bool bIncludeChildren)
        {
            //GLOG 6708: Access this.Blocks instead of m_oBlocks
            //to ensure collection is initialized if necessary
            if ((this.Blocks != null) && (this.Blocks.Count > 0))
                return true;

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasBlocks(true))
                        return true;
                }
            }

            return false;
        }
        #endregion
        #region *********************static members*********************
        ///// <summary>
        ///// fills an ordered dictionaryh with the
        ///// child segment structure of the specified segment
        ///// </summary>
        ///// <param name="oSegment"></param>
        ///// <param name="oStructure"></param>
        //public static void GetChildTableCollectionStructure(Segment oSegment, ref ChildStructure oStructure)
        //{
        //    for (int i = 0; i < oSegment.Segments.Count; i++)
        //    {
        //        Segment oChild = oSegment.Segments[i];
        //        if (oChild.Segments.Count > 0)
        //        {
        //            //recurse
        //            GetChildTableCollectionStructure(oChild, ref oStructure);
        //        }

        //        //limit child structure to table collections and
        //        //table collection items, as these are the only children
        //        //whose location we can know on re-insertion
        //        if (oChild is CollectionTableItem)
        //        {
        //            //get child segment prefill
        //            Prefill oPrefill = new Prefill(oChild);

        //            oStructure.Add(oChild.FullTagID + "_" + oChild.ID, oPrefill);
        //        }
        //    }
        //}

        /// <summary>
        /// returns a list of segment IDs of segments that
        /// contain the segment with the specified ID
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public static List<string> GetParentSegmentIDs(string xSegmentID)
        {
            //GLOG 4449
            return GetParentSegmentIDs(xSegmentID, false);
        }
        public static List<string> GetParentSegmentIDs(string xSegmentID, bool bIncludeUserSegments)
        {
            List<string> oParentList = null;

            // Get list of other segments containing this segment
            ArrayList aRelatedList = LMP.Data.Application.GetContainingSegmentIDs(xSegmentID);

            if (aRelatedList != null)
            {
                oParentList = new List<string>();

                for (int i = 0; i < aRelatedList.Count; i++)
                {
                    string xNewID = (((object[])aRelatedList[i])[0]).ToString();
                    if (!xNewID.Contains("."))
                        xNewID = xNewID + ".0";

                    //GLOG 4449: Don't include UserSegments in List unless specified
                    if (xNewID.Contains(".0") || bIncludeUserSegments)
                    {
                        //Don't add duplicates
                        if (!oParentList.Contains(xNewID))
                            oParentList.Add(xNewID);
                    }
                }
            }

            return oParentList;
        }
        /// <summary>
        /// returns the required XML tags for new MacPac Segment
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xTagID"></param>
        /// <param name="xObjectData"></param>
        /// <param name="bDoAllStories"></param>
        /// <returns></returns>
        public static string GetFormattedNewSegmentXML(string xSource, string xTagID, string xObjectData, string xBookmarkTag, bool bDoAllStories)
        {
            //we use a different method for word open xml
            if (LMP.String.IsWordOpenXML(xSource))
            {
                return GetFormattedNewSegmentOpenXML(xSource, xTagID,
                    xObjectData, bDoAllStories);
            }

            string xTemp = xSource;

            try
            {
                //get prefix assigned to Forte namespace
                string xPrefix = LMP.String.GetNamespacePrefix(xTemp,
                    ForteConstants.MacPacNamespace);

                if (xPrefix == "")
                {
                    //macpac namespace doesn't exist in the file -
                    //add it - find first namespace attribute
                    int iNSPos = xTemp.IndexOf("xmlns:");
                    xTemp = xTemp.Substring(0, iNSPos) + "xmlns:ns0=\"" + ForteConstants.MacPacNamespace + "\" " +
                        xTemp.Substring(iNSPos);

                    xPrefix = "ns0";
                }

                // opening tag for mSEG
                string xmSEGStart = "<" + xPrefix + ":mSEG ";
                // closing tag for mSEG
                string xmSEGClose = "</" + xPrefix + ":mSEG>";
                // xml for mSEG tag and attributes
                string xmSEGFull = "";
                string xCurTag = "";

                int iPartCount = 0;
                int iStoryStart = 0;
                int iBodyStart = 0;
                int iBodyEnd = 0;
                int iStoryEnd = 0;
                int iPos = 0;
                int iTxBxStart = 0;
                int iTxBxEnd = 0;
                int iSectionCount = 0;
                int iFoundStories = 0;
                string xStoryTemp = "";
                bool bEmptyStory = true;
                bool bEmptyBody = true;

                //Fixed tag for all Parts of Segment
                xmSEGFull = xmSEGStart + "TagID=\"" + xTagID + "\" ObjectData=\"" + xObjectData
                    + "\" PartNumber=\"{0}\">";

                //mSEG starts directly after body tag
                iStoryStart = xTemp.IndexOf("<w:body>");

                // save first section start for tagging if necessary
                iBodyStart = iStoryStart;
                while (iStoryStart > 0)
                {
                    iSectionCount++;
                    iPos = 0;
                    // find end of section
                    iStoryEnd = xTemp.IndexOf("<w:sectPr", iStoryStart);
                    xStoryTemp = xTemp.Substring(iStoryStart, iStoryEnd - iStoryStart);
                    // look for any textboxes
                    iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>");
                    if (bEmptyBody)
                    {
                        // always tag main body if there's more than one section
                        if (iSectionCount > 1)
                        {
                            bEmptyBody = false;
                        }
                        else if (iTxBxStart < 0)
                        {
                            // check for existence of text, field, extra paragraphs or applied formatting
                            // in Section XML
                            // if only doing main story, always tag body, even if blank
                            bEmptyBody = bDoAllStories && ((xStoryTemp.IndexOf("<w:p>") < 0 &&
                                xStoryTemp.IndexOf("<w:t>") < 0 &&
                                xStoryTemp.IndexOf("<w:pPr>") < 0 &&
                                xStoryTemp.IndexOf("<w:fld") < 0) &&
                                xStoryTemp.IndexOf("<w:tbl>") < 0);
                        }
                    }
                    while (iTxBxStart > 0)
                    {
                        if (bEmptyBody)
                        {
                            // check for existence of text, field, or applied formatting
                            // between textbox stories
                            bEmptyBody = ((xStoryTemp.IndexOf("<w:t>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:pPr>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:fld", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:tbl>", iPos, iTxBxStart - 1 - iPos) < 0));
                        }
                        iTxBxEnd = xStoryTemp.IndexOf("</w:txbxContent>", iTxBxStart);
                        // Insert closing tag before end of textbox
                        xStoryTemp = xStoryTemp.Insert(iTxBxEnd, xmSEGClose);
                        // locate end of opening tag - mSEG follows immediately after
                        iTxBxStart = xStoryTemp.IndexOf(">", iTxBxStart);
                        // insert opening tag, replacing token and incrementing Part Count
                        xCurTag = string.Format(xmSEGFull, ++iPartCount);
                        if (iPartCount == 1)
                        {
                            //Bookmark first mSEG tag
                            xCurTag = string.Concat(xCurTag, xBookmarkTag);
                        }
                        xStoryTemp = xStoryTemp.Insert(iTxBxStart + 1, xCurTag);
                        iFoundStories++;
                        iPos = iTxBxEnd + xCurTag.Length + xmSEGClose.Length + 1;
                        //search for next textbox
                        iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>", iPos);
                    }
                    if (iPos > 0 && bEmptyBody)
                    {
                        // check for existence of text, field, or applied formatting
                        // after last textbox story
                        bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos) < 0) &&
                            (xStoryTemp.IndexOf("<w:pPr>", iPos) < 0) &&
                            (xStoryTemp.IndexOf("<w:fld", iPos) < 0));
                    }
                    // Replace original XML with modified string
                    xTemp = xTemp.Remove(iStoryStart, iStoryEnd - iStoryStart);
                    xTemp = xTemp.Insert(iStoryStart, xStoryTemp);
                    iStoryStart = xTemp.IndexOf("<wx:sect>", iStoryStart + xStoryTemp.Length);
                }
                //GLOG 3541: Set end position to before last <w:sectPr> tag
                //If the document contains continuous sections, there may be multiple such
                //tags after the last <wx:sect> tag
                iStoryEnd = xTemp.LastIndexOf("<w:sectPr");
                // Body is not empty
                if (!bEmptyBody)
                {
                    // locate end of opening tag for first section - mSEG follows immediately after
                    iPos = xTemp.IndexOf(">", iBodyStart);

                    // if single section doc starts with a table, check whether there's
                    // anything else in the body other than the trailing paragraph mark -
                    // if not, move closing tag directly after table closing tag
                    if ((iSectionCount == 1) && (xTemp.Substring(iPos + 1, 7) == "<w:tbl>"))
                    {
                        // get text without headers and footers
                        int iEnd = xTemp.IndexOf("<w:sectPr");
                        string xSection = xTemp.Substring(0, iEnd);

                        // Count number of tables in story
                        int iTableCount = String.CountChrs(xSection, "<w:tbl>");
                        if (iTableCount == 1)
                        {
                            int iTableEnd = xSection.IndexOf("</w:tbl>");
                            if (iTableEnd > 0)
                            {
                                // There will always be at least one <w:p> tag,
                                // even if entire selection was within table
                                // if there is only one '<w:p ' and no '<w:r>' tags after table
                                // move position of closing mSeg tab directly after '</w:tbl>'
                                if (String.CountChrs(xSection.Substring(iTableEnd), "<w:p ") == 1 &&
                                    xSection.IndexOf("<w:r>", iTableEnd) == -1)
                                    iStoryEnd = iTableEnd + 8;
                            }
                        }
                    }

                    // insert closing tag at end of last section
                    xTemp = xTemp.Insert(iStoryEnd, xmSEGClose);
                    // insert opening tag, replacing token and incrementing Part Count
                    xCurTag = string.Format(xmSEGFull, ++iPartCount);
                    if (iPartCount == 1)
                    {
                        //Bookmark first mSEG tag
                        xCurTag = string.Concat(xCurTag, xBookmarkTag);
                    }
                    xTemp = xTemp.Insert(iPos + 1, xCurTag);
                    iFoundStories++;
                }
                if (bDoAllStories)
                // Check for Headers and Footers
                {
                    foreach (string xStory in new string[] { "w:hdr", "w:ftr" })
                    {
                        // Search for header stories
                        iStoryStart = xTemp.IndexOf("<" + xStory + " w:type");
                        while (iStoryStart > 0)
                        {
                            bEmptyStory = true;
                            iPos = 0;
                            // find end of section
                            iStoryEnd = xTemp.IndexOf("</" + xStory + ">", iStoryStart);
                            xStoryTemp = xTemp.Substring(iStoryStart, iStoryEnd - iStoryStart);
                            // If Header/Footer already contains an external child segment, don't 
                            // Wrap it in a new mSEG
                            if (!xStoryTemp.Contains(xmSEGStart))
                            {
                                // look for any textboxes
                                iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>");
                                if (bEmptyStory && (iTxBxStart < 0))
                                {
                                    // check for existence of text or field
                                    // in Header XML
                                    bEmptyStory = (xStoryTemp.IndexOf("<w:t>") < 0 &&
                                        xStoryTemp.IndexOf("<w:fld") < 0);
                                }
                                while (iTxBxStart > 0)
                                {
                                    if (bEmptyStory)
                                    {
                                        // check for existence of text or fields
                                        // between textbox stories
                                        bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                            (xStoryTemp.IndexOf("<w:fld", iPos, iTxBxStart - 1 - iPos) < 0));
                                    }
                                    iTxBxEnd = xStoryTemp.IndexOf("</w:txbxContent>", iTxBxStart);
                                    // Insert closing tag before end of textbox
                                    xStoryTemp = xStoryTemp.Insert(iTxBxEnd, xmSEGClose);
                                    // locate end of opening tag - mSEG follows immediately after
                                    iTxBxStart = xStoryTemp.IndexOf(">", iTxBxStart);
                                    // insert opening tag, replacing token and incrementing Part Count
                                    xCurTag = string.Format(xmSEGFull, ++iPartCount);
                                    if (iPartCount == 1)
                                    {
                                        //Bookmark first mSEG tag
                                        xCurTag = string.Concat(xCurTag, xBookmarkTag);
                                    }
                                    xStoryTemp = xStoryTemp.Insert(iTxBxStart + 1, xCurTag);
                                    iFoundStories++;
                                    iPos = iTxBxEnd + xCurTag.Length + xmSEGClose.Length + 1;
                                    //search for next textbox
                                    iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>", iPos);
                                }
                                if (iPos > 0 && bEmptyStory)
                                {
                                    // check for existence of text or fields
                                    // after last textbox story
                                    bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos) < 0) &&
                                        (xStoryTemp.IndexOf("<w:fld", iPos) < 0));
                                }
                                // Replace original XML with modified string
                                xTemp = xTemp.Remove(iStoryStart, iStoryEnd - iStoryStart);
                                xTemp = xTemp.Insert(iStoryStart, xStoryTemp);
                                iStoryEnd = iStoryStart + xStoryTemp.Length;
                                // Header is not empty
                                if (!bEmptyStory)
                                {
                                    // insert closing tag at end of last section
                                    xTemp = xTemp.Insert(iStoryEnd, xmSEGClose);
                                    // locate end of opening tag for first section - mSEG follows immediately after
                                    iPos = xTemp.IndexOf(">", iStoryStart);
                                    // insert opening tag, replacing token and incrementing Part Count
                                    xCurTag = string.Format(xmSEGFull, ++iPartCount);
                                    if (iPartCount == 1)
                                    {
                                        //Bookmark first mSEG tag
                                        xCurTag = string.Concat(xCurTag, xBookmarkTag);
                                    }
                                    xTemp = xTemp.Insert(iPos + 1, xCurTag);
                                    iFoundStories++;
                                }
                            }
                            iStoryStart = xTemp.IndexOf("<" + xStory + " w:type", iStoryStart + xStoryTemp.Length);
                        }
                    }
                }

                //12-15-10 (dm): all wx:sect tags are now removed below -
                //this is necessary in Word 2010
                //if (iSectionCount > 1)
                //{
                //    // Need to remove all wx:sect tags between first opening and last closing for valid XML
                //    xTemp = xTemp.Replace("</wx:sect><wx:sect>", "");
                //}
                // No content in any stories, tag main body story
                if (iFoundStories == 0)
                {
                    // find end of section
                    iBodyEnd = xTemp.LastIndexOf("<w:sectPr", xTemp.Length);
                    // locate end of opening tag for first section - mSEG follows immediately after
                    iPos = xTemp.IndexOf(">", iBodyStart);
                    // insert closing tag at end of last section
                    xTemp = xTemp.Insert(iBodyEnd, xmSEGClose);
                    // insert opening tag, replacing token and incrementing Part Count
                    xCurTag = string.Format(xmSEGFull, 1);
                    //Bookmark first mSEG tag
                    xCurTag = string.Concat(xCurTag, xBookmarkTag);
                    xTemp = xTemp.Insert(iPos + 1, xCurTag);
                }
            }
            catch
            {
                // On Error Restore original value
                xTemp = xSource;
            }

            //remove auxiliary sub-section nodes, which Word infers from heading levels -
            //these are of no use to Word and can cause errors when inserting the xml
            xTemp = xTemp.Replace("<wx:sub-section>", "");
            xTemp = xTemp.Replace("</wx:sub-section>", "");
            xTemp = xTemp.Replace("<wx:sect>", ""); //12-15-10 (dm)
            xTemp = xTemp.Replace("</wx:sect>", ""); //12-15-10 (dm)
            //10.2: Add required bookmarks, Document Variables and Reserved Attribute to XML
            xTemp = LMP.Conversion.PreconvertIfNecessary(xTemp, false);
            return xTemp.Trim();
        }

        /// <summary>
        /// returns the required content controls for new MacPac Segment
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xTagID"></param>
        /// <param name="xObjectData"></param>
        /// <param name="bDoAllStories"></param>
        /// <returns></returns>
        public static string GetFormattedNewSegmentOpenXML(string xSource, string xTagID, string xObjectData, bool bDoAllStories)
        {
            const string mpCCNodeClose = "</w:sdtContent></w:sdt>";
            string xTemp = xSource;
            //AlternateContent XML for Textboxes containing Graphics can result 
            //in portions of graphic being cut off.  To avoid, remove AlternatContent nodes
            //and replace with contents of <w:pict> nodes contained within.
            int iStart = xTemp.IndexOf("<mc:AlternateContent");
            while (iStart > -1)
            {
                int iEnd = xTemp.IndexOf("</mc:AlternateContent>", iStart);
                if (iEnd > -1)
                {
                    string xPictXML = xTemp.Substring(iStart, iEnd - iStart);
                    int iPictStart = xPictXML.IndexOf("<w:pict>");
                    if (iPictStart > -1)
                    {
                        int iPictEnd = xPictXML.IndexOf("</w:pict>");
                        if (iPictEnd > -1)
                        {
                            xPictXML = xPictXML.Substring(iPictStart, (iPictEnd + @"</w:pict>".Length) - iPictStart);
                            xTemp = xTemp.Substring(0, iStart) + xPictXML + xTemp.Substring(iEnd + @"</mc:AlternateContent>".Length);
                        }
                    }
                }
                iStart = xTemp.IndexOf("<mc:AlternateContent>", iStart + 1);
            }

            try
            {
                //get segment id
                int iPos = xObjectData.IndexOf("SegmentID=") + 10;
                int iPos2 = xObjectData.IndexOf('|', iPos);
                string xID = xObjectData.Substring(iPos, iPos2 - iPos);

                //pad start to ensure 19 characters
                xID = new string('0', 19 - xID.Length) + xID;

                //bookmark names can only contain alphanumeric characters -
                //replace negative sign and dot if necessary
                xID = xID.Replace('-', 'm');
                xID = xID.Replace('.', 'd');

                //construct doc varvalue (w/o part number)
                string xDocVarValue = xTagID + "��ObjectData=" + xObjectData +
                    "��PartNumber=";

                string xCC = "";
                int iPartCount = 1;
                int iStoryStart = 0;
                int iBodyStart = 0;
                int iStoryEnd = 0;
                int iTxBxStart = 0;
                int iTxBxEnd = 0;
                int iSectionCount = 0;
                int iFoundStories = 0;
                string xStoryTemp = "";
                bool bEmptyStory = true;
                bool bEmptyBody = true;
                string xDocVarID = "";

                //mSEG starts directly after body tag
                iStoryStart = xTemp.IndexOf("<w:body>");

                // save first section start for tagging if necessary
                iBodyStart = iStoryStart;
                while (iStoryStart > 0)
                {
                    iSectionCount++;
                    iPos = 0;
                    // find end of section
                    iStoryEnd = xTemp.IndexOf("<w:sectPr", iStoryStart);
                    xStoryTemp = xTemp.Substring(iStoryStart, iStoryEnd - iStoryStart);
                    // look for any textboxes
                    iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>");
                    if (bEmptyBody)
                    {
                        // always tag main body if there's more than one section
                        if (iSectionCount > 1)
                        {
                            bEmptyBody = false;
                        }
                        else if (iTxBxStart < 0)
                        {
                            // check for existence of text, field, extra paragraphs or applied formatting
                            // in Section XML
                            // if only doing main story, always tag body, even if blank
                            bEmptyBody = bDoAllStories && ((xStoryTemp.IndexOf("<w:p>") < 0 &&
                                xStoryTemp.IndexOf("<w:t>") < 0 &&
                                xStoryTemp.IndexOf("<w:pPr>") < 0 &&
                                xStoryTemp.IndexOf("<w:fld") < 0) &&
                                xStoryTemp.IndexOf("<w:tbl>") < 0);
                        }
                    }
                    while (iTxBxStart > 0)
                    {
                        if (bEmptyBody)
                        {
                            // check for existence of text, field, or applied formatting
                            // between textbox stories
                            bEmptyBody = ((xStoryTemp.IndexOf("<w:t>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:pPr>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:fld", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                (xStoryTemp.IndexOf("<w:tbl>", iPos, iTxBxStart - 1 - iPos) < 0));
                        }
                        iTxBxEnd = xStoryTemp.IndexOf("</w:txbxContent>", iTxBxStart);

                        // Insert closing tag before end of textbox
                        xStoryTemp = xStoryTemp.Insert(iTxBxEnd, mpCCNodeClose);

                        // locate end of opening tag - mSEG follows immediately after
                        iTxBxStart = xStoryTemp.IndexOf(">", iTxBxStart);

                        // insert opening tag
                        xCC = GetNewmSEGStartXML(xID, (iPartCount == 1), out xDocVarID);
                        xStoryTemp = xStoryTemp.Insert(iTxBxStart + 1, xCC);

                        //insert doc var
                        AddDocVarToSegmentXML(ref xTemp, "mpo" + xDocVarID,
                            xDocVarValue + iPartCount.ToString());

                        iPartCount++;
                        iFoundStories++;

                        //search for next textbox
                        iPos = iTxBxEnd + xCC.Length + mpCCNodeClose.Length + 1;
                        iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>", iPos);
                    }
                    if (iPos > 0 && bEmptyBody)
                    {
                        // check for existence of text, field, or applied formatting
                        // after last textbox story
                        bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos) < 0) &&
                            (xStoryTemp.IndexOf("<w:pPr>", iPos) < 0) &&
                            (xStoryTemp.IndexOf("<w:fld", iPos) < 0));
                    }
                    // Replace original XML with modified string
                    xTemp = xTemp.Remove(iStoryStart, iStoryEnd - iStoryStart);
                    xTemp = xTemp.Insert(iStoryStart, xStoryTemp);
                    iStoryStart = xTemp.IndexOf("<wx:sect>", iStoryStart + xStoryTemp.Length);
                }
                //GLOG 3541: Set end position to before last <w:sectPr> tag
                //If the document contains continuous sections, there may be multiple such
                //tags after the last <wx:sect> tag
                iStoryEnd = xTemp.LastIndexOf("<w:sectPr");
                // Body is not empty
                if (!bEmptyBody)
                {
                    // locate end of opening tag for first section - mSEG follows immediately after
                    iPos = xTemp.IndexOf(">", iBodyStart);

                    // if single section doc starts with a table, check whether there's
                    // anything else in the body other than the trailing paragraph mark -
                    // if not, move closing tag directly after table closing tag
                    if ((iSectionCount == 1) && (xTemp.Substring(iPos + 1, 7) == "<w:tbl>"))
                    {
                        // get text without headers and footers
                        int iEnd = xTemp.IndexOf("<w:sectPr");
                        string xSection = xTemp.Substring(0, iEnd);

                        // Count number of tables in story
                        int iTableCount = String.CountChrs(xSection, "<w:tbl>");
                        if (iTableCount == 1)
                        {
                            int iTableEnd = xSection.IndexOf("</w:tbl>");
                            if (iTableEnd > 0)
                            {
                                // There will always be at least one <w:p> tag,
                                // even if entire selection was within table
                                // if there is only one '<w:p ' and no '<w:r>' tags after table
                                // move position of closing mSeg tab directly after '</w:tbl>'
                                if (String.CountChrs(xSection.Substring(iTableEnd), "<w:p ") == 1 &&
                                    xSection.IndexOf("<w:r>", iTableEnd) == -1)
                                    iStoryEnd = iTableEnd + 8;
                            }
                        }
                    }

                    //insert close of content control node at end of last section
                    xTemp = xTemp.Insert(iStoryEnd, mpCCNodeClose);

                    //insert start of content control node
                    xCC = GetNewmSEGStartXML(xID, (iPartCount == 1), out xDocVarID);
                    xTemp = xTemp.Insert(iPos + 1, xCC);

                    //insert doc var
                    AddDocVarToSegmentXML(ref xTemp, "mpo" + xDocVarID,
                        xDocVarValue + iPartCount.ToString());

                    iPartCount++;
                    iFoundStories++;
                }
                if (bDoAllStories)
                // Check for Headers and Footers
                {
                    foreach (string xStory in new string[] { "w:hdr", "w:ftr" })
                    {
                        // Search for header stories
                        iStoryStart = xTemp.IndexOf("<" + xStory + " ");
                        while (iStoryStart > 0)
                        {
                            bEmptyStory = true;
                            iPos = 0;
                            // find end of section
                            iStoryEnd = xTemp.IndexOf("</" + xStory + ">", iStoryStart);
                            xStoryTemp = xTemp.Substring(iStoryStart, iStoryEnd - iStoryStart);
                            // If Header/Footer already contains an external child segment, don't 
                            // Wrap it in a new mSEG
                            if (!xStoryTemp.Contains("w:tag w:val=\"mps"))
                            {
                                // look for any textboxes
                                iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>");
                                if (bEmptyStory && (iTxBxStart < 0))
                                {
                                    // check for existence of text or field
                                    // in Header XML
                                    bEmptyStory = (xStoryTemp.IndexOf("<w:t>") < 0 &&
                                        xStoryTemp.IndexOf("<w:fld") < 0);
                                }
                                while (iTxBxStart > 0)
                                {
                                    if (bEmptyStory)
                                    {
                                        // check for existence of text or fields
                                        // between textbox stories
                                        bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos, iTxBxStart - 1 - iPos) < 0) &&
                                            (xStoryTemp.IndexOf("<w:fld", iPos, iTxBxStart - 1 - iPos) < 0));
                                    }
                                    iTxBxEnd = xStoryTemp.IndexOf("</w:txbxContent>", iTxBxStart);

                                    // Insert closing tag before end of textbox
                                    xStoryTemp = xStoryTemp.Insert(iTxBxEnd, mpCCNodeClose);

                                    // locate end of opening tag - mSEG follows immediately after
                                    iTxBxStart = xStoryTemp.IndexOf(">", iTxBxStart);

                                    // insert opening tag, replacing token and incrementing Part Count
                                    xCC = GetNewmSEGStartXML(xID, (iPartCount == 1), out xDocVarID);
                                    xStoryTemp = xStoryTemp.Insert(iTxBxStart + 1, xCC);

                                    //insert doc var
                                    AddDocVarToSegmentXML(ref xTemp, "mpo" + xDocVarID,
                                        xDocVarValue + iPartCount.ToString());

                                    iPartCount++;
                                    iFoundStories++;

                                    //search for next textbox
                                    iPos = iTxBxEnd + xCC.Length + mpCCNodeClose.Length + 1;
                                    iTxBxStart = xStoryTemp.IndexOf("<w:txbxContent>", iPos);
                                }
                                if (iPos > 0 && bEmptyStory)
                                {
                                    // check for existence of text or fields
                                    // after last textbox story
                                    bEmptyStory = ((xStoryTemp.IndexOf("<w:t>", iPos) < 0) &&
                                        (xStoryTemp.IndexOf("<w:fld", iPos) < 0));
                                }
                                // Replace original XML with modified string
                                xTemp = xTemp.Remove(iStoryStart, iStoryEnd - iStoryStart);
                                xTemp = xTemp.Insert(iStoryStart, xStoryTemp);
                                iStoryEnd = iStoryStart + xStoryTemp.Length;
                                // Header is not empty
                                if (!bEmptyStory)
                                {
                                    //insert close of content control node at end of last section
                                    xTemp = xTemp.Insert(iStoryEnd, mpCCNodeClose);

                                    // locate end of opening tag for first section - mSEG follows immediately after
                                    iPos = xTemp.IndexOf(">", iStoryStart);

                                    //insert start of content control node
                                    xCC = GetNewmSEGStartXML(xID, (iPartCount == 1), out xDocVarID);
                                    xTemp = xTemp.Insert(iPos + 1, xCC);

                                    //insert doc var
                                    AddDocVarToSegmentXML(ref xTemp, "mpo" + xDocVarID,
                                        xDocVarValue + iPartCount.ToString());

                                    iPartCount++;
                                    iFoundStories++;
                                }
                            }
                            iStoryStart = xTemp.IndexOf("<" + xStory + " ", iStoryStart + xStoryTemp.Length);
                        }
                    }
                }

                if (iSectionCount > 1)
                {
                    // Need to remove all wx:sect tags between first opening and last closing for valid XML
                    xTemp = xTemp.Replace("</wx:sect><wx:sect>", "");
                }

                // No content in any stories, tag main body story
                if (iFoundStories == 0)
                {
                    // find body start
                    int iBodyTagStart = xTemp.IndexOf("<w:body>");

                    // locate end of opening tag for first section - mSEG follows immediately after
                    iPos = iBodyTagStart + "<w:body>".Length;

                    // insert opening tag, replacing token and incrementing Part Count
                    xCC = GetNewmSEGStartXML(xID, (iPartCount == 1), out xDocVarID) + mpCCNodeClose;
                    xTemp = xTemp.Insert(iPos, xCC);

                    //insert doc var
                    AddDocVarToSegmentXML(ref xTemp, "mpo" + xDocVarID,
                        xDocVarValue + iPartCount.ToString());
                }
            }
            catch
            {
                // On Error Restore original value
                xTemp = xSource;
            }

            //remove auxiliary sub-section nodes, which Word infers from heading levels -
            //these are of no use to Word and can cause errors when inserting the xml
            xTemp = xTemp.Replace("<wx:sub-section>", "");
            xTemp = xTemp.Replace("</wx:sub-section>", "");

            return xTemp.Trim();
        }
        /// <summary>
        /// inserts the specified segment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the segment definition</param>
        /// <param name="oLocation">the location to insert the segment</param>
        /// <param name="iInsertionOptions">options available for segment insertion</param>
        /// <param name="oMPDocument">MacPac document representing the Word document into which the segment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill segment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        /// <returns></returns>
        internal static Segment Insert(string xID, Segment oParent, Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, ForteDocument oMPDocument,
            Prefill oPrefill, bool bUseTargetedXMLInsertion, string xXML)
        {
            return Insert(xID, oParent, iInsertionLocation, iInsertionBehavior, oMPDocument, oPrefill,
                bUseTargetedXMLInsertion, true, xXML, false, false, false, null);
        }
        //GLOG 6021
        internal static Segment Insert(string xID, Segment oParent, Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, ForteDocument oMPDocument,
            Prefill oPrefill, bool bUseTargetedXMLInsertion, bool bInsertingInMainStory,
            string xXML, bool bUpdateAllStyles, bool bForcePrefillOverride, bool bAttachToTemplate,
            CollectionTableStructure oChildStructure)
        {
            return Insert(xID, oParent, iInsertionLocation, iInsertionBehavior, oMPDocument, oPrefill,
                bUseTargetedXMLInsertion, bInsertingInMainStory, xXML, bUpdateAllStyles, bForcePrefillOverride, bAttachToTemplate,
                oChildStructure, null);
        }
        /// <summary>
        /// inserts the specified segment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the segment definition</param>
        /// <param name="oLocation">the location to insert the segment</param>
        /// <param name="iInsertionOptions">options available for segment insertion</param>
        /// <param name="oMPDocument">MacPac document representing the Word document into which the segment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill segment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="InsertingInHeaderFooter">Set to true by ContentManager when inserting in a Header/Footer</param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        /// <returns></returns>
        internal static Segment Insert(string xID, Segment oParent, Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, ForteDocument oMPDocument,
            Prefill oPrefill, bool bUseTargetedXMLInsertion, bool bInsertingInMainStory,
            string xXML, bool bUpdateAllStyles, bool bForcePrefillOverride, bool bAttachToTemplate,
            CollectionTableStructure oChildStructure, ArrayList aSections) //GLOG 6021:  Additional parameter to specify sections for insertion
        {
            DateTime t0 = DateTime.Now;

            bool bTempParaCreated = false;
            bool bInsertedAtEnd = false; //GLOG 7827
            Segment oSegment = null;
            Segment oContentSegment = null; //GLOG 6920
            ISegmentDef oContentDef = null; //GLOG 6920
            object oXMLArray = null; //GLOG 7876
            //GLOG 4418
            string[] xTrailerXML = null;
            bool bUseQuickTrailerInsertion = false;
            int iTrailerIndex = 0;
            bool bOneInserted = false;
            string xSegmentName = "";
            string xTagID = "";
            string xScheme = ""; //GLOG 8343
            bool bContainsMPNumbering = false; //GLOG 8343
            string xSchemes = ""; //GLOG 8343

            //GLOG #5925 CEH
            //turn off track changes if necessary
            bool bTrackChanges = oMPDocument.WordDocument.TrackRevisions;
            if (bTrackChanges)
                oMPDocument.WordDocument.TrackRevisions = false;

            //temporary only - seeing if we can get rid of the bDesign parameter
            bool bDesign = oMPDocument.Mode == ForteDocument.Modes.Design;

            //get the segment def for this segment
            ISegmentDef oDef = Segment.GetDefFromID(xID);

            //oUndo = new LMP.Forte.MSWord.Undo();
            //oUndo.MarkStart("Segment Insertion - " + oDef.DisplayName);

            Word.Document oWordDoc = oMPDocument.WordDocument;

            short shLocation;

            //if no location has been specified, use the default location
            if (iInsertionLocation == 0)
                shLocation = (short)InsertionLocations.Default;
            else
                shLocation = (short)iInsertionLocation;

            //delete previous existing instances
            //of single instance segments
            oSegment = CreateSegmentObject(oDef.TypeID, oMPDocument);
            oSegment.Definition = oDef;
            //GLOG 6913: Section below moved up
            //GLOG 6799 (dm) - moved this block up for new segment bookmark validation
            //GLOG 6920: Moved up again so that appropriate InsertXML override can be called
            bool bInsertingSavedContent = oDef.TypeID == mpObjectTypes.SavedContent;
            string xValidationID = xID;
            if (bInsertingSavedContent)
            {
                //GLOG 6920:  If Saved Segment, get Segment Definition from XML
                //this is saved content - segment needs to be
                //switched over to segment of contained xml
                string xContainedID = oDef.ChildSegmentIDs;
                if (!xContainedID.Contains(".") || xContainedID.EndsWith(".0"))
                {
                    //this is an admin segment
                    int iID1;
                    int iID2;
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    LMP.Data.Application.SplitID(xContainedID, out iID1, out iID2);
                    oContentDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                    oContentSegment = CreateSegmentObject(oContentDef.TypeID, oMPDocument);
                    oContentSegment.Definition = oContentDef;
                }
                else
                {
                    //this is a user segment
                    //GLOG 4728: Filter collection by User to include accessible objects
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                    oContentDef = (UserSegmentDef)oDefs.ItemFromID(xContainedID);
                    oContentSegment = CreateSegmentObject(oContentDef.TypeID, oMPDocument);
                    oContentSegment.Definition = oContentDef;
                }
                xValidationID = xContainedID; //GLOG 6799 (dm)
            }
            else
            {
                oContentSegment = oSegment;
                oContentDef = oDef;
                oContentSegment.Definition = oDef;
            }
            //GLOG item #4734 - dcf
            //get specific insertion behavior if insertion behavior is
            //set to default
            if (iInsertionBehavior == InsertionBehaviors.Default)
                iInsertionBehavior = GetDefaultInsertionBehaviors(oContentDef.TypeID);


            //GLOG 6799 (dm, 5/30/13)
            if (xValidationID.EndsWith(".0"))
                xValidationID = xValidationID.Replace(".0", "");
            LMP.Benchmarks.Print(t0, "Stage 1");
            //GLOG 6021
            object[] oSections = new object[0];
            if (aSections != null)
                oSections = aSections.ToArray();
            //GLOG 6920:  Where appropriate below, we should be checking properties of oContentSegment in case oSegment is Saved Content
            if (oContentSegment is ISingleInstanceSegment && aSections == null) //GLOG 6021: Don't check this if called from Trailer dialog
            {
                //GLOG 6820: Don't need to preserve Segment bookmark here, since content is only being deleted
                System.Array aInsertionLoc = LMP.Forte.MSWord.WordDoc.GetInsertionLocations(oWordDoc, shLocation, oSections, false); //GLOG 6021

                Segments oExistingSegments = null;
                foreach (Word.Range oLocation in aInsertionLoc)
                {
                    Word.Range oInsertRange = oLocation.Duplicate;
                    oExistingSegments = ((ISingleInstanceSegment)oContentSegment)
                        .GetExistingSegments(oWordDoc.Sections.First);
                    if (oExistingSegments != null)
                    {
                        for (int i = oExistingSegments.Count - 1; i >= 0; i--)
                        {
                            int iSections = oWordDoc.Sections.Count; // oContentSegment.ForteDocument.WordDocument.Sections.Count;
                            oContentSegment.ForteDocument.DeleteSegment(oExistingSegments.ItemFromIndex(i));
                            //GLOG 4862: If number of sections has changed, Refresh Tags
                            //as XMLNodes in Headers/Footers might have changed
                            if (iSections > oWordDoc.Sections.Count) // oContentSegment.ForteDocument.WordDocument.Sections.Count)
                                oContentSegment.ForteDocument.Refresh(bDesign ?
                                    ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes :
                                    ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes);

                        }
                    }
                }
            }
            //deal with insertion location
            //GLOG 6820:  Temp paragraph doesn't need to be inserted for trailers
            System.Array aInsertionLocations = LMP.Forte.MSWord.WordDoc.GetInsertionLocations(oWordDoc, shLocation, oSections, oContentDef.TypeID != mpObjectTypes.Trailer, (int)oContentDef.TypeID); //GLOG 6021 //GLOG 7608 //GLOG 7548
            foreach (Word.Range oLocation in aInsertionLocations)
            {
                //GLOG 15965 (dm) - range may no longer exist when reinserting trailers after recreating a multisection segment
                if (oLocation != null)
                {
                    //GLOG 6913: If oSegment cleared by Trailer insertion, recreate new object
                    //GLOG 7000: Reinitialize Segment object on each pass, so that Nodes will reflect latest state of document
                    if (bOneInserted)
                    {
                        oSegment = CreateSegmentObject(oContentDef.TypeID, oMPDocument);
                        oSegment.Definition = oContentDef;
                    }
                    Word.Range oInsertRange = oLocation.Duplicate;
                    int iSection = oLocation.Sections[1].Index; //GLOG 6021
                    //GLOG 5472: If Trailer or Draft Stamp, skip insertion after first section if all stories used by Content are linked
                    if (bOneInserted && (oContentDef.TypeID == mpObjectTypes.Trailer || oContentDef.TypeID == mpObjectTypes.DraftStamp))
                    {
                        Word.Section oCurSection = oInsertRange.Sections[1];
                        bool bLinkedFirstFooter = false;
                        bool bLinkedFirstHeader = false;
                        bool bLinkedPrimaryFooter = false;
                        bool bLinkedPrimaryHeader = false;
                        bool bLinkedEvenFooter = false;
                        bool bLinkedEvenHeader = false;

                        bool bSectionContainsLinkedStories = LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(oCurSection,
                            ref bLinkedPrimaryFooter, ref bLinkedPrimaryHeader, ref bLinkedFirstFooter, ref bLinkedFirstHeader,
                            ref bLinkedEvenFooter, ref bLinkedEvenHeader, true, false);
                        if (bSectionContainsLinkedStories)
                        {
                            string xSegXML = oContentDef.XML;
                            string xBodyXML = LMP.String.ExtractBodyXML(xSegXML);

                            //This can appear 2 ways, depending on which version of Word was used to save
                            xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                                "<w:body><w:p/><w:sectPr>");
                            xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                                "<w:body><wx:sect><w:p/><w:sectPr>");

                            //GLOG 5403 (dm) - the preconversion process adds a space to the w:br node
                            if (!LMP.String.IsWordOpenXML(xSegXML))
                            {
                                xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                                    "<w:body><w:p/><w:sectPr>");
                                xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                                    "<w:body><wx:sect><w:p/><w:sectPr>");
                            }
                            //GLOG 5472: Skip if Segment has empty body, and all Headers and Footers used by content are linked in current section
                            if (!LMP.String.ContentContainsUnlinkedStories(oDef.XML, bLinkedPrimaryFooter, bLinkedPrimaryHeader,
                                bLinkedFirstFooter, bLinkedFirstHeader, bLinkedEvenFooter, bLinkedEvenHeader) &&
                                (xBodyXML.IndexOf("<w:body><w:p/><w:sectPr>") > -1 || xBodyXML.IndexOf("<w:body><w:p /><w:sectPr>") > -1 ||
                                xBodyXML.IndexOf("<w:body><wx:sect><w:p/><w:sectPr>") > -1 || xBodyXML.IndexOf("<w:body><w:p /><w:sectPr>") > -1))
                                continue;
                        }
                    }
                    //GLOG 7876: Insert finished Trailer XML directly after first section
                    if (bUseQuickTrailerInsertion && oXMLArray != null)
                    {
                        LMP.Data.FirmApplicationSettings oFirmSettings =
                            new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
                        if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains("CONVERTTRAILERSTO9XFORMAT") ||
                            oFirmSettings.UseMacPac9xStyleTrailer)
                        {
                            LMP.Forte.MSWord.WordDoc.InsertConverted9xTrailerXMLAtLocation(oLocation, ref oXMLArray);
                        }
                        else
                        {
                            LMP.Forte.MSWord.WordDoc.InsertTrailerXMLAtLocation(oLocation, ref oXMLArray);
                        }
                        continue;
                    }
                    //insert wrapper segment if specified and not already wrapped
                    Segment oInsertedChild = null;
                    InsertWrapperIfNecessary(xID, oDef, ref oParent, ref oInsertRange,
                        ref iInsertionBehavior, oMPDocument, oPrefill, bUseTargetedXMLInsertion,
                        bInsertingInMainStory, ref oInsertedChild);

                    if (oInsertedChild != null)
                        //target segment is included in definition of wrapper segment,
                        //so was already inserted along with it
                        return oInsertedChild;

                    //deal with insertion behavior
                    bool bInsertInNextPageSection = (iInsertionBehavior &
                        InsertionBehaviors.InsertInSeparateNextPageSection) ==
                        InsertionBehaviors.InsertInSeparateNextPageSection;

                    bool bInsertInContinuousSection = (iInsertionBehavior &
                        InsertionBehaviors.InsertInSeparateContinuousSection) ==
                        InsertionBehaviors.InsertInSeparateContinuousSection;

                    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType;

                    if (oContentDef.IntendedUse != mpSegmentIntendedUses.AsDocument && !(oContentSegment is Paper))
                        //segment is not a document segment and is not paper -
                        //don't remove header content
                        if (oContentSegment is IDocumentStamp)
                        {
                            //GLOG 4243: Set Header/Footer Insertion option depending on whether
                            //Stamp is being inserted in single section only, or entire document
                            if (aInsertionLocations.Length > 1)
                                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Append;
                            else
                                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_ReplaceSameType;
                        }
                        else
                            iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
                    else
                    {
                        //segment is a document segment or is paper -
                        //set header/footer insertion behavior based on
                        //specified insertion behavior - if none specified, set behavior
                        //according to intended use - 
                        //If Segment is inserted in a header/footer, only insert main body XML
                        if ((iInsertionBehavior & InsertionBehaviors.KeepExistingHeadersFooters) > 0 ||
                            ((iInsertionLocation == InsertionLocations.InsertAtSelection)
                            || (iInsertionLocation == InsertionLocations.Default)) && !bInsertingInMainStory)
                            iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
                        else if (((iInsertionBehavior & InsertionBehaviors.KeepExistingHeadersFooters) == 0) &&
                            ((iInsertionBehavior & InsertionBehaviors.InsertInSeparateNextPageSection) > 0))
                            //Replace existing header/footers if not explicitly set
                            iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace;
                        else
                        {
                            iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion
                                .mpHeaderFooterInsertion_ReplaceSameOrNoType;
                        }
                    }
                    LMP.Benchmarks.Print(t0, "Stage 2");
                    //GLOG - 3590 - CEH
                    if (bInsertInNextPageSection || bInsertInContinuousSection)
                    {
                        bInsertedAtEnd = false; //GLOG 7827
                        //ensure that markup is showing
                        if (oMPDocument.FileFormat == mpFileFormats.Binary)
                            oWordDoc.ActiveWindow.View.ShowXMLMarkup = -1;

                        //insert section at location - 
                        //GLOG #3989 - dcf -
                        //some prep work needs to be done if inserting
                        //in a section at either start or end of a document
                        object oMissing = System.Reflection.Missing.Value;
                        if (oInsertRange.Tables.Count > 0 &&
                            (iInsertionLocation == InsertionLocations.InsertAtStartOfAllSections ||
                            iInsertionLocation == InsertionLocations.InsertAtStartOfCurrentSection ||
                            iInsertionLocation == InsertionLocations.InsertAtStartOfDocument))
                        {
                            Word.Table oTable = oInsertRange.Tables[1];
                            LMP.Forte.MSWord.WordDoc.AddParaBeforeTable(oTable);
                            object oParaRng = oInsertRange.Paragraphs[1].Range;
                            oWordDoc.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827
                            //oSegment.ForteDocument.WordDocument.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827

                            object oCount = 1;
                            oInsertRange = oInsertRange.Paragraphs[1].Next(ref oCount).Range;
                            oInsertRange = (Word.Range)oParaRng;
                            oInsertRange.StartOf(ref oMissing, ref oMissing);
                            bTempParaCreated = true;
                        }
                        else if (iInsertionLocation == InsertionLocations.InsertAtEndOfAllSections ||
                            iInsertionLocation == InsertionLocations.InsertAtEndOfCurrentSection ||
                            iInsertionLocation == InsertionLocations.InsertAtEndOfDocument)
                        {
                            object oEnd = oInsertRange.End;
                            Word.Range oLoc = oInsertRange.Document.Range(ref oEnd, ref oEnd);

                            //GLOG 6464 (dm) - insert "TEMP" placeholder into an empty paragraph -
                            //any existing text in the last paragraph was getting deleted with the placeholder
                            if (oInsertRange.Paragraphs[1].Range.Text != "\r")
                                oInsertRange.InsertAfter("\r");

                            oInsertRange.InsertAfter("TEMP\r");
                            object oParaRng = oInsertRange.Paragraphs.Last.Range;
                            oWordDoc.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827
                            //oSegment.ForteDocument.WordDocument.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827
                            bTempParaCreated = true;
                            bInsertedAtEnd = true; //GLOG 7827
                            oInsertRange.EndOf(ref oMissing, ref oMissing);
                        }
                        if (oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                        {
                            //JTS 6/9/10: 10.2
                            oInsertRange = LMP.Forte.MSWord.WordDoc.InsertSection_CC(oInsertRange,
                                (iInsertionBehavior & InsertionBehaviors.RestartPageNumbering) > 0,
                                bInsertInContinuousSection);
                        }
                        else
                        {
                            //GLOG 3165: Make use of 'Restart Page Numbering' option
                            oInsertRange = LMP.Forte.MSWord.WordDoc.InsertSection(oInsertRange,
                                (iInsertionBehavior & InsertionBehaviors.RestartPageNumbering) > 0,
                                bInsertInContinuousSection);
                        }
                    }
                    LMP.Benchmarks.Print(t0, "Stage 3");

                    //use definition xml if there is no supplied xml -
                    //this will exist, e.g., when previewing a document-
                    //in that case we pass in the xml directly from the designer
                    if (string.IsNullOrEmpty(xXML))
                        xXML = oDef.XML;

                    //GLOG 8213: Strip orphaned w:DocVar nodes from XML
                    if (!bDesign)
                        xXML = String.DeleteOrphanedDocVarsInXml(xXML);

                    //preconvert if necessary (dm 2/9/11)
                    //GLOG 5932 (dm) - don't do if preinjunction and file format is .doc -
                    //adding the bookmarks isn't necessary in this case and was causing
                    //problem in Cleary's letterhead in Word 2007 - the Reserved attribute
                    //and doc vars will be added by RefreshTags if it hasn't been done here
                    if ((!LMP.String.IsWordOpenXML(xXML)) &&
                        (LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion() ||
                        (oMPDocument.FileFormat == mpFileFormats.OpenXML)))
                    {
                        int iID1;
                        int iID2;
                        LMP.Data.Application.SplitID(xID, out iID1, out iID2);
                        xXML = LMP.Conversion.PreconvertIfNecessary(xXML, false, iID1.ToString());
                    }

                    //if in design mode, and segment will be inserted in header/footer,
                    //set the parent to the top-level segment
                    //GLOG 5317: If oParent has been passed from Designer (because Child Segment
                    //is being updated in Related Segments) don't re-evaluate here -
                    //Segment may be a child of a child
                    if ((oMPDocument.Mode == ForteDocument.Modes.Design) &&
                        (!Segment.TargetIsBody(xXML)) && oParent == null)
                        oParent = oMPDocument.Segments[0];

                    //If body consists of page break only, replace with 
                    //empty paragraph so body won't be inserted
                    if ((!bDesign || oParent != null) && bUseTargetedXMLInsertion)
                    {
                        //This can appear 2 ways, depending on which version of Word was used to save
                        xXML = xXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                            "<w:body><w:p/><w:sectPr>");
                        xXML = xXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                            "<w:body><wx:sect><w:p/><w:sectPr>");

                        //GLOG 5403 (dm) - the preconversion process adds a space to the w:br node
                        if (!LMP.String.IsWordOpenXML(xXML))
                        {
                            xXML = xXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                                "<w:body><w:p/><w:sectPr>");
                            xXML = xXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                                "<w:body><wx:sect><w:p/><w:sectPr>");
                        }
                    }

                    if ((oParent != null) && !(oContentSegment is CollectionTableItem))
                    {
                        //add the parent's tag id to the child's
                        //tags in the xml if necessary -
                        //GLOG 3482 - don't do for collection table items because oInsertRange
                        //may not be inside the table at this point and collection table items
                        //are never external children
                        Segment.AddParentIDToXMLIfNecessary(ref xXML, oParent, oInsertRange);
                    }

                    //GLOG : 8248 : ceh
                    string xDefaultStyle = "";

                    //applies only to mpSegmentIntendedUses.AsParagraphText
                    if (iInsertionLocation == InsertionLocations.InsertAtSelectionAsPlainText)
                    {
                        //Get style of selection, to be used as default
                        try
                        {
                            xDefaultStyle = ((Word.Style)oInsertRange.Paragraphs[1].get_Style()).NameLocal;
                        }
                        catch { }

                        //GLOG 8343 (dm) - check for Macpac scheme at target
                        if (xDefaultStyle != "")
                        {
                            Word.ListTemplate oLT = null;
                            try
                            {
                                oLT = oWordDoc.Styles[xDefaultStyle].ListTemplate;
                            }
                            catch { }
                            if (oLT != null)
                            {
                                string[] aProps = oLT.Name.Split('|');
                                if (aProps[0].StartsWith("zzmp") || (aProps[0] == "HeadingStyles"))
                                    xScheme = aProps[0];
                                else if (oLT.OutlineNumbered && xDefaultStyle.StartsWith("Heading "))
                                {
                                    //native heading scheme not yet converted to MacPac scheme
                                    xScheme = "HeadingStyles";
                                    xSchemes = "HeadingStyles" + StringArray.mpEndOfRecord;
                                }
                            }
                        }

                        //GLOG 8343 - if there's MacPacPac numbering in both the segment and at
                        //the target, insert with source formatting to preserve numbering structure
                        bContainsMPNumbering = LMP.String.ContentUsesNumberingScheme(xXML);
                        if ((xScheme == "") || !bContainsMPNumbering)
                        {
                            //GLOG 8248
                            xXML = LMP.String.ApplyTargetParagraphFormattingToXML(xXML,
                                oLocation.Paragraphs[1].Range.WordOpenXML, xDefaultStyle,
                                bContainsMPNumbering);
                        }

                        //GLOG 8358 (dm) - this wasn't running when the target was unnumbered
                        if (bContainsMPNumbering)
                        {
                            //GLOG 8343 - get named list templates already in the document
                            foreach (Word.ListTemplate oLTTest in oWordDoc.ListTemplates)
                            {
                                string xLT = oLTTest.Name;
                                string[] aProps = xLT.Split('|');
                                if (aProps[0].StartsWith("zzmp") || (aProps[0] == "HeadingStyles"))
                                    xSchemes += aProps[0] + StringArray.mpEndOfRecord;
                            }
                        }
                    }
                    else if (!bUpdateAllStyles)
                    {
                        //GLOG 8343 (dm) - before this enhancement, paragraph text segments were inserting
                        //normal and body text paragraphs with target formatting and everything else with
                        //source formatting - now you can explicitly choose one or the other for paragraph
                        //text segments, so this block now only applies to sentence text paragraphs
                        if (oContentDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                        {
                            //Get style of selection, to be used as default
                            try
                            {
                                xDefaultStyle = ((Word.Style)oInsertRange.Paragraphs[1].get_Style()).NameLocal;
                            }
                            catch { }

                            //GLOG 2543 - always remove style node from sentence text 
                            int iPos = xXML.IndexOf("<w:pStyle ");
                            if (iPos != -1)
                            {
                                int iPos2 = xXML.IndexOf("/>", iPos);
                                xXML = xXML.Substring(0, iPos) + xXML.Substring(iPos2 + 2);
                            }
                        }

                        //Update Styles definitions to match Target Doc, plus any styles unique to the source
                        //so that derived style attributes will match base styles in Target -
                        //we get the xml for the first paragraph because it's the smallest unit we
                        //can get without breaking Word's XMLSelectionChange event for the session
                        //GLOG 5128: Make sure both Target and Source XML are using the same format
                        if (LMP.String.IsWordOpenXML(xXML))
                            xXML = LMP.String.MergeStylesWordOpenXML(xXML, oWordDoc.Paragraphs[1].Range.WordOpenXML, xDefaultStyle);
                        else
                        {
                            //GLOG 5489:  In Word 2007, Range.XML can result in error 5460 if Mirror Indent paragraph formatting
                            //exists anywhere in document.  Ignore any error in get_XML, and just leave
                            //Segment styles unchanges
                            string xDocXML = "";
                            try
                            {
                                xDocXML = oWordDoc.Paragraphs[1].Range.get_XML(false);
                            }
                            catch
                            {
                            }
                            xXML = LMP.String.MergeStylesXML(xXML, xDocXML, xDefaultStyle);
                        }
                    }

                    //GLOG 8358 (dm) - delete active scheme doc vars if the content isn't numbered or
                    //there's already an active scheme in the document
                    bool bDeleteVars = false;
                    if (oContentDef.IntendedUse != mpSegmentIntendedUses.AsDocument)
                        bDeleteVars = !String.ContentUsesNumberingScheme(xXML);
                    if (!bDeleteVars)
                    {
                        string xActiveScheme = "";
                        try
                        {
                            xActiveScheme = oWordDoc.Variables["zzmpFixedCurScheme_9.0"].Value;
                        }
                        catch { }
                        bDeleteVars = (xActiveScheme != "");
                    }
                    if (bDeleteVars)
                    {
                        int iDVPos = xXML.IndexOf("<w:docVar w:name=\"zzmpFixedCurScheme\"");
                        if (iDVPos > -1)
                        {
                            int iDVPos2 = xXML.IndexOf(">", iDVPos);
                            xXML = xXML.Substring(0, iDVPos) + xXML.Substring(iDVPos2 + 1);
                        }
                        iDVPos = xXML.IndexOf("<w:docVar w:name=\"zzmpFixedCurScheme_9.0\"");
                        if (iDVPos > -1)
                        {
                            int iDVPos2 = xXML.IndexOf(">", iDVPos);
                            xXML = xXML.Substring(0, iDVPos) + xXML.Substring(iDVPos2 + 1);
                        }
                    }

                    //disable Word event handlers
                    LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                    try
                    {
                        object oNewBookmark = "mpNewSegment";
                        //Make sure there's no existing mpNewSegment bookmark
                        //Otherwise GetNewSegmentTagID below will return wrong Tag ID
                        Word.Bookmarks oBmks = oWordDoc.Bookmarks;
                        if (oBmks.Exists(oNewBookmark.ToString()))
                        {
                            oBmks.get_Item(ref oNewBookmark).Delete();
                        }
                    }
                    catch { }

                    //GLOG 4418: If trailer is being inserted in all sections, speed it up
                    //by plugging the mSEG contents of the first generated trailer directly
                    //into the XML used for subsequent trailers.  This avoids the time required
                    //for setting variables and running variable actions
                    if (!LMP.String.IsWordOpenXML(xXML) && oDef.TypeID == mpObjectTypes.Trailer && xTrailerXML != null && !bDesign)
                    {
                        string xNSPrefix = LMP.String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);
                        //Increment index used in XML for each subsequent trailer, so each will be seen as belonging
                        //to a different Segment by RefreshTags
                        bUseQuickTrailerInsertion = LMP.String.SubstituteTrailerXML(
                            ref xXML, ++iTrailerIndex, xNSPrefix, oContentDef.Name, xTrailerXML);
                    }

                    //10.2 (dm) - generate new doc var ids to avoid conflicts on insertion
                    //GLOG 5932 (dm) - don't do unless xml is preconverted - we're no longer
                    //preconverting before insertion in non-reconstitution scenarios
                    if ((oMPDocument.Mode != ForteDocument.Modes.Design) &&
                        LMP.Conversion.IsPreconvertedXML(xXML))
                        LMP.Forte.MSWord.WordDoc.RegenerateDocVarIDs(ref xXML, oWordDoc);

                    try
                    {
                        //doc vars from source xml to target document
                        //12-10-10 (dm) - moved before InsertXML(), since retagging
                        //in Word 2010 now occurs immediately after xml insertion and
                        //doc vars are required for that
                        LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(oLocation.Document, xXML, bInsertingSavedContent == true); //GLOG 7906: Include Encrypted doc vars in Saved Content


                        LMP.Benchmarks.Print(t0, "Stage 4");
                        //insert segment xml - get segment 
                        //node of inserted segment
                        oContentSegment.InsertXML(xXML, oParent, oInsertRange,
                            iHeaderFooterInsertionType, bUseTargetedXMLInsertion, oContentDef.IntendedUse);

                        LMP.Benchmarks.Print(t0, "Stage 5");
                        //GLOG 7444 (dm) - don't run the following in design
                        if ((oContentSegment is CollectionTable) && !bDesign)
                        {
                            //bookmark segment - these are not bookmarked
                            //as they are built in, and don't get opened in the
                            //designer where bookmarking occurs automatically
                            object o1 = 1;
                            if (oMPDocument.FileFormat == mpFileFormats.Binary)
                            {
                                Word.XMLNode oNode = oInsertRange.Paragraphs[1].Range.XMLNodes[1];
                                object oNodeRng = oNode.Range.Tables[1].Range;
                                string xTag = "";
                                for (int a = 1; a <= oNode.Attributes.Count; a++)
                                {
                                    if (oNode.Attributes[a].BaseName == "Reserved")
                                    {
                                        xTag = oNode.Attributes[a].NodeValue;
                                        break;
                                    }
                                }
                                Word.Bookmark oBmk = oNode.Range.Bookmarks.Add("_" + xTag, ref oNodeRng);
                                LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark(oWordDoc, oNode);
                            }
                            else
                            {
                                Word.ContentControl oCC = oInsertRange.Paragraphs[1].Range.ContentControls.get_Item(ref o1);
                                object oCCRng = oCC.Range.Tables[1].Range;
                                Word.Bookmark oBmk = oCC.Range.Bookmarks.Add("_" + oCC.Tag, ref oCCRng);
                                LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark_CC(oWordDoc, oCC);
                            }

                            //GLOG 7070 (dm) - if inserting at end of parent, expand parent's bookmark to
                            //encompass the table - this was an issue on insertion of O'Melveneys'
                            //business segments because the signature was replaced by the author's pref
                            //beyond the bounds of the parent
                            if (oInsertRange.Start > 0)
                            {
                                Word.Bookmark oParentBmk = null;
                                Word.Range oTargetRng = oInsertRange.Duplicate;
                                if (oParent != null)
                                    oParentBmk = oParent.PrimaryBookmark;
                                else
                                {
                                    int iStart = oTargetRng.Start - 1;
                                    oTargetRng.SetRange(iStart, iStart);
                                    oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oTargetRng);
                                }
                                if ((oParentBmk != null) && (oParentBmk.End == oInsertRange.End))
                                {
                                    oTargetRng.SetRange(oParentBmk.Start, oInsertRange.Tables[1].Range.End);
                                    object oTargetRngObject = oTargetRng;
                                    oTargetRng.Document.Bookmarks.Add(oParentBmk.Name, ref oTargetRngObject);
                                }
                            }
                        }


                        //reenable Word event handlers
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                        //delete segment bounding objects so that users never see
                        //segment ccs or tags
                        //Don't do this for Trailers, as all bounding objects will be removed later
                        //GLOG 6768: Don't delete Bounding Objects in Design
                        //GLOG 6795: Don'r delete Bounding Object for Master Data Segment
                        //if (oContentDef.TypeID != mpObjectTypes.Trailer && oContentDef.TypeID != mpObjectTypes.MasterData && !bDesign)
                        if (!bDesign) //GLOG 6780: Delete Bounding Objects in Master Data also
                            LMP.Forte.MSWord.WordDoc.DeleteSegmentBoundingObjects(oWordDoc, false, true); //GLOG 7306 (dm)

                        oSegment.CreationStatus = Status.BoilerplateInserted;
                    }
                    catch (LMP.Exceptions.SegmentInsertionCancelledException)
                    {
                        //Insertion was cancelled by Architect class InsertXML
                        return null;
                    }
                    catch (System.Exception oE)
                    {
                        string xErrMsg = "";
                        //Check for predefined error message returned by COM method
                        if (oE.InnerException != null)
                        {
                            xErrMsg = oE.InnerException.Message;
                            if (xErrMsg.StartsWith("<"))
                            {
                                //Get error message from Resource string
                                xErrMsg = xErrMsg.Replace("<", "");
                                xErrMsg = xErrMsg.Replace(">", "");
                                xErrMsg = LMP.Resources.GetLangString(xErrMsg);
                            }
                        }
                        if (string.IsNullOrEmpty(xErrMsg))
                            //pass on original Exception
                            throw oE;
                        else
                            //raise special error message text
                            throw new LMP.Exceptions.SegmentInsertionException(xErrMsg, oE);
                    }

                    LMP.Forte.MSWord.WordDoc.RemoveUnavailableXMLSchema(oWordDoc);

                    LMP.Benchmarks.Print(t0, "Stage 6");
                    //get full tag id of the inserted segment
                    if (!bUseQuickTrailerInsertion) //GLOG 4418: skip all this for all but first trailer
                    {
                        //refresh document tags - if this is runtime, segment tag ids
                        //should get GUID indexes
                        oMPDocument.RefreshTags(!bDesign);

                        //reenable Word event handlers
                        //GLOG 8044: This line should have been removed as part of GLOG 6820
                        //LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                        oWordDoc.Application.ScreenUpdating = false;

                        xTagID = "";
                        try
                        {
                            //GLOG 6799 (dm) - supply segment id to ensure that we
                            //find the right segment
                            xTagID = GetNewSegmentTagID(oMPDocument, xValidationID);
                        }
                        catch { }
                        //GLOG 6820: Move after above line, since GetNewSegmentTagID may trigger XML events
                        //reenable Word event handlers
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                        oWordDoc.Application.ScreenUpdating = false;

                        //Insertion may have done nothing if headers/footers are linked
                        //and there's no body XML - continue with next location
                        if (string.IsNullOrEmpty(xTagID))
                            continue;

                        //CEH - 3333 - CEH
                        string xStylesXML = oDef.XML;

                        if (bInsertingSavedContent)
                        {
                            //get the segment from the xml in the document
                            oSegment = Segment.GetSegment(xTagID, oMPDocument);

                            //GLOG 6465 (dm) - GetSegment() prematurely sets the creation
                            //status to Finished - restore it to BoilerplateInserted
                            oSegment.CreationStatus = Status.BoilerplateInserted;
                        }
                        else
                        {
                            //initialize segment with tag data, etc.
                            oSegment.Initialize(xTagID, oParent);
                        }

                        if (!bDesign)
                        {
                            //JTS 4/1/13:  Set Dynamic Editing Environment once at start, so that it won't need to be set individually in each variable action
                            oSegment.ForteDocument.DynamicEditingInProgress = true;
                            try
                            {
                                if (!bInsertingSavedContent || oPrefill != null)
                                {
                                    //initialize authors
                                    //GLOG 6943: Use Parent Authors for linked children
                                    //GLOG 7842: If Parent Segment doesn't have authors, get from Prefill
                                    if (!oSegment.IsTopLevel && oSegment.LinkAuthorsToParent && oSegment.Parent.Authors.Count > 0)
                                    {
                                        //GLOG 8198: Confirm that Parent LeadAuthor still exists in database
                                        LocalPersons oPersons = new LocalPersons(LMP.Data.Application.User.ID);

                                        string xAuthorID = "";
                                        try
                                        {
                                            xAuthorID = oSegment.Parent.Authors.GetLeadAuthor().ID;
                                        }
                                        catch { }
                                        if (xAuthorID != "" && oPersons.IsInCollection(xAuthorID))
                                        {
                                            oSegment.InitializeAuthors(null);
                                        }
                                        else
                                        {
                                            //If author doesn't exist, initialize using Prefill authors
                                            oSegment.InitializeAuthors(oPrefill);
                                        }
                                    }
                                    else
                                        oSegment.InitializeAuthors(oPrefill);

                                    if (oSegment.Authors.Count == 0 && oSegment.DefinitionRequiresAuthor)
                                    {
                                        //author is required, but no authors are available - alert
                                        MessageBox.Show(LMP.Resources.GetLangString("Msg_AuthorRequiredToInsertSegment"),
                                            LMP.ComponentProperties.ProductName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Exclamation);
                                        return null;
                                    }

                                    //execute AfterDefaultAuthorSet actions
                                    oSegment.Actions.Execute(Segment.Events.AfterDefaultAuthorSet);

                                    //raise after default author set
                                    if (oSegment.AfterDefaultAuthorSet != null)
                                        oSegment.AfterDefaultAuthorSet(oSegment, new EventArgs());

                                    //set segment status
                                    oSegment.CreationStatus = Status.DefaultAuthorsSet;
                                }

                                //add the segment to the appropriate collection - 
                                //we need to do this at this point because subsequent
                                //code may refer to the segment's parent or ForteDocument
                                if (oSegment.IsTopLevel)
                                    oMPDocument.Segments.Add(oSegment);
                                else
                                    oSegment.Parent.Segments.Add(oSegment);

                                //GLOG 4519: update variable that holds id of collection item, if one exists
                                //do for first item in collection only
                                if (oSegment is CollectionTableItem && (oSegment.Parent.Segments[0] == oSegment))
                                {
                                    Segment oTopSegment = oSegment.Parent.Parent;
                                    if (oTopSegment != null)
                                    {
                                        //Look for variable in form of LetterSignatureID, PleadingCaptionID, etc.
                                        Variable oVar = null;
                                        try
                                        {
                                            oVar = oTopSegment.Variables.ItemFromName(
                                                oSegment.TypeID.ToString() + "ID");
                                        }
                                        catch { }
                                        try
                                        {
                                            if (oVar != null)
                                                oVar.SetValue(oSegment.ID1.ToString(), false);
                                        }
                                        catch { }
                                    }
                                }

                                if (bAttachToTemplate)
                                {
                                    string xTemplate = Expression.Evaluate(oSegment.WordTemplate, oSegment, oMPDocument);
                                    try
                                    {
                                        oMPDocument.AttachToTemplate(xTemplate, false);
                                    }
                                    catch
                                    {
                                        System.Windows.Forms.MessageBox.Show(LMP.Resources.GetLangString("Msg_UnableToAttachTemplate") + xTemplate,
                                            LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
                                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                                    }
                                }

                                //GLOG 8248 (dm) - update styles shouldn't run for paragraph or segment text, since
                                //xml may contain a lot of unrelated styles from the source document - ideally,
                                //bUpdateStyles would be false for these types, but we can clean this up for 11.3
                                if (bUpdateAllStyles && (oContentDef.IntendedUse != mpSegmentIntendedUses.AsParagraphText) &&
                                    (oContentDef.IntendedUse != mpSegmentIntendedUses.AsSentenceText))
                                {
                                    if (oSegment.HasDefinition)
                                        oMPDocument.UpdateStylesFromXML(xStylesXML);
                                }
                                else if (!string.IsNullOrEmpty(oSegment.RequiredStyles))
                                {
                                    if (oSegment.HasDefinition)
                                    {
                                        string[] aStyleList = oSegment.RequiredStyles.Split(new char[] { ',' });
                                        oMPDocument.UpdateStylesFromXML(xStylesXML, aStyleList);
                                    }
                                }

                                if (!bInsertingSavedContent || oPrefill != null)
                                {
                                    //set up the segment that was inserted
                                    //GLOG 6943: Authors already initialized above
                                    oSegment.InitializeValues(oPrefill, "", bForcePrefillOverride, oChildStructure, false); //GLOG 8373
                                }
                                else
                                    //GLOG 6315 (dm) - unfinished status of children was
                                    //preventing post-creation author update - finished status
                                    //is reached at the end of InitializeValues()
                                    oSegment.SetChildSegmentsStatus(Status.Finished);
                            }
                            catch (System.Exception oE)
                            {
                                throw oE;
                            }
                            finally
                            {
                                //Clear Dynamic Editing flag
                                oSegment.ForteDocument.DynamicEditingInProgress = false;
                            }
                        }
                        else
                        {
                            //add the segment to the appropriate collection - 
                            //we need to do this at this point because subsequent
                            //code may refer to the segment's parent or ForteDocument
                            if (oSegment.IsTopLevel)
                                oMPDocument.Segments.Add(oSegment);
                            else
                                oSegment.Parent.Segments.Add(oSegment);

                            //Copy any Required Styles in Designer
                            if (!string.IsNullOrEmpty(oSegment.RequiredStyles))
                            {
                                if (oSegment.HasDefinition)
                                {
                                    string[] aStyleList = oSegment.RequiredStyles.Split(new char[] { ',' });
                                    oMPDocument.UpdateStylesFromXML(oSegment.Definition.XML, aStyleList);
                                }
                            }
                            oSegment.RestoreDeletedScopesForDesign();

                            //hide TagExpandedValue field codes in designer -
                            //10.2 (dm) - skip this for now when using content controls -
                            //it's not yet clear whether this will ultimately be necessary
                            if (oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                oSegment.RefreshExpandedTagValueCodes(false);
                        }
                        //execute AfterSegmentXMLInsert actions
                        oSegment.Actions.Execute(Segment.Events.AfterSegmentXMLInsert);
                    }

                    LMP.Benchmarks.Print(t0, "Stage 7");
                    //insert pleading paper, if appropriate -
                    //we do this here instead of when the XML
                    //is inserted because the parent has to be fully
                    //initialized for the pleading paper insertion
                    //to execute successfully
                    if (oSegment is LMP.Architect.Base.ILitigationAddOnSegment)
                        PleadingPaper.InsertExistingInCurrentSection(oSegment.ForteDocument, oSegment);

                    //GLOG : 6967 : ceh - remmed out per comment #28798
                    //GLOG : 6967 : ceh
                    //check for Pleading Paper
                    LMP.Forte.MSWord.PleadingPaper oPaper = new LMP.Forte.MSWord.PleadingPaper();

                    //reset compatibility option - no need with Forte format
                    //if (oPaper.HasPleadingPaper() &&
                    //(LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oSegment.ForteDocument.WordDocument) < 15))
                    //{
                    //oSegment.ForteDocument.WordDocument.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = false;
                    //}

                    if (oSegment is PleadingPaper)
                    {
                        //adjust header to match pleading paper
                        //if parent is toa
                        if (oSegment.Parent != null && oSegment.Parent.TypeID == mpObjectTypes.TOA)
                        {
                            //GLOG : 6967 : ceh
                            //LMP.Forte.MSWord.PleadingPaper oPaper = new LMP.Forte.MSWord.PleadingPaper();

                            //GLOG : 8554 : ceh - ensure "show white space" is turned on before aligning header to line1
                            bool bPageBoundaries = LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveWindow.View.DisplayPageBoundaries;
                            LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveWindow.View.DisplayPageBoundaries = true;

                            //GLOG 6852: Use Bookmark range
                            Word.HeadersFooters oHFs = oSegment.PrimaryRange.Sections.First.Headers;

                            //JTS 5/7/10: Make sure XML events are not run due to selection changes in COM function
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                            oPaper.AlignHeaderToLine1(oHFs[Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage]);
                            oPaper.AlignHeaderToLine1(oHFs[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary]);
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                            //GLOG : 8554 : ceh
                            if (!bPageBoundaries)
                                LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveWindow.View.DisplayPageBoundaries = false;
                        }
                    }

                    //GLOG 6121 - this is necessary because ccs embedded in delete scopes
                    //are not routinely retagged on insertion - a conflict arises from a
                    //recreate sequence in which the same non-default paper may get
                    //inserted twice, once with prefill and again with the child structure
                    //GLOG 6381 (dm) - retagging is now done routinely, making the
                    //following code duplicative
                    //if ((oMPDocument.FileFormat == mpFileFormats.OpenXML) && (oSegment is Paper))
                    //{
                    //    Word.ContentControl[] aCCs = oSegment.Nodes.GetContentControls(
                    //        oSegment.FullTagID);
                    //    object oSegCCs = (object)aCCs;
                    //    LMP.Forte.MSWord.WordDoc.RetagDeletedScopes(oSegCCs);
                    //}

                    if (oChildStructure != null)
                    {
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        oSegment.InsertCollectionTableStructure(oSegment, oChildStructure);
                        LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    }

                    //set segment status
                    oSegment.CreationStatus = Status.Finished;

                    //GLOG 7876: Previous code to pre-populate WordML XML for Trailers has been replaced by new quick insertion code

                    if (bTempParaCreated)
                    {
                        try
                        {
                            //GLOG 7827:  Different handling required for deleted range before or after section break
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All; //GLOG 6075 (dm)
                            object xName = "zzmpTempSectionPara";
                            Word.Bookmark oBmk = oWordDoc.Bookmarks.get_Item(ref xName);
                            Word.Range oRng = oBmk.Range.Paragraphs.Last.Range;
                            if (bInsertedAtEnd)
                            {
                                //Before section break, simple Delete() will work
                                oRng.Delete();
                            }
                            else
                            {
                                //GLOG 7937 (dm) - if the target range is two tabs and the paragraph mark,
                                //which will be the case when AddParaBeforeTable() is used with a 3-column table,
                                //the GLOG 7827 code below will result in the first row of the table getting
                                //deleted as well - delete the paragraph text first, then the paragraph mark
                                object oUnit = Word.WdUnits.wdCharacter;
                                object oCount = -1;
                                oRng.MoveEnd(ref oUnit, ref oCount);
                                oRng.Delete();

                                //set range back to paragraph
                                oRng = oBmk.Range.Paragraphs.Last.Range;

                                //GLOG 7827:  Last paragraph may not be empty if before table,
                                //so explicitly indicate number of characters to be deleted;
                                //otherwise paragraph mark won't be deleted
                                //object oUnit = Word.WdUnits.wdCharacter;
                                //object oCount = oRng.Characters.Count;
                                //oRng.Delete(ref oUnit, ref oCount);
                                oRng.Delete();
                            }
                            oBmk.Delete();
                        }
                        catch { }
                        finally
                        {
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None; //GLOG 6075 (dm)
                        }
                    }
                    //GLOG 5472
                    bOneInserted = true;
                    xSegmentName = oSegment.Name;
                    //JTS 6/30/11:  Remove Content Controls or XML Tags from Trailer content
                    //GLOG 5661: Don't remove Bounding Objects if ConvertTrailersTo9xFormat metadata value is found.
                    //Separate custom code will deal with this at a later point.
                    if (oSegment is Trailer && !bDesign) //GLOG 6021: Code reorganized to convert to 9.x format here instead of in DocumentStampUI
                    {
                        object oNodesArray = null;
                        if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
                        {
                            //GLOG 6944: Use Bookmarks if present
                            if (oSegment.WordTags.GetLength(0) == 0)
                            {
                                oNodesArray = oSegment.Bookmarks;
                            }
                            else
                            {
                                //xml tags
                                Word.XMLNode[] oNodes = oSegment.WordTags;
                                oNodesArray = oNodes;
                            }
                        }
                        else
                        {
                            //GLOG 6944: Use Bookmarks if present
                            if (oSegment.ContentControls.GetLength(0) == 0)
                            {
                                oNodesArray = oSegment.Bookmarks;
                            }
                            else
                            {
                                //content controls
                                Word.ContentControl[] oNodes = oSegment.ContentControls;
                                oNodesArray = oNodes;
                            }
                        }

                        oSegment.ForteDocument.Segments.Delete(oSegment.FullTagID);

                        //2-27-12 (dm) - pass in oTags, so that deleted tags/cc
                        //can be removed from the collection
                        LMP.Forte.MSWord.Tags oTags = oMPDocument.Tags;
                        //GLOG 7876
                        if (aInsertionLocations.GetUpperBound(0) > 0)
                        {
                            //GLOG 7876: Create array for XML inserted in Body,
                            //3 Footer and 3 Header stories
                            oXMLArray = new string[7];
                            bUseQuickTrailerInsertion = true;
                        }

                        //GLOG 6108 (dm) - added 9.x trailer option as firm application setting
                        LMP.Data.FirmApplicationSettings oFirmSettings =
                            new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
                        if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains("CONVERTTRAILERSTO9XFORMAT") ||
                            oFirmSettings.UseMacPac9xStyleTrailer)
                        {
                            int iLocation9x = 100; //JTS 8/12/09 - Default to Footer
                            switch (iInsertionLocation)
                            {
                                //Insertion locations translate to End of Document 9.x type
                                case Segment.InsertionLocations.InsertAtEndOfAllSections:
                                case Segment.InsertionLocations.InsertAtEndOfCurrentSection:
                                case Segment.InsertionLocations.InsertAtEndOfDocument:
                                    iLocation9x = 101;
                                    break;
                            }
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            DateTime t1 = DateTime.Now;
                            try
                            {
                                //GLOG 6676 (dm): there won't be any prefill when updating an
                                //existing 9.x trailer - avoid object reference error
                                string xPrefill = "";
                                if (oPrefill != null)
                                    xPrefill = oPrefill.ToString();

                                //GLOG 4404: Extra parameter specifying to also insert converted XML in 
                                //additional sections
                                LMP.Forte.MSWord.WordDoc.ConvertTrailersTo9xFormat(ref oNodesArray,
                                    oWordDoc.FullName, iLocation9x, xPrefill,
                                    iInsertionLocation == InsertionLocations.InsertAtStartOfDocument, ref oTags, ref oXMLArray); //GLOG 7876
                            }
                            finally
                            {
                                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                            }
                            LMP.Benchmarks.Print(t1, "ConvertTrailersTo9xFormat");

                        }
                        else
                        {
                            LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            DateTime t1 = DateTime.Now;
                            try
                            {
                                LMP.Forte.MSWord.WordDoc.DeleteTrailerBoundingObjects(
                                    ref oNodesArray, ref oTags, ref oXMLArray); //GLOG 7876
                            }
                            finally
                            {
                                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                            }
                            LMP.Benchmarks.Print(t1, "DeleteTrailerBoundingObjects");
                        }
                        oSegment = null;
                    }
                }
            }

            //GLOG #5925 CEH
            //GLOG 6281 (dm) - was erring in trailer
            if (bTrackChanges)
                oWordDoc.TrackRevisions = true;

            if (oSegment != null)
            {
                //mark as having been initialized with a prefill
                oSegment.Prefill = oPrefill;
            }

            //GLOG 8343 (dm)
            if (iInsertionLocation == InsertionLocations.InsertAtSelectionAsPlainText)
            {
                if (bContainsMPNumbering)
                {
                    Word.Application oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                    if (xScheme != "")
                    {
                        oSegment.PrimaryRange.Select();
                        try
                        {
                            oWord.Run("zzmpChangeToDocumentScheme", xScheme, true);
                        }
                        catch { }
                    }

                    //delete any schemes that weren't already in the document
                    foreach (Word.ListTemplate oLTTest in oWordDoc.ListTemplates)
                    {
                        string[] aProps = oLTTest.Name.Split('|');
                        if ((aProps[0].StartsWith("zzmp") || (aProps[0] == "HeadingStyles")) &&
                            (!xSchemes.Contains(aProps[0] + StringArray.mpEndOfRecord)))
                        {
                            try
                            {
                                oWord.Run("zzmpDeleteScheme", aProps[0], true);
                            }
                            catch { }
                        }
                    }
                }
            }

            //oUndo.MarkEnd();
            LMP.Benchmarks.Print(t0, xSegmentName);
            return oSegment;
        }
        //GLOG item #4734 - dcf
        /// <summary>
        /// returns the default insertion behavior for a segment type
        /// </summary>
        /// <param name="iSegmentType"></param>
        /// <returns></returns>
        private static InsertionBehaviors GetDefaultInsertionBehaviors(LMP.Data.mpObjectTypes iSegmentType)
        {
            switch (iSegmentType)
            {
                case mpObjectTypes.TOA:
                    return InsertionBehaviors.InsertInSeparateNextPageSection | InsertionBehaviors.RestartPageNumbering;
                default:
                    //unspecified
                    return InsertionBehaviors.Default;
            }
        }
        /// <summary>
        /// Populate Array with contents of all mSEG tags matching TagID
        /// </summary>
        /// <param name="xSectionXML"></param>
        /// <param name="xTagID"></param>
        /// <param name="xNodes"></param>
        private static void PopulateTrailerNodeArray(string xSectionXML, string xTagID, ref string[] xNodes)
        {
            try
            {
                string xNSPrefix = LMP.String.GetNamespacePrefix(xSectionXML, ForteConstants.MacPacNamespace);
                xTagID = Regex.Escape(xTagID);
                Regex oRegex = new Regex("<" + xNSPrefix + ":mSEG TagID=\\\"" + xTagID + ".*?</" + xNSPrefix + ":mSEG>");
                MatchCollection oMatches = oRegex.Matches(xSectionXML);
                if (oMatches.Count > 0)
                {
                    xNodes = new string[oMatches.Count];
                    for (int i = 0; i < oMatches.Count; i++)
                    {
                        Match oMatch = oMatches[i];
                        string xTag = oMatch.Value;
                        int iStart = xTag.IndexOf(">") + 1;
                        int iEnd = xTag.IndexOf("</" + xNSPrefix + ":mSEG>");
                        xNodes[i] = xTag.Substring(iStart, iEnd - iStart);
                    }
                }
            }
            catch
            {
                xNodes = null;
            }
        }
    
        /// <summary>
        /// replaces one segment with another
        /// </summary>
        /// <param name="xNewSegmentID">ID of segment to be inserted</param>
        /// <param name="xOldSegmentID">ID of segment to be replaced</param>
        /// <param name="oOldSegmentParent">parent of segment to be replaced - limits
        /// search of old segment to parent - can be null</param>
        /// <param name="oMPDocument">MacPac document containing segment to be replaced -
        /// will be used to locate old segment if no parent segment is specified</param>
        /// <returns></returns>
        internal static void Replace(string xNewSegmentID, string xOldSegmentID,
            Segment oOldSegmentParent, ForteDocument oMPDocument)
        {
            Replace(xNewSegmentID, xOldSegmentID, oOldSegmentParent, oMPDocument, null, null, false);
        }
        internal static void Replace(string xNewSegmentID, string xOldSegmentID, 
            Segment oOldSegmentParent, ForteDocument oMPDocument, Prefill oPrefill, 
            string xBodyText, bool bDesign)
        {

            //alert if new segment doesn't exist
            ISegmentDef oDef = null;

            try
            {
                oDef = Segment.GetDefFromID(xNewSegmentID);
            }
            catch { }

            if (oDef == null)
                throw new LMP.Exceptions.SegmentDefinitionException(
                    LMP.Resources.GetLangString("Msg_CantReplaceSegmentWithNonExistentSegment") + 
                    xNewSegmentID);

            //get domain of segments that we should search to
            //find the specified existing segment
            Segments aDomain = oOldSegmentParent == null ? 
                oMPDocument.Segments : oOldSegmentParent.Segments;

            //find all instances of the existing segment in the domain
            Segment[] aOldSegments = Segment.FindSegments(xOldSegmentID, aDomain);

            //cycle through all found instances of the segment,
            //replacing each with an instance of the new segment
            foreach (Segment oSegment in aOldSegments)
            {
                //GLOG 4439 - moved this block into a separate method that can be
                //used to target a specific segment, as opposed to all like
                //segments in the domain
                ReplaceIndividualSegment(oSegment, xNewSegmentID, oDef, oPrefill,
                    xBodyText, bDesign);
            }
        }

        /// <summary>
        /// updates oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        public static void Update(Segment oSegment, Prefill oPrefill, string xBodyText)
        {
            ReplaceIndividualSegment(oSegment, oSegment.ID, oSegment.Definition, oPrefill,
                xBodyText, false);
        }

        /// <summary>
        /// replaces oSegment as specified
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xNewSegmentID"></param>
        /// <param name="oNewSegmentDef"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        /// <param name="bDesign"></param>
        private static void ReplaceIndividualSegment(Segment oSegment, string xNewSegmentID,
           ISegmentDef oNewSegmentDef, Prefill oPrefill, string xBodyText, bool bDesign)
        {
            ForteDocument oMPDocument = oSegment.ForteDocument;
            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents =
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents;

            try
            {
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                Segment oTarget = oSegment;
                Segment oParent = oSegment.Parent;
                Word.ContentControl oParentCC = null;
                //GLOG 8400
                bool bUpdateStyles = false;

                //GLOG 4433 (dm) - flag pleading table item replacement
                if ((oSegment is LMP.Architect.Api.CollectionTableItem) && (oParent != null) &&
                    (oParent is LMP.Architect.Api.CollectionTable))
                {
                    //7-20-11 (dm) - insert as full row when existing segment doesn't
                    //allow side-by-side - if such an existing segment isn't itself
                    //full row, it's probably the result of bad conversion
                    LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();
                    LMP.Architect.Api.CollectionTable oPT =
                        (LMP.Architect.Api.CollectionTable)oParent;
                    LMP.Architect.Api.CollectionTableItem oItem =
                        (LMP.Architect.Api.CollectionTableItem)oSegment;
                    if (oSegment.TypeID == mpObjectTypes.PleadingCaption)
                    {
                        //caption is always full row
                        oPT.ReplacementInProgress = true;
                    }
                    //JTS 6/13/13: Check for PrimaryBookmark first
                    else if (oSegment.PrimaryBookmark != null &&
                        ((!oItem.AllowSideBySide) || oPleading.ItemIsFullRow_Bookmark(oSegment.PrimaryBookmark)))
                    {
                        //insert as full row
                        oPT.PendingInsertionLocation =
                            CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    }
                    else if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary &&
                        ((!oItem.AllowSideBySide) || oPleading.ItemIsFullRow(oSegment.PrimaryWordTag)))
                    {
                        //insert as full row
                        oPT.PendingInsertionLocation =
                            CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    }
                    else if ((!oItem.AllowSideBySide) ||
                        oPleading.ItemIsFullRow_CC(oSegment.PrimaryContentControl))
                    {
                        //insert as full row
                        oPT.PendingInsertionLocation =
                            CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    }
                    else
                    {
                        //existing item is not full row
                        bool bAllowSideBySide = (LMP.Architect.Api.Segment.GetPropertyValueFromXML(
                            oNewSegmentDef.XML, "AllowSideBySide").ToUpper() == "TRUE");
                        if (bAllowSideBySide)
                        {
                            //insert in current cell
                            //oPT.PendingInsertionLocation =
                            //    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                            //    CollectionTableStructure.ItemInsertionLocations.ExistingCell;
                            Word.Range oRange = oItem.PrimaryRange;
                            if (oRange.Cells[1].ColumnIndex > 1)
                            {
                                oPT.PendingInsertionLocation =
                                    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                    CollectionTableStructure.ItemInsertionLocations.RightCell;
                            }
                            else
                            {
                                oPT.PendingInsertionLocation =
                                    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                    CollectionTableStructure.ItemInsertionLocations.LeftCell;
                            }
                        }
                        else
                        {
                            //selected item does not allow side-by-side insertion
                            string xMsg = "Msg_CannotReplaceCollectionTableItem";
                            throw new LMP.Exceptions.SegmentException(
                                LMP.Resources.GetLangString(xMsg));
                        }
                    }
                }

                //GLOG 3118 - if the only child of a transparent segment or
                //pleading collection, switch target to parent
                if ((!oSegment.IsTopLevel) && (oParent.IsTransparent ||
                    (oParent is CollectionTable)) && (oParent.Segments.Count == 1))
                {
                    //GLOG 4433 (dm) - if switching target to parent, we need to
                    //also switch the node index that's being preserved
                    oMPDocument.RaiseBeforeSegmentReplacedByActionEvent(oParent);

                    //delete child - this won't happen automatically
                    //because delete event will only be raised for parent
                    oParent.Segments.Delete(oSegment.FullTagID);
                    oTarget = oParent;
                    oParent = null;
                }
                else
                {
                    //GLOG 3118 - raise event so that editor knows to preserve
                    //the index of the segment node getting replaced
                    oMPDocument.RaiseBeforeSegmentReplacedByActionEvent(oSegment);
                }

                //get location of old segment
                Word.Range oRng = oTarget.PrimaryRange;
                if (oTarget.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    try
                    {
                        oParentCC = oTarget.PrimaryContentControl.ParentContentControl;
                    }
                    catch { }

                    //GLOG 7068 (dm) - the following code was preventing the update of
                    //related segments after saving paper in design because there's no
                    //parent - I'm removing it altogether, rather than wrapping it in a try
                    //block as above, because the parent isn't needed for bookmarks as it
                    //was for content controls
                    //if (oParentCC == null)
                    //    oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmark(oTarget.PrimaryBookmark);
                }

                object oMissing = System.Reflection.Missing.Value;
                oRng.StartOf(ref oMissing, ref oMissing);

                //GLOG item #5913 - dcf
                CollectionTableStructure oChildStructure = oSegment.ChildCollectionTableStructure;

                //delete current segment, unless deletion is built
                //into the insertion code for this object type
                //JTS 10/05/04: Never delete empty section if in Design mode
                if (!(oTarget is ISingleInstanceSegment) || bDesign)
                {
                    try
                    {
                        oMPDocument.DeleteSegment(oTarget, true, true, false);
                        //GLOG 8400: Always copy styles when recreating document segment
                        if (oMPDocument.Segments.Count == 0 || oTarget.IntendedUse == mpSegmentIntendedUses.AsDocument)
                            bUpdateStyles = true;
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                            oTarget.ID, oE);
                    }
                }

                //GLOG 5289: If start position of deleted Segment coincides with start of parent ContentControl
                //we need to make sure replacement is also inserted within that ContentControl and before it
                if (oTarget.ForteDocument.FileFormat == mpFileFormats.OpenXML && 
                    oParentCC != null && oRng.Start == oParentCC.Range.Start - 1)
                {
                    object oUnit = Word.WdUnits.wdCharacter;
                    object oCount = 1;
                    oRng.Move(ref oUnit, ref oCount);
                }
                //select insertion location
                oRng.Select();

                try
                {
                    //insert new segment
                    //GLOG 8400
                    Segment oSeg = oMPDocument.InsertSegment(xNewSegmentID, oParent,
                        Segment.InsertionLocations.InsertAtSelection,
                        Segment.InsertionBehaviors.Default, true, oPrefill,
                        xBodyText, false, bUpdateStyles, null, bDesign, false, 
                        oChildStructure);

                    oSeg.Refresh();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentException(
                        LMP.Resources.GetLangString("Error_CouldNotInsertSegment") +
                        xNewSegmentID, oE);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;

                //GLOG 3118 - clear flag
                oMPDocument.ReplaceSegmentNodeIndex = -1;
            }
        }

        /// <summary>
        /// inserts the segment with the specified ID for design purposes
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oMPDocument"></param>
        /// <returns></returns>
        internal static Segment InsertForDesign(string xSegmentID, ForteDocument oMPDocument)
        {
            return SetUpForDesign(xSegmentID, oMPDocument, true);
        }
        /// <summary>
        /// sets up the specified segment for design - 
        /// assumes that valid xml exists in the specified document
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oMPDocument"></param>
        /// <returns></returns>
        internal static Segment SetUpForDesign(string xSegmentID, ForteDocument oMPDocument)
        {
            return SetUpForDesign(xSegmentID, oMPDocument, false);
        }
        /// <summary>
        /// sets up the specified segment for design purposes -
        /// inserts the segment xml if specified, else assumes
        /// that valid xml exists in the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the segment definition</param>
        /// <param name="oMPDocument">MacPac document representing the Word document into which the segment will be inserted.</param>
        /// <param name="bInsertXML">determines if the segment xml will be inserted as part of the setup</param>
        /// <returns></returns>
        private static Segment SetUpForDesign(string xSegmentID, ForteDocument oMPDocument, bool bInsertXML)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                ISegmentDef oDef = GetDefFromID(xSegmentID);

                //GLOG 2250 (2/13/09, dm) - LSIGs and BSIGs that existed before
                //these were changed to collection classes may not be in tables -
                //create segment object as corresponding non-table segment type
                //and modify definition accordingly
                if (((oDef.TypeID == mpObjectTypes.AgreementSignature) ||
                    (oDef.TypeID == mpObjectTypes.LetterSignature)) &&
                    !Segment.IsTableSegment(oDef.XML))
                {
                    string xSearch = "ObjectTypeID=" + ((Int32)oDef.TypeID).ToString() + "|";
                    if (oDef.TypeID == mpObjectTypes.AgreementSignature)
                    {
                        oDef.XML = oDef.XML.Replace(xSearch, "ObjectTypeID=" +
                            ((Int32)mpObjectTypes.AgreementSignatureNonTable).ToString() + "|");
                        oDef.TypeID = mpObjectTypes.AgreementSignatureNonTable;
                    }
                    else
                    {
                        oDef.XML = oDef.XML.Replace(xSearch, "ObjectTypeID=" +
                            ((Int32)mpObjectTypes.LetterSignatureNonTable).ToString() + "|");
                        oDef.TypeID = mpObjectTypes.LetterSignatureNonTable;
                    }
                }

                //create the appropriate derived segment object -
                //determined by the object type of the segment definition -
                //we need to see if the definition is for a user or admin segment -
                //segments designed by a content designer will have definition
                //of user segment, but a type ID that is admin segment (e.g. Letter)
                LMP.Data.mpObjectTypes iTypeID;

                if (oDef is AdminSegmentDef)
                    iTypeID = oDef.TypeID;
                else
                    iTypeID = mpObjectTypes.UserSegment;

                Segment oSegment = CreateSegmentObject(iTypeID, oMPDocument);

                //set user segment definition
                oSegment.Definition = oDef;

                //set FixedTopLevelSegment property
                oMPDocument.FixedTopLevelSegment = oSegment;

                //disable Word event handlers
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                //enter design mode
                oMPDocument.Mode = ForteDocument.Modes.Design;

                //TODO: modify document based on segment insertion options
                if (bInsertXML)
                {
                    if (oDef.XML != "")
                    {
                        //preconvert if necessary (dm 2/9/11)
                        string xXML = oDef.XML;

                        //GLOG 5932 (dm) - don't do if preinjunction and file format is .doc -
                        //adding the bookmarks isn't necessary in this case and was causing
                        //problem in Cleary's letterhead in Word 2007 - the Reserved attribute
                        //and doc vars will be added by RefreshTags if it hasn't been done here
                        if ((!LMP.String.IsWordOpenXML(xXML)) &&
                            (LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion() ||
                            (oMPDocument.FileFormat == mpFileFormats.OpenXML)))
                        {
                            int iID1;
                            int iID2;
                            LMP.Data.Application.SplitID(xSegmentID, out iID1, out iID2);
                            xXML = LMP.Conversion.PreconvertIfNecessary(xXML, false, iID1.ToString());
                        }

                        ////massage xml based on bounding object of target
                        //LMP.Data.mpFileFormats oSegmentBoundingObjectType =
                        //    LMP.Data.Application.GetBoundingObjectType(oDef.XML);
                        //LMP.Data.mpFileFormats oDocBoundingObjectType =
                        //    oMPDocument.FileFormat;

                        //6-25-10 (dm) - all segment def xml will have been preconverted,
                        //so add doc vars from source xml to target document
                        //12-10-10 (dm) - moved before InsertXML(), since retagging
                        //in Word 2010 now occurs immediately after xml insertion and
                        //doc vars are required for that
                        LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(
                            oMPDocument.WordDocument, xXML);

                        //insert segment xml
                        oSegment.InsertXML(xXML, null, oMPDocument.WordDocument.Content, 
                            LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace, true, 0);

                        //Update styles - otherwise only in-use styles will be included
                        oMPDocument.UpdateStylesFromXML(xXML);
                    }
                    else
                        //empty xml not allowed for segment
                        throw new LMP.Exceptions.SegmentDefinitionException(
                            LMP.Resources.GetLangString("Error_InvalidSegmentXML") + xSegmentID);
                }

                LMP.Forte.MSWord.WordDoc.RemoveUnavailableXMLSchema(oSegment.ForteDocument.WordDocument);

                //refresh document tags
                oMPDocument.RefreshTags(false);

                //reenable Word event handlers
                LMP.Architect.Api.ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                //get full tag id of the inserted segment
                string xTagID = "";

                try
                {
                    xTagID = GetNewSegmentTagID(oMPDocument);
                }
                catch
                {
                    //tag ID isn't where it should be - alert
                    throw new LMP.Exceptions.BookmarkException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidOrMissingNewSegmentBookmark"));
                }

                //initialize segment with tag data, etc.
                oSegment.Initialize(xTagID);

                oSegment.CreationStatus = Status.BoilerplateInserted;

                //restore deleted scopes for editing
                oSegment.RestoreDeletedScopesForDesign();

                //insert placeholder text
                LMP.Forte.MSWord.WordDoc.InsertPlaceholderText(oMPDocument.Tags);

                //hide TagExpandedValue field codes in designer -
                //10.2 (dm) - skip this for now when using content controls -
                //it's not yet clear whether this will ultimately be necessary
                if (oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oSegment.RefreshExpandedTagValueCodes(false);

                //add the segment to the collection
                oMPDocument.Segments.Add(oSegment);

                //set segment status
                oSegment.CreationStatus = Status.Finished;

                //select start of mSEG
                Word.Range oSegmentRng = oSegment.PrimaryRange;
                object oMissing = System.Reflection.Missing.Value;
                oSegmentRng.Collapse(ref oMissing);
                oSegmentRng.Select();

                LMP.Benchmarks.Print(t0);

                return oSegment;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotEnterDesignMode"), oE);
            }
        }
        /// <summary>
        /// returns the segment with the specified tag id in the specified document
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="oParent"></param>
        /// <returns></returns>
        internal static Segment GetSegment(string xTagID, ForteDocument oMPDocument)
        {
            return GetSegment(xTagID, oMPDocument, true, true);
        }
        internal static Segment GetSegment(string xTagID, ForteDocument oMPDocument, bool bExecuteActions, bool bPromptForMissingAuthors)
        {
            return GetSegment(xTagID, oMPDocument, bExecuteActions, bPromptForMissingAuthors, true);
        }
        internal static Segment GetSegment(string xTagID, ForteDocument oMPDocument, bool bExecuteActions, bool bPromptForMissingAuthors, bool bForceHashtable)
        {
            try
            {
                //get node store containing only those
                //nodes for the specified tag id
                NodeStore oNodes = new NodeStore(oMPDocument.Tags, xTagID,
                    mpNodeStoreMatchTypes.Equals, bForceHashtable);

                //get segment id from segment node
                string xTypeID = null;
                mpObjectTypes iTypeID = 0;

                try
                {
                    //get segment type ID from mSEG
                    xTypeID = oNodes.GetItemObjectDataValue(xTagID, "ObjectTypeID");
                    iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
                }

                //create segment object
                Segment oSegment = Segment.CreateSegmentObject(iTypeID, oMPDocument);

                //set nodes since we've already retrieved them
                oSegment.Nodes = oNodes;

                oSegment.Initialize(xTagID);

                oSegment.PromptForMissingAuthors = bPromptForMissingAuthors;

                if (oSegment.ForteDocument.Mode != ForteDocument.Modes.Design &&
                    (oSegment.Parent == null || !oSegment.LinkAuthorsToParent))
                {
                    oSegment.m_bAllowUpdateForAuthor = false;
                    if (bForceHashtable)
                        oSegment.SetAuthorsFromDocument(bExecuteActions);
                    else
                    {
                        string xAuthorsXML = null;
                        try
                        {
                            xAuthorsXML = oNodes.GetItemAuthorsData(xTagID);
                        }
                        catch {}
                        if (xAuthorsXML != null)
                        {
                            oSegment.Authors.XML = xAuthorsXML;
                        }
                        else
                            oSegment.SetAuthorsFromDocument(bExecuteActions);
                    }

                    oSegment.m_bAllowUpdateForAuthor = true;
                }

                //the segment has already been created in the document
                //- we're just retrieving the code construct
                oSegment.CreationStatus = Status.Finished;
                return oSegment;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotGetSegment") + xTagID, oE);
            }
        }
        /// <summary>
        /// returns the tag ID of the nearest common ancestor of the segments with the
        /// specified tag IDs - if the specified tag ids are the same, returns the same ID -
        /// if no common ancestor is found, returns null
        /// </summary>
        /// <param name="xSegment1TagID"></param>
        /// <param name="xSegment2TagID"></param>
        /// <returns></returns>
        public static string GetNearestCommonAncestorTagID(string xSegment1TagID,
            string xSegment2TagID)
        {
            string xCommon = null;
            string xAncestor1 = null;
            string xAncestor2 = null;

            //if specified IDs are the same, return the same ID
            if (xSegment1TagID == xSegment2TagID)
                return xSegment1TagID;

            //get each segment's top level ancestor 
            int iPos1 = xSegment1TagID.IndexOf('.');
            int iPos2 = xSegment2TagID.IndexOf('.');
            if (iPos1 == -1)
                xAncestor1 = xSegment1TagID;
            else
                xAncestor1 = xSegment1TagID.Substring(0, iPos1);
            if (iPos2 == -1)
                xAncestor2 = xSegment2TagID;
            else
                xAncestor2 = xSegment2TagID.Substring(0, iPos2);

            //cycle through each successive generation
            //until there's no longer a common ancestor
            while (xAncestor1 == xAncestor2)
            {
                xCommon = xAncestor1;
                if (iPos1 != -1)
                    iPos1 = xSegment1TagID.IndexOf('.', iPos1 + 1);
                if (iPos2 != -1)
                    iPos2 = xSegment2TagID.IndexOf('.', iPos2 + 1);
                if ((iPos1 == -1) || (iPos2 == -1))
                    break;
                xAncestor1 = xSegment1TagID.Substring(0, iPos1);
                xAncestor2 = xSegment2TagID.Substring(0, iPos2);
            }

            //return common ancestor
            return xCommon;
        }
        /// <summary>
        /// inserts the specified XML at the specified location -
        /// executes a generic targeted insertion, inserting 
        /// headers/footers/textframes independently
        /// </summary>
        /// <param name="xXML">xml to insert</param>
        /// <param name="oLocation">location to begin insertion</param>
        /// <param name="bInsertHeadersFooters">if true, will insert header/footer xml</param>
        internal static void InsertXML(string xXML, Word.Range oLocation, 
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, 
            LMP.Forte.MSWord.mpSectionOnePageSetupOptions iDoSectionOnePageSetup, bool bTargeted, 
            mpSegmentIntendedUses iIntendedUse, mpObjectTypes iObjectType, ForteDocument oForteDoc) //GLOG 6983
        {
            DateTime t0 = DateTime.Now;
            //bool bConvertedXML = false;

            Trace.WriteNameValuePairs("iHeaderFooterInsertionType",
                iHeaderFooterInsertionType.ToString(),
                "iPageSetupExecutionType", iDoSectionOnePageSetup);

            ////TODO: If possible, check if current Word version supports XML Tags
            //if (!LMP.String.IsWordOpenXML(xXML) && oLocation.Document.SaveFormat == 0)
            //{
            //    xXML = Conversion.PreconvertIfNecessary(xXML);
            //    bConvertedXML = true;
            //}

            //parse segment xml
            string xBodyXML;
            string xSectionsXML;
            string xPageSetupValues;
            string xCompatOptions;

            //call COM to do the insertion
      

            //disable Word event handlers
            //GLOG 7048 (dm) - no need to create ForteDocument now that IgnoreWordXMLEvents is static
            //ForteDocument oMPDoc = new ForteDocument(oLocation.Document);
            //GLOG 7404 (dm) - get current value before disabling
            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

            //Make sure MP Schema is attached before inserting
            LMP.Forte.MSWord.WordDoc.AddMP10SchemaReference(oLocation.Document); //GLOG 7048 (dm)

            //GLOG 6983: No longer determined Object Type from XML

            short shObjectTypeID = (short)iObjectType;
            //get targeted xml from specified XML string
            ParseSegmentXML(xXML, out xBodyXML, out xSectionsXML, out xPageSetupValues, out xCompatOptions);
            //GLOG 8836: Don't replace Page Setup and body page break in Design mode
            if (oForteDoc.Mode != Base.ForteDocument.Modes.Design)
            {
                if ((iObjectType == mpObjectTypes.TOA || iObjectType == mpObjectTypes.PleadingExhibit)
                    && iDoSectionOnePageSetup == Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True
                    &&  oForteDoc.WordDocument.Sections.Count > 1)
                {
                    //GLOG 8770: If inserting TOA or PleadingExhibit into document, use existing margins instead of those in XML
                    string[] arPS = xPageSetupValues.Split('�');
                    StringBuilder oSB = new StringBuilder();
                    arPS[4] = (oForteDoc.WordDocument.Application.Selection.Sections[1].PageSetup.LeftMargin * 20).ToString();
                    arPS[5] = (oForteDoc.WordDocument.Application.Selection.Sections[1].PageSetup.RightMargin * 20).ToString();
                    for (int i = 0; i < arPS.GetLength(0); i++)
                    {
                        oSB.Append(arPS[i]);
                        if (i < arPS.GetUpperBound(0))
                            oSB.Append("�");
                    }
                    xPageSetupValues = oSB.ToString();
                }
                //This can appear 2 ways, depending on which version of Word was used to save
                xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                    "<w:body><w:p/><w:sectPr>");
                xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
                    "<w:body><wx:sect><w:p/><w:sectPr>");

                //GLOG 5403 (dm) - the preconversion process adds a space to the w:br node
                if (!LMP.String.IsWordOpenXML(xXML))
                {
                    xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                        "<w:body><w:p/><w:sectPr>");
                    xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
                        "<w:body><wx:sect><w:p/><w:sectPr>");
                }
            }
            if (bTargeted)
            {
                short shIntendedUse = (short)iIntendedUse;
                //insert xml using the 'targeted' technique
                LMP.Forte.MSWord.WordDoc.InsertTargetedSegmentXML(oLocation, xBodyXML, xSectionsXML,
                    xPageSetupValues, xCompatOptions, iHeaderFooterInsertionType, iDoSectionOnePageSetup, 
                    shObjectTypeID, shIntendedUse, true);
            }
            else
            {
                //GLOG 6983: ObjectType now passed as a parameter
                LMP.Forte.MSWord.mpPleadingCollectionTypes iCollectionType;
                switch (iObjectType)
                {
                    case mpObjectTypes.PleadingCounsels:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingCounsels;
                        break;
                    case mpObjectTypes.PleadingCaptions:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingCaptions;
                        break;
                    case mpObjectTypes.PleadingSignatures:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingSignatures;
                        break;
                    default:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.None;
                        break;
                }
                
                //insert xml in one shot
                LMP.Forte.MSWord.WordDoc.InsertSegmentXML(oLocation, xXML, xPageSetupValues, xCompatOptions,
                    iDoSectionOnePageSetup, iCollectionType);
            }

//            if (bConvertedXML)
//            {
//#if StripTags
//                //Force removal of all tags to simulate InsertXML with CustomXML patch
//                object oUrn = "urn-legalmacpac-data/10";

//                Word.XMLSchemaReference oSchema = null;

//                try
//                {
//                    oSchema = oLocation.Document.XMLSchemaReferences.get_Item(ref oUrn);
//                }
//                catch { }

//                if (oSchema != null)
//                    oSchema.Delete();
//#endif
//                LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();

//                //LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
//                //oConvert.ConvertBookmarksToTags(oLocation.Document, true);
//            }

            //re-enable Word event handlers
            //GLOG 7404 (dm) - restore starting value
            ForteDocument.IgnoreWordXMLEvents = iEvents;

            LMP.Benchmarks.Print(t0, bTargeted ? "Targeted" : "Not Targeted");
        }

        /// <summary>
        /// returns all segments of shType that start in oSection
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="oSection"></param>
        /// <param name="shType"></param>
        /// <returns></returns>
        public static Segments GetSegments(ForteDocument oMPDocument, Word.Section oSection,
            mpObjectTypes shType)
        {
            return GetSegments(oMPDocument, oSection, shType, 0);
        }

        /// <summary>
        /// returns all segments that start in oSection
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="oSection"></param>
        /// <returns></returns>
        public static Segments GetSegments(ForteDocument oMPDocument, Word.Section oSection)
        {
            return GetSegments(oMPDocument, oSection, mpObjectTypes.Segment, 0);
        }

        /// <summary>
        /// returns the specified number of segments of shType
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="shType"></param>
        /// <param name="iCount"></param>
        /// <returns></returns>
        public static Segments GetSegments(ForteDocument oMPDocument, mpObjectTypes shType,
            int iCount)
        {
            return GetSegments(oMPDocument, null, shType, iCount);
        }

        /// <summary>
        /// returns all segments of shType in oMPDocument
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="shType"></param>
        /// <returns></returns>
        public static Segments GetSegments(ForteDocument oMPDocument, mpObjectTypes shType)
        {
            return GetSegments(oMPDocument, null, shType, 0);
        }

        /// <summary>
        /// returns the specified number of segments of shType in oMPDocument -
        /// if oSection is provided, limits to segments that start in that section
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="oSection"></param>
        /// <param name="shType"></param>
        /// <param name="iCount"></param>
        /// <returns></returns>
        public static Segments GetSegments(ForteDocument oMPDocument, Word.Section oSection,
            mpObjectTypes shType, int iCount)
        {
            DateTime t0 = DateTime.Now;

            Segments oSegments = new Segments(oMPDocument);

            //get node store for entire document
            NodeStore oNodes = new NodeStore(oMPDocument.Tags);

            //get all nodes of specified type
            string xXPath;
            if (shType != mpObjectTypes.Segment)
            {
                string xTypeID = ((short)shType).ToString();
                xXPath = @"//Node[@ElementName='mSEG' and contains(@ObjectData, '|ObjectTypeID=" +
                    xTypeID + "|')]";
            }
            else
            {
                //no object type specified
                xXPath = @"//Node[@ElementName='mSEG']";
            }

            ReadOnlyNodeStore oTypeNodes = oNodes.GetNodes(xXPath);

            if (oTypeNodes.Count > 0)
            {
                //cycle through found nodes, looking for associated tags
                //that start in oSection
                int iFound = 0;
                int iSecStart = 0;
                if (oSection != null)
                    iSecStart = oSection.Range.Start;

                for (int i = 0; i < oTypeNodes.Count; i++)
                {
                    //get tag id
                    string xTagID = oTypeNodes.GetItemTagID(i);

                    //get associated tags
                    if (oMPDocument.FileFormat == mpFileFormats.Binary)
                    {
                        Word.XMLNode[] oTags = oNodes.GetWordTags(xTagID);
                        if (oTags.Length > 0)
                        {
                            foreach (Word.XMLNode oTag in oTags)
                            {
                                if (oSection == null ||
                                    oTag.Range.Sections.First.Range.Start == iSecStart)
                                {
                                    //we've found a tag in the right section - get the segment
                                    Segment oSegment = oMPDocument.FindSegment(xTagID);

                                    //add to collection
                                    oSegments.Add(oSegment);
                                    iFound++;

                                    //return collection if we've reached the number desired
                                    if (iFound == iCount)
                                        return oSegments;
                                }
                            }
                        }
                        else
                        {
                            //get bookmarks
                            Word.Bookmark[] oBmks = oNodes.GetBookmarks(xTagID);
                            if (oBmks.Length > 0)
                            {
                                foreach (Word.Bookmark oBmk in oBmks)
                                {
                                    if (oSection == null ||
                                        oBmk.Range.Sections.First.Range.Start == iSecStart)
                                    {
                                        //we've found a tag in the right section - get the segment
                                        Segment oSegment = oMPDocument.FindSegment(xTagID);

                                        //add to collection
                                        oSegments.Add(oSegment);
                                        iFound++;

                                        //return collection if we've reached the number desired
                                        if (iFound == iCount)
                                            return oSegments;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Word.ContentControl[] oCCs = oNodes.GetContentControls(xTagID);
                        if (oCCs.Length > 0)
                        {
                            foreach (Word.ContentControl oCC in oCCs)
                            {
                                if (oSection == null ||
                                    oCC.Range.Sections.First.Range.Start == iSecStart)
                                {
                                    //we've found a tag in the right section - get the segment
                                    Segment oSegment = oMPDocument.FindSegment(xTagID);

                                    //add to collection
                                    oSegments.Add(oSegment);
                                    iFound++;

                                    //return collection if we've reached the number desired
                                    if (iFound == iCount)
                                        return oSegments;
                                }
                            }
                        }
                        else
                        {
                            //get bookmarks
                            Word.Bookmark[] oBmks = oNodes.GetBookmarks(xTagID);
                            if (oBmks.Length > 0)
                            {
                                foreach (Word.Bookmark oBmk in oBmks)
                                {
                                    if (oSection == null ||
                                        oBmk.Range.Sections.First.Range.Start == iSecStart)
                                    {
                                        //we've found a tag in the right section - get the segment
                                        Segment oSegment = oMPDocument.FindSegment(xTagID);
                                        
                                        //add to collection
                                        oSegments.Add(oSegment);
                                        iFound++;

                                        //return collection if we've reached the number desired
                                        if (iFound == iCount)
                                            return oSegments;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            LMP.Benchmarks.Print(t0);

            return oSegments;
        }

        /// <summary>
        /// returns all of the segments in the document that are set to display
        /// in the toolkit task pane - added for GLOG 7101 (dm)
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <returns></returns>
        public static void GetToolkitTaskPaneSegments(Segments oSegments,
            ref Segments oToolkitSegments)
        {
            for (int i = 0; i < oSegments.Count; i++)
            {
                Segment oSegment = oSegments[i];
                if (LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID))
                    oToolkitSegments.Add(oSegment);
                GetToolkitTaskPaneSegments(oSegment.Segments, ref oToolkitSegments);
            }
        }

        /// <summary>
        /// modifies the provided xml string by adding the id of the specified parent
        /// to the object data of all parts of the top-level segment
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oParent"></param>
        internal static void AddParentIDToXML(ref string xXML, Segment oParent)
        {
            //build ParentTagID string
            string xParentTagID = oParent.FullTagID;
            int lPos = xParentTagID.LastIndexOf('.');
            if (lPos > -1)
                xParentTagID = xParentTagID.Substring(lPos + 1);
            string xParentIDString = "ParentTagID=" + xParentTagID + "|";

            //remove empty ParentTagID property throughout
            xXML = xXML.Replace("ParentTagID=|", "");

            //cycle through xml, adding ParentTagID to all parts of the top-level segment
            string xTagID = null;
            //GLOG 4920 (10.2): Different method of parsing XML for Content Controls
            //1-28-11 (dm) - we need to update the doc var in WordML as well
            //if (LMP.String.IsWordOpenXML(xXML))
            //{
                bool bAdd;
                XmlDocument oXMLDoc = new XmlDocument();
                oXMLDoc.LoadXml(xXML);

                string xNamespacePrefix;
                XmlNamespaceManager oNamespaceManager = new XmlNamespaceManager(oXMLDoc.NameTable);
                if (LMP.String.IsWordOpenXML(xXML))
                {
                    xNamespacePrefix = String.GetNamespacePrefix(xXML,
                        ForteConstants.WordOpenXMLNamespace);
                    oNamespaceManager.AddNamespace(xNamespacePrefix,
                        ForteConstants.WordOpenXMLNamespace);
                }
                else
                {
                    xNamespacePrefix = String.GetNamespacePrefix(xXML,
                        ForteConstants.WordNamespace);
                    oNamespaceManager.AddNamespace(xNamespacePrefix,
                        ForteConstants.WordNamespace);
                }

                //7-22-10 (dm) - the order of doc var nodes is arbitary, so the first
                //mSEG doc var is not necessarily the top-level segment - modified the
                //query to include the target segment id
                string xSegmentID = Segment.GetSegmentIDFromXML(xXML);
                string xmSEGTest = "//w:docVar[contains(@w:val, '��ObjectData=SegmentID=" +
                    xSegmentID + "|')]";
                XmlNodeList oMatches = oXMLDoc.SelectNodes(xmSEGTest, oNamespaceManager);
                foreach (XmlNode oMatch in oMatches)
                {
                    bAdd = false;
                    XmlNode oVal = oMatch.SelectSingleNode("@w:val", oNamespaceManager);
                    if (oVal != null)
                    {
                        string xVal = oVal.Value;
                        if (xTagID == null)
                        {
                            int iPos = xVal.IndexOf("��");
                            xTagID = xVal.Substring(0, iPos);
                            bAdd = true;
                        }
                        else
                        {
                            bAdd = xVal.StartsWith(xTagID + "��");

                        }

                        //insert ParentTagID
                        if (bAdd)
                        {
                            //strip existing ParentTagID
                            int iPos2 = xVal.IndexOf("ParentTagID=");
                            if (iPos2 > -1) 
                            {
                                int iPos3 = xVal.IndexOf('|', iPos2);
                                if (iPos3 > -1 && iPos3 < xVal.Length)
                                    xVal = xVal.Substring(0, iPos2) + xVal.Substring(iPos3+1);
                                else
                                    xVal = xVal.Substring(0, iPos2);
                            }

                            //insert after ObjectTypeID
                            iPos2 = xVal.IndexOf("ObjectTypeID=");
                            int iPos4 = xVal.IndexOf('|', iPos2);
                            xVal = xVal.Insert(iPos4 + 1, xParentIDString);
                            oVal.Value = xVal;
                        }
                    }
                }
                xXML = oXMLDoc.OuterXml;
            //}

            if (!LMP.String.IsWordOpenXML(xXML))
            {
                //get prefix assigned to Forte namespace
                string xPrefix = LMP.String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);
                string xmSEGStart = "<" + xPrefix + ":mSEG ";
                int iPos = xXML.IndexOf(xmSEGStart);
                while (iPos > -1)
                {
                    //mSEG found - determine whether this a target tag
                    bAdd = false;
                    int iPos2 = xXML.IndexOf('>', iPos);
                    string xStartTag = xXML.Substring(iPos, iPos2 - iPos);
                    int iTagIDStartPos = xStartTag.IndexOf("TagID=" + '\"') + 7;
                    int iTagIDEndPos = xStartTag.IndexOf('\"', iTagIDStartPos);

                    if (xTagID == null)
                    {
                        //this is the first mSEG - get tag id and flag to add
                        xTagID = xStartTag.Substring(iTagIDStartPos,
                            iTagIDEndPos - iTagIDStartPos);
                        bAdd = true;
                    }
                    else
                    {
                        //add if tag id matches target
                        bAdd = (xStartTag.Substring(iTagIDStartPos,
                            iTagIDEndPos - iTagIDStartPos) == xTagID);
                    }

                    //insert ParentTagID
                    if (bAdd)
                    {
                        //strip existing ParentTagID
                        int iPos3 = xXML.IndexOf("ParentTagID", iPos);
                        if ((iPos3 > -1) && (iPos3 < iPos2))
                        {
                            int iPos4 = xXML.IndexOf('|', iPos3) + 1;
                            xXML = xXML.Substring(0, iPos3) + xXML.Substring(iPos4);
                        }

                        //insert after ObjectTypeID
                        iPos3 = xXML.IndexOf("ObjectTypeID=", iPos);
                        iPos3 = xXML.IndexOf('|', iPos3);
                        xXML = xXML.Insert(iPos3 + 1, xParentIDString);
                    }

                    //look for next mSEG
                    iPos = xXML.IndexOf(xmSEGStart, iPos + 1);
                }
            }
            //GLOG 8658: Remove any unexpected whitespace
            xXML = xXML.Replace(" />", "/>");
        }

        /// <summary>
        /// if the specified segment xml and insertion range will leave the inserted
        /// top-level segment physically outside of the specified parent segment, modifies
        /// the xml by adding the id of the specified parent to the object data of all parts
        /// of the inserted top-level segment
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oParent"></param>
        /// <param name="oInsertRange"></param>
        internal static void AddParentIDToXMLIfNecessary(ref string xXML, Segment
            oParent, Word.Range oInsertRange)
        {
            //exit if no parent specified
            if (oParent == null)
                return;

            //determine whether the segment has any parts in the body - if not,
            //it will be an external child regardless of where it is inserted
            bool bIsInParent = false;
            bool bTargetIsBody = Segment.TargetIsBody(xXML);
            if (bTargetIsBody)
            {
                //segment is in body - get the mSEG in the document (if any) containing
                //the range at which it will be inserted
                if (LMP.String.IsWordOpenXML(xXML))
                {
                    //GLOG 4920: Different handling for Content Controls
                    Word.ContentControl oParentCC = LMP.Forte.MSWord.WordDoc.GetParentSegmentContentControl(oInsertRange);
                    if (oParentCC != null)
                    {
                        //the insertion range is in an mSEG - see if it's the specified parent
                        string xParentTagID = oParent.FullTagID;
                        int lPos = xParentTagID.LastIndexOf('.');
                        if (lPos > -1)
                            xParentTagID = xParentTagID.Substring(lPos + 1);
                        bIsInParent = (LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(oParentCC, "TagID") == xParentTagID);
                    }
                }
                else
                {
                    Word.XMLNode oParentSegTag = LMP.Forte.MSWord.WordDoc.GetParentSegmentTag(oInsertRange);

                    if (oParentSegTag != null)
                    {
                        //the insertion range is in an mSEG - see if it's the specified parent
                        string xParentTagID = oParent.FullTagID;
                        int lPos = xParentTagID.LastIndexOf('.');
                        if (lPos > -1)
                            xParentTagID = xParentTagID.Substring(lPos + 1);
                        bIsInParent = (oParentSegTag.SelectSingleNode("@TagID", "",
                            false).NodeValue == xParentTagID);
                    }
                }
            }

            if (!bIsInParent)
            {
                //this is an external child - add parent tag id to xml
                Segment.AddParentIDToXML(ref xXML, oParent);

                //update parent segment's object data
                oParent.Nodes.SetItemObjectDataValue(oParent.FullTagID,
                    "ExternalChildren", "true");
            }
        }

        /// <summary>
        /// inserts wrapper segment for segment xID if specified in oDef and not already
        /// getting inserted into a segment of the same type by virtue of oParent or
        /// oLocation - if the xml for the wrapper includes segment xID, returns the
        /// resulting wrapped segment - if the xml includes another wrapped segment,
        /// deletes that segment after insertion, modifying oLocation accordingly
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="oDef"></param>
        /// <param name="oParent"></param>
        /// <param name="oLocation"></param>
        /// <param name="iInsertionBehavior"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="oPrefill"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="bInsertingInMainStory"></param>
        /// <param name="oInsertedChild"></param>
        private static void InsertWrapperIfNecessary(string xID, ISegmentDef oDef,
            ref Segment oParent, ref Word.Range oLocation,
            ref Segment.InsertionBehaviors iInsertionBehavior,
            ForteDocument oMPDocument, Prefill oPrefill, bool bTargetXMLInsertion,
            bool bInsertingInMainStory, ref Segment oInsertedChild)
        {
            bool bContentControls = oMPDocument.FileFormat == 
                LMP.Data.mpFileFormats.OpenXML;

            string xWrapperID = "";
            //GLOG 6920: If this is a Saved Segment, get Wrapper for Segment in XML
            if (oDef.TypeID == mpObjectTypes.SavedContent)
            {
                string xContainedID = oDef.ChildSegmentIDs;

                if (!xContainedID.Contains(".") || xContainedID.EndsWith(".0"))
                {
                    //this is an admin segment
                    int iID1;
                    int iID2;
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    LMP.Data.Application.SplitID(xContainedID, out iID1, out iID2);
                    ISegmentDef oSegDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                    xWrapperID = Segment.GetWrapperID(oSegDef, bContentControls);
                }
                else
                {
                    //this is a user segment
                    //GLOG 4728: Filter collection by User to include accessible objects
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                    ISegmentDef oSegDef = (UserSegmentDef)oDefs.ItemFromID(xContainedID);
                    xWrapperID = Segment.GetWrapperID(oSegDef, bContentControls);
                }

            }
            else
            {
                //get wrapper segment for type - these are built into collection item types
                xWrapperID = Segment.GetWrapperID(oDef, bContentControls);
            }
            if (xWrapperID == "")
                //this isn't a collection item type
                return;

            //get the segment def for the wrapper
            ISegmentDef oWrapperDef = Segment.GetDefFromID(xWrapperID);
            mpObjectTypes oWrapperType = oWrapperDef.TypeID;

            //exit if already 'wrapped', i.e. specified or prospective parent
            //is a segment of the same type as the default wrapper
            if (oParent == null)
            {
                //look for adjacent collection table of appropriate type
                LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();
                if (bContentControls)
                {
                    Word.ContentControl oParentCC = oPleading.GetCollectionTableContentControl(oLocation,
                        (short)oWrapperType);
                    if (oParentCC == null)
                    {
                        //GLOG 3701 (dm) - we decided to no longer seek an existing table
                        //elsewhere in the document
                        ////look for table elsewhere in doc
                        //oParent = Segment.FindSegment(oWrapperType, oMPDocument.Segments);
                        //if (oParent != null)
                        //{
                        //    //table of appropriate type exists
                        //    oParent.PrimaryWordTag.Range.Select();
                        //    return;
                        //}
                        Word.Bookmark oParentBmk = oPleading.GetCollectionTableBookmark(oLocation, (short)oWrapperType);
                        if (oParentBmk != null)
                        {
                            //parent tag found
                            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
                            while (xTagID != "")
                            {
                                Segment oContext = oMPDocument.FindSegment(xTagID);
                                if (oContext.TypeID == oWrapperType)
                                    //exit if already wrapped
                                    return;
                                else
                                {
                                    //check all ancestors
                                    int iPos = xTagID.LastIndexOf('.');
                                    if (iPos > -1)
                                        xTagID = xTagID.Substring(0, iPos);
                                    else
                                        xTagID = "";
                                }
                            }
                        }
                        else
                        {
                            if (LMP.Forte.MSWord.WordDoc.InsertingTableIntoTable(oDef.XML, oLocation))
                                //don't insert wrapper if insertion of child will be disallowed
                                return;
                        }
                    }
                    else
                    {
                        //parent tag found
                        string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oParentCC);
                        while (xTagID != "")
                        {
                            Segment oContext = oMPDocument.FindSegment(xTagID);
                            if (oContext.TypeID == oWrapperType)
                                //exit if already wrapped
                                return;
                            else
                            {
                                //check all ancestors
                                int iPos = xTagID.LastIndexOf('.');
                                if (iPos > -1)
                                    xTagID = xTagID.Substring(0, iPos);
                                else
                                    xTagID = "";
                            }
                        }
                    }
                }
                else
                {
                    Word.XMLNode oParentTag = oPleading.GetCollectionTableTag(oLocation,
                        (short)oWrapperType);
                    if (oParentTag == null)
                    {
                        //GLOG 3701 (dm) - we decided to no longer seek an existing table
                        //elsewhere in the document
                        ////look for table elsewhere in doc
                        //oParent = Segment.FindSegment(oWrapperType, oMPDocument.Segments);
                        //if (oParent != null)
                        //{
                        //    //table of appropriate type exists
                        //    oParent.PrimaryWordTag.Range.Select();
                        //    return;
                        //}
                        Word.Bookmark oParentBmk = oPleading.GetCollectionTableBookmark(oLocation, (short)oWrapperType);
                        if (oParentBmk != null)
                        {
                            //parent tag found
                            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
                            while (xTagID != "")
                            {
                                Segment oContext = oMPDocument.FindSegment(xTagID);
                                if (oContext.TypeID == oWrapperType)
                                    //exit if already wrapped
                                    return;
                                else
                                {
                                    //check all ancestors
                                    int iPos = xTagID.LastIndexOf('.');
                                    if (iPos > -1)
                                        xTagID = xTagID.Substring(0, iPos);
                                    else
                                        xTagID = "";
                                }
                            }
                        }
                        else
                        {
                            if (LMP.Forte.MSWord.WordDoc.InsertingTableIntoTable(oDef.XML, oLocation))
                                //don't insert wrapper if insertion of child will be disallowed
                                return;
                        }
                    }
                    else
                    {
                        //parent tag found
                        string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oParentTag);
                        while (xTagID != "")
                        {
                            Segment oContext = oMPDocument.FindSegment(xTagID);
                            if (oContext.TypeID == oWrapperType)
                                //exit if already wrapped
                                return;
                            else
                            {
                                //check all ancestors
                                int iPos = xTagID.LastIndexOf('.');
                                if (iPos > -1)
                                    xTagID = xTagID.Substring(0, iPos);
                                else
                                    xTagID = "";
                            }
                        }
                    }
                }
            }
            else if (oParent.TypeID == oWrapperType)
                //exit if already wrapped
                return;

            //insert wrapper segment
            oLocation.Select();
            Segment oWrapper = Segment.Insert(xWrapperID, oParent,
                InsertionLocations.InsertAtSelection, iInsertionBehavior, oMPDocument,
                oPrefill, bTargetXMLInsertion, bInsertingInMainStory, null, false,
                false, false, null);

            //determine whether specified segment was inserted with wrapper
            Word.Range oRng = null;
            Segment oChild = null;
            if (oWrapper.Segments.Count > 0)
            {
                oChild = oWrapper.Segments[0];
                if (oChild.ID1.ToString() == xID)
                {
                    //the wrapper already contains the correct segment
                    oInsertedChild = oChild;
                    return;
                }
                else
                {
                    //replace current child
                    if ((oChild is LMP.Architect.Api.CollectionTableItem) &&
                        (oWrapper is LMP.Architect.Api.CollectionTable))
                    {
                        LMP.Architect.Api.CollectionTable oPT =
                            (LMP.Architect.Api.CollectionTable)oWrapper;
                        oPT.ReplacementInProgress = true;
                    }

                    //get location of current segment, accounting for start tag
                    oRng = oChild.PrimaryRange;
                    int iStart = oRng.Start - 1;
                    oRng.SetRange(iStart, iStart);

                    //delete incorrect segment
                    oMPDocument.DeleteSegment(oChild);
                }
            }
            else
            {
                //wrapper has no children
                oRng = oWrapper.PrimaryRange;
                object oMissing = System.Reflection.Missing.Value;
                oRng.StartOf(ref oMissing, ref oMissing);
            }

            //reset parameters for insertion in wrapper
            oLocation = oRng;
            iInsertionBehavior = InsertionBehaviors.Default;
            oParent = oWrapper;
        }

        /// <summary>
        /// returns the default wrapper id specified in oDef
        /// </summary>
        /// <param name="oDef"></param>
        /// <returns></returns>
        public static string GetWrapperID(ISegmentDef oDef, bool bContentControls)
        {
            //default wrapper is no longer configurable -
            //it is now built into certain segment types
            mpCollectionSegmentIDs iSegmentID;
            switch (oDef.TypeID)
            {
                case mpObjectTypes.PleadingCaption:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCaptionsCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCaptions;
                    break;
                case mpObjectTypes.PleadingCounsel:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCounselsCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCounsels;
                    break;
                case mpObjectTypes.PleadingSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingSignatures;
                    break;
                case mpObjectTypes.LetterSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.LetterSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.LetterSignatures;
                    break;
                case mpObjectTypes.AgreementSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.AgreementSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.AgreementSignatures;
                    break;
                case mpObjectTypes.CollectionTableItem:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.CollectionTableCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.CollectionTable;
                    break;
                default:
                    return "";
            }

            return ((System.Int32)iSegmentID).ToString();
        }

        /// <summary>
        /// determines whether the specified xml contains a segment with
        /// any parts in the body
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        internal static bool TargetIsBody(string xXML)
        {
            bool bIsInBody = false;
            if (LMP.String.IsWordOpenXML(xXML))
            {
                //GLOG 5267 (dm)
                int iPos = xXML.IndexOf("<w:body>");
                int iPos2 = xXML.IndexOf("</w:body>");
                string xBodyXML = xXML.Substring(iPos, iPos2 - iPos);
                bIsInBody = xBodyXML.Contains("w:tag w:val=\"mps");
            }
            else
            {
                //remove sectPr nodes from xml
                Regex oRegex = new Regex("<w:sectPr( .*|)/?>.*?</?w:sectPr>",
                    RegexOptions.Singleline);
                string xBodyXML = oRegex.Replace(xXML, "");

                //get search string for mSEG start tag
                string xPrefix = LMP.String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);
                string xmSEGStart = "<" + xPrefix + ":mSEG ";

                //see if there's an mSEG in the body
                bIsInBody = (xBodyXML.IndexOf(xmSEGStart) > -1);
            }

            return bIsInBody;
        }

        /// <summary>
        /// returns the value of the specified segment property for the
        /// top-level segment in the specified xml
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public static string GetPropertyValueFromXML(string xXML, string xPropertyName)
        {
            string xValue = "";

            //create xml document for xpath searchablity
            XmlDocument oXMLDoc = new XmlDocument();
            oXMLDoc.LoadXml(xXML);

            XmlNamespaceManager oNamespaceManager = new XmlNamespaceManager(
                oXMLDoc.NameTable);

            string xObjectData = null;

            if (String.IsWordOpenXML(xXML))
            {
                //get top-level segment content control
                oNamespaceManager.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);
                string xXPath = "//w:sdt/w:sdtPr/w:tag[starts-with(@w:val, 'mps')]";
                XmlNode oSegNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);

                string xDocVarName = oSegNode.Attributes["w:val"].Value.Substring(0,11).Replace("mps", "mpo");

                //get doc var
                xXPath = "//w:docVar[@w:name='" + xDocVarName + "']";
                XmlNode oDocVarNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);
                string xVal = String.Decrypt(oDocVarNode.Attributes["w:val"].Value);

                //get object data string
                xObjectData = xVal.Substring(xVal.IndexOf("��ObjectData="));
            }
            else
            {
                string xNamespacePrefix = String.GetNamespacePrefix(xXML,
                    ForteConstants.MacPacNamespace);
                oNamespaceManager.AddNamespace(xNamespacePrefix, ForteConstants.MacPacNamespace);

                string xXPath = @"//" + xNamespacePrefix + ":mSEG";
                XmlNode oSegNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                xObjectData = String.Decrypt(oObjectData.Value);
            }

            //find location of specified name
            int iPos1 = xObjectData.IndexOf("|" + xPropertyName + "=");
            if (iPos1 > -1)
            {
                //name was found, get value
                int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                int iValueStartPos = iPos1 + xPropertyName.Length + 2;
                xValue = xObjectData.Substring(
                    iValueStartPos, iPos2 - iValueStartPos);
            }

            return xValue;
        }
        #endregion
		#region *********************private members*********************

        //deletes the deactivation doc var
        private void Activate(Segment oSegment)
        {
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Activate(oSegment.Segments[i]);
            }

            oSegment.m_bActivated = true;

            //activate segment - delete deactivation docvar
            object oName = "Deactivation_" + oSegment.TagID;
            string xVarValue = null;
            Word.Variable oVar = null;
            try
            {
                oVar = oSegment.ForteDocument.WordDocument.Variables.get_Item(ref oName);
                xVarValue = oVar.Value;
            }
            catch { }

            if (xVarValue != null)
            {
                oVar.Delete();
            }

        }
        //saves variable values to a docvar
        private void Deactivate(Segment oSegment)
        {
            StringBuilder oSB = new StringBuilder();

            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Deactivate(oSegment.Segments[i]);
            }

            oSegment.m_bActivated = false;

            //cycle through all tagged variables in the segment,
            //adding the variable values from each - we take a
            //snapshot of the tagged data in the document, so
            //that we can later compare if data has been changed
            //by the user
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                Variable oVar = oSegment.Variables[i];

                //remmed 5-25-11 (dm)
                ////skip if tagless
                //if (oVar.IsTagless)
                //    continue;

                //string xValue = null;

                //xValue = oVar.Value;

                //if (oSegment.ForteDocument.FileFormat == mpFileFormats.OpenXML)
                //{
                //    xValue = oVar.GetValueFromSource_CC(oVar.AssociatedContentControls[0]);
                //}
                //else
                //{
                //    xValue = oVar.GetValueFromSource();
                //}

                string xValue = oVar.GetValueFromSource();

                //GLOG 5554 (dm) - remmed out the following block - we want the
                //value from source even for dates
                ////GLOG 3493: If Value Source Expression uses [DateFormat] fieldcode
                ////save the format, even if current Value is a manually-entered date
                //if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT_"))
                //{
                //    DateTime oDT = DateTime.MinValue;
                //    if (DateTime.TryParse(xValue, out oDT))
                //        //Value is Date Text, convert to format string
                //        xValue = LMP.Architect.Base.Application.GetDateFormatPattern(xValue);
                //}

                oSB.AppendFormat("{0}" + StringArray.mpEndOfElement.ToString() + "{1}" +
                    StringArray.mpEndOfRecord.ToString(), oVar.Name, xValue);
            }

            string xValues = oSB.ToString();

            if (xValues != "")
            {
                xValues = xValues.Substring(0, xValues.Length - 1);

                xValues = LMP.Forte.MSWord.Application.Encrypt(
                    xValues, LMP.Data.Application.EncryptionPassword);
            }
            else
            {
                //doc var value can't be empty -
                //Word doesn't create docvars with empty values
                xValues = " ";
            }

            object oName = "Deactivation_" + oSegment.TagID;
            this.ForteDocument.WordDocument.Variables.get_Item(ref oName).Value = xValues;
        }
        private void SetAuthorsFromDocument()
        {
            SetAuthorsFromDocument(true);
        }
        private void SetAuthorsFromDocument(bool bExecuteActions)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                //get authors from document
                string xAuthors = this.Nodes.GetItemAuthorsData(this.FullTagID);

                //Set this to avoid Author Events from Executing Variable actions
                this.m_bSkipAuthorActions = !bExecuteActions;

                if (xAuthors == "" && this.MaxAuthors > 0) //JTS 3/26/13: Don't set Authors if MaxAuthors=0
                {
                    //If current segment doesn't contain author information use parent
                    if (this.Parent != null)
                        m_oAuthors.SetAuthors(this.Parent.Authors);

                    //Set author to Current user
                    if (this.ForteDocument.Mode != ForteDocument.Modes.Design && 
                        (m_oAuthors == null || m_oAuthors.Count == 0) &&
                        UserIsOnAuthorList())
                    {
                        //add default author
                        m_oAuthors.Add(LMP.Data.Application.User.PersonObject);
                    }
                    LMP.Benchmarks.Print(t0);
                    return;
                }

                //parse authors XML from string-
                int iPos = xAuthors.IndexOf("|");

                string xAuthorXML;
                if (iPos > -1)
                    xAuthorXML = xAuthors.Substring(0, iPos);
                else
                    xAuthorXML = xAuthors;

                if (xAuthorXML != "")
                    m_oAuthors.XML = xAuthorXML;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotSetAuthorsFromDocument"), oE);
            }
            finally
            {
                this.m_bSkipAuthorActions = false;
                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns the value of the specified attribute
        /// in the specified source xml
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        private static string xGetValue(string xSource, string xName)
        {
            //search for specified attribute name
            string xSearch = "w:" + xName + "=";
            int iPos = xSource.IndexOf(xSearch);

            if (iPos > -1)
            {
                //attribute was found - get value start
                iPos += xSearch.Length;

                //get value end
                int iPos2 = xSource.IndexOf("\"", iPos + 1);

                //parse and return value
                return xSource.Substring(iPos + 1, iPos2 - iPos - 1);
            }
            else
                //GLOG 5516: Also handle contexts where attribute names don't use w: prefix
                xSearch = " " + xName + "=";
            iPos = xSource.IndexOf(xSearch);

            if (iPos > -1)
            {
                //attribute was found - get value start
                iPos += xSearch.Length;

                //get value end
                int iPos2 = xSource.IndexOf("\"", iPos + 1);

                //parse and return value
                return xSource.Substring(iPos + 1, iPos2 - iPos - 1);
            }
            else
                return null;
        }
        /// <summary>
        /// returns the full tag id of a newly inserted segment
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        private static string GetNewSegmentTagID(ForteDocument oMPDocument)
        {
            return GetNewSegmentTagID(oMPDocument, "");
        }

        /// <summary>
        /// returns the full tag id of a newly inserted segment
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        private static string GetNewSegmentTagID(ForteDocument oMPDocument,
            string xSegmentID)
        {
            //get the word tag containing the new segment bookmark
            if (oMPDocument.Mode == ForteDocument.Modes.Design)
            {
                //get the word tag containing the new segment bookmark
                if (oMPDocument.FileFormat ==
                    LMP.Data.mpFileFormats.Binary)
                {
                    //word xml nodes
                    Word.XMLNode oSegmentTag = LMP.Forte.MSWord.WordDoc.GetNewSegmentTag(
                        oMPDocument.WordDocument, true);

                    if (oSegmentTag == null)
                        //something's wrong - the word tag is not where it should be
                        throw new LMP.Exceptions.WordTagException(
                            LMP.Resources.GetLangString(
                            "Error_InvalidOrMissingNewSegmentBookmark"));

                    try
                    {
                        //get tag id of inserted segment node -
                        //the tag id was reindexed by RefreshTags
                        return oMPDocument.Tags.GetTag(oSegmentTag, false, "", "").FullTagID;
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.WordTagException(
                            LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
                    }
                }
                else
                {
                    //content controls
                    Word.ContentControl oSegmentTag = LMP.Forte.MSWord.WordDoc.GetNewSegmentContentControl(
                        oMPDocument.WordDocument, true);

                    if (oSegmentTag == null)
                        //something's wrong - the word tag is not where it should be
                        throw new LMP.Exceptions.WordTagException(
                            LMP.Resources.GetLangString(
                            "Error_InvalidOrMissingNewSegmentBookmark"));

                    try
                    {
                        //get tag id of inserted segment node -
                        //the tag id was reindexed by RefreshTags
                        string xTagID = oMPDocument.Tags.GetTag_CC(oSegmentTag, false, "", "").FullTagID;
                        return xTagID;
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.WordTagException(
                            LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
                    }
                }
            }
            else
            {
                //GLOG 6799 (dm) - added xSegmentID parameter for optional validation
                Word.Bookmark oSegmentBmk = LMP.Forte.MSWord.WordDoc.GetNewSegmentBookmark(
                    oMPDocument.WordDocument, true, xSegmentID);

                if (oSegmentBmk == null)
                {
                    //something's wrong
                    throw new LMP.Exceptions.WordTagException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidOrMissingNewSegmentBookmark"));
                }

                try
                {
                    //get tag id of inserted segment node -
                    //the tag id was reindexed by RefreshTags
                    return oMPDocument.Tags.GetTag_Bookmark(
                        oSegmentBmk, false, "", "").FullTagID;
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.WordTagException(
                        LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
                }
            }
        }

        /// <summary>
        /// returns the segment def of the segment with the specified ID
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public static ISegmentDef GetDefFromID(string xID)
        {
            //GLOG 3474: Access changed to internal to be accessible from Variables.AssignDefaultValues
            //parse ID
            int iID1;
            int iID2;
            String.SplitID(xID, out iID1, out iID2);

            if (iID2 == LMP.Data.ForteConstants.mpFirmRecordID)
            {
                //the segment is an admin segment - 
                //get admin segment definition
                try
                {
                    //get definition
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    return (AdminSegmentDef)oDefs.ItemFromID(iID1);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidSegmentID") + iID1.ToString(), oE);
                }
            }
            else
            {
                //the segment is a user segment - 
                //get user segment definition
                try
                {
                    //get definition
                    //Don't filter this on User, in case segment being inserted is in a Shared folder
                    //Or we're in Admin Mode
                    UserSegmentDefs oDefs = new UserSegmentDefs(
                        mpUserSegmentsFilterFields.Owner, 0);
                    
                    UserSegmentDef oUserDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                    if (oUserDef.TypeID == mpObjectTypes.UserSegment)
                        return oUserDef;
                    else
                    {
                        //convert to admin segment def - this is a segment that was
                        //created by a content designer
                        return oUserDef.ConvertToAdminSegmentDef(0, true);
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidSegmentID") + xID, oE);
                }
            }
        }
        /// <summary>
        /// returns a new segment of the specified object type
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <returns></returns>
        public static Segment CreateSegmentObject(mpObjectTypes iObjectType, ForteDocument oMPDocument)
        {
            Trace.WriteNameValuePairs("iObjectType", iObjectType);

            //create instance
            Segment oSegment = (Segment)Assembly.GetExecutingAssembly()
                .CreateInstance("LMP.Architect.Api." + iObjectType.ToString());

            //assign MacPac document to Segment
            oSegment.ForteDocument = oMPDocument;

            return oSegment;
        }
        /// <summary>
        /// returns a new child segment that has been added
        /// to the segment's child segments collection
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private Segment AddChildSegment(mpObjectTypes iObjectTypeID, string xTagID)
        {

            //create instance based on type of segment
            Segment oSegment = Segment.CreateSegmentObject(iObjectTypeID, this.ForteDocument);

            //GLOG : 8102 : ceh - no authors in Tools, so don't prompt
            oSegment.PromptForMissingAuthors = !(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator);
            
            oSegment.Initialize(xTagID, this);

            //get authors from document if the parent
            //has already set authors - we also allow
            //child authors to be set when the creation
            //status of the parent is unknown, as this will
            //be the case when a child is inserted into an
            //existing segment (ie, one that is not currently
            //being created.
            if (this.CreationStatus == Status.Unknown ||
                this.CreationStatus >= Status.DefaultAuthorsSet)
            {
                oSegment.SetAuthorsFromDocument();
            }

            //subscribe to changes in child segment validation
            oSegment.ValidationChange += new LMP.Architect.Base.ValidationChangedHandler(oChildSegment_ValidationChange);
            oSegment.AfterAllVariablesVisited += new AfterAllVariablesVisitedHandler(oChildSegment_AfterAllVariablesVisited);

            //add to collection of child segments
            this.m_oChildSegments.Add(oSegment);

            //set creation status
            oSegment.CreationStatus = Status.DefaultAuthorsSet;

            return oSegment;
        }
        /// <summary>
        /// returns the child segments of this segment
        /// </summary>
        private void GetChildSegments()
        {
            //create new collection
            this.m_oChildSegments = new Segments(this.ForteDocument);

            //get mSEG nodes contained in segment nodes
            ReadOnlyNodeStore oChildSegmentNodes = this.Nodes.GetmSEGNodes();

            //cycle through child segment nodes, adding each, 
            //except the parent, as a child segment
            for (int i = 0; i < oChildSegmentNodes.Count; i++)
            {
                string xTagID = oChildSegmentNodes.GetItemTagID(i);

                if (xTagID != this.FullTagID)
                {
                    //this is a child node - add as a child segment -
                    string xTypeID = null;
                    mpObjectTypes iTypeID = 0;

                    try
                    {
                        //get segment ID from mSEG
                        xTypeID = oChildSegmentNodes.GetItemObjectDataValue(xTagID, "ObjectTypeID");
                        iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentDefinitionException(
                            LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
                    }

                    //add child segment
                    AddChildSegment(iTypeID, xTagID);
                }
            }

            //subscribe to events
            this.m_oChildSegments.SegmentAdded += new SegmentAddedHandler(m_oChildSegments_SegmentAdded);
            this.m_oChildSegments.SegmentDeleted += new SegmentDeletedHandler(m_oChildSegments_SegmentDeleted);
        }
        private static Segment FindSegment(string xTagID, Segments oSegments)
        {
            for (int i = 0; i < oSegments.Count; i++)
            {
                if (oSegments[i].FullTagID == xTagID)
                    //we've found the segment
                    return oSegments[i];
                else
                {
                    //attempt to find segment in child segments
                    Segment oSegment = FindSegment(xTagID, oSegments[i].Segments);
                    if (oSegment != null)
                        return oSegment;
                }
            }
            return null;
        }
        /// <summary>
        /// returns the segment with the specified Segment ID 
        /// that is a member or descendant of the specified 
        /// segment collection
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oRootSegmentsCollection"></param>
        /// <returns></returns>
        internal static Segment[] FindSegments(string xSegmentID, Segments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //append .0 to segment ID if shorthand ID was supplied
            if (xSegmentID.IndexOf('.') == -1)
                xSegmentID += ".0";

            //cycle through collection, finding all instances
            //having the specified tag id
            for (int i = 0; i < oSegments.Count; i++)
            {
                //GLOG 6452: If recreating a Saved Segment, xSegmentID will be RecreateRedirectID
                if (oSegments[i].ID == xSegmentID || oSegments[i].RecreateRedirectID == xSegmentID)
                    //we've found the segment
                    oSegArrayList.Add(oSegments[i]);
                else
                {
                    //attempt to find segment in child segments
                    Segment[] aChildSegments = FindSegments(xSegmentID, oSegments[i].Segments);
                    foreach (Segment oSegment in aChildSegments)
                        oSegArrayList.Add(oSegment);
                }
            }

            //copy segments to segment array
            Segment[] aSegArray = new Segment[oSegArrayList.Count];

            for(int i=0; i<oSegArrayList.Count; i++)
                aSegArray[i] = (Segment)oSegArrayList[i];

            return aSegArray;
        }
        /// <summary>
        /// returns the first instance of the segment of the specified
        /// type that is a member of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"> </param>
        /// <returns></returns>
        public static Segment FindSegment(mpObjectTypes iObjectTypeID, Segments oSegments)
        {
            Segment oFoundSeg = null;
            Segment oSeg = null;

            //cycle recursively through segments 
            //in domain, looking for a match
            for (int i = 0; i < oSegments.Count; i++)
            {
                oSeg = oSegments[i];

                if (oSeg.TypeID == iObjectTypeID)
                    //segment is of specified type - return
                    return oSeg;
                else if (oSeg.Segments.Count > 0)
                {
                    //segment has children - check children for match
                    oFoundSeg = FindSegment(iObjectTypeID, oSeg.Segments);
                    if (oFoundSeg != null)
                        return oFoundSeg;
                }
            }

            return null;
        }

        /// <summary>
        /// returns all segments of the specified type that are 
        /// members or descendants of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"></param>
        /// <returns></returns>
        internal static Segment[] FindSegments(mpObjectTypes iObjectTypeID,
            Segments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //cycle through collection, finding all segments of the specified type
            for (int i = 0; i < oSegments.Count; i++)
            {
                Segment oSegment = oSegments[i];
                if (oSegment.TypeID == iObjectTypeID)
                    oSegArrayList.Add(oSegment);
                Segment[] oChildren = FindSegments(iObjectTypeID, oSegment.Segments);
                foreach (Segment oChild in oChildren)
                {
                    if (oChild.TypeID == iObjectTypeID)
                        oSegArrayList.Add(oChild);
                }
            }

            //copy segments to segment array
            Segment[] aSegArray = new Segment[oSegArrayList.Count];

            for (int i = 0; i < oSegArrayList.Count; i++)
                aSegArray[i] = (Segment)oSegArrayList[i];

            return aSegArray;
        }

        /// <summary>
        /// returns all collection table item segments that are 
        /// members or descendants of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"></param>
        /// <returns></returns>
        internal static Segment[] FindCollectionTableItemChildren(Segments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //cycle through collection, finding all collection table item segments
            for (int i = 0; i < oSegments.Count; i++)
            {
                Segment oSegment = oSegments[i];
                if (oSegment is CollectionTableItem || oSegment is Paper || oSegment is Sidebar) //GLOG 7848
                    oSegArrayList.Add(oSegment);
                Segment[] oChildren = FindCollectionTableItemChildren(oSegment.Segments);
                foreach (Segment oChild in oChildren)
                {
                    if (oChild is CollectionTableItem || oChild is Paper || oChild is Sidebar) //GLOG 7848
                        oSegArrayList.Add(oChild);
                }
            }

            //copy segments to segment array
            Segment[] aSegArray = new Segment[oSegArrayList.Count];

            for (int i = 0; i < oSegArrayList.Count; i++)
                aSegArray[i] = (Segment)oSegArrayList[i];

            return aSegArray;
        }

        /// <summary>
        /// updates authors of child segments to match this segment's authors
        /// </summary>
        public void UpdateChildSegmentAuthors()
        {
            DateTime t0 = DateTime.Now;

            if (!(this.CreationStatus >= Status.VariableDefaultValuesSet ||
                    this.CreationStatus == Status.Unknown))
                return;

            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oChild = this.Segments[i];
                try
                {
                    //Set this to avoid Author Events from Executing Variable actions
                    oChild.m_bSkipAuthorActions = this.m_bSkipAuthorActions;
                    oChild.m_bAllowUpdateForAuthor = this.m_bAllowUpdateForAuthor;
                    //do only if link is specified
                    if (oChild.LinkAuthorsToParent)
                    {
                        oChild.Authors.SetAuthors(this.Authors);
                        if (this.PreviousAuthors != null)
                            oChild.PreviousAuthors.SetAuthors(this.PreviousAuthors);
                    }
                }
                finally
                {
                    oChild.m_bAllowUpdateForAuthor = true;
                    oChild.m_bSkipAuthorActions = false;
                }
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// updates values of properties that have linked values related to the specified author -
        /// -2 = no author fields,
        /// 0 = update all author fields, -1 = update lead author only,
        /// 1 = update first author, 2 = update second author, etc.
        /// </summary>
        /// <param name="shAuthorIndex"></param>
        /// <param name="shParentAuthorIndex"></param>
        /// <param name="bExecuteActions"></param>
        private void UpdateForAuthor(short shAuthorIndex, short shParentAuthorIndex, bool bExecuteActions)
        {
            DateTime t0 = DateTime.Now;

            //GLOG 7126 (dm) - use separate method to update finished segment
            if (this.IsFinished)
            {
                UpdateSnapshotForAuthor(shAuthorIndex, shParentAuthorIndex, bExecuteActions);
                return;
            }

            //cycle through variables, checking whether default value contains a field code
            //that might evaluate differently for new author
            for (int i = 0; i < this.Variables.Count; i++)
            {
                //get variable
                Variable oVar = this.Variables[i];

                //get default value - exit if empty
                string xExp = oVar.DefaultValue;

                if (Expression.ContainsAuthorCode(xExp, shAuthorIndex, shParentAuthorIndex))
                {
                    //variable's default value contains a related field code,
                    //so update is required - set new value and execute actions if specified - 
                    oVar.SetValue(Expression.Evaluate(xExp, this, this.ForteDocument), false);

                    if (bExecuteActions)
                        oVar.VariableActions.Execute();
                }
                else if (bExecuteActions)
                {
                    //check action parameters for related field code
                    for (int j = 0; j < oVar.VariableActions.Count; j++)
                    {
                        //get parameters
                        string xParam = ((VariableAction)oVar.VariableActions[j]).Parameters;
                        if (Expression.ContainsAuthorCode(xParam, shAuthorIndex,
                            shParentAuthorIndex))
                        {
                            //update is required - execute actions now
                            oVar.VariableActions.Execute();
                            break;
                        }
                        else if (oVar.VariableActions[j].Type == VariableActions.Types.IncludeExcludeText &&
                            Expression.ContainsAuthorCode(oVar.SecondaryDefaultValue, shAuthorIndex, shParentAuthorIndex))
                        {
                            //GLOG 4292: Also update if there is an IncludeExcludeText action associated with an
                            //Author Code in the Secondary Default Value
                            oVar.VariableActions.Execute();
                            break;
                        }
                    }
                }
            }

            //execute segment action
            this.Actions.Execute(Events.AfterAuthorUpdated);

            //raise event
            if (this.AfterAuthorUpdated != null)
                this.AfterAuthorUpdated(this, new EventArgs());

            LMP.Benchmarks.Print(t0, "Segment = " + this.DisplayName);
        }

        /// <summary>
        /// updates snapshot to reflect change of author - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="shAuthorIndex"></param>
        /// <param name="shParentAuthorIndex"></param>
        /// <param name="bExecuteActions"></param>
        private void UpdateSnapshotForAuthor(short shAuthorIndex, short shParentAuthorIndex, bool bExecuteActions)
        {
            string xAuthors = Snapshot.GetAuthorsSnapshotValue(this.Authors);
            this.Snapshot.SetValue("Authors", xAuthors);

            //load variable config xml
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(this.Snapshot.Configuration);
            XmlNodeList oNodes = oXML.SelectNodes("/Segment/Variable");

            string[] aVars = this.Snapshot.Values.Split(Snapshot.mpSnapshotFieldDelimiter);
            for (int i = 0; i < aVars.Length; i++)
            {
                if (aVars[i] != "")
                {
                    string[] aVals = aVars[i].Split(Snapshot.mpSnapshotValueDelimiter);
                    XmlNode oNode = oXML.SelectSingleNode("/Segment/Variable[@Name='" +
                        aVals[0] + "']");
                    if (oNode != null)
                    {
                        string xExp = oNode.Attributes["DefaultValue"].Value;
                        if (Expression.ContainsAuthorCode(xExp, shAuthorIndex, shParentAuthorIndex))
                        {
                            //variable's default value contains a related field code,
                            //so update is required - set new value and execute actions if specified - 
                            this.Snapshot.SetValue(aVals[0], Expression.Evaluate(xExp, this, this.ForteDocument));

                            //if (bExecuteActions)
                            //    oVar.VariableActions.Execute();
                        }
                    }
                }
            }

            //save
            this.Snapshot.Save(this.Snapshot.Values);

            ////execute segment action
            //this.Actions.Execute(Events.AfterAuthorUpdated);

            //raise event
            if (this.AfterAuthorUpdated != null)
                this.AfterAuthorUpdated(this, new EventArgs());
        }

        ///// <summary>
        ///// sets up the segment for initial use
        ///// </summary>
        protected virtual void InitializeValues(Prefill oPrefill, bool bForcePrefillOverride)
        {
            InitializeValues(oPrefill, "", bForcePrefillOverride, false);
        }

        //GLOG : 6987 : CEH
        //added new bInitializePrefillAuthors argument specific to ApplyPrefill functionality
        protected virtual void InitializeValues(Prefill oPrefill, bool bForcePrefillOverride, bool bInitializePrefillAuthors)
        {
            InitializeValues(oPrefill, "", bForcePrefillOverride, bInitializePrefillAuthors);
        }
        //GLOG : 6987 : CEH
        protected virtual void InitializeValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride)
        {
            InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, null, false); //GLOG 8373
        }
        protected virtual void InitializeValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride, bool bInitializePrefillAuthors)
        {
            InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, null, bInitializePrefillAuthors); //GLOG 8373
        }

        /// <summary>
		/// sets up the segment for initial use
		/// </summary>
        protected virtual void InitializeValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride, CollectionTableStructure oChildStructure, bool bInitializePrefillAuthors) //GLOG 8373
     	{
            //initialize authors
            if (!string.IsNullOrEmpty(xRelativePath))
            {
				//GLOG 8409
                this.InitializeAuthors(null, bInitializePrefillAuthors);

                //execute AfterDefaultAuthorSet actions
                this.Actions.Execute(Segment.Events.AfterDefaultAuthorSet);

                //raise after default author set
                if (this.AfterDefaultAuthorSet != null)
                    this.AfterDefaultAuthorSet(this, new EventArgs());

                //set segment status
                this.CreationStatus = Status.DefaultAuthorsSet;
            }
            //GLOG : 6987 : CEH
            //initialize authors only when called from Insert Data
            else if ((oPrefill != null) && bInitializePrefillAuthors)
				//GLOG 8409
                this.InitializeAuthors(oPrefill, false);
			//raise before segment setup event
			if(this.BeforeSetup != null)
				this.BeforeSetup(this, new EventArgs());

			//set variable default/prefill values
            bool bAssigned = false;
            while (!bAssigned)
            {
                if (oPrefill == null)
                    bAssigned = this.Variables.AssignDefaultValues();
                else
                {
                    if (xRelativePath == "" && this.LevelChooserRequired())
                    {
                        //Set Court Levels from Prefill
                        AdminSegment oASeg = (AdminSegment)this;
                        try
                        {
                            oASeg.L0 = Int32.Parse(oPrefill["L0"]);
                            oASeg.L1 = Int32.Parse(oPrefill["L1"]);
                            oASeg.L2 = Int32.Parse(oPrefill["L2"]);
                            oASeg.L3 = Int32.Parse(oPrefill["L3"]);
                            oASeg.L4 = Int32.Parse(oPrefill["L4"]);

                            //Need to update Segment Object Data also
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "L0", oASeg.L0.ToString());
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "L1", oASeg.L1.ToString());
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "L2", oASeg.L2.ToString());
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "L3", oASeg.L3.ToString());
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "L4", oASeg.L4.ToString());
                        }
                        catch { }
                    }

                    //set culture if it's in the prefill
                    string xCulture = oPrefill["Culture"];
                    if (!string.IsNullOrEmpty(xCulture))
                    {
                        //GLOG 4018: If Language changed by Prefill, run appropriate actions
                        if (this.Culture != int.Parse(xCulture))
                        {
                            this.Culture = int.Parse(xCulture);
                            this.Nodes.SetItemObjectDataValue(this.FullTagID, "Culture", this.Culture.ToString());
                            this.Actions.Execute(Events.AfterLanguageUpdated);
                        }
                    }

                    bAssigned = this.Variables.AssignDefaultValues(oPrefill,
                        xRelativePath, bForcePrefillOverride);
                }
            }

            //set status
            this.CreationStatus = Segment.Status.VariableDefaultValuesSet;

            //execute AfterDefaultValuesSet actions
            this.Actions.Execute(Segment.Events.AfterDefaultValuesSet);

            //raise after default values set
            if (this.AfterDefaultValuesSet != null)
                this.AfterDefaultValuesSet(this, new EventArgs());

            string xSegmentIDs = "|";

            //setup all children- we do this after we set up the
            //the parent because some child segment variables
            //may depend on the value of variables in the parent
            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oChild = this.Segments[i];

                //GLOG 6004 (dm) - initialize values for types of children not
                //included in child structures
                //GLOG 8373:  Initialize existing child if Segment used in original document being recreated has been deleted
                if ((oChildStructure == null || !oChildStructure.ContainsChildOfType(oChild.TypeID)) || ((!(oChild is CollectionTable)) &&
                    (!(oChild is Paper))))
                {
                    //child structure is not available - initialize child values

                    //determine the number of sibling segments with this Segment ID -
                    //apply an index to the relative path to know which prefill value
                    //to retrieve - e.g. if there are two sibling segments named X,
                    //and the prefill contains prefill values for two (or more)
                    //such segments, we need to apply the first set of prefill values
                    //to X and the second set of prefill values to X.1
                    int iNumSegs = LMP.String.CountChrs("|" + xSegmentIDs, oChild.ID + "|");

                    if (iNumSegs == 0)
                    {
                        //this is the first segment with this ID at this level
						//GLOG 6943: Pass bInitializePrefillAuthors value to child
                        oChild.InitializeValues(oPrefill, xRelativePath +
                            String.RemoveIllegalNameChars(oChild.DisplayName) + ".", bForcePrefillOverride, bInitializePrefillAuthors);
                    }
                    else
                    {
                        //this is not the first segment with this ID at this level -
                        //get prefill value from .Y index of item - second item has
                        //an index tag of .1
                        oChild.InitializeValues(oPrefill, xRelativePath +
                            String.RemoveIllegalNameChars(oChild.DisplayName) +
                            "." + iNumSegs + ".", bForcePrefillOverride);
                    }
                }
                    
                xSegmentIDs += oChild.ID + "|";

                //GLOG 3605 (2/11/09 dm)
                oChild.CreationStatus = Status.Finished;
            }

            //execute AfterSegmentGenerated actions
            this.Actions.Execute(Segment.Events.AfterSegmentGenerated);

            //raise after segment generated - right now, this occurs right after
            //the AfterDefaultValuesSet event - this might not be the case in the future
            if (this.Generated != null)
                this.Generated(this, new EventArgs());

            //GLOG 3144: Ensure Special caption border is correct length initially
            //TODO: It might make sense to use the Generated event for this, but it's not
            //currently in use, and I'm not sure where would be appropriate to place this (JTS)
            //GLOG 4282: Run for any Collection Table item
            if (this is CollectionTableItem)
            {
                this.UpdateTableBorders();
            }

            //GLOG 7048 (dm) - if doc editor node was already added, it will be displaying
            //as invalid because values hadn't yet been intitialized - let it know that
            //this has changed
            this.RaiseValidationChangeIfNecessary(new LMP.Architect.Base.ValidationChangedEventArgs(true));
        }
        
        private void InitializeAuthors()
        {
			//GLOG 8409
            InitializeAuthors(null, true);
        }
        private void InitializeAuthors(Prefill oPrefill)
        {
            InitializeAuthors(oPrefill, true);  //GLOG 8409
        }
        private void InitializeAuthors(Prefill oPrefill, bool bPrompt) //GLOG 8409
        {
            bool bAllowManualAuthor = false;

            //GLOG #4327 - dcf
            if (!this.DefinitionRequiresAuthor && !this.LinkAuthorsToParent)
                return;
            //GLOG 8862
            else if (!this.IsTopLevel && this.LinkAuthorsToParent && oPrefill == null && this.Parent != null && this.Parent.Authors.Count > 0)
            {
                //this is a child segment whose authors
                //are linked to the parent segment - set
                //authors from parent
                m_oAuthors.SetAuthors(this.Parent.Authors);
                if (this.Parent.PreviousAuthors != null)
                    m_oPrevAuthors.SetAuthors(this.Parent.PreviousAuthors);
            }
            else
            {
                //we need to set authors
                LocalPersons oPersons = null;
                LMP.Controls.mpListTypes iType = LMP.Controls.mpListTypes.AllAuthors;
                string xACP = this.AuthorControlProperties;
                if (!string.IsNullOrEmpty(xACP))
                {
                    string[] aCP = xACP.Split(StringArray.mpEndOfSubValue);
                    //GLOG 3719: Determine if Manual Authors should be allowed
                    if (xACP.Contains("AllowManualInput="))
                    {
                        for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                        {
                            if (aCP[i].StartsWith("AllowManualInput="))
                            {
                                bAllowManualAuthor = aCP[i].Substring(17).ToUpper() == "TRUE";
                                break;
                            }
                        }
                    }

                    //If Author Control Properties include ListType, filter People list based on it
                    if (xACP.Contains("ListType="))
                    {
                        for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                        {
                            if (aCP[i].StartsWith("ListType="))
                            {
                                //Filter Persons collection based on Segment's Author Control setting
                                try
                                {
                                    iType = (LMP.Controls.mpListTypes)(Int32.Parse(aCP[i].Substring(9)));
                                }
                                catch { }
                                //JTS 3/22/10: Include Additional Office records
                                if (iType == LMP.Controls.mpListTypes.Attorneys)
                                    oPersons = new LocalPersons(mpPeopleListTypes.AllPeople, mpTriState.True, UsageStates.OfficeActive);
                                else if (iType == LMP.Controls.mpListTypes.NonAttorneys)
                                    oPersons = new LocalPersons(mpPeopleListTypes.AllPeople, mpTriState.False, UsageStates.OfficeActive);
                                else
                                    //GLOG 4838 //GLOG 4957
                                    oPersons = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);
                                break;
                            }
                        }
                    }
                }

                //list type was not specified - get default author list
                if(oPersons == null)
                    //JTS 3/22/10: Make sure additional office records are included in collection
                    oPersons = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);
                
                if (oPrefill != null && oPrefill.Authors.Count > 0)
                {
                    //Get Author XML from Prefill -
                    //GLOG 4417 (dm) - prevent error when MaxAuthors = 0
                    this.Authors.XML = oPrefill.AuthorsXML(Math.Max(this.MaxAuthors, 1));

                    //ensure author is in user db
                    Persons oPeopleInDB = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllActivePeopleInDatabase);
                    Author oLeadAuthor = this.Authors.GetLeadAuthor();
                    Person oLeadAuthorPerson = null;
                    DialogResult iImportAuthors = DialogResult.Ignore;

					//GLOG : 6482 : ceh
                    string xAuthor = null;
                    int iID1 = 0;
                    int iID2 = 0;

                    //GLOG 3719: Don't display prompt and replace authors 
                    //if initializing a child Segment that allows manual authors
                    if(!oPersons.IsInCollection(oLeadAuthor.ID) && (this.IsTopLevel || 
                        this.LinkAuthorsToParent || !bAllowManualAuthor))
                    {
                        if (!oLeadAuthor.ID.EndsWith(".0"))
                        {
                            //this is a private person that no longer exists -
                            //set author to current user
                            //GLOG 8409: Show Prompt only if specified
                            if (bPrompt)
	                            MessageBox.Show(LMP.Resources.GetLangString("Prompt_AuthorDoesNotExistUseUser"),
    	                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                            this.Authors.RemoveAll();

                            //get the default office person of the current user
                            int iDefaultOfficeID = LMP.Data.Application.User.PersonObject.DefaultOfficeRecordID;

                            if (iDefaultOfficeID == 0)
                                oLeadAuthorPerson = LMP.Data.Application.User.PersonObject;
                            else
                                oLeadAuthorPerson = oPeopleInDB.ItemFromID(iDefaultOfficeID.ToString() + ".0");

                            this.Authors.Add(oLeadAuthorPerson, oLeadAuthor.BarID); //GLOG 6943: Set Bar ID from Prefilll
                        }
                        else
                        {
							//GLOG : 6482 : ceh
                            LMP.Data.Application.SplitID(oLeadAuthor.ID, out iID1, out iID2);

                            if (!string.IsNullOrEmpty(oLeadAuthor.FullName))
                                xAuthor = oLeadAuthor.FullName + " (" + iID1.ToString() + ")";
                            else
                                xAuthor = iID1.ToString();

                            //GLOG 4538: Modify message displayed if User is not in filtered Author list
                            string xNewAuthor = "";
                            if (UserIsOnAuthorList())
                                xNewAuthor = "you";
                            else
                                xNewAuthor = "first Author in list";

                            //GLOG 8409: Show Prompt only if specified
                            if (bPrompt)
                                //prompt to either copy author or set user as author
                                iImportAuthors = MessageBox.Show(
                                    string.Format(LMP.Resources.GetLangString("Prompt_Authors_CopyFromNetworkDBOrSetUserAsAuthor"), this.DisplayName, xAuthor, xNewAuthor),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            else
                                iImportAuthors = DialogResult.No;
                        }

                        System.Windows.Forms.Application.DoEvents();

                        LMP.Forte.MSWord.Application.RefreshScreen();
                    }
                    if (iImportAuthors == DialogResult.Yes)
                    {
						
                        //GLOG 4538
						
						//GLOG : 6482 : ceh
                        //int iID1 = 0;
                        //int iID2 = 0;
						
                        try
                        {
                            LMP.Data.Application.SplitID(oLeadAuthor.ID, out iID1, out iID2);
                            //GLOG 8276
                            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                            {
                                oLeadAuthorPerson = oPersons.CopyPublicUser(iID1, true, true);
                            }
                            else
                                oLeadAuthorPerson = oPersons.CopyNetworkUser(iID1, true);
                        }
                        catch
                        {
                            //GLOG 4538: Indicate to User that either User or First Author in filtered list
                            //will be set as author

							//GLOG : 6482 : ceh
                            //string xAuthor = null;
                            //if (!string.IsNullOrEmpty(oLeadAuthor.FullName))
                            //    xAuthor = oLeadAuthor.FullName + " (" + iID1.ToString() + ")";
                            //else
                            //    xAuthor = iID1.ToString();

                            //GLOG 8409: Show Prompt only if specified
                            if (bPrompt)
                            {
                                string xMsg = string.Format(LMP.Resources.GetLangString(
                                    "Prompt_Authors_CouldNotCopyAuthor"));

                                //couldn't copy author from network db - 
                                //alert user
                                //GLOG 4538: Use iID1 in case oLeadAuthorPerson is null
                                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            System.Windows.Forms.Application.DoEvents();
                        }
                        //GLOG 4538: If original Prefill Author no longer exists and oLeadAuthorPerson is null
                        //still clear Authors so code below will reset to User or first person in filtered list
                        this.Authors.RemoveAll();
                        //GLOG 6943: Set Bar ID from Prefill
                        if (oLeadAuthorPerson != null)
                            this.Authors.Add(oLeadAuthorPerson, oLeadAuthor.BarID);
                        //GLOG 6943: Add other Authors as well
                        for (int i = 0; i < oPrefill.Authors.Count; i++)
                        {
                            Person oAddPerson = null;
                            string xID = oPrefill.Authors[i][0];
                            if (xID.EndsWith(".0") && xID != oLeadAuthor.ID)
                            {
                                try
                                {
                                    LMP.Data.Application.SplitID(xID, out iID1, out iID2);
                                    //GLOG 8276
                                    if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                                        oAddPerson = oPersons.CopyPublicUser(iID1, true, true);
                                    else
                                        oAddPerson = oPersons.CopyNetworkUser(iID1, true);
                                }
                                catch 
                                {
                                }
                            }
                            if (oAddPerson != null)
                                this.Authors.Add(oAddPerson, oPrefill.Authors[i][3]);
                        }
                    }
                    //GLOG 4634: Only clear if No selected to Prompt, or if Manual Authors are not allowed
                    else if (iImportAuthors == DialogResult.No || 
                        (!oPersons.IsInCollection(oLeadAuthor.ID) && !bAllowManualAuthor))
                        //GLOG 4538: Clear authors so current user will be used as indicated in prompt
                        this.Authors.RemoveAll();
                }

                //if no authors, set the author to the first person on the list or the
                //user, unless the user is excluded by the list type
                if ((this.Authors.Count == 0) && ((oPersons.Count > 0) || UserIsOnAuthorList()))
                {
                    Person oPerson = null;
                    //GLOG 4838: Check for Default Office Record as well as Primary ID
                    if (oPersons.IsInCollection(LMP.Data.Application.User.PersonObject.ID) || 
                        oPersons.IsInCollection(LMP.Data.Application.User.PersonObject.DefaultOfficeRecordID) ||
                        oPersons.Count == 0)
                    {
                        oPerson = LMP.Data.Application.User.PersonObject;

                        string xDefaultOfficeRecordID1 = oPerson.DefaultOfficeRecordID.ToString() + ".0";

                        if (xDefaultOfficeRecordID1 != "0.0" && oPerson.ID != xDefaultOfficeRecordID1)
                        {
                            //get the person record specified by the default office record id
                            oPerson = oPersons.ItemFromID(xDefaultOfficeRecordID1);
                        }
                    }
                    else
                        //Set Default to first item in filtered collection
                        oPerson = oPersons.ItemFromIndex(1);

                    if (this is AdminSegment)
                    {
                        AdminSegment oAS = (AdminSegment)this;
                        //Get Default Bar ID
                        string xBarID = oPerson.DefaultBarID(oAS.L0, oAS.L1, oAS.L2, oAS.L3, oAS.L4);
                        this.Authors.Add(oPerson, xBarID);
                    }
                    else
                        //add default author
                        this.Authors.Add(oPerson);
                }
            }
        }

		/// <summary>
		/// raises the ValidationChanged event if the validation
		/// of the segment has changed
		/// </summary>
		/// <param name="e"></param>
		private void RaiseValidationChangeIfNecessary(LMP.Architect.Base.ValidationChangedEventArgs e)
		{
			bool bSegmentCurrentlyValid = this.IsValid;
			bool bValidationChanged = (m_bSegmentPreviouslyValid != bSegmentCurrentlyValid);

			if(bValidationChanged)
			{
				if(bSegmentCurrentlyValid && !m_bSegmentPreviouslyValid)
				{
					//execute segment actions that should run when the 
					//segment is valid
					this.Actions.Execute(Segment.Events.AfterSegmentValid);
				}

				//raise segment ValidationChanged event
				if(this.ValidationChange != null)
					this.ValidationChange(this, 
						new LMP.Architect.Base.ValidationChangedEventArgs(e.Validated));
            
                //validation state has changed -
                //mark validation state for next iteration of this method
                m_bSegmentPreviouslyValid = bSegmentCurrentlyValid;
            }
		}
        /// <summary>
        /// refreshes this segment's child segments, variables, and blocks
        /// </summary>
        private void RefreshMembers(Segment.RefreshTypes oType)
        {
            DateTime t0 = DateTime.Now;

            if (oType != RefreshTypes.ChildSegmentsOnly)
            {
                //refresh variables
                m_oVariables = new Variables(this);

                //subscribe to notifications that variables have been validated
                m_oVariables.ValidationChanged += new LMP.Architect.Base.ValidationChangedHandler(OnVariablesValidationChanged);

                //refresh blocks
                m_oBlocks = new Blocks(this);
            }

            if (oType != RefreshTypes.NoChildSegments)
            {
                //refresh child segments
                for (int i = 0; i < this.Segments.Count; i++)
                    this.Segments[i].RefreshMembers(RefreshTypes.All);
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// initializes the segment
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oMPDocument"></param>
        protected void Initialize(string xTagID)
        {
            Initialize(xTagID, null);
        }
        /// <summary>
        /// initializes the segment
        /// </summary>
        /// <param name="xTagID"></param>
        protected virtual void Initialize(string xTagID, Segment oParent)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xTagID", xTagID);

            //set tag id
            this.FullTagID = xTagID;

            //set the parent segment
            this.Parent = oParent;

            //GLOG item #4219 - dcf - 
            //set language to language of parent,
            //if this is a child
            if (this.Parent != null)
                this.Culture = this.Parent.Culture;

            object oName = "Deactivation_" + this.TagID;
            string xVarValue = null;
            try
            {
                Word.Variable oVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);
                xVarValue = oVar.Value;
            }
            catch { }

            this.m_bActivated = xVarValue == null;

            //hook up to event handlers
            this.Authors.AuthorAdded += new AuthorAddedHandler(m_oAuthors_AuthorAdded);
            this.Authors.AuthorsReindexed += new AuthorsReindexedHandler(m_oAuthors_AuthorsReindexed);
            this.Authors.LeadAuthorChanged += new LeadAuthorChangedHandler(m_oAuthors_LeadAuthorChanged);

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// parses the specified xml into body and section xml,
        /// and returns a delimited string of page setup values
        /// </summary>
        /// <param name="xSegmentXML"></param>
        /// <param name="xBodyXML"></param>
        /// <param name="xSectionsXML"></param>
        /// <param name="xPageSetupValues"></param>
        protected static void ParseSegmentXML(string xSegmentXML, out string xBodyXML,
            out string xSectionsXML, out string xPageSetupValues, out string xCompatOptions)
        {
            DateTime t0 = DateTime.Now;

            //get segment xml without header and footer nodes
            Regex oRegex = null;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                //word open xml - headers and footers are in separate parts -
                //just remove the references to them in the <w:sectPr> nodes
                oRegex = new Regex("<w:(head|foot)erReference .*?/>", RegexOptions.Singleline);
            else
                oRegex = new Regex("<w:(hd|ft)r .*?</w:(hd|ft)r>", RegexOptions.Singleline);
            xBodyXML = oRegex.Replace(xSegmentXML, "");

            string xDocLayoutValues = GetDocLayoutValues(xSegmentXML);
            //GLOG 5516
            string xDocBackgroundValues = GetPageBackgroundValues(xSegmentXML);

            //create regular expression that matches w:sectPr nodes
            //OLD REGEX - did not result in appropriate matches
            //oRegex = new Regex("<w:sectPr( .*|)/?>.*?</?w:sectPr>", RegexOptions.Singleline);
            oRegex = new Regex("(<w:sectPr .*?>|<w:sectPr?>).*?</?w:sectPr>", RegexOptions.Singleline);

            //cycle through <w:sectPr> nodes, building sections and page setup strings,
            //the former as xml, the latter as a delimited string
            StringBuilder oSBSections = new StringBuilder("<Sections>");
            StringBuilder oSBPageSetup = new StringBuilder();
            MatchCollection oMatches = oRegex.Matches(xSegmentXML);
            foreach (Match oMatch in oMatches)
            {
                //append xml for section
                string xSectionXML = oMatch.Value;
                oSBSections.Append(xSectionXML);

                //append page setup values - add sections separator if necessary
                if (oSBPageSetup.Length > 0)
                    oSBPageSetup.Append('|');
                oSBPageSetup.Append(GetPageSetupValues(xSectionXML, xDocLayoutValues));
            }

            //add end tag
            oSBSections.Append("</Sections>");


            //convert to strings
            xCompatOptions = GetCompatibilityOptions(xSegmentXML);
            xSectionsXML = oSBSections.ToString();
            xPageSetupValues = oSBPageSetup.ToString();
            LMP.Benchmarks.Print(t0);
        }
        protected static string GetPageBackgroundValues(string xXML)
        {
            //GLOG 5516: Parse Document Background settings from XML
            //TODO: Extend to support Gradients, Textures and Patterns -
            //currently just handles solid background
            StringBuilder oSB = new StringBuilder();
            Match oBackground = Regex.Match(xXML, "<w:background.*?/>");
            if (oBackground.Value != "")
            {
                string xHexColor = "";
                string xFillColor = "";
                if (LMP.String.IsWordOpenXML(xXML))
                {
                    //TODO: Commented code is start of Gradient background support
                    //Match oFill1 = Regex.Match(oBackground.Value, "<v:background.*?/>");
                    //if (oFill1.Value != "")
                    //{
                    //    xHexColor = xGetValue(oFill1.Value, "fillcolor");
                    //}
                    //else
                    xHexColor = xGetValue(oBackground.Value, "color");

                    //Match oFill2 = Regex.Match(oBackground.Value, "<v:fill.*?/>");
                    //if (oFill2.Value != "")
                    //{
                    //    xFillColor = xGetValue(oFill2.Value, "color2");
                    //    xAngle = xGetValue(oFill2.Value, "angle");
                    //    xGradient = xGetValue(oFill2.Value, "type");
                    //}
                }
                else
                {
                    //Different attribute name for Word 2003 XML
                    xHexColor = xGetValue(oBackground.Value, "bgcolor");
                }

                if (xHexColor != "")
                {
                    //In Word 2003 XML, Hex Number is preceded by '#'
                    xHexColor = xHexColor.Trim(new char[] { '#' });
                    xFillColor = xFillColor.Trim(new char[] { '#' });
                    if (xHexColor != "")
                    {
                        //For some reason, Hex Value appears reversed, except for last character, e.g., '05F4EA' becomes 'E4F50A'
                        char[] aHex = xHexColor.ToCharArray();
                        Array.Reverse(aHex, 0, aHex.Length - 1);
                        xHexColor = new string(aHex);
                    }
                    oSB.Append(int.Parse(xHexColor, System.Globalization.NumberStyles.HexNumber) + "�");
                    //TODO: Gradient background support
                    //if (xFillColor != "")
                    //{
                    //    char[] aHex = xFillColor.ToCharArray();
                    //    Array.Reverse(aHex, 0, aHex.Length - 1);
                    //    xFillColor = new string(aHex);
                    //    oSB.Append("�" + int.Parse(xFillColor, System.Globalization.NumberStyles.HexNumber) + "�" + xAngle + "�" + xGradient);
                    //}
                    //else
                    //    oSB.Append("���");
                }
                else
                {
                    oSB.Append("�");
                }
            }
            else
            {
                oSB.Append("�");
            }
            return oSB.ToString();
        }
        protected static string GetDocLayoutValues(string xSegmentXML)
        {
            DateTime t0 = DateTime.Now;

            StringBuilder oSB = new StringBuilder();

            Match oDocSettings = Regex.Match(xSegmentXML, "<w:docPr>.*?</w:docPr>");
            //GLOG 3722: Get additional Page Setup settings saved at Document Level
            if (oDocSettings.Value != "")
            {
                //GLOG 3722: Different Even/Odd Headers
                Match oEvenAndOdd = Regex.Match(oDocSettings.Value, "<w:evenAndOddHeaders/>");
                bool bEvenAndOdd = (oEvenAndOdd.Value != "");
                oSB.AppendFormat("{0}�", bEvenAndOdd ? "1" : "0");

                //GLOG 3722: Mirror Margins
                Match oMirror = Regex.Match(oDocSettings.Value, "<w:mirrorMargins/>");
                bool bMirror = (oMirror.Value != "");
                oSB.AppendFormat("{0}�", bMirror ? "1" : "0");

                //GLOG 3722: Book fold Printing
                Match oBookFold = Regex.Match(oDocSettings.Value, "<w:bookFoldPrinting/>");
                bool bBookFold = (oBookFold.Value != "");
                oSB.AppendFormat("{0}�", bBookFold ? "1" : "0");
                if (bBookFold)
                {
                    Match oBookFoldSheets = Regex.Match(oDocSettings.Value, "<w:bookFoldPrintingSheets.*?/>");
                    if (oBookFoldSheets.Value != "")
                    {
                        //Convert value to integer for Word PageSetup dialog, corresponding to index in list
                        //  0 - Auto
                        //  1 - All
                        //  2 - 4
                        //  3 - 8
                        //  4 - 12
                        //  ..and so on in multiples of 4
                        int iSheets = Int32.Parse(xGetValue(oBookFoldSheets.Value, "val"));
                        if (iSheets == -4)
                            iSheets = 0;
                        else
                            iSheets = (iSheets / 4) + 1;
                        oSB.AppendFormat("{0}�", iSheets.ToString());
                    }
                    else
                    {
                        //Default if missing from XML is "All"
                        oSB.Append("1�");
                    }
                }
                else
                {
                    oSB.Append("0�");
                }

                //GLOG 3722: Two pages per sheet
                Match oTwoPages = Regex.Match(oDocSettings.Value, "<w:printTwoOnOne/>");
                bool bTwoPages = (oTwoPages.Value != "");
                oSB.AppendFormat("{0}�", bTwoPages ? "1" : "0");

                //GLOG 3722: Gutter at Top
                Match oTopGutter = Regex.Match(oDocSettings.Value, "<w:gutterAtTop/>");
                bool bTopGutter = (oTopGutter.Value != "");
                oSB.AppendFormat("{0}�", bTopGutter ? "1" : "0"); //GLOG 5516

            }
            else
            {
                oSB.Append("0�0�0�0�0�0�"); //GLOG 5516
            }
            //GLOG 5516: Page Border settings
            Match oAlignBorders = Regex.Match(oDocSettings.Value, "<w:alignBordersAndEdges/>");
            bool bAlign = (oAlignBorders.Value != "");
            oSB.AppendFormat("{0}�", bAlign ? "1" : "0");


            //These settings are named differently in 2003 and WordOpen XML
            Match oSurroundHeader;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                oSurroundHeader = Regex.Match(oDocSettings.Value, "<w:bordersDontSurroundHeader/>");
            else
                oSurroundHeader = Regex.Match(oDocSettings.Value, "<w:bordersDoNotSurroundHeader/>");
            bool bSurroundHeader = (oSurroundHeader.Value != "");
            oSB.AppendFormat("{0}�", bSurroundHeader ? "0" : "1");

            Match oSurroundFooter;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                oSurroundFooter = Regex.Match(oDocSettings.Value, "<w:bordersDontSurroundFooter/>");
            else
                oSurroundFooter = Regex.Match(oDocSettings.Value, "<w:bordersDoNotSurroundFooter/>");
            bool bSurroundFooter = (oSurroundFooter.Value != "");
            oSB.AppendFormat("{0}�", bSurroundFooter ? "0" : "1");
            LMP.Benchmarks.Print(t0);

            return oSB.ToString();
        }
        /// <summary>
        /// returns a delimited string of page setup
        /// values from the specified section xml
        /// </summary>
        /// <param name="xSectionXML"></param>
        /// <returns></returns>
        protected static string GetPageSetupValues(string xSectionXML, string xDocLayoutValues)
        {
            //GLOG 3722: Returns delimited string containing these Page Setup settings.
            //These correspond to parameters of WordBasic.PageSetupMargins command:
            //  1. Orientation
            //  2. Height
            //  3. Width
            //  4. Paper Size
            //  5. Left Margin
            //  6. Right Margin
            //  7. Top Margin
            //  8. Bottom Margin
            //  9. Header Distance
            // 10. Footer Distance
            // 11. Gutter Distance
            // 12. Vertical Alignment
            // 13. Different First Page
            // 14. Page 1 Tray
            // 15. Other Pages Tray
            // 16. Show Line Numbers
            // 17. Starting Number
            // 18. Count By
            // 19. Restart mode
            // 20. Distance from text
            //
            //Settings from DocPr XML:
            // 21. Different Even and Odd
            // 22. Mirror Margins
            // 23. Bookfold Printing
            // 24. Bookfold sheets
            // 25. Two per page
            // 26. Gutter at top

            //Page Border Settings (GLOG 5516)
            //From DocPr:
            // 27. Align Borders and Edges
            // 28. Surround Headers
            // 29. Surround Footers

            //From SectPr:
            // 30. Offset
            // 31. Display first page
            // 32. Display other pages
            // 33. Z-Order
            // 34. Top Style
            // 35. Top width
            // 36. Top distance
            // 37. Top color
            // 38. Top Shadow
            // 39. Left Style
            // 40. Left width
            // 41. Left distance
            // 42. Left color
            // 43. Left Shadow
            // 44. Bottom Style
            // 45. Bottom width
            // 46. Bottom distance
            // 47. Bottom color
            // 48. Bottom Shadow
            // 49. Right Style
            // 50. Right width
            // 51. Right distance
            // 52. Right color
            // 53. Right Shadow

            DateTime t0 = DateTime.Now;

            StringBuilder oSB = new StringBuilder();
            //GLOG 4230: Strip out footer/header XML to ensure only section-specific nodes are being evaluated.
            //For instance, individual table cells might contain v:Align node, which we don't want applied to
            //page setup.
            //GLOG 4537: Need to adjust Regex to ensure all hdr/ftr nodes were included, not just the first.
            //Singleline option is required, because pictures objects in the header or footer will result in
            //embedded \n characters, which will stop continued matching by default
            Match oStrip = Regex.Match(xSectionXML, "<w:(ftr|hdr)(.*</w:(ftr|hdr)>)*", RegexOptions.Singleline);
            if (oStrip != null && !string.IsNullOrEmpty(oStrip.Value))
                xSectionXML = xSectionXML.Remove(oStrip.Index, oStrip.Length);

            string [] aDocLayoutProps = xDocLayoutValues.Split('�');
            //get page size values
            Match oPageSize = Regex.Match(xSectionXML, "<w:pgSz.*?/>");
            if (oPageSize.Value != "")
            {
                //GLOG 3722: Include xml for Orientation
                string xOrient = xGetValue(oPageSize.Value, "orient");
                if (!string.IsNullOrEmpty(xOrient) && xOrient.ToUpper() == "LANDSCAPE")
                    oSB.Append("1�");
                else
                    oSB.Append("0�");

                int iPgHeight = Int32.Parse(xGetValue(oPageSize.Value, "h"));
                int iPgWidth = Int32.Parse(xGetValue(oPageSize.Value, "w"));
                if (aDocLayoutProps[4] == "1")
                {
                    //if 2-on-one is enabled, actual height or width will be double value in XML
                    if (xOrient.ToUpper() == "LANDSCAPE")
                        iPgWidth = iPgWidth * 2;
                    else
                        iPgHeight = iPgHeight * 2;
                }
                else if (aDocLayoutProps[2] == "1")
                {
                    //If bookfold printing enabled, actual width will be double value in XML
                    iPgWidth = iPgWidth * 2;
                }

                oSB.AppendFormat("{0}�", iPgHeight.ToString());
                oSB.AppendFormat("{0}�", iPgWidth.ToString());
                //GLOG 3722: Include xml for Paper Size code
                string xCode = xGetValue(oPageSize.Value, "code");
                if (!string.IsNullOrEmpty(xCode))
                    oSB.AppendFormat("{0}�", xCode);
                else
                    oSB.Append("1�");
            }
            else
                //Use default settings (but I think pgSz XML will always be present)
                oSB.Append("0�12240�15840�1�");

            //get page margin values
            Match oPageMargins = Regex.Match(xSectionXML, "<w:pgMar.*?/>");
            if (oPageMargins.Value != "")
            {
                try
                {
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "left"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "right"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "top"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "bottom"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "header"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "footer"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "gutter"));
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_InvalidPageSetupValue"), oE);
                }
            }
            else
            {
                //Use default values
                oSB.Append("1440�1440�1440�1440�720�720�0�");
            }
            //GLOG 3722: Include xml for Page Alignment
            Match oAlign = Regex.Match(xSectionXML, "<w:vAlign.*?/>");
            if (oAlign.Value != "")
            {
                try
                {
                    //Convert XML value to integer for Page Setup dialog
                    string xAlign = xGetValue(oAlign.Value, "val");
                    int iAlign = 0;
                    switch (xAlign.ToUpper())
                    {
                        case "CENTER":
                            iAlign = 1;
                            break;
                        case "BOTH":
                            iAlign = 2;
                            break;
                        case "BOTTOM":
                            iAlign = 3;
                            break;
                    }
                    oSB.AppendFormat("{0}�", iAlign.ToString());
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_InvalidPageSetupValue"), oE);
                }
            }
            else
                oSB.Append("0�");

            //get different first page value
            Match oTitlePage = Regex.Match(xSectionXML, "<w:titlePg.*?/>");
            bool bTitlePage = (oTitlePage.Value != "");
            oSB.AppendFormat("{0}�", bTitlePage ? "1" : "0");

            //get paper tray settings
            Match oPaperSrc = Regex.Match(xSectionXML, "<w:paperSrc.*?/>");
            if (oPaperSrc.Value != "")
            {
                string xFirst = xGetValue(oPaperSrc.Value, "first");
                //GLOG 8563
                if (string.IsNullOrEmpty(xFirst))
                    xFirst = "0";
                oSB.AppendFormat("{0}�", xFirst);
                string xOther = xGetValue(oPaperSrc.Value, "other");
                //GLOG 8563
                if (string.IsNullOrEmpty(xOther))
                    xOther = "0";
                oSB.AppendFormat("{0}�", xOther);
            }
            else
            {
                oSB.Append("0�0�");
            }

            //get paper tray settings
            Match oLineNumbers = Regex.Match(xSectionXML, "<w:lnNumType.*?/>");
            if (oLineNumbers.Value != "")
            {
                oSB.Append("1�");
                //starting number in XML is one less than actual value
                string xStart = xGetValue(oLineNumbers.Value, "start");
                if (!string.IsNullOrEmpty(xStart))
                {
                    oSB.AppendFormat("{0}�", Int32.Parse(xStart) + 1);
                }
                else
                {
                    oSB.Append("1�");
                }

                oSB.AppendFormat("{0}�", xGetValue(oLineNumbers.Value, "count-by"));

                string xNumMode = xGetValue(oLineNumbers.Value, "restart");
                if (!string.IsNullOrEmpty(xNumMode))
                {
                    //Default restart numbering each page
                    int iRestart = 0;
                    if (xNumMode == "new-section")
                        iRestart = 1;
                    else if (xNumMode == "continuous")
                        iRestart = 2;
                    oSB.AppendFormat("{0}�", iRestart);
                }
                else
                {
                    oSB.Append("0�");
                }
                string xDistance = xGetValue(oLineNumbers.Value, "distance");
                if (!string.IsNullOrEmpty(xDistance))
                {
                    oSB.AppendFormat("{0}�", xDistance);
                }
                else
                {
                    oSB.Append("0�");
                }

            }
            else
            {
                oSB.Append("0�1�1�0�0�");
            }
            oSB.Append(xDocLayoutValues);
            //GLOG 5516: Parse Page Border options from XML
            //TODO: Extend to support Border Art options
            Match oPageBorders = Regex.Match(xSectionXML, "<w:pgBorders.*?</w:pgBorders>");
            if (oPageBorders.Value != "")
            {
                string xTest = xGetValue(oPageBorders.Value, "offsetFrom");
                if (xTest == "page")
                    //Offset from page
                    oSB.Append("1�");
                else
                    //Offset from text
                    oSB.Append("0�");

                //Enable First Page and Other Pages Options
                xTest = xGetValue(oPageBorders.Value, "display");
                if (xTest == "firstPage" || xTest == "first-page")
                    oSB.Append("-1�0�");
                else if (xTest == "notFirstPage" || xTest == "not-first-page")
                    oSB.Append("0�-1�");
                else
                    oSB.Append("-1�-1�");

                //Always in Front value
                xTest = xGetValue(oPageBorders.Value, "zorder");
                //Check Word 2003 attribute name also
                if (string.IsNullOrEmpty(xTest))
                    xTest = xGetValue(oPageBorders.Value, "z-order");
                if (xTest == "back")
                    oSB.Append("0�");
                else
                    oSB.Append("1�");

                string[] xBorders = new string[] { "top", "left", "bottom", "right" };
                foreach (string xBorder in xBorders)
                {
                    Match oBorder = Regex.Match(oPageBorders.Value, "<w:" + xBorder + ".*?/>");
                    if (oBorder.Value != "")
                    {
                        xTest = xGetValue(oBorder.Value, "val");
                        int iLineType = 0;
                        //test both WordOpen and 2003 variants
                        switch (xTest)
                        {
                            case "single":
                                iLineType = 1;
                                break;
                            case "dotted":
                                iLineType = 2;
                                break;
                            case "dashSmallGap":
                            case "dash-small-gap":
                                iLineType = 3;
                                break;
                            case "dashed":
                                iLineType = 4;
                                break;
                            case "dotDash":
                            case "dot-dash":
                                iLineType = 5;
                                break;
                            case "dotDotDash":
                            case "dot-dot-dash":
                                iLineType = 6;
                                break;
                            case "double":
                                iLineType = 7;
                                break;
                            case "triple":
                                iLineType = 8;
                                break;
                            case "thickThinSmallGap":
                            case "thick-thin-small-gap":
                                iLineType = 9;
                                break;
                            case "thinThickSmallGap":
                            case "thin-thick-small-gap":
                                iLineType = 10;
                                break;
                            case "thinThickThinSmallGap":
                            case "thin-thick-thin-small-gap":
                                iLineType = 11;
                                break;
                            case "thinThickMediumGap":
                            case "thin-thick-medium-gap":
                                iLineType = 12;
                                break;
                            case "thickThinMediumGap":
                            case "thick-thin-medium-gap":
                                iLineType = 13;
                                break;
                            case "thinThickThinMediumGap":
                            case "thin-thick-thin-medium-gap":
                                iLineType = 14;
                                break;
                            case "thinThickLargeGap":
                            case "thin-thick-large-gap":
                                iLineType = 15;
                                break;
                            case "thickThinLargeGap":
                            case "thick-thin-large-gap":
                                iLineType = 16;
                                break;
                            case "thinThickThinLargeGap":
                            case "thin-thick-thin-large-gap":
                                iLineType = 17;
                                break;
                            case "wave":
                                iLineType = 18;
                                break;
                            case "doubleWave":
                            case "double-wave":
                                iLineType = 19;
                                break;
                            case "dashDotStroked":
                            case "dash-dot-stroked":
                                iLineType = 20;
                                break;
                            case "threeDEmboss":
                            case "three-d-emboss":
                                iLineType = 21;
                                break;
                            case "threeDEngrave":
                            case "three-d-engrave":
                                iLineType = 22;
                                break;
                            case "outset":
                                iLineType = 23;
                                break;
                            case "inset":
                                iLineType = 24;
                                break;
                        }
                        string xSize = xGetValue(oBorder.Value, "sz");
                        string xSpace = xGetValue(oBorder.Value, "space");
                        string xColor = xGetValue(oBorder.Value, "color");
                        int iColor = (int)Word.WdColor.wdColorAutomatic;
                        if (!string.IsNullOrEmpty(xColor) && xColor != "auto")
                        {
                            //For some reason, Hex Value appears reversed, except for last character, e.g., '05F4EA' becomes 'E4F50A'
                            char[] aHex = xColor.ToCharArray();
                            Array.Reverse(aHex, 0, aHex.Length - 1);
                            xColor = new string(aHex);
                            try
                            {
                                iColor = int.Parse(xColor, System.Globalization.NumberStyles.HexNumber);
                            }
                            catch { }
                        }

                        string xShadow = xGetValue(oBorder.Value, "shadow");
                        int iShadow = (xShadow == "1" || xShadow == "on") ? 1 : 0;
                        oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}�", iLineType, xSize, xSpace, iColor, iShadow);
                    }
                    else
                        oSB.Append("�����");
                }
            }
            else
                oSB.Append("������������������������");
            LMP.Benchmarks.Print(t0);
            return oSB.ToString();
        }
        
        public static string GetCompatibilityOptions(string xSegmentXML)
        {
            //GLOG 4967 (dm) - open xml has multiple w:compat nodes -
            //make sure to start with the right one
            string xXML = null;
            bool bWordOpenXML = false;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
            {
                bWordOpenXML = true;
                int iPos = xSegmentXML.IndexOf("<pkg:part pkg:name=\"/word/settings.xml\"");
                xXML = xSegmentXML.Substring(iPos);
            }
            else
                xXML = xSegmentXML;

            Regex oRegex = new Regex("<w:compat>.*?</w:compat>");
            Match oCompat = oRegex.Match(xXML);
            string xCompatXML = "";
            string xCompatValues = "";
            string xFormat = "{0}�{1}";
            StringBuilder oSB = new StringBuilder();
            if (oCompat != null && oCompat.Value != "")
            {
                int iIndex;
                bool bValue;
                XmlDocument oXML = new XmlDocument();
                xCompatXML = oCompat.Value.Replace("w:", "");
                oXML.LoadXml(xCompatXML);
                foreach (XmlNode oNode in oXML.DocumentElement.ChildNodes)
                {
                    iIndex = 0;
                    //Default is false for all but 5 items - presence of node indicates change from default
                    bValue = true;
                    switch (oNode.Name)
                    {
                        case "suppressTopSpacing":
                            iIndex = 8;
                            break;
                        case "suppressBottomSpacing":
                            iIndex = 29;
                            break;
                        case "wrapTrailSpaces":
                            iIndex = 4;
                            break;
                        case "printColBlack":
                            iIndex = 3;
                            break;
                        case "usePrinterMetrics":
                            iIndex = 26;
                            break;
                        //GLOG 5709: Checked state in UI corresponds to False setting for compatibility option
                        case "doNotExpandShiftReturn":
                            iIndex = 14;
                            bValue = false;
                            break;
                        //GLOG 5112: The following settings don't default to True in WordOpenXML -
                        //only set if corresonding Tag is present
                        case "breakWrappedTables":
                            if (bWordOpenXML)
                                iIndex = 43;
                            break;
                        case "snapToGridInCell":
                            if (bWordOpenXML)
                                iIndex = 44;
                            break;
                        case "wrapTextWithPunct":
                            if (bWordOpenXML)
                                iIndex = 47;
                            break;
                        case "useAsianBreakRules":
                            if (bWordOpenXML)
                                iIndex = 48;
                            break;
                        case "dontGrowAutoFit":
                            if (bWordOpenXML)
                                iIndex = 50;
                            break;
                        default:
                            break;
                    }
                    if (iIndex > 0)
                    {
                        oSB.Append(string.Format(xFormat, iIndex, bValue));
                        oSB.Append("|");
                    }
                }
            }
            //GLOG 5112: These settings are handled differently in old-style XML
            if (!bWordOpenXML)
            {
                //Default for these 5 is true - 
                //if missing from XML, corresponding option needs to be set to true
                if (xCompatXML.IndexOf("breakWrappedTables") == -1)
                {
                    oSB.Append(string.Format(xFormat, 43, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("snapToGridInCell") == -1)
                {
                    oSB.Append(string.Format(xFormat, 44, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("wrapTextWithPunct") == -1)
                {
                    oSB.Append(string.Format(xFormat, 47, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("useAsianBreakRules") == -1)
                {
                    oSB.Append(string.Format(xFormat, 48, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("dontGrowAutofit") == -1)
                {
                    oSB.Append(string.Format(xFormat, 50, true));
                    oSB.Append("|");
                }

            }
            xCompatValues = oSB.ToString();
            if (xCompatValues != "")
                xCompatValues = xCompatValues.TrimEnd('|');
            return xCompatValues;
        }
        /// <summary>
        /// Converts delimited list of levels into string array
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public static int[] GetJurisdictionArray(string xValue)
        {
            //Initialize all levels to 0
            int[] xLevels = new int[] { 0, 0, 0, 0, 0 };
            string[] xL = xValue.Split(StringArray.mpEndOfRecord);

            for (int i = 0; i <= xL.GetUpperBound(0); i++)
            {
                try
                {
                    xLevels[i] = Int32.Parse(xL[i]);
                }
                catch { }
            }
            return xLevels;
        }
        /// <summary>
        /// True if Segment is set up to be shell for other Segments of the same type
        /// </summary>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        public static bool IsShellSegment(Segment oSeg)
        {
            return oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument && oSeg.ShowChooser == true
                && oSeg.Variables.Count == 0 && oSeg.Blocks.Count == 0 &&
                oSeg.MaxAuthors == 0;
        }
        /// <summary>
        /// appends the specified contacts to each CI-enabled Variable
        /// </summary>
        /// <param name="oContacts"></param>
        private void SetCIEnabledVariables(ICContacts oContacts)
        {
            SetCIEnabledVariables(this.Variables.CIEnabledVariables, oContacts, true);
            //GLOG 5940: Child Segment handling moved to overload
        }

        /// <summary>
        /// adds the specified contacts to each CI-enabled Variable
        /// </summary>
        /// <param name="oContacts"></param>
        /// <param name="bAppend">if true, contacts will be appended</param>
        private void SetCIEnabledVariables(List<Variable> oVarList, ICContacts oContacts, bool bAppend)
        {
            //GLOG 5940: Child Segments are now handled by this method, so that children of children will be included
            if (oContacts == null)
                return;

            foreach (Variable oVar in oVarList)
            {
                //refer back to variables collection because oVar will no longer
                //be current if a variable action of the previous variable in the cycle
                //has forced a segment refresh, e.g. following the restoration of
                //a deleted scope that has multiple variables in the scope (dm 5/18/09)
                this.Variables.ItemFromName(oVar.Name).SetValueFromContacts(
                    oContacts, bAppend);
            }
            // Populate CI-Enabled variables of Child Segments also
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].SetCIEnabledVariables(this.Segments[i].Variables.CIEnabledVariables, oContacts, true);
        }

        /// <summary>
        /// Retrieve Max Contacts information by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="iMax"></param>
        /// <param name="iToMax"></param>
        /// <param name="iFromMax"></param>
        /// <param name="iCCMax"></param>
        /// <param name="iBCCMax"></param>
        private void GetCIMaxContacts(ref int iMax, ref int iToMax, 
                                    ref int iFromMax, ref int iCCMax, 
                                    ref int iBCCMax, ref int iOtherMax)
        {
            this.GetCIVariableInfo(this.Variables.CIEnabledVariables, ref iMax, ref iToMax, 
                                   ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);

            // Check child segments for additional ci-enabled variables
            for (int j = 0; j < this.Segments.Count; j++)
                this.Segments[j].GetCIMaxContacts(ref iMax, ref iToMax, ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);
        }

        /// <summary>
        /// Retrieve Display Name by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="xToLabel"></param>
        /// <param name="xFromLabel"></param>
        /// <param name="xCCLabel"></param>
        /// <param name="xBCCLabel"></param>
        private void GetCIDisplayName(ref string xToLabel, ref string xFromLabel,
                                    ref string xCCLabel, ref string xBCCLabel)
        {
            this.GetCIVariableInfo(this.Variables.CIEnabledVariables, ref xToLabel, 
                                   ref xFromLabel, ref xCCLabel, ref xBCCLabel);

            // Check child segments for additional ci-enabled variables
            for (int j = 0; j < this.Segments.Count; j++)
                this.Segments[j].GetCIDisplayName(ref xToLabel, ref xFromLabel, ref xCCLabel, ref xBCCLabel);
        }

        /// <summary>
        /// Get Display Name by cycling through all variables in List collection
        /// </summary>
        /// <param name="oVarList"></param>
        /// <param name="xToLabel"></param>
        /// <param name="xFromLabel"></param>
        /// <param name="xCCLabel"></param>
        /// <param name="xBCCLabel"></param>
        private void GetCIVariableInfo(List<Variable> oVarList, ref string xToLabel, ref string xFromLabel,
                                       ref string xCCLabel, ref string xBCCLabel)
        {
            //cycle through each ci-enabled variable
            foreach (Variable oVar in oVarList)
            {
                switch (oVar.CIContactType)
                {
                    case ciContactTypes.ciContactType_To:
                        if (oVar.DisplayName != "")
                            xToLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_From:
                        if (oVar.DisplayName != "")
                            xFromLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_CC:
                        if (oVar.DisplayName != "")
                            xCCLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_BCC:
                        if (oVar.DisplayName != "")
                            xBCCLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                }
            }
        }

        /// <summary>
        /// Get Max Contacts by cycling through all variables in List collection
        /// </summary>
        /// <param name="oVarList"></param>
        /// <param name="iMax"></param>
        /// <param name="iToMax"></param>
        /// <param name="iFromMax"></param>
        /// <param name="iCCMax"></param>
        /// <param name="iBCCMax"></param>
        private void GetCIVariableInfo(List<Variable> oVarList, ref int iMax, ref int iToMax, 
                                       ref int iFromMax, ref int iCCMax, ref int iBCCMax, ref int iOtherMax)
        {
            string[] aProps = null;

            //cycle through each variable in oVarlist
            foreach (Variable oVar in oVarList)
            {
                //GLOG - 3503 - ceh
                //don't reset max property if variable is not visible
                //GLOG 8236: Don't return immediately if invisible control
                //is encountered, but move on to the next variable
                if (oVar.DisplayIn == Variable.ControlHosts.None)
                    continue;

                //#3503 - ceh 
                //limit max contacts for variable controls of type 'Detail Grid'
                if (oVar.ControlType == mpControlTypes.DetailGrid && oVar.ControlProperties.IndexOf("MaxEntities=") != -1)
                {
                    //split into array
                    aProps = oVar.ControlProperties.Split(StringArray.mpEndOfSubValue);

                    //cycle through property name/value pairs
                    for (int j = 0; j < aProps.Length; j++)
                    {
                        int iPos = aProps[j].IndexOf("MaxEntities=");
                        if (iPos != -1)
                        {
                            //get value
                            string xValue = aProps[j].Substring(12);

                            //evaluate control prop value, which may be a MacPac expression
                            xValue = Expression.Evaluate(xValue, this, this.ForteDocument);

                            //convert to integer
                            iMax = Int32.Parse(xValue);

                            if (iMax == 0)
                                break;

                            //check AssociatedCcontrol count and
                            //update iMax as necessary
                            int iCurrentCount = 0;

                            //find last instance of Index
                            //GLOG 7597:  Create AssociatedControl if necessary
                            int iNewPos = -1;
                            IControl oCIControl = null;
                            if (oVar.AssociatedControl != null)
                            {
                                oCIControl = oVar.AssociatedControl;
                            }
                            else
                            {
                                oCIControl = oVar.CreateAssociatedControl(null);
                            }
                            iNewPos = oCIControl.Value.LastIndexOf("Index=");
                            if (iNewPos != -1)
                            {
                                //get current index value
                                string xNewValue = oCIControl.Value.Substring(iNewPos + 7); //GLOG 7597
                                iNewPos = xNewValue.IndexOf("\"");
                                iCurrentCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                            }
                            else
                            {
                                iMax = iMax - iCurrentCount;
                                break;
                            }
                            //update iMax
                            iMax = iMax - iCurrentCount;
                            if (iMax <= 0)
                                iMax = -1;
                            break;
                        }
                    }
                }
                else
                {
                    iMax = 0;
                }

                //GLOG 8236: Code here moved above

                switch (oVar.CIContactType)
                {
                    case ciContactTypes.ciContactType_To:
                        iToMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_From:
                        iFromMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_CC:
                        iCCMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_BCC:
                        iBCCMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_Other:
                        iOtherMax = iMax;
                        break;
                }
            }
        }

        /// <summary>
        /// Select CI Data to retrieve by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="iDataTo"></param>
        /// <param name="iDataFrom"></param>
        /// <param name="iDataCC"></param>
        /// <param name="iDataBCC"></param>
        /// <param name="iAlerts"></param>
        public void GetCIFormat(ref ciRetrieveData iDataTo, ref ciRetrieveData iDataFrom,
            ref ciRetrieveData iDataCC, ref ciRetrieveData iDataBCC, ref object[,] aCustomEntities, ref ciAlerts iAlerts)
        {
            //GLOG 3461: We're no longer saving these values between calls
            //CI-enabled variables may have been added or removed since previous call
            //// If Format has already been retrieved just return current values
            //if (m_bCIFormatRetrieved)
            //{
            //    iDataTo = iDataTo | m_iRetrieveDataTo;
            //    iDataFrom = iDataFrom | m_iRetrieveDataFrom;
            //    iDataCC = iDataCC | m_iRetrieveDataCC;
            //    iDataBCC = iDataBCC | m_iRetrieveDataBCC;
            //    aCustomEntities = m_aCustomEntities;
            //    iAlerts = iAlerts | m_iCIAlerts;
            //    return;
            //}
            Contacts.GetCIFormat(this.Variables.CIEnabledVariables, ref iDataTo, ref iDataFrom, ref iDataCC, 
                ref iDataBCC, ref aCustomEntities, ref iAlerts);

            // Check child segments for additional ci-enabled variables
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].GetCIFormat(ref iDataTo, ref iDataFrom, ref iDataCC, 
                    ref iDataBCC, ref aCustomEntities, ref iAlerts);
        }

        /// <summary>
        /// save author info to Segment mSEGs
        /// </summary>
        private void SaveAuthorsDetail(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;
            oSegment.Nodes.SetItemAuthorsData(oSegment.FullTagID, oSegment.Authors.XML);
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns TRUE if the current user is on the list specified for
        /// this segment's author control
        /// </summary>
        /// <returns></returns>
        private bool UserIsOnAuthorList()
        {
            LMP.Controls.mpListTypes iType = LMP.Controls.mpListTypes.AllAuthors;
            string xACP = this.AuthorControlProperties;
            if (xACP != null && xACP.Contains("ListType="))
            {
                //author list is filtered - get list type
                string[] aCP = xACP.Split(StringArray.mpEndOfSubValue);
                for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                {
                    if (aCP[i].StartsWith("ListType="))
                    {
                        try
                        {
                            iType = (LMP.Controls.mpListTypes)(Int32.Parse(aCP[i].Substring(9)));
                        }
                        catch { }
                        break;
                    }
                }
            }

            //determine whether list type includes user
            Person oUser = LMP.Data.Application.User.PersonObject;
            if (iType == LMP.Controls.mpListTypes.Attorneys)
                return oUser.IsAttorney;
            else if (iType == LMP.Controls.mpListTypes.NonAttorneys)
                return !oUser.IsAttorney;
            else if (iType == LMP.Controls.mpListTypes.AllAuthors)
                return oUser.IsAuthor;
            else
                return true;
        }
        /// <summary>
        /// Get full path and name of Template to use in creating new blank document for segment
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xSourceXML"></param>
        /// <returns></returns>
        public static string GetNewDocumentTemplate(string xID, string xSourceXML)
        {
            //GLOG 3088
            DateTime t0 = DateTime.Now;
            try
            {
                ISegmentDef oDef = Segment.GetDefFromID(xID);
                //GLOG 8655: If Saved Content, look for ID of contained Segment
                if (oDef.TypeID == mpObjectTypes.SavedContent)
                {
                    xID = oDef.ChildSegmentIDs;
                    oDef = Segment.GetDefFromID(xID);
                    //Use XML of related segment in case definition has changed since saved segment was created
                    xSourceXML = "";
                }
                if (xID.EndsWith(".0"))
                    xID = xID.Substring(0, xID.Length - 2);

                if (string.IsNullOrEmpty(xSourceXML))
                {
                    //if XML not specified get XML from the segment def for this segment
                    xSourceXML = oDef.XML;
                }

                //Create XML Document from XML
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(xSourceXML);
                XmlNamespaceManager oNmSpcMgr = new XmlNamespaceManager(oXML.NameTable);

                //10.2 (dm)
                bool bSeekContentControl = (LMP.String.IsWordOpenXML(xSourceXML));
                string xXPath = "";
                if (bSeekContentControl)
                {
                    oNmSpcMgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                    xXPath = "//w:sdtPr/w:tag";
                }
                else
                {
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.MacPacNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.MacPacNamespace);
                    xXPath = "//" + xNSPrefix + ":mSEG[@ObjectData]";
                }

                //get all mSEG nodes
                XmlNodeList oMSegNodes = oXML.SelectNodes(xXPath, oNmSpcMgr);
                foreach (XmlNode oNode in oMSegNodes)
                {
                    //Check ObjectData for SegmentID match
                    string xObjectData = null;
                    if (bSeekContentControl)
                    {
                        //content control - get object data from associated doc var
                        string xTag = oNode.Attributes["w:val"].Value;
                        if (LMP.String.GetBoundingObjectBaseName(xTag) == "mSEG")
                        {
                            string xDocVarID = LMP.String.GetDocVarIDFromTag(xTag);
                            xXPath = "//w:docVar[@w:name='mpo" + xDocVarID + "']";
                            XmlNode oDVNode = oXML.SelectSingleNode(xXPath, oNmSpcMgr);
                            if (oDVNode != null)
                                xObjectData = oDVNode.Attributes["w:val"].Value;
                        }
                    }
                    else
                        xObjectData = oNode.Attributes["ObjectData"].Value;

                    if (!(string.IsNullOrEmpty(xObjectData)))
                    {
                        //Need to make sure string is decrypted
                        xObjectData = LMP.String.Decrypt(xObjectData);
                        if (xObjectData.Contains("SegmentID=" + xID + "|"))
                        {
                            //Locate WordTemplate property in ObjectData
                            int iStart = xObjectData.IndexOf("|WordTemplate=");
                            if (iStart > -1)
                            {
                                iStart = iStart + @"|WordTemplate=".Length;
                                int iEnd = xObjectData.IndexOf("|", iStart);
                                if (iEnd > iStart)
                                {
                                    string xTemplate = xObjectData.Substring(iStart, iEnd - iStart);
                                    //If Property contains fieldcode, can't evaluate here, since the 
                                    //corresponding Segment and ForteDocument objects don't exist yet.
                                    //Return default template and Segment.Insert will handle
                                    //evaluating and attaching to template after insertion
                                    //GLOG 3203: Skip if WordTemplate value is blank
                                    if (xTemplate != "" && !(xTemplate.Contains("[") && xTemplate.Contains("]")))
                                    {
                                        //Make sure template extension is appropriate for Word version
                                        if (xTemplate.ToLower().EndsWith(".dot"))
                                            xTemplate = xTemplate + "x";
                                        //If no path specified, used MacPac Templates directory
                                        if (System.IO.Path.GetDirectoryName(xTemplate) == string.Empty)
                                        {
                                            //Use default templates location
                                            xTemplate = Data.Application.TemplatesDirectory + @"\" + @xTemplate;
                                        }
                                        //Check that specified template actually exists
                                        if (System.IO.File.Exists(xTemplate))
                                            return xTemplate;
                                    }
                                }
                            }
                            //SegmentID found, but no word template specified
                            break;
                        }
                    }
                }
                //return default template (ForteNormal.dotx)
                return LMP.Data.Application.BaseTemplate;
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }

        }
        /// <summary>
        /// Get Segment ID from first defined mSEG in XML
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xSourceXML"></param>
        /// <returns></returns>
        public static string GetSegmentIDFromXML(string xSourceXML)
        {
            //GLOG 3088
            DateTime t0 = DateTime.Now;
            try
            {
                if (string.IsNullOrEmpty(xSourceXML))
                    return "";
                
                //Create XML Document from XML
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(xSourceXML);
                XmlNamespaceManager oNmSpcMgr = new XmlNamespaceManager(oXML.NameTable);
                if (LMP.String.IsWordOpenXML(xSourceXML))
                {
                    //get tag of first content control
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.WordOpenXMLNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.WordOpenXMLNamespace);
                    XmlNode oNode = oXML.SelectSingleNode("//" + xNSPrefix +
                        ":tag[starts-with(@w:val,'mps')]", oNmSpcMgr);
                    if (oNode != null)
                    {
                        //search for associated doc var
                        string xTag = oNode.Attributes["w:val"].Value;
                        string xDocVar = "mpo" + xTag.Substring(3, 8);
                        XmlNode oDocVarNode = oXML.SelectSingleNode(
                            "//w:docVar[@w:name='" + xDocVar + "']", oNmSpcMgr);
                        if (oDocVarNode != null)
                        {
                            string xValue = oDocVarNode.Attributes["w:val"].Value;
                            int iPos = xValue.IndexOf("SegmentID=");
                            if (iPos > -1)
                            {
                                iPos += 10;
                                int iPos2 = xValue.IndexOf('|', iPos);
                                string xID = xValue.Substring(iPos, iPos2 - iPos);
                                return xID;
                            }
                        }
                    }
                }
                else
                {
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.MacPacNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.MacPacNamespace);

                    //get all mSEG nodes
                    XmlNode oNode = oXML.SelectSingleNode("//" + xNSPrefix + ":mSEG[@ObjectData]", oNmSpcMgr);
                    if (oNode != null)
                    {
                        //Look in ObjecData for SegmentID
                        string xObjectData = oNode.Attributes["ObjectData"].Value;
                        if (!(string.IsNullOrEmpty(xObjectData)))
                        {
                            //Need to make sure string is decrypted
                            xObjectData = LMP.String.Decrypt(xObjectData);
                            //Locate WordTemplate property in ObjectData
                            int iStart;
                            //SegmentID may be at start of ObjectData, or after Tag Prefix
                            if (xObjectData.StartsWith("SegmentID="))
                                iStart = 0 + @"SegmentID=".Length;
                            else
                            {
                                iStart = xObjectData.IndexOf(String.mpTagPrefixIDSeparator + "SegmentID=");
                                if (iStart > -1)
                                    iStart = iStart + (String.mpTagPrefixIDSeparator + @"SegmentID=").Length;
                            }

                            if (iStart > -1)
                            {
                                int iEnd = xObjectData.IndexOf("|", iStart);
                                if (iEnd > -1)
                                {
                                    string xID = xObjectData.Substring(iStart, iEnd - iStart);
                                    return xID;
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }

        }

        /// <summary>
        /// returns TRUE if the specified xml is for a segment that's in a table
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        public static bool IsTableSegment(string xXML)
        {
            //search for table start tag
            int iPos = xXML.IndexOf("<w:tbl>");
            if (iPos == -1)
                //no table
                return false;

            //get table xml
            int iPos2 = xXML.IndexOf("</w:tbl>", iPos);
            string xTable = xXML.Substring(iPos, iPos2 - iPos);

            //return TRUE is mpNewSegment bookmark is inside the table
            if (xTable.IndexOf("w:name=" + '\"' + "mpNewSegment" + '\"') > -1)
                return true;

            //if the table has multiple rows, the bookmark will be before the table -
            //return TRUE if there are no intervening paragraph end tags
            iPos2 = xXML.IndexOf("w:name=" + '\"' + "mpNewSegment" + '\"');
            //GLOG 6799: Don't check if no bookmark found
            if (iPos2 > -1)
            {
                string xPreceding = xXML.Substring(iPos2, iPos - iPos2);
                bool bIsTable = ((xPreceding.IndexOf("</w:p>") == -1) &&
                    (xPreceding.IndexOf("<w:p/>") == -1));
                return bIsTable;
            }
            else
                return false;
        }

        public static bool DefinitionContainsAuthorPreferences(string xSegmentXML)
        {
            //GLOG 4578 (dm) - changed this from a property to a static method
            xSegmentXML = xSegmentXML.ToUpper();
            return (xSegmentXML.Contains("[AUTHORPREFERENCE_") || xSegmentXML.Contains("[LEADAUTHORPREFERENCE_") ||
                xSegmentXML.Contains("[AUTHORPARENTPREFERENCE") || xSegmentXML.Contains("[LEADAUTHORPARENTPREFERENCE_"));
        }

        /// <summary>
        /// gets the opening xml for a new mSEG content control
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="bAddNewSegmentBookmark"></param>
        /// <param name="xDocVarID"></param>
        /// <returns></returns>
        private static string GetNewmSEGStartXML(string xSegmentID, bool bAddNewSegmentBookmark,
            out string xDocVarID)
        {
            const string mpCCNodeStart = "<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr><w:sdtContent>";
            //GLOG 15872: Make sure each generated ID is unique
            //generate new document variable id
            xDocVarID = LMP.Conversion.GenerateDocVarID();

            //construct tag string
            string xTag = "mps" + xDocVarID + xSegmentID + "000000000";
            string xCCNodeStart = string.Format(mpCCNodeStart, xTag);

            //bookmark start of content control range if specified
            if (bAddNewSegmentBookmark)
                xCCNodeStart = string.Concat(xCCNodeStart, NEW_SEGMENT_BMK_OPENXML);

            return xCCNodeStart;
        }

        /// <summary>
        /// adds the specified document variable to the supplied xml
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        private static void AddDocVarToSegmentXML(ref string xXML, string xName, string xValue)
        {
            const string mpDocVarNode = "<w:docVar w:name=\"{0}\" w:val=\"{1}\"/>";

            //look for existing <w:docVars> node - if none, add to end of <w:settings>
            int iPos = xXML.IndexOf("</w:docVars>");
            if (iPos == -1)
            {
                iPos = xXML.IndexOf("</w:settings>");
                xXML = xXML.Insert(iPos, "<w:docVars></w:docVars>");
                iPos = iPos + 11;
            }

            //insert doc var
            xXML = xXML.Insert(iPos, string.Format(mpDocVarNode, xName, xValue));
        }
        /// <summary>
        /// fills an ordered dictionary with the
        /// child segment structure of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oStructure"></param>
        protected override void GetChildCollectionTableStructure(ref CollectionTableStructure oStructure)
        {
            int iRowIndex = 0; //GLOG 6361 (dm)

            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oChild = this.Segments[i];

                //GLOG 8373: Don't include Child segments that have been deleted from database as part of structure
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oTest = null;
                try
                {
                    oTest = (AdminSegmentDef)oDefs.ItemFromID(oChild.ID1);
                }
                catch
                {
                    continue;
                }
                if (oTest == null || oTest.TypeID != oChild.TypeID)
                    continue;

                if (oChild.Segments.Count > 0)
                {
                    //recurse
                    oChild.GetChildCollectionTableStructure(ref oStructure);
                }

                //limit child structure to table collections and
                //table collection items, as these are the only children
                //whose location we can know on re-insertion
                if (oChild is CollectionTableItem || oChild is Paper)
                {
                    //get child segment prefill
                    Prefill oPrefill = new Prefill(oChild);

                    //GLOG 6361 (dm) - append location of collection table items -
                    //this will allow us to recreate in same position
                    //GLOG 7848: Dictionary key now in this format:
                    //<FullTagID>|<SegmentID>|<ObjectTypeID>|<Location>
                    string xKey = oChild.FullTagID + "|" + oChild.ID + "|" + (int)oChild.TypeID;
                    if (oChild is CollectionTableItem)
                    {
                        CollectionTableStructure.ItemInsertionLocations iLoc =
                            CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                        if (((CollectionTableItem)oChild).AllowSideBySide)
                        {
                            Word.Cell oCell = oChild.PrimaryRange.Cells[1];
                            if (oCell.ColumnIndex > 1)
                            {
                                iLoc = CollectionTableStructure.ItemInsertionLocations.RightCell;
                                if (oCell.RowIndex == iRowIndex)
                                    iLoc = iLoc | CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                            }
                            else
                            {
                                iLoc = CollectionTableStructure.ItemInsertionLocations.LeftCell;
                                //JTS 7/27/16: first item may have been on right side
                                if (oCell.RowIndex == iRowIndex)
                                    iLoc = iLoc | CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                            }
                            iRowIndex = oCell.RowIndex;
                        }
                        xKey = xKey + "|" + ((short)iLoc).ToString();
                    }
                    else
                        xKey = xKey + "|0"; //GLOG 7848: Use unspecified value for non Collection item location

                    oStructure.Dictionary.Add(xKey, oPrefill);

                    //GLOG 6002 (dm) - add sidebar here to maintain
                    //relationship with parent pleading paper
                    //GLOG 6519:  Support Letterhead or Pleading Paper as Sidebar parent
                    if (oChild is Paper) // if (oChild is PleadingPaper)
                    {
                        for (int j = 0; j < oChild.Segments.Count; j++)
                        {
                            Segment oGrandchild = oChild.Segments[j];
                            if (oGrandchild is Sidebar)
                            {
                                oPrefill = new Prefill(oGrandchild);
                                //GLOG 7848
                                oStructure.Dictionary.Add(oGrandchild.FullTagID + "|" + oGrandchild.ID + "|" + (int)oGrandchild.TypeID + "|0", oPrefill);
                                break;
                            }
                        }
                    }
                }
            }
        }
        internal void InsertCollectionTableStructure(Segment oTarget, CollectionTableStructure oStructure)
        {
            //GLOG 5944 (dm) - set flag that child structure is being inserted
            oTarget.ForteDocument.ChildStructureInsertionInProgess = true;

            //GLOG 5887 (dm) - raise event
            oTarget.ForteDocument.RaiseBeforeCollectionTableStructureInsertedEvent(oTarget);

            //GLOG 5909 (dm) - get any non-native mDels in collection tables -
            //we'll need to restore these later
            ListDictionary omDels = new ListDictionary();
            GetNonNativemDelsInCollectionTables(oTarget, ref omDels);

            //delete all table collection items -
            //we'll add back below the ones in the collection table structure
            Segment[] aCollectionTableItems = Segment.FindCollectionTableItemChildren(oTarget.Segments);

            //GLOG 7848: Track Object types that need updating
            Dictionary<mpObjectTypes, bool> oUpdateTypes = new Dictionary<mpObjectTypes, bool>();

            //GLOG 6002 (dm) - hang on to pleading paper as sidebar target -
            //since structure is flat, child sidebar will always follow
            //immediately in collection
            Segment oPPaper = null;

            for (int i = 0; i < aCollectionTableItems.Length; i++)
            {
                //GLOG 7848: Determine if item is a type that needs updating.
                //If not, just apply prefill to the existing item
                Segment oItem = aCollectionTableItems[i];
                mpObjectTypes oType = oItem.TypeID;
                if (oItem is Paper)
                    oPPaper = oItem;

                if (!oUpdateTypes.ContainsKey(oType))
                {
                    List<string> aSiblings =  oStructure.GetSegmentsOfSameType(aCollectionTableItems, oType);
                    List<string> aStructureSiblings = oStructure.GetStructureItemsOfSameType(oType);
                    //GLOG 8373: Don't mark item to be replaced if original segment being recreated no longer exists
                    if (!oStructure.ContainsChildOfType(oType) || aSiblings.SequenceEqual(aStructureSiblings))
                    {
                        //Existing collection is identical to that being recreated
                        oUpdateTypes.Add(oType, false);
                    }
                    else
                    {
                        oUpdateTypes.Add(oType, true);
                    }
                }
                //If existing and pre-existing collections aren't identical, 
                //Delete items to be reinserted
                if (oUpdateTypes[oType] == true)
                {
                    //GLOG 5962 (dm) - don't delete paper - deletion is built into
                    //paper class and we need existing paper to get the target sections
                    //if (!(oItem is LMP.Architect.Api.Paper))
                    //GLOG 6002 (dm) - need to exclude sidebar as well
                    if (oItem is LMP.Architect.Api.CollectionTableItem)
                    {
                        //GLOG 5909 (dm) - don't preserve non-native mDels here
                        oItem.ForteDocument.DeleteSegment(oItem, false, false, true);
                    }
                }
                else
                {
                    //Item already exists - just apply prefill
                    foreach (string xKey in oStructure.Dictionary.Keys)
                    {
                        string[] xProps = xKey.Split('|');
                        string xChildID = xProps[1];
                        if (xChildID.EndsWith(".0"))
                            xChildID = xChildID.Replace(".0", "");
                        if (xChildID == oItem.ID.Replace(".0", ""))
                        {
                            oItem.ApplyPrefill((Prefill)oStructure.Dictionary[xKey]);
                            break;
                        }
                    }
                }
            }


            //cycle through supplied child structure,
            //adding each of the collections items
            //in the structure
            foreach (string xKey in oStructure.Dictionary.Keys)
            {
                //GLOG 6361 (dm) - the insertion location for collection table items
                //is now appended to the key
                string[] xProps = xKey.Split('|');
                string xChildID = xProps[1]; //GLOG 7848
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xChildID.EndsWith(".0"))
                    xChildID = xChildID.Replace(".0", "");
                //GLOG 7848: Insert only if existing item was deleted above
                int iTypeID = Int32.Parse(xProps[2]);
                //GLOG 8373: Additional test not necessary
                //GLOG 8415
                //GLOG 8790: Insert Sidebar only if existing Sidebar was deleted above or pleading paper being replaced had no sidebar
                if (oUpdateTypes.ContainsKey((mpObjectTypes)iTypeID) && (oUpdateTypes[(mpObjectTypes)iTypeID] == true || 
                    ((mpObjectTypes)iTypeID == mpObjectTypes.Sidebar && oStructure.GetSegmentsOfSameType(aCollectionTableItems, mpObjectTypes.Sidebar).Count == 0)))
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();

                    //GLOG 8373: Avoid error if child segment has been deleted
                    AdminSegmentDef oDef = null;

                    try
                    {
                        oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xChildID));
                    }
                    catch
                    {
                        continue;
                    }


                    if (oDef.IsPaperType)
                    {
                        //GLOG 6002 (dm) - added support for sidebars
                        Segment oSeg = oTarget.ForteDocument.InsertSegment(oDef.ID.ToString(),
                             oTarget, Segment.InsertionLocations.InsertAtSelection,
                             Segment.InsertionBehaviors.Default, false, (Prefill)oStructure[xKey], null, false, false, null);
                        //GLOG 6519:  Recognize either Letterhead or Pleading Paper as Sidebar parent
                        if (oSeg is Paper) // if (oSeg is PleadingPaper)
                            oPPaper = oSeg;

                        //GLOG 5799 (dm)
                        oTarget.Refresh();
                    }
                    else if (oDef.TypeID == mpObjectTypes.Sidebar)
                    {
                        if (oPPaper != null)
                        {
                            //GLOG 6002 (dm) - added support for sidebars
                            oTarget.ForteDocument.InsertSegment(oDef.ID.ToString(),
                                 oPPaper, Segment.InsertionLocations.InsertAtSelection,
                                 Segment.InsertionBehaviors.Default, false, (Prefill)oStructure[xKey], null, false, false, null);
                        }
                    }
                    else
                    {
                        InsertTableCollectionItem(oTarget, oDef, (Prefill)oStructure[xKey], (CollectionTableStructure.ItemInsertionLocations)System.Int16.Parse(xProps[3])); //GLOG 7848

                        //GLOG 5909 (dm) - restore non-native mDels
                        LMP.Forte.MSWord.Tags oTags = oTarget.ForteDocument.Tags;
                        foreach (DictionaryEntry oEntry in omDels)
                        {
                            Segment oItem = Segment.GetSegment(oEntry.Key.ToString(),
                                oTarget.ForteDocument);
                            Word.Range oRng = null;

                            oRng = oItem.PrimaryRange;
                            LMP.Forte.MSWord.WordDoc.RestoremDels(oRng, oEntry.Value.ToString(), ref oTags);
                        }
                    }
                }
            }

            //GLOG 5887 (dm) - raise event
            oTarget.ForteDocument.RaiseAfterCollectionTableStructureInsertedEvent(oTarget);

            //GLOG 5944 (dm)
            oTarget.ForteDocument.ChildStructureInsertionInProgess = false;
        }
        private static Segment InsertTableCollectionItem(Segment oTargetSegment, AdminSegmentDef oDef, Prefill oPrefill, CollectionTableStructure.ItemInsertionLocations iLocation)
        {
            //GLOG 6361 (dm) - added iLocation parameter
            string xType = oDef.TypeID.ToString();
            mpObjectTypes oCollectionType = (mpObjectTypes)Enum.Parse(typeof(mpObjectTypes), xType + "s");

            //get collection segment
            Segment oParent = null;
            if ((oTargetSegment.TypeID == oCollectionType))
                //collection is oTargetSegment
                oParent = oTargetSegment;
            else
            {
                //get transparent child
                for (int i = 0; i < oTargetSegment.Segments.Count; i++)
                {
                    Segment oChild = oTargetSegment.Segments[i];
                    if (oChild.TypeID == oCollectionType)
                    {
                        oParent = oChild;
                        break;
                    }
                }
            }

            //GLOG : 7281 : CEH - No matching collection found
            if (oParent == null)
                return null;

            //pleading table items are assigned directly to pleadings
            Segment oTarget = oParent;
            if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                oTarget = oTarget.Parent;

            //GLOG 6361 (dm) - set insertion location
            ((CollectionTable)oParent).PendingInsertionLocation = (CollectionTableStructure.ItemInsertionLocations)iLocation;
            //GLOG 6783: Individual rows may not be accessible if table
            //contains merged cells, so select last character instead
            oParent.PrimaryRange.Characters.Last.Select();

            //insert
            Segment oNewSegment = null;

            //add to existing table
            oNewSegment = oTargetSegment.ForteDocument.InsertSegment(oDef.ID.ToString(),
                 oParent, Segment.InsertionLocations.InsertAtSelection,
                 Segment.InsertionBehaviors.Default, false, oPrefill, null, false, false, null);

            return oNewSegment;
        }
        /// <summary>
        /// creates a dictionary entry for each child collection table of oSegment
        /// that contains non-native mDels 
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="omDels"></param>
        private static void GetNonNativemDelsInCollectionTables(Segment oSegment,
            ref ListDictionary omDels)
        {
            LMP.Forte.MSWord.Tags oTags = oSegment.ForteDocument.Tags;
            Word.Range oRng = null;

            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];
                if (oChild is CollectionTable)
                {
                    if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
                    {
                        if (oChild.FirstWordTag != null)
                            oRng = oChild.FirstWordTag.Range;
                        else
                            oRng = oChild.FirstBookmark.Range;
                    }
                    else
                    {
                        if (oChild.FirstContentControl != null)
                            oRng = oChild.FirstContentControl.Range;
                        else
                            oRng = oChild.FirstBookmark.Range;
                    }
                    string xmDels = LMP.Forte.MSWord.WordDoc.GetmDels(oRng, ref oTags, -1,
                        false, false, false, oChild.TagID);
                    if (!string.IsNullOrEmpty(xmDels))
                        omDels.Add(oChild.FullTagID, xmDels);
                }

                GetNonNativemDelsInCollectionTables(oChild, ref omDels);
            }
        }
        public override bool PrepareForFinish()
        {
            //GLOG 8512: Run BeforeSegmentFinish actions if defined
            this.Actions.Execute(Segment.Events.BeforeSegmentFinish);

            //Raise BeforeFinish event
            if (this.BeforeFinish != null)
                this.BeforeFinish(this, new EventArgs());

            return base.PrepareForFinish();
        }
        #endregion
        #region *********************event handlers*********************
        private void m_oAuthors_AuthorAdded(object AuthorsCollection, AuthorAddedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for additional author
            if (m_bAllowUpdateForAuthor &&
                (this.CreationStatus >= Segment.Status.VariableDefaultValuesSet) &&
                (m_oAuthors.Count > 1))
                UpdateForAuthor(oArgs.NewIndex, -2, !m_bSkipAuthorActions);

            //update child segment authors
            UpdateChildSegmentAuthors();
        }
        private void m_oAuthors_AuthorsReindexed(object AuthorsCollection, AuthorsReindexedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for authors
            if (m_bAllowUpdateForAuthor && 
                this.CreationStatus >= Segment.Status.VariableDefaultValuesSet)
                UpdateForAuthor(-1, -2, !m_bSkipAuthorActions); //GLOG 6998: Update variables for all authors, not just LeadAuthor

            //update child segment authors
            UpdateChildSegmentAuthors();
        }
        private void m_oAuthors_LeadAuthorChanged(object AuthorsCollection, LeadAuthorChangedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for new lead author
            if (m_bAllowUpdateForAuthor && 
                this.CreationStatus >= Segment.Status.VariableDefaultValuesSet)
                UpdateForAuthor(-1, -2, !m_bSkipAuthorActions);

            //update child segment authors
           UpdateChildSegmentAuthors();
        }
        private void m_oVariables_BeforeContactsUpdateEvent(object sender, BeforeContactsUpdateEventArgs oArgs)
        {
            System.Windows.Forms.MessageBox.Show("About to update contacts.");
        }
        private void OnVariablesValidationChanged(object sender, LMP.Architect.Base.ValidationChangedEventArgs e)
		{
			RaiseValidationChangeIfNecessary(e);
		}
		private void oChildSegment_ValidationChange(object sender, LMP.Architect.Base.ValidationChangedEventArgs e)
		{
			RaiseValidationChangeIfNecessary(e);
        }
        private void oChildSegment_AfterAllVariablesVisited(object sender, EventArgs e)
        {
            //Check if event should also be raised for parent segment
            if (this.AllRequiredValuesVisited)
            {
                if (this.AfterAllVariablesVisited != null)
                    this.AfterAllVariablesVisited(this, new EventArgs());
            }
        }
        private void m_oChildSegments_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            this.ForteDocument.RaiseSegmentAddedEvent(oArgs.Segment);
        }
        private void m_oChildSegments_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            this.ForteDocument.RaiseSegmentDeletedEvent(oArgs.Segment);
        }
        private void OnVariableVisited(object sender, VariableEventArgs oArgs)
        {
            if (this.AllRequiredValuesVisited)
            {
                if (this.AfterAllVariablesVisited != null)
                    this.AfterAllVariablesVisited(this, new EventArgs());
            }
        }
        #endregion
    }
    
    /// <summary>
    /// defines a collection of Segments -
    /// contains methods to manage the collection-
    /// created by Dan Fisherman - 11/01/05
    /// </summary>
    public class Segments : NameObjectCollectionBase, IEnumerable
    {
        #region *********************events*********************
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        #endregion
        #region *********************fields*********************
        private ForteDocument m_oMPDocument;
        #endregion
        #region *********************constructors*********************
        public Segments(ForteDocument oMPDocument)
        {
            m_oMPDocument = oMPDocument;
        }
        #endregion
        #region *********************methods*********************
        public Segment ItemFromIndex(int iIndex)
        {
            return (Segment)base.BaseGet(iIndex);
        }
        
        public Segment ItemFromID(string xID)
        {
            return (Segment)base.BaseGet(xID);
        }

        public Segment this[int iIndex]
        {
            get { return ItemFromIndex(iIndex); }
        }
        public Segment this[string xID]
        {
            get
            {
                return ItemFromID(xID);
            }
        }
        public override int Count
        {
            get { return base.Count; }
        }

        public void Add(Segment oSegment)
        {
            base.BaseAdd(oSegment.FullTagID, oSegment);

            //raise event
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSegment));
        }

        public void Delete(string xID)
        {
            //get segment
            Segment oSegment = ItemFromID(xID);

            //remove from collection
            base.BaseRemove(xID);

            //raise event
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSegment));
        }
        public bool PrepareForFinish()
        {
            foreach (Segment oSegment in this.BaseGetAllValues())
            {
                if (!oSegment.PrepareForFinish())
                    return false;
            }

            return true;
        }
        public bool ExecutePostFinish()
        {
            foreach (Segment oSegment in this.BaseGetAllValues())
            {
                if (!oSegment.ExecutePostFinish())
                    return false;
            }

            return true;
        }
        public void Clear()
        {
            //run Delete() override in order to raise Deleted event for each segment
            //Start with last item, since Count will be reduced after each deletion
            for (int i = this.Count - 1; i >= 0; i--)
                Delete(this[i].FullTagID);
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// returns true iff all segments in the collection are valid
        /// </summary>
        public bool AreValid
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                    if (!this[i].IsValid)
                        //segment is invalid
                        return false;

                return true;
            }
        }
        /// <summary>
        /// Returns true if all 'Must Visit' variables of each segment have been visited
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                    if (!this[i].AllRequiredValuesVisited)
                        //segment is incomplete
                        return false;

                return true;

            }
        }
        /// <summary>
        /// returns the MacPac document containing this collection
        /// </summary>
        public ForteDocument ForteDocument
        {
            get { return this.m_oMPDocument; }
        }

        public bool ContentIsMissing
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].ContentIsMissing)
                        return true;
                }
                return false;
            }
        }

        #endregion
        #region *********************event handlers*********************
        #endregion

        #region IEnumerable Members

        //        IEnumerator IEnumerable.GetEnumerator()
        //        {
        //            throw new Exception("The method or operation is not implemented.");
        //        }

        //        #endregion

        //        private class SegmentEnumerator:IEnumerator{

        //    #region IEnumerator Members

        //public object  Current
        //{
        //    get { throw new Exception("The method or operation is not implemented."); }
        //}

        //public bool  MoveNext()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        public void Reset()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
