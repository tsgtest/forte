using System;
using System.Collections;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using LMP.Data;
using LMP.Architect.Base;

namespace LMP.Architect.Api
{
    /// <summary>
    /// declares the delegate for a SegmentRefreshedByAction event handler
    /// </summary>
    public delegate void SegmentRefreshedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentDistributedSectionsHandler(object sender, SegmentEventArgs oArgs);

	public class VariableAction:ActionBase
	{
		#region *********************fields*********************
		int m_iLookupListID = 0;
		VariableActions.Types m_oType = VariableActions.Types.InsertText;
		private Variable m_oVariable;
		#endregion
        #region *********************enumerations*********************
        public enum AlternateFormatTypes
        {
            None = 0,
            Columns = 1, 
            Table = 2
        }
        #endregion
		#region *********************constructors*********************
		internal VariableAction(Variable oVariable)
            :base()
		{
			m_oVariable = oVariable;
			base.m_oSegment = m_oVariable.Segment;
            base.m_oForteDocument = oVariable.ForteDocument;
            base.m_oDocument = oVariable.ForteDocument.WordDocument;
		}
		#endregion
		#region *********************properties*********************
		/// <summary>
		/// gets/sets the ID of the list used to map
        /// the variable value to another value
		/// </summary>
		public int LookupListID
		{
			get{return m_iLookupListID;}
			set
			{
				if(m_iLookupListID != value)
				{
					m_iLookupListID = value;
					this.IsDirty = true;
				}
			}
		}

		public VariableActions.Types Type
		{
			get{return m_oType;}
			set
			{
				if(m_oType != value)
				{
					m_oType = value;
					this.IsDirty = true;
				}
			}
		}
	
		public override bool IsValid
		{
			get
			{
				return true;
			}
		}

		#endregion
		#region *********************methods*********************
		/// <summary>
		/// returns the VariableAction as an array
		/// </summary>
		/// <returns></returns>
		public override string[] ToArray(bool bIncludeID)
		{
			string[] aAction;

			if(bIncludeID)
			{
				aAction = new string[6];
				aAction[0] = this.ID;
				aAction[1] = this.ExecutionIndex.ToString();
				aAction[2] = this.LookupListID.ToString();
				aAction[3] = this.ExecutionCondition;
				aAction[4] = ((int)this.Type).ToString();
				aAction[5] = this.Parameters;
			}
			else
			{
				aAction = new string[5];
				aAction[0] = this.ExecutionIndex.ToString();
				aAction[1] = this.LookupListID.ToString();
				aAction[2] = this.ExecutionCondition;
				aAction[3] = ((int) this.Type).ToString();
				aAction[4] = this.Parameters;
			}

			return aAction;
		}

        /// <summary>
        /// executes this action
        /// </summary>
        public override void Execute()
        {
            this.Execute(false);
        }

        /// <summary>
        /// executes this action
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scope will not be deleted when value is empty
        /// </param>
        public void Execute(bool bIgnoreDeleteScope)
		{
			DateTime t0 = DateTime.Now;

			string xParameters = this.Parameters;
			string xValue = m_oVariable.Value;

			Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if segment is deactivated (5/10/11, dm)
            if (!m_oSegment.Activated)
                return;

            //exit if condition for insertion is not satisfied
			if(!ExecutionIsSpecified())
				return;

			//get mapped value if mapping domain
			//(i.e. value set) is specified
			if(this.LookupListID > 0)
				xValue = LMP.Architect.Api.Expression.Lookup(
					xValue, this.LookupListID, m_oSegment, m_oForteDocument);

            if(xParameters != "")
			{
                string xParamValue = xValue;

                //GLOG 2151: Mark any reserved Expression characters before evaluating
                //They will treated as the literal characters by the actions
                xParamValue = Expression.MarkLiteralReservedCharacters(xParamValue);

				//evaluate [MyValue] first
                xParameters = FieldCode.EvaluateMyValue(xParameters, xParamValue);
			}

            //7-21-11 (dm) - disable events here rather than in the individual actions -
            //events were disabled for only a portion of each action, which didn't include
            //the call to SetDynamicEditingEnvironment, a potential trigger - additionally,
            //IgnoreWordXMLEvents was being set to None at the end of each action,
            //regardless of the starting value
            ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            try
            {
                switch (m_oType)
                {
                    case VariableActions.Types.InsertText:
                        InsertText(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.InsertDetail:
                        InsertDetail(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.InsertDetailIntoTable:
                        InsertDetailIntoTable(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.InsertReline:
                        InsertReline(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.InsertTextAsTable:
                        InsertTextAsTable(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.ExecuteOnBookmark:
                        ExecuteOnBookmark(xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnDocument:
                        ExecuteOnDocument(xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnApplication:
                        ExecuteOnApplication(xParameters);
                        break;
                    case VariableActions.Types.ReplaceSegment:
                        ReplaceSegment(xParameters);
                        break;
                    case VariableActions.Types.IncludeExcludeText:
                        IncludeExcludeText(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    //GLOG - 3395 - CEH
                    case VariableActions.Types.IncludeExcludeBlocks:
                        IncludeExcludeBlocks(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.SetVariableValue:
                        SetVariableValue(xValue, xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnPageSetup:
                        ExecuteOnPageSetup(xParameters);
                        break;
                    case VariableActions.Types.SetDocVarValue:
                        SetDocVarValue(xValue, xParameters);
                        break;
                    case VariableActions.Types.SetDocPropValue: //GLOG 7495
                        SetDocPropValue(xValue, xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnStyle:
                        ExecuteOnStyle(xParameters);
                        break;
                    case VariableActions.Types.InsertCheckbox:
                        InsertCheckbox(xValue, xParameters);
                        break;
                    case VariableActions.Types.RunMethod:
                        RunMethod(xParameters);
                        break;
                    case VariableActions.Types.RunMacro:
                        RunMacro(xParameters);
                        break;
                    case VariableActions.Types.InsertDate:
                        InsertDate(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.SetAsDefaultValue:
                        SetAsDefaultValue(xValue, xParameters);
                        break;
                    case VariableActions.Types.InsertSegment:
                        InsertSegment(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case VariableActions.Types.ExecuteOnTag:
                        ExecuteOnTag(m_oVariable, xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnSegment: //GLOG 7229
                        ExecuteOnSegment(xParameters);
                        break;
                    case VariableActions.Types.ExecuteOnBlock:
                        ExecuteOnBlock(xParameters);
                        break;
                    case VariableActions.Types.RunVariableActions:
                        RunVariableActions(xParameters);
                        break;
                    case VariableActions.Types.SetupLabelTable:
                        SetupLabelTable(xValue, xParameters);
                        break;
                    case VariableActions.Types.SetupDetailTable:
                        SetupDistributedDetailTable(xValue, xParameters);
                        break;
                    case VariableActions.Types.SetupDistributedSections:
                        SetupDistributedSegmentSections(xValue, xParameters);
                        break;
                    //case VariableActions.Types.InsertBarCode:
                    //    InsertBarCode(xParameters);
                    //    break;
                    case VariableActions.Types.ApplyStyleSheet:
                        ApplyStyleSheet(xValue, xParameters);
                        break;
                    case VariableActions.Types.SetPaperSource:
                        SetPaperSource(xValue, xParameters);
                        break;
                    //GLOG 3583
                    case VariableActions.Types.UnderlineToLongest:
                        UnderlineTagToLongest(m_oVariable, xParameters);
                        break;
                    case VariableActions.Types.InsertTOA:
                        InsertTOA(xValue, xParameters);
                        break;
                }

                //cycle through associated tags, executing embedded commands
                if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //xml tags
                    Word.XMLNode[] aNodes = this.m_oVariable.AssociatedWordTags;
                    if (aNodes.Length > 0)
                    {
                        foreach (Word.XMLNode oNode in aNodes)
                            LMP.Forte.MSWord.WordDoc.ExecuteEmbeddedCommands(oNode.Range);
                    }
                }
                else
                {
                    //content controls
                    Word.ContentControl[] aCCs = this.m_oVariable.AssociatedContentControls;
                    if (aCCs.Length > 0)
                    {
                        foreach (Word.ContentControl oCC in aCCs)
                            LMP.Forte.MSWord.WordDoc.ExecuteEmbeddedCommands(oCC.Range);
                    }
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_VariableActionFailed") +
                    m_oVariable.Name, oE);
            }
            finally
            {
                //7-21-11 (dm) - restore event handling
                ForteDocument.IgnoreWordXMLEvents = oEvents;
            }

			LMP.Benchmarks.Print(t0, 
				Convert.ToString(m_oType), "Variable=" + this.m_oVariable.Name, "Value=" + xValue, "Parameters=" + xParameters);
		}

		/// <summary>
		/// returns true iff execution condition is met or if there is no execution condition
		/// </summary>
		/// <returns></returns>
		public override bool ExecutionIsSpecified()
		{
			string xExecutionCondition = this.ExecutionCondition;

			Trace.WriteNameValuePairs("xExecutionCondition", xExecutionCondition);

			//return true if no execution condition
            if (xExecutionCondition == "")
            {
                ////execute master detail actions only after the segment has been created
                //if (m_oVariable.IsMasterDetailVariable && this.Type == VariableActions.Types.RunVariableActions)
                //{
                //    return m_oVariable.Segment.CreationStatus == Segment.Status.Finished;
                //}
                //else
                //{
                    return true;
                //}
            }

			//determine if there is a [MyValue] code in the execution condition string
			bool bMyValueExists = xExecutionCondition.IndexOf(Expression.mpMyValueCode) > -1;

			//evaluate condition
			if(bMyValueExists)
				//replace all [MyValue] codes with control value
                xExecutionCondition = FieldCode.EvaluateMyValue(
                    xExecutionCondition, m_oVariable.Value.ToString());

			//evaluate resulting execution condition
			string xRet = Expression.Evaluate(xExecutionCondition, 
                m_oSegment, this.m_oVariable.ForteDocument);

			bool bRet = false;

			try
			{
				//convert to boolean
				bRet = String.ToBoolean(xRet);
			}
			catch
			{
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_InvalidExecutionCondition") + 
					xExecutionCondition);
			}
			return bRet;
		}
		#endregion
		#region *********************private members*********************
        /// <summary>
        /// inserts xValue at tag containing the definition for this variable -
        /// if xDelimReplacement is not supplied, replaces char 10/13 with char 11 -
        /// xExp overrides xValue if supplied -
        /// applies formatting per any embedded formatting tokens
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xDelimReplacement, shUnderlineLength, iDeleteScope, xExp</param>
        private void InsertText(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
            string xDelimReplacement = "";
            short shUnderlineLength = 0;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 4)
                //4 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xDelimReplacement = Expression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument);
                    if (xDelimReplacement == "Null")
                        //keyword specifies that line breaks should simply be removed
                        xDelimReplacement = "";
                    else if (xDelimReplacement == "")
                        xDelimReplacement = "\v";

                    shUnderlineLength = short.Parse(Expression.Evaluate(aParams[1],
                        m_oSegment, m_oForteDocument));
                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                    xExpression = aParams[3];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xDelimReplacement",
                xDelimReplacement, "shUnderlineLength", shUnderlineLength, "iDeleteScope",
                iDeleteScope, "xExpression", xExpression);

            if (xExpression != "")
            {
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
            }

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;

            m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            //set argument values
            Object oNodesArray = null;
            bool bIsSegNode = false;
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);
            if (!bContentControls)
            {
                //xml tags
                Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;

                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (oNodes[0].BaseName == "mSEG");
                }
            }
            else
            {
                //content controls
                Word.ContentControl[] oNodes = m_oVariable.ContainingContentControls;

                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oNodes[0].Tag) == "mSEG");
                }
            }

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xValue == "") && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || ((xValue == "") && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }

            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            object missing = System.Reflection.Missing.Value;

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert text
            bool bRefreshRequired = false;
            try
            {
                if (m_oVariable.IsMultiValue)
                {
                    //insert text at each node
                    if (!bContentControls)
                    {
                        //xml tags
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText_Distributed(m_oDocument, ref oNodesArray,
                            xValue, xDelimReplacement, shUnderlineLength, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref missing);
                    }
                    else
                    {
                        //content controls
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText_Distributed_CC(m_oDocument, ref oNodesArray,
                            xValue, xDelimReplacement, shUnderlineLength, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref missing);
                    }
                }
                else if (!bIsSegNode)
                {
                    //insert text at each node
                    if (!bContentControls)
                    {
                        //xml tags
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText(m_oDocument, ref oNodesArray,
                            xValue, xDelimReplacement, shUnderlineLength, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref missing, m_oVariable.ValueSourceExpression);
                    }
                    else
                    {
                        //content controls
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText_CC(m_oDocument, ref oNodesArray,
                            xValue, xDelimReplacement, shUnderlineLength, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref missing, m_oVariable.ValueSourceExpression);
                    }
                }
                else
                {
                    //restore scope and insert text for each mDel found
                    LMP.Forte.MSWord.WordDoc.InsertText_RestoreScope(m_oDocument, ref oNodesArray, xValue,
                        xDelimReplacement, shUnderlineLength, iDeleteScope, xSegmentXML,
                        m_oVariable.Name, oInsertionLocation, ref xVariablesInScope,
                        ref oTags);
                }
            }
            catch (System.Exception oE)
            {
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw oE;
                }
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);

                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            if (bExpandedScope)
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = ((xValue == "") && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh node store
                m_oSegment.RefreshNodes();

            //update tag type
            if (m_oVariable.IsMultiValue)
            {
                string xTagType = m_oSegment.Nodes.GetItemElementName(m_oVariable.TagID);
                if (xTagType == "mVar")
                    m_oVariable.TagType = ForteDocument.TagTypes.Variable;
                else if (xTagType == "mDel")
                    m_oVariable.TagType = ForteDocument.TagTypes.DeletedBlock;
                else
                    m_oVariable.TagType = ForteDocument.TagTypes.Mixed;
            }
        }

        /// <summary>
        /// inserts the specified SubVar-related detail
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertDetail(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            //GLOG 7824: Run this action for Labels and Envelopes in Design only
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment && m_oForteDocument.Mode != ForteDocument.Modes.Design)
            {
                return;
            }

            string xDetailItemSeparator = "";
            string xEntitySeparator = "";
            string xTemplate = "";
            string xInsertXML = "";
            string xSegmentXML = "";
            string xVariablesInScope = "";
            short shUnderlineLength = 0;
            int iThreshold = 0;
            object missing = System.Reflection.Missing.Value;
            AlternateFormatTypes iAlternateFormat = AlternateFormatTypes.None;
            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            string xExpression = "";
            Variable oVarSource = null;
            Word.Range oInsertionLocation = null;
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
            Word.View oView = null;
            ArrayList aXML = null;
            string[] xDocVarsArray = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //determine whether current document is using content controls
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 7)
                //7 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xDetailItemSeparator = Expression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument);

                    xEntitySeparator = Expression.Evaluate(aParams[1],
                        m_oSegment, m_oForteDocument);

                    xTemplate = Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);

                    iAlternateFormat = (AlternateFormatTypes)Int32.Parse(aParams[3]);

                    iThreshold = Int32.Parse(aParams[4]);

                    //reset threshold to impossible value if zero
                    iThreshold = iThreshold == 0 ? iThreshold = -9999 : iThreshold;

                    shUnderlineLength = short.Parse(Expression.Evaluate(aParams[5],
                        m_oSegment, m_oForteDocument));

                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[6], m_oSegment, m_oForteDocument));
                    if (aParams.GetUpperBound(0) > 6)
                        xExpression = aParams[7];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xValue", xValue, "xDetailItemSeparator",
                    xDetailItemSeparator, "xEntitySeparator", xEntitySeparator, "iDeleteScope",
                    iDeleteScope, "xTemplate", xTemplate, "DetailType", iAlternateFormat, "Split Threshold", iThreshold,
                    "xExpression", xExpression);

                //Use a different variable or expression as source of value
                if (xExpression != "")
                {
                    //TODO: Originally, this paramater was a Variable name
                    //In order not to break existing segments, we check
                    //first to see if non-expression parameter is a valid variable name
                    //This can be removed, once any existing segments have been adjusted
                    if (!xExpression.Contains("["))
                    {
                        //get target segment(s)
                        Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xExpression);
                        if (oTargets != null)
                        {
                            foreach (Segment oTarget in oTargets)
                            {
                                //get target variable
                                oVarSource = oTarget.GetVariable(xExpression);

                                //if variable exists, set value
                                if (oVarSource != null)
                                {
                                    xValue = oVarSource.Value;
                                    break;
                                }
                            }
                        }
                        if (oVarSource == null)
                        {
                            xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
                        }
                    }
                    else
                        xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
                }

                //exit if value of assignment is mpUndefined
                if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                    return;

                //Convert xValue to XML Nodes, convert to insertable XML string
                try
                {
                    //get detail and entity separators
                    ModifyItemSeparatorsIfNecessary(aParams, ref xEntitySeparator, ref xDetailItemSeparator);

                    //Template is token reserved for empty value
                    //GLOG 2083: Recognize both old and new Fieldcode format
                    if (xTemplate.ToUpper() == "[SUBVAR_EMPTY]" || xTemplate.ToUpper() == "[SUBVAR__EMPTY]")
                        xValue = "";

                    if (!string.IsNullOrEmpty(xValue) && !xValue.StartsWith("<"))
                    {
                        string xXMLPart = "";
                        string xDelimPart = "";
                        int iXMLStart = xValue.IndexOf('<');
                        //GLOG 5034: Check whether '<' is start of appended XML portion, 
                        //or is instead part of text within delimited values
                        if (iXMLStart > -1 && xValue.EndsWith(">"))
                        {
                            //XML Value has been appended to delimited values -
                            //Convert Delimited Portion only
                            xXMLPart = xValue.Substring(iXMLStart);
                            xDelimPart = xValue.Substring(0, iXMLStart);
                        }
                        else
                            xDelimPart = xValue;
                        //specified value is a pipe delimited string -
                        //create xml from string
                        int i = 0;
                        string[] aValues = xDelimPart.Split('|');
                        string xSubVarName = "";
                        if (xTemplate != "")
                        {
                            //Get SubVar name from Template if defined
                            int iStart = xTemplate.IndexOf("__") + 2;
                            //Check for old format fieldcode
                            if (iStart == 1)
                                iStart = xTemplate.IndexOf("_") + 1;

                            int iEnd = xTemplate.IndexOf("]");
                            if (iStart > 0 && iEnd > -1 && (iEnd > iStart))
                                xSubVarName = xTemplate.Substring(iStart, iEnd - iStart);
                        }
                        if (xSubVarName == "")
                            xSubVarName = "FULLNAME";
                        StringBuilder oSB = new StringBuilder();
                        foreach (string aValue in aValues)
                        {
                            i++;
                            //GLOG 5034:  Replace any reserved XML characters within Detail values
                            oSB.AppendFormat("<{0} Index=\"{1}\" UNID=\"0\">{2}</{0}>",
                                xSubVarName, i.ToString(), LMP.String.ReplaceXMLChars(aValue)); //GLOG 6000
                        }
                        xValue = oSB.ToString();
                        //Reappend original XML portion
                        if (xXMLPart != "")
                            xValue = xValue + xXMLPart;
                    }


                    if (xValue != "")
                    {
                        oXMLSource.LoadXml("<zzmpD>" + xValue + "</zzmpD>");

                        //select detail nodes in XML
                        XmlNodeList oNodeList = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");

                        string xOpeningXML = LMP.String.GetMinimalOpeningXML(bContentControls);
                        string xClosingXML = LMP.String.GetMinimalClosingXML(bContentControls);

                        //create ArrayList whose members are XML detail elements and appropriate separators -
                        //10.2 (dm) - added parameter for the doc vars
                        aXML = GetSubXMLArray(oNodeList, xTemplate, xEntitySeparator,
                            xDetailItemSeparator, xOpeningXML, ref xDocVarsArray,
                            LMP.Architect.Api.Application.CurrentWordApp.Options.AutoFormatAsYouTypeReplaceQuotes); //GLOG 6065: Format for Smart Quotes if appropriate

                        //process ArrayList and return XML string ready for insertion
                        xInsertXML = GetXMLStringFromElementList(aXML, iAlternateFormat, iThreshold,
                            xEntitySeparator, xTemplate, xOpeningXML, xClosingXML);
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.XMLException(
                        LMP.Resources.GetLangString("Error_InvalidDetailXML") +
                        xParameters, oE);
                }
            }

            //get current xml markup state for later restoration
            oView = this.m_oForteDocument.WordDocument.ActiveWindow.View;

            int iShowTags = oView.ShowXMLMarkup;

            //set editing environment
            m_oForteDocument.SetDynamicEditingEnvironment();

            bool bIsSegNode = false;
            if (bContentControls)
            {
                if (m_oVariable.ContainingContentControls != null)
                {
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(
                        m_oVariable.ContainingContentControls[0].Tag) == "mSEG");
                }
                else
                {
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(
                        m_oVariable.ContainingBookmarks[0].Name.Substring(1, 3)) == "mSEG");
                }
            }
            else
            {
                if (m_oVariable.ContainingWordTags != null)
                {
                    bIsSegNode = (m_oVariable.ContainingWordTags[0].BaseName == "mSEG");
                }
                else
                {
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(
                        m_oVariable.ContainingBookmarks[0].Name.Substring(1, 3)) == "mSEG");
                }
            }

            //exit if value is empty and scope is already deleted
            bool bIsEmpty = (xInsertXML.IndexOf("<w:t>") == -1);
            if (bIsSegNode && bIsEmpty && !bIgnoreDeleteScope &&
                !m_oVariable.IsMultiValue)
                return;

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || (bIsEmpty && (shUnderlineLength == 0))));

            //get segment xml
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            if (iAlternateFormat == AlternateFormatTypes.Table &&
                iThreshold > 0 && m_oVariable.IsMultiValue)
            {
                int iDetailCount = 1;
                if (aXML != null)
                    iDetailCount = aXML.Count;
                bool bRefresh = false;

                if (bContentControls)
                {
                    //content controls
                    bRefresh = LMP.Forte.MSWord.WordDoc.SetupDistributedDetailTable_CC(
                        m_oVariable.AssociatedContentControls[0], iDetailCount,
                        ref oTags, 1, false, 0, 0, 0, iThreshold);
                }
                else
                {
                    //xml tags
                    bRefresh = LMP.Forte.MSWord.WordDoc.SetupDistributedDetailTable(
                        m_oVariable.AssociatedWordTags[0], iDetailCount,
                        ref oTags, 1, false, 0, 0, 0, iThreshold);
                }

                //GLOG 5027: RefreshTags should never run when saving Design
                if (this.m_oForteDocument.Mode != ForteDocument.Modes.Design && bRefresh)
                {
                    //Refresh since Nodes were added or removed
                    m_oForteDocument.RefreshTags();
                    m_oSegment.RefreshNodes();
                }
            }
            else if (iAlternateFormat == AlternateFormatTypes.Columns && iThreshold > 0 &&
                m_oVariable.IsMultiValue)
            {
                int iDetailCount = 1;
                if (aXML != null)
                    iDetailCount = aXML.Count;
                bool bRefresh = false;
                int iLength = 0;

                if (bContentControls)
                    iLength = m_oVariable.AssociatedContentControls.GetLength(0);
                else
                    iLength = m_oVariable.AssociatedWordTags.GetLength(0);

                if (iDetailCount >= iThreshold && iLength == 1)
                {
                    //restore delete scope before setting up columns - GLOG 2953 (8/19/08)
                    if (bIsSegNode)
                    {
                        object oSegNodesArray = null;

                        if (bContentControls)
                            oSegNodesArray = m_oVariable.ContainingContentControls;
                        else
                            oSegNodesArray = m_oVariable.ContainingWordTags;

                        LMP.Forte.MSWord.WordDoc.InsertText_RestoreScope(m_oDocument, ref oSegNodesArray, "", "",
                            0, iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref xVariablesInScope, ref oTags);
                        RefreshVariables(xVariablesInScope, false);
                        m_oVariable.TagType = ForteDocument.TagTypes.Variable;
                    }

                    if (bContentControls)
                    {
                        LMP.Forte.MSWord.WordDoc.SetupDistributedDetailColumns_CC(
                            m_oVariable.AssociatedContentControls[0],
                            LMP.Forte.MSWord.mpDetailColumnActions.ActionSplitCell);
                    }
                    else
                    {
                        LMP.Forte.MSWord.WordDoc.SetupDistributedDetailColumns(
                            m_oVariable.AssociatedWordTags[0],
                            LMP.Forte.MSWord.mpDetailColumnActions.ActionSplitCell);
                    }
                    bRefresh = true;
                }
                else if (iDetailCount < iThreshold && iLength == 2)
                {
                    if (bContentControls)
                    {
                        LMP.Forte.MSWord.WordDoc.SetupDistributedDetailColumns_CC(
                            m_oVariable.AssociatedContentControls[0],
                            LMP.Forte.MSWord.mpDetailColumnActions.ActionJoinCells);
                    }
                    else
                    {
                        LMP.Forte.MSWord.WordDoc.SetupDistributedDetailColumns(
                            m_oVariable.AssociatedWordTags[0],
                            LMP.Forte.MSWord.mpDetailColumnActions.ActionJoinCells);
                    }
                    bRefresh = true;
                }
                //GLOG 5027: RefreshTags should never run when saving Design
                if (this.m_oForteDocument.Mode != ForteDocument.Modes.Design && bRefresh)
                {
                    //Refresh since Nodes were added or removed
                    m_oForteDocument.RefreshTags();
                    m_oSegment.RefreshNodes();
                }
            }

            //update variable following RefreshTags()
            oTags = m_oForteDocument.Tags;

            //get nodes array
            Object oNodesArray = null;
            int iAssoTags = 0;
            if (bContentControls)
            {
                //content controls
                Word.ContentControl[] oCCs = null;
                if (m_oVariable.IsMultiValue)
                {
                    oCCs = m_oVariable.AssociatedContentControls;
                    oNodesArray = oCCs;
                    iAssoTags = oCCs.Length;
                }
                else
                {
                    oCCs = m_oVariable.ContainingContentControls;

                    if (oCCs != null)
                    {
                        oNodesArray = oCCs;
                        iAssoTags = oCCs.Length;
                    }
                    else
                    {
                        Word.Bookmark[] oBmks = m_oVariable.ContainingBookmarks;
                        oNodesArray = oBmks;
                        iAssoTags = oBmks.Length;
                    }
                }
            }
            else
            {
                //xml tags
                Word.XMLNode[] oNodes = null;
                if (m_oVariable.IsMultiValue)
                {
                    oNodes = m_oVariable.AssociatedWordTags;
                    oNodesArray = oNodes;
                    iAssoTags = oNodes.Length;
                }
                else
                {
                    oNodes = m_oVariable.ContainingWordTags;

                    if (oNodes != null)
                    {
                        oNodesArray = oNodes;
                        iAssoTags = oNodes.Length;
                    }
                    else
                    {
                        Word.Bookmark[] oBmks = m_oVariable.ContainingBookmarks;
                        oNodesArray = oBmks;
                        iAssoTags = oBmks.Length;
                    }
                }
            }

            //load values array
            string[] oValues;
            if (iAssoTags > 1 && m_oVariable.IsMultiValue && xValue != "")
            {
                oValues = new string[iAssoTags];
                for (int i = 0; i < iAssoTags; i++)
                {
                    //if using Multiple Values, each node will get one detail item
                    oValues[i] = GetXMLStringFromElementList(aXML, iAlternateFormat, iThreshold,
                        xEntitySeparator, xTemplate, LMP.String.GetMinimalOpeningXML(bContentControls),
                        LMP.String.GetMinimalClosingXML(bContentControls), i);
                }
            }
            else
                //otherwise, all nodes get the full value
                oValues = new string[] { xInsertXML };
            Object oValuesArray = oValues;

            //10.2
            if (xDocVarsArray == null)
                xDocVarsArray = new string[1] { "" };
            object oDocVarsArray = xDocVarsArray;

            //insert xml
            bool bRefreshRequired = false;


            try
            {
                if (m_oVariable.IsMultiValue)
                {
                    if (bContentControls && (m_oVariable.TagType == ForteDocument.TagTypes.Variable))
                    {
                        //(dm) added this branch for 10.2 - I realized that there's no need to
                        //run the backend code that determines whether to restore deleted scopes when
                        //we know that there are none - we should make the same change for xml tags,
                        //but I don't dare to do it as we're getting ready to release 10.1.2
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertXML_CC(m_oDocument, ref oNodesArray,
                            ref oValuesArray, shUnderlineLength, LMP.Forte.MSWord.mpRelineFormatOptions.RelineFormatNone,
                            iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation, true,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref oDocVarsArray, ref missing);
                    }
                    else
                    {
                        //insert xml at each node, restoring scope where necessary
                        LMP.Forte.MSWord.WordDoc.InsertXML_Distributed(m_oDocument, ref oNodesArray, ref oValuesArray,
                            shUnderlineLength, LMP.Forte.MSWord.mpRelineFormatOptions.RelineFormatNone, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation, ref bIgnoreDeleteScope,
                            ref xVariablesInScope, ref oTags, bContentControls, ref oDocVarsArray);
                        bRefreshRequired = true;
                    }
                }
                else if (!bIsSegNode)
                {
                    //insert xml at each node
                    if (bContentControls)
                    {
                        //content controls
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertXML_CC(m_oDocument, ref oNodesArray,
                            ref oValuesArray, shUnderlineLength, LMP.Forte.MSWord.mpRelineFormatOptions.RelineFormatNone,
                            iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation, false,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref oDocVarsArray, ref missing);
                    }
                    else
                    {
                        //xml tags
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertXML(m_oDocument, ref oNodesArray,
                            ref oValuesArray, shUnderlineLength, LMP.Forte.MSWord.mpRelineFormatOptions.RelineFormatNone,
                            iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation, false,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags, ref oDocVarsArray,
                            ref missing);
                    }
                }
                else
                {
                    //restore scope and insert xml for each mDel found
                    LMP.Forte.MSWord.WordDoc.InsertXML_RestoreScope(m_oDocument, ref oNodesArray,
                        ref oValuesArray, shUnderlineLength, LMP.Forte.MSWord.mpRelineFormatOptions.RelineFormatNone,
                        iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                        ref xVariablesInScope, ref oTags, bContentControls, ref oDocVarsArray);
                }
            }
            catch (System.Exception oE)
            {
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw;
                }
            }
            finally
            {
                if (iShowTags == 0)
                    //hide tags, as that was the original state
                    //of the tags before editing
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);

                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            if ((bExpandedScope) || (m_oVariable.IsMultiValue))
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = (bIsEmpty && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh node store
                m_oSegment.RefreshNodes();

            //update tag type
            if (m_oVariable.IsMultiValue)
            {
                string xTagType = m_oSegment.Nodes.GetItemElementName(m_oVariable.TagID);
                if (xTagType == "mVar")
                    m_oVariable.TagType = ForteDocument.TagTypes.Variable;
                else if (xTagType == "mDel")
                    m_oVariable.TagType = ForteDocument.TagTypes.DeletedBlock;
                else
                    m_oVariable.TagType = ForteDocument.TagTypes.Mixed;
            }
        }
        /// <summary>
        /// inserts the specified SubVar-related detail into a table
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertDetailIntoTable(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment) //GLOG 7127
            {
                return;
            }

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //Discard any non-XML at start of value
            int iXMLStartPos = xValue.IndexOf("<");
            if (iXMLStartPos == -1)
                xValue = "";
            else
                xValue = xValue.Substring(iXMLStartPos);

            string xTemplate = "";
            int iFirstDetailRow = 1;

            object missing = System.Reflection.Missing.Value;
            XmlDocument oXMLSource = new System.Xml.XmlDocument();

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2){
                //5 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            }

            try
            {
                xTemplate = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                iFirstDetailRow = int.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters, oE);
            }

            Trace.WriteNameValuePairs("xTemplate", xTemplate, 
                "iFirstDetailRow", iFirstDetailRow);

            //set editing environment
            m_oForteDocument.SetDynamicEditingEnvironment();

            //set argument values
            //TODO: 10.2 - branch for cc support
            Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;

            int iNodes = oNodes.Length;

            if (iNodes == 0)
                throw new LMP.Exceptions.VariableException(
                    LMP.Resources.GetLangString("NoWordTagsFoundForVariableAction"));

            //get the xml of the node
            string xNodeXml = oNodes[0].get_XML(false);

            //ensure that the xml contains a table
            int iPosStart = xNodeXml.IndexOf("<w:tbl>");

            if (iPosStart == 0)
                //xml doesn't contain a table
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString("NoTableInXml"));

            int iPosEnd = xNodeXml.IndexOf("</w:tbl>") + 8;

            string xOrigTableXML = xNodeXml.Substring(iPosStart, iPosEnd - iPosStart);

            //remove all rows starting with the FirstDetailRow - 
            //find the nth instance of the <w:tr> tag
            int iPos = 0;
            for (int i = 1; i <= iFirstDetailRow; i++)
                iPos = xOrigTableXML.IndexOf("<w:tr ", iPos + 1);

            if (iPos == -1)
                //first detail row doesn't currently exist in table -
                //take whole table, minus end tag
                xOrigTableXML = xOrigTableXML.Substring(0, xOrigTableXML.Length - 8);
            else
                //take table up to first detail row
                xOrigTableXML = xOrigTableXML.Substring(0, iPos);

            oXMLSource.LoadXml(string.Concat("<zzmpD>", xValue, "</zzmpD>"));

            //select detail nodes in XML
            XmlNodeList SourceXMLNodes = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");

            string xOpeningXML = LMP.String.GetMinimalOpeningXML(false);

            //get table xml to insert
            string xTableRowXML = GetTableRowXML(xValue, xTemplate, 1);
            string xTableXML = xOpeningXML + xOrigTableXML + xTableRowXML + 
                "</w:tbl>" + LMP.String.GetMinimalClosingXML(false);

            //insert detail into table
            try
            {
                //disable event handler that prevents deletion of mDel tags
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                object oMissing = System.Reflection.Missing.Value;

                foreach (Word.XMLNode oNode in oNodes)
                    LMP.Forte.MSWord.WordDoc.ReplaceTableInTag(oNode, xTableXML);
                //Word may have deleted and recreate original Nodes
                //so we need to refresh the nodestore
                //GLOG 6687: Make sure Tag Prefix ID isn't included when saving Design XML
                this.m_oForteDocument.RefreshTags(this.m_oForteDocument.Mode != ForteDocument.Modes.Design);
                this.m_oSegment.RefreshNodes();
            }
            catch(System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
                LMP.Benchmarks.Print(t0, m_oVariable.Name);
            }
        }

        /// <summary>
        /// Sets up label table with appropriate cells populated with tags
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void SetupLabelTable(string xValue, string xParameters)
        {
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment) //GLOG 7127
            {
                //don't run any setup of sections for envelopes, as the new (10.5.1) functionality
                //uses Word's merge feature to create the appropriate number of envelope sections
                return;
            }

            //GLOG 6687:  Don't ever run this action in Design Mode
            if (m_oForteDocument.Mode == ForteDocument.Modes.Design)
                return;

            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            int iStartPos = 1;
            bool bFullPageOfSame = false;
            int iLabelCount = 0;
            string xVariableTarget = "";

            Variable oVarTarget = null;


            object missing = System.Reflection.Missing.Value;
            XmlDocument oXMLSource = new System.Xml.XmlDocument();

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 3)
                //3 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableTarget = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    try
                    {
                        iStartPos = int.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    }
                    catch
                    {
                        //Default to 1
                        iStartPos = 1;
                    }
                    if (aParams[2] != "")
                    {
                        try
                        {
                            bFullPageOfSame = bool.Parse(Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                        }
                        catch
                        {
                            bFullPageOfSame = false;
                        }
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xValue", xValue, "xVariableTarget",
                    xVariableTarget, "iStartPos", iStartPos, "bFullPageOfSame", bFullPageOfSame);
            }
            if (xVariableTarget != "")
            {
                //Different variable holds the target tag
                Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableTarget);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVarTarget = oTarget.GetVariable(xVariableTarget);

                        //stop after finding match
                        if (oVarTarget != null)
                        {
                            break;
                        }
                    }
                }
                if (oVarTarget == null)
                {
                    return;
                }
            }
            else
                oVarTarget = m_oVariable;

            //get current xml markup state for later restoration
            Word.View oView = this.m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            //set editing environment
            m_oForteDocument.SetDynamicEditingEnvironment();

            if (bFullPageOfSame)
            {
                //Marker for full page of same
                iLabelCount = -1;
            }
            else
                iLabelCount = String.GetXMLEntityCount(xValue, null);

            //Setup table for labels
            try
            {
                //10.2 - JTS 4/2/10
                bool bContentControls = (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML);
                Word.Range oRng = null;

                if (bContentControls)
                    oRng = oVarTarget.AssociatedContentControls[0].Range;
                else
                    oRng = oVarTarget.AssociatedWordTags[0].Range;

                //disable event handler that prevents deletion of mDel tags
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                LMP.Forte.MSWord.WordDoc.SetupLabelTable(oRng, iLabelCount, iStartPos, bContentControls);

                ////Word may have deleted and recreated original Nodes
                ////so we need to refresh the nodestore
                //GLOG 6687:  Make sure TagPrefixID isn't included in Design XML
                this.m_oForteDocument.RefreshTags(this.m_oForteDocument.Mode != ForteDocument.Modes.Design);

                this.m_oSegment.RefreshNodes();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                if (iShowTags == 0)
                    //hide tags, as that was the original state
                    //of the tags before editing
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// Sets up service list table with appropriate cells populated with tags
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void SetupDistributedDetailTable(string xValue, string xParameters)
        {
            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            int iDetailCount = 0;
            int iStartPos = 1;
            int iHeaderRows = 0;
            int iMaxRows = 0;
            int iMinRows = 0;
            bool bFullRow = false;
            string xVariableTarget = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;

            Variable oVarTarget = null;

            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 6)
                //6 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableTarget = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    iStartPos = Int32.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    bFullRow = bool.Parse(Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                    iHeaderRows = Int32.Parse(Expression.Evaluate(aParams[3], m_oSegment, m_oForteDocument));
                    if (aParams[4] != "")
                        iMinRows = Int32.Parse(Expression.Evaluate(aParams[4], m_oSegment, m_oForteDocument));
                    if (aParams[5] != "")
                        iMaxRows = Int32.Parse(Expression.Evaluate(aParams[5], m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xVariableTarget", xVariableTarget, "xValue", xValue, 
                    "iStartPos", iStartPos, "bFullRow", bFullRow, "iHeaderRows", iHeaderRows);
            }

            if (xVariableTarget != "")
            {
                //Different variable holds the target tag
                Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableTarget);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVarTarget = oTarget.GetVariable(xVariableTarget);

                        //stop after finding match
                        if (oVarTarget != null)
                        {
                            break;
                        }
                    }
                }
                if (oVarTarget == null)
                {
                    return;
                }

                //GLOG 3456 (dm) - if this an mDel, reinsert the table before proceeding
                if (oVarTarget.TagType == ForteDocument.TagTypes.DeletedBlock)
                {
                    //get names of subvariables from RunVariableActions parameter
                    string[] aSubVariables = null;
                    for (int i = 0; i < m_oVariable.VariableActions.Count; i++)
                    {
                        VariableAction oAction = m_oVariable.VariableActions[i];
                        if (oAction.Type == VariableActions.Types.RunVariableActions)
                        {
                            string[] aRVParams = oAction.Parameters.Split(
                                LMP.StringArray.mpEndOfSubField);
                            aSubVariables = aRVParams[0].Split(',');
                            break;
                        }
                    }

                    if (aSubVariables != null)
                    {
                        //reinsert the table by executing the target variable's actions
                        //with the parameter to leave the delete scope when value is empty
                        oVarTarget.VariableActions.Execute(true);

                        //temporarily clear the value of the master variable
                        Variable oMasterVariable = m_oSegment.Variables.ItemFromName(
                            m_oVariable.Name);
                        oMasterVariable.SetValue("", false);

                        //execute actions of other variables to clear values
                        foreach (string xSubVariable in aSubVariables)
                        {
                            if (xSubVariable != oVarTarget.Name)
                            {
                                Variable oSubVariable = null;
                                try
                                {
                                    oSubVariable = m_oSegment.Variables.ItemFromName(
                                        xSubVariable);
                                }
                                catch { }
                                if (oSubVariable != null)
                                    oSubVariable.VariableActions.Execute(true);
                            }
                        }

                        //restore value of master variable
                        oMasterVariable.SetValue(xValue, false);
                    }
                }
            }
            else
                oVarTarget = m_oVariable;

            //Get count of Detail entities
            iDetailCount = String.GetXMLEntityCount(xValue, null);
            ////Get last Index to determine count of details in Value
            //int iPos = xValue.LastIndexOf("Index=\"");
            //if (iPos > -1)
            //{
            //    iPos = iPos + 7;
            //    //Find closing quote of Index attribute
            //    int iEndPos = xValue.IndexOf("\"", iPos);
            //    //Count is between the two positions
            //    iDetailCount = Int32.Parse(xValue.Substring(iPos, iEndPos - iPos));
            //}
            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            //set editing environment
            m_oForteDocument.SetDynamicEditingEnvironment();

            //Setup table for detail
            try
            {
                //disable event handler that prevents deletion of mDel tags
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                DateTime t2 = DateTime.Now;
                bool bRefreshNeeded = false;
                if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //xml tags
                    Word.XMLNode oNode = oVarTarget.AssociatedWordTags[0];
                    bRefreshNeeded = LMP.Forte.MSWord.WordDoc.SetupDistributedDetailTable(
                        oNode, iDetailCount, ref oTags, iStartPos, bFullRow, iHeaderRows,
                        iMinRows, iMaxRows, -9999);
                }
                else
                {
                    //content controls
                    Word.ContentControl oCC = oVarTarget.AssociatedContentControls[0];
                    bRefreshNeeded = LMP.Forte.MSWord.WordDoc.SetupDistributedDetailTable_CC(
                        oCC, iDetailCount, ref oTags, iStartPos, bFullRow, iHeaderRows,
                        iMinRows, iMaxRows, -9999);
                }
                LMP.Benchmarks.Print(t2, "SetupDistributedDetailTable");

                ////Build parameter string for InsertDetail action:
                ////xDetailItemSeparator|xEntitySeparator|xTemplate|iAlternateFormat|iThreshold|shUnderlineLength|iDeleteScope
                //string xInsertParams = xDetailItemSeparator + LMP.StringArray.mpEndOfSubField + "" +
                //    LMP.StringArray.mpEndOfSubField + xTemplate + LMP.StringArray.mpEndOfSubField + "0" +
                //    LMP.StringArray.mpEndOfSubField + "0" + LMP.StringArray.mpEndOfSubField +
                //    "0" + LMP.StringArray.mpEndOfSubField + ((int)LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target).ToString();

                if (bRefreshNeeded)
                {
                    ////Word may have deleted and recreated original Nodes
                    ////so we need to refresh the nodestore
                    DateTime t1 = DateTime.Now;
                    this.m_oForteDocument.RefreshTags(this.m_oForteDocument.Mode != ForteDocument.Modes.Design);
                    LMP.Benchmarks.Print(t1, "RefreshTags");

                    t1 = DateTime.Now;
                    this.m_oSegment.RefreshNodes();
                    LMP.Benchmarks.Print(t1, "RefreshNodes");
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                LMP.Benchmarks.Print(t0);
            }
        }
        private void SetupDistributedSegmentSections(string xValue, string xParameters)
        {
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment) //GLOG 7127
            {
                //don't run any setup of sections for envelopes, as the new (10.5.1) functionality
                //uses Word's merge feature to create the appropriate number of envelope sections
                return;
            }

            //GLOG 6687:  Don't ever run this action in Design Mode
            if (m_oForteDocument.Mode == ForteDocument.Modes.Design)
                return;

            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            string xVariableName = "";
            Variable oVarSource = null;
            int iDetailCount = 0;
            int iMaxSections = 0;

            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameter are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableName = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    if (aParams[1] != "")
                        iMaxSections = Int32.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xExpression", xVariableName);
            }
            if (xVariableName != "")
            {
                //Different variable holds the target tag
                Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableName);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVarSource = oTarget.GetVariable(xVariableName);

                        //stop after finding match
                        if (oVarSource != null)
                        {
                            xValue = oVarSource.Value;
                            break;
                        }
                    }
                }
                if (oVarSource == null)
                {
                    return;
                }
            }
            
            //Get last Index to determine count of details in Value
            iDetailCount = String.GetXMLEntityCount(xValue, "");

            //Limit Sections based on parameter
            if (iMaxSections > 0)
                iDetailCount = Math.Min(iDetailCount, iMaxSections);

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;

            //set editing environment
            m_oForteDocument.SetDynamicEditingEnvironment();

            Word.Range oRange = m_oSegment.PrimaryRange;

            //Setup table for detail
            try
            {
                //JTS (11/14/08): Don't do anything if there are already the correct number of sections
                if (oRange.Sections.Count != Math.Max(iDetailCount, 1))
                {
                    //disable event handler that prevents deletion of mDel tags
                    DateTime t1 = DateTime.Now;
                    LMP.Forte.MSWord.WordDoc.SetupDistributedSegmentSections(
                        oRange, iDetailCount, m_oSegment.PrimaryBookmark);

                    LMP.Benchmarks.Print(t1, "SetupDistributedSegmentSections");

                    ////Word may have deleted and recreated original Nodes
                    ////so we need to refresh the nodestore
                    t1 = DateTime.Now;
                    this.m_oForteDocument.RefreshTags(this.m_oForteDocument.Mode != ForteDocument.Modes.Design);
                    LMP.Benchmarks.Print(t1, "RefreshTags");

                    t1 = DateTime.Now;
                    //JTS 01/04/09: Need to include children to ensure Letterhead nodes are current
                    if (m_oSegment.Parent != null && m_oSegment.IsTransparent)
                    {
                        //GLOG 4025: If Segment containing action is Transparent, Refresh Parent Segment Nodes
                        Segment oParent = m_oSegment.Parent;
                        while (oParent.Parent != null && oParent.IsTransparent)
                            oParent = oParent.Parent;
                        oParent.RefreshNodes(true);
                    }
                    else
                        this.m_oSegment.RefreshNodes(true);

                    LMP.Benchmarks.Print(t1, "RefreshNodes");

                    //GLOG 3491 (2/25/09 dm) - raise event to let ui know to refresh tree -
                    //this is necessary because blocks may be added or deleted
                    m_oSegment.RefreshBlocks();
                    this.m_oForteDocument.RaiseSegmentDistributedSectionsChangedEvent(m_oSegment);
                }

            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);

                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns the xml
        /// </summary>
        /// <param name="xTemplate"></param>
        /// <returns></returns>
        private string GetTableRowXML(string xSource, string xTemplate, int iMinRows)
        {
            if (xSource == "" && iMinRows < 1)
                return "";

            string xTableXML = "";
            //GLOG 2083: Conform all fieldcodes to old format
            //JTS 2/24/16: This wasn't doing anything without assignment
            xTemplate = xTemplate.Replace("[SubVar__", "[SubVar_");
            //get subvars in template
            MatchCollection oSubVars = Regex.Matches(xTemplate, @"\[SubVar_.*?\]");

            //get names of fields in source that should be retrieved
            string[] aFields = new string[oSubVars.Count];

            int iCounter = 0;

            //cycle through subvars, adding field names to array for later use
            foreach (Match oSubVar in oSubVars)
            {
                aFields[iCounter] = oSubVar.Value.Substring(
                    8, oSubVar.Value.Length - 9);
                iCounter++;
            }

            string xOpeningXML = LMP.String.GetMinimalOpeningXML(false);

            //Get proper namespace prefix for MacPac 
            string xNS = LMP.String.GetNamespacePrefix(
                xOpeningXML, LMP.Data.ForteConstants.MacPacNamespace);

            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            oXMLSource.LoadXml("<zzmpD>" + xSource + "</zzmpD>");

            //select detail nodes in XML
            XmlNodeList oSourceXMLNodes = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");

            if (oSourceXMLNodes.Count > 0 || iMinRows > 0)
            {
                int iNumEntities = 0;

                //count entities in source
                if (oSourceXMLNodes.Count > 0)
                {
                    iNumEntities = int.Parse(oSourceXMLNodes[
                        oSourceXMLNodes.Count - 1].Attributes["Index"].Value);
                }

                //encapsulate template with row/cell xml
                xTemplate = "<w:tr><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/></w:tcPr><w:p>" + 
                    xTemplate + "</w:p></w:tc></w:tr>";

                //add appropriate xml for cell breaks
                xTemplate = xTemplate.Replace(@"\a", "</w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/></w:tcPr><w:p>");
                
                //replace linebreaks with proper xml
                xTemplate = xTemplate.Replace(@"\v", "<w:br/>");
                xTemplate = xTemplate.Replace(@"\r\n", "\r");
                xTemplate = xTemplate.Replace(@"\r", "<w:/p><w:p>");

                //cycle through entities, creating xml 
                //from source data and template
                for (int i = 1; i <= Math.Max(iNumEntities, iMinRows); i++)
                {
                    string xEntityXML = xTemplate;

                    //cycle through fields, getting the 
                    //appropriate data from the source -
                    //then replace corresponding text in template
                    foreach (string xField in aFields)
                    {
                        //Set defaults for empty fields
                        string xUNID = "0";
                        string xName = xField;
                        string xIndex = i.ToString();
                        string xText = "";

                        StringBuilder oSB = new StringBuilder();
                        if (i <= iNumEntities)
                        {
                            XmlNode oNode = oXMLSource.SelectSingleNode(
                                @"(/zzmpD/" + xField + "|/zzmpD/" + xField.ToUpper() + ")[@Index = '" + i.ToString() + "']");
                            if (oNode != null)
                            {
                                xUNID = oNode.Attributes["UNID"].Value;
                                xName = oNode.Name;
                                xIndex = oNode.Attributes["Index"].Value;
                                xText = oNode.InnerText.Replace("\r\n", "\v");
                                xText = LMP.String.ReplaceXMLChars(xText);
                            }

                            // Build WordML string for mSubVar Tag
                            oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}�UNID={3}��\"><w:r><w:t>{4}</w:t></w:r></{0}:mSubVar>",
                                xNS, xName, xIndex, xUNID, xText);

                        }
                        else
                        {
                            // Leave cells blank
                            oSB.Append("");
                        }
                        //replace subvar text in template
                        xEntityXML = xEntityXML.Replace("[SubVar_" + xField + "]", oSB.ToString());
                        //Remove any linebreaks at the start a cell (due to empty fields)
                        xEntityXML = xEntityXML.Replace("<w:p><w:br/>", "<w:p> ");
                    }

                    xTableXML += xEntityXML;
                }

            }
            return xTableXML;
        }
        /// <summary>
        /// processes XML string from designated ArrayList of XML element strings 
        /// </summary>
        /// <param name="aXML"></param>
        /// <param name="iAlternateFormat"></param>
        /// <param name="iHeadingRows"></param>
        /// <param name="xEntitySeparator"></param>
        /// <returns></returns>
        protected internal string GetXMLStringFromElementList(ArrayList aXML, AlternateFormatTypes iAlternateFormat,
            int iThreshold, string xEntitySeparator, string xTemplate, string xOpeningXML,
            string xClosingXML)
        {
            return GetXMLStringFromElementList(aXML, iAlternateFormat, iThreshold, xEntitySeparator, xTemplate,
                xOpeningXML, xClosingXML, -1);
        }
        protected internal string GetXMLStringFromElementList(ArrayList aXML, AlternateFormatTypes iAlternateFormat,
            int iThreshold, string xEntitySeparator, string xTemplate, string xOpeningXML,
            string xClosingXML, int iSingleElement)
        {
            //process array list to construct XML string
            //memo type recipients will be shuffled if count > threshold value
            //this will result in 2 columns of recipients inside a memo table cell

            int iSplitValue = 0;
            int iNodeIndex = 0;
            StringBuilder oSBInsert = new StringBuilder();
            string xInsertXML = null;

            int iEntityCount = aXML.Count;

            if (iEntityCount >= iThreshold)
                if (Number.IsOdd(iEntityCount))
                    iSplitValue = (iEntityCount + 1) / 2;
                else
                    iSplitValue = iEntityCount / 2;
            else
                iSplitValue = iEntityCount;

            int iStartIndex = 1;
            int iEndIndex = iEntityCount;

            if (iSingleElement > -1)
            {
                //Get XML for one Distributed Detail Node
                if (iSingleElement > iEntityCount - 1)
                {
                    if (iEntityCount > 1)
                        return "";
                    else
                        //Always return single entity (e.g. for Full page of label)
                        iSingleElement = 0;
                }
                if (iAlternateFormat == AlternateFormatTypes.Columns)
                {
                    //Memo details are split between two nodes
                    if (iSingleElement == 0)
                    {
                        iStartIndex = 1;
                        iEndIndex = iSplitValue;
                    }
                    else
                    {
                        iStartIndex = iSplitValue + 1;
                        iEndIndex = iEntityCount;
                    }
                }
                else
                {
                    //One detail per node
                    iStartIndex = iSingleElement + 1;
                    iEndIndex = iSingleElement + 1;
                }
            }
            for (int j = iStartIndex; j <= iEndIndex; j++)
            {
                iNodeIndex = j - 1;
                ArrayList aItem = (ArrayList)aXML[iNodeIndex];
                for (int k = 0; k < aItem.Count; k++)
                {
                    //reconstruct shuffled or non- shuffled XML string
                    //based on iIndex will have been set appropriately

                    string[,] xXMLMember = (string[,])aItem[k];
                    string xSep = null;

                    ////Don't insert SubVar if value is blank and it's a multi-SubVar detail
                    //if (!xXMLMember[0, 1].Contains("><w:r><w:t></w:t></w:r></ns0:mSubVar>") || aItem.Count == 1)
                    //{
                        xSep = xXMLMember[0, 0];

                        //we only want the entity separator if there's a template
                        if (xTemplate != "" && xSep != xEntitySeparator)
                            xSep = "";
                        
                        //Omit separator for first item
                        if (j == iStartIndex && k == 0)
                            xSep = "";
                        else
                        {
                            //Need to include text tags if
                            //separator is not predefined escape char
                            switch (xSep)
                            {
                                case "\r":
                                case "\v":
                                case "\r\n":
                                case "\r\r":
                                case "":
                                    break;
                                case "\t":
                                    //GLOG 4599: Replace Tab separator with appropriate WordML
                                    xSep = "<w:r><w:tab/></w:r>";
                                    break;
                                default:
                                    xSep = "<w:r><w:t>" + xSep + "</w:t></w:r>";
                                    break;
                            }
                        }

                        //string xTrailingSep = xXMLMember[0, 2];

                        //if (xTrailingSep == null)
                        //    xTrailingSep = "";

                        //if (xTemplate != "")
                        //{
                        //    switch (xTrailingSep)
                        //    {
                        //        case "\r":
                        //        case "\v":
                        //        case "\r\n":
                        //        case "\r\r":
                        //        case "":
                        //            break;
                        //        case "\t":
                        //            //GLOG 4599: Replace Tab separator with appropriate WordML
                        //            xTrailingSep = "<w:r><w:tab/></w:r>";
                        //            break;
                        //        default:
                        //            xTrailingSep = "<w:r><w:t>" + xTrailingSep + "</w:t></w:r>";
                        //            break;
                        //    }
                        //}

                        //oSBInsert.AppendFormat("{0}{1}{2}", xSep, xXMLMember[0, 1], xTrailingSep);
                        oSBInsert.AppendFormat("{0}{1}", xSep, xXMLMember[0, 1]);
//                    }
                }
            }

            //final processing handles escape characters
            //{zzTAB} and {zzCR} tokens will be replaced by mpCOM function
            //TODO: investigate issues w/ direct insertion of tab and CR XML runs

            xInsertXML = oSBInsert.ToString();
            if (string.IsNullOrEmpty(xInsertXML))
                return "";

            // Replace TAB CR and LF with corresponding WordML tags
            xInsertXML = xInsertXML.Replace("\r\n", "\r");
            xInsertXML = xInsertXML.Replace("\r\r", "</w:p><w:p/><w:p>");
            xInsertXML = xInsertXML.Replace("\r", "</w:p><w:p>");
            xInsertXML = xInsertXML.Replace("\v", "<w:br/>");
            xInsertXML = xInsertXML.Replace("\t", "</w:t></w:r><w:r><w:tab/></w:r><w:r><w:t>");

            //replace any double encoding generated as part of template processing above
            xInsertXML = xInsertXML.Replace("</w:t></w:r></w:t></w:r>", "</w:t></w:r>");
            xInsertXML = xInsertXML.Replace("<w:r><w:t><w:r><w:t>", "<w:r><w:t>");
            xInsertXML = xInsertXML.Replace("<w:r><w:t> <w:r><w:t>", "<w:r><w:t>");
            xInsertXML = xInsertXML.Replace("<w:t></w:t>", "");
            xInsertXML = xOpeningXML + "<w:p>" + xInsertXML + "</w:p>" + xClosingXML;

            //return processed string
            return xInsertXML;
        }

        /// <summary>
        /// creates arraylist of XML elements contained in specified nodelist
        /// </summary>
        /// <param name="oNodeList"></param>
        /// <returns></returns>
        protected internal ArrayList GetSubXMLArray(XmlNodeList oNodeList, string xTemplate,
            string xEntitySeparator, string xDetailItemSeparator, string xOpeningXML,
            ref string[] xDocVarsArray, bool bFormatSmartQuotes)
        {
            //create array of template subvar field codes for processing below
            //each member will be replaced w/ appropriate XML and will be used
            //to build item/entity XML
            string[] xTemplateElements = null;
            string[,] xNewEntity = new string[1, 3];
            string xItemSeparator = null;
            int iIndex = 0;
            ArrayList aTemp = new ArrayList();

            //determine whether to use open xml/content controls
            bool bOpenXML = LMP.String.IsWordOpenXML(xOpeningXML);

            //Get proper namespace prefix for MacPac
            string xNS = null;
            if (!bOpenXML)
            {
                xNS = LMP.String.GetNamespacePrefix(xOpeningXML,
                    LMP.Data.ForteConstants.MacPacNamespace);
            }

            if (xTemplate != "")
            {
                //GLOG 2083: Conform all fieldcodes to old format
                xTemplate = xTemplate.Replace("[SubVar__", "[SubVar_");
                
                xTemplateElements = xTemplate.Split('[');

                //handle portion of template following last SubVar field code
                int iLast = xTemplateElements.Length - 1;
                string xLastElement = xTemplateElements[iLast];
                string xLastField = null;
                xLastElement = xLastElement.Substring(xLastElement.IndexOf("]") + 1);

                //if there is additional text separate, encode, and reappend
                if (xLastElement != "")
                {
                    xLastField = xTemplateElements[iLast].Substring(0, xTemplateElements[iLast].IndexOf("]") + 1);
                    xLastElement = "<w:r><w:t>" + xLastElement + "</w:t></w:r>";
                    xTemplateElements[iLast] = xLastField + xLastElement;
                }

                if (xTemplateElements[0] != "")
                {
                    //append first member of array - which will be portion of template string 
                    //prior to first instance of [SubVar field code - to second member -
                    //this will ensure entire template string will be processed
                    xTemplateElements[1] = "<w:r><w:t>" + xTemplateElements[0] + "</w:t></w:r>" + xTemplateElements[1];
                }
            }

            //10.2 (dm) - get new doc var id for each sub var that we'll be adding
            ArrayList aDocVars = new ArrayList();
            string[] xDocVarIDsArray = null;
            //JTS 6/27/10: Document Variables are now being created in all instances
            //if (bOpenXML)
            //{
                string xDocVarIDs = LMP.Forte.MSWord.WordDoc.GetNewDocVarIDs(
                    m_oDocument, (short)oNodeList.Count);
                xDocVarIDsArray = xDocVarIDs.Split('|');
            //

            ArrayList aItem = null;
            foreach (XmlNode oNode in oNodeList)
            {
                //add non-empty nodes to array
                string xText = oNode.InnerText;
                if (xText != "")
                {
                    //massage text
                    xText = xText.Replace("\r\n", "\r");
                    xText = xText.Replace("\r", "\v");
                    xText = LMP.String.ReplaceXMLChars(xText);
                    //GLOG 6065: Format InnerText to replace quotes with smart quotes if appropriate
                    if (bFormatSmartQuotes)
                        xText = LMP.String.ReplaceQuotesWithSmartQuotes(xText);

                    //xNewEntity array will store separator and XML string for each node
                    xNewEntity = new string[1, 3];

                    int iCurIndex = Int32.Parse(oNode.Attributes.GetNamedItem("Index").Value);

                    if (iIndex != iCurIndex)
                    {
                        //  Index change indicates a new Detail Entity
                        //  change separator accordingly
                        iIndex = iCurIndex;
                        xItemSeparator = xEntitySeparator;

                        if (aItem != null)
                            aTemp.Add(aItem);
                        //Start new ArrayList for next Detail Entity
                        aItem = new ArrayList();
                    }
                    else
                        xItemSeparator = xDetailItemSeparator;

                    //construct XML string for each node using template if it exists
                    //GLOG 2083: Fieldcode might use 1 or 2 underscores
                    if (xTemplate == "" || xTemplate.ToUpper().Contains("[SUBVAR_" + oNode.Name.ToUpper() + "]"))
                    {
                        //ensure node name is case insensitive
                        string xNodeName = null;

                        if (xTemplate.Contains(oNode.Name.ToUpper()))
                            xNodeName = oNode.Name.ToUpper();
                        else
                            xNodeName = oNode.Name;

                        string xUNID = "0";
                        try
                        {
                            xUNID = oNode.Attributes.GetNamedItem("UNID").Value;
                        }
                        catch { }

                        // Build WordML string for mSubVar Tag
                        StringBuilder oSBNew = new StringBuilder();
                        //content control
                        int iCount = aDocVars.Count;
                        string xID = xDocVarIDsArray[iCount];
                        string xTag = "mpu" + xID + new string('0', 28);

                        //add doc var to array
                        string xName = "mpo" + xID;
                        string xValue = oNode.Name + "��ObjectData=Index=" +
                            oNode.Attributes.GetNamedItem("Index").Value +
                            "�UNID=" + xUNID + "��";
                        string xDocVar = xName + '|' + xValue;
                        aDocVars.Add(xDocVar);
                        if (bOpenXML)
                        {
                            oSBNew.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                            "<w:sdtContent><w:bookmarkStart w:id=\"{2}\" w:name=\"_{0}\" w:displacedBySDT=\"prev\"/>" +
                            "<w:r><w:t>{1}</w:t></w:r><w:bookmarkEnd w:id=\"{2}\" w:displacedBySDT=\"next\"/></w:sdtContent></w:sdt>",
                            xTag, xText, iCount.ToString());
                        }
                        else
                        {
                            //xml tag
                            //JTS 6/27/10: Include Reserved attribute and bookmark
                            //oSBNew.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}�UNID={3}��\" Reserved=\"{4}\">" +
                            //    "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{4}\" w:displacedBySDT=\"prev\"/>" +
                            //    "<w:r><w:t>{5}</w:t></w:r><aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"next\"/></{0}:mSubVar>",
                            //    xNS, oNode.Name, oNode.Attributes.GetNamedItem("Index").Value, xUNID, xTag, xText, iCount.ToString());

                            //removed displacedBySDT attribute - dcf - 8/31/10
                            oSBNew.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}�UNID={3}��\" Reserved=\"{4}\">" +
                                "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{4}\"/>" +
                                "<w:r><w:t>{5}</w:t></w:r><aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\"/></{0}:mSubVar>",
                                xNS, oNode.Name, oNode.Attributes.GetNamedItem("Index").Value, xUNID, xTag, xText, iCount.ToString());
                        }

                        //process template
                        if (xTemplate != "")
                        {
                            //search array and find SubVar field code corresponding to node being processed
                            //replace w/ XML string from above
                            for (int j = 0; j < xTemplateElements.Length; j++)
                            {
                                if (xTemplateElements[j].ToUpper().Contains("SUBVAR_" + xNodeName.ToUpper() + "]"))
                                {
                                    string xXML = xTemplateElements[j].Replace("SubVar_" + xNodeName + "]", oSBNew.ToString());

                                    ////search for trailing literal text
                                    //if (bOpenXML)
                                    //{
                                    //    int iTrailingTextPos = xXML.LastIndexOf("</w:sdt>") + "</w:sdt>".Length;

                                    //    //get trailing text as separator entry
                                    //    xNewEntity[0, 2] = xXML.Substring(iTrailingTextPos);

                                    //    //trim off trailing text from main xml
                                    //    xXML = xXML.Substring(0, iTrailingTextPos);
                                    //}
                                    
                                    xNewEntity[0, 1] = xXML;
                                    break;
                                }
                            }
                            if (string.IsNullOrEmpty(xNewEntity[0, 1]))
                                xNewEntity[0, 1] = "";
                        }

                        else
                            xNewEntity[0, 1] = oSBNew.ToString();

                        //store appropriate item separator
                        xNewEntity[0, 0] = xItemSeparator;
                        aItem.Add(xNewEntity);
                    }
                }
            }
            if (aItem != null)
                aTemp.Add(aItem);

            //10.2 - convert doc var array list to array for consumption by mpCOM
            if (aDocVars.Count > 0)
                xDocVarsArray = (string[])aDocVars.ToArray(typeof(string));

            return aTemp;
        }
        /// <summary>
        /// returns detail item or entity item separators
        /// </summary>
        /// <param name="xEntitySeparator"></param>
        /// <param name="xDetailItemSeparator"></param>
        protected internal void ModifyItemSeparatorsIfNecessary(string[] aParams, ref string xEntitySeparator, 
            ref string xDetailItemSeparator)
        {
            if (xDetailItemSeparator == "Null")
                //keyword specifies that line breaks should simply be removed
                xDetailItemSeparator = "";
            else if (xDetailItemSeparator == "")
                xDetailItemSeparator = "\v";

            if (xEntitySeparator == "Null")
                //keyword specifies that line breaks should simply be removed
                xEntitySeparator = "";
            else if (xEntitySeparator == "")
                xEntitySeparator = "\r";

            // Item separator can't be Chr(13) if Entity separator is Chr(11), because 
            // mSubVar tag can't contain a partial paragraph
            if (xEntitySeparator == "\v" && xDetailItemSeparator == "\r")
                xDetailItemSeparator = "\v";

        }
        /// <summary>
        /// inserts the specified SubVar-related reline
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertReline(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            string xSpecialSeparator = "";
            string xSpecialDelimiter = "";
            string xStandardDelimiter = "";
            string xLabelText = "";
            string xInsertXML = "";
            short shFormat = 0;
            short shUnderlineLength = 0;
            string[] xDocVarsArray  = null;
            LMP.Controls.mpRelineFormatOptions iDefOptions = 0;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //determine whether current document is using content controls
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);
            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 5)
                //5 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    //Label text preceding Re line
                    xLabelText = aParams[0];
                    if (xLabelText == "Null")
                        xLabelText = "";
                    else if (xLabelText == "")
                        xLabelText = "Re:[Char_9]";
                    xLabelText = Expression.Evaluate(xLabelText, m_oSegment, m_oForteDocument);

                    //Separator character(s) between Standard and Special Relines
                    xStandardDelimiter = aParams[1];
                    if (xStandardDelimiter == "Null")
                        xStandardDelimiter = "";
                    else if (xStandardDelimiter == "")
                        xStandardDelimiter = "[Char_13][Char_9]";
                    xStandardDelimiter = Expression.Evaluate(xStandardDelimiter,
                        m_oSegment, m_oForteDocument);

                    //Character(s) separating labels and values in Special Re line
                    xSpecialSeparator = aParams[2];
                    if (xSpecialSeparator == "Null")
                        xSpecialSeparator = "";
                    else if (xSpecialSeparator == "")
                        xSpecialSeparator = "[Char_9]:[Char_9]";
                    xSpecialSeparator = Expression.Evaluate(xSpecialSeparator,
                        m_oSegment, m_oForteDocument);

                    //Character(s) at end of each Special line
                    xSpecialDelimiter = aParams[3];
                    if (xSpecialDelimiter == "Null")
                        xSpecialDelimiter = "";
                    else if (xSpecialDelimiter == "")
                        xSpecialDelimiter = "[Char_11]";
                    xSpecialDelimiter = Expression.Evaluate(xSpecialDelimiter,
                        m_oSegment, m_oForteDocument);

                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[4], m_oSegment, m_oForteDocument));

                    iDefOptions = (LMP.Controls.mpRelineFormatOptions)Int32.Parse(Expression.Evaluate(aParams[5], 
                        m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                Trace.WriteNameValuePairs("xValue", xValue, "xStandardDelimiter", xStandardDelimiter, "xSpecialSeparator",
                    xSpecialSeparator, "xSpecialDelimiter", xSpecialDelimiter, "iDeleteScope",
                    iDeleteScope, "xLabelText", xLabelText);

                //exit if value of assignment is mpUndefined
                if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                    return;

            }
            if (xValue.IndexOf('<') != 0)
            {
                //Make sure string is in XML form
                xValue = LMP.Controls.Reline.StringToRelineXML(xValue, iDefOptions, "");
            }

            try
            {
                xInsertXML = GetRelineDocXML(xValue, xLabelText, xStandardDelimiter,
                    xSpecialDelimiter, xSpecialSeparator, ref xDocVarsArray, ref shFormat, bContentControls);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_InvalidDetailXML") +
                    xParameters, oE);
            }

            //get current xml markup state for later restoration
            Word.View oView = this.m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            //set argument values
            Object oNodesArray = null;
            bool bIsSegNode = false;
            if (!bContentControls)
            {
                //xml tags
                Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;
                if (oNodes != null)
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (oNodes[0].BaseName == "mSEG");
                }
                else
                {
                    //segment is bookmark-bounded
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = oBmkNodes[0].Name.Substring(1, 3) == "mps";
                }
            }
            else
            {
                //content controls
                Word.ContentControl[] oNodes = m_oVariable.ContainingContentControls;
                if (oNodes != null)
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oNodes[0].Tag) == "mSEG");
                }
                else
                {
                    //segment is bookmark-bounded
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = oBmkNodes[0].Name.Substring(1, 3) == "mps";
                }
            }

            //exit if value is empty and scope is already deleted
            bool bIsEmpty = (xInsertXML.IndexOf("<w:t>") == -1);
            if (bIsSegNode && bIsEmpty && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || (bIsEmpty && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            LMP.Forte.MSWord.mpRelineFormatOptions iRelineFormat = (LMP.Forte.MSWord.mpRelineFormatOptions)shFormat;
            //GLOG 3510: Indicate for InsertXML action to remove underline from 
            //last line if there is text immediately following mVar which has been included
            //in Underline Last
            if (this.m_oForteDocument.Mode != ForteDocument.Modes.Design && 
                (iRelineFormat & LMP.Forte.MSWord.mpRelineFormatOptions.RelineUnderline) == 0 &&
                (iRelineFormat & LMP.Forte.MSWord.mpRelineFormatOptions.RelineUnderlineLast) == 0)
                iRelineFormat = iRelineFormat | LMP.Forte.MSWord.mpRelineFormatOptions.RemoveUnderlineLast;

            Object oValuesArray = new string[] { xInsertXML };

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert
            bool bRefreshRequired = false;
            try
            {
                //10.2
                if (xDocVarsArray == null)
                    xDocVarsArray = new string[1] { "" };
                object oDocVarsArray = xDocVarsArray;
                if (!bIsSegNode)
                {
                    if (bContentControls)
                    {
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertXML_CC(m_oDocument, ref oNodesArray,
                            ref oValuesArray, shUnderlineLength, iRelineFormat, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation, false,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref oDocVarsArray, ref missing);
                    }
                    else
                    {
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertXML(m_oDocument, ref oNodesArray,
                            ref oValuesArray, shUnderlineLength, iRelineFormat, iDeleteScope,
                            xSegmentXML, m_oVariable.Name, oInsertionLocation, false,
                            ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                            ref oDocVarsArray, ref missing);
                    }
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.InsertXML_RestoreScope(m_oDocument, ref oNodesArray,
                        ref oValuesArray, shUnderlineLength, iRelineFormat, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, oInsertionLocation,
                        ref xVariablesInScope, ref oTags, bContentControls, ref oDocVarsArray); //JTS 6/17/10
                }
            }
            catch (System.Exception oE)
            {
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw;
                }
            }
            finally
            {
                if (iShowTags == 0)
                    //hide tags, as that was the original state
                    //of the tags before editing
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            if (bExpandedScope)
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = (bIsEmpty && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh node store
                m_oSegment.RefreshNodes();
        }
        private string GetRelineDocXML(string xText, string xLabelText, string xStandardDelimiter, string xSpecialDelimiter,
            string xSpecialSeparator, ref string[] xDocVarsArray, ref short shFormat, bool bContentControls)
        {
            string xStandardXML = "";
            string xSpecialXML = "";
            string xInsertXML = "";
            string xPrefill = "";
            string xFormatXML = "";
            string xSpecialFormatXML = "";
            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            //Convert xValue to XML Nodes
            oXMLSource.LoadXml(string.Concat("<zzmpD>", xText, "</zzmpD>"));

            string xOpeningXML = LMP.String.GetMinimalOpeningXML(bContentControls);
            string xClosingXML = LMP.String.GetMinimalClosingXML(bContentControls);

            //Get proper namespace prefix for MacPac 
            string xNS = LMP.String.GetNamespacePrefix(xOpeningXML, ForteConstants.MacPacNamespace);

            //determine whether to use open xml/content controls
            bool bOpenXML = LMP.String.IsWordOpenXML(xOpeningXML);
            //select detail nodes in XML
            XmlNodeList oNodeList = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");
            ArrayList aDocVars = new ArrayList();
            XmlNode oAtt = null;
            foreach (XmlNode oNode in oNodeList)
            {
                switch (oNode.Name.ToUpper())
                {
                    case "STANDARD":
                        xStandardXML = LMP.String.ReplaceXMLChars(oNode.InnerText);
                        //GLOG 6065: Format InnerText to replace quotes with smart quotes if appropriate
                        if (LMP.Architect.Api.Application.CurrentWordApp.Options.AutoFormatAsYouTypeReplaceQuotes)
                        {
                            xStandardXML = LMP.String.ReplaceQuotesWithSmartQuotes(xStandardXML);
                        }
                        //If standard Re line is not blank, build appropriate XML
                        if (xStandardXML != "")
                        {
                            oAtt = oNode.Attributes.GetNamedItem("Format");
                            if (oAtt != null)
                                shFormat = (short)(shFormat | short.Parse(oAtt.Value));
                            LMP.Controls.mpRelineFormatOptions iFormat = (LMP.Controls.mpRelineFormatOptions)shFormat;
                            // Get selected formatting options to build appropriate w:rPr tags
                            if ((iFormat & LMP.Controls.mpRelineFormatOptions.RelineBold) == LMP.Controls.mpRelineFormatOptions.RelineBold)
                                xFormatXML = xFormatXML + "<w:b/>";
                            if ((iFormat & LMP.Controls.mpRelineFormatOptions.RelineItalic) == LMP.Controls.mpRelineFormatOptions.RelineItalic)
                                xFormatXML = xFormatXML + "<w:i/>";
                            if (xFormatXML != "")
                                xFormatXML = "<w:rPr>" + xFormatXML + "</w:rPr>";
                            oAtt = oNode.Attributes.GetNamedItem("Default");
                            {
                                if (oAtt != null)
                                    xPrefill = oAtt.Value;
                            }
                            //JTS 6/27/10: Doc Variables will be created for XML Tag insertion as well
                            StringBuilder oSB = new StringBuilder();
                            string xID = LMP.Forte.MSWord.WordDoc.GetNewDocVarIDs(m_oDocument, 1);
                            string xTag = "mpu" + xID + new string('0', 28);
                            int iCount = aDocVars.Count;
                            //add doc var to array
                            string xName = "mpo" + xID;
                            string xValue = oNode.Name + "��ObjectData=��Format=" +
                                shFormat.ToString() + "�Default=" + xPrefill;
                            string xDocVar = xName + '|' + xValue;
                            aDocVars.Add(xDocVar);
                            // Build WordML string for mSubVar Tag
                            if (bOpenXML)
                            {
                                oSB.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                                    "<w:sdtContent><w:bookmarkStart w:id=\"{3}\" w:name=\"_{0}\" w:displacedBySDT=\"prev\"/>" +
                                    "<w:r>{1}<w:t>{2}</w:t></w:r><w:bookmarkEnd w:id=\"{3}\" w:displacedBySDT=\"next\"/>" +
                                    "</w:sdtContent></w:sdt>",
                                    xTag, xFormatXML, xStandardXML.Replace("\r\n", "\v"), iCount.ToString());

                            }
                            else
                            {
                                //JTS 6/27/10: Include Reserved attribute and Bookmark in XML
                                //oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"��Format={2}�Default={3}\" Reserved=\"{4}\">" +
                                //"<aml:annotation aml:id=\"{7}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{4}\" w:displacedBySDT=\"prev\"/>" +
                                //"<w:r>{5}<w:t>{6}</w:t></w:r>" +
                                //"<aml:annotation aml:id=\"{7}\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"next\"/></{0}:mSubVar>",
                                //     xNS, oNode.Name, shFormat.ToString(), xPrefill, xTag, xFormatXML,
                                //     xStandardXML.Replace("\r\n", "\v"), iCount.ToString());

                                //removed displaceBySDT attribute - dcf - 8/31/10
                                oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"��Format={2}�Default={3}\" Reserved=\"{4}\">" +
                                "<aml:annotation aml:id=\"{7}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{4}\"/>" +
                                "<w:r>{5}<w:t>{6}</w:t></w:r>" +
                                "<aml:annotation aml:id=\"{7}\" w:type=\"Word.Bookmark.End\"/></{0}:mSubVar>",
                                     xNS, oNode.Name, shFormat.ToString(), xPrefill, xTag, xFormatXML,
                                     xStandardXML.Replace("\r\n", "\v"), iCount.ToString());
                            }
                            xStandardXML = oSB.ToString();
                        }
                        break;
                    case "SPECIAL":
                        xSpecialXML = oNode.InnerXml;
                        XmlDocument oSpecialXML = new XmlDocument();
                        oSpecialXML.LoadXml(string.Concat("<zzmpS>", xSpecialXML, "</zzmpS>"));
                        XmlNodeList oSpecialNodes = oSpecialXML.SelectNodes("/zzmpS[Value!='']");
                        // Only build special XML if all values are not blank
                        if (oSpecialNodes.Count > 0)
                        {

                            oAtt = oNode.Attributes.GetNamedItem("Format");
                            if (oAtt != null)
                                shFormat = (short)(shFormat | short.Parse(oAtt.Value));
                            LMP.Controls.mpRelineFormatOptions iFormat = (LMP.Controls.mpRelineFormatOptions)shFormat;
                            // Get selected formatting options to build appropriate w:rPr tags
                            if ((iFormat & LMP.Controls.mpRelineFormatOptions.SpecialBold) == LMP.Controls.mpRelineFormatOptions.SpecialBold)
                                xSpecialFormatXML = xSpecialFormatXML + "<w:b/>";
                            if ((iFormat & LMP.Controls.mpRelineFormatOptions.SpecialItalic) == LMP.Controls.mpRelineFormatOptions.SpecialItalic)
                                xSpecialFormatXML = xSpecialFormatXML + "<w:i/>";
                            if (xSpecialFormatXML != "")
                                xSpecialFormatXML = "<w:rPr>" + xSpecialFormatXML + "</w:rPr>";
                            // If No Standard Re line, use Speciall formatting for label
                            if (xStandardXML == "")
                            {
                                xFormatXML = xSpecialFormatXML;
                            }
                            oAtt = oNode.Attributes.GetNamedItem("Default");
                            {
                                if (oAtt != null)
                                    xPrefill = oAtt.Value;
                            }

                            oSpecialNodes = oSpecialXML.SelectNodes("/zzmpS/*");
                            string xTemp = "";
                            int iIndex = 0;
                            foreach (XmlNode oSpecialNode in oSpecialNodes)
                            {
                                if (oSpecialNode.Name.ToUpper() == "LABEL" || oSpecialNode.Name.ToUpper() == "VALUE")
                                {
                                    string xSubVarXML = LMP.String.ReplaceXMLChars(oSpecialNode.InnerText);
                                    string xCurDelimiter = "";
                                    if (oSpecialNode.Name.ToUpper() == "LABEL")
                                        xCurDelimiter = xSpecialDelimiter;
                                    else
                                        xCurDelimiter = xSpecialSeparator;
                                    iIndex++;
                                    //Insert Delimiter if not first item
                                    if (xTemp != "")
                                        xTemp = xTemp + "<w:r>" + xSpecialFormatXML + "<w:t>" + xCurDelimiter + "</w:t></w:r>";
                                    StringBuilder oSB = new StringBuilder();
                                    //JTS 6/27/10: Doc Variables will be created for XML Tag insertion as well
                                    //content control
                                    string xSubVarDocID = LMP.Forte.MSWord.WordDoc.GetNewDocVarIDs(
                                        m_oDocument, 1);
                                    int iCount = aDocVars.Count;
                                    string xTag = "mpu" + xSubVarDocID + new string('0', 28);
                                    //add doc var to array
                                    string xName = "mpo" + xSubVarDocID;
                                    string xValue = oSpecialNode.Name + "��ObjectData=Index=" +
                                        iIndex.ToString() + "���";
                                    if (iIndex == 1)
                                        //12-20-10 (dm) - add temp id for post-injunction retagging
                                        xValue += "��TempID=1";
                                    string xDocVar = xName + '|' + xValue;
                                    aDocVars.Add(xDocVar);
                                    //// Build WordML string for mSubVar Tag
                                    //if (bOpenXML)
                                    //{
                                    //    oSB.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                                    //        "<w:sdtContent><w:bookmarkStart w:id=\"{3}\" w:name=\"_{0}\"/>" +
                                    //        "<w:r>{1}<w:t>{2}</w:t></w:r><w:bookmarkEnd w:id=\"{3}\"/></w:sdtContent></w:sdt>",
                                    //        xTag, xSpecialFormatXML, LMP.String.ReplaceXMLChars(oSpecialNode.InnerText.Replace("\r\n", "\v")),
                                    //        iCount.ToString());
                                    //}
                                    //else
                                    //{
                                    //    //dm 7/15/10: added Reserved attribute
                                    //    oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}���\" Reserved=\"{5}\">" +
                                    //        "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{5}\" w:displacedBySDT=\"prev\"/>" +
                                    //        "<w:r>{3}<w:t>{4}</w:t></w:r><aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"next\"/></{0}:mSubVar>",
                                    //         xNS, oSpecialNode.Name, iIndex, xSpecialFormatXML,
                                    //         LMP.String.ReplaceXMLChars(oSpecialNode.InnerText.Replace("\r\n", "\v")), xTag, iCount.ToString());
                                    //}

                                    //added/removed displacedBySDT attribute where appropriate - dcf - 8/31/10
                                    // Build WordML string for mSubVar Tag
                                    //GLOG 6065: Format InnerText to replace quotes with smart quotes if appropriate
                                    if (bOpenXML)
                                    {
                                        oSB.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                                            "<w:sdtContent><w:bookmarkStart w:id=\"{3}\" w:name=\"_{0}\" w:displacedBySDT=\"prev\"/>" +
                                            "<w:r>{1}<w:t>{2}</w:t></w:r><w:bookmarkEnd w:id=\"{3}\" w:displacedBySDT=\"next\"/></w:sdtContent></w:sdt>",
                                            xTag, xSpecialFormatXML, LMP.String.ReplaceQuotesWithSmartQuotes(LMP.String.ReplaceXMLChars(oSpecialNode.InnerText.Replace("\r\n", "\v"))),
                                            iCount.ToString());
                                    }
                                    else
                                    {
                                        //dm 7/15/10: added Reserved attribute
                                        oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}���\" Reserved=\"{5}\">" +
                                            "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{5}\"/>" +
                                            "<w:r>{3}<w:t>{4}</w:t></w:r><aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\"/></{0}:mSubVar>",
                                             xNS, oSpecialNode.Name, iIndex, xSpecialFormatXML,
                                             LMP.String.ReplaceQuotesWithSmartQuotes(LMP.String.ReplaceXMLChars(oSpecialNode.InnerText.Replace("\r\n", "\v"))),
                                             xTag, iCount.ToString());
                                    }
                                    xTemp = xTemp + oSB.ToString();
                                }
                            }
                            if (xTemp != "")
                            {
                                StringBuilder oSB = new StringBuilder();
                                //JTS 6/27/10: Doc Variables will be created for XML Tag insertion as well
                                //content control
                                string xID = LMP.Forte.MSWord.WordDoc.GetNewDocVarIDs(m_oDocument, 1);
                                int iCount = aDocVars.Count;
                                string xTag = "mpu" + xID + new string('0', 28);
                                //add doc var to array
                                string xName = "mpo" + xID;
                                //12-20-10 (dm) - add temp id for post-injunction retagging
                                string xValue = oNode.Name + "��ObjectData=��Format=" +
                                    shFormat.ToString() + "�Default=" + xPrefill + "��TempID=2";
                                string xDocVar = xName + '|' + xValue;
                                aDocVars.Add(xDocVar);
                                // Build WordML string for mSubVar Tag
                                //if (bOpenXML)
                                //{
                                //    oSB.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                                //        "<w:sdtContent><w:bookmarkStart w:id=\"{2}\" w:name=\"_{0}\"/>" +
                                //        "{1}<w:bookmarkEnd w:id=\"{2}\"/></w:sdtContent></w:sdt>",
                                //        xTag, xTemp, iCount.ToString());

                                //}
                                //else
                                //{
                                //    //dm 7/15/10: added Reserved attribute
                                //    oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"��Format={2}�Default={3}\" Reserved=\"{5}\">" +
                                //        "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{5}\" w:displacedBySDT=\"prev\"/>" +
                                //        "{4}<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"next\"/>" + 
                                //        "</" + xNS + ":mSubVar>",
                                //         xNS, oNode.Name, shFormat.ToString(), xPrefill, xTemp, xTag, iCount.ToString());
                                //}

                                //added/removed displacedBySDT attribute where appropriate - dcf - 8/31/10
                                if (bOpenXML)
                                {
                                    oSB.AppendFormat("<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr>" +
                                        "<w:sdtContent><w:bookmarkStart w:id=\"{2}\" w:name=\"_{0}\" w:displacedBySDT=\"prev\"/>" +
                                        "{1}<w:bookmarkEnd w:id=\"{2}\" w:displacedBySDT=\"next\"/></w:sdtContent></w:sdt>",
                                        xTag, xTemp, iCount.ToString());

                                }
                                else
                                {
                                    //dm 7/15/10: added Reserved attribute
                                    oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"��Format={2}�Default={3}\" Reserved=\"{5}\">" +
                                        "<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.Start\" w:name=\"_{5}\"/>" +
                                        "{4}<aml:annotation aml:id=\"{6}\" w:type=\"Word.Bookmark.End\"/>" + 
                                        "</" + xNS + ":mSubVar>",
                                         xNS, oNode.Name, shFormat.ToString(), xPrefill, xTemp, xTag, iCount.ToString());
                                }
                                xSpecialXML = oSB.ToString();
                            }
                        }
                        else
                        {
                            xSpecialXML = "";
                        }
                        break;
                }
            }
            if (xStandardXML != "" || xSpecialXML != "")
            {
                if (bOpenXML)
                {
                    //8-1-11 (dm) - WordML includes xml:space attribute in xOpeningXML
                    xInsertXML = xOpeningXML + "<w:p><w:r>" + xFormatXML +
                        "<w:t xml:space=\"preserve\">" + xLabelText + "</w:t></w:r>";
                }
                else
                {
                    xInsertXML = xOpeningXML + "<w:p><w:r>" + xFormatXML + "<w:t>" + xLabelText + "</w:t></w:r>";
                }
                if (xStandardXML != "")
                {
                    xInsertXML = xInsertXML + xStandardXML;
                    if (xSpecialXML != "")
                        xInsertXML = xInsertXML + xStandardDelimiter;
                }
                if (xSpecialXML != "")
                    xInsertXML = xInsertXML + xSpecialXML;

                // Replace CR and LF with corresponding WordML tags
                xInsertXML = xInsertXML.Replace("\r\t", "\r<w:r><w:t>\t</w:t></w:r>");
                xInsertXML = xInsertXML.Replace("\r\n", "\r");
                xInsertXML = xInsertXML.Replace("\r", "</w:p><w:p>");
                xInsertXML = xInsertXML.Replace("\v", "<w:br/>");
                xInsertXML = xInsertXML + "</w:p>" + xClosingXML;
                xInsertXML = xInsertXML.Replace("\t", "</w:t><w:tab/><w:t>");
                xInsertXML = xInsertXML.Replace("<w:t></w:t>", "");
            }
            //10.2 - convert doc var array list to array for consumption by mpCOM
            if (aDocVars.Count > 0)
                xDocVarsArray = (string[])aDocVars.ToArray(typeof(string));
            return xInsertXML;
        }
        /// <summary>
        /// if xValue is TRUE, inserts secondary default value at tag containing the
        /// definition for this variable; if there's no secondary default value,
        /// reinserts stored prior value; if xValue is FALSE, deletes specified scope -
        /// applies formatting per any embedded formatting tokens
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">iDeleteScope</param>
		private void IncludeExcludeText(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
		{
            DateTime t0 = DateTime.Now;

            string xDelimReplacement = "";
            string xExpression = "";
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;

			if(xParameters == "")
				//parameters are required for this action
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_InvalidParameterString"));

			//parse parameter string
			string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

			if(aParams.Length < 1 || aParams.Length > 3)
				//3 parameters are used, but also allow 1 or 2 for backward compatibility
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
					xParameters);
			else
			{
				try
				{
                    //get parameters
					iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes) int.Parse(
						Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument));

                    //get delimiter replacement
                    if (aParams.Length > 1)
                    {
                        xDelimReplacement = Expression.Evaluate(aParams[1],
                            m_oSegment, m_oForteDocument);
                    }
                    if (xDelimReplacement.ToUpper() == "NULL")
                        //keyword specifies that line breaks should simply be removed
                        xDelimReplacement = "";
                    else if (xDelimReplacement == "")
                        xDelimReplacement = "\v";

                    if (aParams.Length > 2)
                        xExpression = aParams[2];
                }
				catch(System.Exception oE)
				{
					//one of the parameters is invalid
					throw new LMP.Exceptions.ArgumentException(
						LMP.Resources.GetLangString("Error_InvalidParameterValue") +
						xParameters, oE);
				}
			}

			Trace.WriteNameValuePairs("xValue", xValue, "iDeleteScope", iDeleteScope,
                "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
			if(xValue == ((int) LMP.mpTriState.Undefined).ToString())
				return;

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            //set argument values
            Object oNodesArray = null;
            bool bIsSegNode = false;
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);
            if (!bContentControls)
            {
                //xml tags
                Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;

                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (oNodes[0].BaseName == "mSEG");
                }
            }
            else
            {
                //content controls
                Word.ContentControl[] oNodes = m_oVariable.ContainingContentControls;
                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oNodes[0].Tag) == "mSEG");
                }
            }
            bool bInclude = ((!string.IsNullOrEmpty(xValue) && xValue.ToUpper() != "NULL" && 
                String.ToBoolean(xValue)) || bIgnoreDeleteScope);
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            object missing = System.Reflection.Missing.Value;

            //get text to insert
            string xText = "";
            if (bInclude && !bIgnoreDeleteScope)
            {
                xText = m_oVariable.SecondaryDefaultValue;
                if (xText != null && xText != "")
                    xText = Expression.Evaluate(xText, m_oSegment, m_oForteDocument);
                else
                    xText = m_oVariable.ReserveValue;

                //this is a de facto exclude
                if (xText == "")
                    bInclude = false;
            }

            //exit if set to exclude and scope is already deleted
            if (bIsSegNode && !bInclude)
                return;

            //determine whether this is an expanded delete scope scenario
            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || !bInclude));
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

			//run action
            bool bRefreshRequired = false;
			try
			{
                if (bInclude)
                {
                    if (!bIsSegNode)
                        bRefreshRequired = LMP.Forte.MSWord.WordDoc.IncludeText(m_oDocument, ref oNodesArray,
                            xText, xDelimReplacement, iDeleteScope, xSegmentXML,
                            m_oVariable.Name, oInsertionLocation, ref xVariablesInScope,
                            ref oTags, ref missing);
                    else
                        LMP.Forte.MSWord.WordDoc.IncludeText_RestoreScope(m_oDocument, ref oNodesArray,
                            xText, xDelimReplacement, iDeleteScope, xSegmentXML,
                            m_oVariable.Name, oInsertionLocation, ref xVariablesInScope,
                            ref oTags);
                }
                else
                    bRefreshRequired = LMP.Forte.MSWord.WordDoc.ExcludeText(m_oDocument, ref oNodesArray,
                        iDeleteScope, m_oVariable.Name, ref bIgnoreDeleteScope,
                        ref xVariablesInScope, ref oTags);
			}
			catch(System.Exception oE)
			{
				int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
				if (iErr == mpNoDeleteScopeLocationException)
				{
					//TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
				else
				{
					//rethrow
					throw;
				}
			}
			finally
			{
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false, false);
    
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
			}

            if (bExpandedScope)
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = ((!bInclude) && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh node store
                m_oSegment.RefreshNodes();

            LMP.Benchmarks.Print(t0, this.m_oVariable.Name);
        }

        /// <summary>
        /// if xValue is TRUE, includes specified block -
        /// if xValue is FALSE, deletes specified block
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xBlockName</param>
        private void IncludeExcludeBlocks(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            //GLOG - 3395 - CEH
            string xBlockName = "";
            string xExpression = "";
            string xName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1 && aParams.Length != 2)
                //2 parameters are used, but als allow 1 for backward compatibility
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //GLOG - 3395 - CEH
                    //Allow for more multiple Block names
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Block names
                    aNames = xName.Split(',');

                    if (aParams.Length == 2)
                        xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes
            if (m_oSegment == null)
                return;

            //set other argument values
            bool bInclude = string.IsNullOrEmpty(xValue) ? false : (bIgnoreDeleteScope || String.ToBoolean(xValue));
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            //10.2 - JTS 3/29/10
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            string xSegmentXML = "";
            if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
            {
                //for pleading tables, use xml of parent
                if (m_oSegment.Parent.HasDefinition)
                    xSegmentXML = m_oSegment.Parent.Definition.XML;
            }
            else if (m_oSegment.HasDefinition)
                xSegmentXML = m_oSegment.Definition.XML;

            //GLOG - 3395 - CEH
            //Cycle through each block name
            foreach (string xItem in aNames)
            {
                string[] aBlocks;
                //GLOG 6266: Support * wildcard for Block names
                if (xItem.Contains("*"))
                {
                    string xMatchingBlocks = "";
                    //Check for both existing and deleted Blocks that match name
                    //Check only Blocks that are not currently deleted
                    ReadOnlyNodeStore oBlocks = m_oSegment.Nodes.GetNodes(@"//Node[(@ElementName='mBlock') or 
                                (@ElementName='mDel')][contains(@TagID, '" + xItem.Replace("*", "") + "')]");

                    for (int b = 0; b < oBlocks.Count; b++)
                    {
                        string xBlockTagID = oBlocks[b].Attributes["TagID"].Value;
                        xBlockTagID = xBlockTagID.Substring(xBlockTagID.LastIndexOf(".") + 1);
                        xMatchingBlocks = xMatchingBlocks + xBlockTagID + ",";
                    }
                    xMatchingBlocks = xMatchingBlocks.TrimEnd(',');
                    aBlocks = xMatchingBlocks.Split(',');
                }
                else
                {
                    aBlocks = new string[] { xItem };
                }
                foreach (string xVar in aBlocks)
                {
                    xBlockName = Expression.Evaluate(xVar, m_oSegment,
                        m_oForteDocument);

                    //get tag type
                    string xTagID = m_oSegment.FullTagID + '.' + xBlockName;
                    string xTagType = "";
                    try
                    {
                        xTagType = m_oSegment.Nodes.GetItemElementName(xTagID);
                    }
                    catch { }
                    if (xTagType == "")
                    {
                        if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                        {
                            //the target tag has been deleted -
                            //prompt user and offer to delete the executing variable
                            string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (oDR == DialogResult.Yes)
                                m_oSegment.Variables.Delete(m_oVariable);
                        }
                        break;
                    }

                    //exit if nothing to do
                    if ((bInclude && (xTagType == "mBlock")) ||
                        ((!bInclude) && (xTagType == "mDel")))
                        break;

                    //get mSEG part number
                    string xParts = m_oSegment.Nodes.GetItemPartNumbers(xTagID);
                    string[] aParts = xParts.Split(';');

                    Word.XMLNode oWordTag = null;
                    Word.ContentControl oCC = null;
                    Word.Bookmark oBmk = null;

                    if (bContentControls)
                    {
                        //get Word tag
                        if (bInclude)
                        {
                            //get containing mSEG
                            oCC = m_oSegment.Nodes.GetContentControl(m_oSegment.FullTagID,
                                Int32.Parse(aParts[0]));

                            if (oCC == null)
                            {
                                //attempt to get mSEG bookmark
                                oBmk = m_oSegment.Nodes.GetBookmark(m_oSegment.FullTagID,
                                    Int32.Parse(aParts[0]));
                            }
                        }
                        else
                        {
                            //get mBlock
                            oCC = m_oSegment.Nodes.GetContentControls(xTagID)[0];
                        }
                    }
                    else
                    {
                        //get Word tag
                        if (bInclude)
                        {
                            //get containing mSEG
                            oWordTag = m_oSegment.Nodes.GetWordTag(m_oSegment.FullTagID,
                                Int32.Parse(aParts[0]));

                            if (oWordTag == null) //JTS 7/17/13
                            {
                                //attempt to get mSEG bookmark
                                oBmk = m_oSegment.Nodes.GetBookmark(m_oSegment.FullTagID,
                                    Int32.Parse(aParts[0]));
                            }
                        }
                        else
                            //get mBlock
                            oWordTag = m_oSegment.Nodes.GetWordTags(xTagID)[0];
                    }

                    //run action
                    try
                    {
                        //10.2 - JTS 3/29/10
                        if (bContentControls)
                        {
                            if (bInclude)
                            {
                                bool bRestored = false;

                                if (oCC != null)
                                {
                                    bRestored = LMP.Forte.MSWord.WordDoc.IncludeBlock_CC(m_oDocument, oCC, xSegmentXML,
                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
                                }
                                else
                                {
                                    bRestored = LMP.Forte.MSWord.WordDoc.IncludeBlock_Bmk(m_oDocument, oBmk, xSegmentXML,
                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
                                }

                                if (!bRestored)
                                {
                                    //GLOG 6045 (dm) - block not restored due to incompatible graphic in xml -
                                    //recreate segment
                                    Segment.Replace(m_oSegment.ID1.ToString(), m_oSegment.ID1.ToString(),
                                        m_oSegment.Parent, m_oForteDocument, new Prefill(m_oSegment), "", false);

                                    return;
                                }
                            }
                            else
                            {
                                LMP.Forte.MSWord.WordDoc.ExcludeBlock_CC(m_oDocument, (Word.ContentControl)oCC, xBlockName,
                                    ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags);
                            }
                        }
                        else
                        {
                            if (bInclude)
                            {
                                if (oWordTag != null)
                                {
                                    LMP.Forte.MSWord.WordDoc.IncludeBlock(m_oDocument, oWordTag, xSegmentXML, xBlockName,
                                        oInsertionLocation, ref xVariablesInScope, ref oTags);
                                }
                                else
                                {
                                    LMP.Forte.MSWord.WordDoc.IncludeBlock_Bmk(m_oDocument, oBmk, xSegmentXML,
                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
                                }
                            }
                            else
                            {
                                LMP.Forte.MSWord.WordDoc.ExcludeBlock(m_oDocument, oWordTag, xBlockName,
                                    ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags);
                            }
                        }
                    }
                    catch (System.Exception oE)
                    {
                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                        if (iErr == mpNoDeleteScopeLocationException)
                        {
                            //TODO: prompt user for location and reexecute action -
                            //for now, just offer to delete the executing variable
                            string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (oDR == DialogResult.Yes)
                                m_oSegment.Variables.Delete(m_oVariable);
                            break;
                        }
                        else
                        {
                            //rethrow
                            throw oE;
                        }
                    }
                    finally
                    {
                        //reenable event handler
                        //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
                    }

                    //update segment
                    bool bDeleted = ((!bInclude) && (!bIgnoreDeleteScope));
                    if (bDeleted)
                    {
                        //delete block
                        Block oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
                        m_oSegment.Blocks.Delete(oBlock, false);
                    }

                    //refresh variables collection if tags have been added or deleted
                    RefreshVariables(xVariablesInScope, bDeleted, true);

                    if (!bDeleted)
                    {
                        //add block
                        Block oBlock = null;
                        try
                        {
                            oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
                        }
                        catch { }
                        if (oBlock == null)
                        {
                            //create new block
                            string xDef = m_oSegment.Nodes.GetItemObjectData(xTagID).Substring(1);
                            oBlock = m_oSegment.Blocks.GetBlockFromDefinition(xDef);
                            oBlock.SegmentName = m_oSegment.FullTagID;
                            oBlock.TagID = xTagID;
                            oBlock.TagParentPartNumber = aParts[0];
                            m_oSegment.Blocks.Save(oBlock);
                        }
                        else
                        {
                            //block has already been added as part of refresh
                            //for the added variables in the scope - just notify ui
                            m_oForteDocument.RaiseBlockAddedEvent(oBlock);
                        }
                    }
                }
                #region *********************old code*********************
                //if (!bDeleted)
                //{
                //    //add block
                //    if (string.IsNullOrEmpty(xVariablesInScope))
                //    {
                //        //create new block
                //        string xDef = m_oSegment.Nodes.GetItemObjectData(xTagID).Substring(1);
                //        Block oBlock = m_oSegment.Blocks.GetBlockFromDefinition(xDef);
                //        oBlock.SegmentName = m_oSegment.Name;
                //        oBlock.TagID = xTagID;
                //        oBlock.TagParentPartNumber = aParts[0];
                //        m_oSegment.Blocks.Save(oBlock);
                //    }
                //    else
                //    {
                //        //block has already been added as part of refresh
                //        //for the added variables in the scope - just notify ui
                //        Block oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
                //        m_oForteDocument.RaiseBlockAddedEvent(oBlock);
                //    }
                //}
                #endregion

            }

            LMP.Benchmarks.Print(t0, m_oVariable.Name);
        }
//        private void IncludeExcludeBlocks(string xValue, string xParameters,
//           bool bIgnoreDeleteScope)
//        {
//            //GLOG - 3395 - CEH
//
//            string xBlockName = "";
//            string xExpression = "";
//            string xName = "";
//            string[] aNames = null;

//            if (xParameters == "")
//                //parameters are required for this action
//                throw new LMP.Exceptions.ArgumentException(
//                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

//            //parse parameter string
//            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

//            if (aParams.Length != 1 && aParams.Length != 2)
//                //2 parameters are used, but als allow 1 for backward compatibility
//                throw new LMP.Exceptions.ArgumentException(
//                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
//                    xParameters);
//            else
//            {
//                try
//                {
//                    //GLOG - 3395 - CEH
//                    //Allow for more multiple Block names
//                    //get parameters
//                    xName = aParams[0];
//                    //Can be list of Block names
//                    aNames = xName.Split(',');

//                    if (aParams.Length == 2)
//                        xExpression = aParams[1];
//                }
//                catch (System.Exception oE)
//                {
//                    //one of the parameters is invalid
//                    throw new LMP.Exceptions.ArgumentException(
//                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
//                        xParameters, oE);
//                }
//            }

//            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
//                "xExpression", xExpression);

//            if (xExpression != "")
//                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

//            //exit if value of assignment is mpUndefined
//            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
//                return;

//            oMPDoc.SetDynamicEditingEnvironment(m_oDocument);

//            //standalone variables cannot have expanded delete scopes
//            if (m_oSegment == null)
//                return;

//            //set other argument values
//            bool bInclude = string.IsNullOrEmpty(xValue) ? false : (bIgnoreDeleteScope || String.ToBoolean(xValue));
//            Word.Range oInsertionLocation = null;
//            string xVariablesInScope = "";
//            //10.2 - JTS 3/29/10
//            bool bContentControls = (m_oForteDocument.FileFormat ==
//                LMP.Data.mpFileFormats.OpenXML);
//            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
//            string xSegmentXML = "";
//            if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
//            {
//                //for pleading tables, use xml of parent
//                if (m_oSegment.Parent.HasDefinition)
//                    xSegmentXML = m_oSegment.Parent.Definition.XML;
//            }
//            else if (m_oSegment.HasDefinition)
//                xSegmentXML = m_oSegment.Definition.XML;

//            //GLOG - 3395 - CEH
//            //Cycle through each block name
//            foreach (string xItem in aNames)
//            {
//                string[] aBlocks;
//                //GLOG 6266: Support * wildcard for Block names
//                if (xItem.Contains("*"))
//                {
//                    string xMatchingBlocks = "";
//                    //Check for both existing and deleted Blocks that match name
//                    //Check only Blocks that are not currently deleted
//                    ReadOnlyNodeStore oBlocks = m_oSegment.Nodes.GetNodes(@"//Node[(@ElementName='mBlock') or 
//                                (@ElementName='mDel')][contains(@TagID, '" + xItem.Replace("*", "") + "')]");

//                    for (int b = 0; b < oBlocks.Count; b++)
//                    {
//                        string xBlockTagID = oBlocks[b].Attributes["TagID"].Value;
//                        xBlockTagID = xBlockTagID.Substring(xBlockTagID.LastIndexOf(".") + 1);
//                        xMatchingBlocks = xMatchingBlocks + xBlockTagID + ",";
//                    }
//                    xMatchingBlocks = xMatchingBlocks.TrimEnd(',');
//                    aBlocks = xMatchingBlocks.Split(',');
//                }
//                else
//                {
//                    aBlocks = new string[] { xItem };
//                }
//                foreach (string xVar in aBlocks)
//                {
//                    xBlockName = Expression.Evaluate(xVar, m_oSegment,
//                        m_oForteDocument);

//                    //get tag type
//                    string xTagID = m_oSegment.FullTagID + '.' + xBlockName;
//                    string xTagType = "";
//                    try
//                    {
//                        xTagType = m_oSegment.Nodes.GetItemElementName(xTagID);
//                    }
//                    catch { }
//                    if (xTagType == "")
//                    {
//                        if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
//                        {
//                            //the target tag has been deleted -
//                            //prompt user and offer to delete the executing variable
//                            string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
//                            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
//                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
//                            if (oDR == DialogResult.Yes)
//                                m_oSegment.Variables.Delete(m_oVariable);
//                        }
//                        break;
//                    }

//                    //exit if nothing to do
//                    if ((bInclude && (xTagType == "mBlock")) ||
//                        ((!bInclude) && (xTagType == "mDel")))
//                        break;

//                    //get mSEG part number
//                    string xParts = m_oSegment.Nodes.GetItemPartNumbers(xTagID);
//                    string[] aParts = xParts.Split(';');

//                    Word.XMLNode oWordTag = null;
//                    Word.ContentControl oCC = null;
//                    Word.Bookmark oBmk = null;

//                    if (bContentControls)
//                    {
//                        //get Word tag
//                        if (bInclude)
//                        {
//                            //get containing mSEG
//                            oCC = m_oSegment.Nodes.GetContentControl(m_oSegment.FullTagID,
//                                Int32.Parse(aParts[0]));

//                            if (oCC == null)
//                            {
//                                //attempt to get mSEG bookmark
//                                oBmk = m_oSegment.Nodes.GetBookmark(m_oSegment.FullTagID,
//                                    Int32.Parse(aParts[0]));
//                            }
//                        }
//                        else
//                        {
//                            //get mBlock
//                            oCC = m_oSegment.Nodes.GetContentControls(xTagID)[0];
//                        }
//                    }
//                    else
//                    {
//                        //get Word tag
//                        if (bInclude)
//                        {
//                            //get containing mSEG
//                            oWordTag = m_oSegment.Nodes.GetWordTag(m_oSegment.FullTagID,
//                                Int32.Parse(aParts[0]));

//                            if (oCC == null)
//                            {
//                                //attempt to get mSEG bookmark
//                                oBmk = m_oSegment.Nodes.GetBookmark(m_oSegment.FullTagID,
//                                    Int32.Parse(aParts[0]));
//                            }
//                        }
//                        else
//                            //get mBlock
//                            oWordTag = m_oSegment.Nodes.GetWordTags(xTagID)[0];
//                    }

//                    //run action
//                    try
//                    {
//                        //10.2 - JTS 3/29/10
//                        if (bContentControls)
//                        {
//                            if (bInclude)
//                            {
//                                bool bRestored = false;

//                                if (oCC != null)
//                                {
//                                    bRestored = oMPDoc.IncludeBlock_CC(m_oDocument, oCC, xSegmentXML,
//                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
//                                }
//                                else
//                                {
//                                    bRestored = oMPDoc.IncludeBlock_Bmk(m_oDocument, oBmk, xSegmentXML,
//                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
//                                }

//                                if (!bRestored)
//                                {
//                                    //GLOG 6045 (dm) - block not restored due to incompatible graphic in xml -
//                                    //recreate segment
//                                    Segment.Replace(m_oSegment.ID1.ToString(), m_oSegment.ID1.ToString(),
//                                        m_oSegment.Parent, m_oForteDocument, new Prefill(m_oSegment), "", false);
//                                    return;
//                                }
//                            }
//                            else
//                                oMPDoc.ExcludeBlock_CC(m_oDocument, (Word.ContentControl)oCC, xBlockName,
//                                    ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags);
//                        }
//                        else
//                        {
//                            if (bInclude)
//                            {
//                                if (oWordTag != null)
//                                {
//                                    oMPDoc.IncludeBlock(m_oDocument, oWordTag, xSegmentXML, xBlockName,
//                                        oInsertionLocation, ref xVariablesInScope, ref oTags);
//                                }
//                                else
//                                {
//                                    oMPDoc.IncludeBlock_Bmk(m_oDocument, oBmk, xSegmentXML,
//                                         xBlockName, oInsertionLocation, ref xVariablesInScope, ref oTags);
//                                }
//                            }
//                            else
//                                oMPDoc.ExcludeBlock(m_oDocument, oWordTag, xBlockName,
//                                    ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags);
//                        }
//                    }
//                    catch (System.Exception oE)
//                    {
//                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
//                        if (iErr == mpNoDeleteScopeLocationException)
//                        {
//                            //TODO: prompt user for location and reexecute action -
//                            //for now, just offer to delete the executing variable
//                            string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
//                            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
//                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
//                            if (oDR == DialogResult.Yes)
//                                m_oSegment.Variables.Delete(m_oVariable);
//                            break;
//                        }
//                        else
//                        {
//                            //rethrow
//                            throw;
//                        }
//                    }
//                    finally
//                    {
//                        //reenable event handler
//                        //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
//                    }

//                    //update segment
//                    bool bDeleted = ((!bInclude) && (!bIgnoreDeleteScope));
//                    if (bDeleted)
//                    {
//                        //delete block
//                        Block oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
//                        m_oSegment.Blocks.Delete(oBlock, false);
//                    }

//                    //refresh variables collection if tags have been added or deleted
//                    RefreshVariables(xVariablesInScope, bDeleted, true);

//                    if (!bDeleted)
//                    {
//                        //add block
//                        Block oBlock = null;
//                        try
//                        {
//                            oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
//                        }
//                        catch { }
//                        if (oBlock == null)
//                        {
//                            //create new block
//                            string xDef = m_oSegment.Nodes.GetItemObjectData(xTagID).Substring(1);
//                            oBlock = m_oSegment.Blocks.GetBlockFromDefinition(xDef);
//                            oBlock.SegmentName = m_oSegment.FullTagID;
//                            oBlock.TagID = xTagID;
//                            oBlock.TagParentPartNumber = aParts[0];
//                            m_oSegment.Blocks.Save(oBlock);
//                        }
//                        else
//                        {
//                            //block has already been added as part of refresh
//                            //for the added variables in the scope - just notify ui
//                            m_oForteDocument.RaiseBlockAddedEvent(oBlock);
//                        }
//                    }
//                }
//                #region *********************old code*********************
//                //if (!bDeleted)
//                //{
//                //    //add block
//                //    if (string.IsNullOrEmpty(xVariablesInScope))
//                //    {
//                //        //create new block
//                //        string xDef = m_oSegment.Nodes.GetItemObjectData(xTagID).Substring(1);
//                //        Block oBlock = m_oSegment.Blocks.GetBlockFromDefinition(xDef);
//                //        oBlock.SegmentName = m_oSegment.Name;
//                //        oBlock.TagID = xTagID;
//                //        oBlock.TagParentPartNumber = aParts[0];
//                //        m_oSegment.Blocks.Save(oBlock);
//                //    }
//                //    else
//                //    {
//                //        //block has already been added as part of refresh
//                //        //for the added variables in the scope - just notify ui
//                //        Block oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
//                //        m_oForteDocument.RaiseBlockAddedEvent(oBlock);
//                //    }
//                //}
//                #endregion

//            }

//        }

        /// <summary>
        /// inserts date/time at tag containing the definition for this variable
        /// if xFormat is a valid date format, current date will be inserted,
        /// and if xFormat contains /F switch, inserts date/time as a field.
        /// If xFormat is not a date format, it will be inserted as literal text using InsertText
        /// </summary>
        /// <param name="xFormat"></param>
        /// <param name="xParameters">shUnderlineLength, iDeleteScope, xPrefix, xExp</param>
        private void InsertDate(string xFormat, string xParameters,
            bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            bool bAsField = false;
            short shUnderlineLength = 0;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
            string xPrefix = "";
            string xExpression = "";
            string xDate = "";
            short shLCID = 0;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 4)
                //4 parameters are required - but 4 are also accepted for backward compatibility
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    shUnderlineLength = short.Parse(Expression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument));
                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    xPrefix = Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);
                    xExpression = aParams[3];
                    if (m_oSegment.SupportsMultipleLanguages)
                    {
                        shLCID = (short)m_oSegment.Culture;

                        if (shLCID <= 0)
                        {
                            //undefined LCID - segment culture 
                            //is a macpac "bilingual" language -
                            //parse LCID from 3rd character on of culture id
                            try
                            {
                                string xCulture = Math.Abs(m_oSegment.Culture).ToString().Substring(2);
                                shLCID = short.Parse(xCulture);
                            }
                            catch
                            {
                                throw new LMP.Exceptions.CultureIDException(
                                    LMP.Resources.GetLangString("Error_InvalidBilingualID") + " (" + shLCID.ToString() + ")");
                            }
                        }
                    }
                    else
                    {
                        //GLOG 4757: If Language control is not enabled, 
                        //set shLCID to 0 to use language setting at Tag location
                        shLCID = 0;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xFormat", xFormat, "shUnderlineLength",
                shUnderlineLength, "iDeleteScope", iDeleteScope, "xPrefix",
                xPrefix, "xExpression", xExpression, "xDate", xDate, "shLCID", shLCID);

            if (xExpression != "")
                xFormat = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xFormat == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            m_oForteDocument.SetDynamicEditingEnvironment();

            //handle field switch
            int iPos = xFormat.ToUpper().IndexOf(" /F");
            if (iPos != -1)
            {
                bAsField = true;
                xFormat = xFormat.Remove(iPos, 3);
            }

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            //set argument values
            Object oNodesArray = null;
            bool bIsSegNode = false;
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);

            //JTS 1/11/16:  10.5 code was never adjusted to deal with Bookmark segment bounding objects
            if (!bContentControls)
            {
                //xml tags
                Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;

                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (oNodes[0].BaseName == "mSEG");
                }
            }
            else
            {
                //content controls
                Word.ContentControl[] oNodes = m_oVariable.ContainingContentControls;

                if (oNodes == null || oNodes.Length == 0)
                {
                    Word.Bookmark[] oBmkNodes = m_oVariable.ContainingBookmarks;
                    oNodesArray = oBmkNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oBmkNodes[0].Name.Substring(1)) == "mSEG");
                }
                else
                {
                    oNodesArray = oNodes;
                    bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oNodes[0].Tag) == "mSEG");
                }
            }

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xFormat == "") && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || ((xFormat == "") && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            object missing = System.Reflection.Missing.Value;

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert text
            bool bRefreshRequired = false;
            try
            {
                if (LMP.Architect.Base.Application.IsValidDateFormat(xFormat))
                {

                    //insert as date
                    if (!bIsSegNode)
                    {
                        if (bContentControls)
                            bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertFormattedCurrentDate_CC(m_oDocument,
                                ref oNodesArray, xFormat, bAsField, xPrefix, shUnderlineLength,
                                iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                                ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                                ref missing, ref shLCID);
                        else
                            bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertFormattedCurrentDate(m_oDocument,
                                ref oNodesArray, xFormat, bAsField, xPrefix, shUnderlineLength,
                                iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                                ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                                ref missing, ref shLCID);
                    }
                    else
                    {
                        LMP.Forte.MSWord.WordDoc.InsertFormattedCurrentDate_RestoreScope(m_oDocument,
                            ref oNodesArray, xFormat, bAsField, xPrefix, shUnderlineLength,
                            iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                            ref xVariablesInScope, ref oTags);
                    }
                }
                else
                    //insert as literal text
                    if (!bIsSegNode)
                    {
                        if (bContentControls)
                            bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText_CC(m_oDocument, ref oNodesArray,
                                xFormat, "", shUnderlineLength, iDeleteScope, xSegmentXML,
                                m_oVariable.Name, oInsertionLocation, ref bIgnoreDeleteScope,
                                ref xVariablesInScope, ref oTags, ref missing, "");
                        else
                            bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertText(m_oDocument, ref oNodesArray,
                                xFormat, "", shUnderlineLength, iDeleteScope, xSegmentXML,
                                m_oVariable.Name, oInsertionLocation, ref bIgnoreDeleteScope,
                                ref xVariablesInScope, ref oTags, ref missing, "");
                    }
                    else
                    {
                        LMP.Forte.MSWord.WordDoc.InsertText_RestoreScope(m_oDocument, ref oNodesArray,
                            xFormat, "", shUnderlineLength, iDeleteScope, xSegmentXML,
                            m_oVariable.Name, oInsertionLocation, ref xVariablesInScope,
                            ref oTags);
                    }
            }
            catch (System.Exception oE)
            {
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw;
                }
            }
            finally
            {
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            if (bExpandedScope)
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = ((xFormat == "") && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh the node store
                m_oSegment.RefreshNodes();

            LMP.Benchmarks.Print(t0);
        }

        //private void InsertDate(string xFormat, string xParameters,
        //   bool bIgnoreDeleteScope)
        //{
        //    bool bAsField = false;
        //    short shUnderlineLength = 0;
        //    LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
        //    string xPrefix = "";
        //    string xExpression = "";
        //    string xDate = "";

        //

        //    if (xParameters == "")
        //        //parameters are required for this action
        //        throw new LMP.Exceptions.ArgumentException(
        //            LMP.Resources.GetLangString("Error_InvalidParameterString"));

        //    //parse parameter string
        //    string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

        //    if (aParams.Length != 4)
        //        //4 parameters are required - but 4 are also accepted for backward compatibility
        //        throw new LMP.Exceptions.ArgumentException(
        //            LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
        //            xParameters);
        //    else
        //    {
        //        try
        //        {
        //            //get parameters
        //            shUnderlineLength = short.Parse(Expression.Evaluate(aParams[0],
        //                m_oSegment, m_oForteDocument));
        //            iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
        //                Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
        //            xPrefix = Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);
        //            xExpression = aParams[3];
        //        }
        //        catch (System.Exception oE)
        //        {
        //            //one of the parameters is invalid
        //            throw new LMP.Exceptions.ArgumentException(
        //                LMP.Resources.GetLangString("Error_InvalidParameterValue") +
        //                xParameters, oE);
        //        }
        //    }

        //    Trace.WriteNameValuePairs("xFormat", xFormat, "shUnderlineLength",
        //        shUnderlineLength, "iDeleteScope", iDeleteScope, "xPrefix",
        //        xPrefix, "xExpression", xExpression, "xDate", xDate);

        //    if (xExpression != "")
        //        xFormat = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

        //    //exit if value of assignment is mpUndefined
        //    if (xFormat == ((int)LMP.mpTriState.Undefined).ToString())
        //        return;

        //    oMPDoc.SetDynamicEditingEnvironment(m_oDocument);

        //    //handle field switch
        //    int iPos = xFormat.ToUpper().IndexOf(" /F");
        //    if (iPos != -1)
        //    {
        //        bAsField = true;
        //        xFormat = xFormat.Remove(iPos, 3);
        //    }

        //    //standalone variables cannot have expanded delete scopes - adjust if necessary
        //    if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
        //        iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

        //    //set other argument values
        //    Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;
        //    Object oNodesArray = oNodes;
        //    bool bIsSegNode = (oNodes[0].BaseName == "mSEG");

        //    //exit if value is empty and scope is already deleted
        //    if (bIsSegNode && (xFormat == "") && !bIgnoreDeleteScope)
        //        return;

        //    bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
        //        (bIsSegNode || ((xFormat == "") && (shUnderlineLength == 0))));

        //    //set other argument values
        //    string xSegmentXML = "";
        //    if (bExpandedScope)
        //    {
        //        if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
        //        {
        //            //for pleading tables, use xml of parent
        //            if (m_oSegment.Parent.HasDefinition)
        //                xSegmentXML = m_oSegment.Parent.Definition.XML;
        //        }
        //        else if (m_oSegment.HasDefinition)
        //            xSegmentXML = m_oSegment.Definition.XML;
        //    }
        //    Word.Range oInsertionLocation = null;
        //    string xVariablesInScope = "";
        //    LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
        //    object missing = System.Reflection.Missing.Value;

        //    //disable event handler that prevents deletion of mDel tags
        //    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

        //    //insert text
        //    bool bRefreshRequired = false;
        //    try
        //    {
        //        if (LMP.Architect.Base.Application.IsValidDateFormat(xFormat))
        //        {
        //            //insert as date
        //            if (!bIsSegNode)
        //            {
        //                bRefreshRequired = oMPDoc.InsertFormattedCurrentDate(m_oDocument,
        //                    ref oNodesArray, xFormat, bAsField, xPrefix, shUnderlineLength,
        //                    iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
        //                    ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
        //                    ref missing, ref iLCID);
        //            }
        //            else
        //            {
        //                oMPDoc.InsertFormattedCurrentDate_RestoreScope(m_oDocument,
        //                    ref oNodesArray, xFormat, bAsField, xPrefix, shUnderlineLength,
        //                    iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
        //                    ref xVariablesInScope, ref oTags);
        //            }
        //        }
        //        else
        //            //insert as literal text
        //            if (!bIsSegNode)
        //            {
        //                bRefreshRequired = oMPDoc.InsertText(m_oDocument, ref oNodesArray,
        //                    xFormat, "", shUnderlineLength, iDeleteScope, xSegmentXML,
        //                    m_oVariable.Name, oInsertionLocation, ref bIgnoreDeleteScope,
        //                    ref xVariablesInScope, ref oTags, ref missing, "");
        //            }
        //            else
        //            {
        //                oMPDoc.InsertText_RestoreScope(m_oDocument, ref oNodesArray,
        //                    xFormat, "", shUnderlineLength, iDeleteScope, xSegmentXML,
        //                    m_oVariable.Name, oInsertionLocation, ref xVariablesInScope,
        //                    ref oTags);
        //            }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
        //        if (iErr == mpNoDeleteScopeLocationException)
        //        {
        //            //TODO: prompt user for location and reexecute action
        //            //for now, just offer to delete the executing variable
        //            string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
        //            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
        //                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //            if (oDR == DialogResult.Yes)
        //                m_oSegment.Variables.Delete(m_oVariable, false, false);
        //            return;
        //        }
        //        else
        //        {
        //            //rethrow
        //            throw;
        //        }
        //    }
        //    finally
        //    {
        //        //reenable event handler
        //        ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
        //    }

        //    if (bExpandedScope)
        //    {
        //        //refresh variables collection if tags have been added or deleted
        //        bool bDeleted = ((xFormat == "") && (!bIgnoreDeleteScope));
        //        RefreshVariables(xVariablesInScope, bDeleted);
        //    }
        //    else if (bRefreshRequired)
        //        //just refresh the node store
        //        m_oSegment.RefreshNodes();
        //}

        /// <summary>
        /// refreshes variables collection after tags have been added or deleted
        /// as the result of the reinsertion or deletion of an expanded delete scope
        /// </summary>
        /// <param name="xVariablesInScope"></param>
        /// <param name="bDeleted"></param>
        private void RefreshVariables(string xVariablesInScope, bool bDeleted)
        {
            this.RefreshVariables(xVariablesInScope, bDeleted, false);
        }

        /// <summary>
        /// refreshes variables and blocks collections after tags have been added or deleted
        /// as the result of the reinsertion or deletion of an expanded delete scope
        /// </summary>
        /// <param name="xVariablesInScope"></param>
        /// <param name="bDeleted"></param>
        /// <param name="bTargetIsBlock"></param>
        private void RefreshVariables(string xVariablesInScope, bool bDeleted,
            bool bTargetIsBlock)
        {
            if (string.IsNullOrEmpty(xVariablesInScope))
            {
                //no other variables in the scope - just refresh the node store
                m_oSegment.RefreshNodes();

                //update variable properties
                //GLOG 4784
                if (!bTargetIsBlock && m_oVariable.TagType != ForteDocument.TagTypes.None)
                {
                    if (!m_oVariable.IsMultiValue)
                    {
                        if (bDeleted)
                        {
                            //associated tag is now an mDel
                            m_oVariable.TagType = ForteDocument.TagTypes.DeletedBlock;
                        }
                        else
                        {
                            //associated tag is now an mVar
                            m_oVariable.TagType = ForteDocument.TagTypes.Variable;
                        }
                    }
                }
            }
            else
            {
                //**********************************************************
                //JTS 4/30/10: This has been restructured so that Variables/Blocks in Scope
                //are added or removed from the collections individually, rather than
                //depending on Refreshing the entire Segment.  With a large number of varaibles
                //this can add a lot of time to Include/Exclude Block and other actions
                //**********************************************************
                //create array of other variables/blocks in scope
                string[] aOtherVars = xVariablesInScope.Split(LMP.StringArray.mpEndOfField);

                //handle other variables/blocks that have been deleted with scope
                //if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                //{
                for (int i = 0; i < aOtherVars.Length; i++)
                {
                    string[] aOtherVar = aOtherVars[i].Split(LMP.StringArray.mpEndOfValue);
                    if (aOtherVar[1] == "0")
                    {
                        if (aOtherVar[0] != "mBlock")
                        {
                            //get variable
                            Variable oVar = m_oSegment.Variables.ItemFromName(aOtherVar[2]);

                            //notify ui that variable has been deleted
                            if (m_oForteDocument.FileFormat ==
                                LMP.Data.mpFileFormats.Binary)
                            {
                                //xml tags
                                Word.XMLNode[] oWordXMLNodes = null;
                                string xBaseName = ""; //GLOG 4609
                                try
                                {
                                    oWordXMLNodes = oVar.AssociatedWordTags;
                                    //GLOG 4609: Test for BaseName of First tag.
                                    //If only tag has been deleted, this will result in an error
                                    //and xBaseName will remain empty.  If other Associated tags
                                    //remain, xBaseName will be set successfully
                                    xBaseName = oWordXMLNodes[0].BaseName;
                                }
                                catch { }
                                //GLOG 4609: Don't raise Variable Deleted Event if there are Tags remaining
                                if (oWordXMLNodes == null ||
                                    (oWordXMLNodes != null && oWordXMLNodes.Length == 1 && xBaseName == ""))
                                    m_oSegment.Variables.Delete(oVar, false, false, false);
                                //m_oForteDocument.RaiseVariableDeletedEvent(oVar);
                            }
                            else
                            {
                                //content controls
                                //dm 5-11-10 - modified to match GLOG 4609
                                //changes made to xml tag block above
                                Word.ContentControl[] oCCs = null;
                                string xTag = "";
                                try
                                {
                                    oCCs = oVar.AssociatedContentControls;
                                    xTag = oCCs[0].Tag;
                                }
                                catch { }
                                if (oCCs == null ||
                                    (oCCs != null && oCCs.Length == 1 && string.IsNullOrEmpty(xTag))) //GLOG 6768
                                    m_oSegment.Variables.Delete(oVar, false, false, false);
                                //m_oForteDocument.RaiseVariableDeletedEvent(oVar);
                            }
                        }
                        else
                        {
                            //get block
                            Block oBlock = m_oSegment.Blocks.ItemFromName(aOtherVar[2]);
                            m_oSegment.Blocks.Delete(oBlock, false);
                            //notify ui that block has been deleted
                            //m_oForteDocument.RaiseBlockDeletedEvent(oBlock);
                        }
                    }
                }
                //}

                //store name of this variable
                string xPrimaryVar = m_oVariable.Name;

                //JTS 5/10/10: If called from IncludeExcludBlock action, just Refresh Nodes
                if (!bTargetIsBlock)
                {
                    //refresh segment
                    m_oSegment.Refresh(Segment.RefreshTypes.NoChildSegments);
                    //get new primary variable
                    Variable oPrimaryVar = m_oSegment.Variables.ItemFromName(xPrimaryVar);

                    //notify ui that segment has been refreshed
                    m_oForteDocument.RaiseSegmentRefreshedByActionEvent(oPrimaryVar.Segment);
                }
                else
                {
                    m_oSegment.RefreshNodes();
                }

                //handle other variables/blocks that have been added with scope
                //JTS 4/30/10: Add all Variables first, before running actions
                //One added variable may reference another in an expression
                for (int i = 0; i < aOtherVars.Length; i++)
                {
                    string[] aOtherVar = aOtherVars[i].Split(LMP.StringArray.mpEndOfValue);
                    if (aOtherVar[1] == "1")
                    {
                        if (aOtherVar[0] != "mBlock")
                        {
                            //get variable
                            Variable oVar = null;
                            try
                            {
                                oVar = m_oSegment.Variables.ItemFromName(aOtherVar[2]);
                            }
                            catch { }
                            if (oVar == null)
                            {
                                string xTagID = m_oSegment.FullTagID + "." + aOtherVar[2];
                                string xDef = "";
                                if (aOtherVar[0] == "mVar")
                                    xDef = m_oSegment.Nodes.GetItemObjectData(xTagID);
                                else
                                {
                                    //GLOG 5315 (dm) - for mDels, get def from deleted scopes xml
                                    xDef = m_oSegment.Nodes.GetVariableDefFromDeletedScopes(
                                        m_oSegment, aOtherVar[2]);
                                }
                                xDef = xDef.Substring(xDef.IndexOf("VariableDefinition=") + 19);
                                oVar = m_oSegment.Variables.GetVariableFromDefinition(xDef);
                                //get mSEG part number
                                string xParts = m_oSegment.Nodes.GetItemPartNumbers(xTagID);
                                string[] aParts = xParts.Split(';');
                                oVar.SegmentName = m_oSegment.FullTagID;
                                oVar.TagID = xTagID;
                                oVar.TagParentPartNumbers = aParts[0];
                                //GLOG 5315 (dm) - reset tag type if necessary
                                if (aOtherVar[0] == "mDel")
                                    oVar.TagType = ForteDocument.TagTypes.DeletedBlock;
                                m_oSegment.Variables.Save(oVar);

                            }
                            else
                            {
                                //notify ui that variable has been added
                                m_oForteDocument.RaiseVariableAddedEvent(oVar);
                            }
                        }
                        else
                        {
                            Block oBlock = null;
                            try
                            {
                                //get block
                                oBlock = m_oSegment.Blocks.ItemFromName(aOtherVar[2]);
                            }
                            catch { }
                            if (oBlock == null)
                            {
                                string xTagID = m_oSegment.FullTagID + "." + aOtherVar[2];
                                //create new block
                                string xDef = m_oSegment.Nodes.GetItemObjectData(xTagID).Substring(1);
                                oBlock = m_oSegment.Blocks.GetBlockFromDefinition(xDef);
                                //get mSEG part number
                                string xParts = m_oSegment.Nodes.GetItemPartNumbers(xTagID);
                                string[] aParts = xParts.Split(';');
                                oBlock.SegmentName = m_oSegment.FullTagID;
                                oBlock.TagID = xTagID;
                                oBlock.TagParentPartNumber = aParts[0];
                                m_oSegment.Blocks.Save(oBlock);
                            }
                            else
                                //notify ui that block has been added
                                m_oForteDocument.RaiseBlockAddedEvent(oBlock);
                        }
                    }
                }
                if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                {
                    //Now cycle through Variables to run actions
                    for (int i = 0; i < aOtherVars.Length; i++)
                    {
                        string[] aOtherVar = aOtherVars[i].Split(LMP.StringArray.mpEndOfValue);
                        if (aOtherVar[1] == "1")
                        {
                            if (aOtherVar[0] != "mBlock")
                            {

                                Variable oVar = m_oSegment.Variables.ItemFromName(aOtherVar[2]);
                                //if in edit mode and still contains placeholder text,
                                //assign default value w/o deleting expanded scopes
                                //GLOG 6486 (dm) - this should be case insensitive
                                if (oVar.Value.ToUpper() == oVar.DisplayName.ToUpper())
                                    oVar.AssignDefaultValue(true);
                                else if (oVar.HasMultipleTags && !oVar.IsMultiValue)
                                {
                                    //Make sure restored tags match current value
                                    for (int iA = 0; iA < oVar.VariableActions.Count; iA++)
                                    {
                                        VariableActions.Types iType = oVar.VariableActions[iA].Type;

                                        if (iType == VariableActions.Types.InsertDetail ||
                                            iType == VariableActions.Types.InsertText ||
                                            iType == VariableActions.Types.InsertBarCode ||
                                            iType == VariableActions.Types.InsertCheckbox ||
                                            iType == VariableActions.Types.InsertDate ||
                                            iType == VariableActions.Types.InsertReline ||
                                            iType == VariableActions.Types.IncludeExcludeText)
                                        {
                                            oVar.VariableActions[iA].Execute();
                                            break;
                                        }
                                    }
                                }
                                else if (oVar.IsLanguageDependent)
                                {
                                    //GLOG 3528: Update restored Variable for current language
                                    string xExp = oVar.DefaultValue;
                                    if (Expression.ContainsPreferenceCode(xExp) ||
                                        (xExp.IndexOf("[SegmentLanguage") > -1))
                                    {
                                        //default value contains a language or pref code -
                                        //reset variable value
                                        string xDefault = Expression.Evaluate(xExp, m_oSegment,
                                            m_oForteDocument);
                                        if (xDefault != "null")
                                            oVar.SetValue(xDefault, false);
                                    }
                                    oVar.VariableActions.Execute();
                                }
                                else if (oVar.IsAuthorRelated)
                                {
                                    //GLOG 3528: Update restored Variable for current authors
                                    string xExp = oVar.DefaultValue;
                                    if (Expression.ContainsPreferenceCode(xExp) ||
                                        (Expression.ContainsAuthorCode(xExp, -1, -2)))
                                    {
                                        //default value contains an Author or Preference
                                        string xDefault = Expression.Evaluate(xExp, m_oSegment,
                                            m_oForteDocument);
                                        if (xDefault != "null")
                                            oVar.SetValue(xDefault, false);
                                    }
                                    oVar.VariableActions.Execute();
                                }
                            }
                        }
                    }
                }
                #region *********************old code*********************
                ////create array of other variables/blocks in scope
                //string[] aOtherVars = xVariablesInScope.Split(LMP.StringArray.mpEndOfField);

                ////handle other variables/blocks that have been deleted with scope
                //if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                //{
                //    for (int i = 0; i < aOtherVars.Length; i++)
                //    {
                //        string[] aOtherVar = aOtherVars[i].Split(LMP.StringArray.mpEndOfValue);
                //        if (aOtherVar[1] == "0")
                //        {
                //            if (aOtherVar[0] != "mBlock")
                //            {
                //                //get variable
                //                Variable oVar = m_oSegment.Variables.ItemFromName(aOtherVar[2]);

                //                //notify ui that variable has been deleted
                //                if (m_oForteDocument.BoundingObjectType ==
                //                    LMP.Data.BoundingObjectTypes.WordXMLNodes)
                //                {
                //                    //xml tags
                //                    Word.XMLNode[] oWordXMLNodes = null;
                //                    string xBaseName = ""; //GLOG 4609
                //                    try
                //                    {
                //                        oWordXMLNodes = oVar.AssociatedWordTags;
                //                        //GLOG 4609: Test for BaseName of First tag.
                //                        //If only tag has been deleted, this will result in an error
                //                        //and xBaseName will remain empty.  If other Associated tags
                //                        //remain, xBaseName will be set successfully
                //                        xBaseName = oWordXMLNodes[0].BaseName;
                //                    }
                //                    catch { }
                //                    //GLOG 4609: Don't raise Variable Deleted Event if there are Tags remaining
                //                    if (oWordXMLNodes == null ||
                //                        (oWordXMLNodes != null && oWordXMLNodes.Length == 1 && xBaseName == ""))
                //                        m_oForteDocument.RaiseVariableDeletedEvent(oVar);
                //                }
                //                else
                //                {
                //                    //content controls
                //                    Word.ContentControl[] oCCs = null;
                //                    try
                //                    {
                //                        oCCs = oVar.AssociatedContentControls;
                //                    }
                //                    catch { }
                //                    if ((oCCs != null) && (oCCs.Length == 1))
                //                        m_oForteDocument.RaiseVariableDeletedEvent(oVar);
                //                }
                //            }
                //            else
                //            {
                //                //get block
                //                Block oBlock = m_oSegment.Blocks.ItemFromName(aOtherVar[2]);

                //                //notify ui that block has been deleted
                //                m_oForteDocument.RaiseBlockDeletedEvent(oBlock);
                //            }
                //        }
                //    }
                //}

                ////store name of this variable
                //string xPrimaryVar = m_oVariable.Name;

                ////refresh segment
                //m_oSegment.Refresh(Segment.RefreshTypes.NoChildSegments);

                ////get new primary variable
                //Variable oPrimaryVar = m_oSegment.Variables.ItemFromName(xPrimaryVar);

                ////notify ui that segment has been refreshed
                //m_oForteDocument.RaiseSegmentRefreshedByActionEvent(oPrimaryVar.Segment);

                ////handle other variables/blocks that have been added with scope
                //if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                //{
                //    for (int i = 0; i < aOtherVars.Length; i++)
                //    {
                //        string[] aOtherVar = aOtherVars[i].Split(LMP.StringArray.mpEndOfValue);
                //        if (aOtherVar[1] == "1")
                //        {
                //            if (aOtherVar[0] != "mBlock")
                //            {
                //                //get variable
                //                Variable oVar = m_oSegment.Variables.ItemFromName(aOtherVar[2]);

                //                //if in edit mode and still contains placeholder text,
                //                //assign default value w/o deleting expanded scopes
                //                if (oVar.Value == oVar.DisplayName)
                //                    oVar.AssignDefaultValue(true);
                //                else if (oVar.HasMultipleTags && !oVar.IsMultiValue)
                //                {
                //                    //Make sure restored tags match current value
                //                    for (int iA = 0; iA < oVar.VariableActions.Count; iA++)
                //                    {
                //                        VariableActions.Types iType = oVar.VariableActions[iA].Type;

                //                        if (iType == VariableActions.Types.InsertDetail ||
                //                            iType == VariableActions.Types.InsertText ||
                //                            iType == VariableActions.Types.InsertBarCode ||
                //                            iType == VariableActions.Types.InsertCheckbox ||
                //                            iType == VariableActions.Types.InsertDate ||
                //                            iType == VariableActions.Types.InsertReline)
                //                        {
                //                            oVar.VariableActions[iA].Execute();
                //                            break;
                //                        }
                //                    }
                //                }
                //                else if (oVar.IsLanguageDependent)
                //                {
                //                    //GLOG 3528: Update restored Variable for current language
                //                    string xExp = oVar.DefaultValue;
                //                    if (Expression.ContainsPreferenceCode(xExp) ||
                //                        (xExp.IndexOf("[SegmentLanguage") > -1))
                //                    {
                //                        //default value contains a language or pref code -
                //                        //reset variable value
                //                        string xDefault = Expression.Evaluate(xExp, m_oSegment,
                //                            m_oForteDocument);
                //                        if (xDefault != "null")
                //                            oVar.SetValue(xDefault, false);
                //                    }
                //                    oVar.VariableActions.Execute();
                //                }
                //                else if (oVar.IsAuthorRelated)
                //                {
                //                    //GLOG 3528: Update restored Variable for current authors
                //                    string xExp = oVar.DefaultValue;
                //                    if (Expression.ContainsPreferenceCode(xExp) ||
                //                        (Expression.ContainsAuthorCode(xExp, -1, -2)))
                //                    {
                //                        //default value contains an Author or Preference
                //                        string xDefault = Expression.Evaluate(xExp, m_oSegment,
                //                            m_oForteDocument);
                //                        if (xDefault != "null")
                //                            oVar.SetValue(xDefault, false);
                //                    }
                //                    oVar.VariableActions.Execute();
                //                }
                //                //notify ui that variable has been added
                //                m_oForteDocument.RaiseVariableAddedEvent(oVar);
                //            }
                //            else
                //            {
                //                //get block
                //                Block oBlock = m_oSegment.Blocks.ItemFromName(aOtherVar[2]);

                //                //notify ui that block has been added
                //                m_oForteDocument.RaiseBlockAddedEvent(oBlock);
                //            }
                //        }
                //    }
                //}
                #endregion
            }
        }

        /// <summary>
        /// inserts specified text in a table with the specified number of columns
        /// at the tag containing the definition for this variable
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xItemSeparator, shUnderlineLength, iDeleteScope, shNumCols, xExp</param>
        private void InsertTextAsTable(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            short shUnderlineLength = 0;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;
            string xExpression = "";
            string xItemSeparator = "";
            short shNumCols = 0;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 5)
                //5 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xItemSeparator = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    shUnderlineLength = short.Parse(Expression.Evaluate(
                        aParams[1], m_oSegment, m_oForteDocument));
                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                    shNumCols = short.Parse(Expression.Evaluate(
                        aParams[3], m_oSegment, m_oForteDocument));
                    xExpression = aParams[4];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xItemSeperator",
                xItemSeparator, "shUnderlineLength", shUnderlineLength, "iDeleteScope",
                iDeleteScope, "shNumCols", shNumCols, "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

            //set argument values -
            //TODO: 10.2 branch for CC support
            Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;
            Object oNodesArray = oNodes;
            bool bIsSegNode = (oNodes[0].BaseName == "mSEG");

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xValue == "") && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                (bIsSegNode || ((xValue == "") && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            object missing = System.Reflection.Missing.Value;

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert
            bool bRefreshRequired = false;
            try
            {
                if (!bIsSegNode)
                {
                    bRefreshRequired = LMP.Forte.MSWord.WordDoc.InsertTextAsTable(m_oDocument, ref oNodesArray,
                        xValue, xItemSeparator, shNumCols, shUnderlineLength, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, oInsertionLocation,
                        ref bIgnoreDeleteScope, ref xVariablesInScope, ref oTags,
                        ref missing);
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.InsertTextAsTable_RestoreScope(m_oDocument, ref oNodesArray,
                        xValue, xItemSeparator, shNumCols, shUnderlineLength,
                        iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
                        ref xVariablesInScope, ref oTags);
                }
            }
            catch (System.Exception oE)
            {
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw;
                }
            }
            finally
            {
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            if (bExpandedScope)
            {
                //refresh variables collection if tags have been added or deleted
                bool bDeleted = ((xValue == "") && (!bIgnoreDeleteScope));
                RefreshVariables(xVariablesInScope, bDeleted);
            }
            else if (bRefreshRequired)
                //just refresh the node store
                m_oSegment.RefreshNodes();
        }

        /// <summary>
        /// inserts a checked or unchecked macrobutton that runs zzmpToggleCheckbox
        /// </summary>
        /// <param name="xValue"></param>
        private void InsertCheckbox(string xValue, string xParameters)
        {
            string xFont = "Wingdings";
            //string xCheckedASCII = "";
            //string xUnCheckedASCII = "";
            string xChar = "";

            Trace.WriteNameValuePairs("xValue", xValue);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;
            //10.2 - JTS 3/29/10
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);

            Word.XMLNode[] oNodes = null;
            Word.ContentControl[] oCCs = null;
            if (bContentControls)
            {
                oCCs = m_oVariable.ContainingContentControls;
            }
            else
            {
                oNodes = m_oVariable.ContainingWordTags;
            }

            xChar = (xValue.ToUpper() == "TRUE") ? ((char)253).ToString() : ((char)168).ToString();

            #region OLD
            ////parse parameter string
            //string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            //if (aParams.Length != 3)
            //    //3 parameters are required
            //    throw new LMP.Exceptions.ArgumentException(
            //        LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
            //        xParameters);
            //else
            //{
            //    try
            //    {
            //        //get parameters
            //        xFont = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
            //        xCheckedASCII = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
            //        xUnCheckedASCII = Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);
            //    }
            //    catch (System.Exception oE)
            //    {
            //        //one of the parameters is invalid
            //        throw new LMP.Exceptions.ArgumentException(
            //            LMP.Resources.GetLangString("Error_InvalidParameterValue") +
            //            xParameters, oE);
            //    }
            //}

            //Trace.WriteNameValuePairs("xValue", xValue, "xFont", xFont, "xCheckedASCII", 
            //    xCheckedASCII, "xUncheckedASCII", xUnCheckedASCII);

            //xChar = bChecked ? ((char)int.Parse(xCheckedASCII)).ToString() : 
            //    ((char) int.Parse(xUnCheckedASCII)).ToString();

            ////get font and ASCII numbers of checkboxes from firm app keyset
            //LMP.Data.KeySet oK = LMP.Data.KeySet.GetFirmAppKeys(LMP.Data.Application.User.ID);

            //if (xFont == "")
            //{
            //    xFont = oK.GetValue("CheckboxFont");

            //    //raise error if data is missing
            //    if (xFont == "")
            //        throw new LMP.Exceptions.DataException(
            //            LMP.Resources.GetLangString("Error_MissingFirmAppKey") + "CheckboxFont");
            //}

            //if (bChecked)
            //{
            //    if (xCheckedASCII == "")
            //    {
            //        xCheckedASCII = oK.GetValue("CheckboxCheckedASCII");

            //        //raise error if data is missing
            //        if (xCheckedASCII == "")
            //            throw new LMP.Exceptions.DataException(
            //                LMP.Resources.GetLangString("Error_MissingFirmAppKey") + "CheckboxCheckedASCII");
            //    }

            //    xChar = ((char)System.Convert.ToInt32(xCheckedASCII)).ToString();
            //}
            //else
            //{
            //    if (xUnCheckedASCII == "")
            //    {
            //        xUnCheckedASCII = oK.GetValue("CheckboxUnCheckedASCII");

            //        //raise error if data is missing
            //        if (xUnCheckedASCII == "")
            //            throw new LMP.Exceptions.DataException(
            //                LMP.Resources.GetLangString("Error_MissingFirmAppKey") + "CheckboxUnCheckedASCII");
            //    }

            //    xChar = ((char)System.Convert.ToInt32(xUnCheckedASCII)).ToString();
            //}
            #endregion

            m_oForteDocument.SetDynamicEditingEnvironment();

            LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            //GLOG 5275: disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
            try
            {
                if (bContentControls)
                {
                    for (int i = 0; i < oCCs.Length; i++)
                    {
                        //GLOG 5802:  Don't run action if Checkbox is already correct value - check stated may have already been set by macrobutton
                        if (xValue.ToUpper() != m_oVariable.GetValueFromSource_CC(oCCs[i]).ToUpper())
                            LMP.Forte.MSWord.WordDoc.InsertCheckbox_CC(m_oDocument, oCCs[i], xChar, xFont, ref oTags);
                    }
                }
                else
                {
                    for (int i = 0; i < oNodes.Length; i++)
                    {
                        //GLOG 5802:  Don't run action if Checkbox is already correct value - check stated may have already been set by macrobutton
                        if (xValue.ToUpper() != m_oVariable.GetValueFromSource(oNodes[i]).ToUpper())
                            LMP.Forte.MSWord.WordDoc.InsertCheckbox(m_oDocument, oNodes[i], xChar, xFont, ref oTags);
                    }
                }
            }
            catch (System.Exception oE)
            {
                //GLOG 5275: use same error handling as other actions
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == mpNoDeleteScopeLocationException)
                {
                    //TODO: prompt user for location and reexecute action
                    //for now, just offer to delete the executing variable
                    string xMsg = LMP.Resources.GetLangString("Prompt_MissingDeleteScopeTarget");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                        m_oSegment.Variables.Delete(m_oVariable, false, false);
                    return;
                }
                else
                {
                    //rethrow
                    throw;
                }
            }
            finally
            {
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }
        }

        /// <summary>
        /// NOTE:  THIS ACTION SHOULD NO LONGER BE AVAILABLE SINCE WORD
        /// NO LONGER SUPPORTS U.S. POSTAL CODES
        /// Inserts a U.S. Postal Barcode above or below each detail address of
        /// Variable specified in Parameters
        /// </summary>
        /// <param name="xValue"></param>
        //private void InsertBarCode(string xParameters)
        //{
        //
        //    string xVariableName = "";
        //    short shPosition = 0;
        //    float ftExtraSpace = 0;

        //    if (xParameters == "")
        //        //parameters are required for this action
        //        throw new LMP.Exceptions.ArgumentException(
        //            LMP.Resources.GetLangString("Error_InvalidParameterString"));

        //    //parse parameter string
        //    string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

        //    if (aParams.Length != 3)
        //        //3 parameters are required
        //        throw new LMP.Exceptions.ArgumentException(
        //            LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
        //            xParameters);
        //    else
        //    {
        //        try
        //        {
        //            //get parameters
        //            xVariableName = aParams[0];
        //            shPosition = short.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
        //            ftExtraSpace = float.Parse(Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
        //        }
        //        catch (System.Exception oE)
        //        {
        //            //one of the parameters is invalid
        //            throw new LMP.Exceptions.ArgumentException(
        //                LMP.Resources.GetLangString("Error_InvalidParameterValue") +
        //                xParameters, oE);
        //        }
        //    }

        //    LMP.Trace.WriteNameValuePairs("xExpression", xVariableName, "shPosition", shPosition, 
        //        "ftExtraSpace", ftExtraSpace);

        //    //get current xml markup state for later restoration
        //    Word.View oView = this.m_oForteDocument.WordDocument.ActiveWindow.View;
        //    int iShowTags = oView.ShowXMLMarkup;

        //    oMPDoc.SetDynamicEditingEnvironment(m_oDocument);
        //    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
        //    //get target segment(s)
        //    try
        //    {
        //        Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableName);
        //        if (oTargets != null)
        //        {
        //            foreach (Segment oTarget in oTargets)
        //            {
        //                //get target variable
        //                Variable oVar = oTarget.GetVariable(xVariableName);

        //                //if variable exists, run action
        //                if (oVar != null && oVar.Value != "" && m_oForteDocument.Mode != ForteDocument.Modes.Design)
        //                {
        //                    if (oVar.ForteDocument.BoundingObjectType == LMP.Data.BoundingObjectTypes.WordXMLNodes)
        //                    {
        //                        Word.XMLNode[] oNodes = oVar.AssociatedWordTags;
        //                        for (int i = 0; i < oNodes.Length; i++)
        //                            oMPDoc.InsertBarCode(oNodes[i], shPosition, ftExtraSpace);
        //                    }
        //                    else
        //                    {
        //                        Word.ContentControls[] oCCs = oVar.AssociatedContentControls;
        //                        for (int i = 0; i < oCCs.Length; i++)
        //                            oMPDoc.InsertBarCode(oCCs[i], shPosition, ftExtraSpace);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        if (iShowTags == 0)
        //            //hide tags, as that was the original state
        //            //of the tags before editing
        //            LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(m_oForteDocument.WordDocument, false);

        //        ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
        //    }
        //}
        /// <summary>
        /// InsertSegment Variable Action
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        /// <param name="bIgnoreDeleteScope"></param>
        private void InsertSegment(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            string xExp = "";
            string xExpRes = "";
            string xSegmentID = "";
            short shUnderlineLength = 0;
            LMP.Forte.MSWord.mpDeleteScopes iDeleteScope = 0;

            //JTS 10/10/21: Don't run this action in Design mode
            if (this.m_oForteDocument.Mode == ForteDocument.Modes.Design)
                return;

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);


            if (aParams.Length != 3)
                //1 parameter required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    shUnderlineLength = short.Parse(Expression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument));
                    iDeleteScope = (LMP.Forte.MSWord.mpDeleteScopes)System.Convert.ToInt32(
                        Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    xExp = aParams[2].ToString();

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }
            Trace.WriteNameValuePairs("xExp", xExp);

            //evaluate expression

            try
            {
                if (!System.String.IsNullOrEmpty(xExp))
                    xExpRes = Expression.Evaluate(
                        xExp, this.m_oSegment, this.m_oForteDocument);
            }
            catch { }

            // Parameter takes precedence over Value for Segment ID
            if (xExp != "")
                xSegmentID = xExpRes;
            else
                xSegmentID = xValue;

            // Remove '.0' from end of Segment ID
            if (xSegmentID.EndsWith(".0"))
            {
                xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);
            }

            if (xSegmentID != "" && xSegmentID != "0")
            {
                //Segment ID must be a numeric
                int iTest = 0;
                if (!Int32.TryParse(xSegmentID, out iTest))
                    return;

                //validate proposed Segment ID
                LMP.Data.AdminSegmentDef oDef =
                    GetAdminSegmentDefinition(xSegmentID);

                //Not a valid Segment ID
                if (oDef == null)
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString("Msg_SegmentDefinitionDoesNotExist") + xSegmentID + ".");
            }

            //set InsertText argument values
            //InsertText is used to automate process of inserting mVar/mDel
            //and setting appropriate Segment attributes
            bool bIsSegNode = false;

            //GLOG 5219: Content Control support
            //set argument values
            Object oNodesArray = null;
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);
            if (!bContentControls)
            {
                //xml tags
                Word.XMLNode[] oNodes = m_oVariable.ContainingWordTags;
                oNodesArray = oNodes;
                bIsSegNode = (oNodes[0].BaseName == "mSEG");
            }
            else
            {
                //content controls
                Word.ContentControl[] oNodes = m_oVariable.ContainingContentControls;
                oNodesArray = oNodes;
                bIsSegNode = (LMP.String.GetBoundingObjectBaseName(oNodes[0].Tag) == "mSEG");
            }

            bool bExpandedScope = ((iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph) &&
                ((bIsSegNode && ((xSegmentID != "" && xSegmentID != "0") || bIgnoreDeleteScope)) ||
                ((!bIsSegNode) && (xSegmentID == "" || xSegmentID == "0"))) && (shUnderlineLength == 0));

            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is CollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            Word.Range oInsertionLocation = null;
            string xVariablesInScope = "";
            LMP.Forte.MSWord.Tags oTags = this.m_oForteDocument.Tags;
            LMP.Forte.MSWord.WordDoc oMPDoc = new LMP.Forte.MSWord.WordDoc();
            Word.Range oRng = null;
            object missing = System.Reflection.Missing.Value;

            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
            try
            {
                // Look for segment associated with tagless variable
                Segment oSegment = this.m_oVariable.AssociatedSegment;
                if (oSegment != null)
                {
                    //JTS 6/13/13: Use PrimaryRange instead of WordTags or ContentControls collections
                    oRng = oSegment.PrimaryRange;
                    this.m_oForteDocument.DeleteSegment(oSegment, false, false, false);
                }

                if (xSegmentID != "" && xSegmentID != "0")
                {
                    if (this.m_oVariable.TagType == ForteDocument.TagTypes.DeletedBlock)
                    {
                        // Run InsertText to convert back to mVar
                        LMP.Forte.MSWord.WordDoc.InsertText_RestoreScope(this.m_oForteDocument.WordDocument, ref oNodesArray,
                            " ", "", 0, iDeleteScope, xSegmentXML, m_oVariable.Name,
                            oInsertionLocation, ref xVariablesInScope, ref oTags);
                        m_oForteDocument.RefreshTags();
                        this.RefreshVariables(null, false);
                    }
                    if (this.m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        if (this.m_oVariable.ContainingWordTags[0].BaseName == "mVar")
                            oRng = this.m_oVariable.AssociatedWordTags[0].Range.Duplicate;
                    }
                    else
                    {
                        if (String.GetBoundingObjectBaseName(
                            this.m_oVariable.ContainingContentControls[0].Tag) == "mVar")
                            oRng = this.m_oVariable.AssociatedContentControls[0].Range.Duplicate;
                    }

                    this.m_oVariable.SetTagState(false, true, null);
                    Segment oNewSeg = InsertSegmentInRange(xSegmentID, oRng);
                    if (oNewSeg != null)
                    {
                        // Mark segment as belonging to this variable
                        oNewSeg.ParentVariableID = this.m_oVariable.ID;
                        this.m_oForteDocument.RefreshTags();
                        bExpandedScope = true;
                    }
                }
                else if (this.m_oVariable.TagType != ForteDocument.TagTypes.DeletedBlock)
                {
                    // Insert tag where segment previously existed
                    this.m_oVariable.SetTagState(true, false, oRng);
                    this.m_oForteDocument.RefreshTags();
                    this.m_oSegment.RefreshNodes();
                    //standalone variables cannot have expanded delete scopes - adjust if necessary
                    if ((m_oSegment == null) && (iDeleteScope >= LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Paragraph))
                        iDeleteScope = LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target;

                    if (iDeleteScope != LMP.Forte.MSWord.mpDeleteScopes.mpDeleteScope_Target)
                    {
                        // Run InsertText to insert mDel
                        oTags = this.m_oForteDocument.Tags;

                        //GLOG 5219: Content Control support
                        if (!bContentControls)
                        {
                            oNodesArray = m_oVariable.ContainingWordTags;
                            LMP.Forte.MSWord.WordDoc.InsertText(this.m_oForteDocument.WordDocument, ref oNodesArray,
                                "", "", 0, iDeleteScope, xSegmentXML, m_oVariable.Name,
                                oInsertionLocation, ref bIgnoreDeleteScope,
                                ref xVariablesInScope, ref oTags, ref missing, "");
                        }
                        else
                        {
                            oNodesArray = m_oVariable.ContainingContentControls;
                            LMP.Forte.MSWord.WordDoc.InsertText_CC(this.m_oForteDocument.WordDocument, ref oNodesArray,
                                "", "", 0, iDeleteScope, xSegmentXML, m_oVariable.Name,
                                oInsertionLocation, ref bIgnoreDeleteScope,
                                ref xVariablesInScope, ref oTags, ref missing, "");
                        }
                        this.m_oForteDocument.RefreshTags();
                        bExpandedScope = true;
                    }
                }

            }
            finally
            {
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }
            //refresh variables collection if tags have been added or deleted
            if (bExpandedScope)
            {
                //GLOG 4784: Refresh and notify UI that Variable definitions have changed
                m_oSegment.Refresh();
                m_oForteDocument.RaiseSegmentRefreshedByActionEvent(m_oSegment);
            }

        }
        private void ApplyStyleSheet(string xValue, string xParameters)
        {
            //GLOG 3220
            string xExpression = "";
            LMP.Data.AdminSegmentDef oDef = null;

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length > 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xExpression = aParams[0];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }
            Trace.WriteNameValuePairs("xValue", xValue, "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            if (xValue != "" && xValue != "0")
            {
                //Get Integer portion of Admin Segment ID
                if (xValue.EndsWith(".0"))
                    xValue = xValue.Substring(0, xValue.Length - 2);
                //Style Sheet Segment ID must be a numeric
                int iTest = 0;
                if (!Int32.TryParse(xValue, out iTest))
                    return;

                //validate proposed Segment ID
                oDef = GetAdminSegmentDefinition(xValue);

                //Not a valid Segment ID
                if (oDef == null)
                    return;

                m_oForteDocument.UpdateStylesFromXML(oDef.XML);
            }
        }
        private void InsertTOA(string xValue, string xParameters)
        {
            //GLOG 3220
            string xBlockName = "";
            bool bUsePassim = false;

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //one parameter for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xBlockName = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    bUsePassim = bool.Parse(Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }
            Trace.WriteNameValuePairs("xValue", xValue, "xExpression", xBlockName);

            //delete current toa
            if (m_oForteDocument.FileFormat == LMP.Data
                .mpFileFormats.Binary)
            {
                m_oSegment.Variables.ItemFromName(xBlockName)
                    .AssociatedWordTags[0].Range.Text = "";
            }
            else
            {
                m_oSegment.Variables.ItemFromName(xBlockName)
                    .AssociatedContentControls[0].Range.Text = "";
            }

            if (xValue.ToLower() == "true")
            {
                //insert new toa
                LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();

                if (m_oForteDocument.FileFormat == LMP.Data
                    .mpFileFormats.Binary)
                {
                    oPleading.InsertTOAFieldCodes(m_oSegment.Variables
                        .ItemFromName(xBlockName).AssociatedWordTags[0].Range,
                        bUsePassim);
                }
                else
                {
                    oPleading.InsertTOAFieldCodes(m_oSegment.Variables
                        .ItemFromName(xBlockName).AssociatedContentControls[0].Range,
                        bUsePassim);
                }
            }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// validates conditions necessary for the insertion of a segment
        /// called by InsertSegment variable action
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="xBmk"></param>
        /// <returns></returns>
        protected bool ValidateSegmentInsertionRange(LMP.Data.AdminSegmentDef oDef, Word.Range oRange)
        {
            string xMsg = null;

            //validate the oDef insertionlocation option - bitwise comparison since
            //MenuInsertionOptions property value is a bitwise sum of several values
            int iInsertOpt = oDef.MenuInsertionOptions;

            if ((iInsertOpt & (int)Segment.InsertionLocations.InsertAtSelection) !=
                (int)Segment.InsertionLocations.InsertAtSelection)
            {
                //segment to be inserted must have MenuInsertionOptions set to Selection
                xMsg = LMP.Resources.GetLangString(
                    "Msg_SegmentInsertionInvalidSegmentInsertionLocationOption");
                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (oRange == null)
            {
                return false;
            }
            else
            {
                //bookmark exists - validate insertion range
                xMsg = null;
                bool bValidLocation = false;
                LMP.Forte.MSWord.TagInsertionValidityStates iValid = LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoParentTag;
                LMP.Forte.MSWord.WordDoc oMPDoc = new LMP.Forte.MSWord.WordDoc();
                Word.XMLNode oParentTag = null;
                Word.ContentControl oParentCC = null;
                //GLOG 5219: Content Control support
                if (LMP.Architect.Api.Application.CurrentWordVersion == 11)
                {
                    //Range.XMLParentNode can lead to access violation exceptions
                    //in Word 2003 - uses selection object instead
                    Word.Range oSelRng = LMP.Architect.Api.Application.CurrentWordApp.Selection.Range;
                    oRange.Select();
                    oParentTag = LMP.Architect.Api.Application.CurrentWordApp.Selection.XMLParentNode;
                    oSelRng.Select();
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(
                        oRange, oParentTag, LMP.Forte.MSWord.TagTypes.Segment);
                }
                else if (this.m_oForteDocument.FileFormat == mpFileFormats.Binary)
                {
                    oParentTag = oRange.XMLParentNode;
                }
                else
                {
                    oParentCC = oRange.ParentContentControl;
                }

                if (this.m_oForteDocument.FileFormat == mpFileFormats.Binary)
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(
                        oRange, oParentTag, LMP.Forte.MSWord.TagTypes.Segment);
                else
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(
                        oRange, oParentCC, LMP.Forte.MSWord.TagTypes.Segment);

                switch (iValid)
                {
                    case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists:
                        //selection is physically outside the story's existing segment tags
                        xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableOrSegmentLocation");
                        break;
                    case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoParentTag:
                        //selection contains top level segment
                        xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidSelectionContainsTopLevelSegment");
                        break;
                    case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidOutsideStorySegment:
                        //selection is physically outside the story's existing segment tags
                        xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableOrSegmentLocation");
                        break;
                    case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidParentTag:
                        //selection is nested inside mVar or msubVar
                        xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableParentTag");
                        break;
                    case LMP.Forte.MSWord.TagInsertionValidityStates.Valid:
                    default:
                        bValidLocation = true;
                        break;
                }

                //message if child segment insertion range not allowed
                if (bValidLocation == false)
                {
                    xMsg = xMsg.Replace("\\r", "\r");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// inserts Segment of designated ID into designated range
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private Segment InsertSegmentInRange(string xSegmentID, Word.Range oRange)
        {
            Trace.WriteNameValuePairs("xSegmentID", xSegmentID);


            if (xSegmentID == "" || xSegmentID == null)
                return null;

            //validate proposed Segment ID
            LMP.Data.AdminSegmentDef oDef =
                GetAdminSegmentDefinition(xSegmentID);

            if (oDef == null)
            {
                return null;
            }

            //validate insertion conditions - insertion location property for segment, 
            //and selected range in target doc
            if (!ValidateSegmentInsertionRange(oDef, oRange))
                return null;

            oRange.Select();

            Prefill oPrefill = null;

            object oInd = (object)"mpNewSegment";
            bool bReturnBookmark = false;

            if (this.m_oDocument.Bookmarks.Exists("mpNewSegment"))
            {
                this.m_oDocument.Bookmarks.get_Item(ref oInd).Delete();
                bReturnBookmark = true;
            }
            //TODO:  may want to make InsertionBehavior be a parameter
            Segment oInsertedSeg = Segment.Insert(xSegmentID
                , m_oSegment, Segment.InsertionLocations.InsertAtSelection,
                Segment.InsertionBehaviors.KeepExistingHeadersFooters,
                m_oForteDocument, oPrefill, true, null);

            //return mpNewSegmentBookmark if it existed before
            if (bReturnBookmark)
            {
                object oSegBKRange = (object)this.m_oForteDocument.Segments[0]
                        .PrimaryRange.Characters.First;

                Word.Bookmark oSegBmk = m_oDocument.Bookmarks.Add("mpNewSegment", ref oSegBKRange);
                oSegBmk.End = oSegBmk.End - 1;
            }
            return oInsertedSeg;
        }

        #endregion
    }
	
	/// <summary>
	/// Summary description for VariableActions.
	/// </summary>
	public class VariableActions: ActionsCollectionBase
	{
        #region *********************enumerations*********************
        public enum Types
		{
			InsertText = 1,
			ExecuteOnBookmark = 2,
			ExecuteOnDocument = 3,
			ExecuteOnApplication = 4,
			IncludeExcludeText = 5,
			InsertCheckbox = 6,
			SetVariableValue = 7,
			RunMacro = 8,
			RunMethod = 9,
			InsertBoilerplateAtTag = 10,
			InsertBoilerplateAtLocation = 11,
			SetDocVarValue = 12,
			SetAsDefaultValue = 13,
			ExecuteOnStyle = 14,
			ExecuteOnPageSetup = 15,
			InsertTextAsTable = 16,
			InsertDate = 17,
			UpdateMemoTypePeople = 18,
			EndExecution = 19,
            ReplaceSegment = 20,
            InsertDetail = 21,
            InsertReline = 22,
            InsertSegment = 23,
            InsertDetailIntoTable = 24,
            ExecuteOnTag = 25,
            RunVariableActions = 26,
            IncludeExcludeBlocks = 27,
            SetupLabelTable = 28,
            InsertBarCode = 29,
            SetupDetailTable = 30,
            SetupDistributedSections = 31,
            ExecuteOnBlock = 32,
            ApplyStyleSheet = 33,
            SetPaperSource = 34,
            UnderlineToLongest = 35,
            InsertTOA = 36,
            SetDocPropValue = 37, //GLOG 7495
            ExecuteOnSegment = 38 //GLOG 7229
        }
        #endregion
        #region *********************fields*********************
        private Variable m_oVariable;
		#endregion
        #region *********************events*********************
        #endregion
		#region *********************constructors*********************
		public VariableActions(Variable oVariable):base()
		{
			m_oVariable = oVariable;
		}
		#endregion
		#region *********************properties*********************
        public new VariableAction this[int iIndex]
        {
            get { return (VariableAction)this.ItemFromIndex(iIndex); }
        }
        public new VariableAction ItemFromIndex(int iIndex)
        {
            return (VariableAction)base.ItemFromIndex(iIndex);
        }
		#endregion
		#region *********************methods*********************
		public new VariableAction Create()
		{
			return (VariableAction) base.Create();
		}
        /// <summary>
        /// returns a clone of this variable action
        /// </summary>
        /// <param name="oSourceAction">the variable action to be cloned</param>
        /// <returns></returns>
        public VariableAction Clone(VariableAction oSourceAction)
        {
            //create new variable action
            VariableAction oNewAction = this.Create();

            //populate with values from source action
            oNewAction.Type = oSourceAction.Type;
            oNewAction.LookupListID = oSourceAction.LookupListID;
            oNewAction.ExecutionCondition = oSourceAction.ExecutionCondition;
            oNewAction.Type = oSourceAction.Type;
            oNewAction.Parameters = oSourceAction.Parameters;

            return oNewAction;
        }

        /// <summary>
        /// executes actions
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scopes will not be deleted when value is empty
        /// </param>
        public void Execute(bool bIgnoreDeleteScope)
        {
            Segment oSegment = m_oVariable.Segment;
            if (oSegment is LMP.Architect.Base.IStaticCreationSegment &&
                !oSegment.OverrideStaticCreationCondition)
            {
                //don't execute actions
            }
            else
            {
                //GLOG 7855 - Create a single Undo record for changes made by actions
                LMP.Forte.MSWord.Undo oUndo = LMP.Forte.MSWord.WordApp.StartUndoRecording("Variable Actions - " + m_oVariable.Name);
                bool bReprotect = false;
                string xPwd = "";
                if (oSegment.ForteDocument.WordDocument.ProtectionType == Word.WdProtectionType.wdAllowOnlyFormFields)
                {
                    Segment oTopSegment = oSegment;
                    while (oTopSegment.Parent != null)
                    {
                        oTopSegment = oTopSegment.Parent;
                    }
                    xPwd = oTopSegment.ProtectedFormPassword;
                    if (xPwd.ToUpper() == "[EMPTY]")
                    {
                        xPwd = "";
                    }
                    object oPwd = xPwd;
                    try
                    {
                        oSegment.ForteDocument.WordDocument.Unprotect(ref oPwd);
                    }
                    catch
                    {
                        return;
                    }
                    bReprotect = true;
                }
                try
                {
                    //cycle through actions in collection and execute
                    for (int i = 0; i < this.Count; i++)
                    {
                        VariableAction oAction = this[i];

                        //end execution if specified
                        if ((oAction.Type == Types.EndExecution) &&
                            (oAction.ExecutionIsSpecified()))
                            return;

                        try
                        {
                            oAction.Execute(bIgnoreDeleteScope);
                        }
                        catch (System.Exception oE)
                        {
                            throw new LMP.Exceptions.ActionException(
                                LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                                oAction.ExecutionIndex.ToString(), oE);
                        }
                    }
                }
                finally
                {
                    if (bReprotect)
                    {
                        object oPwd = xPwd;
                        object oTrue = true;
                        object oFalse = false;
                        oSegment.ForteDocument.WordDocument.Protect(Word.WdProtectionType.wdAllowOnlyFormFields, ref oTrue, ref oPwd, ref oFalse, ref oFalse);
                    }
                    //GLOG 7855
                    LMP.Forte.MSWord.WordApp.EndUndoRecording(oUndo);
                }
            }
        }

        /// <summary>
        /// executes actions
        /// </summary>
        public override void Execute()
        {
            this.Execute(false);
        }
        #endregion
		#region *********************private members*********************
		/// <summary>
		/// returns a VariableAction populated with data supplied by specified array
		/// </summary>
		/// <param name="aNewValues"></param>
		/// <returns></returns>
		protected override LMP.Architect.Base.ActionBase GetActionFromArray(string[] aNewValues)
		{
			VariableAction oAction = null;

			try
			{
				oAction = new VariableAction(m_oVariable);
				oAction.SetID(aNewValues[0]);
				oAction.ExecutionIndex = Convert.ToInt32(aNewValues[1]);
				oAction.LookupListID = Convert.ToInt32(aNewValues[2]);
				oAction.ExecutionCondition = aNewValues[3].ToString();
				oAction.Type = (VariableActions.Types) Convert.ToInt32(aNewValues[4]);
				oAction.Parameters = aNewValues[5].ToString();
				oAction.IsDirty = false;
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.DataException(
					LMP.Resources.GetLangString("Error_InvalidVariableActionDefinition" + 
					string.Join(LMP.StringArray.mpEndOfSubValue.ToString(), aNewValues)), oE);
			}

			return oAction;
		}
		protected override LMP.Architect.Base.ActionBase GetNewActionInstance()
		{
			return new VariableAction(m_oVariable);
		}
		#endregion
	}
}
