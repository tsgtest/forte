using System;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using System.Xml;
using LMP.Data;
using LMP.Architect.Base;

namespace LMP.Architect.Api
{
    public delegate void CurrentSegmentChangedEventHandler(object sender, CurrentSegmentSwitchedEventArgs args);

	/// <summary>
	/// contains the members of a MacPac document -
	/// a Word document created by MacPac.
	/// </summary>
	public class ForteDocument: LMP.Architect.Base.ForteDocument
	{
        #region *********************fields*********************
		private LMP.Forte.MSWord.Tags m_oDoTags;
		private Word.Document m_oWordDocument;
        private NodeStore m_oOrphanNodes;
        private Variables m_oVariables;
        private Blocks m_oBlocks;
		private Segments m_oSegments;
		private static WordXMLEvents m_iIgnoreWordXMLEvents = WordXMLEvents.None;
        private Modes m_iMode;
        private Segment m_xFixedTopLevelSegment;
        private int m_iWindowHandle = 0;
        private int m_iReplaceSegmentNodeIndex = -1;
        private bool m_bDeleteScopesUpdatingInDesign = false;
        //private mpFileFormats m_oFileFormat = mpFileFormats.None;
        private bool m_bFunctionalityIsAvailable = true;
        private bool m_bChildStructureInsertionInProgress = false; //GLOG 5944 (dm)
        private bool m_bDynamicEditingSet = false; //JTS 4/1/13
        #endregion
		#region *********************events*********************
        public event VariableDeletedHandler VariableDeleted;
        public event VariableAddedHandler VariableAdded;
        public event VariableDefinitionChangedHandler VariableDefinitionChanged;
        public event BlockDeletedHandler BlockDeleted;
        public event BlockAddedHandler BlockAdded;
        public event BlockDefinitionChangedHandler BlockDefinitionChanged;
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        public event SegmentRefreshedByActionHandler SegmentRefreshedByAction;
        public event SegmentDistributedSectionsHandler SegmentDistributedSectionsChanged;
        public event BeforeSegmentReplacedByActionHandler BeforeSegmentReplacedByAction;
        public event BeforeCollectionTableStructureInsertedHandler BeforeCollectionTableStructureInserted;
        public event AfterCollectionTableStructureInsertedHandler AfterCollectionTableStructureInserted;
        //public event CurrentSegmentChangedEventHandler CurrentSegmentChanged;
        #endregion
		#region *********************constructor*********************
		public ForteDocument(Word.Document oDocument)
		{
			this.m_oWordDocument = oDocument;

			//TODO: this line needs to be restored 
			//after we implement targeted XML header/footer insertion
			//this.Refresh();
		}
		#endregion
		#region *********************properties*********************
        //returns true iff the macpac document
        //has finished segments
        public bool ContainsFinishedSegments
        {
            get
            {
                return LMP.Forte.MSWord.WordDoc.HasSnapshotDocVars(this.WordDocument);
            }
        }

		//returns the Word nodes in this document
		public LMP.Forte.MSWord.Tags Tags
		{
			get
			{
				if(m_oDoTags == null)
                    //added parameter (11/30/09 dm)
					this.RefreshTags(this.Mode != Modes.Design);

				return m_oDoTags;
			}
		}

        /// <summary>
        /// returns true iif the tags in the document
        /// have not yet been retrieved
        /// </summary>
        public bool TagsRetrieved
        {
            get { return m_oDoTags != null; }
        }

		/// <summary>
		/// returns the Word document that is associated with this MacPac document
		/// </summary>
		public Word.Document WordDocument
		{
			get{return m_oWordDocument;}
		}

		/// <summary>
		/// returns the type of document this is - choices 
		/// are listed in the domain ForteDocument.Types
		/// </summary>
		public ForteDocument.Types Type
		{
			get
			{
				//TODO: implement this function -
				//for now, return MacPac 10
				return ForteDocument.Types.MacPac10;
			}
		}

		/// <summary>
		/// returns the segments currently in the document
		/// </summary>
		public Segments Segments
		{
			get
			{
                if (m_oSegments == null)
                {
                    m_oSegments = new Segments(this);

                    //subscribe to events
                    m_oSegments.SegmentAdded += new SegmentAddedHandler(m_oSegments_SegmentAdded);
                    m_oSegments.SegmentDeleted += new SegmentDeletedHandler(m_oSegments_SegmentDeleted);
                }

				return m_oSegments;
			}
		}

        //returns true if MacPac functionality is available in this document
        public bool MacPacFunctionalityIsAvailable
        {
            get { return m_bFunctionalityIsAvailable; }
            set { m_bFunctionalityIsAvailable = value; }
        }

        ///// <summary>
        ///// returns the standalone variables of this ForteDocument
        ///// </summary>
        //public Variables Variables
        //{
        //    get
        //    {
        //        if (m_oVariables == null)
        //            RefreshVariables();

        //        return m_oVariables;
        //    }
        //    private set { this.m_oVariables = value; }
        //}

        ///// <summary>
        ///// returns whether the ForteDocument has variables without invoking its
        ///// variables property and thereby populating the collection
        ///// </summary>
        //public bool HasVariables
        //{
        //    get { return ((m_oVariables != null) && (m_oVariables.Count > 0)); }
        //}

        ///// <summary>
        ///// returns the standalone blocks of this ForteDocument
        ///// </summary>
        //public Blocks Blocks
        //{
        //    get
        //    {
        //        if (m_oBlocks == null)
        //            RefreshBlocks();

        //        return m_oBlocks;
        //    }
        //    private set { this.m_oBlocks = value; }
        //}
		/// flag to turn off event handling
		/// </summary>
		public static WordXMLEvents IgnoreWordXMLEvents
		{
			get{return m_iIgnoreWordXMLEvents;}
			set{m_iIgnoreWordXMLEvents = value;}
		}
        /// <summary>
        /// Returns true if document does not match any of the criteria
        /// indicating that automatic trailer update should not occur
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static bool IsDocumentTrailerable(Word.Document oDoc)
        {
            //GLOG - 3906 - CEH
            int iFormat = 0;
            string xCustomCode = LMP.Data.Application.GetMetadata("CustomCode");

            try
            {
                //Get Save Format
                iFormat = oDoc.SaveFormat;
            }
            catch { }           

            
            LMP.Data.FirmApplicationSettings oFirmAppSettings = 
                new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);

            if (oDoc == null) // || oDoc.ReadOnly) GLOG 5764:  Read-only should not prevent trailer insertion
                return false;
            //GLOG 4283
            else if (oFirmAppSettings.SkipTrailerForTempFiles && IsDocumentInTemporaryFolder(oDoc))
                return false;
            //Littler customization
            else if (xCustomCode.Contains("NoTrailerForTempDoc") &&
                    (oDoc.Path.ToUpper().Contains(@"\TEMPORARY")))
                return false;
            else if (LMP.Forte.MSWord.WordDoc.SkipTrailerUpdate(oDoc))
                //Document is marked for no trailer based on custom macro criteria
                return false;
            else if (iFormat > 1 && (iFormat < 12 || iFormat > 16))
                //Document is not a Word document
                return false;
            else if (DocCannotBeUnprotected(oDoc))
                //GLOG 7107 (dm) - we need to check this before checking sections
                //for property tags, which can insert an extra paragraph in the header
                return false;
            else if (AllSectionsMarkedForNoTrailer(oDoc))
                //Document has been marked for no trailer in property tags
                return false;
            else
            {
                //return false for non-Word or special-use documents
                string xFileName = oDoc.Name.ToUpper();
                if (xFileName.EndsWith(".DOT") || xFileName.EndsWith(".DOTX") ||
                    xFileName.EndsWith(".DOTM") || xFileName.EndsWith(".DIC") ||
                    xFileName.EndsWith(".INI") || xFileName.EndsWith(".MBP") ||
                    xFileName.EndsWith(".STY") || xFileName.EndsWith(".TMP") ||
                    xFileName.EndsWith(".CMD") || xFileName.EndsWith(".BAT"))
                    return false;
            }
            return true;
        }
        public static bool IsDocumentInTemporaryFolder(Word.Document oDoc)
        {
            //GLOG 4283: Returns true if document path is in one of the predefined Temp locations
            string xFilePath = oDoc.Path.ToUpper();
            //Get Temporary Internet Files Special Folder path
            string xTempInternet = System.Environment.GetFolderPath((System.Environment.SpecialFolder)System.Environment.SpecialFolder.InternetCache);
            return (xFilePath.Contains(xTempInternet.ToUpper()) ||
                xFilePath.Contains(System.Environment.GetEnvironmentVariable("TEMP").ToUpper()) ||
                xFilePath.Contains(System.Environment.GetEnvironmentVariable("TMP").ToUpper()));
        }
        /// <summary>
        /// Get/Set 'NoTrailer' property in ObjectData of mDocProps tag
        /// </summary>
        public bool AllowDocumentTrailer
        {
            get 
            {
                string xVal = LMP.Forte.MSWord.WordDoc.GetDocPropValue(m_oWordDocument, "NoTrailer");
                if (string.IsNullOrEmpty(xVal))
                    return true;
                else 
                    return xVal.ToUpper() != "TRUE"; 
                
            }
            set
            {
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                LMP.Forte.MSWord.WordDoc.SetDocPropValue(m_oWordDocument, "NoTrailer", (!value).ToString());
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
            }
        }
        /// <summary>
        /// True if 'NoTrailer' property is true in ObjectData of mSecProps tag
        /// </summary>
        /// <param name="iSecIndex"></param>
        /// <returns></returns>
        public bool IsSectionMarkedForNoTrailer(int iSecIndex)
        {
            string xVal = LMP.Forte.MSWord.WordDoc.GetSectionPropValue(m_oWordDocument, iSecIndex, "NoTrailer");

            if (string.IsNullOrEmpty(xVal))
            {
                return false;
            }
            else
            {
                return xVal.ToUpper() == "TRUE";
            }
        }
        /// <summary>
        /// Set NoTrailer property in ObjectData of mSecProps tag
        /// </summary>
        /// <param name="iSecIndex"></param>
        /// <param name="bAllow"></param>
        public void MarkSectionForNoTrailer(int iSecIndex, bool bAllow)
        {
            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
            LMP.Forte.MSWord.WordDoc.SetSectionPropValue(m_oWordDocument, iSecIndex, "NoTrailer", (!bAllow).ToString());
            ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
        }
        /// <summary>
        /// Returns true of Document is marked for No Trailer in mDocProps tag
        /// or if all sections are marked for No Trailer in mSecProps tags
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static bool AllSectionsMarkedForNoTrailer(Word.Document oDoc)
        {
            if (oDoc == null)
                return true;
            ForteDocument oMPDoc = new ForteDocument(oDoc);

            if (!oMPDoc.AllowDocumentTrailer)
            {
                //Trailer is disabled at document level
                return true;
            }
            else
            {
                //Check if any section is not marked for no trailer
                for (int i = 1; i <= oMPDoc.WordDocument.Sections.Count; i++)
                {
                    if (!oMPDoc.IsSectionMarkedForNoTrailer(i))
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// returns true if document is document is password protected and
        /// password is unavailable - added for GLOG 7107 (dm)
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static bool DocCannotBeUnprotected(Word.Document oDoc)
        {
            bool bValue = false;
            //GLOG 7958
            bool bSaved = oDoc.Saved;
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
            try
            {
                oEnv.UnprotectDocument();
            }
            catch (LMP.Exceptions.PasswordException)
            {
                bValue = true;
            }
            finally
            {
                oEnv.RestoreProtection();
                //GLOG 7958: Restore original Saved state
                if (bSaved && !oDoc.Saved)
                    oDoc.Saved = true;
            }
            return bValue;
        }

        /// <summary>
        /// returns the orphan nodes for this MacPac Document
        /// </summary>
        public NodeStore OrphanNodes
        {
            get { return m_oOrphanNodes; }
            private set { m_oOrphanNodes = value; }
        }

        public Modes Mode
        {
            get { return m_iMode; }
            set {m_iMode = value;}
        }

        public Segment FixedTopLevelSegment
        {
            get { return m_xFixedTopLevelSegment; }
            set { m_xFixedTopLevelSegment = value; }
        }
        public void UnloadVariableControls()
        {
            for (int i = 0; i < m_oSegments.Count; i++)
            {
                m_oSegments[i].UnloadVariableControls(true);
            }
        }

        public int WindowHandle
        {
            get { return m_iWindowHandle; }
        }

        /// <summary>
        /// gets/sets index of editor node of segment about to be replaced
        /// </summary>
        public int ReplaceSegmentNodeIndex
        {
            get { return m_iReplaceSegmentNodeIndex; }
            set { m_iReplaceSegmentNodeIndex = value; }
        }

        /// <summary>
        /// gets/sets flag indicating whether the designer
        /// is in the process of refreshing delete scopes
        /// </summary>
        public bool DeleteScopesUpdatingInDesign
        {
            get { return m_bDeleteScopesUpdatingInDesign; }
            set { m_bDeleteScopesUpdatingInDesign = value; }
        }

        //dcf - 10/1/10
        //remmed out the previous version of this method below -
        //new version gets the file format directly from the
        //document instead of from a set variable -  this
        //allows us to account for changes of format due to a save as
        /// <summary>
        /// returns the file format of the document
        /// </summary>
        public LMP.Data.mpFileFormats FileFormat
        {
            get
            {
                return (mpFileFormats)LMP.Forte.MSWord.WordDoc.GetDocumentFileFormat(m_oWordDocument);
            }
        }

        public bool ChildStructureInsertionInProgess
        {
            get { return m_bChildStructureInsertionInProgress; }
            set { m_bChildStructureInsertionInProgress = value; }
        }
        #endregion
		#region *********************methods*********************
        public void RefreshTags()
        {
            this.RefreshTags(true);
        }
        public void RefreshTags(string xContentXML)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                m_oDoTags = LMP.Forte.MSWord.WordDoc.RefreshTagsFromXml(xContentXML);
                LMP.Benchmarks.Print(t0, "Refresh from XML");
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        /// <summary>
        /// retrieves the Word tags collection for this document
        /// </summary>
        /// <param name="bUseGUIDIndexes"></param>
        public void RefreshTags(bool bUseGUIDIndexes)
		{
			try
			{
                Trace.WriteInfo("Start RefreshTags");
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

				DateTime t0 = DateTime.Now;

                //get tag id of fixed top-level segment
                string xFixedTopLevelTagID = "";
                if (this.FixedTopLevelSegment != null)
                {
                    xFixedTopLevelTagID = this.FixedTopLevelSegment
                        .Definition.Name + "_1";
                }

                //get document tags
                //12/27/10 JTS: Added Tags property to indicate that corrupted XML Tags have been deleted
                //by RefreshTags - 0=None; 1=Standalone mBlock, mVar, mDel; 
                //2=Trailer-type mSEG; 3=Non Trailer mSEG or non-standalone mBlock, mVar or mDel
                LMP.Forte.MSWord.mpInvalidTagsRemovedTypes iInvalidTagsDeleted = LMP.Forte.MSWord.mpInvalidTagsRemovedTypes.None;

                Trace.WriteInfo("Before COMRefreshTags");
                m_oDoTags = LMP.Forte.MSWord.WordDoc.RefreshTags(this.m_oWordDocument,
                    bUseGUIDIndexes, xFixedTopLevelTagID, ref iInvalidTagsDeleted);

                //if (Segment.oUndo != null)
                //{
                //    bool b = Segment.oUndo.IsRecording();
                //}

                if (iInvalidTagsDeleted > LMP.Forte.MSWord.mpInvalidTagsRemovedTypes.TrailerTagsOnly)
                {
                    if (LMP.Data.Application.User.FirmSettings.AlertUnusableTagsRemoved)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_InvalidTagsDeleted"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }

                //set WindowHandle property - input focus will
                //reliably be in the document here
                if (m_iWindowHandle == 0)
                    m_iWindowHandle = LMP.WinAPI.GetFocus();

                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;

                if (LMP.Trace.Level != System.Diagnostics.TraceLevel.Off)
                {
                    //GLOG 5482: Ignore error here if XMLSummary is not populated due to invalid tags being removed
                    try
                    {
                        LMP.Trace.WriteNameValuePairs(
                            "m_oDoTags.XMLSummary", m_oDoTags.XMLSummary);
                    }
                    catch { }
                }

				LMP.Benchmarks.Print(t0);
			}
			catch(System.Exception oE)
			{
                //GLOG 3932 (dm) - added specific handling for the native bug whereby
                //XML attributes are truncated when opening a .docx file in Word 2003
                int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                if (iErr == -2146823659)
                {
                    string xMsg = LMP.Resources.GetLangString("Error_TruncatedWordXMLAttributes");
                    DialogResult oDR =MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                    {
                        ForteDocument.IgnoreWordXMLEvents = WordXMLEvents.All;
                        object oUrn = "urn-legalmacpac-data/10";
                        Word.XMLSchemaReference oSchema = null;
                        try
                        {
                            oSchema = this.WordDocument.XMLSchemaReferences.get_Item(ref oUrn);
                        }
                        catch { }
                        if (oSchema != null)
                            oSchema.Delete();
                        ForteDocument.IgnoreWordXMLEvents = WordXMLEvents.None;
                    }
                    else
                    {
                        throw new LMP.Exceptions.COMEventException(
                            LMP.Resources.GetLangString("Error_CouldNotRefreshTags"), oE);
                    }
                }
                else
                {
                    throw new LMP.Exceptions.COMEventException(
                        LMP.Resources.GetLangString("Error_CouldNotRefreshTags"), oE);
                }
			}
		}
        /// <summary>
        /// returns true iff the document contains a segment
        /// of the specified type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public bool ContainsSegmentOfType(mpObjectTypes iType)
        {
            return ContainsSegmentOfType(this.Segments, iType);
        }
        /// returns true iff the specified segments collection 
        /// contains a segment of the specified type
        private bool ContainsSegmentOfType(Segments oSegments, mpObjectTypes iType)
        {
            //cycle through segments collection
            for (int i = 0; i < oSegments.Count; i++)
            {
                Segment oSegment = oSegments[i];

                //check for segment of specified type in child segments
                bool bContainsSegment = ContainsSegmentOfType(oSegment.Segments, iType);

                //return true if either the child or the parent
                //are of the specified type
                if (bContainsSegment || oSegment.TypeID == iType)
                    return true;
            }

            return false;
        }

        //GLOG : 6483 : CEH
        /// <summary>
        /// inserts the segment with the specified definition ID at the 
        /// specified location in the specified ForteDocument
        /// </summary>
        /// <param name="iDefinitionID"></param>
        /// <param name="oLocation"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="iInsertionOptions"></param>
        /// <returns></returns>
        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, string xXML, Prefill oPrefill)
        {
            return this.InsertSegment(xDefinitionID, oParent,
                iInsertionLocation, iInsertionBehavior, true, oPrefill, "",
                false, false, xXML, false, false, null);
        }
        /// <summary>
        /// inserts the segment with the specified definition ID at the 
        /// specified location in the specified ForteDocument
        /// </summary>
        /// <param name="iDefinitionID"></param>
        /// <param name="oLocation"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="iInsertionOptions"></param>
        /// <returns></returns>
        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, string xXML)
        {
            return this.InsertSegment(xDefinitionID, oParent,
                iInsertionLocation, iInsertionBehavior, true, null, "", 
                false, false, xXML, false, false, null);
        }
        /// <summary>
        /// inserts the segment with the specified definition ID at the 
        /// specified location in the specified ForteDocument
        /// </summary>
        /// <param name="iDefinitionID"></param>
        /// <param name="oLocation"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="iInsertionOptions"></param>
        /// <returns></returns>
        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, 
            bool bInsertingInMainStory, string xXML)
        {
            return this.InsertSegment(xDefinitionID, oParent, 
                iInsertionLocation, iInsertionBehavior, bInsertingInMainStory, 
                null, "", false, false, xXML, false, false, null);
        }

        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, bool bInsertingInMainStory, 
            Prefill oPrefill, string xBodyText, bool bAttachToTemplate,
            bool bUpdateAllStyles, string xXML)
        {
            return this.InsertSegment(xDefinitionID, oParent,
                iInsertionLocation, iInsertionBehavior, bInsertingInMainStory, 
                oPrefill, xBodyText, bAttachToTemplate, bUpdateAllStyles, xXML, false, false, null);
        }
        //GLOG 6021
        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, bool bInsertingInMainStory,
            Prefill oPrefill, string xBodyText, bool bAttachToTemplate,
            bool bUpdateAllStyles, string xXML, bool bDesign, bool bForcePrefillOverride,
            CollectionTableStructure oChildStructure)
        {
            return this.InsertSegment(xDefinitionID, oParent, iInsertionLocation, iInsertionBehavior,
                bInsertingInMainStory, oPrefill, xBodyText, bAttachToTemplate, bUpdateAllStyles, xXML, 
                bDesign, bForcePrefillOverride, oChildStructure, null);
        }
        /// <summary>
        /// inserts the segment with the specified definition ID at the 
        /// specified location in the specified ForteDocument
        /// </summary>
        /// <param name="iDefinitionID"></param>
        /// <param name="oParent"></param>
        /// <param name="oLocation"></param>
        /// <param name="oMPDocument"></param>
        /// <param name="iInsertionOptions"></param>
        /// <returns></returns>
        public Segment InsertSegment(string xDefinitionID, Segment oParent,
            Segment.InsertionLocations iInsertionLocation,
            Segment.InsertionBehaviors iInsertionBehavior, bool bInsertingInMainStory, 
            Prefill oPrefill, string xBodyText, bool bAttachToTemplate, 
            bool bUpdateAllStyles, string xXML, bool bDesign, bool bForcePrefillOverride,
            CollectionTableStructure oChildStructure, System.Collections.ArrayList aSectionList) //GLOG 6021: Additional parameter for list of sections for insertion
        {
            DateTime t0 = DateTime.Now;
            //oParent = null;
            Segment oSegment = null;

            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //save state
            Environment oEnv = new Environment(this.WordDocument);
            oEnv.SaveState();

            try
            {
                //insert
                oSegment = Segment.Insert(xDefinitionID, oParent, iInsertionLocation,
                   iInsertionBehavior, this, oPrefill, true, bInsertingInMainStory, xXML, 
                   bUpdateAllStyles, bForcePrefillOverride, bAttachToTemplate, oChildStructure, aSectionList); //GLOG 6021

                if (oSegment != null && !bDesign && !string.IsNullOrEmpty(xBodyText))
                {
                    Word.Range oBodyRng = oSegment.GetBody();
                    //GLOG 2677 06/24/08 JTS
                    if (oBodyRng != null)
                    {
                        // If BodyText parameter specified, set text of appropriate Block
                        //GLOG 5298: If Block contains more than 49 paragraphs, get_Style will fail.
                        //Check just first paragraph instead
                        Word.Style oBodyStyle = (Word.Style)oBodyRng.Paragraphs[1].Range.get_Style();
                        string xStyleName = oBodyStyle.NameLocal;

                        if (LMP.String.IsWordXML(xBodyText) || LMP.String.IsWordOpenXML(xBodyText))
                        {
                            //Body Text is Word Document XML
                            object oTransform = null;
                            ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                            oBodyRng.InsertXML(xBodyText, ref oTransform);
                            oBodyRng = oSegment.GetBody();
                            object oFirstStyle = oBodyRng.Paragraphs[1].Range.get_Style(); //GLOG 8427
                            while (!string.IsNullOrEmpty(oBodyRng.Text) && oBodyRng.Text.EndsWith("\r"))
                            {
                                //Clean up trailing paragraph before closing tag
                                object oUnit = Word.WdUnits.wdCharacter;
                                object oCount = 1;
                                //Make sure we're only deleting returns - not ending Word tags 
                                //or other non-printing characters
                                if (oBodyRng.Characters.Last.Text == "\r")
                                {
                                    //GLOG 8414: Reapply starting Style to last paragraph if deleting paragraph has changed it,
                                    //or apply Body style if initial paragraph style was Normal
                                    //GLOG 8427: Only do this for binary format
                                    if (oBodyRng.Paragraphs.Last.Range.Text.Length > 1 && this.FileFormat == mpFileFormats.Binary)
                                    {
                                        Word.Style oParaStyle = (Word.Style)oBodyRng.Paragraphs.Last.Range.get_Style();
                                        object oParaStyleName = oParaStyle.NameLocal;
                                        object oNormalStyle = Word.WdBuiltinStyle.wdStyleNormal;
                                        //GLOG 8427
                                        if (oParaStyle.NameLocal == oBodyRng.Document.Styles[ref oNormalStyle].NameLocal || oParaStyle.NameLocal == xStyleName)
                                            oParaStyleName = oFirstStyle;
                                        oBodyRng.Characters.Last.Delete(ref oUnit, ref oCount);
                                        oBodyRng.Paragraphs.Last.set_Style(ref oParaStyleName);
                                    }
                                    else
                                        oBodyRng.Characters.Last.Delete(ref oUnit, ref oCount);
                                }
                                else
                                    break;
                            }
                            //Body might contain XML tags - if so refresh
                            if (oBodyRng.XMLNodes.Count > 0)
                            {
                                foreach (Word.XMLNode oNode in oBodyRng.XMLNodes)
                                {
                                    //GLOG 2545: mBlocks cannot directly contain other mBlocks.
                                    //Strip out any of these that might have been included in original selection.
                                    if (oNode.BaseName == "mBlock")
                                    {
                                        oNode.Delete();
                                    }
                                }
                                if (oBodyRng.XMLNodes.Count > 0)
                                    this.Refresh(RefreshOptions.RefreshTagsWithGUIDIndexes, false);
                            }
                            //GLOG 5298: get_Style cannot be used for more than 49 paragraphs (get error 91)
                            //Instead, we need to test each paragraph individually to check that all use Normal style
                            //Moved into COM function for speed
                            bool bIsNormalStyle = LMP.Forte.MSWord.WordDoc.RangeIsAllNormalStyle(oBodyRng);
                            if (bIsNormalStyle)
                            {
                                //the inserted body was not a full paragraph, or 
                                //was previously styled in Normal - turn to style of body
                                object oStyleName = (object)xStyleName;
                                oBodyRng.set_Style(ref oStyleName);
                            }
                        }
                        else
                        {
                            //Input is not Word Document XML
                            oBodyRng.Text = xBodyText;
                            //GLOG 5298: If Body not inserted as XML, just set Body style unconditionally
                            object oStyleName = (object)xStyleName;
                            oBodyRng.set_Style(ref oStyleName);

                        }
                    }
                }
            }
            finally
            {
                this.WordDocument.Activate();

                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;

                //restore state
                oEnv.RestoreState();
            }

            LMP.Benchmarks.Print(t0, oSegment != null ? oSegment.Name : "");

            return oSegment;
        }

        /// <summary>
        /// deletes the specified segment from the document
        /// </summary>
        /// <param name="oSegment"></param>
        public void DeleteSegment(Segment oSegment)
        {
            DeleteSegment(oSegment, true, true, true, true, false);
        }
        public void DeleteSegment(Segment oSegment, bool bRefreshTags)
        {
            DeleteSegment(oSegment, true, true, true, bRefreshTags, false);
        }
        public void DeleteSegment(Segment oSegment, bool bDeleteEmptyParagraph,
            bool bPreserveNonNativemDels, bool bDeleteEmptySection)
        {
            DeleteSegment(oSegment, bDeleteEmptyParagraph, bPreserveNonNativemDels, bDeleteEmptySection, true, false);
        }
        public void DeleteSegment(Segment oSegment, bool bDeleteEmptyParagraph,
            bool bPreserveNonNativemDels, bool bDeleteEmptySection, bool bRefreshTags, bool bPreserveDocVars) //GLOG 8549
        {
            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;

            DateTime t0 = DateTime.Now;

            try
            {
                LMP.Trace.WriteNameValuePairs("oSegment.ID", oSegment.ID, 
                    "oSegment.TagID", oSegment.FullTagID);
                
                //Need to delete external children separately
                //To ensure all headers/footers are removed
                int iChildCount = oSegment.Segments.Count;
                for (int i = iChildCount - 1; i >= 0; i--)
                {
                    if (oSegment.Segments[i].IsExternalChild)
                        DeleteSegment(oSegment.Segments[i], bDeleteEmptyParagraph,
                            bPreserveNonNativemDels, bDeleteEmptySection);
                }
                //JTS: 5/23/10: 10.2
                object oNodes = null;
                LMP.Forte.MSWord.Tags oTags = this.Tags;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                if (this.m_iMode != Modes.Design)
                {
                    Word.Bookmark[] aBmks = oSegment.Nodes.GetBookmarks(oSegment.FullTagID);

                    oNodes = (object)aBmks;
                    LMP.Forte.MSWord.WordDoc.DeleteBookmarks(oNodes, ref oTags, bDeleteEmptyParagraph,
                        bPreserveNonNativemDels, bDeleteEmptySection, bPreserveDocVars); //GLOG 8549
                }
                else if (this.FileFormat == mpFileFormats.OpenXML)
                {
                    Word.ContentControl[] aCCs = oSegment.Nodes.GetContentControls(oSegment.FullTagID);
                    oNodes = (object)aCCs;
                    foreach (Word.ContentControl oCC in aCCs)
                    {
                        string xBmkName = "_" + oCC.Tag;
                        object oBmkName = (object)xBmkName;

                        if (oSegment.ForteDocument.WordDocument.Bookmarks.Exists(xBmkName))
                        {
                            oSegment.ForteDocument.WordDocument.Bookmarks.get_Item(ref oBmkName).Delete();
                        }
                    }

                    LMP.Forte.MSWord.WordDoc.DeleteContentControls(oNodes, ref oTags, bDeleteEmptyParagraph,
                        bPreserveNonNativemDels, bDeleteEmptySection);
                }
                else
                {
                    //delete all mSEG tags associated with the specified segment
                    Word.XMLNode[] aNodes = oSegment.Nodes.GetWordTags(oSegment.FullTagID);
                    oNodes = (object)aNodes;
                    LMP.Forte.MSWord.WordDoc.DeleteTags(oNodes, ref oTags, bDeleteEmptyParagraph,
                        bPreserveNonNativemDels, bDeleteEmptySection);
                }


                //GLOG 4418: Added new parameter so that when multiple trailer segments are being deleted
                //it is not necessary to Refresh after each deletion
                if (bRefreshTags)
                {
                    //refresh tags - use indexes instead of GUIDs
                    //when in design mode
                    bool bRefreshWithGUIDs = (this.Mode != Modes.Design);
                    this.RefreshTags(bRefreshWithGUIDs);
                }

                //10-21-11 (dm) - delete snapshot doc vars
                if (Snapshot.Exists(oSegment))
                    Snapshot.Delete(oSegment, true);

                //delete the segment object from the collection
                if (oSegment.IsTopLevel)
                {
                    this.Segments.Delete(oSegment.FullTagID);
                }
                else
                {
                    Segment oParent = oSegment.Parent;

                    //GLOG 6362 (dm) - refresh collection table parent after deleting
                    //first item in collection to account for any non-native mDels
                    bool bIsFirstItemInCollection = ((oParent is CollectionTable) &&
                        (!oParent.IsTopLevel) && (oParent.Segments[0].TagID == oSegment.TagID));

                    oParent.Segments.Delete(oSegment.FullTagID);

                    //refresh parent's node store - it may have been necessary to
                    //delete and recreate parent's tag in order to delete child
                    try
                    {
                        oParent.RefreshNodes();

                        //GLOG 6362 (dm)
                        if (bIsFirstItemInCollection)
                            oParent.Parent.RefreshNodes();
                    }
                    catch { }
                }
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
                LMP.Benchmarks.Print(t0, oSegment.DisplayName);
            }
        }

        /// <summary>
        /// inserts the segment with the specified ID for design purposes
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public Segment InsertSegmentForDesign(string xSegmentID)
        {
            return SetUpSegmentForDesign(xSegmentID, true);
        }

        /// <summary>
        /// sets up the segment with the specified ID for design purposes -
        /// assumes valid xml exists in the document
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public Segment SetUpSegmentForDesign(string xSegmentID)
        {
            return SetUpSegmentForDesign(xSegmentID, false);
        }
        /// <summary>
        /// Deletes existing segment and replaces with latest definition of same segment
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oParentSegment"></param>
        public void UpdateSegmentDesign(string xSegmentID, Segment oParentSegment)
        {
            Segment.Replace(xSegmentID, xSegmentID, oParentSegment, this, null, null, true);

            //refresh parent
            if (oParentSegment != null)
            {
                Segment oNewParent = null;

                //GLOG 3640 (dm) - it turns out that the issue I addressed in GLOG 4381
                //below is a more general problem when replacing collection table items -
                //if the user has changed the type, the parent collection segment will
                //no longer be in the document - in this case, refresh the top-level parent
                if (oParentSegment is CollectionTable)
                {
                    Segments oSegs = null;
                    if (oParentSegment.IsTopLevel)
                        oSegs = this.Segments;
                    else
                        oSegs = oParentSegment.Parent.Segments;
                    oNewParent = Segment.FindSegment(oParentSegment.TypeID, oSegs);
                    if (oNewParent == null)
                        oNewParent = this.Segments[0];
                }
                else
                    oNewParent = oParentSegment;

                //refresh
                oNewParent.Refresh();
            }
        }
        public void UpdateSegment(string xSegmentID, Segment oParentSegment, Prefill oPrefill, string xBodyText)
        {
            UpdateSegment(xSegmentID, oParentSegment, oPrefill, xBodyText, false);
        }
        public void UpdateSegment(string xSegmentID, Segment oParentSegment, Prefill oPrefill, string xBodyText, bool bDesign)
        {
            Segment.Replace(xSegmentID, xSegmentID, oParentSegment, this, oPrefill, xBodyText, bDesign);
            //GLOG 4433 (dm) - the following code errs when recreating collection table items,
            //because the parent also gets replaced - it's also unnecessary because the
            //entire doc gets refreshed immediately after this method is called
            //if (oParentSegment != null)
            //    oParentSegment.Refresh();
        }
        /// <summary>
        /// sets up the segment with the specified ID for design purposes-
        /// either runs InsertForDesign or SetUpForDesign, based on specified request
        /// </summary>
        /// <param name="xDefinitionID"></param>
        /// <param name="bInsert"></param>
        /// <returns></returns>
        private Segment SetUpSegmentForDesign(string xDefinitionID, bool bInsert)
        {
            DateTime t0 = DateTime.Now;
            Segment oSegment = null;

            //save state
            LMP.Architect.Api.Environment oEnv = new Environment(this.WordDocument);
            oEnv.SaveState();

            try
            {
                if (bInsert)
                    oSegment = Segment.InsertForDesign(xDefinitionID, this);
                else
                    oSegment = Segment.SetUpForDesign(xDefinitionID, this);
            }
            finally
            {
                //restore state
                oEnv.RestoreState();

                //after restoring state, always force ShowXMLMarkup when entering 
                //Design mode pass Undefined to avoid resetting anything else
                mpTriState iUndefined = mpTriState.Undefined;

                oEnv.SetExecutionState(iUndefined, iUndefined, 
                    iUndefined, iUndefined, mpTriState.True, false);
            }

            //mark as not dirty
            this.WordDocument.Saved = true;

            LMP.Benchmarks.Print(t0);

            return oSegment;
        }

        /// <summary>
		/// returns the segment with the specified tag id in the specified MacPac document
		/// </summary>
		/// <param name="xTagID"></param>
		/// <param name="oMPDocument"></param>
        /// <returns></returns>
        public Segment GetSegment(string xTagID, ForteDocument oMPDocument)
        {
            Segment oSegment = Segment.GetSegment(xTagID, oMPDocument, true, true);
            return oSegment;
        }
        public Segment GetSegment(string xTagID, ForteDocument oMPDocument, bool bExecuteActions)
        {
            Segment oSegment = Segment.GetSegment(xTagID, oMPDocument, bExecuteActions, true);
            return oSegment;
        }
        public Segment GetSegment(string xTagID, ForteDocument oMPDocument, bool bExecuteActions, bool bPromptForMissingAuthors)
		{
            Segment oSegment = Segment.GetSegment(xTagID, oMPDocument, bExecuteActions, bPromptForMissingAuthors, true);
            return oSegment;
        }
        public Segment GetSegment(string xTagID, ForteDocument oMPDocument, bool bExecuteActions, bool bPromptForMissingAuthors, bool bForceHashTable)
		{
            Segment oSegment = Segment.GetSegment(xTagID, oMPDocument, bExecuteActions, bPromptForMissingAuthors, bForceHashTable);
            return oSegment;
        }
        /// <summary>
        /// returns value of designated word tag attribute
        /// </summary>
        /// <param name="oXMLNode"></param>
        /// <param name="xAttributeName"></param>
        /// <returns></returns>
        public string GetAttributeValue(Word.XMLNode oXMLNode, string xAttributeName)
        {
            return LMP.String.Decrypt(oXMLNode.SelectSingleNode(
                "@" + xAttributeName, null, true).NodeValue);
        }
        /// <summary>
        /// returns value of designated content control attribute
        /// </summary>
        /// <param name="oXMLNode"></param>
        /// <param name="xAttributeName"></param>
        /// <returns></returns>
        public string GetAttributeValue(Word.ContentControl oCC, string xAttributeName)
        {
            return LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(oCC, xAttributeName);
        }
        /// <summary>
        /// returns value of designated segment bookmark attribute
        /// </summary>
        /// <param name="oXMLNode"></param>
        /// <param name="xAttributeName"></param>
        /// <returns></returns>
        public string GetAttributeValue(Word.Bookmark oBmk, string xAttributeName)
        {
            return LMP.Forte.MSWord.WordDoc.GetAttributeValue_Bmk(oBmk, xAttributeName);
        }
        /// <summary>
		/// inserts at the specified location the segments defined in the specified task set
		/// </summary>
		/// <param name="iTaskSetID"></param>
		/// <param name="oLocation"></param>
		/// <returns></returns>
		public Segment[] InsertTaskSet(int iTaskSetID, Word.Range oLocation)
		{
			//TODO: implement this method
			return null;
		}

		/// <summary>
		/// returns the segment with the specified segment tag ID
		/// </summary>
		/// <param name="xSegmentTagID"></param>
		/// <returns></returns>
        public Segment FindSegment(string xSegmentTagID)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xSegmentTagID", xSegmentTagID);

            //get the position of the first segment separator
            int iPos = xSegmentTagID.IndexOf(".");

            if (this.Segments.Count == 0)
            {
                //ensure that we have a populated segments collection
                this.RefreshSegments(false, false);
            }

            Segment oSegment = null;
            string xTagID = "";

            if (iPos == -1)
            {
                try
                {
                    //there is no segment separator - return 
                    //the segment with this tag ID
                    oSegment = this.Segments[xSegmentTagID];
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString(
                            "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                }
            }
            else
            {
                //cycle through all segments in tag ID,
                //returning each as the child of the previous tag
                while (iPos > -1)
                {
                    //get nested segment
                    xTagID = xSegmentTagID.Substring(0, iPos);

                    try
                    {
                        if (oSegment == null)
                        {
                            //this is the top level segment
                            oSegment = this.Segments[xTagID];
                            if (oSegment == null)
                                return null;
                        }
                        else
                            oSegment = oSegment.Segments[xTagID];
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString(
                                "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                    }

                    //get the next segment separator
                    iPos = xSegmentTagID.IndexOf(".", iPos + 1);
                }

                try
                {
                    //get last segment in tag ID
                    oSegment = oSegment.Segments[xSegmentTagID];
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString(
                        "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                }
            }

            LMP.Benchmarks.Print(t0);

            return oSegment;
        }
        /// <summary>
        /// refreshes this ForteDocument
        /// </summary>
        /// <param name="oOptions"></param>
        public void Refresh(RefreshOptions oOptions)
        {
            Refresh(oOptions, true, true, true);
        }
        public void Refresh(RefreshOptions oOptions, bool bExecuteActions)
        {
            Refresh(oOptions, bExecuteActions, true, true);
        }
        public void Refresh(RefreshOptions oOptions, bool bExecuteActions, bool bPromptForMissingAuthors)
        {
            Refresh(oOptions, bExecuteActions, bPromptForMissingAuthors, true);
        }
        public void Refresh(RefreshOptions oOptions, bool bExecuteActions, bool bPromptForMissingAuthors, bool bLoadContentControls)
		{
			DateTime t0 = DateTime.Now;

            //GLOG 8007 - reinsert textboxes converted in Word 2013 since opening
            //to prevent access issues
            if (LMP.Forte.MSWord.WordDoc.IsConvertedInWord2013(this.WordDocument))
            {
                ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                LMP.Forte.MSWord.WordDoc.ReinsertTextboxesInAllStories(this.WordDocument);
                ForteDocument.IgnoreWordXMLEvents = iEvents;

                //GLOG 8007 (7/2/15, dm) - prevent repeated reinsertion of textboxes
                LMP.Forte.MSWord.WordDoc.MarkCompatibilityMode(this.WordDocument);
            }

            //refresh tags if specified - depending on options,
            //we might refresh TagIDs using GUIDs(for the editor) 
            //or Integers(for the designer)
            if (oOptions == RefreshOptions.RefreshTagsWithGUIDIndexes)
                this.RefreshTags(true);
            else if (oOptions == RefreshOptions.RefreshTagsWithIntegerIndexes)
                this.RefreshTags(false);

			//get top level segments
            this.RefreshSegments(bExecuteActions, bPromptForMissingAuthors, bLoadContentControls);

            //JTS 12/27/12: Skip this, since mVar and mBlock may now be at top level
            //LMP.Forte.MSWord.WordDoc.DeleteStandaloneTags(this.WordDocument);

            LMP.Benchmarks.Print(t0);
		}

        /// <summary>
        /// raises the BeforeVariableActionsExecuted event
        /// </summary>
        /// <param name="xTagID"></param>
        internal void RaiseVariableDeletedEvent(Variable oVar)
        {
            if (this.VariableDeleted != null)
                this.VariableDeleted(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        /// raises the VariableAdded event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseVariableAddedEvent(Variable oVar)
        {
            if (this.VariableAdded != null)
                this.VariableAdded(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        /// raises the VariableDefinitionChanged event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseVariableDefinitionChangedEvent(Variable oVar)
        {
            if (this.VariableDefinitionChanged != null)
                this.VariableDefinitionChanged(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        internal void RaiseBlockDeletedEvent(Block oBlock)
        {
            if (this.BlockDeleted != null)
                this.BlockDeleted(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the BlockAdded event
        /// </summary>
        /// <param name="oBlock"></param>
        internal void RaiseBlockAddedEvent(Block oBlock)
        {
            if (this.BlockAdded != null)
                this.BlockAdded(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the BlockDefinitionChanged event
        /// </summary>
        /// <param name="oBlock"></param>
        internal void RaiseBlockDefinitionChangedEvent(Block oBlock)
        {
            if (this.BlockDefinitionChanged != null)
                this.BlockDefinitionChanged(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the SegmentRefreshedByAction event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseSegmentRefreshedByActionEvent(Segment oSegment)
        {
            if (this.SegmentRefreshedByAction != null)
                this.SegmentRefreshedByAction(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the SegmentDeleted event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentDeletedEvent(Segment oSeg)
        {
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the SegmentAdded event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentAddedEvent(Segment oSeg)
        {
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the SegmentDistributedSectionsChanged event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentDistributedSectionsChangedEvent(Segment oSeg)
        {
            if (this.SegmentDistributedSectionsChanged != null)
                this.SegmentDistributedSectionsChanged(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the BeforeSegmentReplacedByAction event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseBeforeSegmentReplacedByActionEvent(Segment oSegment)
        {
            if (this.BeforeSegmentReplacedByAction != null)
                this.BeforeSegmentReplacedByAction(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the BeforeCollectionTableStructureInserted event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseBeforeCollectionTableStructureInsertedEvent(Segment oSegment)
        {
            if (this.BeforeCollectionTableStructureInserted != null)
                this.BeforeCollectionTableStructureInserted(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the AfterCollectionTableStructureInserted event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseAfterCollectionTableStructureInsertedEvent(Segment oSegment)
        {
            if (this.AfterCollectionTableStructureInserted != null)
                this.AfterCollectionTableStructureInserted(this, new SegmentEventArgs(oSegment));
        }

        public void RebuildSegment(Segment oSegment)
		{
		}
		public void RebuildSegmentAs(Segment oSegment, string xSegmentID, 
            Segment.InsertionLocations iInsertionLocation, 
            Segment.InsertionBehaviors iInsertionBehavior)
		{
            Prefill oPrefill = oSegment.CreatePrefill();

			//get current insertion location
            Word.Range oRng = oSegment.PrimaryRange;

			//delete current segment
            object oMissing = System.Type.Missing;
            oRng.Delete(ref oMissing, ref oMissing);

            //insert new segment of specified type at location
            Segment.Insert(xSegmentID, null, iInsertionLocation,
                iInsertionBehavior, this, oPrefill, true, null);

            //add old body
            //TODO: add old body to rebuilt segment
		}

        public void RefreshOrphanNodes()
        {
            this.OrphanNodes = new NodeStore(this.Tags, true);
        }
        
        /// <summary>
        /// Attach the current document to the specified template
        /// </summary>
        /// <param name="xNewTemplate"></param>
        /// <param name="bForceUpdate"></param>
        public void AttachToTemplate(string xNewTemplate, bool bForceUpdate)
        {
            if (xNewTemplate != "" && xNewTemplate != null)
            {
                
                //If no path specified, used MacPac Templates directory
                if (Path.GetDirectoryName(xNewTemplate) == string.Empty)
                {
                    //Use default templates location
                    xNewTemplate = Data.Application.TemplatesDirectory + @"\" + @xNewTemplate;
                }
                
                //GLOG 3088: Do nothing if template is already attached
                Word.Template oCurrentTemplate = (Word.Template)m_oWordDocument.get_AttachedTemplate();
                if (xNewTemplate.ToUpper() == oCurrentTemplate.FullName.ToUpper()
                    && !bForceUpdate)
                    return;

                if (System.IO.File.Exists(xNewTemplate))
                {
                    object oTemplate = (object)xNewTemplate;
                    try
                    {
                        m_oWordDocument.set_AttachedTemplate(ref oTemplate);
                    }
                    catch(System.Exception oE)
                    {
                        //attempt to find a variant that will work - .dot or .dotx
                        string xVariantTemplate = null;

                        if (xNewTemplate.EndsWith(".dotx"))
                            xVariantTemplate = xNewTemplate.Substring(0, xNewTemplate.Length - 1);
                        else if (xNewTemplate.EndsWith(".dot"))
                            xVariantTemplate = xNewTemplate + "x";
                        else
                            throw oE;

                        if (File.Exists(xVariantTemplate))
                        {
                            try
                            {
                                oTemplate = (object)xVariantTemplate;

                                //attempt to attach to the variant template
                                m_oWordDocument.set_AttachedTemplate(ref oTemplate);
                            }
                            catch
                            {
                                throw oE;
                            }
                        }
                        else
                            //variant doesn't exist - raise original error
                            throw oE;

                    }

                    //Update styles from template
                    m_oWordDocument.UpdateStyles();
                }
                else
                {
                    throw new LMP.Exceptions.FileException(
                        LMP.Resources.GetLangString(
                        "Error_FileDoesNotExist") + xNewTemplate);
                }
            }
        }

        /// <summary>
        /// Copies all or specified styles defined in source XML
        /// </summary>
        /// <param name="xBaseXML"></param>
        /// <param name="aStyleList"></param>
        public void UpdateStylesFromXML(string xBaseXML)
        {
            UpdateStylesFromXML(xBaseXML, null, m_oWordDocument);
        }
        public void UpdateStylesFromXML(string xBaseXML, string[] aStyleList)
        {
            UpdateStylesFromXML(xBaseXML, aStyleList, m_oWordDocument);
        }
        /// <summary>
        /// Copies all or specified styles defined in source XML
        /// </summary>
        /// <param name="xBaseXML"></param>
        /// <param name="aStyleList"></param>
        public static void UpdateStylesFromXML(string xBaseXML, Word.Document oTargetDocument)
        {
            UpdateStylesFromXML(xBaseXML, null, oTargetDocument);
        }
        public static void UpdateStylesFromXML(string xBaseXML, string[] aStyleList, Word.Document oTargetDocument)
        {
            //NOTE (JTS 11/24/15):  COM OrganizerCopyStyles does not appear to work correctly
            //with Style Sheet Segments that have gone through conversion to OPC and back to Flat XML -
            //it was necessary to import these segments from the 11.1 DB to have styles updated.
            //Need to look into this further, but one thing I noticed was that the 11.1 segments contained
            //a StylesWithEffects part, while the reconverted segments did not.
            DateTime t0 = DateTime.Now;

            string xSourcePath = "";

            try
            {
                //Extract style nodes from XML
                //string xStyleXML = LMP.String.GetMinimalStyleDocumentXML(xBaseXML);
                string xStyleXML = xBaseXML;
                // Convert XML text to byte array
                byte[] bytXML = System.Text.Encoding.UTF8.GetBytes((xStyleXML).ToCharArray());

                // Create temporary file
                xSourcePath = Path.GetTempFileName().Replace(".tmp", ".xml");

                try
                {
                    FileStream oStream = File.Open(xSourcePath, FileMode.Create);

                    // Write bytXML to temporary file
                    oStream.Write(bytXML, 0, bytXML.Length);
                    oStream.Close();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.XMLException(oE.Message);
                }

                //GLOG 7915 (dm) - the ContentControlExit event is sometimes firing here
                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                try
                {
                    DateTime t1 = DateTime.Now;
                    LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(oTargetDocument, xSourcePath, aStyleList);
                    LMP.Benchmarks.Print(t1, "OrganizerCopyStyles");
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.WordDocException(oE.Message);
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = oEvents;
                }
            }
            finally
            {
                try
                {
                    // clean up temporary file
                    if (File.Exists(xSourcePath))
                        File.Delete(xSourcePath);
                }
                catch (IOException)
                {
                    OS.AddFileToDeletionList(xSourcePath);
                }
                LMP.Benchmarks.Print(t0);
            }

        }
        /// <summary>
        /// Returns one-dimensional array containing names of styles in document
        /// </summary>
        /// <param name="bInUseOnly"></param>
        /// <returns></returns>
        public string[] GetStyleArray(bool bInUseOnly)
        {
            System.Collections.Generic.List<string> aStyles = new System.Collections.Generic.List<string>();
            foreach (Word.Style oStyle in this.WordDocument.Styles)
            {
                if (!bInUseOnly || oStyle.InUse)
                {
                    aStyles.Add(oStyle.NameLocal);
                }
            }
            return aStyles.ToArray();
        }
        /// <summary>
        /// Insert sample paragraphs in each document style
        /// </summary>
        public void InsertStyleSummary(bool bInUseOnly, string xDescription)
        {
            const string xTemplate = "{0} {0} {0} {0} {0}\r\n";
            string xSampleText = LMP.Resources.GetLangString("Prompt_StyleSummarySample");
            //GLOG 4440: This code has been restored since Word 2003 compatibility is no longer a concern
            try
            {
                //Convert from XML to avoid Compatibility Mode
                this.WordDocument.Convert();
            }
            catch { }
            string[] aStyles = GetStyleArray(bInUseOnly);
            Word.Range oRng = this.WordDocument.Content;
            object oEnd = Word.WdCollapseDirection.wdCollapseEnd;
            object oSpaceAfter = 24;
            object oNormalStyleID = Word.WdBuiltinStyle.wdStyleNormal;
            object oDefaultParaID = Word.WdBuiltinStyle.wdStyleDefaultParagraphFont;
            Word.Style oNormal = this.WordDocument.Styles.get_Item(ref oNormalStyleID);
            string xNormalName = oNormal.NameLocal;
            string xSentence = string.Format(xSampleText, xNormalName + " paragraph");
            string xParagraph = string.Format(xTemplate, xSentence);
            object oNormalObject = (object)xNormalName;
            oRng.set_Style(ref oNormalObject);
            oRng.InsertAfter(xDescription + "\r\n");
            oRng.Font.Bold = 1;
            oRng.Paragraphs[1].Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            oRng.Paragraphs[1].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdYellow;
            oRng.ParagraphFormat.SpaceAfter = 24;
            oRng.Collapse(ref oEnd);
            oRng.InsertAfter(xParagraph);
            oRng.Font.Reset();
            oRng.ParagraphFormat.SpaceAfter = 24;
            oRng.Collapse(ref oEnd);
            foreach (string xStyle in aStyles)
            {
                object oStyleObject = (object)xStyle;
                Word.Style oStyle = this.WordDocument.Styles.get_Item(ref oStyleObject);
                object oStyleName = (object)oStyle.NameLocal;
                string xType = "";
                switch (oStyle.Type)
                {
                    case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeCharacter:
                        //Skip Default Paragraph Font style
                        if (oStyle.NameLocal == this.WordDocument.Styles.get_Item(ref oDefaultParaID).NameLocal)
                            continue;
                        xType = " character";
                        break;
                    case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph:
                        //Skip Normal Style, since it's already included
                        if (oStyle.NameLocal == this.WordDocument.Styles.get_Item(ref oNormalStyleID).NameLocal)
                            continue;
                        xType = " paragraph";
                        break;
                    case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeTable:
                        xType = " table";
                        //Don't process this, as it's only valid inside a table
                        continue;
                    case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeList:
                        xType = " list";
                        break;
                }
                xSentence = string.Format(xSampleText, oStyle.NameLocal + xType);
                xParagraph = string.Format(xTemplate, xSentence);
                //Reset Normal first
                oRng.set_Style(ref oNormalObject);
                oRng.set_Style(ref oStyleName);
                oRng.InsertAfter(xParagraph);
                oRng.ParagraphFormat.SpaceAfter = 24;
                oRng.Collapse(ref oEnd);
            }
            oRng.set_Style(ref oNormalObject);
        }
        /// <summary>
        /// Removes the temp Callout shape object if it exists
        /// </summary>
        public void ClearCallout()
        {
            Word.Document oDoc = this.WordDocument;
            LMP.Forte.MSWord.WordDoc.RemoveCalloutIfNecessary(oDoc);
        }

        public Segment GetTopLevelSegmentForRange(Word.Range oRng)
        {
            return GetTopLevelSegmentForRange(oRng, false);
        }

        public Segment GetTopLevelSegmentForRange(Word.Range oRng,
            bool bIncludeIntendedAsDocument)
        {
            DateTime t0 = DateTime.Now;

            //GLOG 6005 (dm) - added bIncludeIntendedAsDocument parameter
            Segment oSegment = null;
            if (this.FileFormat == mpFileFormats.OpenXML)
            {
                //GLOG 4215: Allows getting top level segment for a range without affecting current Selection
                Word.ContentControl oCC = null;
                try
                {
                    oCC = oRng.ParentContentControl;
                }
                catch { }
                if (oCC == null)
                {
                    //If not parent tag, look for first tag within range
                    if (oRng.Start == oRng.End)
                    {
                        //If range is collapsed, extend end 1 character to encompass any starting XML tag
                        object oChar = Word.WdUnits.wdCharacter;
                        object oCount = 1;
                        try
                        {
                            oRng.MoveEnd(ref oChar, ref oCount);
                        }
                        catch { }
                    }
                    if (oRng.ContentControls.Count > 0)
                    {
                        object iIndex = 1;
                        oCC = oRng.ContentControls.get_Item(ref iIndex);
                    }
                }

                //keep checking parent node until an mSEG is found
                while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
                    oCC = oCC.ParentContentControl;

                string xSegFullTagID = "";
                if (oCC == null)
                {
                    Word.Bookmark oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRng);
                    if (oParentBmk != null)
                        xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
                }
                else
                {
                    //get the segment corresponding to tag
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oCC);
                }
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.FindSegment(xSegFullTagID);
            }
            else if (LMP.Forte.MSWord.WordApp.Version < 15) //XML Tags not supported in Word 2013
            {
                //GLOG 4215: Allows getting top level segment for a range without affecting current Selection
                Word.XMLNode oTag = null;
                try
                {
                    oTag = oRng.XMLParentNode;
                }
                catch { }
                if (oTag == null)
                {
                    //If not parent tag, look for first tag within range
                    if (oRng.Start == oRng.End)
                    {
                        //If range is collapsed, extend end 1 character to encompass any starting XML tag
                        object oChar = Word.WdUnits.wdCharacter;
                        object oCount = 1;
                        try
                        {
                            oRng.MoveEnd(ref oChar, ref oCount);
                        }
                        catch { }
                    }
                    if (oRng.XMLNodes.Count > 0)
                    {
                        oTag = oRng.XMLNodes[1];
                    }
                }

                string xSegFullTagID = "";

                //keep checking parent node until an mSEG is found
                while ((oTag != null) && (oTag.BaseName != "mSEG"))
                    oTag = oTag.ParentNode;

                if (oTag == null)
                {
                    Word.Bookmark oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRng);
                    if (oParentBmk != null)
                        xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
                }
                else
                {
                    //get the segment corresponding to tag
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oTag);
                }
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.FindSegment(xSegFullTagID);
            }

            //7/10/08 - removed the following block because an mSEG
            //may belong to a different client
            //if (oSegment == null)
            //    //something is wrong - there's an mSEG tag but no
            //    //corresponding mSEG
            //    throw new LMP.Exceptions.SegmentException(
            //        LMP.Resources.GetLangString("Error_mSEGButNoSegment") + xSegFullTagID);

            //cycle through parents until we 
            //get to the top level segment
            //GLOG 6005 (dm) - added exceptions for TOA and intended "as document"
            if (oSegment != null)
            {
                while ((oSegment.Parent != null) && ((!bIncludeIntendedAsDocument) ||
                    ((!(oSegment is LMP.Architect.Api.TOA)) &&
                    (oSegment.IntendedUse != mpSegmentIntendedUses.AsDocument))))
                    oSegment = oSegment.Parent;
            }

            LMP.Benchmarks.Print(t0);

            return oSegment;

        }
        /// <summary>
        /// returns the top level segment that
        /// contains the current selection
        /// </summary>
        /// <returns></returns>
        public Segment GetSegmentFromSelection(LMP.Data.mpObjectTypes iType)
        {
            //JTS 5/31/10 10.2: Added Content Control compatibility
            Segment oSegment = null;
            if (this.FileFormat == mpFileFormats.OpenXML)
            {
                Word.Range oRange = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range;
                Word.ContentControl oCC = oRange.ParentContentControl;
                string xSegFullTagID = "";

                //GLOG 4979 (dm) - cursor may be immediately before
                //top-level content control
                if (oCC == null)
                {
                    object oMissing = System.Type.Missing;
                    oRange.Move(ref oMissing, ref oMissing);
                    oCC = oRange.ParentContentControl;
                }

                if (oCC == null)
                {
                    Word.Bookmark oBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRange);

                    if (oBmk == null)
                    {
                        return null;
                    }
                    else
                    {
                        //get the segment corresponding to bookmark
                        xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oBmk);

                        if (!System.String.IsNullOrEmpty(xSegFullTagID))
                            oSegment = this.FindSegment(xSegFullTagID);
                    }
                }
                else
                {
                    //keep checking parent node until an mSEG is found
                    while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
                        oCC = oCC.ParentContentControl;
                }

                if (oCC != null)
                {
                    //get the segment corresponding to tag
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oCC);
                    if (!System.String.IsNullOrEmpty(xSegFullTagID))
                        oSegment = this.FindSegment(xSegFullTagID);
                }
            }
            else if (LMP.Forte.MSWord.WordApp.Version < 15) //XML Tags not supported in Word 2013
            {
                string xSegFullTagID = "";

                Word.Application oWordApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                Word.Range oRange = oWordApp.Selection.Range;

                Word.XMLNode oTag = oWordApp.Selection.XMLParentNode;

                if (oTag == null)
                {
                    Word.Bookmark oBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRange);

                    if (oBmk == null)
                    {
                        return null;
                    }
                    else
                    {
                        //get the segment corresponding to bookmark
                        xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oBmk);

                        if (!System.String.IsNullOrEmpty(xSegFullTagID))
                            oSegment = this.FindSegment(xSegFullTagID);
                    }
                }
                else
                {
                    //keep checking parent node until an mSEG is found
                    while ((oTag != null) && (oTag.BaseName != "mSEG"))
                        oTag = oTag.ParentNode;
                }

                if (oTag != null)
                {
                    //get the segment corresponding to tag
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oTag);
                    if (!System.String.IsNullOrEmpty(xSegFullTagID))
                        oSegment = this.FindSegment(xSegFullTagID);
                }
            }

            //cycle through parents until we
            //find the segment of the specified type,
            //or we get to the top level segment
            if (oSegment != null)
            {
                if (oSegment.TypeID == iType)
                {
                    return oSegment;
                }
                else
                {
                    while (oSegment.Parent != null)
                    {
                        if (oSegment.Parent.TypeID == iType)
                            return oSegment;

                        oSegment = oSegment.Parent;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// returns the top level segment that
        /// contains the current selection
        /// </summary>
        /// <returns></returns>
        public Segment GetTopLevelSegmentFromSelection()
        {
            //JTS 1/17/13: Use GetTopLevelSegmentForRange on Selection
            Word.Range oRange = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range;
            return GetTopLevelSegmentForRange(oRange);
            
            ////JTS 5/31/10 10.2: Added Content Control compatibility
            //Segment oSegment = null;
            //Word.Bookmark oBmk = null;
            //if (this.FileFormat == mpFileFormats.OpenXML)
            //{
            //    Word.Range oRange = LMP.Forte.MSWord.WordApp
            //        .CurrentWordApplication().Selection.Range;
            //    Word.ContentControl oCC = oRange.ParentContentControl;
            //    //GLOG 4979 (dm) - cursor may be immediately before
            //    //top-level content control
            //    if (oCC == null)
            //    {
            //        object oMissing = System.Type.Missing;
            //        oRange.Move(ref oMissing, ref oMissing);
            //        oCC = oRange.ParentContentControl;
            //    }

            //    if (oCC == null)
            //        oBmk = GetTopLevelSegmentForRange
            //        return null;
            //    else
            //        //keep checking parent node until an mSEG is found
            //        while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
            //            oCC = oCC.ParentContentControl;
            //    string xSegFullTagID = "";
            //    if (oCC != null)
            //    {
            //        //get the segment corresponding to tag
            //
            //        xSegFullTagID = oDoc.GetFullTagID_CC(oCC);
            //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
            //            oSegment = this.FindSegment(xSegFullTagID);
            //    }
            //}
            //else
            //{
            //    Word.XMLNode oTag = LMP.Forte.MSWord.WordApp
            //        .CurrentWordApplication().Selection.XMLParentNode;

            //    if (oTag == null)
            //        return null;
            //    else
            //        //keep checking parent node until an mSEG is found
            //        while ((oTag != null) && (oTag.BaseName != "mSEG"))
            //            oTag = oTag.ParentNode;
            //    string xSegFullTagID = "";
            //    if (oTag != null)
            //    {
            //        //get the segment corresponding to tag
            //
            //        xSegFullTagID = oDoc.GetFullTagID(oTag);
            //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
            //            oSegment = this.FindSegment(xSegFullTagID);
            //    }
            //}

            ////7/10/08 - removed the following block because an mSEG
            ////may belong to a different client
            ////if (oSegment == null)
            ////    //something is wrong - there's an mSEG tag but no
            ////    //corresponding mSEG
            ////    throw new LMP.Exceptions.SegmentException(
            ////        LMP.Resources.GetLangString("Error_mSEGButNoSegment") + xSegFullTagID);

            ////cycle through parents until we 
            ////get to the top level segment
            ////if (oSegment != null)
            ////{
            ////    while (oSegment.Parent != null)
            ////        oSegment = oSegment.Parent;
            ////}

            //return oSegment;
        }

        /// <summary>
        /// updates the contact detail for the macpac document
        /// </summary>
        public bool UpdateContactDetail()
        {
            try
            {
                bool bDetailUpdated = false;
                for (int i = 0; i < this.Segments.Count; i++)
                    bDetailUpdated = this.Segments[i].UpdateContactDetail();
                return bDetailUpdated;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotUpdateMPDocumentContactDetail"), oE);
            }
            }

        /// <summary>
        /// returns true if the document contains any segments or standalone
        /// variables or blocks belonging to the current client
        /// </summary>
        /// <returns></returns>
        public bool ContainsClientContent()
        {
            //segments
            for (int i = 0; i < this.Segments.Count; i++)
            {
                if (this.Segments[i].BelongsToCurrentClient)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// creates a snapshot for each segment
        /// in the MacPac Document
        /// </summary>
        public void CreateSegmentSnapshots()
        {
            //cycle through all top level segments in macpac document
            //creating a "snapshot" of variable values
            for (int i = 0; i < this.Segments.Count; i++)
            {
                //GLOG 6060 (dm) - skip trailers
                Segment oSegment = this.Segments[i];
                if (oSegment.TypeID != mpObjectTypes.Trailer && 
                    !(oSegment is LMP.Architect.Base.IStaticDistributedSegment))  //GLOG 7874
                    Snapshot.Create(oSegment);
            }
        }

        /// <summary>
        /// returns whether the MacPac Document contains any unfinished segments
        /// </summary>
        /// <param name="bIncludeStaticDistributedSegments"></param>
        /// <returns></returns>
        public bool ContainsUnfinishedContent(bool bIncludeStaticDistributedSegments)
        {
            //GLOG 15860 (dm) - added bIncludeStaticDistributedSegments parameter
            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oSegment = this.Segments[i];
                //10-10-11 (dm) - modified to check for variables and blocks
                //instead of snapshots - segments designed without variables
                //will not have snapshots but should be treated as finished
                if ((bIncludeStaticDistributedSegments || !(oSegment is LMP.Architect.Base.IStaticDistributedSegment)) &&
                    ((oSegment.HasVariables(true) || oSegment.HasBlocks(true))))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// returns whether the MacPac Document contains any unfinished segments
        /// </summary>
        /// <returns></returns>
        public bool ContainsUnfinishedContent()
        {
            return ContainsUnfinishedContent(true);
        }

        public bool ContainsUnfinishedTopLevelContent()
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                Segment oSegment = this.Segments[i];
                //10-10-11 (dm) - modified to check for variables and blocks
                //instead of snapshots - segments designed without variables
                //will not have snapshots but should be treated as finished
                if (oSegment.HasVariables(false) || oSegment.HasBlocks(false))
                    return true;
            }
            return false;
        }
        public void SetDynamicEditingEnvironment()
        {
            SetDynamicEditingEnvironment(false);
        }
        public void SetDynamicEditingEnvironment(bool bForce)
        {
            if (bForce || !m_bDynamicEditingSet)
            {
                DateTime t0 = DateTime.Now;
                WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = WordXMLEvents.All;
                try
                {
                    LMP.Forte.MSWord.WordDoc.SetDynamicEditingEnvironment(this.m_oWordDocument);
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = iIgnore;
                }
                LMP.Benchmarks.Print(t0);
            }
        }
        public bool DynamicEditingInProgress
        {
            get { return m_bDynamicEditingSet; }
            set 
            {
                DateTime t0 = DateTime.Now;
                WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = WordXMLEvents.All;
                try
                {

                    if (value == true && !m_bDynamicEditingSet)
                    {
                        //If changing to True, setup Dynamic Editing Environment
                        LMP.Forte.MSWord.WordDoc.SetDynamicEditingEnvironment(this.m_oWordDocument);
                    }
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = iIgnore;
                    LMP.Benchmarks.Print(t0, value.ToString());
                    m_bDynamicEditingSet = value;
                }
            }
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// populates segments collection with data from Tags collection
        /// </summary>
        private void RefreshSegments()
        {
            RefreshSegments(true);
        }
        private void RefreshSegments(bool bExecuteActions)
        {
            RefreshSegments(bExecuteActions, true);
        }
        private void RefreshSegments(bool bExecuteActions, bool bPromptForMissingAuthors)
        {
            RefreshSegments(bExecuteActions, bPromptForMissingAuthors, true);
        }
        private void RefreshSegments(bool bExecuteActions, bool bPromptForMissingAuthors, bool bLoadContentControls)
        {
            if (m_oSegments != null)
                //clear existing segments collection
                m_oSegments.Clear();
            else
            {
                //create new segments collection
                m_oSegments = new Segments(this);

                //subscribe to events
                m_oSegments.SegmentAdded += new SegmentAddedHandler(m_oSegments_SegmentAdded);
                m_oSegments.SegmentDeleted += new SegmentDeletedHandler(m_oSegments_SegmentDeleted);
            }

            //exit if the tags collection 
            //hasn't yet been retrieved
            if (this.Tags == null)
                return;

            //cycle through tags collection adding segments
            string xSegments = "";
            for (int i = 1; i <= this.Tags.Count; i++)
            {
                LMP.Forte.MSWord.Tag oTag = this.Tags.get_Item(i);
                if (oTag.ElementName == "mSEG")
                {
                    //add segment to segments collection only
                    //if not already added via another part
                    string xTagID = oTag.FullTagID;
                    if (!xSegments.Contains(xTagID + "|"))
                    {
                        Segment oSeg = this.GetSegment(xTagID, this, bExecuteActions, bPromptForMissingAuthors, bLoadContentControls);
                        m_oSegments.Add(oSeg);
                        xSegments = xSegments + xTagID + "|";

                        //set unlinked child segment authors -
                        //GetSegment() has already set oSeg's authors
                        //and author changes are automatically propagated
                        //to linked child segment authors
                        if (bLoadContentControls)
                            oSeg.SetChildAuthorsFromDocument(bExecuteActions);

                        //GLOG 3605 - set status of child segments to finished
                        oSeg.SetChildSegmentsStatus(Segment.Status.Finished);
                    }
                }
            }
        }

        ///// <summary>
        ///// populates the standalone variables collection with data
        ///// from the Tags collection
        ///// </summary>
        //private void RefreshVariables()
        //{
        //    RefreshOrphanNodes();
        //    m_oVariables = new Variables(this);
        //}

        /// <summary>
        /// populates the standalone blocks collection with data
        /// from the Tags collection
        /// </summary>
        private void RefreshBlocks()
        {
            RefreshOrphanNodes();
            m_oBlocks = new Blocks(this);
        }
        #endregion
        #region *********************event handlers*********************
        void m_oSegments_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            Segment oSeg = oArgs.Segment;
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSeg));
        }
        void m_oSegments_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            Segment oSeg = oArgs.Segment;
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSeg));
        }
        #endregion
    }
}
