using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Reflection;
using LMP.Controls;
using Word=Microsoft.Office.Interop.Word;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Architect.Api
{
    public class UserVariable: Variable
	{
        internal UserVariable(Segment oParent) : base(oParent)
        {
            //get new GUID as ID -
            //user variable IDs are prefixed with a 'U'
            this.ID = "U" + System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
        internal UserVariable(ForteDocument oParent) : base(oParent)
        {
            //get new GUID as ID -
            //user variable IDs are prefixed with a 'U'
            this.ID = "U" + System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
    }

	public class AdminVariable: Variable
	{
        internal AdminVariable(Segment oParent) : base(oParent)
        {
            //get new GUID as ID
            this.ID = System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
        internal AdminVariable(ForteDocument oParent) : base(oParent)
        {
            //get new GUID as ID
            this.ID = System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
    }

    /// <summary>
    /// contains properties that define the arguments for the VariableDeleted
    /// and VariableAdded events
    /// </summary>
    public class VariableEventArgs
    {
        private Variable m_oVar;

        public VariableEventArgs(Variable oVar)
        {
            m_oVar = oVar;
        }

        public Variable Variable
        {
            get { return m_oVar; }
            set { m_oVar = value; }
        }
    }
    /// <summary>
    /// contains the properties to handle a request to update Contacts
    /// </summary>
    public class BeforeContactsUpdateEventArgs
    {
        public bool Cancel = false;
    }
    public delegate void BeforeContactsUpdateHandler(object sender, BeforeContactsUpdateEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableDeleted event handler
    /// </summary>
    public delegate void VariableDeletedHandler(object sender, VariableEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableAdded event handler
    /// </summary>
    public delegate void VariableAddedHandler(object sender, VariableEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableDefinitionChanged event handler
    /// </summary>
    public delegate void VariableDefinitionChangedHandler(object sender, VariableEventArgs oArgs);

	public delegate void ValueAssignedEventHandler(object sender, EventArgs e);
    public delegate void VariableVisitedHandler(object sender, VariableEventArgs e);

	/// <summary>
	/// contains the methods and properties that define a
	/// MacPac Variable - created by Daniel Fisherman 09/20/05
	/// </summary>
    public abstract class Variable : LMP.Architect.Base.Variable
	{
		#region *********************fields*********************
        private VariableActions m_oActions = null;
		private ControlActions m_oControlActions;
		private Segment m_oSegment;
        private ForteDocument m_oForteDocument;
        private Variable m_oUISourceVariable = null;
        #endregion
        #region *********************events*********************
        public event ValueAssignedEventHandler ValueAssigned;
        public event VariableVisitedHandler VariableVisited;
        #endregion
        #region *********************constructors*********************
        internal Variable(Segment oParent)
        {
            m_oSegment = oParent;
            m_oForteDocument = m_oSegment.ForteDocument;
            m_oActions = new VariableActions(this);
            m_oControlActions = new ControlActions(this);

            //subscribe to events
            m_oActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oActions_ActionsDirtied);
            m_oControlActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oControlActions_ActionsDirtied);
        }

        internal Variable(ForteDocument oParent)
        {
            m_oForteDocument = oParent;
            m_oActions = new VariableActions(this);
            m_oControlActions = new ControlActions(this);

            //subscribe to events
            m_oActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oActions_ActionsDirtied);
            m_oControlActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oControlActions_ActionsDirtied);
        }
        #endregion
		#region *********************properties*********************
        /// <summary>
        /// returns the first word range associated with this variable
        /// </summary>
        public Word.Range FirstAssociatedWordRange
        {
            get
            {
                if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
                {
                    Word.XMLNode[] aTags = this.AssociatedWordTags;

                    if (aTags.Length > 0)
                        return aTags[0].Range;
                    else
                        return null;
                }
                else
                {
                    Word.ContentControl[] aCCs = this.AssociatedContentControls;

                    if (aCCs.Length > 0)
                        return aCCs[0].Range;
                    else
                        return null;
                }
            }
        }
		/// <summary>
		/// returns the Word.XMLNodes that are associated with this variable
		/// </summary>
        public Word.XMLNode[] AssociatedWordTags
        {
            get
            {
                if (this.TagID != "")
                {
                    //get the associated node from the tag id
                    Word.XMLNode[] oTags = null;
                    string xTagID = null;

                    if ((this.TagType == ForteDocument.TagTypes.Variable) ||
                        (this.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (this.TagType == ForteDocument.TagTypes.Mixed))
                        xTagID = this.TagID;
                    else if (this.TagType == ForteDocument.TagTypes.None && this.AssociatedSegment != null)
                        xTagID = this.AssociatedSegment.FullTagID;

                    if (xTagID != null)
                        //get associated tags
                        oTags = this.Parent.Nodes.GetWordTags(xTagID);
                    else
                        //variable def is in mSEG - return an empty array
                        oTags = new Word.XMLNode[0];

                    return oTags;
                }
                else
                    return null;
            }
        }
        /// <summary>
        /// returns the content controls that are associated with this variable
        /// </summary>
        public Word.ContentControl[] AssociatedContentControls
        {
            get
            {
                if (this.TagID != "")
                {
                    //get the associated node from the tag id
                    Word.ContentControl[] oTags = null;
                    string xTagID = null;

                    if ((this.TagType == ForteDocument.TagTypes.Variable) ||
                        (this.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (this.TagType == ForteDocument.TagTypes.Mixed))
                        xTagID = this.TagID;
                    else if (this.TagType == ForteDocument.TagTypes.None && this.AssociatedSegment != null)
                        xTagID = this.AssociatedSegment.FullTagID;

                    if (xTagID != null)
                        //get associated tags
                        oTags = this.Parent.Nodes.GetContentControls(xTagID);
                    else
                        //variable def is in mSEG - return an empty array
                        oTags = new Word.ContentControl[0];

                    return oTags;
                }
                else
                    return null;
            }
        }
        /// <summary>
        /// returns the content controls that are associated with this variable
        /// </summary>
        public Word.Bookmark[] AssociatedBookmarks
        {
            get
            {
                if (this.TagID != "")
                {
                    //get the associated node from the tag id
                    Word.Bookmark[] oTags = null;
                    string xTagID = null;

                    if ((this.TagType == ForteDocument.TagTypes.Variable) ||
                        (this.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (this.TagType == ForteDocument.TagTypes.Mixed))
                        xTagID = this.TagID;
                    else if (this.TagType == ForteDocument.TagTypes.None && this.AssociatedSegment != null)
                        xTagID = this.AssociatedSegment.FullTagID;

                    if (xTagID != null)
                        //get associated tags
                        oTags = this.Parent.Nodes.GetBookmarks(xTagID);
                    else
                        //variable def is in mSEG - return an empty array
                        oTags = new Word.Bookmark[0];

                    return oTags;
                }
                else
                    return null;
            }
        }
        /// <summary>
        /// returns the Word.XMLNode that contains the definition for this variable
        /// </summary>
        public Word.XMLNode[] ContainingWordTags
        {
            get
            {
                DateTime t0 = DateTime.Now;

                //we need to get these on the fly each time because
                //Word XMLNode object variables are notoriously unstable - 
                //e.g. copying and pasting in Word will cause XMLNode vars to reference
                //other XMLNode objects
                Word.XMLNode[] oTags = null;

                if (this.TagType == ForteDocument.TagTypes.Variable)
                    oTags = this.AssociatedWordTags;

                if (oTags == null)
                {
                    if (this.IsStandalone)
                        //something is wrong - standalone vars must have
                        //containing mVars that are also an associated tag 
                        throw new LMP.Exceptions.VariableException(
                            LMP.Resources.GetLangString("Error_NoAssoTagForStandaloneVar"));
                    else if (this.TagType == ForteDocument.TagTypes.Segment)
                    {
                        //variable def in in mSEG object data attribute -
                        //return all segment parts
                        oTags = m_oSegment.Nodes.GetWordTags(m_oSegment.FullTagID);
                    }
                    else if (m_oSegment.PrimaryWordTag != null)
                    {
                        //variable def is in mSEG DeletedScopes attribute -
                        //return only the relevant segment parts
                        //GLOG 6700 (dm) - ensure that TagParentPartNumbers is up-to-date
                        this.TagParentPartNumbers = m_oSegment.Nodes.GetItemPartNumbers(this.TagID);
                        string[] aParts = this.TagParentPartNumbers.Split(';');
                        oTags = new Word.XMLNode[aParts.Length];
                        for (int i = 0; i < aParts.Length; i++)
                        {
                            oTags[i] = m_oSegment.Nodes.GetWordTag(m_oSegment.FullTagID,
                                int.Parse(aParts[i]));   
                        }
                    }
                }

                LMP.Benchmarks.Print(t0);

                return oTags;
            }
        }
        /// <summary>
        /// returns the content controls that contains the definition for this variable
        /// </summary>
        public Word.ContentControl[] ContainingContentControls
        {
            get
            {
                DateTime t0 = DateTime.Now;

                //we need to get these on the fly each time because
                //Word XMLNode object variables are notoriously unstable - 
                //e.g. copying and pasting in Word will cause XMLNode vars to reference
                //other XMLNode objects
                Word.ContentControl[] oTags = null;

                if (this.TagType == ForteDocument.TagTypes.Variable)
                    oTags = this.AssociatedContentControls;

                if (oTags == null)
                {
                    if (this.IsStandalone)
                        //something is wrong - standalone vars must have
                        //containing mVars that are also an associated tag 
                        throw new LMP.Exceptions.VariableException(
                            LMP.Resources.GetLangString("Error_NoAssoTagForStandaloneVar"));
                    else if (this.TagType == ForteDocument.TagTypes.Segment)
                    {
                        //variable def in in mSEG object data attribute -
                        //return all segment parts
                        oTags = m_oSegment.Nodes.GetContentControls(m_oSegment.FullTagID);
                    }
                    else if (m_oSegment.PrimaryContentControl != null)
                    {
                        //variable def is in mSEG DeletedScopes attribute -
                        //return only the relevant segment parts
                        //GLOG 6700 (dm) - ensure that TagParentPartNumbers is up-to-date
                        this.TagParentPartNumbers = m_oSegment.Nodes.GetItemPartNumbers(this.TagID);
                        string[] aParts = this.TagParentPartNumbers.Split(';');
                        oTags = new Word.ContentControl[aParts.Length];
                        for (int i = 0; i < aParts.Length; i++)
                        {
                            oTags[i] = m_oSegment.Nodes.GetContentControl(m_oSegment.FullTagID,
                                int.Parse(aParts[i]));
                        }
                    }
                }

                LMP.Benchmarks.Print(t0);

                return oTags;
            }
        }
        /// <summary>
        /// returns the content controls that contains the definition for this variable
        /// </summary>
        public Word.Bookmark[] ContainingBookmarks
        {
            get
            {
                DateTime t0 = DateTime.Now;

                //we need to get these on the fly each time because
                //Word XMLNode object variables are notoriously unstable - 
                //e.g. copying and pasting in Word will cause XMLNode vars to reference
                //other XMLNode objects
                Word.Bookmark[] oTags = null;

                if (this.TagType == ForteDocument.TagTypes.Variable)
                    oTags = this.AssociatedBookmarks;

                if (oTags == null)
                {
                    if (this.IsStandalone)
                        //something is wrong - standalone vars must have
                        //containing mVars that are also an associated tag 
                        throw new LMP.Exceptions.VariableException(
                            LMP.Resources.GetLangString("Error_NoAssoTagForStandaloneVar"));
                    else if (this.TagType == ForteDocument.TagTypes.Segment)
                    {
                        //variable def in in mSEG object data attribute -
                        //return all segment parts
                        oTags = m_oSegment.Nodes.GetBookmarks(m_oSegment.FullTagID);
                    }
                    else if (m_oSegment.PrimaryBookmark != null)
                    {
                        //variable def is in mSEG DeletedScopes attribute -
                        //return only the relevant segment parts
                        //GLOG 6700 (dm) - ensure that TagParentPartNumbers is up-to-date
                        this.TagParentPartNumbers = m_oSegment.Nodes.GetItemPartNumbers(this.TagID);
                        string[] aParts = this.TagParentPartNumbers.Split(';');
                        oTags = new Word.Bookmark[aParts.Length];
                        for (int i = 0; i < aParts.Length; i++)
                        {
                            oTags[i] = m_oSegment.Nodes.GetBookmark(m_oSegment.FullTagID,
                                int.Parse(aParts[i]));
                        }
                    }
                }

                LMP.Benchmarks.Print(t0);

                return oTags;
            }
        }
		public VariableActions VariableActions
		{
			get{return m_oActions;}
		}		
		public ControlActions ControlActions
		{
            get{return m_oControlActions;}
		}
		public Segment Segment
		{
			get{return m_oSegment;}
		}
        public ForteDocument ForteDocument
        {
            get
            {
                return this.Segment != null ?
                    m_oSegment.ForteDocument : m_oForteDocument;
            }
        }
        public Variables Parent
        {
            get
            {
                ////return the segment variables collection
                ////if this variable is not standalone, else
                ////the document's variables collection
                //if (this.IsStandalone)
                //    return this.ForteDocument.Variables;
                //else
                    return this.Segment.Variables;
            }
        }
        public Segment AssociatedSegment
        {
            get 
            {
                ArrayList oSegArrayList = new ArrayList();

                //cycle through collection, finding all segments of the specified type
                for (int i = 0; i < this.Segment.Segments.Count; i++)
                {
                    Segment oSegment = this.Segment.Segments[i];
                    if (oSegment.ParentVariableID == this.ID)
                        return oSegment;
                }
                return null;
            }
        }
        /// <summary>
        /// gets the variable whose control directly sets/reflects the value of this variable
        /// </summary>
        public Variable UISourceVariable
        {
            get
            {
                if (m_oUISourceVariable == null)
                {
                    //detail variables may be a subfield of a different variable's control -
                    //if so, the name of this variable will be in the Expression parameter
                    //of the InsertDetail variable action
                    if (((this.ValueSourceExpression == "[DetailValue]") ||
                        (this.ValueSourceExpression == "[DetailValue]" + "__" + this.Name)) &&
                        (this.ControlType != mpControlTypes.DetailGrid) &&
                        (this.ControlType != mpControlTypes.DetailList))
                    {
                        //get parameters of InsertDetail action
                        string xParameters = "";
                        for (int i = 0; i < this.VariableActions.Count; i++)
                        {
                            VariableAction oAction = this.VariableActions[i];
                            if (oAction.Type == VariableActions.Types.InsertDetail)
                            {
                                xParameters = oAction.Parameters;
                                break;
                            }
                        }

                        //if parameters found, check Expression for the name of a variable
                        //with a detail control
                        if (xParameters != "")
                        {
                            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
                            if (aParams.GetUpperBound(0) > 6)
                            {
                                string xExpression = aParams[7];
                                int iPos = xExpression.IndexOf("[Variable_");
                                while (iPos > -1)
                                {
                                    iPos += 10;
                                    int iPos2 = xExpression.IndexOf("]", iPos);
                                    try
                                    {
                                        //JTS 12/19/08: Fieldcode could be [Variable__ or [Variable_,
                                        //so trim any starting '_' character
                                        m_oUISourceVariable = m_oSegment.Variables.ItemFromName(
                                            xExpression.Substring(iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray()));
                                    }
                                    catch { }

                                    if ((m_oUISourceVariable != null) &&
                                        ((m_oUISourceVariable.ControlType == mpControlTypes.DetailGrid) ||
                                        (m_oUISourceVariable.ControlType == mpControlTypes.DetailList)))
                                        break;
                                    else
                                    {
                                        m_oUISourceVariable = null;
                                        iPos = xExpression.IndexOf("[Variable_", iPos2);
                                    }
                                }
                            }
                        }
                    }
                    //GLOG - 3255 - CEH
                    //this might not be the nicest way to implement...
                    else if (this.DefaultValue != null)
                    {
                        //get variable from Default Value
                        string xName = this.DefaultValue;
                        //GLOG 6719:  Don't attempt to parse if there is more than one Variable code referenced.
                        if (String.CountChrs(xName, "Variable_") == 1)
                        {

                            int iPos = 0;
                            iPos = xName.IndexOf("Variable_");

                            if (iPos > 0)
                            {
                                int iPos2 = -1;
                                int iPos3 = -1;
                                //GLOG 6719: Get last opening bracket before 'Variable_' text
                                //get opening bracket position before 'Variable_' string
                                if (xName.IndexOf("[") > -1)
                                {
                                    iPos2 = xName.Substring(0, iPos).LastIndexOf("[");
                                }
                                if (iPos2 > -1)
                                {
                                    //GLOG 6719: Get first closing bracket after 'Variable_' text
                                    //get closing bracket position following 'Variable_' string
                                    iPos3 = xName.IndexOf("]", iPos);
                                    if (iPos3 > -1)
                                    {

                                        //get string between brackets
                                        xName = xName.Substring(iPos2 + 1, (iPos3 - iPos2) - 1);

                                        //remove 'Variable_' strings
                                        xName = xName.Replace("Variable__", "");
                                        xName = xName.Replace("Variable_", "");

                                        //return Variable object
                                        //GLOG 6719: Leave null if error occurs
                                        try
                                        {
                                            m_oUISourceVariable = Segment.GetVariable(xName);
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }

                    if (m_oUISourceVariable == null)
                        m_oUISourceVariable = this;
                }

                return m_oUISourceVariable;
            }
        }
        /// <summary>
        /// returns true iff the variable is a master detail variable -
        /// i.e. a variable that has a detail grid control, has a default
        /// value of [DetailValue], but has no insert detail action
        /// </summary>
        public bool IsMasterDetailVariable
        {
            get
            {
                bool bHasDetailGridControl = this.ControlType == mpControlTypes.DetailGrid;
                bool bHasDetailValueDefault = this.DefaultValue == "" || this.DefaultValue.ToUpper().Trim() == "[DETAILVALUE]";

                if (bHasDetailGridControl && bHasDetailValueDefault)
                {
                    //check actions for InsertDetail type action
                    int iCount = this.VariableActions.Count;
                    for (int i = 0; i < iCount; i++)
                    {
                        if (this.VariableActions[i].Type == VariableActions.Types.InsertDetail ||
                        this.VariableActions[i].Type == VariableActions.Types.InsertDetailIntoTable)
                        {
                            //variable has an insert detail action -
                            //not a master detail variable
                            return false;
                        }
                    }

                    //variable does not have an insert detail action -
                    //is a master detail variable
                    return true;
                }
                else
                {
                    //is not a master detail variable
                    return false;
                }
            }
        }
        /// <summary>
        /// returns true iff the variable is tagless
        /// </summary>
        public bool IsTagless
        {
            get
            {
                if (this.ForteDocument.FileFormat ==
                    LMP.Data.mpFileFormats.Binary)
                    return this.AssociatedWordTags.Length == 0;
                else
                    return this.AssociatedContentControls.Length == 0;
            }
        }
        public string Value
        {
            get
            {
                if (m_xValue == null)
                {
                    //5-11-11 (dm) - if segment is deactivated, get value from variable
                    if (this.Segment.Activated)
                        m_xValue = this.GetValueFromSource();
                    else
                    {
                        //6-1-11 (dm) - account for variable that wasn't in the collection
                        //at the time of deactivation
                        string xValue = this.DeactivationValue;
                        if (xValue != null)
                            m_xValue = xValue.Replace(ForteConstants.mpFixedValueMarker, "");
                        else
                            m_xValue = "";
                    }

                    SetValidationField();
                }

                return m_xValue;
            }
        }
        public bool HasValidValue
        {
            get
            {
                //test validation if necessary
                if (!this.m_bValidityTested)
                    SetValidationField();

                return m_bValueIsValid;
            }
        }
        /// <summary>
        /// returns true iff the value satisfies the validation condition
        /// </summary>
        /// <summary>
        /// returns true iff the variable is a standalone variable
        /// </summary>
        public bool IsStandalone
        {
            get { return this.Segment == null; }
        }
        /// <summary>
        /// True if user is required to visit this variable in the UI
        /// </summary>
        public bool MustVisit
        {
            get { return m_bMustVisit; }
            set
            {
                if (m_bMustVisit != value)
                {
                    m_bMustVisit = value;
                    this.m_bIsDirty = true;

                    //raise event if there are subscribers
                    if (!m_bMustVisit && this.VariableVisited != null)
                        this.VariableVisited(this, new VariableEventArgs(this));
                }
            }
        }
        /// <summary>
        /// Returns true if Variable is language-specific via Date format, Preference, Default Value or Variable Action
        /// </summary>
        public bool IsLanguageDependent
        {
            //GLOG 3528
            get
            {
                string xDef = this.ToString(true);
                //check variable actions for InsertDate action
                bool bDefinesInsertDateAction = false;
                for (int j = 0; j < this.VariableActions.Count; j++)
                {
                    if (this.VariableActions[j].Type == VariableActions.Types.InsertDate)
                        bDefinesInsertDateAction = true;
                }

                return (bDefinesInsertDateAction ||
                    (Expression.ContainsPreferenceCode(xDef) ||
                    (xDef.IndexOf("[SegmentLanguage") > -1)));
            }
        }
        public bool IsAuthorRelated
        {
            get
            {
                //get default value - exit if empty
                string xExp = this.DefaultValue;
                if (string.IsNullOrEmpty(xExp))
                {
                    return false;
                }
                else if (Expression.ContainsAuthorCode(xExp, -1, -2))
                {
                    return true;
                }
                else
                {
                    //check action parameters for related field code
                    for (int j = 0; j < this.VariableActions.Count; j++)
                    {
                        //get parameters
                        string xParam = ((VariableAction)this.VariableActions[j]).Parameters;
                        if (Expression.ContainsAuthorCode(xParam, -1, -2))
                        {
                            return true;
                        }
                        else if (this.VariableActions[j].Type == VariableActions.Types.IncludeExcludeText &&
                            Expression.ContainsAuthorCode(this.SecondaryDefaultValue, -1, -2))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public bool HasMultipleTags
        {
            get
            {
                if (m_oForteDocument.FileFormat ==
                    LMP.Data.mpFileFormats.Binary)
                    return (this.AssociatedWordTags.Length > 1);
                else
                    return (this.AssociatedContentControls.Length > 1);
            }
        }
        /// <summary>
        /// gets/sets value of variable in deactivation doc var for the segment
        /// </summary>
        public string DeactivationValue
        {
            get
            {
                //get deactivation doc var for segment
                string xVarValue = this.Segment.GetDeactivationData();

                //segment isn't currently deactivated
                if (string.IsNullOrEmpty(xVarValue))
                    return null;

                //decrypt
                xVarValue = LMP.String.Decrypt(xVarValue);

                //search for variable in doc var value
                xVarValue = StringArray.mpEndOfRecord + xVarValue + StringArray.mpEndOfRecord;
                int iPos = xVarValue.IndexOf(StringArray.mpEndOfRecord + this.Name + StringArray.mpEndOfElement);
                if (iPos != -1)
                {
                    //value exists
                    iPos = xVarValue.IndexOf(StringArray.mpEndOfElement, iPos) + 1;
                    int iPos2 = xVarValue.IndexOf(StringArray.mpEndOfRecord, iPos);
                    return xVarValue.Substring(iPos, iPos2 - iPos);
                }
                else
                    //no value for this variable
                    return null;
            }
            set
            {
                //get deactivation doc var for segment
                string xVarValue = this.Segment.GetDeactivationData();

                //set only if segment is currently deactivated
                if (xVarValue != null)
                {
                    if (xVarValue == " ")
                        //clear out empty string placeholder
                        xVarValue = "";
                    else
                        //decrypt
                        xVarValue = LMP.String.Decrypt(xVarValue);

                    //search for variable in doc var value
                    xVarValue = StringArray.mpEndOfRecord + xVarValue + StringArray.mpEndOfRecord;
                    int iPos = xVarValue.IndexOf(StringArray.mpEndOfRecord + this.Name + StringArray.mpEndOfElement);
                    if (iPos != -1)
                    {
                        //modify existing value
                        iPos = xVarValue.IndexOf(StringArray.mpEndOfElement, iPos) + 1;
                        int iPos2 = xVarValue.IndexOf(StringArray.mpEndOfRecord, iPos);
                        xVarValue = xVarValue.Substring(0, iPos) + value +
                            xVarValue.Substring(iPos2);
                    }
                    else
                    {
                        //add value
                        xVarValue.TrimEnd(StringArray.mpEndOfRecord);
                        xVarValue = xVarValue + StringArray.mpEndOfRecord + this.Name + StringArray.mpEndOfElement + value;
                    }

                    //trim leading/trailing field separators
                    xVarValue = xVarValue.Trim(StringArray.mpEndOfRecord);

                    //encrypt
                    LMP.String.Encrypt(xVarValue, LMP.Data.Application.EncryptionPassword);

                    //update doc var
                    object oName = "Deactivation_" + this.Segment.TagID;
                    this.ForteDocument.WordDocument.Variables.get_Item(ref oName).Value = xVarValue;
                }
            }
        }
        #endregion
		#region *********************methods*********************
		public string[] ToArray()
		{
			return ToArray(false);
		}

		/// <summary>
		/// returns the persistent values of the Variable
		/// as an array - the persistent values are those
		/// that are stored in the Word Tag. SegmentName,
		/// TagID, and Actions are added to the array at run time
		/// </summary>
		/// <param name="bPersistentValuesOnly"></param>
		/// <returns></returns>
		public string[] ToArray(bool bPersistentValuesOnly)
		{
			string[] aDef = null;
			int iOffset = 0;

			if(bPersistentValuesOnly)
			{
				aDef = new string[33];
			}
			else
			{
				aDef = new string[37];

				//populate the non-persistent indices
				aDef[0] = this.IsDirty.ToString();
				aDef[1] = this.SegmentName;
				aDef[2] = this.TagID;
				aDef[3] = this.TagParentPartNumbers;
				iOffset = 4;
			}

			aDef[iOffset] = this.ID;
			aDef[iOffset + 1] = this.Name;
			aDef[iOffset + 2] = this.DisplayName;
			aDef[iOffset + 3] = this.TranslationID.ToString();
			aDef[iOffset + 4] = this.ExecutionIndex.ToString();
			//there may be two values stored in DefaultValue field
			string xDefaultValue = this.DefaultValue;
			if (this.SecondaryDefaultValue != null)
				xDefaultValue = xDefaultValue + "�" + this.SecondaryDefaultValue;
			aDef[iOffset + 5] = xDefaultValue;
			aDef[iOffset + 6] = this.ValueSourceExpression;

            //GLOG 2355 - convert detail grid reserve value from xml to delimited
            //string to reduce size of ObjectData attribute in Word
            aDef[iOffset + 7] = this.DetailReserveValueToDelimitedString(this.ReserveValue);

			aDef[iOffset + 8] = this.Reserved;
			aDef[iOffset + 9] = ((byte) this.ControlType).ToString();
			aDef[iOffset + 10] = this.ControlProperties;
			aDef[iOffset + 11] = this.ValidationCondition;
			aDef[iOffset + 12] = this.ValidationResponse;
			aDef[iOffset + 13] = this.HelpText;
			aDef[iOffset + 14] = this.VariableActionsToString();
            aDef[iOffset + 15] = this.ControlActionsToString();
            aDef[iOffset + 16] = this.RequestCIForEntireSegment.ToString();
            aDef[iOffset + 17] = ((int) this.CIContactType).ToString();
            aDef[iOffset + 18] = this.CIDetailTokenString;
            aDef[iOffset + 19] = ((int) this.CIAlerts).ToString();
            aDef[iOffset + 20] = ((int)this.DisplayIn).ToString();
            aDef[iOffset + 21] = ((int)this.DisplayLevel).ToString();
            aDef[iOffset + 22] = this.DisplayValue;
            aDef[iOffset + 23] = this.Hotkey;
            aDef[iOffset + 24] = this.MustVisit.ToString();
            aDef[iOffset + 25] = ((int)this.AllowPrefillOverride).ToString();
            aDef[iOffset + 26] = this.IsMultiValue.ToString();
            aDef[iOffset + 27] = this.AssociatedPrefillNames;
            aDef[iOffset + 28] = this.TabNumber.ToString();
            aDef[iOffset + 29] = this.CIEntityName;
            //GLOG 2645
            aDef[iOffset + 30] = this.RuntimeControlValues;
            //GLOG 1408
            aDef[iOffset + 31] = this.NavigationTag;
            //10.2
            aDef[iOffset + 32] = this.ObjectDatabaseID.ToString();
            return aDef;
        }
        
		/// <summary>
		/// returns the variable as a delimited string -
		/// includes both persistent and non-persistent values
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
            return this.ToString(false);
		}
		/// <summary>
		/// returns the variable as a delimited string - 
		/// includes persistent values only if specified
		/// </summary>
		/// <param name="bPersistentValuesOnly"></param>
		/// <returns></returns>
		public string ToString(bool bPersistentValuesOnly)
		{
            //Need to replace pipes, since those will separate multiple
            //VariableDefinitions in the ObjectData
            return string.Join("�", this.ToArray(bPersistentValuesOnly)).Replace("|", 
                String.mpPipeReplacementChars);
		}

        /// <summary>
        /// sets the value of the variable to its default value
        /// </summary>
        public void AssignDefaultValue()
        {
            this.AssignDefaultValue(false);
        }
        public void SetValueFromContacts(ICContacts oContacts, bool bAppend)
        {
            if (this.ControlType == mpControlTypes.SalutationCombo)
            {
                LMP.Controls.SalutationCombo oSal;

                //create salutation control if necessary
                if (this.AssociatedControl == null)
                    this.CreateAssociatedControl(null);

                oSal = (LMP.Controls.SalutationCombo)this.AssociatedControl;

                //pass contacts to control
                oSal.SetContacts(oContacts, this.CIContactType, bAppend);

                //set variable value to the value of the control
                this.SetValue(this.AssociatedControl.Value);

                //GLOG 2645
                this.SetRuntimeControlValues(this.AssociatedControl.SupportingValues);
            }
            else
            {
                string xOldVal = this.Value;
                string xNewVal = "";
                int iItemCount = 0;

                int c = 0;

                for (int i = 1; i <= oContacts.Count(); i++)
                {
                    //cycle through contacts, adding to control 
                    //only those contacts of the specified type
                    Object oIndex = i;
                    ICContact oContact = oContacts.Item(oIndex);
                    string xCIFormat = Expression.Evaluate(
                        this.CIDetailTokenString, this.Segment, this.ForteDocument);
                    string[] xFields = null;
                    string xFieldList = "";
                    string xField = null;
                    int iPos = 0;
                    int iPos2 = 0;

                    iPos = xCIFormat.IndexOf("<");

                    // Get list of CI tokens to replace
                    while (iPos > -1)
                    {
                        iPos2 = xCIFormat.IndexOf(">", iPos + 1);
                        if (iPos2 > 0)
                        {
                            xField = xCIFormat.Substring(iPos + 1, iPos2 - (iPos + 1));

                            //add field to list if the field isn't conditioned on a business address type
                            //or it is conditioned as such and the address is a business is a business address
                            if (!xField.ToLower().EndsWith("ifbusiness") ||
                                oContact.AddressTypeName.ToLower() != "home")
                            {
                                //trim "ifbusiness" from field name
                                if(xField.ToLower().EndsWith("ifbusiness"))
                                    xField = xField.Substring(0, xField.Length - 10);

                                if (xFieldList != "")
                                    xFieldList = xFieldList + "|";

                                xFieldList = xFieldList + xField;

                                iPos = xCIFormat.IndexOf("<", iPos + 1);
                            }
                            else
                            {
                                //field needs to be removed, as it shouldn't be included
                                xCIFormat = xCIFormat.Substring(0, iPos) + xCIFormat.Substring(iPos2 + 1);
                            }
                        }
                        else
                        {
                            iPos = xCIFormat.IndexOf("<", iPos + 1);
                        }

                    }

                    xFields = xFieldList.Split('|');

                    //remove "ifbusiness" from CI format string
                    xCIFormat = xCIFormat.Replace("IFBUSINESS", "");

                    // build XML string, but replacing < and > with @[ and ]@
                    for (int iIndex = 0; iIndex < xFields.Length; iIndex++)
                        xCIFormat = xCIFormat.Replace("<" + xFields[iIndex] + ">",
                            "@[" + xFields[iIndex] + " Index=\"\" UNID=\"\"]@<" +
                            xFields[iIndex] + ">@[/" + xFields[iIndex] + "]@");

                    //GLOG item #6745 - dcf - added condition to set iItemCount only
                    //if it has not already been set
                    if (bAppend && xOldVal != "" && iItemCount == 0)
                    {
                        //GLOG : 7997 : ceh - find last instance of Index & get value
                        int iNewPos = xOldVal.LastIndexOf("Index=");
                        if (iNewPos != -1)
                        {
                            //get last index value
                            string xNewValue = xOldVal.Substring(iNewPos + 7);
                            iNewPos = xNewValue.IndexOf("\"");
                            try
                            {
                                iItemCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                            }
                            catch
                            {
                                //GLOG item #4200 - dcf
                                //xml string does not include Index="x"
                                //search for Index='x', which is the format
                                //returned by the document analyzer
                                iNewPos = xNewValue.IndexOf("'");
                                iItemCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                            }
                        }
                        c = iItemCount;
                    }

                    //GLOG 5359 - 1/5/11: Start with unchanged CI Format from Variable properties for each pass
                    string xAdjustedCIFormat = xCIFormat;
                    //There appears to be a bug where get_ContactType returns 0 instead of Other
                    //if Custom UI was loaded with a Contact prefilled, so in addition to 
                    //checking type, we need to back it up by checking the CustomTypeName
                    if (oContact.ContactType == this.CIContactType ||
                        (this.CIContactType == ciContactTypes.ciContactType_Other && 
                        oContact.CustomTypeName == this.DisplayName))
                    {
                        if (this.CIContactType == ciContactTypes.ciContactType_Other)
                        {
                            if (oContact.CustomTypeName != this.DisplayName)
                            {
                                //Custom type doesn't match current variable
                                continue;
                            }
                            else
                            {
                                //Custom UI only supports one entity per category
                                bAppend = false;
                                iItemCount = 0;
                                c = 0;
                            }
                        }
                        iItemCount++;
                        c++;
                        //GLOG 5359
                        xAdjustedCIFormat = xAdjustedCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                        //GLOG 4825: Prevent duplication of Company name if FullName and Company are the same
                        if (xAdjustedCIFormat.Contains("<FULLNAME") && oContact.FullName == oContact.Company)
                        {
                            xAdjustedCIFormat = xAdjustedCIFormat.Replace("<COMPANY>", "");
                            xAdjustedCIFormat = xAdjustedCIFormat.Replace("<TITLE>", "");
                        }
                        //GLOG : 8031 : ceh
                        //Fill in CoreAddress token first
                        string xNewDetail = xAdjustedCIFormat.Replace("<COREADDRESS>", 
                                                                      LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                        //fill in other details
                        xNewDetail = oContact.GetDetail(xNewDetail, true);

                        //replace any ampersands in the detail
                        xNewDetail = xNewDetail.Replace("&", "&amp;");
                        //replace temp markers for XML tags
                        xNewDetail = xNewDetail.Replace("@[", "<");
                        xNewDetail = xNewDetail.Replace("]@", ">");
                        // Fill in Index attribute
                        xNewDetail = xNewDetail.Replace("Index=\"\"", "Index=\"" + c.ToString() + "\"");
                        // Fill in UNID attribute
                        xNewDetail = xNewDetail.Replace("UNID=\"\"", "UNID=\"" + oContact.UNID + "\"");

                        //replace '\v' with '\r\n' - '\v' conflicts with XML
                        xNewDetail = xNewDetail.Replace("\v", "\r\n");
                        LMP.Trace.WriteNameValuePairs("xNewDetail", xNewDetail);

                        XmlDocument oXML = new XmlDocument();
                        oXML.LoadXml("<zzmpC>" + xNewDetail + "</zzmpC>");
                        string xAdjustedDetail = "";
                        foreach (XmlNode oNode in oXML.ChildNodes[0].ChildNodes)
                        {
                            if (oNode.InnerText != "")
                                xAdjustedDetail += oNode.OuterXml;
                        }
                        xNewVal += xAdjustedDetail;

                        //if(oContact != null)
                        //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContact);

                        oContact = null;
                    }
                    if (this.CIContactType == ciContactTypes.ciContactType_Other && c > 0)
                        //There can only be one matching entry, so stop looking
                        break;
                }

                if (bAppend)
                {
                    if (xNewVal != "")
                    {
                        // append new XML to existing value
                        this.SetValue(xOldVal + xNewVal);
                    }
                }
                else
                    this.SetValue(xNewVal);
            }
        }

        /// <summary>
        /// sets the value of the variable to its default value
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scopes will not be deleted when value is empty
        /// </param>
        public void AssignDefaultValue(bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;
            bool bFirstTry = true;
            string xOfficeDefault = null;
            string xFirmDefault = null;
            string xDefaultValue = "";
            string xPref = "";
            Segment oTarget = null;
            int iTry = 1;
            try
            {
                do
                {
                    if (iTry == 1)
                    {
                        //evaluate default value expression
                        xDefaultValue = Expression.Evaluate(
                            this.DefaultValue, this.m_oSegment, this.ForteDocument);
                    }
                    else
                    {
                        //GLOG 3689:  Action failed with Author Preference - use Office or Firm default instead
                        if (xOfficeDefault == null)
                        {
                            string xCode = "";
                            int iPrefStart = -1;
                            iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[AUTHORPARENTPREFERENCE_");
                            if (iPrefStart > -1)
                            {
                                int iPrefEnd = -1;
                                iPrefEnd = this.DefaultValue.IndexOf("]", iPrefStart);
                                if (iPrefEnd > -1)
                                {
                                    xCode = this.DefaultValue.Substring(iPrefStart, iPrefEnd + 1 - iPrefStart);
                                    xPref = xCode.Substring(xCode.LastIndexOf("_") + 1).TrimEnd(']');
                                    oTarget = this.Segment;
                                    if (xCode.ToUpper().Contains("AUTHORPARENTPREFERENCE"))
                                    {
                                        //Use top level parent as the keyset Scope
                                        while (oTarget.Parent != null)
                                            oTarget = oTarget.Parent;
                                    }
                                    int iOffice = 0;
                                    try
                                    {
                                        iOffice = oTarget.Authors.GetLeadAuthor().PersonObject.OfficeID;
                                    }
                                    catch { }
                                    //Check value in Office Preferences
                                    KeySet oKeys = new KeySet(mpPreferenceSetTypes.Author, oTarget.ID, iOffice.ToString());
                                    bool bDefaultPref = false;
                                    //Replace Preference code with Office Default for evaluation
                                    xOfficeDefault = this.DefaultValue.Replace(xCode, oKeys.GetValue(xPref, out bDefaultPref));
                                    //JTS 9/24/09: Manual Author won't have Office, so always use Firm Default
                                    if (!bDefaultPref && !oTarget.Authors.GetLeadAuthor().IsManualAuthor)
                                    {
                                        //There are separate Office and Firm  keysets -
                                        //Replace Preference code with Firm Default Value
                                        xFirmDefault = this.DefaultValue.Replace(xCode, oKeys.GetDefaultValue(xPref));
                                    }
                                    else
                                    {
                                        //There is no separate Office keyset, only Firm default
                                        xFirmDefault = xOfficeDefault;
                                        iTry++;
                                    }
                                }
                            }
                        }
                        string xTestDefault = null;
                        if (iTry == 2)
                            //Try separate Office default value
                            xTestDefault = Expression.Evaluate(xOfficeDefault, this.m_oSegment, this.ForteDocument);
                        if (iTry == 3 || xTestDefault == xDefaultValue)
                        {
                            //Office Preference non-existent or same as Author Preference - use Firm setting
                            xDefaultValue = Expression.Evaluate(xFirmDefault, this.m_oSegment, this.ForteDocument);
                        }
                        else
                        {
                            xDefaultValue = xTestDefault;
                        }
                    }
                    //if this is a user preference that has not yet been set,
                    //use secondary default value
                    if ((xDefaultValue == "null") && (Expression.ContainsPreferenceCode(
                        this.DefaultValue, Expression.PreferenceCategories.User)) &&
                        (this.SecondaryDefaultValue != ""))
                    {
                        xDefaultValue = Expression.Evaluate(this.SecondaryDefaultValue,
                            this.m_oSegment, this.ForteDocument);
                    }

                    //get current value
                    string xValue = this.Value;

                    if (xDefaultValue == "null" || xDefaultValue == "")
                    {
                        switch (this.ControlType)
                        {
                            case mpControlTypes.Checkbox:
                                xDefaultValue = "false";
                                break;
                            case mpControlTypes.Calendar:
                                xDefaultValue = DateTime.Today.ToString();
                                break;
                            case mpControlTypes.Spinner:
                                xDefaultValue = "0";
                                break;
                            default:
                                xDefaultValue = "";
                                break;
                        }
                    }

                    //assign result as value of variable
                    if (!LMP.String.CompareBooleanCaseInsensitive(xValue, xDefaultValue))
                    {
                        try
                        {
                            this.SetValue(xDefaultValue, true, bIgnoreDeleteScope);
                            if (iTry > 1)
                            {
                                //GLOG 3689: Original Author Preference was invalid.
                                //Action was completed using Office or Firm Preference instead
                                if (!this.Segment.InvalidPreferenceVariables.Contains(this.Name + "|") &&
                                    //JTS 9/24/09: Don't display message for Manual author
                                    !this.Segment.Authors.GetLeadAuthor().IsManualAuthor)
                                {
                                    string xMsg = string.Format(LMP.Resources.GetLangString("Msg_InvalidAuthorPreferenceValue"), xPref,
                                        (iTry < 3 ? "Office" : "Firm"), oTarget.DisplayName);
                                    MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //Flag that message has been displayed in Segment - action may run again if 
                                    //a different variable's action refreshes the Variables collection
                                    this.Segment.InvalidPreferenceVariables += this.Name + "|";
                                }
                                iTry = 4;
                            }
                        }
                        catch (LMP.Exceptions.ActionException oLE)
                        {
                            if (iTry == 3)
                                //Firm default also invalid
                                throw oLE;
                            else if (Expression.ContainsPreferenceCode(this.DefaultValue, Expression.PreferenceCategories.Author))
                                iTry++;
                        }
                        catch (System.Exception oE)
                        {
                            throw oE;
                        }
                    }
                    else
                    {
                        //GLOG 3689: End loop if value hasn't changed
                        iTry = 4;
                        //SetValidationField variable, as the SetValidationField function
                        //runs only when the value is set
                        this.SetValidationField();

                        //execute actions for include/exclude type variable if necessary -
                        //text may need updating even when default value matches current value;
                        //TODO: this issue theoretically applies to any variable with an action
                        //that has a field code in one of its parameters, so this code may need
                        //to be generalized at some point if we can find an efficient way to do so
                        if (((this.ValueSourceExpression == "[TagValue] ^!= ") ||
                            (this.ValueSourceExpression == "[TagValue] ^!=") ||
                            (this.ValueSourceExpression == "[TagValue] ^!= [Empty]") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!= ") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!=") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!= [Empty]")) &&
                            (xValue.ToLower() == "true") && (this.SecondaryDefaultValue != null))
                        {
                            //compare tag value to secondary default value
                            xDefaultValue = Expression.Evaluate(
                                this.SecondaryDefaultValue, this.m_oSegment, this.ForteDocument);
                            string xNodeText = xDefaultValue;
                            //10.2
                            if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
                            {
                                Word.XMLNode oWordNode = this.Parent.Nodes.GetWordTags(this.TagID)[0];
                                xNodeText = oWordNode.Text;
                                //GLOG 3318: Compare initial input case if All Caps is applied
                                if (!string.IsNullOrEmpty(xNodeText) && xNodeText.ToUpper() == xNodeText)
                                    //GLOG 8407: Only check revisions if Segment is fully created
                                    xNodeText = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                            }
                            else
                            {
                                Word.ContentControl oCC = this.Parent.Nodes.GetContentControls(this.TagID)[0];
                                xNodeText = oCC.Range.Text;
                                if (!string.IsNullOrEmpty(xNodeText) && xNodeText.ToUpper() == xNodeText)
                                    //GLOG 8407: Only check revisions if Segment is fully created
                                    xNodeText = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                            }

                            if (xNodeText != xDefaultValue)
                                this.VariableActions.Execute(bIgnoreDeleteScope);
                        }
                        else if (!bIgnoreDeleteScope)
                        {
                            //ensure that block gets deleted if necessary -
                            //this may not have been done in design because variable def
                            //contains field codes or actions dependent on runtime values -
                            //added 12/16/08 for Jones Day and Cleary fax configuration
                            //where cc table is in a block that gets excluded when there
                            //are no cc's (Doug)
                            for (int i = 0; i < this.VariableActions.Count; i++)
                            {
                                VariableAction oAction = this.VariableActions[i];
                                //GLOG - 3395 - CEH
                                if (oAction.Type == VariableActions.Types.IncludeExcludeBlocks)
                                {
                                    //get parameters
                                    string xInclude;
                                    string xExpression = "";
                                    string[] aParams = oAction.Parameters.Split(
                                        LMP.StringArray.mpEndOfSubField);
                                    string xBlockName = Expression.Evaluate(aParams[0],
                                        m_oSegment, m_oForteDocument);
                                    if (aParams.Length == 2)
                                        xExpression = aParams[1];
                                    if (xExpression != "")
                                        xInclude = Expression.Evaluate(xExpression,
                                            m_oSegment, m_oForteDocument);
                                    else
                                        xInclude = xValue;

                                    //execute only if exclusion is specified and
                                    //block is still in the document
                                    if (xInclude.ToLower() == "false")
                                    {
                                        Block oBlock = null;
                                        try
                                        {
                                            oBlock = m_oSegment.Blocks.ItemFromName(
                                                xBlockName);
                                        }
                                        catch { }
                                        if (oBlock != null)
                                        {
                                            this.VariableActions.Execute();
                                            break;
                                        }
                                    }
                                }
                                else if (oAction.Type == Api.VariableActions.Types.ApplyStyleSheet)
                                {
                                    //GLOG 8655: Always apply style sheet, as source segment may have changed
                                    this.VariableActions.Execute();
                                }
                            }
                        }
                    }
                }
                while (iTry > 1 && iTry < 4);
            }
            catch (System.Exception oE)
            {
                if (this.IsStandalone)
                    //not all standalone vars can have their
                    //default value set - e.g. when the default value contains
                    //a field code that requires a containing segment
                    MessageBox.Show(LMP.Resources.GetLangString(
                        "Msg_CouldNotSetValueOfStandaloneVar") + this.DisplayName,
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                else
                    throw new LMP.Exceptions.ValueException(
                        LMP.Resources.GetLangString(
                        "Error_CouldNotAssignDefaultValueToVariable") + this.Name + " (" + this.ID + ")", oE);
            }

            LMP.Benchmarks.Print(t0, this.Name);
        }
        ///// <summary>
        ///// sets the value of the variable to the specified value -
        ///// executes actions if specified and appropriate
        ///// </summary>
        ///// <param name="bValue"></param>
        ///// <param name="bExecuteActions"></param>
        //public void SetValue(string xValue, bool bExecuteActions,
        //    bool bIgnoreDeleteScope)
        //{
        //    //exit if segment is deactivated (5/10/11, dm)
        //    if (!this.Segment.Activated)
        //        return;

        //    //mark that value has been actively set
        //    m_bHasInitializedValue = true;

        //    //every variable has a starting value
        //    //when the content is inserted - there
        //    //is no such thing as content whose variables
        //    //have no value - hence, if the field (m_xValue)
        //    //hasn't been assigned, do so now - this is 
        //    //different than the default value, which will
        //    //be assigned as the value in another iteration
        //    //through this property set
        //    if (m_xValue == null)
        //        m_xValue = this.GetValueFromSource();

        //    if (xValue == null)
        //    {
        //        m_xValue = this.GetValueFromSource();
        //        return;
        //    }

        //    if (!LMP.String.CompareBooleanCaseInsensitive(xValue, m_xValue))
        //    {
        //        //value has changed - set value
        //        m_xValue = xValue;

        //        //save the value of the sticky field
        //        SavePreferenceIfNecessary();

        //        //set reserve value if necessary
        //        NodeStore oNS = this.Parent.Nodes;

        //        if (((this.TagType == ForteDocument.TagTypes.Segment) &&
        //            ((this.ValueSourceExpression == "") ||
        //            (this.ValueSourceExpression.Contains("[DetailValue"))) ||
        //            (this.ValueSourceExpression.Contains("[ReserveValue]"))))
        //        {
        //            this.ReserveValue = xValue;
        //            this.IsDirty = true;
        //            this.Parent.Save(this, true);
        //        }
        //        //set the private validation field to
        //        //mark whether or not the variable
        //        //now has a valid value
        //        SetValidationField();

        //        //test new
        //        if (bExecuteActions && !(this.Segment is IStaticCreationSegment))
        //        {
        //            if (!(this.Segment is IStaticRecipientCreationSegment) ||
        //                this.Name != "Recipients")
        //            {
        //                //execute actions
        //                this.VariableActions.Execute(bIgnoreDeleteScope);
        //            }
        //            else
        //            {
        //            }
        //        }

        //        //raise event if there are subscribers
        //        if (this.ValueAssigned != null)
        //            this.ValueAssigned(this, new EventArgs());
        //    }
        //}
        public void SetValue(string xValue, bool bExecuteActions, bool bIgnoreDeleteScope)
        {
            SetValue(xValue, bExecuteActions, bIgnoreDeleteScope, false);
        }
        public void SetValue(string xValue, bool bExecuteActions,
            bool bIgnoreDeleteScope, bool bSkipSource) //GLOG 8816
        {
            //exit if segment is deactivated (5/10/11, dm)
            if (!this.Segment.Activated)
                return;

            //mark that value has been actively set
            m_bHasInitializedValue = true;

            //every variable has a starting value
            //when the content is inserted - there
            //is no such thing as content whose variables
            //have no value - hence, if the field (m_xValue)
            //hasn't been assigned, do so now - this is 
            //different than the default value, which will
            //be assigned as the value in another iteration
            //through this property set
            if (!bSkipSource && m_xValue == null) //GLOG 8816
                m_xValue = this.GetValueFromSource();

            //Do check source if new value is null
            if (xValue == null)
            {
                m_xValue = this.GetValueFromSource();
                return;
            }

            //GLOG 8816
            if (m_xValue == null || !LMP.String.CompareBooleanCaseInsensitive(xValue, m_xValue))
            {
                //value has changed - set value
                m_xValue = xValue;

                //save the value of the sticky field
                SavePreferenceIfNecessary();

                //set reserve value if necessary
                NodeStore oNS = this.Parent.Nodes;

                if (((this.TagType == ForteDocument.TagTypes.Segment) &&
                    ((this.ValueSourceExpression == "") ||
                    (this.ValueSourceExpression.Contains("[DetailValue"))) ||
                    (this.ValueSourceExpression.Contains("[ReserveValue]"))))
                {
                    this.ReserveValue = xValue;
                    this.IsDirty = true;
                    this.Parent.Save(this, true);
                }
                //set the private validation field to
                //mark whether or not the variable
                //now has a valid value
                SetValidationField();

                if (bExecuteActions)
                {
                    //execute actions
                    this.VariableActions.Execute(bIgnoreDeleteScope);
                }

                //raise event if there are subscribers
                if (this.ValueAssigned != null)
                    this.ValueAssigned(this, new EventArgs());
            }
        }
        
        public void SetValue(string xValue)
		{
			this.SetValue(xValue, true);
		}
        public void SetValue(string xValue, bool bExecuteActions)
        {
            this.SetValue(xValue, bExecuteActions, false);
        }

        /// <summary>
        /// returns true iff this variable has a
        /// distributed segment variable action -
        /// either distributed sections or label table
        /// </summary>
        /// <returns></returns>
        public bool HasDistributedSegmentVariableActions()
        {
            bool bHasDistributedVariableAction = false;

            for (int i = 0; i < this.VariableActions.Count; i++)
            {
                VariableActions.Types iType = this.VariableActions[i].Type;

                if (iType == VariableActions.Types.SetupDistributedSections ||
                    iType == VariableActions.Types.SetupLabelTable)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// returns true iff this variable has a
        /// distributed segment variable action
        /// </summary>
        /// <returns></returns>
        public bool HasDetailTableAction()
        {
            bool bHasDistributedVariableAction = false;

            for (int i = 0; i < this.VariableActions.Count; i++)
            {
                if (this.VariableActions[i].Type ==
                    VariableActions.Types.InsertDetail)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// returns the value defined by the ValueSourceExpression for this variable
        /// </summary>
        /// <returns></returns>
        public string GetValueFromSource()
        {
            if (this.ForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.Binary)
                //xml tags
                return GetValueFromSource(null);
            else
                //content controls
                return GetValueFromSource_CC(null);
        }

        /// <summary>
        /// returns the value defined by the ValueSourceExpression for this variable
        /// </summary>
        /// <param name="oWordNode">determines which of the tags associated with this
        /// variable to use as the source where appropriate - if null or invalid,
        /// uses the first associated tag in the node store</param>
        /// <returns></returns>
        public string GetValueFromSource(Word.XMLNode oWordNode)
        {
            //GLOG 7921/7947 (dm) - this code was causing the value to be lost
            //on refresh and seems to no longer be necessary - it also relies
            //on the presence of variable actions that are no longer used
            //if (this.Segment is IStaticDistributedSegment && 
            //    this.HasDistributedSegmentVariableActions())
            //{
            //    return m_xValue == null ? "" : m_xValue;
            //}

            try
            {
                DateTime t0 = DateTime.Now;

                //validate specified tag
                if (oWordNode != null)
                {
                    if (oWordNode.BaseName == "mVar")
                    {
                        string xTagID = "";
                        Word.XMLNode oTagID = oWordNode.SelectSingleNode("@TagID", "", true);
                        if (oTagID != null)
                            xTagID = oTagID.NodeValue;
                        //GLOG 968 (dm) - added second condition below
                        if ((xTagID != this.Name)  && !this.IsDependentVariable(xTagID))
                            oWordNode = null;
                    }
                    else
                        oWordNode = null;
                }

                //get value source expression
                string xValue = "";
                if (m_oSegment != null)
                    xValue = this.ValueSourceExpression;

                if (xValue == "")
                {
                    //value source expression is empty, which is the default -
                    //get the nodestore for this collection of vars
                    NodeStore oNS;
                    oNS = this.Parent.Nodes;

                    //get the element name of this var
                    string xElementName = oNS.GetItemElementName(this.TagID);

                    if (xElementName == "mSEG")
                    {
                        //mSEG - return value held in variable definition
                        xValue = this.ReserveValue;
                    }
                    else if (xElementName == "mVar")
                    {
                        int iStartIndex = 0;
                        int iEndIndex = 0;
                        string xTempValue = "";
                        //For Multiple Value type, Value will be concatenation of all associated nodes
                        if (this.IsMultiValue)
                        {
                            iEndIndex = this.AssociatedWordTags.GetUpperBound(0);
                            oWordNode = null;
                        }

                        for (int iCount = iStartIndex; iCount <= iEndIndex; iCount++)
                        {
                            //return text from associated xml node
                            if (oWordNode == null)
                                oWordNode = this.AssociatedWordTags[iCount];

                            //check for sub vars
                            Word.XMLNode oSubVar = oWordNode.SelectSingleNode("child::x:mSubVar[1]",
                                "xmlns:x='urn-legalmacpac-data/10'", true);
                            if (oSubVar != null)
                            {
                                //Convert mSubVar Tags to standard XML tags

                                //TODO: check for text not in sub vars - in general, this indicates
                                //that the user entered text directly into the document
                                //that ended up outside of a sub var - only reline allows
                                //non-subvar text in a variable that contains subvars

                                xTempValue = LMP.Forte.MSWord.WordDoc.GetSubVarXML( oWordNode);
                            }
                            else
                            {
                                //GLOG 7394 (dm) - we need to take revisions into account here,
                                //as Range.Text might include deletions
                                xTempValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oWordNode.Range);
                                //xTempValue = oWordNode.Text;

                                //If value consists only of spaces, strip from result
                                if (LMP.String.ContainsOnlySpaces(xTempValue))
                                    xTempValue = "";

                                if (xTempValue == ((char)21).ToString())
                                {
                                    //source value is a field code - get
                                    //code content to get printed character
                                    string xCode = oWordNode.Range.Fields[1].Code.Text;

                                    if (xCode.EndsWith(((char)253).ToString()))
                                        xTempValue = "True";
                                    //GLOG 5802:  Checkbox inserted as Char 168 appears as Char 40 in string value
                                    else if (xCode.EndsWith(((char)168).ToString()) || xCode.EndsWith(((char)40).ToString()))
                                    {
                                        xTempValue = "False";
                                    }
                                }

                                //if text is null or empty, contains placeholder text, or is not upper case,
                                //bypass processing - xValue will be node.Text
                                if (!string.IsNullOrEmpty(xTempValue) &&
                                    xTempValue != this.DisplayName.ToUpper() &&
                                    xTempValue.ToUpper() == xTempValue)
                                {
                                    LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;

                                    //Make sure resetting Node won't trigger change event
                                    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                                    ////node text is upper case - 
                                    ////we need to get the input as it was typed - if
                                    ////the node text was formatted to all caps, oWordNode.Text
                                    ////returns the all cap version, even when the text was
                                    ////not input in all caps
                                    try
                                    {
                                        //GLOG 8407: Only check revisions if Segment is fully created
                                        xTempValue = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                                    }
                                    finally
                                    {
                                        ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
                                    }
                                }
                                //GLOG 3583: If Tag has had Underline to Longest applied, remove extra
                                //Tabs and Hard Spaces that have been added
                                if (oWordNode != null && xTempValue != "")
                                {
                                    Word.Range oTagRange = oWordNode.Range;
                                    if (oTagRange.Characters.First.Font.Underline == Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone &&
                                            oTagRange.Characters.Last.Font.Underline == Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle)
                                    {
                                        if (oTagRange.ParagraphFormat.Alignment == Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter)
                                        {
                                            //Remove Hard spaces from centered text
                                            xTempValue = xTempValue.Replace(((char)System.Convert.ToByte("160")).ToString(), "");
                                        }
                                        else
                                        {
                                            //Remove trailing Tabs from end of text
                                            xTempValue = xTempValue.TrimEnd('\t');
                                        }
                                    }
                                }
                                if (iCount < iEndIndex)
                                    //use delimiter between multiple non-xml values
                                    xTempValue = xTempValue + StringArray.mpEndOfValue;
                            }
                            xValue = xValue + xTempValue;
                            oWordNode = null;
                        }
                        if (this.IsMultiValue && xValue.Replace(StringArray.mpEndOfValue.ToString(), "") == "")
                            //If all tags are empty, return empty string
                            xValue = "";
                    }
                }
                else
                {
                    //get value from expression from code.
                    if ((oWordNode == null) && !this.IsTagless)
                        oWordNode = this.AssociatedWordTags[0];

                    //GLOG 4476 (dm) - consolidated [TagValue] and [MyTagValue] into a
                    //single block to avoid duplicate code
                    if (xValue.Contains("[TagValue]") || xValue.Contains("[MyTagValue]"))
                    {
                        //GLOG 7394 (dm) - we need to take revisions into account here,
                        //as Range.Text might include deletions
                        //GLOG 8407 - Avoid call to GetTextWithRevisions during segment creation
                        string xTagValue = "";
                        if (this.Segment.CreationStatus == Base.Segment.Status.Finished)
                        	xTagValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oWordNode.Range);
                        else
                            xTagValue = oWordNode.Text;

                        //GLOG 3318: Get text in case original input if All Caps is applied to tag
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue == xTagValue.ToUpper())
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, this.Segment.CreationStatus == Base.Segment.Status.Finished);

                        //GLOG 2151: Replace any reserved Expression characters before evaluating
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oWordNode.Range.Fields.Count > 0))
                            xTagValue = oWordNode.Range.Fields[1].Result.Text;

                        // TagValue is now deprecated.
                        if (xValue.Contains("[TagValue]"))
                        {
                            //replace [TagValue] code
                            xValue = xValue.Replace("[TagValue]", xTagValue);
                        }

                        //get value from expression the newly introduced MyTagValue 
                        if (xValue.Contains("[MyTagValue]"))
                        {
                            //replace [MyTagValue] code
                            DateTime oDateTime;
                            //GLOG 6594:  Test date using the underlying language for the range -
                            //Date formats for a particular language may not be valid for the System culture
                            System.Globalization.CultureInfo oCulture = null;
                            //GLOG 6634: Accessing the Range LanguageID in Word 2007 can result in Word freezing
                            //if next variable accessed is in header.  Instead, just use Segment Language
                            int iLCID = this.Segment.Culture;
                            if (LMP.Forte.MSWord.WordApp.Version > 12)
                                iLCID = (int)(oWordNode.Range.LanguageID);
                            try
                            {
                                oCulture = new System.Globalization.CultureInfo(iLCID);
                            }
                            catch
                            {
                                //If LanguageID not a valid Culture ID, use System default
                                oCulture = System.Globalization.CultureInfo.CurrentCulture;
                            }
                            if (DateTime.TryParse(xTagValue, oCulture, System.Globalization.DateTimeStyles.None, out oDateTime)) //GLOG 6594
                            {
                                //tag contains a date - check for date field
                                //GLOG 8407: Only check revisions if Segment is fully created
                                string xDateFormat = LMP.Forte.MSWord.WordDoc
                                    .GetDateFieldFormat(oWordNode.Range, this.Segment.CreationStatus == Base.Segment.Status.Finished);

                                if (xDateFormat != "")
                                {
                                    if (xValue == "[MyTagValue]")
                                    {
                                        //Only return Date Format if not part of a larger expression
                                        LMP.Benchmarks.Print(t0, this.Name);
                                        return xDateFormat;
                                    }
                                    else
                                        xTagValue = xDateFormat;
                                }
                            }

                            xValue = xValue.Replace("[MyTagValue]", xTagValue);
                        }
                    }

                    //GLOG 3491 (2/24/09, dm) - retrieve expanded value of this tag -
                    //this field code was previously always getting evaluated using
                    //the variable's first associated tag
                    if (xValue.ToLower().StartsWith("[tagexpandedvalue"))
                    {
                        //parameters are start and end delimiters - these will only
                        //be present when there's preceding or trailing text in design
                        string xStartDelimiter = "";
                        string xEndDelimiter = "";
                        string xCode = xValue.Substring(0, xValue.Length - 1);
                        string[] aParams = xCode.Split(new string[] { "__" },
                            StringSplitOptions.None);
                        if (aParams.Length > 1)
                            xStartDelimiter = aParams[1];
                        if (aParams.Length > 2)
                            xEndDelimiter = aParams[2];

                        //the XMLSelectionChange event may be triggered during the
                        //retrieval process - disable handling
                        LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreEvents =
                            ForteDocument.IgnoreWordXMLEvents;
                        ForteDocument.IgnoreWordXMLEvents =
                            LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                        try
                        {
                            //get value
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xValue = LMP.Forte.MSWord.WordDoc.GetExpandedTagValue(
                                oWordNode, xStartDelimiter, xEndDelimiter, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                        }
                        finally
                        {
                            ForteDocument.IgnoreWordXMLEvents = iIgnoreEvents;
                        }

                        if (xValue == null)
                            xValue = "";
                    }

                    //GLOG 968 (4/17/09, dm)
                    //for single value variables, this field code was previously always
                    //getting evaluated using the variable's first associated tag
                    if (xValue.Contains("[DetailValue"))
                    {
                        //get detail value
                        string xDetailValue = GetDetailValue(this, oWordNode, null, false);

                        //we support this field code with or w/o the variable name
                        xValue = xValue.Replace("[DetailValue]", xDetailValue);
                        xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                            xDetailValue);
                    }

                    //If value consists only of spaces, strip from result
                    if (LMP.String.ContainsOnlySpaces(xValue))
                        xValue = "";
                    if (oWordNode != null && xValue != "")
                    {
                        //GLOG 3583: If Tag has had Underline to Longest applied, remove extra
                        //Tabs and Hard Spaces that have been added
                        Word.Range oTagRange = oWordNode.Range;
                        if (oTagRange.Characters.First.Font.Underline == Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone &&
                                oTagRange.Characters.Last.Font.Underline == Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle)
                        {
                            if (oTagRange.ParagraphFormat.Alignment == Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter)
                            {
                                //Remove Hard spaces from centered text
                                xValue = xValue.Replace(((char)System.Convert.ToByte("160")).ToString(), "");
                            }
                            else
                            {
                                //Remove trailing Tabs from end of text
                                xValue = xValue.TrimEnd('\t');
                            }
                        }
                    }
                    //replace [ReserveValue] code
                    xValue = xValue.Replace("[ReserveValue]", this.ReserveValue);

                    //append variable name to codes where necessary
                    xValue = xValue.Replace("[RelineValue]", "[RelineValue" +
                        "__" + this.Name + "]");

                    //evaluate
                    string xSavedValue = xValue; //GLOG 6598
                    xValue = Expression.Evaluate(xValue, this.m_oSegment, this.ForteDocument);
                    //GLOG 6598:  There's no way to differentiate between "d" and "dd" token for 2-digit days
                    //Test replacing single 'd' with 'dd' in evaluated format, and use that if it matches
                    //one of the formats configured for the control.
                    if (xSavedValue.ToUpper().Contains("[DATEFORMAT_") && this.ControlType == mpControlTypes.DateCombo)
                    {
                        if (xValue.StartsWith("d M"))
                        {
                            string xTestFormat = "d" + xValue;
                            if (this.ControlProperties.Contains(xTestFormat)) //GLOG 7321 - Make sure 'dd MMMM' format is not assigned unless configured for the control
                                xValue = xTestFormat;
                        }
                        else if (xValue.StartsWith("MMMM d") && !xValue.StartsWith("MMMM dd"))
                        {
                            string xTestFormat = xValue.Replace("MMMM d", "MMMM dd");
                            if (this.ControlProperties.Contains(xTestFormat))
                                xValue = xTestFormat;
                        }
                    }
                }

                //GLOG 6041 (dm) - strip character representing a comment
                xValue = xValue.Replace('\u0005'.ToString(), "");
                //GLOG 6529 - strip character representing a footnote/endnote
                xValue = xValue.Replace('\u0002'.ToString(), "");

                LMP.Benchmarks.Print(t0, this.Name);
                return xValue == null ? "" : xValue;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ValueSourceException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotEvaluateValueSourceExpression") +
                    this.ValueSourceExpression, oE);
            }
        }

        /// <summary>
        /// returns the value defined by the ValueSourceExpression for this variable
        /// </summary>
        /// <param name="oCC">determines which of the content controls associated with this
        /// variable to use as the source where appropriate - if null or invalid,
        /// uses the first associated content control in the node store</param>
        /// <returns></returns>
        public string GetValueFromSource_CC(Word.ContentControl oCC)
        {
            //GLOG 7921/7947 (dm) - this code was causing the value to be lost
            //on refresh and seems to no longer be necessary - it also relies
            //on the presence of variable actions that are no longer used
            ////if this variable is part of a distributed segment,
            ////and it has a distributed variable action, don't
            ////update it's value from the document source - these
            ////variables are static (e.g. envelope and labels recipients)
            //if ((this.Segment is IStaticDistributedSegment) &&
            //    this.HasDistributedSegmentVariableActions())
            //{
            //    return m_xValue == null ? "" : m_xValue;
            //}

            try
            {
                DateTime t0 = DateTime.Now;
                Word.Range oCCRange = null;

                //validate specified tag
                if (oCC != null)
                {
                    if (LMP.String.GetBoundingObjectBaseName(oCC.Tag) == "mVar")
                    {
                        string xTagID = this.ForteDocument.GetAttributeValue(oCC, "TagID");
                        //GLOG 968 (dm) - added second condition below
                        if ((xTagID != this.Name) && !this.IsDependentVariable(xTagID))
                            oCC = null;
                    }
                    else
                        oCC = null;
                }

                //get value source expression
                string xValue = "";
                if (m_oSegment != null)
                {
                    xValue = this.ValueSourceExpression;
                    
                    //we have no need to support this programmatically generated
                    //field code on the content control side
                    if (xValue.ToLower().StartsWith("[tagexpandedvalue"))
                        xValue = "";
                }

                if (xValue == "")
                {
                    //value source expression is empty, which is the default -
                    //get the nodestore for this collection of vars
                    NodeStore oNS;
                    oNS = this.Parent.Nodes;

                    //get the element name of this var
                    string xElementName = oNS.GetItemElementName(this.TagID);

                    if (xElementName == "mSEG")
                    {
                        //mSEG - return value held in variable definition
                        xValue = this.ReserveValue;
                    }
                    else if (xElementName == "mVar")
                    {
                        int iStartIndex = 0;
                        int iEndIndex = 0;
                        string xTempValue = "";
                        //For Multiple Value type, Value will be concatenation of all associated nodes
                        if (this.IsMultiValue)
                        {
                            iEndIndex = this.AssociatedContentControls.GetUpperBound(0);
                            oCC = null;
                        }

                        for (int iCount = iStartIndex; iCount <= iEndIndex; iCount++)
                        {
                            //return text from associated content control
                            if (oCC == null)
                                oCC = this.AssociatedContentControls[iCount];

                            //10.2 (dm) - I think this code is obsolete - are sub vars
                            //ever used without a corresponding value source expression?
                            //check for sub vars
                            object oIndex = 1;
                            if (oCC.Range.ContentControls.Count > 0 && 
                                LMP.String.GetBoundingObjectBaseName(
                                oCC.Range.ContentControls.get_Item(ref oIndex).Tag) == "mSubVar")
                            {
                                //Convert mSubVar Tags to standard XML tags

                                //TODO: check for text not in sub vars - in general, this indicates
                                //that the user entered text directly into the document
                                //that ended up outside of a sub var - only reline allows
                                //non-subvar text in a variable that contains subvars

                                xTempValue = LMP.Forte.MSWord.WordDoc.GetSubVarXML_CC(oCC);
                            }
                            else
                            {
                                oCCRange = oCC.Range;

                                //GLOG 7394 (dm) - we need to take revisions into account here,
                                //as Range.Text might include deletions
                                xTempValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oCCRange);
                                //xTempValue = oCCRange.Text;

                                //If value consists only of spaces, strip from result
                                if (LMP.String.ContainsOnlySpaces(xTempValue))
                                    xTempValue = "";

                                if (xTempValue == ((char)21).ToString())
                                {
                                    //source value is a field code - get
                                    //code content to get printed character
                                    string xCode = oCCRange.Fields[1].Code.Text;

                                    if (xCode.EndsWith(((char)253).ToString()))
                                        xTempValue = "True";
                                    //GLOG 5802:  Checkbox inserted as Char 168 appears as Char 40 in string value
                                    else if (xCode.EndsWith(((char)168).ToString()) || xCode.EndsWith(((char)40).ToString()))
                                        xTempValue = "False";
                                }

                                //if text is null or empty, contains placeholder text, or is not upper case,
                                //bypass processing - xValue will be node.Text
                                if (!string.IsNullOrEmpty(xTempValue) &&
                                    xTempValue != this.DisplayName.ToUpper() &&
                                    xTempValue.ToUpper() == xTempValue)
                                {
                                    LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;

                                    //Make sure resetting Node won't trigger change event
                                    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

                                    ////node text is upper case - 
                                    ////we need to get the input as it was typed - if
                                    ////the node text was formatted to all caps, oWordNode.Text
                                    ////returns the all cap version, even when the text was
                                    ////not input in all caps
                                    try
                                    {
                                        //GLOG 8407: Only check revisions if Segment is fully created
                                        xTempValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCCRange, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                                    }
                                    finally
                                    {
                                        ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
                                    }
                                }
                                if (iCount < iEndIndex)
                                    //use delimiter between multiple non-xml values
                                    xTempValue = xTempValue + StringArray.mpEndOfValue;
                            }
                            xValue = xValue + xTempValue;
                            oCC = null;
                        }
                        if (this.IsMultiValue && xValue.Replace(StringArray.mpEndOfValue.ToString(), "") == "")
                            //If all tags are empty, return empty string
                            xValue = "";
                    }
                }
                else
                {
                    //get value from expression from code.
                    if ((oCC == null) && !this.IsTagless)
                        oCC = this.AssociatedContentControls[0];

                    //get content control range
                    if (oCC != null)
                        oCCRange = oCC.Range;

                    //GLOG 4476 (dm) - consolidated [TagValue] and [MyTagValue] into a
                    //single block to avoid duplicate code
                    if (xValue.Contains("[TagValue]") || xValue.Contains("[MyTagValue]"))
                    {
                        //GLOG 7394 (dm) - we need to take revisions into account here,
                        //as Range.Text might include deletions
                        //GLOG 8407 - Avoid call to GetTextWithRevisions during segment creation
                        string xTagValue = "";
                        if (this.Segment.CreationStatus == Base.Segment.Status.Finished)
                        	xTagValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oCCRange);
                        else
                            xTagValue = oCCRange.Text;

                        //GLOG 3318: Get text in case original input if All Caps is applied to tag
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue == xTagValue.ToUpper())
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCCRange, this.Segment.CreationStatus == Base.Segment.Status.Finished);

                        //GLOG 2151: Replace any reserved Expression characters before evaluating
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oCCRange.Fields.Count > 0))
                            xTagValue = oCCRange.Fields[1].Result.Text;

                        // TagValue is now deprecated.
                        if (xValue.Contains("[TagValue]"))
                        {
                            //replace [TagValue] code
                            xValue = xValue.Replace("[TagValue]", xTagValue);
                        }

                        //get value from expression the newly introduced MyTagValue 
                        if (xValue.Contains("[MyTagValue]"))
                        {
                            //replace [MyTagValue] code
                            DateTime oDateTime;
                            //GLOG 6594:  Test date using the underlying language for the range -
                            //Date formats for a particular language may not be valid for the System culture
                            System.Globalization.CultureInfo oCulture = null;
                            //GLOG 6634: Accessing the Range LanguageID in Word 2007 can result in Word freezing
                            //if next variable accessed is in header.  Instead, just use Segment Language
                            int iLCID = this.Segment.Culture;
                            if (LMP.Forte.MSWord.WordApp.Version > 12)
                                iLCID = (int)(oCCRange.LanguageID);

                            try
                            {
                                oCulture = new System.Globalization.CultureInfo(iLCID);
                            }
                            catch
                            {
                                //If LanguageID not a valid Culture ID, use System default
                                oCulture = System.Globalization.CultureInfo.CurrentCulture;
                            }
                            if (DateTime.TryParse(xTagValue, oCulture, System.Globalization.DateTimeStyles.None, out oDateTime)) //GLOG 6594
                            {
                                //tag contains a date - check for date field
                                //GLOG 8407: Only check revisions if Segment is fully created
                                string xDateFormat = LMP.Forte.MSWord.WordDoc
                                    .GetDateFieldFormat(oCCRange, this.Segment.CreationStatus == Base.Segment.Status.Finished);

                                if (xDateFormat != "")
                                {
                                    if (xValue == "[MyTagValue]")
                                    {
                                        //Only return Date Format if not part of a larger expression
                                        LMP.Benchmarks.Print(t0, this.Name);
                                        return xDateFormat;
                                    }
                                    else
                                        xTagValue = xDateFormat;
                                }
                            }

                            xValue = xValue.Replace("[MyTagValue]", xTagValue);
                        }
                    }

                    //GLOG 968 (4/17/09, dm)
                    //for single value variables, this field code was previously always
                    //getting evaluated using the variable's first associated tag
                    if (xValue.Contains("[DetailValue"))
                    {
                        //get detail value
                        string xDetailValue = GetDetailValue(this, null, oCC, false);

                        //we support this field code with or w/o the variable name
                        xValue = xValue.Replace("[DetailValue]", xDetailValue);
                        xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                            xDetailValue);
                    }

                    //If value consists only of spaces, strip from result
                    if (LMP.String.ContainsOnlySpaces(xValue))
                        xValue = "";

                    //replace [ReserveValue] code
                    xValue = xValue.Replace("[ReserveValue]", this.ReserveValue);

                    //append variable name to codes where necessary
                    xValue = xValue.Replace("[RelineValue]", "[RelineValue" +
                        "__" + this.Name + "]");

                    //evaluate
                    string xSavedValue = xValue; //GLOG 6598
                    xValue = Expression.Evaluate(xValue, this.m_oSegment, this.ForteDocument);
                    //GLOG 6598:  There's no way to differentiate between "d" and "dd" token for 2-digit days
                    //Test replacing single 'd' with 'dd' in evaluated format, and use that if it matches
                    //one of the formats configured for the control.
                    if (xSavedValue.ToUpper().Contains("[DATEFORMAT_") && this.ControlType == mpControlTypes.DateCombo)
                    {
                        if (xValue.StartsWith("d M"))
                        {
                            string xTestFormat = "d" + xValue;
                            if (this.ControlProperties.Contains(xTestFormat)) //GLOG 7321 - Make sure 'dd MMMM' format is not assigned unless configured for the control
                                xValue = xTestFormat;
                        }
                        else if (xValue.StartsWith("MMMM d") && !xValue.StartsWith("MMMM dd"))
                        {
                            string xTestFormat = xValue.Replace("MMMM d", "MMMM dd");
                            if (this.ControlProperties.Contains(xTestFormat))
                                xValue = xTestFormat;
                        }
                    }
                }

                //GLOG 6041 (dm) - strip character representing a comment
                xValue = xValue.Replace('\u0005'.ToString(), "");
                //GLOG 6529 - strip character representing a footnote/endnote
                xValue = xValue.Replace('\u0002'.ToString(), "");

                LMP.Benchmarks.Print(t0, this.Name);
                return xValue == null ? "" : xValue;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ValueSourceException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotEvaluateValueSourceExpression") +
                    this.ValueSourceExpression, oE);
            }
        }

        /// <summary>
        /// GLOG 2645: Set RuntimeControlValues and update ObjectData
        /// </summary>
        /// <param name="xValues"></param>
        internal void SetRuntimeControlValues(string xValues, bool bSaveDefinition)
        {
            SetStringPropertyValue(ref m_xRuntimeControlValues, xValues, 0);
            if (this.IsDirty && bSaveDefinition)
                this.Parent.Save(this);
        }
        public void SetRuntimeControlValues(string xValues)
        {
            SetRuntimeControlValues(xValues, true);
        }
        /// <summary>
        /// inserts mVar word tag at current selection
        /// </summary>
        /// <param name="range"></param>
        public Word.XMLNode InsertAssociatedTag()
        {
            try
            {
                //get tag for new mVar tag
                bool bIsNew = (this.TagID == "");
                string xTagID = this.Name;
                string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
                this.TagID = xFullTagID;
                this.TagType = ForteDocument.TagTypes.Variable;

                //get object data for new mVar tag
                string xObjectData;
                if (bIsNew)
                {
                    //get definition
                    xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
                }
                else
                {
                    //get from node store, so as not to include optional properties
                    //that aren't in existing tags
                    try
                    {
                        xObjectData = this.Parent.Nodes.GetItemObjectData(xFullTagID);
                    }
                    catch
                    {
                        //Definition wasn't in Node Store - might be reinserting
                        //as part of an InsertSegment action
                        xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
                    }
                }

                //replace xml chars and trim leading pipe
                xObjectData = LMP.String.ReplaceXMLChars(xObjectData).TrimStart('|');

                //ignore word event handler that execute after an mVar
                //has been inserted - this should only be a problem in
                //versions of Word > 11, where we subscribe to XMLAfterInsert -
                //however, we don't conditionalize, just to be safe
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                Word.XMLNode oNewNode = LMP.Forte.MSWord.WordDoc.InsertmVarAtSelection(xTagID,
                    xObjectData, this.DisplayName);
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordEvents;

                //add tag to collection
                m_oForteDocument.Tags.Insert(oNewNode, !bIsNew);

                //refresh node store
                m_oSegment.RefreshNodes();

                //update tag parent part numbers
                this.TagParentPartNumbers = this.Parent.Nodes.GetItemPartNumbers(this.TagID);

                return oNewNode;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInsertWordTag"), oE);
            }
        }
        /// <summary>
        /// inserts mVar content control at current selection
        /// </summary>
        /// <param name="range"></param>
        public Word.ContentControl InsertAssociatedContentControl()
        {
            try
            {
                //get tag for new mVar tag
                bool bIsNew = (this.TagID == "");
                string xTagID = this.Name;
                string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
                this.TagID = xFullTagID;
                this.TagType = ForteDocument.TagTypes.Variable;

                //get object data for new mVar tag
                string xObjectData;
                if (bIsNew)
                {
                    //get definition
                    xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
                }
                else
                {
                    //get from node store, so as not to include optional properties
                    //that aren't in existing tags
                    try
                    {
                        xObjectData = this.Parent.Nodes.GetItemObjectData(xFullTagID);
                    }
                    catch
                    {
                        //Definition wasn't in Node Store - might be reinserting
                        //as part of an InsertSegment action
                        xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
                    }
                }

                //replace xml chars and trim leading pipe
                xObjectData = LMP.String.ReplaceXMLChars(xObjectData).TrimStart('|');

                //ignore word event handler that execute after an mVar
                //has been inserted - this should only be a problem in
                //versions of Word > 11, where we subscribe to XMLAfterInsert -
                //however, we don't conditionalize, just to be safe
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                Word.ContentControl oCC = LMP.Forte.MSWord.WordDoc.InsertmVarAtSelection_CC(xTagID,
                    xObjectData, this.DisplayName);
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordEvents;

                //add tag to collection
                m_oForteDocument.Tags.Insert_CC(oCC, !bIsNew);

                //refresh node store
                m_oSegment.RefreshNodes();

                //update tag parent part numbers
                this.TagParentPartNumbers = this.Parent.Nodes.GetItemPartNumbers(this.TagID);

                return oCC;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInsertWordTag"), oE);
            }
        }
        /// <summary>
        /// creates the associated control for the Variable -
        /// returns the control (for convenience)
        /// </summary>
        /// <param name="oParentControl">the control that hosts this control</param>
        /// <returns></returns>
        public IControl CreateAssociatedControl(Control oParentControl)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                Control oCtl = null;
                //create new control
                if (this.IsMultiValue)
                {
                    switch (this.ControlType)
                    {
                        case mpControlTypes.Checkbox:
                        case mpControlTypes.Combo:
                        case mpControlTypes.DropdownList:
                        case mpControlTypes.MultilineCombo:
                        case mpControlTypes.MultilineTextbox:
                        case mpControlTypes.Textbox:
                            //Use MultiValueControl, configured for the selected control type
                            oCtl = new LMP.Controls.MultiValueControl(this.ControlType);
                            break;
                        default:
                            oCtl = LMP.Architect.Base.Application.CreateControl(this.ControlType, oParentControl);
                            break;
                    }
                }
                else
                {
                    oCtl = LMP.Architect.Base.Application.CreateControl(this.ControlType, oParentControl);
                }
                
                try
                {
                    IControl oIControl = (IControl)oCtl;

                    //set the control as the associated control
                    this.AssociatedControl = oIControl;

                    //set up control by setting control properties
                    SetAssociatedControlProperties();

                    //do any control-specific setup of control
                    oIControl.ExecuteFinalSetup();

                    return oIControl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                    LMP.Benchmarks.Print(t0);
                }

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    this.ControlType.ToString(), oE);
            }
        }
        /// <summary>
        /// sets the properties of the associated control
        /// </summary>
        public void SetAssociatedControlProperties()
        {
            DateTime t0 = DateTime.Now;
            
            Control oCtl = (Control)this.AssociatedControl;
            System.Type oCtlType = oCtl.GetType();

            if (oCtl is Chooser)
            {
                Chooser oChooser = (Chooser)oCtl;

                //set target object type id and target object id
                oChooser.TargetObjectID = this.Segment.ID1;
                oChooser.TargetObjectType = this.Segment.TypeID;
            }
            else if (oCtl is MultiValueControl)
            {
                if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //Configure control to display single or multiple values
                    ((MultiValueControl)oCtl).NumberOfValues = this.AssociatedWordTags.GetLength(0);
                }
                else
                {
                    //Configure control to display single or multiple values
                    ((MultiValueControl)oCtl).NumberOfValues = this.AssociatedContentControls.GetLength(0);
                }
            }

            //set up control properties-
            //get control properties from variable
            string[] aProps = null;

            if (this.ControlProperties != "")
                aProps = this.ControlProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, this.Segment, this.ForteDocument);

                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType,
                        LMP.Culture.USEnglishCulture);

                DateTime t1 = DateTime.Now;
                oPropInfo.SetValue(oCtl, oValue, null);
                LMP.Benchmarks.Print(t1, "SetPropertyValue", xName);
            }
            IControl oICtl = (IControl)oCtl;

            LMP.Benchmarks.Print(t0);
        }
        public bool SetTagState(bool bSetTagged, bool bDeleteVariableText, Word.Range oRng)
        {
            return SetTagState(bSetTagged, bDeleteVariableText, oRng, false);
        }
        /// <summary>
        /// Moves variable definition from mVar tag to mSeg
        /// </summary>
        /// <param name="bDeleteVariableText"></param>
        public bool SetTagState(bool bSetTagged, bool bDeleteVariableText, Word.Range oRng, bool bForceValid)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("bSetTagged", bSetTagged, "bDeleteVariableText",
                bDeleteVariableText);
            if (!bSetTagged && this.TagType == ForteDocument.TagTypes.None)
            {
                // Variable is already tagless, do nothing
                return false;
            }
            else if (bSetTagged && this.TagType == ForteDocument.TagTypes.Variable)
            {
                // Variable already has tag, no dothing
                return false;
            }
            else if (bSetTagged)
            {
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                try
                {
                    LMP.Forte.MSWord.TagInsertionValidityStates iValid = 0;

                    if (this.Segment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        iValid = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(
                            oRng, this.Segment.PrimaryWordTag, LMP.Forte.MSWord.TagTypes.Variable);
                    }
                    else
                    {
                        iValid = LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(
                            oRng, this.Segment.PrimaryContentControl, LMP.Forte.MSWord.TagTypes.Variable);
                    }

                    if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid && !bForceValid)
                    {
                        throw new LMP.Exceptions.XMLNodeException(LMP.Resources.GetLangString("Msg_InvalidTagInsertionLocation"));
                    }
                    this.TagType = ForteDocument.TagTypes.Variable;
                    //remove variable def from mSeg object data
                    string xObjectData = this.Segment.Nodes.GetItemObjectData(this.Segment.FullTagID);
                    int iPos1 = xObjectData.IndexOf("VariableDefinition=" + this.ID);
                    if (iPos1 > -1)
                    {
                        int iPos2 = xObjectData.IndexOf("|", iPos1 + 1) + 1;
                        string xNew = xObjectData.Substring(0, iPos1) + xObjectData.Substring(iPos2);
                        this.Segment.Nodes.SetItemObjectData(this.Segment.FullTagID, xNew);
                    }
                    
                    //Insert Tag only if Range has been specified
                    if (oRng != null)
                    {
                        oRng.Select();
                        if (this.Segment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            this.InsertAssociatedTag();
                        else
                            this.InsertAssociatedContentControl();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
                }
            }
            else if (!bSetTagged)
            {
                //delete mVar and add variable def to mSeg

                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                try
                {
                    if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        if (bDeleteVariableText && this.AssociatedWordTags[0].BaseName == "mVar")
                        {
                            object oMissing = System.Reflection.Missing.Value;
                            this.AssociatedWordTags[0].Range.Delete(ref oMissing, ref oMissing);
                        }
                    }
                    else
                    {
                        if (bDeleteVariableText && String.GetBoundingObjectBaseName(
                            this.AssociatedContentControls[0].Tag) == "mVar")
                        {
                            object oMissing = System.Reflection.Missing.Value;
                            this.AssociatedContentControls[0].Range.Delete(ref oMissing, ref oMissing);
                        }
                    }

                    string xVarObjectData = "VariableDefinition=" + this.ToString(true) + "|";
                    string xSegObjectData = null;

                    if (this.m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //JTS 6/13/13: Use PrimaryBookmark for Segment if available
                        if (this.Segment.PrimaryBookmark != null)
                        {
                            xSegObjectData = this.m_oForteDocument.GetAttributeValue(
                                 this.Segment.PrimaryBookmark, "ObjectData");
                        }
                        else
                        {
                            xSegObjectData = this.m_oForteDocument.GetAttributeValue(
                                 this.Segment.PrimaryWordTag, "ObjectData");
                        }

                        //Remove mVar tag
                        this.AssociatedWordTags[0].Delete();

                        //append Variable objectdata to existing PrimaryWordTag object data
                        if (this.Segment.PrimaryBookmark != null)
                        {
                            LMP.Forte.MSWord.WordDoc.SetAttributeValue_Bookmark(this.Segment.PrimaryBookmark,
                                "ObjectData", xSegObjectData + xVarObjectData,
                                LMP.Data.Application.EncryptionPassword);
                        }
                        else
                        {
                            LMP.Forte.MSWord.WordDoc.SetAttributeValue(this.Segment.PrimaryWordTag,
                                "ObjectData", xSegObjectData + xVarObjectData,
                                LMP.Data.Application.EncryptionPassword);
                        }
                    }
                    else
                    {
                        if (this.Segment.PrimaryBookmark != null)
                        {
                            xSegObjectData = this.m_oForteDocument.GetAttributeValue(
                                 this.Segment.PrimaryBookmark, "ObjectData");
                        }
                        else
                        {
                            xSegObjectData = this.m_oForteDocument.GetAttributeValue(
                                 this.Segment.PrimaryContentControl, "ObjectData");
                        }
                        //Remove mVar cc
                        this.AssociatedContentControls[0].Delete(false);

                        //append Variable objectdata to existing PrimaryWordTag object data
                        if (this.Segment.PrimaryBookmark != null)
                        {
                            LMP.Forte.MSWord.WordDoc.SetAttributeValue_Bookmark(this.Segment.PrimaryBookmark,
                                "ObjectData", xSegObjectData + xVarObjectData,
                                LMP.Data.Application.EncryptionPassword);
                        }
                        else
                        {
                            LMP.Forte.MSWord.WordDoc.SetAttributeValue_CC(this.Segment.PrimaryContentControl,
                                "ObjectData", xSegObjectData + xVarObjectData,
                                LMP.Data.Application.EncryptionPassword);
                        }
                    }

                    this.TagType = ForteDocument.TagTypes.None;

                    //update segment tag and refresh node store
                    this.Segment.ForteDocument.RefreshTags();
                    this.Segment.RefreshNodes();
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
                    LMP.Benchmarks.Print(t0);
                }

            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// sets the private validation field to true iff
        /// the validation condition is satisfied
        /// </summary>
        public void SetValidationField()
        {
            DateTime t0 = DateTime.Now;

            if (this.ValidationCondition == "")
                //there is no condition - value is always valid
                m_bValueIsValid = true;
            else
            {
                string xValue = m_xValue;
                if (xValue == null)
                    xValue = GetValueFromSource();

                //GLOG 2151: Mark reserved expression characters to be handled as literals in expression
                xValue = Expression.MarkLiteralReservedCharacters(xValue);
                //evaluate [MyValue] field code
                //string xCondition = this.ValidationCondition.Replace("[MyValue]", xValue);
                string xCondition = FieldCode.EvaluateMyValue(this.ValidationCondition, xValue);

                LMP.Trace.WriteNameValuePairs("xCondition", xCondition, "xValue", xValue);

                bool bIsValid = false;

                if (xValue != "zzmpTempValue" && xValue != this.DisplayName)
                {
                    //finish evaluating condition
                    string xIsValid = Expression.Evaluate(xCondition, this.Segment, this.ForteDocument);

                    try
                    {
                        //test to see if condition evaluates to a boolean
                        bIsValid = Boolean.Parse(xIsValid);
                    }
                    catch
                    {
                        //condition doesn't evaluate to a boolean - alert
                        throw new LMP.Exceptions.ExpressionException(
                            LMP.Resources.GetLangString("Error_InvalidValidationCondition") +
                            this.ValidationCondition);
                    }
                }

                m_bValueIsValid = bIsValid;
            }

            this.m_bValidityTested = true;
            LMP.Benchmarks.Print(t0, this.DisplayName);
        }
        /// <summary>
        /// set the variable value to the updated contact detail
        /// </summary>
        /// <param name="xContactDetailXML"></param>
        /// <returns></returns>
        public bool UpdateContactDetail()
        {
            bool bDetailUpdated = false;

            if (this.CIContactType == 0)
                //variable is not configured to 
                //have contact detail
                return false;

            //cycle through each item of contact detail-
            //if it has a UNID replace it with updated detail-
            //else leave as is
            string xUpdatedDetail = null;
            Match oMatch = null;
            int j = 1;
            ArrayList oUNIDs = new ArrayList();
            string xMatch = null;
            string xContactDetailXML = this.Value;

            try
            {
                do
                {
                    //get entity UNID
                    string xPattern = "Index=\"" + j.ToString() + "\" UNID=\".*?\"";
                    oMatch = Regex.Match(xContactDetailXML, xPattern);

                    if (oMatch == null || oMatch.Value == "")
                        //there are no more matches
                        break;

                    //parse UNID from match
                    xMatch = oMatch.Value;
                    int iPos = xMatch.IndexOf("UNID=");
                    xMatch = xMatch.Substring(iPos + 6, xMatch.Length - (iPos + 7));

                    if (xMatch != "0")
                    {
                        //at least one match
                        bDetailUpdated = true;

                        //entity has a UNID - get the contact
                        //associated with the UNID
                        string[] aUNIDS = { xMatch };

                        ICContacts oContacts = null;
                        try
                        {
                            oContacts = Contacts.GetContactsFromUNIDs(this, aUNIDS);
                            System.Windows.Forms.Application.DoEvents();
                        }
                        catch { }

                        //GLOG 15812
                        if (oContacts != null && oContacts.Count() == 1)
                        {
                            object oIndex = 1;
                            ICContact oContact = oContacts.Item(oIndex);

                            //contact was found - get matching nodes
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"[^<>]*\">[^<>]*</[^<>]*>";
                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);
                            string xItemXML = "";
                            foreach (Match oElement in oMatches)
                                xItemXML += oElement.Value;
                            XmlDocument oXML = new XmlDocument();
                            oXML.LoadXml("<zzmpD>" + xItemXML + "</zzmpD>");
                            XmlNodeList oNodeList = oXML.ChildNodes[0].ChildNodes;
                            //Value may contain CI and non-CI fields -
                            //Leave non-CI values unchanged
                            foreach (XmlNode oNode in oNodeList)
                            {
                                string xCIFormat = "<" + oNode.Name + ">";
                                string xVal = "";
                                xCIFormat = xCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                                //GLOG : 8031 : ceh
                                //fill in CoreAddress token
                                xVal = xCIFormat.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                                //fill in other details
                                xVal = oContact.GetDetail(xVal, true);
                                //Field is a CI Detail item, replace existing value
                                if (xVal != xCIFormat)
                                {
                                    oNode.InnerText = xVal;
                                }
                                xUpdatedDetail += oNode.OuterXml;
                            }
                        }
                        else
                        {
                            //contact was not found - use existing detail
                            //GLOG 15812
                            if (!this.Segment.MissingContactVariables.Contains(this.DisplayName))
                            {
                                this.Segment.MissingContactVariables.Add(this.DisplayName);
                            }
                            //GLOG : 15890 : ceh
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"[^<>]*\">[^<>]*</[^<>]*>";
                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                            foreach (Match oElement in oMatches)
                               xUpdatedDetail += oElement.Value;
                        }
                    }
                    else
                    {
                        //the entity does not have a UNID -
                        //use existing detail
                        xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                            "\" UNID=\"0\">[^<>]*</[^<>]*>";
                        MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                        foreach (Match oElement in oMatches)
                            xUpdatedDetail += oElement.Value; ;
                    }

                    j++;
                } while (oMatch != null && oMatch.Value != "");

                //GLOG 15812: Message now handled by Segment
                ////GLOG : 8395 : CEH - inform user about missing Contact
                //if (bContactNotFound)
                //{
                //    LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenUpdating = true;
                //    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_CouldNotFindContact_UseExistingDetails"), "variable", this.DisplayName),
                //                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                //                    MessageBoxIcon.Information);
                //}

                //GLOG 3197: Don't attempt to update variable whose value doesn't contain Contact XML
                if (xUpdatedDetail != null && !LMP.String.CompareBooleanCaseInsensitive(this.Value, xUpdatedDetail))
                    //updated detail is different - set variable value
                    this.SetValue(xUpdatedDetail, true);
            }
            catch
            {
                //some general problem occured - alert 
                //user that contacts will not be updated
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotUpdateVariableContactDetail") + this.DisplayName,
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }

            //variable contains at least one updatable contact detail
            return bDetailUpdated;

        }

        /// <summary>
        /// returns TRUE if this variable is the UI source variable for
        /// the variable with the specified name
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool IsDependentVariable(string xName)
        {
            Variable oVar = null;
            try
            {
                oVar = this.Segment.Variables.ItemFromName(xName);
            }
            catch { }

            if (oVar == null)
                return false;
            else
                return (oVar.UISourceVariable == this);
        }

        /// <summary>
        /// converts detail grid reserve value to xml if necessary - we now write it to the
        /// node store as a delimited string to reduce the size of the object data attribute
        /// in Word - see GLOG 2355
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        internal string DetailReserveValueToXML(string xValue)
        {
            //exit if it's a different control type or value is already xml
            if ((this.ControlType != mpControlTypes.DetailGrid) || xValue.Contains("</") ||
                System.String.IsNullOrEmpty(xValue))
                return xValue;

            //get field names
            string[] aFields = this.GetDetailGridFieldNames();

            //get entities
            string[] aEntities = xValue.Split(LMP.StringArray.mpEndOfSubField);

            //create xml document
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml("<zzmpD></zzmpD>");

            //cycle through entities
            for (int i = 0; i < aEntities.Length; i++)
            {
                string[] aItems = aEntities[i].Split(
                    LMP.StringArray.mpEndOfSubValue);

                for (int j = 2; j < aItems.Length; j++)
                {
                    //create node only if field has a value
                    if (aItems[j] != "")
                    {
                        //create node
                        XmlNode oFieldNode = oXML.CreateNode(XmlNodeType.Element,
                            aFields[j - 2], "");
                        oFieldNode.InnerText = aItems[j];

                        //create index attribute
                        XmlAttribute oA = oXML.CreateAttribute("Index");
                        oA.InnerText = aItems[0];
                        oFieldNode.Attributes.Append(oA);

                        //create unid attribute
                        oA = oXML.CreateAttribute("UNID");
                        oA.InnerText = aItems[1];
                        oFieldNode.Attributes.Append(oA);

                        //add node
                        oXML.DocumentElement.AppendChild(oFieldNode);
                    }
                }
            }

            string xItems = oXML.OuterXml;
            xItems = xItems.Replace("<zzmpD>", "");
            xItems = xItems.Replace("</zzmpD>", "");
            xItems = xItems.TrimEnd('\r', '\n');
            return xItems;
        }

        /// <summary>
        /// converts detail grid reserve value to a delimited string if necessary - this is
        /// done to reduce the size of the object data attribute in Word - see GLOG 2355
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        internal string DetailReserveValueToDelimitedString(string xValue)
        {
            //exit if it's a different control type or value is not xml
            if ((this.ControlType != mpControlTypes.DetailGrid) || !xValue.Contains("</"))
                return xValue;

            //get field names
            string[] aFields = this.GetDetailGridFieldNames();

            StringBuilder oSB = new StringBuilder();
            XmlDocument oXML = new System.Xml.XmlDocument();
            oXML.LoadXml(string.Concat("<zzmpD>", xValue, "</zzmpD>"));
            XmlNodeList oNodeList;
            int iIndex = 0;
            do
            {
                iIndex++;
                string xIndex = iIndex.ToString();
                oNodeList = oXML.DocumentElement.SelectNodes(
                    "(/zzmpD/*|/zzmpD/*)[@Index='" + xIndex + "']");
                if (oNodeList.Count > 0)
                {
                    //append entity separator
                    if (oSB.Length > 0)
                        oSB.Append(LMP.StringArray.mpEndOfSubField);

                    //get UNID, if one exists
                    string xUNID = "";
                    try
                    {
                        xUNID = oNodeList[0].Attributes.GetNamedItem("UNID").Value;
                    }
                    catch { }
                    if (xUNID == "")
                        xUNID = "0";

                    //append index and UNID
                    oSB.AppendFormat("{0}�{1}�", xIndex, xUNID);

                    //append the field values
                    for (int i = 0; i < aFields.Length; i++)
                    {
                        string xFieldValue = "";
                        for (int j = 0; j < oNodeList.Count; j++)
                        {
                            if (oNodeList[j].Name.ToUpper() == aFields[i].ToUpper())
                            {
                                xFieldValue = oNodeList[j].InnerText;
                                break;
                            }
                        }
                        oSB.AppendFormat("{0}�", xFieldValue);
                    }

                    oSB.Remove(oSB.Length - 1, 1);
                }
            } while (oNodeList.Count > 0);

            return oSB.ToString();
        }
        #endregion
		#region *********************private members*********************
        internal void SavePreferenceIfNecessary()
        {
            //GLOG 3627: Don't save Temp value as a preference
            if (this.Value == "zzmpTempValue")
                return;

            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("this.DefaultValue", this.DefaultValue);

            if (System.String.IsNullOrEmpty(this.DefaultValue))
                return;
            else if (!Expression.ContainsPreferenceCode(this.DefaultValue,
                Expression.PreferenceCategories.User))
                return;

            //check default value for user preference field code
            int iPos = this.DefaultValue.ToUpper().IndexOf("PREFERENCE_");

            //get field code name
            string xFieldCode = this.DefaultValue.Substring(1, iPos + 9).ToUpper();

            //get target segment - field code may reference a parent, child, or sibling
            Segment oTarget = this.Segment.GetReferenceTarget(ref xFieldCode);

            //get key
            string xKey = this.DefaultValue.Substring(iPos + 11).TrimStart('_');
            xKey = xKey.TrimEnd(']');

            switch (xFieldCode)
            {
                case "OBJECTPREFERENCE":
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserObjectPref,
                       oTarget.ID1.ToString(), LMP.Data.Application.User.ID, 0,
                       oTarget.Culture);
                    break;
                case "TYPEPREFERENCE":
                    //GLOG 5239 (dm) - removed reference to oTarget.Definition
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserTypePref,
                        ((int)oTarget.TypeID).ToString(),
                        LMP.Data.Application.User.ID, 0, oTarget.Culture);
                    break;
                case "APPLICATIONPREFERENCE":
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserAppPref,
                        "0", LMP.Data.Application.User.ID, 0, oTarget.Culture);
                    break;
            }

            LMP.Benchmarks.Print(t0);
        }


        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by xActionParameters
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xActionParameters"></param>
        /// <returns></returns>
        private static string GetVariableDetailValue(Variable oVar, string xActionParameters,
            Word.XMLNode oTargetTag, Word.ContentControl oTargetCC, bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            //get separators from action parameters
            Segment oSegment = oVar.Segment;
            ForteDocument oMPDocument = oSegment.ForteDocument;
            string[] aParams = xActionParameters.Split(LMP.StringArray.mpEndOfSubField);
            string xItemSeparator = Expression.Evaluate(aParams[0], oSegment, oMPDocument);
            string xEntitySeparator = Expression.Evaluate(aParams[1], oSegment, oMPDocument);
            //GLOG 4651: Get Alternate Format type to determine how entities inside table cells should be handled
            VariableAction.AlternateFormatTypes iAltFormat = VariableAction.AlternateFormatTypes.None;
            try
            {
                iAltFormat = (VariableAction.AlternateFormatTypes)Int32.Parse(aParams[3]);
            }
            catch { }

            //get fields
            string xFields = "";
            string xTargetFields = "|";
            string xControlProps = "";
            mpControlTypes oControlType = oVar.ControlType;

            if ((oControlType != mpControlTypes.DetailList) &&
                (oControlType != mpControlTypes.DetailGrid))
            {
                //get control variable
                Variable oUISourceVar = oVar.UISourceVariable;
                oControlType = oUISourceVar.ControlType;
                if ((oControlType == mpControlTypes.DetailList) ||
                    (oControlType == mpControlTypes.DetailGrid))
                    xControlProps = oUISourceVar.ControlProperties;

                //get target fields from template
                string xTemplate = Expression.Evaluate(aParams[2], oSegment, oMPDocument);
                int iPos = xTemplate.IndexOf("[SubVar_");
                while (iPos != -1)
                {
                    iPos = iPos + 8;
                    int iPos2 = xTemplate.IndexOf(']', iPos);
                    if (iPos2 == -1)
                    {
                        //this is only ever the case due to a bug
                        //in Expression.EvaluateOperation()
                        break;
                    }
                    //JTS 12/19/08: Fieldcode may use [SubVar__, so trim any starting '_' character
                    xTargetFields = xTargetFields + xTemplate.Substring(
                        iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray()) + "|";
                    iPos = xTemplate.IndexOf("[SubVar_", iPos2);
                }

                if ((xControlProps == "") || (xTargetFields == "|"))
                    return "";
            }
            else
                xControlProps = oVar.ControlProperties;

            if (oControlType == mpControlTypes.DetailList)
            {
                //list
                string xName = "Item";
                string xRows = "1";

                //get ci token if specified
                int iPos = xControlProps.IndexOf("CIToken=<");
                if (iPos != -1)
                {
                    iPos += 9;
                    int iPos2 = xControlProps.IndexOf(">", iPos);
                    xName = xControlProps.Substring(iPos, iPos2 - iPos);
                }

                //get rows per item if specified
                iPos = xControlProps.IndexOf("RowsPerItem=");
                if (iPos != -1)
                {
                    iPos += 12;
                    int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
                    if (iPos2 == -1)
                        iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
                    xRows = xControlProps.Substring(iPos, iPos2 - iPos);
                }

                xFields = xName + StringArray.mpEndOfValue + xRows;
            }
            else if (oControlType == mpControlTypes.DetailGrid)
            {
                //grid - parse config string to get field names and # of rows
                int iPos = xControlProps.IndexOf("ConfigString=") + 13;
                int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
                if (iPos2 == -1)
                    iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
                string xConfigString = xControlProps.Substring(iPos, iPos2 - iPos);
                string[] aConfig = xConfigString.Split(StringArray.mpEndOfSubField);
                byte bytFieldCount = (byte)(aConfig.Length / 5);
                int iOffset = 0;
                for (short sh = 1; sh <= bytFieldCount; sh++)
                {
                    string xField = aConfig[iOffset + 1];
                    if ((xTargetFields == "|") ||
                        (xTargetFields.ToUpper().IndexOf("|" + xField.ToUpper() + "|") > -1))
                    {
                        xFields = xFields + xField + StringArray.mpEndOfValue;
                        if (LMP.String.IsNumericInt32(aConfig[iOffset + 4]))
                            //get number of rows
                            xFields = xFields + aConfig[iOffset + 4] + StringArray.mpEndOfField;
                        else
                            //TODO: use list name to get number of rows - for now, assume 1
                            xFields = xFields + "1" + StringArray.mpEndOfField;
                    }
                    iOffset = iOffset + 5;
                }
                xFields = xFields.Substring(0, xFields.Length - 1);
            }
            else
                return "";

            //get value from associated tags -
            //for distributed detail, value will be concatenation of all associated nodes
            object oVarNodes = null;
            if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tags
                Word.XMLNode[] oWordNodes = null;
                if (oVar.IsMultiValue)
                    oWordNodes = oVar.AssociatedWordTags;
                else
                {
                    //GLOG 968 (dm) - use specified tag if provided; otherwise use first one
                    if (oTargetTag == null)
                        oTargetTag = oVar.AssociatedWordTags[0];
                    oWordNodes = new Word.XMLNode[1] { oTargetTag };
                }
                oVarNodes = oWordNodes;
            }
            else
            {
                //content controls
                Word.ContentControl[] oCCs = null;
                if (oVar.IsMultiValue)
                    oCCs = oVar.AssociatedContentControls;
                else
                {
                    //GLOG 968 (dm) - use specified tag if provided; otherwise use first one
                    if (oTargetCC == null)
                        oTargetCC = oVar.AssociatedContentControls[0];
                    oCCs = new Word.ContentControl[1] { oTargetCC };
                }
                oVarNodes = oCCs;
            }
            string xAmbiguousEntities = "";

            //GLOG 4561: Added parameter indicating whether to assume only 1 entity per mVar tag inside table cell
            //GLOG 8407: Only check revisions if Segment is fully created
            string xDetailValue = LMP.Forte.MSWord.WordDoc.GetDetailValue(ref oVarNodes, xFields,
                xEntitySeparator, xItemSeparator, ref xAmbiguousEntities,
                bReorderEntitiesByLocation, oVar.IsMultiValue || iAltFormat != VariableAction.AlternateFormatTypes.None,
                oVar.ForteDocument.WordDocument, oVar.Segment.CreationStatus == Base.Segment.Status.Finished);

            LMP.Benchmarks.Print(t0);

            return xDetailValue;
        }

        /// <summary>
        /// returns an xml string representing the value of oVar in the document, where
        /// oVar is a tagless varaible with a detail grid that populates other variables
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private static string GetMasterVariableDetailValue(Variable oVar,
            Word.XMLNode oTargetTag, Word.ContentControl oTargetCC,
            bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            string xValue = "";
            string xSortedValue = "";

            if (oVar.ControlType == mpControlTypes.DetailGrid)
            {
                int iPos = -1;
                string xReserveValue = oVar.ReserveValue;
                Segment oSegment = oVar.Segment;
                ForteDocument oMPDocument = oSegment.ForteDocument;

                //get names of "sub-variables" from this variable's RunVariableActions actions
                for (int i = 0; i < oVar.VariableActions.Count; i++)
                {
                    VariableAction oAction = oVar.VariableActions[i];
                    if (oAction.Type == VariableActions.Types.RunVariableActions)
                    {
                        //get sub-variable
                        string[] aParams = oAction.Parameters.Split(
                            LMP.StringArray.mpEndOfSubField);
                        string[] aVarNames = aParams[0].Split(',');
                        for (int v = 0; v < aVarNames.GetLength(0); v++)
                        {
                            //JTS 12/19/08: Trim any extra spaces from ends of string
                            string xVarName = aVarNames[v].Trim();
                            Variable oSubVar = null;
                            //Variable might not be found if it's currently in a hidden block
                            try
                            {
                                oSubVar = oSegment.Variables.ItemFromName(xVarName);
                            }
                            catch { }

                            if (oSubVar != null)
                            {
                                //if the Expression parameter of the InsertDetail action evaluates
                                //empty due to a condition other that the value of the master variable,
                                //the document will not reflect the true value of this sub-variable -
                                //extract the current value from the ReserveValue property
                                string xPreserve = "";
                                string xParameters = "";
                                for (int j = 0; j < oSubVar.VariableActions.Count; j++)
                                {
                                    VariableAction oSubAction = oSubVar.VariableActions[j];
                                    if (oSubAction.Type == VariableActions.Types.InsertDetail)
                                    {
                                        xParameters = oSubAction.Parameters;
                                        break;
                                    }
                                }

                                if (xParameters != "")
                                {
                                    //get Expression parameter
                                    aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
                                    if (aParams.GetUpperBound(0) > 6)
                                    {

                                        //JTS 12/19/08: Could be either [Variable__ or [Variable_
                                        //replace field code that returns value of master variable
                                        string xExpression = aParams[7].Replace("[Variable__" +
                                            oVar.Name + "]", "x");
                                        xExpression = xExpression.Replace("[Variable_" +
                                            oVar.Name + "]", "x");
                                        //evaluate
                                        xExpression = Expression.Evaluate(xExpression, oSegment,
                                            oMPDocument);
                                        if (xExpression == "")
                                        {
                                            //value won't be inserted in the doc - get impacted fields
                                            //from Template parameter
                                            string xFields = "";
                                            string xTemplate = Expression.Evaluate(aParams[2],
                                                oSegment, oMPDocument);
                                            iPos = xTemplate.IndexOf("[SubVar_");
                                            while (iPos > -1)
                                            {
                                                iPos += 8;
                                                int iPos2 = xTemplate.IndexOf("]", iPos);
                                                if (xFields != "")
                                                    xFields += "|";
                                                //JTS 12/19/08: Trim starting '_' to account for new [SubVar__xxx] format
                                                xFields += xTemplate.Substring(iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray());
                                                iPos = xTemplate.IndexOf("[SubVar_", iPos2);
                                            }

                                            //extract values of these fields from current ReserveValue
                                            if (xFields != "")
                                            {
                                                string[] aFields = xFields.Split('|');
                                                for (int j = 0; j < aFields.Length; j++)
                                                {
                                                    xPreserve += ExtractDetailFieldValue(xReserveValue,
                                                        aFields[j]);
                                                }
                                            }
                                        }
                                    }
                                }
                                //append
                                if (xPreserve != "")
                                    //use ReserveValue
                                    xValue += xPreserve;
                                else
                                {
                                    //get value from doc
                                    Word.XMLNode oTag = null;
                                    Word.ContentControl oCC = null;
                                    if (oMPDocument.FileFormat ==
                                        LMP.Data.mpFileFormats.Binary)
                                    {
                                        //xml tags
                                        if (oTargetTag != null)
                                        {
                                            //use tag only if it belongs to this subvariable
                                            string xTagID = oTargetTag.SelectSingleNode(
                                                "@TagID", "", true).NodeValue;
                                            if (xTagID == oSubVar.Name)
                                                oTag = oTargetTag;
                                        }
                                    }
                                    else
                                    {
                                        //content controls
                                        if (oTargetCC != null)
                                        {
                                            //use tag only if it belongs to this subvariable
                                            string xTagID = oMPDocument.GetAttributeValue(oTargetCC, "TagID");
                                            if (xTagID == oSubVar.Name)
                                                oCC = oTargetCC;
                                        }
                                    }

                                    xValue += GetVariableDetailValue(oSubVar, xParameters,
                                        oTag, oCC, bReorderEntitiesByLocation);
                                }
                            }
                        }
                    }
                }
                //If no SubVariables were found, but there's a reserve value, use that
                if (xValue == "" && xReserveValue != "")
                {
                    xValue = xReserveValue;
                    //If ReserveValue has been refreshed from Tag, reserved XML characters
                    //might already be restored - ensure & and ' are always replaced
                    xValue = String.RestoreXMLChars(xValue);
                    xValue = xValue.Replace("&", "&amp;");
                    xValue = xValue.Replace("'", "&apos;");
                }
                //reorder by index
                int iIndex = 1;
                bool bFound = true;
                while (bFound)
                {
                    iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"");
                    bFound = (iPos > -1);
                    while (iPos > -1)
                    {
                        iPos = xValue.LastIndexOf('<', iPos);
                        int iPos2 = xValue.IndexOf("</", iPos);
                        iPos2 = xValue.IndexOf(">", iPos2);
                        xSortedValue += xValue.Substring(iPos, (iPos2 - iPos) + 1);
                        iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"", iPos2);
                    }
                    iIndex++;
                }
            }

            LMP.Benchmarks.Print(t0);

            return xSortedValue;
        }

        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertDetail action(s) of it or, if tagless, of the other
        /// variables it populates
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        public static string GetDetailValue(Variable oVar)
        {
            return GetDetailValue(oVar, null, null, false);
        }

        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertDetail action(s) of it or, if tagless, of the other
        /// variables it populates
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        public static string GetDetailValue(Variable oVar, Word.XMLNode oTargetTag,
            Word.ContentControl oTargetCC, bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            //get InsertDetail parameters
            string xParameters = "";
            for (int i = 0; i < oVar.VariableActions.Count; i++)
            {
                VariableAction oAction = oVar.VariableActions[i];
                if (oAction.Type == VariableActions.Types.InsertDetail)
                {
                    xParameters = oAction.Parameters;
                    break;
                }
            }

            //if variable does not have InsertDetail action, it may
            //be a tagless "master" variable
            string xValue = "";
            if (xParameters != "")
                xValue = GetVariableDetailValue(oVar, xParameters, oTargetTag,
                    oTargetCC, bReorderEntitiesByLocation);
            else if (oVar.Segment is LMP.Architect.Base.IStaticDistributedSegment)
                //GLOG 7921 - for labels and envelopes, always use reserve value
                xValue = oVar.ReserveValue;
            else
                xValue = GetMasterVariableDetailValue(oVar, oTargetTag, oTargetCC,
                    bReorderEntitiesByLocation);

            //GLOG 5344: Make sure reserved characters in value aren't handled as fieldcodes
            xValue = Expression.MarkLiteralReservedCharacters(xValue);

            LMP.Benchmarks.Print(t0, oVar.Name + " value: " + xValue);

            return xValue;
        }

        /// <summary>
        /// returns the xField elements of xValue -
        /// faster than loading xml and using xpath
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xField"></param>
        /// <returns></returns>
        private static string ExtractDetailFieldValue(string xValue, string xField)
        {
            string xDetail = "";
            string xValueUpper = xValue.ToUpper();
            string xFieldUpper = xField.ToUpper();

            int iPos = xValueUpper.IndexOf("<" + xFieldUpper + " ");
            while (iPos > -1)
            {
                int iPos2 = xValue.IndexOf("</", iPos);
                iPos2 = xValue.IndexOf(">", iPos2);
                xDetail += xValue.Substring(iPos, (iPos2 - iPos) + 1);
                iPos = xValueUpper.IndexOf("<" + xFieldUpper + " ", iPos2);
            }

            return xDetail;
        }
        private string VariableActionsToString()
        {
            //get actions string
            string xActions = "";

            //cycle through actions, converting object to string
            //and adding it to the actions string
            for (int i = 0; i < this.VariableActions.Count; i++)
            {
                VariableAction oAction = this.VariableActions[i];

                //convert object to string
                StringBuilder oSB = new StringBuilder();
                oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}", oAction.ExecutionIndex,
                    oAction.LookupListID, oAction.ExecutionCondition,
                    (int)oAction.Type, oAction.Parameters);

                //add to actions string
                xActions += oSB.ToString();

                //add record separator if not last action
                if (i < this.VariableActions.Count - 1)
                    xActions += StringArray.mpEndOfRecord;
            }

            return xActions;
        }
        /// <summary>
        /// returns the collection of control actions
        /// assigned to this variable as a delimited string
        /// </summary>
        /// <returns></returns>
        private string ControlActionsToString()
        {
            //get actions string
            string xActions = "";

            //cycle through actions, converting object to string
            //and adding it to the actions string
            for (int i = 0; i < this.ControlActions.Count; i++)
            {
                ControlAction oAction = this.ControlActions[i];

                //convert object to string
                StringBuilder oSB = new StringBuilder();
                oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}", oAction.ExecutionIndex,
                    oAction.ExecutionCondition, (int)oAction.Event,
                    (int)oAction.Type, oAction.Parameters);

                //add to actions string
                xActions += oSB.ToString();

                //add record separator if not last action
                if (i < this.ControlActions.Count - 1)
                    xActions += StringArray.mpEndOfRecord;
            }

            return xActions;
        }
        #endregion
		#region *********************event handlers*********************
		private void m_oActions_ActionsDirtied(object sender, EventArgs e)
		{
			//variable actions have been edited - set variable dirty
			this.IsDirty = true;
		}

		private void m_oControlActions_ActionsDirtied(object sender, EventArgs e)
		{
			//control actions have been edited - set variable dirty
			this.IsDirty = true;
		}

        #endregion
	}

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac variables - created by Daniel Fisherman 09/20/05
	/// </summary>
	public class Variables : LMP.Architect.Base.Variables
	{
		#region *********************constants*********************
		#endregion
		#region *********************fields*********************
		private Hashtable m_oDefsByID;
		private Hashtable m_oDefsByName;
		private Hashtable m_oDefsByTagID;
		private ArrayList m_oDefsByIndex;
        //private ArrayList m_oCIEnabledDefs;
        private List <Variable> m_oCIEnabledDefs;
		private Segment m_oSegment;
        private ForteDocument m_oMPDocument;
		private bool m_bVarsPreviouslySetValidationField;

		public event LMP.Architect.Base.ValidationChangedHandler ValidationChanged;
        public event VariableVisitedHandler VariableVisited;
		#endregion
        #region *********************events*********************
        public event BeforeContactsUpdateHandler BeforeContactsUpdateEvent;
        #endregion
		#region *********************constructors*********************
        /// <summary>
        /// creates a variables collection belonging
        /// to the specified segment
        /// </summary>
        /// <param name="oSegmentNodes"></param>
        /// <param name="oParent"></param>
        internal Variables(Segment oParent)
		{
			m_oSegment = oParent;

			//populate internal storage from node store
            PopulateStorage(m_oSegment.Nodes);
		}
        /// <summary>
        /// creates a variables collection belonging
        /// to the specified MacPac Document
        /// </summary>
        /// <param name="oSegmentNodes"></param>
        internal Variables(ForteDocument oParent)
        {
            m_oMPDocument = oParent;

            //populate internal storage from node store
            PopulateStorage(oParent.OrphanNodes);
        }
		#endregion
		#region *********************properties*********************
		public Variable this[int iIndex]
		{
			get{return ItemFromIndex(iIndex);}
		}
		public Variable this[string xID]
		{
			get{return this.ItemFromID(xID);}
		}

		public int Count
		{
			get
			{
				return m_oDefsByID.Count;
			}
		}
		public Segment Segment
		{
			get{return m_oSegment;}
		}
        public ForteDocument ForteDocument
        {
            get
            {
                return m_oSegment != null ?
                    m_oSegment.ForteDocument : m_oMPDocument;
            }
        }
        /// <summary>
        /// returns true iff the values of all
        /// variables in the collectio are valid
        /// </summary>
		public bool ValuesAreValid
		{
			get
			{
				//return true if all vars are valid
				for(int i=0; i<this.Count; i++)
					if(!this[i].HasValidValue)
						return false;

				return true;
			}
		}
        /// <summary>
        /// Returns true of all "Must Visit" variables have been set
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                //return true if all vars are valid
                for (int i = 0; i < this.Count; i++)
                    if (this[i].MustVisit)
                        return false;

                return true;
            }
        }
		/// <summary>
		/// returns the segment nodes for this collection of variables
		/// </summary>
		internal NodeStore Nodes
		{
            get
            {
                if (this.Segment == null)
                    //this collection belongs to a MacPac Document-
                    //return the orphan nodes
                    return this.ForteDocument.OrphanNodes;
                else
                    //this collection belongs to a segment -
                    //return the segment's nodes
                    return this.Segment.Nodes;
            }
		}
        /// <summary>
        /// returns the collection variables 
        /// that are CI-enabled as an array
        /// </summary>
        public List<Variable> CIEnabledVariables
        {
            get { return m_oCIEnabledDefs; }
        }

        /// <summary>
        /// TRUE if the parent segment has a new variables collection
        /// </summary>
        public bool CollectionIsObsolete
        {
            get { return this != this.Segment.Variables; }
        }
        #endregion
		#region *********************methods*********************
		/// <summary>
		/// returns a new admin variable
		/// </summary>
		/// <returns></returns>
		public Variable CreateAdminVariable()
		{
			Variable oVar = new AdminVariable(this.Segment);
			oVar.SegmentName = this.Segment.FullTagID;

			//subscribe to notifications 
			oVar.ValueAssigned += new ValueAssignedEventHandler(OnVariableValueAssigned);
            oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            return oVar;
		}

		/// <summary>
		/// returns a new user variable
		/// </summary>
		/// <returns></returns>
		public Variable CreateUserVariable()
		{
			Variable oVar = new UserVariable(this.Segment);
			oVar.SegmentName = this.Segment.FullTagID;

			//subscribe to notifications 
			oVar.ValueAssigned +=new ValueAssignedEventHandler(OnVariableValueAssigned);
            oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            
            return oVar;
		}

		/// <summary>
		/// returns the variable at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public Variable ItemFromIndex(int iIndex)
		{
			//get the variable at the specified index
			if(iIndex > m_oDefsByIndex.Count - 1 || iIndex < 0)
			{
				//index is not in range - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemNotInCollection"));
			}

			//get variable ID at specified index
			Variable oVar = (Variable) m_oDefsByIndex[iIndex];

			return oVar;
		}

		/// <summary>
		/// returns the variable definition with the specified ID
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public Variable ItemFromID(string xID)
		{
			Variable oVar = null;

			Trace.WriteNameValuePairs("xID", xID);

			try
			{
				oVar = (Variable) m_oDefsByID[xID];
			}
			catch{}

			if(oVar == null)
			{
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString(
					"Error_ItemWithIDNotInCollection") + xID);
			}

			return oVar;
		}

		/// <summary>
		/// returns the variable definition with the specified ID
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public Variable ItemFromName(string xName)
		{
			Variable oVar = null;

			Trace.WriteNameValuePairs("xName", xName);

			try
			{
				oVar = (Variable) m_oDefsByName[xName];
			}
			catch{}

			if(oVar == null)
			{
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString(
					"Error_ItemWithIDNotInCollection") + xName);
			}

			return oVar;
		}

        /// <summary>
		/// returns the variable whose selection tag is the specified Word tag
		/// </summary>
		/// <param name="oWordTag"></param>
		/// <returns></returns>
		public Variable ItemFromAssociatedTag(Word.XMLNode oWordTag)
		{
			Variable oVar = null;
            //GLOG 8288: If Tag is mSubVar, get parent Variable Tag
            if (oWordTag.BaseName == "mSubVar")
            {
                //for mSubVars, we're actually interested in containing mVar -
                //keep checking parent node until an mVar is found
                Word.XMLNode oTargetTag = oWordTag.ParentNode;
                oWordTag = oTargetTag;
                while ((oTargetTag != null) &&
                    (oTargetTag.BaseName != "mVar"))
                {
                    oWordTag = oTargetTag;
                    oTargetTag = oTargetTag.ParentNode;
                }
            }
            if ((oWordTag.BaseName != "mVar") && (oWordTag.BaseName != "mDel"))
            {
                return null;
                ////mVar tags are required for this function
                //throw new LMP.Exceptions.WordTagException(
                //    LMP.Resources.GetLangString("Error_VariableTagRequired"));
            }

			//get tag id of specified Word tag
            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oWordTag);

			try
			{
				//get variable def from hashtable that maps 
				//selection tag names to variables
				oVar = (Variable) this.m_oDefsByTagID[xTagID];
			}
			catch{}

			if(oVar == null)
				return null;
			else
				return oVar;
		}

        /// <summary>
        /// returns the variable whose selection tag is the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        public Variable ItemFromAssociatedContentControl(Word.ContentControl oCC)
        {
            Variable oVar = null;

            string xTag = oCC.Tag;
            string xElement = LMP.String.GetBoundingObjectBaseName(xTag);
            //GLOG 8288: If CC is mSubVar, get parent Variable CC
            if (xElement == "mSubVar")
            {
                //for mSubVars, we're actually interested in containing mVar -
                //keep checking parent node until an mVar is found
                Word.ContentControl oTargetCC = oCC.ParentContentControl;
                oCC = oTargetCC;
                while ((oTargetCC != null) &&
                    (String.GetBoundingObjectBaseName(oTargetCC.Tag) != "mVar"))
                {
                    oCC = oTargetCC;
                    oTargetCC = oTargetCC.ParentContentControl;
                }
                xTag = oCC.Tag;
                xElement = LMP.String.GetBoundingObjectBaseName(xTag);
            }
            if ((xElement != "mVar") && (xElement != "mDel"))
            {
                return null;
                ////mVar tags are required for this function
                //throw new LMP.Exceptions.WordTagException(
                //    LMP.Resources.GetLangString("Error_VariableTagRequired"));
            }

            //get tag id of specified Word tag
            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oCC);

            try
            {
                //get variable def from hashtable that maps 
                //selection tag names to variables
                oVar = (Variable)this.m_oDefsByTagID[xTagID];
            }
            catch { }

            if (oVar == null)
                return null;
            else
                return oVar;
        }

        /// <summary>
        /// saves the specified variable to the Word XML Tag
        /// </summary>
        /// <param name="oVar"></param>
        public void Save(Variable oVar)
        {
            this.Save(oVar, false);
        }

		/// <summary>
		/// saves the specified variable to the Word XML Tag
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bOnlyReserveValueChanged"></param>
		public void Save(Variable oVar, bool bOnlyReserveValueChanged)
		{
			DateTime t0 = DateTime.Now;

			try
			{
				Trace.WriteNameValuePairs("oVar.ID", oVar.ID);

				//save only if variable is dirty
				if(!oVar.IsDirty)
					return;

				if(!oVar.HasValidDefinition)
					throw new LMP.Exceptions.DataException(
						LMP.Resources.GetLangString(
						"Error_InvalidVariableDefinition") + string.Join("|", oVar.ToArray()));

                if (m_oDefsByID[oVar.ID] != null)
                {
                    //variable is already in collection
                    string xTagID = oVar.TagID;
				    if(!this.Nodes.NodeExists(xTagID))
				    {
					    //can't save to specified tag - alert
					    throw new LMP.Exceptions.WordXmlNodeException(
						    LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + xTagID);
				    }

				    //get new definition 
				    string xVarForTag = oVar.ToString(true);

                    //update the variable definitions in the node store and document
                    if ((oVar.TagType == ForteDocument.TagTypes.DeletedBlock) ||
                        (oVar.TagType == ForteDocument.TagTypes.Mixed))
                    {
                        //variable def is in deleted scopes
                        this.Nodes.UpdateVariableDefInDeletedScopes(oVar);
                    }

                    if (oVar.TagType != ForteDocument.TagTypes.DeletedBlock)
                    {
                        //search for existing variable def in tag's object data
                        string xObjectData = this.Nodes.GetItemObjectData(xTagID);
                        string xNewObjectData = "";
                        const byte mpLenVarDef = 19;

                        int iPos1 = xObjectData.IndexOf("VariableDefinition=" + oVar.ID);
                        if (iPos1 > -1)
                        {
                            //variable definition already exists - replace with updated definition
                            int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                            xNewObjectData = xObjectData.Substring(0, iPos1 + mpLenVarDef) +
                                xVarForTag + xObjectData.Substring(iPos2);
                        }
                        else
                        {
                            //raise error
                            throw new LMP.Exceptions.WordXmlNodeException(
                                LMP.Resources.GetLangString("Error_ObjectDataValueNotFound") +
                                "VariableDefinition=" + oVar.ID);
                        }

                        //set object data for specified Node
                        this.Nodes.SetItemObjectData(xTagID, xNewObjectData);
                    }

                    //raise event if the definition has 
                    //changed - definition has not changed if
                    //reserve value is the only thing that's changed
                    if(!bOnlyReserveValueChanged)
                        this.ForteDocument.RaiseVariableDefinitionChangedEvent(oVar);
                }
                else
                {
                    //variable is not yet in the collection - this method assumes that
                    //that the definition for the new variable is already in the
                    //appropriate Word XMLNode(s) and that the ForteDocument.Tags collection
                    //has been updated to reflect any newly added XMLNodes (if TagType = Variable)
                    //or definitions (if TagType = Segment) - it also assumes that all of the
                    //properties of the new variable have been set - all of this is handled by
                    //the front-end

                    //refresh node store
                    if (!oVar.IsStandalone)
                        m_oSegment.RefreshNodes();
                    else
                        m_oMPDocument.RefreshOrphanNodes();

                    //add variable to internal arrays
                    AddToInternalArrays(oVar);
                    CompressIndexArray();

                    //raise event
                    this.ForteDocument.RaiseVariableAddedEvent(oVar);
                }

				//remove dirt
				oVar.IsDirty = false;
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.VariableException(
					LMP.Resources.GetLangString("Error_CantSaveVariable") + oVar.ID, oE);
			}
			finally
			{
				LMP.Benchmarks.Print(t0);
			}
		}

        /// <summary>
        /// returns true if the variable with the specified name is in the collection
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool VariableExists(string xName)
        {
			Variable oVar = null;

			Trace.WriteNameValuePairs("xName", xName);

			try
			{
				oVar = (Variable) m_oDefsByName[xName];
			}
			catch{}

            return (oVar != null);
        }

	    /// <summary>
	    /// deletes the variable at the specified collection index and
        /// its associated tags if specified
	    /// </summary>
	    /// <param name="iIndex"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(int iIndex, bool bDeleteAssociatedTags)
	    {
		    Variable oVar = null;

		    try
		    {
			    oVar = this.ItemFromIndex(iIndex);
		    }
		    catch(System.Exception oE)
		    {
			    throw new LMP.Exceptions.NotInCollectionException(
				    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex, oE);
		    }

		    Delete(oVar, bDeleteAssociatedTags, false);
	    }

		/// <summary>
		/// deletes the variable with the specified name and
        /// its associated tags if specified
		/// </summary>
		/// <param name="xName"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(string xName, bool bDeleteAssociatedTags)
		{
			Variable oVar = null;

			try
			{
				oVar = (Variable) m_oDefsByName[xName];
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xName, oE);
			}

			Delete(oVar, bDeleteAssociatedTags, false);
		}

		/// <summary>
        /// deletes the specified variable and its associated tags if specified
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(Variable oVar, bool bDeleteAssociatedTags, bool bDeleteVariableText)
        {
            Delete(oVar, bDeleteAssociatedTags, bDeleteVariableText, true);
        }

		/// <summary>
        /// deletes the specified variable and its associated tags if specified
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(Variable oVar, bool bDeleteAssociatedTags, bool bDeleteVariableText,
            bool bRemovedDeletedScopeXML)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("oVar.ID", oVar.ID, "bDeleteAssociatedTags",
                bDeleteAssociatedTags);

            //JTS 4/30/10: If variable Tag was removed as part of the DeleteScope of a different
            //variable or block, it won't exist in Nodestore, but we still need to delete
            //the object from the Variables collection
            if (!this.Nodes.NodeExists(oVar.TagID) && (bDeleteAssociatedTags || bDeleteVariableText))
            {
                //can't find specified tag - alert
                throw new LMP.Exceptions.WordXmlNodeException(
                    LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oVar.TagID);
            }

            //remove definition from mSEG attributes
            if (oVar.TagType != ForteDocument.TagTypes.Variable)
            {
                if (oVar.TagType == ForteDocument.TagTypes.Segment)
                {
                    //variable def in in object data
                    string xObjectData = this.Nodes.GetItemObjectData(oVar.TagID);
                    int iPos1 = xObjectData.IndexOf("VariableDefinition=" + oVar.ID);
                    if (iPos1 > -1)
                    {
                        int iPos2 = xObjectData.IndexOf("|", iPos1 + 1) + 1;
                        string xNew = xObjectData.Substring(0, iPos1) + xObjectData.Substring(iPos2);
                        this.Nodes.SetItemObjectData(oVar.TagID, xNew);
                    }
                }
                else if (bRemovedDeletedScopeXML)
                {
                    //variable def in in deleted scopes
                    this.Nodes.RemoveVariableDefFromDeletedScopes(oVar);
                }
            }
            //delete associated tags
            if (bDeleteAssociatedTags)
            {
                ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                //JTS 5/10/10: 10.2
                if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    Word.ContentControl[] oAssociatedCCs = oVar.AssociatedContentControls;
                    if (oAssociatedCCs != null)
                    {
                        for (int i = oAssociatedCCs.Length - 1; i >= 0; i--)
                        {
                            Word.ContentControl oCC = oAssociatedCCs[i];

                            //remove tag from Tags collection
                            this.ForteDocument.Tags.Delete_CC(oCC);

                            //GLOG 6326 (dm) - delete associated doc var and bookmark
                            LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
                                this.ForteDocument.WordDocument, false);

                            //delete actual Word XMLNode, plus text if specified
                            oCC.Delete(bDeleteVariableText);
                        }
                    }
                }
                else
                {
                    Word.XMLNode[] oAssociatedTags = oVar.AssociatedWordTags;
                    if (oAssociatedTags != null)
                    {
                        for (int i = oAssociatedTags.Length - 1; i >= 0; i--)
                        {
                            Word.XMLNode oTag = oAssociatedTags[i];

                            //delete tag range content if specified.
                            if (bDeleteVariableText)
                            {
                                object oMissing = System.Reflection.Missing.Value;
                                oTag.Range.Delete(ref oMissing, ref oMissing);
                            }
                            //remove tag from Tags collection
                            this.ForteDocument.Tags.Delete(oTag);

                            //GLOG 6326 (dm) - delete associated doc var and bookmark
                            Word.XMLNode oReserved = oTag.SelectSingleNode("@Reserved", "", true);
                            if (oReserved != null)
                            {
                                LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
                                    this.ForteDocument.WordDocument, false);
                            }

                            //delete actual Word XMLNode
                            oTag.Delete();
                        }
                    }
                }
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
  
            //delete deactivation value (5/19/11 dm)
            if (!m_oSegment.Activated)
            {
                //get the deactivation document variable that
                //contains the data for this reviewed variable
                object oName = "Deactivation_" + m_oSegment.TagID;
                Word.Variable oDocVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);

                //get the value of the document variable
                string xValue = StringArray.mpEndOfRecord +
                    LMP.String.Decrypt(oDocVar.Value) + StringArray.mpEndOfRecord;

                //search for the deactivated name/value pair
                int iPos = xValue.IndexOf(StringArray.mpEndOfRecord +
                    oVar.Name + StringArray.mpEndOfElement);

                if (iPos > -1)
                {
                    //pair was found - find the end of the name/value pair
                    int iPos2 = xValue.IndexOf(StringArray.mpEndOfRecord, iPos + 1);

                    //remove the name value pair
                    xValue = xValue.Substring(0, iPos) + xValue.Substring(iPos2);
                    xValue = xValue.Trim(StringArray.mpEndOfRecord);

                    //set document variable to the new value
                    oDocVar.Value = LMP.String.Encrypt(xValue,
                        LMP.Data.Application.EncryptionPassword);
                }
            }

            //delete the var from the internal arrays
            DeleteFromInternalArrays(oVar);

            //refresh node store
            if (m_oSegment != null)
                m_oSegment.RefreshNodes();
            else
                m_oMPDocument.RefreshOrphanNodes();

            //raise event
            this.ForteDocument.RaiseVariableDeletedEvent(oVar);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// deletes the specified variable and its associated tags
        /// </summary>
        /// <param name="oVar"></param>
        public void Delete(Variable oVar)
        {
            Delete(oVar, true, false);
        }

        /// <summary>
        /// deletes the specified variable, its associated tags
        /// and if specified, the text range contained within the 
        /// deleted tag
        /// </summary>
        /// <param name="oVar"></param>
        public void Delete(Variable oVar, bool bDeleteRange)
        {
            Delete(oVar, true, bDeleteRange);
        }
        /// <summary>
        /// deletes the variable at the specified collection index and its associated tags
        /// </summary>
        /// <param name="iIndex"></param>
        public void Delete(int iIndex)
        {
            Delete(iIndex, true);
        }

        /// <summary>
        /// deletes the variable with the specified name and its associated tags
        /// </summary>
        /// <param name="xName"></param>
        public void Delete(string xName)
        {
            Delete(xName, true);
        }

        /// <summary>
        /// sets the values of the variables in the 
        /// collection to their default values, or
        /// the relevant prefill value, if one is specified.
        /// If Variable is in a child Segment, Prefill Name
        /// will consist of relative path plus variable name
        /// </summary>
        /// <returns>TRUE if all specified values were assigned</returns>
        public bool AssignDefaultValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride)
        {
            DateTime t0 = DateTime.Now;
            int iPos1 = 0;
            //GLOG 6591
            int iVarCount = this.Count;
            for (int i = 0; i < iVarCount; i++) //GLOG 6591
            {

                //exit if assignment of a value has forced a segment refresh,
                //e.g. when variables are added or deleted by an action
                //with an expanded deletion scope which contains other tags
                //GLOG 6591: If action of previous variable has changed number of variables, start over
                //Moved to top of loop
                if (this.CollectionIsObsolete || iVarCount != this.Count)
                    return false;

                //GLOG 3475: Additional Prefill Override options added
                bool bPrefillOverride = false;
                Segment oSegment = null;

                switch (this[i].AllowPrefillOverride)
                {
                    case Variable.PrefillOverrideTypes.Never:
                        bPrefillOverride = false;
                        break;
                    case Variable.PrefillOverrideTypes.Always:
                        bPrefillOverride = true;
                        break;
                    case Variable.PrefillOverrideTypes.SameSegmentID:
                        //GLOG 3475: Check if current Segment or any Parent Segments match ID
                        oSegment = this[i].Segment;
                        do
                        {
                            if (oPrefill.SegmentID == oSegment.ID)
                            {
                                bPrefillOverride = true;
                                break;
                            }
                            else
                                oSegment = oSegment.Parent;
                        } while (oSegment != null);
                        break;
                    case Variable.PrefillOverrideTypes.ForSameType:
                        //GLOG 3475: Check if current Segment or any Parent Segments match type
                        oSegment = this[i].Segment;
                        do
                        {
                            if (oPrefill.SegmentType == oSegment.TypeID)
                            {
                                bPrefillOverride = true;
                                break;
                            }
                            else
                                oSegment = oSegment.Parent;
                        } while (oSegment != null);
                        break;
                }
                
                //get variable
                Variable oVar = this[i];

                if (oVar != null)
                {
                    string xPrefill = null;
                    List<string> aNameList = new List<string>();
                    aNameList.Add(oVar.Name);
                    if (oVar.AssociatedPrefillNames != "")
                    {
                        //Check for matches with alternate names as well
                        string[] aVarNames = oVar.AssociatedPrefillNames.Split(',');
                        aNameList.AddRange(aVarNames);
                    }
                    foreach (string xName in aNameList)
                    {
                        //search for segment ID-qualified prefill name
                        string xQualifiedName = this.Segment.ID1.ToString() + "_" + xName;
                        xPrefill = oPrefill[xRelativePath + xQualifiedName];

                        if (xPrefill == null)
                        {
                            //search for segment TypeID-qualified prefill name
                            xQualifiedName = this.Segment.TypeID.ToString() + "_" + xName;
                            xPrefill = oPrefill[xRelativePath + xQualifiedName];

                            if (xPrefill == null)
                            {
                                xPrefill = oPrefill[xRelativePath + xQualifiedName];

                                if (xPrefill == null)
                                {
                                    xPrefill = oPrefill[xRelativePath + xName];

                                    if (xPrefill == null && xRelativePath != "")
                                    {
                                        //exact match not found -
                                        //relative path was specified, but no exact match was found -
                                        //check for just name without relative path - this is useful
                                        //when working with data packets, as prefill name/value pairs
                                        //don't use relative paths (they are divorced from segment
                                        //structure)

                                        //check for field in the prefill of the form
                                        //SegmentTypeVariableName - this is useful when a prefill
                                        //contains multiple fields of same type - e.g. LetterRecipients, ServiceRecipients -
                                        //which will be the case for master data, grail data
                                        xPrefill = oPrefill[xQualifiedName];

                                        if (xPrefill == null)
                                        {
                                            //no match for the fully qualified form -
                                            //check for the generic form
                                            xPrefill = oPrefill[xName];
                                        }
                                    }

                                    if (xPrefill == null)
                                    {
                                        //still no match - look for Child segment variables 
                                        //with the same name - e.g. when we use a letter prefill
                                        //to generate a memo, memo cc has no relative path, but the
                                        //letter prefill cc does, as letter cc is found in the signature
                                        for (int c = 0; c < oPrefill.Count; c++)
                                        {

                                            if (oPrefill.ItemName(c).EndsWith("." + xQualifiedName))
                                            {
                                                //SegmentTypeVariableName
                                                xPrefill = oPrefill[xQualifiedName];
                                            }
                                            else if (oPrefill.ItemName(c).EndsWith("." + xName))
                                            {
                                                xPrefill = oPrefill[c];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (xPrefill != null)
                            break;
                    }

                    //GLOG 3058: added condition to use prefill value when
                    //we're forcing prefill overrides and there is a prefill value
                    //GLOG 3124: Don't force override for Preference codes
                    //if (xPrefill == null || !oVar.AllowPrefillOverride ||
                    //    (bForcePrefillOverride && ((Expression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) &&
                    //    !Expression.ContainsPreferenceCode(oVar.DefaultValue)))) ||
                    //    !bForcePrefillOverride)

                    //conditions revamped per GLOG items 3081 and 3181
                    bool bContainsAuthorDetailCode = Expression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) &&
                        !Expression.ContainsPreferenceCode(oVar.DefaultValue);
                    if (bContainsAuthorDetailCode)
                    {
                        //GLOG 6123 (dm) - the intent of this exception was to ensure accurate
                        //accurate author detail, not to ignore the prefill for boolean variables
                        //that default based on author critera
                        string xAuthorValue = Expression.Evaluate(oVar.DefaultValue,
                            m_oSegment, m_oMPDocument).ToUpper();
                        bContainsAuthorDetailCode = ((xAuthorValue != "TRUE") &&
                            (xAuthorValue != "FALSE"));
                    }
                    //bool bContainsAuthorCode = Expression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) ||
                    //    Expression.ContainsPreferenceCode(oVar.DefaultValue);

                    //GLOG 3475
                    if (xPrefill == null ||
                        //GLOG 4519: Don't always evaluate Default Value with Author Field
                        //go by Saved Data Override value instead
                        //(this.Segment.LinkAuthorsToParent && this.Segment.Parent != null && bContainsAuthorCode) ||
                        (!bForcePrefillOverride && (!bPrefillOverride || bContainsAuthorDetailCode)))
                    {
                        //either 1) there is no prefill for this variable,
                        //2) the segment is linked to it's parent's authors and
                        //the default value of the variable contains an
                        //expression related to the author (GLOG item #2510),
                        //3) the variable can't accept a prefill value, or
                        //4)we're forcing prefill values on all variables -
                        //assign the default value if the value
                        //hasn't been set previously by another variable -
                        //e.g. through a variable action
                        //GLOG 6914 (dm) - skip detail sub variables - master variable
                        //is responsible for initializing its subs
                        if ((!oVar.HasInitializedValue) && (!oVar.IsDetailSubComponent))
                            this[i].AssignDefaultValue();
                    }
                    else
                    {
                        //set value to prefill - determine if prefill value is xml
                        XmlDocument oXML = new XmlDocument();
                        try
                        {
                            //GLOG 6149
                            //GLOG 7725: Make sure any &amp; tokens already in the string are converted properly
                            oXML.LoadXml("<Prefill>" + String.RestoreXMLChars(xPrefill) + "</Prefill>");
                        }
                        catch { }

                        if (oXML.DocumentElement != null && oXML.DocumentElement.FirstChild != null &&
                            oXML.DocumentElement.FirstChild.NodeType == XmlNodeType.Element)
                        {
                            //prefill is xml - check if variable actions require xml -
                            //if not, change value to text - this is useful for reline,
                            //in particular, as some relines use xml and some use text
                            bool bActionRequiresXML = false;
                            //GLOG 5999: If configured control is one of the types requiring XML, set to true
                            if (oVar.ControlType == mpControlTypes.DetailGrid ||
                                oVar.ControlType == mpControlTypes.DetailList || oVar.ControlType == mpControlTypes.RelineGrid)
                            {
                                bActionRequiresXML = true;
                            }
                            else
                            {
                                for (int j = 0; j < oVar.VariableActions.Count; j++)
                                {
                                    VariableAction oA = oVar.VariableActions[j];
                                    if (oA.Type == VariableActions.Types.InsertDetail ||
                                        oA.Type == VariableActions.Types.InsertDetailIntoTable ||
                                        oA.Type == VariableActions.Types.InsertReline ||
                                        oA.Type == VariableActions.Types.SetupDetailTable ||
                                        oA.Type == VariableActions.Types.SetupLabelTable ||
                                        oA.Type == VariableActions.Types.SetVariableValue ||
                                        oA.Type == VariableActions.Types.SetupDistributedSections)
                                    {
                                        //variable action requires xml
                                        bActionRequiresXML = true;
                                        break;
                                    }
                                }
                            }
                            if (!bActionRequiresXML)
                            {
                                //convert to text - extract first element
                                xPrefill = oXML.DocumentElement.FirstChild.InnerText;
                            }
                            else
                            {
                                //GLOG 6149: Restore double quotes in XML value
                                //GLOG 7725: Make sure &amp; tokens in the string are handled properly
                                xPrefill = String.RestoreXMLChars(xPrefill);
                            }
                        }
                        //unrem this code if we're updating contact detail
                        //when assigning variable values
                        if ((oVar.CIContactType != 0 || oVar.CIEntityName != "") && xPrefill.Contains("UNID="))
                        {
                            if (PrefillIsCIDetailDeficient(xPrefill, oVar))
                            {
                                //this variable is linked to CI and does not contain
                                //the required detail - automatically update contacts
                                xPrefill = GetUpdatedContactDetail(xPrefill, oVar, true);
                            }
                            //GLOG 5894: Discard CI Detail Token STring portion of Prefill Value if present
                            int iTokenStart = xPrefill.IndexOf("<CIDetailTokenString>");
                            int iTokenEnd = xPrefill.IndexOf("</CIDetailTokenString>");
                            if (iTokenStart > -1 && iTokenEnd > -1 && iTokenEnd > iTokenStart)
                            {
                                xPrefill = xPrefill.Substring(0, iTokenStart);
                            }
                        }

                        bool bUsePrefillValue = true;
                        int iCount = oVar.VariableActions.Count;

                        //GLOG 3474: Make sure any Variables with a ReplaceSegment action linked to the Variable value
                        //are only set to Prefill value if it corresponds to a valid assigned Segment ID
                        for (int k = 0; k < iCount; k++)
                        {
                            VariableAction oA = oVar.VariableActions[k];
                            if (oA.Type == VariableActions.Types.ReplaceSegment
                                && oA.Parameters.ToUpper().Contains("MYVALUE"))
                            {
                                //GLOG 15747: If New Segment ID parameter contains [MyValue] as part
                                //of a larger expression, substitute Prefill value to test for valid Segment ID
                                string[] aParams = oA.Parameters.Split(LMP.StringArray.mpEndOfSubField);
                                string xNewID = aParams[1];
                                if (xNewID.ToUpper().Contains("MYVALUE"))
                                {
                                    try
                                    {
                                        int iMVPos = xNewID.IndexOf("[myvalue]", StringComparison.CurrentCultureIgnoreCase);
                                        if (iMVPos > -1)
                                        {
                                            xNewID = xNewID.Substring(0, iMVPos) + xPrefill + xNewID.Substring(iMVPos + 9);
                                        }
                                        else
                                        {
                                            iMVPos = xNewID.IndexOf("myvalue", StringComparison.CurrentCultureIgnoreCase);
                                            if (iMVPos > -1)
                                            {
                                                xNewID = xNewID.Substring(0, iMVPos) + xPrefill + xNewID.Substring(iMVPos + 7);
                                            }
                                        }
                                        xNewID = Expression.Evaluate(xNewID, this.Segment, this.ForteDocument);
                                        ISegmentDef oSeg = Segment.GetDefFromID(xNewID);
                                        //Only check Admin Segment IDs
                                        if (oSeg is AdminSegmentDef)
                                        {
                                            //See if there are specific Assignments of this type for the parent Segment
                                            Assignments oAssignments = new Assignments(oVar.Segment.TypeID, oVar.Segment.ID1,
                                                oSeg.TypeID);
                                            if (oAssignments.Count > 0)
                                            {
                                                bUsePrefillValue = false;
                                                for (int l = 0; l < oAssignments.Count; l++)
                                                {
                                                    if (oAssignments.ItemFromIndex(l).AssignedObjectID == ((AdminSegmentDef)oSeg).ID)
                                                    {
                                                        //This is an assigned ID
                                                        bUsePrefillValue = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        //Value is not a valid Segment ID
                                        bUsePrefillValue = false;
                                    }
                                }
                                //Don't check any more actions
                                break;
                            }
                            //GLOG : 8066 : ceh - make sure variables with an IncludeExcludeBlocks action get updated
                            else if (oA.Type == VariableActions.Types.IncludeExcludeBlocks
                                && oA.Parameters.ToUpper().Contains("MYVALUE"))
                            {
                                //get parameters
                                string[] aParams = oA.Parameters.Split(LMP.StringArray.mpEndOfSubField);
                                string xBlockName = Expression.Evaluate(aParams[0],
                                                        m_oSegment, this.ForteDocument); 

                                //execute only if exclusion is specified and
                                //block is still in the document
                                if (xPrefill.ToLower() == "false")
                                {
                                    Block oBlock = null;
                                    try
                                    {
                                        oBlock = m_oSegment.Blocks.ItemFromName(
                                            xBlockName);
                                    }
                                    catch { }

                                    if (oBlock != null)
                                    {
                                        oA.Execute();
                                        break;
                                    }
                                }
                            }
                        }

                        //GLOG 3474: New ID for ReplaceSegment was not valid assignment for parent Segment
                        if (!bUsePrefillValue)
                        {
                            oVar.AssignDefaultValue();
                        }
                        else if (!LMP.String.CompareBooleanCaseInsensitive(oVar.Value, xPrefill))
                        {
                            //GLOG - 3783 - ceh
                            string xCIToken = "";
                            string xControlProps = oVar.ControlProperties;

                            //get ci token if specified
                            iPos1 = xControlProps.IndexOf("CIToken=<");
                            int iIndexCount = 1;

                            if (iPos1 != -1)
                            {
                                iPos1 += 9;
                                int iPos2 = xControlProps.IndexOf(">", iPos1);
                                xCIToken = xControlProps.Substring(iPos1, iPos2 - iPos1);


                                //find last instance of Index & get value
                                int iNewPos = xPrefill.LastIndexOf("Index=");
                                if (iNewPos != -1)
                                {
                                    //get last index value
                                    string xNewValue = xPrefill.Substring(iNewPos + 7);
                                    iNewPos = xNewValue.IndexOf("\"");
                                    try
                                    {
                                        iIndexCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                                    }
                                    catch
                                    {
                                        //GLOG item #4200 - dcf
                                        //xml string does not include Index="x"
                                        //search for Index='x', which is the format
                                        //returned by the document analyzer
                                        iNewPos = xNewValue.IndexOf("'");
                                        iIndexCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                                    }
                                }

                                //cycle through indexes and replace CI Token element if necessary
                                for (int k = 1; k <= iIndexCount; k++)
                                {
                                    if (!xPrefill.Contains("<" + xCIToken + " Index=" + "\"" + k.ToString() + "\""))
                                    {
                                        xPrefill = xPrefill.Replace("FULLNAMEWITHPREFIXANDSUFFIX", xCIToken);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //get sub controls if associated control is DetailGrid
                                if (oVar.ControlType == mpControlTypes.DetailGrid)
                                {
                                    string xConfigString = xControlProps.Split('�')[0];
                                    int iPos = xConfigString.IndexOf("=");
                                    xConfigString = xConfigString.Substring(iPos + 1);
                                    string[] aConfigParams = xConfigString.Split('�');
                                    string xFields = "";

                                    //get every 2nd array item - that's the field name
                                    for (int j = 0; j < aConfigParams.Length; j++)
                                    {
                                        if (j % 5 == 1)
                                        {
                                            xFields += aConfigParams[j] + ",";
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(xFields))
                                    {
                                        xFields = "," + xFields;
                                        //GLOG 7588: If Prefill uses a different FULLNAME token, conform to current variable
                                        if (xFields.ToUpper().Contains(",FULLNAMEWITHPREFIXANDSUFFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAME,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAME>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAME>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAME>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAMEWITHPREFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAMEWITHPREFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAMEWITHPREFIX>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAMEWITHSUFFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAMEWITHSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAMEWITHSUFFIX>");
                                        }
                                        //check each row of the prefill value - if the value of
                                        //each of these fields is empty, remove the row from the 
                                        //prefill for this variable
                                        //string xPrefillXml = "<Prefill>" + xPrefill + "</Prefill>";
                                        string xPrefillXml = "<Prefill>" + xPrefill.Replace("&quot;", "\"").Replace("&", "&amp;") + "</Prefill>";
                                        XmlDocument oPrefillXML = new XmlDocument();
                                        oPrefillXML.LoadXml(xPrefillXml);

                                        int k = 0;

                                        XmlNodeList oNodes = null;
                                        do
                                        {
                                            k++;
                                            bool bEntityContainsValues = false;
                                            oNodes = oPrefillXML.SelectNodes("//*[@Index='" + k.ToString() + "']");

                                            foreach (XmlNode oNode in oNodes)
                                            {
                                                if (xFields.ToUpper().Contains("," + oNode.Name.ToUpper() + ","))
                                                {
                                                    //node is one of the specified fields -
                                                    //check for value
                                                    if (!string.IsNullOrEmpty(oNode.InnerText))
                                                    {
                                                        bEntityContainsValues = true;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (!bEntityContainsValues)
                                            {
                                                foreach (XmlNode oNode in oNodes)
                                                {
                                                    xPrefill = xPrefill.Replace(oNode.OuterXml, "");
                                                }

                                            }
                                        } while (oNodes.Count > 0);
                                    }
                                }

                                //GLOG 7588: This is now being handled above
                                ////GLOG - 3783 - ceh
                                ////necessary to clean up manually inserted recipients
                                //iPos1 = xControlProps.IndexOf("FULLNAMEWITHPREFIXANDSUFFIX");
                                //if (iPos1 != -1)
                                //{
                                //    xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                //    xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                //}
                            }

                            try
                            {
                                //prefill value is different than existing value
                                oVar.SetValue(xPrefill);
                            }
                            catch (System.Exception oE)
                            {
                                //could not assign prefill value - alert and assign default value
                                MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidPrefillValue"),
                                    oVar.DisplayName, oE.Message),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                oVar.AssignDefaultValue();
                            }
                        }
                        else
                        {
                            //SetValidationField variable, as the SetValidationField function
                            //runs only when the value is set
                            oVar.SetValidationField();

                            //execute actions for include/exclude type variable if necessary -
                            //text may need updating even when default value matches current value;
                            //TODO: this issue theoretically applies to any variable with an action
                            //that has a field code in one of its parameters, so this code may need
                            //to be generalized at some point if we can find an efficient way to do so
                            //GLOG 6838: Test for additional variants of ValueSourceExpression
                            if (((oVar.ValueSourceExpression == "[TagValue] ^!= ") ||
                                (oVar.ValueSourceExpression == "[TagValue] ^!=") ||
                                (oVar.ValueSourceExpression == "[TagValue] ^!= [Empty]") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!= ") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!=") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!= [Empty]")) &&
                                (xPrefill.ToLower() == "true") && (oVar.SecondaryDefaultValue != null))
                            {

                                xPrefill = Expression.Evaluate(
                                    oVar.SecondaryDefaultValue, this.m_oSegment, this.ForteDocument);
                                string xNodeText = xPrefill;
                                //10.2
                                if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
                                {
                                    //compare tag value to secondary default value
                                    Word.XMLNode oWordNode = this.Nodes.GetWordTags(oVar.TagID)[0];

                                    //GLOG 3318: Compare initial input case if All Caps is applied
                                    xNodeText = oWordNode.Text;
                                    if (!string.IsNullOrEmpty(xNodeText) && xNodeText.ToUpper() == xNodeText)
                                        //GLOG 8407: Only check revisions if Segment is fully created
                                        xNodeText = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                                }
                                else
                                {
                                    Word.ContentControl oCC = this.Nodes.GetContentControls(oVar.TagID)[0];
                                    xNodeText = oCC.Range.Text;
                                    if (!string.IsNullOrEmpty(xNodeText) && xNodeText.ToUpper() == xNodeText)
                                        //GLOG 8407: Only check revisions if Segment is fully created
                                        xNodeText = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range, this.Segment.CreationStatus == Base.Segment.Status.Finished);
                                }
                                if (xNodeText != xPrefill)
                                    oVar.VariableActions.Execute();
                                //GLOG 2957: Also update preference
                                oVar.SavePreferenceIfNecessary();
                            }
                            else if (oVar.Segment is LMP.Architect.Base.IDocumentStamp)
                            {
                                //GLOG 3508: Make sure checkbox options selected in Trailer dialog
                                //are saved as preference if the Variable is Tagless
                                oVar.SavePreferenceIfNecessary();
                            }
                        }
                    }
                }
            }
            LMP.Benchmarks.Print(t0, this.Segment.FullTagID);

            return true;
        }

        /// <summary>
        /// sets the values of the variables in the 
        /// collection to their default values
        /// </summary>
        /// <returns>TRUE if all specified values were assigned</returns>
        public bool AssignDefaultValues()
        {
            DateTime t0 = DateTime.Now;
            for (int i = 0; i < this.Count; i++)
            {
                //exit if assignment of a value has forced a segment refresh,
                //e.g. when variables are added or deleted by an action
                //with an expanded deletion scope which contains other tags
                if (this.CollectionIsObsolete)
                    return false;

                //GLOG 6914 (dm) - skip detail sub variables - master variable
                //is responsible for initializing its subs
                if ((this[i] != null) && !this[i].IsDetailSubComponent)
                {
                    this[i].AssignDefaultValue();
                }
            }
            LMP.Benchmarks.Print(t0);

            return true;
        }

        /// <summary>
        /// returns a variable populated with the supplied definition values
        /// </summary>
        /// <param name="xVariableDef"></param>
        /// <returns></returns>
        public Variable GetVariableFromDefinition(string xVariableDef)
        {
            xVariableDef = xVariableDef.Trim('|');

            //convert definition to array
            string[] aVariableDef = xVariableDef.Split('�');

            //get Variable from array
            return GetVariableFromArray(aVariableDef);
        }

        /// <summary>
        /// returns the updated contact detail
        /// of the specified contact detail
        /// </summary>
        /// <param name="xContactDetailXML"></param>
        /// <returns></returns>
        internal string GetUpdatedContactDetail(string xContactDetailXML, Variable oVar)
        {
            return GetUpdatedContactDetail(xContactDetailXML, oVar, false);
        }
        internal string GetUpdatedContactDetail(string xContactDetailXML, Variable oVar, bool bMissingOnly)
        {
            //cycle through each item of contact detail-
            //if it has a UNID replace it with updated detail-
            //else leave as is
            string xUpdatedDetail = "";
            //GLOG 5894: Get Source variable Detail Token String from Prefill
            ArrayList aPrefillCIFields = new ArrayList();
            if (xContactDetailXML.Contains("<CIDetailTokenString>") && xContactDetailXML.Contains("</CIDetailTokenString>"))
            {
                int iStart = xContactDetailXML.IndexOf("<CIDetailTokenString>") + @"<CIDetailTokenString>".Length;
                int iEnd = xContactDetailXML.IndexOf("</CIDetailTokenString>");
                if (iEnd > iStart)
                {
                    string xPrefillCIFields = xContactDetailXML.Substring(iStart, iEnd - iStart);
                    //Fieldcode separator could be either 1 or 2 underscores - make consistent
                    xPrefillCIFields = xPrefillCIFields.Replace("[CIDetail__", "[CIDetail_");
                    string xTokenPattern = @"\[CIDetail_\w*\]";
                    MatchCollection oTokens = Regex.Matches(xPrefillCIFields, xTokenPattern);
                    foreach (Match oToken in oTokens)
                    {
                        string xToken = oToken.Value.Replace("[CIDetail_", "").Replace("]", "");
                        aPrefillCIFields.Add(xToken.ToUpper());
                    }
                }
            }
            Match oMatch = null;
            int j = 1;
            ArrayList oUNIDs = new ArrayList();
            string xMatch = null;

            try
            {
                do
                {
                    string xCurDetail = "";
                    string xNewDetail = "";
                    //get entity UNID
                    string xPattern = "Index=\"" + j.ToString() + "\" UNID=\".*?\"";
                    oMatch = Regex.Match(xContactDetailXML, xPattern);

                    if (oMatch == null || oMatch.Value == "")
                        //there are no more matches
                        break;

                    //parse UNID from match
                    xMatch = oMatch.Value;
                    int iPos = xMatch.IndexOf("UNID=");
                    xMatch = xMatch.Substring(iPos + 6, xMatch.Length - (iPos + 7));

                    if (xMatch != "0")
                    {
                        //entity has a UNID - get the contact
                        //associated with the UNID
                        string[] aUNIDS = { xMatch };

                        ICContacts oContacts = null;
                        try
                        {
                            System.Windows.Forms.Application.DoEvents();
                            oContacts = Contacts.GetContactsFromUNIDs(oVar, aUNIDS);
                            System.Windows.Forms.Application.DoEvents();
                        }
                        catch { }

                        //this will err if no contacts returned-
                        //user will be alerted below
                        if (oContacts != null && oContacts.Count() == 1)
                        {
                            //contact was found - get updated detail
                            xNewDetail = Contacts.GetDetail(oContacts, oVar, j);
                            //GLOG 5894: UNID of returned contact may not include Address Type values
                            //Replace current UNID with original string to ensure match between new and old 
                            Object oIndex = 1;
                            xNewDetail = xNewDetail.Replace("UNID=\"" + oContacts.Item(oIndex).UNID.ToString() + "\"", "UNID=\"" + aUNIDS[0] + "\"");
                        }

                        //GLOG 5894:  If specified, use value in Prefill if it was part of the original CI Detail Token String,
                        //otherwise, value from newly-retrieved Contact will be used.
                        if (bMissingOnly || oContacts == null || oContacts.Count() != 1)
                        {
                            //contact was not found - use existing detail
                            //JTS 9/24/09: This needs to use specific UNID, instead of '0'
                            //'|' is reserved RegEx character, so replace all instances with '\|'
                            //GLOG 4857: '^' is also reserved, and may occur in Groupwise UNID
                            //GLOG 7683: ( and ) added, as these may occur in Interaction UNID
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"" + xMatch.Replace("|", "\\|").Replace("^", "\\^").Replace("(", "\\(").Replace(")", "\\)") + "\">[^<>]*</[^<>]*>";
                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);
                            //GLOG 5894: Replace new Contact value with Prefill value if field was part of original CI Detail Token String.
                            if (xNewDetail != "")
                            {
                                MatchCollection oCurMatches = Regex.Matches(xNewDetail, xPattern);
                                foreach (Match oCurMatch in oCurMatches)
                                {
                                    string xField = oCurMatch.Value.Substring(1, oCurMatch.Value.IndexOf(" ") - 1);
                                    if (aPrefillCIFields.Contains(xField))
                                    {
                                        foreach (Match oElement in oMatches)
                                        {
                                            if (oElement.Value.StartsWith("<" + xField + " "))
                                            {
                                                xCurDetail += oElement.Value;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                        xCurDetail += oCurMatch.Value;
                                }
                            }
                            else
                            {
                                foreach (Match oElement in oMatches)
                                {
                                    xCurDetail += oElement.Value;
                                }
                            }

                        }
                        else
                            xCurDetail = xNewDetail;

                    }
                    else
                    {
                        //the entity does not have a UNID -
                        //use existing detail
                        xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                            "\" UNID=\"0\">[^<>]*</[^<>]*>";
                        MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                        foreach (Match oElement in oMatches)
                            xCurDetail += oElement.Value; ;
                    }
                    xUpdatedDetail += xCurDetail;
                    j++;
                } while (oMatch != null && oMatch.Value != "");

                return xUpdatedDetail;
            }
            catch
            {
                //some general problem occured - alert 
                //user that contact will not be updated -
                //get contact display name from xml
                string xName = null;
                int iPos = xContactDetailXML.IndexOf(">");
                if (iPos > -1)
                {
                    int iPos2 = xContactDetailXML.IndexOf("<", iPos);
                    xName = xContactDetailXML.Substring(iPos + 1, iPos2 - iPos - 1);
                }
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotUpdateContacts") + 
                    (xName != null ? xName : xContactDetailXML),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);

                return xContactDetailXML;
            }
        }
        #endregion
		#region *********************private members*********************
		/// <summary>
		/// returns the object data for all collection nodes as an array
		/// </summary>
		/// <returns></returns>
        private void PopulateStorage(ReadOnlyNodeStore oNodes)
        {
            DateTime t0 = DateTime.Now;

            m_oDefsByID = new Hashtable();
            m_oDefsByName = new Hashtable();
            m_oDefsByIndex = new ArrayList();
            m_oDefsByTagID = new Hashtable();
            m_oCIEnabledDefs = new List<Variable>();

            string xVariableDef = "";
            string[] aVariableDef;
            string xVars = "|";

            //create storage space
            string[] aVariableDefArray = new string[oNodes.Count];

            //cycle through collection, adding ObjectData string to array
            for (int i = 0; i < oNodes.Count; i++)
            {
                string xElementName = oNodes.GetItemElementName(i);
                string xTagID = oNodes.GetItemTagID(i);

                //skip variable definitions in tags of contained mSEGs,
                //since these variables belong to subsegment
                if ((m_oSegment != null) && (xElementName == "mSEG") && (xTagID != m_oSegment.FullTagID))
                    continue;

                //get object data - append deleted scopes to the end of the string to ensure that
                //we include the definitions of variables with expanded delete scopes that are
                //currently in their deleted state - separator is added to allow us to
                //determine which segment part the deleted scope is in
                string xObjectData = "";
                try
                {
                    //dm (2/11/10) - ensure that there's a trailing pipe between object data
                    //and deleted scopes - for some reason, the object data for generic letter
                    //was missing this, causing the first variable def in the deleted scopes
                    //to be appended to the end of the last variable def in the object data
                    xObjectData = oNodes.GetItemObjectData(i);
                    if (xObjectData.Substring(xObjectData.Length - 1, 1) != "|")
                        xObjectData += "|";

                    //10.2 (dm) - variable defs are now stored in doc vars, so are no longer
                    //in the deleted scopes xml itself
                    string xDeletedScopes = oNodes.GetItemDeletedScopes(i);

                    //1-14-11 (dm) - we may now have a mix of formats in the same
                    //DeletedScopes attribute - this means that we always need to
                    //replace any content controls with variable defs before appending
                    if (xDeletedScopes.Length > 2)
                    {
                        xDeletedScopes = this.GetDeletedScopeVariableDefs(xDeletedScopes);
                        xObjectData += xDeletedScopes;
                    }
                }
                catch
                {
                    xObjectData = "";
                }

                if (xObjectData != "")
                {
                    //find location of first variable def
                    int iPos1 = xObjectData.IndexOf("VariableDefinition=");

                    string xSegmentName = "";
                    ForteDocument.TagTypes oVarDefTagType = ForteDocument.TagTypes.Variable;

                    while (iPos1 > -1)
                    {
                        //variable def was found - get segment name
                        if (xSegmentName == "")
                        {
                            //trim tag name from tag id if the tag is an mvar
                            if (xElementName == "mVar")
                            {
                                int iPos = xTagID.LastIndexOf(".");

                                if (iPos > -1)
                                    xSegmentName = xTagID.Substring(0, iPos);
                            }
                            else
                                xSegmentName = xTagID;
                        }

                        //if this is a segment node, determine which attribute contains
                        //this variable definition
                        if (xElementName == "mSEG")
                        {
                            int iSepPos = xObjectData.IndexOf("��", iPos1);
                            //JTS 3/29/10: If ObjectData doesn't include delimiter, LastIndexOf will cause error below
                            if (iSepPos == -1)
                                iSepPos = xObjectData.Length;
                            int iPartStartPos = xObjectData.LastIndexOf("Part=", iSepPos);
                            if (iPartStartPos != -1)
                            {
                                //if part # was found, definition must be in mSEG's
                                //DeletedScopes attribute
                                oVarDefTagType = ForteDocument.TagTypes.DeletedBlock;
                            }
                            else
                                //no part # - definition is in mSEG obejct data
                                oVarDefTagType = ForteDocument.TagTypes.Segment;
                        }

                        //get variable def
                        int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                        //GLOG 4460: If final VariableDefinition in ObjectData isn't terminated with
                        //a pipe character, Substring function below will cause an error.
                        //Use end of ObjectData or DeleteTags string instead
                        if (iPos2 == -1)
                        {
                            iPos2 = xObjectData.IndexOf("��", iPos1 + 1);
                            if (iPos2 == -1)
                                iPos2 = xObjectData.Length;
                        }

                        int iValueStartPos = iPos1 + "VariableDefinition".Length + 1;

                        //parse variable def
                        xVariableDef = xObjectData.Substring(
                            iValueStartPos, iPos2 - iValueStartPos);
                        xVariableDef = xVariableDef.TrimEnd(new char[] { '|' });
                        //Restore any Pipe characters within the individual Variable Definition
                        xVariableDef = xVariableDef.Replace(String.mpPipeReplacementChars, "|");
                        //convert to array
                        aVariableDef = xVariableDef.Split('�');

                        string xID = aVariableDef[0];
                        string xName = aVariableDef[1];

                        //check for duplicates and variable defs in the deletion scope
                        //xml of another variable - duplicates may occur if a variable
                        //with an expanded delete scope is in more than one part or
                        //has both mVars and mDels
                        if ((xVars.IndexOf('|' + xName + '|') == -1) &&
                            (!VariableIsDeleted(xObjectData, xID, xName)))
                        {
                            //get variable object
                            Variable oVar = this.GetVariableFromArray(aVariableDef);

                            //set runtime properties
                            oVar.SegmentName = xSegmentName;
                            if (oVarDefTagType == ForteDocument.TagTypes.DeletedBlock)
                                oVar.TagID = xTagID + "." + xName;
                            else
                                oVar.TagID = xTagID;
                            oVar.TagParentPartNumbers = oNodes.GetItemPartNumbers(oVar.TagID);

                            //set tag type
                            string xTagType = oNodes.GetItemElementName(oVar.TagID);
                            switch (xTagType)
                            {
                                case "mVar":
                                    oVar.TagType = ForteDocument.TagTypes.Variable;
                                    break;
                                case "mDel":
                                    oVar.TagType = ForteDocument.TagTypes.DeletedBlock;
                                    break;
                                case "mixed":
                                    oVar.TagType = ForteDocument.TagTypes.Mixed;
                                    break;
                                default:
                                    oVar.TagType = ForteDocument.TagTypes.Segment;
                                    break;
                            }

                            //set tag prefix id
                            oVar.TagPrefixID = oNodes.GetItemTagPrefixID(xTagID);

                            //10.2 (dm) - set next object database id for segment
                            m_oSegment.NextObjectDatabaseID = oVar.ObjectDatabaseID + 1;

                            oVar.IsDirty = false;

                            //add to internal arrays
                            AddToInternalArrays(oVar);

                            //add variable to CI variables collection
                            //if variable is CI-enabled
                            if (oVar.CIDetailTokenString != "" &&
                                (oVar.CIContactType != 0 || oVar.CIEntityName != ""))
                                m_oCIEnabledDefs.Add(oVar);

                            //prevent duplicates
                            xVars = xVars + xName + '|';
                        }

                        //find location of next variable def
                        iPos1 = xObjectData.IndexOf("VariableDefinition=", iPos1 + 1);
                    }
                }
            }

            CompressIndexArray();

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// Make the ExecutionIndex equal the array index position + 1. This correction is necessary
        /// when two or more variables stored in the m_oDefsByIndex arrayList have the same ExectionIndex. 
        /// This can also happen when a variable is deleted.
        /// </summary>
        private void AssociateExecutionIndexToIndexArrayPosition()
        {
            DateTime t0 = DateTime.Now;

            //GLOG 1407 - exit if called while delete scopes are getting updated -
            //this will avoid a potential object data mismatch between associated mVars
            //in different delete states
            if (this.ForteDocument.DeleteScopesUpdatingInDesign)
                return;

            for (int i = 0; i < m_oDefsByIndex.Count; i++)
            {
                if (m_oDefsByIndex[i] != null)
                {
                    Variable oVar = ((Variable)(m_oDefsByIndex[i]));

                    // Correct the execution index to correspond to i + 1.
                    if (oVar.ExecutionIndex != i + 1)
                    {
                        oVar.ExecutionIndex = i + 1;

                        // Save the change to the variable's execution index so that when a refresh is
                        // performed the new i + 1 execution index value is retrieved.
                        this.Save(oVar);
                    }
                }
            }

            LMP.Benchmarks.Print(t0);
        }

		/// <summary>
		/// removes null elements in internal index array
		/// </summary>
		private void CompressIndexArray()
		{
			//cycle through array from last to 
			//first index removing null elements
			for(int i=m_oDefsByIndex.Count - 1; i>=0; i--)
				if(m_oDefsByIndex[i] == null)
					m_oDefsByIndex.RemoveAt(i);
            if (this.ForteDocument.Mode == ForteDocument.Modes.Design)
                AssociateExecutionIndexToIndexArrayPosition();
		}
		/// <summary>
		/// returns a variable populated with the supplied definition values
		/// </summary>
		/// <param name="aDef"></param>
		/// <returns></returns>
		private Variable GetVariableFromArray(object[] aDef)
		{
			DateTime t0 = DateTime.Now;

			Variable oVar = null;
			string xActions = "";

			try
			{
				string xID = aDef[0].ToString();

                if (xID.StartsWith("U"))
                {
                    //definition is for a user variable -
                    if (this.Segment == null)
                        oVar = new UserVariable(this.ForteDocument);
                    else
                        oVar = new UserVariable(this.Segment);
                }
                else
                {
                    //definition is for an admin variable
                    if (this.Segment == null)
                        oVar = new AdminVariable(this.ForteDocument);
                    else
                        oVar = new AdminVariable(this.Segment);
                }
                oVar.ID = xID;
				oVar.Name = aDef[1].ToString();
				oVar.DisplayName = aDef[2].ToString();
				oVar.TranslationID = Convert.ToInt32(aDef[3]);
				oVar.ExecutionIndex = Convert.ToInt32(aDef[4]);

				//there may be two values stored in DefaultValue field
				string[] aDefaultValue = aDef[5].ToString().Split('�');
				oVar.DefaultValue = aDefaultValue[0];
				if (aDefaultValue.Length > 1)
					oVar.SecondaryDefaultValue = aDefaultValue[1];

				oVar.ValueSourceExpression = aDef[6].ToString();

                //GLOG 2355 - convert detail grid reserve value to xml if necessary -
                //this requires loading control type and properties first
                oVar.ControlType = (LMP.Data.mpControlTypes)System.Convert.ToByte(aDef[9]);
                oVar.ControlProperties = aDef[10].ToString();
                //GLOG 6149:  Replace any &quot; tokens left in Reserve Value with double qoutes
                oVar.ReserveValue = oVar.DetailReserveValueToXML(aDef[7].ToString().Replace("&quot;", "\""));

				//index 8 is not currently used
				//oVar.Reserved = aDef[8].ToString();

				oVar.ValidationCondition = aDef[11].ToString();
				oVar.ValidationResponse = aDef[12].ToString();
				oVar.HelpText = aDef[13].ToString();

				//populate variable actions collection
				xActions = aDef[14].ToString();
				oVar.VariableActions.SetFromString(xActions);

				//populate control actions collection
				xActions = aDef[15].ToString();
				oVar.ControlActions.SetFromString(xActions);

                oVar.RequestCIForEntireSegment = bool.Parse(aDef[16].ToString());
                oVar.CIContactType = (ciContactTypes) Convert.ToInt32(aDef[17]);
                oVar.CIDetailTokenString = aDef[18].ToString();
                oVar.CIAlerts = (ciAlerts)Convert.ToInt32(aDef[19]);
                oVar.DisplayIn = (Variable.ControlHosts)Convert.ToInt32(aDef[20]);
                oVar.DisplayLevel = (Variable.Levels)Convert.ToInt32(aDef[21]);
                oVar.DisplayValue = aDef[22].ToString();
                oVar.Hotkey = aDef[23].ToString();

                //these properties were added after segment production began,
                //and hence, may not be in the variable definition string
                if (aDef.GetUpperBound(0) > 23)
                    oVar.MustVisit = Convert.ToBoolean(aDef[24]);
                else
                    oVar.MustVisit = false;
                if (aDef.GetUpperBound(0) > 24)
                {
                    //GLOG 3475: Convert old True/False value to new Enum
                    if (!String.IsNumericInt32(aDef[25].ToString()))
                    {
                        if (aDef[25].ToString().ToUpper() == "FALSE")
                            oVar.AllowPrefillOverride = Variable.PrefillOverrideTypes.Never;
                        else 
                            //if AllowPrefillOverride=True and the variable is an author pref,
                            //reset AllowPrefillOverride to SameSegmentID.
                            if(oVar.DefaultValue.ToUpper().Contains("AUTHORPREFERENCE_"))
                            oVar.AllowPrefillOverride = Variable.PrefillOverrideTypes.SameSegmentID;
                        else
                            oVar.AllowPrefillOverride = Variable.PrefillOverrideTypes.Always;
                    }
                    else
                        oVar.AllowPrefillOverride = (Variable.PrefillOverrideTypes)Convert.ToInt32(aDef[25]);
                }
                else
                    //default is to allow prefill override
                    oVar.AllowPrefillOverride = Variable.PrefillOverrideTypes.Always;
                
                if (aDef.GetUpperBound(0) > 25)
                    oVar.IsMultiValue = Convert.ToBoolean(aDef[26]);
                else
                    oVar.IsMultiValue = false;

                if (aDef.GetUpperBound(0) > 26)
                    oVar.AssociatedPrefillNames = aDef[27].ToString();
                else
                    oVar.AssociatedPrefillNames = "";

                if (aDef.GetUpperBound(0) > 27)
                    oVar.TabNumber = Int32.Parse(aDef[28].ToString());
                else
                    oVar.TabNumber = 1;

                if (aDef.GetUpperBound(0) > 28)
                    oVar.CIEntityName = aDef[29].ToString();
                else
                    oVar.CIEntityName = "";

                //GLOG 2645
                if (aDef.GetUpperBound(0) > 29)
                    oVar.SetRuntimeControlValues(aDef[30].ToString(), false);
                else
                    oVar.SetRuntimeControlValues("", false);

                //GLOG 1408
                if (aDef.GetUpperBound(0) > 30)
                    oVar.NavigationTag = aDef[31].ToString();
                else
                    oVar.NavigationTag = "";

                //10.2
                if (aDef.GetUpperBound(0) > 31)
                    oVar.ObjectDatabaseID = Int32.Parse(aDef[32].ToString());
                else
                    oVar.ObjectDatabaseID = 0;

                //subscribe to events
				oVar.ValueAssigned += new ValueAssignedEventHandler(OnVariableValueAssigned);
                oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            }
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.DataException(
					LMP.Resources.GetLangString(
						"Error_InvalidVariableDefinition") + oVar.ID, oE);
			}

			LMP.Benchmarks.Print(t0, oVar.DisplayName);
			return oVar;
		}

		/// <summary>
		/// true IFF definition for the variable with xID and xName
		/// is in the delete scope xml of another variable
		/// </summary>
		/// <param name="xObjectData"></param>
		/// <param name="xVariableName"></param>
		/// <returns></returns>
		private bool VariableIsDeleted(string xDeletedScopes, string xID, string xName)
		{
			//search for delete scope records in xDeletedScopes
			int iPos = xDeletedScopes.IndexOf("DeletedTag=");
			while (iPos != -1)
			{
				//delete scope found - get name of variable
				iPos = iPos + 11;
				int iPos2 = xDeletedScopes.IndexOf('�', iPos);
                if (xDeletedScopes.Substring(iPos, iPos2 - iPos) != xName)
                {
                    //delete scope is for a different variable - check whether
                    //it contains definition for this variable
                    iPos2 = xDeletedScopes.IndexOf("�/w:body>|", iPos);
                    string xScope = xDeletedScopes.Substring(iPos, iPos2 - iPos);
                    if (xScope.IndexOf("VariableDefinition=" + xID) != -1)
                        return true;
                }
                else
                {
                    //delete scope is for this variable, so variable is still active -
                    //verify that there's a corresponding mDel
                    //GLOG 6328 (dm) - include element parameter - this will avoid
                    //loading an obsolete definition in place of the one contained in
                    //a subsequently added variable with the same name
                    if (this.Nodes.NodeExists(this.Segment.FullTagID + '.' + xName, "mDel"))
                        return false;
                    else
                        //no mDel - don't add variable, but leave delete scope in tag -
                        //we may at some point want to prompt users for the insertion
                        //location when the mDel is missing
                        return true;
                }
				iPos = xDeletedScopes.IndexOf("DeletedTag=", iPos);
			}
			return false;
		}

		/// <summary>
		/// adds the specified variable to the internal arrays -
		/// assumes that variable has not already been added
		/// </summary>
		/// <param name="oVar"></param>
		private void AddToInternalArrays(Variable oVar)
		{
			//add to hashtables
			m_oDefsByID.Add(oVar.ID, oVar);
			m_oDefsByName.Add(oVar.Name, oVar);

			//add to tag id hashtable
            XmlNode oNode = null;
            string xFullTagID = null;
            if (!oVar.IsStandalone)
                xFullTagID = System.String.Concat(oVar.SegmentName, '.', oVar.Name);
            else
                xFullTagID = oVar.Name;
            try
            {
                oNode = this.Nodes.GetItem(xFullTagID);
            }
            catch{}
            if (oNode !=null)
				m_oDefsByTagID.Add(xFullTagID, oVar);

			//add to array list at proper index
            //GLOG 7610 (dm) - position relative to the defined execution
            //indexes of the existing variables, not their current positions
            int iCount = m_oDefsByIndex.Count;
            if (iCount == 0)
                m_oDefsByIndex.Add(oVar);
            else
            {
                for (int i = 0; i < iCount; i++)
                {
                    string[] aProps = m_oDefsByIndex[i].ToString().Split(
                        LMP.StringArray.mpEndOfValue);
                    if (int.Parse(aProps[8]) > oVar.ExecutionIndex)
                    {
                        m_oDefsByIndex.Insert(i, oVar);
                        break;
                    }
                    else if (i == iCount - 1)
                        m_oDefsByIndex.Add(oVar);
                }
            }

            //if(oVar.ExecutionIndex > m_oDefsByIndex.Count)
            //{
            //    //index exceeds the number of array elements -
            //    //add appropriate number of null elements
            //    int iNumElementsToAdd = oVar.ExecutionIndex - m_oDefsByIndex.Count - 1;
            //    for(int i=1; i<=iNumElementsToAdd; i++)
            //        m_oDefsByIndex.Add(null);

            //    //add item at specified index
            //    m_oDefsByIndex.Add(oVar);
            //}
            //else
            //{
            //    //there's already an array element for this execution index
            //    if(m_oDefsByIndex[oVar.ExecutionIndex - 1] == null)
            //    {
            //        //there is no variable at the specified index -
            //        //set var at specified index
            //        m_oDefsByIndex[oVar.ExecutionIndex - 1] = oVar;
            //    }
            //    else
            //    {
            //        //there's already a var at index - 
            //        //insert at specified index
            //        m_oDefsByIndex.Insert(oVar.ExecutionIndex - 1, oVar);
            //    }
            //}
		}
		/// <summary>
		/// deletes the specified variable from the internal arrays
		/// </summary>
		/// <param name="oVar"></param>
		private void DeleteFromInternalArrays(Variable oVar)
		{			
			//delete from internal arrays
			m_oDefsByIndex.Remove(oVar);
			m_oDefsByID.Remove(oVar.ID);
			m_oDefsByName.Remove(oVar.Name);
			m_oDefsByTagID.Remove(oVar.TagID);
            
            //GLOG 3461: Remove from list of CI-enabled variables
            m_oCIEnabledDefs.Remove(oVar);
            if (this.ForteDocument.Mode == ForteDocument.Modes.Design)
                AssociateExecutionIndexToIndexArrayPosition();
		}

        /// <summary>
        /// returns true iff the supplied prefill does not
        /// contain data for the required CI field
        /// </summary>
        /// <param name="xPrefill"></param>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private bool PrefillIsCIDetailDeficient(string xPrefill, Variable oVar)
        {
            DateTime t0 = DateTime.Now;

            //get required CI fields for variable
            string xRequiredCIFields = oVar.CIDetailTokenString;
            //GLOG 5894: Get Source variable Detail Token String from Prefill
            string xPrefillCIFields = "";
            if (xPrefill.Contains("<CIDetailTokenString>") && xPrefill.Contains("</CIDetailTokenString>"))
            {
                int iStart = xPrefill.IndexOf("<CIDetailTokenString>") + @"<CIDetailTokenString>".Length;
                int iEnd = xPrefill.IndexOf("</CIDetailTokenString>");
                if (iEnd > iStart)
                {
                    xPrefillCIFields = xPrefill.Substring(iStart, iEnd - iStart);
                    //Fieldcode separator could be either 1 or 2 underscores - make consistent
                    xPrefillCIFields = xPrefillCIFields.Replace("[CIDetail__", "[CIDetail_");
                }

            }
            //JTS 9/24/09: Fieldcode separator could be either 1 or 2 underscores - make consistent
            xRequiredCIFields = xRequiredCIFields.Replace("[CIDetail__", "[CIDetail_");
            //GLOG 5894: If Source and Target have same CI Detail Token String, no need to check further
            if (xPrefillCIFields.ToUpper() == xRequiredCIFields.ToUpper())
                return false;
            //cycle through each CI field,
            //checking if the prefill contains data for that field -
            //e.g. a letter prefill will typically not contain the phone
            //and fax numbers required by the fax recipients variable
            MatchCollection oMatches = Regex.Matches(xRequiredCIFields, @"\[CIDetail_\w*\]");
            foreach (Match oMatch in oMatches)
            {
                //parse out the field name
                string xField = oMatch.ToString();
                xField = xField.Substring(10, xField.Length - 11);
                //GLOG 5894:  Compare fields against Detail Token STring in Prefill, if available
                //Otherwise, use Variable XML Value
                if (xPrefillCIFields != "")
                {
                    xField = "[CIDetail_" + xField + "]";
                    //return true if the field name is
                    //not included in the Source Detail Token String
                    if (!xPrefillCIFields.ToUpper().Contains(xField.ToUpper()))
                        return true;
                }
                else
                {
                    //return true if the field name is
                    //not an element name in the xml
                    if (!xPrefill.ToUpper().Contains("<" + xField.ToUpper() + " "))
                        return true;
                }
            }

            LMP.Benchmarks.Print(t0);
            return false;
        }

        /// <summary>
        /// replaces each deleted scope in the specified string with a pipe-delimited
        /// string containing the definitions of the variables in that scope
        /// </summary>
        /// <param name="xDeletedScopes"></param>
        /// <returns></returns>
        private string GetDeletedScopeVariableDefs(string xDeletedScopes)
        {
            //cycle through deleted scopes
            string xDeletedScopesMod = xDeletedScopes;
			int iPos = xDeletedScopesMod.IndexOf("DeletedTag=");
            while (iPos != -1)
            {
                //get deleted scope
                string xVariableDefs = "";
                iPos = xDeletedScopesMod.IndexOf('�', iPos) + 1;
                int iPos2 = xDeletedScopesMod.IndexOf("�/w:body>|", iPos);
                string xScope = xDeletedScopesMod.Substring(iPos, iPos2 - iPos);

                //cycle through w:tag nodes, looking for mVars
                int iPos3 = xScope.IndexOf("�w:tag w:val=");
                if (iPos3 != -1)
                {
                    while (iPos3 != -1)
                    {
                        iPos3 += 14;
                        int iPos4 = xScope.IndexOf("\"", iPos3);
                        string xTag = xScope.Substring(iPos3, iPos4 - iPos3);
                        if (LMP.String.GetBoundingObjectBaseName(xTag) == "mVar")
                        {
                            //get object data of mVar
                            xVariableDefs += LMP.Forte.MSWord.WordDoc.GetAttributeValueFromTag(
                                xTag, "ObjectData");
                            if (xVariableDefs.Substring(xVariableDefs.Length - 1, 1) != "|")
                                xVariableDefs += '|';
                        }
                        iPos3 = xScope.IndexOf("�w:tag w:val=", iPos4);
                    }
                }
                else
                {
                    //binary branch (1/17/11 dm) - in 10.2, the DeletedScopes attribute
                    //may contain a mix of open xml and WordML scopes
                    iPos3 = xScope.IndexOf("VariableDefinition=");
                    while (iPos3 != -1)
                    {
                        int iPos4 = xScope.IndexOf('|', iPos3) + 1;
                        xVariableDefs += xScope.Substring(iPos3, iPos4 - iPos3);
                        iPos3 = xScope.IndexOf("VariableDefinition=", iPos4);
                    }
                }

                //replace scope with variable defs - need to leave wrapped in w:body tags
                //for compatibility with existing code
                xDeletedScopesMod = xDeletedScopesMod.Substring(0, iPos) + "�w:body>" +
                    xVariableDefs + xDeletedScopesMod.Substring(iPos2);

                iPos = xDeletedScopesMod.IndexOf("DeletedTag=", iPos);
            }

            return xDeletedScopesMod;
        }
		#endregion
		#region *********************event handlers*********************
		/// <summary>
		/// handles notification that a variable has been assigned -
		/// used to keep track of valid variables - when all variables
		/// are valid, this method will raise an event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVariableValueAssigned(object sender, EventArgs e)
		{
			Variable oVar = sender as Variable;
			bool bVarIsValid = oVar.HasValidValue;

			//validation may have changed if the var is valid and previously
			//the variables collection was not valid, or visa versa - this is
			//for performance only
            bool bValidationMayHaveChanged = (bVarIsValid != m_bVarsPreviouslySetValidationField);
            
			if(bValidationMayHaveChanged)
			{
				bool bVarsAreValid = this.ValuesAreValid;
				if(m_bVarsPreviouslySetValidationField != bVarsAreValid)
				{
					//validation has changed - notify subscribers
					if(this.ValidationChanged != null)
						//raise VariablesChanged event -
						//pass in whether or not values are SetValidationField
                        this.ValidationChanged(this,
                            new LMP.Architect.Base.ValidationChangedEventArgs(bVarsAreValid));

					//mark for next iteration
					m_bVarsPreviouslySetValidationField = bVarsAreValid;
                }
            }
        }
        void OnVariableVisited(object sender, VariableEventArgs oArgs)
        {
            if (this.VariableVisited != null)
            {
                this.VariableVisited(this, oArgs);
            }
        }

        #endregion
	}
}
