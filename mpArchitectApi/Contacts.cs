using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Architect.Api
{
    public static class Contacts
    {
        public enum Types
        {
            To = 1,
            From = 2,
            CC = 4,
            BCC = 8,
            Other = 16
        }
        
        /// <summary>
        /// returns the XML detail for the specified contacts
        /// in the format required by the specified variable
        /// </summary>
        /// <param name="oContacts"></param>
        /// <param name="oVariable"></param>
        /// <returns></returns>
        public static string GetDetail(ICContacts oContacts, Variable oVariable, int iFirstIndex)
        {
            string xCumulativeDetail = "";

            //get the CI format for the specified variable
            string xCIFormat = Expression.Evaluate(oVariable.CIDetailTokenString,
                oVariable.Segment, oVariable.Segment.ForteDocument);

            string[] xFields = null;
            string xFieldList = "";
            int j = 0;
            int c = iFirstIndex;
            int i = xCIFormat.IndexOf("<");
            string xAddressTypeName = "";

            // Get list of CI tokens to replace
            while (i > -1)
            {
                j = xCIFormat.IndexOf(">", i + 1);
                if (j > 0)
                {
                    if (xFieldList != "")
                        xFieldList = xFieldList + "|";
                    xFieldList = xFieldList + xCIFormat.Substring(i + 1, j - (i + 1));
                }
                i = xCIFormat.IndexOf("<", i + 1);
            }
            xFields = xFieldList.Split('|');

            // build XML string, but replacing < and > with @[ and ]@
            for (i = 0; i < xFields.Length; i++)
                xCIFormat = xCIFormat.Replace("<" + xFields[i] + ">",
                    "@[" + xFields[i] + " Index=\"\" UNID=\"\"]@<" +
                    xFields[i] + ">@[/" + xFields[i] + "]@");

            for (i = 1; i <= oContacts.Count(); i++)
            {
                //cycle through contacts, adding to control 
                //only those contacts of the specified type
                Object oIndex = i;

                if ((int)oContacts.Item(oIndex).ContactType == (int)oVariable.CIContactType)
                {
                    ICContact oContact = oContacts.Item(oIndex);
                    xCIFormat = xCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                    //GLOG : 7908 - cleanup tokens to avoid xml error
                    xCIFormat = xCIFormat.Replace("IFBUSINESS>", ">");

                    //GLOG : 7940 - deal with null return
                    try
                    {
                        xAddressTypeName = oContact.AddressTypeName.ToLower();
                    }
                    catch { }

                    //cleanup if conditioned as business and address is a business address
                    if (xAddressTypeName != "home")
                        xCIFormat = xCIFormat.Replace("IFBUSINESS", "");

                    //GLOG : 8031 : ceh
                    //fill in CoreAddress token
                    string xDetail = xCIFormat.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));

                    //fill in other details
                    xDetail = oContact.GetDetail(xDetail, true);

                    //replace any ampersands in the detail
                    xDetail = xDetail.Replace("&", "&amp;");

                    //replace temp markers for XML tags
                    xDetail = xDetail.Replace("@[", "<");
                    xDetail = xDetail.Replace("]@", ">");

                    // Fill in Index attribute
                    xDetail = xDetail.Replace("Index=\"\"", string.Concat("Index=\"",
                        c.ToString(), "\""));

                    // Fill in UNID attribute
                    xDetail = xDetail.Replace("UNID=\"\"", string.Concat("UNID=\"", oContact.UNID,
                        "\""));

                    //GLOG 4857: replace '\v' with '\r\n' - '\v' conflicts with XML
                    xDetail = xDetail.Replace("\v", "\r\n");
                    c++;
                    xCumulativeDetail += xDetail;
                }
            }

            return xCumulativeDetail;
        }

        private static string GetDetail(ICContacts oContacts, string xCIDetailTokenString, Segment oSegment, int iFirstIndex)
        {
            string xCumulativeDetail = "";

            //get the CI format for the specified variable
            string xCIFormat = Expression.Evaluate(xCIDetailTokenString, oSegment, oSegment.ForteDocument);

            string[] xFields = null;
            string xFieldList = "";
            int j = 0;
            int c = iFirstIndex;
            int i = xCIFormat.IndexOf("<");
            string xAddressTypeName = "";

            // Get list of CI tokens to replace
            while (i > -1)
            {
                j = xCIFormat.IndexOf(">", i + 1);
                if (j > 0)
                {
                    if (xFieldList != "")
                        xFieldList = xFieldList + "|";
                    xFieldList = xFieldList + xCIFormat.Substring(i + 1, j - (i + 1));
                }
                i = xCIFormat.IndexOf("<", i + 1);
            }
            xFields = xFieldList.Split('|');

            // build XML string, but replacing < and > with @[ and ]@
            for (i = 0; i < xFields.Length; i++)
                xCIFormat = xCIFormat.Replace("<" + xFields[i] + ">",
                    "@[" + xFields[i] + " Index=\"\" UNID=\"\"]@<" +
                    xFields[i] + ">@[/" + xFields[i] + "]@");

            for (i = 1; i <= oContacts.Count(); i++)
            {
                //cycle through contacts, adding to control 
                //only those contacts of the specified type
                Object oIndex = i;

                ICContact oContact = oContacts.Item(oIndex);
                xCIFormat = xCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                //GLOG : 7908 - cleanup tokens to avoid xml error
                xCIFormat = xCIFormat.Replace("IFBUSINESS>", ">");

                //GLOG : 7940 - deal with null return
                try
                {
                    xAddressTypeName = oContact.AddressTypeName.ToLower();
                }
                catch { }

                //cleanup if conditioned as business and address is a business address
                if (xAddressTypeName != "home")
                    xCIFormat = xCIFormat.Replace("IFBUSINESS", "");

                //GLOG : 8031 : ceh
                //fill in CoreAddress token
                string xDetail = xCIFormat.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));

                //fill in other detail
                xDetail = oContact.GetDetail(xDetail, true);

                //Replace any ampersands in the detail
                xDetail = xDetail.Replace("&", "&amp;");

                //replace temp markers for XML tags
                xDetail = xDetail.Replace("@[", "<");
                xDetail = xDetail.Replace("]@", ">");

                // Fill in Index attribute
                xDetail = xDetail.Replace("Index=\"\"", string.Concat("Index=\"",
                    c.ToString(), "\""));

                // Fill in UNID attribute
                xDetail = xDetail.Replace("UNID=\"\"", string.Concat("UNID=\"", oContact.UNID,
                    "\""));

                //GLOG 4857: replace '\v' with '\r\n' - '\v' conflicts with XML
                xDetail = xDetail.Replace("\v", "\r\n");
                c++;
                xCumulativeDetail += xDetail;
            }

            return xCumulativeDetail;
        }
        /// <summary>
        /// calls CI to populate a single variable
        /// </summary>
        public static string GetDetail(string xCIConfiguration, Segment oSegment, int iFirstIndex, int iMaxContacts)
        {
            string xLabel = "";
            string xDetail = "";

            //GLOG : 7469 : ceh
            if (!Data.Application.AllowCIWithoutOutlookRunning())
                return "";

            Data.Application.StartCIIfNecessary();
            ciAlerts iAlerts = ciAlerts.ciAlert_None;

            string xCITokenString = xCIConfiguration;

            ciRetrieveData iData = Contacts.GetCIFormat(xCITokenString, ref iAlerts, oSegment);

            try
            {
                ICContacts oContacts = null;

                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                //#3218 - ceh 
                //use variable display name as the label for the right side list box in the CI dialog
                oContacts = Data.Application.CISession
                    .GetContactsEx(iData, ciRetrieveData.ciRetrieveData_None,
                    ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None,
                    ICI.ciSelectionlists.ciSelectionList_To, 0, iAlerts, xLabel, null, null, null, iMaxContacts,
                    0,0,0);

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                if ((oContacts != null) && (oContacts.Count() > 0))
                {
                    xDetail = Contacts.GetDetail(oContacts, xCITokenString, oSegment, iFirstIndex);

                    //if (oContacts != null)
                    //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

                    oContacts = null;

                    return xDetail;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.CIException(
                    LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
            }
            finally
            {
                ////Restore state
                //oEnv.RestoreState();
            }

            return "";
        }

        /// <summary>
        /// returns the collection of contacts 
        /// corresponding to the specified array
        /// </summary>
        public static ICContacts GetContactsFromUNIDs(Variable oVar, string[] aUNIDs)
        {
            DateTime t0 = DateTime.Now;

            if (oVar.CIContactType == 0)
                return null;

            Data.Application.StartCIIfNecessary();

            ciRetrieveData iRetrieveDataTo = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataFrom = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataCC = ciRetrieveData.ciRetrieveData_None;
            ciRetrieveData iRetrieveDataBCC = ciRetrieveData.ciRetrieveData_None;
            ciAlerts iAlerts = ciAlerts.ciAlert_None;
            ciRetrieveData iRetrieveData = 0;

            List<Variable> oVarList = new List<Variable>();
            oVarList.Add(oVar);
            object[,] aCustomEntity = null;

            GetCIFormat(oVarList, ref iRetrieveDataTo, ref iRetrieveDataFrom,
                ref iRetrieveDataCC, ref iRetrieveDataBCC, ref aCustomEntity, ref iAlerts);

            switch (oVar.CIContactType)
            {
                case ciContactTypes.ciContactType_To:
                    iRetrieveData = iRetrieveDataTo;
                    break;
                case ciContactTypes.ciContactType_From:
                    iRetrieveData = iRetrieveDataFrom;
                    break;
                case ciContactTypes.ciContactType_CC:
                    iRetrieveData = iRetrieveDataCC;
                    break;
                case ciContactTypes.ciContactType_BCC:
                    iRetrieveData = iRetrieveDataBCC;
                    break;
            }

            //Save state
            Environment oEnv = new Environment(oVar.Segment.ForteDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                //create an empty contacts class
                ICContacts oContacts = GetCIContacts();

                //cycle through UNIDs, getting contact for each
                for (int i = 0; i < aUNIDs.Length; i++)
                {
                    //get UNID
                    string xUNID = aUNIDs[i];
                    if (xUNID.StartsWith("UNID="))
                    {
                        //parse out UNID
                        int iPos = xUNID.LastIndexOf("\"");
                        xUNID = xUNID.Substring(6, iPos - 6);
                    }

                    Data.Application.StartCIIfNecessary();

                    //get contact from UNID
                    ICContact oContact = Data.Application.CISession
                        .GetContactFromUNID(xUNID, iRetrieveData, iAlerts);

                    //#2802 - ceh
                    if (oContact == null)
                    {
                        if (iAlerts == ciAlerts.ciAlert_None)
                        {
                            //no contact with specified UNID
                            throw new LMP.Exceptions.UNIDException(
                                LMP.Resources.GetLangString("Error_InvalidCIUNID") + xUNID);
                        }
                    }
                    else
                    {
                        ciContactTypes oType = oVar.CIContactType;
                        oContact.ContactType = oType;
                        int iBefore = oContacts.Count();

                        //add contact to end of collection
                        oContacts.Add(oContact, iBefore);
                    }

                }

                return oContacts;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.CIException(
                    LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
            }
            finally
            {
                //Restore state
                oEnv.RestoreState();

                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// Select CI Data to retrieve by cycling through all variables in List collection
        /// </summary>
        /// <param name="oVarList"></param>
        /// <param name="iDataTo"></param>
        /// <param name="iDataFrom"></param>
        /// <param name="iDataCC"></param>
        /// <param name="iDataBCC"></param>
        /// <param name="iAlerts"></param>
        public static void GetCIFormat(List<Variable> oVarList, ref ciRetrieveData iDataTo, ref ciRetrieveData iDataFrom,
            ref ciRetrieveData iDataCC, ref ciRetrieveData iDataBCC, ref object[,] aCustomEntities, ref ciAlerts iAlerts)
        {
            object[,] aTempEntities = null;
            if (aCustomEntities != null)
            {
                //GLOG - 3407 - CEH
                if (oVarList.Count > 0)
                {
                    //create array and transfer existing items to it
                    aTempEntities = new object[oVarList.Count, 5];
                    int iUBound = aCustomEntities.GetUpperBound(0);

                    for (int iCount = 0; iCount <= iUBound; iCount++)
                    {
                        for (int j = 0; j < 5; j++)
                            aTempEntities[iCount, j] = aCustomEntities[iCount, j];
                    }
                }
                else
                    return;
            }
            else
                //create empty entities array -
                //we create the largest possible size (because we)
                //can't redimension dynamically - we'll cut it
                //down later
                aTempEntities = new object[oVarList.Count, 5];


            int i = 0;

            //cycle through each ci-enabled variable
            foreach (Variable oVar in oVarList)
            {
                //remove "IfBusiness" tag, as this is specific to Forte
                string xTokenString = oVar.CIDetailTokenString.Replace("IfBusiness", "")
                    .Replace("IFBUSINESS", "").Replace("ifbusiness", "");

                try
                {
                    xTokenString = Expression.Evaluate(
                        xTokenString, oVar.Segment, oVar.Segment.ForteDocument);
                }
                catch { };

                iAlerts = iAlerts | oVar.CIAlerts;

                ciRetrieveData iRetrieveData = ciRetrieveData.ciRetrieveData_None;
                // Check for Name fields to be retrieved
                foreach (string xField in new string[] {"<COMPANY>", "<TITLE>", "<DEPARTMENT>",
                    "<PREFIX>", "<SUFFIX>", "<FULLNAME>", "<FULLNAMEWITH",
                    "<FIRSTNAME>", "<MIDDLENAME>", "<LASTNAME>", "<MAILINGDETAIL>", "<FULLDETAIL>", "<GOESBY>"}) //GLOG 7794, 15877
                {
                    if (xTokenString.ToUpper().Contains(xField))
                    {
                        iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Names;
                        // no need to check remainder
                        break;
                    }
                }

                // Check for Address fields to be retrieved
                foreach (string xField in new string[] {"<COREADDRESS>", "<CITY>", "<STATE>", "<COUNTRY>",
                    "<STREET", "<ZIP", "<ADDITIONALINFORMATION>", "<ADDRESSTYPENAME>", "<ADDRESSID>", "<MAILINGDETAIL>", "<FULLDETAIL>"}) //GLOG 7794
                {
                    if (xTokenString.ToUpper().Contains(xField))
                    {
                        iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Addresses;
                        // no need to check remainder
                        break;
                    }
                }

                // Check for Phone or Phone Extension
                if (xTokenString.ToUpper().Contains("<PHONE") || xTokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Phones;

                // Check for Fax
                if (xTokenString.ToUpper().Contains("<FAX>") || xTokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token)
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Faxes;

                // Check for Email Address
                if (xTokenString.ToUpper().Contains("<EMAILADDRESS>") || xTokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token)
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_EAddresses;

                // Check for Salutation
                if (xTokenString.ToUpper().Contains("<SALUTATION>") || xTokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token)
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Salutation;

                // Check for Custom fields
                if (xTokenString.ToUpper().Contains("<CUSTOM") || xTokenString.ToUpper().Contains("<COREADDRESS>") || xTokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_CustomFields;

                // Retrieve Names only if nothing else specified
                if (iRetrieveData == ciRetrieveData.ciRetrieveData_None)
                    iRetrieveData = ciRetrieveData.ciRetrieveData_Names;

                if (oVar.CIContactType == ciContactTypes.ciContactType_To)
                    iDataTo = iDataTo | iRetrieveData;
                else if (oVar.CIContactType == ciContactTypes.ciContactType_From)
                    iDataFrom = iDataFrom | iRetrieveData;
                else if (oVar.CIContactType == ciContactTypes.ciContactType_CC)
                    iDataCC = iDataCC | iRetrieveData;
                else if (oVar.CIContactType == ciContactTypes.ciContactType_BCC)
                    iDataBCC = iDataBCC | iRetrieveData;
                else if (((int)oVar.CIContactType) == 16)
                {
                    //contact type is other - populate
                    //entities array for use with Custom CI UI -
                    //entities array has the following fields:
                    //Entity Name, ciRetrieveData, Existing Contact Display Name, 
                    //unknown (ask Charlie), and Existing Contact UNID
                    aTempEntities[i, 0] = oVar.DisplayName;
                    aTempEntities[i, 1] = (int)iRetrieveData;
                    string xUNID;
                    string xDisplayName;
                    //TODO:  There appears to be a bug in cSession.GetContactsFromArray
                    //so that it does not return the new values if the UI is prefilled with 
                    //existing Contacts.  For now, defaulting to empty values
                    GetExistingCIContactInfo(oVar, out xUNID, out xDisplayName);
                    aTempEntities[i, 4] = null; // xUNID;
                    aTempEntities[i, 2] = ""; // xDisplayName;
                    i++;
                }
            }

            //trim entities array
            aCustomEntities = new object[i, 5];

            for (int iCount = 0; iCount <= i - 1; iCount++)
            {
                for (int j = 0; j < 5; j++)
                    aCustomEntities[iCount, j] = aTempEntities[iCount, j];
            }

            LMP.Trace.WriteNameValuePairs("iDataTo", iDataTo, "iDataFrom", iDataFrom, "iDataCC",
                iDataCC, "iDataBCC", iDataBCC, "iAlerts", iAlerts);
        }

        public static ciRetrieveData GetCIFormat(string xCITokenString, ref ciAlerts iAlerts, Segment oSegment)
        {
            try
            {
                xCITokenString = Expression.Evaluate(
                    xCITokenString, oSegment, oSegment.ForteDocument);
            }
            catch { };

            ciRetrieveData iRetrieveData = ciRetrieveData.ciRetrieveData_None;

            // Check for Name fields to be retrieved
            foreach (string xField in new string[] {"<COMPANY>", "<TITLE>", "<DEPARTMENT>",
                "<PREFIX>", "<SUFFIX>", "<FULLNAME>", "<FULLNAMEWITH",
                "<FIRSTNAME>", "<MIDDLENAME>", "<LASTNAME>", "<MAILINGDETAIL>", "<FULLDETAIL>"}) //GLOG 7794
            {
                if (xCITokenString.ToUpper().Contains(xField))
                {
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Names;
                    // no need to check remainder
                    break;
                }
            }

            // Check for Address fields to be retrieved
            foreach (string xField in new string[] {"<COREADDRESS>", "<CITY>", "<STATE>", "<COUNTRY>",
                "<STREET", "<ZIP", "<ADDITIONALINFORMATION>", "<ADDRESSTYPENAME>", "<ADDRESSID>", "<MAILINGDETAIL>", "<FULLDETAIL>"}) //GLOG 7794
            {
                if (xCITokenString.ToUpper().Contains(xField))
                {
                    iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Addresses;
                    // no need to check remainder
                    break;
                }
            }

            // Check for Phone or Phone Extension
            if (xCITokenString.ToUpper().Contains("<PHONE") || xCITokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token)
                iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Phones;

            // Check for Fax
            if (xCITokenString.ToUpper().Contains("<FAX>") || xCITokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token))
                iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Faxes;

            // Check for Email Address
            if (xCITokenString.ToUpper().Contains("<EMAILADDRESS>") || xCITokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token))
                iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_EAddresses;

            // Check for Salutation
            if (xCITokenString.ToUpper().Contains("<SALUTATION>") || xCITokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token))
                iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_Salutation;

            // Check for Custom fields
            if (xCITokenString.ToUpper().Contains("<CUSTOM") || xCITokenString.ToUpper().Contains("<COREADDRESS>") || xCITokenString.ToUpper().Contains("<FULLDETAIL>")) //GLOG 7794:  <FULLDETAIL> could include any Token))
                iRetrieveData = iRetrieveData | ciRetrieveData.ciRetrieveData_CustomFields;

            // Retrieve Names only if nothing else specified
            if (iRetrieveData == ciRetrieveData.ciRetrieveData_None)
                iRetrieveData = ciRetrieveData.ciRetrieveData_Names;

            LMP.Trace.WriteNameValuePairs("iRetrieveData", iRetrieveData, "iAlerts", iAlerts);

            return iRetrieveData;
        }

        /// <summary>
        /// fills xUNID and xDisplayName with the 
        /// UNID and Display Name for the contact
        /// associated with the specified variable -
        /// assumes only one contact is present for
        /// that variable, which should always be the case
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xUNID"></param>
        /// <param name="xDisplayName"></param>
        private static void GetExistingCIContactInfo(Variable oVar, out string xUNID, out string xDisplayName)
        {
            string[] aFieldNames = { "DISPLAYNAME", "FULLNAME", "LASTNAME", "FIRSTNAME" };
            xUNID = "";
            xDisplayName = "";

            //parse existing contact info from variable value -
            string xValue = oVar.Value;
            if (!string.IsNullOrEmpty(xValue))
            {
                //there is a contact for this variable
                int iPosStart = xValue.IndexOf("UNID=\"");

                if (iPosStart > -1)
                {
                    //there is a UNID value - recipient is a CI Contact
                    iPosStart += 6;
                    int iPosEnd = oVar.Value.IndexOf(">", iPosStart);
                    xUNID = xValue.Substring(iPosStart, iPosEnd - iPosStart - 1);

                    //get name to display in CI dialog
                    foreach (string xFieldName in aFieldNames)
                    {
                        iPosStart = xValue.ToUpper().IndexOf("<" + xFieldName + " ");
                        if (iPosStart > -1)
                        {
                            //name field found - parse out value
                            iPosStart = xValue.ToUpper().IndexOf(">", iPosStart);
                            if (iPosStart > -1)
                            {
                                iPosEnd = xValue.ToUpper().IndexOf("<", iPosStart);
                                xDisplayName = xValue.Substring(iPosStart + 1, iPosEnd - iPosStart - 1);
                                break;
                            }
                        }
                    }

                }
            }
        }

        /// <summary>
        /// creates mail merge data file from selected CI contacts
        /// </summary>
        public static void CreateDataFile()
        {
            string xLabel = "Select Contacts For Merge File";

            //GLOG item #6652 - dcf - changed to writable DB directory
            string xFile = LMP.Data.Application.WritableDBDirectory + "\\mpMergeFile.txt";

            //delete existing merge temp file if necessary
            if (System.IO.File.Exists(xFile))
                System.IO.File.Delete(xFile);

            Data.Application.StartCIIfNecessary();

            ciAlerts iAlerts = ciAlerts.ciAlert_None;
            ciRetrieveData iData = ciRetrieveData.ciRetrieveData_All;

            try
            {
                ICContacts oContacts = null;

                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                //#3218 - ceh 
                //use variable display name as the label for the right side list box in the CI dialog
                oContacts = Data.Application.CISession
                    .GetContactsEx(iData, ciRetrieveData.ciRetrieveData_None,
                    ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None,
                    ICI.ciSelectionlists.ciSelectionList_To, 0, iAlerts, xLabel, null, null, null, 0, 0, 0, 0);

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                if (oContacts.Count() > 0)
                {
                    try
                    {
                        //there are contacts - create merge text file
                        oContacts.CreateDataFile(xFile);

                        //convert merge text file into a Word table file
                        LMP.Forte.MSWord.WordDoc.CreateWordDataSourceDocument(xFile);
                    }
                    finally
                    {
                    }
                }
            }
            finally
            {
                //delete merge text file
                if(System.IO.File.Exists(xFile))
                    System.IO.File.Delete(xFile);

                LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenUpdating = true;
            }
        }

        //GLOG : 8819 : ceh
        private static ICContacts GetCIContacts()
        {
            ICContacts oContacts = null;
            Assembly oAsm = null;
            string xAssemblyName = "CIO.dll";
            string xClassName = "TSG.CI.CContacts";

            string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAssemblyName;

            try
            {
                //load assembly
                oAsm = Assembly.LoadFrom(xAsmDllFullName);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
            }

            try
            {
                //create class instance
                oContacts = (ICContacts)oAsm.CreateInstance(xClassName, true);

                if (oContacts == null)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
            }

            return oContacts;

        }

    }
}
