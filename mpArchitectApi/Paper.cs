using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Paper : AdminSegment, ISingleInstanceSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //GLOG 7995 (dm) - remmed in light of Microsoft hotfix
                ////GLOG 6967 (dm) - if necessary, revert to Word 2010 compatibility mode and
                ////suppress extra space at top
                //Word.Document oDoc = this.ForteDocument.WordDocument;
                //if ((this is PleadingPaper) && (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oDoc) > 14))
                //{
                //    LMP.fWordObjects.cWord14.SetCompatibilityMode(oDoc, 14);
                //    oDoc.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = true;
                //}

                //get existing segment of this type
                Segment oExistingPaper = null;
                if (oParent == null)
                {
                    //get from location
                    Segments oSegs = Segment.GetSegments(this.ForteDocument,
                        oLocation.Sections.First, this.Definition.TypeID, 1);
                    if (oSegs.Count == 1)
                    {
                        oExistingPaper = oSegs[0];

                        //get parent of existing paper
                        oParent = oExistingPaper.Parent;

                    }
                    if (oExistingPaper == null)
                    {
                        //set parent to first document-type segment in the section
                        oSegs = Segment.GetSegments(this.ForteDocument, oLocation.Sections.First);
                        for (int i = 0; i < oSegs.Count; i++)
                        {
                            if (oSegs[i].IntendedUse == LMP.Data.mpSegmentIntendedUses.AsDocument)
                            {
                                oParent = oSegs[i];
                                break;
                            }
                        }
                    }
                    //add parent id to object data of new paper
                    if (oParent != null)
                        Segment.AddParentIDToXML(ref xXML, oParent);
                }
                else
                {
                    //get from parent
                    for (int i = 0; i < oParent.Segments.Count; i++)
                    {
                        Segment oSegment = oParent.Segments[i];
                        if (this.Definition != null && oSegment.TypeID == this.Definition.TypeID)
                        {
                            oExistingPaper = oSegment;
                            break;
                        }
                    }
                }
                List<int> aSections = new List<int>();
                //delete existing paper
                if (oExistingPaper != null)
                {
                    aSections.Add(oLocation.Sections[1].Index);
                    //GLOG 3111: Get all sections containing parts of the existing Paper
                    //for (int s = 0; s <= oExistingPaper.WordTags.Length - 1; s++)
                    if (oExistingPaper.ForteDocument.Mode == ForteDocument.Modes.Design)
                    {
                        if (oExistingPaper.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        {
                            Word.XMLNode[] aTags = oExistingPaper.WordTags;
                            for (int s = 0; s <= aTags.Length - 1; s++)
                            {
                                int iSection = aTags[s].Range.Sections[1].Index;
                                if (!aSections.Contains(iSection))
                                    aSections.Add(iSection);
                            }
                        }
                        else
                        {
                            Word.ContentControl[] aCCs = oExistingPaper.ContentControls;
                            for (int s = 0; s <= aCCs.Length - 1; s++)
                            {
                                int iSection = aCCs[s].Range.Sections[1].Index;
                                if (!aSections.Contains(iSection))
                                    aSections.Add(iSection);
                            }
                        }
                    }
                    else
                    {
                        //GLOG 6939 (dm) - mSEGs are now bookmarks at runtime
                        Word.Bookmark[] aBkmks = oExistingPaper.Bookmarks;
                        for (int s = 0; s <= aBkmks.Length - 1; s++)
                        {
                            int iSection = aBkmks[s].Range.Sections[1].Index;
                            if (!aSections.Contains(iSection))
                                aSections.Add(iSection);
                        }
                    }

                    aSections.Sort();
                    try
                    {
                        //delete
                        this.ForteDocument.DeleteSegment(oExistingPaper);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
                    }
                }
                LMP.Forte.MSWord.mpHeaderFooterInsertion iHFInsertion = 
                    LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_ReplaceSameType;

                //GLOG 6983
                mpObjectTypes iObjectType = 0;
                if (this is PleadingPaper)
                {
                    //GLOG 4471: for Pleading Paper, replace existing footers,
                    //so that any existing Page Numbering is also replaced
                    iHFInsertion = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_PleadingPaper;
                    iObjectType = mpObjectTypes.PleadingPaper;
                }
                else if (this is Letterhead)
                {
                    iObjectType = mpObjectTypes.Letterhead;
                }

                //GLOG 8541: Need to copy variables since ObjectData may have been changed by adding Parent ID
                LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(
                    this.ForteDocument.WordDocument, xXML, true);

                //GLOG 3762
                if (aSections.Count > 0)
                {
                    object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                    foreach (int iSection in aSections)
                    {
                        //GLOG 3111: Insert Paper in same sections that held previous Paper
                        Word.Range oSecRange = oLocation.Document.Sections[iSection].Range;
                        oSecRange.Collapse(ref oDirection);

                        //GLOG 6001 (dm) - the paper in each section needs unique tags
                        if (iSection > aSections[0])
                        {
                            if ((this.ForteDocument.Mode != ForteDocument.Modes.Design) &&
                                LMP.Conversion.IsPreconvertedXML(xXML))
                            {
                                LMP.Forte.MSWord.WordDoc.RegenerateDocVarIDs(
                                    ref xXML, this.ForteDocument.WordDocument);
                            }
                            LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(
                                this.ForteDocument.WordDocument, xXML, true);
                        }

                        //insert new paper
                        Segment.InsertXML(xXML, oSecRange, iHFInsertion, //GLOG 4471
                            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                            iIntendedUse, iObjectType, this.ForteDocument); //GLOG 6983
                    }
                }
                else
                {
                    //insert new paper
                    Segment.InsertXML(xXML, oLocation, iHFInsertion, //GLOG 4471
                        LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                        iIntendedUse, iObjectType, this.ForteDocument); //GLOG 6983
                }

                if (this is PleadingPaper)
                {
                    LMP.Forte.MSWord.PleadingPaper oCOM = new LMP.Forte.MSWord.PleadingPaper();
                    Word.Section oSection = oLocation.Sections.First;
                    if (oParent is LMP.Architect.Base.ILitigationAddOnSegment)
                        //adjust TOA/Exhibits section
                        oCOM.AdjustLitigationAddOnSection(oSection, (oParent is TOA));
                    else
                        //adjust TOC section
                        oCOM.AdjustTOCSection(oSection);

                    //GLOG : 6967 : CEH
                    //reset compatibility option - no need with new format
                    //if (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(this.ForteDocument.WordDocument) < 15)
                        //this.ForteDocument.WordDocument.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = false;
                    

                }

                //update variable that holds id of paper
                if (oParent != null)
                {
                    // Refresh Parent Segment in case any Header/Footer mSegs were deleted
                    oParent.Refresh();
                    Variable oVar = null;
                    try
                    {
                        //JTS 12/19/08: First look for type-individualized variable,
                        //e.g., MemoLetterheadID
                        oVar = oParent.Variables.ItemFromName(
                            oParent.TypeID.ToString() + this.Definition.TypeID.ToString() + "ID");
                    }
                    catch { }
                    if (oVar == null)
                    {
                        try
                        {
                            oVar = oParent.Variables.ItemFromName(
                                this.Definition.TypeID.ToString() + "ID");
                        }
                        catch { }
                    }
                    try
                    {
                        
                        if (oVar != null)
                            oVar.SetValue(this.Definition.ID.ToString(), false);
                    }
                    catch { }
                }

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        public void DeleteExistingInstance(Word.Section oSection)
        {
        }
        public Segments GetExistingSegments(Word.Section oSection)
        {
            return null;
        }
    }
}
