using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace LMP.Architect.Api
{
    public class Prefill: Base.Prefill
    {
        #region *********************constructors*********************
        public Prefill()
        {
        }
        /// <summary>
        /// creates a prefill populated with values 
        /// from the variables of the specified segment
        /// </summary>
        /// <param name="oVariables"></param>
        public Prefill(Segment oSegment)
        {
            CreatePrefill(oSegment);
        }
        /// <summary>
        /// Creates a prefill populated with values
        /// parsed from name/value pairs in the specified string
        /// </summary>
        /// <param name="xContentString"></param>
        public Prefill(string xContentString, string xName, string xSegmentID)
        {
            CreatePrefill(xContentString, xName, xSegmentID, null);
        }
        public Prefill(string xContentString, string xName, string xSegmentID, string xVariableSetID)
        {
            CreatePrefill(xContentString, xName, xSegmentID, xVariableSetID);
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// adds all the variable values 
        /// in the specified segment to the prefill
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xBasePath"></param>
        /// <summary>
        /// Populate name/value pairs by parsing input string
        /// </summary>
        /// <param name="xContentString"></param>
        private void AddSegmentValues(Segment oSegment, string xBasePath)
        {
            if (oSegment.TypeID == LMP.Data.mpObjectTypes.Architect ||
                oSegment.TypeID == LMP.Data.mpObjectTypes.MasterData)
                xBasePath = "";
            else if (!string.IsNullOrEmpty(xBasePath))
            {
                if (!xBasePath.EndsWith("."))
                    xBasePath += ".";
            }

            //cycle through all child segments,
            //adding the variable values from each
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                // For child segments, relative path is included with variable name, in format 'path.var'
                Segment oChild = oSegment.Segments[i];

                string xRelativePath = null;

                xRelativePath = String.RemoveIllegalNameChars(oChild.DisplayName) + ".";

                if (!string.IsNullOrEmpty(xBasePath))
                {
                    if (!xBasePath.EndsWith("."))
                        xBasePath += ".";

                    // Append current child path to end of existing path
                    xRelativePath = xBasePath + xRelativePath;
                }

                ////if segment is deactivated, update deactivation data, prompting
                ////user to resolve any discrepancies with tag values (5/19/11 dm)
                //if (oSegment.Activated && !oChild.Activated)
                //    oChild.ReviewData();

                AddSegmentValues(oChild, xRelativePath);
            }

            //GLOG item #6293 - dcf -
            //use snapshot for finished components, and 
            //segment values for unfinished components
            if (oSegment.IsFinished)
            {
                this.LoadValues(oSegment.Snapshot.ToPrefill().ToString());
            }
            else
            {
                //cycle through all variables in the segment,
                //adding the variable values from each
                for (int i = 0; i < oSegment.Variables.Count; i++)
                {
                    Variable oVar = oSegment.Variables[i];
                    //GLOG 3493: If Value Source Expression uses [DateFormat] fieldcode
                    //save the format, even if current Value is a manually-entered date
                    string xValue = oVar.Value;

                    //GLOG 7965 (dm) - replace shift-return placeholders used by TOC -
                    //these may be in the value because the last refresh occurred
                    //during TOC pleading paper insertion
                    xValue = xValue.Replace("ZZMPSHIFTRETURN", "\v");
                    xValue = xValue.Replace("zzmpShiftReturn", "\v");

                    //GLOG 5894: If Variable takes value from CI, store CI Detail Token String in Prefill
                    if ((oVar.CIContactType != 0 || oVar.CIEntityName != "") && 
                        xValue.Contains("UNID=") && oVar.CIDetailTokenString != "")
                    {
                        xValue = xValue + "<CIDetailTokenString>" + oVar.CIDetailTokenString + "</CIDetailTokenString>";
                    }
                    if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT_"))
                    {
                        DateTime oDT = DateTime.MinValue;
                        if (DateTime.TryParse(xValue, out oDT))
                            //Value is Date Text, convert to format string
                            xValue = LMP.Architect.Base.Application.GetDateFormatPattern(xValue);
                    }
                    if (m_oValues[xBasePath + oVar.Name] != null)
                    {
                        //key already exists - cycle through
                        //counter finding first available index
                        for (int j = 1; true; j++)
                        {
                            if (m_oValues[xBasePath + j.ToString() + oVar.Name] == null)
                            {
                                //index is available
                                m_oValues.Set(xBasePath + j.ToString() + "." + oVar.Name, xValue);
                                break;
                            }
                        }
                    }
                    else
                        m_oValues.Set(xBasePath + oVar.Name, xValue);
                }
            }

            //GLOG item #34384 - save segment language
            m_oValues.Set("Culture", oSegment.Culture.ToString());
            
            //Also save segment authors for top-level only
            //GLOG item #6453 - dcf - replaced xBasePath = "" with oSegment.Parent == null
            //GLOG 6453 (8/8/12, dm) - replaced oSegment.Parent == null condition
            //the intent is to not set the authors in the child portion of the parent's
            //prefill, not to never set the authors in the prefill of segments with parents
            if (xBasePath.EndsWith("."))
                xBasePath = xBasePath.Remove(xBasePath.Length - 1);
            if ((xBasePath.IndexOf('.') == -1) && (oSegment.Authors.Count > 0))
            {
                string xAuthorIDs = "";
                for (short i = 1; i <= oSegment.Authors.Count; i++)
                {
                    LMP.Data.Author oAuthor = oSegment.Authors.ItemFromIndex(i);
                    string xLeadAuthor = oSegment.Authors.GetLeadAuthor().ID;
                    //Save ID, Is LeadAuthor, FullName and BarID
                    xAuthorIDs = xAuthorIDs + oAuthor.ID + 
                        mpPrefillAuthorsValueSeparator + (oAuthor.ID == xLeadAuthor ? "-1" : "0") + 
                        mpPrefillAuthorsValueSeparator + oAuthor.FullName +
                        mpPrefillAuthorsValueSeparator + oAuthor.BarID + mpPrefillAuthorsSeparator.ToString();
                }
                m_oValues.Set("Authors", xAuthorIDs);
            }
            //Save top-level segment court levels
            //GLOG 6453 (dm) - same adjustment is required here
            if ((xBasePath.IndexOf('.') == -1) && oSegment.LevelChooserRequired())
            {
                AdminSegment oASeg = (AdminSegment)oSegment;
                m_oValues.Set("L0", oASeg.L0.ToString());
                m_oValues.Set("L1", oASeg.L1.ToString());
                m_oValues.Set("L2", oASeg.L2.ToString());
                m_oValues.Set("L3", oASeg.L3.ToString());
                m_oValues.Set("L4", oASeg.L4.ToString());
            }
        }
        private void CreatePrefill(Segment oSegment)
        {
            this.Name = oSegment.DisplayName;
            //GLOG 3475
            this.SegmentID = oSegment.ID;

            ////if segment is deactivated, update deactivation data, prompting
            ////user to resolve any discrepancies with tag values (5/19/11 dm)
            //if (!oSegment.Activated)
            //    oSegment.ReviewData();

            //GLOG item #6293 - dcf -
            //removed the condition below to account for
            //the situation where a finished segment contains
            //unfinished children - in this case, the prefill
            //will use the snapshot for finished components and
            //the segment for unfinished components
            //if (Snapshot.Exists(oSegment))
            //{
                //this.LoadValues(oSegment.Snapshot.ToPrefill().ToString());
            //}
            //else
            //{
                AddSegmentValues(oSegment, oSegment.TagID);
            //}
        }
        #endregion
    }
}
