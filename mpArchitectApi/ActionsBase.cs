using System;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using System.Text;

namespace LMP.Architect.Api
{
	/// <summary>
	/// defines the abstract ActionBase class, from which
	/// VariableAction, ControlAction, and SegmentAction are derived
	/// </summary>
	public abstract class ActionBase : LMP.Architect.Base.ActionBase
	{
        protected Word.Document m_oDocument;

        protected ForteDocument m_oForteDocument;
        protected Segment m_oSegment;
        protected const int mpNoDeleteScopeLocationException = -2146823664;
        #region *********************methods*********************
        ///// <summary>
        ///// returns the action as an array
        ///// </summary>
        ///// <returns></returns>
        //public abstract string[] ToArray(bool bIncludeID);
        ///// <summary>
        ///// returns the action as an array
        ///// </summary>
        ///// <returns></returns>
        //public string[] ToArray()
        //{
        //    return this.ToArray(false);
        //}
			
        ///// <summary>
        ///// sets the ID of the action to a new ID
        ///// </summary>
        //internal void SetID()
        //{
        //    if(m_xID == "")
        //        m_xID = System.Guid.NewGuid().ToString();
        //}

        ///// <summary>
        ///// sets the ID of the action to the specified ID
        ///// </summary>
        ///// <param name="ID"></param>
        //internal void SetID(string ID)
        //{
        //    m_xID = ID;
        //}

        ///// <summary>
        ///// returns the action as a delimited string
        ///// </summary>
        ///// <returns></returns>
        //public override string ToString()
        //{
        //    return string.Join("�", (string[]) this.ToArray());
        //}

        ///// <summary>
        ///// executes the action
        ///// </summary>
        //public abstract void Execute();

        ///// <summary>
        ///// returns true iff execution condition is met or if there is no execution condition
        ///// </summary>
        ///// <returns></returns>
        //public abstract bool ExecutionIsSpecified();

        /// <summary>
        /// executes the specified VBA command string on the specified bookmark
        /// </summary>
        /// <param name="xParameters">bookmark, command string</param>
        protected override void ExecuteOnBookmark(string xParameters)
        {
            string xBookmark = "";
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xBookmark = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xCommandString = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xBookmark", xBookmark, "xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");
            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;

            m_oForteDocument.SetDynamicEditingEnvironment();
            //GLOG 7697: Disable XML Events during Execution
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            try
            {
                LMP.Forte.MSWord.WordDoc.ExecuteOnBookmark(m_oDocument, xBookmark, xCommandString);
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Bookmark - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
                //GLOG 7697: Restore XML Events state
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
        }

        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Word tags of the specified Block
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected override void ExecuteOnBlock(string xParameters)
        {
            Block oBlock = null;
            string xCommandString = "";
            string xName = "";
            string xBlockName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xName = aParams[1];
                    //GLOG 6266: Support comma-delimited list of Block names or Wildcard patterns
                    aNames = xName.Split(',');
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
                int iShowTags = oView.ShowXMLMarkup;
                m_oForteDocument.SetDynamicEditingEnvironment();
                try
                {
                    foreach (string xItem in aNames)
                    {
                        string[] aBlocks;
                        //GLOG 6266: Support * wildcard for Block names
                        if (xItem.Contains("*"))
                        {
                            string xMatchingBlocks = "";
                            //Check only Blocks that are not currently deleted and match name
                            string xRegExp = @"//Node[(@ElementName='mBlock')][contains(@TagID, '" + xItem.Replace("*", "") + "')]";
                            ReadOnlyNodeStore oBlocks = m_oSegment.Nodes.GetNodes(xRegExp);
                            for (int b = 0; b < oBlocks.Count; b++)
                            {
                                string xBlockTagID = oBlocks[b].Attributes["TagID"].Value;
                                xBlockTagID = xBlockTagID.Substring(xBlockTagID.LastIndexOf(".") + 1);
                                xMatchingBlocks = xMatchingBlocks + xBlockTagID + ",";
                            }
                            xMatchingBlocks = xMatchingBlocks.TrimEnd(',');
                            aBlocks = xMatchingBlocks.Split(',');
                        }
                        else
                        {
                            aBlocks = new string[] { xItem };
                        }
                        foreach (string xVar in aBlocks)
                        {
                            xBlockName = Expression.Evaluate(xVar, m_oSegment, m_oForteDocument);
                            if (xBlockName != "" && xCommandString != "")
                            {
                                //get the specified block - we'll
                                //be acting on this block
                                oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
                            }
                            else
                            {
                                //one of the parameters is invalid
                                throw new LMP.Exceptions.ArgumentException(
                                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                                    xParameters);
                            }
                            Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name,
                                "xCommandString", xCommandString, "xBlockName", xBlockName);

                            //replace "^=" with "=" -
                            //this will probably be a common mistake,
                            //so we'll just convert it without alerting
                            //the user
                            xCommandString = xCommandString.Replace("^=", "=");

                            //10.2 - JTS 3/29/10
                            bool bContentControls = (m_oForteDocument.FileFormat ==
                                LMP.Data.mpFileFormats.OpenXML);
                            Word.Range oRng;
                            if (bContentControls)
                            {
                                oRng = oBlock.AssociatedContentControl.Range;
                            }
                            else
                            {
                                //select entire tag - this guarantees
                                //that the action will target the entire tag
                                //tag even if there is no text yet in the tag
                                oView.ShowXMLMarkup = -1;
                                oRng = oBlock.AssociatedWordTag.Range;
                            }
                            //GLOG 7697: Disable XML Events during Execution
                            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            try
                            {
                                LMP.Forte.MSWord.WordDoc.ExecuteOnRange(oRng, xCommandString);
                            }
                            catch
                            {
                                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                                    "Error_CouldNotExecuteVariableAction") + "Execute On Block - " + this.Parameters);
                            }
                            finally
                            {
                                //GLOG 7697: Restore XML Events state
                                ForteDocument.IgnoreWordXMLEvents = iIgnore;
                            }
                        }
                    }
                }
                finally
                {
                    //hide tags if these were hidden before running this action
                    if (iShowTags == 0)
                        oView.ShowXMLMarkup = 0;

                }
            }

        }
        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Word tags of the specified Variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected void ExecuteOnTag(Variable oVar, string xParameters)
        {
            string xCommandString = "";
            string xVariableName = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xVariableName = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("oVar.Name", oVar.Name,
                "xCommandString", xCommandString, "xVariableName", xVariableName);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            //10.2 - JTS 3/29/10
            bool bContentControls = (m_oForteDocument.FileFormat ==
                LMP.Data.mpFileFormats.OpenXML);

            m_oForteDocument.SetDynamicEditingEnvironment();
            //GLOG 7697: Disable XML Events during Execution
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            if (xVariableName != "")
            {
                //get the specified variable - we'll
                //be acting on this variable
                oVar = m_oSegment.Variables.ItemFromName(xVariableName);
            }
            if (bContentControls)
            {
                try
                {
                    foreach (Word.ContentControl oCC in oVar.AssociatedContentControls)
                    {
                        Word.Range oRng = oCC.Range;
                        LMP.Forte.MSWord.WordDoc.ExecuteOnRange(oRng, xCommandString);
                    }
                }
                catch
                {
                    throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                        "Error_CouldNotExecuteVariableAction") + "Execute On Content Control - " + this.Parameters);
                }
                finally
                {
                    //GLOG 7697: Restore XML Events state
                    ForteDocument.IgnoreWordXMLEvents = iIgnore;
                }
            }
            else
            {
                Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
                int iShowTags = oView.ShowXMLMarkup;
                try
                {
                    if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        foreach (Word.XMLNode oNode in oVar.AssociatedWordTags)
                        {
                            //select entire tag - this guarantees
                            //that the action will target the entire tag
                            //tag even if there is no text yet in the tag
                            oView.ShowXMLMarkup = -1;
                            Word.Range oRng = oNode.Range;
                            LMP.Forte.MSWord.WordDoc.ExecuteOnRange(oRng, xCommandString);
                        }
                    }
                    else
                    {
                        foreach (Word.ContentControl oCC in oVar.AssociatedContentControls)
                        {
                            //select entire tag - this guarantees
                            //that the action will target the entire tag
                            //tag even if there is no text yet in the tag
                            oView.ShowXMLMarkup = -1;
                            Word.Range oRng = oCC.Range;
                            LMP.Forte.MSWord.WordDoc.ExecuteOnRange(oRng, xCommandString);
                        }
                    }
                }
                catch
                {
                    throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                        "Error_CouldNotExecuteVariableAction") + "Execute On Tag - " + this.Parameters);
                }
                finally
                {
                    //hide tags if these were hidden before running this action
                    if (iShowTags == 0)
                        oView.ShowXMLMarkup = 0;
                    //GLOG 7697: Restore XML Events state
                    ForteDocument.IgnoreWordXMLEvents = iIgnore;
                }
            }
        }
        /// <summary>
        /// GLOG 7229: executes the specified VBA command string 
        /// on the primary range of the specified Segment(s)
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected override void ExecuteOnSegment(string xParameters)
        {
            string xCommandString = "";
            string xSegmentID = "";
            Segment[] aSegments = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xSegmentID = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xCommandString", xCommandString, "xSegmentID", xSegmentID);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            if (xSegmentID != "")
            {
                if (aParams[1].ToUpper().Contains("CHILD"))
                {
                    //Target Segment is a child of this segment
                    aSegments = Segment.FindSegments(xSegmentID, m_oSegment.Segments);
                }
                else
                {
                    //Search entire document for matching segment
                    aSegments = Segment.FindSegments(xSegmentID, m_oForteDocument.Segments);
                }
            }
            else
            {
                aSegments = new Segment[] { m_oSegment };
            }
            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            try
            {
                foreach (Segment oSegment in aSegments)
                {
                    Word.Range oRng = oSegment.PrimaryRange;
                    LMP.Forte.MSWord.WordDoc.ExecuteOnRange(oRng, xCommandString);
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Segment - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
        }
        /// <summary>
        /// executes the specified VBA command string on the document
        /// </summary>
        /// <param name="xParameters">command string</param>
        protected override void ExecuteOnDocument(string xParameters)
        {
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();
            //GLOG 7697: Disable XML Events during Execution
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            try
            {
                LMP.Forte.MSWord.WordDoc.ExecuteWordCommands(m_oDocument, xCommandString);
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Document - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
                //GLOG 7697: Restore XML Events state
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
        }

        /// <summary>
        /// executes the specified VBA command string on the Word application
        /// </summary>
        /// <param name="xParameters">command string</param>
        protected override void ExecuteOnApplication(string xParameters)
        {
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();
            //GLOG 7697: Disable XML Events during Execution
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            try
            {
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                LMP.Forte.MSWord.WordDoc.ExecuteWordCommands(LMP.Architect.Api.Application.CurrentWordApp, xCommandString);
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Application - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
        }

        /// <summary>
        /// replaces all instances of the specified
        /// segment with the specified segment
        /// </summary>
        /// <param name="xParameters"></param>
        protected override void ReplaceSegment(string xParameters)
        {

            //GLOG 6687:  Don't ever run this action in Design Mode
            if (m_oForteDocument.Mode == ForteDocument.Modes.Design)
                return;

            string xExistingSegmentID = null;
            string xNewSegmentID = null;
            bool bUsePrefill = false;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            //GLOG 3247: 3 parameters are supported, but still allow 2 for older content
            if (aParams.Length < 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xExistingSegmentID = Expression.Evaluate(
                        aParams[0], m_oSegment, m_oForteDocument);
                    xNewSegmentID = Expression.Evaluate(
                        aParams[1], m_oSegment, m_oForteDocument);
                    if (aParams.Length > 2)
                        bool.TryParse(Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument), out bUsePrefill);
                } 
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xExistingSegmentID", xExistingSegmentID, 
                "xNewSegmentID", xNewSegmentID);

            //GLOG 3499 - strip fixed admin segment id2 before comparing ids
            xExistingSegmentID = xExistingSegmentID.Replace(".0", "");
            xNewSegmentID = xNewSegmentID.Replace(".0", "");

            if (xExistingSegmentID == xNewSegmentID)
                //the new segment is the same as the old segment -
                //replacement is superfluous
                return;

            //GLOG 3719 (dm) - added support for comma delimited list of ids and
            //specific handling for switching psigs between table and non-table format
            xExistingSegmentID = xExistingSegmentID.Replace(" ", "");
            string[] aExistingIDs = xExistingSegmentID.Split(',');
            xNewSegmentID = xNewSegmentID.Replace(" ", "");
            string[] aNewIDs = xNewSegmentID.Split(',');
            if (aExistingIDs.Length != aNewIDs.Length)
            {
                //one-to-one mapping is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters);
            }

            //get the object type of first segment on each list
            LMP.Data.mpObjectTypes oExistingType =
                LMP.Data.Application.GetSegmentTypeID(System.Int32.Parse(aExistingIDs[0]));
            LMP.Data.mpObjectTypes oNewType =
                LMP.Data.Application.GetSegmentTypeID(System.Int32.Parse(aNewIDs[0]));
            if (((oExistingType == LMP.Data.mpObjectTypes.PleadingSignature) &&
                (oNewType == LMP.Data.mpObjectTypes.PleadingSignatureNonTable)) ||
                ((oExistingType == LMP.Data.mpObjectTypes.PleadingSignatureNonTable) &&
                (oNewType == LMP.Data.mpObjectTypes.PleadingSignature)))
            {
                //call dedicated method to switch between table and non-table format
                this.ChangePleadingSignatureFormat((oExistingType ==
                    LMP.Data.mpObjectTypes.PleadingSignatureNonTable), aExistingIDs,
                    aNewIDs, bUsePrefill);
                return;
            }

            m_oForteDocument.SetDynamicEditingEnvironment();

            for (int i = 0; i < aExistingIDs.Length; i++)
            {
                //GLOG 3564: Get Parent of matching segment for use with Replace
                //This allows action to work on Segments that are not a child of this segment
                Segment oParent = null;
                Prefill oPrefill = null;
                Segment[] aSegments;
                //Since Trailer and DraftStamp won't have a Parent Segment
                //set search scope to entire document, otherwise limit to children of current Segment
                if (oExistingType == LMP.Data.mpObjectTypes.Trailer || 
                    oExistingType == LMP.Data.mpObjectTypes.DraftStamp)
                {
                    aSegments = Segment.FindSegments(aExistingIDs[i], m_oForteDocument.Segments);
                }
                else
                {
                    aSegments = Segment.FindSegments(aExistingIDs[i], m_oSegment.Segments);
                }
                if (aSegments.Length > 0)
                {
                    if (bUsePrefill)
                    {
                        //GLOG 3247: Get existing values to prefill new segment
                        oPrefill = new Prefill(aSegments[0]);
                    }
                    oParent = aSegments[0].Parent;
                }
                //replace the segment with the new one
                Segment.Replace(aNewIDs[i], aExistingIDs[i], oParent, m_oForteDocument, oPrefill, null, false);

                //GLOG 4845: This is no longer necessary, because Replace should preserve index
                ////GLOG 4519: If replacing Collection item, Refresh ForteDocument so that editor tree
                ////reflects correct order of segments in document
                //if (oParent != null && oParent.IsTransparent) //GLOG 4584
                //    m_oForteDocument.Refresh(ForteDocument.RefreshOptions.None, false);

                //notify UI that containing Segment has been refreshed
                m_oForteDocument.RaiseSegmentRefreshedByActionEvent(oParent);
            }
        }

        /// <summary>
        /// sets the value of another variable belonging to this segment
        /// or a related segment
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xVariableName, xExp</param>
        protected void SetVariableValue(string xValue, string xParameters)
        {
            //GLOG 6414 (dm) - skip during child structure insertion
            if (m_oForteDocument.ChildStructureInsertionInProgess)
                return;

            string xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName",
                xName, "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //get target segment(s)
            Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xName);
            if (oTargets != null)
            {
                foreach (Segment oTarget in oTargets)
                {
                    //get target variable
                    Variable oVar = oTarget.GetVariable(xName);

                    //if variable exists, set value
                    if (oVar != null)
                    {
                        if (oVar.Value != xValue)
                        {
                            oVar.SetValue(xValue);
                            //GLOG 3144: Handle special caption borders, since Editor event not raised here
                            //GLOG 4282: Run for any Collection Table item
                            if (oVar.Segment is CollectionTableItem)
                                oVar.Segment.UpdateTableBorders();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// runs the specified macro
        /// </summary>
        /// <param name="xParameters">xMacroName, xArgs</param>
        protected override void RunMacro(string xParameters)
        {
            string xMacroName = "";
            string xArgs = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);

            try
            {
                //get parameters
                xMacroName = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                xArgs = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
            }

            if (xMacroName == "")
                //alert - macro name is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);

            Trace.WriteNameValuePairs("xMacroName", xMacroName, "xArgs", xArgs);
            //4522: Disable XML events during macro execution, since macro might
            //be changing tags in the document
            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents =
                ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            try
            {
                //run macro
                LMP.Forte.MSWord.WordApp.CallMacro(
                    xMacroName, xArgs, LMP.StringArray.mpEndOfElement.ToString());
                //make sure any tag changes made by macro are reflected
                //GLOG 6687: Make sure Tag Prefix ID isn't included in Design XML
                m_oForteDocument.RefreshTags(m_oForteDocument.Mode != ForteDocument.Modes.Design);
                m_oSegment.RefreshNodes();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
            }
        }

        /// <summary>
        /// runs the specified method
        /// </summary>
        /// <param name="xParameters"></param>
        protected override void RunMethod(string xParameters)
        {
            mpMethodTypes iType = mpMethodTypes.NET;
            string xServer = "";
            string xClass = "";
            string xMethodName = "";
            string xArgs = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 5)
                //5 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);

            try
            {
                //get parameters
                iType = (mpMethodTypes)Int32.Parse(Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument));
                xServer = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                xClass = Expression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);
                xMethodName = Expression.Evaluate(aParams[3], m_oSegment, m_oForteDocument);
                xArgs = Expression.Evaluate(aParams[4], m_oSegment, m_oForteDocument);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
            }

            if (iType == mpMethodTypes.COM)
            {
                //execute COM method
                if (xMethodName == "")
                    //alert - method name is required
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);

                Trace.WriteNameValuePairs("xServer", xServer, "xClass", xClass,
                    "xMethodName", xMethodName, "xArgs", xArgs);

                //call method - get object containing the method
                if (xServer != "" && xClass != "")
                {
                    //server and class provided - get object from these values
                    LMP.Reflection.InvokeCOMMethod(xServer, xClass, xMethodName,
                        xArgs, LMP.StringArray.mpEndOfElement.ToString());
                }
                else
                {
                    //server and class must either both be empty
                    //or both filled - alert that this is not the case
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
                }
            }
            else
            {
                //execute .NET method
                if (xServer == "" && xClass == "")
                {
                    //no server and class provided - assume the method is
                    //a member of the segment object
                    LMP.Reflection.InvokeMethod(m_oSegment, xMethodName,
                        xArgs, LMP.StringArray.mpEndOfElement.ToString());
                }
                else
                {
                    //TODO: execute method in specified assembly and class
                    throw new LMP.Exceptions.NotImplementedException("Not Implemented yet.");
                }
            }
        }

        /// <summary>
        /// sets Word document variable
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected override void SetDocVarValue(string xValue, string xParameters)
        {
            object xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            //raise error if variable name is empty
            if (xName.ToString() == "")
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //set variable - if doesn't exist, Word will create it
            m_oDocument.Variables.get_Item(ref xName).Value = xValue;
        }

        /// <summary>
        /// sets Word custom document property
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected override void SetDocPropValue(string xValue, string xParameters)
        {
            //GLOG 7495: Set Built-in or Custom Document Property
            string xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            //raise error if variable name is empty
            if (xName.ToString() == "")
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            if (xValue.Length > 255)
            {
                //Text Doc Properties allow maximum of 255 characters
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }
            Microsoft.Office.Core.DocumentProperty oProp = null;
            Microsoft.Office.Core.DocumentProperties oProps = null;

            oProps = (Microsoft.Office.Core.DocumentProperties)m_oDocument.BuiltInDocumentProperties;
            try
            {
                oProp = oProps[xName];
            }
            catch { }
            if (oProp != null)
            {
                //Built-in property
                try
                {
                    oProp.Value = xValue;
                }
                catch
                {
                    //error writing to built-in property - some of these are read-only
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
                }
            }
            else
            {
                //Custom Document Property
                oProps = (Microsoft.Office.Core.DocumentProperties)m_oDocument.CustomDocumentProperties;
                try
                {
                    oProp = oProps[xName];
                }
                catch
                {
                }
                if (oProp == null)
                {
                    oProps.Add(xName, false, Microsoft.Office.Core.MsoDocProperties.msoPropertyTypeString, (object)xValue, null);
                }
                else
                {
                    oProp.Value = xValue;
                }
            }

        }

        /// <summary>
        /// sets preference or user application setting for user or lead author
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">key name, key type, expression</param>
        protected override void SetAsDefaultValue(string xValue, string xParameters)
        {
            string xName = "";
            LMP.Data.mpKeySetTypes iType = LMP.Data.mpKeySetTypes.UserTypePref;
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 3)
                //3 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    iType = (LMP.Data.mpKeySetTypes)System.Convert.ToByte
                        (Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    xExpression = aParams[2];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            if (iType == LMP.Data.mpKeySetTypes.FirmApp)
            {
                //firm app settings are read-only
                throw new LMP.Exceptions.KeySetException(LMP.Resources
                    .GetLangString("Error_FirmAppKeyReadOnly"));
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "iType", iType, "xExpression", xExpression);

            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //entity is user or lead author
            string xEntityID = "";
            if (iType == LMP.Data.mpKeySetTypes.AuthorPref)
                try
                {
                    //get lead author id
                    xEntityID = m_oSegment.Authors.GetLeadAuthor().ID;
                }
                catch
                {
                    //no lead author
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
                }
            else
                //get user id
                xEntityID = LMP.Data.LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);

            //set scope based on type
            int iScope = 0;
            if ((iType == LMP.Data.mpKeySetTypes.AuthorPref) ||
                (iType == LMP.Data.mpKeySetTypes.UserObjectPref))
                //TODO: this is temporary only - we need to implement
                //this for user segments as well
                //TODO: implement for 2-field scope id
                iScope = m_oSegment.ID1;
            else if (iType == LMP.Data.mpKeySetTypes.UserTypePref)
                iScope = (int)m_oSegment.TypeID;

            //set key value
            LMP.Data.KeySet.SetKeyValue(xName, xValue, iType, iScope.ToString(),
                xEntityID, 0, m_oSegment.Culture);
        }

        /// <summary>
        /// executes the specified VBA command string on the specified page setup object -
        /// if iSection = -1, applies to entire document; if 0, applies to the last section
        /// </summary>
        /// <param name="xParameters">section index, command string</param>
        protected void ExecuteOnPageSetup(string xParameters)
        {
            short shSection = 0;
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    shSection = short.Parse(aParams[0]);
                    xCommandString = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("shSection", shSection,
                "xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();
            short shStartSection = shSection;
            short shEndSection = shSection;
            if (shSection == 0)
            {
                //Execute on all sections in Parent Segment
                shStartSection = (short)m_oSegment.PrimaryRange.Sections.First.Index;
                shEndSection = (short)m_oSegment.PrimaryRange.Sections.Last.Index;
            }
            try
            {
                for (short sec = shStartSection; sec <= shEndSection; sec++)
                    LMP.Forte.MSWord.WordDoc.ExecuteOnPageSetup(m_oDocument, sec, xCommandString);
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Page Setup - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
            }
        }

        /// <summary>
        /// executes the specified command on the specified style
        /// </summary>
        /// <param name="xParameters">style name, command string</param>
        protected void ExecuteOnStyle(string xParameters)
        {
            string xStyle = "";
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);

            try
            {
                //get parameters
                xStyle = Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                xCommandString = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters, oE);
            }

            Trace.WriteNameValuePairs("xStyle", xStyle, "xCommandString", xCommandString);

            if (xStyle == "" || xCommandString == "")
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters);
            }

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();
            try
            {
                LMP.Forte.MSWord.WordDoc.ExecuteOnStyle(m_oDocument, xStyle, xCommandString);
            }
            catch(System.Exception)
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Style - "+ this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
            }
        }
        /// <summary>
        /// runs the variable actions of another variable belonging to this segment
        /// or a related segment
        /// </summary>
        /// <param name="xParameters">xVariableName</param>
        protected void RunVariableActions(string xParameters)
        {
            string xName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Variable names
                    aNames = xName.Split(',');
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xName", xName);

            foreach (string xVar in aNames)
            {
                string xVarRef = xVar;
                //get target segment(s)
                Segment[] oTargets = m_oSegment.GetReferenceTargets(ref xVarRef);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        Variable oVar = oTarget.GetVariable(xVarRef.Trim());

                        //if variable exists, run actions
                        if (oVar != null)
                            oVar.VariableActions.Execute();
                    }
                }
            }
        }

        protected void SetPaperSource(string xValue, string xParameters){

            string xSetPageOne = "";
            string xSetOtherPages = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xSetPageOne = aParams[0];
                    xSetOtherPages = aParams[1];
                    //GLOG 4752: Added parameter for Expression
                    if (aParams.Length > 2)
                        xExpression = aParams[2];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xSetPageOne", xSetPageOne, "xSetOtherPages", xSetOtherPages, "xExpression", xExpression);

            //GLOG 4752
            if (xExpression != "")
                xValue = Expression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            short shTrayID = short.Parse(xValue);

            if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                Word.XMLNode[] aNodes = m_oSegment.WordTags;

                if (aNodes.Length > 0)
                {
                    if (xSetPageOne.ToUpper() == "TRUE" && xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource(aNodes, shTrayID, shTrayID);
                    else if (xSetPageOne.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource(aNodes, shTrayID, -1);
                    else if (xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource(aNodes, -1, shTrayID);
                }
                else
                {
                    //get segment bookmarks
                    if (xSetPageOne.ToUpper() == "TRUE" && xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, shTrayID, shTrayID);
                    else if (xSetPageOne.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, shTrayID, -1);
                    else if (xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, -1, shTrayID);
                }
            }
            else
            {
                Word.ContentControl[] aCCs = m_oSegment.ContentControls;

                if (aCCs.Length > 0)
                {
                    if (xSetPageOne.ToUpper() == "TRUE" && xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_CC(aCCs, shTrayID, shTrayID);
                    else if (xSetPageOne.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_CC(aCCs, shTrayID, -1);
                    else if (xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_CC(aCCs, -1, shTrayID);
                }
                else
                {
                    //get segment bookmarks
                    if (xSetPageOne.ToUpper() == "TRUE" && xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, shTrayID, shTrayID);
                    else if (xSetPageOne.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, shTrayID, -1);
                    else if (xSetOtherPages.ToUpper() == "TRUE")
                        LMP.Forte.MSWord.WordDoc.SetPaperSource_Bmk(m_oSegment.Bookmarks, -1, shTrayID);
                }
            }
        }
        /// <summary>
        /// Formats all tags for the specified Variable
        /// to Add/Remove Underlining to length of longest line
        /// </summary>
        /// <param name="oSourceVar"></param>
        /// <param name="xParameters"></param>
        protected void UnderlineTagToLongest(Variable oSourceVar, string xParameters)
        {
            bool bUnderline;
            string xVariableName = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    bUnderline = bool.Parse(Expression.Evaluate(aParams[0], m_oSegment, m_oForteDocument));
                    xVariableName = Expression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("oSourceVar.Name", oSourceVar.Name,
                "bUnderline", bUnderline, "xVariableName", xVariableName);

            Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            int iShowTags = oView.ShowXMLMarkup;
            m_oForteDocument.SetDynamicEditingEnvironment();

            try
            {

                //GLOG 3583: disable event handlers
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                if (xVariableName != "")
                {
                    Segment[] oSegments = m_oSegment.GetReferenceTargets(ref xVariableName);
                    if (oSegments.Length > 0 && xVariableName != "")
                    {
                        foreach (Segment oTarget in oSegments)
                        {
                            //get target variable
                            Variable oVar = oTarget.GetVariable(xVariableName);
                            //if variable exists, run action
                            if (oVar != null && oVar.Value != "" && !oVar.IsTagless
                                && m_oForteDocument.Mode != ForteDocument.Modes.Design)
                            {
                                if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                {
                                    Word.XMLNode[] oNodes = oVar.AssociatedWordTags;
                                    for (int i = 0; i < oNodes.Length; i++)
                                    {
                                        Word.Range oRng = oNodes[i].Range;
                                        LMP.Forte.MSWord.WordDoc.UnderlineRangeToLongestLine(oRng, bUnderline);
                                    }
                                }
                                else
                                {
                                    Word.ContentControl[] oCCs = oVar.AssociatedContentControls;
                                    for (int i = 0; i < oCCs.Length; i++)
                                    {
                                        Word.Range oRng = oCCs[i].Range;
                                        LMP.Forte.MSWord.WordDoc.UnderlineRangeToLongestLine(oRng, bUnderline);
                                    }
                                }
                            }
                        }
                    }
                }
                else if (oSourceVar != null && !oSourceVar.IsTagless && m_oForteDocument.Mode != ForteDocument.Modes.Design)
                {
                    if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //If no Variable Name specified, act on Source Variable
                        Word.XMLNode[] oNodes = oSourceVar.AssociatedWordTags;
                        for (int i = 0; i < oNodes.Length; i++)
                        {
                            Word.Range oRng = oNodes[i].Range;
                            LMP.Forte.MSWord.WordDoc.UnderlineRangeToLongestLine(oRng, bUnderline);
                        }
                    }
                    else
                    {
                        //If no Variable Name specified, act on Source Variable
                        Word.ContentControl[] oCCs = oSourceVar.AssociatedContentControls;
                        for (int i = 0; i < oCCs.Length; i++)
                        {
                            Word.Range oRng = oCCs[i].Range;
                            LMP.Forte.MSWord.WordDoc.UnderlineRangeToLongestLine(oRng, bUnderline);
                        }
                    }
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Underline to Longest - " + this.Parameters);
            }
            finally
            {
                //hide tags if these were hidden before running this action
                if (iShowTags == 0)
                    oView.ShowXMLMarkup = 0;
                //GLOG 3583
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

            }
        }
        /// <summary>
        /// returns AdminSegment definition for specified segment ID
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        protected LMP.Data.AdminSegmentDef GetAdminSegmentDefinition(string xInsertedSegmentID)
        {

            string xMsg = null;

            //validate xSegmentID - message user and return is no def exists
            LMP.Data.AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
            LMP.Data.AdminSegmentDef oDef = null;

            try
            {
                oDef = (LMP.Data.AdminSegmentDef)oDefs
                    .ItemFromID(Int32.Parse(xInsertedSegmentID));
            }
            catch { }

            if (oDef == null)
            {
                xMsg = LMP.Resources.GetLangString("Msg_SegmentDefinitionDoesNotExist");
                MessageBox.Show(xMsg + xInsertedSegmentID + ".",
                    LMP.String.MacPacProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return null;
            }
            return oDef;
        }

        /// <summary>
        /// used to refesh variables collection after tags have been added or deleted
        /// as the result of the reinsertion or deletion of an expanded delete scope
        /// </summary>
        /// <param name="xVariablesAdded"></param>
        protected void RefreshVariables(string xVariablesAdded)
        {
            //when action is executed while assigning default values, the segment
            //has not yet have been added to ForteDocument.Segments, so can't be
            //refreshed in the XML delete/insert event handlers in MacPac10 - refresh here
            if (m_oSegment.CreationStatus != Segment.Status.Finished)
                m_oSegment.Refresh();

            //initialize other variables in scope
            if ((xVariablesAdded != "") && (xVariablesAdded != null))
            {
                string[] aVars = xVariablesAdded.Split(LMP.StringArray.mpEndOfField);
                for (int i = 0; i < aVars.Length; i++)
                {
                    m_oSegment.Variables.ItemFromName(aVars[i]).AssignDefaultValue();
                }
            }
        }
        #endregion
		#region *********************private members*********************
        /// <summary>
        /// switches pleading signatures between table and non-table format as specified
        /// </summary>
        /// <param name="bToTable"></param>
        /// <param name="aExistingIDs"></param>
        /// <param name="aNewIDs"></param>
        /// <param name="bUsePrefill"></param>
        private void ChangePleadingSignatureFormat(bool bToTable, string[] aExistingIDs,
            string[] aNewIDs, bool bUsePrefill)
        {

            //create hashtable with id mappings
            Hashtable oIDs = new Hashtable();
            for (int i = 0; i < aExistingIDs.Length; i++)
                oIDs.Add(aExistingIDs[i], aNewIDs[i]);

            m_oForteDocument.SetDynamicEditingEnvironment();

            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents =
                ForteDocument.IgnoreWordXMLEvents;

            try
            {
                Word.Range oRng = null;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                if (bToTable)
                {
                    //replace non-table signatures with collections
                    Segment[] oSegments = Segment.FindSegments(
                        LMP.Data.mpObjectTypes.PleadingSignatureNonTable,
                        m_oSegment.Segments);
                    for (int i = 0; i < oSegments.Length; i++)
                    {
                        Segment oSig = oSegments[i];
                        if (oIDs.ContainsKey(oSig.ID1.ToString()))
                        {
                            //there's a mapping for existing segment
                            int iContiguous = 0;
                            ArrayList aIDs = new ArrayList();
                            ArrayList aPrefills = new ArrayList();

                            //get id and prefill
                            aIDs.Add(oIDs[oSig.ID1.ToString()]);
                            if (bUsePrefill)
                                aPrefills.Add(new Prefill(oSig));
                            else
                                aPrefills.Add(null);

                            //store insertion location
                            Word.Range oSigRng = oSig.PrimaryRange;

                            Word.Range oStartRng = oSigRng.Duplicate;
                            object oMissing = System.Reflection.Missing.Value;
                            oStartRng.StartOf(ref oMissing, ref oMissing);

                            //determine whether any additional non-table sigs are
                            //adjacent to this one - we'll want to replace with
                            //a single collection table
                            while (i < oSegments.Length - iContiguous - 1)
                            {
                                Segment oNextSig = oSegments[i + iContiguous + 1];
                                if (oIDs.ContainsKey(oNextSig.ID1.ToString()))
                                {
                                    //there's a mapping for the next child sig
                                    string xText = null;
                                    //JTS 6/13/13: Use PrimaryRange instead of WordTag or ContentControl
                                    Word.Range oNextSigRng = oNextSig.PrimaryRange;

                                    //if there's nothing between the end of one sig and the
                                    //start of the next except paragraph marks, sigs are
                                    //contiguous
                                    int iStart = oSigRng.End + 2;
                                    int iEnd = oNextSigRng.Start - 1;
                                    if (iEnd > iStart)
                                    {
                                        object oStart = iStart;
                                        object oEnd = iEnd;
                                        oRng = m_oDocument.Range(ref oStart, ref oEnd);
                                        xText = oRng.Text.Replace("\r", "");
                                    }
                                    if ((xText == null) || (xText == ""))
                                    {
                                        //get id and prefill for contiguous sig
                                        iContiguous++;
                                        aIDs.Add(oIDs[oNextSig.ID1.ToString()]);
                                        if (bUsePrefill)
                                            aPrefills.Add(new Prefill(oNextSig));
                                        else
                                            aPrefills.Add(null);

                                        //delete any intervening empty paragraphs
                                        if (xText == "")
                                            oRng.Delete(ref oMissing, ref oMissing);

                                        //GLOG 3719: Reset range used for starting point in next pass
                                        oSigRng = oNextSig.PrimaryRange;
                                    }
                                    else
                                        break;
                                }
                                else
                                    //There's a non-table signature that is not mapped to a table type
                                    break;
                            }

                            //delete the contiguous sigs
                            //GLOG 3719: Delete in reverse order and clean up empty paragraphs between
                            for (int j = i + iContiguous; j >= i; j--)
                            {
                                try
                                {
                                    //GLOG 7167: Use PrimaryRange here
                                    Word.Range oCurRng = oSegments[j].PrimaryRange;
                                    oCurRng.StartOf(ref oMissing, ref oMissing);
                                    m_oForteDocument.DeleteSegment(oSegments[j], true, true, false);
                                    //Delete left over empty paragraph, unless it contains an XML Tag
                                    if (oCurRng.Paragraphs[1].Range.Text == "\r" && oCurRng.Paragraphs[1].Range.XMLNodes.Count == 0)
                                    {
                                        oCurRng.Characters.First.Delete(ref oMissing, ref oMissing);
                                    }
                                }
                                catch (System.Exception oE)
                                {
                                    throw new LMP.Exceptions.SegmentException(
                                        LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                                        oSegments[j].ID, oE);
                                }
                            }

                            //select insertion location
                            oStartRng.Select();

                            //insert collection
                            try
                            {
                                Segment oParent = null;
                                for (int j = 0; j < iContiguous + 1; j++)
                                {
                                    Segment oSeg = m_oForteDocument.InsertSegment(aIDs[j].ToString(),
                                        oParent, Segment.InsertionLocations.InsertAtSelection,
                                        Segment.InsertionBehaviors.Default, true,
                                        (Prefill)aPrefills[j], "", false, false, null);

                                    //subsequent sigs in this grouping should target the
                                    //new collection
                                    oParent = oSeg.Parent;
                                }
                            }
                            catch (System.Exception oE)
                            {
                                throw new LMP.Exceptions.SegmentException(
                                    LMP.Resources.GetLangString("Error_CouldNotInsertSegment"), oE);
                            }

                            i = i + iContiguous;
                        }
                    }
                }
                else
                {
                    //replace collections with non-table signatures
                    Segment[] oSegments = Segment.FindSegments(
                        LMP.Data.mpObjectTypes.PleadingSignatures, m_oSegment.Segments);
                    for (int i = 0; i < oSegments.Length; i++)
                    {
                        //get ids and prefills of sigs in this collection
                        Segment oCollection = oSegments[i];
                        int iItems = oCollection.Segments.Count;
                        ArrayList aIDs = new ArrayList();
                        ArrayList aPrefills = new ArrayList();
                        for (int j = 0; j < iItems; j++)
                        {
                            Prefill oPrefill = null;
                            Segment oSig = oCollection.Segments[j];
                            if (oIDs.ContainsKey(oSig.ID1.ToString()))
                            {
                                aIDs.Add(oIDs[oSig.ID1.ToString()]);
                                if (bUsePrefill)
                                    oPrefill = new Prefill(oSig);
                                aPrefills.Add(oPrefill);
                            }
                        }

                        if (aIDs.Count > 0)
                        {
                            //get location of collection table
                            oRng = oCollection.PrimaryRange;

                            object oMissing = System.Reflection.Missing.Value;
                            oRng.StartOf(ref oMissing, ref oMissing);

                            //delete collection table
                            try
                            {
                                m_oForteDocument.DeleteSegment(oCollection, true, true, false);
                            }
                            catch (System.Exception oE)
                            {
                                throw new LMP.Exceptions.SegmentException(
                                    LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                                    oCollection.ID, oE);
                            }

                            //select insertion location
                            oRng.Select();

                            //insert non-table segments
                            try
                            {
                                for (int j = 0; j < aIDs.Count; j++)
                                {
                                    Segment oSeg = m_oForteDocument.InsertSegment(aIDs[j].ToString(),
                                        m_oSegment, Segment.InsertionLocations.InsertAtSelection,
                                        Segment.InsertionBehaviors.Default, true,
                                        (Prefill)aPrefills[j], "", false, false, null);

                                    if (j < aIDs.Count - 1)
                                    {
                                        //there are additional signatures to insert - 
                                        //move selection to below this sig, adding a
                                        //normal paragraph in between
                                        oRng = oSeg.PrimaryRange;

                                        oRng.EndOf(ref oMissing, ref oMissing);
                                        oRng.Move(ref oMissing, ref oMissing);
                                        oRng.InsertParagraphAfter();
                                        object oNormalStyleID = Word.WdBuiltinStyle.wdStyleNormal;
                                        Word.Style oNormalStyle = m_oForteDocument.WordDocument.Styles.get_Item(
                                            ref oNormalStyleID);
                                        object oNormal = (object)oNormalStyle;
                                        oRng.set_Style(ref oNormal);
                                        oRng.EndOf(ref oMissing, ref oMissing);
                                        oRng.Select();
                                    }

                                }
                            }
                            catch (System.Exception oE)
                            {
                                throw new LMP.Exceptions.SegmentException(
                                    LMP.Resources.GetLangString("Error_CouldNotInsertSegment"), oE);
                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
            }
        }
        #endregion
	}

	/// <summary>
	/// defines the abstract ActionsCollection class, from which
	/// VariableActions, ControlActions, and SegmentActions are derived
	/// </summary>
	public abstract class ActionsCollectionBase : LMP.Architect.Base.ActionsCollectionBase
	{
        //#region *********************fields*********************
        //private ArrayList m_oActions = new ArrayList();
        //#endregion
        //#region *********************constructors*********************
        //public ActionsCollectionBase()
        //{
        //}
        //#endregion
        //#region *********************properties*********************
        //public ActionBase this[int iIndex]
        //{
        //    get{return this.ItemFromIndex(iIndex);}
        //}
        //public int Count
        //{
        //    get
        //    {
        //        return m_oActions.Count;
        //    }
        //}
        //#endregion
        //#region *********************methods*********************
        ///// <summary>
        ///// returns a new ActionBase
        ///// </summary>
        ///// <returns></returns>
        //protected ActionBase Create()
        //{
        //    try
        //    {
        //        ////save current edits to collection
        //        //this.UpdateCollectionWithCurrentAction();

        //        //request the new object of the appropriate type -
        //        //GetNewActionInstance is abstract, and hence
        //        //implemented in each derived class
        //        ActionBase oAction = this.GetNewActionInstance();
        //        oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);
				
        //        //flag new action so it can be added to internal collection
        //        oAction.IsDirty = true;
        //        return oAction;
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_ActionCreationFailed"), oE);
        //    }
        //}

        ///// <summary>
        ///// returns the variable ActionBase with the specified index
        ///// </summary>
        ///// <param name="iIndex"></param>
        ///// <returns></returns>
        //public ActionBase ItemFromIndex(int iIndex)
        //{
        //    Trace.WriteNameValuePairs("iIndex", iIndex);

        //    ////save current edits to the collection
        //    //this.UpdateCollectionWithCurrentAction();

        //    //check for valid supplied index
        //    if(iIndex < 0 || iIndex > m_oActions.Count - 1)
        //    {
        //        throw new LMP.Exceptions.NotInCollectionException(
        //            LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
        //            iIndex.ToString());
        //    }

        //    //get the array that defines data for the ActionBase
        //    string[] aAction = (string[]) m_oActions[iIndex];

        //    //get an object from the array - set as current object
        //    ActionBase oAction = GetActionFromArray(aAction);

        //    //subscribe to notifications that this action has been dirtied (edited)
        //    oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);
        //    return oAction;
        //}

        ///// <summary>
        ///// deletes the specified variable ActionBase
        ///// </summary>
        ///// <param name="iIndex"></param>
        //public virtual void Delete(int iIndex)
        //{
        //    try
        //    {
        //        Trace.WriteNameValuePairs("iIndex", iIndex);

        //        ////save current edits to collection
        //        //this.UpdateCollectionWithCurrentAction();

        //        //test for valid index
        //        if(iIndex < 0 || iIndex > m_oActions.Count - 1)
        //        {
        //            throw new LMP.Exceptions.NotInCollectionException(
        //                LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
        //                iIndex.ToString());
        //        }

        //        //remove from array list
        //        m_oActions.RemoveAt(iIndex);

        //        //adjust all execution indices from deletion index to last index
        //        for(int i=iIndex; i<m_oActions.Count; i++)
        //        {
        //            //reassign execution index
        //            ((string[]) m_oActions[i])[1] = (i + 1).ToString();
        //        }
			
        //        //alert subscribers that collection has changed
        //        if(this.ActionsDirtied != null)
        //            this.ActionsDirtied(this, new EventArgs());
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_ActionDeletionFailed") + iIndex.ToString(), oE);
        //    }
        //}

        ///// <summary>
        ///// sets the internal storage for this collection
        ///// from a delimited string - should be called only 
        ///// by the Variables class -
        /////this is needed because variable actions are stored
        /////as an index in the variable array - it's faster than
        /////having to retrieve and parse the data a second time from 
        /////the object data attribute
        ///// </summary>
        ///// <param name="oActions"></param> 
        //public void SetFromString(string xActions)
        //{
        //    try
        //    {
        //        Trace.WriteNameValuePairs("xActions", xActions);

        //        //clear internal storage
        //        m_oActions.Clear();

        //        if(xActions != "")
        //        {
        //            //convert actions string into array of ActionBase def strings
        //            string[] aActionStrings = xActions.Split(LMP.StringArray.mpEndOfRecord);
        //            //cycle through ActionBase def strings, converting each to an array
        //            //and adding to the sorted list
        //            for(int i=0; i <= aActionStrings.GetUpperBound(0); i++)
        //            {
        //                //get ActionBase def string - add an ID to beginning -
        //                //this will be the ID of the ActionBase - we need IDs only
        //                //at run time (it's better not to take up space in document)
        //                string xAction = System.Guid.NewGuid().ToString() + 
        //                    LMP.StringArray.mpEndOfSubValue + aActionStrings[i];

        //                //split into array
        //                string[] aAction = xAction.Split(LMP.StringArray.mpEndOfSubValue);

        //                if(aAction.GetUpperBound(0) != 5)
        //                    //bad ActionBase string
        //                    throw new LMP.Exceptions.DataException(
        //                        LMP.Resources.GetLangString(
        //                        "Error_InvalidVariableActionDefinition" + xAction));

        //                //add to array list at position specified by ExecutionIndex (array index 1)
        //                m_oActions.Insert(Convert.ToInt32(aAction[1]) - 1, aAction);
        //            }
        //        }
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotPopulateActionsCollection"), oE);
        //    }
        //}

        ///// <summary>
        ///// returns a VariableAction populated with data supplied by specified array
        ///// </summary>
        ///// <param name="aNewValues"></param>
        ///// <returns></returns>
        //protected abstract ActionBase GetActionFromArray(string[] aNewValues);
        ///// <summary>
        ///// derived class will implement to return a new instance of the individual
        ///// </summary>
        ///// <returns></returns>
        //protected abstract ActionBase GetNewActionInstance();
        ///// <summary>
        ///// executes the appropriate actions
        ///// </summary>
        //public virtual void Execute()
        //{
        //    //cycle through segment actions, executing actions in the collection
        //    for(int i=0; i<this.Count; i++)
        //    {
        //        ActionBase oAction = (ActionBase) this[i];

        //        try
        //        {
        //            oAction.Execute();
        //        }
        //        catch(System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.ActionException(
        //                LMP.Resources.GetLangString("Error_CouldNotExecuteAction") + 
        //                oAction.ExecutionIndex.ToString(), oE);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Moves up the ExecutionIndex of the action
        ///// </summary>
        //public void MoveUp(ActionBase oAction)
        //{
        //    try
        //    {
        //        //move up if necessary
        //        if (oAction.ExecutionIndex > 1)
        //        {
        //            //get array of values
        //            string[] aNewValues = oAction.ToArray(true);

        //            //the execution index has changed-
        //            //remove from current array position
        //            m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

        //            //re-insert at next index up
        //            m_oActions.Insert(oAction.ExecutionIndex - 2, aNewValues);

        //            //increment execution index of supplied var
        //            oAction.ExecutionIndex = oAction.ExecutionIndex - 1;

        //            ////we need to update execution indexes
        //            for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
        //                //reassign execution index
        //                ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


        //            //alert subscribers that collection has changed
        //            if (this.ActionsDirtied != null)
        //                this.ActionsDirtied(this, new EventArgs());
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString("Error_CouldNotMoveUpAction") +
        //            oAction.ExecutionIndex.ToString(), oE);
        //    }

        //}

        ///// <summary>
        ///// Moves down the ExecutionIndex of the action
        ///// </summary>
        //public void MoveDown(ActionBase oAction)
        //{
        //    try
        //    {
        //        //move down if necessary
        //        if (oAction.ExecutionIndex < m_oActions.Count)
        //        {
        //            //get array of values
        //            string[] aNewValues = oAction.ToArray(true);

        //            //the execution index has changed-
        //            //remove from current array position
        //            m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

        //            //re-insert at next index down
        //            m_oActions.Insert(oAction.ExecutionIndex, aNewValues);

        //            ////we need to update execution indexes
        //            for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
        //                //reassign execution index
        //                ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


        //            //alert subscribers that collection has changed
        //            if (this.ActionsDirtied != null)
        //                this.ActionsDirtied(this, new EventArgs());
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString("Error_CouldNotMoveDownAction") +
        //            oAction.ExecutionIndex.ToString(), oE);
        //    }
        //}

        ///// <summary>
        ///// returns the collection of actions as a string
        ///// </summary>
        ///// <returns></returns>
        //public override string ToString()
        //{
        //    StringBuilder oSB = new StringBuilder();

        //    //cycle through actions, appending each to strin
        //    for (int i = 0; i < this.Count; i++)
        //    {
        //        oSB.AppendFormat("{0}{1}", this[i].ToString(),
        //            LMP.StringArray.mpEndOfRecord.ToString());
        //    }

        //    //trim trailing record delimiter
        //    return oSB.ToString(0, Math.Max(oSB.Length - 1,0));
        //}

        //#endregion
        //#region *********************private members*********************
        ///// <summary>
        ///// saves the specified ActionBase to internal storage
        ///// </summary>
        //private void SaveAction(ActionBase oAction)
        //{
        //    DateTime t0 = DateTime.Now;

        //    try
        //    {
        //        //end if no current action or current action has no edits
        //        if(oAction  == null || !oAction.IsDirty)
        //            return;

        //        //ensure valid data
        //        if(!oAction.IsValid)
        //            throw new LMP.Exceptions.DataException(
        //                LMP.Resources.GetLangString(
        //                "Error_InvalidActionDefinition") + oAction.ToString());

        //        //IDs are assigned only when item is saved
        //        bool bIsNew = (oAction.ID == "");

        //        if(bIsNew)
        //            //get ID for item
        //            oAction.SetID();
				
        //        //if execution index is not specified, set to last in collection
        //        if(oAction.ExecutionIndex == 0)
        //            oAction.ExecutionIndex = m_oActions.Count + 1;

        //        //get array of values
        //        string[] aNewValues = oAction.ToArray(true);
			
        //        if(bIsNew)
        //        {
        //            //VariableAction is new-
        //            //insert into array list
        //            int iExecutionIndex = oAction.ExecutionIndex;
			
        //            //if execution index is not specified, set to last in collection
        //            if(iExecutionIndex == 0)
        //                iExecutionIndex = m_oActions.Count + 1;

        //            if(iExecutionIndex > m_oActions.Count)
        //                //specified execution index is greater 
        //                //than the last execution index - add
        //                //to end of list
        //                m_oActions.Add(aNewValues);
        //            else
        //            {
        //                //insert at position specified by ExecutionIndex
        //                m_oActions.Insert(iExecutionIndex - 1, aNewValues);

        //                //adjust all appropriate execution indices
        //                for(int i=iExecutionIndex; i < m_oActions.Count; i++)
        //                    ((string[]) m_oActions[i])[1] = (i + 1).ToString();
        //            }
        //        }
        //        else
        //        {
        //            //save existing ActionBase - cycle through actions
        //            //looking for the ActionBase we want to save
        //            for(int i=0; i < m_oActions.Count; i++)
        //            {
        //                string[] aOldValues = (string[]) m_oActions[i];

        //                if(aOldValues[0] == oAction.ID)
        //                {
        //                    //we've found the ActionBase in the collection-
        //                    //replace the item with the updated array
        //                        m_oActions[i] = aNewValues;

        //                    break;
        //                }
        //            }
        //        }

        //        LMP.Benchmarks.Print(t0);
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotUpdateCollection"), oE);
        //    }
        //}
        ///// <summary>
        ///// handles notification from action that it has been dirtied
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void OnActionDirtied(object sender, EventArgs e)
        //{
        //    //save action
        //    SaveAction((ActionBase)sender);

        //    if(ActionsDirtied != null)
        //        //notify subscribers that the collection has been edited
        //        ActionsDirtied(this, new EventArgs());
        //}
        //#endregion
	}
}
