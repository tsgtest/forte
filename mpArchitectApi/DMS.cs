using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace LMP.Architect.Api
{
    public static class DMS
    {
        static fDMS.CDMS m_oDMS = null;
        static DMS()
        {
            fDMS.mpDMSs iDMS = fDMS.mpDMSs.mpDMS_Windows;
            m_oDMS = new fDMS.CDMS();
            //Get DMS type from Firm Application Settings
            LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
            try
            {
                int iTest = oSettings.DMSType;
                iDMS = (fDMS.mpDMSs)iTest;
                m_oDMS.set_TypeID(ref iDMS);
            }
            catch 
            {
                iDMS = fDMS.mpDMSs.mpDMS_Windows;
                m_oDMS.set_TypeID(ref iDMS);
            }
        }
        public static fDMS.CDMS COMObject
        {
            get { return m_oDMS; }
        }
        /// <summary>
        /// Returns true of activedocument is profiled in DMS
        /// </summary>
        public static bool IsProfiled
        {
            //JTS 12/16/10: If not Profiled, GetProfileValue might return null instead of ""
            get { return m_oDMS.get_TypeID() != fDMS.mpDMSs.mpDMS_Windows
                && !string.IsNullOrEmpty(GetProfileValue("DocNumber")); }
        }
        /// <summary>
        /// Return 
        /// </summary>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        public static string GetProfileValue(string xPropName)
        {
            return GetProfileValue(xPropName, "");
        }
        public static string GetProfileValue(string xPropName, string xDateFormat)
        {
            string xValue = "";
            fDMS._IDMS oProfile = m_oDMS.Profile();
            try
            {
                switch (xPropName.ToUpper())
                {
                    case "DOCNUMBER":
                        xValue = oProfile.DocNumber;
                        break;
                    case "VERSION":
                        xValue = oProfile.Version;
                        break;
                    case "LIBRARY":
                        xValue = oProfile.Library;
                        break;
                    case "CLIENTID":
                        xValue = oProfile.ClientID;
                        break;
                    case "CLIENTNAME":
                        xValue = oProfile.ClientName;
                        break;
                    case "MATTERID":
                        xValue = oProfile.MatterID;
                        break;
                    case "MATTERNAME":
                        xValue = oProfile.MatterName;
                        break;
                    case "AUTHORID":
                        xValue = oProfile.AuthorID;
                        break;
                    case "AUTHORNAME":
                        xValue = oProfile.AuthorName;
                        break;
                    case "TYPISTID":
                        xValue = oProfile.TypistID;
                        break;
                    case "TYPISTNAME":
                        xValue = oProfile.TypistName;
                        break;
                    case "ABSTRACT":
                        xValue = oProfile.Abstract;
                        break;
                    case "DOCNAME":
                        xValue = oProfile.DocName;
                        break;
                    case "DOCTYPEDESCRIPTION":
                        xValue = oProfile.DocTypeDescription;
                        break;
                    case "DOCTYPEID":
                        xValue = oProfile.DocTypeID;
                        break;
                    case "FILENAME":
                        xValue = oProfile.FileName;
                        break;
                    case "GROUPNAME":
                        xValue = oProfile.GroupName;
                        break;
                    case "PATH":
                        xValue = oProfile.Path;
                        break;
                    case "REVISIONDATE":
                        xValue = oProfile.RevisionDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CREATIONDATE":
                        xValue = oProfile.CreationDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CUSTOM1":
                        xValue = oProfile.Custom1;
                        break;
                    case "CUSTOM2":
                        xValue = oProfile.Custom2;
                        break;
                    case "CUSTOM3":
                        xValue = oProfile.Custom3;
                        break;
                    default:
                        object oProp = xPropName;
                        //Get any other profile property using DMS-specific field ID
                        xValue = oProfile.GetProfileInfo(ref oProp);
                        break;
                }
            }
            catch
            {
            }
            return xValue;
        }
    }
}
