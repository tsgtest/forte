using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class Letter : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.Letter, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class LetterSignatures : CollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class LetterSignature : CollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.LetterSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class LetterSignatureNonTable : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, false,
                    iIntendedUse, mpObjectTypes.LetterSignatureNonTable, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class Letterhead : Paper
    {
        // GLOG : 3135 : JAB
        // Provide the ability for the letterhead to update its content.
        public void RefreshContent()
        {
            Prefill oPrefill = this.CreatePrefill();
            Segment.Replace(this.ID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// When authors are updated, the letterhead is replaced with the default 
        /// letterhead. This method provides a mechanism by with editted content
        /// can restore their non default letterhead.
        /// </summary>
        /// <param name="xNewID"></param>
        public void RefreshContent(string xNewID)
        {
            Prefill oPrefill = this.CreatePrefill();
            Segment.Replace(xNewID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }
    }
}
