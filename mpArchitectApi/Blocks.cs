using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Api
{
    /// <summary>
    /// contains properties that define the arguments for the BlockDeleted and BlockAdded events
    /// </summary>
    public class BlockEventArgs
    {
        private Block m_oBlock;

        public BlockEventArgs(Block oBlock)
        {
            m_oBlock = oBlock;
        }

        public Block Block
        {
            get { return m_oBlock; }
            set { m_oBlock = value; }
        }
    }

    /// <summary>
    /// declares the delegate for a BlockDeleted event handler
    /// </summary>
    public delegate void BlockDeletedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a BlockAdded event handler
    /// </summary>
    public delegate void BlockAddedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a BlockDefinitionChanged event handler
    /// </summary>
    public delegate void BlockDefinitionChangedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// contains the methods and properties that define a MacPac Block
    /// </summary>
	public class Block : LMP.Architect.Base.Block
	{
		#region *********************constants*********************
		#endregion
		#region *********************fields*********************
        private Segment m_oSegment;
        private ForteDocument m_oMPDocument;
		#endregion
		#region *********************constructors*********************
		internal Block(Segment oParent)
		{
			m_oSegment = oParent;
		}
        internal Block(ForteDocument oParent)
        {
            m_oMPDocument = oParent;
        }
        #endregion
		#region *********************properties*********************

        public Segment Segment
        {
            get { return m_oSegment; }
        }

        public ForteDocument ForteDocument
        {
            get
            {
                return this.Segment != null ?
                    m_oSegment.ForteDocument : m_oMPDocument;
            }
        }

        /// <summary>
        /// returns the blocks collection to which this block belongs
        /// </summary>
        public Blocks Parent
        {
            get
            {
                ////return the segment blocks collection
                ////if this block is not standalone, else
                ////the document's blocks collection
                //if (this.IsStandalone)
                //    return this.ForteDocument.Blocks;
                //else
                return this.Segment.Blocks;
            }
        }

        /// <summary>
        /// returns true iff the block is a standalone block
        /// </summary>
        public bool IsStandalone
        {
            get { return this.Segment == null; }
        }

        /// <summary>
        /// returns the content control that is
        /// associated with this block
        /// </summary>
        public Word.ContentControl AssociatedContentControl
        {
            get
            {
                if (this.TagID != "")
                {
                    NodeStore oNodes = this.Parent.Nodes;
                    string xElementName = oNodes.GetItemElementName(this.TagID);
                    if (xElementName == "mBlock")
                        return oNodes.GetContentControls(this.TagID)[0];
                    else
                        return null;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// returns the Word.XMLNode that is
        /// associated with this block
        /// </summary>
        public Word.XMLNode AssociatedWordTag
        {
            get
            {
                if (this.TagID != "")
                {
                    NodeStore oNodes = this.Parent.Nodes;
                    string xElementName = oNodes.GetItemElementName(this.TagID);
                    if (xElementName == "mBlock")
                        return oNodes.GetWordTags(this.TagID)[0];
                    else
                        return null;
                }
                else
                    return null;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// inserts mBlock word tag at current selection
        /// </summary>
        /// <param name="range"></param>
        public Word.XMLNode InsertAssociatedTag(string xTagID, string xObjectData)
        {
            try
            {
                //get tag for new mBlock tag
                xTagID = this.Name;
                string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
                this.TagID = xFullTagID;

                //10.2 (dm) assign object database id
                this.ObjectDatabaseID = this.m_oSegment.NextObjectDatabaseID;

                string xDef = this.ToString();
                xObjectData = (xDef).Substring(xDef.IndexOf("|" + this.Name) + 1);

                //insert mBlock tag
                Word.XMLNode oNewNode = LMP.Forte.MSWord.WordDoc.InsertmBlockAtSelection(xTagID, xObjectData);

                //add tag to collection
                m_oSegment.ForteDocument.Tags.Insert(oNewNode, false);

                return oNewNode;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInsertWordTag"), oE);
            }

        }
 
        /// <summary>
        /// inserts mBlock content control at current selection
        /// </summary>
        /// <param name="range"></param>
        public Word.ContentControl InsertAssociatedContentControl(string xTagID, string xObjectData)
        {
            try
            {
                //get tag id for new mBlock tag
                xTagID = this.Name;
                string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
                this.TagID = xFullTagID;

                //get object data
                string xDef = this.ToString();
                xObjectData = (xDef).Substring(xDef.IndexOf("|" + this.Name) + 1);

                //insert mBlock tag
                Word.ContentControl oCC = LMP.Forte.MSWord.WordDoc.InsertmBlockAtSelection_CC(xTagID, xObjectData);

                //add tag to collection
                m_oSegment.ForteDocument.Tags.Insert_CC(oCC, false);

                return oCC;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInsertWordTag"), oE);
            }

        }
        #endregion
	}

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac blocks
	/// </summary>
    public class Blocks: LMP.Architect.Base.Blocks
    {
		#region *********************fields*********************
        private Hashtable m_oDefsByName;
        private Hashtable m_oDefsByTagID;
        private ArrayList m_oDefsByIndex;
        private Segment m_oSegment;
        private ForteDocument m_oMPDocument;
        #endregion
		#region *********************constructors*********************
        /// <summary>
        /// creates a blocks collection belonging
        /// to the specified segment
        /// </summary>
        /// <param name="oParent"></param>
        internal Blocks(Segment oParent)
		{
			m_oSegment = oParent;

			//populate internal storage from node store
            PopulateStorage(m_oSegment.Nodes);
		}

        /// <summary>
        /// creates a blocks collection belonging
        /// to the specified MacPac Document
        /// </summary>
        /// <param name="oParent"></param>
        internal Blocks(ForteDocument oParent)
        {
            m_oMPDocument = oParent;

            //populate internal storage from node store
            PopulateStorage(oParent.OrphanNodes);
        }
		#endregion
        #region *********************properties*********************
        public Block this[int iIndex]
        {
            get { return ItemFromIndex(iIndex); }
        }
        public int Count
        {
            get
            {
                return m_oDefsByName.Count;
            }
        }
        public Segment Segment
        {
            get { return m_oSegment; }
        }
        public ForteDocument ForteDocument
        {
            get
            {
                return m_oSegment != null ?
                    m_oSegment.ForteDocument : m_oMPDocument;
            }
        }
        /// <summary>
        /// returns the nodes for this collection of blocks
        /// </summary>
        internal NodeStore Nodes
        {
            get
            {
                if (this.Segment == null)
                    //this collection belongs to a MacPac Document-
                    //return the orphan nodes
                    return this.ForteDocument.OrphanNodes;
                else
                    //this collection belongs to a segment -
                    //return the segment's nodes
                    return this.Segment.Nodes;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// returns a new block
        /// </summary>
        /// <returns></returns>
        public Block CreateBlock()
        {
            Block oBlock = new Block(this.Segment);
            oBlock.SegmentName = this.Segment.FullTagID;
            return oBlock;
        }

        public Block ItemFromIndex(int iIndex)
        {
            if (iIndex > m_oDefsByIndex.Count - 1 || iIndex < 0)
            {
                //index is not in range - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"));
            }

            //get the block at the specified index
            Block oBlock = (Block)m_oDefsByIndex[iIndex];

            return oBlock;
        }

        public Block ItemFromName(string xName)
        {
            Block oBlock = null;

            Trace.WriteNameValuePairs("xName", xName);

            try
            {
                oBlock = (Block)m_oDefsByName[xName];
            }
            catch { }

            if (oBlock == null)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString(
                    "Error_ItemWithIDNotInCollection") + xName);
            }

            return oBlock;
        }

        /// <summary>
        /// returns the block whose associated tag is the specified Word tag
        /// </summary>
        /// <param name="oWordTag"></param>
        /// <returns></returns>
        public Block ItemFromAssociatedTag(Word.XMLNode oWordTag)
        {
            Block oBlock = null;

            if (oWordTag.BaseName != "mBlock")
            {
                //mBlock tags are required for this function
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString("Error_BlockTagRequired"));
            }

            //get tag id of specified Word tag
            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oWordTag);

            try
            {
                //get block from hashtable that maps 
                //selection tag names to blocks
                oBlock = (Block)this.m_oDefsByTagID[xTagID];
            }
            catch { }

            if (oBlock == null)
                return null;
            else
                return oBlock;
        }

        /// <summary>
        /// returns the block whose associated tag is the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        public Block ItemFromAssociatedContentControl(Word.ContentControl oCC)
        {
            Block oBlock = null;

            string xTag = oCC.Tag;
            if (LMP.String.GetBoundingObjectBaseName(xTag) != "mBlock")
            {
                //mBlock tags are required for this function
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString("Error_BlockTagRequired"));
            }

            //get tag id of specified Word tag
            string xTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oCC);

            try
            {
                //get block from hashtable that maps 
                //selection tag names to blocks
                oBlock = (Block)this.m_oDefsByTagID[xTagID];
            }
            catch { }

            if (oBlock == null)
                return null;
            else
                return oBlock;
        }

        /// <summary>
        /// saves the specified block to the Word XML Tag
        /// </summary>
        /// <param name="oBlock"></param>
        public void Save(Block oBlock)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name);

                //save only if variable is dirty
                if (!oBlock.IsDirty)
                    return;

                if (!oBlock.HasValidDefinition)
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidBlockDefinition") + string.Join("|", oBlock.ToArray()));

                if (this.BlockExists(oBlock.Name))
                {
                    if (!this.Nodes.NodeExists(oBlock.TagID))
                    {
                        //can't save to specified tag - alert
                        throw new LMP.Exceptions.WordXmlNodeException(
                            LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oBlock.TagID);
                    }

                    //get new definition 
                    string xBlock = oBlock.ToString(true);

                    //set object data for specified Node
                    this.Nodes.SetItemObjectData(oBlock.TagID, xBlock);

                    //raise event
                    this.ForteDocument.RaiseBlockDefinitionChangedEvent(oBlock);
                }
                else
                {
                    //block is not yet in the collection - this method assumes that
                    //that the definition for the new block is already in the
                    //Word XMLNode and the ForteDocument.Tags collection -
                    //it also assumes that all of the properties of the new block
                    //have been set - all of this is handled by the front-end

                    //refresh node store
                    if (m_oSegment != null)
                        m_oSegment.RefreshNodes();
                    else
                        m_oMPDocument.RefreshOrphanNodes();

                    //add to internal arrays
                    m_oDefsByName.Add(oBlock.Name, oBlock);
                    m_oDefsByTagID.Add(oBlock.TagID, oBlock);
                    m_oDefsByIndex.Add(oBlock);

                    //raise event
                    this.ForteDocument.RaiseBlockAddedEvent(oBlock);
                }

                //remove dirt
                oBlock.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.VariableException(
                    LMP.Resources.GetLangString("Error_CantSaveBlock") + oBlock.Name, oE);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// returns true if the block with the specified name is in the collection
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool BlockExists(string xName)
        {
            Block oBlock = null;

            Trace.WriteNameValuePairs("xName", xName);

            try
            {
                oBlock = (Block)m_oDefsByName[xName];
            }
            catch { }

            return (oBlock != null);
        }

        /// <summary>
        /// deletes the specified block and its associated tag
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="bDeleteAssociatedTag"></param>
        public void Delete(Block oBlock)
        {
            Delete(oBlock, true);
        }

        /// <summary>
        /// deletes the specified block and its associated tag if specified
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="bDeleteAssociatedTag"></param>
        public void Delete(Block oBlock, bool bDeleteAssociatedTag)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name);

            if (!this.Nodes.NodeExists(oBlock.TagID))
            {
                //can't find specified tag - alert
                throw new LMP.Exceptions.WordXmlNodeException(
                    LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oBlock.TagID);
            }

            //delete tag
            if (bDeleteAssociatedTag)
            {
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                //JTS 5/10/10: 10.2
                if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    //GLOG 6326 (dm) - delete associated doc var and bookmark
                    Word.ContentControl oCC = oBlock.AssociatedContentControl;
                    LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
                        this.ForteDocument.WordDocument, false);

                    oCC.Delete(false);
                }
                else
                {
                    //GLOG 6326 (dm) - delete associated doc var and bookmark
                    Word.XMLNode oTag = oBlock.AssociatedWordTag;
                    Word.XMLNode oReserved = oTag.SelectSingleNode("@Reserved", "", true);
                    if (oReserved != null)
                    {
                        LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
                            this.ForteDocument.WordDocument, false);
                    }

                    oTag.Delete();
                }
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }

            //delete the block from the internal arrays
            DeleteFromInternalArrays(oBlock);

            //JTS 5/10/10: Not required when being called from RefreshVariables
            if (bDeleteAssociatedTag)
                //GLOG item #4424 and #4425 - dcf
                this.ForteDocument.RefreshTags(this.ForteDocument.Mode != ForteDocument.Modes.Design);

            //refresh node store
            if (m_oSegment != null)
                m_oSegment.RefreshNodes();
            else
                this.ForteDocument.RefreshOrphanNodes();

            //raise event
            this.ForteDocument.RaiseBlockDeletedEvent(oBlock);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns a block populated with the supplied definition values
        /// </summary>
        /// <param name="xBlockDef"></param>
        /// <returns></returns>
        public Block GetBlockFromDefinition(string xBlockDef)
        {
            //trim tag prefix id - added 8/7/08 for GLOG 2937
            int iPos = xBlockDef.IndexOf(LMP.String.mpTagPrefixIDSeparator);
            if (iPos != -1)
            {
                xBlockDef = xBlockDef.Substring(iPos +
                    LMP.String.mpTagPrefixIDSeparator.Length);
            }

            //convert definition to array
            string[] aBlockDef = xBlockDef.Split('|');

            //get Block from array
            return GetBlockFromArray(aBlockDef);
        }

        #endregion
        #region *********************private members*********************
        /// <summary>
        /// returns the object data for all block nodes as an array
        /// </summary>
        /// <returns></returns>
        private void PopulateStorage(ReadOnlyNodeStore oNodes)
        {
            DateTime t0 = DateTime.Now;

            m_oDefsByName = new Hashtable();
            m_oDefsByIndex = new ArrayList();
            m_oDefsByTagID = new Hashtable();

            //cycle through collection, adding ObjectData string to array
            for (int i = 0; i < oNodes.Count; i++)
            {
                string xElementName = oNodes.GetItemElementName(i);
                if (xElementName == "mBlock")
                {
                    string xObjectData = "";
                   try
                    {
                        xObjectData = oNodes.GetItemObjectData(i);
                    }
                    catch
                    {
                        xObjectData = "";
                    }

                    if (xObjectData != "")
                    {
                        string xTagID = oNodes.GetItemTagID(i);
                        string xTagParentPartNumber = oNodes.GetItemPartNumbers(i);

                        //get segment name by trimming tag name from tag id
                        string xSegmentName = "";
                        int iPos = xTagID.LastIndexOf(".");
                        if (iPos > -1)
                            xSegmentName = xTagID.Substring(0, iPos);

                        //convert to array - modified 8/7/08 for GLOG 2937
                        string[] aDef = oNodes.GetItemObjectDataArray(xTagID);

                        //get variable object
                        Block oBlock = this.GetBlockFromArray(aDef);

                        //set runtime properties
                        oBlock.SegmentName = xSegmentName;
                        oBlock.TagID = xTagID;
                        oBlock.TagParentPartNumber = xTagParentPartNumber;
                        oBlock.TagPrefixID = oNodes.GetItemTagPrefixID(xTagID);
                        oBlock.IsDirty = false;

                        //10.2 (dm) - set next object database id for segment
                        m_oSegment.NextObjectDatabaseID = oBlock.ObjectDatabaseID + 1;

                        //add to internal arrays
                        m_oDefsByName.Add(oBlock.Name, oBlock);
                        m_oDefsByTagID.Add(oBlock.TagID, oBlock);
                        m_oDefsByIndex.Add(oBlock);
                    }
                }
            }

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns a block populated with the supplied definition values
        /// </summary>
        /// <param name="aDef"></param>
        /// <returns></returns>
        private Block GetBlockFromArray(string[] aDef)
        {
            DateTime t0 = DateTime.Now;

            Block oBlock = null;
            if (m_oSegment != null)
                oBlock = new Block(m_oSegment);
            else
                oBlock = new Block(m_oMPDocument);

            try
            {
                oBlock.Name = aDef[0].ToString();
                oBlock.DisplayName = aDef[1].ToString();
                oBlock.TranslationID = Convert.ToInt32(aDef[2]);
                oBlock.ShowInTree = Convert.ToBoolean(aDef[3].ToString());
                oBlock.IsBody = Convert.ToBoolean(aDef[4].ToString());
                oBlock.HelpText = aDef[5].ToString();
                oBlock.Hotkey = aDef[6].ToString();

                // Since this is a newly introduced property it may not be
                // persisted in the xml and the array of properties string 
                // may not contain this property.

                if(aDef.Length < 8)
                {
                    // This property is not in the xml. Set the value accordingly such that
                    // it produces the behaviour exhibitted prior to the introduction of 
                    // this enhancement, ie, show this block in the advanced section.
                    oBlock.DisplayLevel = LMP.Architect.Api.Variable.Levels.Advanced;
                }
                else
                {
                    // Use the value persisted in the xml.

                    try
                    {
                        oBlock.DisplayLevel = (Variable.Levels)Convert.ToInt32(aDef[7]);
                    }
                    catch
                    {
                        // Previously this was a boolean. Make a best effort attempt to preserve the
                        // the boolean setting:
                        //      True => Place this block in the basic section. 
                        //      False => place this in the advanced section.
                        //
                        // Revert to the behavior prior to this enhancement should there be an error.
                        // This is also necessary since the type of the Block.Display has changed after
                        // being released.

                        try
                        {
                            bool bShowBlockInBasicSection = Convert.ToBoolean(aDef[7]);
                            oBlock.DisplayLevel = bShowBlockInBasicSection ? Variable.Levels.Basic : Variable.Levels.Advanced;
                        }
                        catch
                        {
                            // Revert to the behavior prior to this enhancement should there be an error.
                            oBlock.DisplayLevel = Variable.Levels.Advanced;
                        }
                    }
                }
                //GLOG 2165: Set Starting Text if it exists in XML
                if (aDef.Length > 8)
                {
                    oBlock.StartingText = aDef[8].ToString();
                }

                //10.2
                if (aDef.Length > 9 && !string.IsNullOrEmpty(aDef[9]))
                    oBlock.ObjectDatabaseID = Int32.Parse(aDef[9].ToString());
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                        "Error_InvalidBlockDefinition") + oBlock.TagID, oE);
            }

            LMP.Benchmarks.Print(t0);
            return oBlock;
        }

        /// <summary>
        /// deletes the specified block from the internal arrays
        /// </summary>
        /// <param name="oVar"></param>
        private void DeleteFromInternalArrays(Block oBlock)
        {
            //delete from internal arrays
            m_oDefsByIndex.Remove(oBlock);
            m_oDefsByName.Remove(oBlock.Name);
            m_oDefsByTagID.Remove(oBlock.TagID);
        }
        #endregion
    }
}
