using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class NumberedLabels : AdminSegment
    {
        private static Prefill oPrefill = null;

        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.NumberedLabels, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
        public override bool ExecutePostFinish()
        {
            //GLOG item #6377 - dcf
            string xBasePath = this.TagID + ".";

            LMP.Forte.MSWord.NumberedLabels oNumberedLabels = new LMP.Forte.MSWord.NumberedLabels();

            oNumberedLabels.Description = this.DisplayName;
            //GLOG : 5611 : ceh - fill direction
            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "FillDirection"]))
            {
                oNumberedLabels.VerticalFill = oPrefill[xBasePath + "FillDirection"] != "Vertically" ? false : true;
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "NumberOfLabels"]))
            {
                oNumberedLabels.NumLabels = Int32.Parse(oPrefill[xBasePath + "NumberOfLabels"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingColumn"]))
            {
                oNumberedLabels.StartColumn = short.Parse(oPrefill[xBasePath + "StartingColumn"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingRow"]))
            {
                oNumberedLabels.StartRow = short.Parse(oPrefill[xBasePath + "StartingRow"]);
            }

            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingNumber"]))
            {
                oNumberedLabels.StartNumber = int.Parse(oPrefill[xBasePath + "StartingNumber"]);
            }
            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "Digits"]))
            {
                oNumberedLabels.Digits = short.Parse(oPrefill[xBasePath + "Digits"]);
            }
            if (!string.IsNullOrEmpty(oPrefill[xBasePath + "NumberFormat"]))
            {
                oNumberedLabels.NumberFormat = oPrefill[xBasePath + "NumberFormat"];
            }

            oNumberedLabels.WriteLabels();

            return base.ExecutePostFinish();
        }
        public override bool PrepareForFinish()
        {
            string xInvalidVarDisplayName = null;

            //GLOG item #6377 - dcf
            string xBasePath = this.TagID + ".";

            oPrefill = new Prefill(this);

            //validate data
            if (!String.IsNumericInt32(oPrefill[xBasePath + "NumberOfLabels"]))
            {
                xInvalidVarDisplayName = "Number of Labels";
            }
            else if (!string.IsNullOrEmpty(oPrefill[xBasePath + "StartingNumber"]) && !String.IsNumericInt32(oPrefill[xBasePath + "StartingNumber"]))
            {
                xInvalidVarDisplayName = "Starting Number";
            }

            //GLOG : 6377 : CEH
            if (xInvalidVarDisplayName != null)
            {
                MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_InvalidVariableValueNumeric"),
                    xInvalidVarDisplayName),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);

                return false;
            }

            return true;
        }

        protected override void InitializeValues(Prefill oPrefill, string xRelativePath, bool bForcePrefillOverride, bool bChildStructureAvailable)
        {
            base.InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, bChildStructureAvailable);
        }
    }
}
