using System;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using LMP.Data;
using System.Collections;
using LMP.Controls;

namespace LMP.Architect.Api
{
    public class Application: LMP.Architect.Base.Application
    {
        public Application() { }
        #region *********************static fields*********************
        private static Word.Application m_oWordApp = null;
        private static int m_iWordVersion = 0;
        #endregion
        #region *********************static properties*********************
        public static Word.Application CurrentWordApp
        {
            get{return m_oWordApp;}
            set{m_oWordApp = value;}
        }
        public static int CurrentWordVersion
        {
            get
            {
                if (m_iWordVersion == 0)
                {
                    //GLOG 5443 (dm) - this may be called before we have Word app
                    if (m_oWordApp == null)
                        m_oWordApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;

                    //get Word major version number as integer
                    string xVersion = CurrentWordApp.Version;
                    m_iWordVersion = int.Parse(xVersion.Substring(0, xVersion.IndexOf('.')));
                }
                return m_iWordVersion;
            }
        }
        #endregion
    }
}
