using System;
using System.Xml;
using System.Text;
using System.Collections.Specialized;
using LMP.Data;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Api
{
    abstract public class AdminSegment : Segment
    {
        #region *********************enumerations*********************
        #endregion
        #region *********************fields*********************
        private AnswerFileSegments m_oAnswerFileSegments = null;
        #endregion
		#region *********************constructors*********************
        internal AdminSegment():base() 
        {
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the definition of the segment
        /// </summary>
        public new AdminSegmentDef Definition
        {
            get { return (AdminSegmentDef)base.Definition; }
            internal set { base.Definition = value; }
        }
        public AnswerFileSegments AnswerFileSegments
        {
            get
            {
                if (this.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
                    return null;
                if (m_oAnswerFileSegments == null)
                {
                    if (this.HasDefinition)
                        m_oAnswerFileSegments = new AnswerFileSegments(this.Definition.ChildSegmentIDs);
                    else
                        m_oAnswerFileSegments = new AnswerFileSegments("");
                }
                return m_oAnswerFileSegments;
            }
        }
        #endregion
        #region *********************methods*********************
        protected override void Initialize(string xTagID, Segment oParent)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                base.Initialize(xTagID, oParent);

                //get segment properties as list dictionary
                ListDictionary oLD = this.Nodes
                    .GetItemObjectDataListDictionary(this.FullTagID);

                //get segment properties
                string xID = oLD["SegmentID"].ToString();
                if (xID.Contains("."))
                {
                    int iPos = xID.IndexOf(".");
                    this.ID1 = int.Parse(xID.Substring(0, iPos));
                    this.ID2 = int.Parse(xID.Substring(iPos + 1));
                }
                else
                {
                    this.ID1 = Int32.Parse(oLD["SegmentID"].ToString());
                }

                this.TypeID = (mpObjectTypes)Int32.Parse(oLD["ObjectTypeID"].ToString());
                this.Name = oLD["Name"].ToString();
                this.DisplayName = oLD["DisplayName"].ToString();

                //these are optional values - ignore missing values
                if (PropertyValueExists(oLD, "ShowChooser"))
                    this.ShowChooser = Boolean.Parse(oLD["ShowChooser"].ToString());
                if (PropertyValueExists(oLD, "MaxAuthors"))
                    this.MaxAuthors = Int32.Parse(oLD["MaxAuthors"].ToString());
                if (PropertyValueExists(oLD, "AuthorNodeUILabel"))
                {
                    //GLOG 6653
                    string xAuthorNodeUILabel = oLD["AuthorNodeUILabel"].ToString();
                    if (xAuthorNodeUILabel == "1")
                    {
                        this.AuthorsNodeUILabel = "Author";
                    }
                    else if (xAuthorNodeUILabel == "2")
                    {
                        this.AuthorsNodeUILabel = "Attorney";
                    }
                    else if (!string.IsNullOrEmpty(xAuthorNodeUILabel))
                    {
                        this.AuthorsNodeUILabel = xAuthorNodeUILabel;
                    }
                    else
                    {
                        this.AuthorsNodeUILabel = "Author";
                    }
                }

                if (PropertyValueExists(oLD, "AuthorsHelpText"))
                    this.AuthorsHelpText = oLD["AuthorsHelpText"].ToString();

                if (PropertyValueExists(oLD, "ProtectedFormPassword"))
                    this.ProtectedFormPassword = oLD["ProtectedFormPassword"].ToString();
                if (PropertyValueExists(oLD, "AuthorControlProperties"))
                    this.AuthorControlProperties = oLD["AuthorControlProperties"].ToString();
                if (PropertyValueExists(oLD, "TranslationID"))
                    this.TranslationID = Int32.Parse(oLD["TranslationID"].ToString());
                if (PropertyValueExists(oLD, "DefaultWrapperID"))
                    this.DefaultWrapperID = Int32.Parse(oLD["DefaultWrapperID"].ToString());
                if (PropertyValueExists(oLD, "IsTransparentDef"))
                    this.IsTransparentDef = bool.Parse(oLD["IsTransparentDef"].ToString());
                if (PropertyValueExists(oLD, "DisplayWizard"))
                    this.DisplayWizard = bool.Parse(oLD["DisplayWizard"].ToString());

                if (PropertyValueExists(oLD, "WordTemplate"))
                    this.WordTemplate = oLD["WordTemplate"].ToString();
                if (PropertyValueExists(oLD, "RequiredStyles"))
                    this.RequiredStyles = oLD["RequiredStyles"].ToString();
                if (PropertyValueExists(oLD, "RecreateRedirectID"))
                    this.RecreateRedirectID = oLD["RecreateRedirectID"].ToString();

                //transparent if defined as such, not top-level, and the only sibling of its type
                if (!this.IsTopLevel)
                {
                    if ((this.Parent.IsTransparentDef == true) &&
                        (this.Parent.IsTransparent == false))
                        //if parent that is normally transparent is showing,
                        //so should child that is normally transparent -
                        //this comes into play when a pleading caption is
                        //copied from a pleading into a blank document
                        this.IsTransparent = false;
                    else if (this.IsOnlyInstanceOfType)
                        this.IsTransparent = this.IsTransparentDef;
                    else
                    {
                        //ensure that no siblings of the same type are transparent
                        Segments oSiblings = this.Parent.Segments;
                        //GLOG 15907
                        bool bTransparent = this.IsTransparentDef;
                        for (int i = 0; i < oSiblings.Count; i++)
                        {
                            Segment oSibling = oSiblings[i];
                            if ((oSibling.TypeID == this.TypeID) &&
                                (oSibling.FullTagID != this.FullTagID) && (oSibling is CollectionTableItem || oSibling.HasVisibleUIElements == true))
                            {
                                oSibling.IsTransparent = false;
                                bTransparent = false;
                            }
                        }
                        this.IsTransparent = bTransparent;
                    }
                }

                //primary if no other instances or if already has been
                //established as primary -
                //GLOG item 2250 (2/10/09 dm) - this property was previously limited
                //to collection table items, but now applies to all segments
                bool bIsPrimary = false;
                if (PropertyValueExists(oLD, "IsPrimary"))
                    bIsPrimary = bool.Parse(oLD["IsPrimary"].ToString());
                else if (this.IsOnlyInstanceOfType)
                    bIsPrimary = true;
                this.IsPrimary = bIsPrimary;

                //get LinkAuthorsToParent definition
                if (PropertyValueExists(oLD, "LinkAuthorsToParentDef"))
                    this.LinkAuthorsToParentDef = (LinkAuthorsToParentOptions)
                        Int32.Parse(oLD["LinkAuthorsToParentDef"].ToString());

                //get LinkAuthorsToParent runtime value
                this.LinkAuthorsToParent = ((this.LinkAuthorsToParentDef ==
                    LinkAuthorsToParentOptions.Always) || ((this.LinkAuthorsToParentDef ==
                    LinkAuthorsToParentOptions.FirstChildOfType) && bIsPrimary));

                //get author control properties
                if (PropertyValueExists(oLD, "AuthorControlActions"))
                    this.Authors.ControlActions.SetFromString(oLD["AuthorControlActions"].ToString());

                if (oLD["L0"] != null && oLD["L1"] != null && oLD["L2"] != null
                    && oLD["L3"] != null && oLD["L4"] != null)
                {
                    //all levels exist - get levels
                    this.L0 = Int32.Parse(oLD["L0"].ToString());
                    this.L1 = Int32.Parse(oLD["L1"].ToString());
                    this.L2 = Int32.Parse(oLD["L2"].ToString());
                    this.L3 = Int32.Parse(oLD["L3"].ToString());
                    this.L4 = Int32.Parse(oLD["L4"].ToString());
                }
                else if (oLD["L0"] != null || oLD["L1"] != null || oLD["L2"] != null
                    || oLD["L3"] != null || oLD["L4"] != null)
                    //some levels exist, while others don't - alert
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString("Error_MissingSegmentLevelsProperty"));

                //GLOG 3413: Test for valid integer value in ObjectData before attempting to set these
                int iTest = 0;
                if (Int32.TryParse(oLD["MenuInsertionOptions"].ToString(), out iTest))
                    this.MenuInsertionOptions = (InsertionLocations)iTest;
                if (Int32.TryParse(oLD["DefaultMenuInsertionBehavior"].ToString(), out iTest))
                    this.DefaultMenuInsertionBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(oLD["DefaultDragLocation"].ToString(), out iTest))
                    this.DefaultDragLocation = (InsertionLocations)iTest;
                if (Int32.TryParse(oLD["DefaultDragBehavior"].ToString(), out iTest))
                    this.DefaultDragBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(oLD["DefaultDoubleClickLocation"].ToString(), out iTest))
                    this.DefaultDoubleClickLocation = (InsertionLocations)iTest;
                if (Int32.TryParse(oLD["DefaultDoubleClickBehavior"].ToString(), out iTest))
                    this.DefaultDoubleClickBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(oLD["IntendedUse"].ToString(), out iTest))
                    this.IntendedUse = (mpSegmentIntendedUses)iTest;

                this.HelpText = oLD["HelpText"].ToString();

                //get jurisdiction control properties
                if (PropertyValueExists(oLD, "ShowCourtChooser"))
                    this.ShowCourtChooser = Boolean.Parse(oLD["ShowCourtChooser"].ToString());
                if (PropertyValueExists(oLD, "CourtChooserUILabel"))
                    this.CourtChooserUILabel = oLD["CourtChooserUILabel"].ToString();
                if (PropertyValueExists(oLD, "CourtChooserHelpText"))
                    this.CourtChooserHelpText = oLD["CourtChooserHelpText"].ToString();
                if (PropertyValueExists(oLD, "CourtChooserControlProperties"))
                    this.CourtChooserControlProperties = oLD["CourtChooserControlProperties"].ToString();
                if (PropertyValueExists(oLD, "CourtChooserControlActions"))
                {
                    this.CourtChooserControlActions.SetFromString(oLD["CourtChooserControlActions"].ToString());
                }
                //get dialog options
                if (PropertyValueExists(oLD, "ShowSegmentDialog"))
                    this.ShowSegmentDialog = Boolean.Parse(oLD["ShowSegmentDialog"].ToString());
                if (PropertyValueExists(oLD, "DialogCaption"))
                    this.DialogCaption = oLD["DialogCaption"].ToString();
                if (PropertyValueExists(oLD, "DialogTabCaptions"))
                    this.DialogTabCaptions = oLD["DialogTabCaptions"].ToString();
                if (PropertyValueExists(oLD, "DefaultTrailerID"))
                    this.DefaultTrailerID = Int32.Parse(oLD["DefaultTrailerID"].ToString());

                //get language properties
                if (PropertyValueExists(oLD, "SupportedLanguages"))
                    this.SupportedLanguages = oLD["SupportedLanguages"].ToString();

                if (this.IsTopLevel || !this.LanguageIsSupported(this.Parent.Culture))
                {
                    if (PropertyValueExists(oLD, "Culture"))
                        this.Culture = Int32.Parse(oLD["Culture"].ToString());
                }
                else
                    this.Culture = this.Parent.Culture;

                if (PropertyValueExists(oLD, "LanguageControlActions"))
                {
                    this.LanguageControlActions.SetFromString(oLD["LanguageControlActions"].ToString());
                }

                //get collection table item properties
                if (this is CollectionTableItem)
                {
                    if (PropertyValueExists(oLD, "AllowSideBySide"))
                    {
                        ((CollectionTableItem)this).AllowSideBySide =
                            bool.Parse(oLD["AllowSideBySide"].ToString());
                    }
                }

                //get tag prefix id
                this.TagPrefixID = this.Nodes.GetItemTagPrefixID(xTagID);

                if (this.Definition == null)
                {
                    try
                    {
                        //get the segment def for this segment - one may not exist, e.g. when a segment has not yet been saved
                        //GLOG 4292: ID2 may not be 0 if this is a UserSegment created by Content Designer
                        this.Definition = (AdminSegmentDef)Segment.GetDefFromID(this.ID1 + "." + this.ID2);
                    }

                    catch { }

                    //pleading table segments may not have their own records in the database -
                    //get the segment def from the existing segment
                    if ((this is CollectionTable) && (this.Definition == null))
                    {
                        CollectionTable oPT = (CollectionTable)this;
                        this.Definition = oPT.GetSegmentDefFromSegment(oLD["Name"].ToString());
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSegment") + this.ID, oE);
            }

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns true iff the admin segment with the specified name exists
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public static bool Exists(string xName)
        {
            return SimpleDataCollection.GetScalar(
                "spSegmentsExistsByName", new object[] { xName }).ToString() == "-1";
        }
        #endregion
    }
    public sealed class AnswerFileSegments
    {
        #region *********************constants*********************
        private const char cSepItem = StringArray.mpEndOfField;
        private const char cSepVal = StringArray.mpEndOfValue;
        #endregion
        #region *********************fields*********************
        private System.Collections.Generic.List<AnswerFileSegment> m_aSegments;
        #endregion
        #region *********************constructors*********************
        public AnswerFileSegments(string xSegmentList)
        {
            m_aSegments = new System.Collections.Generic.List<AnswerFileSegment>();
            PopulateList(xSegmentList);
        }
        #endregion
        #region *********************properties*********************
        public int Count
        {
            get { return m_aSegments.Count; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// Add new item to internal list and return reference
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        /// <returns></returns>
        public AnswerFileSegment Add(string xSegmentID, Segment.InsertionLocations iLocation, 
            Segment.InsertionBehaviors iBehavior)
        {
            AnswerFileSegment oNew = new AnswerFileSegment(xSegmentID, iLocation, iBehavior);
            m_aSegments.Add(oNew);
            return oNew;
        }
        /// <summary>
        /// Remove item at specified position from internal list
        /// </summary>
        /// <param name="iIndex"></param>
        public void Remove(int iIndex)
        {
            try
            {
                m_aSegments.RemoveAt(iIndex);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex, oE);
            }
        }
        /// <summary>
        /// Moves an item to a new position in the list
        /// </summary>
        /// <param name="iCurIndex"></param>
        /// <param name="iNewIndex"></param>
        public void Move(int iCurIndex, int iNewIndex)
        {
            try
            {
                AnswerFileSegment oItem = m_aSegments[iCurIndex];

                m_aSegments.RemoveAt(iCurIndex);
                m_aSegments.Insert(iNewIndex, oItem);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iCurIndex, oE);
            }
        }
        public AnswerFileSegment Item(int i)
        {
            return m_aSegments[i];
        }
        public override string ToString()
        {
            string xTemp = "";
            foreach (AnswerFileSegment oSeg in m_aSegments)
            {
                xTemp = xTemp + oSeg.SegmentID + cSepVal + ((int)oSeg.InsertionLocation).ToString() +
                    cSepVal + ((int)oSeg.InsertionBehavior).ToString() + cSepItem;
            }
            xTemp = xTemp.TrimEnd(cSepItem);
            return xTemp;
        }
        private void PopulateList(string xInput)
        {
            if (xInput == "")
                return;
            string[] aItems = xInput.Split(cSepItem);
            foreach (string xItem in aItems)
            {
                string[] aVals = xItem.Split(cSepVal);
                AnswerFileSegment oNew = new AnswerFileSegment();
                oNew.SegmentID = aVals[0];
                if (aVals.GetUpperBound(0) > 0)
                    oNew.InsertionLocation = (Segment.InsertionLocations)Int32.Parse(aVals[1]);
                if (aVals.GetUpperBound(0) > 1)
                    oNew.InsertionBehavior = (Segment.InsertionBehaviors)Int32.Parse(aVals[2]);
                m_aSegments.Add(oNew);
            }
        }
        #endregion
    }
    public sealed class AnswerFileSegment
    {
        #region *********************fields*********************
        private string m_xSegmentID = "";
        private Segment.InsertionLocations m_iLocation = Segment.InsertionLocations.InsertInNewDocument;
        private Segment.InsertionBehaviors m_iBehavior = Segment.InsertionBehaviors.InsertInSeparateNextPageSection;
        #endregion
        #region *********************constructors*********************
        internal AnswerFileSegment()
        {
        }
        internal AnswerFileSegment(string xSegmentID, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iBehavior)
        {
            this.SegmentID = xSegmentID;
            this.InsertionLocation = iLocation;
            this.InsertionBehavior = iBehavior;
        }
        #endregion
        #region *********************properties*********************
        public string SegmentID
        {
            get { return m_xSegmentID; }
            set { m_xSegmentID = value; }
        }
        public Segment.InsertionLocations InsertionLocation
        {
            get { return m_iLocation; }
            set { m_iLocation = value; }
        }
        public Segment.InsertionBehaviors InsertionBehavior
        {
            get { return m_iBehavior; }
            set { m_iBehavior = value; }
        }
        #endregion
    }
}
