using System;
using System.Collections;
using System.Collections.Specialized;
using LMP.Data;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace LMP.Architect.Api
{
    ///// <summary>
    ///// contains properties that define the arguments for the AuthorAdded event
    ///// </summary>
    //public class AuthorAddedEventArgs
    //{
    //    private short m_shNewIndex = 0;

    //    public AuthorAddedEventArgs(short shNewIndex)
    //    {
    //        m_shNewIndex = shNewIndex;
    //    }

    //    public short NewIndex
    //    {
    //        get { return m_shNewIndex; }
    //        set { m_shNewIndex = value; }
    //    }
    //}

    ///// <summary>
    ///// contains properties that define the arguments for the LeadAuthorChanged event
    ///// </summary>
    //public class LeadAuthorChangedEventArgs
    //{
    //    private string m_xNewLeadAuthorID = null;

    //    public LeadAuthorChangedEventArgs(string xNewLeadAuthorID)
    //    {
    //        m_xNewLeadAuthorID = xNewLeadAuthorID;
    //    }

    //    public string NewLeadAuthorID
    //    {
    //        get{return m_xNewLeadAuthorID;}
    //        set{m_xNewLeadAuthorID = value;}
    //    }
    //}

    ///// <summary>
    ///// contains properties that define the 
    ///// arguments for the AuthorsReindexedEventArgs event - none
    ///// </summary>
    //public class AuthorsReindexedEventArgs{}
		

    ///// <summary>
    ///// declares the delegate for an LeadAuthorChanged event handler
    ///// </summary>
    //public delegate void LeadAuthorChangedHandler(object AuthorsCollection, 
    //    LeadAuthorChangedEventArgs oArgs);

    ///// <summary>
    ///// declares the delegate for an AuthorAdded event handler
    ///// </summary>
    //public delegate void AuthorAddedHandler(object AuthorsCollection, 
    //    AuthorAddedEventArgs oArgs);

    ///// <summary>
    ///// declares the delegate for an AuthorAdded event handler
    ///// </summary>
    //public delegate void AuthorsReindexedHandler(object AuthorsCollection, 
    //    AuthorsReindexedEventArgs oArgs);
	
	/// <summary>
	/// contains the methods and properties that define 
	/// a collection of MacPac Authors -
	/// created by Daniel Fisherman, 10/04
	/// </summary>
	public class Authors : LMP.Architect.Base.Authors
	{
		private Segment m_oSegment = null;

		//we define both a hashtable and an ArrayList because
		//none of the .NET collections allow us to call by
		//key and index, as well as allow us to move authors
		//by index - the best collection is the NameObjectCollectionBase, 
		//but there was no way to move authors around using this class
        private ControlActions m_oControlActions;
        public event LMP.Architect.Base.AuthorAddedHandler AuthorAdded = null;
        public event LMP.Architect.Base.LeadAuthorChangedHandler LeadAuthorChanged = null;
        public event LMP.Architect.Base.AuthorsReindexedHandler AuthorsReindexed = null;
		#region *********************constructors*********************
		public Authors(Segment oSegment)
		{
			m_oSegment = oSegment;
            m_oControlActions = new ControlActions(this);
		}
		#endregion
		#region *********************properties*********************
		/// <summary>
		/// sets/gets the parent MPObject for this authors collection
		/// </summary>
		public Segment Segment
		{
			get{return m_oSegment;}
			set{m_oSegment = value;}
		}

        public ControlActions ControlActions
        {
            get { return m_oControlActions; }
        }
		#endregion
		#region *********************methods*********************
        protected override void RaiseAuthorAddedEventIfNecessary()
        {
            //raise event
            if (AuthorAdded != null)
                AuthorAdded(this, new LMP.Architect.Base.AuthorAddedEventArgs((short)this.Count));
        }
        protected override void RaiseAuthorsReindexedEvent()
        {
            AuthorsReindexed(m_oSegment, new LMP.Architect.Base.AuthorsReindexedEventArgs());
        }
        protected override void RaiseAuthorsReindexedEventIfNecessary()
        {
            if (AuthorsReindexed != null)
                AuthorsReindexed(m_oSegment, new LMP.Architect.Base.AuthorsReindexedEventArgs());
        }
        protected override void RaiseLeadAuthorChangedEventIfNecessary(Author oNewLeadAuthor)
        {
            if (LeadAuthorChanged != null)
                LeadAuthorChanged(m_oSegment,
                    new LMP.Architect.Base.LeadAuthorChangedEventArgs(oNewLeadAuthor.ID));
        }

        //GLOG item #8027 - switched first param from Person to string - dcf
        protected override Person RaisePromptToCopyPublicPersonIfNecessary(string xPersonID1, LocalPersons oPersons, bool bIsLegacyAuthor)
        {
            //prompt to copy record from Network DB - prompt first time only
            //and only if the segment is not a child whose authors are linked
            //to the parent, and only if specified
            DialogResult iImportAuthors = DialogResult.None;

            if (this.Segment.PromptForMissingAuthors)
            {
                if (iImportAuthors == DialogResult.None &&
                    this.Segment.MaxAuthors > 0
                    && (this.Segment.Parent == null || !this.Segment.LinkAuthorsToParent))
                {
                    if (bIsLegacyAuthor)
                    {
                        //prompt to either copy author or set user as author
                        //GLOG 4538: Resource string modified to include a format token
                        //GLOG : 6482 : ceh
                        iImportAuthors = MessageBox.Show(
                            string.Format(LMP.Resources.GetLangString("Prompt_Authors_CopyFromNetworkDBOrSetUserAsAuthor"), 
                            this.Segment.DisplayName, xPersonID1, "you"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        //GLOG 7473 (dm) - moved the following two lines up into this branch,
                        //so that they only run when we've displayed a message - the unconditional
                        //call to DoEvents here was allowing documents to close prematurely
                        //during silent trailer insertion
                        System.Windows.Forms.Application.DoEvents();
                        LMP.Forte.MSWord.Application.RefreshScreen();
                    }
                    else
                    {

                        //GLOG 8198:  Don't prompt for deleted Public authors
                        //MessageBox.Show(LMP.Resources.GetLangString("Prompt_AuthorDoesNotExistUseUser"), LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return null;
                        ////prompt to copy author or do nothing
                        //iImportAuthors = MessageBox.Show(LMP.Resources.GetLangString("Prompt_Authors_CopyFromNetworkDB"),
                        //    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                }
                if (iImportAuthors == DialogResult.Yes)
                {
                    try
                    {
                        //GLOG 8276
                        if (MacPac.MacPacImplementation.IsFullLocal)
                            return oPersons.CopyPublicUser(int.Parse(xPersonID1), true, true);
                        else
                            return oPersons.CopyNetworkUser(int.Parse(xPersonID1), true);
                    }
                    catch
                    {
                        //couldn't copy author from network db - 
                        //alert user
                        //GLOG : 6482 : ceh
                        //string xAuthor = null;

                        //if (!string.IsNullOrEmpty(xFullName))
                        //    xAuthor = xFullName + " (" + iID1.ToString() + ")";
                        //else
                        //    xAuthor = iID1.ToString();

                        string xMsg = string.Format(LMP.Resources.GetLangString(
                            "Prompt_Authors_CouldNotCopyAuthor"));

                        MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        System.Windows.Forms.Application.DoEvents();
                        //GLOG 8198: If Network author could not be copied, set to User
                        return LMP.Data.Application.User.PersonObject;
                    }
                }
            }
            return null;
        }
		#endregion
	}
}
