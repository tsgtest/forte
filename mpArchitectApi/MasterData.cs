﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMP.Data;

namespace LMP.Architect.Api
{
    public class MasterData: AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, 
            Microsoft.Office.Interop.Word.Range oLocation, 
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, 
            bool bTargeted, LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.MasterData, this.ForteDocument); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
