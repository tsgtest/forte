using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools;
using LMP.MacPac;
using System.Drawing;
using Infragistics.Win;
using LMP.Architect.Api;

namespace LMP.MacPac102007
{
    /// <summary>
    /// manages the collection of office task panes 
    /// for the set of open Word Documents - this is
    /// not the collection MacPac task pane controls -
    /// that collection is managed by LMP.MacPac.TaskPanes
    /// </summary>
    internal static class TaskPanes
    {
        #region****************************fields****************************
        private static Hashtable m_oCustomTaskPanes = new Hashtable();
        private static CustomTaskPane m_oCurTaskPane;
        private static bool m_bShowingTaskPane = false; //GLOG 6195

        public static event LMP.Architect.Api.CurrentSegmentChangedEventHandler CurrentTopLevelSegmentSwitched;
        #endregion
        #region****************************properties****************************
        public static CustomTaskPane CurrentTaskPane
        {
            get
            {
                if (m_oCurTaskPane == null)
                    return null;

                try
                {
                    bool bTestVisible = m_oCurTaskPane.Visible;
                }
                catch
                {
                    //object has been disposed already
                    m_oCurTaskPane = null;
                }
                return m_oCurTaskPane;
            }
            set
            {

                try
                {
                    if (TaskPanes.CurrentTaskPane != null)
                    {
                        //save task pane width as a user app preference
                        LMP.MacPac.Session.CurrentUser.UserSettings.TaskPaneWidth = TaskPanes.CurrentTaskPane.Width.ToString();
                    }

                    if (value != null)
                    {
                        //set the new current task pane width
                        string xTPWidth = LMP.MacPac.Session.CurrentUser.UserSettings.TaskPaneWidth;

                        if (!string.IsNullOrEmpty(xTPWidth))
                        {
                            //there is a user app preference value - use it
                            value.Width = int.Parse(xTPWidth);
                        }
                        else
                        {
                            //no value set yet -
                            //set task pane width - sliding width - for screens with width 1024,
                            //task pane width = 30% of active window - for larger resolutions,
                            //screen width varies up to 35% of active window
                            int iWidth = Math.Min(Screen.PrimaryScreen.Bounds.Width, 1280);
                            double dFactor = .05 * ((iWidth - 1024) / (1280 - 1024));

                            value.Width = Math.Max(value.Width,
                                (int)(ThisAddIn.WordApplication.ActiveWindow.Width * (.30 + dFactor)));
                        }
                    }
                }
                catch { }

                m_oCurTaskPane = value;
				//GLOG 7814
                if (m_oCurTaskPane != null)
                {
                    LMP.MacPac.Application.ActiveTaskPane = (LMP.MacPac.TaskPane)m_oCurTaskPane.Control;
                }
                else
                {
                    LMP.MacPac.Application.ActiveTaskPane = null;
                }
            }
        }
        #endregion
        #region****************************methods****************************
        /// <summary>
        /// returns the number of task panes in the collection
        /// </summary>
        public static int Count
        {
            get { return m_oCustomTaskPanes.Count; }
        }
        /// <summary>
        /// returns the task pane associated with the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static CustomTaskPane Item(Word.Document oDoc)
        {
            if (m_oCustomTaskPanes[oDoc] != null)
            {
                CustomTaskPane oTaskPane = (CustomTaskPane)m_oCustomTaskPanes[oDoc];
                try
                {
                    bool bTestVisible = oTaskPane.Visible;
                }
                catch (ObjectDisposedException)
                {
                    //object has been disposed already
                    return null;
                }
                return oTaskPane;
            }
            else
                return null;
        }
        /// <summary>
        /// hides and shows the appropriate task panes
        /// </summary>
        public static LMP.MacPac.TaskPane ShowTaskPaneIfNecessary(Word.Document oDoc)
        {
            DateTime t0 = DateTime.Now;

            try
            {   
                if (!LMP.MacPac.Session.IsInitialized && LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
                {
                    //we need to initialize session to see whether or not
                    //to show the task pane for this template
                    LMP.MacPac.Session.StartIfNecessary(oDoc.Application);
                }

                if (oDoc.Name.EndsWith(".dotx") || oDoc.Name.EndsWith(".dotm") ||
                    oDoc.Name.EndsWith(".dot") ||

                //GLOG 15875:  If Protected document already has TaskPane created it should be redisplayed
                //Don't display for protected document
                //(LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false) ||

                ////JTS 1/19/11: For the moment, treat Documents with revisions the same as protected
                //LMP.Architect.Api.Environment.DocumentHasRevisions(oDoc, false)) ||

                //GLOG 5443 (dm) - don't show if docx file doesn't support content controls
                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, false) ||
                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, false) ||
                (LMP.MacPac.Session.CurrentUser == null))
                {
                    ThisAddIn.WordApplication.ScreenUpdating = true;
                    RemoveUnused(); //GLOG 5803 (dm)
                    return null;
                }

                LMP.MacPac.TaskPane oTP = null;

                if (oDoc != null)
                {
                    //look up existing task pane
                    object oAssocTaskPane = m_oCustomTaskPanes[oDoc];

                    if (oAssocTaskPane != null)
                    {
                        //task pane was already created for document - show
                        CurrentTaskPane = (CustomTaskPane)oAssocTaskPane;

                        try
                        {
                            oTP = (LMP.MacPac.TaskPane)CurrentTaskPane.Control;
                        }
                        catch { }

                        if (oTP != null)
                        {
                            //GLOG 2893: Don't redisplay Taskpane if it had been manually closed
                            //Or if we have specified not to show the Taskpane (as in Pierce
                            //insertion using Client Matter number)
                            if (oTP.AutoClosed && !oTP.DoNotShowOnDocChange)
                            {
                                CurrentTaskPane.Visible = true;
                                oTP.Visible = true;
                                oTP.AutoClosed = false;
                            }
                        }
                        else
                        {
                            CurrentTaskPane.Visible = true;
                            oDoc.Activate();
                        }
                    }
                    //GLOG 15875: Don't create TaskPane for Protected Document
                    else if (!LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false) && ShouldShowTaskPane(oDoc) && !TaskPane.HideForNewDocument)
                    {
                        //ensure that the MacPac 10 Document Schema is attached
                        LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();

                        //add the task pane
                        oTP = Add();
                    }
                }

                ThisAddIn.WordApplication.ScreenUpdating = true;

                //remove unused task panes
                RemoveUnused();

                return oTP;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordIntegrationException(
                    LMP.Resources.GetLangString("Error_CouldNotIntegrateCorrectlyWithWord"), oE);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// shows the task pane for the active document
        /// </summary>
        public static void ShowTaskPane()
        {
            //GLOG 6195: Prevent displaying multiple taskpanes
            if (m_bShowingTaskPane)
                return;

            if (TaskPanes.CurrentTaskPane != null)
            {
                if (!TaskPanes.CurrentTaskPane.Visible)
                    TaskPanes.CurrentTaskPane.Visible = true;
            }
            else
            {
                m_bShowingTaskPane = true; //GLOG 6195
                try
                {
                    Word.Document oDoc = null;

                    try
                    {
                        oDoc = ThisAddIn.WordApplication.ActiveDocument;
                    }
                    catch { }

                    if (oDoc == null)
                    {
                        //no active document - alert
                        string xCaption = LMP.ComponentProperties.ProductName;
                        string xMsg = LMP.Resources.GetLangString("Msg_ActiveDocumentRequired");
                        MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    ForteDocument oMPDocument = new LMP.Architect.Api.ForteDocument(oDoc);
                    oMPDocument.Refresh(LMP.Architect.Api.ForteDocument.RefreshOptions.None);

                    if (oMPDocument.Segments.Count > 0)
                    {
                        //set current segment
                        Segment oNewSegment = oMPDocument.GetTopLevelSegmentForRange(
                            oDoc.Application.Selection.Range, true);

                        ThisAddIn.CurrentSegment = oNewSegment;
                    }

                    TaskPanes.ShowTaskPane(oDoc);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.UIException(
                        LMP.Resources.GetLangString("Error_CouldNotDisplayTaskPane"), oE);
                }
                finally
                {
                    m_bShowingTaskPane = false; //GLOG 6195
                }
            }
        }
        /// <summary>
        /// hides and shows the appropriate task panes
        /// </summary>
        public static TaskPane ShowTaskPane(Word.Document oDoc)
        {
            TaskPane oTP = null;
            
            try
            {
                if (oDoc != null)
                {
                    //look up existing task pane
                    object oAssocTaskPane = m_oCustomTaskPanes[oDoc];

                    if (oAssocTaskPane != null)
                    {
                        //task pane was already created for document - show
                        CurrentTaskPane = (CustomTaskPane)oAssocTaskPane;

                        CurrentTaskPane.Visible = true;

                        oTP = CurrentTaskPane.Control as TaskPane;
                    }
                    else
                    {
                        //removed 11/18/09 - dcf
                        //ensure that the MacPac 10 Document Schema is attached
                        //LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();

                        //add the task pane
                        oTP = Add();
                    }
                }

                ThisAddIn.WordApplication.ScreenUpdating = true;

                //remove unused task panes
                RemoveUnused();

                return oTP;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordIntegrationException(
                    LMP.Resources.GetLangString("Error_CouldNotIntegrateCorrectlyWithWord"), oE);
            }
        }
        /// <summary>
        /// Hides the task pane associated with the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        public static void HideTaskPane(Word.Document oDoc)
        {
            CustomTaskPane oTaskPane = Item(oDoc);
            if (oTaskPane != null)
                oTaskPane.Visible = false;
        }
        public static void HideAllTaskPanes()
        {
            CustomTaskPaneCollection oCol = ThisAddIn.CustomWordTaskPanes;
            for (int i = oCol.Count-1; i >= 0; i--)
                oCol.RemoveAt(i);

            m_oCustomTaskPanes.Clear();
        }
        /// <summary>
        /// hides the currently visible task pane
        /// </summary>
        public static void HideCurrentTaskPane()
        {
            DateTime t0 = DateTime.Now;

            try
            {
                if (CurrentTaskPane != null && CurrentTaskPane.Visible)
                {
                    CurrentTaskPane.Visible = false;
                    try
                    {
                        LMP.MacPac.TaskPane oTP = (LMP.MacPac.TaskPane)CurrentTaskPane.Control;
                        //GLOG 2893: Mark taskpane as having been closed by Document Change event
                        oTP.AutoClosed = true;
                    }
                    catch { }

                    CurrentTaskPane = null;

                    //GLOG 3389: If no document windows are open, remove unused TaskPane objects
                    if (LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
                        RemoveUnused();
                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns the TaskPane collection
        /// </summary>
        /// <returns></returns>
        public static List<LMP.MacPac.TaskPane> Collection()
        {
            List<LMP.MacPac.TaskPane> oList = new List<LMP.MacPac.TaskPane>();

            IEnumerator oEnum = m_oCustomTaskPanes.Values.GetEnumerator();

            while (oEnum.MoveNext())
                oList.Add((LMP.MacPac.TaskPane)oEnum.Current);

            return oList;
        }
        /// <summary>
        /// refreshes all task panes
        /// </summary>
        public static void RefreshAll()
        {
            //refresh content manager and appearance of all task panes
            IEnumerator oEnum = m_oCustomTaskPanes.Values.GetEnumerator();
            while (oEnum.MoveNext())
                ((TaskPane)((CustomTaskPane)oEnum.Current).Control).Refresh(true, false, true);

            if (ThisAddIn.WordApplication.Documents.Count > 0)
            {
                //refresh editor or designer of active task pane -
                //we do only the active task pane because it would
                //take too long to do all - also, refreshing the designer
                //or editor trees isn't usually necessary for non-active
                //taskpanes
                TaskPane oActiveTP = LMP.MacPac.TaskPanes.Item(
                    ThisAddIn.WordApplication.ActiveDocument);

                if (oActiveTP != null)
                    oActiveTP.Refresh(false, true, false, true); //GLOG 4766: Update Segment tab for current selection
            }
        }
        /// <summary>
        /// refreshes the task pane associated with the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        public static void Refresh(Word.Document oDoc)
        {
            ((TaskPane)((CustomTaskPane)m_oCustomTaskPanes[oDoc]).Control).Refresh();
        }
        /// <summary>
        /// removes task panes that no longer have an associated document
        /// </summary>
        private static void RemoveUnused()
        {
            ICollection oDocs = m_oCustomTaskPanes.Keys;
            List<Word.Document> oNonExistentDocs = new List<Word.Document>();

            //cycle through hashtable entries, 
            //looking for docs that have been closed
            foreach (Word.Document oDoc in oDocs)
            {
                try
                {
                    //this line will err if the doc
                    //no longer exists
                    string xName = oDoc.Name;
                }
                catch
                {
                    //add entry to the list of entries
                    //to be deleted from the hashtable
                    oNonExistentDocs.Add(oDoc);
                }
            }

            //cycle through list of entries to delete
            foreach (Word.Document oDoc in oNonExistentDocs)
            {
                //dispose of taskpane
                CustomTaskPane oTaskPane = (CustomTaskPane)m_oCustomTaskPanes[oDoc];
                //GLOG 3389: Remove MacPac.TaskPane object as well
                LMP.MacPac.TaskPanes.Remove(oDoc);

                oTaskPane.Dispose();

                //delete entry in hashtable
                m_oCustomTaskPanes.Remove(oDoc);
            }
        }
        /// <summary>
        /// returns true iff we should show the 
        /// task pane for the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        private static bool ShouldShowTaskPane(Word.Document oDoc)
        {
            if (oDoc.ActiveWindow != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
            {
                return false;
            }
            else if (oDoc.Path == "")
            {
                if (!LMP.MacPac.Session.CurrentUser.UserSettings.ShowTaskPaneWhenCreatingBlankMPDocument)
                {
                    //don't show taskpane if template is ForteNormal.dot(x) and
                    //no pending action has been defined to work on this document
                    Word.Template oTemplate = (Word.Template)oDoc.get_AttachedTemplate();
                    //GLOG 7945:  Make sure all comparison strings are lowercase
                    if ((oTemplate.Name.ToLower() == "fortenormal.dotx" || oTemplate.Name.ToLower() == "fortenormal.dot" || oTemplate.Name.ToLower() == "macpacnormal.dotx" || oTemplate.Name.ToLower() == "macpacnormal.dot")
                        && !LMP.MacPac.TaskPane.HasPendingAction)
                    {
                        ThisAddIn.WordApplication.ScreenUpdating = true;
                        return false;
                    }
                }

                return LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc);
            }
            else
            {
                //document is being opened-
                if (LMP.MacPac.Session.IsInitialized)
                {
                    //check if document contains macpac content
                    bool bContainsContent = false;
                    try
                    {
                        bContainsContent = LMP.Forte.MSWord.WordDoc
                             .ContainsNonTrailerMPContent(oDoc);
                    }
                    catch { }

                    if (bContainsContent)
                        return LMP.MacPac.Session.CurrentUser
                            .UserSettings.ShowTaskPaneWhenOpeningMPDoc;
                    else
                    {
                        try
                        {
                            //GLOG : 6573 : JSW
                            //this may be a round tripped 10.1.2x doc
                            //if so, it needs to be reconstituted
                            if (LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
                            {
                                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                                //GLOG 6801 (dm): don't reconstitute mSEGs
                                bool bReconstituted = oConvert.ReconstituteIfNecessary(
                                    oDoc, false, false);
                                if (bReconstituted)
                                {
                                    if (LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(oDoc) == true)
                                        return LMP.MacPac.Session.CurrentUser
                                            .UserSettings.ShowTaskPaneWhenOpeningMPDoc;
                                }
                            }
                        }
                        catch { }

                        return LMP.MacPac.Session.CurrentUser
                        .UserSettings.ShowTaskPaneWhenOpeningNonMPDoc;
                    }
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// adds a task pane for the active document -
        /// returns the LMP task pane that was added
        /// </summary>
        private static LMP.MacPac.TaskPane Add()
        {
            return TaskPanes.Add(true);
        }

        /// <summary>
        /// adds a task pane for the active document -
        /// returns the LMP task pane that was added
        /// </summary>
        private static LMP.MacPac.TaskPane Add(bool bVisible)
        {
            DateTime t0 = DateTime.Now;

            Word.Document oDoc = null;

            try
            {
                oDoc = ThisAddIn.WordApplication.ActiveDocument;
            }
            catch
            {
                return null;
            }

            //task pane does not exist - create
            //LMP.MacPac.TaskPane2007 oLMPTaskPane =
            //    new LMP.MacPac.TaskPane2007(ThisAddIn.WordApplication);
            LMP.MacPac.TaskPane oLMPTaskPane = new LMP.MacPac.TaskPane();

            oLMPTaskPane.CurrentTopLevelSegmentSwitched += 
                new LMP.Architect.Api.CurrentSegmentChangedEventHandler(
                    oLMPTaskPane_CurrentTopLevelSegmentSwitched);

            CustomTaskPane oCustomTaskPane = ThisAddIn.CustomWordTaskPanes.Add(
                oLMPTaskPane, LMP.ComponentProperties.ProductName);

            //set the Word task pane of this macpac task pane
            oLMPTaskPane.WordTaskPane = oCustomTaskPane;

            CurrentTaskPane = oCustomTaskPane;
            
            //show task pane
            //GLOG 7201 (dm) - added bVisible parameter
            //GLOG item #7757 - dcf - I moved this line below
            //CurrentTaskPane = oCustomTaskPane - in Word 2013, this
            //was causing the task pane to display twice
            oCustomTaskPane.Visible = bVisible;

            //add to hashtable for future use
            if (!m_oCustomTaskPanes.ContainsKey(oDoc))
            {
                m_oCustomTaskPanes.Add(oDoc, oCustomTaskPane);
            }

            LMP.Benchmarks.Print(t0);

            return oLMPTaskPane;
        }

        static void oLMPTaskPane_CurrentTopLevelSegmentSwitched(object sender, LMP.Architect.Api.CurrentSegmentSwitchedEventArgs args)
        {
            if (TaskPanes.CurrentTopLevelSegmentSwitched != null)
                TaskPanes.CurrentTopLevelSegmentSwitched(sender, args);
        }

        /// <summary>
        /// creates a hidden task pane for TOC/TOA if necessary
        /// </summary>
        public static LMP.MacPac.TaskPane AddHiddenTaskPaneIfNecessary(Word.Document oDoc)
        {
            //GLOG 7256 (dm) - added return value
            DateTime t0 = DateTime.Now;

            try
            {
                LMP.MacPac.TaskPane oTP = null;

                if (oDoc == null)
                    return null;
                
                if (!LMP.MacPac.Session.IsInitialized && LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
                {
                    LMP.MacPac.Session.StartIfNecessary(oDoc.Application);
                }

                if (oDoc.Name.EndsWith(".dotx") || oDoc.Name.EndsWith(".dotm") ||
                    oDoc.Name.EndsWith(".dot") ||

                //Don't display for protected document
                (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false) ||

                //GLOG 5443 (dm) - don't show if docx file doesn't support content controls
                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, false)) ||
                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, false) ||
                (LMP.MacPac.Session.CurrentUser == null))
                {
                    ThisAddIn.WordApplication.ScreenUpdating = true;
                    RemoveUnused(); //GLOG 5803 (dm)
                    return null;
                }

                //if no existing task pane, create one if there's content
                object oAssocTaskPane = m_oCustomTaskPanes[oDoc];
                if (oAssocTaskPane != null)
                {
                    //GLOG 7184 (dm) - we weren't returning existing task pane
                    //task pane was already created for document - show
                    CurrentTaskPane = (CustomTaskPane)oAssocTaskPane;
                    try
                    {
                        oTP = (LMP.MacPac.TaskPane)CurrentTaskPane.Control;
                    }
                    catch { }
                    return oTP;
                }
                else
                {
                    //check if document contains macpac content
                    bool bContainsContent = false;
                    try
                    {
                        bContainsContent = LMP.Forte.MSWord.WordDoc
                             .ContainsNonTrailerMPContent(oDoc);
                    }
                    catch { }

                    //doc may need to be reconstituted
                    if (!bContainsContent)
                    {
                        try
                        {
                            if (LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
                            {
                                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                                bContainsContent = oConvert.ReconstituteIfNecessary(
                                    oDoc, false, false);
                            }
                        }
                        catch { }
                    }

                    if (bContainsContent)
                    {
                        ThisAddIn.WordApplication.ScreenUpdating = false;

                        //ensure that the MacPac 10 Document Schema is attached
                        LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();

                        //add hidden task pane
                        oTP = Add(false);
                    }
                }

                ThisAddIn.WordApplication.ScreenUpdating = true;

                //remove unused task panes
                RemoveUnused();

                return oTP;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordIntegrationException(
                    LMP.Resources.GetLangString("Error_CouldNotIntegrateCorrectlyWithWord"), oE);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }
        #endregion
    }
}
