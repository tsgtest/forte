using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using LMP.MacPac;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac.Word2007
{
    /// <summary>
    ///  class interface LMP.MacPac.Application -
    ///  allows COM applications, specifically
    ///  Word templates to act on the MacPac10 application
    /// </summary>
    [ComVisible(true)]
    [Guid("DBE28ECA-3B55-43b4-BD29-840F3D9E9045")]
    public interface IMP10WordCommands
    {
        void EditPaste(Word.Document oDocument);
        void EditUndo(Word.Document oDocument);
        void EditRedo(Word.Document oDocument);
        void NextCell(Word.Document oDocument);
        void ToggleFormsCheckbox();
        void ToggleFormsCheckboxSet();
        void ExecuteMenuCommand(string CommandID);
    }
    /// <summary>
    /// LMP.MacPac.Application class-
    /// defines the top level class for MacPac 10 -
    /// exposes event handlers and other methods to COM -
    /// created by Daniel Fisherman - 08/22/05
    /// </summary>
    [ComVisible(true)]
    [Guid("364CAAEC-1B64-4ba4-BD97-6DC34CC9BE0D"),
    ClassInterface(ClassInterfaceType.None)]
    public class WordCommands:IMP10WordCommands
    {
        #region IMPApplication Members

        public void EditPaste(Word.Document oDocument)
        {
            LMP.MacPac.Application.EditPaste(oDocument);
        }

        public void EditUndo(Word.Document oDocument)
        {
            LMP.MacPac.Application.EditUndo(oDocument);
        }

        public void EditRedo(Word.Document oDocument)
        {
            LMP.MacPac.Application.EditRedo(oDocument);
        }

        public void NextCell(Word.Document oDocument)
        {
            LMP.MacPac.Application.NextCell(oDocument);
        }

        public void ToggleFormsCheckbox()
        {
            LMP.MacPac.Application.ToggleFormsCheckbox();
        }

        public void ToggleFormsCheckboxSet()
        {
            LMP.MacPac.Application.ToggleFormsCheckboxSet();
        }

        public void ExecuteMenuCommand(string CommandID)
        {
            throw new Exception("The method or operation is not implemented for Word 2007.");
        }
        #endregion
    }
}
