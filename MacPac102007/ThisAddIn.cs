using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using LMP.Data;
using LMP.MacPac;
using LMP.Architect.Api;

namespace LMP.MacPac102007
{
    public partial class ThisAddIn
    {
        #region****************************fields****************************
        private Word.ApplicationEvents4_DocumentChangeEventHandler m_oDocChangeHandler;
        private Word.ApplicationEvents4_DocumentOpenEventHandler m_oDocOpenHandler;
        private Word.ApplicationEvents4_WindowSelectionChangeEventHandler m_oWindowSelectionChangeHandler;
        private Word.ApplicationEvents4_XMLSelectionChangeEventHandler m_oAppXMLSelectionChangeHandler;
        private Word.ApplicationEvents4_DocumentBeforeSaveEventHandler m_oDocBeforeSaveHandler;
        private MacPac10Ribbon m_oRibbon;
        private static CustomTaskPaneCollection m_oCustomWordTaskPanes;
        private static bool m_bIsValid = false;
        private static bool m_bDisableEventHandlers = false;
        private static AddinObject m_oAddinObject;
        static private Segment m_oCurrentSegment;
        static private Segment m_oPreviousSegment;
        private bool bEnteringNonMainPane = false;
        private LMP.MacPac.PacketRequestListener m_oPacketBuilder;
        #endregion
        #region****************************event handlers****************************
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            try
            {
                //10/12/09 JTS: Need to run this even if Word session has been started with CreateObject
                //if (!this.Application.Visible)
                //    return;

                Trace.WriteInfo("Executing LMP.MacPac102007.ThisAddIn.ThisAddIn_Startup");

                try
                {
                    //GLOG item #4335 - dcf
                    //set friendly name so ribbon menu items don't show MacPac102007 -
                    //skip if user doesn't have permission to write value
                    //GLOG : 8078 : jsw
                    //registry entry no longer located under HKCU
                    //LMP.Registry.SetCurrentUserValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn",
                    //    "FriendlyName", LMP.ComponentProperties.ProductName);
                }
                catch { }

                //prompt for license number if necessary
                if (LMP.MacPac.Application.MacPacIsLegal())
                {
                    m_bIsValid = true;
                }
                else
                {
                    m_bIsValid = false;

                    MessageBox.Show(LMP.Resources.GetLangString("Msg_EvalHasExpired"),
                       LMP.ComponentProperties.ProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return;
                }


				//GLOG item #4850 - jsw
                //copy startup templates to startup template folder if necessary
                LMP.MacPac.Application.CopyStartupTemplates();
                
                //10/12/09 JTS: If Word session is invisible, Ribbon object may not be created
                if (m_oRibbon != null && m_oRibbon.Ribbon != null)
                    m_oRibbon.Ribbon.Invalidate();

                ThisAddIn.WordApplication = this.Application;
                m_oDocChangeHandler = new Word.ApplicationEvents4_DocumentChangeEventHandler(OnDocumentChange);
                this.Application.DocumentChange += m_oDocChangeHandler;

                m_oWindowSelectionChangeHandler = new Word.ApplicationEvents4_WindowSelectionChangeEventHandler(OnWindowSelectionChange);
                this.Application.WindowSelectionChange += m_oWindowSelectionChangeHandler;

                m_oAppXMLSelectionChangeHandler = new Word.ApplicationEvents4_XMLSelectionChangeEventHandler(OnXMLSelectionChange);
                this.Application.XMLSelectionChange += m_oAppXMLSelectionChangeHandler;

                //DocumentBeforeSave handler (3/10/11, dm)
                m_oDocBeforeSaveHandler = new Word.ApplicationEvents4_DocumentBeforeSaveEventHandler(OnDocumentBeforeSave);
                this.Application.DocumentBeforeSave += m_oDocBeforeSaveHandler;

                //Set DocumentOpen event handler
                string xRegKey = Registry.MacPac10RegistryRoot + "\\DMSOverrides";
                if (Registry.GetLocalMachineValue(xRegKey, "FileOpen") != "1")
                {
                    m_oDocOpenHandler = new Word.ApplicationEvents4_DocumentOpenEventHandler(OnDocumentOpen);
                    this.Application.DocumentOpen += m_oDocOpenHandler;
                }
                //10/12/09 JTS: If Word Session was created invisible with CreateObject, AutoExec macros will
                //not run.  Need to run Forte AutoExec directly to initialize session properly.
                if (!this.Application.Visible)
                {
                    Trace.WriteInfo("Executing AutoExec for Invisible Word session");
                    try
                    {
                        LMP.Forte.MSWord.WordApp.CallMacro("MP10.mdlfc.AutoExec", "", "_");
                    }
                    catch { }
                }

                TaskPanes.CurrentTopLevelSegmentSwitched += 
                    new CurrentSegmentChangedEventHandler(TaskPanes_CurrentTopLevelSegmentSwitched);

                if (LMP.MacPac.MacPacImplementation.IsServer)
                {
                    LMP.MacPac.Session.StartIfNecessary(ThisAddIn.WordApplication);
                    m_oPacketBuilder = new PacketRequestListener(5000);
                    //m_oPacketBuilder.StartListening();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void TaskPanes_CurrentTopLevelSegmentSwitched(object sender, CurrentSegmentSwitchedEventArgs args)
        {
            m_oCurrentSegment = args.NewSegment;
            m_oRibbon.Ribbon.Invalidate();
            LMP.MacPac.Application.MacPac10Ribbon = m_oRibbon.Ribbon;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            try
            {
                //JTS 3/21/10: Moved from Taskpane so that On-Demand uploade will
                //occur even if TaskPane was not displayed
                if (Session.IsInitialized)
                {
                    try
                    {
                        string xSkipSync = Registry.GetMacPac10Value("SkipSyncOnExit");

                        //GLOG 3623: Perform upload-only On-Demand Sync at exit
                        //GLOG item #7200 - dcf - sync on exit on in a MacPac
                        //implementation that supports network functionality
                        if (LMP.MacPac.MacPacImplementation.IsFullWithNetworkSupport &&
                            !Session.AdminMode && (string.IsNullOrEmpty(xSkipSync) || xSkipSync != "1"))
                            LMP.MacPac.Application.ExecuteOnDemandSync(true);
                    }
                    catch
                    {
                        //Ignore any error at this point - Network may not be available
                        //if Windows is shutting down
                    }
                    LMP.Data.Application.Logout(); //GLOG 7546
                    OS.DeleteTemporaryFiles();

                    //quit CI if necessary
                    LMP.MacPac.Application.ReleaseCIObjects();
                }
                m_oRibbon = null;
                this.Application.DocumentChange -= m_oDocChangeHandler;
                this.Application.WindowSelectionChange -= m_oWindowSelectionChangeHandler;
            }
            catch { }
            finally
            {
                try
                {
                    //GLOG item #4335 - dcf
                    //set friendly name so ribbon menu items don't show MacPac102007-
                    //VSTO seems to write this key back to MacPac102007 on exit of addin -
                    //skip if user doesn't have permission to write value
                    //GLOG : 8078 : jsw
                    //registry entry no longer located under HKCU
                    //LMP.Registry.SetCurrentUserValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn",
                    //    "FriendlyName", LMP.ComponentProperties.ProductName);
                }
                catch { }
            }
        }
        private void OnDocumentChange()
        {
            DateTime t0 = DateTime.Now;
            LMP.Benchmarks.Print(t0, "OnDocumentChange start");

            bool bDisableEventHandlers = ThisAddIn.DisableEventHandlers;

            //JTS 7/26/10: Don't handle event if Word session is invisible
            if (bDisableEventHandlers || Application.Visible != true)
                return;

            try
            {
                //Prevent recursive calls
                ThisAddIn.DisableEventHandlers = true;
                Trace.WriteInfo("Executing LMP.MacPac102007.ThisAddIn.OnDocumentChange");

                //hide existing task pane, if one exists
                TaskPanes.HideCurrentTaskPane();
                TaskPanes.CurrentTaskPane = null;

                Word.Document oDoc = null;

                try
                {
                    oDoc = Application.ActiveDocument;
                }
                catch { }

                //show task pane if appropriate
                if (oDoc != null)
                {
                    //GLOG : 6398 : CEH
                    //don't run event when in Reviewing pane - 
                    if (LMP.Forte.MSWord.WordDoc.InReviewingPane(oDoc))
                        return;
                    //GLOG item #6023 - dcf
                    else if (oDoc.ActiveWindow.ActivePane.Index > 1 && !bEnteringNonMainPane)
                    {
                        //we're not in the main pane - our code below,
                        //specifically the call to cWordDoc.HasRevisions(),
                        //causes the document change event to fire when
                        //comments are shown inline with text - this causes
                        //OnDocumentChange() to fire infinitely - to fix,
                        //set a flag here that indicates that we're not in
                        //the main pane - on the subsequent iteration of
                        //OnDocumentChange() we will simply exit, thus preventing
                        //HasRevisions() from being called - we need to allow
                        //OnDocumentChange() to execute once since we may actually
                        //be changing focus from a different document.
                        bEnteringNonMainPane = true;
                    }
                    else if (oDoc.ActiveWindow.ActivePane.Index > 1)
                    {
                        bEnteringNonMainPane = false;
                        return;
                    }

                    LMP.MacPac.TaskPane oMPTaskPaneControl = null;

                    if (oDoc.Path == "")
                    {
                        ////GLOG item #5582 and #5585 - dcf
                        //if (oDoc.ActiveWindow != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow)
                        //    && oDoc.Name != "")
                        //GLOG 5601 (dm) - if a comparison is configured to Hide Source Documents,
                        //there's no reliable way to identify the comparison results document when
                        //this event fires after creation - check for revisions instead
                        if ((oDoc.ActiveWindow != null) && (oDoc.Name != "") && 
                            !(((oDoc.Name.StartsWith("Document") && oDoc.Name.Length < 12) || 
                            (oDoc.Name.StartsWith("Form Letters") && oDoc.Name.Length < 16)) && 
                            String.IsNumericInt32(oDoc.Name.Substring(oDoc.Name.Length - 1, 1))) &&  //GLOG 6698
                            LMP.Forte.MSWord.WordDoc.HasRevisions(oDoc))
                        {
                            //document is the main pane of the comparison document -
                            //remove all tags by removing document schema -
                            //fyi, comparison source document windows have oDoc.Name == ""
                            try
                            {
                                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();

                                //GLOG 6513 (dm) - removing the schema crashes Word 2010
                                //when comparing .doc letters or faxes and is not
                                //necessary to remove the tags - it doesn't crash and is
                                //necessary in Word 2007
                                if (LMP.Forte.MSWord.WordApp.Version < 14)
                                {
                                    object oSchema = "urn-legalmacpac-data/10";
                                    oDoc.XMLSchemaReferences.get_Item(ref oSchema).Delete();
                                }

                                LMP.MacPac.Application.RemoveBoundingObjects(oDoc,
                                    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, false);
                                //GLOG 6288:  Comparison may have automatically removed some Content
                                //Controls if they're at the start of a table, leaving orphaned mVars and mBlocks
                                //that have not been removed by RemoveBoundingObjects.  Cycle through 
                                //ContentControls to clean these up.  Non-MacPac Content Controls will be left in place.
                                LMP.Forte.MSWord.WordDoc.RemoveContentControlsInDocument(oDoc, false);
                                //prevent reconstitution by removing bookmarks and doc vars
                                oConvert.DeleteBookmarksAndDocVars(oDoc);
                            }
                            catch { }
                        }
                        else
                        {
                            //GLOG 7298: If Ribbon-only, don't display taskpane when changing to
                            //an unsaved document unless it was already displayed
                            if (MacPac.MacPacImplementation.IsToolkit)
                            {
                                CustomTaskPane oTaskPane = TaskPanes.Item(oDoc);
                                if (oTaskPane != null)
                                {
                                    oMPTaskPaneControl = TaskPanes.ShowTaskPaneIfNecessary(oDoc);
                                }
                            }
                            else
                            {
                                //this may be a new doc - determine whether
                                //to show a task pane
                                oMPTaskPaneControl = TaskPanes.ShowTaskPaneIfNecessary(oDoc);
                            }
                        }
                    }
                    else
                    {
                        //if active doc has a task pane, show it now
                        CustomTaskPane oWordCustomTaskPane = TaskPanes.Item(oDoc);
                        if (oWordCustomTaskPane != null)
                        {
                            try
                            {
                                oMPTaskPaneControl = (LMP.MacPac.TaskPane)oWordCustomTaskPane.Control;
                            }
                            catch { }

                            if (oMPTaskPaneControl != null)
                            {
                                //GLOG 2893: Don't redisplay TaskPane if it had been manually closed
                                //GLOG 7103 (dm): if toolkit, display only if there's an
                                //unfinished segment of a supported type
                                bool bDisplay = !LMP.MacPac.MacPacImplementation.IsToolkit;
                                if (!bDisplay)
                                {
                                    Segments oSegments = oMPTaskPaneControl.ForteDocument.Segments;
                                    for (int i = 0; i < oSegments.Count; i++)
                                    {
                                        if (LMP.Data.Application.DisplayInToolkitTaskPane(oSegments[i].TypeID) &&
                                            !oSegments[i].IsFinished)
                                        {
                                            bDisplay = true;
                                            break;
                                        }
                                    }
                                }
                                if (oMPTaskPaneControl.AutoClosed && bDisplay && !oMPTaskPaneControl.DoNotShowOnDocChange)
                                {
                                    oMPTaskPaneControl.AutoClosed = false;
                                    oWordCustomTaskPane.Visible = true;
                                }
                            }
                            else if (!LMP.MacPac.MacPacImplementation.IsToolkit)
                                oWordCustomTaskPane.Visible = true;

                            //04-10-12 (dm) - prevent multiple taskbars when switching
                            //between documents with "show all windows in taskbar"
                            //turned off - the old task pane wasn't getting hidden
                            //because CurrentTaskPane was null
                            if (!Application.ShowWindowsInTaskbar)
                                TaskPanes.CurrentTaskPane = oWordCustomTaskPane;
                        }
                    }

                    if (oMPTaskPaneControl != null)
                    {
                        //set current segment
                        bool bSaved = oMPTaskPaneControl.ForteDocument.WordDocument.Saved; //GLOG 6851 (dm)
                        //GLOG 6005 (dm) - accept all "as document" segments as top-level
                        Segment oNewSegment = oMPTaskPaneControl.ForteDocument.GetTopLevelSegmentForRange(
                            oDoc.Application.Selection.Range, true);
                        oMPTaskPaneControl.ForteDocument.WordDocument.Saved = bSaved; //GLOG 6851 (dm)
                        m_oCurrentSegment = oNewSegment;
                        oMPTaskPaneControl.RefreshAppearance();
                        //GLOG 7819: Ensure xml events are enabled after closing document
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    }
                    else
                    {
                        m_oCurrentSegment = null;
                    }
                }
                else
                {
                    //GLOG 5249: Make sure no TaskPane is left if there's no Active Docuement
                    TaskPanes.HideAllTaskPanes();
                    m_oCurrentSegment = null;
                }

                try
                {
                    m_oRibbon.Ribbon.Invalidate();
                }
                catch { }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                ThisAddIn.DisableEventHandlers = bDisableEventHandlers;

                LMP.Benchmarks.Print(t0);
            }
        }
        private void OnDocumentOpenRetagging(Word.Document oDoc)
        {
            //JTS 7/26/10: Don't handle event if Word session is invisible
            if (!LMP.MacPac.Application.Paused && (oDoc.Application.Visible == true))
            {
	            DateTime t0 = DateTime.Now;

                bool bSaved = oDoc.Saved;

                if (LMP.Forte.MSWord.WordDoc.RequiresRetagging(oDoc))
                {
                    LMP.MacPac.Application.AddBoundingObjectsToBookmarkedDoc();
                }

                oDoc.Saved = bSaved;

    	        LMP.Benchmarks.Print(t0);
                }
    }

        private void OnDocumentOpen(Word.Document oDoc)
        {
            bool bSaved = oDoc.Saved;
            bool bReprotect = false;
            //JTS 7/26/10: Don't handle event if Word session is invisible
            if (oDoc.Name.EndsWith(".dotx") || oDoc.Name.EndsWith(".dotm") ||
                oDoc.Name.EndsWith(".dot") || ThisAddIn.DisableEventHandlers || oDoc.Application.Visible != true)
	            return;

            FirmApplicationSettings oSettings = null; //GLOG 7869
            LMP.Architect.Api.Environment oEnv = new Architect.Api.Environment(oDoc);

            try
            {
                //Prevent recursive calls
                ThisAddIn.DisableEventHandlers = true;

                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return;

                ////retag document if necessary
                //LMP.MacPac.Application.AddBoundingObjectsToBookmarkedDocIfNecessary();

                // GLOG : 6052 : CEH
                oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);  //GLOG 7869
                //GLOG 15875: If Document is protected and contains Forte content, unprotect so that Tags can be refreshed
                if (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false) && LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(oDoc))
                {
                    //Try unprotecting using known password
                    try
                    {
                        oEnv.UnprotectDocument();
                    }
                    catch { }
                    if (!LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false))
                    {
                        bReprotect = true;
                    }
                }
                if (LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc) &&
                    (!LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false)) &&
                    (!LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, false)) &&
                    (!LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, false)))
                {
                    //GLOG 5971 (dm) - don't do if document is protected or
                    //is a docx in 2003 compatibility mode
                    //GLOG 6802 (dm) - added IsForteDoc condition

                    //GLOG 8007 (dm) - mark compatibility mode of documents opened in Word 2013
                    if (LMP.Forte.MSWord.WordApp.Version > 14)
                        LMP.Forte.MSWord.WordDoc.MarkCompatibilityMode(oDoc);
                    
                    //GLOG 6802 (dm) - remove mSEG content controls - not doing xml tags
                    //because 1) we'll need them if this 10.1.2 document that hasn't
                    //been preconverted yet and 2) they're invisible to users
                    //GLOG 6858 (dm) - extended to .doc files so as to no longer
                    //have to support tagged mSEGs - preconversion will now be done
                    //here as well as necessary
                    //GLOG 6912 (dm) - reversed GLOG 6858 change - removal of segment
                    //xml tags will now be deferred until RefreshTags
                    if (LMP.Forte.MSWord.WordDoc.GetDocumentFileFormat(oDoc) == 2)
                    {
                        //GLOG 7306 (dm) - don't add mpNewSegment doc var
                        LMP.Forte.MSWord.WordDoc.DeleteSegmentBoundingObjects(oDoc, true, false);
                    }
                    //GLOG 6131 (dm) - added option to not finish on open
                    //GLOG 7916 (dm) - don't run pre and post finish code on open
                    if (LMP.MacPac.Session.DisplayFinishButton && oSettings.FinishDocumentsUponOpen)
                        LMP.MacPac.Application.FinishDocument(oDoc, true);
                }

                TaskPanes.ShowTaskPaneIfNecessary(oDoc);

                //GLOG : 2620 : CEH
                LMP.MacPac.Application.ActivateMacPacRibbonIfAppropriateOnOpen(oDoc);

                //GLOG : 5759 : CEH
                //set default zoom
                UserApplicationSettings oUserSettings = LMP.MacPac.Session.CurrentUser.UserSettings;
                //GLOG : 6597 : JSW
                //check Update Zoom setting before applying
                if (oUserSettings.UpdateZoom == true)
                    oDoc.ActiveWindow.ActivePane.View.Zoom.Percentage = oUserSettings.DefaultPageZoomPercentage;

                //GLOG 5896: Fixes issue where sometimes page 1 footer text is not visible
                LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();

                if (oSettings.AlertWhenNonForteDocumentOpened)
                    LMP.Architect.Api.Environment.DocumentIsNonForteDocument(oDoc, true);

                if ((oSettings.TrailerIntegrationOptions &
                    FirmApplicationSettings.mpTrailerIntegrationOptions.FileOpen) > 0)
                    LMP.MacPac.Application.InsertTrailerIfNecessary();

                oDoc.UndoClear();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                if (bReprotect)
                {
                    HideTaskPaneForProtectedDoc(oDoc);
                    oEnv.RestoreProtection();
                }
                ThisAddIn.DisableEventHandlers = false;
                oDoc.Saved = bSaved;
                //GLOG 5639: Avoid error on Local Open with Worksite
                //GLOG 7869: Only call DoEvents if DMS is Worksite and doc is binary format
                if (oSettings != null && oSettings.DMSType == 3 && oDoc.SaveFormat == 0)
                    System.Windows.Forms.Application.DoEvents();
            }
        }
        private void OnWindowSelectionChange(Microsoft.Office.Interop.Word.Selection Sel)
        {
            try
            {
                if (ThisAddIn.DisableEventHandlers)
                    return;

                //This event will be raised after document protection state changes
                Word.Document oDoc = null;
                try
                {
                    //JTS 7/26/10: Don't handle event if Word session is invisible
                    //DM 8/16/10: moved Jeffrey's 7/26 fix into the try block -
                    //apparently the selection isn't always available here either
                    //when editing a numbering scheme
                    if (Sel.Application.Visible != true)
                        return;

                    //GLOG 920 - the document isn't always available here, e.g.
                    //when editing a numbering scheme
                    oDoc = Sel.Document;
                }
                catch { }

                HideTaskPaneForProtectedDoc(oDoc);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void HideTaskPaneForProtectedDoc(Word.Document oDoc)
        {

            if (oDoc != null)
            {
                LMP.MacPac.TaskPane oTP = null;
                CustomTaskPane oTaskPane = null;
                try
                {
                    oTaskPane = TaskPanes.Item(oDoc);
                }
                catch { }
                
                if (oTaskPane != null)
                {
                    try
                    {
                        oTP = (LMP.MacPac.TaskPane)oTaskPane.Control;
                    }
                    catch { }
                }

                if (oDoc.ProtectionType != Word.WdProtectionType.wdNoProtection && 
                    (oTP != null && !LMP.MacPac.Application.ShouldShowTaskPaneForProtectedDoc(oTP.ForteDocument)))
                {
                    if (TaskPanes.Item(oDoc) != null && TaskPanes.Item(oDoc).Visible)
                        TaskPanes.Item(oDoc).Visible = false;
                }
            }

        }
        private void OnXMLSelectionChange(Word.Selection Sel,
            Word.XMLNode OldXMLNode, Word.XMLNode NewXMLNode, ref int Reason)
        {
            LMP.MacPac.TaskPane oTP = null;
            CustomTaskPane oTaskPane = null;

            try
            {
                try
                {
                    //JTS 7/26/10: Don't handle event if Word session is invisible
                    if (Sel.Application.Visible != true || Sel.Application.Visible != true)
                        return;

                    oTaskPane = TaskPanes.Item(Sel.Document);
                }
                catch { }

                if (oTaskPane != null)
                {
                    try
                    {
                        oTP = (LMP.MacPac.TaskPane)oTaskPane.Control;
                    }
                    catch { }
                }

                if (oTP != null)
                {
                    //12-24-10 (dm) - added to prevent "This object model command is not available
                    //while in the current event" error in Word 2010 during XMLAfterInsert handling
                    if ((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLSelectionChange) > 0)
                        return;

                    Segment oNewSegment = null;
                    Segment oOldSegment = null;

                    if (NewXMLNode != null && NewXMLNode.NamespaceURI == "urn-legalmacpac-data/10")
                    {
                        //GLOG 6005 (dm) - accept all "as document" segments as top-level
                        oNewSegment = oTP.ForteDocument.GetTopLevelSegmentForRange(NewXMLNode.Range, true);
                    }
                    else if (NewXMLNode == null)
                    {
                        oNewSegment = oTP.ForteDocument.GetTopLevelSegmentForRange(Sel.Range, true);
                    }
                    if (OldXMLNode != null && OldXMLNode.NamespaceURI == "urn-legalmacpac-data/10")
                    {
                        //GLOG 6005 (dm) - accept all "as document" segments as top-level
                        oOldSegment = oTP.ForteDocument.GetTopLevelSegmentForRange(OldXMLNode.Range, true);
                    }


                    m_oCurrentSegment = oNewSegment;

                    if (oNewSegment != oOldSegment)
                        m_oRibbon.Ribbon.Invalidate();
                }
                else
                {
                    m_oCurrentSegment = null;
                }
            }
            catch (System.Exception oE)
            {
                //GLOG 6529: Ignore specific error encountered when undoing Insert Footnote/Endnote within XML Tag
                if (oE.Message.Contains("This object model command is not available"))
                {
                    return;
                }
                else
                {
                    LMP.Error.Show(oE);
                }
            }
        }

        private void OnDocumentBeforeSave(Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            try
            {
                //10/20/11: Do nothing if Word session is invisible
                if (!ThisAddIn.WordApplication.Visible)
                    return;

                //if (!LMP.MacPac.Session.DisplayFinishButton)
                //{
                    //8-22-11 (dm) - exit if this is an autosave
                    if (LMP.Forte.MSWord.WordApp.IsAutoSaveEvent())
                        return;

                    //8-23-11 (dm) - exit if document isn't dirty
                    //8-24-11 (dm) - exit if not an appropriate target
                    //GLOG 5760: exit if Template format
                    if (Doc.Saved || (Doc.SaveFormat != 0 && Doc.SaveFormat != 12 && Doc.SaveFormat != 16))
                        return;

                    //3-10-11 (dm) - add bookmarks in case the document is saved in a different format -
                    //when there's a task pane, this is done in that DocumentBeforeSave handler
                    LMP.MacPac.TaskPane oTP = null;
                    try
                    {
                        oTP = LMP.MacPac.TaskPanes.Item(Doc);
                    }
                    catch { }

                    if (oTP == null)
                    {
                        LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                        oConvert.AddBookmarks(Doc, false);
                    }
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion
        #region****************************properties****************************
        public static CustomTaskPaneCollection CustomWordTaskPanes
        {
            get { return m_oCustomWordTaskPanes; }
        }

        /// <summary>
        /// returns true iff the add-in
        /// has a valid license key
        /// </summary>
        public static bool IsValid
        {
            get { return m_bIsValid; }
        }
        public static Word.Application WordApplication;
        /// <summary>
        /// When true, document change and open events will be skipped
        /// </summary>
        public static bool DisableEventHandlers
        {
            get { return m_bDisableEventHandlers || LMP.MacPac.Application.DisableTaskpaneEvents; }
            set { m_bDisableEventHandlers = value; }
        }
        public static Segment CurrentSegment
        {
            get { return m_oCurrentSegment; }
            internal set
            {
                m_oPreviousSegment = m_oCurrentSegment;
                m_oCurrentSegment = value;
            }
        }
        public static Segment PreviousSegment
        {
            get { return m_oPreviousSegment; }
            internal set { m_oPreviousSegment = value; }
        }
        #endregion
        #region****************************methods****************************
        protected override object RequestService(Guid serviceGuid)
        {
            try
            {
                Trace.WriteInfo("Executing LMP.MacPac102007.ThisAddIn.RequestService");
                if (LMP.Forte.MSWord.GlobalMethods.CurWordApp == null)
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp = this.Application;

                if (serviceGuid == typeof(Office.IRibbonExtensibility).GUID)
                {
                    if (m_oRibbon == null)
                        m_oRibbon = new MacPac10Ribbon();

                    return m_oRibbon;
                }
            }
            catch (System.ExecutionEngineException oE)
            {
                LMP.Error.Show(oE);
            }

            return base.RequestService(serviceGuid);
        }
        //GLOG 4483: Returns an object that can be accessed via COMAddins("ForteAddIn").Object in VBA
        protected override object RequestComAddInAutomationService()
        {
            if (m_oAddinObject == null)
                m_oAddinObject = new AddinObject();
            return m_oAddinObject;
        }
        #endregion
        #region****************************VSTO generated code****************************
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            try
            {
                Trace.WriteInfo("Executing LMP.MacPac102007.ThisAddIn.InternalStartup");

                this.Startup += new System.EventHandler(ThisAddIn_Startup);
                this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);

                m_oCustomWordTaskPanes = this.CustomTaskPanes;
            }
            catch (System.ExecutionEngineException oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}
