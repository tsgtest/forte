using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using LMP.MacPac;
using LMP.Architect.Api;
using LMP.Data;
using LMP.Controls;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac102007
{
    [ComVisible(true)]
    public class MacPac10Ribbon : Office.IRibbonExtensibility
    {
        internal Office.IRibbonUI Ribbon;
        private bool m_bRibbonLoaded = false;
        private System.Timers.Timer m_oPasteTimer;
        private LMP.MacPac.Application m_oMPApp;
        private int m_iShowTags = 0;

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern int GetPrivateProfileString(
            string xAppName, string xKeyName, string xDefault, StringBuilder xReturnedString,
            int iSize, string xFileName);
        
        public MacPac10Ribbon()
        {
            //create paste timer
            m_oPasteTimer = new System.Timers.Timer(10);
            m_oPasteTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                m_oPasteTimer_Elapsed);

            //5-19-11 (dm)
            m_oMPApp = new LMP.MacPac.Application();
        }

        private void m_oPasteTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_oPasteTimer.Enabled)
                {
                    //stop timer
                    m_oPasteTimer.Stop();

                    try
                    {
                        Word.Document oDocument = ThisAddIn.WordApplication.ActiveDocument;

                        //10-27-10 (dm) - adds doc vars for pasted tags/content controls in
                        //lieu of XMLAfterInsert and ContentControlAfterAdd handling
                        if (!LMP.MacPac.Application.TaskPaneExists(oDocument))
                            LMP.Forte.MSWord.WordDoc.AfterPasteWithoutTaskPane(oDocument);

                        //restore Word XML event handling
                        LMP.MacPac.Application.SetWordXMLEventHandling(oDocument, true);

                        //turn off tags - timer is only started if tags were off to begin with
                        //10-17-11 (dm) - timer now runs unconditionally, so restore only when necessary
                        if (m_iShowTags == 0)
                            oDocument.ActiveWindow.View.ShowXMLMarkup = 0;
                        else
                            m_iShowTags = 0;

                        //restore screen updating
                        ThisAddIn.WordApplication.ScreenUpdating = true;
                    }
                    catch
                    {
                        //GLOG 8311: Don't restart - we don't want to get into an endless loop
                        //restart timer - paste may not be done yet
                        //m_oPasteTimer.Start();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            //GLOG 7898 (dm)
            string xRibbonName = "";
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
                xRibbonName = "ForteToolsRibbon.xml";
            else
                xRibbonName = "ForteRibbon.xml";

            //get location of ribbon xml -
            //new location is data directory - dcf 06/23/11
            string xFile = LMP.Data.Application.PublicDataDirectory + "\\" + xRibbonName; //JTS 3/28/13

            if (!File.Exists(xFile))
                xFile = LMP.MacPac.Application.BinDirectory + @"\" + xRibbonName;

            try
            {
                string xXML = null;

                if (File.Exists(xFile))
                {
                    //get ribbon xml
                    xXML = File.ReadAllText(xFile);
                    xXML = SubstituteCommands(xXML);

                    //GLOG 6660 - modify xml to not use backstage xml code for word 2007
                    //GLOG : 6755 : CEH - modify xml to show new xml code for word 2013
                    if (LMP.Forte.MSWord.WordApp.Version >= 14)
                    {
                        xXML = xXML.Replace("<customUI xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\" onLoad=\"OnLoad\" loadImage=\"GetImage\" xmlns:x=\"MacPacRibbon\">",
                            "<customUI xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\" onLoad=\"OnLoad\" loadImage=\"GetImage\" xmlns:x=\"MacPacRibbon\">");
                    }

                    if (LMP.Forte.MSWord.WordApp.Version >= 14 && LMP.Forte.MSWord.WordApp.Version < 15)
                    {
                        //GLOG 7420: backstage tag may now include OnShow and OnHide callbacks
                        xXML = xXML.Replace("<!--<backstage14", "<backstage");
                        xXML = xXML.Replace("</backstage14>-->", "</backstage>");
                    }
                    else if (LMP.Forte.MSWord.WordApp.Version >= 15)
                    {
                        //GLOG 7420: backstage tag may now include OnShow and OnHide callbacks
                        xXML = xXML.Replace("<!--<backstage15", "<backstage");
                        xXML = xXML.Replace("</backstage15>-->", "</backstage>");
                    }
                    else if (LMP.Forte.MSWord.WordApp.Version == 12) //GLOG 7930
                    {
                        xXML = xXML.Replace("<!--<officeMenu>", "<officeMenu>");
                        xXML = xXML.Replace("</officeMenu>-->", "</officeMenu>");
                    }

                    //GLOG : 7985 : JSW  
                    xXML = ConvertForteLabelToUpper(xXML);

                    //GLOG 8896 (dm) - translate TOC UI
                    xXML = TranslateTOCUIIfNecessary(xXML);

                    //GLOG : 6112 : CEH
                    //get Admin Ribbon xml if necessary
                    //if(LMP.Registry.GetMacPac10Value("DisplayAdministrationRibbon") == "1")
                    if (LMP.Data.Application.AdminDBExists)
                    //GLOG : 8224 : JSW
                    //load AdminXML if user has admin access
                    {
                        try
                        {
                            
                            //connect to admin db
                            LMP.Data.LocalConnection.ConnectToDB(true);
                            //GLOG 8289
                            if (LMP.Data.Person.HasAdminAccess(System.Environment.UserName, LMP.MacPac.MacPacImplementation.IsFullWithNetworkSupport))
                                xXML = GetAdminXML(xXML);
                            
                            LMP.Data.LocalConnection.Close();
                        }
                        catch { }
                    }
                    return xXML;
                }
                else
                {
                    MessageBox.Show("Could not find '" + xFile + "'.");
                    return "";
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return "";
            }
        }

        #endregion

        #region Ribbon Callbacks
        public void MP10FileNew(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                Cancel = true;
                LMP.MacPac.Application.CreateNewDocument();
                LMP.MacPac.Application.ForceProfileIfNecessary();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileSave(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5339: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileSave();
                    return;
                }

                //GLOG 3448: Check Mode of current TaskPane -
                //If in Design Mode, always run MacPac Save function
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }

                //GLOG item #7803 - dcf
                //conditionalized on existence of a task pane
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileSave)
                    || (oTP != null && oTP.Mode == ForteDocument.Modes.Design))
                {
                    LMP.MacPac.Application.FileSave();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileSaveAs(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5339: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileSaveAs();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileSaveAs))
                {
                    LMP.MacPac.Application.FileSaveAs();

                    //refresh ribbon - trailer is disabled for .doc files
                    this.Ribbon.Invalidate();

                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileSaveAll(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5339: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileSaveAll();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileSaveAll))
                {
                    LMP.MacPac.Application.FileSaveAll();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileClose(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5099: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileClose();
                    return;
                }

                //GLOG 6851 (dm) - always run MacPac FileClose in design mode - this
                //was previously contingent on FileClose trailer integration
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileClose)
                    || (oTP != null && oTP.Mode == ForteDocument.Modes.Design))
                {
                    LMP.MacPac.Application.FileClose();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileCloseAll(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5099: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileCloseAll();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileCloseAll))
                {
                    LMP.MacPac.Application.FileCloseAll();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void MP10FileExit(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FileExit();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FileExit))
                {
                    LMP.MacPac.Application.FileExit();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void MP10FilePrint(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 5099: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FilePrint();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrint))
                {
                    LMP.MacPac.Application.FilePrint();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void MP10FilePrintDefault(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {

                //GLOG 5099: If Protected View Document is active, allow Native Word to handle
                if (LMP.Forte.MSWord.WordApp.ProtectedViewIsActive())
                {
                    Cancel = false;
                    return;
                }
                Word.Application oWordApp = ThisAddIn.WordApplication;

                if (!Session.IsInitialized)
                {
                    LMP.Forte.MSWord.WordApp.FilePrintDefault();
                    return;
                }

                //GLOG 7803 (dm) - conditionalized on existence of a task pane
                TaskPane oTP = null;
                try
                {
                    oTP = (LMP.MacPac.TaskPane)TaskPanes.Item(oWordApp.ActiveDocument).Control;
                }
                catch { }
                if (oTP != null)
                {
                    //GLOG 3491 - update variable value if necessary - this will ensure
                    //that all associated tags reflect current value
                    LMP.MacPac.Application.UpdateVariableValueFromSelection();
                }

                if (TrailerIntegrationEnabled(FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrintDefault))
                {
                    LMP.MacPac.Application.FilePrintDefault();
                    Cancel = true;
                }
                else
                    Cancel = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void MP10Paste(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 4483: if MacPac is Paused, set Cancel to false so Native Word paste will be used
                //GLOG 8207: Pass to native function if Forte is expired
                if (!ThisAddIn.IsValid || LMP.MacPac.Application.Paused)
                {
                    Cancel = false;
                    return;
                }
                else
                {
                    Word.Document oDocument = ThisAddIn.WordApplication.ActiveDocument;

                    //GLOG item #5254 - dcf - added condition on active window
                    if (oDocument != null && oDocument.ActiveWindow != null)
                    {
                        //10-17-11 (dm) - the timer now runs regardless of whether tags
                        //are showing, so we need to store value this value
                        m_iShowTags = oDocument.ActiveWindow.View.ShowXMLMarkup;

                        //if tags aren't showing, adjust paste location if necessary
                        //JTS 2/22/13: XML Tags not supported in Word 2013, so disregard ShowXMLMarkup setting
                        //GLOG 7153: Changed order of conditions to avoid unnecessary FileFormat check
                        if ((LMP.Forte.MSWord.WordApp.Version < 15) && (LMP.Forte.MSWord.WordDoc.GetDocumentFileFormat(oDocument) == 1) && 
                            (m_iShowTags == 0))
                        {
                            Word.Selection oSelection = ThisAddIn.WordApplication.Selection;
                            DialogResult iRet = DialogResult.None;
                            LMP.MacPac.Application.SuppressWordXMLSelectionChangeEvent(oDocument);
                            LMP.Forte.MSWord.mpPasteAdjustmentPrompts iPrompt = LMP.Forte.MSWord.mpPasteAdjustmentPrompts.None;
                            string xMsgInfo = "";
                            LMP.Forte.MSWord.WordDoc.AdjustPasteLocation(ref iPrompt, ref xMsgInfo);

                            if (iPrompt == LMP.Forte.MSWord.mpPasteAdjustmentPrompts.EndOfmSEG)
                            {
                                //cursor is at end of segment
                                iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_PastingAtEndOfSegment"),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (iRet == DialogResult.No)
                                {
                                    //move selection past closing mSEG tag
                                    //modified for GLOG 3678 (3/17/09 dm)
                                    //extended to content controls (10/22/10 dm)
                                    int iLoc = 0;
                                    Word.XMLNode oTag = oSelection.XMLParentNode;
                                    if (oTag == null)
                                        iLoc = oSelection.Range.ParentContentControl.Range.End + 1;
                                    else
                                        iLoc = oSelection.XMLParentNode.Range.End + 1;
                                    oSelection.SetRange(iLoc, iLoc);
                                }
                            }
                            else if (iPrompt == LMP.Forte.MSWord.mpPasteAdjustmentPrompts.OverwritingEmptyBlockTag)
                            {
                                //GLOG 3997 - overwriting an empty block mVar or mBlock - prompt
                                //user whether they actually intend the pasted text to become the
                                //value of the variable or the block
                                string xMsg = string.Format(LMP.Resources.GetLangString(
                                    "Prompt_PastingOverEmptyTag"), xMsgInfo);
                                iRet = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (iRet == DialogResult.Yes)
                                {
                                    //user wants the pasted text to go into the tag - collapse
                                    //selection and readjust as necessary
                                    object oMissing = System.Reflection.Missing.Value;
                                    oSelection.StartOf(ref oMissing, ref oMissing);
                                    iPrompt = LMP.Forte.MSWord.mpPasteAdjustmentPrompts.None;
                                    xMsgInfo = "";
                                    LMP.Forte.MSWord.WordDoc.AdjustPasteLocation(ref iPrompt,
                                        ref xMsgInfo);
                                }
                            }
                        }

                        //10-17-11 (dm) - moved out of file format conditional - the
                        //doc vars weren't getting added when pasting content into
                        //a document without a task pane
                        m_oPasteTimer.Start();

                        //GLOG 5629: Allow Native Word Paste to proceed
                        Cancel = false;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public bool TrailerIntegrationEnabled(Data.FirmApplicationSettings.mpTrailerIntegrationOptions iOption)
        {
            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
            return (oSettings.TrailerIntegrationOptions & iOption) > 0;
        }
        public void OnLoad(Office.IRibbonUI ribbonUI)
        {
            this.Ribbon = ribbonUI;
            MacPac.Application.MacPac10Ribbon = this.Ribbon;

            m_bRibbonLoaded = true;
            this.Ribbon.Invalidate();
        }

        public void OnButtonClick(Office.IRibbonControl control)
        {
            try
            {
                ExecuteCommand(control.Id);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void TrackFunction(Office.IRibbonControl control, ref bool Cancel)
        {
            Cancel = false;
        }
        /// <summary>
        /// callback for TOC functions
        /// </summary>
        /// <param name="control"></param>
        public void OnTOCButtonClick(Office.IRibbonControl control)
        {
            try
            {
             
                Word.Application oWordApp = ThisAddIn.WordApplication;

                //GLOG 4429 (dm)
                if (oWordApp.Documents.Count == 0)
                {
                    MessageBox.Show(LMP.Resources.GetLangString
                        ("Msg_PleaseOpenDocument"), ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                string xTag = control.Tag;
                Word.Document oDoc = oWordApp.ActiveDocument;

                if (xTag == "zzmpTableOfContentsInsert")
                {
                    //GLOG 7201 (dm) - replaced call to ShowTaskPaneIfNecessary() with
                    //TOC-specific method to ensure that there's a task pane if necessary
                    if (oDoc != null)
                        LMP.MacPac102007.TaskPanes.AddHiddenTaskPaneIfNecessary(oDoc);
                }

                //run macro
                oWordApp.RunOld(xTag);

                ////GLOG item #7301 - dcf
                ////update validation indicators in task pane
                //((LMP.MacPac.TaskPane)LMP.MacPac102007.TaskPanes.Item(oDoc).Control).UpdateEditorSegmentImages();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public bool GetVisible(Office.IRibbonControl control)
        {
            try
            {
                if (control.Id == LMP.ComponentProperties.ProductName)
                {
                    //make ribbon tab visible only if license key is valid
                    return ThisAddIn.IsValid;
                }

                Word.Application oWordApp = ThisAddIn.WordApplication;

                //initialize MacPac if necessary
                Session.StartIfNecessary(oWordApp);

                string xIDRoot = GetIDRoot(control);

                switch (xIDRoot)
                {
                    case "ShowAdministrator":
                    case "ConvertAdminDB":
                    case "PreconvertAdminDB":
                    case "TagBookmarkedDoc":
                        return Session.IsInitialized && Session.AdminMode;
                    case "ConvertUserSegments":
                        ////GLOG 5422: Hide this command until Word is restarted if forced Sync has updated Admin data
                        //if (Session.IsInitialized && !MacPac.Application.IsUserSyncInProgress(false))
                        //{
                        //    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner,
                        //        Session.CurrentUser.ID);

                        //    if (oDefs.Count == 0)
                        //        Session.CurrentUser.UserSettings.UserSegmentsConverted = true;

                        //    return !Session.AdminMode && !Session.CurrentUser.UserSettings.UserSegmentsConverted;
                        //}
                        //else
                            return false;
                    case "PreconvertDocument":
                        //GLOG : 8326 : jsw
                        //disabled in 11.3
                        //return Session.IsInitialized && Session.HostedInPostInjunctionWord;
                            return false;
                    case "ChangeUser":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowProxying;
                    case "LegacyDocuments":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowLegacyDocumentFunctionality;
                    case "Pause":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowPauseResume;
                    case "RemoveTags":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval
                             && !Session.CurrentUser.FirmSettings.AllowDocumentMemberTagRemoval;
                    case "RemoveVariableTags":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowDocumentMemberTagRemoval
                             && !Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval;
                    case "RemoveTagsMenuItem":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval;
                    case "RemoveVariableTagsMenuItem":
                        return Session.IsInitialized && Session.CurrentUser.FirmSettings.AllowDocumentMemberTagRemoval; //GLOG 6137
                    case "AutomationMenu":
                        return Session.IsInitialized && (Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval &&
                            Session.CurrentUser.FirmSettings.AllowDocumentMemberTagRemoval);
                    case "Tags":
                        return Session.IsInitialized && (Session.CurrentUser.FirmSettings.AllowPauseResume ||
                            Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval);
                    case "HelpMenu":
                        //GLOG 7891 : always show button
                        return true;
                        //GLOG : 3624 : CEH
                        //hide Help documents menu if Help/Documents directory doesn't exist
                        //return Session.IsInitialized && Directory.Exists(
                            //LMP.Data.Application.GetDirectory(mpDirectories.Help) + "\\Documents");
                    //GLOG : 7494 : CEH
                    case "UpdateForWord2013Compatibility":
                        return (LMP.Forte.MSWord.WordApp.Version >= 15);
                    case "CreateMasterData":
                        return LMP.Utility.InDevelopmentMode;
                    case "LayoutPleadingCounsels":
                    case "LayoutPleadingCaptions":
                    case "LayoutPleadingSignatures":
                    case "LayoutLetterSignatures":
                    case "LayoutAgreementSignatures":
                        return true;
                    case "PrintSelectedLetterhead":
                      //GLOG : 6478 : jsw
                      {
                            try
                            {
                                return (ThisAddIn.WordApplication != null) &&
                                    (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                    (!LMP.MacPac.Application.Paused) &&
                                    ThisAddIn.CurrentSegment.Contains(mpObjectTypes.Letterhead);
                            }
                            catch
                            {
                                return false;
                            }
                        }
                    case "Synchronize":
                      //GLOG 8220
                      {
                          return (!LMP.MacPac.MacPacImplementation.IsFullLocal);
                      }
                    default:
                      if (!string.IsNullOrEmpty(control.Tag))
                      {
                          string xPattern = control.Tag.ToLower();
                          bool bIsMatch = System.Text.RegularExpressions.Regex.IsMatch(
                              ThisAddIn.CurrentSegment.Name.ToLower(), xPattern);

                          return bIsMatch;
                      }
                      else
                      {
                          // No other controls should be affected by this
                          return Session.IsInitialized;
                      }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return true;
            }
        }

        public Office.RibbonControlSize GetSize(Office.IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "Pause":
                        if (!Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval)
                            return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
                        else
                            return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeRegular;
                    case "RemoveTags":
                        if (!Session.CurrentUser.FirmSettings.AllowPauseResume)
                            return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
                        else
                            return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeRegular;
                    default:
                        return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
                }
            }
            catch
            {
                return Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            }
        }

        /// <summary>
        /// returns the "root" of the ID of the specified control-
        /// the root is all text up to "__".  MacPac controls that
        /// have the same root will have the same dynamic ribbon
        /// behavior - e.g. in GetEnabled, GetLabel, etc.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private string GetIDRoot(Office.IRibbonControl control)
        {
            string xID = control.Id;
            int iRootEnd = xID.IndexOf("__");

            if (iRootEnd > -1)
                return xID.Substring(0, iRootEnd);
            else
                return xID;
        }
        /// <summary>
        /// Callback used to set appropriate visible state for Recently-used templates list
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetEnabled(Office.IRibbonControl control)
        {
            try
            {
                if (m_bRibbonLoaded)
                {
                    LMP.MacPac.Session.StartIfNecessary(ThisAddIn.WordApplication);

                    if (!LMP.MacPac.Session.IsInitialized)
                        return false;

                    Word.Window oWindow = null;

                    try{
                        oWindow = ThisAddIn.WordApplication.ActiveDocument.ActiveWindow;
                    }
                    catch{}

                    bool bIsNotCompareDoc = false;

                    if (oWindow != null)
                        bIsNotCompareDoc = !LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oWindow);
                    else
                    {
                        //GLOG item #7114 - dcf
                        bIsNotCompareDoc = true;
                    }

                    LMP.MacPac.TaskPane oTP = null;
                    int iTabIndex = -1;
                    bool bDesignMode = false;
                    bool bContainsUnfinishedContent = false;

                    try
                    {
                        oTP = LMP.MacPac.TaskPanes.Item(ThisAddIn.WordApplication.ActiveDocument);
                        iTabIndex = oTP.ActiveTab;
                        bDesignMode = oTP.Mode == ForteDocument.Modes.Design;
                        //GLOG :  6445 :  JSW
                        bContainsUnfinishedContent = oTP.ForteDocument.ContainsUnfinishedContent();
                    }
                    catch { }

                    //GLOG : 8318 : jsw
                    //if the addin is not valid
                    //disable all menu items
                    if (!ThisAddIn.IsValid)
                        return false;

                    if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                    {
                        
                        if (control.Id == "HelpMenu")
                        {
                            return true;
                        }
                        else
                        {
                            return ThisAddIn.WordApplication.Documents.Count > 0;
                        }
                    }

                    switch (GetIDRoot(control))
                    {
                        case "FavoriteSegments":
                            //GLOG 6181: Disable Favorites menu in Protected View
                            //(bIsNotCompareDoc will be false since there is no ActiveDocument)
                            return (ThisAddIn.WordApplication != null &&
                                !LMP.MacPac.Application.Paused && 
                                bIsNotCompareDoc);
                        case "QuickSegments":
                            return true;
                        case "PasteSegmentXML":
                            // Enable button only if XML has been copied
                            if (System.IO.File.Exists(MacPac.TaskPane.SegmentPasteFile))
                                return !LMP.MacPac.Application.Paused && bIsNotCompareDoc;
                            else
                                return false;
                        case "ShowTaskPane":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        case "AttachToTemplate":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                        case "SelectTaskPane":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        //GLOG : 7494 : CEH
                        case "UpdateForWord2013Compatibility":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "RefreshTaskPane":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        case "ToggleXMLCodes":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        case "Convert":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                        case "CreateLegacyPrefill":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                        case "UpdateContactDetail":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                        case "SetNormalStyleFont":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "InsertTrailer":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "InsertDraftStamp":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        case "OfficeAddress":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "CreateNewDMSDocument":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "UpdateDocumentContent":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                        case "InsertContactDetail":
                        case "InsertFormattedAddresses":
                        case "AddressBook":
                        case "btnInsertNamesAdvanced":
                        case "InsertContactDetail2":
                        //GLOG : 7720 : ceh
                        //GLOG : 7719 : ceh
                        case "SetSentenceSpacingToOne":
                        case "SetSentenceSpacingToTwo":
                        case "InsertNonBreakingSpaceInDates":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        //GLOG : 6025 : CEH
                        case "InsertPageNumber":
                        case "RemovePageNumber":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        case "SymbolsMenu":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        //case "PleadingCounsel": //GLOG 6418 (dm)
                        case "LayoutPleadingCounsels":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.PleadingCounsels);
                        //case "PleadingCaption": //GLOG 6418 (dm)
                        case "LayoutPleadingCaptions":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.PleadingCaptions);
                        //case "PleadingSignature": //GLOG 6418 (dm)
                        case "LayoutPleadingSignatures":
                        //GLOG : 8035 : JSW
                        case "LayoutPleadingSigSignatures":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.PleadingSignatures);
                        case "LayoutLetterSignatures":
                        //GLOG : 8035 : JSW
                        case "LayoutLetterSigSignatures":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.LetterSignatures);
                        case "LayoutAgreementSignatures":
                        //GLOG : 8035 : JSW
                        case "LayoutAgreementSigSignatures":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.AgreementSignatures);
                        case "PleadingPaper":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) &&
                                ThisAddIn.CurrentSegment.Contains(mpObjectTypes.PleadingPaper);
                        case "Synchronize":
                            return !LMP.MacPac.Application.OnDemandSyncExecuting && !LMP.MacPac.Application.Paused;
                        case "PrintDraft":
                        case "PrintFinal":
                        //GLOG : 7323 : ceh
                        case "PrintCurrentPage":
                            return ThisAddIn.WordApplication != null && ThisAddIn.WordApplication.Documents.Count > 0;
                        case "AutomationMenu":
                            //GLOG item #6400 - dcf
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode;
                            //return (ThisAddIn.WordApplication != null) &&
                            //    (ThisAddIn.WordApplication.Documents.Count > 0) &&
                            //    (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && !bDesignMode && ThisAddIn.CurrentSegment != null;
                        case "Transform":
                            //GLOG 6087 (dm)
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && !bDesignMode;
                        case "CreateLabelsMain":
                            //GLOG 6087 (dm)
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "CreateEnvelopesMain":
                            //GLOG 6087 (dm)
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused);
                        case "SaveData":
                        case "SaveSegmentWithData":
                        case "RecreateSegment":
                            //GLOG item #6128 - dcf
                            //iTabIndex = 2 means that the Finished Data tab is active
                            return (ThisAddIn.WordApplication != null) && 
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && (!ThisAddIn.CurrentSegment.IsFinished ||
                                iTabIndex == 3) && !bDesignMode;
                        case "RemoveVariableTagsMenuItem":
                        case "RemoveVariableTags":
                            //GLOG 6317
                            //GLOG :  6445 :  JSW
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc &&
                                (ThisAddIn.CurrentSegment == null || (ThisAddIn.CurrentSegment != null && !ThisAddIn.CurrentSegment.IsFinished) || bContainsUnfinishedContent) & !bDesignMode;
                        case "RemoveTagsMenuItem":
                        case "RemoveTags":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc && ThisAddIn.CurrentSegment != null && !bDesignMode;
                        case "EditPleadingCounsel":
                            Segment[] aCounsels = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.PleadingCounsel);
                            foreach (Segment oCounsel in aCounsels)
                            {
                                if (!oCounsel.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditPleadingCaption":
                            Segment[] aCaptions = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.PleadingCaption);
                            foreach (Segment oCaption in aCaptions)
                            {
                                if (!oCaption.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditPleadingSignature":
                        //GLOG : 8035 : JSW
                        case "EditPleadingSigSignature":
                            Segment[] aPleadingSignatures = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.PleadingSignature);
                            foreach (Segment oSignature in aPleadingSignatures)
                            {
                                if (!oSignature.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditPleadingPaper":
                            Segment[] aPleadingPapers = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.PleadingPaper);
                            foreach (Segment oPaper in aPleadingPapers)
                            {
                                if (!oPaper.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditLetterSignature":
                        //GLOG : 8035 : JSW
                        case "EditLetterSigSignature":
                            Segment[] aLetterSignatures = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.LetterSignature);
                            foreach (Segment oSignature in aLetterSignatures)
                            {
                                if (!oSignature.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditAgreementSignature":
                        //GLOG : 8035 : JSW
                        case "EditAgreementSigSignature":
                            Segment[] aAgreementSignatures = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.AgreementSignature);
                            foreach (Segment oSignature in aAgreementSignatures)
                            {
                                if (!oSignature.IsFinished)
                                    return true;
                            }

                            return false;
                        case "EditLetterLetterhead":
                            {
                                Segment[] aLetterLetterhead = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.Letterhead);
                                if (aLetterLetterhead != null && aLetterLetterhead.Length > 0)
                                    return !aLetterLetterhead[0].IsFinished;
                                else
                                    return false;
                            }
                        case "EditMemoLetterhead":
                            {
                                Segment[] aLetterLetterhead = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.Letterhead);
                                if (aLetterLetterhead != null && aLetterLetterhead.Length > 0)
                                    return !aLetterLetterhead[0].IsFinished;
                                else
                                    return false;
                            }
                        case "EditFaxLetterhead":
                            {
                                Segment[] aLetterLetterhead = ThisAddIn.CurrentSegment.FindChildren(mpObjectTypes.Letterhead);
                                if (aLetterLetterhead != null && aLetterLetterhead.Length > 0)
                                    return !aLetterLetterhead[0].IsFinished;
                                else
                                    return false;
                            }
                        case "InsertPleadingSectionBreak":
                            return (ThisAddIn.WordApplication != null) &&
                                (ThisAddIn.WordApplication.Documents.Count > 0) &&
                                (!LMP.MacPac.Application.Paused) && bIsNotCompareDoc;
                        default:
                            // No other controls should be affected by this
                            return !LMP.MacPac.Application.Paused;
                    }
                }
                else
                    return false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return true;
            }
        }
        ///// <summary>
        ///// Callback used to set appropriate text for Recently-used templates list
        ///// </summary>
        ///// <param name="control"></param>
        ///// <returns></returns>
        //public string GetLabel(Office.IRibbonControl control)
        //{
        //    try
        //    {
        //        if (control.Id.StartsWith("TemplateMRU") && m_bRibbonLoaded)
        //        {
        //            int iPos = Int32.Parse(control.Id.Substring(11));
        //            if (iPos <= LMP.MacPac.Application.TemplateHistory.Count)
        //            {
        //                string xTemplate = LMP.MacPac.Application.TemplateHistory[iPos - 1];
        //                // Display name only
        //                return "&" + iPos.ToString() + "  " + System.IO.Path.GetFileName(xTemplate);
        //            }
        //            else if (iPos == 1)
        //                return "<Select a new Template>";
        //            else
        //                return "";
        //        }
        //        else
        //            return "";
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //}

        /// <summary>
        /// inserts the segment specified in the control.Tag
        /// property into a new document - control.Tag must
        /// contain the name of the segment
        /// </summary>
        /// <param name="control"></param>
        public void InsertSegmentInNewDocument(Office.IRibbonControl control)
        {
            try
            {
                Word.Application oWordApp = ThisAddIn.WordApplication;

                //initialize MacPac if necessary
                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return;

                //GLOG 7143 (dm) - don't allow in Word 2013 if save format is .doc
                if ((LMP.Forte.MSWord.WordApp.Version > 14) && (oWordApp.DefaultSaveFormat.ToUpper() == "DOC"))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NewDocAutomationNotSupportedInWord2013"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string xID = null;
                FolderMember oMember = null; //GLOG 6986
                if (!string.IsNullOrEmpty(control.Tag))
                {
                    //get segment ID from control tag
                    xID = LMP.Architect.Api.Expression.Evaluate("[SegmentID__" + control.Tag + "]", null, null);
                }
                else
                {
                    using (LMP.MacPac.SegmentForm oForm = new LMP.MacPac.SegmentForm(false, "", null))
                    {
                        oForm.Text = LMP.Resources.GetLangString("Dlg_InsertSegmentInNewDocument");
                        //oForm.Size = new Size(650, 500);
                        oForm.StartPosition = FormStartPosition.CenterParent;
                        oForm.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                            FolderTreeView.mpFolderTreeViewOptions.Prefills |
                            FolderTreeView.mpFolderTreeViewOptions.Segments |
                            FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                            FolderTreeView.mpFolderTreeViewOptions.UserSegments;
                        if (oForm.ShowDialog() == DialogResult.OK)
                        {
                            xID = oForm.Value;
                            oMember = oForm.SelectedNode.Tag as FolderMember; //GLOG 6986
                        }
                        // GLOG : 7276 : JSW
                        // Invalidate the ribbon to update the favorite segments selection.
                        LMP.MacPac.Application.MacPac10Ribbon.Invalidate();

                    }
                }

                if (!string.IsNullOrEmpty(xID))
                {

                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xID.EndsWith(".0"))
                        xID = xID.Substring(0, xID.IndexOf(".0"));

                    //GLOG 6986: Handle User Content separately
                    if (!xID.Contains("."))
                    {
                        //GLOG : 6874 : jsw
                        //check if segment is external content
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = null;
                        oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xID));
                        if (oDef.TypeID == mpObjectTypes.NonMacPacContent)
                            TaskPane.CreateExternalContent(oDef.ID);
                        //GLOG : 8019 : ceh - implement for segment packets
                        else if (oDef.TypeID == mpObjectTypes.SegmentPacket)
                            LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, null, LMP.MacPac102007.TaskPanes.ShowTaskPane);
                        //GLOG : 8023 : ceh - implement for answer files
                        else if (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                            TaskPane.InsertAnswerFile(xID, null, null, LMP.MacPac102007.TaskPanes.ShowTaskPane);
                        else
                            TaskPane.InsertSegmentInNewDoc(xID, null, null);
                    }
                    else
                    {
                        if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                        {
                            VariableSetDefs oVDefs = new VariableSetDefs(mpVariableSetsFilterFields.User, Session.CurrentUser.ID);
                            VariableSetDef oVDef = (VariableSetDef)oVDefs.ItemFromID(xID);
                            xID = oVDef.SegmentID;
                            Prefill oPrefill = new Prefill(oVDef.ContentString, oVDef.Name, oVDef.SegmentID);
                            TaskPane.InsertSegmentInNewDoc(xID, oPrefill, null);
                        }
                        else
                        {
                            TaskPane.InsertSegmentInNewDoc(xID, null, null);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void CreateExternalContent(Office.IRibbonControl control)
        {
            try
            {
                Word.Application oWordApp = ThisAddIn.WordApplication;

                //initialize MacPac if necessary
                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return;

                //get segment ID from control tag
                string xID = LMP.Architect.Api.Expression.Evaluate("[SegmentID__" + control.Tag + "]", null, null);

                TaskPane.CreateExternalContent(Int32.Parse(xID));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void OnFavoriteSegmentClick(Office.IRibbonControl control)
        {
            int iDocCount = ThisAddIn.WordApplication.Documents.Count;

            //GLOG : 8296 : ceh - no need for this here
            //if (iDocCount > 0)
            //{
            //    if (LMP.Architect.Api.Environment.SaveFormatDoesNotSupportXMLTags(
            //        ThisAddIn.WordApplication.ActiveDocument, true))
            //        return;
            //}

            System.Windows.Forms.Application.DoEvents();
            //GLOG 4472: Restructured so that it is not necessary to show the TaskPane
            //for the current document if insertion location is In New Document.
            //This also avoids bug causing repeated switching between new and original
            //document after insertion

            Segment.InsertionLocations iInsertLocation = Segment.InsertionLocations.Default;
            Segment.InsertionBehaviors iInsertBehavior = Segment.InsertionBehaviors.Default;
            try
            {
                //GLOG 6133
                int iSegmentIndex = int.Parse(control.Id.Substring(control.Id.Length - 2));
                string xSegmentID = FavoriteSegments.SegmentID(iSegmentIndex);

                mpFolderMemberTypes oType = FavoriteSegments.SegmentFolderMemberType(iSegmentIndex);

                switch (oType)
                {
                    case mpFolderMemberTypes.AdminSegment:
                        {
                            AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                            AdminSegmentDef oDef = null;

                            try
                            {
							    //GLOG 5448:  Extract Integer portion of Segment ID before Cast
                                string xID = xSegmentID;
                                if (xID.IndexOf(".") > -1)
                                    xID = xID.Substring(0, xSegmentID.IndexOf("."));
                                oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xID));
                            }
                            catch
                            {
                                MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                    ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                FavoriteSegments.Remove(iSegmentIndex);
                                FavoriteSegments.Save();
                                return;
                            }

                            if (oDef.IsExternalContent)
                            {
                                string xFile = oDef.XML;

                                if (!File.Exists(xFile))
                                {
                                    //GLOG : 15913 : ceh - Extract filename part of existing path to append to Templates path
                                    xFile = System.IO.Path.GetFileName(xFile);
                                    xFile = LMP.Data.Application.TemplatesDirectory + "\\" + xFile;
                                    if (!File.Exists(xFile))
                                        throw new LMP.Exceptions.FileException(
                                            LMP.Resources.GetLangString("Error_CouldNotFindFile") + xFile);
                                }

                                System.Diagnostics.Process.Start(xFile);
                            }
                            else if (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet)
                            {
                                if (iDocCount > 0)
                                {
                                    Word.Document oActiveDoc = ThisAddIn.WordApplication.ActiveDocument;

                                    if (oActiveDoc != null)
                                    {
                                        ForteDocument oMPDoc = new ForteDocument(oActiveDoc);
                                        oMPDoc.UpdateStylesFromXML(oDef.XML);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertSegment_NoActiveDoc"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }
                            }

                            else
                            {
                                int iLoc = FavoriteSegments.SegmentInsertionLocation(iSegmentIndex);

                                // Glog 2475 : If the user specifies the default location (ie, 0) then use
                                // the default location specified in the definition.
                                if (iLoc == 0)
                                {
                                    iInsertLocation = (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation;
                                }
                                else
                                {
                                    iInsertLocation = (Segment.InsertionLocations)iLoc;
                                }
                                iInsertBehavior = (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior;
                                if (iInsertLocation != Segment.InsertionLocations.InsertInNewDocument &&
                                    iDocCount != 0)
                                {
                                    //GLOG 4472: Make sure Taskpane is available for inserting in current document
                                    TaskPanes.ShowTaskPane(ThisAddIn.WordApplication.ActiveDocument);
                                }

                                if (iDocCount > 0 ||
                                    iInsertLocation == Segment.InsertionLocations.InsertInNewDocument)
                                {
                                    MacPac.Application.InsertSegment(oDef,
                                        iInsertLocation,
                                        (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                                }
                                else
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertSegment_NoActiveDoc"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }
                            }
                            break;
                        }
                    case mpFolderMemberTypes.UserSegment:
                        {
                            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                            UserSegmentDef oDef = null;

                            try
                            {
                                oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);
                            }
                            catch
                            {
                                MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                    ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                FavoriteSegments.Remove(iSegmentIndex);
                                FavoriteSegments.Save();
                                return;
                            }

                            if (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet)
                            {
                                if (iDocCount > 0)
                                {
                                    Word.Document oActiveDoc = ThisAddIn.WordApplication.ActiveDocument;

                                    if (oActiveDoc != null)
                                    {
                                        ForteDocument oMPDoc = new ForteDocument(oActiveDoc);
                                        oMPDoc.UpdateStylesFromXML(oDef.XML);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertSegment_NoActiveDoc"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                int iLoc = FavoriteSegments.SegmentInsertionLocation(iSegmentIndex);

                                // Glog 2475 : If the user specifies the default location (ie, 0) then use
                                // the default location specified in the definition.
                                if (iLoc == 0)
                                {
                                    //GLOG 6503:  Handle insertion same as when double-clicking on Content Manager node
                                    if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                                        iInsertLocation = Segment.InsertionLocations.InsertInNewDocument;
                                    else
                                        iInsertLocation = Segment.InsertionLocations.InsertAtSelection;
                                }
                                else
                                {
                                    iInsertLocation = (Segment.InsertionLocations)iLoc;
                                }
                                iInsertBehavior = (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior;
                                if (iInsertLocation != Segment.InsertionLocations.InsertInNewDocument &&
                                    iDocCount != 0)
                                {
                                    //GLOG 4472: Make sure Taskpane is available for inserting in current document
                                    TaskPanes.ShowTaskPane(ThisAddIn.WordApplication.ActiveDocument);
                                }

                                //GLOG : 8100 : ceh - deal with segment packets
                                if (iDocCount > 0 ||
                                    iInsertLocation == Segment.InsertionLocations.InsertInNewDocument ||
                                    oDef.TypeID == mpObjectTypes.SegmentPacket)
                                {
                                    if (oDef.TypeID == mpObjectTypes.SegmentPacket)
                                    {
                                        LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, null, LMP.MacPac102007.TaskPanes.ShowTaskPane);
                                    }
                                    else
                                    {
                                        MacPac.Application.InsertSegment(oDef,
                                            iInsertLocation,
                                            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertSegment_NoActiveDoc"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }

                            }
                            break;
                        }
                    case mpFolderMemberTypes.VariableSet:
                        {
                            VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.User,
                                LMP.MacPac.Session.CurrentUser.ID);
                            VariableSetDef oDef = null;

                            try
                            {
                                oDef = (VariableSetDef)oDefs.ItemFromID(xSegmentID);
                            }
                            catch
                            {
                                MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                    ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                FavoriteSegments.Remove(iSegmentIndex);
                                FavoriteSegments.Save();
                                return;
                            }
                            string xSegDefID = oDef.SegmentID;
                            //Get appropriate Insertion options for related Segment
                            if (!xSegDefID.Contains(".") || xSegDefID.EndsWith(".0"))
                            {
                                //Admin Segment
                                AdminSegmentDefs oAdminDefs = new AdminSegmentDefs();
                                int iID1 = 0;
                                int iID2 = 0;
                                LMP.String.SplitID(xSegDefID, out iID1, out iID2);
                                AdminSegmentDef oAdminDef;
                                try
                                {
                                    oAdminDef = (AdminSegmentDef)oAdminDefs.ItemFromID(iID1);
                                }
                                catch
                                {
                                    MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                        ComponentProperties.ProductName, MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);

                                    FavoriteSegments.Remove(iSegmentIndex);
                                    FavoriteSegments.Save();
                                    return;
                                }

                                iInsertBehavior = (Segment.InsertionBehaviors)oAdminDef.DefaultDoubleClickBehavior;
                                iInsertLocation = (Segment.InsertionLocations)oAdminDef.DefaultDoubleClickLocation;
                            }
                            else
                            {
                                //user segment
                                UserSegmentDefs oUserDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User,
                                    LMP.MacPac.Session.CurrentUser.ID);
                                UserSegmentDef oUserDef;
                                try
                                {
                                    oUserDef = (UserSegmentDef)oUserDefs.ItemFromID(xSegDefID);
                                }
                                catch
                                {
                                    MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                        ComponentProperties.ProductName, MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);

                                    FavoriteSegments.Remove(iSegmentIndex);
                                    FavoriteSegments.Save();
                                    return;
                                }

                                if (oUserDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                                {
                                    iInsertLocation = Segment.InsertionLocations.InsertInNewDocument;
                                }
                                else if (oUserDef.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent ||
                                    oUserDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText ||
                                    oUserDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                                {
                                    iInsertLocation = Segment.InsertionLocations.InsertAtSelection;
                                }
                            }
                            if (iInsertLocation != Segment.InsertionLocations.InsertInNewDocument &&
                                iDocCount != 0)
                            {
                                //GLOG 4472: Make sure Taskpane is available for inserting in current document
                                TaskPanes.ShowTaskPane(ThisAddIn.WordApplication.ActiveDocument);
                            }

                            if (iDocCount > 0 ||
                                iInsertLocation == Segment.InsertionLocations.InsertInNewDocument)
                            {
                                MacPac.Application.InsertSegment(oDef, iInsertLocation, iInsertBehavior);
                            }
                            else
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertSegment_NoActiveDoc"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                            }
                            break;
                        }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Callback executed when clicking on item in Template History list
        /// </summary>
        /// <param name="control"></param>
        public void OnHistoryClick(Office.IRibbonControl control)
        {
            try
            {
                if (control.Id.StartsWith("TemplateMRU") && m_bRibbonLoaded)
                {
                    int iPos = Int32.Parse(control.Id.Substring(11));
                    if (iPos <= LMP.MacPac.Application.TemplateHistory.Count || iPos == 1)
                    {
                        string xNewTemplate = "";
                        if (LMP.MacPac.Application.TemplateHistory.Count > 0)
                            xNewTemplate = LMP.MacPac.Application.TemplateHistory[iPos - 1];
                        LMP.MacPac.Application.AttachToTemplate(xNewTemplate);
                        //Invalidate menu to force update of history list
                        for (int i = 1; i <= 5; i++)
                            this.Ribbon.InvalidateControl("TemplateMRU" + i.ToString());
                    }

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// executes commands on the MacPac tab of the Word ribbon -
        /// can only be called when Macpac is hosted in Word 2007 or later
        /// </summary>
        /// <param name="xCommandID"></param>
        public void ExecuteCommand(string xCommandID)
        {
            try
            {
                Word.Application oWordApp = ThisAddIn.WordApplication;
                bool bShowTaskPane = false;

                //GLOG 1329: Disable event handlers to prevent unexpected calls to 
                //DocumentChange and DocumentOpen during Session initialization
                ThisAddIn.DisableEventHandlers = true;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return;

                if (xCommandID.StartsWith("User"))
                {
                    //change user -
                    //parse ID from command id
                    double dblUserID = double.Parse(xCommandID.Substring(4));
                    int iUserID = (int)dblUserID; ;

                    try
                    {
                        LMP.MacPac.Session.SetProxy(iUserID);
                        TaskPanes.RefreshAll();
                        this.Ribbon.Invalidate();
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Error.Show(oE);
                    }
                }
                else if (xCommandID.StartsWith("Macro_"))
                {
                    string xTemp = xCommandID.Substring(6);
                    ExecuteCustomMacro(xTemp);
                }
                else
                {
                    //GLOG 4750/4762: There may not be a Taskpane, so we shouldn't depend
                    //on that to obtain a ForteDocument object for testing
                    bool bMacPacFunctionalityIsAvailable = true;
                    ForteDocument oMPDoc = null;
                    try
                    {
                        oMPDoc = new ForteDocument(oWordApp.ActiveDocument);
                    }
                    catch { }
                    if (oMPDoc != null)
                    {
                        bMacPacFunctionalityIsAvailable = oMPDoc.MacPacFunctionalityIsAvailable;
                    }

                    //GLOG 6829 (dm) - check for missing segments - refresh if necessary
                    LMP.MacPac.TaskPane oTaskPane = null;
                    try
                    {
                        oTaskPane = LMP.MacPac.TaskPanes.Item(oWordApp.ActiveDocument);
                    }
                    catch { }
                    if ((oTaskPane != null) && (oTaskPane.ForteDocument.Mode != ForteDocument.Modes.Design) &&
                        oTaskPane.ForteDocument.Segments.ContentIsMissing)
                        oTaskPane.Refresh(false, true, false);

                    switch (xCommandID)
                    {
                        case "CreateNewDocument":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.CreateNewDocument();
                            break;
                        case "CreateNewDMSDocument":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.CreateNewDocument();
                            LMP.MacPac.Application.ForceProfileIfNecessary();
                            break;
                        case "CreateNewWordDocument":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.CreateNewWordDocument();
                            break;
                        case "CreateEnvelopes":
                            //GLOG : 6461 : CEH
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                return;

                            ThisAddIn.DisableEventHandlers = false;

                            //GLOG 15822 (dm) - get top level segment if we don't already have it
                            Segment oSeg = ThisAddIn.CurrentSegment;
                            if (oSeg == null)
                            {
                                if (oTaskPane != null)
                                    oMPDoc = oTaskPane.ForteDocument;
                                else
                                    oMPDoc = new ForteDocument(oWordApp.ActiveDocument);
                                oSeg = oMPDoc.GetTopLevelSegmentFromSelection();
                            }

                            //GLOG 7795 (dm) - don't show task pane if not inserted
                            if (LMP.MacPac.Application.CreateEnvelopes(oSeg,
                                LMP.MacPac102007.TaskPanes.ShowTaskPane))
                                bShowTaskPane = true;
                            break;
                        case "CreateLabels":
                            //GLOG : 6461 : CEH
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                return;
                            ThisAddIn.DisableEventHandlers = false;

                            //GLOG 15822 (dm) - get top level segment if we don't already have it
                            Segment oCSeg = ThisAddIn.CurrentSegment;
                            if (oCSeg == null)
                            {
                                if (oTaskPane != null)
                                    oMPDoc = oTaskPane.ForteDocument;
                                else
                                    oMPDoc = new ForteDocument(oWordApp.ActiveDocument);
                                oCSeg = oMPDoc.GetTopLevelSegmentFromSelection();
                            }

                            //GLOG 7795 (dm) - don't show task pane if not inserted
                            if (LMP.MacPac.Application.CreateLabels(oCSeg,
                                LMP.MacPac102007.TaskPanes.ShowTaskPane))
                                bShowTaskPane = true;
                            break;
                        case "ShowTaskPane":
                            if (oTaskPane != null && !LMP.MacPac.Application.ShouldShowTaskPaneForProtectedDoc(oTaskPane.ForteDocument))
                            {
                                if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                    return;
                            }
                            else if ((oTaskPane == null && LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true)) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                return;
                            else if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, false))
                            {
                                //GLOG 6578 (dm) - created separate block for this condition because reconstitution may
                                //be necessary if there's already a hidden task pane
                                if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true))
                                    return;
                                else if (TaskPanes.CurrentTaskPane != null)
                                {
                                    ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                                    oConvert.ReconstituteIfNecessary(oWordApp.ActiveDocument, false, false);
                                    ForteDocument.IgnoreWordXMLEvents = iEvents;
                                }
                            }

                            LMP.MacPac102007.TaskPanes.ShowTaskPane(oWordApp.ActiveDocument); //GLOG 7576: Ensure TaskPane for active document is displayed
                            System.Windows.Forms.Application.DoEvents(); //GLOG 15823 (dm)
                            LMP.MacPac.Application.SelectTaskPane();
                            break;
                        case "RefreshTaskPane":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            
                            TaskPanes.RefreshAll();
                            break;
                        case "CreateLegacyPrefill":
                            if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowLegacyDocumentFunctionality)
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                //remove ribbon item
                                RemoveRibbonItem(xCommandID);

                                break;
                            }
                            if (!Session.AdminMode && LMP.MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true)) //GLOG 5443 (dm)
                                return;
                            LMP.MacPac.Application.CreateLegacyPrefill();
                            break;
                        case "Convert":
                            if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowLegacyDocumentFunctionality)
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                //remove ribbon item
                                RemoveRibbonItem(xCommandID);

                                break;
                            }
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true)) //GLOG 5443 (dm)
                                return;
                            LMP.MacPac102007.TaskPanes.ShowTaskPane();
                            //GLOG 2835: re-enable events to ensure Taskpane will be created for new document
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.ConvertLegacyDocument();
                            break;
                        case "SelectTaskPane":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            LMP.MacPac.Application.SelectTaskPane();
                            break;
                        case "ToggleXMLCodes":
                            LMP.Forte.MSWord.Application.ToggleXMLCodes();
                            break;
                        case "ManagePeople":
                            if (!Session.AdminMode && LMP.MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            LMP.MacPac.Application.ManagePeople();
                            break;
                        case "ResetNormalStyles":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.ResetNormalStyles();
                            break;
                        case "EditAuthorPreferences":
                            if (!Session.AdminMode && LMP.MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            LMP.MacPac.Application.EditAuthorPreferences();
                            break;
                        case "ManageUserAppSettings":
                            if (!Session.AdminMode && LMP.MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            LMP.MacPac.Application.ManageUserAppSettings();
                            break;
                        // GLOG : 3135 : JAB
                        // Handle update of the document content.
                        case "UpdateDocumentContent":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true)  ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            LMP.MacPac.Application.UpdateDocumentContent();
                            break;
                        // JAB TODO : REMOVE this case.
                        case "UpdateContactDetail":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            LMP.MacPac.Application.UpdateDocumentContactDetail();
                            break;
                        case "PasteSegmentXML":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            TaskPanes.ShowTaskPane();
                            //GLOG 3212: Make sure TaskPane will display if pasting in new document
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.TaskPane.PasteSegmentXML();
                            this.Ribbon.InvalidateControl("PasteSegmentXML");
                            break;
                        case "OfficeAddress":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.InsertOfficeAddress();
                            break;
                        case "InsertContactDetail":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.InsertContactDetail();
                            break;
                        case "InsertFormattedAddresses":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.InsertFormattedAddresses();
                            break;
                        case "SetNormalStyleFont":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.SetNormalStyleFont();
                            break;
                        //GLOG : 7720 : ceh
                        case "SetSentenceSpacingToOne":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.SetSentenceSpacingToOne();
                            break;
                        //GLOG : 7720 : ceh
                        case "SetSentenceSpacingToTwo":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.SetSentenceSpacingToTwo();
                            break;
                        case "InsertPageNumber":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.InsertPageNumber();
                            break;
                        case "RemovePageNumber":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            LMP.MacPac.Application.RemovePageNumber();
                            break;
                        case "Synchronize":
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            LMP.MacPac.Application.ExecuteOnDemandSync();
                            break;
                        case "ShowAdministrator":
                            LMP.MacPac.Application.ShowAdministrator();
                            break;
                        case "ConvertUserSegments":
                            //GLOG 5422: Disable function after Admin data has been updated
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            LMP.MacPac.Application.ConvertUserSegmentsIfNecessary();
                            break;
                        case "SaveAllSegmentDesigns":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.TaskPane.SaveAllSegments();
                            break;
                        //GLOG : 7494 : CEH
                        case "UpdateForWord2013Compatibility":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.TaskPane.UpdateDocumentForWord2013Compatibility();
                            break;
                        //GLOG : 6979 : CEH
                        case "ConvertSegmentTables":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.TaskPane.ConvertAllTables();
                            break;
						//GLOG : 6979 : CEH
                        case "TestSegmentTables":
                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.TaskPane.ConvertAllTables(true);
                            break;
                        case "ConvertAdminDB":
                            LMP.MacPac.Application.ConvertDBToContentControls();
                            break;
                        case "PreconvertAdminDB":
                            LMP.MacPac.Application.PreconvertDB();
                            break;
                        case "CreateInterrogatories":
                            LMP.MacPac.Application.CreateInterrogatories();
                            break;
                        case "TagBookmarkedDoc":
                            LMP.MacPac.Application.AddBoundingObjectsToBookmarkedDoc();
                            break;
                        case "InsertTrailer":
                            //GLOG 5443 (dm): don't allow trailer if .docx file doesn't support content controls
                            //GLOG 7496 (dm) - removed condition that disallowed for .doc in Word 2013 -
                            //we now allow trailer in .doc even when there's no support for xml tags
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                !bMacPacFunctionalityIsAvailable)
                                    return;

                            //GLOG 7342 (dm) - if doc can't be unprotected, stop before inserting extra paragraph in
                            //header when checking whether document is marked for no trailer
                            if (ForteDocument.DocCannotBeUnprotected(oWordApp.ActiveDocument))
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_ProtectedDocument"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                return;
                            }
                            
                            LMP.MacPac.Application.InsertTrailer();
                            break;
                        case "InsertDraftStamp":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;
                            LMP.MacPac.Application.InsertDraftStamp();
                            break;
                        case "RefreshNormalTemplateStyles":
                            break;
                        case "InsertSymbol":
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                return;
                            //GLOG : 7637 : ceh
                            try
                            {
                                ThisAddIn.WordApplication.RunOld("InsertSymbol");
                            }
                            catch { }
                            
                            break;
                        case "Pause":
                            if (!bMacPacFunctionalityIsAvailable)
                                return;

                            if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowPauseResume)
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                //remove ribbon item
                                RemoveRibbonItem(xCommandID);

                                break;
                            }
                            if (!LMP.MacPac.Application.Paused)
                            {
                                LMP.MacPac.Application.Paused = true;
                                TaskPanes.HideAllTaskPanes();
                                LMP.MacPac.Application.DisableTaskpaneEvents = true;
                            }
                            else
                            {
                                LMP.MacPac.Application.DisableTaskpaneEvents = false;
                                LMP.MacPac.Application.Paused = false;
                            }

                            //refresh ribbon
                            Ribbon.Invalidate();
                            break;
                        case "RemoveAllTags":
                            if (!bMacPacFunctionalityIsAvailable)
                                return;

                            if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowDocumentTagRemoval)
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                //remove ribbon item
                                RemoveRibbonItem(xCommandID);

                                break;
                            }
                            else if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true)) //GLOG 5443 (dm)
                                return;

                            //GLOG 7480 (dm) - allow user to proceed in Word 2013 .doc format, so that
                            //orphaned bookmarks and doc vars can be removed
                            bool bRemoved = false;
                            if (!LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, false))
                            {
                                bRemoved = LMP.MacPac.Application.RemoveBoundingObjects(oWordApp.ActiveDocument,
                                    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All);
                            }
                            else
                            {
                                bRemoved = (MessageBox.Show(LMP.Resources.GetLangString("Prompt_RemoveDocVarsAndBookmarksInDocument"),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) == DialogResult.Yes);
                            }

                            if (bRemoved)
                            {
                                try
                                {
                                    //GLOG item #5906 - dcf -
                                    //make sure that Finished Data tab is not visible
                                    //GLOG 7480 (dm) - there won't be a task pane in Word 2013 .doc
                                    Microsoft.Office.Tools.CustomTaskPane oCTP = TaskPanes.Item(oWordApp.ActiveDocument);
                                    if (oCTP != null)
                                        ((LMP.MacPac.TaskPane) oCTP.Control).DataReviewerVisible = false;

                                    ThisAddIn.CurrentSegment = null;
                                    LMP.MacPac102007.TaskPanes.HideCurrentTaskPane();
                                    this.Ribbon.Invalidate();

                                    //GLOG : 6534 : JSW 
                                    //remove any leftover mp bookmarks
                                    try
                                    {
                                        LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                                        oConvert.DeleteBookmarksAndDocVars(oWordApp.ActiveDocument);
                                    }
                                    catch { }
                                }
                                catch { }
                            }

                            break;
                        case "RemoveVariableAndBlockTags":
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowDocumentMemberTagRemoval)
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);

                                    //remove ribbon item
                                    RemoveRibbonItem(xCommandID);

                                    break;
                                }
                                else if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                    LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true)) //GLOG 5443 (dm))
                                    return;

                                try
                                {
                                    //GLOG item #6058 - dcf
                                    DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_RemoveVarAndBlockTagsInDocument"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                                    if (iRes == DialogResult.Yes)
                                    {
                                        LMP.MacPac.Application.ScreenUpdating = false;

                                        LMP.MacPac.TaskPane oTP = null;

                                        try
                                        {
                                            oTP = LMP.MacPac.TaskPanes.Item(oWordApp.ActiveDocument);
                                        }
                                        catch { }

                                        if (oTP != null)
                                        {
                                            //GLOG : 6049 : JSW
                                            if (LMP.MacPac.Session.CurrentUser.UserSettings.CloseTaskPaneOnFinish)
                                                oTP.WordTaskPane.Visible = false;
                                            LMP.MacPac.Application.FinishDocument(oTP.ForteDocument, true); //GLOG 6811

                                            //GLOG 7921 (dm) - if finishing labels/envelopes in temp doc, the document
                                            //corresponding to oTP will no longer exist, resulting in errors below
                                            string xName = null;
                                            try
                                            {
                                                xName = oTP.ForteDocument.WordDocument.Name;
                                            }
                                            catch { }
                                            if (xName != null)
                                                oTP.UpdateAfterFinish();
                                        }
                                        else
                                        {
                                            LMP.MacPac.Application.FinishDocument(LMP.Forte.MSWord.WordApp.ActiveDocument(), false); //GLOG 7916 (dm)
                                        }
                                    }
                                }
                                finally
                                {
                                    LMP.MacPac.Application.ScreenUpdating = true;
                                }
                                break;
                            }
                        case "AboutMacPac":
                            LMP.MacPac.Application.ShowAbout();
                            break;
                        case "SaveData":
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;

                            LMP.MacPac.Application.SaveData(ThisAddIn.CurrentSegment);
                            break;
                        case "SaveSegmentWithData":
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;

                            LMP.MacPac.Application.SaveSegmentWithData(ThisAddIn.CurrentSegment);
                            break;
                        case "RecreateSegment":
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable) 
                                return;

                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.RecreateSegment(ThisAddIn.CurrentSegment);
                            break;
                        case "CopySegment":
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) ||
                                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable)
                                return;

                            LMP.MacPac.Application.CopySegmentContent(ThisAddIn.CurrentSegment);
                            break;
                        case "DeleteSegment":
                            {
                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oWordApp.ActiveDocument);

                                if (oTP == null || !bMacPacFunctionalityIsAvailable)
                                    return;

                                oTP.DeleteSegment(ThisAddIn.CurrentSegment);
                                break;
                            }
                        case "TransformDocument":
                            {
                                //GLOG : 6476 : CEH
                                if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) ||
                                    LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oWordApp.ActiveDocument);

                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                Transform();
                                break;
                            }
                        case "RefreshStyles":
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) || //GLOG 5443 (dm)
                                !bMacPacFunctionalityIsAvailable) 
                                return;

                            LMP.MacPac.Application.RefreshStyles(ThisAddIn.CurrentSegment);
                            break;
                        case "RefreshSegmentNode":
                            {
                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.RefreshSegmentNode(ThisAddIn.CurrentSegment);
                                break;
                            }
                        case "CreateMasterData":
                            {
                                LMP.MacPac.Application.CreateMasterData();
                                bShowTaskPane = true;
                                break;
                            }
                        case "AddLetterSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.AddLetterSignature(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "ChangeLetterSignatureType":
                            {
                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                               oTP.ChangeLetterSignatureType(ThisAddIn.CurrentSegment);

                                bShowTaskPane = true;
                                break;
                            }
                        case "EditLetterSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.LetterSignature, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "EditLetterhead":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.Letterhead, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "ChangeLetterheadType":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                bool bChanged = LMP.MacPac.Application.ChangeLetterhead(ThisAddIn.CurrentSegment);

                                if (bChanged)
                                {
                                    //only one of these will be visible
                                    LMP.MacPac.Application.MacPac10Ribbon.InvalidateControl("EditLetterLetterhead");
                                    LMP.MacPac.Application.MacPac10Ribbon.InvalidateControl("EditMemoLetterhead");
                                    LMP.MacPac.Application.MacPac10Ribbon.InvalidateControl("EditFaxLetterhead");
                                }

                                bShowTaskPane = false;
                                break;
                            }
                        case "ChangePleadingPaperType":
                            {
                                Word.Document oDoc = oWordApp.ActiveDocument;

                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, true))
                                    return;

                                //GLOG 7995 (dm) - remmed in light of Microsoft hotfix
                                ////GLOG 7816 (dm) - if document is in 2013 compatibility mode, prompt user
                                ////that reversion to 2010 compatibility mode is necessary to proceed
                                ////GLOG 6967 (dm) - only prompt toolkit users and don't convert
                                ////until after chooser dialog
                                //if (LMP.MacPac.MacPacImplementation.IsToolkit &&
                                //    (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oDoc) > 14))
                                //{
                                //    DialogResult iResult = MessageBox.Show(LMP.Resources.GetLangString
                                //        ("Msg_CompatibilityModeDoesNotSupportPleadingPaper"), LMP.ComponentProperties.ProductName,
                                //        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                //    if (iResult == DialogResult.No)
                                //        return;
                                //}

                                //GLOG 7273 (dm) - create an ForteDocument if necessary to give
                                //Tools access to the current pleading paper
                                Segment oCurrentSeg = null;
                                if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                                {
                                    //GLOG : 7203 : ceh
                                    int iIndex = oDoc.Application.Selection.Sections[1].Index;
                                    bool bSubsequentSection = oDoc.Sections.Count > iIndex;
                                    if (oDoc.Sections[iIndex].PageSetup.DifferentFirstPageHeaderFooter != -1)
	                                {
                                        DialogResult iYesNo = DialogResult.No;
                                        if (bSubsequentSection)
                                        {
                                            iYesNo = MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ContinuePleadingPaperOneSectionWithSubsequent"), iIndex),
                                                LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);
                                        }
                                        else
                                        {
                                            iYesNo = MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ContinuePleadingPaperOneSection"), iIndex),
                                                LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);
                                        }
                                        if (iYesNo != DialogResult.Yes)
                                            return;
	                                }

                                    oMPDoc = null;
                                    TaskPane oTP = LMP.MacPac.TaskPanes.Item(oDoc);
                                    if (oTP != null)
                                        oMPDoc = oTP.ForteDocument;
                                    else
                                        oMPDoc = new ForteDocument(oDoc);

                                    oCurrentSeg = oMPDoc.GetTopLevelSegmentFromSelection();
                                }
                                else
                                    oCurrentSeg = ThisAddIn.CurrentSegment;

                                //change pleading paper
                                bool bChanged = LMP.MacPac.Application.ChangePleadingPaper(oCurrentSeg);
                                
                                LMP.MacPac.Application.MacPac10Ribbon.InvalidateControl("EditPleadingPaper");
                                
                                if (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator) //GLOG 7739 (dm)
                                    bShowTaskPane = true; //GLOG 7618
                                break;
                            }
                        //GLOG : 7203 : ceh
                        case "ChangePleadingPaperTypeAllSections":
                            {
                                Word.Document oDoc = oWordApp.ActiveDocument;

                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, true))
                                    return;

                                //GLOG 7995 (dm) - remmed in light of Microsoft hotfix
                                ////GLOG 7816 (dm) - if document is in 2013 compatibility mode, prompt user
                                ////that reversion to 2010 compatibility mode is necessary to proceed
                                ////GLOG 6967 (dm) - only prompt toolkit users and don't convert
                                ////until after chooser dialog
                                //if (LMP.MacPac.MacPacImplementation.IsToolkit &&
                                //    (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oDoc) > 14))
                                //{
                                //    DialogResult iResult = MessageBox.Show(LMP.Resources.GetLangString
                                //        ("Msg_CompatibilityModeDoesNotSupportPleadingPaper"), LMP.ComponentProperties.ProductName,
                                //        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                //    if (iResult == DialogResult.No)
                                //        return;
                                //}

                                //GLOG 7273 (dm) - create an MPDocument if necessary to give
                                //Tools access to the current pleading paper
                                Segment oCurrentSeg = null;
                                if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                                {
                                    DialogResult iYesNo = DialogResult.No;
                                    iYesNo = MessageBox.Show(LMP.Resources.GetLangString("Prompt_ContinuePleadingPaperAllSections"),
                                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question);
                                    if (iYesNo != DialogResult.Yes)
                                        return;

                                    oMPDoc = null;
                                    TaskPane oTP = LMP.MacPac.TaskPanes.Item(oDoc);
                                    if (oTP != null)
                                        oMPDoc = oTP.ForteDocument;
                                    else
                                        oMPDoc = new ForteDocument(oDoc);

                                    oCurrentSeg = oMPDoc.GetTopLevelSegmentFromSelection();
                                }
                                else
                                    oCurrentSeg = ThisAddIn.CurrentSegment;

                                //change pleading paper
                                bool bChanged = LMP.MacPac.Application.ChangePleadingPaper(oCurrentSeg, true);

                                LMP.MacPac.Application.MacPac10Ribbon.InvalidateControl("EditPleadingPaper");
                                if (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator) //GLOG 7739 (dm)
                                    bShowTaskPane = true; //GLOG 7618
                                break;
                            }
                        case "ReplacePleadingPaper":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.Application.ReplacePleadingPaper();                               
                                break;
                            }
                        case "EditPleadingPaper":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.PleadingPaper, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "AddAgreementSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.AddAgreementSignature(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "ChangeAgreementSignatureType":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.ChangeAgreementSignatureType(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "EditAgreementSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.AgreementSignature, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "AddCounsel":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.AddCounsel(ThisAddIn.CurrentSegment);

                                bShowTaskPane = true;
                                break;
                            }
                        case "AddCaption":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.AddCaption(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "AddPleadingSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.AddPleadingSignature(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "EditPleadingSignature":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);
                                //GLOG : 6160 : CEH
                                if (ThisAddIn.CurrentSegment.Contains(mpObjectTypes.PleadingSignatureNonTable))
                                    oTP.SelectChild(mpObjectTypes.PleadingSignatureNonTable, ThisAddIn.CurrentSegment);
                                else
                                    oTP.SelectChild(mpObjectTypes.PleadingSignature, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "ChangePleadingCounselType":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.ChangePleadingCounselType(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "ChangePleadingCaptionType":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.ChangePleadingCaptionType(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "EditPleadingCounsel":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.PleadingCounsel, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "EditPleadingCaption":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.SelectChild(
                                    mpObjectTypes.PleadingCaption, ThisAddIn.CurrentSegment);

                                bShowTaskPane = false;
                                break;
                            }
                        case "ChangePleadingSignatureType":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.ChangePleadingSignatureType(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "AlignTextToPleadingPaper":
                            if (!bMacPacFunctionalityIsAvailable)
                                return;

                            oMPDoc = null;

                            if (ThisAddIn.CurrentSegment != null)
                            {
                                oMPDoc = ThisAddIn.CurrentSegment.ForteDocument;
                            }
                            else
                            {
                                oMPDoc = new ForteDocument(ThisAddIn.WordApplication.ActiveDocument);
                            }

                            LMP.MacPac.Application.AlignTextToPleadingPaper(oMPDoc);
                            break;
                        case "InsertExhibits":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.InsertExhibits(ThisAddIn.CurrentSegment);
                            }
                            bShowTaskPane = true;
                            break;
                        case "EditVariable":
                            {
                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                oTP.EditVariable();
                            }
                            break;
                        case "CreateDataSourceFile":
                            {
                                //GLOG : 7576 : ceh
                                //re-enable events to ensure Taskpane will be created for new merged document
                                ThisAddIn.DisableEventHandlers = false;
                                Contacts.CreateDataFile();
                            }
                            break;
                        case "InsertTOA":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                //GLOG 7256 (dm) - toolkit requires a hidden task pane
                                TaskPane oTP = null;
                                if (LMP.MacPac.MacPacImplementation.IsToolkit)
                                    oTP = LMP.MacPac102007.TaskPanes.AddHiddenTaskPaneIfNecessary(oWordApp.ActiveDocument);
                                else
                                    oTP = LMP.MacPac102007.TaskPanes.ShowTaskPaneIfNecessary(oWordApp.ActiveDocument);
                                if (oTP != null && !oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                //GLOG 7243 (dm) - TOA button should update existing in all cases, not just toolkit
                                if ((oTP != null) && oTP.ForteDocument.ContainsSegmentOfType(mpObjectTypes.TOA))
                                {
                                    //GLOG 7184 (dm) - Toolkit TOA button updates existing
                                    Segments oSegs = LMP.Architect.Api.Segment.GetSegments(oTP.ForteDocument,
                                        mpObjectTypes.TOA, 1);
                                    LMP.MacPac.Application.UpdateTOA(oSegs[0]);
                                }
                                else
                                {
                                    //insert
                                    bool bInserted = LMP.MacPac.Application.InsertTOA(ThisAddIn.CurrentSegment);

                                    //GLOG item #7168 - dcf
                                    if (bInserted)
                                    {
                                        //ensure that task pane shows after insertion of TOA
                                        oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(oWordApp.ActiveDocument);
                                    }
                                }
                            }
                            break;
                        case "UpdateTOA":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                LMP.MacPac.Application.UpdateTOA(ThisAddIn.CurrentSegment);
                            }
                            break;
                        case "RefreshTOA":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                LMP.MacPac.Application.RefreshFieldCodes(ThisAddIn.CurrentSegment);
                            }
                            break;
                        case "ApplyQuoteStyle":
                            {
                                LMP.MacPac.Application.SetQuoteStyle(ThisAddIn.CurrentSegment);
                            }
                            break;
                        case "ShowSegmentHelp":
                            {
                                try
                                {
                                    if (!bMacPacFunctionalityIsAvailable)
                                        return;

                                    LMP.MacPac.Help oHelp = new LMP.MacPac.Help(ThisAddIn.CurrentSegment.Definition.HelpText);
                                    oHelp.Text = LMP.ComponentProperties.ProductName + " Help - " + ThisAddIn.CurrentSegment.DisplayName;
                                    oHelp.Show();
                                }
                                catch (System.Exception oE)
                                {
                                    LMP.Error.Show(oE);
                                }
                            }
                            break;
                        case "InsertAgreementTitlePage":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.InsertAgreementTitlePage(ThisAddIn.CurrentSegment);
                            }
                            bShowTaskPane = true;
                            break;
                        case "PrintFinal":
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                        return;

                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.PrintFinal(ThisAddIn.WordApplication.ActiveDocument);
                                break;
                            }
                        case "PrintDraft":
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.PrintDraft(ThisAddIn.WordApplication.ActiveDocument);
                                break;
                            }
                        case "PrintCurrentPage":
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.PrintCurrentPage(ThisAddIn.WordApplication.ActiveDocument);
                                break;
                            }
                        case "PrintSelectedLetterhead":
                        	//GLOG : 6478 : jsw
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.PrintSelectedLetterhead(ThisAddIn.CurrentSegment);
                                break;
                            }
                        case "PreconvertDocument": //JTS 11/06/11: Preconversion
                            {
                                if (!bMacPacFunctionalityIsAvailable)
                                    return;

                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.PreconvertDocument(oWordApp.ActiveDocument);
                                break;
                            }
                        case "InsertPleadingSectionBreak": //GLOG 6266
                            {
                                if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                    return;
                                ThisAddIn.DisableEventHandlers = false;
                                LMP.MacPac.Application.InsertSectionBreak(oWordApp.ActiveDocument, ThisAddIn.CurrentSegment);
                                break;
                            }
                        case "InsertNonBreakingSpaceInDates": //GLOG 7719
                            {
                                if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                                    return;
                                LMP.Forte.MSWord.WordDoc.MonthsWithNonBreakingSpaces();
                                break;
                            }
                        case "EditFavoriteSegments":
                            {
                                LMP.MacPac.Application.EditFavoriteSegments();
                                break;
                            }
                        case "RemoveTestDocuments":  //GLOG : 6112 : CEH
                            LMP.Forte.MSWord.WordApp.RemoveTestDocuments();
                            break;
                        case "ShowOrphanedDocVarsInDesign": //GLOG 8213
                            LMP.MacPac.Application.ShowOrphanedDocVarsInDesign();
                            break;
                        case "ListVBAExpressions":
                            LMP.MacPac.Application.ListVBAExpressions();
                            break;
                        case "CreateGrailDataDump":
                            LMP.MacPac.Application.CreateGrailDataDump();
                            break;
                        case "InitializeForte":    //GLOG : 6112 : CEH

                            if (LMP.MacPac.Application.ContainsSegmentInDesign())
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_SegmentInDesignNotAllowed"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                                return;
                            }

                            //quit session
                            Session.Quit();

                            //clear any existing AdminMode key
                            LMP.Registry.SetCurrentUserValue(
                                @"Software\The Sackett Group\Deca", "AdminMode", "");

                            //refresh ribbon
                            this.Ribbon.Invalidate();
                            
                            //restart session
                            Session.StartIfNecessary(oWordApp);

                            //GLOG : 6593 : ceh
                            //initialize Numbering
                            //GLOG 15940: Avoid error message if Numbering is not installed
                            try
                            {
                                oWordApp.RunOld("zzmpReinitialize");
                            }
                            catch { }

                            //refresh taskpane
                            if (!LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true) &&
                            !LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, true) &&
                            !LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true) &&
                            bMacPacFunctionalityIsAvailable)
                                TaskPanes.RefreshAll();
                            
                            break;
                        case "EditCIIni":   //GLOG : 6112 : CEH
                            //get path to ci.ini
                            string xCIIni = LMP.Data.Application.GetDirectory(mpDirectories.CIData) + @"\CI.ini";
                            if (File.Exists(xCIIni))
                            {
                                try
                                {
                                    //open file in notepad
                                    System.Diagnostics.Process.Start("notepad.exe", xCIIni);
                                }
                                catch (System.Exception)
                                {
                                    ////couldn't open file - alert
                                    //string xMsg = string.Concat(string.Format(LMP.Resources.GetLangString(
                                    //    "Error_Notepad_Show", xCIIni)), " ", e.Message);
                                    //Exception oE = new Exception(xMsg);
                                    //throw oE;
                                }
                            }
                            else
                            {
                                throw new LMP.Exceptions.FileException(
                                    LMP.Resources.GetLangString("Error_CouldNotFindFile") + xCIIni);
                            }
                            break;
                        case "EditNumberingIni":    //GLOG : 6112 : CEH
                            //GLOG : 8661 : jsw
                            string xNumeringIni = LMP.Data.Application.GetDirectory(mpDirectories.NumberingData) + @"\tsgNumbering.INI";
                            if (File.Exists(xNumeringIni))
                            {
                                try
                                {
                                    //open file in notepad
                                    System.Diagnostics.Process.Start("notepad.exe", xNumeringIni);
                                }
                                catch (System.Exception)
                                {
                                    ////couldn't open file - alert
                                    //string xMsg = string.Concat(LMP.Resources.GetLangString(
                                    //    "Error_Benchmarks_Show"), " ", e.Message);
                                    //Exception oE = new Exception(xMsg);
                                    //throw oE;
                                }
                            }
                            else
                            {
                                throw new LMP.Exceptions.FileException(
                                    LMP.Resources.GetLangString("Error_CouldNotFindFile") + xNumeringIni);
                            }
                            break;
                        case "LayoutAgreementSignatures":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.LayoutAgreementSignatures(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "LayoutLetterSignatures":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.LayoutLetterSignatures(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "LayoutPleadingSignatures":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.LayoutPleadingSignatures(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "LayoutPleadingCaptions":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.LayoutPleadingCaptions(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "LayoutPleadingCounsels":
                            {
                                //GLOG 7651
                                if (LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oWordApp.ActiveDocument, true))
                                    return;

                                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(
                                    ThisAddIn.CurrentSegment.ForteDocument.WordDocument);

                                if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                                    return;

                                oTP.LayoutPleadingCounsels(ThisAddIn.CurrentSegment);
                                bShowTaskPane = true;
                                break;
                            }
                        case "InsertMultipleSegments": //GLOG 7316

                            ThisAddIn.DisableEventHandlers = false;
                            LMP.MacPac.Application.InsertMultipleSegments("", null, LMP.MacPac102007.TaskPanes.ShowTaskPane);
                            break;
                    }

                    //GLOG item #5891 - dcf
                    if (bShowTaskPane && Session.CurrentWordApp.Documents.Count > 0) //GLOG 7316
                    {
                        Word.Document oDoc = oWordApp.ActiveDocument;

                        if (!LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, true) &&
                            !LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, true)) //GLOG 5443 (dm)
                        {
                            LMP.MacPac102007.TaskPanes.ShowTaskPaneIfNecessary(oDoc);

                            TaskPane oActiveTP = LMP.MacPac.TaskPanes.Item(oDoc);
                            //GLOG 7618: Select Editor if necessary
                            if (oActiveTP != null && oActiveTP.ActiveTab != 1 )
                            {
                                oActiveTP.SelectTab(TaskPane.Tabs.Edit);
                                oActiveTP.Refresh (false, true, true, true);
                                oActiveTP.SelectEditorNodeOfSelectedSegment();
                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                //Re-enable Open and Change handlers
                ThisAddIn.DisableEventHandlers = false;
            }
        }
        /// <summary>
        /// executes the specified custom macro - 
        /// control.Id contains the name of the macro
        /// along with all macro arguments - must be
        /// specified in the form Macro_MacroName_Arg1_Arg2_Etc
        /// </summary>
        /// <param name="control"></param>
        public void ExecuteCustomMacro(Office.IRibbonControl control)
        {
            try
            {
                string xTemp = control.Id.Substring(6);
                ExecuteCustomMacro(xTemp);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// executes the macro with the specified name - 
        /// passes in macro arguments as well
        /// </summary>
        /// <param name="xMacroName_Arg1_Arg2_Etc"></param>
        public void ExecuteCustomMacro(string xMacroName_Arg1_Arg2_Etc)
        {
            //split at first "_"
            int iPos = xMacroName_Arg1_Arg2_Etc.IndexOf("_");

            //get macro name and arguments
            string xMacroName = null;
            string xArgs = "";

            if (iPos > -1)
            {
                xMacroName = xMacroName_Arg1_Arg2_Etc.Substring(0, iPos);
                xArgs = xMacroName_Arg1_Arg2_Etc.Substring(iPos + 1);
            }
            else
                xMacroName = xMacroName_Arg1_Arg2_Etc;

            LMP.Forte.MSWord.WordApp.CallMacro(xMacroName, xArgs, "_");
        }
        private void RemoveRibbonItem(string xCommandID)
        {
            string xElementStart = null;
            string xElementEnd = "/>";

            //GLOG 7898 (dm)
            string xRibbonName = "";
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
                xRibbonName = "ForteToolsRibbon.xml";
            else
                xRibbonName = "ForteRibbon.xml";

            string xXML = File.ReadAllText(LMP.Data.Application.AppDirectory + "\\" + xRibbonName);

            switch (xCommandID)
            {
                case "Pause":
                    xElementStart = "<button id=\"Pause\"";
                    break;
                case "RemoveTags":
                    xElementStart = "<button id=\"RemoveTags\"";
                    break;
                case "ChangeUser":
                    xElementStart = "<dynamicMenu id=\"ChangeUser\"";
                    break;
                case "CreateLegacyPrefill":
                case "Convert":
                    xElementStart = "<menu id=\"LegacyDocuments\"";
                    xElementEnd = "</menu>";
                    break;
                default:
                    return;
            }

            if (xXML.IndexOf("<!--" + xElementStart) > -1)
                //item is already commented out -
                //don't do anything
                return;

            xXML = xXML.Replace(xElementStart, "<!--" + xElementStart);

            //find end of xml element -
            //there could be several of these elements,
            //as we support the ability to add Forte ribbon items
            //to other ribbon tabs - we have to find all
            //end of elements, and edit each
            int iPos = xXML.IndexOf(xElementStart);

            while (iPos > -1)
            {
                iPos = xXML.IndexOf(xElementEnd, iPos);

                //add end of comment to end of element
                xXML = xXML.Substring(0, iPos + xElementEnd.Length) + 
                    "-->" + xXML.Substring(iPos + xElementEnd.Length);

                //find next instance of this element
                iPos = xXML.IndexOf(xElementStart, iPos);
            }

            //write to file
            File.WriteAllText(LMP.Data.Application.AppDirectory + "\\" + xRibbonName, xXML);            
        }

        public string GetTabLabel(Office.IRibbonControl oCtl)
        {
            if (ThisAddIn.CurrentSegment != null)
            {
                //GLOG : 7129 : 8197 : jsw
               if (LMP.Forte.MSWord.WordApp.Version == 15)
                    return LMP.Data.Application.GetObjectTypeDisplayName(
                        ThisAddIn.CurrentSegment.TypeID).ToUpper();
                else
                    return LMP.Data.Application.GetObjectTypeDisplayName(
                        ThisAddIn.CurrentSegment.TypeID);
            }
            else
                //GLOG 8191: avoid <<No label>> entry in customize ribbon tab list
                return "Segment";
        }

        // GLOG : 5025 : CEH
        public bool GetSegmentTabVisible(Office.IRibbonControl oCtl)
        {
            bool bTabVisible = false;
            //GLOG : 8041 : ceh
            bool bLabelEnvelopeIsOnlySegment = (ThisAddIn.CurrentSegment is LMP.Architect.Base.IStaticDistributedSegment && 
                                                ThisAddIn.CurrentSegment.ForteDocument.Segments.Count < 2);

            //GLOG 6006 (dm) - don't show segment-specific ribbon in design mode
            //GLOG : 8041 : ceh - don't show segment-specific ribbon if finished label/envelope is only segment
            if ((ThisAddIn.CurrentSegment != null) && (ThisAddIn.CurrentSegment.ForteDocument.Mode != ForteDocument.Modes.Design) &&
                !(bLabelEnvelopeIsOnlySegment && !ThisAddIn.CurrentSegment.ForteDocument.ContainsUnfinishedContent()))
            {
                bTabVisible = Session.CurrentUser.FirmSettings.ShowSegmentSpecificRibbons;
            }

            return bTabVisible;
        }

        public bool GetPressed(Office.IRibbonControl oCtl)
        {
            if (oCtl.Id == "Pause")
            {
                return LMP.MacPac.Application.Paused;
            }
            else
                return true;
        }
        /// <summary>
        /// opens the specified help file
        /// </summary>
        /// <param name="oCtl"></param>
        /// <returns></returns>
        public void OpenHelpFile(Office.IRibbonControl oCtl)
        {
            try
            {
                LMP.MacPac.Application.ShowHelpFile(oCtl.Tag);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #region These methods allow macpac commands to be displayed on custom ribbon tabs
        public string GetFavoriteSegmentsMenu(Office.IRibbonControl oCtl)
        {
            try
            {
                string xMenu = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                string xHotKeys = "";
                string xNonChars = " //,.:()[]_-123456789<>;{}+&";
                Word.Application oWordApp = ThisAddIn.WordApplication;
                string xName = "";

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return "";

                for (int i = 0; i < FavoriteSegments.Count; i++)
                {
                    //GLOG 7464: Replace reserved characters with XML tokens
                    //and also make sure '&' doesn't display as a hotkey
                    xName = String.ReplaceXMLChars(FavoriteSegments.SegmentDisplayName(i)).Replace("&amp;", "&amp;&amp;");
                    
                    string xChar = "";
                    string xLabel = "";
                    //GLOG : 6303 : jsw
                    //add intended use/folder member type icon to favorites 
                    string xTag = "";
                    //GLOG : 6662 : JSW
                    //dynamic image line in button xml 
                    string xGetImageLine = ""; 
                    mpSegmentIntendedUses lIntendedUse = FavoriteSegments.SegmentIntendedUses(i);
                    mpFolderMemberTypes lFolderType = FavoriteSegments.SegmentFolderMemberType(i);
                    //GLOG : 8363 : JSW
                    string xFileExtension = ""; 

                    int iCount = 0;
                    int iMin = 1;

                    //GLOG : 3179 : jsw
                    //hotkey is 1-9 for first 9 segments, then unique 
                    //character from segment name
                    if (i < 9)
                    {
                        xLabel = " &amp;" + (i + 1).ToString() + ". " + xName;
                    }
                    else
                    {
                        for (int y = 0; y < xName.Length; y++)
                        {
                            xChar = xName.Substring(y, 1);

                            if (!xHotKeys.Contains(xChar) && !xNonChars.Contains(xChar))
                            {
                                {
                                    xLabel = (i + 1).ToString() + ". " + 
                                        xName.Substring(0, y) + "&amp;" + xName.Substring(y);
                                }
                                break;
                            }
                        }
                    }
                    if (xLabel == "")
                    {
                        for (iMin = 1; iMin < 20; iMin++)
                        {
                            for (int x = 0; x < xName.Length; x++)
                            {
                                iCount = 0; 
                                xChar = xName.Substring(x, 1);
                                for (int z = 0; z < xHotKeys.Length; z++)
                                {
                                    if (xChar == xHotKeys.Substring(z, 1))
                                        iCount++;
                                }
                                if (iCount == iMin)
                                {
                                    xLabel = (i + 1).ToString() + ". " + 
                                        xName.Substring(0, x) + "&amp;" + xName.Substring(x);
                                    break;
                                }
                            }
                            if (xLabel != "")
                                break;
                        }
                    }

                    xHotKeys += xChar;

                    //GLOG : 6303 : jsw
                    if (lFolderType == mpFolderMemberTypes.UserSegment || lFolderType == mpFolderMemberTypes.VariableSet)
                    {
                        if (lFolderType == mpFolderMemberTypes.VariableSet)
                            xTag = lFolderType.ToString();
                        else
                            xTag = lFolderType.ToString() + lIntendedUse.ToString();
                        
                    }
                    else
                    {
                        //GLOG : 8363 : JSW
                        //set tag to external document type if appropriate
                        if (lIntendedUse == mpSegmentIntendedUses.AsDocument)
                        {
                            xFileExtension = System.IO.Path.GetExtension(xName);
                            
                            //GLOG : 8795 : jsw
                            if (xFileExtension != "" && xFileExtension.Length <= 4)
                            {
                                if (xFileExtension.StartsWith(".xls") || xFileExtension.StartsWith(".xlt"))
                                {
                                    // Excel file extensions get an XL image.
                                    xTag = "XLDocument";
                                }
                                else if (xFileExtension.ToLower().StartsWith(".doc") || xFileExtension.ToLower().StartsWith(".dot"))
                                {
                                    // Word files extensions get a Word image.
                                    xTag = "WordDocument";
                                }
                                else
                                {
                                    // All other types of external content files get an external content image.
                                    xTag = "ExternalContent";
                                }
                            }
                            else
                            {
                                xTag = lIntendedUse.ToString();
                            }
                        }
                        else
                        {
                            xTag = lIntendedUse.ToString();
                        }
                    }

                    //GLOG : 6662 : JSW
                    if (lIntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                        xGetImageLine = "\" imageMso=\"AdpDiagramIndexesKeys";
                    else
                        xGetImageLine = "\" getImage=\"GetDynamicImage";

                    //GLOG 6133: ensure numeric portion of ID is fixed length - can be 1 to 35
                    xMenu += "<button id=\"FavoriteSegment" + i.ToString().PadLeft(2, '0') +
                       "\" label=\"" + xLabel +
                       xGetImageLine +
                       "\" keytip=\"" + (i + 1).ToString() +
                       "\" tag=\"" + xTag +
                       "\" screentip=\"Create a new " + xName +
                       "\" onAction=\"OnFavoriteSegmentClick\" />";
                }

                xMenu += "</menu>";

                return xMenu;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return "";
            }
        }
        public string GetProxiesMenu(Office.IRibbonControl oCtl)
        {
            try
            {
                string xMenu = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                Word.Application oWordApp = ThisAddIn.WordApplication;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return "";

                if (!LMP.MacPac.Session.CurrentUser.FirmSettings.AllowProxying)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FunctionNotAllowed"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    //remove ribbon item
                    RemoveRibbonItem("ChangeUser");

                    return "";
                }

                //get proxies
                LMP.Data.LocalPersons oPossibleUsers = Session.LoginUser.GetProxiableUsers();
                int iCount = oPossibleUsers.Count;
                int iCurUserID = Session.CurrentUser.ID;

                if (Session.LoginUser.ID == iCurUserID)
                    xMenu += "<button id=\"User" + Session.LoginUser.ID + "\" label=\"" +
                        LMP.MacPac.Session.LoginUser.PersonObject.DisplayName +
                        "\" imageMso=\"Head\" onAction=\"OnButtonClick\" />";
                else
                    xMenu += "<button id=\"User" + Session.LoginUser.ID + "\" label=\"" +
                        LMP.MacPac.Session.LoginUser.PersonObject.DisplayName +
                        "\" onAction=\"OnButtonClick\" />";

                if (iCount > 0)
                {
                    for (int i = 1; i <= iCount; i++)
                    {
                        LMP.Data.Person oPerson = oPossibleUsers.ItemFromIndex(i);
                        //GLOG 8190:  If User has inadvertently been added as proxy for self, ignore this item
                        if (oPerson.ID1 == Session.LoginUser.ID)
                        {
                            continue;
                        }
                        //GLOG 6966: Make sure string can be parsed as a Double even if
                        //the Decimal separator is something other than '.' in Regional Settings
                        string xID = oPerson.ID;
                        if (xID.EndsWith(".0"))
                            xID = xID.Replace(".0", "");
                        if (iCurUserID == (int)double.Parse(xID))
                            xMenu += "<button id=\"User" + xID + "\" label=\"" +
                                oPerson.DisplayName + "\" imageMso=\"Head\" onAction=\"OnButtonClick\" />";
                        else
                            xMenu += "<button id=\"User" + xID + "\" label=\"" +
                                oPerson.DisplayName + "\" onAction=\"OnButtonClick\" />";

                    }

                    xMenu += "</menu>";
                    return xMenu;
                }
                else
                {
                    //alert user
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoProxyRights"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return "";
            }
        }
        public void ShowHelpAbout(Office.IRibbonControl oCtl)
        {
            try
            {
                LMP.MacPac.Application.ShowAbout();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 3624 : CEH
        public string GetHelpDocumentsMenu(Office.IRibbonControl oCtl)
        {
            try
            {
                string xMenu = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                Word.Application oWordApp = ThisAddIn.WordApplication;
                //GLOG : 8418 : JSW
                int iFolderIncrementer = 0;

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                    return "";

                //get help documents
                string xHelpDocumentsDirectory = LMP.Data.Application.GetDirectory(mpDirectories.RootDirectory) +
                    @"\Tools\User Documentation\pdf\";

                //GLOG : 8418 : JSW
                DirectoryInfo oDir = new DirectoryInfo(xHelpDocumentsDirectory);
                xMenu += GetHelpDirectories(oDir, iFolderIncrementer);

                xMenu += "<menuSeparator id=\"HelpSeparator1\"/><button id=\"AboutMacPac\" label=\"About " + 
                    LMP.ComponentProperties.ProductName + "\" onAction=\"ShowHelpAbout\"/>";

                //if (i > 0)
                //{
                    xMenu += "</menu>";
                    return xMenu;
                //}
                //else
                //{
                //    //alert user
                //    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoHelpDocuments"),
                //        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    return "";
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return "";
            }
        }

        public void CreateNewDocument(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateNewDocument");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateNewWordDocument(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateNewWordDocument");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateDataSourceFile(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateDataSourceFile");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void SaveData(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SaveData");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void SaveSegmentWithData(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SaveSegmentWithData");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RecreateSegment(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RecreateSegment");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void Transform()
        {
            //prompt to confirm execution
            DialogResult iChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Prompt_ReplaceDocumentContent"),
                LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (iChoice == DialogResult.No)
                return;
            using (SegmentForm oForm = new SegmentForm(false, "", null))
            {
                oForm.Text = LMP.Resources.GetLangString("Dlg_TransformTo");
                oForm.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                    FolderTreeView.mpFolderTreeViewOptions.Prefills |
                    FolderTreeView.mpFolderTreeViewOptions.Segments |
                    FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                    FolderTreeView.mpFolderTreeViewOptions.UserSegments;
                //GLOG : 8380 : jsw
                oForm.ExcludeNonMacPacTypes = true;
                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

                    TaskPane oTP = LMP.MacPac102007.TaskPanes.ShowTaskPane(oDoc);

                    string xIDs = oForm.Value;
                    Infragistics.Win.UltraWinTree.UltraTreeNode oNode = oForm.SelectedNode;
                    LMP.MacPac.Application.Transform(oNode, oTP);
                }
            }
        }
        public void CopySegment(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CopySegment");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void DeleteSegment(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("DeleteSegment");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RefreshStyles(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RefreshStyles");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RefreshSegmentNode(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RefreshSegmentNode");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddLetterSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddLetterSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddLetterLetterhead(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddLetterhead");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangeLetterSignatureType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangeLetterSignatureType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangePleadingCounselType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangePleadingCounselType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangePleadingCaptionType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangePleadingCaptionType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditLetterSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditLetterSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditLetterhead(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditLetterhead");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditPleadingPaper(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditPleadingPaper");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditPleadingCounsel(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditPleadingCounsel");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditPleadingCaption(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditPleadingCaption");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangeLetterheadType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangeLetterheadType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddAgreementSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddAgreementSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangeAgreementSignatureType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangeAgreementSignatureType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditAgreementSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditAgreementSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void LayoutAgreementSignatures(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("LayoutAgreementSignatures");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void LayoutLetterSignatures(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("LayoutLetterSignatures");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void LayoutPleadingSignatures(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("LayoutPleadingSignatures");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void LayoutPleadingCaptions(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("LayoutPleadingCaptions");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void LayoutPleadingCounsels(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("LayoutPleadingCounsels");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateEnvelopes(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateEnvelopes");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateLabels(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateLabels");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertExhibits(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertExhibits");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertAgreementTitlePage(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertAgreementTitlePage");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertTOA(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertTOA");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertMultipleSegments(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertMultipleSegments");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void UpdateTOA(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("UpdateTOA");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AlignTextToPleadingPaper(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AlignTextToPleadingPaper");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateNewDMSDocument(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateNewDMSDocument");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void InsertContactDetail(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertContactDetail");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void InsertFormattedAddresses(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertFormattedAddresses");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void RemoveAllTags(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RemoveAllTags");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //JTS 11/06/11: Preconversion
        public void PreconvertDocument(Office.IRibbonControl oCtl)
        {
            //GLOG : 8326 : jw2
            //no longer supporting this in 11.3
            MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_FunctionalityNotSupported")), LMP.ComponentProperties.ProductName,
            MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
            //try
            //{
            //    ExecuteCommand("PreconvertDocument");
            //}
            //catch (System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}
        }

        public void RemoveVariableAndBlockTags(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RemoveVariableAndBlockTags");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ShowTaskPane(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ShowTaskPane");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RefreshTaskPane(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RefreshTaskPane");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateLegacyPrefill(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateLegacyPrefill");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void Convert(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("Convert");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void Pause(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("Pause");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ShowSegmentHelp(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ShowSegmentHelp");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ToggleXMLCodes(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ToggleXMLCodes");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ManagePeople(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ManagePeople");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ResetNormalStyles(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ResetNormalStyles");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditAuthorPreferences(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditAuthorPreferences");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ManageUserAppSettings(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ManageUserAppSettings");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void UpdateDocumentContent(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("UpdateDocumentContent");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void UpdateContactDetail(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("UpdateContactDetail");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void PasteSegmentXML(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PasteSegmentXML");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void OfficeAddress(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("OfficeAddress");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void SetNormalStyleFont(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SetNormalStyleFont");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertPageNumber(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertPageNumber");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7720 : ceh
        public void SetSentenceSpacingToOne(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SetSentenceSpacingToOne");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7720 : ceh
        public void SetSentenceSpacingToTwo(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SetSentenceSpacingToTwo");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        public void InsertPleadingSectionBreak(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertPleadingSectionBreak");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 7719 : ceh
        public void InsertNonBreakingSpaceInDates(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertNonBreakingSpaceInDates");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6025 : ceh
        public void RemovePageNumber(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RemovePageNumber");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void TransformDocument(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("TransformDocument");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void Synchronize(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("Synchronize");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void TurnOff(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("TurnOff");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ShowAdministrator(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ShowAdministrator");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void PrintFinal(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PrintFinal");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void PrintDraft(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PrintDraft");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 7323 : ceh
        public void PrintCurrentPage(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PrintCurrentPage");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		//GLOG : 6478 : jsw
        public void PrintSelectedLetterhead(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PrintSelectedLetterhead");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        public void ConvertUserSegments(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ConvertUserSegments");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void SaveAllSegmentDesigns(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("SaveAllSegmentDesigns");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7494 : CEH
        public void UpdateForWord2013Compatibility(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("UpdateForWord2013Compatibility");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6979 : CEH
        public void ConvertSegmentTables(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ConvertSegmentTables");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6979 : CEH
        public void TestSegmentTables(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("TestSegmentTables");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void ConvertAdminDB(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ConvertAdminDB");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void PreconvertAdminDB(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("PreconvertAdminDB");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void TagBookmarkedDoc(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("TagBookmarkedDoc");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateInterrogatories(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateInterrogatories");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void InsertTrailer(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertTrailer");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void InsertDraftStamp(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InsertDraftStamp");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void CreateMasterData(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateMasterData");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RefreshNormalTemplateStyles(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RefreshNormalTemplateStyles");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditFavoriteSegments(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditFavoriteSegments");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddCounsel(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddCounsel");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditCounsel(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditCounsel");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddCaption(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddCaption");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditCaption(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditCaption");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditVariable(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditVariable");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void AddPleadingSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("AddPleadingSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangePleadingSignatureType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangePleadingSignatureType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditPleadingSignature(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditPleadingSignature");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ApplyQuoteStyle(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ApplyQuoteStyle");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangePleadingPaperType(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangePleadingPaperType");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7203 : ceh
        public void ChangePleadingPaperTypeAllSections(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ChangePleadingPaperTypeAllSections");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion

        /// <summary>
        /// shows a message that a document is required
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        private void DisplayDocumentRequiredMsg()
        {
            string xCaption = LMP.ComponentProperties.ProductName;
            string xMsg = LMP.Resources.GetLangString("Msg_ActiveDocumentRequired");
            MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public Boolean GetMacPacSegmentGroupVisible(Office.IRibbonControl control)
        {
            return control.Id == "SegmentFunctions" + ThisAddIn.CurrentSegment.TypeID.ToString();
        }


        /// <summary>
        /// returns submenu content based on specified control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetMenuContent(Office.IRibbonControl control)
        {
            try
            {
                switch (GetIDRoot(control))
                {
                    case "ChangeUser":
                        return GetProxiesMenu(control);
                    case "FavoriteSegments":
                        return GetFavoriteSegmentsMenu(control);
                    default:
                        return "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return "";
            }
        }

        public string GetLabel(Office.IRibbonControl control)
        {
            //GLOG : 6477 : JSW
            //get labels for accented characters
            string xChar = "";
            string xLabel = "";
            switch (GetIDRoot(control))
            {
                case "Pause":
                    if (LMP.MacPac.Application.Paused)
                        return LMP.Resources.GetLangString("Label_Resume");
                    else
                        return LMP.Resources.GetLangString("Label_Pause");

                case "RemoveVariableTagsMenuItem":
                    if (LMP.MacPac.Session.DisplayFinishButton)
                        return LMP.Resources.GetLangString("Label_FinishDocument");
                    else
                        return LMP.Resources.GetLangString("Label_RemoveVariablesInDocument");
                //GLOG : 6477 : JSW
                case "SymbolAAcute":
                    xChar = ((char)225).ToString();
                    xLabel = " &1.  " + xChar;
                    return xLabel;
                case "SymbolAGraveCap":
                    xChar = ((char)192).ToString();
                    xLabel = " &2.  " + xChar;
                    return xLabel;
                case "SymbolAGrave":
                    xChar = ((char)224).ToString();
                    xLabel = " &3.  "  + xChar;
                    return xLabel;
                case "SymbolAUmlautCap":
                    xChar = ((char)196).ToString();
                    xLabel = " &4.  " + xChar;
                    return xLabel;
                case "SymbolAUmlaut":
                    xChar = ((char)228).ToString();
                    xLabel =  " &5.  " + xChar;
                    return xLabel;
                //GLOG : 6741 : JSW
                case "SymbolCedillaCap":
                    xChar = ((char)199).ToString();
                    xLabel = " &6.  " + xChar;
                    return xLabel;
                //GLOG : 8112 : JSW
                case "SymbolCedilla":
                    xChar = ((char)231).ToString();
                    xLabel = " &7.  " + xChar;
                    return xLabel;
                case "SymbolEAiguCap":
                    xChar = ((char)201).ToString();
                    xLabel = " &8.  " + xChar;
                    return xLabel;
                case "SymbolEAigu":
                    xChar = ((char)233).ToString();
                    xLabel = " &9.  " + xChar;
                    return xLabel;
                case "SymbolEGraveCap":
                    xChar = ((char)200).ToString();
                    xLabel = "1&0.  " + xChar;
                    return xLabel;
                case "SymbolEGrave":
                    xChar = ((char)232).ToString();
                    xLabel = "1&1.  " + xChar;
                    return xLabel;
                case "SymbolIAcute":
                    xChar = ((char)237).ToString();
                    xLabel = "1&2.  " + xChar;
                    return xLabel;
                case "SymbolSpanishNCap":
                    xChar = ((char)209).ToString();
                    xLabel = "1&3.  " + xChar;
                    return xLabel;
                case "SymbolSpanishN":
                    xChar = ((char)241).ToString();
                    xLabel = "1&4.  " + xChar;
                    return xLabel;
                case "SymbolOAcute":
                    xChar = ((char)243).ToString();
                    xLabel = "1&5.  " + xChar;
                    return xLabel;
                case "SymbolOUmlaut":
                    xChar = ((char)246).ToString();
                    xLabel = "1&6.  " + xChar;
                    return xLabel;
                //case "SymbolOE":
                //    //this is coming up blank
                //    //typed html directly into button label 
                //    //instead 
                //    xChar = ((char)156).ToString();
                //    xLabel = "1&6.  " + xChar;
                //    return xLabel;
                case "SymbolUAcute":
                    xChar = ((char)250).ToString();
                    xLabel = "1&9.  " + xChar;
                    return xLabel;
                //GLOG : 7778 : JSW
                case "SymbolUUmlautCap":
                    xChar = ((char)220).ToString();
                    xLabel = "2&0.  "+ xChar;
                    return xLabel;
                case "SymbolUUmlaut":
                    xChar = ((char)252).ToString();
                    xLabel = "2&1.  " + xChar;
                    return xLabel;
                case "SymbolSpanishQuestion":
                    xChar = ((char)191).ToString();
                    xLabel = "2&2.  " + xChar;
                    return xLabel;
                case "SymbolSpanishExclamation":
                    xChar = ((char)161).ToString();
                    xLabel = "2&3.  " + xChar;
                    return xLabel;
                case "SymbolSharpS":
                    xChar = ((char)223).ToString();
                    xLabel = "1&8.  "  + xChar;
                    return xLabel;
                default:
                    return "";
            }
        }

        public bool GetCheckboxState(Office.IRibbonControl control)
        {
            //parse ID from command id
            double dblUserID = double.Parse(control.Id.Substring(4));
            int iUserID = (int)dblUserID; ;

            return iUserID == Session.CurrentUser.ID;
        }

        /// <summary>
        /// inserts the symbol associated with the specified ribbon control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public void InsertSymbol(Office.IRibbonControl control)
        {
            Word.Application oWordApp = ThisAddIn.WordApplication;
            //Not available for protected document
            if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true))
                return;

            int iAsc = 0;
            string xFontName = "";
            bool bUnicode = false;

            switch (GetIDRoot(control))
            {
                //GLOG : 6477 : JSW
                //accented characters
                case "SymbolAGrave":
                    iAsc = 224;
                    break;
                case "SymbolEAigu":
                    iAsc = 233;
                    break;
                case "SymbolEAiguCap":
                    iAsc = 201;
                    break;
                case "SymbolEGrave":
                    iAsc = 232;
                    break;
                case "SymbolEGraveCap":
                    iAsc = 200;
                    break;
                case "SymbolSpanishN":
                    iAsc = 241;
                    break;
                case "SymbolSpanishNCap":
                    iAsc = 209;
                    break;
                case "SymbolSpanishExclamation":
                    iAsc = 161;
                    break;
                case "SymbolSpanishQuestion":
                    iAsc = 191;
                    break;
                case "SymbolAUmlaut":
                    iAsc = 228;
                    break;
                case "SymbolBulletOpen":
                    iAsc = 164;
                    break;
                case "SymbolBulletClosed":
                    iAsc = 183;
                    xFontName = "Symbol";
                    break;
                case "SymbolCent":
                    iAsc = 162;
                    break;
                case "SymbolCheckboxChecked":
                    iAsc = 253;
                    xFontName = "Wingdings";
                    break;
                case "SymbolCheckboxUnchecked":
                    iAsc = 168;
                    xFontName = "Wingdings";
                    break;
                case "SymbolCopyright":
                    iAsc = 169;
                    break;
                case "SymbolEnDash":
                    iAsc = 150;
                    break;
                case "SymbolEmDash":
                    iAsc = 151;
                    break;
                case "SymbolDefendant":
                    iAsc = 68;
                    xFontName = "Symbol";
                    break;
                case "SymbolDegree":
                    iAsc = 176;
                    break;
                //GLOG : 6477 : JSW
                case "SymbolDivision":
                    iAsc = 247;
                    break;
                case "SymbolEuro":
                    iAsc = 128;
                    break;
                case "SymbolOneHalf":
                    iAsc = 189;
                    break;
                case "SymbolOneQuarter":
                    iAsc = 188;
                    break;
                case "SymbolParagraph":
                    iAsc = 182;
                    break;
                case "SymbolPlaintiff":
                    iAsc = 112;
                    xFontName = "Symbol";
                    break;
                //GLOG : 6477 : JSW
                case "SymbolPlusMinus":
                    iAsc = 177;
                    break;
                case "SymbolPoundSterling":
                    iAsc = 163;
                    break;
                case "SymbolRegistration":
                    iAsc = 174;
                    break;
                case "SymbolSectionMark":
                    iAsc = 167;
                    break;
                case "SymbolServiceMark":
                    iAsc = -1;
                    break;
                case "SymbolTrademark":
                    iAsc = 153;
                    break;
                case "SymbolYen":
                    iAsc = 165;
                    break;
                //GLOG : 6741 : JSW
                case "SymbolCedillaCap":
                    iAsc = 199;
                    break;
                //GLOG : 8112 : JSW
                case "SymbolCedilla":
                    iAsc = 231;
                    break;
                case "SymbolOE":
                    iAsc = 156;
                    break;
                case "SymbolAAcute":
                    iAsc = 225;
                    break;
                case "SymbolIAcute":
                    iAsc = 237;
                    break;
                case "SymbolOAcute":
                    iAsc = 243;
                    break;
                case "SymbolUAcute":
                    iAsc = 250;
                    break;
                case "SymbolOUmlaut":
                    iAsc = 246;
                    break;
                case "SymbolSharpS":
                    iAsc = 223;
                    break;
                case "SymbolAGraveCap":
                    iAsc = 192;
                    break;
                case "SymbolAUmlautCap":
                    iAsc = 196;
                    break;
                //GLOG : 7778 : JSW
                case "SymbolUUmlautCap":
                    iAsc = 220;
                    break;
                case "SymbolUUmlaut":
                    iAsc = 252;
                    break;
                default:
                    try
                    {
                        oWordApp.ScreenUpdating = false;

                        //parse ascii number of character from control ID
                        string xIDs = control.Id.Substring(1);

                        //split multiple IDs
                        string[] aIDs = xIDs.Split('_');

                        foreach (string xID in aIDs)
                        {
                            iAsc = 0;

                            bool ret = int.TryParse(xID, out iAsc);

                            if (iAsc == 0)
                                throw new LMP.Exceptions.ArgumentException(
                                    LMP.Resources.GetLangString("Error_InvalidSymbolCharacter") + control.Id);

                            //insert symbol with specified ID
                            LMP.Forte.MSWord.WordDoc.InsertSymbol(iAsc, xFontName, bUnicode);
                        }

                        oWordApp.ScreenUpdating = true;
                        return;
                    }
                    finally
                    {
                        oWordApp.ScreenUpdating = true;
                    }
            }
            LMP.Forte.MSWord.WordDoc.InsertSymbol(iAsc, xFontName, bUnicode);
        }

        public void MP10Copy(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 8207: Pass to native function if Forte is expired
                if (!ThisAddIn.IsValid || LMP.MacPac.Application.Paused)
                {
                    //set Cancel to false so native copy will be used
                    Cancel = false;
                    return;
                }
                else
                {
                    //10-12-11 (dm) - use native copy when there no bounding objects
                    //in the selection
                    bool bHandle = false;
                    try
                    {
                        Word.Selection oSelection = ThisAddIn.WordApplication.Selection;
                        if (oSelection != null)
                        {
                            //expand to paragraph - Word may include parent
                            //bounding object
                            Word.Range oRange = oSelection.Range;
                            object oUnit = Word.WdUnits.wdParagraph;
                            oRange.Expand(ref oUnit);
                            bHandle = ((oRange.ContentControls.Count > 0) ||
                                (oRange.XMLNodes.Count > 0));
                        }
                    }
                    catch { }
                    if (bHandle)
                    {
                        //call MacPac copy method
                        Word.Document oDocument = ThisAddIn.WordApplication.ActiveDocument;

                        //GLOG item #7419 - dcf
                        if (m_oMPApp == null)
                            m_oMPApp = new LMP.MacPac.Application();

                        m_oMPApp.EditCopy(oDocument);
                        //GLOG 5629: Stop native command from running
                        Cancel = true;
                    }
                    else
                        Cancel = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void MP10Cut(Office.IRibbonControl control, ref bool Cancel)
        {
            try
            {
                //GLOG 8207: Pass to native function if Forte is expired
                if (!ThisAddIn.IsValid || LMP.MacPac.Application.Paused)
                {
                    //set Cancel to false so native cut will be used
                    Cancel = false;
                }
                else
                {
                    //10-12-11 (dm) - use native cut when there no bounding objects
                    //in the selection
                    bool bHandle = false;
                    try
                    {
                        Word.Selection oSelection = ThisAddIn.WordApplication.Selection;
                        if (oSelection != null)
                        {
                            //expand to paragraph - Word may include parent
                            //bounding object
                            Word.Range oRange = oSelection.Range;
                            object oUnit = Word.WdUnits.wdParagraph;
                            oRange.Expand(ref oUnit);
                            bHandle = ((oRange.ContentControls.Count > 0) ||
                                (oRange.XMLNodes.Count > 0));
                        }
                    }
                    catch { }
                    if (bHandle)
                    {
                        //call MacPac cut method
                        Word.Document oDocument = ThisAddIn.WordApplication.ActiveDocument;

                        //GLOG item #7419 - dcf
                        if (m_oMPApp == null)
                            m_oMPApp = new LMP.MacPac.Application();

                        m_oMPApp.EditCut(oDocument);

                        //GLOG 5629: Stop native command from running
                        Cancel = true;
                    }
                    else
                        Cancel = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6112 : CEH
        public void RemoveTestDocuments(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("RemoveTestDocuments");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG 8213
        public void ShowOrphanedDocVarsInDesign(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ShowOrphanedDocVarsInDesign");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void ListVBAExpressions(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("ListVBAExpressions");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6112 : CEH
        public void InitializeForte(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("InitializeForte");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6112 : CEH
        public void CreateGrailDataDump(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("CreateGrailDataDump");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6112 : CEH
        public void EditCIIni(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditCIIni");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6112 : CEH
        public void EditNumberingIni(Office.IRibbonControl oCtl)
        {
            try
            {
                ExecuteCommand("EditNumberingIni");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void OnBackstageShow(object oContextObject)
        {
            //GLOG 7420: Disable Taskpane events while Backstage is displayed
            LMP.MacPac.Application.DisableTaskpaneEvents = true;
        }
        public void OnBackstageHide(object oContextObject)
        {
            //GLOG 7420: Disable Taskpane events while Backstage is displayed
            LMP.MacPac.Application.DisableTaskpaneEvents = false;
            //GLOG 7858: Invalidate ribbon to ensure control enabled states
            //are correct after document is created from File | New
            this.Ribbon.Invalidate();
        }
        #endregion

        #region Helpers
        public stdole.IPictureDisp GetDynamicImage(Office.IRibbonControl control)
        {
            switch (GetIDRoot(control))
            {
                case "Pause":
                    if (LMP.MacPac.Application.Paused)
                        return PictureConverter.ImageToPictureDisp(
                            Properties.Resources.Resume);
                    else
                        return PictureConverter.ImageToPictureDisp(
                            Properties.Resources.Pause);
                case "RemoveTags":
                    if (!Session.CurrentUser.FirmSettings.AllowPauseResume)
                    {
                        return PictureConverter.ImageToPictureDisp(
                            Properties.Resources.WordTag);
                    }
                    else
                    {
                        return PictureConverter.ImageToPictureDisp(
                            Properties.Resources.RemoveTags);
                    }
                case "ManagePeople":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.People);
                case "ManageUserAppSettings":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.AppPref);
                case "EditAuthorPreferences":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.AuthPref);
                case "ShowTaskPane":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.ShowTP);
                case "RefreshTaskPane":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.RefreshTP);
                case "SaveData":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.SaveData);
                case "RefreshSegmentNode":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.RefreshTP);
                case "HelpMenu":
                case "SegmentHelp":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.Help);
                case "AddCounsel":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.AddCounsel);
                case "AddCaption":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.AddCaption);
                case "PleadingFunctionsMenu":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.Pleading);
                case "AddSignature":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.AddSignature);
                case "InsertExhibits":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.InsertExhibits);
                case "InsertTOA":
                case "UpdateTOA":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.TOA);
                case "ApplyQuoteStyle":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.QuoteStyle);
                case "InsertAgreementTitlePage":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.InsertExhibits);
                case "ChangePleadingPaperType":
                    return PictureConverter.ImageToPictureDisp(
                        Properties.Resources.PleadingPaper);
                case "BackstageCreateMacPacDocument":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.ForteLogo32);
                case "FastCommandCreateMacPacDocument":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.ForteLogo16);
                case "OfficeMenuCreateMacPacDocument": //GLOG 7930: New Forte Document on 2007 Office Menu
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.ForteLogo32);
                default:
					//GLOG : 6303 : jsw
					//add intended use icon to favorites 
                    switch (control.Tag.ToString())
                    {
                        case "AsAnswerFile":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.MultipleSegments);
                        case "AsDocument":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.DocumentSegment);
                        case "AsDocumentComponent":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.ComponentSegment);
                        case "AsMasterDataForm":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.MultipleSegments);
                        case "AsParagraphText":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.TextBlock);
                        case "AsSentenceText":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.SentenceText);
                        case "AsStyleSheet":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.StyleSheet);
                        case "VariableSet":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.SavedData);
                        case "UserSegmentAsDocument":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.UserDocument);
                        case "UserSegmentAsStyleSheet":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.UserStyleSheet);
                        case "UserSegmentAsParagraphText":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.UserTextBlock);
                        case "UserSegmentAsDocumentComponent":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.UserComponentSegment);
                        //GLOG : 6709 : jsw
                        case "UserSegmentAsAnswerFile":
                            return PictureConverter.IconToPictureDisp(
                                Properties.Resources.MultipleSegments);
                        case "UserSegmentAsSentenceText":
                            return PictureConverter.ImageToPictureDisp(
                                Properties.Resources.UserSentence);
                        //GLOG : 8363 : JSW
                        case "XLDocument":
                            return PictureConverter.ImageToPictureDisp(
                                Properties.Resources.XLDocument);
                        case "WordDocument":
                            return PictureConverter.ImageToPictureDisp(
                               Properties.Resources.WordDocument);
                        case "ExternalContent":
                            return PictureConverter.ImageToPictureDisp(
                                Properties.Resources.ExternalContent);

                        default:
                            return null;
                    }
            }
        }
        public stdole.IPictureDisp GetImage(string imageName)
        {
            switch (imageName)
            {
                case "Yen":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Yen);

                case "Euro":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Euro);

                case "PoundSterling":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.PoundSterling);

                case "BulletClosed":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.BulletClosed);

                case "BulletOpen":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.BulletOpen);

                case "Paragraph":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.ParagraphMark);

                case "Cent":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Cent);

                case "CheckboxChecked":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.CheckboxChecked);

                case "CheckboxUnchecked":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Checkbox);

                case "Defendant":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Defendant);

                case "Degree":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Degree);

                //GLOG : 6477 : JSW
                case "Division":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Division);
                
                case "OneHalf":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.OneHalf);

                case "OneQuarter":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.OneQuarter);

                case "Plaintiff":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Plaintiff);

                //GLOG : 6477 : JSW
                case "PlusMinus":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.PlusMinus);

                case "SectionMark":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.SectionMark);

                case "ServiceMark":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.ServiceMark);

                case "Trademark":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Trademark);

                case "EnDash":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.EnDash);

                case "EmDash":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.EmDash);

                case "Copyright":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.Copyright);

                case "Registration":
                    return PictureConverter.IconToPictureDisp(Properties.Resources.RegistrationMark);

                case "ApplicationPreferences":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.AppPref);

                case "AttachTo":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.AttachTo);

                case "AuthorPreferences":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.AuthPref);

                case "Convert":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.Convert);

                case "NewDoc":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.NewDoc);

                case "Paste":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.Paste);

                case "People":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.People);

                case "Prefill":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.CreatePrefill);

                case "RefreshTaskPane":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.RefreshTP);

                case "SelectTaskPane":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.SelectTP);

                case "ShowTaskPane":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.ShowTP);

                case "ToggleXML":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.WordTag);

                case "Office":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.Office);

                case "Sync":
                    return PictureConverter.ImageToPictureDisp(Properties.Resources.Sync);

                default:
                    //get image from file
                    string xPicFile = imageName;
                    System.Drawing.Image oImage = null;

                    try
                    {
                        //test if an image file was specified
                        FileInfo oFI = new FileInfo(imageName);
                        if (oFI.Exists)
                            //attempt to get image from specified file
                            oImage = System.Drawing.Image.FromFile(imageName);
                        else
                        {
                            //attempt to get image from file in bin
                            xPicFile = LMP.Data.Application.AppDirectory + "\\" + imageName;
                            oFI = new FileInfo(xPicFile);
                            
                            if(oFI.Exists)
                                oImage = System.Drawing.Image.FromFile(xPicFile);
                        }
                        
                    }
                    catch
                    {
                        //either not a file or not an image file
                    }

                    if (oImage != null)
                        return PictureConverter.ImageToPictureDisp(oImage);
                    else
                        return null;
            }
        }
        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Replace built-in Word commands based on Registry settings
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        private static string SubstituteCommands(string xXML)
        {
            string xInsert = "";
            const string xTemplate = "<command idMso=\"{0}\" onAction=\"{1}\" />\r";
            int iPos = xXML.IndexOf("</commands>");
            if (iPos == -1)
                return xXML;
            string xRegKey = Registry.MacPac10RegistryRoot + "\\DMSOverrides";
            //Add replacement commands to XML as appropriate
            if (Registry.GetLocalMachineValue(xRegKey, "FileSave") != "1")
                xInsert += string.Format(xTemplate, "FileSave", "MP10FileSave");

            if (Registry.GetLocalMachineValue(xRegKey, "FileSaveAs") != "1")
                xInsert += string.Format(xTemplate, "FileSaveAs", "MP10FileSaveAs");

            if (Registry.GetLocalMachineValue(xRegKey, "FileSaveAll") != "1")
                xInsert += string.Format(xTemplate, "SaveAll", "MP10FileSaveAll");

            if (Registry.GetLocalMachineValue(xRegKey, "FileClose") != "1")
            {
                xInsert += string.Format(xTemplate, "FileClose", "MP10FileClose");
            }
            if (Registry.GetLocalMachineValue(xRegKey, "FileCloseAll") != "1")
                xInsert += string.Format(xTemplate, "FileCloseAll", "MP10FileCloseAll");

            if (Registry.GetLocalMachineValue(xRegKey, "FileExit") != "1")
                xInsert += string.Format(xTemplate, "FileExit", "MP10FileExit");

            if (Registry.GetLocalMachineValue(xRegKey, "FilePrint") != "1")
                xInsert += string.Format(xTemplate, "FilePrint", "MP10FilePrint");

            if (Registry.GetLocalMachineValue(xRegKey, "FilePrintDefault") != "1")
                xInsert += string.Format(xTemplate, "FilePrintQuick", "MP10FilePrintDefault");

            if (xInsert != "")
                return xXML.Insert(iPos, xInsert);
            else
                return xXML;
        }

        //GLOG : 6112 : CEH
        /// <summary>
        /// Inserts Administration Tab XML
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        private static string GetAdminXML(string xXML)
        {
            string xInsert = "";
            int iPos = xXML.IndexOf("</tabs>");
            if (iPos == -1)
                return xXML;

            //start of Admin tab
            xInsert = "<tab idQ=\"x:mpTab3\" ";

            //GLOG : 7129 : 8197 : jsw  
            if (LMP.Forte.MSWord.WordApp.Version == 15)
                xInsert += "label=\"MACPAC UTILITIES\" >\r";
            else
                //GLOG item #7751 - dcf
                xInsert += "label=\"MacPac Utilities\" >\r";

            //main group
            xInsert += "<group id=\"Main\" " +
                              "label=\"General\">\r";
            //GLOG : 8318 : jsw
            //add getEnabled attribute to all Utility tab buttons
            xInsert += "<button id=\"InitializeForte\" " +
                               "label=\"Initialize Forte\" " +
                               "size=\"large\" " +
                               "screentip=\"Initialize Forte\" " +
                               "onAction=\"InitializeForte\" " +
                               "getEnabled=\"GetEnabled\" " +
                               "imageMso=\"RecurrenceEdit\" " +
                               "keytip=\"I\"" +
                               "/>\r";
            if (LMP.Registry.GetMacPac10Value("ShowGrailReportRibbonButton") == "1")
            {
                xInsert += "<button id=\"CreateGrailDataDump\" " +
                                   "label=\"Create Grail Data Report\" " +
                                   "size=\"large\" " +
                                   "screentip=\"Creates a report that details all information retrieved by MacPac for a particular client matter or case number.\" " +
                                   "onAction=\"CreateGrailDataDump\" " +
                                   "getEnabled=\"GetEnabled\" " +
                                   "imageMso=\"RecurrenceEdit\" " +
                                   "keytip=\"I\"/>\r";
            }
            xInsert += "<button id=\"CloseTestDocuments\" " +
                                "label=\"Close Test Documents\" " +
                                "size=\"large\" " +
                                "screentip=\"Close Test Documents\" " +
                                "onAction=\"RemoveTestDocuments\" " +
                                "getEnabled=\"GetEnabled\" " +
                                "imageMso=\"FileClose\" " +
                                "keytip=\"C\"/>\r";
            //GLOG 8213
            if (LMP.Registry.GetMacPac10Value("ShowOrphanVariables") == "1")
            {

                xInsert += "<button id=\"ShowOrphanedDocVarsInDesign\" " +
                                    "label=\"Orphaned Doc Vars\" " +
                                    "size=\"large\" " +
                                    "screentip=\"Show Segments with orphaned document variables\" " +
                                    "onAction=\"ShowOrphanedDocVarsInDesign\" " +
                                    "getEnabled=\"GetEnabled\" " +
                                    "imageMso=\"AdpDiagramTableModesMenu\" " +
                                    "keytip=\"V\"/>\r";
            }
            //GLOG 15734
            if (LMP.Registry.GetMacPac10Value("ShowListVBA") == "1")
            {
                xInsert += "<button id=\"ListVBAExpressions\" " +
                                    "label=\"List VBA Expressions\" " +
                                    "size=\"large\" " +
                                    "screentip=\"List VBA Expressions\" " +
                                    "onAction=\"ListVBAExpressions\" " +
                                    "getEnabled=\"GetEnabled\" " +
                                    "imageMso=\"ToggleXmlMappingPane\" " +
                                    "keytip=\"C\"/>\r";
            }
            xInsert += "</group>\r";

            //edit group
            xInsert += "<group id=\"Edit\" " +
                              "label=\"Edit INI Files\">\r";
            xInsert += "<button id=\"CI\" " +
                                    "label=\"Contact Integration\" " +
                                    "size=\"large\" " +
                                    "screentip=\"Edit the Contact Integration Initialization File\" " +
                                    "onAction=\"EditCIIni\" " +
                                    "getEnabled=\"GetEnabled\" " +
                                    "imageMso=\"AddressBook\" " +
                                    "keytip=\"O\"/>\r";
            xInsert += "<button id=\"Numbering\" " +
                                    "label=\"Numbering\" " +
                                    "size=\"large\" " +
                                    "screentip=\"Edit the Numbering Initialization File\" " +
                                    "onAction=\"EditNumberingIni\" " +
                                    "getEnabled=\"GetEnabled\" " +
                                    "imageMso=\"Bullets\" " +
                                    "keytip=\"N\"/>\r";
            xInsert += "</group>\r";

            //GLOG 7898 (dm) - add task pane group for Forte Tools
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                xInsert += "<group id=\"TaskPane\" label=\"Task Pane\" ><button id=\"ShowTaskPane\" label=\"Show/Select\" size=\"large\" screentip=\"Show or " +
                    "select the Forte Tools task pane\" getImage=\"GetDynamicImage\" onAction=\"ShowTaskPane\"  getEnabled =\"GetEnabled\" keytip=\"W\" />" +
                    "<button id=\"RefreshTaskPane\" label=\"Refresh\" size=\"large\" screentip=\"Update the Forte Tools task pane with the latest data\" " +
                    "getImage=\"GetDynamicImage\" onAction=\"RefreshTaskPane\"  getEnabled =\"GetEnabled\" keytip=\"R\" /></group> \r";
            }

            //add application group - help & administrator
            xInsert += "<group id=\"Application\" label=\"Application\" ><button id=\"ShowAdministrator_2\" label=\"Show Administrator\" size=\"large\" " + 
                "screentip=\"Display the MacPac administrator\" imageMso=\"DatabaseSqlServer\" onAction=\"ShowAdministrator\" getEnabled=\"GetEnabled\" " + 
                "getVisible =\"GetVisible\" keytip=\"AA\" /><dynamicMenu id=\"HelpMenu__3\" label=\"Help\"  size =\"large\" getImage=\"GetDynamicImage\" screentip=\"View " +
                LMP.ComponentProperties.ProductName + " help\" getContent=\"GetHelpDocumentsMenu\" getEnabled=\"GetEnabled\" getVisible =\"GetVisible\" keytip=\"H\" /> </group> \r";

            //end of Admin tab
            xInsert += "</tab>\r";

            xInsert = xInsert.Replace("MACPAC", LMP.ComponentProperties.ProductName.ToUpper());
            xInsert = xInsert.Replace("MacPac", LMP.ComponentProperties.ProductName);

            return xXML.Insert(iPos, xInsert);
        }

        //GLOG : 7985 : JSW
        /// <summary>
        /// Inserts Upper Case Label for Forte tab for Word 15
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        private static string ConvertForteLabelToUpper(string xXML)
        {                    
            //if word version is 15, find Forte label
            //GLOG : 8195 : jsw
            if (LMP.Forte.MSWord.WordApp.Version == 15)
            {
                int iStartPos = xXML.IndexOf("x:MacPac");
                //GLOG : 8195 : jsw
                if (iStartPos > -1)
                {
                    iStartPos = xXML.IndexOf("label=\"", iStartPos);
                    iStartPos = iStartPos + "label=\"".Length;
                    int iEndPos = xXML.IndexOf("\"\r\n", iStartPos);
                    string xLabel = xXML.Substring(iStartPos, (iEndPos - iStartPos));
                    //convert to uppercase
                    xLabel = xLabel.ToUpper();
                    //and plug back in
                    xXML = xXML.Substring(0, iStartPos) + xLabel + xXML.Substring(iEndPos);
                }
            }

            return xXML;
        }

        //GLOG 8896 (dm)
        /// <summary>
        /// translates TOC ribbon items to language specified in Numbering ini
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        private static string TranslateTOCUIIfNecessary(string xXML)
        {
            //GLOG 15919 (dm) - don't assume that TOC is in xml - not all Forte clients use Numbering
            int iStartPos = xXML.IndexOf("<splitButton idQ=\"x:spl3\">");
            if (iStartPos < 0)
                return xXML;

            string xNumberingIni = LMP.Data.Application.GetDirectory(mpDirectories.NumberingData) + @"\tsgNumbering.INI";
            if (File.Exists(xNumberingIni))
            {
                string xLanguage = "";
                StringBuilder xValue = new StringBuilder(512);
                int iRet = 0;
                iRet = GetPrivateProfileString("General", "UILanguage", "", xValue, 512, xNumberingIni);
                if (iRet > 0)
                    xLanguage = xValue.ToString();
                if (xLanguage == "3084") //French Canadian
                {
                    //modify labels
                    int iPos = xXML.IndexOf(" label=\"", iStartPos) + 8;
                    int iPos2 = xXML.IndexOf("\"", iPos);
                    xXML = xXML.Substring(0, iPos) + "Table des Mati�res" + xXML.Substring(iPos2);
                    iPos = xXML.IndexOf(" label=\"", iPos) + 8;
                    iPos2 = xXML.IndexOf("\"", iPos);
                    xXML = xXML.Substring(0, iPos) + "Ins�rer ou modifier &amp;Table des Mati�res" + xXML.Substring(iPos2);

                    //modify screentips
                    iPos = xXML.IndexOf(" screentip=\"", iStartPos) + 12;
                    iPos2 = xXML.IndexOf("\"", iPos);
                    xXML = xXML.Substring(0, iPos) + "Ins�rer ou modifier Table des Mati�res (Alt+Maj+N,T)" +
                        xXML.Substring(iPos2);
                    iPos = xXML.IndexOf(" screentip=\"", iPos) + 12;
                    iPos2 = xXML.IndexOf("\"", iPos);
                    xXML = xXML.Substring(0, iPos) + "Ins�rer ou modifier Table des Mati�res (Alt+Maj+N,T)" +
                        xXML.Substring(iPos2);
                }
            }

            return xXML;
        }

        /// <summary>
        /// this class provides methods that .NET 
        /// graphics to COM graphics, and visa versa
        /// </summary>

        //GLOG : 8418 : JSW
        /// <summary>
        /// creates menu and buttons representing folder structure of Help directory
        /// </summary>
        /// <param name="oDir"></param>
        /// <param name="iSubDir"></param>
        /// <returns>string</returns>
        private static string GetHelpDirectories(DirectoryInfo oDir, int iSubDir)
        {

            string xDirPath = oDir.FullName;
            string xDirName = oDir.Name;

            string[] xFiles = Directory.GetFiles(xDirPath, "*.*?", SearchOption.TopDirectoryOnly);
            Array.Sort(xFiles);
            string xMenu = "";
            string xName = "";
            int iPos = 0;   //GLOG 7891
            int i = 0;
            string xHelpDocumentsDirectory = LMP.Data.Application.GetDirectory(mpDirectories.RootDirectory) +
                    @"\Tools\User Documentation\pdf\";
            int iHelpDocsDirNameLength = xHelpDocumentsDirectory.Length;

            foreach (DirectoryInfo oSubDir in oDir.GetDirectories())
            {
                //make sure ids need unique names, use folder name plus an incrementer, 
                //in case subfolders have duplicate names
                //remove illegal chars from id.  
                xMenu += "<menu id=\"HelpMenu" + String.RemoveIllegalIDChars(oSubDir.Name) + iSubDir.ToString() + "\" label=\""
                    + oSubDir.Name + "\" >"; 
                iSubDir++;
                xMenu += GetHelpDirectories(oSubDir, iSubDir);
                xMenu += "</menu>";
            }

            foreach (string xPath in xFiles)
            {
                if (xPath.EndsWith(".css"))
                    continue;

                xName = Path.GetFileNameWithoutExtension(xPath);
                string xExt = Path.GetExtension(xPath).ToLower();

                //GLOG : 7897 : ceh
                //When in Forte Tools, display only the files with "Forte Tools" after the underscore.
                //When in Forte, display everything but the files with "Forte Tools" after the underscore.
                //(This is necessary because we're still using the "MacPac 10" versions pending the completion of the "Forte" set.)
                iPos = xName.IndexOf("_Forte Tools");

                //GLOG : 8482 : ceh - need to look at the Registry because per GLOG 7898,
                //IsToolKit will return False when logged in as admin

                //if (LMP.MacPac.MacPacImplementation.IsToolkit)
                if (LMP.Registry.GetMacPac10Value("RibbonOnly") == "1")
                {
                    if (iPos < 0)
                        continue;
                }
                else if (iPos > -1)
                    continue;

                //GLOG 7891 : strip application name
                //iPos = xName.IndexOf("_");
                //if (iPos > -1)
                //    xName = xName.Substring(0, iPos);

                //GLOG : 8349 : ceh - find last occurrence
                //don't truncate filename if the only underscore is the first character
                iPos = xName.LastIndexOf("_");
                if (iPos > 0)
                    xName = xName.Substring(0, iPos);

                //Ignore temp files in directory
                //GLOG 4445 (dm) - get only html files -
                //GLOG item #3818 - expanded to doc/pdf files as well
                if ((!xName.StartsWith("~")) && (xExt == ".html" || xExt == ".doc" || xExt == ".docx" || xExt == ".pdf"))
                {
                    i++;
                    //need unique value for the id, use folder name, subdir increment plus a file increment
                    //to ensure there are no duplicates even if sub folders have duplicate names
                    xMenu += "<button id=\"Help" + String.RemoveIllegalIDChars(oDir.Name) + iSubDir.ToString() + "_" + i.ToString() + "\" label=\"" 
                        + xName + "\" onAction=\"OpenHelpFile" + "\" tag=\""
                        //need subfolder info in addition to file name
                        //+ Path.GetFileName(xPath) + "\" />";
                        + xPath.Substring(iHelpDocsDirNameLength) + "\" />"; 
                }
            }

            return xMenu;
        }
        internal class PictureConverter : AxHost
        {
            private PictureConverter() : base(System.String.Empty) { }

            static public stdole.IPictureDisp ImageToPictureDisp(Image image)
            {
                return (stdole.IPictureDisp)GetIPictureDispFromPicture(image);
            }

            static public stdole.IPictureDisp IconToPictureDisp(Icon icon)
            {
                return ImageToPictureDisp(icon.ToBitmap());
            }

            static public Image PictureDispToImage(stdole.IPictureDisp picture)
            {
                return GetPictureFromIPicture(picture);
            }
        }
        #endregion
    }
}
