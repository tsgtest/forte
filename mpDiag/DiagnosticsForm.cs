using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace LMP.MacPac10.Diagnostics
{
    public partial class DiagnosticsForm : Form
    {
        private string m_xReport;
        public DiagnosticsForm()
        {
            InitializeComponent();
        }

        private void tsAnalyzeSetup_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList oMsgs;
                this.lstAnalyzerResults.Items.Clear();
                m_xReport = "";
                if (Analyzer.SetUpIsValid(out oMsgs))
                {
                    this.lstAnalyzerResults.Items.Add(LMP.ComponentProperties.ProductName + " is set up correctly.");
                }
                else
                {
                    foreach (string xMsg in oMsgs)
                    {
                        this.lstAnalyzerResults.Items.Add(xMsg);
                        m_xReport = m_xReport + xMsg + "\r\n";
                    }
                    this.tsWriteToLog.Visible = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }
        }

        private void tsWriteToLog_Click(object sender, EventArgs e)
        {
            //launch program associated with .txt files and paste error automatically

            //Get the temp directory.
            string xFile = System.IO.Path.GetTempPath() + "Diagnostics Report.txt";

            //create new text file in user documents location
            StreamWriter oSW = new StreamWriter(xFile);
            oSW.Write(m_xReport);
            oSW.Close();

            //launch new text file
            Process oProcess = new Process();
            oProcess.StartInfo.FileName = xFile;
            oProcess.Start();

            oProcess.WaitForInputIdle();
        }
    }
}