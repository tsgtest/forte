namespace LMP.MacPac10.Diagnostics
{
    partial class DiagnosticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiagnosticsForm));
            this.lstAnalyzerResults = new System.Windows.Forms.ListBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsAnalyzeSetup = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsWriteToLog = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstAnalyzerResults
            // 
            this.lstAnalyzerResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstAnalyzerResults.FormattingEnabled = true;
            this.lstAnalyzerResults.IntegralHeight = false;
            this.lstAnalyzerResults.Location = new System.Drawing.Point(0, 25);
            this.lstAnalyzerResults.Name = "lstAnalyzerResults";
            this.lstAnalyzerResults.Size = new System.Drawing.Size(617, 388);
            this.lstAnalyzerResults.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsAnalyzeSetup,
            this.tsWriteToLog});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(617, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsAnalyzeSetup
            // 
            this.tsAnalyzeSetup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsAnalyzeSetup.Image = ((System.Drawing.Image)(resources.GetObject("tsAnalyzeSetup.Image")));
            this.tsAnalyzeSetup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsAnalyzeSetup.Name = "tsAnalyzeSetup";
            this.tsAnalyzeSetup.Size = new System.Drawing.Size(85, 22);
            this.tsAnalyzeSetup.Text = "Analyze &Setup";
            this.tsAnalyzeSetup.Click += new System.EventHandler(this.tsAnalyzeSetup_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 413);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(617, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsWriteToLog
            // 
            this.tsWriteToLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsWriteToLog.Image = ((System.Drawing.Image)(resources.GetObject("tsWriteToLog.Image")));
            this.tsWriteToLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsWriteToLog.Name = "tsWriteToLog";
            this.tsWriteToLog.Size = new System.Drawing.Size(76, 22);
            this.tsWriteToLog.Text = "&Write to Log";
            this.tsWriteToLog.Visible = false;
            this.tsWriteToLog.Click += new System.EventHandler(this.tsWriteToLog_Click);
            // 
            // DiagnosticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 435);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lstAnalyzerResults);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DiagnosticsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diagnose " + LMP.ComponentProperties.ProductName + " Issues";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstAnalyzerResults;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsAnalyzeSetup;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton tsWriteToLog;
    }
}

