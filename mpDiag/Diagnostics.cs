using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Security.Policy;
using System.Runtime.InteropServices;

namespace LMP.MacPac10.Diagnostics
{
    static class Diagnostics
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DiagnosticsForm());
        }
    }

    public class Analyzer
    {
        /// <summary>
        /// returns true if the current setup for Forte is valid
        /// </summary>
        /// <returns></returns>
        public static bool SetUpIsValid(out ArrayList aMessages)
        {
            string xVal;
            bool bValid = true;
            aMessages = new ArrayList();
            //GLOG : 6404 : jsw
            string xHKLM = "";

            //check for existing Forte reg keys
            //get correct reg string according to operating system
            //GLOG : 6404 : jsw
            if (Is64Bit())
                xHKLM = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node";
            else
                xHKLM = "HKEY_LOCAL_MACHINE\\SOFTWARE";

            string xRootDirectory = Registry.GetMacPac10Value("RootDirectory");
            if (string.IsNullOrEmpty(xRootDirectory))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_MissingRegValue") + xHKLM + "\\The Sackett Group\\Deca\\RootDirectory");
                bValid = false;
            }
            else
            {
                xRootDirectory = LMP.OS.EvaluateEnvironmentVariables(xRootDirectory);
                xRootDirectory = xRootDirectory.TrimEnd('\\');
                
                xVal = Registry.GetLocalMachineValue(@"SOFTWARE\The Sackett Group\Deca",
                    "DataDirectory");
                xVal = xVal.TrimEnd('\\');

                //if (string.IsNullOrEmpty(xVal)
                if (!File.Exists(xVal + @"\" + LMP.Data.Application.UserDBName))
                {
                    if (!File.Exists(xRootDirectory + @"\Data\" + LMP.Data.Application.UserDBName))
                    {
                        //GLOG : 6735 : jsw
                        xVal = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName;
                        if (!File.Exists(xVal))
                        {
                            string xMsg = LMP.Resources.GetLangString("Error_MissingUserDB").Replace("Forte.mdb",
                                LMP.Data.Application.UserDBName);
                            aMessages.Add(xMsg);
                            bValid = false;
                        }
                    }
                }
                //make sure file is in correct dir
                if (!File.Exists(xRootDirectory + @"\Data\Forte.xsd"))
                {
                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingMacPac10Schema"));
                    bValid = false;
                }
            }

            xVal = "DisableEncryption";
            if (string.IsNullOrEmpty(Registry.GetMacPac10Value(xVal)))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_MissingRegValue") + xHKLM + "\\The Sackett Group\\Deca\\" + xVal);
                bValid = false;
            }

            xVal = "BenchmarkMode";
            if (string.IsNullOrEmpty(Registry.GetMacPac10Value(xVal)))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_MissingRegValue") + xHKLM + "\\The Sackett Group\\Deca\\" + xVal);
                bValid = false;
                return bValid;
            }

            xVal = Registry.GetLocalMachineValue(@"SOFTWARE\The Sackett Group\Document Analyzer",
                "DataDirectory");
            if (string.IsNullOrEmpty(xVal))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_MissingRegValue") +
                    xHKLM + "\\The Sackett Group\\Document Analyzer\\DataDirectory");
                bValid = false;
            }
            else
            {
                xVal = LMP.OS.EvaluateEnvironmentVariables(xVal);
                xVal = xVal.TrimEnd('\\');

                //make sure file is in correct dir
                if (!File.Exists(xVal + @"\daInstructionSets.mdb"))
                {
                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingDocumentAnalyzerDB"));
                    bValid = false;
                }
            }

            //check that VSTO Addin registry keys are installed
            xVal = Registry.GetLocalMachineValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn", "Manifest");
            if (string.IsNullOrEmpty(xVal))
            {
                xVal = Registry.GetCurrentUserValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn", "Manifest");
            }
            if (string.IsNullOrEmpty(xVal))
            {
                aMessages.Add("VSTO AddIn registry keys missing");
                bValid = false;
            }

            //check that VSTO Addin registry keys are installed
            xVal = Registry.GetLocalMachineValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn", "LoadBehavior");
            if (string.IsNullOrEmpty(xVal))
            {
                xVal = Registry.GetCurrentUserValue(@"Software\Microsoft\Office\Word\Addins\ForteAddIn", "LoadBehavior");
            }
            if (xVal != "3")
            {
                aMessages.Add(@"Software\Microsoft\Office\Word\Addins\ForteAddIn\LoadBehavior=" + xVal);
                bValid = false;
            }

            //check that MacPac 10 Doc Schema is installed
            string xSchemaLibKey1 = Registry.GetCurrentUserValue(@"Software\Microsoft\Schema Library\urn-legalmacpac-data/10\0", "Key");
            string xSchemaLibKey2 = Registry.GetCurrentUserValue(@"Software\Microsoft\Schema Library\urn-legalmacpac-data/10\0", "Location");
            string xSchemaLibKey3 = Registry.GetCurrentUserValue(@"Software\Microsoft\Schema Library\urn-legalmacpac-data/10\0\Alias", "1033");
            
            if (string.IsNullOrEmpty(xSchemaLibKey1) || 
                string.IsNullOrEmpty(xSchemaLibKey2) || 
                string.IsNullOrEmpty(xSchemaLibKey3) || (!File.Exists(xSchemaLibKey2)))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_SchemaNotInstalled"));
                bValid = false;
            }

            //check for startup folder registry entry
            xVal = Registry.GetCurrentUserValue(@"Software\Microsoft\Office\14.0\Word\Options", "STARTUP-PATH");
            if (string.IsNullOrEmpty(xVal))
            {
                xVal = Registry.GetCurrentUserValue(@"Software\Microsoft\Office\12.0\Word\Options", "STARTUP-PATH");
                if (string.IsNullOrEmpty(xVal))
                {

                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingRegValue") + xVal);
                    bValid = false;
                }
            }

            //check if Forte.dotm is there
            xVal = xVal.TrimEnd('\\');
            if (!File.Exists(xVal + @"\Forte.dotm"))
            {
                aMessages.Add(LMP.Resources.GetLangString("Error_MissingStartupFolder"));
                bValid = false;
            }

            //check bin files
            string xBinDirectory = xRootDirectory + "\\Bin\\";
            string[] aBinFiles = new string[] { "Forte.dll","ForteAddIn.dll", "ForteAddIn.vsto", 
                "fAdmin.dll", "fArchitect.dll", "fBase.dll", "LMP.Forte.MSWord.dll", "fWord.dll", "fControls.dll", 
                "fDMControls.dll", "fDMS.dll", "SyncClient.dll", "fConvert.dll", "daWord.dll", "daLib.dll", 
                "DocumentFormat.OpenXml.dll", "SyncClient.XmlSerializers.dll", "Interop.ADODB.dll", 
                "Interop.ADOX.dll", "Interop.CIO.dll", "Interop.CIX.dll", "Interop.daCOM.dll", 
                "Interop.IasHelperLib.dll", "Interop.IManage.dll", "Interop.IMANEXT.dll", "Interop.JRO.dll" };

            foreach (string xBinFile in aBinFiles)
            {
                if (!File.Exists(xBinDirectory + xBinFile))
                {
                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingBinFile") + xBinFile);
                    bValid = false;
                }
            }
            
            //check System files
            string xSys32Directory = Environment.SystemDirectory.ToString() + "\\";
            string[] aSysFiles = new string[] {"COMCT232.ocx","COMCTL32.ocx", "zlibwapi.dll", 
                "Dibsectn.dll", "MFC40.dll", "Mscomct2.ocx", "MSDART.dll", "msvcrt.dll", "MSVCRT40.dll", 
                "Riched32.dll", "richtx32.ocx", "scrrun.dll", "tdbg6.ocx", "tdbgpp.dll", "Tdbl5.ocx", 
                "tdbl6.ocx", "VB5DB.dll", "Vsocx6.ocx", "xarray32.ocx", "xarraydb.ocx", "zlib.dll" };

            foreach (string xSysFile in aSysFiles)
            {
                if (!File.Exists(xSys32Directory + xSysFile))
                {
                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingSystemFile") + xSysFile);
                    bValid = false;
                }
            }
            //GLOG : 6404 : JSW
            //check is system files are registered
            foreach (string xSysFile in aSysFiles)
            {
                if (File.Exists(xSys32Directory + xSysFile))
                {
                    int libId = LoadLibrary(xSysFile);

                    if (libId > 0) FreeLibrary(libId);

                    if (libId <= 0)
                    {
                        aMessages.Add(LMP.Resources.GetLangString("Error_UnregisteredSystemFile") + xSysFile);
                        bValid = false;
                    }
                }
            }

            //check if vsto is installed
            xVal = Registry.GetLocalMachineValue(@"SOFTWARE\Microsoft\VSTO Runtime Setup\v9.0.21022", "Install");
            if (string.IsNullOrEmpty(xVal))
            {
                xVal = Registry.GetLocalMachineValue(@"SOFTWARE\Wow6432Node\Microsoft\VSTO Runtime Setup\v9.0.21022", "Install");
                if (string.IsNullOrEmpty(xVal))
                {
                    aMessages.Add(LMP.Resources.GetLangString("Error_MissingVSTO"));
                    bValid = false;
                }
            }
            
            return bValid;
        }
        /// <summary>
        /// Returns true if the code group exists in the "machine" policy level.
        /// </summary>
        /// <param name="name">The name of the code group.</param>
        public static bool CodeGroupExists()
        {
            const string xPublicKey = "00240000048000009400000006020000002400005253413100040using001004FF6F1E4BA68A67B2A1A2B9B747D98DED0B99167330AB29AA4B7922F59D06A44ACA18399852A8542D7CDC2B6E6074F73F24FE01677C0961F26F37E317FCEAAEA0DB5CE1DCABA49B10DA2812214D7016FFF4C86016E6E467F77667048EA6E7BE3D3D92B88F77A31F5ED7FE3484286DC4F11CDD765C92E4BC72AC41810F2EFB3C2";
            IEnumerator policyEnumerator = SecurityManager.PolicyHierarchy();
            while (policyEnumerator.MoveNext())
            {
                PolicyLevel oLevel = (PolicyLevel)policyEnumerator.Current;
                if (oLevel.Label == "Machine")
                {
                    CodeGroup oRoot = oLevel.RootCodeGroup;
                    foreach (CodeGroup oCodeGroup in oRoot.Children)
                    {
                        StrongNameMembershipCondition oCondition = 
                            oCodeGroup.MembershipCondition as StrongNameMembershipCondition;

                        if (oCondition != null)
                        {
                            if (oCondition.PublicKey.ToString() == xPublicKey)
                                return true;
                        }
                    }
                    return false;
                }
            }
            return false;
        }
        public static bool Is64Bit()
        {
           Microsoft.Win32.RegistryKey oKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\\Wow6432Node");

           if (oKey != null)
                return true;
           else
                return false;
        }

        //GLOG : 6404 : JSW
        //check is system files are registered
        [DllImport("kernel32")]
        public extern static int LoadLibrary(string lpLibFileName);

        [DllImport("kernel32")]
        public extern static bool FreeLibrary(int hLibModule);
    }

}