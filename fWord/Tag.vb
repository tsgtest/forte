Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Tag
        Private m_oWordXMLNode As Word.XMLNode
        Private m_xTagID As String
        Private m_xObjectData As String
        Private m_xElementName As String
        Private m_oTags As LMP.Forte.MSWord.Tags
        Private m_xColIndex As String
        Private m_xFullTagID As String
        Private m_xPart As String
        Private m_xParentPart As String
        Private m_xDeletedScopes As String = Nothing
        Private m_xAuthors As String
        Private m_xParentVariable As String
        Private m_xTempID As String
        Private m_oContentControl As Word.ContentControl = Nothing
        Private m_oBookmark As Word.Bookmark
        Private m_xTag As String

        Public Property WordXMLNode As Word.XMLNode
            Get
                Return m_oWordXMLNode
            End Get
            Set(value As Word.XMLNode)
                m_oWordXMLNode = value
            End Set
        End Property

        Public Property TagID As String
            Get
                Return m_xTagID
            End Get
            Set(value As String)
                m_xTagID = value
            End Set
        End Property

        Public Property Authors As String
            Get
                Return m_xAuthors
            End Get
            Set(value As String)
                m_xAuthors = value
            End Set
        End Property

        Public Property ObjectData As String
            Get
                Return m_xObjectData
            End Get
            Friend Set(value As String)
                m_xObjectData = value
            End Set
        End Property

        Public Property DeletedScopes As String
            Get
                If (IsNothing(m_xDeletedScopes)) Then
                    If Me.ElementName = "mSEG" Then
                        m_xDeletedScopes = BaseMethods.GetAttributeValue(Me.Tag, "DeletedScopes")
                    Else
                        m_xDeletedScopes = ""
                    End If
                End If
                Return m_xDeletedScopes
            End Get
            Set(value As String)
                m_xDeletedScopes = value
            End Set
        End Property

        Public Property ElementName As String
            Get
                Return m_xElementName
            End Get
            Friend Set(value As String)
                m_xElementName = value
            End Set
        End Property

        Public Property Tags As Tags
            Get
                Return m_oTags
            End Get
            Friend Set(value As Tags)
                m_oTags = value
            End Set
        End Property

        Friend Property ColIndex As String
            Get
                'this property  is for internal use only -
                'the ColIndex of a node does not necessarily get
                'updated when it's moved along with its parent
                Return m_xColIndex
            End Get
            Set(value As String)
                m_xColIndex = value
            End Set
        End Property

        Public Property FullTagID As String
            Get
                Return m_xFullTagID
            End Get
            Friend Set(value As String)
                m_xFullTagID = value
            End Set
        End Property

        Public Property Part As String
            Get
                Return m_xPart
            End Get
            Friend Set(value As String)
                m_xPart = value
            End Set
        End Property

        Public Property ParentPart As String
            Get
                Return m_xParentPart
            End Get
            Friend Set(value As String)
                m_xParentPart = value
            End Set
        End Property

        Public Property ParentVariable As String
            Get
                Return m_xParentVariable
            End Get
            Friend Set(value As String)
                m_xParentVariable = value
            End Set
        End Property

        Public Sub New()
            'create empty nodes collection
            m_oTags = New Tags
        End Sub
        'Private Sub Class_Initialize()
        '    'create empty nodes collection
        '    m_oTags = New Tags
        'End Sub

        Friend Sub UpdateFullTagIDs()
            'updates FullTagID property  of all subnodes
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tag.UpdateFullTagIDs"
            Dim i As Integer

            On Error GoTo ProcError

            For i = 1 To m_oTags.Count
                With m_oTags.Item(i)
                    .FullTagID = m_xFullTagID & "." & .TagID
                    .UpdateFullTagIDs()
                End With
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Property TempID As String
            Get
                Return m_xTempID
            End Get
            Set(value As String)
                m_xTempID = value
            End Set
        End Property

        Public Property ContentControl As Word.ContentControl
            Get
                If (IsNothing(m_oContentControl)) Then
                    'GLOG 15828 (dm) - passively setting this property is problematic after adding
                    'content controls to mSEGs for Save Segment, since they're only there temporarily -
                    'in design, the property should be explicitly set for mSEGs, but I added
                    'an exception for non-GUID indexing just in case
                    If (Me.ElementName <> "mSEG") Or (InStr(Me.TagID, "{") = 0) Then
                        m_oContentControl = WordDoc.FindContentControl(Nothing, Me.Tag)
                    End If
                End If
                Return m_oContentControl
            End Get
            Friend Set(value As Word.ContentControl)
                m_oContentControl = value
            End Set
        End Property

        Public Property Bookmark As Word.Bookmark
            Get
                If IsNothing(m_oBookmark) Then
                    On Error Resume Next
                    m_oBookmark = GlobalMethods.CurWordApp.ActiveDocument.Bookmarks("_" & Me.Tag)
                    On Error GoTo 0
                End If
                Return m_oBookmark
            End Get
            Friend Set(value As Word.Bookmark)
                m_oBookmark = value
            End Set
        End Property

        Public Property Tag As String
            Get
                Return m_xTag
            End Get
            Friend Set(value As String)
                m_xTag = value
            End Set
        End Property
    End Class
End Namespace