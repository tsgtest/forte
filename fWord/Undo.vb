Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Undo
        Private m_oUndoRecord As UndoRecord

        Public Sub MarkStart(ByVal xName As String)
            'starts recording of the Word custom undo record
            Const mpThisFunction As String = "LMP.Forte.MSWord.Undo.MarkStart"
            On Error GoTo ProcError
            m_oUndoRecord = GlobalMethods.CurWordApp.UndoRecord
            m_oUndoRecord.StartCustomRecord(xName)
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub MarkEnd()
            'ends recording of the Word custom undo record
            Const mpThisFunction As String = "LMP.Forte.MSWord.Undo.MarkEnd"
            On Error GoTo ProcError
            m_oUndoRecord.EndCustomRecord()
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function IsRecording() As Boolean
            'ends recording of the Word custom undo record
            Const mpThisFunction As String = "LMP.Forte.MSWord.Undo.IsRecording"
            On Error GoTo ProcError
            Return m_oUndoRecord.IsRecordingCustomRecord
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
    End Class
End Namespace