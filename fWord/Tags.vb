Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Forte.MSWord

Namespace LMP.Forte.MSWord
    Public Class Tags
        Private m_Col As Collection
        Private m_ColSegBmks As Collection 'GLOG 7363
        Private m_oParent As Tag
        Private m_iColIndex As Short
        Private m_xNewSegment As String = ""
        Private m_xXMLSummary As String = ""
        Private m_oSummaryXML As Xml.XmlDocument
        Private m_xFixedTopLevelTagID As String = ""
        Private m_bGUIDIndexing As Boolean
        Private m_iBoundingObjectType As mpBoundingObjectTypes
        Private m_iInvalidTagsRemoved As mpInvalidTagsRemovedTypes  '12/27/10 JTS

        Public Sub New()
            m_Col = New Collection
            m_ColSegBmks = New Collection 'GLOG 7363
            m_iInvalidTagsRemoved = mpInvalidTagsRemovedTypes.None '12/27/10 JTS
        End Sub
        'Private Sub Class_Initialize()
        '    m_Col = New Collection
        '    m_ColSegBmks = New Collection 'GLOG 7363
        '    m_iInvalidTagsRemoved = mpInvalidTagsRemovedTypes.None '12/27/10 JTS
        'End Sub

        'Private Sub Class_Terminate()
        '    m_Col = Nothing
        'End Sub

        Friend Sub Add(oWordXMLNodes As Word.XMLNodes, Optional ByVal bIsNew As Boolean = False, _
                Optional ByRef bRestartRefresh As Boolean = False, Optional ByVal xExclusions As String = "")
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Add"
            Dim i As Short
            Dim oWordXMLNode As Word.XMLNode
            Dim oTagID As Word.XMLNode
            Dim oObjectData As Word.XMLNode
            Dim oHost As Word.XMLNode
            Dim xPrefixMapping As String = ""
            Dim bAdd As Boolean
            Dim oWordDoc As WordDoc
            Dim iDeleted As mpInvalidTagsRemovedTypes
            Dim oParentTag As LMP.Forte.MSWord.Tag
            Dim bContainedInBmk As Boolean

            On Error GoTo ProcError
            oWordDoc = New WordDoc
            xPrefixMapping = "xmlns:x='" & GlobalMethods.mpNamespace & "'"

            For i = 1 To oWordXMLNodes.Count
                bAdd = False
                oWordXMLNode = oWordXMLNodes(i)
                bContainedInBmk = oWordDoc.TagIsContainedByBookmark(oWordXMLNode)
                '12/27/10 JTS: Remove any Tags that have become corrupted
                'Set value of InvalidTagsDeleted so Front-end can determine
                'what was removed during Refresh
                iDeleted = Me.DeleteInvalidTag(oWordXMLNode)
                If iDeleted > Me.InvalidTagsDeleted Then
                    Me.InvalidTagsDeleted = iDeleted
                End If
                If iDeleted > 0 Then
                    bRestartRefresh = True
                    Exit Sub
                End If
                If Not bContainedInBmk Then
                    oParentTag = Me.m_oParent

                    If oParentTag Is Nothing Then
                        'only add top-level nodes
                        oHost = oWordXMLNode.SelectSingleNode("ancestor::x:mSEG[1]", _
                            xPrefixMapping)
                        If oHost Is Nothing Then
                            'no parent mSEG
                            bAdd = True
                        ElseIf Not oWordDoc.TagIsAvailableToCurrentClient(oHost) Then
                            'parent mSEG doesn't belong to this client
                            bAdd = True
                        End If
                    Else
                        bAdd = True
                    End If
                Else
                    bAdd = False
                End If

                If bAdd Then
                    AddNode(oWordXMLNode, bIsNew, bRestartRefresh, xExclusions)

                    'GLOG 3641 (dm) - added bRestartRefresh parameter in case in process
                    'adjustments necessitate restarting the refresh process from the beginning
                    If bRestartRefresh Then _
                        Exit Sub
                End If
            Next i
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub AddNode(oWordXMLNode As Word.XMLNode, ByVal bIsNew As Boolean, _
                ByRef bRestartRefresh As Boolean, Optional ByVal xExclusions As String = "",
                Optional bIncludeChildren As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddNode"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xDestination As String = ""
            Dim xText As String = ""
            Dim oPartnerNode As xml.xmlNode
            Dim oParentNode As xml.xmlNode
            Dim oConflictingNode As xml.xmlNode
            Dim xOldID As String = ""
            Dim xTagID As String = ""
            Dim xXPath As String = ""
            Dim xParentID As String = ""
            Dim xFamilyParentID As String = ""
            Dim bMove As Boolean
            Dim xValue As String = ""
            Dim oWordXMLNodes As Word.XMLNodes
            Dim oTag As Tag
            Dim oTagID As Word.XMLNode
            Dim oObjectData As Word.XMLNode
            Dim oAuthorsDetail As Word.XMLNode
            Dim oAttribute As xml.xmlAttribute
            Dim oDocument As Word.Document
            Dim i As Short
            Dim xObjectData As String = ""
            Dim xAuthorsDetail As String = ""
            Dim xPart As String = ""
            Dim xParentFullID As String = ""
            Dim xDefinedVars As String = ""
            Dim oDefinedVars As Object
            Dim xName As String = ""
            Dim oTags As Tags
            Dim oDeletedScopes As Word.XMLNode
            Dim xDeletedScopes As Object
            Dim oPart As Word.XMLNode
            Dim xElementName As String = ""
            Dim xColIndex As String = ""
            Dim xDisplayName As String = ""
            Dim iPropIndex As Short
            Dim oParentVariable As Word.XMLNode
            Dim xParentVariable As String = ""
            Dim xSegmentID As String = ""
            Dim oProps As Object
            Dim xTagPrefixID As String = ""
            Dim xPassword As String = ""
            Dim xExistingPassword As String = ""
            Dim oWordDoc As WordDoc
            Dim oReserved As Word.XMLNode '10.2
            Dim xTag As String = ""
            Dim oConvert As Conversion
            Dim oTempID As Word.XMLNode
            Dim xOldTagID As String = ""
            Dim xOldObjectData As String = ""
            Dim xOldPart As String = ""
            Dim bEncrypted As Boolean
            Dim iEncryptionStatus As mpEncryptionStatus
            Dim oRevision As Word.Revision 'GLOG 7355 (dm)
            Dim oRange As Word.Range 'GLOG 7355 (dm)

            On Error GoTo ProcError

            oWordDoc = New WordDoc

            '10.2 - Get Tag used for Document Variable maintenance
            xTag = BaseMethods.GetTag(oWordXMLNode)

            'preconvert if necessary
            If xTag = "" Then
                oConvert = New Conversion
                oConvert.PreconvertXMLTag(oWordXMLNode, xTag)
            End If

            'add tag to node store only if it contains a TagID attribute
            oTagID = oWordXMLNode.SelectSingleNode("@TagID")
            If Not oTagID Is Nothing Then
                '4/18/11 (dm) - exit if tag is specified for exclusion -
                'we added this to keep deleted tags out of the collection
                'GLOG 7355 (dm) - we no longer check for revisions in advance -
                'we now check each tag before adding it - if the tag is marked
                'for deletion, this revision will be first in the tag's collection
                '        If xExclusions <> "" Then
                '           (WdUnits.wdSectionoTempID = oWordXMLNode.SelectSingleNode("@TempID")
                '            If Not oTempID Is Nothing Then
                '                If InStr(xExclusions, "|" & oTempID.NodeValue & "|") Then _
                '                    Exit Sub
                '            End If
                '        End If

                oRange = oWordXMLNode.Range

                'GLOG : 7595 : CEH
                If oWordDoc.RangeHasRevisions(oRange) Then
                    oRevision = oRange.Revisions(1)
                    If (oRevision.Type = Word.wdRevisionType.wdRevisionDelete) Or _
                            (oRevision.Type = Word.wdRevisionType.wdRevisionCellDeletion) Then
                        If (oRevision.Range.Start < oRange.Start) And _
                                (oRevision.Range.End > oRange.End) Then
                            Exit Sub
                        End If
                    End If
                End If

                m_iColIndex = m_iColIndex + 1
                If Me.Parent Is Nothing Then
                    xColIndex = m_iColIndex
                Else
                    xColIndex = Me.Parent.ColIndex & "." & m_iColIndex
                    xParentID = Me.Parent.TagID
                    xParentFullID = Me.Parent.FullTagID
                End If

                xTagID = oTagID.NodeValue
                xOldTagID = xTagID 'GLOG 5961

                'GLOG 5833 (dm) - there was a bug that encrypted the TagID attribute
                'when a row was manually added to a collection table - decrypt now
                If BaseMethods.IsEncrypted(xTagID) Then _
                    xTagID = BaseMethods.Decrypt(xTagID)

                xOldID = xTagID
                xElementName = oWordXMLNode.BaseName

                '10.2 -(WdUnits.wdSectionTag from Reserved Attribute of XML Node
                oReserved = oWordXMLNode.SelectSingleNode("@Reserved", "", True)
                If Not oReserved Is Nothing Then
                    xTag = oReserved.NodeValue
                Else
                    xTag = ""
                End If

                If xElementName = "mSEG" And SegmentTagExists(xTag) Then
                    'GLOG 7363: If XML Tag with same tag has already been added, retag to generate unique tag
                    BaseMethods.RetagXMLNode(oWordXMLNode)
                    xTag = BaseMethods.GetTag(oWordXMLNode)
                End If
                If xElementName = "mSEG" Then
                    'determine whether node is newly inserted
                    bIsNew = (m_bGUIDIndexing And (InStr(xTagID, "{") = 0))
                    If bIsNew Then
                        'use current client's password
                        xPassword = GlobalMethods.g_xEncryptionPassword
                    Else
                        'GLOG 3641 (dm) - adjust collection table if necessary
                        oWordDoc.AdjustCollectionTableIfNecessary(oWordXMLNode, bRestartRefresh)
                        If bRestartRefresh Then _
                            Exit Sub
                    End If

                    'get author detail
                    oAuthorsDetail = oWordXMLNode.SelectSingleNode("@Authors")
                    If Not oAuthorsDetail Is Nothing Then
                        xAuthorsDetail = oAuthorsDetail.NodeValue
                        If xAuthorsDetail <> "" Then
                            If BaseMethods.IsEncrypted(xAuthorsDetail) Then
                                xAuthorsDetail = BaseMethods.Decrypt(xAuthorsDetail)

                                If BaseMethods.GetEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Off Then
                                    'decompress in document
                                    oAuthorsDetail = oWordXMLNode.SelectSingleNode("@Authors")
                                    oAuthorsDetail.NodeValue = xAuthorsDetail
                                End If
                            Else
                                'compress in document
                                oAuthorsDetail = oWordXMLNode.SelectSingleNode("@Authors")
                                oAuthorsDetail.NodeValue = BaseMethods.Encrypt(xAuthorsDetail, xPassword)
                            End If
                            xAuthorsDetail = BaseMethods.ReplaceXMLChars(xAuthorsDetail)
                        End If
                    End If

                    'get parent variable
                    oParentVariable = oWordXMLNode.SelectSingleNode("@AssociatedParentVariable")
                    'GLOG 4784: Handle encryption
                    If Not oParentVariable Is Nothing Then
                        xParentVariable = oParentVariable.NodeValue
                        If xParentVariable <> "" Then
                            If BaseMethods.IsEncrypted(xParentVariable) Then
                                xParentVariable = BaseMethods.Decrypt(xParentVariable)

                                If BaseMethods.GetEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Off Then
                                    'decompress in document
                                    oParentVariable = oWordXMLNode.SelectSingleNode("@AssociatedParentVariable")
                                    oParentVariable.NodeValue = xParentVariable
                                End If
                            Else
                                'compress in document
                                oParentVariable = oWordXMLNode.SelectSingleNode("@AssociatedParentVariable")
                                oParentVariable.NodeValue = BaseMethods.Encrypt(xParentVariable, xPassword)
                            End If
                        End If
                    End If

                    'get deleted scopes
                    oDeletedScopes = oWordXMLNode.SelectSingleNode("@DeletedScopes")
                    If Not oDeletedScopes Is Nothing Then
                        xDeletedScopes = oDeletedScopes.NodeValue
                        If xDeletedScopes <> "" Then
                            If BaseMethods.IsEncrypted(xDeletedScopes) Then
                                xDeletedScopes = BaseMethods.Decrypt(xDeletedScopes)

                                If BaseMethods.GetEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Off Then
                                    'decompress in document
                                    oDeletedScopes = oWordXMLNode.SelectSingleNode("@DeletedScopes")
                                    oDeletedScopes.NodeValue = xDeletedScopes
                                End If
                            Else
                                'compress in document
                                oDeletedScopes = oWordXMLNode.SelectSingleNode("@DeletedScopes")
                                Dim xCompressed As String = ""
                                xCompressed = BaseMethods.Encrypt(xDeletedScopes, xPassword)
                                oDeletedScopes.NodeValue = xCompressed
                            End If
                        End If
                    End If

                    'get part number
                    oPart = oWordXMLNode.SelectSingleNode("@PartNumber")
                    If Not oPart Is Nothing Then
                        xPart = oPart.NodeValue
                        xOldPart = xPart 'GLOG 5961
                    Else
                        xPart = 1
                    End If
                End If

                'get object data
                oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
                If Not oObjectData Is Nothing Then
                    'GLOG 5961 (dm) - store original node value
                    xOldObjectData = oObjectData.NodeValue
                    xObjectData = BaseMethods.Decrypt(xOldObjectData, xExistingPassword)
                    xObjectData = BaseMethods.ReplaceXMLChars(xObjectData)
                    If xElementName = "mSEG" Then
                        'get external parent
                        lPos = InStr(xObjectData, "ParentTagID=")
                        If lPos > 0 Then
                            lPos = lPos + 12
                            lPos2 = InStr(lPos, xObjectData, "|")
                            'JTS 8/18/16: May be at end of Object Data string
                            If lPos2 = 0 Then
                                lPos2 = Len(xObjectData) + 1
                            End If
                            If Not BaseMethods.IsCollectionTableItem(xObjectData) Then
                                xFamilyParentID = Mid$(xObjectData, lPos, lPos2 - lPos)
                                '                    If xFamilyParentID <> "" Then _
                                '                        xOldID = xFamilyParentID & "." & xOldID
                            Else
                                'GLOG 3482 (3/20/09 dm) - parent tag id was inadvertently
                                'getting added to collection table items, which are never
                                'external children - clean up now
                                xObjectData = Left$(xObjectData, lPos - 13) & _
                                    Mid$(xObjectData, lPos2 + 1)
                            End If
                        End If
                    ElseIf xElementName = "mDel" Then
                        'get segment
                        oProps = Split(xObjectData, "|")
                        xSegmentID = oProps(0)
                    End If

                    'get/add tag prefix id -
                    'tag prefix id should be added at runtime only to newly inserted mSEGs
                    'and their child mVars and mBlocks
                    If xElementName <> "mDel" Then
                        xTagPrefixID = BaseMethods.GetTagPrefixID(xObjectData)

                        'added 2/12/09 (dm) to deal with client tag prefix ids that
                        'inadvertently got into the database - strip existing id if
                        'newly inserted or in design
                        If (xTagPrefixID <> "") And (bIsNew Or Not m_bGUIDIndexing) Then
                            lPos = InStr(xObjectData, GlobalMethods.mpTagPrefixIDSeparator)
                            If lPos > 0 Then _
                                xObjectData = Mid$(xObjectData, lPos + Len(GlobalMethods.mpTagPrefixIDSeparator))
                            xTagPrefixID = ""
                        End If

                        If bIsNew And (xTagPrefixID = "") And (GlobalMethods.g_xClientTagPrefixID <> "") Then
                            xObjectData = GlobalMethods.g_xClientTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & xObjectData
                        ElseIf (xTagPrefixID <> "") And (xTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (GlobalMethods.g_xClientTagPrefixID <> "") And (GlobalMethods.g_xClientTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (xTagPrefixID <> GlobalMethods.g_xClientTagPrefixID) Then
                            'tag does not belong to the current client - do not add node -
                            'this is temporarary - we hope to ultimately offer sharing of mp10
                            'functionality between clients - when we do, we'll add these tags to
                            'the tags collection and use the existing BelongsToCurrentClient
                            'property of segments, variables, and blocks to determine whether we
                            'can get field code values from the current db - exception for trailers
                            If (xElementName <> "mSEG") Or _
                                    (InStr(xObjectData, "ObjectTypeID=401|") = 0) Then
                                'adjust index
                                m_iColIndex = m_iColIndex - 1

                                'contained nodes may belong to the client
                                oWordXMLNodes = oWordXMLNode.SelectNodes("child::*[@TagID]")
                                If oWordXMLNodes.Count > 0 Then
                                    Add(oWordXMLNodes, bIsNew, , xExclusions)
                                End If

                                Exit Sub
                            End If
                        End If
                    End If
                End If

                'modify attributes and/or flag to move if necessary
                If xElementName = "mSEG" Then
                    If xOldID <> Me.FixedTopLevelTagID Then
                        'check whether there's another tag with same id and part #
                        xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                        oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                        If oConflictingNode Is Nothing Then
                            'no conflict - check whether we've already found another
                            'part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                        ElseIf m_bGUIDIndexing Then
                            'GLOG 3003: there is a conflict - increment PartNumber until unused Part is found
                            'Use current conflicting node for mSEG attributes
                            oPartnerNode = oConflictingNode
                            Do
                                xPart = Trim(Str(Val(xPart) + 1))
                                xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                                oConflictingNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                            Loop Until oConflictingNode Is Nothing
                        Else
                            'there is a conflict at design time - check whether we've already found
                            'and reindexed another part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "' and @TagID!='" & xOldID & _
                                "' and @Part!='" & xPart & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                            'if no partner node found, reindex
                            If oPartnerNode Is Nothing Then _
                                ReindexSegment(xTagID, xParentFullID, False)
                        End If

                        If Not oPartnerNode Is Nothing Then
                            'another part found
                            With oPartnerNode.attributes
                                'assign the same new id
                                oAttribute = .getNamedItem("TagID")
                                xTagID = oAttribute.value

                                'if not in same collection as other part, flag to move there
                                oAttribute = .getNamedItem("ColIndex")
                                lPos = InStrRev(oAttribute.value, ".")
                                If (lPos = 0) And (Not Me.Parent Is Nothing) Then
                                    'destination is top level
                                    xDestination = "0"
                                ElseIf lPos > 0 Then
                                    'get destination
                                    xDestination = Left$(oAttribute.value, lPos - 1)
                                    'clear flag if already in same collection
                                    If Not Me.Parent Is Nothing Then
                                        If Me.Parent.ColIndex = xDestination Then _
                                            xDestination = ""
                                    End If
                                End If
                            End With
                        Else
                            'reindex if necessary
                            If (InStr(xTagID, "_") = 0) Or _
                                    (m_bGUIDIndexing And (InStr(xTagID, "{") = 0)) Or _
                                    (Not m_bGUIDIndexing And (InStr(xTagID, "{") <> 0)) Then
                                'item is newly inserted - reindex
                                ReindexSegment(xTagID, xParentFullID, False)

                                'update NewSegment property
                                If Me.Parent Is Nothing Then _
                                    m_xNewSegment = xTagID
                            End If
                        End If

                        'deal with external children
                        If xFamilyParentID <> "" Then
                            'look for parent
                            xXPath = "//mSEG[@OldID='" & xFamilyParentID & "']"
                            oParentNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                            If Not oParentNode Is Nothing Then
                                'we've found a tag for the parent- update ParentTagID in ObjectData
                                oAttribute = oParentNode.attributes.getNamedItem("TagID")
                                If xFamilyParentID <> oAttribute.value Then
                                    xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                        xFamilyParentID, "ParentTagID=" & oAttribute.value & "|")
                                    xFamilyParentID = oAttribute.value
                                End If

                                'check whether child is physically contained within parent -
                                'if not, we'll later need to move it
                                If Me.Parent Is Nothing Then
                                    bMove = True
                                ElseIf xParentID <> xFamilyParentID Then
                                    bMove = True
                                End If
                                If bMove Then
                                    oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                                    xDestination = oAttribute.value
                                End If
                            Else
                                'parent is not in document - update object data
                                'GLOG 5950 (dm) - don't clear ParentTagID immediately -
                                'when some shapes are Word 2010 shapes, and others are
                                'textboxes, the nodes may not load in the expected order -
                                'truthfully, we've just been lucky to date that parents in
                                'textboxes (e.g. pleading paper) have loaded before their
                                'external children in textboxes (e.g. sidebar) - this
                                'is now handled in UniteFamilies()
                                '                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                '                            xFamilyParentID & "|", "")
                                '                        xFamilyParentID = ""
                                xDestination = "Pending" & xFamilyParentID
                            End If
                        End If
                    End If

                    'resolve naming conflicts in variable definitions stored in this mSEG -
                    '1/8/09 (Doug) - just get variable names - I remmed out the conflict
                    'resolution code because 1) RenameItemIfNecessary wasn't working properly
                    'for tagless variables and 2) it didn't seem worth fixing because
                    'I can't imagine how such a conflict between two tagless variables would occur
                    xDefinedVars = GetDefinedVariableNames(xObjectData, xDeletedScopes)
                    '            If xDefinedVars <> "" Then
                    '                oDefinedVars = Split(xDefinedVars, "|")
                    '                For i = 0 To UBound(oDefinedVars) - 1
                    '                    xName = oDefinedVars(i)
                    '                    RenameItemIfNecessary xName, xObjectData, xTagID, _
                    '                        xParentFullID, "mSEG", ""
                    '                Next i
                    '            End If
                ElseIf xElementName = "mDel" Then
                    'ensure that mDel becomes child of its owning segment -
                    'GLOG 3702 (dm) - in design mode, mDels of secondary collection table items
                    'were getting assigned to the primary item when secondary item was the
                    'same segment - this is because the primary item has the same old id -
                    'start by looking for a match for both the old and new ids
                    xXPath = "//mSEG[@OldID='" & xSegmentID & "' and @TagID='" & xParentID & "']"
                    oParentNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                    If oParentNode Is Nothing Then
                        'search for just the old id
                        xXPath = "//mSEG[@OldID='" & xSegmentID & "']"
                        oParentNode = GlobalMethods.g_oContentXML.SelectSingleNode(xXPath)
                    End If
                    If Not oParentNode Is Nothing Then
                        'we've found a tag for the parent- update ObjectData if necessary
                        oAttribute = oParentNode.attributes.getNamedItem("TagID")
                        If xSegmentID <> oAttribute.value Then
                            lPos = InStr(xObjectData, "|")
                            xObjectData = oAttribute.value & Mid$(xObjectData, lPos)
                            xSegmentID = oAttribute.value
                        End If

                        'check whether mDel's immediate parent is owning mSEG -
                        'if not, we'll later need to move it
                        If Me.Parent Is Nothing Then
                            bMove = True
                        ElseIf xParentID <> xSegmentID Then
                            bMove = True
                        End If
                        If bMove Then
                            oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                            xDestination = oAttribute.value
                        End If
                    Else
                        'don't add mDel if parent mSEG isn't in collection
                        m_iColIndex = m_iColIndex - 1
                        Exit Sub
                    End If
                Else
                    'get variable value; this will be added to our content xml, so replace any
                    'character that can't be used "as is" in xml character data
                    If xElementName = "mVar" Then
                        With oWordXMLNode
                            xValue = UCase$(.Text)
                            xValue = Replace(xValue, Chr(7), "Chr7")
                            xValue = Replace(xValue, Chr(11), "Chr11")
                            xValue = Replace(xValue, Chr(12), "Chr12")
                            xValue = Replace(xValue, Chr(13), "Chr13")
                            xValue = Replace(xValue, Chr(21), "Chr21")

                            'GLOG 4476 (dm) - replace field character
                            xValue = Replace(xValue, Chr(19), "Chr19")

                            'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                            xValue = Replace(xValue, Chr(2), "")

                            'GLOG 5978 (dm) - replace non-breaking hyphen
                            xValue = Replace(xValue, Chr(30), "Chr30")

                            'GLOG 6041 (dm) - replace comment characters
                            xValue = Replace(xValue, Chr(5), "Chr5")

                            'GLOG 7417 (dm) - remove graphic
                            xValue = Replace(xValue, Chr(1), "")

                            'Also replace XML Reserved characters
                            xValue = BaseMethods.ReplaceXMLChars(xValue)
                        End With
                    End If

                    'resolve naming conflicts in variables and blocks
                    RenameItemIfNecessary(xTagID, xObjectData, xTagID, xParentFullID, _
                        xElementName, xValue)
                End If

                'add to GlobalMethods.g_oContentXML
                lPos = InStr(GlobalMethods.g_oContentXML.OuterXml, "</mpNodes>")
                If xElementName = "mSEG" Then
                    'mSEG - include defined variables
                    xText = "<mSEG TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' DefinedVariables='" & xDefinedVars & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' Part='" & xPart & "' Authors='" & xAuthorsDetail & "' AssociatedParentVariable='" & xParentVariable & "' />"
                Else
                    'include value
                    xText = "<" & xElementName & " TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' Value='" & xValue & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' />"
                End If

                GlobalMethods.g_oContentXML.LoadXml(Left$(GlobalMethods.g_oContentXML.OuterXml, lPos - 1) & xText & "</mpNodes>")

                'update doc - we need to reour existing object variables because these
                'references are not stable, i.e. they may now point to a different attribute
                'GLOG 5961 (dm) - don't write to document unless value is
                'actually changing or needs to be encrypted or unencrypted -
                'if tags are nested, setting attribute value will show up in
                'comparisons as a revision even if value doesn't change
                If xTagID <> xOldTagID Then
                    oTagID = oWordXMLNode.SelectSingleNode("@TagID")
                    oTagID.NodeValue = xTagID
                    '10.2
                    BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)
                End If

                iEncryptionStatus = BaseMethods.GetEncryptionStatus
                bEncrypted = BaseMethods.IsEncrypted(xOldObjectData)
                xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                If (xObjectData <> "") And ((BaseMethods.Decrypt(xOldObjectData) <> xObjectData) Or _
                        ((xElementName <> "mDel") And (((iEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_On) And _
                        Not bEncrypted)) Or (((iEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Off) And bEncrypted)))) Then
                    oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
                    If xElementName = "mDel" Then
                        'mDel object data should not be compressed
                        oObjectData.NodeValue = xObjectData
                    Else
                        If bIsNew Then
                            'compress with current client's password
                            xPassword = GlobalMethods.g_xEncryptionPassword
                        Else
                            'recompress with existing password
                            xPassword = xExistingPassword
                        End If
                        oObjectData.NodeValue = BaseMethods.Encrypt(xObjectData, xPassword)
                    End If
                    '10.2
                    BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, xPassword, oDocument)
                End If

                'GLOG 3003: Update PartNumber attribute in case changed
                If (xPart <> "") And (xPart <> xOldPart) Then
                    oPart = oWordXMLNode.SelectSingleNode("@PartNumber")
                    oPart.NodeValue = xPart
                    '10.2
                    BaseMethods.SetAttributeValue(xTag, "PartNumber", xPart, xPassword, oDocument)
                End If
                If xDeletedScopes <> "" Then
                    'this is simply to encrypt or unencrypt doc vars if necessary
                    BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedScopes, _
                        xPassword, oDocument)
                End If

                '10/26/11 (dm) - reindex snapshots if necessary - this comes into play
                'when using the Copy Segment/Paste Segment functionality
                If bIsNew Then _
                    BaseMethods.ReindexSnapshot(oDocument, xOldID, xTagID, True)

                'create new tag and(WdUnits.wdSectionits properties
                oTag = New LMP.Forte.MSWord.Tag
                With oTag
                    .ElementName = xElementName
                    .TagID = xTagID
                    .ObjectData = xObjectData
                    .Part = xPart
                    .Tag = xTag '10.2
                    .DeletedScopes = xDeletedScopes
                    .Authors = xAuthorsDetail
                    .ParentVariable = xParentVariable

                    .WordXMLNode = oWordXMLNode
                    If Me.Parent Is Nothing Then
                        .FullTagID = xTagID
                    Else
                        .FullTagID = xParentFullID & "." & xTagID
                        .ParentPart = Me.Parent.Part
                    End If
                    .ColIndex = xColIndex
                    With .Tags
                        .Parent = oTag
                        .FixedTopLevelTagID = Me.FixedTopLevelTagID
                        .GUIDIndexing = Me.GUIDIndexing
                        .BoundingObjectType = mpBoundingObjectTypes.WordXMLNodes
                    End With
                End With

                'add tag to collection
                m_Col.Add(oTag)
                'GLOG 7363: Keep track of Segment tags to avoid duplicates
                If xElementName = "mSEG" Then
                    m_ColSegBmks.Add(xTag)
                End If
                'GLOG 7022: If Segment Bounding Objects are bookmarks,
                'don't process child Content Controls, as they will already
                'have been added by AddNode_Bookmark for the Parent.
                If bIncludeChildren Then
                    'add contained nodes
                    oWordXMLNodes = oWordXMLNode.SelectNodes("child::*[@TagID]")
                    If oWordXMLNodes.Count > 0 Then
                        For i = 1 To oWordXMLNodes.Count
                            If xElementName = "mSEG" Then
                                'add to this tag's collection
                                oTag.Tags.AddNode(oWordXMLNodes(i), bIsNew, bRestartRefresh, xExclusions)
                            Else
                                'add to this tag's parent's collection

                                AddNode(oWordXMLNodes(i), bIsNew, bRestartRefresh, xExclusions)
                            End If
                        Next i
                    ElseIf xElementName = "mVar" Then
                        'look for mDels contained in child mSubVars - if found,
                        'add to this tag's parent's collection
                        oWordXMLNodes = oWordXMLNode.SelectNodes("descendant::x:mDel", _
                            "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                        For i = 1 To oWordXMLNodes.Count
                            AddNode(oWordXMLNodes(i), bIsNew, bRestartRefresh, xExclusions)
                        Next i
                        '            If xElementName = "mSEG" Then
                        '                'add to this tag's collection
                        '                oTag.Tags.Add oWordXMLNodes, bIsNew, bRestartRefresh, xExclusions
                        '            Else
                        '                'add to this tag's parent's collection
                        '
                        '                Add oWordXMLNodes, bIsNew, bRestartRefresh, xExclusions
                        '            End If
                        '        ElseIf xElementName = "mVar" Then
                        '            'look for mDels contained in child mSubVars - if found,
                        '            'add to this tag's parent's collection
                        '           (WdUnits.wdSectionoWordXMLNodes = oWordXMLNode.SelectNodes("descendant::x:mDel", _
                        '                "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                        '            If oWordXMLNodes.Count > 0 Then _
                        '                Add oWordXMLNodes, bIsNew, bRestartRefresh, xExclusions
                    End If
                End If
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public ReadOnly Property Item(lIndex As Integer) As Tag
            Get
                Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Item"

                On Error Resume Next
                Return m_Col(lIndex)
                If Err.Number <> 0 Then
                    On Error GoTo ProcError
                    Err.Raise(mpErrors.mpError_MatchingItemNotInCollection, , _
                        "<Error_ItemNotInCollection>")
                End If

                Exit Property
ProcError:
                GlobalMethods.RaiseError(mpThisFunction)
                Exit Property
            End Get
        End Property

        Public ReadOnly Property Count() As Integer
            Get
                Return m_Col.Count
            End Get
        End Property

        Public Property Parent() As LMP.Forte.MSWord.Tag
            Get
                Return m_oParent
            End Get
            Set(value As LMP.Forte.MSWord.Tag)
                m_oParent = value
            End Set
        End Property

        Public Property FixedTopLevelTagID As String
            Get
                Return m_xFixedTopLevelTagID
            End Get
            Friend Set(value As String)
                m_xFixedTopLevelTagID = value
            End Set
        End Property

        Public Property GUIDIndexing As Boolean
            Get
                Return m_bGUIDIndexing
            End Get
            Friend Set(value As Boolean)
                m_bGUIDIndexing = value
            End Set
        End Property

        'since we're no longer using this, we decided to avoid confusion by not exposing it;
        'it can simply be unremmed if necessary, as m_xSegment is still getting(WdUnits.wdSectionas before
        'Public Property Get NewSegment() as string = ""
        '    NewSegment = m_xNewSegment
        'End Property

        Public ReadOnly Property XMLSummary As String
            Get
                If m_xXMLSummary = "" Then _
                    m_xXMLSummary = BuildXMLSummary()
                Return m_xXMLSummary
            End Get
        End Property

        Private Sub ReindexSegment(ByRef xTagID As String, _
                                   ByVal xParentID As String, _
                                   ByVal bIsOnManualInsertion As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.ReindexSegment"
            Dim lPos As Integer
            Dim xTemp As String = ""
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim i As Short
            Dim xID As String = ""
            Dim iMax As Short
            Dim iIndex As Short
            Dim oAttribute As xml.xmlAttribute

            On Error GoTo ProcError

            xTemp = xTagID

            lPos = InStrRev(xTemp, "_")
            If lPos > 0 Then
                'strip index
                xTemp = Left$(xTemp, lPos)
            Else
                'add separator
                xTemp = xTemp & "_"
            End If

            If m_bGUIDIndexing Then
                'generate GUID
                xID = Replace(BaseMethods.GenerateGUID(True), "-", "")

                'append GUID
                xTagID = xTemp & xID
            Else
                'get short index
                If bIsOnManualInsertion Then
                    'search summary xml for other instances of the same segment;
                    'GLOG 3700 (dm) - disregard parent - cousins should also be reindexed
                    '            xXPath = "//mSEG[starts-with(@TagID, '" & xParentID & "." & xTemp & "')]"
                    xXPath = "//mSEG[contains(@TagID, '." & xTemp & "') and not(contains(" & _
                        "substring-after(@TagID, '." & xTemp & "'), '.'))]"
                    oNodeList = m_oSummaryXML.selectNodes(xXPath)
                Else
                    'search content xml for other instances of the same segment;
                    'GLOG 3700 (dm) - disregard parent - cousins should also be reindexed
                    '            xXPath = "//mSEG[starts-with(@TagID, '" & xTemp & "') and " & _
                    '                "@ParentID='" & xParentID & "']"
                    xXPath = "//mSEG[starts-with(@TagID, '" & xTemp & "')]"
                    oNodeList = GlobalMethods.g_oContentXML.selectNodes(xXPath)
                End If

                'cycle through matches looking for highest existing index
                For i = 0 To (oNodeList.Count - 1)
                    oNode = oNodeList.Item(i)
                    oAttribute = oNode.attributes.getNamedItem("TagID")
                    xID = oAttribute.value

                    'we're only interested in same generation
                    If Not bIsOnManualInsertion Or _
                            (InStr(Len(xParentID) + 2, xID, ".") = 0) Then
                        lPos = InStrRev(xID, "_")
                        iIndex = CInt(Mid$(xID, lPos + 1))
                        If iIndex > iMax Then _
                            iMax = iIndex
                    End If
                Next i

                'increment/append index
                xTagID = xTemp & (iMax + 1)
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub ResolveFixedTopLevelPartConflicts()
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.ResolveFixedTopLevelPartConflicts"
            Dim i As Short
            Dim oTag As Tag
            Dim iMax As Short
            Dim iPart As Short
            Dim xParts As String = ""
            Dim xConflicts As String = ""
            Dim iPos As Integer
            Dim vConflicts As Object
            Dim vTag As Object
            Dim oReserved As Word.XMLNode

            On Error GoTo ProcError

            'cycle through top-level tags, looking for ones that correspond
            'to fixed top-level segment
            For i = 1 To m_Col.Count
                oTag = m_Col.Item(i)
                If oTag.TagID = Me.FixedTopLevelTagID Then
                    'matching tag found - get current part number
                    iPart = CInt(oTag.Part)

                    'update max tracker if necessary
                    If iPart > iMax Then _
                        iMax = iPart

                    'check running string for conflict and update conflict
                    'info string accordingly - if part number does not conflict
                    'with another tag, 1st field will be "0" - 2nd field holds
                    'collection index
                    xConflicts = xConflicts & InStr(xParts, iPart & "|") & _
                        "�" & i & "|"

                    'update running string
                    xParts = xParts & iPart & "|"
                End If
            Next i

            'resolve conflicts (if any)
            If xConflicts <> "" Then
                'there are fixed top-level segment tags -
                'put conflict info into an array
                vConflicts = Split(xConflicts, "|")
                For i = 0 To UBound(vConflicts) - 1
                    vTag = Split(vConflicts(i), "�")
                    If vTag(0) <> "0" Then
                        'conflict found - change part number to 1 more than the
                        'existing highest part number
                        oTag = m_Col.Item(CInt(vTag(1)))
                        iMax = iMax + 1
                        oTag.Part = iMax
                        If Me.BoundingObjectType = mpBoundingObjectTypes.WordXMLNodes Then
                            oTag.WordXMLNode.SelectSingleNode("@PartNumber") _
                                .NodeValue = CStr(iMax)
                            '10.2 - Update Doc Variable also
                            oReserved = oTag.WordXMLNode.SelectSingleNode("@Reserved")
                            If Not oReserved Is Nothing Then
                                BaseMethods.SetAttributeValue(oReserved.NodeValue, "PartNumber", _
                                    CStr(iMax), "")
                            End If
                        Else
                            BaseMethods.SetAttributeValue(oTag.ContentControl.Tag, "PartNumber", _
                                CStr(iMax), "")
                        End If
                    End If
                Next i
            End If

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub RenameItemIfNecessary(ByRef xName As String, _
                                          ByRef xObjectData As String, _
                                          ByVal xTagID As String, _
                                          ByVal xParentID As String, _
                                          ByVal xElementName As String, _
                                          ByVal xValue As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.RenameItemIfNecessary"
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oTestNode As xml.xmlNode
            Dim xChar As String = ""
            Dim xFullTagID As String = ""
            Dim oValue As xml.xmlAttribute
            Dim oObjectData As xml.xmlAttribute
            Dim iPropIndex As Short
            Dim bValueIsSame As Boolean
            Dim bObjectDataIsSame As Boolean
            Dim xDisplayName As String = ""

            On Error GoTo ProcError

            If xElementName = "mVar" Then
                'If mVar is distributed, same tag name is allowed with different value
                If InStr(UCase(BaseMethods.GetObjectDataValue(xObjectData,
                    mpVariableProperties.mpVariableProperty_DistributedDetail, _
                    "mVar", xName)), "TRUE") Then Exit Sub

                'variable - look for 1) a node with the same tag id and parent id, but
                'with a different value or object data, or 2) a definition of same-named
                'variable in an mSEG belonging to parent - check either DefinedVariables attribute
                'starting with 'xName|' or containing '|xName|'
                xXPath = "//mVar[@TagID='" & xName & "' and @ParentID='" & xParentID & _
                    "' and ((@Value!='" & xValue & "') or (@ObjectData!='" & xObjectData & _
                    "'))] | //mSEG[@TagID='" & xParentID & "' and " & _
                    "((substring(@DefinedVariables, 1, " & (Len(xName) + 1) & ")='" & xName & "|') or " & _
                    "(contains(@DefinedVariables, '|" & xName & "|')))]"
            ElseIf xElementName = "mSEG" Then
                'variable definition in mSEG - look for a node with the same tag id
                'and parent id
                If xParentID = "" Then
                    xFullTagID = xTagID
                Else
                    xFullTagID = xParentID & "." & xTagID
                End If
                xXPath = "//mVar[@TagID='" & xName & "' and @ParentID='" & xFullTagID & "']"
            Else
                'block - look for node with same tag id and parent id - if found,
                'rename regardless of value - block ids must always be unique within
                'container
                'GLOG 7022: Include '|' delimiters in comparison so that like-named blocks in Parent Segment
                'won't result in renaming
                xXPath = "//mBlock[@TagID='" & xName & "' and @ParentID='" & xParentID & "']"
            End If

            oNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
            If Not oNode Is Nothing Then
                'get display name
                If xElementName <> "mBlock" Then
                    xDisplayName = BaseMethods.GetObjectDataValue(xObjectData, _
                        mpVariableProperties.mpVariableProperty_DisplayName, "mVar", xName)
                Else
                    xDisplayName = BaseMethods.GetObjectDataValue(xObjectData, _
                        mpBlockProperties.mpBlockProperty_DisplayName, "mBlock", xName)
                End If

                oObjectData = oNode.attributes.getNamedItem("ObjectData")

                If (xElementName = "mVar") And (oNode.Name = "mVar") Then
                    'don't rename if only difference is that one tag contains
                    'placeholder text and the other does not
                    oValue = oNode.Attributes.GetNamedItem("Value")
                    'GLOG 6495 - replace xml chars in oValue
                    Dim xValue2 As String = ""
                    xValue2 = BaseMethods.ReplaceXMLChars(oValue.value)
                    bValueIsSame = ((xValue = xValue2) Or _
                        (xValue = UCase$(xDisplayName)) Or _
                        (xValue2 = UCase$(xDisplayName)))

                    'don't rename if only difference is raw XML chars in the object data
                    bObjectDataIsSame = _
                        (BaseMethods.ReplaceXMLChars(oObjectData.value) = xObjectData)

                    If bValueIsSame And bObjectDataIsSame Then _
                        Exit Sub
                End If

                'rename
                Do
                    If xElementName = "mBlock" Then
                        xName = GetNewBlockName(xName)
                        xDisplayName = GetNewBlockName(xDisplayName)
                    Else
                        xName = GetNewName(xName)
                        xDisplayName = GetNewName(xDisplayName)
                    End If

                    'ensure that new name is unique
                    xXPath = "//" & xElementName & "[@TagID='" & xName & "' and @ParentID='" & _
                        xParentID & "']"
                    If xElementName <> "mBlock" Then
                        'also look for same-named variable definitions in mSEG
                        xXPath = xXPath & " | //mSEG[(@TagID='" & xTagID & "') and " & _
                            "(@ParentID='" & xParentID & "') and " & _
                            "((substring(@DefinedVariables, 1, " & (Len(xName) + 1) & ")='" & xName & "|') or " & _
                            "(contains(@DefinedVariables, '|" & xName & "|')))]"
                    End If
                    oTestNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    If oTestNode Is Nothing Then
                        'new name is unique
                        Exit Do
                    ElseIf (xElementName = "mVar") And _
                            (oTestNode.SelectSingleNode("@Value").InnerText = xValue) Then
                        'tag with this name has same value as this tag - synchronize definitions
                        xObjectData = oTestNode.SelectSingleNode("@ObjectData").InnerText
                        Exit Sub
                    End If
                Loop

                'modify definition accordingly
                If xElementName <> "mBlock" Then
                    AssignNewVariableProps(xObjectData, xName, xDisplayName)
                Else
                    AssignNewBlockProps(xObjectData, xName, xDisplayName)
                End If
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub UniteFamilies()
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.UniteFamilies"
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim xXPath As String = ""
            Dim i As Short
            Dim oAttribute As xml.xmlAttribute
            Dim xColIndex As String = ""
            Dim xDest As String = ""
            Dim oIndicies As Object
            Dim j As Short
            Dim oSourceTag As Tag
            Dim oDest As Tag
            Dim xTagID As String = ""
            Dim oTarget As xml.xmlNode
            Dim iLevel As Short
            Dim arrNodes() As xml.xmlNode
            Dim iLength As Short
            Dim oWordDoc As WordDoc
            Dim xTag As String = ""

            On Error GoTo ProcError

            oWordDoc = New WordDoc 'GLOG 5950 (dm)

            xXPath = "//*[@Destination!='']"
            oNodeList = GlobalMethods.g_oContentXML.selectNodes(xXPath)
            iLength = oNodeList.Count

            If iLength = 0 Then
                Exit Sub
            End If

            ReDim arrNodes(iLength - 1)

            For i = 0 To (iLength - 1)
                oNode = oNodeList.Item(i)
                oAttribute = oNode.attributes.getNamedItem("ColIndex")
                xColIndex = oAttribute.value
                oAttribute = oNode.attributes.getNamedItem("Destination")
                xDest = oAttribute.value

                'get node to move
                oIndicies = Split(xColIndex, ".")
                oSourceTag = m_Col.Item(CInt(oIndicies(0)))

                For j = 1 To UBound(oIndicies)
                    oSourceTag = oSourceTag.Tags.Item(CInt(oIndicies(j)))
                Next j

                'If oSourceTag.ElementName = "mSEG" Then
                If xDest = "0" Then
                    'copy source to top level
                    AddFamilyMember(oSourceTag, True)
                ElseIf Left$(xDest, 7) = "Pending" Then
                    'GLOG 5950 (dm) - parent hasn't been found yet -
                    'determine whether it just loaded later
                    Dim xParentID As String = ""
                    Dim oParentNode As xml.xmlNode
                    Dim xObjectData As String = ""
                    Dim oWordTag As Word.XMLNode
                    Dim oCC As Word.ContentControl
                    Dim oBmk As Word.Bookmark

                    xParentID = Mid$(xDest, 8)
                    'GLOG 6518 (dm) - search for a matching @OldID attribute - we were
                    'previously searching @TagID, which won't work if parent has been reindexed
                    oParentNode = GlobalMethods.g_oContentXML.selectSingleNode( _
                        "//mSEG[@OldID='" & xParentID & "']")
                    If Not oParentNode Is Nothing Then
                        'add to parent's collection
                        oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                        xColIndex = oAttribute.value
                        oDest = ItemFromColIndex(xColIndex)
                        oDest.Tags.AddFamilyMember(oSourceTag, True)

                        'replace pending flag with col index
                        oAttribute = oNode.attributes.getNamedItem("Destination")
                        oAttribute.value = xColIndex

                        'GLOG 6518 - update ParentTagID in object data if necessary
                        oAttribute = oParentNode.attributes.getNamedItem("TagID")
                        If xParentID <> oAttribute.Value Then
                            'JTS 8/18/16: May not be followed by pipe if at end of object data
                            xObjectData = Replace(oSourceTag.ObjectData, _
                                "ParentTagID=" & xParentID, _
                                "ParentTagID=" & oAttribute.Value)
                            oSourceTag.ObjectData = xObjectData
                            If Me.BoundingObjectType = mpBoundingObjectTypes.ContentControls Then
                                oCC = oSourceTag.ContentControl

                                If oCC Is Nothing Then
                                    'get bookmark tag
                                    xTag = Mid$(oSourceTag.Bookmark.Name, 2)
                                Else
                                    xTag = oCC.Tag
                                End If

                                BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "")
                            Else
                                oWordTag = oSourceTag.WordXMLNode

                                If oWordTag Is Nothing Then
                                    'get bookmark tag
                                    xTag = Mid$(oSourceTag.Bookmark.Name, 2)
                                    BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "")
                                Else
                                    oWordDoc.SetAttributeValue(oWordTag, "ObjectData", xObjectData)
                                End If
                            End If
                        End If
                    Else
                        'parent is not in document - update object data
                        xObjectData = Replace(oSourceTag.ObjectData, _
                            "ParentTagID=" & xParentID & "|", "")
                        'JTS 8/18/16: May not be followed by Pipe if at end of Object Data
                        xObjectData = Replace(oSourceTag.ObjectData, _
                            "ParentTagID=" & xParentID, "")
                        oSourceTag.ObjectData = xObjectData
                        If Me.BoundingObjectType = mpBoundingObjectTypes.ContentControls Then
                            oCC = oSourceTag.ContentControl

                            If Not oCC Is Nothing Then
                                BaseMethods.SetAttributeValue(oCC.Tag, "ObjectData", xObjectData, "")
                            Else
                                oBmk = oSourceTag.Bookmark
                                LMP.Forte.MSWord.BaseMethods.SetAttributeValue(Mid$(oBmk.Name, 2), "ObjectData", xObjectData, "")
                            End If
                        Else
                            oWordTag = oSourceTag.WordXMLNode

                            If Not oWordTag Is Nothing Then
                                oWordDoc.SetAttributeValue(oWordTag, "ObjectData", xObjectData)
                            Else
                                oBmk = oSourceTag.Bookmark
                                LMP.Forte.MSWord.BaseMethods.SetAttributeValue(Mid$(oBmk.Name, 2), "ObjectData", xObjectData, "")
                            End If
                        End If
                    End If
                Else
                    'get destination node
                    oIndicies = Split(xDest, ".")
                    oDest = m_Col.Item(CInt(oIndicies(0)))
                    For j = 1 To UBound(oIndicies)
                        oDest = oDest.Tags.Item(CInt(oIndicies(j)))
                    Next j

                    'copy source to destination
                    oDest.Tags.AddFamilyMember(oSourceTag, True)
                End If
                'End If

                'add original node to array
                arrNodes(i) = oNode
            Next i

            'delete moved nodes from original collections - we wait
            'until the end to do this, to preserve indicies during cycle
            For i = UBound(arrNodes) To 0 Step -1
                oNode = arrNodes(i)
                'GLOG 5950 (dm) - dont delete if still flagged as pending
                oAttribute = oNode.attributes.getNamedItem("Destination")
                If InStr(oAttribute.value, "Pending") = 0 Then
                    oAttribute = oNode.attributes.getNamedItem("ColIndex")
                    xColIndex = oAttribute.value
                    oIndicies = Split(xColIndex, ".")
                    iLevel = UBound(oIndicies)
                    If iLevel = 0 Then
                        m_Col.Remove(CInt(oIndicies(0)))
                    Else
                        oSourceTag = m_Col.Item(CInt(oIndicies(0)))
                        For j = 1 To iLevel - 1
                            oSourceTag = oSourceTag.Tags.Item(CInt(oIndicies(j)))
                        Next j
                        oSourceTag.Tags.RemoveTag(CLng(oIndicies(iLevel)))
                    End If
                End If
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub AddFamilyMember(oTag As Tag, bValidateTagID As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddFamilyMember"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xXML As String = ""
            Dim xXPath As String = ""
            Dim oConflictingNode As xml.xmlNode
            Dim xTagID As String = ""
            Dim xFullParentTagID As String = ""
            Dim xParentTagID As String = ""
            Dim oTagID As Word.XMLNode
            Dim oReserved As Word.XMLNode
            Dim xTag As String 'GLOG 7636 (dm)

            On Error GoTo ProcError

            With oTag
                xTagID = .TagID

                If Not m_oParent Is Nothing Then
                    'get both parent tag id and full tag id for later use
                    xFullParentTagID = m_oParent.FullTagID

                    lPos = InStrRev(xFullParentTagID, ".")

                    If lPos > 0 Then
                        xParentTagID = Mid$(xFullParentTagID, lPos + 1)
                    Else
                        xParentTagID = xFullParentTagID
                    End If
                End If

                'check whether there's another already a tag with same id and part #
                If bValidateTagID Then
                    xXPath = "//mSEG[@TagID='" & xTagID & "' and @ParentID='" & _
                        xFullParentTagID & "' and @Part='" & .Part & "']"
                    oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    If Not oConflictingNode Is Nothing Then
                        'reindex
                        ReindexSegment(xTagID, xFullParentTagID, False)
                        .TagID = xTagID
                        'JTS 6/1710: Check BoundingObjectType
                        If Me.BoundingObjectType = mpBoundingObjectTypes.WordXMLNodes Then
                            'GLOG 7636 (dm) - code for bookmark mSEGs
                            If .WordXMLNode Is Nothing Then
                                'get bookmark tag
                                xTag = Mid$(.Bookmark.Name, 2)
                                BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "")
                            Else
                                .WordXMLNode.SelectSingleNode("@TagID").NodeValue = xTagID
                                '10.2 - Update Doc Variable also
                                oReserved = .WordXMLNode.SelectSingleNode("@Reserved")
                                If Not oReserved Is Nothing Then
                                    BaseMethods.SetAttributeValue(oReserved.NodeValue, "TagID", xTagID, "")
                                End If
                            End If
                        ElseIf Me.BoundingObjectType = mpBoundingObjectTypes.ContentControls Then
                            'GLOG 7636 (dm) - code for bookmark mSEGs
                            If .ContentControl Is Nothing Then
                                'get bookmark tag
                                xTag = Mid$(.Bookmark.Name, 2)
                            Else
                                xTag = .ContentControl.Tag
                            End If
                            BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "")
                        End If
                    End If
                End If

                'ensure that parent tag id is correct - replace
                'old id with GUID-based value
                Dim xObjData As String = ""

                xObjData = oTag.ObjectData
                lPos = InStr(xObjData, "ParentTagID=")
                If lPos > 0 Then
                    lPos2 = InStr(lPos, xObjData, "|")
                    'JTS 8/18/16: There may be no pipe char if at end of Object Data
                    If lPos2 = 0 Then
                        lPos2 = Len(xObjData) + 1
                    End If
                    oTag.ObjectData = Left$(xObjData, lPos + 11) + xParentTagID + Mid$(xObjData, lPos2)
                End If
                'add node to collection
                m_Col.Add(oTag)
                m_iColIndex = m_iColIndex + 1
                If m_oParent Is Nothing Then
                    .ColIndex = m_iColIndex
                Else
                    .ColIndex = m_oParent.ColIndex & "." & m_iColIndex
                End If

                'add to GlobalMethods.g_oContentXML
                xXML = "<mSEG TagID='" & .TagID & "' ColIndex='" & .ColIndex & _
                    "' ParentID='" & xParentTagID & "' Part='" & .Part & "' />"

                'update FullTagID property of this node and its subnodes
                If m_oParent Is Nothing Then
                    .FullTagID = .TagID
                Else
                    .FullTagID = xFullParentTagID & "." & .TagID
                End If
                .UpdateFullTagIDs()
            End With

            'load updated content xml
            lPos = InStr(GlobalMethods.g_oContentXML.OuterXml, "</mpNodes>")
            GlobalMethods.g_oContentXML.loadXML(Left$(GlobalMethods.g_oContentXML.OuterXml, lPos - 1) & xXML & "</mpNodes>")

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub RemoveTag(lIndex As Integer)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.RemoveTag"

            On Error GoTo ProcError
            m_Col.Remove(lIndex)
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub AddTag(oTag As Tag)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddTag"

            On Error GoTo ProcError
            m_Col.Add(oTag)
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function ItemFromID(ByVal xFullTagID As String, _
                                   Optional ByVal xPart As String = "") As Tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.ItemFromID"
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim xColIndex As String = ""
            Dim oAttribute As xml.xmlAttribute
            Dim oTag As Tag
            Dim xDescription As String = ""

            On Error GoTo ProcError

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'construct query
            xXPath = "//*[@TagID='" & xFullTagID
            If xPart <> "" Then _
                xXPath = xXPath & "' and @Part='" & xPart
            xXPath = xXPath & "']"

            'execute query
            oNode = m_oSummaryXML.selectSingleNode(xXPath)
            If oNode Is Nothing Then
                'tag not found - raise error
                xDescription = "<Error_ItemWithIDNotInCollection>" & xFullTagID
                If xPart <> "" Then _
                    xDescription = xDescription & "; Part " & xPart
                Err.Raise(mpErrors.mpError_MatchingItemNotInCollection, , xDescription)
            End If

            'get collection index
            oAttribute = oNode.attributes.getNamedItem("ColIndex")
            xColIndex = oAttribute.value

            'find in collection
            oTag = ItemFromColIndex(xColIndex)

            'ColIndex property is used only internally and is not always
            'updated following deletions/insertions/moves - update now
            oTag.ColIndex = xColIndex

            'return tag
            ItemFromID = oTag
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Friend Function BuildXMLSummary(Optional ByVal xParentColIndex As String = "") As String
            'returns xml summary of this collection
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.BuildXMLSummary"
            Dim i As Short
            Dim xText As String = ""
            Dim oTag As Tag
            Dim xColIndex As String = ""
            Dim xVars As String = ""

            On Error GoTo ProcError

            'build string
            For i = 1 To m_Col.Count
                oTag = m_Col.Item(i)
                If xParentColIndex <> "" Then
                    xColIndex = xParentColIndex & "." & i
                Else
                    xColIndex = i
                End If
                With oTag
                    'add node
                    xText = xText & "<" & .ElementName & " ColIndex='" & xColIndex & _
                        "' TagID='" & .FullTagID & "'"

                    If .ElementName = "mSEG" Then
                        'add part
                        xText = xText & " Part='" & .Part & "'"

                        'add names of variables defined in mSEG
                        xVars = GetDefinedVariableNames(.ObjectData, .DeletedScopes)
                        xText = xText & " DefinedVariables='" & xVars & "'"
                    End If

                    'add children
                    xText = xText & ">" & .Tags.BuildXMLSummary(xColIndex) & "</" & _
                        .ElementName & ">"
                End With
            Next i

            If xParentColIndex = "" Then _
                xText = "<Tags>" & xText & "</Tags>"

            BuildXMLSummary = xText

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Delete(ByVal oWordXMLNode As Word.XMLNode)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Delete"
            Dim xColIndex As String = ""
            Dim oTag As Tag
            Dim oTags As Tags
            Dim lIndex As Integer
            Dim lPos As Integer

            On Error GoTo ProcError

            oTags = Me

            'get Tag corresponding to specified WordXMLNode
            oTag = GetTag(oWordXMLNode)

            'find in collection
            xColIndex = oTag.ColIndex
            lPos = InStr(xColIndex, ".")
            While lPos <> 0
                lIndex = CLng(Left$(xColIndex, lPos - 1))
                oTag = oTags.Item(lIndex)
                oTags = oTag.Tags
                xColIndex = Mid$(xColIndex, lPos + 1)
                lPos = InStr(xColIndex, ".")
            End While
            lIndex = CLng(xColIndex)

            'delete
            oTags.RemoveTag(lIndex)

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function Insert(ByVal oWordXMLNode As Word.XMLNode, Optional ByVal bNoReindex As Boolean = False) As Tag
            'inserts oWordXMLNode into the tags collection;
            'returns the new Tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Insert"
            Dim xTagID As String = ""
            Dim oDoc As WordDoc
            Dim xElement As String = ""
            Dim oTagID As Word.XMLNode
            Dim oObjectData As Word.XMLNode
            Dim xObjectData As String = ""
            Dim oTag As Tag
            Dim oTags As Tags
            Dim xFullTagID As String = ""
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oAttribute As xml.xmlAttribute
            Dim xColIndex As String = ""
            Dim xValue As String = ""
            Dim xValue2 As String = ""
            Dim bCreateNew As Boolean
            Dim xParentTagID As String = ""
            Dim xParentPart As String = ""
            Dim oDeletedScopes As Word.XMLNode
            Dim oPart As Word.XMLNode
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xExternalParent As String = ""
            Dim xPart As String = ""
            Dim xDeletedScopes As String = ""
            Dim bReindex As Boolean
            Dim xDisplayName As String = ""
            Dim xDisplayName2 As String = ""
            Dim iPropIndex As Short
            Dim bResolvePartConflict As Boolean
            Dim oAuthors As Word.XMLNode
            Dim xAuthors As String = ""
            Dim oParentVariable As Word.XMLNode
            Dim xParentVariable As String = ""
            Dim oProps As Object
            Dim xSegmentName As String = ""
            Dim xPassword As String = ""
            Dim xTag As String = ""
            Dim oDocument As Word.Document
            Dim xParentTagIDRoot As String = ""
            Dim oParentSegmentBmk As Word.Bookmark 'GLOG 6798
            Dim oParentNode As Word.XMLNode 'GLOG 7083

            On Error GoTo ProcError

            oDoc = New WordDoc
            xElement = oWordXMLNode.BaseName

            On Error Resume Next
            '10.2 - Get Tag used for Document Variable maintenance
            xTag = oWordXMLNode.SelectSingleNode("@Reserved").NodeValue
            On Error GoTo ProcError
            oDocument = oWordXMLNode.Range.Document

            'get id of new tag
            oTagID = oWordXMLNode.SelectSingleNode("@TagID")
            If oTagID Is Nothing Then
                'raise error
                Err.Raise(mpErrors.mpError_Generic, , "<Error_InvalidTagInserted>")
            End If
            xTagID = oTagID.NodeValue

            'get object data
            oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
            If Not oObjectData Is Nothing Then
                xObjectData = BaseMethods.Decrypt(oObjectData.NodeValue, xPassword)
            Else
                'raise error
                Err.Raise(mpErrors.mpError_Generic, , "<Error_InvalidTagInserted>")
            End If

            'load xml summary of overall collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'GLOG 7083 (dm) - this function is called at design, as well as at runtime -
            'check for an mSEG xml tag before assuming an mSEG bookmark
            'get physical container
            oParentNode = oWordXMLNode.ParentNode
            Do While (Not oParentNode Is Nothing)
                If oParentNode.BaseName = "mSEG" Then Exit Do
                oParentNode = oParentNode.ParentNode
            Loop

            'GLOG 6798: Use Segment bookmarks
            If oParentNode Is Nothing Then _
                    oParentSegmentBmk = oDoc.GetParentBookmarkFromTag(oWordXMLNode)

            If (xElement = "mDel") And ((Not oParentSegmentBmk Is Nothing) Or _
                    (Not oParentNode Is Nothing)) Then
                'mDel may belong to parent mSEG
                '7/7/10 (dm) - modified segment name comparison code below - with the previous
                'code, "LetterSignatures" and "Letter" were seen as a match, resulting in an
                'mDel belonging to Letter being added to the wrong collection
                oProps = Split(xObjectData, "|")
                xSegmentName = Left$(oProps(0), InStr(oProps(0), "_") - 1)
                If Not oParentSegmentBmk Is Nothing Then
                    xParentTagID = BaseMethods.GetAttributeValue(Mid(oParentSegmentBmk.Name, 2), "TagID", oDocument)
                    xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Do While xParentTagIDRoot <> xSegmentName
                        oParentSegmentBmk = oDoc.GetParentSegmentBookmark(oParentSegmentBmk)
                        If oParentSegmentBmk Is Nothing Then Exit Do
                        xParentTagID = BaseMethods.GetAttributeValue(Mid(oParentSegmentBmk.Name, 2), "TagID", oDocument)
                        xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Loop
                Else
                    'GLOG 7083 (dm) - restored this code for design time
                    xParentTagID = oParentNode.SelectSingleNode("@TagID").NodeValue
                    xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Do While (oParentNode.BaseName <> "mSEG") Or (xParentTagIDRoot <> xSegmentName)
                        oParentNode = oParentNode.ParentNode
                        If oParentNode Is Nothing Then Exit Do
                        xParentTagID = oParentNode.SelectSingleNode("@TagID").NodeValue
                        xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Loop
                End If

                'segment id may have changed when segment was inserted into collection
                If (xParentTagIDRoot = xSegmentName) And (oProps(0) <> xParentTagID) Then
                    'update segment id in object data
                    xObjectData = Replace(xObjectData, oProps(0), xParentTagID)
                    oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
                    oObjectData.NodeValue = xObjectData
                    '10.2
                    BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDocument)
                End If
            End If

            'get target collection
            If xElement = "mSEG" Then
                'get parent from full tag id, since in the case of mSEGs, this
                'may not be the physical parent
                xFullTagID = oDoc.GetFullTagID(oWordXMLNode)
                lPos = InStrRev(xFullTagID, ".")
                If lPos <> 0 Then
                    xParentTagID = Left$(xFullTagID, lPos - 1)
                    oTag = Me.ItemFromID(xParentTagID)
                    oTags = oTag.Tags
                Else
                    oTags = Me
                End If

                'get other attributes
                oDeletedScopes = oWordXMLNode.SelectSingleNode("@DeletedScopes")
                If Not oDeletedScopes Is Nothing Then _
                    xDeletedScopes = BaseMethods.Decrypt(oDeletedScopes.NodeValue)
                oPart = oWordXMLNode.SelectSingleNode("@PartNumber")
                If Not oPart Is Nothing Then _
                    xPart = oPart.NodeValue
                oAuthors = oWordXMLNode.SelectSingleNode("@Authors")
                If Not oAuthors Is Nothing Then _
                    xAuthors = BaseMethods.Decrypt(oAuthors.NodeValue)
                oParentVariable = oWordXMLNode.SelectSingleNode("@AssociatedParentVariable")
                If Not oParentVariable Is Nothing Then
                    'GLOG 4784: this needs to be decrypted
                    xParentVariable = BaseMethods.Decrypt(oParentVariable.NodeValue)
                End If

                'reindex tag id if necessary
                If (InStr(xTagID, "_") = 0) Or _
                        (m_bGUIDIndexing And (InStr(xTagID, "{") = 0)) Or _
                        (Not m_bGUIDIndexing And (InStr(xTagID, "{") <> 0)) Then
                    'the index is either missing or of the inappropriate type
                    bReindex = True
                Else
                    'search for existing tag with same id and part number
                    '            If m_bGUIDIndexing Then
                    'GLOG 3482 (3/19/09 dm) - assign new guid id if this one exists
                    'anywhere in the document
                    'GLOG 3700 (11/30/09 dm) - this should apply in design as well
                    xXPath = "//mSEG[contains(@TagID, '" & xTagID & "') and " & _
                        "substring-after(@TagID, '" & xTagID & "')='' and " & _
                        "@Part='" & xPart & "']"
                    '            Else
                    '                xXPath = "//mSEG[@TagID='" & xFullTagID & "' and " & _
                    '                    "@Part='" & xPart & "']"
                    '            End If
                    oNode = m_oSummaryXML.selectSingleNode(xXPath)
                    If Not oNode Is Nothing Then
                        'duplicate tag exists
                        If Me.FixedTopLevelTagID = xTagID Then
                            'tag id is fixed - modify part number
                            'after tag is added to the collection
                            bResolvePartConflict = True
                        Else
                            'assign new tag id
                            bReindex = True
                        End If
                    End If
                End If

                If bReindex Then
                    '            '5/12/11 (dm) - get deactivation doc var
                    '            Dim xVarValue as string = ""
                    '            On Error Resume Next
                    '            xVarValue = oDocument.Variables("Deactivation_" & xTagID).Value
                    '            On Error GoTo ProcError

                    '10/25/11 (dm) - reindex snapshot vars - store old tag id
                    Dim xOldTagID As String = ""
                    xOldTagID = xTagID

                    ReindexSegment(xTagID, xParentTagID, True)
                    oTagID = oWordXMLNode.SelectSingleNode("@TagID")
                    oTagID.NodeValue = xTagID
                    '10.2
                    BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)

                    '            '5/12/11 (dm) - add new deactivation doc var if appropriate
                    '            If xVarValue <> "" Then _
                    '                oDocument.Variables.Add "Deactivation_" & xTagID, xVarValue

                    '10/25/11 (dm) - add new snapshot doc vars
                    BaseMethods.ReindexSnapshot(oDocument, xOldTagID, xTagID, False)
                End If

                'validate parent tag id in object data
                lPos = InStr(xObjectData, "ParentTagID=")
                If lPos > 0 Then
                    lPos = lPos + 12
                    lPos2 = InStr(lPos, xObjectData, "|")
                    'JTS 8/18/16: There may be no pipe char if at end of Object Data
                    If lPos2 = 0 Then
                        lPos2 = Len(xObjectData) + 1
                    End If

                    xExternalParent = Mid$(xObjectData, lPos, lPos2 - lPos)

                    'check whether the referenced tag is in the collection
                    xXPath = "//mSEG[@TagID='" & xExternalParent & "']"
                    oNode = m_oSummaryXML.selectSingleNode(xXPath)
                    If oNode Is Nothing Then
                        'parent is not in document - update object data
                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                            xExternalParent & "|", "")
                        'JTS 8/18/16: There may be no pipe char if at end of Object Data
                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                            xExternalParent, "")
                        xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                        oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
                        oObjectData.NodeValue = BaseMethods.Encrypt(xObjectData, xPassword)
                        '10.2
                        BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, xPassword, oDocument)
                    End If
                End If
            Else
                'for other elements, physical containment controls
                If Not oParentSegmentBmk Is Nothing Then
                    oTag = GetTag_Bookmark(oParentSegmentBmk)
                    oTags = oTag.Tags
                    xParentTagID = oTag.FullTagID
                    xFullTagID = xParentTagID & "." & xTagID
                    xParentPart = oTag.Part
                ElseIf Not oParentNode Is Nothing Then
                    'GLOG 7083 (dm) - restored this code for design time
                    oTag = GetTag(oParentNode)
                    oTags = oTag.Tags
                    xParentTagID = oTag.FullTagID
                    xFullTagID = xParentTagID & "." & xTagID
                    xParentPart = oTag.Part
                Else
                    oTags = Me
                    xFullTagID = xTagID
                End If

                'GLOG 2622 - if mVar is distributed, same tag name is allowed with different value
                If (xElement = "mVar") And Not bNoReindex Then
                    bNoReindex = InStr(UCase(BaseMethods.GetObjectDataValue(xObjectData, _
                        mpVariableProperties.mpVariableProperty_DistributedDetail, "mVar", xTagID)), "TRUE")
                End If

                'resolve naming conflicts
                If xElement <> "mDel" And Not bNoReindex Then
                    'look for tag id match
                    xXPath = "//" & xElement & "[@TagID='" & xFullTagID & "']"
                    If xElement = "mVar" Then
                        'also look for same-named variable definitions in mSEG
                        xXPath = xXPath & " | //mSEG[(@TagID='" & xParentTagID & "') " & _
                            "and ((substring(@DefinedVariables, 1, " & (Len(xTagID) + 1) & ")='" & xTagID & "|') or " & _
                            "(contains(@DefinedVariables, '|" & xTagID & "|')))]"
                    End If
                    oNode = m_oSummaryXML.selectSingleNode(xXPath)
                    If Not oNode Is Nothing Then
                        'get matching Tag
                        oAttribute = oNode.attributes.getNamedItem("ColIndex")
                        xColIndex = oAttribute.value
                        oTag = ItemFromColIndex(xColIndex)

                        'if two mVars have the same tag id, but different values or object data,
                        'modify the tag id and object data of the new tag to create a new variable -
                        'two blocks should never have the same tag id, so a new block should
                        'always be created - the same is true of an mVar with a tag id that matches
                        'the name of a variable defined in an mSEG belonging to its parent, i.e. a
                        'new variable should be created unconditionally
                        If xElement = "mVar" Then
                            'get value of inserted tag
                            xValue = UCase$(oWordXMLNode.Text)
                            If xValue <> "" Then
                                xValue = Replace(xValue, Chr(7), "Chr7")
                                xValue = Replace(xValue, Chr(11), "Chr11")
                                xValue = Replace(xValue, Chr(12), "Chr12")
                                xValue = Replace(xValue, Chr(13), "Chr13")
                                xValue = Replace(xValue, Chr(21), "Chr21")

                                'GLOG 4476 (dm) - replace field character
                                xValue = Replace(xValue, Chr(19), "Chr19")

                                'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                                xValue = Replace(xValue, Chr(2), "")

                                'GLOG 5978 (dm) - replace non-breaking hyphen
                                xValue = Replace(xValue, Chr(30), "Chr30")

                                'GLOG 6041 (dm) - replace comment characters
                                xValue = Replace(xValue, Chr(5), "Chr5")

                                'GLOG 7417 (dm) - remove graphic
                                xValue = Replace(xValue, Chr(1), "")

                                'Also replace XML Reserved characters
                                xValue = BaseMethods.ReplaceXMLChars(xValue)
                            End If

                            'get display name - if the value of tag is the display name,
                            'this is simply placeholder text and a new variable should not
                            'be created even if the matching tag now has a different value
                            iPropIndex = mpVariableProperties.mpVariableProperty_DisplayName
                            xDisplayName = LMP.Forte.MSWord.BaseMethods.GetObjectDataValue(xObjectData, _
                                iPropIndex, xElement, xTagID)

                            If xValue <> UCase$(xDisplayName) Then
                                'get value of matching tag
                                xValue2 = UCase$(oTag.WordXMLNode.Text)
                                If xValue2 <> "" Then
                                    xValue2 = Replace(xValue2, Chr(7), "Chr7")
                                    xValue2 = Replace(xValue2, Chr(11), "Chr11")
                                    xValue2 = Replace(xValue2, Chr(12), "Chr12")
                                    xValue2 = Replace(xValue2, Chr(13), "Chr13")
                                    xValue2 = Replace(xValue2, Chr(21), "Chr21")

                                    'GLOG 4476 (dm) - replace field character
                                    xValue2 = Replace(xValue2, Chr(19), "Chr19")

                                    'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                                    xValue2 = Replace(xValue2, Chr(2), "")

                                    'GLOG 5978 (dm) - replace non-breaking hyphen
                                    xValue2 = Replace(xValue2, Chr(30), "Chr30")

                                    'GLOG 6041 (dm) - replace comment characters
                                    xValue2 = Replace(xValue2, Chr(5), "Chr5")

                                    'GLOG 7417 (dm) - remove graphic
                                    xValue = Replace(xValue, Chr(1), "")

                                    'Also replace XML Reserved characters
                                    xValue2 = BaseMethods.ReplaceXMLChars(xValue2)
                                End If

                                'create new variable if the values aren't the same
                                bCreateNew = (xValue2 <> xValue)
                            End If

                            If Not bCreateNew Then
                                'create new variable if the object data isn't the same
                                bCreateNew = (oTag.ObjectData <> BaseMethods.RestoreXMLChars(xObjectData))
                            End If
                        Else
                            'block - get display name
                            iPropIndex = mpBlockProperties.mpBlockProperty_DisplayName
                            xDisplayName = BaseMethods.GetObjectDataValue(xObjectData, _
                                iPropIndex, xElement, xTagID)
                        End If

                        If bCreateNew Or (xElement <> "mVar") Then
                            'modify display name if necessary
                            xDisplayName2 = BaseMethods.GetObjectDataValue(oTag.ObjectData, _
                                iPropIndex, xElement, xTagID)
                            If xDisplayName = xDisplayName2 Then
                                If xElement = "mBlock" Then
                                    xDisplayName = GetNewBlockName(xDisplayName)
                                Else
                                    xDisplayName = GetNewName(xDisplayName)
                                End If
                            End If

                            'append/increment final digit until tag id is unique
                            Do
                                'get new tag id/name
                                If xElement = "mBlock" Then
                                    xTagID = GetNewBlockName(xTagID)
                                Else
                                    xTagID = GetNewName(xTagID)
                                End If

                                'ensure that new tag id/name is unique
                                xXPath = "//" & xElement & "[@TagID='" & xParentTagID & "." & _
                                    xTagID & "']"
                                If xElement = "mVar" Then
                                    'also look for same-named variable definitions in mSEG
                                    xXPath = xXPath & " | //mSEG[(@TagID='" & xParentTagID & "') " & _
                                        "and ((substring(@DefinedVariables, 1, " & (Len(xTagID) + 1) & ")='" & xTagID & "|') or " & _
                                        "(contains(@DefinedVariables, '|" & xTagID & "|')))]"
                                End If
                                oNode = m_oSummaryXML.selectSingleNode(xXPath)
                                If oNode Is Nothing Then Exit Do
                            Loop

                            'update tag id in document
                            oTagID = oWordXMLNode.SelectSingleNode("@TagID")
                            oTagID.NodeValue = xTagID
                            '10.2
                            BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)

                            'update object data
                            If xElement = "mVar" Then
                                AssignNewVariableProps(xObjectData, xTagID, xDisplayName)
                            Else
                                AssignNewBlockProps(xObjectData, xTagID, xDisplayName)
                            End If

                            'update object data in document
                            oObjectData = oWordXMLNode.SelectSingleNode("@ObjectData")
                            xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                            oObjectData.NodeValue = BaseMethods.Encrypt(xObjectData, xPassword)
                            '10.2
                            BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, xPassword, oDocument)
                        End If
                    End If
                End If
            End If

            'update full tag id
            If xParentTagID <> "" Then
                xFullTagID = xParentTagID & "." & xTagID
            Else
                xFullTagID = xTagID
            End If

            'create new tag object and(WdUnits.wdSectionits properties
            oTag = New Tag
            With oTag
                .TagID = xTagID
                .ObjectData = xObjectData
                .ElementName = xElement
                .WordXMLNode = oWordXMLNode
                .FullTagID = xFullTagID
                .ParentPart = xParentPart
                .DeletedScopes = xDeletedScopes
                .Part = xPart
                .Authors = xAuthors
                .ParentVariable = xParentVariable
                '10.2
                .Tag = xTag
            End With

            'add to collection
            oTags.AddTag(oTag)

            '10/25/11 - ensure that bounding object type is set
            If oTags.BoundingObjectType = mpBoundingObjectTypes.None Then _
                oTags.BoundingObjectType = mpBoundingObjectTypes.WordXMLNodes

            'resolve part conflict
            If bResolvePartConflict Then _
                oTags.ResolveFixedTopLevelPartConflicts()

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            'set return value
            Insert = oTag

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetDefinedVariableNames(ByVal xObjectData As String, _
                                                 ByVal xDeletedScopes As String) As String
            'returns a pipe-delimited list of the names of the variables defined
            'in xObjectData and xDeletedScopes
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetDefinedVariableNames"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim lPos3 As Integer
            Dim xNames As String = ""

            On Error GoTo ProcError

            'get variables defined in object data
            lPos = InStr(xObjectData, "VariableDefinition=")
            While lPos > 0
                lPos = InStr(lPos, xObjectData, "�") + 1
                lPos2 = InStr(lPos, xObjectData, "�")
                xNames = xNames & Mid$(xObjectData, lPos, lPos2 - lPos) & "|"
                xObjectData = Mid$(xObjectData, lPos2)
                lPos = InStr(xObjectData, "VariableDefinition=")
            End While

            'get expanded scope variables that are currently in deleted state
            lPos = InStr(xDeletedScopes, "DeletedTag=")
            While lPos > 0
                lPos2 = lPos + 11
                lPos3 = InStr(lPos2, xDeletedScopes, "�")
                xNames = xNames & Mid$(xDeletedScopes, lPos2, lPos3 - lPos2) & "|"
                lPos2 = InStr(lPos, xDeletedScopes, "�/w:body>|")
                xDeletedScopes = Left$(xDeletedScopes, lPos - 1) & _
                    Mid$(xDeletedScopes, lPos2 + 10)
                lPos = InStr(xDeletedScopes, "DeletedTag=")
            End While

            GetDefinedVariableNames = xNames

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub AssignNewVariableProps(ByRef xObjectData As String, _
                                           ByVal xName As String, _
                                           ByVal xDisplayName As String)
            'updates object data after a variable is renamed, generating a new variable id
            'and replacing old name, display name, and execution index
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AssignNewVariableProps"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xGUID As String = ""
            Dim iIndex As Short

            On Error GoTo ProcError

            lPos = InStr(xObjectData, "VariableDefinition=")
            If lPos > 0 Then
                lPos = lPos + 19
                lPos2 = BaseMethods.GetInstancePosition(xObjectData, "�", 3)
                xGUID = BaseMethods.GenerateGUID()
                'strip brackets
                xGUID = Mid$(xGUID, 2, Len(xGUID) - 2)
                xObjectData = Left$(xObjectData, lPos - 1) & xGUID & "�" & _
                    xName & "�" & xDisplayName & Mid$(xObjectData, lPos2)

                'increment execution index
                iIndex = CInt(BaseMethods.GetObjectDataValue(xObjectData, _
                    mpVariableProperties.mpVariableProperty_ExecutionIndex, "mVar", xName))
                iIndex = iIndex + 1
                BaseMethods.SetVariableObjectDataValue(xObjectData, xName, _
                    mpVariableProperties.mpVariableProperty_ExecutionIndex, CStr(iIndex))
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub AssignNewBlockProps(ByRef xObjectData As String, _
                                        ByVal xName As String, _
                                        ByVal xDisplayName As String)
            'updates object data after a block is renamed
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AssignNewBlockProps"
            Dim lPos As Integer

            On Error GoTo ProcError

            lPos = BaseMethods.GetInstancePosition(xObjectData, "|", 2)
            xObjectData = xName & "|" & xDisplayName & Mid$(xObjectData, lPos)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetTag(ByVal oWordXMLNode As Word.XMLNode, _
                               Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                               Optional ByVal xTagTempID As String = "", _
                               Optional ByVal xFullTagID As String = "") _
                               As Tag
            'returns the Tag corresponding to the specified Word tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetTag"
            Dim xElement As String = ""
            Dim oPart As Word.XMLNode
            Dim xPart As String = ""
            Dim oDoc As WordDoc
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim iLength As Short
            Dim i As Integer
            Dim lStart As Integer
            Dim xColIndex As String = ""
            Dim oWordXMLNode2 As Word.XMLNode
            Dim oAttribute As xml.xmlAttribute
            Dim oTag As Tag
            Dim lIndex As Integer
            Dim lPos As Integer
            Dim xTemp As String = ""
            Dim oTempID As Word.XMLNode
            Dim xWordTempID As String = ""

            On Error GoTo ProcError

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            If xTagTempID = "" Then
                'no temp id specified - check whether there's one in the TempID
                'attribute of oWordXMLNode
                oTempID = oWordXMLNode.SelectSingleNode("@TempID")
                If Not oTempID Is Nothing Then _
                    xWordTempID = oTempID.NodeValue
            End If

            'construct query
            oDoc = New WordDoc
            If xFullTagID = "" Then _
                xFullTagID = oDoc.GetFullTagID(oWordXMLNode)
            xElement = oWordXMLNode.BaseName
            lStart = oWordXMLNode.Range.Start
            If xElement = "mSEG" Then
                'get part
                oPart = oWordXMLNode.SelectSingleNode("@PartNumber")
                If Not oPart Is Nothing Then _
                    xPart = oPart.NodeValue
                xXPath = "//mSEG[@TagID='" & xFullTagID & "' and @Part='" & xPart & "']"
            ElseIf bAfterDeleteScopeElementChange Then
                'there's been an mVar/mDel swap - look for old element
                If (xElement = "mVar") Or (xElement = "mBlock") Then
                    xXPath = "//mDel[@TagID='" & xFullTagID & "']"
                ElseIf xElement = "mDel" Then
                    xXPath = "//mVar[@TagID='" & xFullTagID & "'] | //mBlock[@TagID='" & _
                        xFullTagID & "']"
                End If
            Else
                xXPath = "//" & xElement & "[@TagID='" & xFullTagID & "']"
            End If

            'execute query
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count

            If iLength = 0 Then
                'tag not found - raise error
                Err.Raise(mpErrors.mpError_MatchingItemNotInCollection, , _
                    "<Error_ItemWithIDNotInCollection>" & xFullTagID)
            End If

            'cycle through matches
            For i = 0 To iLength - 1
                'get collection index
                oNode = oNodeList.Item(i)
                oAttribute = oNode.Attributes.GetNamedItem("ColIndex")
                xColIndex = oAttribute.Value

                'find in collection
                oTag = ItemFromColIndex(xColIndex)

                'validate
                If iLength > 1 Then
                    'there are multiple tags with the same id -
                    'stop cycling when we have the right one
                    If xTagTempID <> "" Then
                        'Tag.TempID provided
                        If oTag.TempID = xTagTempID Then _
                            Exit For
                    ElseIf xWordTempID <> "" Then
                        'compare TempID attribute of Word tag
                        'GLOG 4377: Avoid error if WordDoc.DeleteTags has already deleted
                        'the Word tag associated with this Tag
                        On Error Resume Next
                        oTempID = Nothing
                        oTempID = oTag.WordXMLNode.SelectSingleNode("@TempID")
                        On Error GoTo ProcError
                        If Not oTempID Is Nothing Then
                            If oTempID.NodeValue = xWordTempID Then _
                                Exit For
                        End If
                    ElseIf bAfterDeleteScopeElementChange Then
                        'the original tag is no longer present, and we'll eventually
                        'hit all matching tags, so just return the first match
                        Exit For
                    Else
                        'try identifying by location in doc
                        On Error Resume Next
                        oWordXMLNode2 = oTag.WordXMLNode
                        If Err().Number = 0 Then
                            If oTag.WordXMLNode.Range.Start = lStart Then _
                                Exit For
                        Else
                            'WordXMLNode no longer exists - assume that this
                            'is the tag we're looking for
                            Exit For
                        End If
                        On Error GoTo ProcError
                    End If
                End If
            Next i

            'ColIndex property is used only internally and is not always
            'updated following deletions/insertions/moves - update now
            oTag.ColIndex = xColIndex

            'return tag
            GetTag = oTag

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function ItemFromColIndex(ByVal xColIndex As String) As Tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.ItemFromColIndex"
            Dim oTag As Tag
            Dim oTags As Tags
            Dim lPos As Integer
            Dim lIndex As Integer

            On Error GoTo ProcError

            oTags = Me
            lPos = InStr(xColIndex, ".")
            While lPos <> 0
                lIndex = CLng(Left$(xColIndex, lPos - 1))
                oTag = oTags.Item(lIndex)
                oTags = oTag.Tags
                xColIndex = Mid$(xColIndex, lPos + 1)
                lPos = InStr(xColIndex, ".")
            End While
            lIndex = CLng(xColIndex)
            ItemFromColIndex = oTags.Item(lIndex)

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function Update(ByVal oWordXMLNode As Word.XMLNode, _
                               Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                               Optional ByVal xTagTempID As String = "", _
                               Optional ByVal xFullTagID As String = "")
            'updates selected properties of the Tag holding oWordXMLNode to reflect
            'the real current values of the underlying attributes - this is useful
            'after 1) an mDel is replaced by an mVar (or vice versa) in an expanded
            'delete scope scenario or 2) we "block" the manual deletion of a tag, which
            'actually involves inserting a replacement for the original tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Update"
            Dim oNode As Word.XMLNode
            Dim oTag As Tag

            On Error GoTo ProcError

            'get Tag corresponding to specified WordXMLNode
            oTag = GetTag(oWordXMLNode, bAfterDeleteScopeElementChange, _
                xTagTempID, xFullTagID)

            'update
            With oTag
                'element name
                .ElementName = oWordXMLNode.BaseName

                'object data
                oNode = oWordXMLNode.SelectSingleNode("@ObjectData", "", True)
                If Not oNode Is Nothing Then
                    .ObjectData = BaseMethods.Decrypt(oNode.NodeValue)
                Else
                    .ObjectData = ""
                End If

                '10.2 -(WdUnits.wdSectionTag from Reserved Attribute of XML Node
                oNode = oWordXMLNode.SelectSingleNode("@Reserved", "", True)
                If Not oNode Is Nothing Then
                    .Tag = oNode.NodeValue
                Else
                    .Tag = ""
                End If

                If .ElementName = "mSEG" Then
                    'deleted scopes
                    oNode = oWordXMLNode.SelectSingleNode("@DeletedScopes", "", True)
                    If Not oNode Is Nothing Then
                        If oNode.NodeValue <> "" Then
                            .DeletedScopes = BaseMethods.Decrypt(oNode.NodeValue)
                        Else
                            .DeletedScopes = ""
                        End If
                    End If

                    'authors
                    oNode = oWordXMLNode.SelectSingleNode("@Authors", "", True)
                    If Not oNode Is Nothing Then
                        If oNode.NodeValue <> "" Then
                            .Authors = BaseMethods.Decrypt(oNode.NodeValue)
                        Else
                            .Authors = ""
                        End If
                    End If
                End If

                'XMLNode
                .WordXMLNode = oWordXMLNode
            End With

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function MoveExternalChildren(ByVal oWordXMLNode As Word.XMLNode) As Integer
            'moves any external children contained in the Tag corresponding to
            'oWordXMLNode to the Tag of a different part of the same segment or,
            'if there are no other parts, to the top level of the collection - this
            'method is called before an mSEG is deleted - returns 1 if any children
            'have been moved, else 0
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.MoveExternalChildren"
            Dim oSource As Tag
            Dim oSourceTags As Tags
            Dim oDest As Tag
            Dim oDestTags As Tags
            Dim i As Integer
            Dim oChild As Tag
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xParentTagID As String = ""
            Dim xXPath As String = ""
            Dim oNodeList As xml.xmlNodeList
            Dim iLength As Short
            Dim oNode As xml.xmlNode
            Dim oAttribute As xml.xmlAttribute
            Dim oTagID As Word.XMLNode
            Dim xTagID As String = ""
            Dim xObjectData As String = ""
            Dim bValidateTagID As Boolean

            On Error GoTo ProcError

            'only segments can have external children
            If oWordXMLNode.BaseName <> "mSEG" Then _
                Exit Function

            'get Tag corresponding to specified WordXMLNode
            oSource = GetTag(oWordXMLNode)

            'check whether segment has external children - if not, exit
            xObjectData = oSource.ObjectData
            lPos = InStr(xObjectData, "ExternalChildren=")
            If lPos > 0 Then
                lPos = lPos + 17
                lPos2 = InStr(lPos, xObjectData, "|")
                If UCase(Mid$(xObjectData, lPos, lPos2 - lPos)) = "FALSE" Then
                    Exit Function
                End If
            Else
                Exit Function
            End If

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'check whether there's another part available
            xTagID = oWordXMLNode.SelectSingleNode("@TagID").NodeValue
            xXPath = "//mSEG[@TagID='" & xTagID & "']"
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count
            If iLength > 1 Then
                'get Tag corresponding to the other part
                For i = 0 To iLength - 1
                    oNode = oNodeList.Item(i)
                    oAttribute = oNode.Attributes.GetNamedItem("Part")
                    If oAttribute.Value <> oSource.Part Then
                        'this is a different part - find it in collection
                        oAttribute = oNode.Attributes.GetNamedItem("ColIndex")
                        oDest = ItemFromColIndex(oAttribute.Value)
                        'set target tags collection
                        oDestTags = oDest.Tags
                        Exit For
                    End If
                Next i
            End If

            'no other parts found,(WdUnits.wdSectiontarget collection to top level
            If oDestTags Is Nothing Then
                oDestTags = Me
                bValidateTagID = True
            End If

            'move external children from oSource to oDest
            oSourceTags = oSource.Tags
            For i = oSourceTags.Count To 1 Step -1
                oChild = oSourceTags.Item(i)
                If oChild.ElementName = "mSEG" Then
                    'if oChild is external, it will have a parent tag id
                    xObjectData = oChild.ObjectData
                    lPos = InStr(xObjectData, "ParentTagID=")
                    If lPos > 0 Then
                        lPos = lPos + 12
                        lPos2 = InStr(lPos, xObjectData, "|")
                        'JTS 8/18/16: There may be no pipe char if at end of Object Data
                        If lPos2 = 0 Then
                            lPos2 = Len(xObjectData) + 1
                        End If

                        xParentTagID = Mid$(xObjectData, lPos, lPos2 - lPos)

                        'if this is an external child of this segment, move
                        'it to the other part
                        If xParentTagID = oSource.TagID Then
                            'add to oDest
                            oDestTags.AddFamilyMember(oChild, bValidateTagID)
                            'remove from oSource
                            oSourceTags.RemoveTag(i)
                            'set function return value
                            MoveExternalChildren = 1
                        End If
                    End If
                End If
            Next i

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetNewName(ByVal xName As String) As String
            'returns modified version of conflicting name
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetNewName"
            Dim xChar As String = ""
            Dim xNew As String = ""

            On Error GoTo ProcError

            xChar = Right$(xName, 1)

            If IsNumeric(xChar) Then
                'last character is numeric - increment
                xNew = Left$(xName, Len(xName) - 1) & (CInt(xChar) + 1)
            Else
                'append "1"
                xNew = xName & "1"
            End If

            GetNewName = xNew

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Private Function GetNewBlockName(ByVal xName As String) As String
            'returns modified version of conflicting name
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetNewBlockName"
            Dim xChar As String = ""
            Dim xChar2 As String = ""
            Dim xNew As String = ""

            On Error GoTo ProcError

            xChar = Right$(xName, 1)
            If Len(xName) > 1 Then
                xChar = Mid$(xName, Len(xName) - 2, 1)
            End If
            If IsNumeric(xChar) And xChar = "_" Then
                'last character is numeric - increment
                xNew = Left$(xName, Len(xName) - 1) & (CInt(xChar) + 1)
            Else
                'append "_1"
                xNew = xName & "_1"
            End If

            GetNewBlockName = xNew

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub MoveWordTag(ByRef oWordTag As Word.XMLNode, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.mpBase.MoveWordTag"

            'move tag
            LMP.Forte.MSWord.BaseMethods.MoveWordTag(oWordTag, oNewRange)

            'update tag in collection
            Me.Update(oWordTag)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function AddTempID(ByVal oWordTag As Word.XMLNode) As String
            'adds a temporary GUID to Word tag and corresponding Tag -
            'returns GUID
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddTempID"
            Dim xTempID As String = ""
            Dim oTempID As Word.XMLNode
            Dim oTag As Tag
            Dim oReserved As Word.XMLNode

            On Error GoTo ProcError

            'generate GUID
            xTempID = LMP.Forte.MSWord.BaseMethods.GenerateGUID()

            'add temp id to Word tag
            oTempID = oWordTag.SelectSingleNode("@TempID")
            If oTempID Is Nothing Then _
                    oTempID = oWordTag.Attributes.Add("TempID", "")
            oTempID.NodeValue = xTempID

            '10.2 - Update Doc Variable also
            oReserved = oWordTag.SelectSingleNode("@Reserved")
            If Not oReserved Is Nothing Then
                LMP.Forte.MSWord.BaseMethods.SetAttributeValue(oReserved.NodeValue, "TempID", xTempID, "", oWordTag.Range.Document)
            End If

            'get Tag
            oTag = GetTag(oWordTag)

            'add temp id to Tag
            oTag.TempID = xTempID

            'return temp id
            AddTempID = xTempID

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Friend Sub Add_CC(oContentControls As Word.ContentControls, _
                          Optional ByVal bIsNew As Boolean = False, _
                          Optional ByRef bRestartRefresh As Boolean = False, _
                          Optional ByVal xExclusions As String = "")
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Add_CC"
            Dim i As Short
            Dim oContentControl As Word.ContentControl
            Dim oHost As Word.ContentControl
            Dim bAdd As Boolean
            Dim oWordDoc As WordDoc
            Dim oParent As Word.ContentControl
            Dim oParentTag As LMP.Forte.MSWord.Tag
            Dim bContainedInBmk As Boolean

            On Error GoTo ProcError
            oWordDoc = New WordDoc

            For i = 1 To oContentControls.Count
                bAdd = False
                oContentControl = oContentControls(i)

                bContainedInBmk = oWordDoc.ContentControlIsContainedByBookmark(oContentControl)

                If Not bContainedInBmk Then
                    oParentTag = Me.Parent

                    If oParentTag Is Nothing Then
                        'only add top-level nodes
                        oParent = oContentControl.ParentContentControl
                        While Not oParent Is Nothing
                            If LMP.Forte.MSWord.BaseMethods.GetBaseName(oParent) = "mSEG" Then
                                oHost = oParent
                                oParent = Nothing
                            Else
                                oParent = oParent.ParentContentControl
                            End If
                        End While
                        If oHost Is Nothing Then
                            'no parent mSEG
                            bAdd = True
                        ElseIf Not oWordDoc.ControlIsAvailableToCurrentClient(oHost) Then
                            'parent mSEG doesn't belong to this client
                            bAdd = True
                        End If

                        'GLOG 4943 (dm)
                        oHost = Nothing
                    Else
                        bAdd = True
                    End If
                Else
                    bAdd = False
                End If

                If bAdd Then
                    AddNode_CC(oContentControl, bIsNew, bRestartRefresh, xExclusions)

                    'GLOG 3641 (dm) - added bRestartRefresh parameter in case in process
                    'adjustments necessitate restarting the refresh process from the beginning
                    If bRestartRefresh Then _
                        Exit Sub
                End If
            Next i
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub AddNode_CC(oContentControl As Word.ContentControl, ByVal bIsNew As Boolean, _
                ByRef bRestartRefresh As Boolean, Optional ByVal xExclusions As String = "", Optional bIncludeChildren As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddNode_CC"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xDestination As String = ""
            Dim xText As String = ""
            Dim oPartnerNode As xml.xmlNode
            Dim oParentNode As xml.xmlNode
            Dim oConflictingNode As xml.xmlNode
            Dim xOldID As String = ""
            Dim xTagID As String = ""
            Dim xXPath As String = ""
            Dim xParentID As String = ""
            Dim xFamilyParentID As String = ""
            Dim bMove As Boolean
            Dim xValue As String = ""
            Dim oTag As Tag
            Dim oAttribute As xml.xmlAttribute
            Dim oDocument As Word.Document
            Dim i As Short
            Dim xObjectData As String = ""
            Dim xAuthorsDetail As String = ""
            Dim xPart As String = ""
            Dim xParentFullID As String = ""
            Dim xDefinedVars As String = ""
            Dim oDefinedVars As Object
            Dim xName As String = ""
            Dim oTags As Tags
            Dim xDeletedScopes As String = ""
            Dim xElementName As String = ""
            Dim xColIndex As String = ""
            Dim xDisplayName As String = ""
            Dim iPropIndex As Short
            Dim xParentVariable As String = ""
            Dim xSegmentID As String = ""
            Dim oProps As Object
            Dim xTagPrefixID As String = ""
            Dim xPassword As String = ""
            Dim oWordDoc As WordDoc
            Dim xTag As String = ""
            Dim oChildCC As Word.ContentControl
            Dim xDocVar As String = ""
            Dim oDeleteScope As DeleteScope 'GLOG 6381 (dm)
            Dim iDeleted As mpInvalidTagsRemovedTypes 'GLOG 6524 (dm)
            Dim oRevision As Word.Revision 'GLOG 7355 (dm)
            Dim oRange As Word.Range 'GLOG 7355 (dm)

            On Error GoTo ProcError

            oWordDoc = New WordDoc
            oDocument = oContentControl.Parent
            xTag = oContentControl.Tag

            '4/18/11 (dm) - exit if content control is specified for exclusion -
            'we added this to keep deleted content controls out of the collection
            'GLOG 7355 (dm) - we no longer check for revisions in advance -
            'we now check each content control before adding it - if the cc is marked
            'for deletion, this revision will be first in the cc's collection
            '    If InStr(xExclusions, "|" & xTag & "|") Then _
            '        Exit Sub

            oRange = oContentControl.Range

            'GLOG : 7595 : CEH
            If oWordDoc.RangeHasRevisions(oRange) Then
                oRevision = oRange.Revisions(1)
                If (oRevision.Type = Word.WdRevisionType.wdRevisionDelete) Or _
                        (oRevision.Type = Word.WdRevisionType.wdRevisionCellDeletion) Then
                    If (oRevision.Range.Start < oRange.Start) And _
                            (oRevision.Range.End > oRange.End) Then
                        Exit Sub
                    End If
                End If
            End If

            xElementName = LMP.Forte.MSWord.BaseMethods.GetBaseName(oContentControl)

            'we need to explicitly exclude mSubVars because new data structure
            'does not distinguish between the tag id and name attributes
            If (xElementName = "mSubVar") Or (xElementName = "") Then
                Exit Sub
            ElseIf xElementName = "mSEG" And SegmentTagExists(xTag) Then
                'GLOG 7363: If CC with same tag has already been added, retag to generate unique tag
                BaseMethods.RetagContentControl(oContentControl)
                xTag = oContentControl.Tag
            End If

            'GLOG 6524 (dm): remove content controls without valid object data -
            'set value of InvalidTagsDeleted so Front-end can determine what
            'was removed during Refresh
            iDeleted = Me.DeleteInvalidContentControl(oContentControl)
            If iDeleted > Me.InvalidTagsDeleted Then
                Me.InvalidTagsDeleted = iDeleted
            End If
            If iDeleted > 0 Then
                bRestartRefresh = True
                Exit Sub
            End If

            'add tag to node store only if it contains a TagID attribute
            xTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "TagID", oDocument)

            '10/21/10 (dm) - the remmed out code is now internal to GetAttributeValue()
            '    '10/8/10 (dm) - the associated doc var may be missing because the user
            '    'used Undo to reverse the deletion of this content control - we need to
            '    'restore it here because we disable the AfterAdd event on undo
            '    If xTagID = "" Then
            '        xDocVar = "mpo" & Mid$(xTag, 4, 8)
            '        On Error Resume Next
            '        oDocument.Variables.Add xDocVar, g_oDocVars(xDocVar)
            '        On Error GoTo ProcError
            '        xTagID = LMP.Forte.MSWord.mpBase.GetAttributeValue(xTag, "TagID", oDocument)
            '    End If

            If xTagID <> "" Then
                m_iColIndex = m_iColIndex + 1
                If Me.Parent Is Nothing Then
                    xColIndex = m_iColIndex
                Else
                    xColIndex = Me.Parent.ColIndex & "." & m_iColIndex
                    xParentID = Me.Parent.TagID
                    xParentFullID = Me.Parent.FullTagID
                End If

                xOldID = xTagID

                If xElementName = "mSEG" Then
                    'determine whether node is newly inserted
                    bIsNew = (m_bGUIDIndexing And (InStr(xTagID, "{") = 0))
                    If bIsNew Then
                        'use current client's password
                        xPassword = GlobalMethods.g_xEncryptionPassword
                    Else
                        'GLOG 3641 (dm) - adjust collection table if necessary
                        oWordDoc.AdjustCollectionTableIfNecessary_CC(oContentControl, bRestartRefresh)
                        If bRestartRefresh Then _
                            Exit Sub
                    End If

                    'get author detail
                    xAuthorsDetail = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "Authors", oDocument)
                    xAuthorsDetail = BaseMethods.ReplaceXMLChars(xAuthorsDetail)

                    'get parent variable
                    xParentVariable = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "AssociatedParentVariable", _
                        oDocument)

                    'get deleted scopes
                    xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDocument)

                    'get part number
                    xPart = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "PartNumber", oDocument)
                    If xPart = "" Then _
                        xPart = 1
                End If

                'get object data
                xObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "ObjectData", oDocument)
                If xObjectData <> "" Then
                    xObjectData = BaseMethods.ReplaceXMLChars(xObjectData)
                    If xElementName = "mSEG" Then
                        'get external parent
                        lPos = InStr(xObjectData, "ParentTagID=")
                        If lPos > 0 Then
                            lPos = lPos + 12
                            lPos2 = InStr(lPos, xObjectData, "|")
                            'JTS 8/18/16: There may be no pipe char if at end of Object Data
                            If lPos2 = 0 Then
                                lPos2 = Len(xObjectData) + 1
                            End If
                            If Not LMP.Forte.MSWord.BaseMethods.IsCollectionTableItem(xObjectData) Then
                                xFamilyParentID = Mid$(xObjectData, lPos, lPos2 - lPos)
                                '                    If xFamilyParentID <> "" Then _
                                '                        xOldID = xFamilyParentID & "." & xOldID
                            Else
                                'GLOG 3482 (3/20/09 dm) - parent tag id was inadvertently
                                'getting added to collection table items, which are never
                                'external children - clean up now
                                xObjectData = Left$(xObjectData, lPos - 13) & _
                                    Mid$(xObjectData, lPos2 + 1)
                            End If
                        End If
                    ElseIf xElementName = "mDel" Then
                        'get segment
                        oProps = Split(xObjectData, "|")
                        xSegmentID = oProps(0)
                    End If

                    'get/add tag prefix id -
                    'tag prefix id should be added at runtime only to newly inserted mSEGs
                    'and their child mVars and mBlocks
                    If xElementName <> "mDel" Then
                        xTagPrefixID = LMP.Forte.MSWord.BaseMethods.GetTagPrefixID(xObjectData)

                        'added 2/12/09 (dm) to deal with client tag prefix ids that
                        'inadvertently got into the database - strip existing id if
                        'newly inserted or in design
                        If (xTagPrefixID <> "") And (bIsNew Or Not m_bGUIDIndexing) Then
                            lPos = InStr(xObjectData, GlobalMethods.mpTagPrefixIDSeparator)
                            If lPos > 0 Then _
                                xObjectData = Mid$(xObjectData, lPos + Len(GlobalMethods.mpTagPrefixIDSeparator))
                            xTagPrefixID = ""
                        End If

                        If bIsNew And (xTagPrefixID = "") And (GlobalMethods.g_xClientTagPrefixID <> "") Then
                            xObjectData = GlobalMethods.g_xClientTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & xObjectData
                        ElseIf (xTagPrefixID <> "") And (xTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (GlobalMethods.g_xClientTagPrefixID <> "") And (GlobalMethods.g_xClientTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (xTagPrefixID <> GlobalMethods.g_xClientTagPrefixID) Then
                            'tag does not belong to the current client - do not add node -
                            'this is temporarary - we hope to ultimately offer sharing of mp10
                            'functionality between clients - when we do, we'll add these tags to
                            'the tags collection and use the existing BelongsToCurrentClient
                            'property of segments, variables, and blocks to determine whether we
                            'can get field code values from the current db - exception for trailers
                            If (xElementName <> "mSEG") Or _
                                    (InStr(xObjectData, "ObjectTypeID=401|") = 0) Then
                                'adjust index
                                m_iColIndex = m_iColIndex - 1

                                'contained nodes may belong to the client
                                For Each oChildCC In oContentControl.Range.ContentControls
                                    If oChildCC.Tag <> xTag Then
                                        'only add this control's immediate children
                                        If oChildCC.ParentContentControl.Tag = xTag Then
                                            AddNode_CC(oChildCC, bIsNew, bRestartRefresh, _
                                                xExclusions)
                                        End If
                                    End If
                                Next oChildCC

                                Exit Sub
                            End If
                        End If
                    End If
                End If

                'modify attributes and/or flag to move if necessary
                If xElementName = "mSEG" Then
                    If xOldID <> Me.FixedTopLevelTagID Then
                        'check whether there's another tag with same id and part #
                        xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                        oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                        If oConflictingNode Is Nothing Then
                            'no conflict - check whether we've already found another
                            'part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                        ElseIf m_bGUIDIndexing Then
                            'GLOG 3003: there is a conflict - increment PartNumber until unused Part is found
                            'Use current conflicting node for mSEG attributes
                            oPartnerNode = oConflictingNode
                            Do
                                xPart = Trim(Str(Val(xPart) + 1))
                                xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                                oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            Loop Until oConflictingNode Is Nothing
                        Else
                            'there is a conflict at design time - check whether we've already found
                            'and reindexed another part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "' and @TagID!='" & xOldID & _
                                "' and @Part!='" & xPart & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            'if no partner node found, reindex
                            If oPartnerNode Is Nothing Then
                                ReindexSegment(xTagID, xParentFullID, False)

                                'GLOG 6381 (dm) - retag deleted scopes
                                oDeleteScope = New DeleteScope
                                oDeleteScope.RetagDeletedScopes(xTag, oDocument)
                                xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, _
                                    "DeletedScopes", oDocument)
                            End If
                        End If

                        If Not oPartnerNode Is Nothing Then
                            'another part found
                            With oPartnerNode.attributes
                                'assign the same new id
                                oAttribute = .getNamedItem("TagID")
                                xTagID = oAttribute.value

                                'if not in same collection as other part, flag to move there
                                oAttribute = .getNamedItem("ColIndex")
                                lPos = InStrRev(oAttribute.value, ".")
                                If (lPos = 0) And (Not Me.Parent Is Nothing) Then
                                    'destination is top level
                                    xDestination = "0"
                                ElseIf lPos > 0 Then
                                    'get destination
                                    xDestination = Left$(oAttribute.value, lPos - 1)
                                    'clear flag if already in same collection
                                    If Not Me.Parent Is Nothing Then
                                        If Me.Parent.ColIndex = xDestination Then _
                                            xDestination = ""
                                    End If
                                End If
                            End With
                        Else
                            'reindex if necessary
                            If (InStr(xTagID, "_") = 0) Or _
                                    (m_bGUIDIndexing And (InStr(xTagID, "{") = 0)) Or _
                                    (Not m_bGUIDIndexing And (InStr(xTagID, "{") <> 0)) Then
                                'item is newly inserted - reindex
                                ReindexSegment(xTagID, xParentFullID, False)

                                'GLOG 6381 (dm) - retag deleted scopes
                                oDeleteScope = New DeleteScope
                                oDeleteScope.RetagDeletedScopes(xTag, oDocument)
                                xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, _
                                    "DeletedScopes", oDocument)

                                'update NewSegment property
                                If Me.Parent Is Nothing Then _
                                    m_xNewSegment = xTagID
                            End If
                        End If

                        'deal with external children
                        If xFamilyParentID <> "" Then
                            'look for parent
                            xXPath = "//mSEG[@OldID='" & xFamilyParentID & "']"
                            oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            If Not oParentNode Is Nothing Then
                                'we've found a tag for the parent- update ParentTagID in ObjectData
                                oAttribute = oParentNode.attributes.getNamedItem("TagID")
                                If xFamilyParentID <> oAttribute.value Then
                                    xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                        xFamilyParentID & "|", "ParentTagID=" & oAttribute.value & "|")
                                    'JTS 8/18/16: There may be no pipe char if at end of Object Data
                                    xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                        xFamilyParentID, "ParentTagID=" & oAttribute.Value)
                                    xFamilyParentID = oAttribute.Value
                                End If

                                'check whether child is physically contained within parent -
                                'if not, we'll later need to move it
                                If Me.Parent Is Nothing Then
                                    bMove = True
                                ElseIf xParentID <> xFamilyParentID Then
                                    bMove = True
                                End If
                                If bMove Then
                                    oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                                    xDestination = oAttribute.value
                                End If
                            Else
                                'parent is not in document - update object data
                                'GLOG 5950 (dm) - don't clear ParentTagID immediately -
                                'when some shapes are Word 2010 shapes, and others are
                                'textboxes, the nodes may not load in the expected order -
                                'truthfully, we've just been lucky to date that parents in
                                'textboxes (e.g. pleading paper) have loaded before their
                                'external children in textboxes (e.g. sidebar) - this
                                'is now handled in UniteFamilies()
                                '                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                '                            xFamilyParentID & "|", "")
                                '                        xFamilyParentID = ""
                                xDestination = "Pending" & xFamilyParentID
                            End If
                        End If
                    End If

                    'resolve naming conflicts in variable definitions stored in this mSEG -
                    '1/8/09 (Doug) - just get variable names - I remmed out the conflict
                    'resolution code because 1) RenameItemIfNecessary wasn't working properly
                    'for tagless variables and 2) it didn't seem worth fixing because
                    'I can't imagine how such a conflict between two tagless variables would occur
                    xDefinedVars = GetDefinedVariableNames(xObjectData, xDeletedScopes)
                    '            If xDefinedVars <> "" Then
                    '                oDefinedVars = Split(xDefinedVars, "|")
                    '                For i = 0 To UBound(oDefinedVars) - 1
                    '                    xName = oDefinedVars(i)
                    '                    RenameItemIfNecessary xName, xObjectData, xTagID, _
                    '                        xParentFullID, "mSEG", ""
                    '                Next i
                    '            End If
                ElseIf xElementName = "mDel" Then
                    'ensure that mDel becomes child of its owning segment -
                    'GLOG 3702 (dm) - in design mode, mDels of secondary collection table items
                    'were getting assigned to the primary item when secondary item was the
                    'same segment - this is because the primary item has the same old id -
                    'start by looking for a match for both the old and new ids
                    xXPath = "//mSEG[@OldID='" & xSegmentID & "' and @TagID='" & xParentID & "']"
                    oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    If oParentNode Is Nothing Then
                        'search for just the old id
                        xXPath = "//mSEG[@OldID='" & xSegmentID & "']"
                        oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    End If
                    If Not oParentNode Is Nothing Then
                        'we've found a tag for the parent- update ObjectData if necessary
                        oAttribute = oParentNode.attributes.getNamedItem("TagID")
                        If xSegmentID <> oAttribute.value Then
                            lPos = InStr(xObjectData, "|")
                            xObjectData = oAttribute.value & Mid$(xObjectData, lPos)
                            xSegmentID = oAttribute.value
                        End If

                        'check whether mDel's immediate parent is owning mSEG -
                        'if not, we'll later need to move it
                        If Me.Parent Is Nothing Then
                            bMove = True
                        ElseIf xParentID <> xSegmentID Then
                            bMove = True
                        End If
                        If bMove Then
                            oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                            xDestination = oAttribute.value
                        End If
                    Else
                        'don't add mDel if parent mSEG isn't in collection
                        m_iColIndex = m_iColIndex - 1
                        Exit Sub
                    End If
                Else
                    'get variable value; this will be added to our content xml, so replace any
                    'character that can't be used "as is" in xml character data
                    If xElementName = "mVar" Then
                        xValue = UCase$(oContentControl.Range.Text)
                        xValue = Replace(xValue, Chr(7), "Chr7")
                        xValue = Replace(xValue, Chr(11), "Chr11")
                        xValue = Replace(xValue, Chr(12), "Chr12")
                        xValue = Replace(xValue, Chr(13), "Chr13")
                        xValue = Replace(xValue, Chr(21), "Chr21")

                        'GLOG 4476 (dm) - replace field character
                        xValue = Replace(xValue, Chr(19), "Chr19")

                        'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                        xValue = Replace(xValue, Chr(2), "")

                        'GLOG 5978 (dm) - replace non-breaking hyphen
                        xValue = Replace(xValue, Chr(30), "Chr30")

                        'GLOG 6041 (dm) - replace comment characters
                        xValue = Replace(xValue, Chr(5), "Chr5")

                        'GLOG 7417 (dm) - remove graphic
                        xValue = Replace(xValue, Chr(1), "")

                        'Also replace XML Reserved characters
                        xValue = BaseMethods.ReplaceXMLChars(xValue)
                    End If

                    'resolve naming conflicts in variables and blocks
                    RenameItemIfNecessary(xTagID, xObjectData, xTagID, xParentFullID, _
                        xElementName, xValue)
                End If

                'add to GlobalMethods.g_oContentXML
                lPos = InStr(GlobalMethods.g_oContentXML.OuterXml, "</mpNodes>")
                If xElementName = "mSEG" Then
                    'mSEG - include defined variables
                    xText = "<mSEG TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' DefinedVariables='" & xDefinedVars & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' Part='" & xPart & "' Authors='" & xAuthorsDetail & "' AssociatedParentVariable='" & xParentVariable & "' />"
                Else
                    'include value
                    xText = "<" & xElementName & " TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' Value='" & xValue & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' />"
                End If

                GlobalMethods.g_oContentXML.loadXML(Left$(GlobalMethods.g_oContentXML.OuterXml, lPos - 1) & xText & "</mpNodes>")

                'update doc
                LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)
                If xObjectData <> "" Then
                    xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, xPassword, oDocument)
                End If

                'GLOG 3003: Update PartNumber attribute in case changed
                If xPart <> "" Then
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "PartNumber", xPart, xPassword, oDocument)
                End If

                If xDeletedScopes <> "" Then
                    'this is simply to encrypt or unencrypt doc vars if necessary
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedScopes, _
                        xPassword, oDocument)
                End If

                '10/26/11 (dm) - reindex snapshots if necessary - this comes into play
                'when using the Copy Segment/Paste Segment functionality
                If bIsNew Then _
                    LMP.Forte.MSWord.BaseMethods.ReindexSnapshot(oDocument, xOldID, xTagID, True)

                'create new tag and(WdUnits.wdSectionits properties
                oTag = New LMP.Forte.MSWord.Tag
                With oTag
                    .ElementName = xElementName
                    .TagID = xTagID
                    .ObjectData = xObjectData
                    .Part = xPart
                    .DeletedScopes = xDeletedScopes
                    .Authors = xAuthorsDetail
                    .ParentVariable = xParentVariable

                    .ContentControl = oContentControl
                    If Me.Parent Is Nothing Then
                        .FullTagID = xTagID
                    Else
                        .FullTagID = xParentFullID & "." & xTagID
                        .ParentPart = Me.Parent.Part
                    End If
                    .ColIndex = xColIndex
                    .Tag = oContentControl.Tag
                    With .Tags
                        .Parent = oTag
                        .FixedTopLevelTagID = Me.FixedTopLevelTagID
                        .GUIDIndexing = Me.GUIDIndexing
                        .BoundingObjectType = mpBoundingObjectTypes.ContentControls
                    End With
                End With

                'add tag to collection
                m_Col.Add(oTag)
                'GLOG 7363: Keep track of segment tags to avoid duplicates
                If xElementName = "mSEG" Then
                    m_ColSegBmks.Add(xTag)
                End If
                'GLOG 7022: If Segment Bounding Objects are bookmarks,
                'don't process child XMLNodes, as they will already
                'have been added by AddNode_Bookmark for the Parent.
                If bIncludeChildren Then
                    'add contained nodes
                    For Each oChildCC In oContentControl.Range.ContentControls
                        If oChildCC.Tag <> xTag Then
                            'JTS 4/16/10 If start of ContentControl Range coincides with start of Parent Range
                            'ContentControls collection will contain both - skip top level CC if found
                            If Not oChildCC.ParentContentControl Is Nothing Then
                                'only add this control's immediate children
                                If oChildCC.ParentContentControl.Tag = xTag Then
                                    If xElementName = "mSEG" Then
                                        'add to this tag's collection
                                        oTag.Tags.AddNode_CC(oChildCC, bIsNew, bRestartRefresh, _
                                            xExclusions)
                                    Else
                                        'add to this tag's parent's collection
                                        AddNode_CC(oChildCC, bIsNew, bRestartRefresh, _
                                            xExclusions)
                                    End If
                                ElseIf (xElementName = "mVar") And (BaseMethods.GetBaseName(oChildCC) = "mDel") Then
                                    'look for mDels contained in child mSubVars - if found,
                                    'add to this tag's parent's collection
                                    AddNode_CC(oChildCC, bIsNew, bRestartRefresh, xExclusions)
                                End If
                            End If
                        End If
                    Next oChildCC
                End If
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetTag_CC(ByVal oCC As Word.ContentControl, _
                                  Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                                  Optional ByVal xTagTempID As String = "", _
                                  Optional ByVal xFullTagID As String = "") As Tag
            'returns the Tag corresponding to the specified Word tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetTag_CC"
            Dim xElement As String = ""
            Dim xPart As String = ""
            Dim oDoc As WordDoc
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim iLength As Short
            Dim i As Integer
            Dim lStart As Integer
            Dim xColIndex As String = ""
            Dim oCC2 As Word.ContentControl
            Dim oAttribute As xml.xmlAttribute
            Dim oTag As Tag
            Dim lIndex As Integer
            Dim lPos As Integer
            Dim xTemp As String = ""
            Dim xWordTempID As String = ""
            Dim oDocument As Word.Document
            Dim xTag As String = ""
            Dim xTempID As String = ""

            On Error GoTo ProcError

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            oDocument = oCC.Range.Document
            xTag = oCC.Tag

            If xTagTempID = "" Then
                'no temp id specified - check whether there's one in the TempID attribute
                xWordTempID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "TempID", oDocument)
            End If
            '    erring on letter insertion, and letterhead textboxes need to have
            '    two trailing paragraphs for include/exclude to work properly
            'construct query
            oDoc = New WordDoc

            If xFullTagID = "" Then
                xFullTagID = oDoc.GetFullTagID_CC(oCC)
            End If

            xElement = LMP.Forte.MSWord.BaseMethods.GetBaseName(oCC)
            lStart = oCC.Range.Start

            If xElement = "mSEG" Then
                'get part
                xPart = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "PartNumber", oDocument)
                xXPath = "//mSEG[@TagID='" & xFullTagID & "' and @Part='" & xPart & "']"
            ElseIf bAfterDeleteScopeElementChange Then
                'there's been an mVar/mDel swap - look for old element
                If (xElement = "mVar") Or (xElement = "mBlock") Then
                    xXPath = "//mDel[@TagID='" & xFullTagID & "']"
                ElseIf xElement = "mDel" Then
                    xXPath = "//mVar[@TagID='" & xFullTagID & "'] | //mBlock[@TagID='" & _
                        xFullTagID & "']"
                End If
            Else
                xXPath = "//" & xElement & "[@TagID='" & xFullTagID & "']"
            End If

            'execute query
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count

            If iLength = 0 Then
                'tag not found - raise error
                Err.Raise(mpErrors.mpError_MatchingItemNotInCollection, , _
                    "<Error_ItemWithIDNotInCollection>" & xFullTagID)
            End If

            'cycle through matches
            For i = 0 To iLength - 1
                'get collection index
                oNode = oNodeList.Item(i)
                oAttribute = oNode.Attributes.GetNamedItem("ColIndex")
                xColIndex = oAttribute.Value

                'find in collection
                oTag = ItemFromColIndex(xColIndex)

                'validate
                If iLength > 1 Then
                    'there are multiple tags with the same id -
                    'stop cycling when we have the right one
                    If xTagTempID <> "" Then
                        'Tag.TempID provided
                        If oTag.TempID = xTagTempID Then _
                            Exit For
                    ElseIf xWordTempID <> "" Then
                        'compare TempID attribute of control
                        'GLOG 4377: Avoid error if WordDoc.DeleteTags has already deleted
                        'the content control associated with this Tag
                        On Error Resume Next
                        xTempID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(oTag.ContentControl.Tag, _
                            "TempID", oDocument)
                        On Error GoTo ProcError
                        If xTempID <> "" Then
                            If xTempID = xWordTempID Then _
                                Exit For
                        End If
                    ElseIf bAfterDeleteScopeElementChange Then
                        'the original tag is no longer present, and we'll eventually
                        'hit all matching tags, so just return the first match
                        Exit For
                    Else
                        'try identifying by location in doc
                        On Error Resume Next
                        oCC2 = oTag.ContentControl
                        If Err().Number = 0 Then
                            If oCC2.Range.Start = lStart Then _
                                Exit For
                        Else
                            'control no longer exists - assume that this
                            'is the tag we're looking for
                            Exit For
                        End If
                        On Error GoTo ProcError
                    End If
                End If
            Next i

            'ColIndex property is used only internally and is not always
            'updated following deletions/insertions/moves - update now
            oTag.ColIndex = xColIndex

            'return tag
            GetTag_CC = oTag

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function Update_CC(ByVal oCC As Word.ContentControl, _
                                  Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                                  Optional ByVal xTagTempID As String = "", _
                                  Optional ByVal xFullTagID As String = "")
            'updates selected properties of the Tag holding oCC to reflect
            'the real current values of the underlying attributes - this is useful
            'after 1) an mDel is replaced by an mVar (or vice versa) in an expanded
            'delete scope scenario or 2) we "block" the manual deletion of a tag, which
            'actually involves inserting a replacement for the original tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Update_CC"
            Dim oTag As Tag
            Dim xTag As String = ""
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            xTag = oCC.Tag
            oDoc = oCC.Range.Document

            'get Tag corresponding to specified content control
            oTag = GetTag_CC(oCC, bAfterDeleteScopeElementChange, _
                xTagTempID, xFullTagID)

            'update
            With oTag
                'element name
                .ElementName = LMP.Forte.MSWord.BaseMethods.GetBaseName(oCC)

                'object data
                .ObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)

                If .ElementName = "mSEG" Then
                    'deleted scopes
                    .DeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDoc)

                    'authors
                    .Authors = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "Authors", oDoc)
                End If

                'content control
                .ContentControl = oCC

                'tag
                .Tag = oCC.Tag
            End With

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Delete_CC(ByVal oCC As Word.ContentControl)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Delete_CC"
            Dim xColIndex As String = ""
            Dim oTag As Tag
            Dim oTags As Tags
            Dim lIndex As Integer
            Dim lPos As Integer

            On Error GoTo ProcError

            oTags = Me

            'get Tag corresponding to specified control
            oTag = GetTag_CC(oCC)

            'find in collection
            xColIndex = oTag.ColIndex
            lPos = InStr(xColIndex, ".")
            While lPos <> 0
                lIndex = CLng(Left$(xColIndex, lPos - 1))
                oTag = oTags.Item(lIndex)
                oTags = oTag.Tags
                xColIndex = Mid$(xColIndex, lPos + 1)
                lPos = InStr(xColIndex, ".")
            End While
            lIndex = CLng(xColIndex)

            'delete
            oTags.RemoveTag(lIndex)

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function Insert_CC(ByVal oCC As Word.ContentControl, Optional ByVal bNoReindex As Boolean = False) As Tag
            'inserts specified content control into the tags collection;
            'returns the new Tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Insert_CC"
            Dim xTagID As String = ""
            Dim oDoc As WordDoc
            Dim xElement As String = ""
            Dim xObjectData As String = ""
            Dim oTag As Tag
            Dim oTags As Tags
            Dim xFullTagID As String = ""
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oAttribute As xml.xmlAttribute
            Dim xColIndex As String = ""
            Dim xValue As String = ""
            Dim xValue2 As String = ""
            Dim bCreateNew As Boolean
            Dim xParentTagID As String = ""
            Dim xParentPart As String = ""
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xExternalParent As String = ""
            Dim xPart As String = ""
            Dim xDeletedScopes As String = ""
            Dim bReindex As Boolean
            Dim xDisplayName As String = ""
            Dim xDisplayName2 As String = ""
            Dim iPropIndex As Short
            Dim bResolvePartConflict As Boolean
            Dim oAuthors As Word.XMLNode
            Dim xAuthors As String = ""
            Dim xParentVariable As String = ""
            Dim oProps As Object
            Dim xSegmentName As String = ""
            Dim xPassword As String = ""
            Dim xTag As String = ""
            Dim oDocument As Word.Document
            Dim xParentTagIDRoot As String = ""
            Dim oDeleteScope As DeleteScope 'GLOG 6381 (dm)
            Dim oParentSegmentBmk As Word.Bookmark 'GLOG 6798
            Dim oParentCC As Word.ContentControl 'GLOG 7083

            On Error GoTo ProcError

            oDoc = New WordDoc
            xElement = LMP.Forte.MSWord.BaseMethods.GetBaseName(oCC)
            xTag = oCC.Tag
            oDocument = oCC.Range.Document

            'get id of new tag
            xTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "TagID", oDocument)

            'get object data
            xObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "ObjectData", oDocument)

            'load xml summary of overall collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'GLOG 7083 (dm) - this function is called at design, as well as at runtime -
            'check for an mSEG CC before assuming an mSEG bookmark
            'get physical container
            oParentCC = oCC.ParentContentControl
            Do While (Not oParentCC Is Nothing)
                If LMP.Forte.MSWord.BaseMethods.GetBaseName(oParentCC) = "mSEG" Then Exit Do
                oParentCC = oParentCC.ParentContentControl
            Loop

            'GLOG 6798: Use Segment bookmarks
            If oParentCC Is Nothing Then _
                    oParentSegmentBmk = oDoc.GetParentBookmarkFromContentControl(oCC)

            If (xElement = "mDel") And ((Not oParentSegmentBmk Is Nothing) Or _
                    (Not oParentCC Is Nothing)) Then
                'mDel may belong to parent mSEG -
                '7/7/10 (dm) - modified segment name comparison code below - with the previous
                'code, "LetterSignatures" and "Letter" were seen as a match, resulting in an
                'mDel belonging to Letter being added to the wrong collection
                oProps = Split(xObjectData, "|")
                xSegmentName = Left$(oProps(0), InStr(oProps(0), "_") - 1)

                If Not oParentSegmentBmk Is Nothing Then
                    xParentTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(Mid(oParentSegmentBmk.Name, 2), "TagID", oDocument)
                    xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Do While xParentTagIDRoot <> xSegmentName
                        oParentSegmentBmk = oDoc.GetParentSegmentBookmark(oParentSegmentBmk)
                        If oParentSegmentBmk Is Nothing Then Exit Do
                        xParentTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(Mid(oParentSegmentBmk.Name, 2), "TagID", oDocument)
                        xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Loop
                Else
                    'GLOG 7083 (dm) - restored this code for design time
                    xParentTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(oParentCC.Tag, "TagID", oDocument)
                    xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Do While (BaseMethods.GetBaseName(oParentCC) <> "mSEG") Or _
                            (xParentTagIDRoot <> xSegmentName)
                        oParentCC = oParentCC.ParentContentControl
                        If oParentCC Is Nothing Then Exit Do
                        xParentTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(oParentCC.Tag, "TagID", oDocument)
                        xParentTagIDRoot = Left$(xParentTagID, InStr(xParentTagID, "_") - 1)
                    Loop
                End If

                'segment id may have changed when segment was inserted into collection
                If (xParentTagIDRoot = xSegmentName) And (oProps(0) <> xParentTagID) Then
                    'update segment id in object data
                    xObjectData = Replace(xObjectData, oProps(0), xParentTagID)
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDocument)
                End If
            End If

            'get target collection
            If xElement = "mSEG" Then
                'get parent from full tag id, since in the case of mSEGs, this
                'may not be the physical parent
                xFullTagID = oDoc.GetFullTagID_CC(oCC)
                lPos = InStrRev(xFullTagID, ".")
                If lPos <> 0 Then
                    xParentTagID = Left$(xFullTagID, lPos - 1)
                    oTag = ItemFromID(xParentTagID)
                    oTags = oTag.Tags
                Else
                    oTags = Me
                End If

                'get other attributes
                xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDocument)
                xPart = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "PartNumber", oDocument)
                xAuthors = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "Authors", oDocument)
                xParentVariable = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "AssociatedParentVariable", oDocument)

                'reindex tag id if necessary
                If (InStr(xTagID, "_") = 0) Or _
                        (m_bGUIDIndexing And (InStr(xTagID, "{") = 0)) Or _
                        (Not m_bGUIDIndexing And (InStr(xTagID, "{") <> 0)) Then
                    'the index is either missing or of the inappropriate type
                    bReindex = True
                Else
                    'search for existing tag with same id and part number
                    '            If m_bGUIDIndexing Then
                    'GLOG 3482 (3/19/09 dm) - assign new guid id if this one exists
                    'anywhere in the document
                    'GLOG 3700 (11/30/09 dm) - this should apply in design as well
                    xXPath = "//mSEG[contains(@TagID, '" & xTagID & "') and " & _
                        "substring-after(@TagID, '" & xTagID & "')='' and " & _
                        "@Part='" & xPart & "']"
                    '            Else
                    '                xXPath = "//mSEG[@TagID='" & xFullTagID & "' and " & _
                    '                    "@Part='" & xPart & "']"
                    '            End If
                    oNode = m_oSummaryXML.selectSingleNode(xXPath)
                    If Not oNode Is Nothing Then
                        'duplicate tag exists
                        If Me.FixedTopLevelTagID = xTagID Then
                            'tag id is fixed - modify part number
                            'after tag is added to the collection
                            bResolvePartConflict = True
                        Else
                            'assign new tag id
                            bReindex = True
                        End If
                    End If
                End If

                If bReindex Then
                    '            '5/12/11 (dm) - get deactivation doc var
                    '            Dim xVarValue as string = ""
                    '            On Error Resume Next
                    '            xVarValue = oDocument.Variables("Deactivation_" & xTagID).Value
                    '            On Error GoTo ProcError

                    '10/25/11 (dm) - reindex snapshot vars - store old tag id
                    Dim xOldTagID As String = ""
                    xOldTagID = xTagID

                    ReindexSegment(xTagID, xParentTagID, True)
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)

                    'GLOG 6381 (dm) - retag deleted scopes
                    oDeleteScope = New DeleteScope
                    oDeleteScope.RetagDeletedScopes(xTag, oDocument)

                    '            '5/12/11 (dm) - add new deactivation doc var if appropriate
                    '            If xVarValue <> "" Then _
                    '                oDocument.Variables.Add "Deactivation_" & xTagID, xVarValue

                    '10/25/11 (dm) - add new snapshot doc vars
                    LMP.Forte.MSWord.BaseMethods.ReindexSnapshot(oDocument, xOldTagID, xTagID, False)
                End If

                'validate parent tag id in object data
                lPos = InStr(xObjectData, "ParentTagID=")
                If lPos > 0 Then
                    lPos = lPos + 12
                    lPos2 = InStr(lPos, xObjectData, "|")
                    'JTS 8/18/16: There may be no pipe char if at end of Object Data
                    If lPos2 = 0 Then
                        lPos2 = Len(xObjectData) + 1
                    End If
                    xExternalParent = Mid$(xObjectData, lPos, lPos2 - lPos)

                    'check whether the referenced tag is in the collection
                    xXPath = "//mSEG[@TagID='" & xExternalParent & "']"
                    oNode = m_oSummaryXML.SelectSingleNode(xXPath)
                    If oNode Is Nothing Then
                        'parent is not in document - update object data
                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                            xExternalParent & "|", "")
                        'JTS 8/18/16: There may be no pipe char if at end of Object Data
                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                            xExternalParent, "")
                        xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                        LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDocument)
                    End If
                End If
            Else
                'for other elements, physical containment controls
                If Not oParentSegmentBmk Is Nothing Then
                    oTag = GetTag_Bookmark(oParentSegmentBmk)
                    oTags = oTag.Tags
                    xParentTagID = oTag.FullTagID
                    xFullTagID = xParentTagID & "." & xTagID
                    xParentPart = oTag.Part
                ElseIf Not oParentCC Is Nothing Then
                    'GLOG 7083 (dm) - restored this code for design time
                    oTag = GetTag_CC(oParentCC)
                    oTags = oTag.Tags
                    xParentTagID = oTag.FullTagID
                    xFullTagID = xParentTagID & "." & xTagID
                    xParentPart = oTag.Part
                Else
                    oTags = Me
                    xFullTagID = xTagID
                End If

                'GLOG 2622 - if mVar is distributed, same tag name is allowed with different value
                If (xElement = "mVar") And Not bNoReindex Then
                    bNoReindex = InStr(UCase(BaseMethods.GetObjectDataValue(xObjectData, _
                        mpVariableProperties.mpVariableProperty_DistributedDetail, "mVar", xTagID)), "TRUE")
                End If

                'resolve naming conflicts
                If xElement <> "mDel" And Not bNoReindex Then
                    'look for tag id match
                    xXPath = "//" & xElement & "[@TagID='" & xFullTagID & "']"
                    If xElement = "mVar" Then
                        'also look for same-named variable definitions in mSEG
                        xXPath = xXPath & " | //mSEG[(@TagID='" & xParentTagID & "') " & _
                            "and ((substring(@DefinedVariables, 1, " & (Len(xTagID) + 1) & ")='" & xTagID & "|') or " & _
                            "(contains(@DefinedVariables, '|" & xTagID & "|')))]"
                    End If
                    oNode = m_oSummaryXML.selectSingleNode(xXPath)
                    If Not oNode Is Nothing Then
                        'get matching Tag
                        oAttribute = oNode.attributes.getNamedItem("ColIndex")
                        xColIndex = oAttribute.value
                        oTag = ItemFromColIndex(xColIndex)

                        'if two mVars have the same tag id, but different values or object data,
                        'modify the tag id and object data of the new tag to create a new variable -
                        'two blocks should never have the same tag id, so a new block should
                        'always be created - the same is true of an mVar with a tag id that matches
                        'the name of a variable defined in an mSEG belonging to its parent, i.e. a
                        'new variable should be created unconditionally
                        If xElement = "mVar" Then
                            'get value of inserted tag
                            xValue = UCase$(oCC.Range.Text)
                            If xValue <> "" Then
                                xValue = Replace(xValue, Chr(7), "Chr7")
                                xValue = Replace(xValue, Chr(11), "Chr11")
                                xValue = Replace(xValue, Chr(12), "Chr12")
                                xValue = Replace(xValue, Chr(13), "Chr13")
                                xValue = Replace(xValue, Chr(21), "Chr21")

                                'GLOG 4476 (dm) - replace field character
                                xValue = Replace(xValue, Chr(19), "Chr19")

                                'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                                xValue = Replace(xValue, Chr(2), "")

                                'GLOG 5978 (dm) - replace non-breaking hyphen
                                xValue = Replace(xValue, Chr(30), "Chr30")

                                'GLOG 6041 (dm) - replace comment characters
                                xValue = Replace(xValue, Chr(5), "Chr5")

                                'GLOG 7417 (dm) - remove graphic
                                xValue = Replace(xValue, Chr(1), "")

                                'Also replace XML Reserved characters
                                xValue = BaseMethods.ReplaceXMLChars(xValue)
                            End If

                            'get display name - if the value of tag is the display name,
                            'this is simply placeholder text and a new variable should not
                            'be created even if the matching tag now has a different value
                            iPropIndex = mpVariableProperties.mpVariableProperty_DisplayName
                            xDisplayName = LMP.Forte.MSWord.BaseMethods.GetObjectDataValue(xObjectData, _
                                iPropIndex, xElement, xTagID)

                            If xValue <> UCase$(xDisplayName) Then
                                'get value of matching tag
                                xValue2 = UCase$(oTag.ContentControl.Range.Text)
                                If xValue2 <> "" Then
                                    xValue2 = Replace(xValue2, Chr(7), "Chr7")
                                    xValue2 = Replace(xValue2, Chr(11), "Chr11")
                                    xValue2 = Replace(xValue2, Chr(12), "Chr12")
                                    xValue2 = Replace(xValue2, Chr(13), "Chr13")
                                    xValue2 = Replace(xValue2, Chr(21), "Chr21")

                                    'GLOG 4476 (dm) - replace field character
                                    xValue2 = Replace(xValue2, Chr(19), "Chr19")

                                    'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                                    xValue2 = Replace(xValue2, Chr(2), "")

                                    'GLOG 5978 (dm) - replace non-breaking hyphen
                                    xValue2 = Replace(xValue2, Chr(30), "Chr30")

                                    'GLOG 6041 (dm) - replace comment characters
                                    xValue2 = Replace(xValue2, Chr(5), "Chr5")

                                    'GLOG 7417 (dm) - remove graphic
                                    xValue = Replace(xValue, Chr(1), "")

                                    'Also replace XML Reserved characters
                                    xValue2 = BaseMethods.ReplaceXMLChars(xValue2)
                                End If

                                'create new variable if the values aren't the same
                                bCreateNew = (xValue2 <> xValue)
                            End If

                            If Not bCreateNew Then
                                'create new variable if the object data isn't the same
                                bCreateNew = (oTag.ObjectData <> BaseMethods.RestoreXMLChars(xObjectData))
                            End If
                        Else
                            'block - get display name
                            iPropIndex = mpBlockProperties.mpBlockProperty_DisplayName
                            xDisplayName = LMP.Forte.MSWord.BaseMethods.GetObjectDataValue(xObjectData, _
                                iPropIndex, xElement, xTagID)
                        End If

                        If bCreateNew Or (xElement <> "mVar") Then
                            'modify display name if necessary
                            xDisplayName2 = LMP.Forte.MSWord.BaseMethods.GetObjectDataValue(oTag.ObjectData, _
                                iPropIndex, xElement, xTagID)
                            If xDisplayName = xDisplayName2 Then
                                If xElement = "mBlock" Then
                                    xDisplayName = GetNewBlockName(xDisplayName)
                                Else
                                    xDisplayName = GetNewName(xDisplayName)
                                End If
                            End If

                            'append/increment final digit until tag id is unique
                            Do
                                'get new tag id/name
                                If xElement = "mBlock" Then
                                    xTagID = GetNewBlockName(xTagID)
                                Else
                                    xTagID = GetNewName(xTagID)
                                End If

                                'ensure that new tag id/name is unique
                                xXPath = "//" & xElement & "[@TagID='" & xParentTagID & "." & _
                                    xTagID & "']"
                                If xElement = "mVar" Then
                                    'also look for same-named variable definitions in mSEG
                                    xXPath = xXPath & " | //mSEG[(@TagID='" & xParentTagID & "') " & _
                                        "and ((substring(@DefinedVariables, 1, " & (Len(xTagID) + 1) & ")='" & xTagID & "|') or " & _
                                        "(contains(@DefinedVariables, '|" & xTagID & "|')))]"
                                End If
                                oNode = m_oSummaryXML.selectSingleNode(xXPath)
                                If oNode Is Nothing Then Exit Do
                            Loop

                            'update tag id in document
                            LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)

                            'update object data
                            If xElement = "mVar" Then
                                AssignNewVariableProps(xObjectData, xTagID, xDisplayName)
                            Else
                                AssignNewBlockProps(xObjectData, xTagID, xDisplayName)
                            End If

                            'update object data in document
                            xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                            LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDocument)
                        End If
                    End If
                End If
            End If

            'update full tag id
            If xParentTagID <> "" Then
                xFullTagID = xParentTagID & "." & xTagID
            Else
                xFullTagID = xTagID
            End If

            'create new tag object and(WdUnits.wdSectionits properties
            oTag = New Tag
            With oTag
                .TagID = xTagID
                .ObjectData = xObjectData
                .ElementName = xElement
                .ContentControl = oCC
                .Tag = xTag
                .FullTagID = xFullTagID
                .ParentPart = xParentPart
                .DeletedScopes = xDeletedScopes
                .Part = xPart
                .Authors = xAuthors
                .ParentVariable = xParentVariable
            End With

            'add to collection
            oTags.AddTag(oTag)

            '10/25/11 - ensure that bounding object type is set
            If oTags.BoundingObjectType = mpBoundingObjectTypes.None Then _
                oTags.BoundingObjectType = mpBoundingObjectTypes.ContentControls

            'resolve part conflict
            If bResolvePartConflict Then _
                oTags.ResolveFixedTopLevelPartConflicts()

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            'set return value
            Insert_CC = oTag

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Friend Sub Add_Bookmark(oBookmarks As Word.Bookmarks, _
                            oDoc As Word.Document, _
                            Optional ByVal bIsNew As Boolean = False, _
                            Optional ByRef bRestartRefresh As Boolean = False, _
                            Optional ByVal xExclusions As String = "", _
                            Optional ByVal bDoBody As Boolean = True, _
                            Optional ByVal bDoNonBody As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Add_Bookmark"
            Dim i As Short
            Dim oBmk As Word.Bookmark
            Dim oHost As Word.Bookmark
            Dim bAdd As Boolean
            Dim oWordDoc As WordDoc
            Dim oParent As Word.Bookmark
            Dim bIsFinishedSegBmk As Boolean
            Dim oParentTag As LMP.Forte.MSWord.Tag
            Dim oRng As Word.Range
            Dim iBmks As Short
            Dim oBRng As Word.Range
            Dim iIndex As Short
            Dim oBmkRng As Word.Range
            Dim bContentControls As Boolean
            Dim bBodyDone As Boolean
            Dim iBodyCount As Short
            Dim iSec As Short 'GLOG 7543
            Dim iSectionLoops As Short 'GLOG 7543
            Dim bShowHidden As Boolean 'GLOG 8581

            'cycle through document bookmarks,
            'adding a tag for each segment bookmark
            'that does not have an associated content control or xml tag -
            'these are bookmarks for finished segments
            On Error GoTo ProcError
            oWordDoc = New WordDoc

            bShowHidden = oBookmarks.ShowHidden 'GLOG 8581
            oBookmarks.ShowHidden = True
            'GLOG 6843: Make sure bookmarks are iterated in same order they appear in document
            oBookmarks.DefaultSorting = Word.WdBookmarkSortBy.wdSortByLocation
            bContentControls = LMP.Forte.MSWord.BaseMethods.FileFormat(oDoc) = mpFileFormats.OpenXML
            'GLOG 7453
            If bDoNonBody Then
                iSectionLoops = oDoc.Sections.Count
            Else
                iSectionLoops = 1
            End If

            'GLOG 7453: Bookmarks in Headers/Footers appear in collection in order created,
            'instead of location order.  Therefore, when collecting H/F bookmarks, do for
            'each section in order
            For iSec = 1 To iSectionLoops
                For i = 1 To oBookmarks.Count
                    bAdd = False
                    oBmk = oBookmarks(i)
                    'GLOG : Bookmarks in Header/Footer stories will appear in order created, not order in document
                    'To ensure proper order for multiple
                    If Not bDoNonBody Or oBmk.Range.Sections(1).Index = iSec Then 'GLOG 7543
                        If (bDoBody And oBmk.StoryType = WdStoryType.wdMainTextStory) Or (bDoNonBody And oBmk.StoryType <> WdStoryType.wdMainTextStory) Then
                            If Left$(oBmk.Name, 4) = "_mps" Then
                                'is a segment bookmark - check for parent cc
                                oBmkRng = oBmk.Range
                                If bContentControls Then
                                    Dim oCC As ContentControl
                                    Dim oContainedCC As ContentControl

                                    oCC = oBmkRng.ParentContentControl

                                    If oCC Is Nothing Then
                                        'check content controls contained in bookmark -
                                        'in some cases, such as in tables, the bookmark start
                                        'will be position 0, while the content control start
                                        'will be 1
                                        If oBmkRng.ContentControls.Count > 0 Then
                                            For Each oContainedCC In oBmkRng.ContentControls
                                                If oContainedCC.Tag = Mid$(oBmk.Name, 2) Then
                                                    'the associated content control is
                                                    'contained in the bookmark -
                                                    'bookmark is not finished
                                                    bIsFinishedSegBmk = False
                                                    Exit For
                                                Else
                                                    bIsFinishedSegBmk = True
                                                End If
                                            Next oContainedCC
                                        Else
                                            'this is a finished segment bookmark
                                            bIsFinishedSegBmk = True
                                        End If
                                    ElseIf oCC.Tag <> Mid$(oBmk.Name, 2) Then
                                        'the parent cc is not the associated bounding object-
                                        'check for ccs that may be contained in the range
                                        oRng = oBmk.Range

                                        For Each oCC In oRng.ContentControls
                                            If oCC.Tag = Mid$(oBmk.Name, 2) Then
                                                bIsFinishedSegBmk = False
                                                Exit For
                                            Else
                                                bIsFinishedSegBmk = True
                                            End If
                                        Next oCC
                                    Else
                                        bIsFinishedSegBmk = False
                                    End If
                                Else
                                    Dim oNode As Word.XMLNode
                                    Dim oContainedNode As Word.XMLNode

                                    oNode = oBmkRng.XMLParentNode

                                    If oNode Is Nothing Then
                                        'check content controls contained in bookmark -
                                        'in some cases, such as in tables, the bookmark start
                                        'will be position 0, while the content control start
                                        'will be 1
                                        If oBmkRng.XMLNodes.Count > 0 Then
                                            For Each oContainedNode In oBmkRng.XMLNodes
                                                If LMP.Forte.MSWord.BaseMethods.GetTag(oContainedNode) = Mid$(oBmk.Name, 2) Then
                                                    'the associated content control is
                                                    'contained in the bookmark -
                                                    'bookmark is not finished
                                                    bIsFinishedSegBmk = False
                                                    Exit For
                                                Else
                                                    bIsFinishedSegBmk = True
                                                End If
                                            Next oContainedNode
                                        Else
                                            'this is a finished segment bookmark
                                            bIsFinishedSegBmk = True
                                        End If
                                    ElseIf LMP.Forte.MSWord.BaseMethods.GetTag(oNode) <> Mid$(oBmk.Name, 2) Then
                                        'the parent cc is not the associated bounding object-
                                        'check for ccs that may be contained in the range
                                        oRng = oBmk.Range

                                        For Each oNode In oRng.XMLNodes
                                            If LMP.Forte.MSWord.BaseMethods.GetTag(oNode) = Mid$(oBmk.Name, 2) Then
                                                bIsFinishedSegBmk = False
                                                Exit For
                                            Else
                                                bIsFinishedSegBmk = True
                                            End If
                                        Next oNode
                                    Else
                                        bIsFinishedSegBmk = False
                                    End If
                                End If
                                If bIsFinishedSegBmk Then
                                    oParentTag = Me.Parent

                                    If oParentTag Is Nothing Then
                                        'only add top-level nodes
                                        oBRng = oBmk.Range
                                        oParent = oWordDoc.GetParentSegmentBookmark(oBmk)

                                        While Not oParent Is Nothing
                                            If LMP.Forte.MSWord.BaseMethods.GetBaseNameFromTag(Mid$(oParent.Name, 2)) = "mSEG" Then
                                                oHost = oParent
                                                oParent = Nothing
                                            ElseIf iIndex > 0 Then
                                                oParent = oWordDoc.GetParentSegmentBookmark(oParent)
                                            End If
                                        End While

                                        If oHost Is Nothing Then
                                            'no parent mSEG
                                            bAdd = True
                                            'GLOG 8278 (dm) - a bug was causing BookmarkIsAvailableToCurrentClient
                                            'to always return TRUE - with the bug fixed, there'd now be extra processing
                                            'of questionable benefit - therefore, I'm remming out the block
                                            '                                ElseIf Not oWordDoc.BookmarkIsAvailableToCurrentClient(oHost) Then
                                            '                                    'parent mSEG doesn't belong to this client
                                            '                                    bAdd = True
                                        End If

                                        'GLOG 4943 (dm)
                                        oHost = Nothing
                                    Else
                                        bAdd = True
                                    End If

                                    If bAdd Then
                                        AddNode_Bookmark(oBmk, bIsNew, bRestartRefresh, xExclusions)
                                        'GLOG 3641 (dm) - added bRestartRefresh parameter in case in process
                                        'adjustments necessitate restarting the refresh process from the beginning
                                        If bRestartRefresh Then _
                                            Exit Sub
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next i
            Next iSec
            oBookmarks.ShowHidden = bShowHidden 'GLOG 8581
            'GLOG 7000: Force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Sub AddNode_Bookmark(ByVal oBmk As Word.Bookmark, ByVal bIsNew As Boolean, _
                ByRef bRestartRefresh As Boolean, Optional ByVal xExclusions As String = "")
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddNode_Bookmark"
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xDestination As String = ""
            Dim xText As String = ""
            Dim oPartnerNode As xml.xmlNode
            Dim oParentNode As xml.xmlNode
            Dim oConflictingNode As xml.xmlNode
            Dim xOldID As String = ""
            Dim xTagID As String = ""
            Dim xXPath As String = ""
            Dim xParentID As String = ""
            Dim xFamilyParentID As String = ""
            Dim bMove As Boolean
            Dim xValue As String = ""
            Dim oTag As Tag
            Dim oAttribute As xml.xmlAttribute
            Dim oDocument As Word.Document
            Dim i As Short
            Dim xObjectData As String = ""
            Dim xAuthorsDetail As String = ""
            Dim xPart As String = ""
            Dim xParentFullID As String = ""
            Dim xDefinedVars As String = ""
            Dim oDefinedVars As Object
            Dim xName As String = ""
            Dim oTags As Tags
            Dim xDeletedScopes As String = ""
            Dim xElementName As String = ""
            Dim xColIndex As String = ""
            Dim xDisplayName As String = ""
            Dim iPropIndex As Short
            Dim xParentVariable As String = ""
            Dim xSegmentID As String = ""
            Dim oProps As Object
            Dim xTagPrefixID As String = ""
            Dim xPassword As String = ""
            Dim oWordDoc As WordDoc
            Dim xTag As String = ""
            Dim oChildBmk As Word.Bookmark
            Dim xDocVar As String = ""
            Dim oContainingBmk As Word.Bookmark
            Dim bContentControls As Boolean
            Dim oRevision As Word.Revision 'GLOG 7355 (dm)
            Dim oRange As Word.Range 'GLOG 7355 (dm)
            Dim xDeletedVars As String 'GLOG 7499 (dm)
            Dim oDeleteScope As DeleteScope 'GLOG 7499 (dm)
            Dim vDeletedVars As Object 'GLOG 7499 (dm)

            On Error GoTo ProcError

            oWordDoc = New WordDoc
            oDocument = oBmk.Parent
            xTag = Mid$(oBmk.Name, 2)

            'GLOG 15961
            'add tag to node store only if it contains a TagID attribute
            xTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "TagID", oDocument)

            If (String.IsNullOrEmpty(xTagID)) Then
                Exit Sub
            End If

            '4/18/11 (dm) - exit if bookmark is specified for exclusion -
            'we added this to keep deleted content controls out of the collection
            'GLOG 7355 (dm) - we no longer check for revisions in advance -
            'we now check each bookmark before adding it - if the bookmark is marked
            'for deletion, this revision will be first in the bookmark's collection
            '    If InStr(xExclusions, "|" & xTag & "|") Then _
            '        Exit Sub
            oRange = oBmk.Range

            'GLOG : 7595 : CEH
            If oWordDoc.RangeHasRevisions(oRange) Then
                'GLOG 15961: If error occurs for first revision, keep trying with subsequent items
                Dim iCount As Integer
                For iCount = 1 To oRange.Revisions.Count
                    On Error GoTo TryAgain
                    oRevision = oRange.Revisions(iCount)
                    If (oRevision.Type = Word.WdRevisionType.wdRevisionDelete) Or _
                            (oRevision.Type = Word.WdRevisionType.wdRevisionCellDeletion) Then
                        If (oRevision.Range.Start <= oRange.Start) And _
                                (oRevision.Range.End >= oRange.End) Then
                            Exit Sub
                        End If
                    End If
                    'exit if no error
                    Exit For
TryAgain:
                    'If error occurred, continue For loop, otherwise display general error message
                Next
            End If
            On Error GoTo ProcError
            'GLOG 8722
            Dim bShowHidden As Boolean = oDocument.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oDocument.Bookmarks.ShowHidden = True
            End If
            xElementName = LMP.Forte.MSWord.BaseMethods.GetBaseNameFromTag(xTag)

            'we need to explicitly exclude mSubVars because new data structure
            'does not distinguish between the tag id and name attributes
            If (xElementName = "mSubVar") Or (xElementName = "") Then
                Exit Sub
            End If

            bContentControls = LMP.Forte.MSWord.BaseMethods.FileFormat(oDocument) = mpFileFormats.OpenXml

            'GLOG 7101 (dm) - we shouldn't be adding to node store without valid object data either
            xObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "ObjectData", oDocument)

            If (xTagID <> "") And (InStr(xTagID, "_") > 1) And (xObjectData <> "") Then 'GLOG 7025: Ignore bookmarks for which no Segment Name was found in ObjectData
                m_iColIndex = m_iColIndex + 1
                If Me.Parent Is Nothing Then
                    xColIndex = m_iColIndex
                Else
                    xColIndex = Me.Parent.ColIndex & "." & m_iColIndex
                    xParentID = Me.Parent.TagID
                    xParentFullID = Me.Parent.FullTagID
                End If

                xOldID = xTagID

                If xElementName = "mSEG" Then
                    'determine whether node is newly inserted
                    bIsNew = (m_bGUIDIndexing And (InStr(xTagID, "{") = 0))
                    If bIsNew Then
                        'use current client's password
                        xPassword = GlobalMethods.g_xEncryptionPassword
                    Else
                        'GLOG 3641 (dm) - adjust collection table if necessary
                        'oWordDoc.AdjustCollectionTableIfNecessary_CC oContentControl, bRestartRefresh
                        If bRestartRefresh Then _
                            Exit Sub
                    End If

                    'get author detail
                    xAuthorsDetail = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "Authors", oDocument)
                    xAuthorsDetail = BaseMethods.ReplaceXMLChars(xAuthorsDetail)

                    'get parent variable
                    xParentVariable = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "AssociatedParentVariable", _
                        oDocument)

                    'get deleted scopes
                    xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDocument)

                    'get part number
                    xPart = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "PartNumber", oDocument)
                    If xPart = "" Then _
                        xPart = 1
                End If

                'get object data
                If xObjectData <> "" Then
                    xObjectData = BaseMethods.ReplaceXMLChars(xObjectData)
                    If xElementName = "mSEG" Then
                        'get external parent
                        lPos = InStr(xObjectData, "ParentTagID=")
                        If lPos > 0 Then
                            lPos = lPos + 12
                            lPos2 = InStr(lPos, xObjectData, "|")
                            'JTS 8/18/16: May be at end of ObjectData
                            If lPos2 = 0 Then
                                lPos2 = Len(xObjectData) + 1
                            End If
                            If Not LMP.Forte.MSWord.BaseMethods.IsCollectionTableItem(xObjectData) Then
                                xFamilyParentID = Mid$(xObjectData, lPos, lPos2 - lPos)
                            Else
                                'GLOG 3482 (3/20/09 dm) - parent tag id was inadvertently
                                'getting added to collection table items, which are never
                                'external children - clean up now
                                xObjectData = Left$(xObjectData, lPos - 13) & _
                                    Mid$(xObjectData, lPos2 + 1)
                            End If
                        End If
                    ElseIf xElementName = "mDel" Then
                        'get segment
                        oProps = Split(xObjectData, "|")
                        xSegmentID = oProps(0)
                    End If

                    'get/add tag prefix id -
                    'tag prefix id should be added at runtime only to newly inserted mSEGs
                    'and their child mVars and mBlocks
                    If xElementName <> "mDel" Then
                        xTagPrefixID = LMP.Forte.MSWord.BaseMethods.GetTagPrefixID(xObjectData)

                        'added 2/12/09 (dm) to deal with client tag prefix ids that
                        'inadvertently got into the database - strip existing id if
                        'newly inserted or in design
                        If (xTagPrefixID <> "") And (bIsNew Or Not m_bGUIDIndexing) Then
                            lPos = InStr(xObjectData, GlobalMethods.mpTagPrefixIDSeparator)
                            If lPos > 0 Then _
                                xObjectData = Mid$(xObjectData, lPos + Len(GlobalMethods.mpTagPrefixIDSeparator))
                            xTagPrefixID = ""
                        End If

                        If bIsNew And (xTagPrefixID = "") And (GlobalMethods.g_xClientTagPrefixID <> "") Then
                            xObjectData = GlobalMethods.g_xClientTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & xObjectData
                        ElseIf (xTagPrefixID <> "") And (xTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (GlobalMethods.g_xClientTagPrefixID <> "") And (GlobalMethods.g_xClientTagPrefixID <> GlobalMethods.mpInternalTagPrefixID) And _
                                (xTagPrefixID <> GlobalMethods.g_xClientTagPrefixID) Then
                            'tag does not belong to the current client - do not add node -
                            'this is temporarary - we hope to ultimately offer sharing of mp10
                            'functionality between clients - when we do, we'll add these tags to
                            'the tags collection and use the existing BelongsToCurrentClient
                            'property of segments, variables, and blocks to determine whether we
                            'can get field code values from the current db - exception for trailers
                            If (xElementName <> "mSEG") Or _
                                    (InStr(xObjectData, "ObjectTypeID=401|") = 0) Then
                                'adjust index
                                m_iColIndex = m_iColIndex - 1

                                'contained nodes may belong to the client
                                For Each oChildBmk In oBmk.Range.Bookmarks
                                    If Mid$(oChildBmk.Name, 2) <> xTag Then
                                        'only add this control's immediate children
                                        oContainingBmk = oWordDoc.GetParentSegmentBookmark(oChildBmk)

                                        If Not oContainingBmk Is Nothing Then
                                            If Mid$(oContainingBmk.Name, 2) = xTag Then
                                                AddNode_Bookmark(oChildBmk, bIsNew, bRestartRefresh, _
                                                    xExclusions)
                                            End If
                                        End If
                                    End If
                                Next oChildBmk

                                Exit Sub
                            End If
                        End If
                    End If
                End If

                'modify attributes and/or flag to move if necessary
                If xElementName = "mSEG" Then
                    If xOldID <> Me.FixedTopLevelTagID Then
                        'check whether there's another tag with same id and part #
                        xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                        oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                        If oConflictingNode Is Nothing Then
                            'no conflict - check whether we've already found another
                            'part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                        ElseIf m_bGUIDIndexing Then
                            'GLOG 3003: there is a conflict - increment PartNumber until unused Part is found
                            'Use current conflicting node for mSEG attributes
                            oPartnerNode = oConflictingNode
                            Do
                                xPart = Trim(Str(Val(xPart) + 1))
                                xXPath = "//mSEG[@OldID='" & xOldID & "' and @Part='" & xPart & "']"
                                oConflictingNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            Loop Until oConflictingNode Is Nothing
                        Else
                            'there is a conflict at design time - check whether we've already found
                            'and reindexed another part of the same segment
                            xXPath = "//mSEG[@OldID='" & xOldID & "' and @TagID!='" & xOldID & _
                                "' and @Part!='" & xPart & "']"
                            oPartnerNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            'if no partner node found, reindex
                            If oPartnerNode Is Nothing Then
                                ReindexSegment(xTagID, xParentFullID, False)

                                'GLOG 6381 (dm) - retag deleted scopes
                                oDeleteScope = New DeleteScope
                                oDeleteScope.RetagDeletedScopes(xTag, oDocument)
                                xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, _
                                    "DeletedScopes", oDocument)
                            End If
                        End If

                        If Not oPartnerNode Is Nothing Then
                            'another part found
                            With oPartnerNode.attributes
                                'assign the same new id
                                oAttribute = .getNamedItem("TagID")
                                xTagID = oAttribute.value

                                'if not in same collection as other part, flag to move there
                                oAttribute = .getNamedItem("ColIndex")
                                lPos = InStrRev(oAttribute.value, ".")
                                If (lPos = 0) And (Not Me.Parent Is Nothing) Then
                                    'destination is top level
                                    xDestination = "0"
                                ElseIf lPos > 0 Then
                                    'get destination
                                    xDestination = Left$(oAttribute.value, lPos - 1)
                                    'clear flag if already in same collection
                                    If Not Me.Parent Is Nothing Then
                                        If Me.Parent.ColIndex = xDestination Then _
                                            xDestination = ""
                                    End If
                                End If
                            End With
                        Else
                            'reindex if necessary
                            If (InStr(xTagID, "_") = 0) Or _
                                    (m_bGUIDIndexing And (InStr(xTagID, "{") = 0)) Or _
                                    (Not m_bGUIDIndexing And (InStr(xTagID, "{") <> 0)) Then
                                'item is newly inserted - reindex
                                ReindexSegment(xTagID, xParentFullID, False)

                                'GLOG 6381 (dm) - retag deleted scopes
                                'GLOG 8206 (dm) - delete old associated doc vars
                                oDeleteScope = New DeleteScope
                                oDeleteScope.RetagDeletedScopes(xTag, oDocument, True) 'GLOG 8206 (dm)
                                xDeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, _
                                    "DeletedScopes", oDocument)

                                'update NewSegment property
                                If Me.Parent Is Nothing Then _
                                    m_xNewSegment = xTagID
                            End If
                        End If

                        'deal with external children
                        If xFamilyParentID <> "" Then
                            'look for parent
                            xXPath = "//mSEG[@OldID='" & xFamilyParentID & "']"
                            oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                            If Not oParentNode Is Nothing Then
                                'we've found a tag for the parent- update ParentTagID in ObjectData
                                oAttribute = oParentNode.attributes.getNamedItem("TagID")
                                If xFamilyParentID <> oAttribute.value Then
                                    xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                        xFamilyParentID & "|", "ParentTagID=" & oAttribute.Value & "|")
                                    'JTS 8/18/16: There may be no pipe char if at end of Object Data
                                    xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                        xFamilyParentID, "ParentTagID=" & oAttribute.Value)
                                    xFamilyParentID = oAttribute.Value
                                End If

                                'check whether child is physically contained within parent -
                                'if not, we'll later need to move it
                                If Me.Parent Is Nothing Then
                                    bMove = True
                                ElseIf xParentID <> xFamilyParentID Then
                                    bMove = True
                                End If
                                If bMove Then
                                    oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                                    xDestination = oAttribute.value
                                End If
                            Else
                                'parent is not in document - update object data
                                'GLOG 5950 (dm) - don't clear ParentTagID immediately -
                                'when some shapes are Word 2010 shapes, and others are
                                'textboxes, the nodes may not load in the expected order -
                                'truthfully, we've just been lucky to date that parents in
                                'textboxes (e.g. pleading paper) have loaded before their
                                'external children in textboxes (e.g. sidebar) - this
                                'is now handled in UniteFamilies()
                                '                        xObjectData = Replace(xObjectData, "ParentTagID=" & _
                                '                            xFamilyParentID & "|", "")
                                '                        xFamilyParentID = ""
                                xDestination = "Pending" & xFamilyParentID
                            End If
                        End If
                    End If

                    'resolve naming conflicts in variable definitions stored in this mSEG -
                    '1/8/09 (Doug) - just get variable names - I remmed out the conflict
                    'resolution code because 1) RenameItemIfNecessary wasn't working properly
                    'for tagless variables and 2) it didn't seem worth fixing because
                    'I can't imagine how such a conflict between two tagless variables would occur
                    xDefinedVars = GetDefinedVariableNames(xObjectData, xDeletedScopes)
                    '            If xDefinedVars <> "" Then
                    '                oDefinedVars = Split(xDefinedVars, "|")
                    '                For i = 0 To UBound(oDefinedVars) - 1
                    '                    xName = oDefinedVars(i)
                    '                    RenameItemIfNecessary xName, xObjectData, xTagID, _
                    '                        xParentFullID, "mSEG", ""
                    '                Next i
                    '            End If
                ElseIf xElementName = "mDel" Then
                    'ensure that mDel becomes child of its owning segment -
                    'GLOG 3702 (dm) - in design mode, mDels of secondary collection table items
                    'were getting assigned to the primary item when secondary item was the
                    'same segment - this is because the primary item has the same old id -
                    'start by looking for a match for both the old and new ids
                    xXPath = "//mSEG[@OldID='" & xSegmentID & "' and @TagID='" & xParentID & "']"
                    oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    If oParentNode Is Nothing Then
                        'search for just the old id
                        xXPath = "//mSEG[@OldID='" & xSegmentID & "']"
                        oParentNode = GlobalMethods.g_oContentXML.selectSingleNode(xXPath)
                    End If
                    If Not oParentNode Is Nothing Then
                        'we've found a tag for the parent- update ObjectData if necessary
                        oAttribute = oParentNode.attributes.getNamedItem("TagID")
                        If xSegmentID <> oAttribute.value Then
                            lPos = InStr(xObjectData, "|")
                            xObjectData = oAttribute.value & Mid$(xObjectData, lPos)
                            xSegmentID = oAttribute.value
                        End If

                        'check whether mDel's immediate parent is owning mSEG -
                        'if not, we'll later need to move it
                        If Me.Parent Is Nothing Then
                            bMove = True
                        ElseIf xParentID <> xSegmentID Then
                            bMove = True
                        End If
                        If bMove Then
                            oAttribute = oParentNode.attributes.getNamedItem("ColIndex")
                            xDestination = oAttribute.value
                        End If
                    Else
                        'don't add mDel if parent mSEG isn't in collection
                        m_iColIndex = m_iColIndex - 1
                        Exit Sub
                    End If
                Else
                    'get variable value; this will be added to our content xml, so replace any
                    'character that can't be used "as is" in xml character data
                    If xElementName = "mVar" Then
                        '                xValue = UCase$(oContentControl.Range.Text)
                        xValue = Replace(xValue, Chr(7), "Chr7")
                        xValue = Replace(xValue, Chr(11), "Chr11")
                        xValue = Replace(xValue, Chr(12), "Chr12")
                        xValue = Replace(xValue, Chr(13), "Chr13")
                        xValue = Replace(xValue, Chr(21), "Chr21")

                        'GLOG 4476 (dm) - replace field character
                        xValue = Replace(xValue, Chr(19), "Chr19")

                        'GLOG 5252: Remove any Footnote/Endnote Characters from XML
                        xValue = Replace(xValue, Chr(2), "")

                        'GLOG 5978 (dm) - replace non-breaking hyphen
                        xValue = Replace(xValue, Chr(30), "Chr30")

                        'GLOG 6041 (dm) - replace comment characters
                        xValue = Replace(xValue, Chr(5), "Chr5")

                        'GLOG 7417 (dm) - remove graphic
                        xValue = Replace(xValue, Chr(1), "")

                        'Also replace XML Reserved characters
                        xValue = BaseMethods.ReplaceXMLChars(xValue)
                    End If

                    'resolve naming conflicts in variables and blocks
                    RenameItemIfNecessary(xTagID, xObjectData, xTagID, xParentFullID, _
                        xElementName, xValue)
                End If

                'add to GlobalMethods.g_oContentXML
                lPos = InStr(GlobalMethods.g_oContentXML.OuterXml, "</mpNodes>")
                If xElementName = "mSEG" Then
                    'mSEG - include defined variables
                    xText = "<mSEG TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' DefinedVariables='" & xDefinedVars & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' Part='" & xPart & "' Authors='" & xAuthorsDetail & "' AssociatedParentVariable='" & xParentVariable & "' />"
                Else
                    'include value
                    xText = "<" & xElementName & " TagID='" & xTagID & "' OldID='" & xOldID & _
                        "' ColIndex='" & xColIndex & "' Destination='" & xDestination & _
                        "' Value='" & xValue & "' ParentID='" & xParentFullID & _
                        "' ObjectData='" & xObjectData & "' />"
                End If

                GlobalMethods.g_oContentXML.loadXML(Left$(GlobalMethods.g_oContentXML.OuterXml, lPos - 1) & xText & "</mpNodes>")

                'update doc
                LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TagID", xTagID, "", oDocument)
                If xObjectData <> "" Then
                    xObjectData = BaseMethods.RestoreXMLChars(xObjectData)
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, xPassword, oDocument)
                End If

                'GLOG 3003: Update PartNumber attribute in case changed
                If xPart <> "" Then
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "PartNumber", xPart, xPassword, oDocument)
                End If

                If xDeletedScopes <> "" Then
                    'this is simply to encrypt or unencrypt doc vars if necessary
                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedScopes, _
                        xPassword, oDocument)

                    'GLOG 7499 (dm) - encrypt/unencrypt doc vars associated with tags/ccs
                    'embedded in the DeletedScopes attribute
                    oDeleteScope = New DeleteScope
                    xDeletedVars = oDeleteScope.GetAssociatedDocVars(xDeletedScopes)
                    If xDeletedVars <> "" Then
                        vDeletedVars = Split(xDeletedVars, "|")
                        For i = 0 To UBound(vDeletedVars)
                            LMP.Forte.MSWord.BaseMethods.SetAttributesEncryption(vDeletedVars(i), _
                                True, xPassword, oDocument)
                        Next i
                    End If
                End If

                '10/26/11 (dm) - reindex snapshots if necessary - this comes into play
                'when using the Copy Segment/Paste Segment functionality
                If bIsNew Then _
                    LMP.Forte.MSWord.BaseMethods.ReindexSnapshot(oDocument, xOldID, xTagID, True)

                'create new tag and(WdUnits.wdSectionits properties
                oTag = New LMP.Forte.MSWord.Tag
                With oTag
                    .ElementName = xElementName
                    .TagID = xTagID
                    .ObjectData = xObjectData
                    .Part = xPart
                    .DeletedScopes = xDeletedScopes
                    .Authors = xAuthorsDetail
                    .ParentVariable = xParentVariable

                    .Bookmark = oBmk

                    If Me.Parent Is Nothing Then
                        .FullTagID = xTagID
                    Else
                        .FullTagID = xParentFullID & "." & xTagID
                        .ParentPart = Me.Parent.Part
                    End If
                    .ColIndex = xColIndex
                    .Tag = Mid$(oBmk.Name, 2)
                    With .Tags
                        .Parent = oTag
                        .FixedTopLevelTagID = Me.FixedTopLevelTagID
                        .GUIDIndexing = Me.GUIDIndexing

                        'GLOG 6801 (dm) - bounding object type for this tag's
                        'collection should match file format
                        If bContentControls Then
                            .BoundingObjectType = mpBoundingObjectTypes.ContentControls
                        Else
                            .BoundingObjectType = mpBoundingObjectTypes.WordXMLNodes
                        End If
                    End With
                End With

                'add tag to collection
                m_Col.Add(oTag)

                xTag = Mid$(oBmk.Name, 2)

                'add contained nodes
                oDocument.Bookmarks.DefaultSorting = Word.WdBookmarkSortBy.wdSortByLocation

                Dim xChildTag As String = ""
                Dim bHasCC As Boolean
                Dim bHasNode As Boolean
                Dim oChildRng As Word.Range


                'add tags for all child segments that are bounded by bookmarks -
                'cycle through all bookmarks in range
                For Each oChildBmk In oBmk.Range.Bookmarks
                    'restrict to segment bookmarks
                    If Left$(oChildBmk.Name, 4) = "_mps" Then
                        'skip parent bookmark, which might be in the collection
                        If Mid$(oChildBmk.Name, 2) <> xTag Then
                            'skip if the bookmark has an associated control - we'll
                            'add the tag based on the control below
                            oChildRng = oChildBmk.Range

                            'include only those bookmarks that don't have ccs -
                            'TODO - add code to exclude bookmarks that don't have xml tags
                            If (bContentControls And Not oWordDoc.BookmarkHasAssociatedContentControl(oChildBmk)) Then
                                'get the containing bookmark
                                oContainingBmk = oWordDoc.GetParentSegmentBookmark(oChildBmk)
                                If Not oContainingBmk Is Nothing Then
                                    'add to collection iff the containing segment of this bookmark
                                    'is the segment that's currently been added - that is, add only
                                    'child segments of the specified bookmark
                                    If Mid$(oContainingBmk.Name, 2) = xTag Then
                                        'If xElementName = "mSEG" Then
                                        'add to this tag's collection
                                        oTag.Tags.AddNode_Bookmark(oChildBmk, bIsNew, bRestartRefresh, _
                                            xExclusions)
                                        'End If
                                    End If
                                End If
                            ElseIf (Not bContentControls And Not oWordDoc.BookmarkHasAssociatedTag(oChildBmk)) Then
                                'get the containing bookmark
                                oContainingBmk = oWordDoc.GetParentSegmentBookmark(oChildBmk)
                                If Not oContainingBmk Is Nothing Then
                                    'add to collection iff the containing segment of this bookmark
                                    'is the segment that's currently been added - that is, add only
                                    'child segments of the specified bookmark
                                    If Mid$(oContainingBmk.Name, 2) = xTag Then
                                        'If xElementName = "mSEG" Then
                                        'add to this tag's collection
                                        oTag.Tags.AddNode_Bookmark(oChildBmk, bIsNew, bRestartRefresh, _
                                            xExclusions)
                                        'End If
                                    End If
                                End If

                            End If
                        End If
                    End If
                Next oChildBmk

                If bContentControls Then
                    'add tags for all child segments that are bounded by ccs -
                    'cycle through all child content controls
                    Dim oChildCC As Word.ContentControl
                    Dim oChildCCBmk As Word.Bookmark

                    For Each oChildCC In oBmk.Range.ContentControls
                        'exclude the parent cc itself
                        If oChildCC.Tag <> xTag Then
                            'GLOG 7022: Don't need to create temp bookmark
                            'get the parent bookmark of the child cc -
                            'if one exists, the cc is truly a child cc
                            oContainingBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oChildCC.Range)

                            If Not oContainingBmk Is Nothing Then
                                'only work with children contained in the parent specified above
                                If oContainingBmk.Name = oBmk.Name Then
                                    'only add this control's immediate children
                                    If Mid$(oContainingBmk.Name, 2) = xTag Then
                                        If xElementName = "mSEG" Then
                                            'add to this tag's collection
                                            '                                    'GLOG 7022: Don't limit this to CCs without ParentContentControl,
                                            '                                    'as mSEG itself may be contained within a Block
                                            '                                    If oChildCC.ParentContentControl Is Nothing Then
                                            oTag.Tags.AddNode_CC(oChildCC, bIsNew, bRestartRefresh, _
                                                xExclusions, False) 'GLOG 7022
                                            '
                                            '                                    End If
                                        Else
                                            '                                    'add to this tag's parent's collection
                                            AddNode_CC(oChildCC, bIsNew, bRestartRefresh, _
                                                xExclusions, False) 'GLOG 7022
                                        End If
                                    ElseIf (xElementName = "mVar") And (BaseMethods.GetBaseName(oChildCC) = "mDel") Then
                                        'look for mDels contained in child mSubVars - if found,
                                        'add to this tag's parent's collection
                                        AddNode_CC(oChildCC, bIsNew, bRestartRefresh, xExclusions, False) 'GLOG 7022
                                    End If
                                End If
                            End If
                        End If
                    Next oChildCC
                Else
                    'add tags for all child segments that are bounded by xml tags -
                    'cycle through all child xml tags
                    Dim oChildNode As Word.XMLNode
                    Dim oChildNodeBmk As Word.Bookmark

                    For Each oChildNode In oBmk.Range.XMLNodes
                        'exclude the parent cc itself
                        If LMP.Forte.MSWord.BaseMethods.GetTag(oChildNode) <> xTag Then
                            'add temp bookmark if necessary
                            If Not oDocument.Bookmarks.Exists("_" & LMP.Forte.MSWord.BaseMethods.GetTag(oChildNode)) Then
                                oChildNodeBmk = oDocument.Bookmarks.Add("zzmp10TmpCCBmk", oChildNode.Range)
                            Else
                                oChildNodeBmk = oDocument.Bookmarks("_" & LMP.Forte.MSWord.BaseMethods.GetTag(oChildNode))
                            End If

                            'get the parent bookmark of the child cc -
                            'if one exists, the cc is truly a child cc
                            oContainingBmk = oWordDoc.GetParentSegmentBookmark(oChildNodeBmk)

                            'delete temp bookmark if necessary
                            If oDocument.Bookmarks.Exists("zzmp10TmpCCBmk") Then
                                oDocument.Bookmarks("zzmp10TmpCCBmk").Delete()
                            End If

                            If Not oContainingBmk Is Nothing Then
                                'only work with children contained in the parent specified above
                                If oContainingBmk.Name = oBmk.Name Then
                                    'only add this control's immediate children
                                    If Mid$(oContainingBmk.Name, 2) = xTag Then
                                        If xElementName = "mSEG" Then
                                            'add to this tag's collection
                                            '                                    'GLOG 7022: Don't limit this to Nodes without a ParentNode,
                                            '                                    'as mSEG itself may be contained within a Block
                                            'If oChildNode.ParentNode Is Nothing Then
                                            oTag.Tags.AddNode(oChildNode, bIsNew, bRestartRefresh, _
                                                xExclusions, False) 'GLOG 7022
                                            'End If
                                        Else
                                            'add to this tag's parent's collection
                                            AddNode(oChildNode, bIsNew, bRestartRefresh, _
                                                xExclusions, False) 'GLOG 7022
                                        End If
                                    ElseIf (xElementName = "mVar") And (BaseMethods.GetBaseName(oChildNode) = "mDel") Then
                                        'look for mDels contained in child mSubVars - if found,
                                        'add to this tag's parent's collection
                                        AddNode(oChildNode, bIsNew, bRestartRefresh, xExclusions, False) 'GLOG 7022
                                    End If
                                End If
                            End If
                        End If
                    Next oChildNode
                End If
                'GLOG 8722
                oDocument.Bookmarks.ShowHidden = bShowHidden
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetTag_Bookmark(ByVal oBookmark As Word.Bookmark, _
                                  Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                                  Optional ByVal xTagTempID As String = "", _
                                  Optional ByVal xFullTagID As String = "") As Tag
            'returns the Tag corresponding to the specified Word tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetTag_Bookmark"
            Dim xElement As String = ""
            Dim xPart As String = ""
            Dim oDoc As WordDoc
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim iLength As Short
            Dim i As Integer
            Dim lStart As Integer
            Dim xColIndex As String = ""
            Dim oBookmark2 As Word.Bookmark
            Dim oAttribute As xml.xmlAttribute
            Dim oTag As Tag
            Dim lIndex As Integer
            Dim lPos As Integer
            Dim xTemp As String = ""
            Dim xWordTempID As String = ""
            Dim oDocument As Word.Document
            Dim xTag As String = ""
            Dim xTempID As String = ""

            On Error GoTo ProcError

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            oDocument = oBookmark.Parent

            If Left$(oBookmark.Name, 1) = "_" Then
                xTag = Mid$(oBookmark.Name, 2)
            Else
                xTag = oBookmark.Name
            End If

            If xTagTempID = "" Then
                'no temp id specified - check whether there's one in the TempID attribute
                xWordTempID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "TempID", oDocument)
            End If

            'construct query
            oDoc = New WordDoc
            If xFullTagID = "" Then
                xFullTagID = oDoc.GetFullTagID_Bookmark(oBookmark)
            End If

            xElement = LMP.Forte.MSWord.BaseMethods.GetBaseNameFromTag(xTag)
            lStart = oBookmark.Range.Start

            If xElement = "mSEG" Then
                'get part
                xPart = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "PartNumber", oDocument)
                xXPath = "//mSEG[@TagID='" & xFullTagID & "' and @Part='" & xPart & "']"
            ElseIf bAfterDeleteScopeElementChange Then
                'there's been an mVar/mDel swap - look for old element
                If (xElement = "mVar") Or (xElement = "mBlock") Then
                    xXPath = "//mDel[@TagID='" & xFullTagID & "']"
                ElseIf xElement = "mDel" Then
                    xXPath = "//mVar[@TagID='" & xFullTagID & "'] | //mBlock[@TagID='" & _
                        xFullTagID & "']"
                End If
            Else
                xXPath = "//" & xElement & "[@TagID='" & xFullTagID & "']"
            End If

            'execute query
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count

            If iLength = 0 Then
                'tag not found - raise error
                Err.Raise(mpErrors.mpError_MatchingItemNotInCollection, , _
                    "<Error_ItemWithIDNotInCollection>" & xFullTagID)
            End If

            'cycle through matches
            For i = 0 To iLength - 1
                'get collection index
                oNode = oNodeList.Item(i)
                oAttribute = oNode.Attributes.GetNamedItem("ColIndex")
                xColIndex = oAttribute.Value

                'find in collection
                oTag = ItemFromColIndex(xColIndex)

                'validate
                If iLength > 1 Then
                    'there are multiple tags with the same id -
                    'stop cycling when we have the right one
                    If xTagTempID <> "" Then
                        'Tag.TempID provided
                        If oTag.TempID = xTagTempID Then _
                            Exit For
                    ElseIf xWordTempID <> "" Then
                        'compare TempID attribute of control
                        'GLOG 4377: Avoid error if WordDoc.DeleteTags has already deleted
                        'the content control associated with this Tag
                        On Error Resume Next
                        xTempID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(Mid$(oTag.Bookmark.Name, 2), _
                            "TempID", oDocument)
                        On Error GoTo ProcError
                        If xTempID <> "" Then
                            If xTempID = xWordTempID Then _
                                Exit For
                        End If
                    ElseIf bAfterDeleteScopeElementChange Then
                        'the original tag is no longer present, and we'll eventually
                        'hit all matching tags, so just return the first match
                        Exit For
                    Else
                        'try identifying by location in doc
                        On Error Resume Next
                        oBookmark2 = oTag.Bookmark
                        If Err().Number = 0 Then
                            If oBookmark2.Range.Start = lStart Then _
                                Exit For
                        Else
                            'control no longer exists - assume that this
                            'is the tag we're looking for
                            Exit For
                        End If
                        On Error GoTo ProcError
                    End If
                End If
            Next i

            'ColIndex property is used only internally and is not always
            'updated following deletions/insertions/moves - update now
            oTag.ColIndex = xColIndex

            'return tag
            GetTag_Bookmark = oTag

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function Update_Bmk(ByVal oBookmark As Word.Bookmark, _
                                  Optional ByVal bAfterDeleteScopeElementChange As Boolean = False, _
                                  Optional ByVal xTagTempID As String = "", _
                                  Optional ByVal xFullTagID As String = "")
            'updates selected properties of the Tag holding oBookmark to reflect
            'the real current values of the underlying attributes - this is useful
            'after 1) an mDel is replaced by an mVar (or vice versa) in an expanded
            'delete scope scenario or 2) we "block" the manual deletion of a tag, which
            'actually involves inserting a replacement for the original tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Update_Bmk"
            Dim oTag As Tag
            Dim xTag As String = ""
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            xTag = Mid$(oBookmark.Name, 2)
            oDoc = oBookmark.Range.Document

            'get Tag corresponding to specified content control
            oTag = GetTag_Bookmark(oBookmark, bAfterDeleteScopeElementChange, _
                xTagTempID, xFullTagID)

            'update
            With oTag
                'element name
                .ElementName = LMP.Forte.MSWord.BaseMethods.GetBaseNameFromTag(xTag)

                'object data
                .ObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)

                If .ElementName = "mSEG" Then
                    'deleted scopes
                    .DeletedScopes = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDoc)

                    'authors
                    .Authors = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(xTag, "Authors", oDoc)
                End If

                .Bookmark = oBookmark

                'tag
                .Tag = xTag
            End With

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Delete_Bookmark(ByVal oBmk As Word.Bookmark)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.Delete_Bookmark"
            Dim xColIndex As String = ""
            Dim oTag As Tag
            Dim oTags As Tags
            Dim lIndex As Integer
            Dim lPos As Integer

            On Error GoTo ProcError

            oTags = Me

            'get Tag corresponding to specified bookmark
            oTag = GetTag_Bookmark(oBmk)

            'find in collection
            xColIndex = oTag.ColIndex
            lPos = InStr(xColIndex, ".")
            While lPos <> 0
                lIndex = CLng(Left$(xColIndex, lPos - 1))
                oTag = oTags.Item(lIndex)
                oTags = oTag.Tags
                xColIndex = Mid$(xColIndex, lPos + 1)
                lPos = InStr(xColIndex, ".")
            End While
            lIndex = CLng(xColIndex)

            'delete
            oTags.RemoveTag(lIndex)

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Property BoundingObjectType As mpBoundingObjectTypes
            Get
                Return m_iBoundingObjectType
            End Get
            Friend Set(value As mpBoundingObjectTypes)
                m_iBoundingObjectType = value
            End Set
        End Property

        Public Sub AddBookmarks()
            'bookmarks all content controls or xml tags in the collection -
            'GLOG 6801 (dm, 5/17/13) - reworked for 10.5 bookmarked mSEGs
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddBookmarks"
            Dim i As Short
            Dim oTag As Tag
            Dim oRange As Word.Range
            Dim xTag As String = ""
            Dim oCC As Word.ContentControl
            Dim oChildCC As Word.ContentControl
            Dim oNode As Word.XMLNode
            Dim oChildNode As Word.XMLNode
            Dim xChildTag As String = ""
            Dim bEcho As Boolean
            Dim oWordApp As WordApp
            Dim oConvert As Conversion
            Dim iLastIndex As Short
            Dim oBmk As Word.Bookmark 'GLOG 6801 (dm)

            On Error GoTo ProcError

            oWordApp = New WordApp
            bEcho = oWordApp.GetEcho()
            oWordApp.Echo(False)

            oConvert = New Conversion

            For i = 1 To m_Col.Count
                oTag = m_Col.Item(i)
                With oTag
                    oRange = Nothing
                    oBmk = .Bookmark

                    'GLOG 6978 (dm) - reinitialize tag - this wasn't happening between an mSEG w/o
                    'bounding object and the next bounding object in the collection
                    xTag = ""

                    If Me.BoundingObjectType = mpBoundingObjectTypes.ContentControls Then
                        'content controls
                        '2/22/11 (dm) - I discovered that I was causing the reverse sequencing
                        'of nested adjacent bookmarks by bookmarking the children before the
                        'parent - this may also be true in the tags branch, and in preconversion
                        'as well, but I wouldn't dare change this on the tags side at this point,
                        'since the retagging/index processing code was built around a certain
                        'expectation - it's also possible that, with this change, it's no longer
                        'necessary to index nested adjacent content controls, but I'm not sure of
                        'this and it can't hurt to add the indexes
                        'GLOG 7115 (dm) - trap for missing content control
                        oCC = Nothing
                        On Error Resume Next
                        oCC = .ContentControl
                        On Error GoTo ProcError

                        If Not oCC Is Nothing Then
                            'bookmark this content control
                            On Error Resume Next
                            oRange = oCC.Range
                            xTag = oCC.Tag
                            On Error GoTo ProcError

                            If xTag <> "" Then
                                oRange.Bookmarks.Add("_" & xTag)
                            End If
                        ElseIf Not oBmk Is Nothing Then
                            'no content control -(WdUnits.wdSectionrange to bookmark
                            'GLOG 6829 (dm) - ignore error if bookmark no longer exists
                            On Error Resume Next
                            oRange = oBmk.Range
                            On Error GoTo ProcError
                        End If

                        If Not oRange Is Nothing Then
                            'bookmark child content controls
                            .Tags.AddBookmarks()

                            'bookmark mSubVars
                            If .ElementName = "mVar" Then
                                For Each oChildCC In oRange.ContentControls
                                    If LMP.Forte.MSWord.BaseMethods.GetBaseName(oChildCC) = "mSubVar" Then
                                        oChildCC.Range.Bookmarks.Add("_" & oChildCC.Tag)
                                    End If
                                Next oChildCC
                            End If

                            'if tag is top-level, index any nested adjacent children
                            If (Me.Parent Is Nothing) And (Not oCC Is Nothing) Then
                                iLastIndex = 0
                                oConvert.IndexNestedAdjacentChildren_CC(oCC, iLastIndex)
                                If iLastIndex > 0 Then
                                    iLastIndex = iLastIndex + 1
                                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TempID", CStr(iLastIndex), "")
                                End If
                            End If
                        End If
                    Else
                        'Bookmark XML Tags if appropriate Reserved Attribute exists
                        oNode = .WordXMLNode
                        If Not oNode Is Nothing Then
                            'use tag
                            oRange = oNode.Range

                            On Error Resume Next
                            xTag = oNode.SelectSingleNode("@Reserved").NodeValue
                            On Error GoTo ProcError
                        ElseIf Not oBmk Is Nothing Then
                            'no tag -(WdUnits.wdSectionrange to bookmark
                            'GLOG 6829 (dm) - ignore error if bookmark no longer exists
                            On Error Resume Next
                            oRange = oBmk.Range
                            On Error GoTo ProcError
                        End If

                        If Not oRange Is Nothing Then
                            'bookmark mSubVars
                            If .ElementName = "mVar" Then
                                For Each oChildNode In oRange.XMLNodes
                                    If oChildNode.BaseName = "mSubVar" Then
                                        xChildTag = ""
                                        On Error Resume Next
                                        xChildTag = oChildNode.SelectSingleNode("@Reserved").NodeValue
                                        On Error GoTo ProcError
                                        If xChildTag <> "" Then _
                                            oChildNode.Range.Bookmarks.Add("_" & xChildTag)
                                    End If
                                Next oChildNode
                            End If

                            'if tag is top-level, index any nested adjacent children
                            If (Me.Parent Is Nothing) And (Not oNode Is Nothing) Then
                                iLastIndex = 0
                                oConvert.IndexNestedAdjacentChildren(oNode, iLastIndex)
                                If iLastIndex > 0 Then
                                    iLastIndex = iLastIndex + 1
                                    LMP.Forte.MSWord.BaseMethods.SetAttributeValue(xTag, "TempID", CStr(iLastIndex), "")
                                End If
                            End If

                            'bookmark child tags
                            .Tags.AddBookmarks()

                            'bookmark this tag
                            If xTag <> "" Then
                                '3/1/11 (dm) - if tag is top-level, expand range to encompass
                                'the start and end tags - this is necessary to avoid a "rich text
                                'can't be applied here" error when attempting to insert the content
                                'control after a .docx save as - do only at runtime, as bookmark
                                'should encompass only the range in the design xml
                                'GLOG 6799 (dm) - added support for tagless mSEGs
                                If Me.GUIDIndexing And (.ElementName = "mSEG") And _
                                       (Not oNode Is Nothing) Then
                                    If oNode.ParentNode Is Nothing Then
                                        oRange.MoveStart(, -1)
                                        oRange.MoveEnd()
                                    End If
                                End If

                                oRange.Bookmarks.Add("_" & xTag)
                            End If
                        End If
                    End If
                End With
            Next i

            oWordApp.Echo(bEcho)
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            oWordApp.Echo(bEcho)
            Exit Sub
        End Sub

        Public Function AddTempID_CC(ByVal oCC As Word.ContentControl) As String
            'adds a temporary GUID for content control and corresponding Tag -
            'returns GUID
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.AddTempID_CC"
            Dim xTempID As String = ""
            Dim oTempID As Word.XMLNode
            Dim oTag As Tag

            On Error GoTo ProcError

            'generate GUID
            xTempID = LMP.Forte.MSWord.BaseMethods.GenerateGUID()

            'add temp id attribute
            LMP.Forte.MSWord.BaseMethods.SetAttributeValue(oCC.Tag, "TempID", xTempID, "", oCC.Range.Document)

            'get Tag
            oTag = GetTag_CC(oCC)

            'add temp id to Tag
            oTag.TempID = xTempID

            'return temp id
            AddTempID_CC = xTempID

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub MoveContentControl(ByRef oCC As Word.ContentControl, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.MoveContentControl"

            'move content control
            LMP.Forte.MSWord.BaseMethods.MoveContentControl(oCC, oNewRange)

            'update tag in collection
            Me.Update_CC(oCC)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub MoveBookmark(ByRef oBmk As Word.Bookmark, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.MoveBookmark"

            'move bookmark
            LMP.Forte.MSWord.BaseMethods.MoveBookmark(oBmk, oNewRange)

            'update tag in collection
            Me.Update_Bmk(oBmk)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Function GetWordXMLNodes(ByVal xXPath As String) As Object
            'returns an array of content controls as specified by the supplied query -
            'compensates for the inability to use XPath to find content controls directly as
            'we do with xml nodes
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetWordXMLNodes"
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim i As Short
            Dim iLength As Short
            Dim oTag As Tag
            Dim xColIndex As String = ""
            Dim oAttribute As xml.xmlAttribute
            Dim oNodeArray() As Word.XMLNode

            On Error Resume Next

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'execute query
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count
            If iLength > 0 Then
                ReDim oNodeArray(iLength - 1)
                For i = 0 To iLength - 1
                    oNode = oNodeList.Item(i)

                    'find Tag in collection
                    oAttribute = oNode.attributes.getNamedItem("ColIndex")
                    xColIndex = oAttribute.value
                    oTag = ItemFromColIndex(xColIndex)

                    'add content control to array
                    oNodeArray(i) = oTag.WordXMLNode
                Next i
            Else
                'return empty array
                ReDim oNodeArray(0)
            End If

            GetWordXMLNodes = oNodeArray

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function GetContentControls(ByVal xXPath As String) As Object
            'returns an array of content controls as specified by the supplied query -
            'compensates for the inability to use XPath to find content controls directly as
            'we do with xml nodes
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.GetContentControls"
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim i As Short
            Dim iLength As Short
            Dim oTag As Tag
            Dim xColIndex As String = ""
            Dim oAttribute As xml.xmlAttribute
            Dim oCCArray() As Word.ContentControl

            On Error Resume Next

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'execute query
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count
            If iLength > 0 Then
                ReDim oCCArray(iLength - 1)
                For i = 0 To iLength - 1
                    oNode = oNodeList.Item(i)

                    'find Tag in collection
                    oAttribute = oNode.attributes.getNamedItem("ColIndex")
                    xColIndex = oAttribute.value
                    oTag = ItemFromColIndex(xColIndex)

                    'add content control to array
                    oCCArray(i) = oTag.ContentControl
                Next i
            Else
                'return empty array
                ReDim oCCArray(0)
            End If

            GetContentControls = oCCArray

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function MoveExternalChildren_CC(ByVal oCC As Word.ContentControl) As Integer
            'moves any external children contained in the Tag corresponding to
            'oCC to the Tag of a different part of the same segment or,
            'if there are no other parts, to the top level of the collection - this
            'method is called before an mSEG is deleted - returns 1 if any children
            'have been moved, else 0
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.MoveExternalChildren_CC"
            Dim oSource As Tag
            Dim oSourceTags As Tags
            Dim oDest As Tag
            Dim oDestTags As Tags
            Dim i As Integer
            Dim oChild As Tag
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim xParentTagID As String = ""
            Dim xXPath As String = ""
            Dim oNodeList As xml.xmlNodeList
            Dim iLength As Short
            Dim oNode As xml.xmlNode
            Dim oAttribute As xml.xmlAttribute
            Dim xTagID As String = ""
            Dim xObjectData As String = ""
            Dim bValidateTagID As Boolean

            On Error GoTo ProcError

            'only segments can have external children
            If LMP.Forte.MSWord.BaseMethods.GetBaseName(oCC) <> "mSEG" Then _
                Exit Function

            'get Tag corresponding to specified WordXMLNode
            oSource = GetTag_CC(oCC)

            'check whether segment has external children - if not, exit
            xObjectData = oSource.ObjectData
            lPos = InStr(xObjectData, "ExternalChildren=")
            If lPos > 0 Then
                lPos = lPos + 17
                lPos2 = InStr(lPos, xObjectData, "|")
                If UCase(Mid$(xObjectData, lPos, lPos2 - lPos)) = "FALSE" Then
                    Exit Function
                End If
            Else
                Exit Function
            End If

            'load xml summary of collection if necessary
            If m_oSummaryXML Is Nothing Then
                m_oSummaryXML = New xml.xmlDocument
                m_oSummaryXML.loadXML(Me.XMLSummary)
            End If

            'check whether there's another part available
            xTagID = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(oCC.Tag, "TagID")
            xXPath = "//mSEG[@TagID='" & xTagID & "']"
            oNodeList = m_oSummaryXML.selectNodes(xXPath)
            iLength = oNodeList.Count
            If iLength > 1 Then
                'get Tag corresponding to the other part
                For i = 0 To iLength - 1
                    oNode = oNodeList.Item(i)
                    oAttribute = oNode.Attributes.GetNamedItem("Part")
                    If oAttribute.Value <> oSource.Part Then
                        'this is a different part - find it in collection
                        oAttribute = oNode.Attributes.GetNamedItem("ColIndex")
                        oDest = ItemFromColIndex(oAttribute.Value)
                        'set target tags collection
                        oDestTags = oDest.Tags
                        Exit For
                    End If
                Next i
            End If

            'no other parts found,(WdUnits.wdSectiontarget collection to top level
            If oDestTags Is Nothing Then
                oDestTags = Me
                bValidateTagID = True
            End If

            'move external children from oSource to oDest
            oSourceTags = oSource.Tags
            For i = oSourceTags.Count To 1 Step -1
                oChild = oSourceTags.Item(i)
                If oChild.ElementName = "mSEG" Then
                    'if oChild is external, it will have a parent tag id
                    xObjectData = oChild.ObjectData
                    lPos = InStr(xObjectData, "ParentTagID=")
                    If lPos > 0 Then
                        lPos = lPos + 12
                        lPos2 = InStr(lPos, xObjectData, "|")
                        'JTS 8/18/16: May be last item in ObjectData
                        If (lPos2 = 0) Then
                            lPos2 = Len(xObjectData) + 1
                        End If
                        xParentTagID = Mid$(xObjectData, lPos, lPos2 - lPos)

                        'if this is an external child of this segment, move
                        'it to the other part
                        If xParentTagID = oSource.TagID Then
                            'add to oDest
                            oDestTags.AddFamilyMember(oChild, bValidateTagID)
                            'remove from oSource
                            oSourceTags.RemoveTag(i)
                            'set function return value
                            MoveExternalChildren_CC = 1
                        End If
                    End If
                End If
            Next i

            'force summary to update on next request
            m_xXMLSummary = ""
            m_oSummaryXML = Nothing

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Friend Function DeleteInvalidTag(oTag As Word.XMLNode) As mpInvalidTagsRemovedTypes
            '12/27/10 JTS: If Tag is Invalid (Missing Schema or Invalid BaseName for MP10 Schema)
            'Delete Tag and any child tags
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.DeleteInvalidTag"
            Dim oWordDoc As WordDoc
            Dim i As Short
            Dim xBaseName As String = ""
            Dim xObjectData As String = ""
            Dim xTagID As String = ""

            DeleteInvalidTag = mpInvalidTagsRemovedTypes.None
            On Error GoTo ProcError
            '5/3/11 (dm) - do not run if track changes is on, as deleted tag will
            'remain in the document, causing an endless loop in CollectTags - we now
            'ignore deleted tags in any case
            If oTag.Range.Document.TrackRevisions Then _
                Exit Function

            oWordDoc = New WordDoc
            If oWordDoc.TagIsInvalid(oTag) Then
                On Error Resume Next
                xBaseName = oTag.BaseName
                xTagID = oTag.SelectSingleNode("@TagID").NodeValue
                xObjectData = oTag.SelectSingleNode("@ObjectData").NodeValue
                If xObjectData <> "" Then _
                    xObjectData = LMP.Forte.MSWord.BaseMethods.Decrypt(xObjectData)
                On Error GoTo ProcError
                If UCase(xBaseName) = "MBLOCK" Or UCase(xBaseName) = "MVAR" Or UCase(xBaseName) = "MDEL" Then
                    If oTag.ParentNode Is Nothing Then
                        'Invalid tag is a standalone Block or Variable without parent mSEG
                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.StandaloneTags
                    Else
                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                    End If
                ElseIf UCase(xBaseName) = "MSEG" And xObjectData <> "" Then
                    '12/27/10: Check ObjectData for Trailer ObjectTypeID
                    If InStr(xObjectData, "ObjectTypeID=401") > 0 Then
                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.TrailerTagsOnly
                    ElseIf InStr(xObjectData, "ObjectTypeID=") = 0 Then
                        'ObjectData attribute may have been corrupted so that it won't decrypt properly.
                        'Check other environmental factors that could indicate if this is a Trailer tag that is being
                        'deleted - is Tag inside a Textbox? Is Style "Forte Trailer"?  Does TagID start with
                        'Footer or EndOfDocument?
                        If oTag.Range.StoryType = wdStoryType.wdTextFrameStory Then
                            If InStr(UCase(xTagID), "FOOTER") = 1 _
                                Or InStr(UCase(xTagID), "ENDOFDOCUMENT") = 1 Then
                                DeleteInvalidTag = mpInvalidTagsRemovedTypes.TrailerTagsOnly
                            Else
                                'GLOG 5482:  Range.Style may return nothing if character styles are applied
                                If Not oTag.Range.Style Is Nothing Then
                                    If oTag.Range.Style = "Forte Trailer" Then
                                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.TrailerTagsOnly
                                    Else
                                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                                    End If
                                Else
                                    DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                                End If
                            End If
                        Else
                            DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                        End If
                    Else
                        DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                    End If
                Else
                    DeleteInvalidTag = mpInvalidTagsRemovedTypes.NonTrailerTags
                End If
                For i = oTag.Range.XMLNodes.Count To 1 Step -1
                    oTag.Range.XMLNodes(i).Delete()
                Next i
                oTag.Delete()
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function

        End Function
        Public Property InvalidTagsDeleted As mpInvalidTagsRemovedTypes
            Set(value As mpInvalidTagsRemovedTypes)
                m_iInvalidTagsRemoved = value
            End Set
            Get
                Return m_iInvalidTagsRemoved
            End Get
        End Property

        Friend Function DeleteInvalidContentControl(oCC As Word.ContentControl) As mpInvalidTagsRemovedTypes
            '10/19/12 (dm): deletes cc and children when there's no readable object data
            Const mpThisFunction As String = "LMP.Forte.MSWord.Tags.DeleteInvalidContentControl"
            Dim xBaseName As String = ""
            Dim xObjectData As String = ""
            Dim oWordDoc As WordDoc

            On Error GoTo ProcError

            DeleteInvalidContentControl = mpInvalidTagsRemovedTypes.None

            xBaseName = LMP.Forte.MSWord.BaseMethods.GetBaseName(oCC)

            'exclude non-Forte ccs
            If xBaseName = "" Then _
                Exit Function

            On Error Resume Next
            xObjectData = LMP.Forte.MSWord.BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData")
            On Error GoTo ProcError

            If xObjectData = "" Then
                'set return value
                If UCase(xBaseName) = "MBLOCK" Or UCase(xBaseName) = "MVAR" Or UCase(xBaseName) = "MDEL" Then
                    If oCC.ParentContentControl Is Nothing Then
                        DeleteInvalidContentControl = mpInvalidTagsRemovedTypes.StandaloneTags
                    Else
                        DeleteInvalidContentControl = mpInvalidTagsRemovedTypes.NonTrailerTags
                    End If
                Else
                    DeleteInvalidContentControl = mpInvalidTagsRemovedTypes.NonTrailerTags
                End If

                'delete cc and children
                oWordDoc = New WordDoc
                oWordDoc.RemoveContentControls(oCC, mpSegmentTagRemovalOptions.All, True)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function

        End Function
        Private Function SegmentTagExists(xTag) As Boolean 'GLOG 7363
            Dim i As Short
            For i = 1 To m_ColSegBmks.Count
                If m_ColSegBmks.Item(i) = xTag Then
                    SegmentTagExists = True
                    Exit Function
                End If
            Next i
            SegmentTagExists = False
        End Function
    End Class
End Namespace
