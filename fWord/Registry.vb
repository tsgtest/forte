Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Registry
        Public Shared Function ForteRegistryRoot() As String
            ForteRegistryRoot = "Software\The Sackett Group\Deca"
        End Function
        Public Shared Function GetForteValue(ByVal Key As String) As String
            Dim xRet As String = ""
            'GLOG 8438
            xRet = RegAPI.GetKeyValue(RegAPI.HKEY_LOCAL_MACHINE, ForteRegistryRoot(), Key)
            'GetValue(hKey.HKEY_LOCAL_MACHINE, ForteRegistryRoot(), Key, xRet)

            GetForteValue = xRet
        End Function
        Public Shared Sub SetForteValue(ByVal Key As String, ByVal Value As String)
            'GLOG 8438
            RegAPI.UpdateKey(RegAPI.HKEY_LOCAL_MACHINE, ForteRegistryRoot(), Key, Value)
            'SetValue(hKey.HKEY_LOCAL_MACHINE, ForteRegistryRoot(), Key, Reg.REG_SZ, Value)
        End Sub
    End Class
    Friend Class RegAPI
        '---------------------------------------------------------------
        '-Registry API Declarations...
        '---------------------------------------------------------------
        Private Declare Function RegCloseKey Lib "advapi32" (ByVal hKey As Integer) As Integer
        Private Declare Function RegCreateKeyEx Lib "advapi32" Alias "RegCreateKeyExA" (ByVal hKey As Integer, ByVal lpSubKey As String, ByVal Reserved As Integer, ByVal lpClass As String, ByVal dwOptions As Integer, ByVal samDesired As Integer, ByRef lpSecurityAttributes As SECURITY_ATTRIBUTES, ByRef phkResult As Integer, ByRef lpdwDisposition As Integer) As Integer
        Private Declare Function RegOpenKeyEx Lib "advapi32" Alias "RegOpenKeyExA" (ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer
        Private Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByVal lpData As String, ByRef lpcbData As Integer) As Integer
        Private Declare Function RegSetValueEx Lib "advapi32" Alias "RegSetValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal Reserved As Integer, ByVal dwType As Integer, ByVal lpData As String, ByVal cbData As Integer) As Integer
        Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
        Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

        '---------------------------------------------------------------
        '- Registry Api Constants...
        '---------------------------------------------------------------
        ' Reg Data Types...
        Const REG_SZ = 1                         ' Unicode nul terminated string
        Const REG_EXPAND_SZ = 2                  ' Unicode nul terminated string
        Const REG_DWORD = 4                      ' 32-bit number

        ' Reg Create Type Values...
        Const REG_OPTION_NON_VOLATILE = 0       ' Key is preserved when system is rebooted

        ' Reg Key Security Options...
        Const READ_CONTROL = &H20000
        Const KEY_QUERY_VALUE = &H1
        Const KEY_SET_VALUE = &H2
        Const KEY_CREATE_SUB_KEY = &H4
        Const KEY_ENUMERATE_SUB_KEYS = &H8
        Const KEY_NOTIFY = &H10
        Const KEY_CREATE_LINK = &H20
        Const KEY_READ = KEY_QUERY_VALUE + KEY_ENUMERATE_SUB_KEYS + KEY_NOTIFY + READ_CONTROL
        Const KEY_WRITE = KEY_SET_VALUE + KEY_CREATE_SUB_KEY + READ_CONTROL
        Const KEY_EXECUTE = KEY_READ
        Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + _
                               KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + _
                               KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL

        ' Reg Key ROOT Types...
        Public Const HKEY_CLASSES_ROOT = &H80000000
        Public Const HKEY_CURRENT_USER = &H80000001
        Public Const HKEY_LOCAL_MACHINE = &H80000002
        Public Const HKEY_USERS = &H80000003
        Public Const HKEY_PERFORMANCE_DATA = &H80000004

        ' Return Value...
        Const ERROR_NONE = 0
        Const ERROR_BADDB = 1
        Const ERROR_BADKEY = 2
        Const ERROR_CANTOPEN = 3
        Const ERROR_CANTREAD = 4
        Const ERROR_CANTWRITE = 5
        Const ERROR_OUTOFMEMORY = 6
        Const ERROR_INVALID_PARAMETER = 7
        Const ERROR_ACCESS_DENIED = 8
        Const ERROR_INVALID_PARAMETERS = 87
        Const ERROR_NO_MORE_ITEMS = 259
        Const ERROR_SUCCESS = 0&

        '---------------------------------------------------------------
        '- Registry Security Attributes TYPE...
        '---------------------------------------------------------------
        Private Structure SECURITY_ATTRIBUTES
            Dim nLength As Integer
            Dim lpSecurityDescriptor As Integer
            Dim bInheritHandle As Boolean
        End Structure
        '-------------------------------------------------------------------------------------------------
        'sample usage - Debug.Print UpodateKey(hKey.HKEY_CLASSES_ROOT, "keyname", "newvalue")
        '-------------------------------------------------------------------------------------------------
        Public Shared Function UpdateKey(KeyRoot As Integer, KeyName As String, SubKeyName As String, SubKeyValue As String) As Boolean
            Dim rc As Integer                                      ' Return Code
            Dim hKey As Integer                                    ' Handle To A Registry Key
            Dim hDepth As Integer                                  '
            Dim lpAttr As SECURITY_ATTRIBUTES                   ' Registry Security Type

            lpAttr.nLength = 50                                 '(WdUnits.wdSectionSecurity Attributes To Defaults...
            lpAttr.lpSecurityDescriptor = 0                     ' ...
            lpAttr.bInheritHandle = True                        ' ...

            '------------------------------------------------------------
            '- Create/Open Registry Key...
            '------------------------------------------------------------
            rc = RegCreateKeyEx(KeyRoot, KeyName, _
                                0, REG_SZ, _
                                REG_OPTION_NON_VOLATILE, KEY_WRITE, lpAttr, _
                                hKey, hDepth)                   ' Create/Open //KeyRoot//KeyName

            If (rc <> ERROR_SUCCESS) Then GoTo CreateKeyError ' Handle Errors...

            '------------------------------------------------------------
            '- Create/Modify Key Value...
            '------------------------------------------------------------
            If (SubKeyValue = "") Then SubKeyValue = " " ' A Space Is Needed For RegSetValueEx() To Work...

            ' Create/Modify Key Value
            rc = RegSetValueEx(hKey, SubKeyName, _
                               0, REG_SZ, _
                               SubKeyValue, Strings.Len(System.Text.Encoding.Unicode.GetBytes(SubKeyValue)))

            If (rc <> ERROR_SUCCESS) Then GoTo CreateKeyError ' Handle Error
            '------------------------------------------------------------
            '- Close Registry Key...
            '------------------------------------------------------------
            rc = RegCloseKey(hKey)                              ' Close Key

            UpdateKey = True                                    ' Return Success
            Exit Function                                       ' Exit
CreateKeyError:
            UpdateKey = False                                   '(WdUnits.wdSectionError Return Code
            rc = RegCloseKey(hKey)                              ' Attempt To Close Key
        End Function
        '-------------------------------------------------------------------------------------------------
        'sample usage - Debug.Print GetKeyValue(hKey.HKEY_CLASSES_ROOT, "COMCTL.ListviewCtrl.1\CLSID", "")
        '-------------------------------------------------------------------------------------------------
        Public Shared Function GetKeyValue(KeyRoot As Integer, KeyName As String, SubKeyRef As String) As String
            Dim i As Integer = 0                                          ' Loop Counter
            Dim rc As Integer = 0                                      ' Return Code
            Dim hKey As Integer = 0                                   ' Handle To An Open Registry Key
            Dim sKeyVal As String = ""
            Dim lKeyValType As Integer = 0                                ' Data Type Of A Registry Key
            Dim tmpVal As String = ""                               ' Tempory Storage For A Registry Key Value
            Dim KeyValSize As Integer = 0                               ' Size Of Registry Key Variable

            ' Open RegKey Under KeyRoot {hKey.HKEY_LOCAL_MACHINE...}
            '------------------------------------------------------------
            rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_READ, hKey) ' Open Registry Key

            If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError ' Handle Error...

            tmpVal = New String(Chr(0), 1024)                             ' Allocate Variable Space
            KeyValSize = 1024                                       ' Mark Variable Size

            '------------------------------------------------------------
            ' Retrieve Registry Key Value...
            '------------------------------------------------------------
            rc = RegQueryValueEx(hKey, SubKeyRef, 0, _
                                 lKeyValType, tmpVal, KeyValSize)    ' Get/Create Key Value

            If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError ' Handle Errors

            tmpVal = Left$(tmpVal, InStr(tmpVal, Chr(0)) - 1)

            '------------------------------------------------------------
            ' Determine Key Value Type For Conversion...
            '------------------------------------------------------------
            Select Case lKeyValType                                  ' Search Data Types...
                Case REG_SZ, REG_EXPAND_SZ                              ' String Registry Key Data Type
                    sKeyVal = tmpVal                                     ' Copy String Value
                Case REG_DWORD                                          ' Double Word Registry Key Data Type
                    For i = Len(tmpVal) To 1 Step -1                    ' Convert Each Bit
                        sKeyVal = sKeyVal + Hex(Asc(Mid(tmpVal, i, 1)))   ' Build Value Char. By Char.
                    Next
                    sKeyVal = Format$("&h" + sKeyVal)                     ' Convert Double Word To String
            End Select

            GetKeyValue = sKeyVal                                   ' Return Value
            rc = RegCloseKey(hKey)                                  ' Close Registry Key
            Exit Function                                           ' Exit

GetKeyError:  ' Cleanup After An Error Has Occured...
            GetKeyValue = vbNullString                              '(WdUnits.wdSectionReturn Val To "" String
            rc = RegCloseKey(hKey)                                  ' Close Registry Key
        End Function
    End Class
End Namespace