Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports System.Drawing.Printing
Imports System.Drawing.Printing.PrinterSettings

Namespace LMP.Forte.MSWord
    Public Class Printer
        'GLOG 8680: Save these values between calls,
        'so that arrays don't need to be repopulated unless printer has changed
        Shared m_arBinNumbers() As Integer = Nothing
        Shared m_arBinNames() As String = Nothing
        Shared m_xPrinterName As String = ""
        Public Function GetPrinterName() As String
            'If Word is running use ActivePrinter, otherwise use system default printer
            If MSWord.Application.WordIsRunning() Then
                Try
                    'GLOG 8917: Don't update Shared variable here
                    Return GlobalMethods.CurWordApp.ActivePrinter
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim oSettings As PrinterSettings = New PrinterSettings()
                    'GLOG 8917: Don't update Shared variable here
                    Return oSettings.PrinterName
                Catch
                End Try
            End If
            Return ""
        End Function
        Public Sub GetBinNamesAndNumbers(ByRef arBins As String(), ByRef arIDs As Integer())
            Const mpThisFunction As String = "LMP.Forte.MSWord.Printer.GetBinNames"
            Dim oPrnDoc As PrintDocument
            Dim i As Integer

            On Error GoTo ProcError
            Dim xCurPrinter As String = GetPrinterName()

            If xCurPrinter <> "" Then
                If xCurPrinter <> m_xPrinterName Or m_arBinNames Is Nothing Or m_arBinNumbers Is Nothing Then
                    'GLOG 15911: Speed up execution by limiting number of times PaperSources object needs to be accessed
                    Dim oSettings As PrinterSettings = New PrinterSettings()
                    Dim oSources As PaperSourceCollection
                    Dim oSource As PaperSource
                    Dim iCount As Integer
                    oSettings.PrinterName = xCurPrinter
                    oSources = oSettings.PaperSources
                    iCount = oSources.Count
                    ReDim m_arBinNumbers(0 To iCount - 1)
                    ReDim m_arBinNames(0 To iCount - 1)
                    i = 0
                    For Each oSource In oSources
                        m_arBinNames(i) = oSources(i).SourceName
                        m_arBinNumbers(i) = oSources(i).RawKind
                        i = i + 1
                    Next
                    m_xPrinterName = xCurPrinter
                End If
                arBins = m_arBinNames
                arIDs = m_arBinNumbers
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Sub SetPaperTray(ByVal oRange As Word.Range, ByVal iTrayID As Integer)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Printer.SetPaperTray"
            On Error GoTo ProcError

            With oRange.PageSetup
                .FirstPageTray = iTrayID
                .OtherPagesTray = iTrayID
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
    End Class
End Namespace