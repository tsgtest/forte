''**********************************************************
'   LMP.Forte.MSWord.mpBase
'   created 6/30/04 by Doug Miller
'
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports System.Runtime.InteropServices

'API functions
Namespace LMP.Forte.MSWord
    Public Enum mpFileFormats
        None = 0
        Binary = 1
        OpenXML = 2
    End Enum

    Friend Enum mpVariableProperties
        mpVariableProperty_ID = 1
        mpVariableProperty_Name = 2
        mpVariableProperty_DisplayName = 3
        mpVariableProperty_TranslationID = 4
        mpVariableProperty_ExecutionIndex = 5
        mpVariableProperty_DefaultValue = 6
        mpVariableProperty_ValueSourceExpression = 7
        mpVariableProperty_ReserveValue = 8
        mpVariableProperty_Reserved = 9
        mpVariableProperty_ControlType = 10
        mpVariableProperty_ControlProperties = 11
        mpVariableProperty_ValidationCondition = 12
        mpVariableProperty_ValidationResponse = 13
        mpVariableProperty_Description = 14
        mpVariableProperty_VariableActionsToString = 15
        mpVariableProperty_ControlActionsToString = 16
        mpVariableProperty_RequestCIForEntireSegment = 17
        mpVariableProperty_CIContactType = 18
        mpVariableProperty_CIDetailTokenString = 19
        mpVariableProperty_CIAlerts = 20
        mpVariableProperty_DisplayIn = 21
        mpVariableProperty_DisplayLevel = 22
        mpVariableProperty_DisplayValue = 23
        mpVariableProperty_Hotkey = 24
        mpVariableProperty_MustVisit = 25
        mpVariableProperty_AllowPrefillOverride = 26
        mpVariableProperty_DistributedDetail = 27
        mpVariableProperty_AssociatedPrefillNames = 28
        mpVariableProperty_TabNumber = 29
        mpVariableProperty_CIEntityName = 30
        mpVariableProperty_RuntimeControlValues = 31
        mpVariableProperty_NavigationTag = 32
        mpVariableProperty_ObjectDatabaseID = 33
    End Enum

    Friend Enum mpBlockProperties
        mpBlockProperty_Name = 1
        mpBlockProperty_DisplayName = 2
        mpBlockProperty_TranslationID = 3
        mpBlockProperty_ShowInTree = 4
        mpBlockProperty_IsBody = 5
        mpBlockProperty_Description = 6
        mpBlockProperty_Hotkey = 7
        mpBlockProperty_DisplayLevel = 8
        mpBlockProperty_StartingText = 9
        mpBlockProperty_ObjectDatabaseID = 10
    End Enum

    Friend Enum mpSubVariableProperties
        mpSubVarProperty_Index = 1
        mpSubVarProperty_UNID = 2
        mpSubVarProperty_Format = 3
        mpSubVarProperty_Default = 4
    End Enum

    Friend Enum mpEncryptionStatus
        mpEncryptionStatus_Uninitialized = 0
        mpEncryptionStatus_On = 1
        mpEncryptionStatus_Off = 2
    End Enum

    Friend Class BaseMethods
        <DllImport("zlibwapi.dll", EntryPoint:="compress", CharSet:=CharSet.Unicode)> _
        Friend Shared Function compress(ByVal dest As Byte(), ByRef destLen As Integer, ByVal src As Byte(), ByVal srcLen As Integer) As Integer
        End Function
        <DllImport("zlibwapi.dll", EntryPoint:="uncompress", CharSet:=CharSet.Unicode)> _
        Friend Shared Function uncompress(ByVal dest As Byte(), ByRef destLen As Integer, ByVal src As Byte(), ByVal srcLen As Integer) As Integer
        End Function

        'Private Declare Function compress Lib "zlibwapi.dll" (dest As Byte(), ByVal destLen As Integer, src As Byte(), ByVal srcLen As Integer) As Integer
        'Private Declare Function uncompress Lib "zlibwapi.dll" (dest As Byte(), destLen As Integer, src As Byte(), ByVal srcLen As Integer) As Integer

        Private Const MAX_PATH As Integer = 260

        Private Shared m_iEncryptionStatus As mpEncryptionStatus
        Public Const INTERNAL_ENCRYPT_PASSWORD As String = "^M�s�PQ]"
        Public Const mpAlgorithmIdentifier As String = "#mp!@"

        Public Shared Function GetCompressionMarker() As String
            'returns the marker that gets prepended
            'to a compressed string
            GetCompressionMarker = ChrW(&H2514) & ChrW(&H2524)
        End Function
        Public Shared Function GetEncryptionMarker() As String
            'returns the marker that get prepended
            'to an encrypted string
            GetEncryptionMarker = "^`~"
        End Function
        Public Shared Function CountChrs(ByVal xSource As String, ByVal xSearch As String) As Integer
            'returns the number of instances
            'of xSearch in xSource
            Dim lPos As Integer = 0
            Dim i As Integer

            On Error GoTo ProcError
            lPos = InStr(xSource, xSearch)
            While lPos
                i = i + 1
                lPos = InStr(lPos + 1, xSource, xSearch)
            End While
            CountChrs = i
            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.mpBase.CountChrs")
            Exit Function
        End Function

        Public Shared Function Max(ByVal dNum1 As Double, ByVal dNum2 As Double) As Double
            On Error GoTo ProcError
            If dNum1 > dNum2 Then
                Max = dNum1
            Else
                Max = dNum2
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.Max")
            Exit Function
        End Function

        Public Shared Function Min(ByVal dNum1 As Double, ByVal dNum2 As Double) As Double
            On Error GoTo ProcError
            If dNum1 > dNum2 Then
                Min = dNum2
            Else
                Min = dNum1
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.Min")
            Exit Function
        End Function

        Public Shared Function ConvertDataTypeKeywords(ByVal vVar As Object) As Object
            'adjusts db values for CallByName
            If Trim(vVar) = "mpTrue" Then
                ConvertDataTypeKeywords = True
            ElseIf Trim(vVar) = "mpFalse" Then
                ConvertDataTypeKeywords = False
            ElseIf IsNumeric(vVar) Then
                'Make sure Numeric arguments are not handled as strings
                ConvertDataTypeKeywords = CDbl(vVar)
            Else
                ConvertDataTypeKeywords = vVar
            End If
        End Function

        Sub SplitString(ByVal xString As String, _
                              xStr1 As String, _
                              xStr2 As String, _
                              Optional ByVal xSep As String = vbCrLf, _
                              Optional ByVal iCount As Short = 0)
            'splits a string in half, filling xStr1 and xStr2 with the results
            Dim lHalf As Integer = 0
            Dim i As Integer = 0
            Dim lPos As Integer = 0

            On Error GoTo ProcError

            '***clean string
            TrimChrs(xString, xSep, True, True)

            If iCount = 0 Then
                If InStr(xString, xSep) = 0 Then
                    xStr1 = xString
                    Exit Sub
                Else
                    iCount = BaseMethods.CountChrs(xString, xSep) + 1
                End If
            End If

            If (iCount Mod 2) Then
                lHalf = (iCount / 2) + 0.5
            Else
                lHalf = (iCount / 2)
            End If

            For i = 1 To lHalf
                lPos = InStr(lPos + Len(xSep), xString, xSep)
            Next i

            xStr1 = Left(xString, lPos - 1)
            xStr2 = Mid(xString, lPos + Len(xSep))
            Exit Sub
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.SplitString")
            Exit Sub
        End Sub

        Public Shared Function TrimChrs(ByVal xText As String, _
                          Optional ByVal xChr As String = "", _
                          Optional ByVal bTrimStart As Boolean = False, _
                          Optional ByVal bTrimEnd As Boolean = True) As String
            'removes all instances of xChr from start or end of xText;
            'if xChr = "", then trims last character
            On Error GoTo ProcError
            If xChr <> "" Then
                If bTrimStart Then
                    While Left(xText, Len(xChr)) = xChr
                        xText = Mid(xText, Len(xChr) + 1)
                    End While
                End If
                If bTrimEnd Then
                    While Right(xText, Len(xChr)) = xChr
                        xText = Left(xText, Len(xText) - Len(xChr))
                    End While
                End If
            Else
                xText = Left(xText, Len(xText) - 1)
            End If

            TrimChrs = xText
            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.TrimChrs")
            Exit Function
        End Function

        Public Shared Function GenerateGUID(Optional bUpperCase As Boolean = False) As String
            Dim NewGUID As Guid
            Dim xNewGuid As String = ""
            Dim iChars As Short = 0
            Dim lReturn As Integer = 0

            On Error GoTo ProcError
            NewGUID = Guid.NewGuid()
            xNewGuid = "{" & NewGUID.ToString().ToUpper() & "}"

            'lReturn = CoCreateGuid(NewGUID)

            'If (lReturn <> 0) Then _
            '    GoTo ProcError

            '' convert binary GUID to string form
            'iChars = StringFromGUID2(NewGUID, xNewGuid, Len(xNewGuid))
            '' convert string to ANSI
            'xNewGuid = System.Text.Encoding.Unicode.GetBytes(xNewGuid).ToString()
            '' remove trailing null character
            'xNewGuid = Left(xNewGuid, iChars - 1)
            '' MSI likes only uppercase letters in GUID
            'If bUpperCase Then _
            '    xNewGuid = UCase(xNewGuid)

            GenerateGUID = xNewGuid

            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.GenerateGUID")
            Exit Function
        End Function

        Shared Function GetInstancePosition(ByVal xSource As String, _
                                     ByVal xSearch As String, _
                                     ByVal lInstance As Integer) As Integer
            'returns the position of the specified instance of xSource;
            'if specified instance does not exist returns 0
            Dim lPos As Integer = 0
            Dim i As Integer = 0
            Dim lv As Integer = 0

            '---validate lInstances
            On Error GoTo ProcError
            lv = BaseMethods.CountChrs(xSource, xSearch)
            If lv < lInstance Then
                GetInstancePosition = 0
                Exit Function
            End If

            lPos = InStr(xSource, xSearch)

            While lPos
                i = i + 1
                If i = lInstance Then GoTo wExit
                lPos = InStr(lPos + 1, xSource, xSearch)
            End While
wExit:
            GetInstancePosition = lPos
            Exit Function
ProcError:
            GlobalMethods.RaiseError("LMP.Forte.MSWord.BaseMethods.GetInstancePosition")
            Exit Function
        End Function

        Public Shared Function GetVariableDefinition(ByVal xObjectData As String, _
                                              ByVal xVariableName As String) As String
            'extracts the definition for xVariableName from xObjectData
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetVariableDefinition"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xDef As String = ""
            Dim xTest As String = ""

            On Error GoTo ProcError

            lPos = 1
            While xDef = ""
                lPos = InStr(lPos, xObjectData, "�" & xVariableName & "�")
                If lPos = 0 Then Exit Function
                lPos2 = InStrRev(xObjectData, "VariableDefinition=", lPos)
                If lPos2 = 0 Then Exit Function
                lPos3 = InStr(lPos2, xObjectData, "|")
                If lPos3 = 0 Then Exit Function
                xTest = Mid$(xObjectData, lPos2, (lPos3 - lPos2) + 1)
                If GetInstancePosition(xTest, "�", mpVariableProperties.mpVariableProperty_Name - 1) = _
                        lPos - lPos2 + 1 Then
                    'definition is valid
                    xDef = xTest
                Else
                    'try again
                    lPos = lPos + 1
                End If
            End While
            GetVariableDefinition = xDef

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub SetVariablePropertyValue(ByVal oNode As Word.XMLNode, _
                                            ByVal xVariableName As String, _
                                            ByVal iPropertyIndex As mpVariableProperties, _
                                            ByVal xValue As String)
            'sets the value of the specified property of xVariableName
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.SetVariablePropertyValue"
            Dim xOD As String = ""
            Dim xPassword As String = ""

            On Error GoTo ProcError

            'get object data
            Dim oObjectData As Word.XMLNode = oNode.SelectSingleNode("@ObjectData")
            xOD = BaseMethods.Decrypt(oObjectData.NodeValue, xPassword)

            'modify definition
            SetVariableObjectDataValue(xOD, xVariableName, iPropertyIndex, xValue)

            'update node
            oObjectData = oNode.SelectSingleNode("@ObjectData")
            oObjectData.NodeValue = Encrypt(xOD, xPassword)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub SetVariableObjectDataValue(ByRef xObjectData As String, _
                                              ByVal xVariableName As String, _
                                              ByVal iPropertyIndex As mpVariableProperties, _
                                              ByVal xValue As String)
            'sets the value of the specified property of xVariableName
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.SetVariableObjectDataValue"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xDef As String = ""
            Dim vParts As Object = Nothing

            On Error GoTo ProcError

            'get definition
            xDef = GetVariableDefinition(xObjectData, xVariableName)
            vParts = Split(xObjectData, xDef)

            'Replace invalid text characters
            xValue = Replace(xValue, Chr(11), "\v")
            xValue = Replace(xValue, Chr(13), "\r\n")

            'set property value
            lPos = GetInstancePosition(xDef, "�", iPropertyIndex - 1)
            lPos2 = InStr(lPos + 1, xDef, "�")
            If lPos2 = 0 Then _
                lPos2 = InStr(lPos + 1, xDef, "|")
            xDef = Left$(xDef, lPos) & xValue & Mid$(xDef, lPos2)

            'insert new definition
            xObjectData = vParts(0) & xDef & vParts(1)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function GetObjectDataValue(ByVal xObjectData As String, _
                                           ByVal iPropertyIndex As Short, _
                                           ByVal xElement As String, _
                                           Optional ByVal xVariableName As String = "") As String
            'gets the value of the specified variable or block property
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetObjectDataValue"
            Dim vProps As Object = Nothing
            Dim xSep As String = ""
            Dim xDef As String = ""
            Dim lPos As Integer = 0

            On Error GoTo ProcError

            'decompress if necessary
            xObjectData = BaseMethods.Decrypt(xObjectData)

            If xElement = "mVar" Then
                xDef = GetVariableDefinition(xObjectData, xVariableName)
                xSep = "�"

                'strip trailing pipe (dm 2/12/10)
                If Right$(xDef, 1) = "|" Then _
                    xDef = Left$(xDef, Len(xDef) - 1)
            ElseIf xElement = "mSubVar" Then
                xDef = xObjectData
                xSep = "�"
            ElseIf xElement = "mBlock" Then
                xDef = xObjectData
                xSep = "|"

                'strip tag prefix id
                lPos = InStr(xDef, GlobalMethods.mpTagPrefixIDSeparator)
                If lPos > 0 Then
                    xDef = Mid$(xDef, lPos + Len(GlobalMethods.mpTagPrefixIDSeparator))
                End If
            End If

            If xDef <> "" Then
                vProps = Split(xDef, xSep)
                'Newer ObjectData items might not exist in previously-created docs
                If (iPropertyIndex - 1) <= UBound(vProps) Then
                    GetObjectDataValue = vProps(iPropertyIndex - 1)
                Else
                    GetObjectDataValue = ""
                End If
            Else
                'element not supported or definition not found
                GetObjectDataValue = ""
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function


        Public Shared Function GetmSEGObjectDataValue(ByVal oTag As Word.XMLNode, _
                                               ByVal xProperty As String) As String
            'gets the value of the specified segment property
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetmSEGObjectDataValue"
            Dim oObjectData As Word.XMLNode = Nothing
            Dim xObjectData As String
            Dim iPos1 As Integer = 0
            Dim iPos2 As Integer = 0
            Dim xValue As String = ""

            On Error GoTo ProcError

            oObjectData = oTag.SelectSingleNode("@ObjectData")

            If oObjectData Is Nothing Then _
                Exit Function

            xObjectData = BaseMethods.Decrypt(oObjectData.NodeValue)

            'moved the remainder of this code into a separate function for use
            'with both old and new data structures (dm 2/4/10)
            GetmSEGObjectDataValue = GetSegmentPropertyValue(xObjectData, xProperty)

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function Encrypt(ByVal xText As String, ByVal xPassword As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.Encrypt"
            Dim lBufferLen As Integer = 0
            Dim xBuffer As String
            Dim lOrigLen As Integer = 0
            Dim lRet As Integer = 0
            Dim i As Short = 0
            Dim xTemp As String = ""
            Dim bytIn() As Byte
            Dim bytOut() As Byte
            Dim xUncompressed As String = ""
            Dim bytIn2() As Byte
            Dim bytOut2() As Byte

            On Error GoTo ProcError

            xUncompressed = xText
            'exit if string is empty,is already encrypted,
            'or compression is turned off and the document belongs to the current client -
            'i.e. the tag prefix id of the string matches that of the workstation -
            'or the workstation belongs to The Sackett Group -  thus, to view metadata,
            'we can disable encryption, and attributes will appear unencrypted
            Dim oWordDoc As WordDoc
            oWordDoc = New WordDoc

            'GLOG 8186 (dm) - we weren't recognizing the pre-10.1.2 encryption here - this may have been
            'intentional, so as to allow this method to be used to directly update the old encryption to the new,
            'but that wasn't working properly in any case and was causing corruption under some circumstances
            '    If (xText = "") Or (xText Like GetEncryptionMarker & "*") Or g_bSuspendCompression Or _
            '            ((GetEncryptionStatus = mpEncryptionStatus_Off) And _
            '            ((xText Like GlobalMethods.g_xClientTagPrefixID & "*") Or UCase(GlobalMethods.g_xClientTagPrefixID) = "SGI")) Then
            If (xText = "") Or IsEncrypted(xText) Or GlobalMethods.g_bSuspendCompression Or _
                    ((GetEncryptionStatus() = mpEncryptionStatus.mpEncryptionStatus_Off) And _
                    ((xText Like GlobalMethods.g_xClientTagPrefixID & "*") Or UCase(GlobalMethods.g_xClientTagPrefixID) = "SGI")) Then
                Encrypt = xText
                Exit Function
            End If

            lOrigLen = Len(xText)
            'xTemp = CompressString(xText)

            'We are using Byte Arrays instead of strings when calling the zlib.dll compress
            'VB will convert Unicode strings to ANSI before passing to API function, and assumes
            'returned strings are also ANSI.
            'Each Unicode character is represented by two items in the byte array
            bytIn = System.Text.Encoding.Unicode.GetBytes(xText)
            lOrigLen = UBound(bytIn) + 1
            'Allocate string space for the buffers
            lBufferLen = (lOrigLen + (lOrigLen * 0.01) + 12) - 1
            ReDim bytOut(lBufferLen)

            'Compress string (temporary string buffer) data
            'lRet = compress(bytIn(0), lOrigLen, bytOut(0), lBufferLen)
            lRet = compress(bytOut, lBufferLen, bytIn, lOrigLen)

            'Convert output byte array back to Unicode string
            'GLOG 8831: Force ANSI 1252 code page
            xTemp = System.Text.Encoding.GetEncoding(1252).GetString(bytOut) ' Unicode.GetString(bytOut)
            xTemp = Left(xTemp, lBufferLen)
            xTemp = CStr(lOrigLen) & "|" & xTemp

            'compression reduced size -
            'prepend mp compression marker
            xText = GetCompressionMarker() & xTemp

            'Encrypt string if Password is supplied
            If xPassword <> "" Then
                Dim xNewText As String = ""
                Dim xEncryptedPass As String = ""
                Dim iPassLen As Short = 0
                Dim iCharInsert As Short = 0
                Dim c As Short = 0

                xNewText = EncryptString(xText, xPassword)

                If xNewText <> xText Then

                    'Encrypt the password and insert the characters throughout the encrypted text
                    xEncryptedPass = EncryptString(xPassword, INTERNAL_ENCRYPT_PASSWORD)
                    iPassLen = Len(xEncryptedPass)
                    iCharInsert = 9
                    For c = 1 To iPassLen
                        If iCharInsert <= Len(xNewText) Then
                            xNewText = Left$(xNewText, iCharInsert - 1) & Mid$(xEncryptedPass, c, 1) & Mid$(xNewText, iCharInsert)
                        Else
                            xNewText = xNewText & Mid$(xEncryptedPass, c)
                            Exit For
                        End If
                        'Set position for next insertion
                        iCharInsert = iCharInsert + 9 + c
                    Next c

                    'Encrypted Password length appended as last 3 characters of encrypted text
                    xNewText = xNewText & BaseMethods.CreatePadString(3 - Len(Hex$(iPassLen)), "0") & Hex$(iPassLen)

                    'prepend mp encryption marker
                    xNewText = GetEncryptionMarker() & xNewText

                    xText = xNewText
                End If
            End If

            'substitute for non-printing characters, as these are not allowed
            'in XMLNode attributes - we use unicode characters to distinguish them
            'from the compression output, which is always limited to ASCII
            For i = 31 To 0 Step -1
                xText = Replace(xText, Chr(i), ChrW(&H2302 + i))
            Next i

            '10/3/11 (dm) - base64 encode
            '10/14/11 (dm_ - do only if specified
            If GlobalMethods.g_bBase64Encoding Then
                If Left$(xText, 3) = GetEncryptionMarker() Then
                    Dim aByte() As Byte
                    aByte = System.Text.Encoding.Unicode.GetBytes(Mid$(xText, 4))
                    xText = GetEncryptionMarker() & "64" & EncodeBase64(aByte)
                End If
            End If

            Encrypt = xText

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function Decrypt(ByVal xText As String, Optional ByRef xPassword As String = "") As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.Decrypt"
            Dim lBufferLen As Integer = 0
            Dim xBuffer As String = ""
            Dim iPos As Integer = 0
            Dim lOrigSize As Integer = 0
            Dim lRet As Integer = 0
            Dim xOrigSize As String = ""
            Dim xCompressed As String = ""
            Dim i As Short = 0
            Dim bytIn() As Byte
            Dim bytOut() As Byte
            Dim lCompLen As Integer = 0

            On Error GoTo ProcError

            'GLOG 3932 (dm) - store original text
            Dim xStartText As String = ""
            xStartText = xText

            '10/3/11 (dm) - remove base64 encoding if necessary
            If Left$(xText, 5) = GetEncryptionMarker() & "64" Then
                xText = GetEncryptionMarker() & System.Text.Encoding.Unicode.GetString(DecodeBase64(Mid$(xText, 6)))
            End If

            If InStr(xText, mpAlgorithmIdentifier) = 0 Then
                'use original decryption method
                Decrypt = DecompressStringOriginal(xText, xPassword)
            Else
                'Check for encryption marker
                If Left$(xText, Len(GetEncryptionMarker())) = GetEncryptionMarker() Then
                    Dim iPassLen As Short = 0
                    Dim xOrigPassword As String = ""
                    Dim c As Short = 0
                    Dim iCharPos As Short = 0

                    'restore non-printing characters
                    For i = 31 To 0 Step -1
                        xText = Replace(xText, ChrW(&H2302 + i), Chr(i))
                    Next i

                    xText = Mid(xText, 4)
                    xOrigPassword = ""
                    iPassLen = Val("&H" & Right(xText, 3))
                    xText = Left(xText, Len(xText) - 3)
                    'Set first character to check
                    iCharPos = 9
                    'Extract encrypted password stored in string
                    For c = 1 To iPassLen
                        If iCharPos <= Len(xText) - (iPassLen - c) Then
                            xOrigPassword = xOrigPassword & Mid$(xText, iCharPos, 1)
                            xText = Left$(xText, iCharPos - 1) & Mid$(xText, iCharPos + 1)
                        Else
                            xOrigPassword = xOrigPassword & Right$(xText, iPassLen - (c - 1))
                            xText = Left$(xText, Len(xText) - (iPassLen - (c - 1)))
                            Exit For
                        End If
                        iCharPos = iCharPos + 9 + c - 1
                    Next c
                    xOrigPassword = DecryptString(xOrigPassword, INTERNAL_ENCRYPT_PASSWORD)
                    xText = DecryptString(xText, xOrigPassword)
                    'Return password originally used to encrypt
                    xPassword = xOrigPassword
                End If

                'exit if no mp compression marker
                If (Len(xText) < 2) Or ((Left$(xText, 2) <> ChrW(&H2514) & ChrW(&H2524)) _
                    And (Left$(xText, 2) <> ChrW(&H2514) & ChrW(&H2518))) Then
                    Decrypt = xText
                    Exit Function
                End If

                'strip compression marker
                xText = Mid$(xText, 3)

                'get position of original size separator
                iPos = InStr(xText, "|")

                xOrigSize = Left$(xText, iPos - 1)
                xCompressed = Mid$(xText, iPos + 1)

                lOrigSize = CLng(xOrigSize)

                'xText = DecompressString(xCompressed, lOrigSize)
                'We are using Byte Arrays instead of strings when calling the zlib.dll decompress
                'VB will convert Unicode strings to ANSI before passing to API function, and assumes
                'returned strings are also ANSI.
                'Each Unicode character is represented by two items in the byte array
                'GLOG 8831: Force ANSI 1252 code page
                bytIn = System.Text.Encoding.GetEncoding(1252).GetBytes(xCompressed)
                lCompLen = UBound(bytIn) + 1
                'Allocate string space
                lBufferLen = (lOrigSize + (lOrigSize * 0.01) + 12) - 1
                ReDim bytOut(lBufferLen)

                'Decompress
                lRet = uncompress(bytOut, lBufferLen, bytIn, lCompLen)

                'Convert byte array output to Unicode string
                xText = System.Text.Encoding.Unicode.GetString(bytOut)
                xText = Left(xText, lBufferLen / 2)
                Decrypt = xText
            End If
            Exit Function
ProcError:
            If (Err.Number = 5) And ((Len(xStartText) = 256) Or (Len(xStartText) = 32766)) Then
                'GLOG 3932 - this appears to be the result of the native bug whereby
                'XML attributes were getting truncated when opening a docx file in Word 2003
                Err.Raise(mpErrors.mpError_TruncatedData, mpThisFunction)
            Else
                GlobalMethods.RaiseError(mpThisFunction)
            End If
            Exit Function
        End Function

        Public Shared Function DecompressStringOriginal(ByVal xText As String, Optional ByRef xPassword As String = "") As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.DecompressStringOriginal"
            Dim lBufferLen As Integer = 0
            Dim xBuffer As String = ""
            Dim iPos As Integer = 0
            Dim lOrigSize As Integer = 0
            Dim lRet As Integer = 0
            Dim xOrigSize As String = ""
            Dim xCompressed As String = ""
            Dim i As Short = 0
            Dim bytIn() As Byte
            Dim bytOut() As Byte
            Dim lCompLen As Integer = 0

            On Error GoTo ProcError

            'exit if no mp compression marker
            If (Len(xText) < 2) Or ((Left$(xText, 2) <> ChrW(&H2514) & ChrW(&H2524)) _
                And (Left$(xText, 2) <> ChrW(&H2514) & ChrW(&H2518))) Then
                DecompressStringOriginal = xText
                Exit Function
            End If

            If Left$(xText, 2) = ChrW(&H2514) & ChrW(&H2518) Then
                'Call old function if old compression marker found
                DecompressStringOriginal = DecompressStringNonUnicode(xText)
                Exit Function
            End If

            'strip compression marker
            xText = Mid$(xText, 3)

            'get position of original size separator
            iPos = InStr(xText, "|")

            'restore non-printing characters
            For i = 31 To 0 Step -1
                xText = Replace(xText, ChrW(&H2302 + i), Chr(i))
            Next i

            xOrigSize = Left$(xText, iPos - 1)
            xCompressed = Mid$(xText, iPos + 1)

            lOrigSize = CLng(xOrigSize)

            'We are using Byte Arrays instead of strings when calling the zlib.dll decompress
            'VB will convert Unicode strings to ANSI before passing to API function, and assumes
            'returned strings are also ANSI.
            'Each Unicode character is represented by two items in the byte array
            'GLOG 8831: Force ANSI 1252 code page
            bytIn = System.Text.Encoding.GetEncoding(1252).GetBytes(xCompressed)
            lCompLen = UBound(bytIn) + 1
            'Allocate string space
            lBufferLen = (lOrigSize + (lOrigSize * 0.01) + 12) - 1
            ReDim bytOut(lBufferLen)

            'Decompress
            lRet = uncompress(bytOut, lBufferLen, bytIn, lCompLen)
            'We are using Byte Arrays instead of strings when calling the zlib.dll decompress
            'VB will convert Unicode strings to ANSI before passing to API function, and assumes
            'returned strings are also ANSI.
            'Each Unicode character is represented by two items in the byte array
            'GLOG 8831: Force ANSI 1252 code page
            bytIn = System.Text.Encoding.GetEncoding(1252).GetBytes(xCompressed)

            'Convert byte array output to Unicode string
            xText = System.Text.Encoding.Unicode.GetString(bytOut)
            xText = Left(xText, lBufferLen / 2)
            'Check for encryption marker
            If Left$(xText, Len(GetEncryptionMarker())) = GetEncryptionMarker() Then
                Dim iPassLen As Short = 0
                Dim xOrigPassword As String = ""
                Dim c As Short = 0
                Dim iCharPos As Short = 0

                xText = Mid(xText, 4)
                xOrigPassword = ""
                iPassLen = Val("&H" & Right(xText, 3))
                xText = Left(xText, Len(xText) - 3)
                'Set first character to check
                iCharPos = 9
                'Extract encrypted password stored in string
                For c = 1 To iPassLen
                    If iCharPos <= Len(xText) - (iPassLen - c) Then
                        xOrigPassword = xOrigPassword & Mid$(xText, iCharPos, 1)
                        xText = Left$(xText, iCharPos - 1) & Mid$(xText, iCharPos + 1)
                    Else
                        xOrigPassword = xOrigPassword & Right$(xText, iPassLen - (c - 1))
                        xText = Left$(xText, Len(xText) - (iPassLen - (c - 1)))
                        Exit For
                    End If
                    iCharPos = iCharPos + 9 + c - 1
                Next c
                xOrigPassword = DecryptString(xOrigPassword, INTERNAL_ENCRYPT_PASSWORD)
                xText = DecryptString(xText, xOrigPassword)
                'Return password originally used to encrypt
                xPassword = xOrigPassword
            End If
            DecompressStringOriginal = xText
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        'Old Function - not compatible with Double-byte unicode characters
        Private Shared Function DecompressStringNonUnicode(ByVal xText As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.DecompressStringNonUnicode"
            Dim lBufferLen As Integer = 0
            Dim xBuffer As String = ""
            Dim iPos As Integer = 0
            Dim lOrigSize As Integer = 0
            Dim lRet As Integer = 0
            Dim xOrigSize As String = ""
            Dim xCompressed As String = ""
            Dim i As Short = 0
            Dim bytIn() As Byte
            Dim bytOut() As Byte
            Dim lCompLen As Integer = 0

            On Error GoTo ProcError

            'exit if no mp compression marker
            If (Len(xText) < 2) Or (Left$(xText, 2) <> ChrW(&H2514) & ChrW(&H2518)) Then
                DecompressStringNonUnicode = xText
                Exit Function
            End If

            'strip compression marker
            xText = Mid$(xText, 3)

            'get position of original size separator
            iPos = InStr(xText, "|")

            'restore non-printing characters
            For i = 31 To 0 Step -1
                xText = Replace(xText, ChrW(&H2302 + i), Chr(i))
            Next i

            xOrigSize = Left$(xText, iPos - 1)
            xCompressed = Mid$(xText, iPos + 1)

            lOrigSize = CLng(xOrigSize)
            'We are using Byte Arrays instead of strings when calling the zlib.dll decompress
            'VB will convert Unicode strings to ANSI before passing to API function, and assumes
            'returned strings are also ANSI.
            'Each Unicode character is represented by two items in the byte array
            'GLOG 8831: Force ANSI 1252 code page
            bytIn = System.Text.Encoding.GetEncoding(1252).GetBytes(xCompressed)
            lCompLen = UBound(bytIn) + 1
            'Allocate string space
            lBufferLen = (lOrigSize + (lOrigSize * 0.01) + 12) - 1
            ReDim bytOut(lBufferLen)

            'Decompress
            lRet = uncompress(bytOut, lBufferLen, bytIn, lCompLen)

            'Convert byte array output to Unicode string
            xText = System.Text.Encoding.Unicode.GetString(bytOut)
            'Make string the size of the uncompressed string
            xText = Left$(xBuffer, lBufferLen)
            DecompressStringNonUnicode = xText
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        '
        Public Shared Function GetOrdinal(i As Short) As String
            'returns the ordinal position of an short
            'as a string - e.g. 15 --> 15th
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetOrdinal"

            On Error GoTo ProcError
            If i >= 4 And i <= 20 Then
                GetOrdinal = i & "th"
            ElseIf Right(i, 1) = 1 Then
                GetOrdinal = i & "st"
            ElseIf Right(i, 1) = 2 Then
                GetOrdinal = i & "nd"
            ElseIf Right(i, 1) = 3 Then
                GetOrdinal = i & "rd"
            Else
                GetOrdinal = i & "th"
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function xSubstitute(ByVal xString As String, _
                             xSearch As String, _
                             xReplace As String) As String
            'replaces xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer = 0
            Dim xNewString As String = ""

            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.xSubstitute"

            On Error GoTo ProcError
            '   get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            '   remove switch all chars
            While iSeachPos
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
                iSeachPos = InStr(UCase(xString), _
                                  UCase(xSearch))
            End While

            xNewString = xNewString & xString
            xSubstitute = xNewString
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetMP10SchemaPrefix(xXML As String)
            'returns the prefix assigned to the mp10 namespace in xXML
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetMP10SchemaPrefix"
            Dim iPos As Integer = 0
            Dim iPos2 As Integer = 0
            Dim xPrefix As String = ""

            On Error GoTo ProcError

            iPos = InStr(xXML, GlobalMethods.mpNamespace)
            If iPos > 0 Then
                iPos2 = InStrRev(xXML, "xmlns:", iPos)
                xPrefix = Mid$(xXML, iPos2 + 6, iPos - iPos2 - 8)
            End If

            GetMP10SchemaPrefix = xPrefix

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetAttributesArray(oTag As Word.XMLNode, _
                                           xAttributes(,) As String) As Integer
            'returns an array containing the names and values of the attributes
            'of the specified tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetAttributesArray"
            Dim i As Short = 0
            Dim oAttribute As Word.XMLNode = Nothing

            On Error GoTo ProcError

            If Not oTag Is Nothing Then
                'load attributes into array
                With oTag.Attributes
                    ReDim xAttributes(.Count - 1, 1)
                    For i = 0 To UBound(xAttributes)
                        oAttribute = .Item(i + 1)
                        xAttributes(i, 0) = oAttribute.BaseName
                        xAttributes(i, 1) = oAttribute.NodeValue
                    Next i
                End With

                'return value is number of attributes
                GetAttributesArray = UBound(xAttributes) + 1
            Else
                'return an empty array
                ReDim xAttributes(0, 0)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function TableIsEmpty(oTable As Word.Table) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.TableIsEmpty"
            Dim xText As String = ""

            On Error GoTo ProcError

            xText = oTable.Range.Text
            xText = Replace(xText, vbCr, "")
            xText = Replace(xText, Chr(7), "")

            TableIsEmpty = (xText = "")

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function CellIsEmpty(oCell As Word.Cell) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.CellIsEmpty"
            Dim xText As String = ""

            On Error GoTo ProcError

            xText = oCell.Range.Text
            xText = Replace(xText, vbCr, "")
            xText = Replace(xText, Chr(7), "")

            CellIsEmpty = (xText = "")

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function RowIsEmpty(oRow As Word.Row) As Boolean
            'added for GLOG 7933/7946 (dm)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RowIsEmpty"
            Dim xText As String = ""

            On Error GoTo ProcError

            xText = oRow.Range.Text
            xText = Replace(xText, vbCr, "")
            xText = Replace(xText, Chr(7), "")

            RowIsEmpty = (xText = "")

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function ReplaceXMLChars(xText As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.ReplaceXMLChars"

            On Error GoTo ProcError

            xText = Replace(xText, "&lt;", "<")
            xText = Replace(xText, "&gt;", ">")
            xText = Replace(xText, "&apos;", "'")
            xText = Replace(xText, "&amp;", "&")
            xText = Replace(xText, "&", "&amp;")
            xText = Replace(xText, "<", "&lt;")
            xText = Replace(xText, ">", "&gt;")
            xText = Replace(xText, "'", "&apos;")

            ReplaceXMLChars = xText

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function RestoreXMLChars(xText As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RestoreXMLChars"

            On Error GoTo ProcError

            xText = Replace(xText, "&amp;", "&")
            xText = Replace(xText, "&lt;", "<")
            xText = Replace(xText, "&gt;", ">")
            xText = Replace(xText, "&apos;", "'")

            RestoreXMLChars = xText

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function XMLIsEmpty(xXML As String) As Boolean
            'returns TRUE unless xXML contains a non-empty paragraph tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.XMLIsEmpty"
            Dim lPos As Integer = 0

            On Error GoTo ProcError

            lPos = InStr(xXML, GlobalMethods.mpStartParaTag)

            While (lPos <> 0) And (Len(xXML) > lPos + 11)
                If Mid$(xXML, lPos + 5, 6) <> GlobalMethods.mpEndParaTag Then _
                    Exit Function
                lPos = InStr(lPos + 11, GlobalMethods.mpStartParaTag)
            End While

            XMLIsEmpty = True

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function CountTagsAtStartOfParagraph(ByVal oPara As Word.Paragraph) As Short
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.CountTagsAtStartOfParagraph"
            Dim i As Short = 0
            Dim rngPara As Word.Range
            Dim iCount As Short = 0

            On Error GoTo ProcError

            rngPara = oPara.Range

            For i = 1 To rngPara.XMLNodes.Count
                If rngPara.XMLNodes(i).Range.Start = rngPara.Start + i Then
                    iCount = iCount + 1
                End If
            Next i

            CountTagsAtStartOfParagraph = iCount

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function GetTagPrefixID(ByVal xObjectData As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTagPrefixID"
            Dim lPos As Integer = 0
            Dim xTagPrefixID As String = ""

            On Error GoTo ProcError

            lPos = InStr(xObjectData, GlobalMethods.mpTagPrefixIDSeparator)
            If lPos > 0 Then
                xTagPrefixID = Left$(xObjectData, lPos - 1)
            End If

            GetTagPrefixID = xTagPrefixID

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function
        Public Shared Function EncryptString(ByVal xData As String, ByVal xPassword As String) As String
            If xPassword = "" Then
                EncryptString = xData
                Exit Function
            End If
            Dim oCrypto As Encryption
            oCrypto = New Encryption
            EncryptString = oCrypto.EncryptData(xData, xPassword)
            oCrypto = Nothing
        End Function
        Public Shared Function DecryptString(ByVal xData As String, ByVal xPassword As String) As String
            If xPassword = "" Then
                DecryptString = xData
                Exit Function
            End If
            Dim oCrypto As Encryption
            oCrypto = New Encryption
            DecryptString = oCrypto.DecryptData(xData, xPassword)
            oCrypto = Nothing
        End Function

        Public Shared Function IsEncrypted(ByVal xText As String) As Boolean
            Dim xMarker As String = ""

            'if the new encryption is being used, compressed
            'strings will begin with the encryption marker
            If xText Like GetEncryptionMarker() & "*" Then
                IsEncrypted = True
            Else
                'either the string is not compressed or the
                'string was compressed with the old compression-
                'if the latter, the string will begin with the
                'old compression marker
                If Len(xText) > 1 Then _
                    xMarker = Left$(xText, 2)
                IsEncrypted = ((xMarker = ChrW(&H2514) & ChrW(&H2524)) Or _
                    (xMarker = ChrW(&H2514) & ChrW(&H2518)))
            End If
        End Function

        Public Shared Function GetEncryptionStatus() As mpEncryptionStatus
            'returns the status of encryption - on or off
            If m_iEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Uninitialized Then
                If GlobalMethods.g_bDisableEncryption Or GlobalMethods.g_xEncryptionPassword = "" Then
                    m_iEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_Off
                Else
                    m_iEncryptionStatus = mpEncryptionStatus.mpEncryptionStatus_On
                End If
            End If

            GetEncryptionStatus = m_iEncryptionStatus
        End Function

        Public Shared Sub MoveWordTag(ByRef oWordTag As Word.XMLNode, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.MoveWordTag"
            Dim iAttempts As Short = 0
            Dim i As Short = 0
            Dim oTag As Tag
            Dim oAttribute As Word.XMLNode = Nothing
            Dim oAttributes As Word.XMLNodes
            Dim xAttributes(,) As String
            Dim iCount As Short = 0
            Dim xBaseName As String = ""

            On Error GoTo ProcError

            'get attributes
            xBaseName = oWordTag.BaseName
            iCount = oWordTag.Attributes.Count
            ReDim xAttributes(iCount - 1, 1)
            For i = 1 To iCount
                xAttributes(i - 1, 0) = oWordTag.Attributes(i).BaseName
                xAttributes(i - 1, 1) = oWordTag.Attributes(i).NodeValue

                '1/6/11 (dm) - delete bookmark
                If xAttributes(i - 1, 0) = "Reserved" Then
                    On Error Resume Next
                    oWordTag.Range.Bookmarks("_" & xAttributes(i - 1, 1)).Delete()
                    On Error GoTo ProcError
                End If
            Next i

            'delete existing tag
            oWordTag.Delete()

            'add tag - try a second time if necessary,
            'as this line fails on first execution in Word 2007
            On Error GoTo TryAgain
            oWordTag = oNewRange.XMLNodes.Add(xBaseName, GlobalMethods.mpNamespace)
            On Error GoTo ProcError

            'add attributes
            For i = 0 To UBound(xAttributes)
                oAttribute = oWordTag.Attributes.Add(xAttributes(i, 0), "")
                oAttribute.NodeValue = xAttributes(i, 1)
                '10.2 Rebookmark new Tag
                'GLOG 6968 (dm) - unremmed the following code to rebookmark the tag,
                'but made it conditional on it being an mSEG
                If (xBaseName = "mSEG") And (oAttribute.BaseName = "Reserved") Then
                    oWordTag.Range.Bookmarks.Add("_" & oAttribute.NodeValue)
                End If
            Next i

            Exit Sub
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function GetTagSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Integer
            'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
            'tags that would otherwise force a new paragraph to be created above the tag(s)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTagSafeParagraphStart"
            Dim i As Short = 0
            Dim rngPara As Word.Range
            Dim iCount As Short = 0
            Dim oTag As Word.XMLNode = Nothing

            On Error GoTo ProcError

            rngPara = oInsertionRange.Paragraphs(1).Range

            'count multiparagraph tags at start of paragraph
            For i = 1 To rngPara.XMLNodes.Count
                oTag = rngPara.XMLNodes(i)
                If (oTag.Range.Start = rngPara.Start + i) And _
                        (oTag.Range.Paragraphs.Count > 1) Then
                    iCount = iCount + 1
                Else
                    Exit For
                End If
            Next i

            GetTagSafeParagraphStart = rngPara.Start + iCount

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function GetTagSafeParagraphEnd(ByVal oInsertionRange As Word.Range) As Integer
            'gets "end of paragraph" insertion location, ensuring that inside any multiparagraph
            'tags that would otherwise force a new paragraph to be created below the tag(s)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTagSafeParagraphEnd"
            Dim i As Short = 0
            Dim rngPara As Word.Range
            Dim iCount As Short = 0
            Dim oTag As Word.XMLNode = Nothing

            On Error GoTo ProcError

            rngPara = oInsertionRange.Paragraphs(1).Range
            With rngPara
                '        'move to end of paragraph
                .MoveEnd(Word.WdUnits.wdCharacter, -1)

                'count multiparagraph tags at start of paragraph
                For i = 1 To .XMLNodes.Count
                    oTag = .XMLNodes(i)
                    If (oTag.Range.End = .End - i) And _
                            (oTag.Range.Paragraphs.Count > 1) Then
                        iCount = iCount + 1
                    Else
                        Exit For
                    End If
                Next i
            End With

            GetTagSafeParagraphEnd = rngPara.End - iCount

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function RangeIsMultiCell(ByVal oRange As Word.Range) As Boolean
            'returns TRUE if oRange spans multiple cells - neither Range.Cells.Count nor
            'Range.Columns.Count are reliable when range is in a nested tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RangeIsMultiCell"
            Dim lStartColIndex As Integer = 0
            Dim lEndColIndex As Integer = 0
            Dim oTestRange As Word.Range

            On Error GoTo ProcError

            'exit if range is not entirely within table
            If Not oRange.Information(Word.WdInformation.wdWithInTable) Then
                RangeIsMultiCell = False
                Exit Function
            End If

            'multicell if spans multiple rows
            If oRange.Rows.Count > 1 Then
                RangeIsMultiCell = True
                Exit Function
            End If

            'get column index at start of range
            oTestRange = oRange.Duplicate
            oTestRange.StartOf()
            lStartColIndex = oTestRange.Cells(1).ColumnIndex

            'get column index at end of range
            oTestRange = oRange.Duplicate
            oTestRange.EndOf()
            If Not oTestRange.Information(Word.WdInformation.wdWithInTable) Then
                'if oRange is entire table, end is outside of table
                oTestRange.MoveEnd(Word.WdUnits.wdCharacter, -2)
            Else
                'range can also be in table, but not in any cell
                While oTestRange.Cells.Count = 0
                    oTestRange.MoveEnd(Word.WdUnits.wdCharacter, -1)
                End While
            End If
            lEndColIndex = oTestRange.Cells(1).ColumnIndex

            'multicell if column indexes differ
            RangeIsMultiCell = (lStartColIndex <> lEndColIndex)

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Function TagIsFullRow(ByVal oWordTag As Word.XMLNode, _
                                     ByVal bIgnoreEmptyCells As Boolean) As Boolean
            TagIsFullRow = RangeIsFullRow(oWordTag.Range, bIgnoreEmptyCells)
        End Function
        Public Shared Function ContentControlIsFullRow(ByVal oCC As Word.ContentControl, _
                                                ByVal bIgnoreEmptyCells As Boolean) As Boolean
            ContentControlIsFullRow = RangeIsFullRow(oCC.Range, bIgnoreEmptyCells)
        End Function
        Public Shared Function BookmarkIsFullRow(ByVal oBmk As Word.Bookmark, ByVal bIgnoreEmptyCells As Boolean) As Boolean
            BookmarkIsFullRow = RangeIsFullRow(oBmk.Range, bIgnoreEmptyCells)
        End Function
        Public Shared Function RangeIsFullRow(ByVal oRange As Word.Range, _
                                       ByVal bIgnoreEmptyCells As Boolean) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RangeIsFullRow"
            Dim oTestRange As Word.Range = Nothing
            Dim xText As String = ""
            Dim lRowEnd As Integer = 0
            Dim oTableRange As Word.Range = Nothing

            On Error GoTo ProcError

            'exit if range is not entirely within table
            If Not oRange.Information(Word.WdInformation.wdWithInTable) Then
                RangeIsFullRow = False
                Exit Function
            End If

            'check for text before start tag
            On Error Resume Next
            oTestRange = oRange.Rows.First.Range
            On Error GoTo ProcError
            If oTestRange Is Nothing Then
                'GLOG 3522: If table contains merged cells, indivual Rows and Columns cannot be accessed
                'Need to use Cell object instead
                oTestRange = oRange.Cells(1).Range
            End If
            oTestRange.SetRange(oTestRange.Start, oRange.Start)
            xText = oTestRange.Text

            'check for text after end tag
            If xText = "" Then
                'GLOG 3522: If table contains merged cells, indivual Rows and Columns cannot be accessed
                'Need to use Cell object instead
                Dim oCell As Word.Cell
                On Error Resume Next
                oCell = oRange.Cells(oRange.Cells.Count)
                On Error GoTo ProcError
                If oCell Is Nothing Then
                    'GLOG 4385 (dm) - oddly, the line above is still erring in collection tables
                    'that contain merged cells - if the tag spans the entire table, it's full row
                    oTableRange = oRange.Tables(1).Range
                    RangeIsFullRow = ((oRange.Start = oTableRange.Start + 1) And _
                        (oRange.End >= oTableRange.End - 3))
                    Exit Function
                End If

                'Find last cell in same row
                Do While Not oCell.Next Is Nothing
                    If oCell.Next.RowIndex = oCell.RowIndex Then
                        oCell = oCell.Next
                    Else
                        Exit Do
                    End If
                Loop
                lRowEnd = oCell.Range.End - 2
                If lRowEnd > oRange.End Then
                    oTestRange = oRange.Duplicate
                    oTestRange.SetRange(oRange.End, lRowEnd)
                    xText = oTestRange.Text
                ElseIf (oCell.ColumnIndex > oRange.Cells.Count) Then
                    'GLOG : 8038/8042 : ceh - ensure marker when last cell is empty
                    xText = Chr(7)
                End If
            End If

            'GLOG 6365 (dm) - added parameter to ignore empty cells
            If bIgnoreEmptyCells Then
                xText = Replace(xText, vbCr, "")
                xText = Replace(xText, Chr(7), "")
            End If

            'full row if no leading or trailing text in row
            RangeIsFullRow = (xText = "")

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsCollectionTableItem(ByVal xObjectData As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsCollectionTableItem"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xObjectTypeID As String = ""

            On Error GoTo ProcError

            'get object type id
            lPos = InStr(xObjectData, "|ObjectTypeID=") + 14
            If lPos > 14 Then
                lPos2 = InStr(lPos, xObjectData, "|")
                xObjectTypeID = Mid$(xObjectData, lPos, lPos2 - lPos)
                Select Case xObjectTypeID
                    'GLOG : 8457 : jsw remove LetterSig and AgreementSig ids "527", "528"
                    Case "500", "501", "502", "509", "513", "525"
                        IsCollectionTableItem = True
                    Case Else
                        IsCollectionTableItem = False
                End Select
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsCollectionTable(ByVal xObjectData As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsCollectionTable"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xObjectTypeID As String = ""

            On Error GoTo ProcError

            'get object type id
            lPos = InStr(xObjectData, "|ObjectTypeID=") + 14
            If lPos > 14 Then
                lPos2 = InStr(lPos, xObjectData, "|")
                xObjectTypeID = Mid$(xObjectData, lPos, lPos2 - lPos)
                Select Case xObjectTypeID
                    Case "506", "507", "508", "526", "527", "528"
                        IsCollectionTable = True
                    Case Else
                        IsCollectionTable = False
                End Select
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function TagIsInline(ByVal oTag As Word.XMLNode) As Boolean
            'returns TRUE if functionally inline, i.e. contains no text, or other tags that
            'start and end, before or after it in the paragraph - this is different than
            'wdXMLNodeLevelInline, which includes tags that are "blockable" - it also provides
            'an alternative to the existing TagIsBlockable() function that 1) doesn't require
            'adding a test paragraph and 2) won't convert blockable tags to block tags
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.TagIsInline"
            Dim oParaRange As Word.Range = Nothing
            Dim oTag2 As Word.XMLNode = Nothing
            Dim oTagRange As Word.Range = Nothing
            Dim oPrecedingRange As Word.Range = Nothing
            Dim oTrailingRange As Word.Range = Nothing
            Dim oTag2Range As Word.Range = Nothing

            On Error GoTo ProcError

            'exit if level isn't inline
            If oTag.Level <> WdXMLNodeLevel.wdXMLNodeLevelInline Then
                TagIsInline = False
                Exit Function
            End If

            oTagRange = oTag.Range
            oParaRange = oTagRange.Paragraphs(1).Range

            'check whether there's preceding text in the paragraph
            oPrecedingRange = oTagRange.Duplicate
            oPrecedingRange.SetRange(oParaRange.Start, oTagRange.Start - 1)
            If oPrecedingRange.Text <> "" Then
                TagIsInline = True
                Exit Function
            End If

            'check whether there's trailing text in the paragraph
            oTrailingRange = oTagRange.Duplicate
            oTrailingRange.SetRange(oTagRange.End + 1, oParaRange.End - 1)
            If oTrailingRange.Text <> "" Then
                TagIsInline = True
                Exit Function
            End If

            'check whether there are preceding tags in the paragraph
            For Each oTag2 In oPrecedingRange.XMLNodes
                oTag2Range = oTag2.Range
                If oTag2.Range.End < oPrecedingRange.End Then
                    TagIsInline = True
                    Exit Function
                End If
            Next oTag2

            'check whether there are trailing tags in the paragraph
            For Each oTag2 In oTrailingRange.XMLNodes
                oTag2Range = oTag2.Range
                If oTag2.Range.Start > oTrailingRange.Start Then
                    TagIsInline = True
                    Exit Function
                End If
            Next oTag2

            TagIsInline = False

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function ClipboardContainsTags(Optional ByVal xElement As String = "", _
                                      Optional ByVal xRTF As String = "")
            'returns TRUE if clipboard (or specified clipboard rtf) contains tags
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.ClipboardContainsTags"
            Dim xSearch As String = ""

            On Error GoTo ProcError

            'get clipboard rtf if not supplied
            If xRTF = "" Then _
                xRTF = My.Computer.Clipboard.GetText(System.Windows.Forms.TextDataFormat.Rtf)

            'search rtf for any tag or just the specified one
            xSearch = "{\xmlname "
            If xElement <> "" Then _
                xSearch = xSearch & xElement & "}"

            ClipboardContainsTags = (InStr(xRTF, xSearch) > 0)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetBaseName(ByVal oContentControl As Word.ContentControl) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetBaseName"
            On Error GoTo ProcError
            GetBaseName = GetBaseNameFromTag(oContentControl.Tag)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetBaseNameFromTag(ByVal xTag As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetBaseNameFromTag"

            On Error GoTo ProcError

            If Left$(xTag, 1) = "_" Then
                xTag = Mid$(xTag, 2)
            End If

            If Left$(xTag, 2) <> "mp" Then _
                Exit Function

            Select Case Mid$(xTag, 3, 1)
                Case "s"
                    GetBaseNameFromTag = "mSEG"
                Case "v"
                    GetBaseNameFromTag = "mVar"
                Case "b"
                    GetBaseNameFromTag = "mBlock"
                Case "d"
                    GetBaseNameFromTag = "mDel"
                Case "u"
                    GetBaseNameFromTag = "mSubVar"
                Case "p"
                    GetBaseNameFromTag = "mDocProps"
                Case "c"
                    GetBaseNameFromTag = "mSecProps"
                Case Else
                    GetBaseNameFromTag = ""
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetAttributeValue(ByVal xTag As String, ByVal xName As String, _
                                          Optional ByVal oDoc As Word.Document = Nothing) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.mpBase.GetAttributeValue"
            Dim xVar As String = ""
            Dim xValue As String = ""
            Dim lPos As String = ""
            Dim lPos2 As String = ""
            Dim iIndex As Short = 0
            Dim xDeletedScopes As String = ""
            Dim xTagID As String = ""
            Dim xAttributes As String = ""

            On Error GoTo ProcError

            'Make sure empty string is always returned instead of Nothing
            GetAttributeValue = ""

            If oDoc Is Nothing Then _
                    oDoc = GlobalMethods.CurWordApp.ActiveDocument
            If xName <> "DeletedScopes" Then
                'attribute is in mpo-prefixed variable
                xVar = "mpo" & Mid$(xTag, 4, 8)

                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError

                '10/21/10 (dm) - check for variable in the store - it may not
                'be in the document because the content was copy/pasted from
                'another document or because the user undid a deletion
                If xValue = "" Then
                    On Error Resume Next
                    xValue = GlobalMethods.g_oDocVars(xVar)
                    On Error GoTo ProcError
                    If xValue <> "" Then _
                        oDoc.Variables.Add(xVar, xValue)
                End If

                If xValue = "" Then _
                    Exit Function

                lPos = InStr(xValue, "��")
                xTagID = Left$(xValue, lPos - 1)
                xAttributes = Mid$(xValue, lPos + 2)
                If xName = "TagID" Or xName = "Name" Then
                    'these attributes are always at start of string,
                    'unlabeled and unencrypted
                    GetAttributeValue = xTagID
                Else
                    'search for specified attribute in encrypted portion of value
                    xAttributes = "��" & BaseMethods.Decrypt(xAttributes) & "��"
                    lPos = InStr(xAttributes, "��" & xName & "=")
                    If lPos <> 0 Then
                        lPos = lPos + Len(xName) + 3
                        lPos2 = InStr(lPos, xAttributes, "��", CompareMethod.Text)
                        GetAttributeValue = Mid$(xAttributes, lPos, lPos2 - lPos)
                    End If
                End If
            Else
                'attribute is in mpd-prefixed variable
                iIndex = 1
                xVar = "mpd" & Mid$(xTag, 4, 8) & "01"

                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError

                '10/21/10 (dm) - check for variable in the store - it may not
                'be in the document because the content was copy/pasted from
                'another document or because the user undid a deletion
                If xValue = "" Then
                    On Error Resume Next
                    xValue = GlobalMethods.g_oDocVars(xVar)
                    On Error GoTo ProcError
                    If xValue <> "" Then _
                        oDoc.Variables.Add(xVar, xValue)
                End If

                'there can be multiple mpd doc vars
                While xValue <> ""
                    xDeletedScopes = xDeletedScopes & xValue
                    iIndex = iIndex + 1
                    xVar = Left$(xVar, 11) & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    xValue = ""
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo ProcError

                    '10/21/10 (dm) - check for variable in the store
                    If xValue = "" Then
                        On Error Resume Next
                        xValue = GlobalMethods.g_oDocVars(xVar)
                        On Error GoTo ProcError
                        If xValue <> "" Then _
                            oDoc.Variables.Add(xVar, xValue)
                    End If
                End While

                GetAttributeValue = BaseMethods.Decrypt(xDeletedScopes)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub SetAttributeValue(ByVal xTag As String, ByVal xName As String, _
                                     ByVal xValue As String, ByVal xPassword As String, _
                                     Optional ByVal oDoc As Word.Document = Nothing)
            Const mpThisFunction As String = "LMP.Forte.MSWord.mpBase.SetAttributeValue"
            Dim xVar As String = ""
            Dim xVarValue As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xTagID As String = ""
            Dim xAttributes As String = ""
            Dim xDeletedScopes As String = ""
            Dim iIndex As Short = 0
            Dim xExistingPassword As String = ""
            Dim xBaseName As String = ""

            On Error GoTo ProcError

            If oDoc Is Nothing Then _
                    oDoc = GlobalMethods.CurWordApp.ActiveDocument

            xBaseName = GetBaseNameFromTag(xTag)

            If xName <> "DeletedScopes" Then
                'attribute is in mpo-prefixed variable
                xVar = "mpo" & Mid$(xTag, 4, 8)

                On Error Resume Next
                xVarValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError

                If xVarValue = "" Then
                    'create variable
                    xVarValue = "��"
                    oDoc.Variables.Add(xVar, xVarValue)
                End If

                lPos = InStr(xVarValue, "��")
                If xName = "TagID" Or xName = "Name" Then
                    'these attributes are always at start of string,
                    'unlabeled and unencrypted
                    xVarValue = xValue & Mid$(xVarValue, lPos)
                Else
                    'search for specified attribute in encrypted portion of value
                    xTagID = Left$(xVarValue, lPos + 1)
                    xAttributes = BaseMethods.Decrypt(Mid$(xVarValue, lPos + 2), xExistingPassword)
                    If xAttributes = "" Then
                        xAttributes = "��"
                    Else
                        xAttributes = "��" & xAttributes & "��"
                    End If
                    lPos = InStr(xAttributes, "��" & xName & "=")
                    If lPos <> 0 Then
                        'existing attribute found
                        lPos = lPos + Len(xName) + 3
                        lPos2 = InStr(lPos, xAttributes, "��")
                        xAttributes = Mid$(xAttributes, 3, lPos - 3) & xValue & _
                            Mid$(xAttributes, lPos2, Len(xAttributes) - lPos2 - 1)
                    Else
                        'add attribute
                        xAttributes = Mid$(xAttributes, 3) & xName & "=" & xValue
                    End If

                    'encrypt
                    If (xBaseName <> "mDel") And (xBaseName <> "mSubVar") Then
                        If xPassword = "" Then
                            'no supplied password
                            If xExistingPassword <> "" Then
                                'use existing password
                                xPassword = xExistingPassword
                            Else
                                'use current client's password
                                xPassword = GlobalMethods.g_xEncryptionPassword
                            End If
                        End If
                        xAttributes = Encrypt(xAttributes, xPassword)
                    End If

                    'recombine with tag id
                    xVarValue = xTagID & xAttributes
                End If

                'write to doc var
                oDoc.Variables(xVar).Value = xVarValue
            Else
                'attribute is in mpd-prefixed variable
                If xPassword = "" Then
                    'no supplied password - attempt to get existing one
                    xVar = "mpd" & Mid$(xTag, 4, 8) & "01"
                    On Error Resume Next
                    xVarValue = BaseMethods.Decrypt(oDoc.Variables(xVar).Value, xExistingPassword)
                    On Error GoTo ProcError
                    If xExistingPassword <> "" Then
                        'use existing password
                        xPassword = xExistingPassword
                    Else
                        'use current client's password
                        xPassword = GlobalMethods.g_xEncryptionPassword
                    End If
                End If

                'encrypt value
                xValue = Encrypt(xValue, xPassword)

                'create/set variables
                While xValue <> ""
                    iIndex = iIndex + 1
                    xVar = "mpd" & Mid$(xTag, 4, 8) & _
                        BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    oDoc.Variables(xVar).Value = Left$(xValue, 65000)
                    xValue = Mid$(xValue, 65001)
                End While

                'delete any remaining unused variables
                iIndex = iIndex + 1
                xVar = "mpd" & Mid$(xTag, 4, 8) & _
                    BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                xVarValue = ""
                On Error Resume Next
                xVarValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError
                While xVarValue <> ""
                    oDoc.Variables(xVar).Delete()
                    iIndex = iIndex + 1
                    xVar = "mpd" & Mid$(xTag, 4, 8) & _
                        BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    xVarValue = ""
                    On Error Resume Next
                    xVarValue = oDoc.Variables(xVar).Value
                    On Error GoTo ProcError
                End While
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub SetAttributesEncryption(ByVal xTag As String, _
                                   ByVal bOn As Boolean, _
                                   Optional ByVal xPassword As String = "", _
                                   Optional ByVal oDoc As Word.Document = Nothing)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.SetAttributesEncryption"
            Dim xVar As String = ""
            Dim xTagID As String = ""
            Dim xAttributes As String = ""
            Dim xValue As String = ""
            Dim iIndex As Short = 0
            Dim lPos As Integer = 0
            Dim bIsEncrypted As Boolean = False

            On Error GoTo ProcError

            If oDoc Is Nothing Then _
                    oDoc = GlobalMethods.CurWordApp.ActiveDocument

            'mpo variable holds everything but deleted scopes
            xVar = "mpo" & Mid$(xTag, 4, 8)

            On Error Resume Next
            xValue = oDoc.Variables(xVar).Value
            On Error GoTo ProcError

            If xValue <> "" Then
                lPos = InStr(xValue, "��")
                xTagID = Left$(xValue, lPos + 1)
                xAttributes = Mid$(xValue, lPos + 2)
                If bOn Then
                    xAttributes = Encrypt(xAttributes, xPassword)
                Else
                    xAttributes = BaseMethods.Decrypt(xAttributes)
                End If
                xValue = xTagID & xAttributes
                oDoc.Variables(xVar).Value = xValue
            End If

            'deleted scopes
            xValue = ""
            xAttributes = ""

            'get first mpd variable
            xVar = "mpd" & Mid$(xTag, 4, 8) & "01"
            On Error Resume Next
            xValue = oDoc.Variables(xVar).Value
            On Error GoTo ProcError

            'exit if encryption is already as specified
            If xValue <> "" Then
                bIsEncrypted = IsEncrypted(xValue)
                If (Not bOn And Not bIsEncrypted) Or (bOn And bIsEncrypted) Then _
                    Exit Sub
            End If

            '10/13/11 (dm) - the first mpd value was always getting duplicated
            iIndex = 1

            'append values of any remaining mpd variables
            While xValue <> ""
                xAttributes = xAttributes & xValue
                xValue = ""
                iIndex = iIndex + 1
                xVar = "mpd" & Mid$(xTag, 4, 8) & _
                    BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError
            End While

            If xAttributes <> "" Then
                'set encryption
                If bOn Then
                    xAttributes = Encrypt(xAttributes, xPassword)
                Else
                    xAttributes = BaseMethods.Decrypt(xAttributes)
                End If

                'create/set variables
                iIndex = 0
                While xAttributes <> ""
                    'split value into as many variables as required
                    iIndex = iIndex + 1
                    xVar = "mpd" & Mid$(xTag, 4, 8) & _
                        BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    oDoc.Variables(xVar).Value = Left$(xAttributes, 65000)
                    xAttributes = Mid$(xAttributes, 65001)
                End While

                'delete any remaining unused variables
                iIndex = iIndex + 1
                xVar = "mpd" & Mid$(xTag, 4, 8) & _
                    BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                xValue = ""
                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo ProcError
                While xValue <> ""
                    oDoc.Variables(xVar).Delete()
                    iIndex = iIndex + 1
                    xVar = "mpd" & Mid$(xTag, 4, 8) & _
                        BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    xValue = ""
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo ProcError
                End While
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function GenerateDocVarID(Optional ByVal oDoc As Word.Document = Nothing) As String
            'generates the 8-digit id used to link a content control
            'with the doc vars that hold its attributes
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GenerateDocVarID"
            Dim xDocVarID As String = ""
            Dim xValue As String = ""

            On Error GoTo ProcError

            If oDoc Is Nothing Then _
                    oDoc = GlobalMethods.CurWordApp.ActiveDocument

            xValue = "x"
            While xValue <> ""
                Randomize()
                xDocVarID = CStr(CLng(100000000 * Rnd()))
                xDocVarID = BaseMethods.CreatePadString(8 - Len(xDocVarID), "0") & xDocVarID

                'test for conflict
                xValue = ""
                On Error Resume Next
                xValue = oDoc.Variables("mpo" & xDocVarID).Value
                On Error GoTo ProcError
            End While

            GenerateDocVarID = xDocVarID

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetSegmentPropertyValue(ByVal xObjectData As String, _
                                                ByVal xProperty As String) As String
            'gets the value of the specified segment property
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetSegmentPropertyValue"
            Dim iPos1 As Integer = 0
            Dim iPos2 As Integer = 0
            Dim xValue As String = ""

            On Error GoTo ProcError

            iPos1 = InStr(xObjectData, xProperty & "=")
            If iPos1 > 0 Then
                'property found - parse from object data
                iPos1 = iPos1 + Len(xProperty) + 1

                'get delimiting pipe - it may or may not exist
                iPos2 = InStr(iPos1, xObjectData, "|")

                If iPos2 > 0 Then
                    'pipe exists - parse to pipe
                    xValue = Mid$(xObjectData, iPos1, iPos2 - iPos1)
                Else
                    'pipe doesn't exist - parse to end of string
                    xValue = Mid$(xObjectData, iPos1)
                End If
            End If

            GetSegmentPropertyValue = xValue

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsWordOpenXML(ByVal xXML As String) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsWordOpenXML"
            On Error GoTo ProcError
            IsWordOpenXML = (InStr(xXML, "<pkg:package ") > 0)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function ContainsContentControls(ByVal xXML As String) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.ContainsContentControls"
            On Error GoTo ProcError
            ContainsContentControls = (InStr(xXML, "<w:sdt>") > 0)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function FileFormat(ByVal oDocument As Word.Document) As mpFileFormats
            'returns the file format of the specified document
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.FileFormat"

            On Error GoTo ProcError

            With oDocument
                If GlobalMethods.g_bTrailerInProgress And (Val(GlobalMethods.CurWordApp.Version) > 14) Then
                    'GLOG 7496 (dm) - to allow for trailer in .doc files in Word 2013,
                    'treat all documents as open xml - we can use content controls
                    'for trailer in .doc because the bounding objects don't need to persist
                    FileFormat = mpFileFormats.OpenXML
                ElseIf .Path = "" Then
                    'doc has not been saved - use default save format
                    'to determine bounding object
                    'GLOG 5500: If Group Policy is enabled for DefaultFormat, value
                    'may not be a blank string - instead of testing for "Docx" or "",
                    'default to OpenXML for any value other than "Doc"
                    If UCase(.Application.DefaultSaveFormat) = "DOC" Then 'GLOG 5500 - case insensitive test
                        FileFormat = mpFileFormats.Binary
                    Else
                        FileFormat = mpFileFormats.OpenXML
                    End If
                ElseIf .SaveFormat > 11 And .SaveFormat < 16 Then
                    FileFormat = mpFileFormats.OpenXML
                Else
                    If Val(oDocument.Application.Version) > 14 Then
                        Dim o As Object = Nothing
                        o = oDocument
                        If o.CompatibilityMode > 11 Then
                            FileFormat = mpFileFormats.OpenXML
                        Else
                            FileFormat = mpFileFormats.Binary
                        End If
                    Else
                        FileFormat = mpFileFormats.Binary
                    End If
                End If
            End With

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function BoundingObjectType(ByVal xXML As String) As mpFileFormats
            'returns the bounding object of the specified xml
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.BoundingObjectType"
            Dim iType As mpFileFormats
            Dim oDocXML As Xml.XmlDocument
            Dim bContainsMPCC As Boolean = False
            Dim bContainsMPTags As Boolean = False
            Dim xPrefix As String = ""
            Dim oNs As Xml.XmlNamespaceManager

            oDocXML = New Xml.XmlDocument
            oDocXML.LoadXml(xXML)
            oNs = New Xml.XmlNamespaceManager(oDocXML.NameTable)

            If InStr(xXML, "<pkg:package ") > 0 Then
                'XML is OpenXML
                oNs.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main")

                bContainsMPCC = Not oDocXML.SelectSingleNode("descendant::w:sdt/w:sdtPr/w:tag[starts-with(@w:val, ""mp"")]", oNs) Is Nothing

                If bContainsMPCC Then
                    iType = mpFileFormats.OpenXML
                Else
                    'check for mp xml tags
                    bContainsMPTags = Not oDocXML.SelectSingleNode("descendant::w:customXml[@w:uri=" & GlobalMethods.mpNamespace & "]", oNs) Is Nothing

                    If bContainsMPTags Then
                        iType = mpFileFormats.Binary
                    Else
                        'leave as no bounding type
                    End If
                End If
            Else
                'XML is WordML
                xPrefix = BaseMethods.GetMP10SchemaPrefix(xXML)
                oNs.AddNamespace(xPrefix, GlobalMethods.mpNamespace)

                'check for mp xml tags
                bContainsMPTags = Not oDocXML.SelectSingleNode("descendant::" + xPrefix + ":*", oNs) Is Nothing

                If bContainsMPTags Then
                    iType = mpFileFormats.Binary
                Else
                    'leave as no bounding type
                End If
            End If

            BoundingObjectType = iType
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function ContentControlIsInline(ByVal oCC As Word.ContentControl) As Boolean
            'returns TRUE if functionally inline, i.e. contains no text, or other content controls
            'that start and end before or after it in the paragraph
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.ContentControlIsInline"
            Dim oParaRange As Word.Range = Nothing
            Dim oCC2 As Word.ContentControl
            Dim oTagRange As Word.Range = Nothing
            Dim oPrecedingRange As Word.Range = Nothing
            Dim oTrailingRange As Word.Range = Nothing

            On Error GoTo ProcError

            oTagRange = oCC.Range

            'check whether oCC contains multiple paragraphs
            If oTagRange.Paragraphs.Count > 1 Then
                ContentControlIsInline = False
                Exit Function
            ElseIf oTagRange.Characters.Last.Text = vbCr Then
                'GLOG 4891 (dm) - the above test will miss a 2-paragraph
                'cc in which the second paragraph is empty
                ContentControlIsInline = False
                Exit Function
            End If

            oParaRange = oTagRange.Paragraphs(1).Range

            'check whether there's preceding text in the paragraph
            oPrecedingRange = oTagRange.Duplicate
            oPrecedingRange.SetRange(oParaRange.Start, oTagRange.Start - 1)
            If oPrecedingRange.Text <> "" Then
                ContentControlIsInline = True
                Exit Function
            End If

            'check whether there's trailing text in the paragraph
            oTrailingRange = oTagRange.Duplicate
            oTrailingRange.SetRange(oTagRange.End + 1, oParaRange.End - 1)
            If oTrailingRange.Text <> "" Then
                ContentControlIsInline = True
                Exit Function
            End If

            'check whether there are preceding tags in the paragraph
            For Each oCC2 In oPrecedingRange.ContentControls
                If oCC2.Range.End < oPrecedingRange.End Then
                    ContentControlIsInline = True
                    Exit Function
                End If
            Next oCC2

            'check whether there are trailing tags in the paragraph
            For Each oCC2 In oTrailingRange.ContentControls
                If oCC2.Range.Start > oTrailingRange.Start Then
                    ContentControlIsInline = True
                    Exit Function
                End If
            Next oCC2

            ContentControlIsInline = False

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetNearestAncestor(ByVal oCC As Word.ContentControl, _
                                           ByVal xTargetElement As String) As Word.ContentControl
            'returns oCC's nearest ancestor of the specified type
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetNearestAncestor"
            Dim oAncestor As Word.ContentControl

            On Error GoTo ProcError

            oAncestor = oCC.ParentContentControl
            While Not oAncestor Is Nothing
                'JTS 4/12/10: If no specific Element, return immediate Parent
                If GetBaseName(oAncestor) = xTargetElement Or xTargetElement = "" Then
                    GetNearestAncestor = oAncestor
                    Exit Function
                Else
                    oAncestor = oAncestor.ParentContentControl
                End If
            End While

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub MoveContentControl(ByRef oCC As Word.ContentControl, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.MoveContentControl"
            Dim xTag As String = ""
            Dim bRebookmark As Boolean = False 'GLOG 6789

            On Error GoTo ProcError

            'get tag
            xTag = oCC.Tag

            'GLOG 6789 - check for existing bookmark
            bRebookmark = oNewRange.Document.Bookmarks.Exists("_" & xTag)

            'delete existing content control
            oCC.Delete()

            'add new content control
            oCC = oNewRange.ContentControls.Add()
            oCC.SetPlaceholderText(, , "")
            oCC.Tag = xTag

            'GLOG 6789 (dm) - restore bookmark
            If bRebookmark Then _
                oCC.Range.Bookmarks.Add("_" & xTag)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub MoveBookmark(ByRef oBmk As Word.Bookmark, ByVal oNewRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.MoveBookmark"
            Dim xTag As String = ""

            On Error GoTo ProcError

            'get tag
            xTag = oBmk.Name 'GLOG 6789 (dm)

            'delete existing bookmark
            oBmk.Delete()

            'add new bookmark
            oBmk = oNewRange.Bookmarks.Add(xTag)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Shared Function GetNamedContentControl(ByVal oRng As Word.Range, ByVal xName As String) As Word.ContentControl
            Dim oCC As Word.ContentControl
            Dim i As Short = 0
            For i = 1 To oRng.ContentControls.Count
                oCC = oRng.ContentControls(i)
                If BaseMethods.GetAttributeValue(oCC.Tag, "Name") = xName Then
                    GetNamedContentControl = oCC
                    Exit Function
                End If
            Next i
        End Function

        Public Shared Sub RetagSegmentContentControls(ByVal oRng As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RetagSegmentContentControls"

            Dim oCC As Word.ContentControl

            For Each oCC In oRng.ContentControls
                If Left$(oCC.Tag, 3) = "mps" Then
                    RetagContentControl(oCC)
                End If
            Next oCC

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub RetagContentControl(ByVal oCC As Word.ContentControl, Optional ByVal bDeleteBookmark As Boolean = True) 'GLOG 7363
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RetagContentControl"
            Dim xTag As String = ""
            Dim oDoc As Word.Document
            Dim xOldID As String = ""
            Dim xNewID As String = ""
            Dim xValue As String = ""
            Dim iIndex As Short = 0
            Dim xVar As String = ""
            Dim bShowHidden As Boolean = False

            On Error GoTo ProcError

            oDoc = oCC.Range.Document

            'retag
            xTag = oCC.Tag
            xOldID = Mid$(xTag, 4, 8)
            xNewID = BaseMethods.GenerateDocVarID(oDoc)
            oCC.Tag = Left$(xTag, 3) & xNewID & Mid$(xTag, 12)

            'add new doc var with same value as original one
            xValue = oDoc.Variables("mpo" & xOldID).Value
            oDoc.Variables.Add("mpo" & xNewID, xValue)

            'add new mpd doc vars as necessary
            If GetBaseNameFromTag(xTag) = "mSEG" Then
                xValue = "x"
                While xValue <> ""
                    xValue = ""
                    iIndex = iIndex + 1
                    xVar = "mpd" & xOldID & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo ProcError
                    If xValue <> "" Then
                        oDoc.Variables.Add("mpd" & xNewID & _
                            BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex), xValue)
                    End If
                End While
            End If
            'GLOG 7363: Delete old bookmark if specified
            If bDeleteBookmark Then
                'replace bookmark
                With oDoc.Bookmarks
                    bShowHidden = .ShowHidden
                    .ShowHidden = True
                    If .Exists("_" & xTag) Then _
                        .Item("_" & xTag).Delete()
                    '        .Add "_" & oCC.Tag, oCC.Range
                    .ShowHidden = bShowHidden
                End With
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Shared Sub RetagXMLNode(ByVal oNode As Word.XMLNode, Optional ByVal bDeleteBookmark As Boolean = True) 'GLOG 7363
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RetagXMLNode"
            Dim xTag As String = ""
            Dim xNewTag As String = ""
            Dim oDoc As Word.Document = Nothing
            Dim xOldID As String = ""
            Dim xNewID As String = ""
            Dim xValue As String = ""
            Dim iIndex As Short = 0
            Dim xVar As String = ""
            Dim bShowHidden As Boolean = False
            Dim oAttr As XMLNode = Nothing
            Dim xObjectData As String = ""
            Dim xAuthors As String = ""
            Dim xTagID As String = ""
            Dim xObjectDBID As String = ""

            On Error GoTo ProcError

            oDoc = oNode.Range.Document

            'retag by setting Reserved Attribute
            oAttr = oNode.SelectSingleNode("@Reserved")
            If Not oAttr Is Nothing Then
                xTag = oAttr.NodeValue
                xOldID = Mid$(xTag, 4, 8)
                xNewID = BaseMethods.GenerateDocVarID(oDoc)
                xNewTag = Left$(xTag, 3) & xNewID & Mid$(xTag, 12)
                oAttr.NodeValue = xNewTag

                'add new doc var with same value as original one
                xValue = oDoc.Variables("mpo" & xOldID).Value
                oDoc.Variables.Add("mpo" & xNewID, xValue)

                'add new mpd doc vars as necessary
                If oNode.BaseName = "mSEG" Then
                    xValue = "x"
                    While xValue <> ""
                        xValue = ""
                        iIndex = iIndex + 1
                        xVar = "mpd" & xOldID & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                        On Error Resume Next
                        xValue = oDoc.Variables(xVar).Value
                        On Error GoTo ProcError
                        If xValue <> "" Then
                            oDoc.Variables.Add("mpd" & xNewID & _
                                BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex), xValue)
                        End If
                    End While
                End If

                'GLOG 7363: Delete old bookmark if specified
                If bDeleteBookmark Then
                    'replace bookmark
                    With oDoc.Bookmarks
                        bShowHidden = .ShowHidden
                        .ShowHidden = True
                        If .Exists("_" & xTag) Then _
                            .Item("_" & xTag).Delete()
                        '            .Add "_" & xNewTag, oNode.Range
                        .ShowHidden = bShowHidden
                    End With
                End If
            Else
                'Reserved Attribute doesn't exist in DeletedScope XML -
                'Generate new Tag and Doc Variable
                xNewID = BaseMethods.GenerateDocVarID(oDoc)
                oAttr = oNode.SelectSingleNode("@ObjectData")
                If Not oAttr Is Nothing Then _
                    xObjectData = oAttr.NodeValue
                oAttr = oNode.SelectSingleNode("@TagID")
                If Not oAttr Is Nothing Then _
                    xTagID = oAttr.NodeValue

                If xTagID = "" Then
                    oAttr = oNode.SelectSingleNode("@Name")
                    If oAttr.Text <> "" Then
                        xTagID = oAttr.NodeValue
                    End If
                End If
                Select Case oNode.BaseName
                    Case "mSEG"
                        'append segment id
                        Dim xSegmentID As String
                        xSegmentID = BaseMethods.GetSegmentPropertyValue(xObjectData, "SegmentID")
                        If (Len(xSegmentID) < 19) Then _
                            xSegmentID = BaseMethods.CreatePadString(19 - Len(xSegmentID), "0") & xSegmentID

                        'tag can only contain alphanumeric characters
                        xSegmentID = Replace(xSegmentID, "-", "m")
                        xSegmentID = Replace(xSegmentID, ".", "d")

                        xNewTag = xNewTag & xSegmentID & BaseMethods.CreatePadString(9, "0")
                    Case "mVar"
                        xNewTag = "mpv" & xNewID
                        'append object database id
                        xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                            mpVariableProperties.mpVariableProperty_ObjectDatabaseID, "mVar", xTagID)
                        xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
                        xNewTag = xNewTag & xObjectDBID & BaseMethods.CreatePadString(22, "0")
                    Case "mBlock"
                        xNewTag = "mpb" & xNewID
                        'append object database id
                        xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                            mpBlockProperties.mpBlockProperty_ObjectDatabaseID, "mBlock")
                        xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
                        xNewTag = xNewTag & xObjectDBID & BaseMethods.CreatePadString(22, "0")
                    Case "mDel"
                        xNewTag = "mpd" & xNewID
                        xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                            mpVariableProperties.mpVariableProperty_ObjectDatabaseID, "mVar", xTagID)
                        xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
                        xNewTag = xNewTag & xObjectDBID & BaseMethods.CreatePadString(22, "0")
                    Case "mSubVar"
                        xNewTag = "mpu" & xNewID & BaseMethods.CreatePadString(28, "0")
                    Case "mDocProps"
                        xNewTag = "mpp" & xNewID & BaseMethods.CreatePadString(28, "0")
                    Case "mSecProps"
                        xNewTag = "mpc" & xNewID & BaseMethods.CreatePadString(28, "0")
                    Case Else
                        Exit Sub
                End Select
                xValue = xTagID
                'Set Document Variable values
                For Each oAttr In oNode.Attributes
                    If oAttr.BaseName <> "TagID" And oAttr.BaseName <> "Name" Then
                        xValue = xValue & "��" & oAttr.BaseName & "=" & BaseMethods.Decrypt(oAttr.NodeValue)
                    End If
                Next oAttr
                xValue = BaseMethods.Encrypt(xValue, "")
                oDoc.Variables.Add("mpo" & xNewID, xValue)
                oNode.Attributes.Add("Reserved", "").NodeValue = xNewTag
                '        oDoc.Bookmarks.Add "_" & xNewTag, oNode.Range
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Shared Sub AddDocVarToStore(ByVal xName As String, Optional ByRef bFound As Boolean = False)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.AddDocVarToStore"
            Dim xValue As String = ""
            Dim xExisting As String = ""

            On Error Resume Next
            xValue = GlobalMethods.CurWordApp.ActiveDocument.Variables(xName).Value
            On Error GoTo ProcError

            If xValue <> "" Then
                'add to store only if not already there
                If GlobalMethods.g_oDocVars Is Nothing Then _
                        GlobalMethods.g_oDocVars = New Collection
                On Error Resume Next
                xExisting = GlobalMethods.g_oDocVars(xName)
                On Error GoTo ProcError
                If xExisting = "" Then _
                    GlobalMethods.g_oDocVars.Add(xValue, xName)
            End If

            '10/25/11 (dm) - optional byref parameter
            bFound = (xValue <> "")

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Shared Function GetAssociatedDocVars(ByVal xTag As String) As String
            'gets all variables associated with the specified tag
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetAssociatedDocVars"
            Dim xVars As String = ""
            Dim xVar As String = ""
            Dim iIndex As Short = 0
            Dim xValue As String = ""
            Dim xmpdRoot As String = ""
            Dim xDeletedVars As String = ""
            Dim oDeleteScope As DeleteScope

            On Error GoTo ProcError

            'there's always exactly one mpo variable
            xVars = "mpo" & Mid$(xTag, 4, 8)

            'get any mpd variables
            If GetBaseNameFromTag(xTag) = "mSEG" Then
                xmpdRoot = "mpd" & Mid$(xTag, 4, 8)
                xValue = "x"
                While xValue <> ""
                    xValue = ""
                    iIndex = iIndex + 1
                    xVar = xmpdRoot & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    On Error Resume Next
                    xValue = GlobalMethods.CurWordApp.ActiveDocument.Variables(xVar).Value
                    On Error GoTo ProcError
                    If xValue <> "" Then
                        xVars = xVars & "|" & xVar

                        'add the doc vars for the tags/ccs in the deleted scopes xml (10/25/10, dm)
                        oDeleteScope = New DeleteScope
                        xDeletedVars = oDeleteScope.GetAssociatedDocVars(BaseMethods.Decrypt(xValue))
                        If xDeletedVars <> "" Then _
                            xVars = xVars & "|" & xDeletedVars
                    End If
                End While
            End If

            GetAssociatedDocVars = xVars

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Shared Sub AddDocVarsToStore(ByVal oRange As Word.Range)
            Const mpThisFunction As String = "BaseMethods.WordDoc.AddDocVarsToStore"
            Dim xTags As String = ""
            Dim vTags As Object = Nothing
            Dim i As Short = 0
            Dim xVars As String = ""
            Dim vVars As Object = Nothing
            Dim j As Short = 0
            Dim bContentControls As Boolean = False
            Dim xTagID As String = ""

            'determine whether content controls or XML Tags are used
            bContentControls = BaseMethods.FileFormat(oRange.Document) = mpFileFormats.OpenXML
            xTags = GetMPContentTagsInRange(oRange, bContentControls)
            If xTags <> "" Then
                vTags = Split(xTags, "|")
                For i = 0 To UBound(vTags)
                    xVars = BaseMethods.GetAssociatedDocVars(vTags(i))
                    vVars = Split(xVars, "|")
                    For j = 0 To UBound(vVars)
                        BaseMethods.AddDocVarToStore(vVars(j))
                    Next j

                    'add deactivation doc var if it exists (5/12/11, dm)
                    If GetBaseNameFromTag(vTags(i)) = "mSEG" Then
                        xTagID = GetAttributeValue(vTags(i), "TagID")
                        '                BaseMethods.AddDocVarToStore "Deactivation_" & xTagID

                        'add snapshot doc vars (10/25/11, dm)
                        Dim bFound As Boolean = False
                        bFound = True
                        j = 0
                        While bFound
                            j = j + 1
                            BaseMethods.AddDocVarToStore("Finished_" & CStr(j) & "_" & xTagID, bFound)
                        End While
                        BaseMethods.AddDocVarToStore("FinishedConfig_" & xTagID)
                    End If
                Next i
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Shared Function GetMPContentTagsInRange(ByVal oRange As Word.Range, _
                                                 ByVal bContentControls As Boolean) As String
            'returns a pipe-delimited string representing the tags of all content controls
            'or xml tags in the specified range
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetMPContentTagsInRange"
            Dim xTags As String = ""
            Dim xTag As String = ""
            Dim oCC As Word.ContentControl = Nothing
            Dim oNode As Word.XMLNode = Nothing
            Dim oReserved As Word.XMLNode = Nothing

            On Error GoTo ProcError

            If bContentControls Then
                For Each oCC In oRange.ContentControls
                    xTags = xTags & oCC.Tag & "|"
                Next oCC
            Else
                '10.2: Tag is in Reserved Attribute of XML Node
                For Each oNode In oRange.XMLNodes
                    oReserved = oNode.SelectSingleNode("@Reserved")
                    If Not oReserved Is Nothing Then
                        xTag = oReserved.NodeValue
                        xTags = xTags & xTag & "|"
                    End If
                Next oNode
            End If

            'strip trailing pipe
            If xTags <> "" Then _
                xTags = Left$(xTags, Len(xTags) - 1)

            GetMPContentTagsInRange = xTags

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub TrimContentControlMarkers(ByRef oRange As Word.Range, _
            ByVal bAtStart As Boolean, ByVal bAtEnd As Boolean)
            'shrinks range to exclude content control boundary markers at start and/or end
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.TrimContentControlMarkers"

            On Error GoTo ProcError

            With oRange
                'exit if no content controls in range
                If .ContentControls.Count = 0 Then Exit Sub

                'trim cc markers at start
                If bAtStart Then
                    While .Start = .ContentControls(1).Range.Start - 1
                        .MoveStart()
                        If .ContentControls.Count = 0 Then Exit Sub
                    End While
                End If

                'trim cc markers at end
                If bAtEnd Then
                    While (.End = .ContentControls(1).Range.End + 1) Or _
                            (.End = .ContentControls(.ContentControls.Count).Range.End + 1)
                        .MoveEnd(, -1)
                        If .ContentControls.Count = 0 Then Exit Sub
                    End While
                End If
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub CCAdjustedEndKey(Optional ByVal iUnit As WdUnits = WdUnits.wdLine, _
            Optional ByVal iExtend As WdMovementType = WdMovementType.wdMove)
            'this is to compensate for the fact that GlobalMethods.CurWordApp.Selection.EndKey will leave the cursor
            'before any content control closing tags at the end of the specified unit -
            'use for consistency with the way EndKey behaves with xml tags hidden
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.CCAdjustedEndKey"
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            With GlobalMethods.CurWordApp
                .Application.Selection.EndKey(iUnit, iExtend)

                oCC = .Application.Selection.Range.ParentContentControl
                While Not oCC Is Nothing
                    If .Application.Selection.End = oCC.Range.End Then
                        If iExtend = WdMovementType.wdMove Then
                            'move
                            .Application.Selection.Move()
                        Else
                            'extend
                            .Application.Selection.MoveEnd()
                        End If
                        oCC = .Application.Selection.Range.ParentContentControl
                    Else
                        oCC = Nothing
                    End If
                End While
            End With
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function CreatePadString(ByVal iLength As Integer, ByVal xChar As String) As String '8736
            If iLength < 1 Then
                iLength = 0
            End If

            Return New String(xChar, iLength)
        End Function

        Public Shared Sub CCAdjustedHomeKey(Optional ByVal iUnit As WdUnits = WdUnits.wdLine, _
            Optional ByVal iExtend As WdMovementType = WdMovementType.wdMove)
            'this is to compensate for the fact that GlobalMethods.CurWordApp.Selection.HomeKey will leave the cursor
            'after any content control start tags at the start of the specified unit -
            'use for consistency with the way HomeKey behaves with xml tags hidden
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.CCAdjustedHomeKey"
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            GlobalMethods.CurWordApp.Selection.HomeKey(iUnit, iExtend)

            oCC = GlobalMethods.CurWordApp.Selection.Range.ParentContentControl
            While Not oCC Is Nothing
                If GlobalMethods.CurWordApp.Selection.Start = oCC.Range.Start Then
                    If iExtend = WdMovementType.wdMove Then
                        'move
                        GlobalMethods.CurWordApp.Selection.Move(, -1)
                    Else
                        'extend
                        GlobalMethods.CurWordApp.Selection.MoveStart(, -1)
                    End If
                    oCC = GlobalMethods.CurWordApp.Selection.Range.ParentContentControl
                Else
                    oCC = Nothing
                End If
            End While
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub AddDocVarsFromStore(ByVal xTag As String)
            'adds the doc vars associated with xTag from the store if they're not already in the doc
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.AddDocVarsFromStore"
            Dim xVar As String = ""
            Dim xValue As String = ""
            Dim xDocVarID As String = ""
            Dim iIndex As Short = 0
            Dim xDeletedVars As String = ""
            Dim oDeleteScope As DeleteScope
            Dim vDeletedVars As Object = Nothing
            Dim i As Short = 0
            Dim xTagID As String = ""

            On Error GoTo ProcError

            xDocVarID = Mid$(xTag, 4, 8)
            xVar = "mpo" & xDocVarID
            On Error Resume Next
            xValue = GlobalMethods.CurWordApp.ActiveDocument.Variables(xVar).Value
            On Error GoTo ProcError

            If xValue = "" Then
                'add mpo variable
                On Error Resume Next
                xValue = GlobalMethods.g_oDocVars(xVar)
                On Error GoTo ProcError
                If xValue <> "" Then _
                    GlobalMethods.CurWordApp.ActiveDocument.Variables.Add(xVar, xValue)

                If GetBaseNameFromTag(xTag) = "mSEG" Then
                    'add mpd doc vars
                    xValue = "x"
                    While xValue <> ""
                        xValue = ""
                        iIndex = iIndex + 1
                        xVar = "mpd" & xDocVarID & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & _
                            CStr(iIndex)
                        On Error Resume Next
                        xValue = GlobalMethods.g_oDocVars(xVar)
                        On Error GoTo ProcError
                        If xValue <> "" Then
                            On Error Resume Next
                            GlobalMethods.CurWordApp.ActiveDocument.Variables.Add(xVar, xValue)
                            On Error GoTo ProcError

                            'add the doc vars for the tags/ccs in the deleted scopes xml
                            oDeleteScope = New DeleteScope
                            xDeletedVars = oDeleteScope.GetAssociatedDocVars(BaseMethods.Decrypt(xValue))
                            If xDeletedVars <> "" Then
                                vDeletedVars = Split(xDeletedVars, "|")
                                For i = 0 To UBound(vDeletedVars)
                                    xValue = ""
                                    On Error Resume Next
                                    xValue = GlobalMethods.g_oDocVars(vDeletedVars(i))
                                    If xValue <> "" Then _
                                        GlobalMethods.CurWordApp.ActiveDocument.Variables.Add(vDeletedVars(i), xValue)
                                    On Error GoTo ProcError
                                Next i
                            End If
                        End If
                    End While

                    '            'add deactivation doc var if it exists (5/12/11, dm)
                    '            xTagID = GetAttributeValue(xTag, "TagID")
                    '            xVar = "Deactivation_" & xTagID
                    '            On Error Resume Next
                    '            xValue = g_oDocVars(xVar)
                    '            On Error GoTo ProcError
                    '            If xValue <> "" Then _
                    '                GlobalMethods.CurWordApp.ActiveDocument.Variables.Add xVar, xValue

                    'add snapshot doc vars (10/25/11, dm)
                    xValue = ""
                    xTagID = GetAttributeValue(xTag, "TagID")
                    xVar = "FinishedConfig_" & xTagID
                    On Error Resume Next
                    xValue = GlobalMethods.g_oDocVars(xVar)
                    On Error GoTo ProcError
                    If xValue <> "" Then
                        GlobalMethods.CurWordApp.ActiveDocument.Variables(xVar).Value = xValue
                        xValue = ""
                        iIndex = 1
                        xVar = "Finished_" & CStr(iIndex) & "_" & xTagID
                        On Error Resume Next
                        xValue = GlobalMethods.g_oDocVars(xVar)
                        On Error GoTo ProcError
                        While xValue <> ""
                            GlobalMethods.CurWordApp.ActiveDocument.Variables(xVar).Value = xValue
                            xValue = ""
                            iIndex = iIndex + 1
                            xVar = "Finished_" & CStr(iIndex) & "_" & xTagID
                            On Error Resume Next
                            xValue = GlobalMethods.g_oDocVars(xVar)
                            On Error GoTo ProcError
                        End While
                    End If
                End If
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function IsForteTag(ByVal oTag As Word.XMLNode) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsForteTag"
            Dim xBaseName As String = ""

            On Error GoTo ProcError

            xBaseName = oTag.BaseName
            Select Case xBaseName
                Case "mSEG", "mVar", "mSubVar", "mBlock", "mDel", "mSecProps", "mDocProps"
                    IsForteTag = True
                Case Else
                    IsForteTag = False
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsForteContentControl(ByVal oCC As Word.ContentControl) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsForteContentControl"
            Dim xBaseName As String = ""

            On Error GoTo ProcError

            xBaseName = GetBaseName(oCC)
            Select Case xBaseName
                Case "mSEG", "mVar", "mSubVar", "mBlock", "mDel", "mSecProps", "mDocProps"
                    IsForteContentControl = True
                Case Else
                    IsForteContentControl = False
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetTag(ByVal oWordTag As Word.XMLNode) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTag"
            Dim oReserved As Word.XMLNode = Nothing

            On Error GoTo ProcError

            oReserved = oWordTag.SelectSingleNode("@Reserved")
            If Not oReserved Is Nothing Then
                GetTag = oReserved.NodeValue
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function IsPostInjunctionWordVersion() As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.IsPostInjunctionWordVersion"
            Dim xXML As String = ""
            Dim iCount As Short = 0
            Dim oDoc As Word.Document
            Dim xWordBuild As String = ""
            Dim xIsPostInjunction As String = ""

            On Error GoTo ProcError
            If Val(GlobalMethods.CurWordApp.Version) >= 14 Then
                IsPostInjunctionWordVersion = True
            ElseIf GlobalMethods.g_iIsPostInjunctionWord <> 0 Then
                'global variable has been set
                If GlobalMethods.g_iIsPostInjunctionWord = 1 Then
                    IsPostInjunctionWordVersion = True
                Else
                    IsPostInjunctionWordVersion = False
                End If
            Else
                xWordBuild = RegAPI.GetKeyValue(RegAPI.HKEY_CURRENT_USER, "Software\The Sackett Group\Deca", "LastUsedWordBuild")
                xIsPostInjunction = RegAPI.GetKeyValue(RegAPI.HKEY_CURRENT_USER, "Software\The Sackett Group\Deca", "IsPostInjunction")

                If xWordBuild <> GlobalMethods.CurWordApp.Build Then
                    'we're in a different Word build.  Check if this is post-injunction or not
                    'by inserting a tag and seeing if it goes into the document
                    xXML = "<?xml version=""1.0"" standalone=""yes""?>"
                    xXML = xXML & "<?mso-application progid=""Word.Document""?><w:wordDocument xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""http://schemas.microsoft.com/office/word/2003/wordml"" xmlns:ns0=""" & GlobalMethods.mpNamespace & """ >" & _
                        "<w:body><ns0:mSEG><w:p><w:r><w:t>test</w:t></w:r></w:p></ns0:mSEG></w:body></w:wordDocument>"

                    oDoc = GlobalMethods.CurWordApp.Documents.Add(, , , False)

                    iCount = oDoc.Content.XMLNodes.Count

                    oDoc.Content.InsertXML(xXML)

                    iCount = oDoc.Content.XMLNodes.Count - iCount
                    'GLOG 6564: Set Saved=true to avoid prompt with DMS
                    oDoc.Saved = True
                    oDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges)

                    IsPostInjunctionWordVersion = iCount = 0

                    RegAPI.UpdateKey(RegAPI.HKEY_CURRENT_USER, "Software\The Sackett Group\Deca", "LastUsedWordBuild", GlobalMethods.CurWordApp.Build)
                    RegAPI.UpdateKey(RegAPI.HKEY_CURRENT_USER, "Software\The Sackett Group\Deca", "IsPostInjunction", IIf(iCount = 0, 1, 0))
                Else
                    'the Word build is the same as last used - get the value from the registry
                    IsPostInjunctionWordVersion = xIsPostInjunction = "1"
                End If

                'set for later retreival
                If IsPostInjunctionWordVersion Then
                    GlobalMethods.g_iIsPostInjunctionWord = 1
                Else
                    GlobalMethods.g_iIsPostInjunctionWord = -1
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        'JTS 5/6/11: Moved to global module
        Public Shared Function GetTextFrame(oShape As Word.Shape) As Word.TextFrame
            'returns the TextFrame of the specified shape
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTextFrame"
            Dim oFrame As Word.TextFrame

            'GLOG 6146: Don't test for TextFrame unless shape is a Textbox
            If oShape.Type = Microsoft.Office.Core.MsoShapeType.msoTextBox Then
                On Error Resume Next
                oFrame = oShape.TextFrame
                On Error GoTo ProcError

                GetTextFrame = oFrame
            End If
            Exit Function

ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function HasTextFrameText(oShape As Word.Shape) As Boolean
            'GLOG 7009
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods. HasTextFrameText"
            On Error GoTo ProcError
            Dim oTextFrame As Word.TextFrame
            oTextFrame = BaseMethods.GetTextFrame(oShape)
            If Not oTextFrame Is Nothing Then
                HasTextFrameText = oTextFrame.HasText
            End If
            Exit Function
ProcError:
            'Return false if shape causes an error
            HasTextFrameText = False
        End Function
        Public Shared Function GetTextFrameText(oShape As Word.Shape) As String
            'GLOG 7009
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetTextFrameText"
            On Error GoTo ProcError
            Dim oTextFrame As Word.TextFrame
            GetTextFrameText = "" 'Initialize to empty string
            oTextFrame = BaseMethods.GetTextFrame(oShape)
            If Not oTextFrame Is Nothing Then
                If oTextFrame.HasText Then
                    GetTextFrameText = oTextFrame.TextRange.Text
                    'GLOG 8759
                    If GetTextFrameText Is Nothing Then
                        GetTextFrameText = vbCr ' May not return expected value if inside table
                    End If
                End If
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function
        Private Shared Function EncodeBase64(ByRef arrData() As Byte) As String
            EncodeBase64 = System.Convert.ToBase64String(arrData)
            'Dim objXML As xml.xmlDocument
            'Dim objNode As Xml.XmlElement

            '' help from MSXML
            'objXML = New xml.xmlDocument

            '' byte array to base64
            'objNode = objXML.CreateElement("b64")
            'objNode.dataType = "bin.base64"
            'objNode.nodeTypedValue = arrData
            'EncodeBase64 = objNode.InnerText

            'objNode = Nothing
            'objXML = Nothing
        End Function

        Private Shared Function DecodeBase64(ByVal strData As String) As Byte()
            'Dim objXML As xml.xmlDocument
            'Dim objNode As MSXML2.IXMLDOMElement

            ''10-13-11 (dm) - restore carriage returns and line feed characters -
            ''these are replaced by Word in the w:docVar node of both WordML and Open XML
            strData = Replace(strData, "_x005f", "", , , vbTextCompare)
            strData = Replace(strData, "_x000d_", Chr(13), , , vbTextCompare)
            strData = Replace(strData, "_x000a_", Chr(10), , , vbTextCompare)

            DecodeBase64 = System.Convert.FromBase64String(strData)
            '' help from MSXML
            'objXML = New xml.xmlDocument
            'objNode = objXML.createElement("b64")
            'objNode.dataType = "bin.base64"
            'objNode.text = strData
            'DecodeBase64 = objNode.nodeTypedValue

            'objNode = Nothing
            'objXML = Nothing
        End Function

        Public Shared Sub ReindexSnapshot(ByVal oDoc As Word.Document, _
                                   ByVal xOldTagID As String, _
                                   ByVal xNewTagID As String, _
                                   ByVal bDeleteOld As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.ReindexSnapshot"
            Dim xVarValue As String = ""
            Dim iIndex As Short = 0

            On Error Resume Next
            xVarValue = oDoc.Variables("FinishedConfig_" & xOldTagID).Value
            On Error GoTo ProcError

            If xVarValue <> "" Then
                oDoc.Variables.Add("FinishedConfig_" & xNewTagID, xVarValue)
                If bDeleteOld Then _
                    oDoc.Variables("FinishedConfig_" & xOldTagID).Delete()
                xVarValue = ""
                iIndex = 1
                On Error Resume Next
                xVarValue = oDoc.Variables("Finished_" & CStr(iIndex) & "_" & xOldTagID).Value
                On Error GoTo ProcError
                While xVarValue <> ""
                    oDoc.Variables.Add("Finished_" & CStr(iIndex) & "_" & xNewTagID, xVarValue)
                    If bDeleteOld Then _
                        oDoc.Variables("Finished_" & CStr(iIndex) & "_" & xOldTagID).Delete()
                    xVarValue = ""
                    iIndex = iIndex + 1
                    On Error Resume Next
                    xVarValue = oDoc.Variables("Finished_" & CStr(iIndex) & _
                        "_" & xOldTagID).Value
                    On Error GoTo ProcError
                End While
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function BeforeBlockLevelCC(ByVal oRange As Word.Range) As Boolean
            'returns true if immediately before a block-level content control
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.BeforeBlockLevelCC"
            Dim oCC As Word.ContentControl
            Dim lParaEnd As Integer = 0
            Dim oTestRange As Word.Range = Nothing

            On Error Resume Next
            oTestRange = oRange.Next(Word.WdUnits.wdCharacter)
            On Error GoTo ProcError
            If oTestRange Is Nothing Then Exit Function

            oTestRange = oRange.Duplicate
            oTestRange.Move()
            oCC = oTestRange.ParentContentControl
            If Not oCC Is Nothing Then
                If oTestRange.Start = oCC.Range.Start Then
                    lParaEnd = oRange.Paragraphs(1).Range.End - 1
                    If oCC.Range.End < lParaEnd Then
                        oTestRange.SetRange(oCC.Range.End, lParaEnd)
                        BeforeBlockLevelCC = (oTestRange.Text = "")
                    Else
                        BeforeBlockLevelCC = True
                    End If
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function AfterBlockLevelCC(ByVal oRange As Word.Range) As Boolean
            'returns true if immediately after a block-level content control
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.AfterBlockLevelCC"
            Dim oCC As Word.ContentControl
            Dim lParaStart As Integer = 0
            Dim oTestRange As Word.Range = Nothing

            On Error GoTo ProcError

            oTestRange = oRange.Duplicate
            If oTestRange.Start = 0 Then Exit Function
            oTestRange.Move(Word.WdUnits.wdCharacter, -1)
            oCC = oTestRange.ParentContentControl
            If Not oCC Is Nothing Then
                If oTestRange.End = oCC.Range.End Then
                    lParaStart = oRange.Paragraphs(1).Range.Start
                    If oCC.Range.Start > lParaStart Then
                        oTestRange.SetRange(lParaStart, oCC.Range.Start)
                        AfterBlockLevelCC = (oTestRange.Text = "")
                    Else
                        AfterBlockLevelCC = True
                    End If
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Function xReturnSmartQuotes(xString As String) As String
            'GLOG 6065: Ported from MP9
            Dim i As Short = 0
            Dim xTemp As String = ""

            xTemp = xString

            Do
                ' Search for Double Quotes
                i = InStr(xTemp, Chr(34))
                Select Case i
                    Case 0
                    Case 1
                        xTemp = xSubstituteFirst(xTemp, Chr(34), ChrW(8220))
                    Case Else
                        Select Case Mid(xTemp, i - 1, 1)
                            Case "-", " ", "(", "{", "[", _
                                Chr(13), Chr(11), Chr(10), Chr(12), Chr(9), _
                                Chr(30), Chr(150), Chr(151), Chr(160)
                                ' replace with opening quote after any type of hyphen, open brace or white space
                                xTemp = xSubstituteFirst(xTemp, Chr(34), ChrW(8220))
                            Case Else
                                ' otherwise replace with closing quote
                                xTemp = xSubstituteFirst(xTemp, Chr(34), ChrW(8221))
                        End Select
                End Select
            Loop While i > 0

            Do
                ' Search for Single Quotes/Apostrophes
                i = InStr(xTemp, Chr(39))
                Select Case i
                    Case 0
                    Case 1
                        xTemp = xSubstituteFirst(xTemp, Chr(39), ChrW(8216))
                    Case Else
                        Select Case Mid(xTemp, i - 1, 1)
                            Case "-", " ", "(", "{", "[", _
                                Chr(13), Chr(11), Chr(10), Chr(12), Chr(9), _
                                Chr(30), Chr(150), Chr(151), Chr(160)
                                ' replace with opening quote after any type of hyphen, open brace or white space
                                xTemp = xSubstituteFirst(xTemp, Chr(39), ChrW(8216))
                            Case Else
                                ' otherwise replace with closing quote
                                xTemp = xSubstituteFirst(xTemp, Chr(39), ChrW(8217))
                        End Select
                End Select
            Loop While i > 0

            xReturnSmartQuotes = xTemp

        End Function
        Public Shared Function xSubstituteFirst(ByVal xString As String, xSearch As String, xReplace As String) As String
            'replaces first xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer = 0
            Dim xNewString As String = ""

            '   get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            '   remove switch all chars
            If iSeachPos Then
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
            End If

            xNewString = xNewString & xString
            xSubstituteFirst = xNewString
        End Function

        Public Shared Function GetCCSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Integer
            'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
            'content controls that would otherwise force a new paragraph to be created above the controls()
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.GetCCSafeParagraphStart"
            Dim i As Short = 0
            Dim rngPara As Word.Range = Nothing
            Dim iCount As Short = 0
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            rngPara = oInsertionRange.Paragraphs(1).Range

            'count multiparagraph tags at start of paragraph
            For i = 1 To rngPara.ContentControls.Count
                oCC = rngPara.ContentControls(i)
                If (oCC.Range.Start = rngPara.Start + i) And _
                        (oCC.Range.Paragraphs.Count > 1) Then
                    iCount = iCount + 1
                Else
                    Exit For
                End If
            Next i

            GetCCSafeParagraphStart = rngPara.Start + iCount

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        'GLOG 6966: Return GlobalMethods.CurWordApp.InchesToPoints accounting for possibility of
        'Decimal separator other than '.' being set in Regional Settings
        Public Shared Function InchesToPointsIntl(ByVal xInput As String) As Single
            Dim xTest As String = ""
            Dim xDecimal As String = ""
            'Determine current decimal separator
            xTest = CStr(5 / 2)
            xDecimal = Mid$(xTest, 2, 1)
            If xDecimal <> "." Then _
                xInput = Replace(xInput, ".", xDecimal)
            Return GlobalMethods.CurWordApp.InchesToPoints(CSng(xInput))
        End Function

        Public Shared Function TempPath() As String
            TempPath = System.IO.Path.GetTempPath()
        End Function

        Public Shared Sub RetagmSEG(ByVal oRange As Word.Range, ByVal xOldTag As String)
            'added for GLOG 6939 (dm)
            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.RetagmSEG"
            Dim oDoc As Word.Document
            Dim xOldID As String = ""
            Dim xNewID As String = ""
            Dim xValue As String = ""
            Dim iIndex As Short = 0
            Dim xVar As String = ""
            Dim bShowHidden As Boolean = False
            Dim xNewTag As String = ""

            On Error GoTo ProcError

            oDoc = oRange.Document

            'retag
            xOldID = Mid$(xOldTag, 4, 8)
            xNewID = BaseMethods.GenerateDocVarID(oDoc)
            xNewTag = Left$(xOldTag, 3) & xNewID & Mid$(xOldTag, 12)

            'add new doc var with same value as original one
            'GLOG 7911 (dm) - exit if no corresponding doc var -
            'this method runs on existing documents, so we
            'can't count on everything being there
            On Error Resume Next
            xValue = oDoc.Variables("mpo" & xOldID).Value
            On Error GoTo ProcError
            If xValue = "" Then Exit Sub

            oDoc.Variables.Add("mpo" & xNewID, xValue)

            'add new mpd doc vars as necessary
            If GetBaseNameFromTag(xOldTag) = "mSEG" Then
                xValue = "x"
                While xValue <> ""
                    xValue = ""
                    iIndex = iIndex + 1
                    xVar = "mpd" & xOldID & BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo ProcError
                    If xValue <> "" Then
                        oDoc.Variables.Add("mpd" & xNewID & _
                            BaseMethods.CreatePadString(2 - Len(CStr(iIndex)), "0") & CStr(iIndex), xValue)
                    End If
                End While
            End If

            'replace bookmark
            With oDoc.Bookmarks
                bShowHidden = .ShowHidden
                .ShowHidden = True
                .Add("_" & xNewTag, oRange)
                .ShowHidden = bShowHidden
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        'GLOG : 6979 : CEH
        Public Shared Sub xStringToArray(ByVal xString As String, _
                                arrP(,) As String, _
                                Optional iNumCols As Short = 1, _
                                Optional xSep As String = ",")

            Const mpThisFunction As String = "LMP.Forte.MSWord.BaseMethods.xStringToArray"

            Dim iNumSeps As Integer = 0
            Dim iNumEntries As Integer = 0
            Dim iSepPos As Integer = 0
            Dim xEntry As String = ""
            Dim i As Integer = 0
            Dim j As Integer = 0

            On Error GoTo ProcError

            '   get ubound of arrP - count delimiter
            '   then divide by iNumCols
            iNumSeps = BaseMethods.CountChrs(xString, xSep)
            iNumEntries = (iNumSeps + 1) / iNumCols

            ReDim arrP(iNumEntries - 1, iNumCols - 1)

            For i = 0 To iNumEntries - 1
                For j = 0 To iNumCols - 1
                    '           get next entry & store
                    iSepPos = InStr(xString, xSep)
                    If iSepPos Then
                        xEntry = Left(xString, iSepPos - 1)
                    Else
                        xEntry = xString
                    End If
                    arrP(i, j) = xEntry

                    '           remove entry from xstring
                    If iSepPos Then _
                        xString = Mid(xString, iSepPos + 1)
                Next j
            Next i
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
    End Class
End Namespace
