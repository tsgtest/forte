'**************************************************************************************************
'Summary of Forte encryption/compression -
'Through mp10.1.2.0, Forte first encrypted strings using the crypto api,
'then compressed that encrypted string.  Unfortunately, since encrypted strings
'were being compressed, the length of the compressed strings generally exceeded that of uncompressed
'strings, resulting in larger Word files.  Beginning in v10.1.2.1, Forte first compresses strings,
'then encrypts them. To encrypt successfully after compressing, we had to change to a proprietary
'encryption algorithm that did not alter the second byte of a unicode character.  Had we continued to
'use the crypto api, Word would err attempting to write the encrypted string to file.

'The proprietary algorithm generates a random eight character number, then cycles through the
'even bytes, offsetting the existing values by the corresponding digit from the random number.
'When all digits have been used, the next even byte is offset by the first digit in the number,
'and the cycle starts again.  The random number is itself encrypted and prepended to the string.
'We offonly even bytes because Word errs when we attempt to write a string with all bytes offset.

'In order to be backward compatible with existing documents whose mp10 tag attributes contain
'the original encryption/compression method, we prepend an algorithm identifier to each string
'that is encrypted/compressed using the new method.  On decryption, if the identifier is found,
'the new decryption/decompression method is used.
'**************************************************************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Namespace LMP.Forte.MSWord
    Public Class Encryption
        Private Const mpAlgorithmIdentifier As String = "#mp!@"

        Private Declare Function GetLastError Lib "kernel32" () As Integer

        Private Declare Function CryptAcquireContext Lib "advapi32.dll" Alias "CryptAcquireContextA" _
            (ByRef phProv As Integer, _
             ByVal pszContainer As String, _
             ByVal pszProvider As String, _
             ByVal dwProvType As Integer, _
             ByVal dwFlags As Integer) As Integer

        Private Declare Function CryptGetProvParam Lib "advapi32.dll" _
            (ByVal hProv As Integer, _
             ByVal dwParam As Integer, _
             ByRef pbData As String, _
             ByRef pdwDataLen As Integer, _
             ByVal dwFlags As Integer) As Integer

        Private Declare Function CryptCreateHash Lib "advapi32.dll" _
            (ByVal hProv As Integer, _
             ByVal Algid As Integer, _
             ByVal hKey As Integer, _
             ByVal dwFlags As Integer, _
             ByRef phHash As Integer) As Integer

        Private Declare Function CryptHashData Lib "advapi32.dll" _
            (ByVal hHash As Integer, _
             ByVal pbData As String, _
             ByVal dwDataLen As Integer, _
             ByVal dwFlags As Integer) As Integer

        Private Declare Function CryptDeriveKey Lib "advapi32.dll" _
            (ByVal hProv As Integer, _
             ByVal Algid As Integer, _
             ByVal hBaseData As Integer, _
             ByVal dwFlags As Integer, _
             ByRef phKey As Integer) As Integer

        Private Declare Function CryptDestroyHash Lib "advapi32.dll" _
            (ByVal hHash As Integer) As Integer

        Private Declare Function CryptEncrypt Lib "advapi32.dll" _
            (ByVal hKey As Integer, _
             ByVal hHash As Integer, _
             ByVal Final As Integer, _
             ByVal dwFlags As Integer, _
             ByVal pbData As Integer, _
             ByRef pdwDataLen As Integer, _
             ByVal dwBufLen As Integer) As Integer

        Private Declare Function CryptDestroyKey Lib "advapi32.dll" _
            (ByVal hKey As Integer) As Integer

        Private Declare Function CryptReleaseContext Lib "advapi32.dll" _
            (ByVal hProv As Integer, _
             ByVal dwFlags As Integer) As Integer

        Private Declare Function CryptDecrypt Lib "advapi32.dll" _
            (ByVal hKey As Integer, _
             ByVal hHash As Integer, _
             ByVal Final As Integer, _
             ByVal dwFlags As Integer, _
             ByVal pbData As Integer, _
             ByRef pdwDataLen As Integer) As Integer

        Private Const SERVICE_PROVIDER As String = "Microsoft Base Cryptographic Provider v1.0"
        Private Const PROV_RSA_FULL As Integer = 1
        Private Const PP_NAME As Integer = 4
        Private Const PP_CONTAINER As Integer = 6
        Private Const CRYPT_NEWKEYSET As Integer = 8
        Private Const ALG_CLASS_DATA_ENCRYPT As Integer = 24576
        Private Const ALG_CLASS_HASH As Integer = 32768
        Private Const ALG_TYPE_ANY As Integer = 0
        Private Const ALG_TYPE_STREAM As Integer = 2048
        Private Const ALG_SID_RC4 As Integer = 1
        Private Const ALG_SID_MD5 As Integer = 3
        Private Const CALG_MD5 As Integer = ((ALG_CLASS_HASH Or ALG_TYPE_ANY) Or ALG_SID_MD5)
        Private Const CALG_RC4 As Integer = ((ALG_CLASS_DATA_ENCRYPT Or ALG_TYPE_STREAM) Or ALG_SID_RC4)
        Private Const ENCRYPT_ALGORITHM As Integer = CALG_RC4
        Private Const CRYPT_VERIFYCONTEXT As Integer = &HF0000000

        Private m_hCryptProv As Integer

        Private Function GetProvider(Password As String) As Boolean
            'Get handle to CSP
            Dim xContainer As String

            xContainer = vbNullString
            If CryptAcquireContext(m_hCryptProv, Password, SERVICE_PROVIDER, PROV_RSA_FULL, CRYPT_NEWKEYSET) = 0 Then
                If CryptAcquireContext(m_hCryptProv, Password, SERVICE_PROVIDER, PROV_RSA_FULL, 0) = 0 Then
                    'Could not acquire context for new key container
                    GetProvider = False
                    Exit Function
                End If
            End If

            GetProvider = True
        End Function
        Public Function EncryptData(ByVal Data As String, ByVal Password As String) As String
            EncryptData = EncryptDecrypt(Data, Password, True)
            ''*********If encrypted strings needed to be saved directly in XML, the code below
            ''*********would run multiple passes until no illegal characters were present.
            ''*********Currently this is handled by the CompressString function, so not needed here
            '    Dim xEncrypted As String
            '    Dim lEncryptionCount As integer
            '    Dim xTempPassword As String
            '    Dim xHex As String
            '
            '    lEncryptionCount = 0
            '    xTempPassword = Password & lEncryptionCount
            '    xEncrypted = EncryptDecrypt(Data, xTempPassword, True)
            '    'Loop until no Null chars - these can interfere with other string functions
            '    Do While (InStr(xEncrypted, chr(0)) > 0)
            '
            '        'Try the next password
            '        lEncryptionCount = lEncryptionCount + 1
            '        xTempPassword = Password & lEncryptionCount
            '        xEncrypted = EncryptDecrypt(Data, xTempPassword, True)
            '
            '        'Don't go on for ever, 1 million attempts should be plenty
            '        If lEncryptionCount = 999999 Then
            '            EncryptData = Data
            '            Err.Raise vbObjectError + 999, "EncryptData", "This data cannot be successfully encrypted"
            '            Exit Function
            '        End If
            '    Loop
            '    'Convert Loop count to 5-digit Hex
            '    xHex = Hex$(lEncryptionCount)
            '    xHex =mpbase.CreatePadString(5 - Len(xHex), "0") & xHex
            '
            '    'Build encrypted string, appending number of encryption iterations
            '    EncryptData = xEncrypted & xHex
        End Function
        Public Function DecryptData(ByVal Data As String, ByVal Password As String) As String
            DecryptData = EncryptDecrypt(Data, Password, False)
            ''******Code below for use if multiple-pass encryption were being implemented
            '    Dim xDecrypted As String
            '    Dim xTempPassword As String
            '    Dim lEncryptionCount As integer
            '    Dim xHex As String
            '
            '    'When encrypting we may have gone through a number of iterations
            '    'How many did we go through?
            '    xHex = "&H" & Right$(Data, 5)
            '    'Number stored in hex - convert to integer
            '    lEncryptionCount = CLng(xHex)
            '    'append number of iterations to password
            '    xTempPassword = Password & lEncryptionCount
            '    xDecrypted = EncryptDecrypt(Left$(Data, Len(Data) - 5), xTempPassword, False)
            '    DecryptData = xDecrypted
        End Function
        Private Function EncryptDecryptUsingAPI(ByVal Data As String, ByVal Password As String, ByVal bEncrypt As Boolean) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.cCryptoAPI.EncryptDecryptUsingAPI"

            Dim lLength As Integer
            Dim xTemp As String
            Dim hHash As Integer
            Dim hKey As Integer
            Dim bError As Boolean
            Dim lErrNumber As Integer
            Dim xErrDesc As String

            'Return data unchanged if error occurs at any point
            EncryptDecryptUsingAPI = Data

            If m_hCryptProv = 0 Then
                If Not GetProvider(Password) Then
                    lErrNumber = GetLastError()
                    If bEncrypt Then
                        xErrDesc = "Data could not be encrypted."
                    Else
                        xErrDesc = "Data could not be decrypted."
                    End If
                    bError = True
                    ' If Provider couldn't be acquired, return data unchanged
                    EncryptDecryptUsingAPI = Data
                    Class_Terminate()
                    GoTo HandleError
                End If
            End If

            '--------------------------------------------------------------------
            'The data will be encrypted with a session key derived from the
            'password.
            'The session key will be recreated when the data is decrypted
            'only if the password used to create the key is available.
            '--------------------------------------------------------------------
            'Create a hash object.
            If CryptCreateHash(m_hCryptProv, CALG_MD5, 0, 0, hHash) = 0 Then
                lErrNumber = GetLastError()
                xErrDesc = "Could not create hash object."
                bError = True
                GoTo CleanUp
            End If

            'Hash the password.
            If CryptHashData(hHash, Password, Len(Password), 0) = 0 Then
                lErrNumber = GetLastError()
                xErrDesc = "Could not hash the password."
                bError = True
                GoTo CleanUp
            End If

            'Derive a session key from the hash object.
            If CryptDeriveKey(m_hCryptProv, ENCRYPT_ALGORITHM, hHash, 0, hKey) = 0 Then
                lErrNumber = GetLastError()
                xErrDesc = "Could not create the session key."
                bError = True
                GoTo CleanUp
            End If

            'Do the work
            xTemp = Data
            lLength = Len(xTemp)
            'We are passing string pointer to API functions instead of the actual string
            'This is because VB will convert Unicode strings to ANSI when passing to API
            'and multi-byte characters will be corrupted
            If bEncrypt Then
                'Encrypt data.
                If CryptEncrypt(hKey, 0, 1, 0, xTemp, lLength, lLength) = 0 Then
                    lErrNumber = GetLastError()
                    xErrDesc = "Could not Encrypt the data."
                    bError = True
                    GoTo CleanUp
                End If
            Else
                'mpBase.Decrypt data.
                If CryptDecrypt(hKey, 0, 1, 0, xTemp, lLength) = 0 Then
                    lErrNumber = GetLastError()
                    xErrDesc = "Could not mpBase.Decrypt the data."
                    bError = True
                    GoTo CleanUp
                End If
            End If

            'This is what we return.
            EncryptDecryptUsingAPI = Mid$(xTemp, 1, lLength)

CleanUp:
            'Destroy session key.
            If hKey <> 0 Then
                CryptDestroyKey(hKey)
            End If

            'Destroy hash object.
            If hHash <> 0 Then
                CryptDestroyHash(hHash)
            End If
HandleError:
            If bError Then
                Err.Raise(lErrNumber, mpThisFunction, xErrDesc)
            End If
        End Function
        Private Function EncryptDecrypt(ByVal Data As String, ByVal Password As String, ByVal bEncrypt As Boolean) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.cCryptoAPI.EncryptDecrypt"
            Dim bUseCryptoAPI As Boolean

            On Error GoTo ProcError

            'Proprietary encrypted data string format
            'I#########PPPPPPPDDDDDDDD
            'I = id of algorithm - #mp!@
            '# = digit of eight digit random number
            'P = character in password
            'D = character in data

            If bEncrypt Then
                EncryptDecrypt = EncryptUsingMP(Data, Password)
            Else
                'decrypt -
                'check which decryption algorithm we should use -
                'older documents were using the CryptoAPI algorithm
                bUseCryptoAPI = Left$(Data, Len(mpAlgorithmIdentifier)) <> mpAlgorithmIdentifier

                If bUseCryptoAPI Then
                    EncryptDecrypt = EncryptDecryptUsingAPI(Data, Password, False)
                Else
                    EncryptDecrypt = DecryptUsingMP(Data, Password)
                End If
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function EncryptUsingMP(ByVal Data As String, ByVal Password As String) As String
            'encrypts the specified string using the specified password
            Const mpThisFunction As String = "LMP.Forte.MSWord.cCryptoAPI.EncryptUsingMP"
            Dim sRandom As Single
            Dim xRandom As String
            Dim xEncrypted As String
            Dim xDecrypted As String
            Dim lPos As Integer
            Dim iAsc As Short
            Dim iBytePos As Integer
            Dim xChar As String
            Dim aByte() As Byte

            'Encrypted data string format
            'I#########PPPPPPPDDDDDDDD
            'I = id of algorithm
            '# = digit of seven digit random number
            'P = character in password
            'D = character in data

            'get seven digit random number prefixed by algorithm ID
            Randomize()
            sRandom = Rnd() * 1000000000

            xRandom = Left$(CLng(sRandom), 8)

            'pad to eight digits if necessary
            xRandom = xRandom & BaseMethods.CreatePadString(8 - Len(xRandom), "0")

            'encrypt number
            For lPos = 1 To 8 Step 2
                iAsc = Mid$(xRandom, lPos, 2)
                xEncrypted = xEncrypted + chr(iAsc)
            Next lPos

            'trim off first two digits of the random number -
            'only digits 3 through 8 are used in the encryption of text
            xRandom = Mid$(xRandom, 3)

            'encrypt password -
            'cycle through characters in text, offsetting each by the number specified
            'by the corresponding digit of the random number - ie first char is offset
            'by the first digit of the number, second char is offset by the second
            'char, etc.  Repeat after x digits.
            aByte = System.Text.Encoding.Unicode.GetBytes(Data)

            For lPos = 0 To UBound(aByte) Step 2
                'offset even characters by "related" digit in the random number -
                'digits start with the third digit in the number, as the first
                'digit is the algorithm ID and the second digit is a dummy -
                'get related digit -
                'we leave odd characters alone because Word XML nodes won't
                'accept strings where odd characters have been manipulated
                Dim iIndex As Short
                Dim iDigit As Short

                iIndex = (lPos Mod 6) + 1
                iDigit = Mid$(xRandom, iIndex, 1)

                'offset byte
                Dim iNew As Short
                iNew = aByte(lPos) + iDigit
                If iNew > 255 Then
                    'cycle back into lower bytes
                    iNew = iNew - 256
                End If

                aByte(lPos) = iNew
            Next lPos

            EncryptUsingMP = mpAlgorithmIdentifier & xEncrypted & System.Text.Encoding.Unicode.GetString(aByte)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Private Function DecryptUsingMP(ByVal Data As String, ByVal Password As String) As String
            'decrypts the specified string if the password is correct
            Const mpThisFunction As String = "LMP.Forte.MSWord.cCryptoAPI.DecryptUsingMP"
            Dim xRandom As String
            Dim xDecrypted As String
            Dim lPos As Integer
            Dim lBytePos As Integer
            Dim iIndex As Short
            Dim iDigit As Short
            Dim aByte() As Byte

            'Encrypted data string format
            'I#########PPPPPPPDDDDDDDD
            'I = id of algorithm
            '# = digit of seven digit random number
            'P = character in password
            'D = character in data

            On Error GoTo ProcError

            aByte = System.Text.Encoding.Unicode.GetBytes(Mid$(Data, Len(mpAlgorithmIdentifier) + 1))

            'decrypt random number
            For lPos = 2 To 6 Step 2
                xRandom = xRandom & Format(aByte(lPos), "00")
            Next lPos

            Dim iNew As Short

            For lBytePos = 8 To UBound(aByte) Step 2
                iIndex = ((lBytePos - 8) Mod 6) + 1
                iDigit = Mid$(xRandom, iIndex, 1)

                iNew = aByte(lBytePos) - iDigit

                If iNew < 0 Then
                    'cycle back to highest byte values
                    iNew = 256 + iNew
                End If
                aByte(lBytePos) = iNew
            Next lBytePos

            DecryptUsingMP = Mid$(System.Text.Encoding.Unicode.GetString(aByte), Len(mpAlgorithmIdentifier))

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Private Sub Class_Terminate()
            'Release provider handle.
            If m_hCryptProv <> 0 Then
                CryptReleaseContext(m_hCryptProv, 0)
            End If
        End Sub
    End Class
End Namespace