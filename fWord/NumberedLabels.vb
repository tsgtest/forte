Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class NumberedLabels
        Public StartNumber As Integer = 1
        Public NumLabels As Integer
        Public StartRow As Short = 1
        Public StartColumn As Short = 1
        Public Description As String = "Labels"
        Public NumberFormat As String = "Arabic"
        Private m_iSeqIndex As Short = 0
        Private EndNumber As Integer
        Public Digits As Short = 1
        Private Columns As Short = 1
        Private Rows As Short = 1
        Private TableColumns As Short
        Private PageRows As Short
        Public VerticalFill As Boolean = False
        Private SeparatorColumns As Boolean
        Private SeparatorRows As Boolean
        Private m_oStatusForm As BatesStatusForm
        Private m_bContentControls As Boolean
        Private m_iDialogStartCol As Short
        Private Pages As Integer = 1
        Private LabelsPerPage As Short
        Public Function WriteLabels() As Integer
            Const mpThisFunction As String = "LMP.Forte.MSWord.NumberedLabels.WriteLabels"
            Dim rngPrimaryHeader As Range
            Dim rngPrimaryFooter As Range
            Dim rngFirstHeader As Range
            Dim rngFirstFooter As Range
            Dim rngMain As Range
            Dim secScope As Section
            Dim lAdjust As Double
            Dim bRet As Boolean
            Dim oTable As Word.Table
            Dim oTableRng As Word.Range
            Dim iCellsPerPage As Short
            Dim iNumCells As Short
            Dim iPages As Short
            Dim xWindow As String

            WriteLabels = False

            Try
                ''show status form
                m_oStatusForm = New BatesStatusForm
                m_oStatusForm.Description = Me.Description
                m_oStatusForm.Show(GlobalMethods.GetWordWin32Window())
                'DoEvents()

                xWindow = GlobalMethods.CurWordApp.ActiveDocument.ActiveWindow.Caption
                GlobalMethods.CurWordApp.Windows(xWindow).Activate()

                GlobalMethods.CurWordApp.ScreenUpdating = False

                Me.EndNumber = (Me.NumLabels + Me.StartNumber) - 1

                System.Windows.Forms.Application.DoEvents()
                secScope = GlobalMethods.CurWordApp.ActiveDocument.Sections.First
                System.Windows.Forms.Application.DoEvents()

                ' document ranges
                bGetDocRanges(rngMain, _
                            rngPrimaryHeader, _
                            rngPrimaryFooter, _
                            rngFirstHeader, _
                            rngFirstFooter, , _
                            secScope)


                'GLOG : 6377 : ceh
                m_bContentControls = BaseMethods.FileFormat(rngMain.Document) = LMP.Forte.MSWord.mpFileFormats.OpenXML


                oTable = rngMain.Tables(1)

                If (oTable.Columns.Count > 1) Then
                    If oTable.Rows(1).Cells(1).Width - oTable.Rows(1).Cells(2).Width > 10 Then
                        Me.SeparatorColumns = True
                    End If
                End If
                Me.PageRows = oTable.Rows.Count
                If (oTable.Rows.Count > 1) Then
                    If (oTable.Rows(1).Height - oTable.Rows(2).Height) > 10 Then
                        Me.SeparatorRows = True
                    End If
                End If
                Me.TableColumns = oTable.Columns.Count
                If Me.SeparatorColumns Then
                    Me.Columns = Math.Ceiling(oTable.Columns.Count / 2)
                Else
                    Me.Columns = oTable.Columns.Count
                End If

                If Me.SeparatorRows Then
                    Me.Rows = oTable.Rows.Count - Math.Ceiling(oTable.Rows.Count / 2)
                Else
                    Me.Rows = oTable.Rows.Count
                End If
                Me.LabelsPerPage = Me.Columns * Me.Rows
                WriteToTable(oTable)
                WriteLabels = True
            Catch ex As Exception
                Throw ex
            Finally
                'm_oStatusForm.prbStatus.Value = 100
                System.Windows.Forms.Application.DoEvents()

                m_oStatusForm.Hide()
                m_oStatusForm = Nothing

                GlobalMethods.CurWordApp.ActiveDocument.Range(0, 0).Select()

                GlobalMethods.CurWordApp.ScreenUpdating = True
                GlobalMethods.CurWordApp.ScreenRefresh()
            End Try
        End Function
        Private Sub WriteToTable(ByVal oTable As Word.Table)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Bates.WriteAcrossThenDown"
            Dim iCellsPerPage As Short
            Dim lNumCells As Integer
            Dim iTableColumns As Short
            Dim oTableRng As Word.Range
            Dim l As Integer
            Dim lCellsRemaining As Integer
            Dim i As Short
            Dim lTemp As Integer

            Try

                'GLOG : 5611 : ceh
                If Not Me.VerticalFill Then
                    'Round up to next integer
                    Me.Pages = Math.Ceiling((((Me.StartRow - 1) * Me.Columns) + Me.NumLabels) / Me.LabelsPerPage)
                Else
                    'Round up to next integer
                    Me.Pages = Math.Ceiling((((Me.StartColumn) * Me.Rows) + Me.NumLabels) / Me.LabelsPerPage)
                End If
                System.Windows.Forms.Application.DoEvents()
                'm_oStatusForm.prbStatus.Value = 10
                SetupSeqField(oTable)
                'fill in labels on the first page
                FillLabels(oTable)

                System.Windows.Forms.Application.DoEvents()
                'm_oStatusForm.prbStatus.Value = 25

                'copy the table
                If Me.Pages > 1 Then
                    'copy and paste first page accordingly
                    oTableRng = oTable.Range
                    oTableRng.Copy()
                    'clear out the first page cells that should be empty
                    EmptyStartingCells(oTable)
                End If


                System.Windows.Forms.Application.DoEvents()
                'm_oStatusForm.prbStatus.Value = 30


                Dim iStatus As Short
                Dim oRow As Word.Row

                'iStatus = m_oStatusForm.prbStatus.Value

                For l = 2 To Me.Pages

                    If l = 2 Then
                        'paste the table at the end of the first page
                        oTableRng.EndOf()

                        'add a blank row, and paste onto it -
                        'this is a workaround for the issue whereby
                        'the different styles in each cell  misapplied
                        'in the pasted table
                        oRow = oTableRng.Tables(1).Rows.Add
                        oRow.Select()
                        oTableRng.Paste()

                        'delete the added empty row
                        oTableRng = oTableRng.Tables(1).Range
                        oRow = oTableRng.Tables(1).Rows.Last
                        oRow.Delete()
                    Else
                        'add remaining pages
                        oTableRng.EndOf()
                        oTableRng.Paste()
                        oTableRng = oTableRng.Tables(1).Range
                    End If
                    System.Windows.Forms.Application.DoEvents()
                    'TODO: Rewrite form for vb.net
                    'm_oStatusForm.prbStatus.Value = m_oStatusForm.prbStatus.Value + ((95 - iStatus) / (m_lPages - 1))
                    If m_oStatusForm.JobCanceled Then
                        Exit For
                    End If
                Next l

                'GLOG : 5611 : ceh
                ResetSeqCodes(oTable)

                'update the fields and unlink
                oTable.Range.Fields.Update()
                oTable.Range.Fields.Unlink()

                If Not m_oStatusForm.JobCanceled Then
                    'clear out ending cells that should be empty
                    EmptyEndingCells(oTable)
                End If
                Exit Sub
            Catch ex As Exception
                Throw ex
                'GlobalMethods.RaiseError(mpThisFunction)
            End Try

        End Sub

        'GLOG : 5611 : ceh
        Private Sub ResetSeqCodes(oTable As Word.Table)
            Dim i As Short
            Dim r As Short
            Dim iNextNum As Integer
            Dim iStartCol As Integer
            Dim iStartRow As Integer
            Dim oFld As Word.Field
            Dim iPage As Short = 1
            Dim iRowCounter As Short = 1

            If m_iSeqIndex < 1 Then
                Exit Sub
            End If
            If Me.VerticalFill Then
                'Labels are vertical - need to set specific starting number for each SEQ field
                'since Word orders these left to right
                iNextNum = Me.StartNumber
                If Me.SeparatorColumns Then
                    iStartCol = Me.StartColumn + (Me.StartColumn - 1)
                Else
                    iStartCol = Me.StartColumn
                End If
                If Me.SeparatorRows Then
                    iStartRow = Me.StartRow + (Me.StartRow - 1)
                Else
                    iStartRow = Me.StartRow
                End If

                'do 1st row
                For iPage = 1 To Me.Pages
                    For i = 1 To oTable.Columns.Count
                        If (iPage > 1 Or i >= iStartCol) And (Not Me.SeparatorColumns Or (i Mod 2)) Then
                            For r = iRowCounter To (iRowCounter + Me.PageRows) - 1
                                If (iPage > 1 Or r >= iStartRow Or i > iStartCol) And (Not Me.SeparatorRows Or ((r - ((iPage - 1) * Me.PageRows)) Mod 2)) Then
                                    Try
                                        oFld = oTable.Rows(r).Cells(i).Range.Fields(m_iSeqIndex)
                                        'replace field text
                                        With oFld
                                            .Code.Text = .Code.Text & " \r" & CStr(iNextNum)
                                        End With
                                    Catch ex As Exception
                                    End Try
                                    iNextNum = iNextNum + 1
                                End If
                                If iNextNum >= Me.StartNumber + Me.NumLabels Then
                                    Return
                                End If
                            Next r
                        End If
                    Next i
                    iRowCounter = iRowCounter + Me.PageRows
                Next iPage
            Else
                'Labels are horizontal - only need to set starting number for first label
                For Each oFld In oTable.Range.Fields
                    If oFld.Type = WdFieldType.wdFieldSequence Then
                        oFld.Code.Text = oFld.Code.Text & " \r" & CStr(Me.StartNumber)
                        Exit For
                    End If
                Next oFld
            End If
        End Sub

        Private Sub EmptyStartingCells(ByVal oTable As Word.Table)
            Dim oRng As Word.Range
            Dim iCol As Short
            Dim iRow As Short
            Dim iStartCol As Integer
            Dim iStartRow As Integer
            Dim oCell As Word.Cell

            If Not (Me.StartColumn = 1 And Me.StartRow = 1) Then
                iStartCol = Me.StartColumn + IIf(Me.SeparatorColumns, Me.StartColumn - 1, 0)
                iStartRow = Me.StartRow + IIf(Me.SeparatorRows, Me.StartRow - 1, 0)
                'GLOG : 5611 : ceh
                If Me.VerticalFill Then
                    'Delete cell contents to left of starting column
                    For iCol = 1 To iStartCol - 1
                        If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                            For iRow = 1 To Me.PageRows
                                If Not Me.SeparatorRows Or (iRow Mod 2) Then
                                    oCell = oTable.Rows(iRow).Cells(iCol)
                                    oCell.Range.Delete()
                                End If
                            Next
                        End If
                    Next
                    'Delete unused cells at start of first column
                    For iRow = 1 To iStartRow - 1
                        If Not Me.SeparatorRows Or (iRow Mod 2) Then
                            oCell = oTable.Rows(iRow).Cells(iStartCol)
                            oCell.Range.Delete()
                        End If
                    Next
                Else
                    'Delete cell contents above starting row
                    For iRow = 1 To iStartRow - 1
                        If Not Me.SeparatorRows Or (iRow Mod 2) Then
                            For iCol = 1 To oTable.Columns.Count
                                If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                                    oCell = oTable.Rows(iRow).Cells(iCol)
                                    oCell.Range.Delete()
                                End If
                            Next
                        End If
                    Next
                    'Delete unused cells at start of first row
                    For iCol = 1 To iStartCol - 1
                        If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                            oCell = oTable.Rows(iStartRow).Cells(iCol)
                            oCell.Range.Delete()
                        End If
                    Next
                End If
            End If
        End Sub

        Private Sub EmptyEndingCells(ByVal oTable As Word.Table)
            Dim iCol As Short
            Dim iRow As Short
            Dim iCount As Integer = 0
            Dim oCell As Word.Cell
            Dim iUnusedCells As Integer
            Dim iTotalCells As Integer = Me.LabelsPerPage * Me.Pages


            If Me.VerticalFill Then
                iUnusedCells = iTotalCells - (Me.NumLabels + ((Me.StartColumn - 1) * Me.Rows) + (Me.StartRow - 1))
                For iCol = oTable.Columns.Count To 1 Step -1
                    If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                        For iRow = 1 To Me.PageRows Step 1
                            If Not Me.SeparatorRows Or (iRow Mod 2) Then
                                Dim iCurRow As Integer = oTable.Rows.Count - (iRow - 1)
                                oCell = oTable.Rows(iCurRow).Cells(iCol)
                                oCell.Range.Delete()
                                iCount = iCount + 1
                            End If
                            If iCount >= iUnusedCells Then
                                Return
                            End If
                        Next
                    End If
                Next
            Else
                iUnusedCells = iTotalCells - (Me.NumLabels + ((Me.StartRow - 1) * Me.Columns) + (Me.StartColumn - 1))
                For iRow = 1 To oTable.Rows.Count Step 1
                    If Not Me.SeparatorRows Or (iRow Mod 2) Then
                        Dim iCurRow As Integer = oTable.Rows.Count - (iRow - 1)
                        For iCol = oTable.Columns.Count To 1 Step -1
                            If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                                oCell = oTable.Rows(iCurRow).Cells(iCol)
                                oCell.Range.Delete()
                                iCount = iCount + 1
                            End If
                            If iCount >= iUnusedCells Then
                                Return
                            End If
                        Next
                    End If
                Next
            End If
        End Sub
        Private Sub SetupSeqField(oTable As Word.Table)
            Dim oFld As Word.Field
            Dim iFld As Short
            Dim xCode As String
            Dim xIdentifier As String
            Dim xNumSwitch = ""
            xIdentifier = Me.Description.Replace(" ", "")

            Select Case Me.NumberFormat.ToUpper()
                Case "ROMAN", "ALPHABETIC"
                    xNumSwitch = " \* " & Me.NumberFormat
                Case "ORDTEXT", "CARDTEXT"
                    xNumSwitch = " \* " & Me.NumberFormat & " \* Caps"
                Case Else
                    xNumSwitch = " \# """ & BaseMethods.CreatePadString(Me.Digits, "0") & """ \* " & Me.NumberFormat

            End Select
            xCode = "SEQ " & xIdentifier & xNumSwitch

            For iFld = 1 To oTable.Range.Fields.Count
                oFld = oTable.Range.Fields(iFld)
                'Cell may contain other fields besides SEQ
                If oFld.Type = WdFieldType.wdFieldSequence Then
                    m_iSeqIndex = iFld
                    oFld.Code.Text = xCode
                    Exit For
                End If
            Next
        End Sub
        Private Sub SetStartNumber(ByVal oTable As Word.Table)
            Dim oFld As Word.Field
            Dim xCode As String
        End Sub
        Private Sub FillLabels(ByVal oTable As Word.Table)
            Dim iCol As Short
            Dim iRow As Short
            Dim oCell As Word.Cell
            Dim bFirstCell As Boolean = True
            Dim iStartRow As Integer
            Dim iEndCol As Integer
            Dim iEndRow As Integer
            Dim iStartCol As Integer
            Dim iRowsUsed As Integer
            Dim iColsUsed As Integer
            Dim iCount As Integer

            If Not Me.VerticalFill Then
                If Me.Pages > 1 Then
                    iStartRow = 1
                    iEndRow = oTable.Rows.Count
                    iStartCol = 1
                Else
                    iStartRow = Me.StartRow + IIf(Me.SeparatorRows, Me.StartRow - 1, 0)
                    iRowsUsed = Math.Ceiling(Me.NumLabels / Me.Columns)
                    If ((Me.NumLabels - 1) Mod Me.Columns) + Me.StartColumn > Me.Columns Then
                        iRowsUsed = iRowsUsed + 1
                    End If
                    iEndRow = iStartRow + (iRowsUsed - 1)
                    If (iEndRow > iStartRow) And Me.SeparatorRows Then
                        iEndRow = iEndRow + 1
                    End If
                    iStartCol = Me.StartColumn + IIf(Me.SeparatorColumns, Me.StartColumn - 1, 0)
                End If
                oCell = oTable.Rows(1).Cells(1)
                oCell.Range.Cut()
                For iRow = iStartRow To iEndRow
                    If Not Me.SeparatorRows Or (iRow Mod 2) Then
                        For iCol = iStartCol To oTable.Columns.Count
                            'GLOG : 7601 : CEH
                            If Not Me.SeparatorColumns Or (iCol Mod 2) Then 'only do for odd columns
                                iCount = iCount + 1
                                oCell = oTable.Rows(iRow).Cells(iCol)
                                oCell.Range.Paste()
                            End If
                            If Me.Pages = 1 And iCount >= Me.NumLabels Then
                                Return
                            End If
                        Next iCol
                        iStartCol = 1
                    End If
                Next iRow
            Else
                If Me.Pages > 1 Then
                    iStartCol = 1
                    iEndCol = oTable.Columns.Count
                    iStartRow = 1
                Else
                    iStartCol = Me.StartColumn + IIf(Me.SeparatorColumns, Me.StartColumn - 1, 0)
                    iColsUsed = Math.Ceiling(Me.NumLabels / Me.Rows)
                    If ((Me.NumLabels - 1) Mod Me.Rows) + Me.StartRow > Me.Rows Then
                        iColsUsed = iColsUsed + 1
                    End If
                    iEndCol = iStartCol + (iColsUsed - 1)
                    If (iEndCol > iStartCol) And Me.SeparatorColumns Then
                        iEndCol = iEndCol + 1
                    End If
                    iStartRow = Me.StartRow + IIf(Me.SeparatorRows, Me.StartRow - 1, 0)
                End If
                oCell = oTable.Rows(1).Cells(1)
                oCell.Range.Cut()
                For iCol = iStartCol To iEndCol
                    If Not Me.SeparatorColumns Or (iCol Mod 2) Then
                        For iRow = iStartRow To oTable.Rows.Count
                            'GLOG : 7601 : CEH
                            If Not Me.SeparatorRows Or (iRow Mod 2) Then 'only do for odd columns
                                iCount = iCount + 1
                                oCell = oTable.Rows(iRow).Cells(iCol)
                                oCell.Range.Paste()
                            End If
                            iStartRow = 1
                            If Me.Pages = 1 And iCount >= Me.NumLabels Then
                                Return
                            End If
                        Next iRow
                    End If
                Next iCol
            End If
        End Sub
        Private Sub bGetDocRanges(Optional ByRef rngMain As Word.Range = Nothing, _
                               Optional ByRef rngPrimaryHeader As Word.Range = Nothing, _
                               Optional ByRef rngPrimaryFooter As Word.Range = Nothing, _
                               Optional ByRef rngFirstHeader As Word.Range = Nothing, _
                               Optional ByRef rngFirstFooter As Word.Range = Nothing, _
                               Optional ByRef vDoc As Object = Nothing, _
                               Optional ByRef vSection As Object = Nothing)
            'fills args with range values
            If IsNothing(vDoc) Then _
                  vDoc = GlobalMethods.CurWordApp.ActiveDocument

            If IsNothing(vSection) = True Then _
                    vSection = vDoc.Sections.First

            rngMain = vSection.Range

            With vSection
                rngPrimaryHeader = .Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                rngPrimaryFooter = .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                rngFirstHeader = .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                rngFirstFooter = .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
            End With
        End Sub
    End Class
End Namespace