Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Public Enum mpReinsertionLocations
    mpReinsertionLocation_Before = 0
    mpReinsertionLocation_After = 1
    mpReinsertionLocation_Current = 2
End Enum

Namespace LMP.Forte.MSWord
    Public Class DeleteScope
        Public Sub Delete(ByVal iScope As mpDeleteScopes, _
                          ByVal xVariableName As String, _
                          ByVal bIsBlock As Boolean, _
                          ByRef bIgnoreDeleteScope As Boolean, _
                          ByRef xVariablesDeleted As String, _
                          ByRef oTags As Tags, _
                          ByRef xPartsUpdated As String, _
                          Optional ByVal oTargetNode As Word.XMLNode = Nothing, _
                          Optional ByVal oTargetCC As Word.ContentControl = Nothing)
            'deletes oTargetNode and specified scope.  Steps:
            '1 - calls InsertmDels() to insert the new mDel and move any existing mDels in the
            'scope, preserving the sequence in the document
            '2 - updates parent mSEG if necessary (if there are multiple associated mVars in the
            'same segment part, this is done only once) - this involves getting the xml for the
            'scope, constructing the minimal string necessary for restoration, and adding it to the
            'mSEG's DeletedScopes attribute
            '3 - gets delimited list of any other variables in the scope, removes them from the tags
            'collection, and sets the byref xVariablesDeleted argument
            '4 - deletes the scope
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Delete"
            Dim oSegmentNode As Word.XMLNode = Nothing
            Dim oAttribute As Word.XMLNode = Nothing
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim rngScope As Word.Range = Nothing
            Dim xXML As String = ""
            Dim rngLocation As Word.Range = Nothing
            Dim xDeletedScopes As String = ""
            Dim i As Short = 0
            Dim oCell As Word.Cell
            Dim oCell2 As Word.Cell
            Dim oPreviousCell As Word.Cell
            Dim oNextCell As Word.Cell
            Dim lLoc As Integer = 0
            Dim sWidth As Single
            Dim sWidth2 As Single
            Dim oTag As Word.XMLNode = Nothing
            Dim oTagID As Word.XMLNode = Nothing
            Dim xWDStartTag As String = ""
            Dim rngSegment As Word.Range = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim xmDels As String = ""
            Dim xPart As String = ""
            Dim xVars As String = ""
            Dim bInTable As Boolean = False
            Dim bEndOfCell2003 As Boolean = False
            Dim rngTemp As Word.Range = Nothing
            Dim xPassword As String = ""
            Dim bEndOfCellAndSeg As Boolean = False
            Dim bAvoidMerger As Boolean = False
            Dim bContentControls As Boolean = False
            Dim rngTarget As Word.Range = Nothing
            Dim oSegmentCC As Word.ContentControl
            Dim oWordDoc As WordDoc
            Dim oDoc As Word.Document
            Dim xObjectDBID As String = ""
            Dim xObjectData As String = ""
            Dim xTag As String = ""
            Dim rngSegmant As Word.Range = Nothing
            Dim oCC As Word.ContentControl
            Dim oReserved As Word.XMLNode = Nothing
            Dim oConvert As Conversion
            Dim iLastIndex As Short = 0
            Dim xSegTag As String = ""
            Dim xEmbedded As String = ""
            Dim xSegmentTag As String = ""
            Dim oSegmentRng As Word.Range = Nothing
            Dim oParentSegmentBmk As Word.Bookmark = Nothing
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim iCount As Short = 0 'GLOG 7389 (dm

            On Error GoTo ProcError

            '10.2 (dm) -  added support for content controls
            bContentControls = Not oTargetCC Is Nothing
            If bContentControls Then
                rngLocation = oTargetCC.Range
            Else
                rngLocation = oTargetNode.Range
            End If

            oWordDoc = New WordDoc
            oDoc = rngLocation.Document
            bTrackChanges = oDoc.TrackRevisions 'GLOG 7389 (dm)

            With rngLocation
                'clear out node
                If Not bIsBlock Then
                    'get any mDels contained in oTargetNode
                    If bContentControls Then
                        xmDels = GetChildmDels_CC(oTargetCC, oTags)

                        'delete the doc vars for any existing sub vars
                        oWordDoc.DeleteAssociatedDocVars_CC(rngLocation, "mSubVar")
                    Else
                        xmDels = GetChildmDels(oTargetNode, oTags)
                        'delete the doc vars for any existing sub vars
                        oWordDoc.DeleteAssociatedDocVars(rngLocation, "mSubVar")
                    End If

                    'delete existing range if it's a block
                    If .Start <> .End Then
                        'GLOG 7389 (dm) - delete ccs/tags with track changes off
                        If bTrackChanges Then
                            oDoc.TrackRevisions = False
                            If bContentControls Then
                                iCount = .ContentControls.Count
                                For i = iCount To 1 Step -1
                                    .ContentControls(i).Delete()
                                Next i
                            Else
                                iCount = .XMLNodes.Count
                                For i = iCount To 1 Step -1
                                    .XMLNodes(i).Delete()
                                Next i
                            End If
                            oDoc.TrackRevisions = True
                        End If

                        .Text = ""

                        'if range is whole cell, the above line
                        'leaves the end of cell marker as part of range
                        If Len(.Text) Then
                            If Asc(Right(.Text, 1)) = 7 Then _
                                .Collapse(WdCollapseDirection.wdCollapseStart)
                        End If

                    End If

                    If bContentControls Then
                        'ensure that deleting the existing range hasn't
                        'resulted in placeholder text getting set
                        oTargetCC.SetPlaceholderText(, , "")
                    End If

                    'restore child mDels
                    If xmDels <> "" Then
                        If bContentControls Then
                            RestoreChildmDels_CC(oTargetCC, xmDels, oTags)
                        Else
                            RestoreChildmDels(oTargetNode, xmDels, oTags)
                        End If
                    End If
                End If

                'exit if tag is not in a table and the scope calls for one
                bInTable = .Information(Word.WdInformation.wdWithInTable)
                If (iScope > mpDeleteScopes.mpDeleteScope_Paragraph) And (Not bInTable) Then
                    bIgnoreDeleteScope = True
                    Exit Sub
                End If

                'expand range
                Select Case iScope
                    Case mpDeleteScopes.mpDeleteScope_Paragraph
                        'paragraph
                        .Expand(Word.WdUnits.wdParagraph)

                        'if last paragraph of tag is empty, expand to include it
                        If bContentControls Then
                            rngTarget = oTargetCC.Range
                        Else
                            rngTarget = oTargetNode.Range
                        End If
                        If .End <= rngTarget.End Then _
                            .MoveEnd(Word.WdUnits.wdParagraph)

                        If bInTable Then
                            'exit if there is only one paragraph in the tag's cell
                            oCell = .Cells(1)
                            'GLOG 4234 - replaced following conditional
                            '                    If (oCell.Range.Start = .Start) And (oCell.Range.End < .End + 2) Then
                            If oCell.Range.Paragraphs.Count = 1 Then
                                bIgnoreDeleteScope = True
                                Exit Sub
                            End If
                        Else
                            'GLOG 2115 (dm) - avoid causing the merger of two tables
                            On Error Resume Next
                            bAvoidMerger = (.Previous(Word.WdUnits.wdCharacter).Information(Word.WdInformation.wdWithInTable) And _
                                .Next(Word.WdUnits.wdCharacter).Information(Word.WdInformation.wdWithInTable))
                            On Error GoTo ProcError
                            If bAvoidMerger Then
                                bIgnoreDeleteScope = True
                                Exit Sub
                            End If

                            'GLOG 7369 (dm) - if range start outside and ends inside a table,
                            'errors will occur when attempting to add the mDel and delete the range -
                            'this may arise when an open xml delete scope is restored in a .doc
                            If Not .Characters.First.Information(Word.WdInformation.wdWithInTable) Then
                                While .Characters.Last.Information(Word.WdInformation.wdWithInTable)
                                    .MoveEnd()
                                End While
                            End If
                        End If

                        rngScope = rngLocation
                    Case mpDeleteScopes.mpDeleteScope_Cell
                        'cell - merge with previous cell
                        oCell = .Cells(1)
                        oPreviousCell = oCell.Previous

                        'validate
                        bIgnoreDeleteScope = True
                        If oPreviousCell Is Nothing Then
                            'exit if no previous cell
                            Exit Sub
                        ElseIf oPreviousCell.RowIndex <> oCell.RowIndex Then
                            'exit if previous cell is not in same row
                            Exit Sub
                        End If
                        bIgnoreDeleteScope = False

                        'set deletion range
                        rngScope = oCell.Range.Duplicate
                    Case mpDeleteScopes.mpDeleteScope_CellAndPreviousCell
                        'cell and previous cell - merge both cells with
                        'cell to left of previous cell
                        oCell2 = .Cells(1)
                        oCell = oCell2.Previous

                        'validate
                        bIgnoreDeleteScope = True
                        If oCell Is Nothing Then
                            'exit if no previous cell
                            Exit Sub
                        ElseIf oCell.RowIndex <> oCell2.RowIndex Then
                            'exit if previous cell is not in same row
                            Exit Sub
                        End If

                        'exit if no cell to left of previous cell
                        oPreviousCell = oCell.Previous
                        If oPreviousCell Is Nothing Then
                            Exit Sub
                        ElseIf oPreviousCell.RowIndex <> oCell.RowIndex Then
                            Exit Sub
                        End If
                        bIgnoreDeleteScope = False

                        'set deletion range
                        rngScope = oCell.Range.Duplicate
                        rngScope.MoveEnd(WdUnits.wdCell)
                    Case mpDeleteScopes.mpDeleteScope_Row
                        'row
                        rngScope = .Rows(1).Range
                    Case mpDeleteScopes.mpDeleteScope_Table
                        'table
                        rngScope = .Tables(1).Range
                    Case mpDeleteScopes.mpDeleteScope_WithPrecedingSpace
                        'delete preceding space
                        With .Previous(Word.WdUnits.wdCharacter, 2).Characters.First
                            If .Text = Chr(32) Then _
                                .Delete()
                        End With

                        'tag will not get deleted with this scope,
                        'so just delete text and exit
                        .Text = ""
                        Exit Sub
                    Case mpDeleteScopes.mpDeleteScope_WithTrailingSpace
                        'delete trailing space
                        With .Next(Word.WdUnits.wdCharacter)
                            If .Text = Chr(32) Then _
                                .Delete()
                        End With

                        'tag will not get deleted with this scope,
                        'so just delete text and exit
                        .Text = ""
                        Exit Sub
                End Select
            End With

            'for scopes where tag will get deleted, store xml for scope in container mSEG
            If bContentControls Then
                'content controls
                oSegmentCC = oWordDoc.GetParentSegmentContentControl(oTargetCC.Range)

                If Not oSegmentCC Is Nothing Then
                    xSegmentTag = oSegmentCC.Tag
                    oSegmentRng = oSegmentCC.Range
                Else
                    oParentSegmentBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oTargetCC.Range)
                    xSegmentTag = Mid$(oParentSegmentBmk.Name, 2)
                    oSegmentRng = oParentSegmentBmk.Range
                End If

                'GLOG 7120 (dm) - restore old conditional for blocks - IncludeExcludeBlock needs
                'to work when mSEG and mBlock are co-extensive, e.g. for TOA
                If (Not oSegmentCC Is Nothing) Or bIsBlock Then
                    'exit if scope includes mSEG
                    If (rngScope.Start < oSegmentRng.Start) And _
                        (rngScope.End > oSegmentRng.End) Then
                        bIgnoreDeleteScope = True
                        Exit Sub
                    End If
                Else
                    'GLOG 7020 (dm) - exit if scope includes mSEG - different code is
                    'required for bookmark mSEGs because start/end of paragraph and
                    'start/end of mSEG may now be the same
                    'GLOG 7871 (dm) - the fix for GLOG 7020 prevented the external
                    'paragraph text from getting deleted in a single segment mSEG -
                    'allow when the ranges are co-extensive
                    If ((rngScope.Start <= oSegmentRng.Start) And _
                        (rngScope.End > oSegmentRng.End)) Or _
                        ((rngScope.Start < oSegmentRng.Start) And _
                        (rngScope.End >= oSegmentRng.End)) Then
                        bIgnoreDeleteScope = True
                        Exit Sub
                    End If
                End If

                'get part number
                xPart = BaseMethods.GetAttributeValue(xSegmentTag, "PartNumber", oDoc)

                'get variable name if not supplied
                xTag = oTargetCC.Tag
                If xVariableName = "" Then _
                    xVariableName = BaseMethods.GetAttributeValue(xTag, "TagID", oDoc)

                'get object database id
                xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)
                If bIsBlock Then
                    xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                        mpBlockProperties.mpBlockProperty_ObjectDatabaseID, "mBlock")
                Else
                    xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                        mpVariableProperties.mpVariableProperty_ObjectDatabaseID, "mVar", xVariableName)
                End If
            Else
                'xml tags
                With oTargetNode
                    'get immediately containing mSEG
                    oSegmentNode = .SelectSingleNode("ancestor::x:mSEG[1]", _
                        "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                    If Not oSegmentNode Is Nothing Then
                        xSegmentTag = BaseMethods.GetTag(oSegmentNode)
                        oSegmentRng = oSegmentNode.Range
                    Else
                        oParentSegmentBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oTargetNode.Range)
                        xSegmentTag = Mid$(oParentSegmentBmk.Name, 2)
                        oSegmentRng = oParentSegmentBmk.Range
                    End If

                    'GLOG 7120 (dm) - restore old conditional for blocks - IncludeExcludeBlock needs
                    'to work when mSEG and mBlock are co-extensive, e.g. for TOA
                    If (Not oSegmentNode Is Nothing) Or bIsBlock Then
                        'exit if scope includes mSEG
                        If (rngScope.Start < oSegmentRng.Start) And _
                            (rngScope.End > oSegmentRng.End) Then
                            bIgnoreDeleteScope = True
                            Exit Sub
                        End If
                    Else
                        'GLOG 7020 (dm) - exit if scope includes mSEG - different code is
                        'required for bookmark mSEGs because start/end of paragraph and
                        'start/end of mSEG may now be the same
                        If (rngScope.Start <= oSegmentRng.Start) And _
                            (rngScope.End >= oSegmentRng.End) Then
                            bIgnoreDeleteScope = True
                            Exit Sub
                        End If
                    End If

                    'get part number
                    xPart = BaseMethods.GetAttributeValue(xSegmentTag, "PartNumber", oDoc)

                    'get variable name if not supplied
                    If xVariableName = "" Then _
                        xVariableName = .SelectSingleNode("@TagID").NodeValue

                    '10.2: Get tag from Reserved Attribute
                    xTag = BaseMethods.GetTag(oTargetNode)

                    'decompress object data before getting xml
                    oAttribute = .SelectSingleNode("@ObjectData")
                    xObjectData = BaseMethods.Decrypt(oAttribute.NodeValue, xPassword)
                    oAttribute.NodeValue = xObjectData
                    '10.2: Get ObjectDatabaseID used for Doc Variables
                    If bIsBlock Then
                        xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                            mpBlockProperties.mpBlockProperty_ObjectDatabaseID, "mBlock")
                    Else
                        xObjectDBID = BaseMethods.GetObjectDataValue(xObjectData, _
                            mpVariableProperties.mpVariableProperty_ObjectDatabaseID, "mVar", xVariableName)
                    End If
                End With
            End If

            Dim oScopeRng As Word.Range = Nothing

            oScopeRng = rngScope.Duplicate

            'GLOG 7389 (dm)
            If bTrackChanges Then _
                oDoc.TrackRevisions = False

            'insert new mDel and move any existing ones
            If bContentControls Then
                iReinsertionLocation = InsertmDels_CC(oScopeRng, oSegmentRng, xSegmentTag, _
                   oTargetCC, xVariableName, xObjectDBID, iScope, oTags)
            Else
                '10.2 - Object database ID also used in Reserved attribute of XML Node
                iReinsertionLocation = InsertmDels(oScopeRng, oSegmentRng, xSegmentTag, oTargetNode, _
                    xVariableName, xObjectDBID, iScope, oTags)
            End If

            'GLOG 7389 (dm)
            If bTrackChanges Then _
                oDoc.TrackRevisions = True

            'adjust for end of cell or end of segment
            If (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) And _
                    (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After) Then
                'get segment range
                If bContentControls Then
                    rngSegment = oSegmentRng
                ElseIf Not oSegmentNode Is Nothing Then 'GLOG 7657 (dm)
                    rngSegment = oSegmentNode.Range
                Else
                    'GLOG 7657 (dm) - add support for bookmark mSEGs
                    rngSegment = oParentSegmentBmk.Range
                End If

                If Right(rngScope.Text, 1) = Chr(7) Then
                    'end of cell
                    bEndOfCell2003 = (GlobalMethods.CurWordApp.Version = "11.0")
                    bEndOfCellAndSeg = (rngScope.Cells(1).Range.End > rngSegment.End)
                    rngScope.MoveEnd(Word.WdUnits.wdCharacter, -1)
                Else
                    'end of mSEG
                    rngScope.MoveEnd(Word.WdUnits.wdCharacter, -(rngScope.End - rngSegment.End))
                End If
            End If

            'remove mVars in scope from tags collection,
            'getting back list of variables that will be deleted
            GetVariablesInScope(rngScope, xVariableName, True, bContentControls, _
                oTags, xVariablesDeleted)

            With rngScope
                'if this is the last paragraph of the mSEG, add a paragraph mark before
                'the mSEG end tag - this will allow us to get the paragraph formatting
                'without including the entire mSEG in the reinsertion xml
                If (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) And _
                        (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After) Then
                    If bEndOfCellAndSeg Then
                        '2/21/09 (dm) - this will allow us to insert a paragraph mark at the
                        'end of a table segment without getting an error and also ensure the
                        'integrity of the delete scope
                        If bContentControls Then
                            .SetRange(.Start, oSegmentRng.End)
                        Else
                            .SetRange(.Start, oSegmentNode.Range.End)
                        End If
                    End If
                    .InsertAfter(vbCr)
                End If

                'get xml for scope
                If InStr(xPartsUpdated, "|" & xPart & "|") = 0 Then
                    'get xml
                    If bContentControls Then
                        'get open xml
                        xXML = .WordOpenXML
                    Else
                        '12/10/10 (dm) - add bookmarks before getting xml
                        oConvert = New Conversion
                        For Each oTag In .XMLNodes
                            oReserved = oTag.SelectSingleNode("@Reserved")
                            If Not oReserved Is Nothing Then
                                oTag.Range.Bookmarks.Add("_" & oReserved.NodeValue)
                                iLastIndex = 0
                                oConvert.IndexNestedAdjacentChildren(oTag, iLastIndex)
                                If iLastIndex > 0 Then
                                    iLastIndex = iLastIndex + 1
                                    BaseMethods.SetAttributeValue(oReserved.NodeValue, _
                                        "TempID", CStr(iLastIndex), "")
                                End If
                            End If
                        Next oTag

                        'get straight xml
                        xXML = .XML
                    End If

                    'GLOG 5091 (dm) - for some reason that I've been unable to determine,
                    'an extra paragraph is winding up at the start of the scope for the
                    'TOA block - there should never be a paragraph before the mBlock
                    If bContentControls And bIsBlock Then
                        'look for a paragraph before the first content control
                        '9/16/11 (dm) - added condition that the extra paragraph ends
                        'before the start of the mBlock - we were stripping the entire body
                        'when there were attributes on the w:p node containing the content
                        'control - this was an issue with Fulbright's converted letterhead
                        'on some machines only
                        Dim lPos3 As Integer = 0
                        lPos = InStr(xXML, "<w:body>")
                        lPos2 = InStr(lPos, xXML, "<w:p ")
                        lPos3 = InStr(lPos, xXML, "<w:sdt>")
                        If lPos2 > 0 And lPos2 < lPos3 Then
                            'one exists - remove from xml
                            lPos = InStr(lPos2, xXML, "</w:p>")
                            If lPos < lPos3 Then
                                xXML = Left$(xXML, lPos2 - 1) & Mid$(xXML, lPos + 6)
                            End If
                        End If
                    End If

                    '2/21/09 (dm) -  this is the only way that mVar will be deleted in the end
                    'of table segment scenario - it would probably be fine in all cases,
                    'but there's no reason to risk it
                    If bEndOfCellAndSeg Then
                        If bContentControls Then
                            oTargetCC.Delete()
                        Else
                            oTargetNode.Delete()
                        End If
                    End If

                    'get wordDocument start tag - we need to include namespaces
                    If BaseMethods.IsWordOpenXML(xXML) Then
                        lPos = InStr(xXML, "<w:document ")
                    Else
                        lPos = InStr(xXML, "<w:wordDocument ")
                    End If
                    lPos2 = InStr(lPos, xXML, ">") + 1
                    xWDStartTag = Mid$(xXML, lPos, lPos2 - lPos)

                    'get body node, excluding section info
                    '<wx:sect> tag may also exist in Word 11, and can be discarded
                    If InStr(xXML, "<wx:sect>") = 0 Then
                        lPos = InStr(xXML, "<w:body>") + 8
                    Else
                        lPos = InStr(xXML, "<w:body>") + 17
                    End If
                    lPos2 = InStr(lPos, xXML, "<w:sectPr>")
                    If lPos2 = 0 Then
                        'the sectPr tag sometimes takes an alternate form -
                        '<w:sectPr wsp:rsidR="00000000" ..." - so test again
                        lPos2 = InStr(lPos, xXML, "<w:sectPr ")
                    End If

                    '7/18/11 (dm) - get embedded content
                    If BaseMethods.IsWordOpenXML(xXML) Then _
                        xEmbedded = GetEmbeddedContent(xXML)

                    'concatenate
                    xXML = xWDStartTag & xEmbedded & "<w:body>" & _
                        Mid$(xXML, lPos, lPos2 - lPos) & "</w:body>"

                    'modify xml to ensure correct cell width - if the columns of a table
                    'are irregular, the xml for a single cell will indicate only the width
                    'of the entire table
                    If (iScope = mpDeleteScopes.mpDeleteScope_Cell) Or _
                            iScope = (mpDeleteScopes.mpDeleteScope_CellAndPreviousCell) Then
                        sWidth = oCell.Width
                        If Not oCell2 Is Nothing Then _
                            sWidth2 = oCell2.Width
                        SetCellWidths(xXML, sWidth, sWidth2)
                    End If

                    'NodeStore class puts object data in an xml attribute,
                    'so the opening bracket need to be replaced
                    xXML = Replace(xXML, "<", "�")
                    xXML = "DeletedTag=" & xVariableName & "�" & xXML & "|"

                    If bContentControls Then
                        'update mSEG
                        xDeletedScopes = BaseMethods.GetAttributeValue(xSegmentTag, _
                            "DeletedScopes", oDoc)
                        DeleteDefinition(xDeletedScopes, xVariableName)
                        BaseMethods.SetAttributeValue(xSegmentTag, "DeletedScopes", _
                            xDeletedScopes & xXML, "", oDoc)

                        'update Tag in tags collection
                        If Not oSegmentCC Is Nothing Then
                            oTags.Update_CC(oSegmentCC)
                        Else
                            oTags.Update_Bmk(oParentSegmentBmk)
                        End If
                    Else
                        'update mSEG
                        xDeletedScopes = BaseMethods.GetAttributeValue(xSegmentTag, "DeletedScopes", oDoc)
                        DeleteDefinition(xDeletedScopes, xVariableName)
                        BaseMethods.SetAttributeValue(xSegmentTag, "DeletedScopes", xDeletedScopes & xXML, "", oDoc)

                        'update Tag in tags collection
                        If Not oSegmentNode Is Nothing Then
                            'GLOG 7536 (dm) - the code to update the xml node attribute was removed at some point -
                            'this caused errors on design preview and design save
                            oAttribute = oSegmentNode.SelectSingleNode("@DeletedScopes")
                            If oAttribute Is Nothing Then _
                                    oAttribute = oSegmentNode.Attributes.Add("DeletedScopes", "")
                            oAttribute.NodeValue = BaseMethods.Encrypt(xDeletedScopes & xXML, xPassword)

                            oTags.Update(oSegmentNode)
                        Else
                            oTags.Update_Bmk(oParentSegmentBmk)
                        End If
                    End If

                    'update record of parts updated
                    xPartsUpdated = xPartsUpdated & xPart & "|"
                Else
                    '10.2: Do for XML Tags as well
                    'delete associated doc var - we only need the one associated with
                    'the content control that's saved to the xml
                    oWordDoc.DeleteRelatedObjects(xTag, oDoc)
                End If

                'delete added paragraph - removed 7/14/08 for GLOG item 2590 - this line
                'should be unnecessary because we're deleting the entire range below
                '        If (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) And _
                '                (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After) Then
                '            .Characters.Last.Delete
                '        End If
            End With

            With rngScope
                'delete scope
                Select Case iScope
                    Case mpDeleteScopes.mpDeleteScope_Paragraph
                        'in Word 2003, empty tags at end of cell won't get deleted
                        'unless there's text before them
                        If bEndOfCell2003 Then _
                            .InsertBefore("x")

                        'GLOG 7389 (dm) - delete ccs/tags with track changes off
                        If bTrackChanges Then
                            .Document.TrackRevisions = False
                            If bContentControls Then
                                iCount = .ContentControls.Count
                                For i = iCount To 1 Step -1
                                    .ContentControls(i).Delete()
                                Next i
                            Else
                                iCount = .XMLNodes.Count
                                For i = iCount To 1 Step -1
                                    .XMLNodes(i).Delete()
                                Next i
                            End If
                            .Document.TrackRevisions = True
                        End If
                        Dim bNextCharInTable As Boolean = False
                        'GLOG 8679: Test for next character before attempting to get information
                        On Error Resume Next
                        Dim oNextChar As Word.Range
                        oNextChar = .Next(Word.WdUnits.wdCharacter)
                        If Not oNextChar Is Nothing Then
                            bNextCharInTable = oNextChar.Information(Word.WdInformation.wdWithInTable)
                        End If
                        On Error GoTo ProcError
                        'GLOG 6337 (dm) - in Word 2010 only, the Delete method doesn't delete
                        'a block tagged paragraph in one shot when immediately above a table
                        If (Val(GlobalMethods.CurWordApp.Version) > 12) And (Not bInTable) And _
                                (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before) And _
                                bNextCharInTable Then
                            .Text = ""
                        Else
                            .Delete()
                        End If

                        'last paragraph of mSEG won't get deleted automatically
                        If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                            If bEndOfCell2003 Then
                                'in Word 2003, deleting the last paragraph mark in a cell causes
                                'paragraph to pick up formatting stored in end of cell marker -
                                'insert a temporary paragraph to hold formatting
                                rngTemp = .Duplicate
                                rngTemp.Move(Word.WdUnits.wdParagraph, -1)
                                rngTemp.StartOf()
                                rngTemp.InsertBefore(vbCr)
                            End If

                            .StartOf(Word.WdUnits.wdParagraph)
                            .Previous(Word.WdUnits.wdCharacter).Delete()

                            If bEndOfCell2003 Then
                                'restore formatting and delete temporary paragraph
                                .ParagraphFormat = rngTemp.ParagraphFormat
                                rngTemp.Delete()
                            End If
                        End If
                    Case mpDeleteScopes.mpDeleteScope_Cell, mpDeleteScopes.mpDeleteScope_CellAndPreviousCell
                        'increase width of previous cell by width of cells that
                        'will be deleted - we don't do an actual merge to avoid
                        'having to deal with preserving formatting
                        oPreviousCell.Width = oPreviousCell.Width + sWidth + sWidth2

                        'delete the cells
                        ' WORD BUG: Need to delete 2nd cell first, otherwise
                        ' it sometimes ends up pointing to the end-of-row marker
                        If Not oCell2 Is Nothing Then _
                            oCell2.Delete()
                        oCell.Delete()

                        'the mDel should be inserted at the end of the previous cell
                        lLoc = oPreviousCell.Range.End - 1
                        .SetRange(lLoc, lLoc)

                        'update the Tags associated with other tags in the row - the
                        'WordXMLNode reference is lost when cells are deleted -
                        'TODO: assign temp ids to other tags in row before
                        'deleting cells to ensure that the right Tag is updated
                        'when its one of multiple associated tags
                        If Not bContentControls Then
                            For Each oTag In .Rows(1).Range.XMLNodes
                                oTagID = oTag.SelectSingleNode("@TagID")
                                If Not oTagID Is Nothing Then _
                                    oTags.Update(oTag)
                                oTagID = Nothing
                            Next oTag
                        Else
                            'not sure whether this is necessary for content controls
                            For Each oCC In .Rows(1).Range.ContentControls
                                '8/3/11 (dm) - explicitly exclude sub vars - the
                                'existing condition wasn't sufficient because
                                'GetAttributeValue treats TagID and Name synonymously
                                If BaseMethods.GetBaseName(oCC) <> "mSubVar" Then
                                    If BaseMethods.GetAttributeValue(oCC.Tag, "TagID", oDoc) <> "" Then _
                                        oTags.Update_CC(oCC)
                                End If
                            Next oCC
                        End If
                    Case mpDeleteScopes.mpDeleteScope_Row
                        .Rows(1).Delete()
                    Case mpDeleteScopes.mpDeleteScope_Table
                        .Tables(1).Delete()
                End Select
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub SetCellWidths(ByRef xXML As String, _
                                  ByVal sWidth1 As Single, _
                                  ByVal sWidth2 As Single)
            'modifies xXML to ensure correct cell widths - if the columns of a table
            'are irregular, the xml for a single cell will indicate only the width
            'of the entire table
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.SetCellWidths"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0

            On Error GoTo ProcError

            'specify to ignore table width
            lPos = InStr(xXML, "<w:tblW ")
            lPos = InStr(lPos, xXML, "w:w=") + 5
            lPos2 = InStr(lPos + 1, xXML, Chr(34))
            xXML = Left$(xXML, lPos - 1) & "0" & Mid$(xXML, lPos2)
            lPos = InStr(lPos, xXML, "w:type=") + 8
            lPos2 = InStr(lPos + 1, xXML, Chr(34))
            xXML = Left$(xXML, lPos - 1) & "auto" & Mid$(xXML, lPos2)

            'adjust width of first cell to specified width (in twips)
            lPos = InStr(lPos, xXML, "<w:gridCol ")
            lPos = InStr(lPos, xXML, "w:w=") + 5
            lPos2 = InStr(lPos + 1, xXML, Chr(34))
            xXML = Left$(xXML, lPos - 1) & (sWidth1 * 20) & Mid$(xXML, lPos2)

            'adjust width of second cell to specified width (in twips)
            If sWidth2 <> 0 Then
                lPos = InStr(lPos, xXML, "<w:gridCol ")
                lPos = InStr(lPos, xXML, "w:w=") + 5
                lPos2 = InStr(lPos + 1, xXML, Chr(34))
                xXML = Left$(xXML, lPos - 1) & (sWidth2 * 20) & Mid$(xXML, lPos2)
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub Restore(ByVal oSegmentBoundingObject As Object, _
                           ByVal xVariableName As String, _
                           ByVal bAsBlock As Boolean, _
                           ByVal iScope As mpDeleteScopes, _
                           ByVal xSegmentXML As String, _
                           ByVal oLocation As Word.Range, _
                           ByRef xVariablesAdded As String, _
                           ByRef oTags As Tags, _
                           ByRef rngmDels() As Word.Range, _
                           ByRef oInsertedNodes() As Word.XMLNode)
            'reinserts deleted scope for xVariableName in oSegmentNode.  Steps:
            '1 - gets insertion xml from the mSEG's DeletedScopes attribute
            '2 - cycles through mSEG's child mDels, calling RestoreScope() for each
            '3 - removes the insertion xml from the mSEG's DeletedScopes attribute
            '4 - returns an array of restored mVars and an array of ranges where mDels were
            'replaced, so that any adjacent mDels can be moved to preserve the sequence
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore"
            Dim xXML As String = ""
            Dim xDeletedScopes As String = ""
            Dim oDelNode As Word.XMLNode = Nothing
            Dim oDelNodes() As Word.XMLNode = Nothing
            Dim xPrefixMapping As String = ""
            Dim bLocationSupplied As Boolean = False
            Dim oInsertedNode As Word.XMLNode = Nothing
            Dim i As Short = 0
            Dim iCount As Short = 0
            Dim oWordDoc As WordDoc
            Dim xPassword As String = ""
            Dim xText As String = ""
            Dim oDoc As Word.Document
            Dim xTag As String = ""
            Dim xBodyXML As String = ""
            Dim xRefBkmk As String = ""
            Dim oSegmentNode As Word.XMLNode = Nothing
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim xBoundingObjectTag As String = ""
            Dim xSegmentID As String = ""
            Dim xPart As String = ""
            Dim xXPath As String = ""

            On Error GoTo ProcError

            oWordDoc = New WordDoc

            On Error Resume Next
            oSegmentNode = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
                oDoc = oSegmentBmk.Range.Document
                xSegmentID = oWordDoc.GetFullTagID_Bookmark(oSegmentBmk)
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentNode.Range
                xBoundingObjectTag = BaseMethods.GetTag(oSegmentNode)
                oDoc = oSegmentRng.Document
                xSegmentID = oWordDoc.GetFullTagID(oSegmentNode)
            End If

            'store clipboard contents
            Clipboard.SaveClipboard()

            If xVariableName = "" Then
                'raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>")
            End If

            'TODO: implement oLocation parameter - the idea is that if the mDel
            'is deleted outside of the firm, and we have the xml for the scope,
            'we can prompt the user to select the insertion location
            bLocationSupplied = (Not oLocation Is Nothing)

            'get insertion xml
            xXML = GetInsertionXML(xBoundingObjectTag, oSegmentRng, xVariableName, xSegmentXML, xBodyXML)
            If xXML = "" Then
                'no xml - raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>" & xVariableName)
            End If

            'get mDels
            xPart = BaseMethods.GetAttributeValue(xBoundingObjectTag, "PartNumber", oDoc)
            xXPath = "//mSEG[@TagID='" & xSegmentID & "' and @Part='" & xPart & "']" & _
                "/mDel[@TagID='" & xSegmentID & "." & xVariableName & "']"
            oDelNodes = oTags.GetWordXMLNodes(xXPath)

            If (oDelNodes(0) Is Nothing) And Not bLocationSupplied Then
                'no location - raise error
                Err.Raise(mpErrors.mpError_InsertionLocationRequired, , _
                    "<Error_DeleteScopeLocationRequired>" & xVariableName)
            End If

            iCount = UBound(oDelNodes) + 1
            ReDim rngmDels(iCount - 1)
            ReDim oInsertedNodes(iCount - 1)

            For i = 0 To iCount - 1
                'get mDel and clear out text
                oDelNode = oDelNodes(i)

                'GLOG 3307 (Doug)
                'rngmDel.Text = ""
                oWordDoc.RemoveTextFrommDel(oDelNode)

                'add mDel location to byref array
                rngmDels(i) = oDelNode.Range

                'restore scope
                oInsertedNode = RestoreScope(oDelNode, oSegmentBoundingObject, xVariableName, _
                    bAsBlock, iScope, xXML, xVariablesAdded, oTags, xBodyXML, xRefBkmk)

                'add restored mVar to byref array
                oInsertedNodes(i) = oInsertedNode
            Next i

            If Not oSegmentNode Is Nothing Then
                'remove delete scope from mSeg
                xDeletedScopes = BaseMethods.Decrypt(oSegmentNode _
                    .SelectSingleNode("@DeletedScopes").NodeValue, xPassword)
                DeleteDefinition(xDeletedScopes, xVariableName)
                oSegmentNode.SelectSingleNode("@DeletedScopes").NodeValue = _
                    BaseMethods.Encrypt(xDeletedScopes, xPassword)
                '10.2: Update DeleteScope in mSEG Doc Variable
                xTag = BaseMethods.GetTag(oSegmentNode)
                If xTag <> "" Then _
                    BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedScopes, "", oDoc)
                'update Tag in tags collection
                oTags.Update(oSegmentNode)

            Else
                'remove delete scope from mSeg
                xDeletedScopes = BaseMethods.GetAttributeValue(xBoundingObjectTag, "DeletedScopes", oDoc)
                DeleteDefinition(xDeletedScopes, xVariableName)
                BaseMethods.SetAttributeValue(xBoundingObjectTag, "DeletedScopes", xDeletedScopes, "", oDoc)
                oTags.Update_Bmk(oSegmentBmk)
            End If

            'remove unavailable schema references
            oWordDoc.RemoveUnavailableXMLSchema(oSegmentRng.Document)

            'restore clipboard contents
            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub Restore_Row(oRng As Word.Range, xXML As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_Row"
            Dim lRowIndex As Integer = 0
            Dim oTable As Word.Table
            Dim lLoc As Integer = 0
            Dim bDeleteExtraPara As Boolean = False
            Dim oRowRange As Word.Range = Nothing
            Dim oBmk As Word.Bookmark = Nothing
            Dim xBookmarks(0, 2) As String
            Dim i As Short = 0
            Dim iCount As Short = 0
            Dim lStart As Integer = 0
            Dim oBmkRange As Word.Range = Nothing
            Dim iFileFormat As mpFileFormats
            Dim lCells As Short = 0
            Dim bShowHidden As Boolean = False

            With oRng
                oTable = .Tables(1)

                'get current row
                lRowIndex = .Cells(1).RowIndex

                If xXML <> "" Then
                    'JTS 3/28/11: Make sure hidden bookmarks are included in collection
                    bShowHidden = oRng.Bookmarks.ShowHidden
                    oRng.Bookmarks.ShowHidden = True
                    'insert xml at end of table
                    .EndOf(Word.WdUnits.wdTable)
                    .Move(Word.WdUnits.wdCharacter, 2)

                    'InsertXML with open xml will force an empty paragraph after the table -
                    'this is in addition to the trailing empty paragraph that will always
                    'be in the xml for a table or row - if open xml, check whether we're
                    'inserting into an empty paragraph
                    If BaseMethods.IsWordOpenXml(xXML) Then
                        bDeleteExtraPara = (.Paragraphs(1).Range.Text <> vbCr)
                    End If

                    'insert xml
                    .InsertXML(xXML)

                    'delete trailing para
                    oTable.Range.Next(Word.WdUnits.wdCharacter).Delete()

                    'delete extra para if necessary
                    If bDeleteExtraPara And (oTable.Range.Next(Word.WdUnits.wdCharacter).Text = vbCr) Then
                        oTable.Range.Next(Word.WdUnits.wdCharacter).Delete()
                    End If

                    '1/31/11 (dm) - preserve the name and relative position of bookmarks -
                    'these may be lost or mispositioned after copy/paste
                    iFileFormat = BaseMethods.FileFormat(.Document)

                    'GLOG 6145 (dm) - we can't assume that the new row is the last row of
                    'the original table - if the segment ends inside oTable, the new row may
                    'get inserted as a separate table
                    If .Tables.Count = 1 Then
                        oRowRange = .Tables(1).Rows.Last.Range
                    Else
                        oRowRange = oTable.Rows.Last.Range
                    End If

                    lCells = oRowRange.Cells.Count
                    'GLOG 5425/5426 (dm) - limit to single cell rows
                    If (iFileFormat = mpFileFormats.Binary) And (lCells = 1) Then
                        lStart = oRowRange.Start
                        iCount = oRowRange.Bookmarks.Count
                        ReDim xBookmarks(iCount - 1, 2)
                        For i = 0 To iCount - 1
                            oBmk = oRowRange.Bookmarks(i + 1)
                            If (oBmk.Start >= lStart) And (oBmk.End <= oRowRange.End) Then
                                xBookmarks(i, 0) = oBmk.Name
                                If BaseMethods.CellIsEmpty(oRowRange.Cells(1)) Then
                                    'if row is empty, bookmark may move to end of cell marker
                                    xBookmarks(i, 1) = "0"
                                    xBookmarks(i, 2) = "0"
                                Else
                                    xBookmarks(i, 1) = CStr(oBmk.Start - lStart)
                                    xBookmarks(i, 2) = CStr(oBmk.End - lStart)
                                End If
                            End If
                        Next i
                    End If

                    'cut from end of table
                    oRowRange.Cut()
                End If

                'paste in correct location
                lLoc = oTable.Rows(lRowIndex).Range.Start
                .SetRange(lLoc, lLoc)
                .Paste()

                '1/31/11 (dm) - restore bookmarks
                If (iFileFormat = mpFileFormats.Binary) And (lCells = 1) Then
                    For i = 0 To iCount - 1
                        If xBookmarks(i, 0) <> "" Then 'GLOG 5425/5426 (dm)
                            oBmkRange = .Duplicate
                            oBmkRange.SetRange(.Start + CLng(xBookmarks(i, 1)), _
                                .Start + CLng(xBookmarks(i, 2)))
                            oBmkRange.Bookmarks.Add(xBookmarks(i, 0))
                        End If
                    Next i
                End If
                'Restore original hidden bookmark state
                oRng.Bookmarks.ShowHidden = bShowHidden
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        'NOTE: this is a new version provided by Mike.  the old version is below.
        'Doug, please review this to ensure that it's usable, ie it won't cause any
        'unexpected problems
        Private Sub Restore_Cells(oRng As Word.Range, _
                                  xXML As String, _
                                  iScope As mpDeleteScopes, _
                                  bContentControls As Boolean, _
                                  ByRef oTags As Tags)
            'restores the "Cell" and "CellAndPreviousCell" delete scopes
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_Cells"
            Dim oPreviousCell As Word.Cell
            Dim oNextCell As Word.Cell
            Dim sWidth As Single
            Dim sWidth2 As Single
            Dim sPreviousWidth As Single
            Dim lColIndex As Integer = 0
            Dim lRowIndex As Integer = 0
            Dim oTable As Word.Table
            Dim oCell As Word.Cell
            Dim iCellsAdded As Short = 0
            Dim i As Short = 0
            Dim oTag As Word.XMLNode = Nothing
            Dim oTagID As Word.XMLNode = Nothing
            Dim oXMLDoc As Xml.XmlDocument
            Dim oNodeList As Xml.XmlNodeList
            Dim oCC As Word.ContentControl
            Dim oConvert As Conversion

            On Error GoTo ProcError

            With oRng
                'get cell that will be "split" (instead of actually splitting the cell,
                'we add new cells and adjust the widths - this way, we don't have to
                'worry about preserving the formatting in the existing cell)
                oTable = .Tables(1)
                If .Cells.Count = 0 Then _
                    .Move(Word.WdUnits.wdCharacter, -1)
                oPreviousCell = .Cells(1)
                lRowIndex = oPreviousCell.RowIndex
                lColIndex = oPreviousCell.ColumnIndex
                'insert xml below existing table -
                'appending directly to table can cause
                'column widths to change if there is an
                'irregular number of columns in the table

                .EndOf(Word.WdUnits.wdTable)
                .Move(Word.WdUnits.wdCharacter, 2)

                'GLOG 6324 (dm) - the following line was converting a single paragraph
                'tag or content control immediately following the table from paragraph
                'level to inline level - InsertParagraphAfter doesn't do this
                .InsertParagraphAfter()
                '        .InsertAfter vbCr

                .Move(Word.WdUnits.wdParagraph, 1)
                .InsertXML(xXML)

                'set range to newly inserted table
                With .Tables(1).Rows.Last.Range
                    oRng.SetRange(.Start, .End)
                End With

                '12/16/10 (dm) - add tags if necessary
                If Not bContentControls Then
                    oConvert = New Conversion
                    oConvert.AddTagsToBookmarkedRangeIfNecessary(oRng)

                    'delete content controls if necessary
                    If BaseMethods.IsWordOpenXML(xXML) Then _
                        oConvert.DeleteContentControlsInRange(oRng, False)
                End If

                'extract cell widths directly from the delete scope XML
                'this will ensure width doesn't change on round trip

                oXMLDoc = New Xml.XmlDocument
                oXMLDoc.LoadXml(xXML)
                oNodeList = oXMLDoc.GetElementsByTagName("w:gridCol")

                sWidth = Val(oNodeList.Item(0).Attributes.Item(0).InnerText) / 20
                If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                    sWidth2 = Val(oNodeList.Item(1).Attributes.Item(0).InnerText) / 20 'GLOG 15924

                sPreviousWidth = oPreviousCell.Width

                'add new cell(s) in the correct location
                oNextCell = oPreviousCell.Next

                'only insert before the next cell if it's in the same row
                If Not oNextCell Is Nothing Then
                    If oNextCell.RowIndex <> oPreviousCell.RowIndex Then _
                            oNextCell = Nothing
                End If

                With oPreviousCell.Row.Cells
                    If Not oNextCell Is Nothing Then
                        'add before next cell
                        oCell = .Add(oNextCell)
                        If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                            .Add(oCell.Next)
                    Else
                        'add at end of row
                        oCell = .Add()
                        If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                            .Add()
                    End If
                End With

                'copy and paste contents of first cell
                .Cells(1).Range.Copy()

                'add padding row if oTable contains only one row
                'otherwise, pasting cell contents appends cells to
                'end of target row
                Dim bDeleteRow As Boolean = False
                If oTable.Rows.Count = 1 Then
                    oTable.Rows.Add()
                    bDeleteRow = True
                End If

                oTable.Cell(lRowIndex, lColIndex + 1).Range.Paste()

                'copy and paste contents of second cell
                If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then
                    .Cells(2).Range.Copy()
                    oTable.Cell(lRowIndex, lColIndex + 2).Range.Paste()
                End If

                'delete padding row if it exists
                If bDeleteRow Then
                    oTable.Rows.Last.Delete()
                End If

                'adjust cell widths
                oCell = oTable.Cell(lRowIndex, lColIndex + 1)
                oCell.Width = sWidth
                If sWidth2 <> 0 Then _
                    oCell.Next.Width = sWidth2
                oCell.Previous.Width = sPreviousWidth - sWidth - sWidth2

                'adding cells breaks the WordXMLNode references in the Tags
                'associated with with the other tags in the row - update now
                iCellsAdded = IIf((iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell), 1, 0) + 1
                With oTable.Rows(lRowIndex).Cells
                    For i = 1 To .Count
                        If (i <= lColIndex) Or (i > lColIndex + iCellsAdded) Then
                            'this was an existing cell - update any tags it it
                            'TODO: assign temp ids to other tags in row before
                            'adding cells to ensure that the right Tag is updated
                            'when its one of multiple associated tags
                            If bContentControls Then
                                For Each oCC In .Item(i).Range.ContentControls
                                    '8/3/11 (dm) - explicitly exclude sub vars - the
                                    'existing condition wasn't sufficient because
                                    'GetAttributeValue treats TagID and Name synonymously
                                    If BaseMethods.GetBaseName(oCC) <> "mSubVar" Then
                                        If BaseMethods.GetAttributeValue(oCC.Tag, "TagID", _
                                                oRng.Document) <> "" Then
                                            oTags.Update_CC(oCC)
                                        End If
                                    End If
                                Next oCC
                            Else
                                For Each oTag In .Item(i).Range.XMLNodes
                                    oTagID = oTag.SelectSingleNode("@TagID")
                                    If Not oTagID Is Nothing Then _
                                        oTags.Update(oTag)
                                    oTagID = Nothing
                                Next oTag
                            End If
                        End If
                    Next i
                End With

                'delete the inserted xml
                .MoveEnd()
                .MoveStart(Word.WdUnits.wdParagraph, -1)
                .Delete()
            End With

            'reset range to new location
            With oTable.Cell(lRowIndex, lColIndex + 1).Range
                oRng.SetRange(.Start, .End)
                If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                    oRng.MoveEnd(WdUnits.wdCell)
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub Restore_CellsOLD(oRng As Word.Range, xXML As String, iScope As mpDeleteScopes, ByRef oTags As Tags)
            'restores the "Cell" and "CellAndPreviousCell" delete scopes
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_CellsOLD"
            Dim oPreviousCell As Word.Cell
            Dim oNextCell As Word.Cell
            Dim sWidth As Single
            Dim sWidth2 As Single
            Dim sPreviousWidth As Single
            Dim lColIndex As Integer = 0
            Dim lRowIndex As Integer = 0
            Dim oTable As Word.Table
            Dim oCell As Word.Cell
            Dim iCellsAdded As Short = 0
            Dim i As Short = 0
            Dim oTag As Word.XMLNode = Nothing
            Dim oTagID As Word.XMLNode = Nothing

            On Error GoTo ProcError

            With oRng
                'get cell that will be "split" (instead of actually splitting the cell,
                'we add new cells and adjust the widths - this way, we don't have to
                'worry about preserving the formatting in the existing cell)
                oTable = .Tables(1)
                oPreviousCell = .Cells(1)
                lRowIndex = oPreviousCell.RowIndex
                lColIndex = oPreviousCell.ColumnIndex

                'insert xml at end of table
                .EndOf(Word.WdUnits.wdTable)
                .Move(Word.WdUnits.wdCharacter, 2)
                .InsertXML(xXML)

                'set range to newly inserted row
                With oTable.Rows.Last.Range
                    oRng.SetRange(.Start, .End)
                End With

                'store cell widths
                sWidth = .Cells(1).Width
                If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                    sWidth2 = .Cells(2).Width
                sPreviousWidth = oPreviousCell.Width

                'add new cell(s) in the correct location
                oNextCell = oPreviousCell.Next

                'only insert before the next cell if it's in the same row
                If oNextCell.RowIndex <> oPreviousCell.RowIndex Then _
                        oNextCell = Nothing

                With oPreviousCell.Row.Cells
                    If Not oNextCell Is Nothing Then
                        'add before next cell
                        oCell = .Add(oNextCell)
                        If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                            .Add(oCell.Next)
                    Else
                        'add at end of row
                        oCell = .Add()
                        If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then _
                            .Add()
                    End If
                End With

                'copy and paste contents of first cell
                .Cells(1).Range.Copy()
                oTable.Cell(lRowIndex, lColIndex + 1).Range.Paste()

                'copy and paste contents of second cell
                If iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Then
                    .Cells(2).Range.Copy()
                    oTable.Cell(lRowIndex, lColIndex + 2).Range.Paste()
                End If

                'adjust cell widths
                oCell = oTable.Cell(lRowIndex, lColIndex + 1)
                oCell.Width = sWidth
                If sWidth2 <> 0 Then _
                    oCell.Next.Width = sWidth2
                oCell.Previous.Width = sPreviousWidth - sWidth - sWidth2

                'adding cells breaks the WordXMLNode references in the Tags
                'associated with with the other tags in the row - update now
                iCellsAdded = IIf(iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell, 1, 0) + 1
                With oTable.Rows(lRowIndex).Cells
                    For i = 1 To .Count
                        If (i <= lColIndex) Or (i > lColIndex + iCellsAdded) Then
                            'this was an existing cell - update any tags it it
                            For Each oTag In .Item(i).Range.XMLNodes
                                oTagID = oTag.SelectSingleNode("@TagID")
                                If Not oTagID Is Nothing Then _
                                    oTags.Update(oTag)
                                oTagID = Nothing
                            Next oTag
                        End If
                    Next i
                End With

                'delete the inserted xml
                .MoveEnd()
                .Delete()
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function GetInsertionLocationFromXML(ByVal xSegmentXML As String, _
                                                     ByVal oSegmentNode As Word.XMLNode, _
                                                     ByVal xVariableName As String, _
                                                     ByVal iScope As mpDeleteScopes) As Word.Range
            'gets location for reinsertion of deleted scope from supplied xml
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetInsertionLocationFromXML"
            Dim lTagStart As Integer = 0
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim bIsEmbedded As Boolean = False
            Dim lTag2Start As Integer = 0
            Dim lPreviousParaEnd As Integer = 0
            Dim lNextParaStart As Integer = 0
            Dim rngSegment As Word.Range = Nothing
            Dim xText As String = ""
            Dim oNode As Word.XMLNode = Nothing
            Dim rngLocation As Word.Range = Nothing
            Dim xParaXML As String = ""
            Dim xSegmentNodeXML As String = ""
            Dim xStylesXML As String = ""
            Dim xPrefix As String = ""
            Dim xStartVarTag As String = ""
            Dim xStartBlockTag As String = ""

            On Error GoTo ProcError

            'get mp10 prefix used in the provided xml
            xPrefix = BaseMethods.GetMP10SchemaPrefix(xSegmentXML)

            'get segment node xml
            xSegmentNodeXML = GetSegmentNodeXML(xSegmentXML, oSegmentNode, xVariableName)
            If xSegmentNodeXML = "" Then Exit Function

            'build tag search strings
            xStartVarTag = "<" & xPrefix & ":mVar "
            xStartBlockTag = "<" & xPrefix & ":mBlock "

            'find tag
            lTagStart = InStr(xSegmentNodeXML, "TagID=" & Chr(34) & xVariableName)
            If lTagStart = 0 Then Exit Function
            lTagStart = InStrRev(xSegmentNodeXML, xStartVarTag, lTagStart)
            lPos = GetParaStart(xSegmentNodeXML, lTagStart, False)
            lPos2 = InStr(lTagStart, xSegmentNodeXML, GlobalMethods.mpEndParaTag)
            lPos3 = InStr(lTagStart, xSegmentNodeXML, GlobalMethods.mpEmptyParaTag)
            bIsEmbedded = ((lPos = 0) Or (lPos > lPos2))

            'get next para start
            If Not bIsEmbedded Then _
                lPos = GetParaStart(xSegmentNodeXML, lPos + 1, False)
            If lPos + lPos3 = 0 Then
                'variable is in last paragraph of mSEG
                rngLocation = oSegmentNode.Range
                rngLocation.EndOf()
                GetInsertionLocationFromXML = rngLocation
                Exit Function
            ElseIf (lPos3 = 0) Or (lPos < lPos3) Then
                'we're only interested in non-empty paras
                lNextParaStart = lPos
            End If

            'get previous para end
            lPos = InStrRev(xSegmentNodeXML, GlobalMethods.mpEndParaTag, lTagStart)
            lPos2 = InStrRev(xSegmentNodeXML, GlobalMethods.mpEmptyParaTag, lTagStart)
            If lPos + lPos2 = 0 Then
                'variable is in first paragraph of mSEG
                rngLocation = oSegmentNode.Range
                rngLocation.StartOf()
                GetInsertionLocationFromXML = rngLocation
                Exit Function
            ElseIf lPos > lPos2 Then
                'we're only interested in non-empty paras
                lPreviousParaEnd = lPos
            End If

            'check whether previous paragraph contains tag
            If lPreviousParaEnd > 0 Then
                If bIsEmbedded Then
                    lPos = GetParaStart(xSegmentNodeXML, lTagStart, True)
                Else
                    lPos = lTagStart
                End If

                'mVar
                lTag2Start = InStrRev(xSegmentNodeXML, xStartVarTag, lPos)
                If lTag2Start = 0 Then
                    'mBlock
                    lTag2Start = InStrRev(xSegmentNodeXML, xStartBlockTag, lPos)
                End If

                If lTag2Start > 0 Then
                    'preceding tag found - see if it's in previous paragraph
                    lPos = InStrRev(xSegmentNodeXML, GlobalMethods.mpEndParaTag, lPreviousParaEnd)
                    lPos2 = InStrRev(xSegmentNodeXML, GlobalMethods.mpEmptyParaTag, lPreviousParaEnd)
                    If lPos2 >= lPos Then _
                        lPos = lPos2
                    If lTag2Start > lPos Then
                        'get tag id
                        lPos = InStr(lTag2Start, xSegmentNodeXML, "TagID=") + 7
                        lPos2 = InStr(lPos, xSegmentNodeXML, Chr(34))
                        xText = Mid$(xSegmentNodeXML, lPos, lPos2 - lPos)

                        'search document for tag
                        oNode = oSegmentNode.SelectSingleNode("x:mVar[@TagID='" & _
                            xText & "']", "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                        If Not oNode Is Nothing Then
                            rngLocation = oNode.Range.Paragraphs(1).Range
                            rngLocation.EndOf()
                            GetInsertionLocationFromXML = rngLocation
                            Exit Function
                        End If
                    End If
                End If
            End If

            'check whether next paragraph contains tag
            If lNextParaStart > 0 Then
                If bIsEmbedded Then
                    lPos = InStr(lTagStart, xSegmentNodeXML, GlobalMethods.mpEndParaTag)
                Else
                    lPos = lTagStart
                End If

                'mVar
                lTag2Start = InStr(lPos, xSegmentNodeXML, xStartVarTag)
                If lTag2Start = 0 Then
                    'mBlock
                    lTag2Start = InStr(lPos, xSegmentNodeXML, xStartBlockTag)
                End If

                If lTag2Start > 0 Then
                    'subsequent tag found - see if it's in next paragraph
                    lPos = GetParaStart(xSegmentNodeXML, lNextParaStart + 1, False)
                    lPos2 = InStr(lNextParaStart + 1, xSegmentNodeXML, GlobalMethods.mpEmptyParaTag)
                    If (lPos2 > 0) And (lPos2 < lPos) Then _
                        lPos = lPos2
                    If (lPos = 0) Or (lTag2Start < lPos) Then
                        'get tag id
                        lPos = InStr(lTag2Start, xSegmentNodeXML, "TagID=") + 7
                        lPos2 = InStr(lPos, xSegmentNodeXML, Chr(34))
                        xText = Mid$(xSegmentNodeXML, lPos, lPos2 - lPos)

                        'search document for tag
                        oNode = oSegmentNode.SelectSingleNode("x:mVar[@TagID='" & _
                            xText & "']", "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                        If Not oNode Is Nothing Then
                            rngLocation = oNode.Range.Paragraphs(1).Range
                            rngLocation.StartOf()
                            GetInsertionLocationFromXML = rngLocation
                            Exit Function
                        End If
                    End If
                End If
            End If

            rngSegment = oSegmentNode.Range

            'get styles node for use below
            lPos = InStr(xSegmentXML, "<w:styles")
            lPos2 = InStr(lPos, xSegmentXML, "</w:styles>")
            xStylesXML = Mid$(xSegmentXML, lPos, lPos2 - lPos + 11)

            'search for paragraph that matches style and beginning text of previous paragraph
            If lPreviousParaEnd > 0 Then
                lPos = GetParaStart(xSegmentNodeXML, lPreviousParaEnd, True) + 5
                xParaXML = Mid$(xSegmentNodeXML, lPos, lPreviousParaEnd - lPos)
                rngLocation = GetParagraph(rngSegment, xParaXML, xStylesXML)
                If Not rngLocation Is Nothing Then
                    rngLocation.EndOf()
                    GetInsertionLocationFromXML = rngLocation
                    Exit Function
                End If
            End If

            'search for paragraph that matches style and beginning text of next paragraph
            If lNextParaStart > 0 Then
                lPos = InStr(lNextParaStart, xSegmentNodeXML, GlobalMethods.mpEndParaTag)
                xParaXML = Mid$(xSegmentNodeXML, lNextParaStart, lPos - lNextParaStart)
                rngLocation = GetParagraph(rngSegment, xParaXML, xStylesXML)
                If Not rngLocation Is Nothing Then
                    rngLocation.StartOf()
                    GetInsertionLocationFromXML = rngLocation
                    Exit Function
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetParagraph(ByVal rngSegment As Word.Range, _
                                      ByVal xParaXML As String, _
                                      ByVal xStylesXML As String) As Word.Range
            'gets the range of the paragraph in rngSegment that matches the specified xml;
            'if not found, returns nothing
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetParagraph"
            Dim xStyle As String = ""
            Dim xText As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim rngLocation As Word.Range = Nothing
            Dim bFound As Boolean = False

            On Error GoTo ProcError

            'style
            lPos = InStr(xParaXML, "<w:pStyle w:val=")
            If lPos > 0 Then
                'get style ID
                lPos = lPos + 17
                lPos2 = InStr(lPos, xParaXML, Chr(34))
                xStyle = Mid$(xParaXML, lPos, lPos2 - lPos)

                'get style name
                lPos = InStr(xStylesXML, "w:styleId=" & Chr(34) & xStyle & Chr(34))
                lPos = InStr(lPos, xStylesXML, "<w:name w:val=")
                lPos = lPos + 15
                lPos2 = InStr(lPos, xStylesXML, Chr(34))
                xStyle = Mid$(xStylesXML, lPos, lPos2 - lPos)
            Else
                'style is Normal
                xStyle = GlobalMethods.CurWordApp.ActiveDocument.Styles(wdBuiltInStyle.wdStyleNormal).NameLocal
            End If

            'text
            lPos = InStr(xParaXML, "<w:t>")
            If lPos > 0 Then
                lPos = lPos + 5
                lPos2 = InStr(lPos, xParaXML, "</w:t>")
                xText = Mid$(xParaXML, lPos, lPos2 - lPos)
            End If

            'search document
            With rngSegment.Find
                .ClearFormatting()
                .Text = xText
                .Format = True
                .Style = xStyle
                .Execute()
                If .Found Then
                    rngLocation = rngSegment.Paragraphs(1).Range
                    If xText = "" Then
                        bFound = (rngLocation.Text = vbCr)
                    Else
                        bFound = (Left$(rngLocation.Text, Len(xText)) = xText)
                    End If
                    If bFound Then
                        GetParagraph = rngLocation
                    End If
                End If
            End With

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetSegmentNodeXML(ByVal xSegmentXML As String, _
                                           ByVal oSegmentNode As Word.XMLNode, _
                                           ByVal xVariableName As String) As String
            'gets the portion of xSegmentXML that represents the xml for the mSEG that
            'contains the xVariableName mVar
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetSegmentNodeXML"
            Dim oXML As xml.xmlDocument
            Dim xXPath As String = ""
            Dim oNode As xml.xmlNode
            Dim xSegmentTagID As String = ""
            Dim xParentTagID As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xObjectData As String = ""
            Dim oNs As Xml.XmlNamespaceManager

            On Error GoTo ProcError

            With oSegmentNode
                xSegmentTagID = .SelectSingleNode("@TagID").NodeValue
                xObjectData = BaseMethods.Decrypt(.SelectSingleNode("@ObjectData") _
                    .NodeValue)
                lPos = InStr(xObjectData, "ParentTagID=")
                If lPos > 0 Then
                    lPos = lPos + 12
                    lPos2 = InStr(lPos, xObjectData, "|")
                    xParentTagID = Mid$(xObjectData, lPos, lPos2 - lPos)
                End If
            End With

            oXML = New xml.xmlDocument
            oXML.LoadXml(xSegmentXML)
            oNs = New Xml.XmlNamespaceManager(oXML.NameTable)
            oNs.AddNamespace("ns0", GlobalMethods.mpNamespace)

            If xParentTagID <> "" Then
                xXPath = "//ns0:mSEG[@TagID='" & xSegmentTagID & "' and " & _
                    "contains(@ObjectData, 'ParentTagID='" & xParentTagID & _
                    "')][//ns0:mVar[@TagID='" & xVariableName & "']]"
            Else
                xXPath = "//ns0:mSEG[@TagID='" & xSegmentTagID & "']" & _
                    "[//ns0:mVar[@TagID='" & xVariableName & "']]"
            End If
            oNode = oXML.SelectSingleNode(xXPath, oNs)
            If Not oNode Is Nothing Then _
                GetSegmentNodeXML = oNode.OuterXml

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetParaStart(ByVal xXML As String, _
                                      ByVal lStartPos As Integer, _
                                      ByVal bReverse As Boolean) As Integer
            'gets the position in xXML of the start tag for the paragraph
            'before (bReverse= true) or after (bReverse = false) lStartPos
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetParaStart"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0

            On Error GoTo ProcError

            If bReverse Then
                lPos = InStrRev(xXML, "<w:p ", lStartPos)
                lPos2 = InStrRev(xXML, "<w:p>", lStartPos)
                GetParaStart = Math.Max(lPos, lPos2)
            Else
                lPos = InStr(lStartPos, xXML, "<w:p ")
                lPos2 = InStr(lStartPos, xXML, "<w:p>")
                If lPos = 0 Then
                    GetParaStart = lPos2
                ElseIf lPos2 = 0 Then
                    GetParaStart = lPos
                Else
                    GetParaStart = Math.Min(lPos, lPos2)
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetInsertionXML(ByVal xSegmentTag As String, _
                                         ByVal oSegmentRange As Word.Range, _
                                         ByVal xVariableName As String, _
                                         ByVal xSegmentXML As String, _
                                         ByRef xBodyXML As String) As String
            'gets the reinsertion xml for xVariableName
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetInsertionXML"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim oDeletedScopes As Word.XMLNode = Nothing
            Dim xDeletedScopes As String = ""
            Dim xDocNode As String = ""
            Dim xStylesNode As String = ""
            Dim xBodyNode As String = ""
            Dim xTag As String = ""
            Dim xSearch As String = ""

            On Error GoTo ProcError

            'retrieving range.xml forces a temp save - we avoid this by
            'getting styles nodes from segment xml if supplied
            If xSegmentXML = "" Then _
                xSegmentXML = oSegmentRange.XML


            'get deleted scopes attribute from mSEG tag
            xDeletedScopes = BaseMethods.GetAttributeValue(xSegmentTag, "DeletedScopes", _
                oSegmentRange.Document)

            If xDeletedScopes <> "" Then
                lPos = InStr(xDeletedScopes, "DeletedTag=" & xVariableName & "�")
                If lPos Then
                    'GLOG 5428 (dm) - account for possibility that a subsequent scope is Word ML,
                    'while this one is open xml - we were previously searching the entire rest of
                    'the DeletedScopes attribute
                    lPos2 = InStr(lPos, xDeletedScopes, "�w:body>")
                    xSearch = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                    If InStr(xSearch, "�w:wordDocument ") > 0 Then
                        'get wordDocument start tag
                        lPos = InStr(lPos, xDeletedScopes, "�w:wordDocument ")
                        lPos2 = InStr(lPos, xDeletedScopes, "�w:body>")
                        xDocNode = Mid$(xDeletedScopes, lPos, lPos2 - lPos)

                        'get body node
                        lPos = InStr(lPos2, xDeletedScopes, "�/w:body>|")
                        xBodyNode = Mid$(xDeletedScopes, lPos2, lPos - lPos2)

                        'include empty paragraph at end to preserve format
                        'of last paragraph of scope
                        xBodyNode = xBodyNode & "<w:p /></w:body>"
                    Else
                        '1/13/11 (dm) - scope may be open xml
                        xTag = xSegmentTag
                        If xTag <> "" Then
                            GetInsertionXML = GetInsertionXML_CC(xTag, oSegmentRange, _
                                xVariableName, xSegmentXML, xBodyXML)
                        End If
                        Exit Function
                    End If
                End If
            End If

            If xBodyNode = "" Then
                'no delete scope definition in tag
                Exit Function
            End If

            'retrieving range.xml forces a temp save - we avoid this by
            'getting styles nodes from segment xml if supplied
            If xSegmentXML = "" Then _
                xSegmentXML = oSegmentRange.XML

            'GLOG 5276 (dm) - get lists node - just do if binary, as conversion from
            'open xml isn't warranted by this issue - with the new code to conform
            'numbered paragraphs to the style, this block is here just to avoid adding
            'a new list template to the document
            Dim xListsNode As String = ""
            If Not BaseMethods.IsWordOpenXml(xSegmentXML) Then
                lPos = InStr(xSegmentXML, "<w:lists")
                If lPos > 0 Then
                    lPos2 = InStr(lPos, xSegmentXML, "</w:lists>")
                    xListsNode = Mid$(xSegmentXML, lPos, lPos2 - lPos + 10)
                End If
            End If

            'get styles node
            If BaseMethods.IsWordOpenXml(xSegmentXML) Then
                'segment design xml is open xml - locate the correct part - there are also
                'w:styles nodes in the /word/styleswitheffects, /word/glossary/styles, and
                '/word/glossary/styleswitheffects parts
                lPos = InStr(xSegmentXML, "<pkg:part pkg:name=""/word/styles.xml""")

                'strip namespaces from styles start tag (1/14/11, dm)
                lPos = InStr(lPos, xSegmentXML, "<w:styles")
                lPos2 = InStr(lPos, xSegmentXML, ">")
                xSegmentXML = Left$(xSegmentXML, lPos - 1) & "<w:styles>" & _
                    Mid$(xSegmentXML, lPos2 + 1)
            Else
                'segment design is straight xml - there's only one w:styles node
                lPos = InStr(xSegmentXML, "<w:styles")
            End If
            lPos2 = InStr(lPos, xSegmentXML, "</w:styles>")
            xStylesNode = Mid$(xSegmentXML, lPos, lPos2 - lPos + 11)

            'GLOG 2115 (dm) - strip Normal style line spacing node - it's not required
            'and can cause problems when the scope is being restored immediately above a table
            xStylesNode = StripNormalStyleLineSpacing(xStylesNode)

            'concatenate
            xDocNode = xDocNode & xListsNode & xStylesNode & xBodyNode & "</w:wordDocument>"

            'replace substitution character and return
            GetInsertionXML = Replace(xDocNode, "�", "<")

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub DeleteDefinition(ByRef xDeletedScopes As String, _
                                     ByVal xVariableName As String)
            'deletes "DeletedTag" definition from xDeletedScopes
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.DeleteDefinition"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xScope As String = ""
            Dim xTag As String = ""
            Dim oWordDoc As WordDoc
            Dim xSearch As String = ""

            On Error Resume Next

            oWordDoc = New WordDoc

            lPos = InStr(xDeletedScopes, "DeletedTag=" & xVariableName & "�")
            If lPos Then
                'get scope
                lPos2 = InStr(lPos, xDeletedScopes, "�/w:body>|") + 10
                xScope = Mid$(xDeletedScopes, lPos, lPos2 - lPos)

                'delete scope
                xDeletedScopes = Left$(xDeletedScopes, lPos - 1) & _
                    Mid$(xDeletedScopes, lPos2)

                'delete associated doc vars
                xSearch = "�w:tag w:val="
                lPos = InStr(xScope, xSearch)
                If lPos = 0 Then
                    'xml tags
                    xSearch = " Reserved="
                    lPos = InStr(xScope, xSearch)
                End If
                While lPos <> 0
                    lPos = lPos + Len(xSearch) + 1
                    lPos2 = InStr(lPos, xScope, Chr(34))
                    xTag = Mid$(xScope, lPos, lPos2 - lPos)
                    oWordDoc.DeleteRelatedObjects(xTag, GlobalMethods.CurWordApp.ActiveDocument)
                    lPos = InStr(lPos, xScope, xSearch)
                End While
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub GetVariablesInScope(ByVal rngScope As Word.Range, _
                                        ByVal xPrimaryVar As String, _
                                        ByVal bDelete As Boolean, _
                                        ByVal bContentControls As Boolean, _
                                        ByRef oTags As Tags, _
                                        ByRef xVars As String, _
                                        Optional ByVal xTagPrefixID As String = "", _
                                        Optional ByVal xPassword As String = "")
            'cycles through rngScope, adding/removing the XMLNodes
            'for other variables to/from oTags;
            'returns a pipe delimited list of the variables added/deleted
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetVariablesInScope"
            Dim oNode As Word.XMLNode = Nothing
            Dim oTagID As Word.XMLNode = Nothing
            Dim xTagID As String = ""
            Dim xNewTagID As String = ""
            Dim oTag As Tag
            Dim oObjectData As Word.XMLNode = Nothing
            Dim xVar As String = ""
            Dim xObjectData As String = ""
            Dim oCC As Word.ContentControl
            Dim xElement As String = ""

            On Error GoTo ProcError

            If bContentControls Then
                'content controls
                For Each oCC In rngScope.ContentControls
                    With oCC
                        xElement = BaseMethods.GetBaseName(oCC)
                        If ((xElement = "mVar") Or (xElement = "mBlock") Or (xElement = "mDel")) And _
                                (.Range.Start >= rngScope.Start) And (.Range.End <= rngScope.End) Then
                            xTagID = BaseMethods.GetAttributeValue(.Tag, "TagID", .Range.Document)
                            If xTagID <> xPrimaryVar Then
                                'this is a tag for a different variable
                                If bDelete Then
                                    'remove from collection
                                    oTags.Delete_CC(oCC)
                                    xNewTagID = xTagID
                                Else
                                    '6/21/11 (dm) - placeholder text was getting reset
                                    'on insertion
                                    oCC.SetPlaceholderText(, , "")

                                    'retag
                                    BaseMethods.RetagContentControl(oCC)

                                    'add to collection
                                    oTag = oTags.Insert_CC(oCC, True)
                                    xNewTagID = oTag.TagID
                                End If

                                'prepend tag type and whether added or deleted
                                xVar = xElement & "�" & CStr(Math.Abs(CInt(Not bDelete))) & _
                                    "�" & xNewTagID

                                'add variable name to the list
                                If InStr(xVars & "|", xVar & "|") = 0 Then
                                    If xVars <> "" Then _
                                        xVars = xVars & "|"
                                    xVars = xVars & xVar
                                End If
                            End If
                        End If
                    End With
                Next oCC
            Else
                'xml tags
                For Each oNode In rngScope.XMLNodes
                    With oNode
                        If ((.BaseName = "mVar") Or (.BaseName = "mBlock") Or (.BaseName = "mDel")) And _
                                (.Range.Start >= rngScope.Start) And (.Range.End <= rngScope.End) Then
                            oTagID = .SelectSingleNode("@TagID")
                            If Not oTagID Is Nothing Then
                                xTagID = oTagID.NodeValue
                                If xTagID <> xPrimaryVar Then
                                    'this is a tag for a different variable
                                    oObjectData = .SelectSingleNode("@ObjectData")
                                    If bDelete Then
                                        'decompress object data
                                        If Not oObjectData Is Nothing Then
                                            oObjectData.NodeValue = _
                                                BaseMethods.Decrypt(oObjectData.NodeValue)
                                        End If

                                        'remove tag from collection
                                        oTags.Delete(oNode)
                                        xNewTagID = xTagID
                                    Else
                                        'recompress object data, adding tag prefix id if necessary
                                        If (.BaseName <> "mDel") And (Not oObjectData Is Nothing) Then
                                            xObjectData = oObjectData.NodeValue
                                            If (xTagPrefixID <> "") And _
                                                    (BaseMethods.GetTagPrefixID(xObjectData) = "") Then
                                                xObjectData = xTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & _
                                                    xObjectData
                                            End If
                                            oObjectData.NodeValue = BaseMethods.Encrypt(xObjectData, xPassword)
                                        End If

                                        'retag
                                        BaseMethods.RetagXMLNode(oNode)

                                        'add tag to collection
                                        oTag = oTags.Insert(oNode, True)
                                        xNewTagID = oTag.TagID
                                    End If

                                    'prepend tag type and whether added or deleted
                                    xVar = .BaseName & "�" & CStr(Math.Abs(CInt(Not bDelete))) & _
                                        "�" & xNewTagID

                                    'add variable name to the list
                                    If InStr(xVars & "|", xVar & "|") = 0 Then
                                        If xVars <> "" Then _
                                            xVars = xVars & "|"
                                        xVars = xVars & xVar
                                    End If
                                End If
                            End If
                        End If
                    End With
                Next oNode
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetChildmDels(ByVal oTag As Word.XMLNode, ByRef oTags As Tags) As String
            'returns attributes of all child mDels of oTag as a delimited string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetChildmDels"
            Dim omDels As Word.XMLNodes
            Dim xmDels As String = ""
            Dim oTagID As Word.XMLNode = Nothing
            Dim oObjectData As Word.XMLNode = Nothing
            Dim oReserved As Word.XMLNode = Nothing
            Dim xReserved As String = ""
            Dim i As Short = 0
            Dim xTempID As String = ""

            On Error GoTo ProcError

            omDels = oTag.SelectNodes("descendant::x:mDel", "xmlns:x='" & GlobalMethods.mpNamespace & "'")
            For i = 1 To omDels.Count
                'assign a temp id
                xTempID = oTags.AddTempID(omDels(i))

                'get properties
                oTagID = omDels(i).SelectSingleNode("@TagID")
                oObjectData = omDels(i).SelectSingleNode("@ObjectData")
                '10.2 - Add Reserved attribute to saved properties
                oReserved = omDels(i).SelectSingleNode("@Reserved")
                If Not oReserved Is Nothing Then
                    xReserved = oReserved.NodeValue
                Else
                    xReserved = ""
                End If
                If (Not oTagID Is Nothing) And (Not oObjectData Is Nothing) Then
                    xmDels = xmDels & oTagID.NodeValue & "|" & oObjectData.NodeValue & _
                        "|" & xTempID & "|" & xReserved & "�"
                End If
            Next i
            If xmDels <> "" Then _
                xmDels = Left$(xmDels, Len(xmDels) - 1)

            GetChildmDels = xmDels
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub RestoreChildmDels(ByVal oWordNode As Word.XMLNode, _
                                     ByVal xmDels As String, _
                                     ByRef oTags As Tags)
            'recreates mDels in oWordNode from provided string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoreChildmDels"
            Dim rngBefore As Word.Range = Nothing
            Dim rngAfter As Word.Range = Nothing
            Dim omDels As Object = Nothing
            Dim oProps As Object = Nothing
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim omDelNode As Word.XMLNode = Nothing
            Dim iAttempts As Short = 0
            Dim oAttribute As Word.XMLNode = Nothing
            Dim lEnd As Integer = 0
            Dim xObjectData As String = ""
            Dim xTempID As String = ""
            Dim xTag As String = ""
            Dim bIsEmpty As Boolean = False

            On Error GoTo ProcError

            If xmDels = "" Then Exit Sub

            rngBefore = oWordNode.Range
            rngBefore.StartOf()
            rngAfter = oWordNode.Range
            rngAfter.EndOf()
            bIsEmpty = (rngBefore.Start = rngAfter.End)

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'position based on reinsertion location
                oProps = Split(omDels(i), "|")
                If (CInt(oProps(2)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bIsEmpty Then
                    rngmDel = rngAfter.Duplicate
                Else
                    rngmDel = rngBefore.Duplicate
                End If

                'add mDel - try a second time if necessary,
                'as this line fails on first execution in Word 2007
                On Error GoTo TryAgain
                omDelNode = rngmDel.XMLNodes.Add("mDel", GlobalMethods.mpNamespace)
                On Error GoTo ProcError

                'set attribute values
                oAttribute = omDelNode.Attributes.Add("TagID", "")
                oAttribute.NodeValue = oProps(0)
                oAttribute = omDelNode.Attributes.Add("ObjectData", "")
                xObjectData = oProps(1) & "|" & oProps(2)
                If UBound(oProps) > 4 Then
                    xObjectData = xObjectData & "|" & oProps(3) & "|" & oProps(4)
                    xTempID = oProps(5)
                    xTag = oProps(6)
                Else
                    xTempID = oProps(3)
                    xTag = oProps(4)
                End If
                oAttribute.NodeValue = xObjectData
                oAttribute = omDelNode.Attributes.Add("Reserved", "")
                oAttribute.NodeValue = xTag

                '10.1 - add bookmark
                '        omDelNode.Range.Bookmarks.Add "_" & xTag

                'update tag in collection
                oTags.Update(omDelNode, , xTempID)

                'move position to end of tag
                lEnd = rngmDel.End + 2
                If (CInt(oProps(2)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bIsEmpty Then
                    rngAfter.SetRange(lEnd, lEnd)
                Else
                    rngBefore.SetRange(lEnd, lEnd)
                End If
            Next i

            Exit Sub
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub RestoremDels(ByVal rngLocation As Word.Range, _
                                ByVal xmDels As String, _
                                ByRef oTags As Tags)
            'recreates mDels from provided string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoremDels"
            Dim rngBefore As Word.Range = Nothing
            Dim rngAfter As Word.Range = Nothing
            Dim omDels As Object = Nothing
            Dim oProps As Object = Nothing
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim omDelNode As Word.XMLNode = Nothing
            Dim iAttempts As Short = 0
            Dim oAttribute As Word.XMLNode = Nothing
            Dim lEnd As Integer = 0
            Dim xObjectData As String = ""
            Dim xTempID As String = ""
            Dim bRangeIsEmpty As Boolean = False
            Dim oTag As Tag
            Dim bShowMarkup As Boolean = False
            Dim xTag As String = ""

            On Error GoTo ProcError

            If xmDels = "" Then Exit Sub

            'show tags
            GlobalMethods.CurWordApp.ScreenUpdating = False
            bShowMarkup = GlobalMethods.CurWordApp.ActiveWindow.View.ShowXMLMarkup
            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, True)

            'set insertion range
            bRangeIsEmpty = ((Len(rngLocation.Text) = 0) Or (rngLocation.Text = vbCr)) 'GLOG 8440
            rngBefore = GetmDelInsertionLocation(rngLocation.Paragraphs.First, _
                mpReinsertionLocations.mpReinsertionLocation_Before, False)
            rngAfter = GetmDelInsertionLocation(rngLocation.Paragraphs.Last, _
                mpReinsertionLocations.mpReinsertionLocation_After, False)

            '    'adjust end for table
            '    If rngLocation.Information(Word.wdInformation.wdWithInTable) Then
            '        If rngLocation = rngLocation.Tables(1).Range Then _
            '            rngAfter.Move Word.WdUnits.wdCharacter, -2
            '    End If

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'position based on reinsertion location
                oProps = Split(omDels(i), "|")
                If (CInt(oProps(2)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bRangeIsEmpty Then
                    rngmDel = rngAfter.Duplicate
                Else
                    rngmDel = rngBefore.Duplicate
                End If

                'add mDel - try a second time if necessary,
                'as this line fails on first execution in Word 2007
                On Error GoTo TryAgain
                omDelNode = rngmDel.XMLNodes.Add("mDel", GlobalMethods.mpNamespace)
                On Error GoTo ProcError

                'set attribute values
                oAttribute = omDelNode.Attributes.Add("TagID", "")
                oAttribute.NodeValue = oProps(0)
                oAttribute = omDelNode.Attributes.Add("ObjectData", "")
                xObjectData = oProps(1) & "|" & oProps(2)
                If UBound(oProps) > 4 Then
                    xObjectData = xObjectData & "|" & oProps(3) & "|" & oProps(4)
                    xTempID = oProps(5)
                    xTag = oProps(6)
                Else
                    xTempID = oProps(3)
                    xTag = oProps(4)
                End If
                oAttribute.NodeValue = xObjectData
                '10.2 - Assign Tag to Reserved attribute
                oAttribute = omDelNode.Attributes.Add("Reserved", "")
                oAttribute.NodeValue = xTag

                '10.2 - add bookmark
                '        omDelNode.Range.Bookmarks.Add "_" & xTag

                'update tag in collection
                If Not oTags Is Nothing Then
                    On Error Resume Next
                    oTag = oTags.GetTag(omDelNode, , xTempID)
                    On Error GoTo ProcError

                    If Not oTag Is Nothing Then
                        oTags.Update(omDelNode, , xTempID)
                    Else
                        'it's possible that tags were refreshed while mDel was gone
                        oTags.Insert(omDelNode)
                    End If
                End If

                'move position to end of tag
                lEnd = rngmDel.End + 2
                If (CInt(oProps(2)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bRangeIsEmpty Then
                    rngAfter.SetRange(lEnd, lEnd)
                Else
                    rngBefore.SetRange(lEnd, lEnd)
                End If
            Next i

            'restore tags
            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, bShowMarkup)

            Exit Sub
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function InsertmDels(ByVal rngScope As Word.Range, _
                                     ByVal oSegmentRng As Word.Range, _
                                     ByVal xSegmentTag As String, _
                                     ByVal oTargetNode As Word.XMLNode, _
                                     ByVal xVariableName As String, _
                                     ByVal xObjectDBID As String, _
                                     ByVal iScope As mpDeleteScopes, _
                                     ByRef oTags As Tags) As mpReinsertionLocations
            '- inserts mDels to replace oTargetNode and all of the existing mDels in the
            'scope, preserving the sequence in the document
            '- mDel is inserted in the subsequent paragraph unless the scope is the last
            'paragraph in the mSEG or table cell
            '- if the insertion location contains blockable tags, mDel is inserted inside
            'innermost tag to preserve blockability
            '- mDel object data structure: "tag id of parent segment|reinsertion location|native
            'to table?|tag id of parent block" - the last two properties indicate whether the
            'scope for an mDel at the start of a table or block should be reinserted inside the
            'table/block or above it
            '- returns reinsertion location
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.InsertmDels"
            Dim iAttempts As Short = 0
            Dim rngSegment As Word.Range = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim lSegEnd As Integer = 0
            Dim rngPara As Word.Range = Nothing
            Dim i As Short = 0
            Dim oTag As Word.XMLNode = Nothing
            Dim rngLocation As Word.Range = Nothing
            Dim oWordDoc As WordDoc
            Dim oTagID As Word.XMLNode = Nothing
            Dim oObjectData As Word.XMLNode = Nothing
            Dim oReserved As Word.XMLNode = Nothing
            Dim xmDels As String = ""
            Dim xTargetProps As String = ""
            Dim oProps As Object = Nothing
            Dim omDels As Object = Nothing
            Dim bTargetIsInString As Boolean = False
            Dim iCount As Short = 0
            Dim bSkip As Boolean = False
            Dim bScopeIsCell As Boolean = False
            Dim rngTag As Word.Range = Nothing
            Dim bIsTarget As Boolean = False
            Dim bParaScopeInCell As Boolean = False
            Dim xParentBlock As String = ""
            Dim bNativeToTable As Boolean = False
            Dim oBlockTag As Word.XMLNode = Nothing
            Dim xTempID As String = ""
            Dim rngmDels As Word.Range = Nothing
            Dim bIsNativeToBlock As Boolean = False
            Dim xReserved As String = ""
            Dim xTag As String = ""
            Dim xObjectData As String = ""
            Dim oDoc As Word.Document
            Dim oParentNode As Word.XMLNode = Nothing
            Dim xSegmentTagID As String = ""

            On Error GoTo ProcError

            'if scope is cell, or cell and previous cell, mDel always gets inserted before
            bScopeIsCell = ((iScope = mpDeleteScopes.mpDeleteScope_Cell) Or _
                (iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell))

            'get reinsertion location
            rngSegment = oSegmentRng
            oDoc = rngSegment.Document
            lSegEnd = rngSegment.End
            If (lSegEnd >= rngScope.End) And Not bScopeIsCell Then
                'insert mDel in next paragraph
                If rngScope.Information(Word.WdInformation.wdWithInTable) And _
                        (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) Then
                    bParaScopeInCell = True
                    If rngScope.End = rngScope.Cells(1).Range.End Then
                        'this is the last paragraph of cell -
                        'insert mDel in previous paragraph
                        iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After
                    Else
                        iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before
                    End If
                Else
                    iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before
                End If
            Else
                'GLOG 8875
                If rngScope.Information(Word.WdInformation.wdWithInTable) And _
                                    (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) Then
                    bParaScopeInCell = True
                End If
                'this is last paragraph of mSEG - insert mDel in previous paragraph
                iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After
            End If

            oWordDoc = New WordDoc

            Dim oParentSegBmk As Word.Bookmark = Nothing
            Dim oParentSegBmk2 As Word.Bookmark = Nothing
            Dim xParentSegBmk As String = ""
            Dim oSegRng As Word.Range = Nothing
            Dim bAdjustSegBmk As Boolean = False
            Dim lParentSegBmkEnd As Integer = 0

            'get target paragraph
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                'put in next paragraph

                '       expand segment range to next paragraph if necessary
                oParentSegBmk = oWordDoc.GetParentSegmentBookmarkFromRange(rngScope)
                rngPara = rngScope.Next(Word.WdUnits.wdParagraph)

                If Not rngPara Is Nothing Then
                    oParentSegBmk2 = oWordDoc.GetParentSegmentBookmarkFromRange(rngPara)
                Else
                    'GLOG 6777: If no next paragraph, insert empty paragraph
                    rngPara = rngScope.Duplicate
                    rngPara.InsertParagraphAfter()
                    rngPara = rngPara.Paragraphs.Last.Range
                End If

                'GLOG 7883 (dm) - new object var was being set to the same object
                'rather than a duplicate, inadvertently causing rngScope to expand
                oSegRng = rngScope.Duplicate

                'GLOG 6815: If range is at start of Segment, may be outside of bookmark
                If oParentSegBmk Is Nothing And Not oParentSegBmk2 Is Nothing Then
                    oSegRng.SetRange(rngScope.Start, oParentSegBmk2.Range.End)
                    oParentSegBmk = oSegRng.Bookmarks.Add(oParentSegBmk2.Name)
                End If

                lParentSegBmkEnd = oParentSegBmk.Range.End
                xParentSegBmk = oParentSegBmk.Name

                If rngScope.StoryType = wdStoryType.wdTextFrameStory Then
                    If Not oParentSegBmk2 Is Nothing Then
                        If oParentSegBmk2.Name <> xParentSegBmk Then
                            'next paragraph is not in the segment - expand segment
                            oSegRng.SetRange(oParentSegBmk.Range.Start, rngPara.End)
                            oSegRng.Bookmarks.Add(xParentSegBmk)

                            If oParentSegBmk.Range.End = lParentSegBmkEnd Then
                                'expanding the segment did not work - this is an issue with Word -
                                'add an extra paragraph to end of the segment
                                oParentSegBmk.Range.InsertParagraphAfter()

                                'rebookmark - bookmark should expand
                                oSegRng.Bookmarks.Add(xParentSegBmk)
                            End If
                        End If
                    Else
                        'next paragraph is not in any segment - expand original
                        'parent to include this paragraph
                        oSegRng.SetRange(oParentSegBmk.Range.Start, rngPara.End + 1)
                        oSegRng.Bookmarks.Add(xParentSegBmk)

                        If oParentSegBmk.Range.End = lParentSegBmkEnd Then
                            'expanding the segment did not work - this is an issue with Word -
                            'add an extra paragraph to end of the segment
                            oParentSegBmk.Range.InsertParagraphAfter()

                            'rebookmark - bookmark should expand
                            oSegRng.Bookmarks.Add(xParentSegBmk)
                        End If
                    End If
                End If

                'shrink range if at end of segment or end of cell -
                'this is necessary to prevent insertion range from getting set
                'to the entire segment or cell
                If rngSegment.Paragraphs.Last.Range.End <= rngPara.End Then
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                ElseIf bParaScopeInCell Then
                    If rngPara.End = rngPara.Cells(1).Range.End Then _
                        rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                End If
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                'put in previous paragraph
                rngPara = rngScope.Previous(Word.WdUnits.wdParagraph)
                If rngPara.Information(Word.WdInformation.wdWithInTable) Then _
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
            Else
                'put in current paragraph
                rngPara = rngScope.Paragraphs(1).Range
            End If

            'if target paragraph contains a block/blockable tag or tags,
            'put mDels inside the inner one
            For i = 1 To rngPara.XMLNodes.Count
                bSkip = False
                oTag = rngPara.XMLNodes(i)
                If (oTag.BaseName <> "mDel") And oWordDoc.TagIsBlockable(oTag) Then
                    'change range from the paragraph to the tag, unless this is the last
                    'paragraph of a multipara tag with the scope specified to be reinserted
                    'before or the first paragraph of a multipara tag with the scope specified
                    'to be reinserted after
                    rngTag = oTag.Range
                    If InStr(rngTag.Text, vbCr) > 0 Then
                        If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                            bSkip = (rngTag.Paragraphs.Last.Range.End <= _
                                rngPara.Paragraphs(1).Range.End)
                        Else
                            bSkip = (rngTag.Paragraphs.First.Range.Start >= _
                                rngPara.Paragraphs(1).Range.Start)
                        End If
                    End If

                    If Not bSkip Then _
                            rngLocation = rngTag
                Else
                    Exit For
                End If
            Next i

            'if no block tag, put at start/end of para
            If rngLocation Is Nothing Then
                rngLocation = rngPara.Duplicate
                If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then _
                    rngLocation.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'assign a temp id to target tag
            xTempID = oTags.AddTempID(oTargetNode)

            '10.2 - create tag for new mDel
            xTag = "mpd" & BaseMethods.GenerateDocVarID()

            'append object database id
            xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
            xTag = xTag & xObjectDBID & BaseMethods.CreatePadString(22, "0")

            'get attributes of new mDel
            bNativeToTable = ((iScope > mpDeleteScopes.mpDeleteScope_Paragraph) Or bParaScopeInCell)
            oBlockTag = oTargetNode.SelectSingleNode("ancestor::x:mBlock[1]", _
                "xmlns:x='" & GlobalMethods.mpNamespace & "'")
            If Not oBlockTag Is Nothing Then _
                xParentBlock = oBlockTag.SelectSingleNode("@TagID").NodeValue
            xSegmentTagID = BaseMethods.GetAttributeValue(xSegmentTag, "TagID", oDoc)
            xObjectData = xSegmentTagID & "|" & iReinsertionLocation & "|" & CStr(Math.Abs(CInt(bNativeToTable))) & _
                "|" & xParentBlock
            '10.2 - Added Tag to properties to restore
            xTargetProps = xVariableName & "|" & xObjectData & "|" & xTempID & "|" & xTag

            '10.2 - create doc var and set attributes
            BaseMethods.SetAttributeValue(xTag, "TagID", xVariableName, "", oDoc)
            BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDoc)

            'get attributes of existing mDels in scope, inserting attributes of new mDel
            'at correct location in the string
            rngmDels = rngScope.Duplicate
            If Right$(rngmDels.Text, 1) = Chr(7) Then
                rngmDels.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If
            iCount = rngmDels.XMLNodes.Count
            For i = 1 To iCount
                oTag = rngmDels.XMLNodes(i)
                If oTag.BaseName = "mDel" Then
                    'assign a temp id to existing mDel
                    xTempID = oTags.AddTempID(oTag)

                    'get properties
                    oTagID = oTag.SelectSingleNode("@TagID")
                    oObjectData = oTag.SelectSingleNode("@ObjectData")
                    oReserved = oTag.SelectSingleNode("@Reserved")
                    If Not oReserved Is Nothing Then
                        xReserved = oReserved.NodeValue
                    Else
                        xReserved = ""
                    End If
                    oProps = Split(oObjectData.NodeValue, "|")
                    If (Not oTagID Is Nothing) And (Not oObjectData Is Nothing) Then
                        'determine whether mDel is native to block that's getting deleted
                        If UBound(oProps) > 1 Then
                            bIsNativeToBlock = (oProps(3) = xVariableName)
                        Else
                            bIsNativeToBlock = False
                        End If
                        If Not bIsNativeToBlock Then
                            'insert target if existing mDel is beyond it or if it's
                            'inside it but configured to be inserted after
                            If (Not bTargetIsInString) And _
                                    (oTag.Range.Start > oTargetNode.Range.Start) And _
                                    ((oTag.Range.Start > oTargetNode.Range.End) Or _
                                    (CInt(oProps(1)) = mpReinsertionLocations.mpReinsertionLocation_After)) Then
                                'insert props of new mDel
                                xmDels = xmDels & xTargetProps & "�"
                                bTargetIsInString = True
                            End If

                            'insert props of existing mDel
                            xmDels = xmDels & oTagID.NodeValue & "|" & oProps(0) & "|" & _
                                iReinsertionLocation
                            If UBound(oProps) = 1 Then
                                'mDel was created before table and block fields were added to the app
                                xmDels = xmDels & "|" & CStr(Math.Abs(CInt(bNativeToTable))) & _
                                    "|" & xParentBlock & "|" & xTempID & "|" & xReserved & "�"
                            Else
                                xmDels = xmDels & "|" & oProps(2) & "|" & oProps(3) & _
                                    "|" & xTempID & "|" & xReserved & "�"
                            End If
                        End If
                    End If
                End If
            Next i

            If xmDels = "" Then
                'no existing mDels
                xmDels = xTargetProps
            ElseIf Not bTargetIsInString Then
                'add new mDel to end of string
                xmDels = xmDels & xTargetProps
            Else
                'trim trailing separator
                xmDels = Left$(xmDels, Len(xmDels) - 1)
            End If

            'delete existing mDels
            For i = iCount To 1 Step -1
                oTag = rngmDels.XMLNodes(i)
                If oTag.BaseName = "mDel" Then
                    oObjectData = oTag.SelectSingleNode("@ObjectData")
                    oProps = Split(oObjectData.NodeValue, "|")
                    If UBound(oProps) > 1 Then
                        bIsNativeToBlock = (oProps(3) = xVariableName)
                    Else
                        bIsNativeToBlock = False
                    End If
                    If Not bIsNativeToBlock Then _
                        oTag.Delete()
                End If
            Next i

            'insert mDels
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngLocation.StartOf()
            Else
                rngLocation.EndOf()
            End If

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'add mDel - try a second time if necessary,
                'as this line fails on first execution in Word 2007
                On Error GoTo TryAgain
                oTag = rngLocation.XMLNodes.Add("mDel", GlobalMethods.mpNamespace)
                On Error GoTo ProcError

                'set attribute values
                oProps = Split(omDels(i), "|")
                oTagID = oTag.Attributes.Add("TagID", "")
                oTagID.NodeValue = oProps(0)
                oObjectData = oTag.Attributes.Add("ObjectData", "")
                oObjectData.NodeValue = oProps(1) & "|" & oProps(2) & "|" & _
                    oProps(3) & "|" & oProps(4)
                '10.2
                oReserved = oTag.Attributes.Add("Reserved", "")
                oReserved.NodeValue = oProps(6)
                'update tag in collection
                bIsTarget = (omDels(i) = xTargetProps)
                'move to end of this mDel
                rngLocation.Move(Word.WdUnits.wdCharacter, 2)
                If Not oParentSegBmk Is Nothing Then
                    'If mDel is outside of parent bookmark, redefine bookmark range
                    If rngLocation.End > oParentSegBmk.End Then
                        Dim oRngBmk As Word.Range = Nothing
                        oRngBmk = oParentSegBmk.Range.Duplicate
                        oRngBmk.SetRange(oParentSegBmk.Range.Start, rngLocation.End + 1) 'GLOG 6962
                        oRngBmk.Bookmarks.Add(xParentSegBmk)
                    End If
                End If
                oTags.Update(oTag, bIsTarget, oProps(5))
                'Apply hidden bookmark to range
                '        If oProps(6) <> "" Then
                '            oTag.Range.Bookmarks.Add "_" & oProps(6)
                '        End If
            Next i
            '10.2: Make sure Bookmarks of containing nodes remain correct
            '    oParentNode = oTargetNode.ParentNode
            '    While Not oParentNode Is Nothing
            '        oWordDoc.ApplyBookmarkToNode oParentNode
            '        oParentNode = oParentNode.ParentNode
            '    End While

            InsertmDels = iReinsertionLocation

            Exit Function
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function GetmDels(ByVal oRange As Word.Range, _
                                 ByRef oTags As Tags, _
                                 Optional ByVal iReinsertionLocation As Short = -1, _
                                 Optional ByVal bExcludeNativeToTable As Boolean = False, _
                                 Optional ByVal bExcludeNativeToBlock As Boolean = False, _
                                 Optional ByVal bDeleteWhenFound As Boolean = False, _
                                 Optional ByVal xExcludeSegmentTagID As String = "") As String
            'returns a delimited string containing all mDels in oRange that meet
            'the specified criteria; deletes mDel if specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetmDels"
            Dim oTag As Word.XMLNode = Nothing
            Dim oTagID As Word.XMLNode = Nothing
            Dim oObjectData As Word.XMLNode = Nothing
            Dim xmDels As String = ""
            Dim oProps As Object = Nothing
            Dim bNativeToTable As Boolean = False
            Dim xTempID As String = ""
            Dim bInclude As Boolean = False
            Dim xExcludeSegmentIDs As String = ""
            Dim xElement As String = ""
            Dim oReserved As Word.XMLNode = Nothing
            Dim xReserved As String = ""
            Dim oBmk As Word.Bookmark = Nothing 'GLOG 6858
            Dim xTagID As String = "" 'GLOG 6858
            Dim lStart As Integer = 0 'GLOG 6858
            Dim lEnd As Integer = 0 'GLOG 6858
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim bShowHidden As Boolean = False 'GLOG 8722

            On Error GoTo ProcError

            xExcludeSegmentIDs = xExcludeSegmentTagID

            'GLOG 6858 (dm) - add child segment bookmarks to exclusion list
            lStart = oRange.Start

            lEnd = oRange.End
            'GLOG 8722
            bShowHidden = oRange.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oRange.Bookmarks.ShowHidden = True
            End If
            If xExcludeSegmentTagID <> "" Then
                For Each oBmk In oRange.Bookmarks
                    If oBmk.Name Like "_mps*" Then
                        If (oBmk.Range.Start >= lStart) And (oBmk.Range.End <= lEnd) Then
                            xTagID = BaseMethods.GetAttributeValue(Mid$(oBmk.Name, 2), "TagID")
                            If InStr(xExcludeSegmentIDs, xTagID) = 0 Then
                                xExcludeSegmentIDs = xExcludeSegmentIDs & "|" & xTagID
                            End If
                        End If
                    End If
                Next oBmk
            End If

            For Each oTag In oRange.XMLNodes
                xElement = oTag.BaseName
                If xElement = "mDel" Then

                    'get properties
                    oTagID = oTag.SelectSingleNode("@TagID")
                    oObjectData = oTag.SelectSingleNode("@ObjectData")
                    '10.2 - Add Reserved attribute to properties being stored
                    oReserved = oTag.SelectSingleNode("@Reserved")
                    If Not oReserved Is Nothing Then
                        xReserved = oReserved.NodeValue
                    Else
                        xReserved = ""
                    End If
                    If (Not oTagID Is Nothing) And (Not oObjectData Is Nothing) Then
                        oProps = Split(oObjectData.NodeValue, "|")
                        If UBound(oProps) > 1 Then _
                            bNativeToTable = (oProps(2) = "1")
                        bInclude = (((iReinsertionLocation = -1) Or _
                                (iReinsertionLocation = CInt(oProps(1)))) And _
                                Not (bExcludeNativeToTable And bNativeToTable) And _
                                InStr(xExcludeSegmentIDs, oProps(0)) = 0)
                        If bInclude Then
                            'GLOG 4377: Only assign TempID if mDel is being preserved
                            'assign a temp id
                            xTempID = oTags.AddTempID(oTag)
                            'TODO: implement bExcludeNativeToBlock
                            xmDels = xmDels & oTagID.NodeValue & "|" & _
                                oObjectData.NodeValue & "|" & xTempID & "|" & xReserved & "�"
                            If bDeleteWhenFound Then
                                'GLOG 7389 (dm) - disable track changes while deleting mDel
                                bTrackChanges = oRange.Document.TrackRevisions
                                If bTrackChanges Then _
                                    oRange.Document.TrackRevisions = False

                                oTag.Delete()

                                'GLOG 7389 (dm)
                                If bTrackChanges Then _
                                    oRange.Document.TrackRevisions = True
                            End If

                            '7/20/11 (dm) - add doc var to store
                            If xReserved <> "" Then _
                                BaseMethods.AddDocVarToStore("mpo" & Mid$(xReserved, 4, 8))
                        End If
                    End If
                ElseIf (xElement = "mSEG") And (xExcludeSegmentTagID <> "") Then
                    '12/26/08 - if we're excluding mDels belonging to a specified segment, we can
                    'assume that we should also exclude mDels belonging to other segments in the range -
                    'this is necessary to ensure that mDels belonging to children get deleted w/parent
                    xExcludeSegmentIDs = xExcludeSegmentIDs & "|" & _
                        oTag.SelectSingleNode("@TagID").NodeValue
                End If
            Next oTag
            If xmDels <> "" Then _
                xmDels = Left$(xmDels, Len(xmDels) - 1)
            'GLOG 8722
            oRange.Bookmarks.ShowHidden = bShowHidden

            GetmDels = xmDels

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub MoveAdjacentmDels(ByVal rngmDel As Word.Range, _
                                     ByVal rngmVar As Word.Range, _
                                     ByVal iScope As mpDeleteScopes, _
                                     ByRef oTags As Tags, _
                                     Optional bContentControls As Boolean = False)
            'preserves the sequence of the variables in the document by moving
            'other mDels in the same paragraph as the target mDel if necessary
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.MoveAdjacentmDels"
            Dim iAttempts As Short = 0
            Dim xmDels As String = ""
            Dim i As Short = 0
            Dim rngPara As Word.Range = Nothing
            Dim rngLocation As Word.Range = Nothing
            Dim oWordDoc As WordDoc
            Dim oTag As Word.XMLNode = Nothing
            Dim omDels As Object = Nothing
            Dim oTagID As Word.XMLNode = Nothing
            Dim oObjectData As Word.XMLNode = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim rngScope As Word.Range = Nothing
            Dim xObjectData As String = ""
            Dim xTempID As String = ""
            Dim oProps As Object = Nothing
            Dim oCC As Word.ContentControl
            Dim xTag As String = ""
            Dim oReserved As Word.XMLNode = Nothing
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim bShowHidden As Boolean = False 'GLOG 8722

            On Error GoTo ProcError

            'exit if not an "expanded" delete scope
            If iScope < mpDeleteScopes.mpDeleteScope_Paragraph Then Exit Sub

            'validate scopes which require a table
            If (iScope > mpDeleteScopes.mpDeleteScope_Paragraph) And _
                (Not rngmVar.Information(Word.WdInformation.wdWithInTable)) Then Exit Sub

            'get reinsertion location
            If rngmDel.Start > rngmVar.Start Then
                iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before
            Else
                iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After
            End If

            'get other mDels in paragraph that need to be moved
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngmDel.MoveStart(Word.WdUnits.wdParagraph, -1)
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                rngmDel.MoveEnd(Word.WdUnits.wdParagraph)
            End If

            If bContentControls Then
                'content controls
                xmDels = GetmDels_CC(rngmDel, oTags, iReinsertionLocation)
            Else
                'xml tags
                xmDels = GetmDels(rngmDel, oTags, iReinsertionLocation)
            End If

            'exit if no other mDels to move
            If xmDels = "" Then Exit Sub

            'GLOG 7389 (dm) - disable track changes while deleting mDel
            bTrackChanges = rngmDel.Document.TrackRevisions
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = False

            'GLOG 8722
            bShowHidden = rngmDel.Bookmarks.ShowHidden
            If Not bShowHidden Then
                rngmDel.Bookmarks.ShowHidden = True
            End If
            'delete mDels from current location
            If bContentControls Then
                'content controls
                For i = rngmDel.ContentControls.Count To 1 Step -1
                    oCC = rngmDel.ContentControls(i)
                    If BaseMethods.GetBaseName(oCC) = "mDel" Then _
                        oCC.Delete()
                Next i
            Else
                'xml tags
                For i = rngmDel.XMLNodes.Count To 1 Step -1
                    With rngmDel.XMLNodes(i)
                        If .BaseName = "mDel" Then
                            'delete associated bookmark (1/5/11, dm)
                            oReserved = .SelectSingleNode("@Reserved")
                            If Not oReserved Is Nothing Then
                                xTag = oReserved.NodeValue
                                On Error Resume Next
                                rngmDel.Document.Bookmarks("_" & xTag).Delete()
                                On Error GoTo ProcError
                            End If

                            .Delete()
                        End If
                    End With
                Next i
            End If
            'GLOG 8722
            rngmDel.Bookmarks.ShowHidden = bShowHidden
            'GLOG 7389 (dm)
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = True

            'set insertion location
            Select Case iScope
                Case mpDeleteScopes.mpDeleteScope_Paragraph
                    rngScope = rngmVar.Paragraphs(1).Range
                Case mpDeleteScopes.mpDeleteScope_Cell
                    rngScope = rngmVar.Cells(1).Range
                Case mpDeleteScopes.mpDeleteScope_CellAndPreviousCell
                    rngScope = rngmVar.Cells(1).Range
                    rngScope.MoveStart(WdUnits.wdCell)
                Case mpDeleteScopes.mpDeleteScope_Row
                    rngScope = rngmVar.Rows(1).Range
                Case mpDeleteScopes.mpDeleteScope_Table
                    rngScope = rngmVar.Tables(1).Range
            End Select
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngPara = rngScope.Paragraphs.First.Range
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                rngPara = rngScope.Paragraphs.Last.Range

                'adjust for end of cell
                If Right$(rngPara.Text, 1) = Chr(7) Then _
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'if target paragraph contains a block/blockable tag or tags,
            'put mDels inside the inner one
            oWordDoc = New WordDoc
            If bContentControls Then
                'content controls
                For i = 1 To rngPara.ContentControls.Count
                    oCC = rngPara.ContentControls(i)
                    If (BaseMethods.GetBaseName(oCC) <> "mDel") And _
                            Not BaseMethods.ContentControlIsInline(oCC) Then
                        rngLocation = oCC.Range
                    Else
                        Exit For
                    End If
                Next i
            Else
                'xml tags
                For i = 1 To rngPara.XMLNodes.Count
                    oTag = rngPara.XMLNodes(i)
                    If (oTag.BaseName <> "mDel") And oWordDoc.TagIsBlockable(oTag) Then
                        rngLocation = oTag.Range
                    Else
                        Exit For
                    End If
                Next i
            End If

            'if no block tag, put at start/end of para
            If rngLocation Is Nothing Then _
                    rngLocation = rngPara.Duplicate

            Dim oParentSegBmk As Word.Bookmark = Nothing
            Dim oNewRng As Word.Range = Nothing
            Dim oTempBmk As Word.Bookmark = Nothing

            'insert mDels
            omDels = Split(xmDels, "�")
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngLocation.StartOf()
            Else
                'get parent segment
                oParentSegBmk = oWordDoc.GetParentSegmentBookmarkFromRange(rngLocation)

                'move to insertion location
                rngLocation.EndOf()

                'if insertion location is at the end of or beyond
                'the parent segment,ensure that ccs will get inserted
                'within the bounds of the parent segment
                If rngLocation.End >= oParentSegBmk.End Then
                    'expand insertion range
                    rngLocation.InsertAfter("a")

                    'mark end of range for later deletion
                    rngLocation.EndOf()
                    oTempBmk = rngLocation.Bookmarks.Add("mpTempBmk")

                    'redefine the parent segment bookmark
                    'to include the expanded range
                    oParentSegBmk.End = rngLocation.End

                    'move the insertion location before the expanded range -
                    'the expanded range will get deleted below
                    rngLocation.Move(Word.WdUnits.wdCharacter, -1)
                End If
            End If

            For i = 0 To UBound(omDels)
                If bContentControls Then
                    'add content control
                    oCC = rngLocation.ContentControls.Add
                    oCC.SetPlaceholderText(, , "")

                    'set tag
                    xTag = omDels(i)
                    oCC.Tag = xTag

                    'add bookmark
                    '            oCC.Range.Bookmarks.Add "_" & xTag

                    'update tag in collection
                    xTempID = BaseMethods.GetAttributeValue(xTag, "TempID", rngLocation.Document)
                    oTags.Update_CC(oCC, , xTempID)
                Else
                    'add mDel - try a second time if necessary,
                    'as this line fails on first execution in Word 2007
                    On Error GoTo TryAgain
                    oTag = rngLocation.XMLNodes.Add("mDel", GlobalMethods.mpNamespace)
                    On Error GoTo ProcError

                    'set attribute values
                    oProps = Split(omDels(i), "|")
                    oTagID = oTag.Attributes.Add("TagID", "")
                    oTagID.NodeValue = oProps(0)
                    oObjectData = oTag.Attributes.Add("ObjectData", "")
                    xObjectData = oProps(1) & "|" & oProps(2)
                    If UBound(oProps) > 4 Then
                        xObjectData = xObjectData & "|" & oProps(3) & "|" & oProps(4)
                        xTempID = oProps(5)
                        xTag = oProps(6)
                    Else
                        xTempID = oProps(3)
                        xTag = oProps(4)
                    End If
                    '10.2 - Create Reserved attribute with Tag
                    oObjectData.NodeValue = xObjectData
                    oReserved = oTag.Attributes.Add("Reserved", "")
                    oReserved.NodeValue = xTag

                    'add bookmark
                    '            oTag.Range.Bookmarks.Add "_" & xTag

                    'update tag in collection
                    oTags.Update(oTag, , xTempID)
                End If

                'move to end of this mDel
                rngLocation.Move(Word.WdUnits.wdCharacter, 2)
            Next i

            If Not oTempBmk Is Nothing Then
                'move to end of expanded range
                oNewRng = oTempBmk.Range

                'reference the expanded range
                oNewRng.MoveStart(Word.WdUnits.wdCharacter, -1)

                'delete the expanded range and its
                'associated bookmark
                oNewRng.Delete()
                oTempBmk.Delete()
            End If

            Exit Sub
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function GetmDelInsertionLocation(ByVal oPara As Word.Paragraph, _
                ByVal iReinsertionLocation As mpReinsertionLocations, _
                ByVal bSeekContentControls As Boolean) As Word.Range
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetmDelInsertionLocation"
            Dim rngSegment As Word.Range = Nothing
            Dim rngPara As Word.Range = Nothing
            Dim oWordDoc As WordDoc
            Dim rngLocation As Word.Range = Nothing
            Dim oTag As Word.XMLNode = Nothing
            Dim rngTag As Word.Range = Nothing
            Dim bSkip As Boolean = False
            Dim i As Short = 0
            Dim rngStart As Word.Range = Nothing
            Dim oCC As Word.ContentControl
            Dim bContentControls As Boolean = False

            On Error GoTo ProcError

            oWordDoc = New WordDoc
            rngPara = oPara.Range

            'shrink range if at end of segment or end of cell -
            'this is necessary to prevent insertion range from getting set
            'to the entire segment or cell
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngStart = rngPara.Document.Range(rngPara.Start, rngPara.Start)
                If bSeekContentControls Then
                    On Error Resume Next
                    rngSegment = oWordDoc.GetParentSegmentContentControl(rngStart).Range
                    On Error GoTo ProcError
                    If rngSegment Is Nothing Then
                        rngSegment = oWordDoc.GetParentSegmentBookmarkFromRange(rngStart).Range
                    End If
                Else
                    On Error Resume Next
                    rngSegment = oWordDoc.GetParentSegmentTag(rngStart).Range
                    On Error GoTo ProcError
                    If rngSegment Is Nothing Then
                        rngSegment = oWordDoc.GetParentSegmentBookmarkFromRange(rngStart).Range
                    End If
                End If
                If rngSegment.Paragraphs.Last.Range.End <= rngPara.End Then
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                ElseIf rngPara.Information(Word.WdInformation.wdWithInTable) Then
                    If rngPara.End = rngPara.Cells(1).Range.End Then _
                        rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                End If
            Else
                If rngPara.Information(Word.WdInformation.wdWithInTable) Then _
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'if target paragraph contains a block/blockable tag or tags,
            'put mDels inside the inner one
            If bSeekContentControls Then
                'content controls
                For i = 1 To rngPara.ContentControls.Count
                    bSkip = False
                    oCC = rngPara.ContentControls(i)
                    If BaseMethods.GetBaseName(oCC) <> "mDel" Then
                        If Not BaseMethods.ContentControlIsInline(oCC) Then
                            'change range from the paragraph to the tag, unless this is the last
                            'paragraph of a multipara tag with the scope specified to be reinserted
                            'before or the first paragraph of a multipara tag with the scope specified
                            'to be reinserted after
                            rngTag = oCC.Range
                            If InStr(rngTag.Text, vbCr) > 0 Then
                                If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                                    bSkip = (rngTag.Paragraphs.Last.Range.End <= _
                                        rngPara.Paragraphs(1).Range.End)
                                Else
                                    bSkip = (rngTag.Paragraphs.First.Range.Start >= _
                                        rngPara.Paragraphs(1).Range.Start)
                                End If
                            End If

                            If Not bSkip Then _
                                    rngLocation = rngTag
                        Else
                            Exit For
                        End If
                    Else
                        Exit For
                    End If
                Next i
            Else
                'xml tags
                For i = 1 To rngPara.XMLNodes.Count
                    bSkip = False
                    oTag = rngPara.XMLNodes(i)
                    If oTag.BaseName <> "mDel" Then
                        If oWordDoc.TagIsBlockable(oTag) Then
                            'change range from the paragraph to the tag, unless this is the last
                            'paragraph of a multipara tag with the scope specified to be reinserted
                            'before or the first paragraph of a multipara tag with the scope specified
                            'to be reinserted after
                            rngTag = oTag.Range
                            If InStr(rngTag.Text, vbCr) > 0 Then
                                If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                                    bSkip = (rngTag.Paragraphs.Last.Range.End <= _
                                        rngPara.Paragraphs(1).Range.End)
                                Else
                                    bSkip = (rngTag.Paragraphs.First.Range.Start >= _
                                        rngPara.Paragraphs(1).Range.Start)
                                End If
                            End If

                            If Not bSkip Then _
                                    rngLocation = rngTag
                        Else
                            Exit For
                        End If
                    Else
                        Exit For
                    End If
                Next i
            End If

            'if no block tag, put at start/end of para
            If rngLocation Is Nothing Then
                rngLocation = rngPara.Duplicate
                If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then _
                    rngLocation.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngLocation.StartOf()
            Else
                rngLocation.EndOf()
            End If

            GetmDelInsertionLocation = rngLocation

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function RestoreScope(ByVal oDelNode As Word.XMLNode, _
                                      ByVal oSegmentBoundingObject As Object, _
                                      ByVal xVariableName As String, _
                                      ByVal bAsBlock As Boolean, _
                                      ByVal iScope As mpDeleteScopes, _
                                      ByRef xXML As String, _
                                      ByRef xVariablesAdded As String, _
                                      ByRef oTags As Tags, _
                                      ByVal xBodyXML As String, _
                                      ByRef xRefBkmk As String) As Word.XMLNode
            '- restores deletion scope represented by omDelNode and returns the primary mVar
            '- for performance, only inserts xml once per segment part, using copy/paste for
            'subsequent associated tags
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoreScope"
            Dim oObjectData As Word.XMLNode = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim oProps As Object = Nothing
            Dim bParaScopeInTable As Boolean = False
            Dim bNativeToTable As Boolean = False
            Dim oLocation As Word.Range = Nothing
            Dim rngmSEG As Word.Range = Nothing
            Dim lStart As Integer = 0
            Dim lEnd As Integer = 0
            Dim bMoveFromTable As Boolean = False
            Dim oRow As Word.Row
            Dim oTable As Word.Table
            Dim oWordXMLNode As Word.XMLNode = Nothing
            Dim bIsLastPara As Boolean = False
            Dim bIsEndOfCell As Boolean = False
            Dim xElement As String = ""
            Dim oInsertedNode As Word.XMLNode = Nothing
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim xTempID As String = ""
            Dim lParaCount As Integer = 0
            Dim lXMLParaCount As Integer = 0
            Dim oTestNode As Word.XMLNode = Nothing
            Dim lErr As Integer = 0
            Dim xTagPrefixID As String = ""
            Dim xObjectData As String = ""
            Dim xPassword As String = ""
            Dim oBlockNode As Word.XMLNode = Nothing
            Dim bStartOfOwnerBlock As Boolean = False
            Dim oParentNode As Word.XMLNode = Nothing
            Dim bIsInmVar As Boolean = False
            Dim xTag As String = "" '10.2
            Dim xDelTag As String = ""
            Dim oDoc As Word.Document
            Dim ocWordDoc As WordDoc
            Dim oConvert As Conversion
            Dim bOpenXml As Boolean = False
            Dim oBmk As Word.Bookmark = Nothing
            Dim xName As String = ""
            Dim bEndOfTextbox As Boolean = False
            Dim oSegmentNode As Word.XMLNode = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim xBoundingObjectTag As String = ""
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim bShowHidden As Boolean = False 'GLOG 8722
            Dim bSegEndsWithTable As Boolean = False 'GLOG 8875

            ocWordDoc = New WordDoc

            On Error Resume Next
            oSegmentNode = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentNode.Range
                xBoundingObjectTag = BaseMethods.GetTag(oSegmentNode)
            End If

            On Error GoTo ProcError

            oConvert = New Conversion
            rngmDel = oDelNode.Range
            oDoc = rngmDel.Document

            '1/21/11 (dm)
            bOpenXml = BaseMethods.IsWordOpenXML(xXML)

            'GLOG 3977 (dm) - determine whether mDel is inside an mVar
            oParentNode = oDelNode.ParentNode
            If Not oParentNode Is Nothing Then _
                bIsInmVar = (oParentNode.BaseName = "mVar")

            'get reinsertion location
            oObjectData = oDelNode.SelectSingleNode("@ObjectData")
            oProps = Split(oObjectData.NodeValue, "|")
            iReinsertionLocation = CInt(oProps(1))
            Dim xParentTagID As String = oProps(0) 'GLOG 8875
            bParaScopeInTable = (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) And _
                rngmDel.Information(Word.WdInformation.wdWithInTable)
            'GLOG 8875
            If bParaScopeInTable Then
                bSegEndsWithTable = oSegmentRng.End <= rngmDel.Tables(1).Range.End
            End If
            If UBound(oProps) = 1 Then
                bNativeToTable = ((iScope > mpDeleteScopes.mpDeleteScope_Paragraph) Or bParaScopeInTable)
            Else
                'GLOG 8875: NativeToTable property may not have been set correctly when saving design if in last paragraph of segment, so also check context
                bNativeToTable = CBool(oProps(2)) Or (bParaScopeInTable And bSegEndsWithTable And xParentTagID = WordDoc.GetAttributeValueFromTag(xBoundingObjectTag, "TagID"))
                If oProps(3) <> "" Then
                    'mDel belongs to a block - if it's in the first paragraph of the block,
                    'ensure that the scope is restored inside the block, not above it
                    oBlockNode = oDelNode.SelectSingleNode("ancestor::x:mBlock[@TagID='" & _
                        oProps(3) & "']", "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                    If Not oBlockNode Is Nothing Then
                        bStartOfOwnerBlock = (oBlockNode.Range.Start > _
                            oDelNode.Range.Paragraphs(1).Range.Start)
                    End If
                End If
            End If

            'assign a temp id to tag
            xTempID = oTags.AddTempID(oDelNode)

            'delete doc var and bookmark if they exist
            xDelTag = BaseMethods.GetTag(oDelNode)
            If xDelTag <> "" Then
                ocWordDoc = New WordDoc
                ocWordDoc.DeleteRelatedObjects(xDelTag, oDoc)
            End If

            'GLOG 7120 (dm) - in 10.5, mSEG and mDel may be co-extensive - preserve mSEG
            Dim xBMK As String = ""
            If Not oSegmentBmk Is Nothing Then _
                xBMK = oSegmentBmk.Name

            'GLOG 7389 (dm) - disable track changes while deleting mDel
            bTrackChanges = rngmDel.Document.TrackRevisions
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = False

            'delete tag
            oDelNode.Delete()

            'GLOG 7389 (dm)
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = True
            'GLOG 8722
            bShowHidden = rngmDel.Bookmarks.ShowHidden
            If Not bShowHidden Then
                rngmDel.Bookmarks.ShowHidden = True
            End If
            'GLOG 7120 (dm) - restore mSEG if it's just been deleted
            If xBMK <> "" Then
                If Not oDoc.Bookmarks.Exists(xBMK) Then _
                    rngmDel.Bookmarks.Add(xBMK)
            End If
            'GLOG 8722
            rngmDel.Bookmarks.ShowHidden = bShowHidden

            'set insertion location
            oLocation = rngmDel.Duplicate
            rngmSEG = oSegmentRng
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                lStart = oLocation.Paragraphs(1).Range.Start
                If bParaScopeInTable And Not bNativeToTable Then
                    'non-native mDel in table
                    oTable = oLocation.Tables(1)
                    If oTable.Range.Start = lStart Then
                        'mDel is in first paragraph of table
                        bMoveFromTable = True
                        If rngmSEG.Start > lStart Then
                            'mDel is in first paragraph of mSEG
                            If lStart = 0 Then
                                'start of doc - add a paragraph before the table
                                With oTable.Rows
                                    oRow = .Add(.Item(1))
                                End With
                                oRow.Cells.Merge()
                                oRow.ConvertToText()
                                For Each oWordXMLNode In oLocation.Document _
                                        .Paragraphs(1).Range.XMLNodes
                                    oWordXMLNode.Delete()
                                Next oWordXMLNode
                            End If

                            'move mSEG
                            rngmSEG.SetRange(oTable.Range.Start - 1, rngmSEG.End)
                            If Not oSegmentNode Is Nothing Then
                                oTags.MoveWordTag(oSegmentNode, rngmSEG)
                            Else
                                oTags.MoveBookmark(oSegmentBmk, rngmSEG)
                            End If

                            'set insertion location to before table
                            lStart = oTable.Range.Start
                            oLocation.SetRange(lStart - 1, lStart - 1)
                        Else
                            'set insertion location to before table
                            oLocation.SetRange(lStart - 1, lStart - 1)
                            oLocation.InsertAfter(vbCr)
                            oLocation.EndOf()
                        End If
                    Else
                        oLocation.StartOf(Word.WdUnits.wdParagraph)
                    End If
                ElseIf rngmSEG.Start > lStart Then
                    'mDel is in first paragraph of mSEG
                    oLocation = oSegmentRng
                    oLocation.StartOf()
                ElseIf bStartOfOwnerBlock Then
                    'mDel is in first paragraph of mBlock
                    oLocation = oBlockNode.Range
                    oLocation.StartOf()
                Else
                    'GLOG 3977 (dm) - mDel nested in mVar may no longer be in the mVar's first
                    'paragraph - ensure that the scope is not inserted inside the mVar
                    If bIsInmVar Then _
                            oLocation = oParentNode.Range.Paragraphs(1).Range

                    oLocation.StartOf(Word.WdUnits.wdParagraph)
                    'Move past any page break at start of paragraph
                    'GLOG 8677
                    oLocation.MoveWhile(CStr(Chr(12)))
                End If
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                lEnd = oLocation.Paragraphs(1).Range.End

                'check whether mDel is in last paragraph of cell
                If bParaScopeInTable Then
                    bIsEndOfCell = (lEnd = oLocation.Cells(1).Range.End)
                End If

                'GLOG 7890 (dm) - modify conditional to match CC version
                '        If oSegmentRng.End < lEnd - 1 Then
                If oSegmentRng.Paragraphs.Last.Range.End = lEnd Then
                    'mDel is in last paragraph of mSEG - ensure that scope gets
                    'reinserted inside mSEG
                    bIsLastPara = True
                    oLocation = rngmSEG
                    oLocation.EndOf()
                    oLocation.InsertAfter(vbCr)
                    oLocation.EndOf()
                Else
                    If bIsEndOfCell Then
                        'mDel is in last paragraph of cell
                        If bNativeToTable Or _
                                (lEnd < oLocation.Tables(1).Range.End - 1) Then
                            'mDel is native to table or not in last cell
                            'Need to use InsertAfter, because InsertParagraphAfter will
                            'place return before any tags at end of paragraph in Word 2003
                            oLocation.Paragraphs(1).Range.InsertAfter(vbCr)
                            oLocation.EndOf(Word.WdUnits.wdParagraph)
                        Else
                            'non-native - insert after table
                            bMoveFromTable = True
                            oLocation.SetRange(lEnd + 1, lEnd + 1)
                        End If
                    ElseIf bIsInmVar Then
                        'GLOG 3977 (dm) - mDel nested in mVar may no longer be in the mVar's last
                        'paragraph - ensure that the scope is not inserted inside the mVar
                        oLocation = oParentNode.Range.Paragraphs.Last.Range
                        oLocation.EndOf(Word.WdUnits.wdParagraph)
                    Else
                        oLocation.EndOf(Word.WdUnits.wdParagraph)
                    End If
                End If
            End If

            'GLOG 3497 - prevent inadvertently embedded mpNewSegment bookmark from
            'adding/moving bookmark when xml is inserted
            xXML = Replace(xXML, "w:name=" & Chr(34) & "mpNewSegment" & Chr(34), _
                "w:name=" & Chr(34) & "mpNewSegmentEmbedded" & Chr(34))

            With oLocation
                'if scope is cell or row, and the table no longer exists,
                'reinsert as table
                If (iScope = mpDeleteScopes.mpDeleteScope_Cell Or _
                        iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Or _
                        iScope = mpDeleteScopes.mpDeleteScope_Row) And _
                        (Not .Information(Word.WdInformation.wdWithInTable)) Then
                    iScope = mpDeleteScopes.mpDeleteScope_Table
                End If

                Select Case iScope
                    Case mpDeleteScopes.mpDeleteScope_Paragraph
                        If xXML <> "" Then
                            'get count of existing paras in mSEG
                            'GLOG 3897: If Deleted Scope is last paragraph in mSEG, rngmSEG won't
                            'include the new paragraph(s) after restoring.  Count Paragraphs in mSEG range instead
                            lParaCount = rngmSEG.Paragraphs.Count
                            Dim oWordDoc As WordDoc
                            oWordDoc = New WordDoc

                            'insert
                            oWordDoc.InsertXMLAtLocation(oLocation, xXML)

                            '.InsertXML xXML

                            'delete trailing para if necessary
                            If bIsLastPara Then
                                If .StoryType = wdStoryType.wdTextFrameStory Then
                                    '1/25/11 (dm)
                                    bEndOfTextbox = True
                                Else
                                    rngmSEG.Characters.Last.Delete()
                                End If
                            ElseIf bIsEndOfCell And Not bMoveFromTable Then
                                'delete trailing paragraph
                                .Cells(1).Range.Characters.Last.Previous.Delete()
                            ElseIf (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before) And _
                                    bMoveFromTable Then
                                rngmDel.Tables(1).Range.Previous(Word.WdUnits.wdCharacter).Delete()
                            End If

                            'expand to scope
                            .Expand(Word.WdUnits.wdParagraph)

                            'there are other circumstances in which an extra paragraph mark will
                            'be inserted (see GLOG item 1685) - if next paragraph is empty, ensure
                            'that number of added paras matches the number of paras in the xml
                            Dim oChar As Range
                            oChar = .Next(Word.WdUnits.wdCharacter, 1)

                            If Not oChar Is Nothing Then
                                If oChar.Text = vbCr Then
                                    'get number of paragraphs that should be added
                                    If xBodyXML <> "" Then
                                        'added branch to support open xml
                                        lXMLParaCount = BaseMethods.countchrs(xBodyXML, GlobalMethods.mpEndParaTag) + _
                                            BaseMethods.countchrs(xXML, GlobalMethods.mpEmptyParaTag) - 1
                                    Else
                                        lXMLParaCount = BaseMethods.countchrs(xXML, GlobalMethods.mpEndParaTag) + _
                                            BaseMethods.countchrs(xXML, GlobalMethods.mpEmptyParaTag) - 1
                                    End If
                                    If (.ListFormat.ListType <> wdListType.wdListNoNumbering) And _
                                            (GlobalMethods.CurWordApp.Version <> "11.0") Then
                                        'numbered paragraph in Word 2007
                                        oChar.Delete()
                                        'GLOG 3897: Compare paragraph count in mSEG to original count
                                    ElseIf rngmSEG.Paragraphs.Count - lParaCount > _
                                            lXMLParaCount Then
                                        'catch-all
                                        oChar.Delete()
                                    End If
                                End If
                            End If

                            'ensure that entire cell isn't copied
                            If bIsEndOfCell And Not bIsLastPara Then _
                                .MoveEnd(Word.WdUnits.wdCharacter, -1)

                            'copy
                            On Error Resume Next
                            .Copy()
                            lErr = Err.Number
                            On Error GoTo ProcError
                            If lErr = 4198 Then
                                'intermittent "command not available" error when
                                'copying range in Word 2003
                                .Select()
                                GlobalMethods.CurWordApp.Selection.Copy()
                            End If

                            'clear
                            'GLOG 6786 (dm) - don't do for blocks because this is
                            'coded to only copy a single paragraph
                            If Not bAsBlock Then
                                xXML = ""
                            End If
                        Else
                            .Paste()

                            '1/26/11 (dm) - rebookmark for paragraph level tag
                            If xRefBkmk <> "" Then _
                                .Bookmarks.Add(xRefBkmk)
                        End If
                    Case mpDeleteScopes.mpDeleteScope_Cell, mpDeleteScopes.mpDeleteScope_CellAndPreviousCell
                        'TODO: use copy/paste for multiple tags ala other scopes
                        Restore_Cells(oLocation, xXML, iScope, False, oTags)
                    Case mpDeleteScopes.mpDeleteScope_Row
                        Restore_Row(oLocation, xXML)
                        xXML = ""
                    Case mpDeleteScopes.mpDeleteScope_Table
                        If xXML <> "" Then
                            'insert xml
                            .InsertXML(xXML)

                            'clear
                            xXML = ""

                            'delete trailing para
                            .Tables(1).Range.Next(Word.WdUnits.wdCharacter).Delete()

                            'expand to scope
                            .Expand(Word.WdUnits.wdTable)

                            'copy
                            .Copy()
                        Else
                            'paste
                            .Paste()
                        End If
                End Select

                'GLOG 3497 - delete embedded bookmark
                If .Document.Bookmarks.Exists("mpNewSegmentEmbedded") Then _
                    .Document.Bookmarks("mpNewSegmentEmbedded").Delete()
            End With

            'add tags if necessary
            If (iScope <> mpDeleteScopes.mpDeleteScope_Cell) And (iScope <> mpDeleteScopes.mpDeleteScope_CellAndPreviousCell) Then
                '1/22/11 (dm) - the code above to expand to the insertion range immediately
                'after xml insertion has never worked properly for multiparagraph blocks -
                'this now prevents the inserted block from getting tagged - I'm putting the
                'code here because I prefer a more targeted fix for the moment
                If bAsBlock Then
                    bShowHidden = oLocation.Bookmarks.ShowHidden
                    oLocation.Bookmarks.ShowHidden = True
                    For i = 1 To oLocation.Bookmarks.Count
                        oBmk = oLocation.Bookmarks(i)
                        xName = oBmk.Name
                        If Left$(xName, 4) = "_mpb" Then
                            If BaseMethods.GetAttributeValue(Mid$(xName, 2), "TagID", oDoc) = _
                                    xVariableName Then
                                Exit For
                            End If
                        End If
                    Next i
                    oLocation = oBmk.Range
                    oLocation.Expand(Word.WdUnits.wdParagraph)
                    oLocation.Bookmarks.ShowHidden = bShowHidden
                End If

                '12/14/10 (dm) - shrink range to exclude mSEG if necessary, so that it's not
                'considered in determining whether we need to add tags
                rngmSEG = oSegmentRng
                If Not oSegmentNode Is Nothing Then
                    'GLOG 6849: Use range of parent node if it exists
                    rngmSEG = oSegmentNode.Range
                    If oLocation.Start < rngmSEG.Start Then
                        oLocation.SetRange(rngmSEG.Start, oLocation.End)
                    ElseIf oLocation.End > rngmSEG.End Then
                        '2/3/11 (dm) - we need to check end as well
                        oLocation.SetRange(oLocation.Start, rngmSEG.End)
                    End If
                End If

                'add tags
                oConvert.AddTagsToBookmarkedRangeIfNecessary(oLocation)

                'delete content controls if necessary
                If bOpenXml Then _
                    oConvert.DeleteContentControlsInRange(oLocation, False)
            End If

            'get new mVar/mBlock
            If bAsBlock Then
                xElement = "mBlock"
            Else
                xElement = "mVar"
            End If
            For i = 1 To oLocation.XMLNodes.Count
                oInsertedNode = oLocation.XMLNodes(i)
                If oInsertedNode.BaseName = xElement Then
                    If oInsertedNode.SelectSingleNode("@TagID").NodeValue = _
                            xVariableName Then
                        Exit For
                    End If
                End If
            Next i

            '1/25/11 (dm)
            If bEndOfTextbox Then
                oInsertedNode.Range.Next.Delete()
            End If

            '1/26/11 (dm) - if the primary tag is paragraph level, its bookmark will not be
            'copied to the clipboard with the scope - this will be a problem for subsequent
            'associated tags - store the bookmark name for rebookmarking after paste
            If (oInsertedNode.Level = WdXMLNodeLevel.wdXMLNodeLevelParagraph And (xRefBkmk = "")) Then
                xRefBkmk = "_" & BaseMethods.GetTag(oInsertedNode)
            End If

            '10.2: Generate new Tag and Doc Vars for inserted Node
            'TODO (JTS 6/26/10): Not entirely sure of the purpose for this, but
            'this has been designed to work like the ContentControl equivalent
            BaseMethods.RetagXMLNode(oInsertedNode)

            xTag = BaseMethods.GetTag(oInsertedNode)

            'add inserted mVars to tags collection, getting back list of variables added
            If iScope = mpDeleteScopes.mpDeleteScope_Paragraph Then
                'ensure that we have all of the paragraphs in the scope -
                'with the other scope types, the range is inherent in the type
                oLocation = oInsertedNode.Range
                oLocation.Expand(Word.WdUnits.wdParagraph)
                If Right$(oLocation.Text, 1) = Chr(7) Then _
                    oLocation.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'GLOG 5276 (dm) - ensure that numbered paragraphs conform to style - this
            'is necessary because some paragraph formatting, e.g. spacing, appears to
            'be implicit in the styles xml, so isn't necessarily applied in the insertion
            ConformNumberedParagraphsToStyle(oLocation, xBodyXML)

            If Not oSegmentNode Is Nothing Or Not oSegmentBmk Is Nothing Then
                If Not oSegmentNode Is Nothing Then
                    'get segment node tag prefix id from XML Node Attribute
                    oObjectData = oSegmentNode.SelectSingleNode("@ObjectData")
                    xTagPrefixID = BaseMethods.GetTagPrefixID(BaseMethods.Decrypt(oObjectData.NodeValue, xPassword))
                Else
                    'GLOG 6798: Get ObjectData for Bookmark
                    xTagPrefixID = BaseMethods.GetTagPrefixID(BaseMethods.GetAttributeValue(Mid(oSegmentBmk.Name, 2), "ObjectData", oDoc))
                End If
                'get variables in scope
                GetVariablesInScope(oLocation, xVariableName, False, False, oTags, _
                    xVariablesAdded, xTagPrefixID, xPassword)
            Else
                'get segment tag prefix id from doc variable
                xObjectData = BaseMethods.GetAttributeValue(xBoundingObjectTag, "ObjectData", oDoc)
                xTagPrefixID = BaseMethods.GetTagPrefixID(xObjectData)
            End If

            'recompress object data, adding tag prefix id if necessary
            oObjectData = oInsertedNode.SelectSingleNode("@ObjectData")
            If Not oObjectData Is Nothing Then
                xObjectData = oObjectData.NodeValue
                If (xTagPrefixID <> "") And _
                        (BaseMethods.GetTagPrefixID(xObjectData) = "") Then
                    xObjectData = xTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & xObjectData
                End If
                oObjectData.NodeValue = BaseMethods _
                    .Encrypt(xObjectData, xPassword)
            End If

            '10.2: Update ObjectData in Doc var
            If xTag <> "" Then _
                BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDoc)

            'GLOG 8722
            bShowHidden = oDoc.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oDoc.Bookmarks.ShowHidden = True
            End If
            If Not oSegmentBmk Is Nothing Then
                'If Restored range is not longer inside parent bookmark, extend bookmark range
                If oLocation.End > oSegmentBmk.End Then
                    Dim oRngBmk As Word.Range = Nothing
                    oRngBmk = oSegmentBmk.Range.Duplicate
                    oRngBmk.SetRange(oSegmentBmk.Range.Start, oLocation.End)
                    oLocation.Document.Bookmarks.Add(oSegmentBmk.Name, oRngBmk)
                End If
            ElseIf oSegmentNode.Range.End > oDoc.Bookmarks("_" & xBoundingObjectTag).End Then
                'GLOG 6849: Make sure Bookmark extends to end of mSEG
                rngmSEG = oSegmentNode.Range.Duplicate
                rngmSEG.Bookmarks.Add("_" & xBoundingObjectTag)
            End If
            'GLOG 8722
            oDoc.Bookmarks.ShowHidden = bShowHidden
            'update tag in collection
            oTags.Update(oInsertedNode, True, xTempID)

            '10.2: Make sure Bookmarks of containing nodes remain correct
            '    oParentNode = oInsertedNode.ParentNode
            '    While Not oParentNode Is Nothing
            '        ocWordDoc.ApplyBookmarkToNode oParentNode
            '        oParentNode = oParentNode.ParentNode
            '    End While

            'return inserted node
            RestoreScope = oInsertedNode

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Restore_Distributed(ByVal oSegmentBoundingObject As Object, _
                                       ByVal xVariableName As String, _
                                       ByVal bAsBlock As Boolean, _
                                       ByVal iScope As mpDeleteScopes, _
                                       ByVal xSegmentXML As String, _
                                       ByRef xVariablesAdded As String, _
                                       ByRef oTags As Tags, _
                                       ByRef rngmDels() As Word.Range, _
                                       ByRef oAssociatedNodes As Object, _
                                       ByRef xValuesArray As Object, _
                                       ByVal bIgnoreDeleteScope As Boolean)
            'reinserts deleted scope for a distributed detail variable, skipping
            'any empty values (to avoid immediate redeletion) - otherwise, this is
            'the same as Restore()
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_Distributed"
            Dim xXML As String = ""
            Dim oAssociatedNode As Word.XMLNode = Nothing
            Dim oInsertedNode As Word.XMLNode = Nothing
            Dim i As Short = 0
            Dim iCount As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim bIsEmpty As Boolean = False
            Dim xText As String = ""
            Dim oWordDoc As WordDoc
            Dim xBodyXML As String = ""
            Dim xRefBkmk As String = ""
            Dim xDeletedScopes As String = ""
            Dim xPassword As String = ""
            Dim xTag As String = ""
            Dim oSegmentNode As Word.XMLNode = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim xSegmentTag As String = ""
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim xBoundingObjectTag As String = ""
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            oWordDoc = New WordDoc

            On Error Resume Next
            oSegmentNode = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
                oDoc = oSegmentBmk.Range.Document
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentNode.Range
                xBoundingObjectTag = BaseMethods.GetTag(oSegmentNode)
                oDoc = oSegmentRng.Document
            End If


            'store clipboard contents
            Clipboard.SaveClipboard()

            If xVariableName = "" Then
                'raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>")
            End If

            'get insertion xml
            xXML = GetInsertionXML(xBoundingObjectTag, oSegmentRng, xVariableName, xSegmentXML, xBodyXML)

            For i = 0 To UBound(oAssociatedNodes)
                oAssociatedNode = oAssociatedNodes(i)
                If UBound(xValuesArray) < i Then
                    bIsEmpty = True
                Else
                    bIsEmpty = (InStr(xValuesArray(i), "<w:t>") = 0)
                End If
                If (oAssociatedNode.BaseName = "mDel") And _
                        ((Not bIsEmpty) Or bIgnoreDeleteScope) Then
                    If (xXML = "") And (iCount = 0) Then
                        'no xml - raise error
                        Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                            "<Error_MissingDeleteScopeXML>" & xVariableName)
                    End If
                    iCount = iCount + 1

                    'get mDel and clear out text
                    rngmDel = oAssociatedNode.Range
                    rngmDel.Text = ""

                    'add mDel location to byref array
                    rngmDels(i) = rngmDel

                    'restore scope
                    oInsertedNode = RestoreScope(oAssociatedNode, oSegmentBoundingObject, _
                        xVariableName, bAsBlock, iScope, xXML, xVariablesAdded, oTags, _
                        xBodyXML, xRefBkmk)

                    'add restored mVar to byref array
                    oAssociatedNodes(i) = oInsertedNode
                End If
            Next i

            'GLOG 6328 (dm) - if design mode, remove delete scope from mSeg
            If bIgnoreDeleteScope Then
                xDeletedScopes = BaseMethods.Decrypt(oSegmentNode _
                    .SelectSingleNode("@DeletedScopes").NodeValue, xPassword)
                DeleteDefinition(xDeletedScopes, xVariableName)
                oSegmentNode.SelectSingleNode("@DeletedScopes").NodeValue = _
                    BaseMethods.Encrypt(xDeletedScopes, xPassword)

                '10.2: Update DeleteScope in mSEG Doc Variable
                xTag = BaseMethods.GetTag(oSegmentNode)
                If xTag <> "" Then _
                    BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedScopes, "", oDoc)

                'update Tag in tags collection
                oTags.Update(oSegmentNode)
            End If

            'restore clipboard contents
            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function AllowManualDeletion(ByVal omDel As Word.XMLNode, _
                                            ByVal rngSelection As Word.Range) As Boolean
            'returns TRUE if entire context for mDel is currently selected, allowing us
            'to assume that user would want mDel to be deleted if selection is deleted
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.AllowManualDeletion"
            Dim oObjectData As Word.XMLNode = Nothing
            Dim oProps As Object = Nothing
            Dim bNativeToTable As Boolean = False
            Dim oTagID As Word.XMLNode = Nothing
            Dim omSEG As Word.XMLNode = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim rngmDel As Word.Range = Nothing
            Dim rngmSEG As Word.Range = Nothing
            Dim rngCell As Word.Range = Nothing
            Dim rngParagraph As Word.Range = Nothing
            Dim bTagsShowing As Boolean = False
            Dim iCount As Short = 0
            Dim lSelectionStart As Integer = 0
            Dim lSelectionEnd As Integer = 0
            Dim oFirstPara As Word.Paragraph
            Dim oBlockNode As Word.XMLNode = Nothing
            Dim rngmBlock As Word.Range = Nothing
            Dim xBlockTagID As String = ""

            On Error GoTo ProcError

            lSelectionStart = rngSelection.Start
            lSelectionEnd = rngSelection.End
            rngmDel = omDel.Range
            rngParagraph = rngmDel.Paragraphs(1).Range

            bTagsShowing = rngmDel.Document.ActiveWindow.View.ShowXMLMarkup

            If bTagsShowing Then
                '6/3/08 - decided to always allow manual deletion with tags showing
                '        'tags showing - allow if tags mDel is cleanly selected by itself
                '        If (lSelectionStart = rngmDel.Start - 1) And _
                '                (lSelectionEnd = rngmDel.End + 1) Then
                AllowManualDeletion = True
                Exit Function
                '        End If
            Else
                'tags not showing - adjust for tags at start of first paragraph of selection
                oFirstPara = rngSelection.Paragraphs(1)
                iCount = BaseMethods.CountTagsAtStartOfParagraph(oFirstPara)
                If lSelectionStart - oFirstPara.Range.Start <= iCount Then _
                    lSelectionStart = oFirstPara.Range.Start
            End If

            'get reinsertion location and other properties
            oObjectData = omDel.SelectSingleNode("@ObjectData")
            oProps = Split(oObjectData.NodeValue, "|")
            iReinsertionLocation = CInt(oProps(1))
            If UBound(oProps) > 1 Then
                If rngmDel.Tables.Count > 0 Then _
                    bNativeToTable = CBool(oProps(2))
                xBlockTagID = oProps(3)
            End If

            'get parent mSEG
            omSEG = omDel.SelectSingleNode("ancestor::x:mSEG[1]", _
                "xmlns:x='" & GlobalMethods.mpNamespace & "'")
            If Not omSEG Is Nothing Then
                'validate - mDel may be in child of it's true parent
                oTagID = omSEG.SelectSingleNode("@TagID")
                If oTagID.NodeValue <> oProps(0) Then
                    omSEG = omSEG.SelectSingleNode("ancestor::x:mSEG[1]", _
                        "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                End If
            End If

            'allow if parent mSEG is being deleted
            If Not omSEG Is Nothing Then
                rngmSEG = omSEG.Range
                If (lSelectionStart <= rngmSEG.Start) And (lSelectionEnd >= rngmSEG.End) Then
                    AllowManualDeletion = True
                    Exit Function
                End If
            Else
                'GLOG 7813 (dm) - added support for bookmark mSEGs - however, due to an
                'intractable error in Word 2010 when deleting the empty paragraph with
                'the three mDels at the end of letter, I'm currently allowing the
                'deletion when the selection includes the start OR end of the mSEG -
                'when attempting to preserve in this case, the delete key warning
                'was appearing and, if you answered yes, an HResult_E_Fail error
                'occurred - the mDels were getting deleted in any case, even in
                'Word 2013 where there's no keyboard handler
                Dim oBmk As Word.Bookmark = Nothing
                Dim oWordDoc As WordDoc
                oWordDoc = New WordDoc
                oBmk = oWordDoc.GetParentSegmentBookmarkFromRange(omDel.Range)
                If Not oBmk Is Nothing Then
                    rngmSEG = oBmk.Range
                    If (lSelectionStart <= rngmSEG.Start) Or (lSelectionEnd >= rngmSEG.End) Then
                        AllowManualDeletion = True
                        Exit Function
                    End If
                Else
                    AllowManualDeletion = True
                    Exit Function
                End If
            End If

            'allow if mDel belongs to a block and the block is being deleted
            If xBlockTagID <> "" Then
                oBlockNode = omDel.SelectSingleNode("ancestor::x:mBlock[@TagID='" & _
                    xBlockTagID & "']", "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                If Not oBlockNode Is Nothing Then
                    rngmBlock = oBlockNode.Range
                    If (lSelectionStart <= rngmBlock.Start) And _
                            (lSelectionEnd >= rngmBlock.End) Then
                        AllowManualDeletion = True
                        Exit Function
                    End If
                End If
            End If

            'allow if context is being deleted
            If (rngmSEG.End > rngParagraph.End) And ((rngParagraph.Start < rngmSEG.Start) Or _
                    (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After)) Then
                'the mDel is not in the last paragraph of the mSEG and it's either
                '1) in the first paragraph or 2) set for reinsertion in the next paragraph -
                'allow if the selection includes the next paragraph as well
                If lSelectionEnd >= rngmDel.Next(Word.WdUnits.wdParagraph).End Then
                    AllowManualDeletion = True
                    Exit Function
                End If
            ElseIf rngmSEG.Start < rngParagraph.Start Then
                'the mDel is not in the first paragraph of the mSEG -
                'allow if the selection includes the previous paragraph as well
                If lSelectionStart <= rngmDel.Previous(Word.WdUnits.wdParagraph).Start Then
                    AllowManualDeletion = True
                    Exit Function
                End If
            End If

            'allow if cell is being deleted
            If bNativeToTable Then
                rngCell = rngmDel.Cells(1).Range
                AllowManualDeletion = ((lSelectionStart <= rngCell.Start) And _
                    (lSelectionEnd >= rngCell.End))
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub ValidateCopiedmDels(ByRef xDeleted As String, ByRef oTags As Tags, _
                                       ByVal rngSelection As Word.Range, _
                                       ByVal iFormat As mpFileFormats)
            'determines whether to exclude mDels from a copied selection using the same
            'criteria as used to determine whether to allow mDels to be manually deleted -
            'if mDel should be excluded, we store its attributes, delete it, and restore
            'after copying
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.ValidateCopiedmDels"
            Dim i As Short = 0
            Dim oTag As Word.XMLNode = Nothing
            Dim lPos As Integer = 0
            Dim oTagID As Word.XMLNode = Nothing
            Dim oObjectData As Word.XMLNode = Nothing
            Dim oReserved As Word.XMLNode = Nothing
            Dim xTempID As String = ""
            Dim xTag As String = ""
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            'create delimited string of excluded mDels
            If iFormat = mpFileFormats.OpenXml Then
                'content controls - added 10/10/19 (dm)
                For i = rngSelection.ContentControls.Count To 1 Step -1
                    oCC = rngSelection.ContentControls(i)
                    If BaseMethods.GetBaseName(oCC) = "mDel" Then
                        If Not AllowManualDeletion_CC(oCC, rngSelection) Then
                            'get tag and location in doc
                            xTag = oCC.Tag
                            lPos = oCC.Range.Start - 1
                            xDeleted = xTag & "|" & CStr(lPos) & "�" & xDeleted

                            'delete content control
                            oCC.Delete()
                        End If
                    End If
                Next i
            Else
                For i = rngSelection.XMLNodes.Count To 1 Step -1
                    oTag = rngSelection.XMLNodes(i)
                    If oTag.BaseName = "mDel" Then
                        If Not AllowManualDeletion(oTag, rngSelection) Then
                            'assign a temp id
                            If Not oTags Is Nothing Then _
                                xTempID = oTags.AddTempID(oTag)

                            'get properties and location in doc
                            oTagID = oTag.SelectSingleNode("@TagID")
                            oObjectData = oTag.SelectSingleNode("@ObjectData")
                            oReserved = oTag.SelectSingleNode("@Reserved")
                            If Not oReserved Is Nothing Then
                                xTag = oReserved.NodeValue
                            End If
                            If (Not oTagID Is Nothing) And (Not oObjectData Is Nothing) Then
                                'add to start of string
                                lPos = oTag.Range.Start - 1
                                '10.2 - Add Tag from Reserved Attribute to string
                                xDeleted = oTagID.NodeValue & "|" & oObjectData.NodeValue & _
                                    "|" & xTempID & "|" & xTag & "|" & CStr(lPos) & "�" & xDeleted
                                'delete tag
                                oTag.Delete()
                            End If
                        End If
                    End If
                Next i
            End If

            If xDeleted <> "" Then _
                xDeleted = Left$(xDeleted, Len(xDeleted) - 1)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub RestoreUncopiedmDels(ByVal xDeleted As String, ByRef oTags As Tags, _
                                        ByVal iFormat As mpFileFormats)
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoreUncopiedmDels"
            Dim omDels As Object = Nothing
            Dim oRange As Word.Range = Nothing
            Dim i As Short = 0
            Dim lPos As Integer = 0
            Dim iAttempts As Short = 0
            Dim oAttribute As Word.XMLNode = Nothing
            Dim omDelNode As Word.XMLNode = Nothing
            Dim xObjectData As String = ""
            Dim oProps As Object = Nothing
            Dim xTempID As String = ""
            Dim xTag As String = ""
            Dim omDelCC As Word.ContentControl

            On Error GoTo ProcError

            If xDeleted = "" Then Exit Sub

            omDels = Split(xDeleted, "�")
            For i = 0 To UBound(omDels)
                oProps = Split(omDels(i), "|")

                'set range - position in doc is last property
                lPos = CLng(oProps(UBound(oProps)))
                oRange = GlobalMethods.CurWordApp.ActiveDocument.Range(lPos, lPos)

                If iFormat = mpFileFormats.OpenXml Then
                    'content controls - added 10/10/19 (dm)
                    omDelCC = oRange.ContentControls.Add
                    omDelCC.SetPlaceholderText(, , "")
                    omDelCC.Tag = oProps(0)

                    'update tag in collection
                    If Not oTags Is Nothing Then
                        oTags.Update_CC(omDelCC)
                    End If
                Else
                    'add mDel - try a second time if necessary,
                    'as this line fails on first execution in Word 2007
                    On Error GoTo TryAgain
                    omDelNode = oRange.XMLNodes.Add("mDel", GlobalMethods.mpNamespace)
                    On Error GoTo ProcError

                    'set attribute values
                    oAttribute = omDelNode.Attributes.Add("TagID", "")
                    oAttribute.NodeValue = oProps(0)
                    oAttribute = omDelNode.Attributes.Add("ObjectData", "")
                    xObjectData = oProps(1) & "|" & oProps(2)
                    If UBound(oProps) > 4 Then
                        xObjectData = xObjectData & "|" & oProps(3) & "|" & oProps(4)
                        xTempID = oProps(5)
                        xTag = oProps(6)
                    Else
                        xTempID = oProps(3)
                        xTag = oProps(4)
                    End If
                    oAttribute.NodeValue = xObjectData

                    '10.2: Assign Tag to Reserved attribute
                    oAttribute = omDelNode.Attributes.Add("Reserved", "")
                    oAttribute.NodeValue = xTag

                    'update tag in collection
                    If Not oTags Is Nothing Then
                        oTags.Update(omDelNode, , xTempID)
                    End If
                End If
            Next i

            Exit Sub
TryAgain:
            If iAttempts > 0 Then
                On Error GoTo ProcError
            End If

            iAttempts = iAttempts + 1
            Resume
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function StripNormalStyleLineSpacing(xStylesXML) As String
            'removes the line spacing node from the Normal style node
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.StripNormalStyleLineSpacing"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xXML As String = ""

            On Error GoTo ProcError

            'GLOG item #5574 - dcf - 6/29/11
            lPos = InStr(xStylesXML, "w:styleId=" & Chr(34) & "Normal" & Chr(34))
            If lPos > 0 Then
                lPos = InStrRev(xStylesXML, "<w:style ", lPos)
                lPos2 = InStr(lPos, xStylesXML, "</w:style>")
                lPos3 = InStr(lPos, xStylesXML, "<w:spacing ")
                If (lPos3 > 0) And (lPos3 < lPos2) Then
                    lPos = InStr(lPos3, xStylesXML, "/>")
                    xXML = Left$(xStylesXML, lPos3 - 1) & Mid$(xStylesXML, lPos + 2)
                End If
            End If

            If xXML = "" Then _
                xXML = xStylesXML

            StripNormalStyleLineSpacing = xXML

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function GetChildmDels_CC(ByVal oCC As Word.ContentControl, ByRef oTags As Tags) As String
            'returns tags of all child mDels of oCC as a delimited string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetChildmDels_CC"
            Dim xmDels As String = ""
            Dim oChildCC As Word.ContentControl

            On Error GoTo ProcError

            For Each oChildCC In oCC.Range.ContentControls
                If BaseMethods.GetBaseName(oChildCC) = "mDel" Then
                    'assign a temp id
                    oTags.AddTempID_CC(oChildCC)

                    'add tag
                    xmDels = xmDels & oChildCC.Tag & "�"
                End If
            Next oChildCC

            If xmDels <> "" Then _
                xmDels = Left$(xmDels, Len(xmDels) - 1)

            GetChildmDels_CC = xmDels
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub RestoreChildmDels_CC(ByVal oCC As Word.ContentControl, _
                                     ByVal xmDels As String, _
                                     ByRef oTags As Tags)
            'recreates mDels in oCC from provided string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoreChildmDels_CC"
            Dim rngBefore As Word.Range = Nothing
            Dim rngAfter As Word.Range = Nothing
            Dim omDels As Object = Nothing
            Dim oProps As Object = Nothing
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim omDelCC As Word.ContentControl
            Dim lEnd As Integer = 0
            Dim xObjectData As String = ""
            Dim xTempID As String = ""
            Dim bIsEmpty As Boolean = False
            Dim xTag As String = ""
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            If xmDels = "" Then Exit Sub

            rngBefore = oCC.Range
            rngBefore.StartOf()
            rngAfter = oCC.Range
            rngAfter.EndOf()
            bIsEmpty = (rngBefore.Start = rngAfter.End)
            oDoc = rngBefore.Document

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'get tag
                xTag = omDels(i)

                'get object data
                xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)

                'position based on reinsertion location
                oProps = Split(xObjectData, "|")
                If (CInt(oProps(1)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bIsEmpty Then
                    rngmDel = rngAfter.Duplicate
                Else
                    rngmDel = rngBefore.Duplicate
                End If

                'add mDel
                omDelCC = rngmDel.ContentControls.Add
                omDelCC.SetPlaceholderText(, , "")
                omDelCC.Tag = xTag

                'add bookmark
                '        omDelCC.Range.Bookmarks.Add "_" & xTag

                'update tag in collection
                xTempID = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)
                oTags.Update_CC(omDelCC, , xTempID)

                'move position to end of tag
                lEnd = rngmDel.End + 2
                'GLOG 6490 (dm) - the line below incorrectly referenced oProps(2)
                If (CInt(oProps(1)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bIsEmpty Then
                    rngAfter.SetRange(lEnd, lEnd)
                Else
                    rngBefore.SetRange(lEnd, lEnd)
                End If
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetmDels_CC(ByVal oRange As Word.Range, _
                                    ByRef oTags As Tags, _
                                    Optional ByVal iReinsertionLocation As Short = -1, _
                                    Optional ByVal bExcludeNativeToTable As Boolean = False, _
                                    Optional ByVal bExcludeNativeToBlock As Boolean = False, _
                                    Optional ByVal bDeleteWhenFound As Boolean = False, _
                                    Optional ByVal xExcludeSegmentTagID As String = "") As String
            'returns a delimited string containing all mDels in oRange that meet
            'the specified criteria; deletes mDel if specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetmDels_CC"
            Dim oCC As Word.ContentControl
            Dim xObjectData As String = ""
            Dim xmDels As String = ""
            Dim oProps As Object = Nothing
            Dim bNativeToTable As Boolean = False
            Dim bInclude As Boolean = False
            Dim xExcludeSegmentIDs As String = ""
            Dim xElement As String = ""
            Dim oDoc As Word.Document
            Dim xTag As String = ""
            Dim oBmk As Word.Bookmark = Nothing 'GLOG 6858
            Dim xTagID As String = "" 'GLOG 6858
            Dim lStart As Integer = 0 'GLOG 6858
            Dim lEnd As Integer = 0 'GLOG 6858
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim bShowHidden As Boolean = False 'GLOG 8722

            On Error GoTo ProcError

            oDoc = oRange.Document
            xExcludeSegmentIDs = xExcludeSegmentTagID

            'GLOG 6858 (dm) - add child segment bookmarks to exclusion list
            lStart = oRange.Start
            lEnd = oRange.End
            'GLOG 8722
            bShowHidden = oRange.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oRange.Bookmarks.ShowHidden = True
            End If
            If xExcludeSegmentTagID <> "" Then
                For Each oBmk In oRange.Bookmarks
                    If oBmk.Name Like "_mps*" Then
                        If (oBmk.Range.Start >= lStart) And (oBmk.Range.End <= lEnd) Then
                            xTagID = BaseMethods.GetAttributeValue(Mid$(oBmk.Name, 2), "TagID")
                            If InStr(xExcludeSegmentIDs, xTagID) = 0 Then
                                xExcludeSegmentIDs = xExcludeSegmentIDs & "|" & xTagID
                            End If
                        End If
                    End If
                Next oBmk
            End If

            For Each oCC In oRange.ContentControls
                xTag = oCC.Tag
                xElement = BaseMethods.GetBaseName(oCC)
                If xElement = "mDel" Then
                    'get properties
                    xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)
                    oProps = Split(xObjectData, "|")
                    If UBound(oProps) > 1 Then _
                        bNativeToTable = (oProps(2) = "1")
                    bInclude = (((iReinsertionLocation = -1) Or _
                            (iReinsertionLocation = CInt(oProps(1)))) And _
                            Not (bExcludeNativeToTable And bNativeToTable) And _
                            InStr(xExcludeSegmentIDs, oProps(0)) = 0)
                    If bInclude Then
                        'GLOG 4377: Only assign TempID if mDel is being preserved
                        'assign a temp id
                        oTags.AddTempID_CC(oCC)
                        'TODO: implement bExcludeNativeToBlock
                        xmDels = xmDels & xTag & "�"
                        If bDeleteWhenFound Then
                            'GLOG 7389 (dm) - disable track changes while deleting mDel
                            bTrackChanges = oRange.Document.TrackRevisions
                            If bTrackChanges Then _
                                oRange.Document.TrackRevisions = False

                            oCC.Delete()

                            'GLOG 7389 (dm)
                            If bTrackChanges Then _
                                oRange.Document.TrackRevisions = True
                        End If

                        '7/20/11 (dm) - add doc var to store
                        BaseMethods.AddDocVarToStore("mpo" & Mid$(xTag, 4, 8))
                    End If
                ElseIf (xElement = "mSEG") And (xExcludeSegmentTagID <> "") Then
                    '12/26/08 - if we're excluding mDels belonging to a specified segment, we can
                    'assume that we should also exclude mDels belonging to other segments in the range -
                    'this is necessary to ensure that mDels belonging to children get deleted w/parent
                    xExcludeSegmentIDs = xExcludeSegmentIDs & "|" & _
                        BaseMethods.GetAttributeValue(xTag, "TagID", oDoc)
                End If
            Next oCC
            If xmDels <> "" Then _
                xmDels = Left$(xmDels, Len(xmDels) - 1)

            'GLOG 8722
            oRange.Bookmarks.ShowHidden = bShowHidden

            GetmDels_CC = xmDels

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub RestoremDels_CC(ByVal rngLocation As Word.Range, _
                                   ByVal xmDels As String, _
                                   ByRef oTags As Tags)
            'recreates mDels from provided string
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoremDels_CC"
            Dim rngBefore As Word.Range = Nothing
            Dim rngAfter As Word.Range = Nothing
            Dim omDels As Object = Nothing
            Dim oProps As Object = Nothing
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim omDelCC As Word.ContentControl
            Dim xObjectData As String = ""
            Dim lEnd As Integer = 0
            Dim xTempID As String = ""
            Dim bRangeIsEmpty As Boolean = False
            Dim oTag As Tag
            Dim xTag As String = ""
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            If xmDels = "" Then Exit Sub

            'set insertion range
            bRangeIsEmpty = ((Len(rngLocation.Text) = 0) Or (rngLocation.Text = vbCr)) 'GLOG 8440
            rngBefore = GetmDelInsertionLocation(rngLocation.Paragraphs.First, _
                mpReinsertionLocations.mpReinsertionLocation_Before, True)
            rngAfter = GetmDelInsertionLocation(rngLocation.Paragraphs.Last, _
                mpReinsertionLocations.mpReinsertionLocation_After, True)
            oDoc = rngBefore.Document

            '    'adjust end for table
            '    If rngLocation.Information(Word.wdInformation.wdWithInTable) Then
            '        If rngLocation = rngLocation.Tables(1).Range Then _
            '            rngAfter.Move Word.WdUnits.wdCharacter, -2
            '    End If

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'get tag
                xTag = omDels(i)

                'get object data
                xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)

                'position based on reinsertion location
                oProps = Split(xObjectData, "|")
                If (CInt(oProps(1)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bRangeIsEmpty Then
                    rngmDel = rngAfter.Duplicate
                Else
                    rngmDel = rngBefore.Duplicate
                End If

                'add mDel
                omDelCC = rngmDel.ContentControls.Add
                omDelCC.SetPlaceholderText(, , "")
                omDelCC.Tag = xTag

                'add bookmark
                '        omDelCC.Range.Bookmarks.Add "_" & xTag

                'update tag in collection
                If Not oTags Is Nothing Then
                    xTempID = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)
                    If xTempID <> "" Then
                        On Error Resume Next
                        oTag = oTags.GetTag_CC(omDelCC, , xTempID)
                        On Error GoTo ProcError
                    End If

                    If Not oTag Is Nothing Then
                        oTags.Update_CC(omDelCC, , xTempID)
                    Else
                        'it's possible that tags were refreshed while mDel was gone
                        oTags.Insert_CC(omDelCC)
                    End If
                End If

                'move position to end of tag
                lEnd = rngmDel.End + 2
                If (CInt(oProps(2)) = mpReinsertionLocations.mpReinsertionLocation_After) And Not bRangeIsEmpty Then
                    rngAfter.SetRange(lEnd, lEnd)
                Else
                    rngBefore.SetRange(lEnd, lEnd)
                End If
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function InsertmDels_CC(ByVal rngScope As Word.Range, _
                                        ByVal oSegmentRng As Word.Range, _
                                        ByVal xSegmentTag As String, _
                                        ByVal oTargetCC As Word.ContentControl, _
                                        ByVal xVariableName As String, _
                                        ByVal xObjectDBID As String, _
                                        ByVal iScope As mpDeleteScopes, _
                                        ByRef oTags As Tags) As mpReinsertionLocations
            '- inserts mDels to replace oTargetNode and all of the existing mDels in the
            'scope, preserving the sequence in the document
            '- mDel is inserted in the subsequent paragraph unless the scope is the last
            'paragraph in the mSEG or table cell
            '- if the insertion location contains blockable tags, mDel is inserted inside
            'innermost tag to preserve blockability
            '- mDel object data structure: "tag id of parent segment|reinsertion location|native
            'to table?|tag id of parent block" - the last two properties indicate whether the
            'scope for an mDel at the start of a table or block should be reinserted inside the
            'table/block or above it
            '- returns reinsertion location
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.InsertmDels_CC"
            Dim rngSegment As Word.Range = Nothing
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim lSegEnd As Integer = 0
            Dim rngPara As Word.Range = Nothing
            Dim i As Short = 0
            Dim oCC As Word.ContentControl
            Dim rngLocation As Word.Range = Nothing
            Dim oWordDoc As WordDoc
            Dim xmDels As String = ""
            Dim xTargetTag As String = ""
            Dim oProps As Object = Nothing
            Dim omDels As Object = Nothing
            Dim bTargetIsInString As Boolean = False
            Dim iCount As Short = 0
            Dim bSkip As Boolean = False
            Dim bScopeIsCell As Boolean = False
            Dim rngTag As Word.Range = Nothing
            Dim bIsTarget As Boolean = False
            Dim bParaScopeInCell As Boolean = False
            Dim xParentBlock As String = ""
            Dim bNativeToTable As Boolean = False
            Dim oBlockCC As Word.ContentControl
            Dim xTempID As String = ""
            Dim rngmDels As Word.Range = Nothing
            Dim bIsNativeToBlock As Boolean = False
            Dim oDoc As Word.Document
            Dim xParentSegment As String = ""
            Dim xObjectData As String = ""
            Dim xTargetProps As String = ""
            Dim xTag As String = ""
            Dim oVar As Word.Variable
            Dim xVarValue As String = ""
            Dim oVals As Object = Nothing

            On Error GoTo ProcError

            oWordDoc = New WordDoc

            'if scope is cell, or cell and previous cell, mDel always gets inserted before
            bScopeIsCell = ((iScope = mpDeleteScopes.mpDeleteScope_Cell) Or _
                (iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell))

            'get reinsertion location
            rngSegment = oSegmentRng
            oDoc = rngSegment.Document
            lSegEnd = rngSegment.End
            If (lSegEnd >= rngScope.End) And Not bScopeIsCell Then 'GLOG 6777
                'insert mDel in next paragraph
                If rngScope.Information(Word.WdInformation.wdWithInTable) And _
                        (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) Then
                    bParaScopeInCell = True
                    If rngScope.End = rngScope.Cells(1).Range.End Then
                        'this is the last paragraph of cell -
                        'insert mDel in previous paragraph
                        iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After
                    Else
                        iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before
                    End If
                Else
                    iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before
                End If
            Else
                'GLOG 8875
                If rngScope.Information(Word.WdInformation.wdWithInTable) And _
                                    (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) Then
                    bParaScopeInCell = True
                End If
                'this is last paragraph of mSEG - insert mDel in previous paragraph
                iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After
            End If

            Dim oParentSegBmk As Word.Bookmark = Nothing
            Dim oParentSegBmk2 As Word.Bookmark = Nothing
            Dim xParentSegBmk As String = ""
            Dim oSegRng As Word.Range = Nothing
            Dim bAdjustSegBmk As Boolean = False
            Dim lParentSegBmkEnd As Integer = 0

            'get target paragraph
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                'put in next paragraph
                '       expand segment range to next paragraph if necessary
                oParentSegBmk = oWordDoc.GetParentSegmentBookmarkFromRange(rngScope)
                rngPara = rngScope.Next(Word.WdUnits.wdParagraph)
                If Not rngPara Is Nothing Then
                    oParentSegBmk2 = oWordDoc.GetParentSegmentBookmarkFromRange(rngPara)
                Else
                    'GLOG 6777: If no next paragraph, insert empty paragraph
                    rngPara = rngScope.Duplicate
                    rngPara.InsertParagraphAfter()
                    rngPara = rngPara.Paragraphs.Last.Range
                End If

                'GLOG 7883 (dm) - new object var was being set to the same object
                'rather than a duplicate, inadvertently causing rngScope to expand
                oSegRng = rngScope.Duplicate

                'GLOG 6815: If range is at start of Segment, may be outside of bookmark
                If oParentSegBmk Is Nothing And Not oParentSegBmk2 Is Nothing Then
                    oSegRng.SetRange(rngScope.Start, oParentSegBmk2.Range.End)
                    oParentSegBmk = oSegRng.Bookmarks.Add(oParentSegBmk2.Name)
                End If

                lParentSegBmkEnd = oParentSegBmk.Range.End
                xParentSegBmk = oParentSegBmk.Name

                If rngScope.StoryType = wdStoryType.wdTextFrameStory Then
                    If Not oParentSegBmk2 Is Nothing Then
                        If oParentSegBmk2.Name <> xParentSegBmk Then
                            'next paragraph is not in the segment - expand segment
                            oSegRng.SetRange(oParentSegBmk.Range.Start, rngPara.End)
                            oSegRng.Bookmarks.Add(xParentSegBmk)

                            If oParentSegBmk.Range.End = lParentSegBmkEnd Then
                                'expanding the segment did not work - this is an issue with Word -
                                'add an extra paragraph to end of the segment
                                oParentSegBmk.Range.InsertParagraphAfter()

                                'rebookmark - bookmark should expand
                                oSegRng.Bookmarks.Add(xParentSegBmk)
                            End If
                        End If
                    Else
                        'next paragraph is not in any segment - expand original
                        'parent to include this paragraph
                        oSegRng.SetRange(oParentSegBmk.Range.Start, rngPara.End + 1)
                        oSegRng.Bookmarks.Add(xParentSegBmk)

                        If oParentSegBmk.Range.End = lParentSegBmkEnd Then
                            'expanding the segment did not work - this is an issue with Word -
                            'add an extra paragraph to end of the segment
                            oParentSegBmk.Range.InsertParagraphAfter()

                            'rebookmark - bookmark should expand
                            oSegRng.Bookmarks.Add(xParentSegBmk)
                        End If
                    End If
                End If

                'shrink range if at end of segment or end of cell -
                'this is necessary to prevent insertion range from getting set
                'to the entire segment or cell
                If rngSegment.Paragraphs.Last.Range.End <= rngPara.End Then
                    rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                ElseIf bParaScopeInCell Then
                    If rngPara.End = rngPara.Cells(1).Range.End Then _
                        rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                End If
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                'put in previous paragraph
                rngPara = rngScope.Previous(Word.WdUnits.wdParagraph)
                If Not rngPara Is Nothing Then
                    If rngPara.Information(Word.WdInformation.wdWithInTable) Then _
                        rngPara.MoveEnd(Word.WdUnits.wdCharacter, -1)
                Else
                    rngPara = rngScope.Paragraphs(1).Range
                End If
            Else
                'put in current paragraph
                rngPara = rngScope.Paragraphs(1).Range
            End If

            'if target paragraph contains a block/blockable tag or tags,
            'put mDels inside the inner one
            For i = 1 To rngPara.ContentControls.Count
                bSkip = False
                oCC = rngPara.ContentControls(i)
                If (BaseMethods.GetBaseName(oCC) <> "mDel") And _
                        Not BaseMethods.ContentControlIsInline(oCC) Then
                    'change range from the paragraph to the tag, unless this is the last
                    'paragraph of a multipara tag with the scope specified to be reinserted
                    'before or the first paragraph of a multipara tag with the scope specified
                    'to be reinserted after
                    rngTag = oCC.Range
                    If InStr(rngTag.Text, vbCr) > 0 Then
                        If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                            bSkip = (rngTag.Paragraphs.Last.Range.End <= _
                                rngPara.Paragraphs(1).Range.End)
                        Else
                            bSkip = (rngTag.Paragraphs.First.Range.Start >= _
                                rngPara.Paragraphs(1).Range.Start)
                        End If
                    End If

                    If Not bSkip Then _
                            rngLocation = rngTag
                Else
                    Exit For
                End If
            Next i

            'if no block tag, put at start/end of para
            If rngLocation Is Nothing Then
                rngLocation = rngPara.Duplicate
                If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then _
                    rngLocation.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'assign a temp id to target tag
            xTempID = oTags.AddTempID_CC(oTargetCC)

            'create tag for new mDel
            xTag = "mpd" & BaseMethods.GenerateDocVarID()

            'append object database id
            xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
            xTag = xTag & xObjectDBID & BaseMethods.CreatePadString(22, "0")

            'get attributes of new mDel
            bNativeToTable = ((iScope > mpDeleteScopes.mpDeleteScope_Paragraph) Or bParaScopeInCell)
            oBlockCC = BaseMethods.GetNearestAncestor(oTargetCC, "mBlock")
            If Not oBlockCC Is Nothing Then _
                xParentBlock = BaseMethods.GetAttributeValue(oBlockCC.Tag, "TagID", oDoc)
            xParentSegment = BaseMethods.GetAttributeValue(xSegmentTag, "TagID", oDoc)
            xObjectData = xParentSegment & "|" & iReinsertionLocation & "|" & _
                CStr(Math.Abs(CInt(bNativeToTable))) & "|" & xParentBlock
            xTargetProps = xTag & "|" & xTempID

            'create doc var and set attributes
            BaseMethods.SetAttributeValue(xTag, "TagID", xVariableName, "", oDoc)
            BaseMethods.SetAttributeValue(xTag, "ObjectData", xObjectData, "", oDoc)

            'get tags of existing mDels in scope, inserting tag of new mDel
            'at correct location in the string
            rngmDels = rngScope.Duplicate
            If Right$(rngmDels.Text, 1) = Chr(7) Then
                rngmDels.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If
            iCount = rngmDels.ContentControls.Count
            For i = 1 To iCount
                oCC = rngmDels.ContentControls(i)
                If BaseMethods.GetBaseName(oCC) = "mDel" Then
                    'assign a temp id to existing mDel
                    xTempID = oTags.AddTempID_CC(oCC)

                    'determine whether mDel is native to block that's getting deleted
                    xObjectData = BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData", oDoc)
                    oProps = Split(xObjectData, "|")
                    If UBound(oProps) > 1 Then
                        bIsNativeToBlock = (oProps(3) = xVariableName)
                    Else
                        bIsNativeToBlock = False
                    End If
                    If Not bIsNativeToBlock Then
                        'insert target if existing mDel is beyond it or if it's
                        'inside it but configured to be inserted after
                        If (Not bTargetIsInString) And _
                                (oCC.Range.Start > oTargetCC.Range.Start) And _
                                ((oCC.Range.Start > oTargetCC.Range.End) Or _
                                (CInt(oProps(1)) = mpReinsertionLocations.mpReinsertionLocation_After)) Then
                            'insert props of new mDel
                            xmDels = xmDels & xTargetProps & "�"
                            bTargetIsInString = True
                        End If

                        'insert props of existing mDel
                        xmDels = xmDels & oCC.Tag & "|" & xTempID & "�"
                    End If
                End If
            Next i

            If xmDels = "" Then
                'no existing mDels
                xmDels = xTargetProps
            ElseIf Not bTargetIsInString Then
                'add new mDel to end of string
                xmDels = xmDels & xTargetProps
            Else
                'trim trailing separator
                xmDels = Left$(xmDels, Len(xmDels) - 1)
            End If

            'delete existing mDels
            For i = iCount To 1 Step -1
                oCC = rngmDels.ContentControls(i)
                If BaseMethods.GetBaseName(oCC) = "mDel" Then
                    xObjectData = BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData", oDoc)
                    oProps = Split(xObjectData, "|")
                    If UBound(oProps) > 1 Then
                        bIsNativeToBlock = (oProps(3) = xVariableName)
                    Else
                        bIsNativeToBlock = False
                    End If
                    If Not bIsNativeToBlock Then _
                        oCC.Delete()
                End If
            Next i

            'insert mDels
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                rngLocation.StartOf()
            Else
                rngLocation.EndOf()
            End If

            omDels = Split(xmDels, "�")
            For i = 0 To UBound(omDels)
                'add mDel
                oCC = rngLocation.ContentControls.Add
                oCC.SetPlaceholderText(, , "")

                'set tag
                oProps = Split(omDels(i), "|")
                oCC.Tag = oProps(0)

                'update reinsertion location - this is the
                'only attribute that may have changed
                xVarValue = BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData")

                oVals = Split(xVarValue, "|")
                oVals(1) = iReinsertionLocation
                BaseMethods.SetAttributeValue(oCC.Tag, "ObjectData", Join(oVals, "|"), "", oDoc)

                'GLOG 6883:  Make sure new mDel is still within parent bookmark
                If Not oParentSegBmk Is Nothing Then
                    If oCC.Range.End > oParentSegBmk.End Then
                        oSegmentRng = oParentSegBmk.Range.Duplicate
                        oSegmentRng.SetRange(oSegmentRng.Start, oCC.Range.End + 1)
                        oParentSegBmk = oSegmentRng.Bookmarks.Add(oParentSegBmk.Name, oSegmentRng)
                    End If
                End If

                'update Tag in collection
                bIsTarget = (omDels(i) = xTargetProps)

                oTags.Update_CC(oCC, bIsTarget, oProps(1))

                'move to end of this mDel
                rngLocation.Move(Word.WdUnits.wdCharacter, 2)
            Next i

            InsertmDels_CC = iReinsertionLocation

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function RestoreScope_CC(ByVal oDelCC As Word.ContentControl, _
                                         ByVal oSegmentBoundingObject As Object, _
                                         ByVal xVariableName As String, _
                                         ByVal bAsBlock As Boolean, _
                                         ByVal iScope As mpDeleteScopes, _
                                         ByRef xXML As String, _
                                         ByRef xVariablesAdded As String, _
                                         ByRef oTags As Tags, _
                                         ByVal xBodyXML As String) As Word.ContentControl
            '- restores deletion scope represented by omDelCC and returns the primary mVar
            '- for performance, only inserts xml once per segment part, using copy/paste for
            'subsequent associated tags
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RestoreScope_CC"
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim oProps As Object = Nothing
            Dim bParaScopeInTable As Boolean = False
            Dim bNativeToTable As Boolean = False
            Dim oLocation As Word.Range = Nothing
            Dim rngmSEG As Word.Range = Nothing
            Dim lStart As Integer = 0
            Dim lEnd As Integer = 0
            Dim bMoveFromTable As Boolean = False
            Dim oRow As Word.Row
            Dim oTable As Word.Table
            Dim oCC As Word.ContentControl
            Dim bIsLastPara As Boolean = False
            Dim bIsEndOfCell As Boolean = False
            Dim xElement As String = ""
            Dim oInsertedCC As Word.ContentControl
            Dim i As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim xTempID As String = ""
            Dim lParaCount As Integer = 0
            Dim lXMLParaCount As Integer = 0
            Dim oTestCC As Word.ContentControl
            Dim lErr As Integer = 0
            Dim xTagPrefixID As String = ""
            Dim xObjectData As String = ""
            Dim xPassword As String = ""
            Dim oBlockCC As Word.ContentControl
            Dim bStartOfOwnerBlock As Boolean = False
            Dim oParentCC As Word.ContentControl
            Dim bIsInmVar As Boolean = False
            Dim oDoc As Word.Document
            Dim bDeleteExtraPara As Boolean = False
            Dim ocWordDoc As WordDoc
            Dim xTag As String = ""
            Dim oSegmentCC As Word.ContentControl
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim xBoundingObjectTag As String = ""
            Dim oParentSegBmk As Word.Bookmark = Nothing
            Dim oLocationContainingBmk As Word.Bookmark = Nothing
            Dim oNewLocation As Word.Range = Nothing
            Dim oLastCharBmk As Word.Bookmark = Nothing
            Dim bAdjustLocation As Boolean = False
            Dim lSegEnd As Integer = 0
            Dim lLocEnd As Integer = 0
            Dim bStartofSec As Boolean = False 'GLOG 7249 (dm)
            Dim bTrackChanges As Boolean = False 'GLOG 7389 (dm)
            Dim bShowHidden As Boolean = False 'GLOG 8722
            Dim bSegEndsWithTable As Boolean = False 'GLOG 8875

            ocWordDoc = New WordDoc

            On Error Resume Next
            oSegmentCC = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentCC.Range
                xBoundingObjectTag = oSegmentCC.Tag
            End If

            On Error GoTo ProcError

            rngmDel = oDelCC.Range
            oDoc = rngmDel.Document

            'GLOG 3977 (dm) - determine whether mDel is inside an mVar
            oParentCC = oDelCC.ParentContentControl
            If Not oParentCC Is Nothing Then _
                bIsInmVar = (BaseMethods.GetBaseName(oParentCC) = "mVar")

            'get reinsertion location
            xTag = oDelCC.Tag
            xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)
            oProps = Split(xObjectData, "|")
            iReinsertionLocation = CInt(oProps(1))
            Dim xParentTagID As String = oProps(0) 'GLOG 8875
            bParaScopeInTable = (iScope = mpDeleteScopes.mpDeleteScope_Paragraph) And _
                rngmDel.Information(Word.WdInformation.wdWithInTable)
            'GLOG 8875
            If bParaScopeInTable Then
                bSegEndsWithTable = oSegmentRng.End <= rngmDel.Tables(1).Range.End
            End If
            If UBound(oProps) = 1 Then
                bNativeToTable = ((iScope > mpDeleteScopes.mpDeleteScope_Paragraph) Or bParaScopeInTable)
            Else
                'GLOG 8875: NativeToTable property may not have been set correctly when saving design if in last paragraph of segment, so also check context
                bNativeToTable = CBool(oProps(2)) Or (bParaScopeInTable And bSegEndsWithTable And xParentTagID = WordDoc.GetAttributeValueFromTag(xBoundingObjectTag, "TagID"))
                If oProps(3) <> "" Then
                    'mDel belongs to a block - if it's in the first paragraph of the block,
                    'ensure that the scope is restored inside the block, not above it
                    oBlockCC = BaseMethods.GetNearestAncestor(oDelCC, "mBlock")
                    Do While Not oBlockCC Is Nothing
                        'check whether this is owner block
                        If BaseMethods.GetAttributeValue(oBlockCC.Tag, "TagID", oDoc) = oProps(3) Then
                            Exit Do
                        Else
                            oBlockCC = BaseMethods.GetNearestAncestor(oBlockCC, "mBlock")
                        End If
                    Loop
                    If Not oBlockCC Is Nothing Then
                        bStartOfOwnerBlock = (oBlockCC.Range.Start > _
                            oDelCC.Range.Paragraphs(1).Range.Start)
                    End If
                End If
            End If

            'assign a temp id to tag
            xTempID = oTags.AddTempID_CC(oDelCC)

            'get the bookmark of the parent segment
            oParentSegBmk = ocWordDoc.GetParentSegmentBookmarkFromRange(oDelCC.Range)
            If oParentSegBmk Is Nothing Then
                'no parent found for mDel
                'If oDelCC.Range.End = oSegmentRng.End Then
                'mDel cc is right at end of segment -
                'this doesn't get picked up as being in
                'the segment bookmark - use segment range
                oParentSegBmk = ocWordDoc.GetParentSegmentBookmarkFromRange(oSegmentRng)
                'End If
            End If

            'GLOG 7120 (dm) - in 10.5, mSEG and mDel may be co-extensive - preserve mSEG
            Dim xBMK As String = ""
            If Not oParentSegBmk Is Nothing Then _
                xBMK = oParentSegBmk.Name

            'GLOG 7389 (dm) - disable track changes while deleting mDel
            bTrackChanges = rngmDel.Document.TrackRevisions
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = False

            'delete tag
            oDelCC.Delete()
            'GLOG 7389 (dm)
            If bTrackChanges Then _
                rngmDel.Document.TrackRevisions = True
            'GLOG 8722
            bShowHidden = rngmDel.Bookmarks.ShowHidden
            If Not bShowHidden Then
                rngmDel.Bookmarks.ShowHidden = True
            End If
            'GLOG 7120 (dm) - restore mSEG if it's just been deleted
            If xBMK <> "" Then
                If Not oDoc.Bookmarks.Exists(xBMK) Then _
                    rngmDel.Bookmarks.Add(xBMK)
            End If
            'GLOG 8722
            rngmDel.Bookmarks.ShowHidden = bShowHidden
            'delete doc var
            ocWordDoc = New WordDoc
            ocWordDoc.DeleteRelatedObjects(xTag, oDoc)

            'set insertion location
            oLocation = rngmDel.Duplicate
            rngmSEG = oSegmentRng
            If iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before Then
                'GLOG 15966: If InsertXML is run in an empty paragraph, the paragraph will be deleted and replaced with insertion.
                'If immediately followed by a table, that means the insertion will end up inside the table.
                'Add an extra empty paragraph above current to ensure existing paragraphs are not deleted.
                If iScope = mpDeleteScopes.mpDeleteScope_Paragraph And Not bParaScopeInTable _
                        And oLocation.Paragraphs(1).Range.Characters.Count = 1 Then
                    oLocation.InsertParagraphBefore()
                    oLocation.Collapse(WdCollapseDirection.wdCollapseStart)
                End If
                lStart = oLocation.Paragraphs(1).Range.Start
                If bParaScopeInTable And Not bNativeToTable Then
                    'non-native mDel in table
                    oTable = oLocation.Tables(1)
                    If oTable.Range.Start = lStart Then
                        'mDel is in first paragraph of table
                        bMoveFromTable = True
                        If rngmSEG.Start > lStart Then
                            'mDel is in first paragraph of mSEG
                            If lStart = 0 Then
                                'start of doc - add a paragraph before the table
                                With oTable.Rows
                                    oRow = .Add(.Item(1))
                                End With
                                oRow.Cells.Merge()
                                oRow.ConvertToText()
                                For Each oCC In oLocation.Document _
                                        .Paragraphs(1).Range.ContentControls
                                    oCC.Delete()
                                Next oCC
                            End If

                            'move mSEG
                            rngmSEG.SetRange(oTable.Range.Start - 1, rngmSEG.End)

                            If Not oSegmentCC Is Nothing Then
                                oTags.MoveContentControl(oSegmentCC, rngmSEG)
                            Else
                                oTags.MoveBookmark(oSegmentBmk, rngmSEG)
                            End If

                            'set insertion location to before table
                            lStart = oTable.Range.Start
                            oLocation.SetRange(lStart - 1, lStart - 1)
                        Else
                            'set insertion location to before table
                            oLocation.SetRange(lStart - 1, lStart - 1)
                            oLocation.InsertAfter(vbCr)
                            oLocation.EndOf()
                        End If
                    Else
                        oLocation.StartOf(Word.WdUnits.wdParagraph)
                    End If
                ElseIf rngmSEG.Start > lStart Then
                    'mDel is in first paragraph of mSEG
                    oLocation = oSegmentRng
                    oLocation.StartOf()
                ElseIf bStartOfOwnerBlock Then
                    'mDel is in first paragraph of mBlock
                    oLocation = oBlockCC.Range
                    oLocation.StartOf()
                Else
                    'GLOG 3977 (dm) - mDel nested in mVar may no longer be in the mVar's first
                    'paragraph - ensure that the scope is not inserted inside the mVar
                    If bIsInmVar Then _
                            oLocation = oParentCC.Range.Paragraphs(1).Range

                    oLocation.StartOf(Word.WdUnits.wdParagraph)
                    oLocation.Collapse(WdCollapseDirection.wdCollapseStart)
                    'Move past any page break at start of paragraph
                    'GLOG 8677
                    'oLocation.MoveWhile(CStr(Chr(12)))
                End If
            ElseIf iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After Then
                lEnd = oLocation.Paragraphs(1).Range.End

                'check whether mDel is in last paragraph of cell
                If bParaScopeInTable Then
                    bIsEndOfCell = (lEnd = oLocation.Cells(1).Range.End)
                End If

                '        If oSegmentRng.End < lEnd - 1 Then
                If oSegmentRng.Paragraphs.Last.Range.End = lEnd Then
                    'mDel is in last paragraph of mSEG - ensure that scope gets
                    'reinserted inside mSEG
                    bIsLastPara = True
                    oLocation = rngmSEG.Duplicate
                    oLocation.EndOf()

                    If oLocation.Information(WdInformation.wdAtEndOfRowMarker) Then
                        oLocation.Move(Word.WdUnits.wdCharacter, -1)
                    End If

                    'scope will be inserted over the 'a' -
                    'the trailing para exists to ensure that the
                    'bookmark encompasses the restored scope
                    'If Len(oLocation.Paragraphs.Last.Range.Text) > 1 Then
                    oLocation.InsertAfter(vbCr)
                    oLocation.EndOf()
                    'End If
                    oLocation.ParagraphFormat.Reset()
                    oLocation.InsertAfter("a" & vbCr)
                    rngmSEG.SetRange(rngmSEG.Start, oLocation.End)
                    rngmSEG.Bookmarks.Add(oParentSegBmk.Name)
                    oLocation.EndOf()
                    oLocation.Move(Word.WdUnits.wdCharacter, -1)
                    oLocation.MoveStart(Word.WdUnits.wdCharacter, -1)
                Else
                    If bIsEndOfCell Then
                        'mDel is in last paragraph of cell
                        If bNativeToTable Or _
                                (lEnd < oLocation.Tables(1).Range.End - 1) Then
                            'mDel is native to table or not in last cell
                            'Need to use InsertAfter, because InsertParagraphAfter will
                            'place return before any tags at end of paragraph in Word 2003
                            oLocation.Paragraphs(1).Range.InsertAfter(vbCr)
                            oLocation.EndOf(Word.WdUnits.wdParagraph)
                        Else
                            'non-native - insert after table
                            bMoveFromTable = True
                            oLocation.SetRange(lEnd + 1, lEnd + 1)
                        End If
                    ElseIf bIsInmVar Then
                        'GLOG 3977 (dm) - mDel nested in mVar may no longer be in the mVar's last
                        'paragraph - ensure that the scope is not inserted inside the mVar
                        oLocation = oParentCC.Range.Paragraphs.Last.Range
                        oLocation.EndOf(Word.WdUnits.wdParagraph)
                    Else
                        oLocation.EndOf(Word.WdUnits.wdParagraph)
                    End If
                End If
            End If

            'GLOG 3497 - prevent inadvertently embedded mpNewSegment bookmark from
            'adding/moving bookmark when xml is inserted
            xXML = Replace(xXML, "w:name=" & Chr(34) & "mpNewSegment" & Chr(34), _
                "w:name=" & Chr(34) & "mpNewSegmentEmbedded" & Chr(34))

            'GLOG 7262 (CEH 11/8/13): code needed per #7262
            ''GLOG 7219 (JTS 9/26/13): At a minimum this code should only be used in the case where
            ''Segment Bounding Object is Bookmark, but it doesn't actually appear to be required at all.
            ''Specific case noted of bcc in Letter still works correctly even without this code.
            ''Unless anyone can see a need for this, we should probably remove completely.
            '    'if the restored scope is at the end of the segment, e.g. bcc in letter,
            '    'the inserted scope xml will be inserted outside the bounds of the parent
            '    'segment (i.e. the letter).  to account for this, we add a temp paragraph
            '    'at the end of the segment, then adjust the insertion location so that
            '    'insertion occurs before the temp paragraph.  this guarantees that the
            '    'xml will be inserted within segment bounds.  we then delete the temp
            '    'paragraph later - dcf - 9/18/12
            '
            If Not oSegmentBmk Is Nothing Then
                'get the segment of the insertion location
                oLocationContainingBmk = ocWordDoc.GetParentSegmentBookmarkFromRange(oLocation)
                'here
                'oLocationContainingBmk = ocWordDoc.GetParentSegmentBookmarkFromRange(rngmSEG)

                lSegEnd = rngmSEG.End
                lLocEnd = oLocation.End

                'adjust insertion location iff it is at the end of or after the segment,
                'and it is not in a table
                bAdjustLocation = lLocEnd >= lSegEnd And Not oLocation.Information(Word.WdInformation.wdWithInTable)

                If bAdjustLocation Then
                    'move insertion location to inside segment, as otherwise
                    'the inserted xml won't get inserted into it
                    oNewLocation = oParentSegBmk.Range
                    oNewLocation.InsertParagraphAfter()
                    oNewLocation.Bookmarks.Add(oParentSegBmk.Name)

                    oLastCharBmk = oNewLocation.Characters.Last.Bookmarks.Add("mpTempLastCharBmk")

                    oLocation = oNewLocation.Characters.Last
                    oLocation.StartOf()
                End If
            End If

            With oLocation
                'if scope is cell or row, and the table no longer exists,
                'reinsert as table
                If (iScope = mpDeleteScopes.mpDeleteScope_Cell Or _
                        iScope = mpDeleteScopes.mpDeleteScope_CellAndPreviousCell Or _
                        iScope = mpDeleteScopes.mpDeleteScope_Row) And _
                        (Not .Information(Word.WdInformation.wdWithInTable)) Then
                    iScope = mpDeleteScopes.mpDeleteScope_Table
                End If

                Select Case iScope
                    Case mpDeleteScopes.mpDeleteScope_Paragraph
                        If xXML <> "" Then
                            'get count of existing paras in mSEG
                            'GLOG 3897: If Deleted Scope is last paragraph in mSEG, rngmSEG won't
                            'include the new paragraph(s) after restoring.  Count Paragraphs in mSEG range instead
                            lParaCount = oSegmentRng.Paragraphs.Count
                            'insert
                            'GLOG 15945: If block has multiple paragraphs, don't include last trailing para after Content Control
                            If bAsBlock Then
                                xXML = xXML.Replace("</w:sdt><w:p /></w:body>", "</w:sdt></w:body>")
                            End If
                            .InsertXML(xXML)

                            'delete trailing para if necessary
                            If bIsLastPara Then
                                'delete the trailing para
                                oSegmentRng.Characters.Last.Delete()

                                'delete the trailing para
                                oSegmentRng.Characters.Last.Delete()
                            ElseIf bIsEndOfCell And Not bMoveFromTable Then
                                'delete trailing paragraph
                                .Cells(1).Range.Characters.Last.Previous.Delete()
                            ElseIf (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_Before) And _
                                    bMoveFromTable Then
                                rngmDel.Tables(1).Range.Previous(Word.WdUnits.wdCharacter).Delete()
                            End If

                            'expand to scope
                            .Expand(Word.WdUnits.wdParagraph)
                            'GLOG 15945: Block may contain more than one paragraph
                            'If so move range end to end of mBlock Content Control
                            If bAsBlock Then
                                Dim oNewCC As ContentControl
                                Dim xBaseName As String = ""
                                oNewCC = .Characters.Last.ParentContentControl
                                If Not oNewCC Is Nothing Then
                                    xBaseName = BaseMethods.GetBaseName(oNewCC)
                                End If
                                While xBaseName <> "" And xBaseName <> "mBlock"
                                    If Not oNewCC Is Nothing Then
                                        oNewCC = oNewCC.ParentContentControl
                                        If Not oNewCC Is Nothing Then
                                            xBaseName = BaseMethods.GetBaseName(oNewCC)
                                        End If
                                    Else
                                        Exit While
                                    End If
                                End While
                                If xBaseName = "mBlock" Then
                                    oLocation.SetRange(oLocation.Start, oNewCC.Range.End + 1)
                                End If
                            End If
                            'there are other circumstances in which an extra paragraph mark will
                            'be inserted (see GLOG item 1685) - if next paragraph is empty, ensure
                            'that number of added paras matches the number of paras in the xml
                            Dim oChar As Range
                            oChar = .Next(Word.WdUnits.wdCharacter, 1)

                            If Not oChar Is Nothing Then
                                If oChar.Text = vbCr Then
                                    'get number of paragraphs that should be added
                                    lXMLParaCount = BaseMethods.CountChrs(xBodyXML, GlobalMethods.mpEndParaTag) + _
                                        BaseMethods.CountChrs(xBodyXML, GlobalMethods.mpEmptyParaTag) - 1
                                    If .ListFormat.ListType <> WdListType.wdListNoNumbering Then
                                        'numbered paragraph
                                        oChar.Delete()
                                        'GLOG 3897: Compare paragraph count in mSEG to original count
                                    ElseIf oSegmentRng.Paragraphs.Count - lParaCount > _
                                            lXMLParaCount Then
                                        'catch-all
                                        oChar.Delete()
                                    End If
                                End If
                            End If

                            'ensure that entire cell isn't copied
                            If bIsEndOfCell And Not bIsLastPara Then _
                                .MoveEnd(Word.WdUnits.wdCharacter, -1)

                            'copy
                            On Error Resume Next
                            .Copy()
                            lErr = Err.Number
                            On Error GoTo ProcError
                            If lErr = 4198 Then
                                'intermittent "command not available" error when
                                'copying range in Word 2003
                                .Select()
                                GlobalMethods.CurWordApp.Selection.Copy()
                            End If

                            'clear
                            'GLOG 6786 (dm) - don't do for blocks because this is
                            'coded to only copy a single paragraph
                            If Not bAsBlock Then
                                xXML = ""
                            End If
                        Else
                            'GLOG 7249 (dm) - insert buffer paragraph at start of section -
                            'pasting content controls in this location can cause instability
                            'in other content controls in the headers/footers of the same section
                            bStartofSec = (.Paragraphs(1).Range.Start = .Sections(1).Range.Start)
                            If bStartofSec Then
                                .InsertParagraphAfter()
                                .EndOf()
                            End If

                            'paste
                            .Paste()

                            'GLOG 7249 (dm) - delete buffer paragraph
                            If bStartofSec Then
                                .Sections(1).Range.Paragraphs(1).Range.Delete()
                            End If
                        End If
                    Case mpDeleteScopes.mpDeleteScope_Cell, mpDeleteScopes.mpDeleteScope_CellAndPreviousCell
                        'TODO: use copy/paste for multiple tags ala other scopes
                        Restore_Cells(oLocation, xXML, iScope, True, oTags)
                    Case mpDeleteScopes.mpDeleteScope_Row
                        Restore_Row(oLocation, xXML)
                        xXML = ""
                    Case mpDeleteScopes.mpDeleteScope_Table
                        If xXML <> "" Then
                            'InsertXML with open xml will force an empty paragraph after the table -
                            'this is in addition to the trailing empty paragraph that will always
                            'be in the xml for a table or row - check whether we're
                            'inserting into an empty paragraph
                            bDeleteExtraPara = (.Paragraphs(1).Range.Text <> vbCr)

                            'insert xml
                            .InsertXML(xXML)

                            'clear
                            xXML = ""

                            'delete trailing para
                            oTable = .Tables(1)
                            oTable.Range.Next(Word.WdUnits.wdCharacter).Delete()

                            'delete extra para if necessary
                            If bDeleteExtraPara And (oTable.Range.Next(Word.WdUnits.wdCharacter).Text = vbCr) Then
                                oTable.Range.Next(Word.WdUnits.wdCharacter).Delete()
                            End If

                            'expand to scope
                            .Expand(Word.WdUnits.wdTable)

                            'copy
                            .Copy()
                        Else
                            'paste
                            .Paste()
                        End If
                End Select

                'GLOG 3497 - delete embedded bookmark
                Dim oBmks As Word.Bookmarks

                oBmks = .Document.Bookmarks

                If oBmks.Exists("mpNewSegmentEmbedded") Then _
                    oBmks("mpNewSegmentEmbedded").Delete()

                'GLOG 7262: code needed per #7262
                'GLOG 7219:  No longer necessary since code that created this bookmark has been removed
                If oBmks.Exists("mpTempLastCharBmk") Then
                    oNewLocation = oBmks.Item("mpTempLastCharBmk").Range
                    oNewLocation.Characters.Last.Delete()
                    oBmks.Item("mpTempLastCharBmk").Delete()
                End If
            End With

            'get new mVar/mBlock
            If bAsBlock Then
                xElement = "mBlock"
            Else
                xElement = "mVar"
            End If
            For i = 1 To oLocation.ContentControls.Count
                oInsertedCC = oLocation.ContentControls(i)
                If BaseMethods.GetBaseName(oInsertedCC) = xElement Then
                    If BaseMethods.GetAttributeValue(oInsertedCC.Tag, "TagID", oDoc) = _
                            xVariableName Then
                        Exit For
                    End If
                End If
            Next i

            '6/21/11 (dm) - placeholder text was getting reon insertion
            oInsertedCC.SetPlaceholderText(, , "")

            'retag inserted content control, first setting byref argument
            BaseMethods.RetagContentControl(oInsertedCC)

            'add inserted mVars to tags collection, getting back list of variables added
            If iScope = mpDeleteScopes.mpDeleteScope_Paragraph Then
                'ensure that we have all of the paragraphs in the scope -
                'with the other scope types, the range is inherent in the type
                oLocation = oInsertedCC.Range
                oLocation.Expand(Word.WdUnits.wdParagraph)
                If Right$(oLocation.Text, 1) = Chr(7) Then _
                    oLocation.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            'GLOG 5276 (dm) - ensure that numbered paragraphs conform to style - this
            'is necessary because some paragraph formatting, e.g. spacing, appears to
            'be implicit in the styles xml, so isn't necessarily applied in the insertion
            ConformNumberedParagraphsToStyle(oLocation, xBodyXML)

            'get segment tag prefix id
            xObjectData = BaseMethods.GetAttributeValue(xBoundingObjectTag, "ObjectData", oDoc)
            xTagPrefixID = BaseMethods.GetTagPrefixID(xObjectData)

            'get variables in scope
            GetVariablesInScope(oLocation, xVariableName, False, True, oTags, _
                xVariablesAdded, xTagPrefixID, xPassword)

            'add tag prefix id to object data if necessary
            xObjectData = BaseMethods.GetAttributeValue(oInsertedCC.Tag, "ObjectData", oDoc)
            If (xTagPrefixID <> "") And (BaseMethods.GetTagPrefixID(xObjectData) = "") Then
                xObjectData = xTagPrefixID & GlobalMethods.mpTagPrefixIDSeparator & xObjectData
            End If
            BaseMethods.SetAttributeValue(oInsertedCC.Tag, "ObjectData", xObjectData, "", oDoc)

            'GLOG 8722
            bShowHidden = oDoc.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oDoc.Bookmarks.ShowHidden = True
            End If
            'GLOG 6902
            If Not oSegmentBmk Is Nothing Then
                'If Restored range is no longer inside parent bookmark, extend bookmark range
                If oLocation.End >= oSegmentBmk.End Then
                    Dim oRngBmk As Word.Range = Nothing
                    oRngBmk = oSegmentBmk.Range.Duplicate
                    oRngBmk.SetRange(oSegmentBmk.Range.Start, oLocation.End)
                    oLocation.Document.Bookmarks.Add(oSegmentBmk.Name, oRngBmk)
                End If
            ElseIf oSegmentCC.Range.End > oDoc.Bookmarks("_" & xBoundingObjectTag).End Then
                'GLOG 6849: Make sure Bookmark extends to end of mSEG
                rngmSEG = oSegmentCC.Range.Duplicate
                rngmSEG.Bookmarks.Add("_" & xBoundingObjectTag)
            End If
            'GLOG 8722
            oDoc.Bookmarks.ShowHidden = bShowHidden
            'update tag in collection
            oTags.Update_CC(oInsertedCC, True, xTempID)

            'return inserted content control
            RestoreScope_CC = oInsertedCC

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Restore_CC(ByVal oSegmentBoundingObject As Object, _
                              ByVal xVariableName As String, _
                              ByVal bAsBlock As Boolean, _
                              ByVal iScope As mpDeleteScopes, _
                              ByVal xSegmentXML As String, _
                              ByVal oLocation As Word.Range, _
                              ByRef xVariablesAdded As String, _
                              ByRef oTags As Tags, _
                              ByRef rngmDels() As Word.Range, _
                              ByRef oInsertedCCs() As Word.ContentControl, _
                              Optional ByRef bContainsWordMLGraphic As Boolean = False)
            'reinserts deleted scope for xVariableName in oSegmentCC.  Steps:
            '1 - gets insertion xml from the mSEG's DeletedScopes attribute
            '2 - cycles through mSEG's child mDels, calling RestoreScope() for each
            '3 - removes the insertion xml from the mSEG's DeletedScopes attribute
            '4 - returns an array of restored mVars and an array of ranges where mDels were
            'replaced, so that any adjacent mDels can be moved to preserve the sequence
            'GLOG 6045 (dm) - added bContainsWordMLGraphic parameter
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_CC"
            Dim xXML As String = ""
            Dim xDeletedScopes As String = ""
            Dim oDelCC As Word.ContentControl
            Dim oDelCCs() As Word.ContentControl
            Dim xPrefixMapping As String = ""
            Dim bLocationSupplied As Boolean = False
            Dim oInsertedCC As Word.ContentControl
            Dim i As Short = 0
            Dim iCount As Short = 0
            Dim oWordDoc As WordDoc
            Dim xPassword As String = ""
            Dim xText As String = ""
            Dim xSegmentID As String = ""
            Dim xPart As String = ""
            Dim xXPath As String = ""
            Dim oDoc As Word.Document
            Dim xBodyXML As String = ""
            Dim oSegmentCC As Word.ContentControl
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim xBoundingObjectTag As String = ""
            Dim oRng As Word.Range = Nothing

            On Error GoTo ProcError
            oWordDoc = New WordDoc

            On Error Resume Next
            oSegmentCC = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
                oDoc = oSegmentBmk.Range.Document
                xSegmentID = oWordDoc.GetFullTagID_Bookmark(oSegmentBmk)
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentCC.Range
                xBoundingObjectTag = oSegmentCC.Tag
                oDoc = oSegmentRng.Document
                xSegmentID = oWordDoc.GetFullTagID_CC(oSegmentCC)
            End If

            'store clipboard contents
            Clipboard.SaveClipboard()

            If xVariableName = "" Then
                'raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>")
            End If

            'TODO: implement oLocation parameter - the idea is that if the mDel
            'is deleted outside of the firm, and we have the xml for the scope,
            'we can prompt the user to select the insertion location
            bLocationSupplied = (Not oLocation Is Nothing)

            'get insertion xml
            xXML = GetInsertionXML_CC(xBoundingObjectTag, oSegmentRng, xVariableName, _
                xSegmentXML, xBodyXML, bContainsWordMLGraphic)
            If bAsBlock And oTags.GUIDIndexing And bContainsWordMLGraphic Then
                'GLOG 6045 (dm) - IncludeBlock at runtime recreates parent when the delete scope
                'contains a WordML graphic - at design time, we simply strip the graphic
                Exit Sub
            ElseIf xXML = "" Then
                'no xml - raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>" & xVariableName)
            End If

            'get mDels
            xPart = BaseMethods.GetAttributeValue(xBoundingObjectTag, "PartNumber", oDoc)
            xXPath = "//mSEG[@TagID='" & xSegmentID & "' and @Part='" & xPart & "']" & _
                "/mDel[@TagID='" & xSegmentID & "." & xVariableName & "']"
            oDelCCs = oTags.GetContentControls(xXPath)

            If (oDelCCs(0) Is Nothing) And Not bLocationSupplied Then
                'no location - raise error
                Err.Raise(mpErrors.mpError_InsertionLocationRequired, , _
                    "<Error_DeleteScopeLocationRequired>" & xVariableName)
            End If

            iCount = UBound(oDelCCs)
            ReDim rngmDels(iCount)
            ReDim oInsertedCCs(iCount)

            For i = 0 To iCount
                'get mDel and clear out text
                oDelCC = oDelCCs(i)

                oRng = Nothing

                On Error Resume Next
                oRng = oDelCC.Range
                On Error GoTo ProcError

                If Not oRng Is Nothing Then
                    'GLOG 3307 (Doug)
                    'rngmDel.Text = ""
                    oWordDoc.RemoveTextFrommDel_CC(oDelCC)

                    'add mDel location to byref array
                    rngmDels(i) = oDelCC.Range

                    'restore scope
                    oInsertedCC = RestoreScope_CC(oDelCC, oSegmentBoundingObject, xVariableName, _
                        bAsBlock, iScope, xXML, xVariablesAdded, oTags, xBodyXML)

                    'add restored mVar to byref array
                    oInsertedCCs(i) = oInsertedCC
                End If
            Next i

            'remove delete scope from mSeg
            xDeletedScopes = BaseMethods.GetAttributeValue(xBoundingObjectTag, "DeletedScopes", oDoc)
            DeleteDefinition(xDeletedScopes, xVariableName)
            BaseMethods.SetAttributeValue(xBoundingObjectTag, "DeletedScopes", xDeletedScopes, "", oDoc)

            'update Tag in tags collection
            If Not oSegmentCC Is Nothing Then
                oTags.Update_CC(oSegmentCC)
            Else
                oTags.Update_Bmk(oSegmentBmk)
            End If

            'restore clipboard contents
            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function GetInsertionXML_CC(ByVal xSegmentTag As String, _
                                            ByVal oSegmentRange As Word.Range, _
                                            ByVal xVariableName As String, _
                                            ByVal xSegmentXML As String, _
                                            ByRef xBodyNode As String, _
                                            Optional ByRef bContainsWordMLGraphic As Boolean = False) As String
            'gets the reinsertion xml for xVariableName
            'GLOG 6045 (dm) - added bContainsWordMLGraphic parameter
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetInsertionXML_CC"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xDeletedScopes As String = ""
            Dim xDocNode As String = ""
            Dim xXML As String = ""
            Dim xStylesNode As String = ""
            Dim xBeforeBody As String = ""
            Dim xRelationships As String = ""
            Dim xParts As String = ""

            On Error GoTo ProcError

            'retrieving range.xml forces a temp save - we avoid this by
            'getting styles nodes from segment xml if supplied
            If xSegmentXML = "" Then _
                xSegmentXML = oSegmentRange.WordOpenXML

            'get deleted scopes attribute from mSEG tag
            xDeletedScopes = BaseMethods.GetAttributeValue(xSegmentTag, "DeletedScopes", _
                oSegmentRange.Document)
            If xDeletedScopes <> "" Then
                lPos = InStr(xDeletedScopes, "DeletedTag=" & xVariableName & "�")
                If lPos Then
                    lPos = InStr(lPos, xDeletedScopes, "�w:document ")

                    '3/29/11 (dm) - convert from WordML if necessary
                    If lPos = 0 Then
                        lPos = InStr(xDeletedScopes, "DeletedTag=" & xVariableName & "�")
                        lPos = lPos + Len(xVariableName) + 12
                        lPos2 = InStr(lPos, xDeletedScopes, "�/w:body>|") + 10
                        xDeletedScopes = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                        xDeletedScopes = ConvertScopeToOpenXml(xDeletedScopes, _
                            oSegmentRange.Document)
                        lPos = 1
                    End If

                    If lPos Then
                        '7/18/11 (dm) - modified to account for possible relationships
                        'and parts for embedded content between w:document and w:body -
                        'get wordDocument start tag
                        lPos2 = InStr(lPos, xDeletedScopes, ">") + 1
                        xDocNode = Mid$(xDeletedScopes, lPos, lPos2 - lPos)

                        'get body node
                        lPos2 = InStr(lPos, xDeletedScopes, "�w:body>")
                        lPos3 = InStr(lPos2, xDeletedScopes, "�/w:body>|")
                        xBodyNode = Mid$(xDeletedScopes, lPos2, lPos3 - lPos2)

                        'include empty paragraph at end to preserve format
                        'of last paragraph of scope
                        xBodyNode = xBodyNode & "<w:p /></w:body>"

                        '7/19/11 (dm) - there shouldn't be any pictures in the main
                        'document part - strip now, so that the pictures can be manually
                        'restored in the designer - we've determined that it's unrealistic
                        'to attempt to directly convert this xml from WordML to Open XML
                        Dim lPos4 As Integer = 0
                        lPos3 = InStr(xBodyNode, "�w:pict>")
                        If lPos3 > 0 Then
                            bContainsWordMLGraphic = True 'GLOG 6045
                            lPos4 = InStr(xBodyNode, "�/w:pict>") + 8
                            xBodyNode = Left$(xBodyNode, lPos3 - 1) & Mid$(xBodyNode, lPos4)
                        End If

                        '7/18/11 (dm) - get any additional relationships and parts
                        xBeforeBody = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                        xBeforeBody = Replace(xBeforeBody, "�", "<")
                        lPos = InStr(xBeforeBody, "<Relationship Id=")
                        While lPos > 0
                            lPos2 = InStr(lPos, xBeforeBody, "/>") + 2
                            xRelationships = xRelationships & _
                                Mid$(xBeforeBody, lPos, lPos2 - lPos)
                            lPos = InStr(lPos2, xBeforeBody, "<Relationship Id=")
                        End While
                        lPos = InStr(xBeforeBody, "<pkg:part ")
                        While lPos > 0
                            lPos2 = InStr(lPos, xBeforeBody, "</pkg:part>") + 11
                            xParts = xParts & Mid$(xBeforeBody, lPos, lPos2 - lPos)
                            lPos = InStr(lPos2, xBeforeBody, "<pkg:part ")
                        End While

                        'GLOG 5276 (dm) - add numbering part
                        If BaseMethods.IsWordOpenXml(xSegmentXML) Then
                            xRelationships = xRelationships & "<Relationship Id=""rId2"" " & _
                                "Type=""http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"" " & _
                                "Target=""numbering.xml""/>"
                            lPos = InStr(xSegmentXML, "<pkg:part pkg:name=""/word/numbering.xml""")
                            If lPos > 0 Then
                                lPos2 = InStr(lPos, xSegmentXML, "</pkg:part>") + 11
                                xParts = xParts & Mid$(xSegmentXML, lPos, lPos2 - lPos)
                            End If
                        End If

                        '9/26/11 (dm) - attempted to add code to create separate parts for
                        'footnotes embedded in WordML, but couldn't get it
                        'working, probably due to a mistake in the remmed out boilerplate
                        'for the part below - settled for stripping the footnotes as we
                        'do embedded pictures
                        '                Dim xNote As String
                        '                Dim xFootnotes As String
                        '                Dim xEndnotes As String
                        Dim lID As Integer = 0
                        lPos = InStr(xBodyNode, "�w:footnote>")
                        While lPos > 0
                            'get footnote
                            lID = lID + 1
                            lPos2 = InStr(lPos, xBodyNode, "�/w:footnote>") + 13
                            '                    xNote = Mid$(xBodyNode, lPos, lPos2 - lPos)
                            '                    xNote = "<w:footnote w:id=" & Chr(34) & CStr(lID) & Chr(34) & _
                            '                        Mid$(xNote, 12)
                            '                    xFootnotes = xFootnotes & xNote

                            'replace with reference
                            '                    xBodyNode = Left$(xBodyNode, lPos - 1) & "<w:footnoteReference w:id=" & _
                            '                        Chr(34) & CStr(lID) & Chr(34) & "/>" & Mid$(xBodyNode, lPos2)
                            xBodyNode = Left$(xBodyNode, lPos - 1) & Mid$(xBodyNode, lPos2)

                            lPos = InStr(lPos, xBodyNode, "�w:footnote>")
                        End While
                        '                If xFootnotes <> "" Then
                        '                    xRelationships = xRelationships & "<Relationship Id=" & Chr(34) & _
                        '                        "rId5" & Chr(34) & " Type=" & Chr(34) & _
                        '                        "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes" & _
                        '                        Chr(34) & " Target=" & Chr(34) & "footnotes.xml" & Chr(34) & "/>"
                        '                    xParts = xParts & "</pkg:part><pkg:part pkg:name=" & Chr(34) & "/word/footnotes.xml" & Chr(34) & " pkg:contentType=" & Chr(34) & "application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml" & Chr(34) & "><pkg:xmlData><w:footnotes mc:Ignorable=" & Chr(34) & "w14 wp14" & Chr(34) & " xmlns:wpc=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" & Chr(34) & " xmlns:mc=" & Chr(34) & _
                        '                        "http://schemas.openxmlformats.org/markup-compatibility/2006" & Chr(34) & " xmlns:o=" & Chr(34) & "urn:schemas-microsoft-com:office:office" & Chr(34) & " xmlns:o=" & Chr(34) & "urn:schemas-microsoft-com:office:office" & Chr(34) & " xmlns:r=" & Chr(34) & "http://schemas.openxmlformats.org/officeDocument/2006/relationships" & Chr(34) & " xmlns:m=" & Chr(34) & "http://schemas.openxmlformats.org/officeDocument/2006/math" & Chr(34) & _
                        '                        " xmlns:v=" & Chr(34) & "urn:schemas-microsoft-com:vml" & Chr(34) & " xmlns:wp14=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" & Chr(34) & " xmlns:wp=" & Chr(34) & "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" & Chr(34) & " xmlns:w10=" & Chr(34) & "urn:schemas-microsoft-com:office:word" & Chr(34) & _
                        '                        " xmlns:w=" & Chr(34) & "http://schemas.openxmlformats.org/wordprocessingml/2006/main" & Chr(34) & " xmlns:w14=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordml" & Chr(34) & " xmlns:wpg=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" & Chr(34) & " xmlns:wpi=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordprocessingInk" & Chr(34) & _
                        '                        " xmlns:wne=" & Chr(34) & "http://schemas.microsoft.com/office/word/2006/wordml" & Chr(34) & " xmlns:wps=" & Chr(34) & "http://schemas.microsoft.com/office/word/2010/wordprocessingShape" & Chr(34) & "><w:footnote w:type=" & Chr(34) & "separator" & Chr(34) & " w:id=" & Chr(34) & "-1" & Chr(34) & "><w:p><w:pPr><w:spacing w:after=" & Chr(34) & "0" & Chr(34) & " w:line=" & Chr(34) & "240" & Chr(34) & " w:lineRule=" & Chr(34) & _
                        '                        "auto" & Chr(34) & "/></w:pPr><w:r><w:separator/></w:r></w:p></w:footnote><w:footnote w:type=" & Chr(34) & "continuationSeparator" & Chr(34) & " w:id=" & Chr(34) & "0" & Chr(34) & "><w:p><w:pPr><w:spacing w:after=" & Chr(34) & "0" & Chr(34) & " w:line=" & Chr(34) & "240" & Chr(34) & " w:lineRule=" & Chr(34) & "auto" & Chr(34) & "/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:footnote>" & _
                        '                        xFootnotes & "</w:footnotes></pkg:xmlData></pkg:part>"
                        '                End If
                    End If
                End If
            End If

            If xBodyNode = "" Then
                'no delete scope definition in tag
                Exit Function
            End If

            'replace substitution character and return
            xDocNode = Replace(xDocNode, "�", "<")
            xBodyNode = Replace(xBodyNode, "�", "<") 'GLOG 5313 (dm)

            'add bookmarks if target doc is binary (1/13/11, dm)
            If BaseMethods.FileFormat(oSegmentRange.Document) = mpFileFormats.Binary Then
                AddBookmarks(xBodyNode)
            End If

            'concatenate
            xDocNode = xDocNode & xBodyNode

            'wrap in generic xml for package and document part
            xXML = "<?xml version=""1.0"" standalone=""yes""?><?mso-application progid=""Word.Document""?>" & _
                "<pkg:package xmlns:pkg=""http://schemas.microsoft.com/office/2006/xmlPackage""><pkg:part pkg:name=""/_rels/.rels"" pkg:contentType=""application/vnd.openxmlformats-package.relationships+xml"" " & _
                "pkg:padding=""512""><pkg:xmlData><Relationships xmlns=""http://schemas.openxmlformats.org/package/2006/relationships""><Relationship Id=""rId1"" " & _
                "Type=""http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"" Target=""word/document.xml""/></Relationships></pkg:xmlData></pkg:part>" & _
                "<pkg:part pkg:name=""/word/document.xml"" pkg:contentType=""application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml""><pkg:xmlData>"
            xDocNode = xXML & xDocNode & "</w:document></pkg:xmlData></pkg:part>"

            'get w:styles nodes
            If BaseMethods.IsWordOpenXML(xSegmentXML) Then
                'segment design xml is open xml - locate the correct part - there are also
                'w:styles nodes in the /word/styleswitheffects, /word/glossary/styles, and
                '/word/glossary/styleswitheffects parts
                lPos = InStr(xSegmentXML, "<pkg:part pkg:name=""/word/styles.xml""")
            Else
                'segment design is straight xml - there's only one w:styles node
                lPos = 1
            End If
            lPos = InStr(lPos, xSegmentXML, "<w:styles")
            lPos = InStr(lPos, xSegmentXML, ">") + 1
            lPos2 = InStr(lPos, xSegmentXML, "</w:styles>")
            xStylesNode = Mid$(xSegmentXML, lPos, lPos2 - lPos)

            'GLOG 2115 (dm) - strip Normal style line spacing node - it's not required
            'and can cause problems when the scope is being restored immediately above a table
            xStylesNode = StripNormalStyleLineSpacing(xStylesNode)

            'wrap in generic xml for styles part - the segment design xml may be straight xml,
            'so add auxiliary hint namespace
            '7/18/11 (dm) - add relationships for embedded parts
            xStylesNode = "<pkg:part pkg:name=""/word/_rels/document.xml.rels"" pkg:contentType=""application/vnd.openxmlformats-package.relationships+xml"" pkg:padding=""256""><pkg:xmlData><Relationships " & _
                "xmlns=""http://schemas.openxmlformats.org/package/2006/relationships""><Relationship Id=""rId1"" Type=""http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"" " & _
                "Target=""styles.xml""/>" & xRelationships & "</Relationships></pkg:xmlData></pkg:part>" & _
                "<pkg:part pkg:name=""/word/styles.xml"" pkg:contentType=""application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml""><pkg:xmlData><w:styles xmlns:r=""http://schemas.openxmlformats.org/officeDocument/2006/relationships"" " & _
                "xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"" xmlns:wx=""http://schemas.microsoft.com/office/word/2003/auxHint"">" & xStylesNode & "</w:styles></pkg:xmlData></pkg:part>"

            'concatenate and add closing tag
            '7/18/11 (dm) - add embedded parts
            GetInsertionXML_CC = xDocNode & xStylesNode & xParts & "</pkg:package>"

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub Restore_Distributed_CC(ByVal oSegmentBoundingObject As Object, _
                                          ByVal xVariableName As String, _
                                          ByVal bAsBlock As Boolean, _
                                          ByVal iScope As mpDeleteScopes, _
                                          ByVal xSegmentXML As String, _
                                          ByRef xVariablesAdded As String, _
                                          ByRef oTags As Tags, _
                                          ByRef rngmDels() As Word.Range, _
                                          ByRef oAssociatedCCs As Object, _
                                          ByRef xValuesArray As Object, _
                                          ByVal bIgnoreDeleteScope As Boolean)
            'reinserts deleted scope for a distributed detail variable, skipping
            'any empty values (to avoid immediate redeletion) - otherwise, this is
            'the same as Restore()
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Restore_Distributed_CC"
            Dim xXML As String = ""
            Dim oAssociatedCC As Word.ContentControl
            Dim oInsertedCC As Word.ContentControl
            Dim i As Short = 0
            Dim iCount As Short = 0
            Dim rngmDel As Word.Range = Nothing
            Dim bIsEmpty As Boolean = False
            Dim xText As String = ""
            Dim xBodyXML As String = ""
            Dim oWordDoc As WordDoc
            Dim xDeletedScopes As String = ""
            Dim oDoc As Word.Document
            Dim oSegmentCC As Word.ContentControl
            Dim oSegmentBmk As Word.Bookmark = Nothing
            Dim oSegmentRng As Word.Range = Nothing
            Dim xBoundingObjectTag As String = ""

            On Error GoTo ProcError
            oWordDoc = New WordDoc

            On Error Resume Next
            oSegmentCC = oSegmentBoundingObject

            If Err.Number = 13 Then
                'type mismatch - the supplied bounding object
                'was a bookmark
                On Error GoTo ProcError
                oSegmentBmk = oSegmentBoundingObject

                oSegmentRng = oSegmentBmk.Range
                xBoundingObjectTag = Mid$(oSegmentBmk.Name, 2)
                oDoc = oSegmentBmk.Range.Document
            Else
                'the supplied bounding object
                'was a content control
                On Error GoTo ProcError
                oSegmentRng = oSegmentCC.Range
                xBoundingObjectTag = oSegmentCC.Tag
                oDoc = oSegmentRng.Document
            End If

            On Error GoTo ProcError

            'store clipboard contents
            Clipboard.SaveClipboard()

            If xVariableName = "" Then
                'raise error
                Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                    "<Error_MissingDeleteScopeXML>")
            End If

            'get insertion xml
            xXML = GetInsertionXML_CC(xBoundingObjectTag, oSegmentRng, xVariableName, _
                xSegmentXML, xBodyXML)

            For i = 0 To UBound(oAssociatedCCs)
                oAssociatedCC = oAssociatedCCs(i)
                If UBound(xValuesArray) < i Then
                    bIsEmpty = True
                Else
                    bIsEmpty = (InStr(xValuesArray(i), "<w:t>") = 0)
                End If
                If (BaseMethods.GetBaseName(oAssociatedCC) = "mDel") And _
                        ((Not bIsEmpty) Or bIgnoreDeleteScope) Then
                    If (xXML = "") And (iCount = 0) Then
                        'no xml - raise error
                        Err.Raise(mpErrors.mpError_InvalidDeleteScope, , _
                            "<Error_MissingDeleteScopeXML>" & xVariableName)
                    End If
                    iCount = iCount + 1

                    'get mDel and clear out text
                    rngmDel = oAssociatedCC.Range
                    rngmDel.Text = ""

                    'add mDel location to byref array
                    rngmDels(i) = rngmDel

                    'restore scope
                    oInsertedCC = RestoreScope_CC(oAssociatedCC, oSegmentBoundingObject, xVariableName, _
                        bAsBlock, iScope, xXML, xVariablesAdded, oTags, xBodyXML)

                    'add restored mVar to byref array
                    oAssociatedCCs(i) = oInsertedCC
                End If
            Next i

            'GLOG 6328 (dm) - if design mode, remove delete scope from mSeg
            If bIgnoreDeleteScope Then
                xDeletedScopes = BaseMethods.GetAttributeValue(xBoundingObjectTag, "DeletedScopes", oDoc)
                DeleteDefinition(xDeletedScopes, xVariableName)
                BaseMethods.SetAttributeValue(xBoundingObjectTag, "DeletedScopes", xDeletedScopes, "", oDoc)

                'update Tag in tags collection
                oTags.Update_CC(oSegmentCC)
            End If

            'restore clipboard contents
            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function ConvertToOpenXml(ByVal xXML As String, ByVal oDoc As Word.Document) As String
            'converts DeletedScopes attribute containing xml tags to open xml/content controls
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.ConvertTompFileFormats.OpenXml"
            Dim xPassword As String = ""
            Dim xOpenXml As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xBodyXML As String = ""
            Dim xXMLMod As String = ""

            On Error GoTo ProcError

            'exit if xml is empty
            If xXML = "" Then _
                Exit Function

            'decrypt
            xXMLMod = BaseMethods.Decrypt(xXML, xPassword)

            'exit if xml is already in open xml format
            If InStr(xXMLMod, "http://schemas.openxmlformats.org/wordprocessingml/2006/main") > 0 Then
                'GLOG 15810: was not returning a value previously
                ConvertToOpenXml = xXML
                Exit Function
            End If

            'convert each deleted scope
            lPos = 1
            While lPos <> 0
                'replace w:wordDocument opening tag with w:document opening tag -
                'add legacy wx and wsp namespaces
                lPos2 = InStr(lPos, xXMLMod, "�") + 1
                xOpenXml = xOpenXml & Mid$(xXMLMod, lPos, lPos2 - lPos)
                xOpenXml = xOpenXml & "�w:document xmlns:ve=""http://schemas.openxmlformats.org/markup-compatibility/2006"" " & _
                    "xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:r=""http://schemas.openxmlformats.org/officeDocument/2006/relationships"" " & _
                    "xmlns:m=""http://schemas.openxmlformats.org/officeDocument/2006/math"" xmlns:v=""urn:schemas-microsoft-com:vml"" " & _
                    "xmlns:wp=""http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"" xmlns:w10=""urn:schemas-microsoft-com:office:word"" " & _
                    "xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"" xmlns:wne=""http://schemas.microsoft.com/office/word/2006/wordml"" " & _
                    "xmlns:wx=""http://schemas.microsoft.com/office/word/2003/auxHint"" xmlns:wsp=""http://schemas.microsoft.com/office/word/2003/wordml/sp2"" " & _
                    "xmlns:aml=""http://schemas.microsoft.com/aml/2001/core"">"

                'get body
                lPos2 = InStr(lPos2, xXMLMod, "�w:body>")
                lPos3 = InStr(lPos2, xXMLMod, "�/w:body>") + 10
                xBodyXML = Mid$(xXMLMod, lPos2, lPos3 - lPos2)

                'replace tags with content controls
                TagsToContentControls(xBodyXML, oDoc, xPassword)

                'concatenate
                xOpenXml = xOpenXml & xBodyXML

                'search for next scope
                lPos = InStr(lPos2, xXMLMod, "|DeletedTag=")
                If lPos <> 0 Then _
                    lPos = lPos + 1
            End While

            ConvertToOpenXml = xOpenXml

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub TagsToContentControls(ByRef xBodyXML As String, ByVal oDoc As Word.Document, _
            ByVal xEncryptionPassword As String)
            'replaces xml tags in xBodyXML with content controls
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.TagsToContentControls"
            Dim xAttributesXML As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim xBaseName As String = ""
            Dim xTag As String = ""
            Dim xDocVarID As String = ""
            Dim bIsEmpty As Boolean = False
            Dim xCCXML As String = ""
            Dim oXML As xml.xmlDocument
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim i As Short = 0
            Dim j As Short = 0
            Dim oAttribute As Xml.XmlAttribute
            Dim xTagID As String = ""
            Dim xAttribute As String = ""
            Dim xAttributes As String = ""
            Dim xFormatting As String = ""

            On Error GoTo ProcError

            lPos = InStr(xBodyXML, "�ns0:")
            While lPos <> 0
                'generate doc var id
                xDocVarID = BaseMethods.GenerateDocVarID(oDoc)

                'move past namespace
                lPos = lPos + 5

                'get tag type
                lPos2 = InStr(lPos, xBodyXML, " ")
                xBaseName = Mid$(xBodyXML, lPos, lPos2 - lPos)

                'get xml tag attributes, adding attribute for doc var id
                lPos2 = InStr(lPos, xBodyXML, ">")
                lPos3 = InStr(lPos, xBodyXML, "/>")
                bIsEmpty = ((lPos3 < lPos2) And (lPos3 <> 0))
                If bIsEmpty Then _
                    lPos2 = lPos3
                xAttributes = Mid$(xBodyXML, lPos, lPos2 - lPos)

                'check for an existing tag - this will be there if
                'deleted scopes have been preconverted
                lPos2 = InStr(xAttributes, " Reserved=" & Chr(34) & "mp") 'GLOG 5265 (dm)
                If lPos2 <> 0 Then
                    'preconverted - use existing tag
                    lPos2 = lPos2 + 11 'GLOG 5265 (dm)
                    lPos3 = InStr(lPos2, xAttributes, Chr(34))
                    xTag = Mid$(xAttributes, lPos2, lPos3 - lPos2)
                Else
                    'not preconverted - generate doc var id
                    xDocVarID = BaseMethods.GenerateDocVarID(oDoc)

                    'get attributes
                    xAttributesXML = xAttributesXML & "<" & xAttributes & _
                        " DocVarID=""" & xDocVarID & """ BaseName=""" & xBaseName & """/>"

                    'create tag for content control
                    Select Case xBaseName
                        Case "mSEG"
                            xTag = "mps"
                        Case "mVar"
                            xTag = "mpv"
                        Case "mBlock"
                            xTag = "mpb"
                        Case "mDel"
                            xTag = "mpd"
                        Case "mSubVar"
                            xTag = "mpu"
                        Case "mDocProps"
                            xTag = "mpp"
                        Case "mSecProps"
                            xTag = "mpc"
                    End Select
                    xTag = xTag & xDocVarID & BaseMethods.CreatePadString(39 - Len(xTag), "0")
                End If

                'replace custom xml tag opening with w:sdt opening
                xCCXML = "�w:sdt>�w:sdtPr>"

                'if this is an mVar, add <w:rPr> node to ensure that its formatting matches
                'that of the surrounding text when the value of the variable is updated
                If xBaseName = "mVar" Then
                    lPos2 = InStrRev(xBodyXML, "�w:rPr>", lPos)
                    If lPos2 > 0 Then
                        'preceding <w:rPr> node found - apply to content control itself
                        lPos3 = InStr(lPos2, xBodyXML, "�/w:rPr>") + 8
                        xFormatting = Mid$(xBodyXML, lPos2, lPos3 - lPos2)
                        xCCXML = xCCXML & xFormatting
                    End If
                End If

                'add tag node
                xCCXML = xCCXML & "�w:tag w:val=""" & xTag & """/>�/w:sdtPr>"

                If bIsEmpty Then
                    'close w:sdt as well
                    lPos2 = InStr(lPos, xBodyXML, "/>") + 2
                    xCCXML = xCCXML & "�w:sdtContent/>�/w:sdt>"
                Else
                    'begin w:sdtContent node
                    lPos2 = InStr(lPos, xBodyXML, ">") + 1
                    xCCXML = xCCXML & "�w:sdtContent>"
                End If
                xBodyXML = Left$(xBodyXML, lPos - 6) & xCCXML & Mid$(xBodyXML, lPos2)

                'search for next tag
                lPos = InStr(xBodyXML, "�ns0:")
            End While

            'replace custom xml closing tags with w:sdt closing
            lPos = InStr(xBodyXML, "�/ns0:")
            While lPos <> 0
                lPos2 = InStr(lPos, xBodyXML, ">")
                xBodyXML = Left$(xBodyXML, lPos - 1) & "�/w:sdtContent>�/w:sdt>" & _
                    Mid$(xBodyXML, lPos2 + 1)
                lPos = InStr(xBodyXML, "�/ns0:")
            End While

            'preserve spaces
            xBodyXML = Replace(xBodyXML, "�w:t>", "�w:t xml:space=" & Chr(34) & _
                "preserve" & Chr(34) & ">")

            'create doc vars for attributes
            If xAttributesXML <> "" Then
                xAttributesXML = "<mpNodes>" & xAttributesXML & "</mpNodes>"
                oXML = New xml.xmlDocument
                oXML.loadXML(xAttributesXML)
                oNodeList = oXML.selectNodes("mpNodes/*")
                For i = 0 To oNodeList.Count - 1
                    oNode = oNodeList.Item(i)

                    'get tag type
                    oAttribute = oNode.Attributes.GetNamedItem("BaseName")
                    xBaseName = oAttribute.Value

                    'get doc var name
                    oAttribute = oNode.Attributes.GetNamedItem("DocVarID")
                    xDocVarID = oAttribute.Value

                    'construct doc var value
                    If xBaseName <> "mSubVar" Then
                        oAttribute = oNode.Attributes.GetNamedItem("TagID")
                    Else
                        oAttribute = oNode.Attributes.GetNamedItem("Name")
                    End If
                    xTagID = oAttribute.Value & "��"

                    'the rest of the value is encrypted
                    xAttributes = ""
                    With oNode.Attributes
                        For j = 0 To .Count - 1
                            oAttribute = .Item(j)
                            xAttribute = oAttribute.Name
                            If (xAttribute <> "TagID") And (xAttribute <> "Name") And _
                                    (xAttribute <> "DeletedScopes") And _
                                    (xAttribute <> "DocVarID") And (xAttribute <> "BaseName") Then
                                xAttributes = xAttributes & xAttribute & "=" & _
                                    oAttribute.Value & "��"
                            End If
                        Next j
                    End With

                    'strip trailing separator
                    If xAttributes <> "" Then _
                        xAttributes = Left$(xAttributes, Len(xAttributes) - 2)

                    'encrypt
                    If (xBaseName <> "mDel") And (xBaseName <> "mSubVar") Then _
                        xAttributes = BaseMethods.Encrypt(xAttributes, xEncryptionPassword)

                    'add variable
                    oDoc.Variables.Add("mpo" & xDocVarID, xTagID & xAttributes)

                    'create mpd variable
                    If xBaseName = "mSEG" Then
                        oAttribute = oNode.Attributes.GetNamedItem("DeletedScopes")
                        If Not oAttribute Is Nothing Then
                            oDoc.Variables.Add("mpd" & xDocVarID & "01", oAttribute.Value)
                        End If
                    End If
                Next i
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function AllowManualDeletion_CC(ByVal omDel As Word.ContentControl, _
                                               ByVal rngSelection As Word.Range) As Boolean
            'returns TRUE if entire context for mDel is currently selected, allowing us
            'to assume that user would want mDel to be deleted if selection is deleted
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.AllowManualDeletion_CC"
            Dim xObjectData As String = ""
            Dim oProps As Object = Nothing
            Dim bNativeToTable As Boolean = False
            Dim xTagID As String = ""
            Dim omSEG As Word.ContentControl
            Dim iReinsertionLocation As mpReinsertionLocations
            Dim rngmDel As Word.Range = Nothing
            Dim rngmSEG As Word.Range = Nothing
            Dim rngCell As Word.Range = Nothing
            Dim rngParagraph As Word.Range = Nothing
            Dim iCount As Short = 0
            Dim lSelectionStart As Integer = 0
            Dim lSelectionEnd As Integer = 0
            Dim oFirstPara As Word.Paragraph
            Dim omBlock As Word.ContentControl
            Dim rngmBlock As Word.Range = Nothing
            Dim xBlockTagID As String = ""
            Dim xTag As String = ""
            Dim oWordDoc As WordDoc
            Dim oParent As Word.ContentControl

            On Error GoTo ProcError

            oWordDoc = New WordDoc

            lSelectionStart = rngSelection.Start
            lSelectionEnd = rngSelection.End
            rngmDel = omDel.Range
            rngParagraph = rngmDel.Paragraphs(1).Range

            'adjust for tags at start of first paragraph of selection
            oFirstPara = rngSelection.Paragraphs(1)
            iCount = BaseMethods.CountTagsAtStartOfParagraph(oFirstPara)
            If lSelectionStart - oFirstPara.Range.Start <= iCount Then _
                lSelectionStart = oFirstPara.Range.Start

            'get reinsertion location and other properties
            xTag = omDel.Tag
            xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData")

            'GLOG : 8085 : ceh - account for missing doc vars
            If xObjectData = "" Then
                AllowManualDeletion_CC = True
                Exit Function
            End If

            oProps = Split(xObjectData, "|")
            iReinsertionLocation = CInt(oProps(1))
            If UBound(oProps) > 1 Then
                If rngmDel.Tables.Count > 0 Then _
                    bNativeToTable = CBool(oProps(2))
                xBlockTagID = oProps(3)
            End If

            'get parent mSEG
            oParent = omDel.ParentContentControl
            Do While Not oParent Is Nothing
                If BaseMethods.GetBaseName(oParent) = "mSEG" Then
                    'validate - mDel may be in child of it's true parent
                    xTagID = BaseMethods.GetAttributeValue(oParent.Tag, "TagID")
                    If xTagID = oProps(0) Then
                        omSEG = oParent
                        Exit Do
                    End If
                End If
                If Not oParent Is Nothing Then _
                        oParent = oParent.ParentContentControl
            Loop

            'allow if parent mSEG is being deleted
            If Not omSEG Is Nothing Then
                rngmSEG = omSEG.Range
                If (lSelectionStart <= rngmSEG.Start) And (lSelectionEnd >= rngmSEG.End) Then
                    AllowManualDeletion_CC = True
                    Exit Function
                End If
            Else
                'GLOG 7813 (dm) - added support for bookmark mSEGs - however, due to an
                'intractable error in Word 2010 when deleting the empty paragraph with
                'the three mDels at the end of letter, I'm currently allowing the
                'deletion when the selection includes the start OR end of the mSEG -
                'when attempting to preserve in this case, the delete key warning
                'was appearing and, if you answered yes, an HResult_E_Fail error
                'occurred - the mDels were getting deleted in any case, even in
                'Word 2013 where there's no keyboard handler
                Dim oBmk As Word.Bookmark = Nothing
                oBmk = oWordDoc.GetParentSegmentBookmarkFromRange(omDel.Range)
                If Not oBmk Is Nothing Then
                    rngmSEG = oBmk.Range
                    If (lSelectionStart <= rngmSEG.Start) Or (lSelectionEnd >= rngmSEG.End) Then
                        AllowManualDeletion_CC = True
                        Exit Function
                    End If
                Else
                    AllowManualDeletion_CC = True
                    Exit Function
                End If
            End If

            'allow if mDel belongs to a block and the block is being deleted
            If xBlockTagID <> "" Then
                oParent = omDel.ParentContentControl
                Do While Not oParent Is Nothing
                    If BaseMethods.GetBaseName(oParent) = "mBlock" Then
                        'find parent block
                        xTagID = BaseMethods.GetAttributeValue(oParent.Tag, "TagID")
                        If xTagID = xBlockTagID Then
                            omBlock = oParent
                            Exit Do
                        End If
                    End If
                    If Not oParent Is Nothing Then _
                            oParent = oParent.ParentContentControl
                Loop

                If Not omBlock Is Nothing Then
                    rngmBlock = omBlock.Range
                    If (lSelectionStart <= rngmBlock.Start) And _
                            (lSelectionEnd >= rngmBlock.End) Then
                        AllowManualDeletion_CC = True
                        Exit Function
                    End If
                End If
            End If

            'allow if context is being deleted
            If (rngmSEG.End > rngParagraph.End) And ((rngParagraph.Start < rngmSEG.Start) Or _
                    (iReinsertionLocation = mpReinsertionLocations.mpReinsertionLocation_After)) Then
                'the mDel is not in the last paragraph of the mSEG and it's either
                '1) in the first paragraph or 2) set for reinsertion in the next paragraph -
                'allow if the selection includes the next paragraph as well
                If lSelectionEnd >= rngmDel.Next(Word.WdUnits.wdParagraph).End Then
                    AllowManualDeletion_CC = True
                    Exit Function
                End If
            ElseIf rngmSEG.Start < rngParagraph.Start Then
                'the mDel is not in the first paragraph of the mSEG -
                'allow if the selection includes the previous paragraph as well
                If lSelectionStart <= rngmDel.Previous(Word.WdUnits.wdParagraph).Start Then
                    AllowManualDeletion_CC = True
                    Exit Function
                End If
            End If

            'allow if cell is being deleted
            If bNativeToTable Then
                rngCell = rngmDel.Cells(1).Range
                AllowManualDeletion_CC = ((lSelectionStart <= rngCell.Start) And _
                    (lSelectionEnd >= rngCell.End))
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function GetAssociatedDocVars(ByVal xDeletedScopes As String) As String
            'returns a pipe-delimited string containing the doc vars associated with
            'the tags or ccs embedded in the DeletedScopes attribute
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetAssociatedDocVars"
            Dim xVars As String = ""
            Dim xSearch As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xTag As String = ""

            On Error GoTo ProcError

            'content controls
            xSearch = "�w:tag w:val="
            lPos = InStr(xDeletedScopes, xSearch)
            '1/17/11 (dm) - ccs and xml tags are no longer mutually exclusive - we always
            'need to search for both - see below
            '    If lPos = 0 Then
            '        'xml tags
            '        xSearch = " Reserved="
            '        lPos = InStr(xDeletedScopes, xSearch)
            '    End If
            While lPos <> 0
                lPos = lPos + Len(xSearch) + 1
                lPos2 = InStr(lPos, xDeletedScopes, Chr(34))
                xTag = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                xVars = xVars & "|" & "mpo" & Mid$(xTag, 4, 8)
                lPos = InStr(lPos, xDeletedScopes, xSearch)
            End While

            'xml tags
            xSearch = " Reserved="
            lPos = InStr(xDeletedScopes, xSearch)
            While lPos <> 0
                lPos = lPos + Len(xSearch) + 1
                lPos2 = InStr(lPos, xDeletedScopes, Chr(34))
                xTag = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                xVars = xVars & "|" & "mpo" & Mid$(xTag, 4, 8)
                lPos = InStr(lPos, xDeletedScopes, xSearch)
            End While

            If xVars <> "" Then _
                xVars = Mid$(xVars, 2)

            GetAssociatedDocVars = xVars

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub AddBookmarks(ByRef xXML As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.AddBookmarks"
            Dim xSearch As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim lPos4 As Integer = 0
            Dim xTag As String = ""
            Dim lCount As Integer = 0
            Dim xContent As String = ""
            Dim bContainsPara As Boolean = False
            Dim xBookmark As String = ""
            Dim iStartCount As Short = 0
            Dim iEndCount As Short = 0

            On Error GoTo ProcError

            'delete any existing WordML bookmarks
            xSearch = "<aml:annotation "
            lPos = InStr(xXML, xSearch)
            While lPos <> 0
                lPos2 = InStr(lPos, xXML, "/>")
                xXML = Left$(xXML, lPos - 1) & Mid$(xXML, lPos2 + 2)
                lPos = InStr(xXML, xSearch)
            End While

            'GLOG 7369 (dm) - delete any existing open xml bookmarks as well -
            'skipping these was throwing off the indexing
            xSearch = "<w:bookmark"
            lPos = InStr(xXML, xSearch)
            While lPos <> 0
                lPos2 = InStr(lPos, xXML, "/>")
                xXML = Left$(xXML, lPos - 1) & Mid$(xXML, lPos2 + 2)
                lPos = InStr(xXML, xSearch)
            End While

            'replace empty sdtContent nodes
            xXML = Replace(xXML, "<w:sdtContent/>", "<w:sdtContent></w:sdtContent>")

            'cycle through content controls
            xSearch = "<w:tag w:val="
            lPos = InStr(xXML, xSearch)
            While lPos <> 0
                'get tag
                lPos = lPos + Len(xSearch) + 1
                lPos2 = InStr(lPos, xXML, Chr(34))
                xTag = Mid$(xXML, lPos, lPos2 - lPos)

                'add bookmark if it doesn't already exist
                If InStr(xXML, "_" & xTag) = 0 Then
                    'get content node
                    lPos = InStr(lPos, xXML, "<w:sdtContent>") + 14
                    lPos4 = lPos
                    iStartCount = 1
                    iEndCount = 0
                    While iStartCount > iEndCount
                        lPos2 = InStr(lPos4, xXML, "<w:sdtContent>")
                        lPos3 = InStr(lPos4, xXML, "</w:sdtContent>")
                        If (lPos2 > 0) And (lPos2 < lPos3) Then
                            iStartCount = iStartCount + 1
                            lPos4 = lPos2 + 1
                        Else
                            iEndCount = iEndCount + 1
                            lPos4 = lPos3 + 1
                        End If
                    End While
                    xContent = Mid$(xXML, lPos, lPos3 - lPos)

                    'determine whether content control is paragraph level
                    bContainsPara = (InStr(xContent, "<w:p>") <> 0)
                    If Not bContainsPara Then _
                        bContainsPara = (InStr(xContent, "<w:p >") <> 0)
                    If Not bContainsPara Then _
                        bContainsPara = (InStr(xContent, "</w:p>") <> 0)
                    If Not bContainsPara Then _
                        bContainsPara = (InStr(xContent, "</w:p >") <> 0)

                    'add bookmark end node
                    xBookmark = "<w:bookmarkEnd w:id=" & Chr(34) & CStr(lCount) & Chr(34)
                    If bContainsPara Then
                        'paragraph level - add w:displacedByCustomXml
                        xBookmark = xBookmark & " w:displacedByCustomXml=" & Chr(34) & _
                            "next" & Chr(34)
                    End If
                    xBookmark = xBookmark & "/>"
                    xXML = Left$(xXML, lPos3 - 1) & xBookmark & Mid$(xXML, lPos3)

                    'add bookmark start node
                    xBookmark = "<w:bookmarkStart w:id=" & Chr(34) & CStr(lCount) & _
                        Chr(34) & " w:name=" & Chr(34) & "_" & xTag & Chr(34)
                    If bContainsPara Then
                        'paragraph level - add w:displacedByCustomXml
                        xBookmark = xBookmark & " w:displacedByCustomXml=" & Chr(34) & _
                            "prev" & Chr(34)
                    End If
                    xBookmark = xBookmark & "/>"
                    xXML = Left$(xXML, lPos - 1) & xBookmark & Mid$(xXML, lPos)
                End If

                lPos = InStr(lPos, xXML, xSearch)
                lCount = lCount + 1
            End While

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function Preconvert(ByVal xXML As String, ByVal oDoc As Word.Document) As String
            'preconverts DeletedScopes attribute
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.Preconvert"
            Dim xAttributesXML As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim lPos3 As Integer = 0
            Dim lPos4 As Integer = 0
            Dim xBaseName As String = ""
            Dim xTag As String = ""
            Dim xDocVarID As String = ""
            Dim bIsEmpty As Boolean = False
            Dim oXML As xml.xmlDocument
            Dim oNode As xml.xmlNode
            Dim oNodeList As xml.xmlNodeList
            Dim i As Short = 0
            Dim j As Short = 0
            Dim oAttribute As xml.xmlAttribute
            Dim xTagID As String = ""
            Dim xAttribute As String = ""
            Dim xPreconvertedXML As String = ""
            Dim xAttributes As String = ""
            Dim iStartCount As Short = 0
            Dim iEndCount As Short = 0
            Dim xRange As String = ""
            Dim bContainsPara As Boolean = False
            Dim xBookmark As String = ""
            Dim lCount As Integer = 0

            On Error GoTo ProcError

            xPreconvertedXML = xXML

            lPos = InStr(xPreconvertedXML, "�ns0:")
            While lPos <> 0
                'generate doc var id
                xDocVarID = BaseMethods.GenerateDocVarID(oDoc)

                'move past namespace
                lPos = lPos + 5

                'get tag type
                lPos2 = InStr(lPos, xPreconvertedXML, " ")
                xBaseName = Mid$(xPreconvertedXML, lPos, lPos2 - lPos)

                'get xml tag attributes, adding attribute for doc var id
                lPos2 = InStr(lPos, xPreconvertedXML, ">")
                lPos3 = InStr(lPos, xPreconvertedXML, "/>")
                bIsEmpty = ((lPos3 < lPos2) And (lPos3 <> 0))
                If bIsEmpty Then _
                    lPos2 = lPos3
                xAttributes = Mid$(xPreconvertedXML, lPos, lPos2 - lPos)

                'exit if already preconverted
                lPos3 = InStr(xAttributes, " Reserved=" & Chr(34) & "mp")
                If lPos3 <> 0 Then
                    Preconvert = xXML
                    Exit Function
                End If

                'generate doc var id
                xDocVarID = BaseMethods.GenerateDocVarID(oDoc)

                'get attributes
                xAttributesXML = xAttributesXML & "<" & xAttributes & _
                    " DocVarID=""" & xDocVarID & """ BaseName=""" & xBaseName & """/>"

                'create tag
                Select Case xBaseName
                    Case "mSEG"
                        xTag = "mps"
                    Case "mVar"
                        xTag = "mpv"
                    Case "mBlock"
                        xTag = "mpb"
                    Case "mDel"
                        xTag = "mpd"
                    Case "mSubVar"
                        xTag = "mpu"
                    Case "mDocProps"
                        xTag = "mpp"
                    Case "mSecProps"
                        xTag = "mpc"
                End Select
                xTag = xTag & xDocVarID & BaseMethods.CreatePadString(39 - Len(xTag), "0")

                'add reserved attribute
                xPreconvertedXML = Left$(xPreconvertedXML, lPos2 - 1) & " Reserved=" & Chr(34) & _
                    xTag & Chr(34) & Mid$(xPreconvertedXML, lPos2)

                'add bookmark
                If bIsEmpty Then
                    'replace abbreviated end tag with full end tag
                    lPos = InStr(lPos2, xPreconvertedXML, "/>")
                    xPreconvertedXML = Left$(xPreconvertedXML, lPos - 1) & ">�/ns0:" & xBaseName & ">" & Mid$(xPreconvertedXML, lPos + 2)
                Else
                    lPos = InStr(lPos2, xPreconvertedXML, ">")
                End If

                'get tag range
                lPos = lPos + 1
                lPos4 = lPos
                iStartCount = 1
                iEndCount = 0
                While iStartCount > iEndCount
                    lPos2 = InStr(lPos4, xPreconvertedXML, "�ns0:")
                    lPos3 = InStr(lPos4, xPreconvertedXML, "�/ns0:")
                    If (lPos2 > 0) And (lPos2 < lPos3) Then
                        iStartCount = iStartCount + 1

                        'if empty, increment end node count as well
                        lPos3 = InStr(lPos2, xPreconvertedXML, ">")
                        lPos4 = InStr(lPos2, xPreconvertedXML, "/>")
                        If (lPos4 < lPos3) And (lPos4 <> 0) Then _
                            iEndCount = iEndCount + 1

                        lPos4 = lPos2 + 1
                    Else
                        iEndCount = iEndCount + 1
                        lPos4 = lPos3 + 1
                    End If
                End While
                xRange = Mid$(xPreconvertedXML, lPos, lPos3 - lPos)

                'determine whether tag is paragraph level
                bContainsPara = (InStr(xRange, "�w:p>") <> 0)
                If Not bContainsPara Then _
                    bContainsPara = (InStr(xRange, "�w:p >") <> 0)
                If Not bContainsPara Then _
                    bContainsPara = (InStr(xRange, "�/w:p>") <> 0)
                If Not bContainsPara Then _
                    bContainsPara = (InStr(xRange, "�/w:p >") <> 0)

                'add bookmark end node
                xBookmark = "�aml:annotation aml:id=" & Chr(34) & CStr(lCount) & Chr(34) & _
                    " w:type=" & Chr(34) & "Word.Bookmark.End" & Chr(34)
                If bContainsPara Then
                    'paragraph level - add w:displacedByCustomXml
                    xBookmark = xBookmark & " w:displacedBySDT=" & Chr(34) & _
                        "next" & Chr(34)
                End If
                xBookmark = xBookmark & "/>"
                xPreconvertedXML = Left$(xPreconvertedXML, lPos3 - 1) & xBookmark & Mid$(xPreconvertedXML, lPos3)

                'add bookmark start node
                xBookmark = "�aml:annotation aml:id=" & Chr(34) & CStr(lCount) & Chr(34) & _
                    " w:type=" & Chr(34) & "Word.Bookmark.Start" & Chr(34) & " w:name=" & _
                    Chr(34) & "_" & xTag & Chr(34)
                If bContainsPara Then
                    'paragraph level - add w:displacedByCustomXml
                    xBookmark = xBookmark & " w:displacedBySDT=" & Chr(34) & _
                        "prev" & Chr(34)
                End If
                xBookmark = xBookmark & "/>"
                xPreconvertedXML = Left$(xPreconvertedXML, lPos - 1) & xBookmark & Mid$(xPreconvertedXML, lPos)

                'get next tag
                lPos = InStr(lPos, xPreconvertedXML, "�ns0:")
                lCount = lCount + 1
            End While

            'create doc vars for attributes
            If xAttributesXML <> "" Then
                xAttributesXML = "<mpNodes>" & xAttributesXML & "</mpNodes>"
                oXML = New xml.xmlDocument
                oXML.loadXML(xAttributesXML)
                oNodeList = oXML.selectNodes("mpNodes/*")
                For i = 1 To oNodeList.Count
                    oNode = oNodeList.Item(0)

                    'get tag type
                    oAttribute = oNode.Attributes.GetNamedItem("BaseName")
                    xBaseName = oAttribute.value

                    'get doc var name
                    oAttribute = oNode.Attributes.GetNamedItem("DocVarID")
                    xDocVarID = oAttribute.value

                    'construct doc var value
                    If xBaseName <> "mSubVar" Then
                        oAttribute = oNode.Attributes.GetNamedItem("TagID")
                    Else
                        oAttribute = oNode.Attributes.GetNamedItem("Name")
                    End If
                    xTagID = oAttribute.value & "��"

                    'the rest of the value is encrypted
                    xAttributes = ""
                    With oNode.Attributes
                        For j = 0 To .Count - 1
                            oAttribute = .Item(j)
                            xAttribute = oAttribute.Name
                            If (xAttribute <> "TagID") And (xAttribute <> "Name") And _
                                    (xAttribute <> "DeletedScopes") And _
                                    (xAttribute <> "DocVarID") And (xAttribute <> "BaseName") Then
                                xAttributes = xAttributes & xAttribute & "=" & _
                                    oAttribute.value & "��"
                            End If
                        Next j
                    End With

                    'strip trailing separator
                    If xAttributes <> "" Then _
                        xAttributes = Left$(xAttributes, Len(xAttributes) - 2)

                    'encrypt
                    If (xBaseName <> "mDel") And (xBaseName <> "mSubVar") Then _
                        xAttributes = BaseMethods.Encrypt(xAttributes, "")

                    'add variable
                    oDoc.Variables.Add("mpo" & xDocVarID, xTagID & xAttributes)

                    'create mpd variable
                    If xBaseName = "mSEG" Then
                        oAttribute = oNode.Attributes.GetNamedItem("DeletedScopes")
                        If Not oAttribute Is Nothing Then
                            oDoc.Variables.Add("mpd" & xDocVarID & "01", oAttribute.value)
                        End If
                    End If
                Next i
            End If

            Preconvert = xPreconvertedXML

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function ConvertScopeToOpenXml(ByVal xScopeXML As String, _
                                               ByVal oDoc As Word.Document) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.ConvertScopeTompFileFormats.OpenXml"
            Dim xOpenXml As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xBodyXML As String = ""

            On Error GoTo ProcError

            xOpenXml = "�w:document xmlns:ve=""http://schemas.openxmlformats.org/markup-compatibility/2006"" " & _
                "xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:r=""http://schemas.openxmlformats.org/officeDocument/2006/relationships"" " & _
                "xmlns:m=""http://schemas.openxmlformats.org/officeDocument/2006/math"" xmlns:v=""urn:schemas-microsoft-com:vml"" " & _
                "xmlns:wp=""http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"" xmlns:w10=""urn:schemas-microsoft-com:office:word"" " & _
                "xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"" xmlns:wne=""http://schemas.microsoft.com/office/word/2006/wordml"" " & _
                "xmlns:wx=""http://schemas.microsoft.com/office/word/2003/auxHint"" xmlns:wsp=""http://schemas.microsoft.com/office/word/2003/wordml/sp2"" " & _
                "xmlns:aml=""http://schemas.microsoft.com/aml/2001/core"">"

            'get body
            lPos = InStr(xScopeXML, "�w:body>")
            lPos2 = InStr(lPos, xScopeXML, "�/w:body>") + 10
            xBodyXML = Mid$(xScopeXML, lPos, lPos2 - lPos)

            'replace tags with content controls
            TagsToContentControls(xBodyXML, oDoc, "")

            'concatenate
            ConvertScopeToOpenXml = xOpenXml & xBodyXML

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Function GetEmbeddedContent(ByVal xXML As String) As String
            'returns the relationship and part elements for content, e.g images,
            'embedded in the w:body element of the specified xml
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.GetEmbeddedContent"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xEmbedded As String = ""
            Dim xBodyXML As String = ""
            Dim xRelationshipsXML As String = ""
            Dim xID As String = ""
            Dim xRelationship As String = ""
            Dim xTarget As String = ""
            Dim xPart As String = ""
            Dim lEmbed As Integer = 0

            On Error GoTo ProcError

            'get body
            lPos = InStr(xXML, "<w:body>")
            lPos2 = InStr(lPos, xXML, "</w:body>")
            xBodyXML = Mid$(xXML, lPos, lPos2 - lPos)

            'get relationships
            lPos = InStr(xXML, "<pkg:part pkg:name=" & Chr(34) & _
                "/word/_rels/document.xml.rels" & Chr(34))
            lPos2 = InStr(lPos, xXML, "</pkg:part>")
            xRelationshipsXML = Mid$(xXML, lPos, lPos2 - lPos)

            'search for embedded content
            lEmbed = InStr(xBodyXML, " r:embed=")
            While lEmbed > 0
                'get relationship id
                lEmbed = lEmbed + 10
                lPos2 = InStr(lEmbed, xBodyXML, Chr(34))
                xID = Mid$(xBodyXML, lEmbed, lPos2 - lEmbed)

                'get relationship
                lPos = InStr(xRelationshipsXML, _
                    "<Relationship Id=" & Chr(34) & xID & Chr(34))
                lPos2 = InStr(lPos, xRelationshipsXML, "/>") + 2
                xRelationship = Mid$(xRelationshipsXML, lPos, lPos2 - lPos)

                'get name of target part
                lPos = InStr(xRelationship, "Target=") + 8
                lPos2 = InStr(lPos, xRelationship, Chr(34))
                xTarget = Mid$(xRelationship, lPos, lPos2 - lPos)
                'GLOG 8651: Relationships added by Oxml may use full path for the Target
                xTarget = xTarget.Replace("/word/", "")
                'get part
                lPos = InStr(xXML, "<pkg:part pkg:name=" & Chr(34) & "/word/" & _
                    xTarget & Chr(34))
                lPos2 = InStr(lPos, xXML, "</pkg:part>") + 11
                xPart = Mid$(xXML, lPos, lPos2 - lPos)

                'append relationship and part
                xEmbedded = xEmbedded & xRelationship & xPart

                'search for other embedded content
                lEmbed = InStr(lEmbed, xBodyXML, " r:embed=")
            End While

            '9/23/11 (dm) - add footnotes
            If InStr(xBodyXML, "<w:footnoteReference ") Then
                'get footnotes relationship
                lPos = InStr(xRelationshipsXML, _
                    " Target=" & Chr(34) & "footnotes.xml" & Chr(34))
                lPos = InStrRev(xRelationshipsXML, "<Relationship ", lPos)
                lPos2 = InStr(lPos, xRelationshipsXML, "/>") + 2
                xRelationship = Mid$(xRelationshipsXML, lPos, lPos2 - lPos)

                'get footnotes part
                lPos = InStr(xXML, "<pkg:part pkg:name=" & Chr(34) & _
                    "/word/footnotes.xml" & Chr(34))
                lPos2 = InStr(lPos, xXML, "</pkg:part>") + 11
                xPart = Mid$(xXML, lPos, lPos2 - lPos)

                'append relationship and part
                xEmbedded = xEmbedded & xRelationship & xPart
            End If

            '9/23/11 (dm) - add endnotes
            If InStr(xBodyXML, "<w:endnoteReference ") Then
                'get endnotes relationship
                lPos = InStr(xRelationshipsXML, _
                    " Target=" & Chr(34) & "endnotes.xml" & Chr(34))
                lPos = InStrRev(xRelationshipsXML, "<Relationship ", lPos)
                lPos2 = InStr(lPos, xRelationshipsXML, "/>") + 2
                xRelationship = Mid$(xRelationshipsXML, lPos, lPos2 - lPos)

                'get endnotes part
                lPos = InStr(xXML, "<pkg:part pkg:name=" & Chr(34) & _
                    "/word/endnotes.xml" & Chr(34))
                lPos2 = InStr(lPos, xXML, "</pkg:part>") + 11
                xPart = Mid$(xXML, lPos, lPos2 - lPos)

                'append relationship and part
                xEmbedded = xEmbedded & xRelationship & xPart
            End If

            'GLOG 15921 - Add comments
            If InStr(xBodyXML, "<w:commentReference ") Then
                'get comments relationship
                lPos = InStr(xRelationshipsXML, _
                    " Target=" & Chr(34) & "comments.xml" & Chr(34))
                lPos = InStrRev(xRelationshipsXML, "<Relationship ", lPos)
                lPos2 = InStr(lPos, xRelationshipsXML, "/>") + 2
                xRelationship = Mid$(xRelationshipsXML, lPos, lPos2 - lPos)

                'get comments part
                lPos = InStr(xXML, "<pkg:part pkg:name=" & Chr(34) & _
                    "/word/comments.xml" & Chr(34))
                lPos2 = InStr(lPos, xXML, "</pkg:part>") + 11
                xPart = Mid$(xXML, lPos, lPos2 - lPos)

                'append relationship and part
                xEmbedded = xEmbedded & xRelationship & xPart
            End If

            GetEmbeddedContent = xEmbedded

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub RetagDeletedScopes(ByVal xTag As String, ByVal oDoc As Word.Document, _
            Optional ByVal bDeleteOldDocVars As Boolean = False)
            'GLOG 6700 (dm) - modified to no longer require segment bounding object
            'GLOG 8206 (dm) - added bDeleteOldDocVars parameter
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RetagDeletedScopes"
            Dim xDeletedScopes As String = ""
            Dim xSearch As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xOldTag As String = ""
            Dim xOldID As String = ""
            Dim xNewID As String = ""
            Dim xNewTag As String = ""
            Dim xValue As String = ""

            On Error GoTo ProcError

            xDeletedScopes = BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDoc)
            If xDeletedScopes = "" Then _
                Exit Sub

            xSearch = "�w:tag w:val="
            lPos = InStr(xDeletedScopes, xSearch)
            While lPos <> 0
                lPos = lPos + Len(xSearch) + 1
                lPos2 = InStr(lPos, xDeletedScopes, Chr(34))
                xOldTag = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                xOldID = Mid$(xOldTag, 4, 8)

                'generate new doc var id
                xNewID = BaseMethods.GenerateDocVarID(oDoc)
                xNewTag = Left$(xOldTag, 3) & xNewID & Mid$(xOldTag, 12)

                'update tag and bookmark in xml
                xDeletedScopes = Replace(xDeletedScopes, xOldTag, xNewTag)

                'GLOG 8205:  Ignore error if old variable no longer exists
                On Error Resume Next
                'add new doc var with same value as original one
                xValue = oDoc.Variables("mpo" & xOldID).Value
                On Error GoTo ProcError
                If xValue <> "" Then
                    oDoc.Variables.Add("mpo" & xNewID, xValue)

                    'GLOG 8206 (dm) - delete old doc var if specified
                    If bDeleteOldDocVars Then
                        oDoc.Variables("mpo" & xOldID).Delete()
                    End If
                End If

                lPos = InStr(lPos, xDeletedScopes, xSearch)
            End While

            'update mpd doc var
            BaseMethods.SetAttributeValue(xTag, "DeletedScopes", _
                xDeletedScopes, "", oDoc)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub RetagDeletedScopes_XMLNode(ByVal oSegmentNode As Word.XMLNode)
            'GLOG 6700 (dm) - added for .doc support
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.RetagDeletedScopes_XMLNode"
            Dim xTag As String = ""
            Dim xDeletedScopes As String = ""
            Dim xSearch As String = ""
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xOldTag As String = ""
            Dim xOldID As String = ""
            Dim xNewID As String = ""
            Dim xNewTag As String = ""
            Dim xValue As String = ""
            Dim oDoc As Word.Document
            Dim oWordDoc As WordDoc

            On Error GoTo ProcError

            xTag = BaseMethods.GetTag(oSegmentNode)
            oDoc = oSegmentNode.Range.Document
            xDeletedScopes = BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oDoc)
            If xDeletedScopes = "" Then _
                Exit Sub

            xSearch = " Reserved="
            lPos = InStr(xDeletedScopes, xSearch)
            While lPos <> 0
                lPos = lPos + Len(xSearch) + 1
                lPos2 = InStr(lPos, xDeletedScopes, Chr(34))
                xOldTag = Mid$(xDeletedScopes, lPos, lPos2 - lPos)
                xOldID = Mid$(xOldTag, 4, 8)

                'generate new doc var id
                xNewID = BaseMethods.GenerateDocVarID(oDoc)
                xNewTag = Left$(xOldTag, 3) & xNewID & Mid$(xOldTag, 12)

                'update tag and bookmark in xml
                xDeletedScopes = Replace(xDeletedScopes, xOldTag, xNewTag)

                'add new doc var with same value as original one
                xValue = oDoc.Variables("mpo" & xOldID).Value
                oDoc.Variables.Add("mpo" & xNewID, xValue)

                lPos = InStr(lPos, xDeletedScopes, xSearch)
            End While

            'update xml node attribute and mpd doc var
            oWordDoc = New WordDoc
            oWordDoc.SetAttributeValue(oSegmentNode, "DeletedScopes", xDeletedScopes)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub ConformNumberedParagraphsToStyle(ByVal oRange As Word.Range, _
                                                     ByVal xBodyXML As String)
            Const mpThisFunction As String = "LMP.Forte.MSWord.cDeleteScope.ConformNumberedParagraphsToStyle"
            Dim lPos As Integer = 0
            Dim lPos2 As Integer = 0
            Dim xProps As String = ""
            Dim lNodes As Integer = 0
            Dim oPara As Word.Paragraph
            Dim oStyle As Word.Style
            Dim oLT As Word.ListTemplate

            On Error GoTo ProcError

            'do only if there's no direct paragraph formatting in xml -
            'remmed because its seems better to force paragraphs with a numbered
            'style to conform to the definition than to exclude paragraphs because
            'they have something like color applied - syncing the paragraph format
            'won't remove color in any case
            '    lPos = InStr(xBodyXML, "<w:pPr>")
            '    While lPos > 0
            '        lPos = lPos + 7
            '        lPos2 = InStr(xBodyXML, "</w:pPr>")
            '        xProps = Mid$(xBodyXML, lPos, lPos2 - lPos)
            '        lNodes = mpBase.mpbase.countchrs(xProps, "<")
            '        If lNodes > 1 Then
            '            Exit Sub
            '        ElseIf lNodes = 1 Then
            '            If InStr(xProps, "<w:pStyle") = 0 Then
            '                Exit Sub
            '            End If
            '        End If
            '        lPos = InStr(lPos, xBodyXML, "<w:pPr>")
            '    End While

            For Each oPara In oRange.Paragraphs
                oStyle = oPara.Style
                If Not oStyle Is Nothing Then
                    oLT = oStyle.ListTemplate
                    If Not oLT Is Nothing Then
                        oPara.Range.ParagraphFormat = oStyle.ParagraphFormat
                    End If
                End If
            Next oPara

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
    End Class
End Namespace
