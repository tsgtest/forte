Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Bates
        Private m_lStartNumber As Integer
        Private m_lNumLabels As Integer
        Private m_iStartRow As Short
        Private m_iStartCol As Short
        Private m_lEndNumber As Integer
        Private m_iDigits As Short
        Private m_xPrefix As String
        Private m_xSuffix As String
        Private m_iHorizAlign As WdParagraphAlignment
        Private m_iVerticalAlign As WdCellVerticalAlignment
        Private m_iColumns As Short
        Private m_iRows As Short
        Private m_bSideBySide As Boolean
        Private m_bType125 As Boolean
        Private m_xFontName As String
        Private m_iFontSize As Short
        Private m_bAllCaps As Boolean
        Private m_bSmallCaps As Boolean
        Private m_bBold As Boolean
        Private m_bUnderline As Boolean
        Private m_dblTopMargin As Double
        Private m_xConfidentialPhrases As String
        Private m_bPositionPhrasesAbove As Boolean
        Private m_oStatusForm As BatesStatusForm
        Private m_bContentControls As Boolean
        Private m_iDialogStartCol As Short
        Private m_lPages As Integer


        Public Property StartNumber As Integer
            Get
                If m_lStartNumber = 0 Then m_lStartNumber = 1
                Return m_lStartNumber
            End Get
            Set(value As Integer)
                m_lStartNumber = value
            End Set
        End Property

        Public Property StartRow As Short
            Get
                If m_iStartRow = 0 Then m_iStartRow = 1
                StartRow = m_iStartRow
            End Get
            Set(value As Short)
                m_iStartRow = value
            End Set
        End Property
        Public Property StartColumn As Short
            'GLOG : 7601 : CEH
            Get
                If m_iStartCol = 0 Then m_iStartCol = 1
                StartColumn = m_iStartCol
            End Get
            Set(value As Short)
                m_iStartCol = Value * 2 - 1
                m_iDialogStartCol = Value
            End Set
        End Property

        Public Property NumLabels As Integer
            Get
                If m_lNumLabels = 0 Then m_lNumLabels = 1
                NumLabels = m_lNumLabels
            End Get
            Set(value As Integer)
                m_lNumLabels = value
            End Set
        End Property

        Friend Property EndNumber As Integer
            Get
                EndNumber = Me.StartNumber + Me.NumLabels - 1
            End Get
            Set(value As Integer)
                m_lEndNumber = value
            End Set
        End Property

        Public Property Digits As Short
            Get
                If m_iDigits = 0 Then
                    m_iDigits = 1
                End If

                Digits = m_iDigits
            End Get
            Set(value As Short)
                m_iDigits = value
            End Set
        End Property

        Public Property HorizontalAlignment As WdParagraphAlignment
            Get
                HorizontalAlignment = m_iHorizAlign
            End Get
            Set(value As WdParagraphAlignment)
                m_iHorizAlign = value
            End Set
        End Property

        Public Property VerticalAlignment As WdCellVerticalAlignment
            Get
                'GLOG : 6377 : CEH
                '    If m_iVerticalAlign = 0 Then
                '        m_iVerticalAlign = WdCellVerticalAlignment.wdCellAlignVerticalCenter
                '    End If
                VerticalAlignment = m_iVerticalAlign
            End Get
            Set(value As WdCellVerticalAlignment)
                m_iVerticalAlign = value
            End Set
        End Property

        Public Property Columns As Short
            Get
                Columns = m_iColumns
            End Get
            Set(value As Short)
                m_iColumns = value
            End Set
        End Property

        Public Property Rows As Short
            Get
                Rows = m_iRows
            End Get
            Set(value As Short)
                m_iRows = value
            End Set
        End Property

        Public Property TopMargin As Double
            Get
                If m_dblTopMargin = 0 Then
                    m_dblTopMargin = 1
                End If
                TopMargin = m_dblTopMargin
            End Get
            Set(value As Double)
                m_dblTopMargin = value
            End Set
        End Property

        Public Property FontSize As Short
            Get
                If m_iFontSize = 0 Then
                    m_iFontSize = 12
                End If
                FontSize = m_iFontSize
            End Get
            Set(value As Short)
                m_iFontSize = value
            End Set
        End Property

        Public Property FontName As String
            Get
                'GLOG : 6377 : CEH
                If m_xFontName = "" Then
                    m_xFontName = "Times New Roman"
                End If
                FontName = m_xFontName
            End Get
            Set(value As String)
                m_xFontName = value
            End Set
        End Property

        Public Property Prefix As String
            Get
                Prefix = m_xPrefix
            End Get
            Set(value As String)
                m_xPrefix = value
            End Set
        End Property

        Public Property ConfidentialPhrases As String
            Get
                ConfidentialPhrases = m_xConfidentialPhrases
            End Get
            Set(value As String)
                m_xConfidentialPhrases = value
            End Set
        End Property

        Public Property PositionPhrasesAbove As Boolean
            Get
                PositionPhrasesAbove = m_bPositionPhrasesAbove
            End Get
            Set(value As Boolean)
                m_bPositionPhrasesAbove = value
            End Set
        End Property

        Public Property Suffix As String
            Get
                Suffix = m_xSuffix
            End Get
            Set(value As String)
                m_xSuffix = value
            End Set
        End Property

        Public Property SideBySide As Boolean
            Get
                SideBySide = m_bSideBySide
            End Get
            Set(value As Boolean)
                m_bSideBySide = value
            End Set
        End Property

        Public Property AllCaps As Boolean
            Get
                AllCaps = m_bAllCaps
            End Get
            Set(value As Boolean)
                m_bAllCaps = value
            End Set
        End Property

        Public Property SmallCaps As Boolean
            Get
                SmallCaps = m_bSmallCaps
            End Get
            Set(value As Boolean)
                m_bSmallCaps = value
            End Set
        End Property

        Public Property Bold As Boolean
            Get
                Bold = m_bBold
            End Get
            Set(value As Boolean)
                m_bBold = value
            End Set
        End Property

        Public Property Underline As Boolean
            Get
                Underline = m_bUnderline
            End Get
            Set(value As Boolean)
                m_bUnderline = value
            End Set
        End Property

        Public Property Type125 As Boolean
            Get
                Type125 = m_bType125
            End Get
            Set(value As Boolean)
                m_bType125 = value
            End Set
        End Property

        Private Function GetValuesString() As String
            GetValuesString = Me.AllCaps & "~" & Me.Bold & "~" & Me.ConfidentialPhrases & "~" & Me.Digits & "~" & _
                Me.FontName & "~" & Me.FontSize & "~" & Me.HorizontalAlignment & "~" & Me.NumLabels & "~" & _
                Me.PositionPhrasesAbove & "~" & Me.Prefix & "~" & Me.SideBySide & "~" & Me.SmallCaps & "~" & Me.StartColumn & "~" & _
                Me.StartNumber & "~" & Me.StartRow & "~" & Me.Suffix & "~" & Me.TopMargin & "~" & Me.Type125 & "~" & _
                Me.Underline & "~" & Me.VerticalAlignment
        End Function

        Public Function WriteLabels(ByVal bAppend As Boolean) As Integer
            Const mpThisFunction As String = "LMP.Forte.MSWord.Bates.WriteLabels"
            Dim rngPrimaryHeader As Range
            Dim rngPrimaryFooter As Range
            Dim rngFirstHeader As Range
            Dim rngFirstFooter As Range
            Dim rngMain As Range
            Dim secScope As Section
            Dim oPageSetup As PageSetup
            Dim lAdjust As Double
            Dim bRet As Boolean
            Dim oTable As Word.Table
            Dim oTableRng As Word.Range
            Dim iCellsPerPage As Short
            Dim iNumCells As Short
            Dim iPages As Short
            Dim xWindow As String

            On Error GoTo ProcError
            WriteLabels = False

            ''show status form
            m_oStatusForm = New BatesStatusForm
            m_oStatusForm.Show(GlobalMethods.GetWordWin32Window())
            'DoEvents()

            xWindow = GlobalMethods.CurWordApp.ActiveDocument.ActiveWindow.Caption
            GlobalMethods.CurWordApp.Windows(xWindow).Activate()

            GlobalMethods.CurWordApp.ScreenUpdating = False

            Me.EndNumber = (Me.NumLabels + Me.StartNumber) - 1

            System.Windows.Forms.Application.DoEvents()
            secScope = GlobalMethods.CurWordApp.ActiveDocument.Sections.First
            System.Windows.Forms.Application.DoEvents()

            ' document ranges
            bGetDocRanges(rngMain, _
                        rngPrimaryHeader, _
                        rngPrimaryFooter, _
                        rngFirstHeader, _
                        rngFirstFooter, , _
                        secScope)


            'GLOG : 6377 : ceh
            m_bContentControls = BaseMethods.FileFormat(rngMain.Document) = LMP.Forte.MSWord.mpFileFormats.OpenXML


            oTable = rngMain.Tables(1)

            oTableRng = oTable.Range
            oTableRng.Copy()

            WriteToTable(oTable)

            'm_oStatusForm.prbStatus.Value = 100
            System.Windows.Forms.Application.DoEvents()

            m_oStatusForm.Hide()
            m_oStatusForm = Nothing

            GlobalMethods.CurWordApp.ActiveDocument.Range(0, 0).Select()

            With GlobalMethods.CurWordApp.ActiveDocument.Styles
                With .Item("Bates Number").Font
                    .Name = Me.FontName
                    .Size = Me.FontSize
                    .AllCaps = False
                    .Bold = False
                    .SmallCaps = False
                    .Underline = False
                    .AllCaps = Me.AllCaps
                    .Bold = Me.Bold
                    .SmallCaps = Me.SmallCaps
                    .Underline = Me.Underline
                End With
                .Item("Bates Number").ParagraphFormat.Alignment = Me.HorizontalAlignment
                With .Item("Confidential").Font
                    .Name = Me.FontName
                    .Size = Me.FontSize
                    .AllCaps = False
                    .Bold = False
                    .SmallCaps = False
                    .Underline = False
                    .AllCaps = Me.AllCaps
                    .Bold = Me.Bold
                    .SmallCaps = Me.SmallCaps
                    .Underline = Me.Underline
                End With
                .Item("Confidential").ParagraphFormat.Alignment = Me.HorizontalAlignment
            End With

            GlobalMethods.CurWordApp.ScreenUpdating = True
            GlobalMethods.CurWordApp.ScreenRefresh()

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub WriteToTable(ByVal oTable As Word.Table)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Bates.WriteAcrossThenDown"
            Dim iCellsPerPage As Short
            Dim lNumCells As Integer
            Dim iTableColumns As Short
            Dim oTableRng As Word.Range
            Dim l As Integer
            Dim lCellsRemaining As Integer
            Dim i As Short
            Dim lTemp As Integer

            On Error GoTo ProcError

            'GLOG : 5611 : ceh
            If Me.SideBySide Then
                lCellsRemaining = 20 - (Me.StartRow - 1)

                For i = Me.StartColumn + 2 To oTable.Columns.Count Step 2
                    lCellsRemaining = lCellsRemaining + 20
                Next i

                m_lPages = 1

                If Me.NumLabels > lCellsRemaining Then
                    lTemp = Me.NumLabels - lCellsRemaining

                    If lTemp Mod 80 = 0 Then
                        lTemp = lTemp / 80
                    ElseIf (lTemp Mod 80) / 80 >= 0.5 Then
                        lTemp = Format(lTemp / 80, "0")
                    Else
                        lTemp = CInt(lTemp / 80) + 1
                    End If

                    m_lPages = lTemp + m_lPages
                End If
            Else
                'GLOG : 7601 : CEH
                iTableColumns = oTable.Columns.Count - 3 '(# separator columns)    '*c
                iCellsPerPage = oTable.Rows.Count * iTableColumns
                lNumCells = (Me.StartRow - 1) * iTableColumns + (Me.StartColumn - 1) + Me.NumLabels

                If lNumCells Mod iCellsPerPage = 0 Then
                    m_lPages = lNumCells / iCellsPerPage
                ElseIf (lNumCells Mod iCellsPerPage) / iCellsPerPage >= 0.5 Then
                    'GLOG : 6377 : CEH - cInt uses Banker's Rounding for 0.5
                    'need to use Format function instead
                    m_lPages = Format(lNumCells / iCellsPerPage, "0")
                Else
                    m_lPages = CInt(lNumCells / iCellsPerPage) + 1
                End If
            End If

            'copy and paste first page accordingly
            oTableRng = oTable.Range
            oTableRng.Cells.VerticalAlignment = Me.VerticalAlignment

            System.Windows.Forms.Application.DoEvents()
            'm_oStatusForm.prbStatus.Value = 10

            'fill in labels on the first page
            FillLabels(oTable)

            System.Windows.Forms.Application.DoEvents()
            'm_oStatusForm.prbStatus.Value = 25

            'copy the table
            oTableRng.Copy()

            'clear out the first page cells that should be empty
            EmptyStartingCells(oTable)

            'set the start number for the first filled cell
            SetStartNumber(oTable)

            System.Windows.Forms.Application.DoEvents()
            'm_oStatusForm.prbStatus.Value = 30

            Dim iStatus As Short
            Dim oRow As Word.Row

            'iStatus = m_oStatusForm.prbStatus.Value

            For l = 2 To m_lPages

                If l = 2 Then
                    'paste the table at the end of the first page
                    oTableRng.EndOf()

                    'add a blank row, and paste onto it -
                    'this is a workaround for the issue whereby
                    'the different styles in each cell  misapplied
                    'in the pasted table
                    oRow = oTableRng.Tables(1).Rows.Add
                    oRow.Select()
                    oTableRng.Paste()

                    'delete the added empty row
                    oTableRng = oTableRng.Tables(1).Range
                    oRow = oTableRng.Tables(1).Rows.Last
                    oRow.Delete()
                Else
                    'add remaining pages
                    oTableRng.EndOf()
                    oTableRng.Paste()
                    oTableRng = oTableRng.Tables(1).Range
                End If
                System.Windows.Forms.Application.DoEvents()
                'TODO: Rewrite form for vb.net
                'm_oStatusForm.prbStatus.Value = m_oStatusForm.prbStatus.Value + ((95 - iStatus) / (m_lPages - 1))
                If m_oStatusForm.JobCanceled Then
                    Exit For
                End If
            Next l

            'GLOG : 5611 : ceh
            If Me.SideBySide Then
                ResetSeqCodes(oTable)
            End If

            'update the fields and unlink
            oTable.Range.Fields.Update()
            oTable.Range.Fields.Unlink()

            If Not m_oStatusForm.JobCanceled Then
                'clear out ending cells that should be empty
                EmptyEndingCells(oTable)
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        'GLOG : 5611 : ceh
        Private Sub ResetSeqCodes(oTable As Word.Table)
            Dim i As Short
            Dim lTemp As Integer
            Dim lNextSeqNo As Integer
            Dim oCol As Word.Column
            Dim oField As Word.Field

            lNextSeqNo = (Me.StartNumber + (20 - Me.StartRow)) + 1

            'do 1st row
            For i = (Me.StartColumn + 1) To oTable.Columns.Count
                If i Mod 2 Then
                    If lTemp <> 0 Then
                        lTemp = lTemp + 20
                    Else
                        lTemp = lNextSeqNo
                    End If
                    ' field
                    oField = oTable.Columns(i).Cells(1).Range.Fields(1)
                    'replace field text
                    With oField
                        .Code.Text = Replace(.Code.Text, "\#", "\r" & CStr(lTemp) & " \#")
                    End With
                End If
            Next i

            'do other rows
            For i = 21 To oTable.Rows.Count Step 20
                System.Windows.Forms.Application.DoEvents()
                For Each oCol In oTable.Columns
                    If oCol.Index Mod 2 Then 'only do for odd columns
                        If lTemp <> 0 Then
                            lTemp = lTemp + 20
                        Else
                            lTemp = lNextSeqNo
                        End If
                        ' field
                        oField = oTable.Cell(oTable.Rows(i).Index, oCol.Index).Range.Fields(1)
                        'replace field text
                        With oField
                            .Code.Text = Replace(.Code.Text, "\#", "\r" & CStr(lTemp) & " \#")
                        End With
                    End If
                Next oCol
            Next i

        End Sub

        Private Sub EmptyStartingCells(ByVal oTable As Word.Table)
            Dim oRng As Word.Range
            Dim i As Short

            If Not (Me.StartColumn = 1 And Me.StartRow = 1) Then
                'GLOG : 5611 : ceh
                If Me.SideBySide Then
                    oRng = oTable.Cell(Me.StartRow - 1, Me.StartColumn).Range
                    oRng.Select()
                    For i = Me.StartRow - 2 To 1 Step -1
                        GlobalMethods.CurWordApp.Selection.MoveUp(WdUnits.wdLine, 1, WdMovementType.wdExtend)
                    Next i
                    GlobalMethods.CurWordApp.Selection.Range.Delete()

                    If Me.StartColumn > 1 Then
                        For i = 1 To Me.StartColumn - 1
                            oTable.Columns(i).Select()
                            GlobalMethods.CurWordApp.Selection.Range.Delete()
                        Next i
                    End If
                Else
                    oRng = oTable.Cell(Me.StartRow, Me.StartColumn).Previous.Range
                    oRng.MoveStart(Word.wdUnits.wdRow, -1)
                    oRng.Delete()

                    If Me.StartRow > 1 Then
                        oRng = oTable.Rows(Me.StartRow - 1).Range
                        oRng.MoveStart(Word.WdUnits.wdTable, -1)
                        oRng.Delete()
                    End If
                End If
            End If
        End Sub

        Private Sub EmptyEndingCells(ByVal oTable As Word.Table)
            Dim oRng As Word.Range
            Dim i As Short
            Dim oCell As Word.Cell
            Dim oTempRng As Word.Range

            'GLOG : 7601 : CEH
            oTempRng = oTable.Range

            'find ending cell
            With oTempRng.Find
                .ClearFormatting()
                .Text = Me.StartNumber + (Me.NumLabels - 1)
                .Execute()
                If .Found Then
                    oCell = oTempRng.Cells(1)
                Else
                    Exit Sub
                End If
            End With

            'GLOG : 5611 : ceh
            If Me.SideBySide Then
                'clear remaining row cells
                If oCell.RowIndex + 1 <= oTable.Rows.Count Then
                    oCell.Select()
                    'GLOG : 8012 : ceh
                    With GlobalMethods.CurWordApp.Selection
                        .MoveDown()
                        .Cells(1).Select()
                        .MoveDown(WdUnits.wdLine, oTable.Rows.Count - (oCell.RowIndex + 1), WdMovementType.wdExtend)
                        .Range.Delete()
                    End With
                End If

                'clear remaining column cells

                For i = (oCell.ColumnIndex + 1) To oTable.Columns.Count
                    If i Mod 2 Then 'only do for odd columns
                        oTable.Cell(m_lPages * 20, _
                                    oTable.Columns(i).Index).Select()
                        GlobalMethods.CurWordApp.Selection.MoveDown(WdUnits.wdLine, -19, WdMovementType.wdExtend)
                        GlobalMethods.CurWordApp.Selection.Range.Delete()
                    End If
                Next i
            Else
                oRng = oCell.Range
                oRng.Move(WdUnits.wdCell)
                oRng.MoveEnd(Word.wdUnits.wdRow, 1)
                oRng.Delete()

                'GLOG : 7520 : JSW
                If oCell.RowIndex + 1 <= oTable.Rows.Count Then
                    oRng = oTable.Rows(oCell.RowIndex + 1).Range
                    oRng.MoveEnd(WdUnits.wdTable)
                    oRng.Delete()
                End If
            End If
        End Sub

        Private Sub SetStartNumber(ByVal oTable As Word.Table)
            Dim oRng As Word.Range
            Dim xCode As String

            'GLOG : 5611 : ceh
            If Me.SideBySide Then
                xCode = "SEQ BATES" & Me.StartColumn & " \r" + CStr(Me.StartNumber) + " \# """ & BaseMethods.CreatePadString(Me.Digits, "0") & """"
            Else
                xCode = "SEQ BATES \r" + CStr(Me.StartNumber) + " \# """ & BaseMethods.CreatePadString(Me.Digits, "0") & """"
            End If

            oRng = oTable.Cell(Me.StartRow, Me.StartColumn).Range
            oRng.Delete()
            oRng = oTable.Cell(Me.StartRow, Me.StartColumn).Range

            'insert cell text into first cell
            If Me.ConfidentialPhrases <> "" Then
                If Me.PositionPhrasesAbove Then
                    oRng.StartOf()
                    oRng.InsertAfter(Me.ConfidentialPhrases)
                    oRng.Style = "Confidential"
                    oRng.InsertParagraphAfter()
                    oRng.EndOf()
                    'If m_bContentControls Or Me.Prefix <> "" Then
                    If Me.Prefix <> "" Then
                        oRng.InsertAfter(Me.Prefix)
                        oRng.EndOf()
                    End If
                    oRng.Document.Fields.Add(oRng, , xCode)
                    'GLOG 6705: Make sure range is at end of cell
                    oRng = oRng.Cells(1).Range
                    oRng.EndOf()
                    oRng.Move(Word.WdUnits.wdCharacter, -1)
                    oRng.InsertAfter(Me.Suffix)
                    oRng.Style = "Bates Number"
                Else
                    oRng.StartOf()
                    'If m_bContentControls Or Me.Prefix <> "" Then
                    If Me.Prefix <> "" Then
                        oRng.InsertAfter(Me.Prefix)
                        oRng.EndOf()
                    End If
                    oRng.Document.Fields.Add(oRng, , xCode)
                    oRng = oRng.Cells(1).Range
                    oRng.EndOf()
                    oRng.Move(Word.WdUnits.wdCharacter, -1)
                    oRng.InsertAfter(Me.Suffix)
                    oRng.Style = "Bates Number"
                    oRng.InsertParagraphAfter()
                    oRng.EndOf()
                    oRng.InsertAfter(Me.ConfidentialPhrases)
                    oRng.Style = "Confidential"
                End If
            Else
                oRng.StartOf()
                ' If m_bContentControls Or Me.Prefix <> "" Then
                If Me.Prefix <> "" Then
                    oRng.InsertAfter(Me.Prefix)
                    oRng.EndOf()
                End If
                oRng.Document.Fields.Add(oRng, , xCode)
                oRng = oRng.Cells(1).Range
                oRng.EndOf()
                oRng.Move(Word.WdUnits.wdCharacter, -1)
                oRng.InsertAfter(Me.Suffix)
                oRng.Style = "Bates Number"
            End If

        End Sub

        Private Sub FillLabels(ByVal oTable As Word.Table)
            Dim i As Short
            Dim oCell As Word.Cell
            Dim xCode As String
            Dim oRng As Word.Range

            'GLOG : 5611 : ceh
            If Me.SideBySide Then
                'fill vertically
                'each column should have its own sequence
                For i = 1 To oTable.Columns.Count
                    If i Mod 2 Then 'only do for odd columns
                        'set seq code string
                        xCode = "SEQ BATES" & i & "\# """ & BaseMethods.CreatePadString(Me.Digits, "0") & """"

                        'set range
                        oCell = oTable.Columns(i).Cells(1)
                        oRng = oCell.Range

                        'insert cell text into first cell
                        If Me.ConfidentialPhrases <> "" Then
                            If Me.PositionPhrasesAbove Then
                                oRng.StartOf()
                                oRng.InsertAfter(Me.ConfidentialPhrases)
                                oRng.Style = "Confidential"
                                oRng.InsertParagraphAfter()
                                oRng.EndOf()
                                If m_bContentControls Or Me.Prefix <> "" Then
                                    oRng.InsertAfter(Me.Prefix)
                                    oRng.EndOf()
                                End If
                                oRng.Document.Fields.Add(oRng, , xCode)
                                oRng = oCell.Range
                                oRng.EndOf()
                                oRng.Move(Word.WdUnits.wdCharacter, -1)
                                oRng.InsertAfter(Me.Suffix)
                                oRng.Style = "Bates Number"
                                oCell.Range.Copy()
                            Else
                                oRng.StartOf()
                                If Me.Prefix <> "" Then
                                    oRng.InsertAfter(Me.Prefix)
                                    oRng.EndOf()
                                End If
                                oRng.Document.Fields.Add(oRng, , xCode)
                                oRng = oCell.Range
                                oRng.EndOf()
                                oRng.Move(Word.WdUnits.wdCharacter, -1)
                                oRng.InsertAfter(Me.Suffix)
                                oRng.Style = "Bates Number"
                                oRng.InsertParagraphAfter()
                                oRng.EndOf()
                                oRng.InsertAfter(Me.ConfidentialPhrases)
                                oRng.Style = "Confidential"
                                oCell.Range.Copy()
                            End If
                        Else
                            oRng.StartOf()
                            If Me.Prefix <> "" Then
                                oRng.InsertAfter(Me.Prefix)
                                oRng.EndOf()
                            End If
                            oRng.Document.Fields.Add(oRng, , xCode)
                            oRng = oCell.Range
                            oRng.EndOf()
                            oRng.Move(Word.WdUnits.wdCharacter, -1)
                            oRng.InsertAfter(Me.Suffix)
                            oCell.Range.Style = "Bates Number"
                            oCell.Range.Copy()
                        End If
                    End If
                Next i
            Else
                'fill horizontally
                'set seq code string
                xCode = "SEQ BATES\# """ & BaseMethods.CreatePadString(Me.Digits, "0") & """"

                'set range
                oCell = oTable.Range.Cells(1)
                oRng = oCell.Range

                'insert cell text into first cell
                If Me.ConfidentialPhrases <> "" Then
                    If Me.PositionPhrasesAbove Then
                        oRng.StartOf()
                        oRng.InsertAfter(Me.ConfidentialPhrases)
                        oRng.Style = "Confidential"
                        oRng.InsertParagraphAfter()
                        oRng.EndOf()
                        If m_bContentControls Or Me.Prefix <> "" Then
                            oRng.InsertAfter(Me.Prefix)
                            oRng.EndOf()
                        End If
                        oRng.Document.Fields.Add(oRng, , xCode)
                        oRng = oCell.Range
                        oRng.EndOf()
                        oRng.Move(Word.WdUnits.wdCharacter, -1)
                        oRng.InsertAfter(Me.Suffix)
                        oRng.Style = "Bates Number"
                        oCell.Range.Copy()
                    Else
                        oRng.StartOf()
                        'If m_bContentControls Or Me.Prefix <> "" Then
                        If Me.Prefix <> "" Then
                            oRng.InsertAfter(Me.Prefix)
                            oRng.EndOf()
                        End If
                        oRng.Document.Fields.Add(oRng, , xCode)
                        oRng = oCell.Range
                        oRng.EndOf()
                        oRng.Move(Word.WdUnits.wdCharacter, -1)
                        oRng.InsertAfter(Me.Suffix)
                        oRng.Style = "Bates Number"
                        oRng.InsertParagraphAfter()
                        oRng.EndOf()
                        oRng.InsertAfter(Me.ConfidentialPhrases)
                        oRng.Style = "Confidential"
                        oCell.Range.Copy()
                    End If
                Else
                    oRng.StartOf()
                    'If m_bContentControls Or Me.Prefix <> "" Then
                    If Me.Prefix <> "" Then
                        oRng.InsertAfter(Me.Prefix)
                        oRng.EndOf()
                    End If
                    oRng.Document.Fields.Add(oRng, , xCode)
                    oRng = oCell.Range
                    oRng.EndOf()
                    oRng.Move(Word.WdUnits.wdCharacter, -1)
                    oRng.InsertAfter(Me.Suffix)
                    oCell.Range.Style = "Bates Number"
                    oCell.Range.Copy()
                End If

                For i = 2 To oTable.Columns.Count
                    'GLOG : 7601 : CEH
                    If i Mod 2 Then 'only do for odd columns
                        oCell = oTable.Range.Cells(i)
                        oCell.Range.Paste()
                    End If
                Next i
            End If

            oTable.Rows(1).Range.Copy()

            For i = 2 To oTable.Rows.Count
                oTable.Rows(i).Range.Paste()
            Next i

            oRng = oTable.Rows(i).Range
            oRng.MoveEnd(WdUnits.wdTable)
            oRng.Rows.Delete()
        End Sub

        Private Function secSetScope(Optional bAppend As Boolean = False, _
                                Optional bAppendFirst As Boolean = False, _
                                Optional bLinkHeaders As Boolean = False, _
                                Optional bLinkFooters As Boolean = False)
            '---set scope- if bAppend then
            '   insert section break, else
            '   clear whole document
            Dim secScope As Word.Section

            If bAppend Then
                If bAppendFirst Then
                    '---if first section set break at start of doc
                    GlobalMethods.CurWordApp.ActiveDocument.Characters.First. _
                        InsertBreak(WdBreakType.wdSectionBreakNextPage)
                    secScope = GlobalMethods.CurWordApp.ActiveDocument.Sections.First
                    '---Unlink the headers & Footers in section following new sec.
                    bUnlinkHeadersFooters(GlobalMethods.CurWordApp.ActiveDocument.Sections(2), bLinkHeaders, _
                        bLinkFooters)
                Else
                    GlobalMethods.CurWordApp.ActiveDocument.Characters.Last. _
                        InsertBreak(WdBreakType.wdSectionBreakNextPage)
                    secScope = GlobalMethods.CurWordApp.ActiveDocument.Sections.Last
                    '---Unlink the headers & Footers
                    bUnlinkHeadersFooters(secScope, bLinkHeaders, bLinkFooters)

                End If


                '---clear out new section -
                '       especially headers/footers
                'CharlieCore - changed for MP 9.2.0 release - template now contains marker for no trailer
                '              in footer
                If bAppendFirst = False Then
                    bClearContent(False, secScope, True, Not bLinkHeaders)
                    '            bClearContent False, secScope, True, Not bLinkHeaders, Not bLinkFooters
                Else
                    bClearContent(False, secScope, False, Not bLinkHeaders)
                    '            bClearContent False, secScope, False, Not bLinkHeaders, Not bLinkFooters
                End If

                secSetScope = secScope
            Else
                '---    clear out all
                'CharlieCore - changed for MP 9.2.0 release - template now contains marker for no trailer
                '              in footer
                '        bClearContent True, , True, True, True
                bClearContent(True, , True, True)
                secSetScope = GlobalMethods.CurWordApp.ActiveDocument.Sections.Last
            End If

        End Function

        Private Sub bGetDocRanges(Optional ByRef rngMain As Word.Range = Nothing, _
                               Optional ByRef rngPrimaryHeader As Word.Range = Nothing, _
                               Optional ByRef rngPrimaryFooter As Word.Range = Nothing, _
                               Optional ByRef rngFirstHeader As Word.Range = Nothing, _
                               Optional ByRef rngFirstFooter As Word.Range = Nothing, _
                               Optional ByRef vDoc As Object = Nothing, _
                               Optional ByRef vSection As Object = Nothing)
            'fills args with range values
            If IsNothing(vDoc) Then _
                  vDoc = GlobalMethods.CurWordApp.ActiveDocument

            If IsNothing(vSection) = True Then _
                    vSection = vDoc.Sections.First

            rngMain = vSection.Range

            With vSection
                rngPrimaryHeader = .Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                rngPrimaryFooter = .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                rngFirstHeader = .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                rngFirstFooter = .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
            End With
        End Sub

        Private Sub bUnlinkHeadersFooters(xSection As Word.Section, _
            Optional bLinkHeaders As Boolean = False, _
            Optional bLinkFooters As Boolean = False)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Bates.bUnlinkHeadersFooters"

            With xSection
                If Not bLinkHeaders Then
                    With .Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary)
                        .LinkToPrevious = False
                    End With
                    With .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                        .LinkToPrevious = False
                    End With
                End If
                If Not bLinkFooters Then
                    With .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary)
                        .LinkToPrevious = False
                    End With
                    With .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                        .LinkToPrevious = False
                    End With
                End If
            End With

        End Sub

        Private Sub bClearContent(Optional bAllSections As Boolean = True, _
                                Optional secCurSection As Word.Section = Nothing, _
                                Optional bMainStory As Boolean = False, _
                                Optional bHeaders As Boolean = False, _
                                Optional bFooters As Boolean = False)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Bates.bClearContent"

            'clears contents of specified stories
            'of specified sections

            '/////////MODIFIED COMPLETELY
            Dim hdrHeader As HeaderFooter
            Dim ftrFooter As HeaderFooter
            Dim secSection As Word.Section

            If bAllSections Then
                For Each secSection In GlobalMethods.CurWordApp.ActiveDocument.Sections
                    With secSection
                        If bMainStory Then
                            .Range.Delete()
                        End If

                        If bHeaders Then
                            For Each hdrHeader In .Headers
                                hdrHeader.Range.Delete()
                            Next hdrHeader
                        End If

                        If bFooters Then
                            For Each ftrFooter In .Footers
                                ftrFooter.Range.Delete()
                            Next ftrFooter
                        End If
                    End With
                Next secSection
            Else
                With secCurSection
                    If bMainStory Then
                        .Range.Delete()
                    End If

                    If bHeaders Then
                        For Each hdrHeader In .Headers
                            hdrHeader.Range.Delete()
                        Next hdrHeader
                    End If

                    If bFooters Then
                        For Each ftrFooter In .Footers
                            ftrFooter.Range.Delete()
                        Next ftrFooter
                    End If
                End With
            End If
        End Sub

        'GLOG : 7601 : CEH
        Private Function LabelCell(ByVal iLabel As Short) As Short
            'returns the table cell associated with the specified label
            Dim iSC As Short
            Dim iSR As Short
            Dim iSP As Short
            Dim iTempRow As Short
            Dim iTemp As Short
            Dim iCount As Short
            Dim iCols As Short
            Dim iRows As Short
            Dim bSpacerCol As Boolean
            Dim bSpacerRow As Boolean
            Dim iIndex As Short

            '    properties of label
            '   table from label type
            '   hardcoded for 5167 (Bates)
            iCols = 4
            iRows = 20
            bSpacerCol = True
            bSpacerRow = False

            If Me.SideBySide Then
                iIndex = ((iLabel - 1) * iCols) + _
                         (((Me.StartRow - 1) * iCols) + 1) + _
                         (m_iDialogStartCol - 1)
            Else
                iIndex = (iLabel - 1) + (((Me.StartRow - 1) * iCols) + m_iDialogStartCol)
            End If

            '---calculate rows
            If iIndex / iCols > Int(iIndex / iCols) Then
                iTemp = 1
            End If
            iTempRow = (Fix(iIndex / iCols) + iTemp) - 1

            If bSpacerRow Then
                '---multiply that result by the actual # of table cols per row
                If bSpacerCol Then
                    iSR = iTempRow * (iCols + (iCols - 1))
                Else
                    iSR = iTempRow * iCols
                End If
            End If

            If bSpacerCol Then
                '---calculate space cols for index position
                iTemp = iIndex - (iTempRow * iCols)
                iCount = iTemp - 1
                iSC = (iTempRow * (iCols - 1)) + iCount
            End If

            If bSpacerRow Then
                '---If more than one page, further adjust; last row of
                'word generated table pages do not have any spacer rows
                If iIndex > iRows * iCols Then
                    If iIndex / (iRows * iCols) > Int(iIndex / (iRows * iCols)) Then
                        iSP = (Int(iIndex / (iRows * iCols)) + 1) - 1
                    Else
                        '---9.4.1
                        iSP = Int(iIndex / (iRows * iCols)) - 1
                        '---9.4.1
                    End If
                    If bSpacerCol Then
                        iSP = iSP * (iCols + (iCols - 1))
                    Else
                        iSP = iSP * iCols
                    End If
                End If
            End If

            '   return the cell index
            LabelCell = iIndex + iSC + iSR - iSP
        End Function
    End Class
End Namespace