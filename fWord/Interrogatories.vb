Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Interrogatories
        Private m_xStyles() As String
        Private m_xRogHeading As String
        Private m_xRogResponseHeading As String
        Private m_bAlternateHeadingsAndResponses As Boolean
        Private m_iStartNumber As Short
        Private m_iEndNumber As Short
        Private m_dHeadingLeftMargin As Double
        Private m_dNextParaLeftMargin As Double
        Private m_bUseFieldCodes As Boolean
        Private m_bAllCaps As Boolean
        Private m_bBold As Boolean
        Private m_bSmallCaps As Boolean
        Private m_bUnderline As Boolean
        Private m_bRepeatPreviousHeading As Boolean
        Private m_iRogHeadingSeparation As mpRogHeadingSeparation
        Private m_iFormatType As mpRogFormatTypes
        Private m_xStyleDefinitions As String
        Private m_xCustomQuestionHeading As String
        Private m_xCustomResponseHeading As String
        Private m_lLastNumberInserted As Integer
        Private m_bUsesPlaceholder As Boolean
        Private m_xDefaultPlaceholder As String
        Private m_bShowAnswerNumber As Boolean
        Private m_bIsSelectedResponse As Boolean

        'ids match fldType in mpPublic.tblInterrogatoryStyles.fldType
        Public Enum mpRogStyles
            mpRogStyles_QuestionHeading = 0
            mpRogStyles_QuestionText = 1
            mpRogStyles_ResponseHeading = 2
            mpRogStyles_ResponseText = 3
            mpRogStyles_RuninQuestion = 4
            mpRogStyles_RuninResponse = 5
        End Enum

        Public Enum mpRogStyleFormats
            mpRogStyleFormats_TypeIndex = 0
            mpRogStyleFormats_NameIndex = 1
            mpRogStyleFormats_LineSpacingIndex = 2
            mpRogStyleFormats_FirstLineIndentIndex = 3
            mpRogStyleFormats_SpaceAfterIndex = 4
            mpRogStyleFormats_SpaceBeforeIndex = 5
            mpRogStyleFormats_BoldIndex = 6
            mpRogStyleFormats_UnderlineIndex = 7
            mpRogStyleFormats_AllCapsIndex = 8
            mpRogStyleFormats_SmallCapsIndex = 9
        End Enum

        Public Enum mpRogTypes
            mpRogTypes_Question = 0
            mpRogTypes_Response = 1
        End Enum

        Public Enum mpRogHeadingSeparation
            RunIn = 1
            SeparateLine = 2
        End Enum

        Public Enum mpRogFormatTypes
            DirectFormat = 1
            Style = 2
        End Enum

        Property StyleDefinitions As String
            Get
                Return m_xStyleDefinitions
            End Get
            Set(value As String)
                m_xStyleDefinitions = value
            End Set
        End Property

        Property FormatType As mpRogFormatTypes
            Get
                Return m_iFormatType
            End Get
            Set(value As mpRogFormatTypes)
                m_iFormatType = value
            End Set
        End Property


        Property RogHeadingSeparation As mpRogHeadingSeparation
            Get
                Return m_iRogHeadingSeparation
            End Get
            Set(value As mpRogHeadingSeparation)
                m_iRogHeadingSeparation = value
            End Set
        End Property

        Property RepeatPreviousHeading As Boolean
            Get
                Return m_bRepeatPreviousHeading
            End Get
            Set(value As Boolean)
                m_bRepeatPreviousHeading = value
            End Set
        End Property

        Property AllCaps As Boolean
            Get
                Return m_bAllCaps
            End Get
            Set(value As Boolean)
                m_bAllCaps = value
            End Set
        End Property

        Property Bold As Boolean
            Get
                Return m_bBold
            End Get
            Set(value As Boolean)
                m_bBold = value
            End Set
        End Property

        Property SmallCaps As Boolean
            Get
                Return m_bSmallCaps
            End Get
            Set(value As Boolean)
                m_bSmallCaps = value
            End Set
        End Property

        Property Underline As Boolean
            Get
                Return m_bUnderline
            End Get
            Set(value As Boolean)
                m_bUnderline = value
            End Set
        End Property

        Property UseFieldCodes As Boolean
            Get
                Return m_bUseFieldCodes
            End Get
            Set(value As Boolean)
                m_bUseFieldCodes = value
            End Set
        End Property

        'GLOG : 6746 : ceh
        Property ShowAnswerNumber As Boolean
            Get
                Return m_bShowAnswerNumber
            End Get
            Set(value As Boolean)
                m_bShowAnswerNumber = value
            End Set
        End Property

        Property IsSelectedResponse As Boolean
            Get
                Return m_bIsSelectedResponse
            End Get
            Set(value As Boolean)
                m_bIsSelectedResponse = value
            End Set
        End Property

        Property NextParaLeftMargin As Double
            Get
                Return m_dNextParaLeftMargin
            End Get
            Set(value As Double)
                m_dNextParaLeftMargin = value
            End Set
        End Property

        Property HeadingLeftMargin As Double
            Get
                Return m_dHeadingLeftMargin
            End Get
            Set(value As Double)
                m_dHeadingLeftMargin = value
            End Set
        End Property

        Property StartNumber As Short
            Get
                Return m_iStartNumber
            End Get
            Set(value As Short)
                m_iStartNumber = value
            End Set
        End Property

        Property EndNumber As Short
            Get
                If m_iEndNumber < StartNumber Then
                    m_iEndNumber = StartNumber
                End If

                Return m_iEndNumber
            End Get
            Set(value As Short)
                If value >= StartNumber Then
                    m_iEndNumber = value
                Else
                    m_iEndNumber = StartNumber
                End If
            End Set
        End Property

        Property AlternateHeadingsAndResponses As Boolean
            Get
                Return m_bAlternateHeadingsAndResponses
            End Get
            Set(value As Boolean)
                m_bAlternateHeadingsAndResponses = value
            End Set
        End Property

        ReadOnly Property RogHeadingSequenceName As String
            Get
                Return UCase(xTrimSpaces(m_xRogHeading))
            End Get
        End Property

        ReadOnly Property RogHeadingResponseSequenceName As String
            Get
                Return UCase(xTrimSpaces(m_xRogResponseHeading))
            End Get
        End Property

        Property CustomQuestionHeading As String
            Get
                Return m_xCustomQuestionHeading
            End Get
            Set(value As String)
                m_xCustomQuestionHeading = value
            End Set
        End Property

        Property CustomResponseHeading As String
            Get
                Return m_xCustomResponseHeading
            End Get
            Set(value As String)
                m_xCustomResponseHeading = value
            End Set
        End Property

        Property RogHeading As String
            Get
                Return m_xRogHeading
            End Get
            Set(value As String)
                m_xRogHeading = value
            End Set
        End Property

        Property RogResponseHeading As String
            Get
                Return m_xRogResponseHeading
            End Get
            Set(value As String)
                m_xRogResponseHeading = value
            End Set
        End Property

        Property UsesPlaceholder As Boolean
            Get
                Return m_bUsesPlaceholder
            End Get
            Set(value As Boolean)
                m_bUsesPlaceholder = value
            End Set
        End Property

        Property DefaultPlaceholder As String
            Get
                Return m_xDefaultPlaceholder
            End Get
            Set(value As String)
                m_xDefaultPlaceholder = value
            End Set
        End Property

        Function Create(ByVal oRng As Word.Range) As Boolean
            'creates interrogatories at the specified range
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.Create"
            Dim xRogHeading As String
            Dim xSeqName As String
            Dim bViewFields As Boolean
            Dim iEnd As Short
            Dim iStart As Short
            Dim i As Double
            Dim iCurIndex As Double
            Dim bExactSpace As Boolean
            Dim xPreviousSeq As String
            Dim oDoc As Word.Document
            Dim bIsAlternateResponse As Boolean
            Dim oFldCode As Field
            Dim oSelRng As Range

            oDoc = oRng.Document
            oDoc.Application.ScreenUpdating = False

            With oDoc.ActiveWindow.View
                bViewFields = .ShowFieldCodes
                .ShowFieldCodes = False
            End With

            If oDoc.Styles(wdBuiltInStyle.wdStyleNormal).ParagraphFormat.LineSpacingRule = wdLineSpacing.wdLineSpaceExactly Then
                bExactSpace = True
            End If

            SetupStyles(oDoc)

            oRng = GlobalMethods.CurWordApp.Selection.Range
            If Right(oRng.Text, 1) = vbCr Then
                oRng.MoveEnd(Word.WdUnits.wdCharacter, -1)
            End If

            On Error Resume Next
            iStart = Me.StartNumber
            iEnd = Me.EndNumber

            If iEnd = 0 Then
                iStart = 1
                iEnd = 1
            Else
                iEnd = ((iEnd - iStart) + 1)
                iStart = 1

                If Me.AlternateHeadingsAndResponses Then
                    iEnd = iEnd * 2
                End If
            End If

            With oRng
                .Text = ""
                .MoveEnd(Word.WdUnits.wdCharacter)
                Select Case Right(.Text, 1)
                    Case vbCr
                        .MoveEnd(Word.WdUnits.wdCharacter, -1)
                    Case Else
                        .MoveEnd(Word.WdUnits.wdCharacter, -1)
                        .InsertAfter(vbCr)
                        .Collapse(WdCollapseDirection.wdCollapseStart)
                End Select
            End With

            iCurIndex = Me.StartNumber

            If Me.RepeatPreviousHeading Then
                Dim xPreviousValues As String
                xPreviousValues = Me.PreviousValues(oRng.Document)
                Dim iPos As Short
                iPos = InStrRev(xPreviousValues, "|")
                m_lLastNumberInserted = Mid(xPreviousValues, iPos + 1)
            End If

            For i = iStart To iEnd
                If Me.RepeatPreviousHeading Then
                    xRogHeading = Me.RogHeading
                    bIsAlternateResponse = True
                Else
                    bIsAlternateResponse = (bIsEven(i) And Me.AlternateHeadingsAndResponses)

                    If bIsAlternateResponse Then
                        xRogHeading = Me.RogResponseHeading
                    Else
                        xRogHeading = Me.RogHeading
                    End If

                    xRogHeading = xTrimTrailingChrs(xRogHeading, " ", True, False)
                End If

                'rog formatting here
                With oRng
                    If Me.FormatType = mpRogFormatTypes.DirectFormat Then
                        ApplyDirectParagraphFormat(oRng, bExactSpace)
                    End If

                    .InsertAfter(Trim(xRogHeading))

                    If Me.FormatType = mpRogFormatTypes.DirectFormat Then
                        ApplyDirectFontFormat(oRng)
                    End If

                    'insert number
                    If Me.UseFieldCodes Then
                        'get sequence name - required for field code
                        If Me.StartNumber > 0 Then
                            If i = 1 Then
                                xSeqName = Me.RogHeadingSequenceName & " \R" & LTrim(Me.StartNumber)
                            ElseIf Me.AlternateHeadingsAndResponses And i = 2 Then
                                xSeqName = Me.RogHeadingResponseSequenceName & " \R" & LTrim(Me.StartNumber)
                            ElseIf bIsAlternateResponse Then
                                xSeqName = Me.RogHeadingResponseSequenceName
                            Else
                                xSeqName = Me.RogHeadingSequenceName
                            End If
                        ElseIf Me.RepeatPreviousHeading Then
                            xPreviousSeq = GetPrevSequence(oDoc)
                            If xPreviousSeq <> "" Then
                                xSeqName = xPreviousSeq & " \C"
                            Else
                                MsgBox("Could not find previous Sequence Field", vbExclamation)
                                Exit Function
                            End If
                        ElseIf bIsAlternateResponse Then
                            xSeqName = Me.RogHeadingResponseSequenceName
                        Else
                            xSeqName = Me.RogHeadingSequenceName
                        End If

                        'GLOG : 5696 : ceh
                        'insert
                        If Me.UsesPlaceholder Then
                            With .Find
                                .ClearFormatting()
                                .Text = Me.DefaultPlaceholder
                                .Execute()
                                If Not .Found Then  'no number should be added
                                    GoTo skipnumber
                                End If
                            End With
                            'GLOG : 6746 : ceh
                        ElseIf (m_bShowAnswerNumber = False And (Me.IsSelectedResponse Or bIsAlternateResponse)) Then
                            GoTo skipnumber
                        Else
                            .InsertAfter(" ")
                            .Collapse(WdCollapseDirection.wdCollapseEnd)
                        End If
                        oFldCode = .Fields.Add(Range:=oRng, _
                                    Type:=WdFieldType.wdFieldSequence, _
                                    Text:=xSeqName)

                        .MoveEnd(Unit:=WdUnits.wdWord, Count:=1)

                        m_lLastNumberInserted = CInt(oFldCode.Result.Characters.First.Text)

                    ElseIf Me.RepeatPreviousHeading Then
                        'insert last number previously inserted
                        If Me.UsesPlaceholder Then
                            With .Find
                                .ClearFormatting()
                                .Text = Me.DefaultPlaceholder
                                .Execute()
                                If Not .Found Then  'no number should be added
                                    GoTo skipnumber
                                Else
                                    oRng.Text = ""
                                End If
                            End With
                            .InsertAfter(m_lLastNumberInserted)
                            'GLOG : 6746 : ceh
                        ElseIf (m_bShowAnswerNumber = False And (Me.IsSelectedResponse Or bIsAlternateResponse)) Then
                            GoTo skipnumber
                        Else
                            .InsertAfter(" " & m_lLastNumberInserted)
                        End If
                    Else
                        If Me.UsesPlaceholder Then
                            With .Find
                                .ClearFormatting()
                                .Text = Me.DefaultPlaceholder
                                .Execute()
                                If Not .Found Then  'no number should be added
                                    GoTo skipnumber
                                Else
                                    oRng.Text = ""
                                End If
                            End With
                            'GLOG : 6746 : ceh
                        ElseIf (m_bShowAnswerNumber = False And (Me.IsSelectedResponse Or bIsAlternateResponse)) Then
                            GoTo skipnumber
                        End If

                        If iCurIndex = 0 Then
                            .InsertAfter("")
                        Else
                            If Me.UsesPlaceholder Then
                                .InsertAfter(Trim(Str(iCurIndex)))
                            Else
                                .InsertAfter(" " & Trim(Str(iCurIndex)))
                            End If
                        End If

                        m_lLastNumberInserted = iCurIndex
                    End If

                    'GLOG : 5696 : CEH
                    .SetRange(.Paragraphs(1).Range.Start, _
                              .Paragraphs(1).Range.End - 1)

                    If .Characters.Last.Text = " " Then
                        .Characters.Last.Delete()
                    End If

skipnumber:
                    .Collapse(WdCollapseDirection.wdCollapseEnd)
                    .InsertAfter(":")
                    .Font.Underline = WdUnderline.wdUnderlineNone

                    If Me.FormatType = mpRogFormatTypes.Style Then
                        'apply appropriate style based on type of rog heading
                        If Me.RogHeadingSeparation = mpRogHeadingSeparation.SeparateLine Then
                            If bIsAlternateResponse Then
                                'apply response style
                                .Paragraphs(1).Style = GetStyleName("ResponseHeading")
                            Else
                                'apply question style
                                .Paragraphs(1).Style = GetStyleName("QuestionHeading")
                            End If
                        Else
                            'JTS 6/28/11:  Style Names modified to match Administrator values
                            If bIsAlternateResponse Then
                                'apply run-in response style
                                .Paragraphs(1).Style = GetStyleName("RunInResponse")
                            Else
                                'apply run-in question style
                                .Paragraphs(1).Style = GetStyleName("RunInDiscovery")
                            End If
                        End If
                    End If

                    If Me.RogHeadingSeparation = mpRogHeadingSeparation.SeparateLine Then
                        .InsertAfter(vbCr)
                    Else
                        .InsertAfter("  ")
                    End If

                    .Collapse(WdCollapseDirection.wdCollapseEnd)

                    If i = iStart Then
                        'add a bookmark that defines the location
                        'of editing that should be selected
                        'upon comp ion of rog insertion
                        '               (WdUnits.wdSectionoSelRng = .Previous(Word.WdUnits.wdParagraph)
                        '                oSelRng.Collapse wdCollapseDirection.wdCollapseStart
                        oSelRng = .Duplicate

                        .Bookmarks.Add("zzmpTempRogSelection", oSelRng)
                    End If

                    If Me.FormatType = mpRogFormatTypes.DirectFormat Then
                        ResetDirectParagraphFormat(oRng)
                    End If

                    'JTS 6/28/11: Don't apply style again for Run-in Heading
                    If Me.FormatType = mpRogFormatTypes.Style And Me.RogHeadingSeparation = mpRogHeadingSeparation.SeparateLine Then
                        'apply text style
                        If bIsAlternateResponse Then
                            .Paragraphs(1).Style = GetStyleName("ResponseText")
                        Else
                            .Paragraphs(1).Style = GetStyleName("QuestionText")
                        End If
                    End If

                    If iEnd > 1 Then
                        If i <> iEnd Then
                            If Me.FormatType = mpRogFormatTypes.DirectFormat Then
                                If Me.RogHeadingSeparation = mpRogHeadingSeparation.SeparateLine Then
                                    .InsertParagraphAfter()
                                    ResetDirectFontFormat(oRng)
                                Else
                                    ResetDirectFontFormat(oRng)
                                    .InsertParagraphAfter()
                                End If
                            Else
                                .InsertParagraphAfter()
                            End If
                        End If

                        .Collapse(WdCollapseDirection.wdCollapseEnd)
                    Else
                        .Collapse(WdCollapseDirection.wdCollapseStart)
                        .Select()
                        With GlobalMethods.CurWordApp.Selection.Font
                            .Reset()
                            .AllCaps = False
                            .Bold = False
                        End With
                    End If
                End With

                If (Not Me.AlternateHeadingsAndResponses) Or bIsAlternateResponse Then
                    iCurIndex = iCurIndex + 1
                End If
            Next i

            SaveValues(oDoc)

            oDoc.ActiveWindow.View.ShowFieldCodes = bViewFields

            If oDoc.Bookmarks.Exists("zzmpTempRogSelection") Then
                'select the start of the first rog text paragraph
                oSelRng.Select()

                'delete temp bookmark
                oDoc.Bookmarks("zzmpTempRogSelection").Delete()
            End If

            oDoc.Application.ScreenUpdating = True
            Exit Function
ProcError:
            oDoc.Application.ScreenUpdating = True
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Private Sub SaveValues(ByVal oDoc As Word.Document)
            'saves the current rogs values to the specified document
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.SaveValues"
            Dim xValues As String

            xValues = Me.AllCaps & "|"
            xValues = xValues & Me.AlternateHeadingsAndResponses & "|"
            xValues = xValues & Me.Bold & "|"
            xValues = xValues & Me.EndNumber & "|"
            xValues = xValues & Me.FormatType & "|"
            xValues = xValues & Me.HeadingLeftMargin & "|"
            xValues = xValues & Me.NextParaLeftMargin & "|"
            xValues = xValues & Me.RepeatPreviousHeading & "|"
            xValues = xValues & Me.RogHeading & "|"
            xValues = xValues & Me.RogHeadingSeparation & "|"
            xValues = xValues & Me.RogResponseHeading & "|"
            xValues = xValues & Me.SmallCaps & "|"
            xValues = xValues & Me.StartNumber & "|"
            xValues = xValues & Me.Underline & "|"
            xValues = xValues & Me.UseFieldCodes & "|"
            xValues = xValues & Me.CustomQuestionHeading & "|"
            xValues = xValues & Me.CustomResponseHeading & "|"
            xValues = xValues & m_lLastNumberInserted & "|"
            xValues = xValues & Me.ShowAnswerNumber

            oDoc.Variables.Item("zzmp10RogsValues").Value = xValues

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Function PreviousValues(ByVal oDoc As Word.Document) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.PreviousValues"
            Dim xValues As String

            On Error Resume Next
            PreviousValues = oDoc.Variables("zzmp10RogsValues").Value

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Private Function GetStyleName(ByVal xRogStyleType As String)
            'returns the name of the style associated
            'with the specified interrogatory type
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.GetStyleName"
            Dim xStyleDefs As String
            Dim iPos1 As Integer
            Dim iPos2 As Integer

            'gets name by parsing the style definitions
            xStyleDefs = Me.StyleDefinitions

            iPos1 = InStr(xStyleDefs, xRogStyleType)

            If iPos1 > 0 Then
                iPos1 = InStr(iPos1, xStyleDefs, ",")
                iPos2 = InStr(iPos1 + 1, xStyleDefs, ",")

                GetStyleName = Mid(xStyleDefs, iPos1 + 1, iPos2 - iPos1 - 1)
            Else
                Err.Raise(-2, , "Missing interrogatory style definition: " & xRogStyleType)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Private Sub ResetDirectFontFormat(ByVal oRng As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.ResetDirectFontFormat"

            With oRng.Font
                .Reset()
                .AllCaps = False
                .Bold = False
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
        Private Sub ResetDirectParagraphFormat(ByVal oRng As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.ResetDirectParagraphFormat"

            With oRng.ParagraphFormat
                .KeepTogether = False
                .KeepWithNext = False
                If Me.RogHeadingSeparation = mpRogHeadingSeparation.SeparateLine Then _
                    .FirstLineIndent = BaseMethods.InchesToPointsIntl(NextParaLeftMargin) 'GLOG 6966
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
        Private Sub ApplyDirectParagraphFormat(ByVal oRng As Word.Range, ByVal bExactSpace As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.ApplyDirectParagraphFormat"
            With oRng.ParagraphFormat
                'check if line spacing is double or exactly single and(WdUnits.wdSectionaccordingly
                If bExactSpace Then
                    .LineSpacingRule = wdLineSpacing.wdLineSpaceExactly
                    .LineSpacing = 24
                Else
                    .LineSpacingRule = wdLineSpacing.wdLineSpaceDouble
                End If
                .KeepTogether = True
                .KeepWithNext = True
                .FirstLineIndent = BaseMethods.InchesToPointsIntl(HeadingLeftMargin) 'GLOG 6966
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Private Sub ApplyDirectFontFormat(ByVal oRng As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.ApplyDirectFontFormat"
            With oRng.Font
                .AllCaps = Me.AllCaps
                .SmallCaps = Me.SmallCaps
                .Bold = Me.Bold
                If Me.Underline Then
                    .Underline = WdUnderline.wdUnderlineSingle
                End If
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
        '
        'Public Sub Finish(bUseCustomRog As Boolean, bUseCustomRogResponse As Boolean, xCustomRog As String, xCustomRogResponse As String, bAlternate As Boolean, xHeadingSetRogs As String, xNumberRog As String, bDirect As Boolean, bRepeatPrevious As Boolean, iEnd As short, iStart As short, iCurIndex As Double, Interrogatories() As String, InterrogatoriesIndex As short, iHLeftMargin As short, bAutoNumRog As Boolean, bBSAllCaps As Boolean, bBSSmallCaps As Boolean, bBSBold As Boolean, bBSUnderlined As Boolean, bBSSep As Boolean, iLeftMargin As short, iHeadingSetRogs As short, bStySep As Boolean, xLineSpacing As String, xFirstLineIndent As String, xSpaceAfter As String, xSpaceBefore As String, xBold As String, xUnderline As String, xAllCaps As String, xSmallCaps As String, xStyleNameResponseHeading As String, xStyleNameQuestionHeading As String, xStyleNameRuninResponse As String, xStyleNameRuninQuestion As String, xStyleNameResponseText As String, xStyleNameQuestionText As String)
        '
        ''  (WdUnits.wdSectionvariables
        '    On Error Resume Next
        '    If xNumberRog <> "" Or xHeadingSetRogs <> "" Then
        '        'restart numbering
        '        With GlobalMethods.CurWordApp.ActiveDocument
        '            .Variables.Add "iRogReset", "1"
        '            .Variables("iRogReset") = "1"
        '        End With
        '        If xHeadingSetRogs <> "" Then
        '            'when macro is rerun, don't change list selection
        '            With GlobalMethods.CurWordApp.ActiveDocument
        '                .Variables.Add "bWasHeadingSet", "True"
        '                .Variables("bWasHeadingSet") = "True"
        '            End With
        '        Else
        '            Dim bResetResponseNumbering As Boolean
        '            bResetResponseNumbering = GlobalMethods.CurWordApp.ActiveDocument.Variables("bResetResponseNumbering")
        '
        '            If bResetResponseNumbering Then
        '                GlobalMethods.CurWordApp.ActiveDocument.Variables("bResetResponseNumbering") = "False"
        '            Else
        '                If xNumberRog <> "" And bAlternate Then
        '                    'when macro is rerun, don't clear "start at" text box
        '                    With GlobalMethods.CurWordApp.ActiveDocument
        '                        .Variables.Add "bResetResponseNumbering", "True"
        '                        .Variables("bResetResponseNumbering") = "True"
        '                    End With
        '                End If
        '            End If
        '        End If
        '    End If
        '    On Error GoTo ProcError
        '
        '    DoEvents
        '    GlobalMethods.CurWordApp.ScreenUpdating = False
        '    If bDirect Then
        '        bWriteInterrogs bRepeatPrevious, iEnd, iStart, iCurIndex, bAlternate, bUseCustomRog, xCustomRog, xCustomRogResponse, Interrogatories, InterrogatoriesIndex, False, iHLeftMargin, bAutoNumRog, bBSAllCaps, bBSSmallCaps, bBSBold, bBSUnderlined, xHeadingSetRogs, xNumberRog, bBSSep, iLeftMargin
        '        ' JAB TODO: Impelement in vb
        '    Else
        '        SetupStyles xLineSpacing, xFirstLineIndent, xSpaceAfter, xSpaceBefore, xBold, xUnderline, xAllCaps, xSmallCaps
        '        DoEvents
        '        bWriteStyInterrogs bRepeatPrevious, iHeadingSetRogs, xHeadingSetRogs, xNumberRog, bAlternate, bUseCustomRog, xCustomRog, xCustomRogResponse, Interrogatories, InterrogatoriesIndex, bStySep, bAutoNumRog, xStyleNameResponseHeading, xStyleNameQuestionHeading, xStyleNameRuninResponse, xStyleNameRuninQuestion, xStyleNameResponseText, xStyleNameQuestionText
        '    End If
        '    Application.Activate
        '
        '    Exit Sub
        'ProcError:
        '    ' JAB TODO: Should I eliminate this, throw the exception and have the caller handle it?
        '    'EchoOn
        '    'g_oError.Show Err, "Could not successfully insert interrogatories."
        '    Exit Sub
        'End Sub

        Private Function GetPrevSequence(ByVal oDoc As Word.Document) As String
            Dim oRange As Word.Range
            Dim bShowFieldCodes As Boolean
            Dim xTemp As String
            Dim iPos As Short

            bShowFieldCodes = oDoc.ActiveWindow.View.ShowFieldCodes

            oDoc.ActiveWindow.View.ShowFieldCodes = True

            oRange = oDoc.Range

            With oRange.Find
                .ClearFormatting()
                .Text = "^d"
                .Forward = False
                .Execute()
                If .Found Then
                    xTemp = Mid(oRange.Fields(1).Code.Text, 5)
                    iPos = InStr(1, xTemp, "\")
                    If iPos Then
                        xTemp = Left(xTemp, iPos - 1)
                    End If
                    xTemp = xTrimTrailingChrs(xTemp, " ")
                End If
            End With

            oDoc.ActiveWindow.View.ShowFieldCodes = bShowFieldCodes

            GetPrevSequence = xTemp

        End Function

        Private Function xTrimSpaces(ByVal xString As String) As String
            xTrimSpaces = BaseMethods.xSubstitute(xString, Chr(32), "")
        End Function

        Private Function xTrimTrailingChrs(ByVal xText As String, _
                                   Optional ByVal xChr As String = "", _
                                   Optional bTrimStart As Boolean = False, _
                                   Optional bTrimEnd As Boolean = True) As String

            '   removes trailing xChr from xText -
            '   if xchr = "" then trim last char

            If xChr <> "" Then
                If bTrimStart Then
                    While Left(xText, Len(xChr)) = xChr
                        xText = Mid(xText, Len(xChr) + 1)
                    End While
                End If
                If bTrimEnd Then
                    While Right(xText, Len(xChr)) = xChr
                        xText = Left(xText, Len(xText) - Len(xChr))
                    End While
                End If
            Else
                xText = Left(xText, Len(xText) - 1)
            End If

            xTrimTrailingChrs = xText
        End Function


        Function bIsEven(iTestNumber) As Boolean
            If iTestNumber Mod 2 = 0 Then
                bIsEven = True
            Else
                bIsEven = False
            End If
        End Function

        Public Sub SetupStyles(ByVal oDoc As Word.Document)
            'creates the necessary interrogatory styles in the specified document
            Const mpThisFunction As String = "LMP.Forte.MSWord.Interrogatories.SetupStyles"

            Dim oStyle As Word.Style
            Dim oStyNormal As Word.Style
            Dim i As Short
            Dim xStyle As String
            Dim vStyles As Object
            Dim vStyle As Object

            vStyles = Split(Me.StyleDefinitions, ";")

            oStyNormal = oDoc.Styles(wdBuiltInStyle.wdStyleNormal)

            For i = LBound(vStyles) To UBound(vStyles)
                xStyle = vStyles(i)
                vStyle = Split(xStyle, ",")

                On Error Resume Next
                oStyle = Nothing
                oStyle = oDoc.Styles(vStyle(1))
                On Error GoTo ProcError

                If oStyle Is Nothing Then
                    'style doesn't exist - create
                    oStyle = oDoc.Styles.Add(vStyle(1), WdStyleType.wdStyleTypeParagraph)
                End If

                With oStyle
                    'define style based on definition
                    .BaseStyle = wdBuiltInStyle.wdStyleNormal

                    With .ParagraphFormat
                        If vStyle(mpRogStyleFormats.mpRogStyleFormats_LineSpacingIndex) = 2 Then
                            If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpacing.wdLineSpaceExactly Then
                                .LineSpacing = 24
                            Else
                                .LineSpacingRule = wdLineSpacing.wdLineSpaceDouble
                            End If
                        Else
                            If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpacing.wdLineSpaceExactly Then
                                .LineSpacing = 12
                            Else
                                .LineSpacingRule = wdLineSpacing.wdLineSpaceSingle
                            End If
                        End If

                        .FirstLineIndent = BaseMethods.InchesToPointsIntl(vStyle(mpRogStyleFormats.mpRogStyleFormats_FirstLineIndentIndex)) 'GLOG 6966
                        .SpaceAfter = vStyle(mpRogStyleFormats.mpRogStyleFormats_SpaceAfterIndex)
                        .SpaceBefore = vStyle(mpRogStyleFormats.mpRogStyleFormats_SpaceBeforeIndex)
                    End With

                    If vStyle(mpRogStyleFormats.mpRogStyleFormats_TypeIndex) = "QuestionText" Or _
                        vStyle(mpRogStyleFormats.mpRogStyleFormats_TypeIndex) = "ResponseText" Then
                        .ParagraphFormat.KeepTogether = False
                        .ParagraphFormat.KeepWithNext = False
                        .NextParagraphStyle = vStyle(mpRogStyleFormats.mpRogStyleFormats_NameIndex)
                    Else
                        .NextParagraphStyle = WdBuiltinStyle.wdStyleBodyText
                        .ParagraphFormat.KeepTogether = True
                        .ParagraphFormat.KeepWithNext = True

                        With .Font
                            .Bold = IIf(vStyle(mpRogStyleFormats.mpRogStyleFormats_BoldIndex) = "True", 1, 0)

                            If vStyle(mpRogStyleFormats.mpRogStyleFormats_UnderlineIndex) Then
                                .Underline = WdUnderline.wdUnderlineSingle
                            Else
                                .Underline = WdUnderline.wdUnderlineNone
                            End If

                            .AllCaps = IIf(vStyle(mpRogStyleFormats.mpRogStyleFormats_AllCapsIndex) = "True", 1, 0)
                            .SmallCaps = IIf(vStyle(mpRogStyleFormats.mpRogStyleFormats_SmallCapsIndex) = "True", 1, 0)
                        End With
                    End If
                End With
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
    End Class
End Namespace