'**********************************************************
'   LMP.Forte.MSWord.Application Class
'   created 2/26/04 by Doug Miller
'
'   'contains methods and functions that operate on Word
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core

Namespace LMP.Forte.MSWord
    Public Class WordApp
        Friend Shared Sub Echo(ByVal bEcho As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.Echo"

            On Error GoTo ProcError

            GlobalMethods.Echo(bEcho)
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Friend Shared Function GetEcho() As Boolean
            GetEcho = GlobalMethods.CurWordApp.ScreenUpdating
        End Function

        Public Shared Sub RefreshScreen()
            GlobalMethods.CurWordApp.ScreenRefresh()
        End Sub
        Public Shared Sub CallMacro(ByVal xMacroName As String, _
                             ByVal xArgs As String, _
                             ByVal xArgDelimiter As String)
            'calls the macro with the specified name - args is a delimited
            'string of macro arguments - must be in the correct order
            GlobalMethods.CallMacro(xMacroName, xArgs, xArgDelimiter)
        End Sub

        Public Shared Function GetWordObjectFromString(ByVal oTopLevelObject As Object, _
            ByVal xReferenceString As String) As Object
            'returns the Word object referenced by the specified string and
            'top level object- e.g. ParagraphFormat.TabStops.Item(1)
            GetWordObjectFromString = GlobalMethods.GetWordObjectFromString(oTopLevelObject, _
                xReferenceString)
        End Function

        Public Shared Function GetWordApplicationValue(ByVal xParams As String) As String
            'returns the value of the specified property of Word
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.GetWordApplicationValue"
            Dim oWordObject As Object
            Dim xPropRef As String
            Dim xObjectRef As String
            Dim lPos As Integer

            On Error GoTo ProcError

            lPos = InStrRev(xParams, ".")

            If lPos = 0 Then
                'application is the referenced object
                oWordObject = GlobalMethods.CurWordApp
                xPropRef = xParams
            Else
                'instruction contains a referenced object
                xPropRef = Trim$(Mid$(xParams, lPos + 1))
                xObjectRef = Trim$(Left$(xParams, lPos - 1))

                'get the object referenced by the string
                oWordObject = GlobalMethods.GetWordObjectFromString(GlobalMethods.CurWordApp, _
                    xObjectRef)
            End If

            'get value of property of referenced object
            On Error Resume Next
            GetWordApplicationValue = CallByName(oWordObject, xPropRef, vbGet)

            If Err.Number > 0 Then
                On Error GoTo ProcError
                Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & "WordApplication_" & xParams)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function CreateForteDocument() As Word.Document
            'creates and returns a new Forte document using the specified template
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.CreateForteDocument"
            Dim xDir As String
            Dim xNewFileDot As String
            Dim oApp As WordApp
            Dim bShowAll As Boolean

            On Error GoTo ProcError
            oApp = New WordApp

            xDir = oApp.GetAppKeyValue("RootDirectory") & "\templates\"

            xNewFileDot = "\ForteNormal.dotx"

            If Dir$(xDir + xNewFileDot) = "" Then
                'look for file in Word Personal files directory
                xDir = GlobalMethods.CurWordApp.Options.DefaultFilePath(Word.WdDefaultFilePath.wdUserTemplatesPath)

                If Dir$(xDir + xNewFileDot) = "" Then
                    Err.Raise(111, "mp10.dot.NewForteDocument", _
                        "The file '" & xDir & xNewFileDot & "' cannot be found.")
                End If
            End If

            Dim oWindow As Word.Window

            On Error Resume Next
            oWindow = GlobalMethods.CurWordApp.ActiveWindow
            On Error GoTo ProcError

            If Not oWindow Is Nothing Then
                'get state of show/hide -
                'reapply in new document
                bShowAll = oWindow.View.ShowAll
            End If

            Echo(False)

            CreateForteDocument = GlobalMethods.CurWordApp.Documents.Add(xDir + xNewFileDot)

            If Not oWindow Is Nothing Then
                CreateForteDocument.ActiveWindow.View.ShowAll = bShowAll
            End If

            Exit Function
ProcError:
            oApp.ShowError()
        End Function

        Public Shared Function CreateDocument(ByVal xTemplate As String, ByVal bVisible As Boolean, ByVal bShowTaskPane As Boolean) As Word.Document
            'creates and returns a new word document using the specified template
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.CreateDocument"
            Dim bShowAll As Boolean

            On Error GoTo ProcError

            Dim oWindow As Word.Window

            On Error Resume Next
            oWindow = GlobalMethods.CurWordApp.ActiveWindow
            On Error GoTo ProcError

            If Not oWindow Is Nothing Then
                'get state of show/hide -
                'reapply in new document
                bShowAll = oWindow.View.ShowAll
            End If

            Echo(False)

            CreateDocument = GlobalMethods.CurWordApp.Documents.Add( _
                Template:=xTemplate, Visible:=bVisible)

            If Not oWindow Is Nothing Then
                CreateDocument.ActiveWindow.View.ShowAll = bShowAll
                CreateDocument.ActiveWindow.Activate() 'JTS 11/1/16: Make sure new document has focus
                CreateDocument.Activate()
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub SaveDocumentAs(ByVal oDoc As Word.Document, ByVal xNewFullName As String, ByVal xPassword As String)
            If xPassword <> "" Then
                oDoc.SaveAs(xNewFullName, , , xPassword, False)
            Else
                oDoc.SaveAs(xNewFullName, , , , False)
            End If
        End Sub
        Public Shared Sub SaveAsDocx(ByVal oDoc As Word.Document, ByVal xNewFullName As String, ByVal xPassword As String)
            If xPassword <> "" Then
                oDoc.SaveAs(xNewFullName, Word.WdSaveFormat.wdFormatXMLDocument, , xPassword, False)
            Else
                oDoc.SaveAs(xNewFullName, Word.WdSaveFormat.wdFormatXMLDocument, , , False)
            End If
        End Sub

        Public Shared Sub CloseDocument(ByVal oDoc As Word.Document, ByVal bSave As Boolean)
            If bSave Then
                oDoc.Close(Word.WdSaveOptions.wdSaveChanges)
            Else
                'GLOG 5475: If Worksite is running, SaveChanges=false parameter of oDoc.Close will not be honored,
                'resulting in unwanted save prompt.  Need to explicitly(WdUnits.wdSectiondocument saved state first.
                oDoc.Saved = True
                oDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
            End If
        End Sub

        'GLOG : 6112 : ceh
        'GLOG : 7948 : ceh - include finished Envelopes & Labels
        Public Shared Sub RemoveTestDocuments()
            Dim oDoc As Word.Document
            Dim i As Short
            Dim iCount As Short
            Dim bClose As Boolean
            Dim xName As String

            iCount = GlobalMethods.CurWordApp.Documents.Count

            If iCount Then
                Echo(False)
                For i = iCount To 1 Step -1
                    oDoc = GlobalMethods.CurWordApp.Documents(i)

                    'only close unsaved documents
                    If oDoc.Path = "" Then
                        'get caption name
                        xName = oDoc.ActiveWindow.Caption
                        If (UCase(xName) Like "DOCUMENT*" And IsNumeric(Mid(xName, 9, 1))) Or _
                        UCase(xName) Like "LABELS*" Or _
                        UCase(xName) Like "ENVELOPES*" Then
                            bClose = True
                        End If
                    End If

                    If bClose Then
                        With oDoc
                            .Saved = True
                            .Close(Word.WdSaveOptions.wdDoNotSaveChanges)
                        End With
                    End If

                    'reset var before next iteration
                    bClose = False

                Next i
                Echo(True)
            End If

        End Sub

        Public Shared Function ActiveDocument() As Word.Document
            Return GlobalMethods.CurWordApp.ActiveDocument
        End Function
        Public Shared Sub EnsureAttachedSchema()
            'attaches the ForteDocumentSchema to the active document if necessary -
            'installs schema to the schema library, if necessary
            Const SCHEMA_URI As String = GlobalMethods.mpNamespace
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.EnsureAttachedSchema"

            Dim oSchema As XMLSchemaReference = Nothing
            Dim xSchemaFile As String = ""
            Dim oNamespace As XMLNamespace = Nothing
            Dim xDesc As String = ""
            Dim bAttach As Boolean = False

            'check for attached schema
            On Error Resume Next
            oSchema = GlobalMethods.CurWordApp.ActiveDocument.XMLSchemaReferences.Item(SCHEMA_URI)
            On Error GoTo ProcError

            If oSchema Is Nothing Then
                bAttach = True
            ElseIf oSchema.Location = "" Then
                bAttach = True
            End If

            If bAttach Then
                'Forte document schema is not attached -
                'check for existence in schema library

                On Error Resume Next
                oNamespace = GlobalMethods.CurWordApp.XMLNamespaces(SCHEMA_URI)
                On Error GoTo ProcError

                If oNamespace Is Nothing Then
                    'schema is not in schema library - add

                    'get location of schema file
                    xSchemaFile = GetAppKeyValue("RootDirectory") & "\Data\Forte.xsd"

                    If Dir$(xSchemaFile, vbNormal) = "" Then
                        'could not find schema file - alert
                        Err.Raise(1234, "mp10.mdlFc.ShowReferencePane", _
                            "Could not find the schema file '" & xSchemaFile & "'.")
                    End If

                    'add schema to library
                    On Error Resume Next
                    oNamespace = GlobalMethods.CurWordApp.XMLNamespaces.Add(xSchemaFile, _
                        SCHEMA_URI, "Forte Document Schema")

                    xDesc = Err.Description
                    On Error GoTo ProcError

                    If oNamespace Is Nothing Then
                        'could not add namespace - alert
                        Err.Raise(1234, "mp10.mdlFc.ShowReferencePane", _
                            "Could not add the schema '" & xSchemaFile & "' to the schema library.  " & xDesc & ".")
                    End If
                End If

                'attach to document
                On Error Resume Next
                GlobalMethods.CurWordApp.XMLNamespaces.Item(SCHEMA_URI).AttachToDocument(GlobalMethods.CurWordApp.ActiveDocument)
                Dim oError As ErrObject = Nothing
                On Error GoTo ProcError

                If Not oError Is Nothing Then
                    'could not attach schema - alert
                    Err.Raise(oError.Number, "LMP.Forte.MSWord.WordApp.EnsureAttachedSchema", _
                        "Could not attach schema '" & xSchemaFile & "' to this document.  " & oError.Description & ".")
                End If
            End If

            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub

        Public Shared Function GetAppKeyValue(xKey As String) As String
            'returns the value for the specified Forte application settings key
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.GetAppKeyValue"
            Dim xRegKey As String
            Dim oReg As Registry

            On Error Resume Next
            oReg = New Registry
            xRegKey = oReg.GetForteValue(xKey)
            On Error GoTo ProcError

            If xRegKey = "" Then
                'something wrong with installation
                Err.Raise(1234, , _
                    "Could not find the following application settings key: " & xKey & _
                    "Please add the appropriate registry keys or reinstall Forte.")
            End If

            'return directory
            GetAppKeyValue = EvaluateEnvironmentVariables(xRegKey)

            Exit Function
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Function

        Private Shared Function EvaluateEnvironmentVariables(xText As String) As String
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.EvaluateEnvironmentVariables"
            Dim iPos As Short
            Dim iPos2 As Short
            Dim xEnvVar As String

            'evaluate path for environment variables
            iPos = InStr(xText, "%")

            While (iPos > 0)
                iPos2 = InStr(iPos + 1, xText, "%")

                If iPos2 = 0 Then
                    'something's wrong
                    Err.Raise(-20019, , "Could not evaluate the string '" & xText & "'.")
                End If

                xEnvVar = Mid$(xText, iPos + 1, iPos2 - iPos - 1)

                'evaluate
                xEnvVar = Environ(xEnvVar)

                xText = Left$(xText, iPos - 1) + xEnvVar + Mid$(xText, iPos2 + 1)
                iPos = InStr(iPos2 + 1, xText, "%")
            End While

            EvaluateEnvironmentVariables = xText

            Exit Function
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)

        End Function

        Public Shared ReadOnly Property Version() As Short
            Get
                'returns the version of the currently active word version
                Dim xVersion As String
                Dim lPos As Integer

                Try
                    xVersion = GlobalMethods.CurWordApp.Version
                    lPos = InStr(xVersion, ".")
                    Version = CInt(Left$(xVersion, lPos - 1))
                Catch
                    Try
                        'Word might be closed - get from registry
                        Return GetWinwordVersionFromRegistry()
                    Catch oE As Exception
                        Throw New Exception("Could not retrieve MS Word version.", oE)
                    End Try
                End Try
            End Get
        End Property

        ''' <summary>
        ''' returns the major version number of Word
        ''' </summary>
        ''' <returns></returns>
        Private Shared Function GetWinwordVersionFromRegistry() As Short
            Dim xWinEXEPath As String
            'GLOG 8438
            'xWinEXEPath = Registry.GetLocalMachineValue(
            '    @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe", "")
            xWinEXEPath = RegAPI.GetKeyValue(RegAPI.HKEY_LOCAL_MACHINE, _
                "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe", "")

            Try
                Dim oVersionInfo As System.Diagnostics.FileVersionInfo =
                    System.Diagnostics.FileVersionInfo.GetVersionInfo(xWinEXEPath)

                Return CShort(oVersionInfo.ProductMajorPart)
            Catch oE As Exception
                Throw New Exception("Could not retrieve MS Word version from registry.", oE)
            End Try
        End Function


        Public Shared Sub SuppressDocXMLTemplateSavePrompt()
            'prevents templates from being saved when they are not open as documents
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.SuppressDocXMLTemplateSavePrompt"

            SuppressTemplateSavePrompt("ForteNormal.dotx")
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub
        Public Shared Sub SuppressAttachedTemplateSavePrompt(ByVal oDoc As Word.Document)
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.SuppressAttachedTemplateSavePrompt"
            SuppressTemplateSavePrompt(oDoc.AttachedTemplate.Name)
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub
        Public Shared Sub SuppressTemplateSavePrompt(ByVal xTemplate As String)
            'prevents templates from being saved when they are not open as documents
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.SuppressTemplateSavePrompt"
            Dim oTemplate As Word.Template = Nothing
            Dim oDoc As Word.Document

            On Error GoTo ProcError

            For Each oTemplate In GlobalMethods.CurWordApp.Templates
                If oTemplate.Name = xTemplate Then
                    Exit For
                End If
            Next oTemplate

            If Not oTemplate Is Nothing Then
                'GLOG 5710: Don't reset Normal.dotm, as this will prevent
                'AutoText entries and Macros from being saved
                If UCase(oTemplate.Name) = "NORMAL.DOTM" Then
                    Exit Sub
                End If
                'check if template is open in Word
                oDoc = Nothing

                On Error Resume Next
                oDoc = GlobalMethods.CurWordApp.Documents(oTemplate.FullName)
                On Error GoTo ProcError

                If oDoc Is Nothing Then
                    'doc is not open in Word - ensure that
                    'the user is not prompted to save it
                    oTemplate.Saved = True
                End If
            End If
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub

        Friend Shared Sub ShowError()
            'display the current error
            Dim xErr As String
            xErr = "The following unexpected error occurred: " & vbCrLf & vbCrLf & _
                "Description: " & Err.Description & vbCrLf & _
                "Source: " & Err.Source

            MsgBox(xErr, vbCritical, "Forte")
        End Sub

        Public Shared Function SelectionIsInRange(oRng As Word.Range) As Boolean
            'returns true iff the current selection is contained in the specified range
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.IsInRange"

            Dim oSel As Word.Range
            Dim bSelStartsWithTag As Boolean
            Dim bSelEndsWithTag As Boolean

            On Error GoTo ProcError

            oSel = GlobalMethods.CurWordApp.Selection.Range

            'exclude starting and ending xml tags from selection
            If Not GlobalMethods.CurWordApp.Selection.XMLParentNode Is Nothing Then
                bSelStartsWithTag = GlobalMethods.CurWordApp.Selection.XMLParentNode.Range.Start = oSel.Start + 1
                bSelEndsWithTag = GlobalMethods.CurWordApp.Selection.XMLParentNode.Range.End = oSel.End - 1
            End If

            'selection starts with tag -
            Return (oSel.StoryType = oRng.StoryType) And _
                ((oSel.Start >= oRng.Start) Or bSelStartsWithTag) And ((oSel.End <= oRng.End) Or bSelEndsWithTag)
            Exit Function
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Function

        Public Shared Function GetNormalTemplateXML() As String
            'returns the XML of the normal template
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.GetNormalTemplateXML"

            Dim oWord As Word.Application
            Dim bLaunchedWord As Boolean
            Dim oDoc As Word.Document

            On Error Resume Next
            oWord = GlobalMethods.CurWordApp
            On Error GoTo ProcError

            If oWord Is Nothing Then
                'start Word
                oWord = New Word.Application
                bLaunchedWord = True
            End If

            'open ForteNormal.dot(x)
            Dim xTemplatesDir As String
            xTemplatesDir = GetAppKeyValue("RootDirectory") + "\Templates\"

            If GlobalMethods.CurWordApp.Version = "11.0" Then
                oDoc = GlobalMethods.CurWordApp.Documents.Open(xTemplatesDir + "ForteNormal.dot")
            Else
                oDoc = GlobalMethods.CurWordApp.Documents.Open(xTemplatesDir + "ForteNormal.dotx")
            End If

            'return normal.dot xml
            GetNormalTemplateXML = oDoc.Content.XML
            'GLOG 6564:(WdUnits.wdSectionSaved=true to avoid prompt with DMS
            oDoc.Saved = True
            oDoc.Close(False)

            If bLaunchedWord Then
                oWord.Quit()
            End If

            Exit Function
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Function

        Public Shared Sub EditCopy(ByRef oTags As Tags, ByRef bRefreshRequired As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.EditCopy"
            Dim xExcludedmDels As String = ""
            Dim oAdjustedRange As Word.Range = Nothing
            Dim oDeleteScope As DeleteScope = Nothing
            Dim oSelRange As Word.Range = Nothing

            On Error GoTo ProcError

            bRefreshRequired = False
            oSelRange = GlobalMethods.CurWordApp.Selection.Range

            Echo(False)

            'validate/adjust selected content
            oAdjustedRange = GetCopyRange(oTags, xExcludedmDels)

            'store document variables for content controls in range -
            'this is necessary because they may get pasted into a different document
            BaseMethods.AddDocVarsToStore(oAdjustedRange)

            'copy - error trapped for Word 2003 as macro gets called
            'regardless of whether the command is available (GLOG 4205) -
            'if range hasn't been adjusted, copy selection to avoid erring
            'when only a graphic is selected (GLOG 4255)
            On Error Resume Next
            If (oAdjustedRange.Start <> GlobalMethods.CurWordApp.Selection.Start) Or _
                    (oAdjustedRange.End <> GlobalMethods.CurWordApp.Selection.End) Then
                oAdjustedRange.Copy()
            Else
                GlobalMethods.CurWordApp.Selection.Copy()
            End If
            'GLOG 5629: Display any Native Word Error text (such as when attempting to copy text within a deletion with Track Changes on)
            If Err.Number <> 0 Then
                MsgBox(Err.Description)
            End If
            On Error GoTo ProcError

            'restore excluded mDels
            If xExcludedmDels <> "" Then
                oDeleteScope = New DeleteScope
                oDeleteScope.RestoreUncopiedmDels(xExcludedmDels, oTags, _
                    BaseMethods.FileFormat(GlobalMethods.CurWordApp.Selection.Document))

                'GLOG 3727 (dm) - flag to refresh and restore original selection
                bRefreshRequired = True
                oSelRange.Select()
            End If

            Echo(True)

            Exit Sub
ProcError:
            MsgBox(Err.Description)
            'GlobalMethods.RaiseError (mpThisFunction)
            Echo(True)
            Exit Sub
        End Sub
        Public Shared Sub FilePrint()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FilePrint"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="FilePrint", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.Dialogs(WdWordDialog.wdDialogFilePrint).Show()
                End If
            End If

            Exit Sub

ProcError:
            'Ignore Cancel or page range error already handled by Word
            If Err.Number <> 102 And Err.Number <> 1045 And Err.Number <> 1050 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FilePrintDefault()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FilePrintDefault"
            On Error GoTo ProcError
            'Run built-in command
            GlobalMethods.CurWordApp.WordBasic.FilePrintDefault()
            'GlobalMethods.CurWordApp.WordBasic.ToolsMacro Name:="FilePrint", Show:=2, Run:=1
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub

        'GLOG : 7323 : ceh
        Public Shared Sub FilePrintCurrentPage()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FilePrintCurrentPage"
            On Error GoTo ProcError
            GlobalMethods.CurWordApp.PrintOut(Range:=WdPrintOutRange.wdPrintCurrentPage)
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub

        Public Shared Sub FileSave()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileSave"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                'GLOG 3777: Avoid error if saving Read-only document, or
                'if saving using same path/name as another open document
                If GlobalMethods.CurWordApp.ActiveDocument.ReadOnly Or GlobalMethods.CurWordApp.ActiveDocument.Path = "" Then
                    FileSaveAs()
                    Exit Sub
                End If
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="FileSave", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.WordBasic.FileSave()
                End If
            End If
            Exit Sub
ProcError:
            'Ignore Cancel
            If Err.Number <> 102 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FileSaveAs()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileSaveAs"
            Dim lErr As Integer = 0
            Dim xDesc As String = ""
ShowDialog:
            On Error Resume Next
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                'GLOG 3777: Always use Dialog.Show method here, because if attempting to
                'save with same path/name as an open document, Word will leave original document
                'with a temp name after using "FileSaveAs"
                '        ' ToolsMacro is most compatible, but not available in protected documents
                '        If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = Word.wdUnits.wdWordProtectionType.wdNoProtection Then
                '            GlobalMethods.CurWordApp.WordBasic.ToolsMacro Name:="FileSaveAs", Show:=2, Run:=1
                '        Else
                GlobalMethods.CurWordApp.Dialogs(WdWordDialog.wdDialogFileSaveAs).Show()
                '        End If
                lErr = Err.Number
                xDesc = Err.Description
            End If
            If lErr = 1057 Then
                'GLOG 3777: Can't save with same name as another open document
                'Word will have already displayed error message if using ToolsMacro method
                Exit Sub
            ElseIf lErr = 5153 Then
                'GLOG 3777: Can't save with same name as another open document
                'If using Dialog.Show method, display Word's internal error message for this condition
                MsgBox(xDesc, vbOKOnly + vbExclamation, "Forte")
                'Display Save As dialog again
                GoTo ShowDialog
                'Ignore Cancel
            ElseIf lErr <> 102 And lErr <> 0 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FileSaveAll()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileSaveAll"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="FileSaveAll", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.WordBasic.FileSaveAll()
                End If
            End If
            Exit Sub
ProcError:
            If Err.Number <> 102 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FileClose()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileClose"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="FileClose", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.WordBasic.FileClose()
                End If
            End If
            Exit Sub
ProcError:
            'Ignore Cancel
            If Err.Number <> 102 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub DocClose()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.DocClose"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="DocClose", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.WordBasic.DocClose()
                End If
            End If
            Exit Sub
ProcError:
            'Ignore Cancel
            If Err.Number <> 102 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FileCloseAll()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileCloseAll"
            On Error GoTo ProcError
            'Run built-in command
            If GlobalMethods.CurWordApp.Documents.Count > 0 Then
                ' ToolsMacro is most compatible, but not available in protected documents
                If GlobalMethods.CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="FileCloseAll", Show:=2, Run:=1)
                Else
                    GlobalMethods.CurWordApp.WordBasic.FileCloseAll()
                End If
            End If
            Exit Sub
ProcError:
            'Ignore Cancel
            If Err.Number <> 102 Then
                Err.Raise(Err.Number, mpThisFunction, Err.Description)
            End If
        End Sub
        Public Shared Sub FileExit()
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FileExit"
            Dim iCount As Short
            On Error GoTo ProcError
            'Close documents individually, since there's no way to trap a cancel of the FileExit process
            While GlobalMethods.CurWordApp.Documents.Count
                iCount = GlobalMethods.CurWordApp.Documents.Count
                FileClose()
                If iCount = GlobalMethods.CurWordApp.Documents.Count Then
                    'Close was cancelled - also cancel exit
                    Exit Sub
                End If
            End While
            'Call FileExit once all windows closed
            GlobalMethods.CurWordApp.WordBasic.FileExit()
            Exit Sub
ProcError:
            Err.Raise(Err.Number, mpThisFunction, Err.Description)
        End Sub

        Public Shared Sub EditPasteWordBasic()
            GlobalMethods.CurWordApp.WordBasic.EditPaste()
        End Sub
        Public Shared Sub EditCopyWordBasic()
            GlobalMethods.CurWordApp.WordBasic.EditCopy()
        End Sub
        Public Shared Sub EditCutWordBasic()
            GlobalMethods.CurWordApp.WordBasic.EditCut()
        End Sub
        Public Shared Function ActiveDocPath() As String
            If Not GlobalMethods.CurWordApp.ActiveDocument Is Nothing Then
                'Use GlobalMethods.CurWordApp.WordBasic to get Path, since DMS may change behavior of GlobalMethods.CurWordApp.ActiveDocument.Path
                ActiveDocPath = GlobalMethods.CurWordApp.WordBasic.FileNameInfo$(GlobalMethods.CurWordApp.WordBasic.FileName$(), 5)
            End If
        End Function

        Public Shared Sub ChangeCase()
            'if tags are off, start tag may be inadvertently selected, preventing ChangeCase
            'from working properly - this method adjusts selection if necessary - note that
            'this will not work with multiple non-"" tags selected, but that's a rarer
            'scenario and a bigger coding project
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.ChangeCase"
            Dim rngTag As Word.Range
            Dim bAdjust As Boolean

            On Error GoTo ProcError

            Dim oSel As Word.Selection = GlobalMethods.CurWordApp.Selection
            If GlobalMethods.CurWordApp.ActiveWindow.View.ShowXMLMarkup = 0 Then
                Echo(False)
                GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, True)
                If oSel.XMLNodes.Count > 0 Then
                    rngTag = oSel.XMLNodes(1).Range
                    bAdjust = ((oSel.Start = rngTag.Start - 1) And (oSel.End <= rngTag.End))
                    If bAdjust Then _
                        oSel.MoveStart()
                End If
                GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="ChangeCase", Show:=2, Run:=1)
                If bAdjust Then _
                    oSel.MoveStart(, -1)
                GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, False, True)
                Echo(True)
            Else
                GlobalMethods.CurWordApp.WordBasic.ToolsMacro(Name:="ChangeCase", Show:=2, Run:=1)
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Echo(True)
            Exit Sub
        End Sub

        Public Shared Sub EditCut(ByRef oTags As Tags, ByRef bRefreshRequired As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.EditCut"
            Dim xExcludedmDels As String
            Dim oDeleteScope As DeleteScope
            Dim oAdjustedRange As Word.Range

            On Error GoTo ProcError

            bRefreshRequired = False

            Echo(False)

            'validate/adjust selected content
            oAdjustedRange = GetCopyRange(oTags, xExcludedmDels)

            'store document variables for content controls in range -
            'this is necessary because they may get pasted into a different document
            '10/17/11 (dm) - this has always been in copy, but was missing from cut
            BaseMethods.AddDocVarsToStore(oAdjustedRange)

            'copy
            On Error Resume Next
            oAdjustedRange.Cut()
            'GLOG 5629: Display any Native Word Error text (such as when attempting to cut text within a deletion with Track Changes on)
            If Err.Number <> 0 Then
                MsgBox(Err.Description)
                Echo(True)
                Exit Sub
            End If
            On Error GoTo ProcError
            'restore excluded mDels
            If xExcludedmDels <> "" Then
                oDeleteScope = New DeleteScope
                oDeleteScope.RestoremDels(oAdjustedRange, xExcludedmDels, oTags)

                'GLOG 3727 (dm) - flag to refresh
                bRefreshRequired = True
            End If

            Echo(True)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Echo(True)
            Exit Sub
        End Sub

        Public Shared Function GetCopyRange(ByRef oTags As Tags, ByRef xExcludedmDels As String) As Word.Range
            'validates/adjusts selected content before a copy or cut
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.GetCopyRange"
            Dim oParentTag As Word.XMLNode
            Dim oParentTag2 As Word.XMLNode
            Dim oRange As Word.Range
            Dim oParentTagRange As Word.Range
            Dim oSelRange As Word.Range
            Dim iTagsAtStart As Short
            Dim oDeleteScope As DeleteScope
            Dim oEndRange As Word.Range
            Dim bExpand As Boolean

            On Error GoTo ProcError

            oSelRange = GlobalMethods.CurWordApp.Selection.Range
            With oSelRange
                If BaseMethods.FileFormat(GlobalMethods.CurWordApp.Selection.Document) = mpFileFormats.OpenXML Then
                    'content controls - added 10/10/19 (dm) -
                    'exclude mDels where appropriate
                    oDeleteScope = New DeleteScope
                    oDeleteScope.ValidateCopiedmDels(xExcludedmDels, oTags, oSelRange, mpFileFormats.OpenXML)
                ElseIf GlobalMethods.CurWordApp.ActiveWindow.View.ShowXMLMarkup = False Then
                    'if tags aren't showing, and the user has selected the first character of
                    'an mSEG through the end of the mSEG, ensure that the start tag is included
                    If Len(.Text) > 1 Then
                        'get parent node at start of selection
                        oRange = .Duplicate
                        oRange.StartOf()
                        oParentTag = oRange.XMLParentNode

                        'cycle up through generations until we reach an mSEG
                        Do While Not oParentTag Is Nothing
                            If oParentTag.BaseName = "mSEG" Then
                                Exit Do
                            Else
                                oParentTag = oParentTag.ParentNode
                                iTagsAtStart = iTagsAtStart + 1
                            End If
                        Loop

                        'if there's a parent mSEG, compare its range to the selection -
                        'expand selection to include start tag if 1) the starts are the same, taking
                        'start tags into account, and 2) the end tag is already selected or there's
                        'nothing but an "" paragraph between the end of the selection and the end tag
                        If Not oParentTag Is Nothing Then
                            oParentTagRange = oParentTag.Range
                            If .Start <= oParentTagRange.Start + iTagsAtStart Then
                                bExpand = (.End >= oParentTagRange.End)
                                If Not bExpand Then
                                    oEndRange = GlobalMethods.CurWordApp.ActiveDocument.Range(.End, oParentTag.Range.End)
                                    bExpand = ((oEndRange.Text = "") And (oEndRange.Paragraphs.Count = 1))
                                End If
                                If bExpand Then
                                    .SetRange(oParentTagRange.Start - 1, BaseMethods.Max(.End, oParentTagRange.End + 1))
                                End If
                            End If
                        End If
                    End If

                    'exclude mDels where appropriate
                    oDeleteScope = New DeleteScope
                    oDeleteScope.ValidateCopiedmDels(xExcludedmDels, oTags, oSelRange, mpFileFormats.Binary)
                End If
            End With

            GetCopyRange = oSelRange

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Echo(True)
            Exit Function
        End Function

        Public Shared Sub CheckIBFMetadata()

            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.CheckIBFMetadata"
            Dim xAppData As String
            Dim xIBFCacheManager As String
            Dim xIBFData As String

            On Error GoTo ProcError

            'check that ibf metadata has been imported
            xAppData = Environ$("USERPROFILE") & "\Local Settings\Application Data\Microsoft\InformationBridgeFramework\Cache\PersistentCache\"
            If Dir$(xAppData) = "24e7a8fa2bb5f5621611c19add738acf0aaa56aa" Then
                'IBF meta data already imported
            Else

                xIBFCacheManager = Environ$("ProgramFiles") & "\Microsoft Information Bridge\1.5\Framework\Microsoft.InformationBridge.Tools.CacheManager.exe"
                xIBFData = GetAppKeyValue("RootDirectory") & "\Bin\mp10IBF.xml"

                'import metadata
                Shell(xIBFCacheManager & " " & "/import:* /file:" & Chr(34) & xIBFData & Chr(34) & " /force", vbHide)

            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Echo(True)
            Exit Sub
        End Sub
        Public Shared Sub ExecuteFileSaveCommand()
            'For use with Interwoven or other DMS that does not allow direct access to a Save command
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.ExecuteFileSaveCommand"
            Dim cbp As Object
            Dim cb As Object
            Dim oBars As Object

            On Error GoTo ProcError

            'GLOG 5123: Don't compare Version to Numeric value
            'Use Alternate method for Word 12 and higher
            oBars = GlobalMethods.CurWordApp.Application.CommandBars
            oBars.ExecuteMso("FileSave")

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Shared Sub ExecuteFileSaveAsCommand()
            'For use with Interwoven or other DMS that does not allow direct access to a Save As command
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.ExecuteFileSaveAsCommand"
            Dim cbp As Object
            Dim cb As Object
            Dim oBars As Object

            On Error GoTo ProcError

            'GLOG 5123: Don't compare Version to Numeric value
            'Use Alternate method for Word 12 and higher
            oBars = GlobalMethods.CurWordApp.Application.CommandBars
            oBars.ExecuteMso("FileSaveAs")
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Shared Function GetLastNormalStylesUpdate() As String
            'returns the date time string of the last normal styles update
            Dim xVal As String

            On Error Resume Next
            xVal = GlobalMethods.CurWordApp.NormalTemplate.CustomDocumentProperties("mpLastNormalStylesUpdate")
            GetLastNormalStylesUpdate = xVal
        End Function

        Public Shared Sub SetLastNormalStylesUpdate(ByVal xDateTime As String)
            'sets the mpLastNormalStylesUpdate document property for Normal
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.SetLastNormalStylesUpdate"
            Dim oDoc As Word.Document
            Dim bOpened As Boolean

            On Error Resume Next
            oDoc = GlobalMethods.CurWordApp.Documents(GlobalMethods.CurWordApp.NormalTemplate.Name)
            On Error GoTo ProcError

            If oDoc Is Nothing Then
                'open invisible - document has to be open to
                'set a custom document property
                oDoc = GlobalMethods.CurWordApp.Documents.Open(GlobalMethods.CurWordApp.NormalTemplate.FullName, , , , , , , , , , , False)
                bOpened = True
            End If

            'set doc prop as a template property - won't get
            'propogated to documents based on Normal
            On Error GoTo AddDocProp
            GlobalMethods.CurWordApp.NormalTemplate.CustomDocumentProperties("mpLastNormalStylesUpdate") = xDateTime

            If bOpened Then
                'close
                oDoc.Close(Word.WdSaveOptions.wdSaveChanges)
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
AddDocProp:
            GlobalMethods.CurWordApp.NormalTemplate.CustomDocumentProperties.Add(Name:="mpLastNormalStylesUpdate", _
                LinkToContent:=False, Type:=MsoDocProperties.msoPropertyTypeDate, _
                Value:=xDateTime)
            Resume Next
        End Sub
        Public Shared Sub DisableForteMenuItems()
            '    Dim oCmdBar As CommandBar
            '    Dim oMenu As CommandBar
            '    Dim oMenu2 As CommandBarPopup
            '   (WdUnits.wdSectionoCmdBar = GlobalMethods.CurWordApp.CommandBars("Menu Bar")
            '   (WdUnits.wdSectionoMenu2 = oCmdBar.Controls("Forte")
            '    Dim oMenuItem As CommandBarButton
            '
            '    For Each oMenuItem In oMenu2.Controls
            '        If oMenuItem.Caption
        End Sub
        Public Shared Function ProtectedViewIsActive() As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.ProtectedViewIsActive"
            On Error GoTo ProcError
            'GLOG 5099: Returns true if current document is in Word 2010 Protected View Mode
            If Val(GlobalMethods.CurWordApp.Version) < 14 Then
                ProtectedViewIsActive = False
            Else
                'Use late binding, since property doesn't exist pre-Word 2010
                Dim oWord As Object
                oWord = GlobalMethods.CurWordApp
                ProtectedViewIsActive = Not oWord.ActiveProtectedViewWindow Is Nothing
            End If
ProcError:
        End Function

        Public Shared Function IsPostInjunctionWordVersion() As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion"

            On Error GoTo ProcError
            IsPostInjunctionWordVersion = BaseMethods.IsPostInjunctionWordVersion
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function StartUndoRecording(ByVal xName As String) As LMP.Forte.MSWord.Undo
            'starts recording of the Word custom undo record
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.StartUndoRecording"

            On Error GoTo ProcError 'GLOG 7855

            Dim oUndo As Undo
            oUndo = New Undo

            oUndo.MarkStart(xName)
            StartUndoRecording = oUndo 'GLOG 7855
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Sub EndUndoRecording(ByVal oUndo As LMP.Forte.MSWord.Undo)
            'end recording of the Word custom undo record
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.EndUndoRecording"
            'GLOG 7855
            On Error GoTo ProcError
            If oUndo Is Nothing Then
                Exit Sub
            End If
            oUndo.MarkEnd()
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Function FilePrintDraft(ByVal oDoc As Word.Document, ByVal xProtectionPassword As String) As Boolean
            'prints to default trays
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FilePrintDraft"
            Dim oSec As Word.Section
            Dim oButton As Object
            Dim iProtection As WdProtectionType
            Dim bReprotect As Boolean

            On Error GoTo ProcError

            If oDoc.ProtectionType <> WdProtectionType.wdNoProtection Then
                iProtection = oDoc.ProtectionType
                oDoc.Unprotect(xProtectionPassword)
                bReprotect = True
            End If

            '  (WdUnits.wdSectiontrays to default
            For Each oSec In oDoc.Sections
                With oSec.PageSetup
                    .FirstPageTray = WdPaperTray.wdPrinterDefaultBin
                    .OtherPagesTray = WdPaperTray.wdPrinterDefaultBin
                End With
            Next oSec

            GlobalMethods.CurWordApp.ScreenUpdating = True

            '   show Print dialog - need this because iManage takes over Print command
            '   Application.Dialogs(wdDialogFilePrint).Show
            On Error Resume Next
            oButton = GlobalMethods.CurWordApp.CommandBars("File").FindControl(Id:=4)
            oButton.Execute()
            On Error GoTo ProcError

            If bReprotect Then
                oDoc.Protect(iProtection, True, xProtectionPassword)
            End If

            FilePrintDraft = True

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function FilePrintFinal(ByVal oDoc As Word.Document, ByVal xProtectionPassword As String, ByRef lFirstPageTray As Integer, ByRef lOtherPagesTray As Integer) As Boolean
            'sets final trays, then presents print dialog
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.FilePrintFinal"
            Dim oFPS As Word.Dialog
            Dim oSec As Word.Section
            Dim oButton As Object
            Dim lAction As Integer
            Dim iProtection As WdProtectionType
            Dim bReprotect As Boolean

            On Error GoTo ProcError

            If oDoc.ProtectionType <> WdProtectionType.wdNoProtection Then
                iProtection = oDoc.ProtectionType
                oDoc.Unprotect(xProtectionPassword)
                bReprotect = True
            End If

            '   show Page Setup
            oDoc.Application.ScreenUpdating = True

            On Error Resume Next
            GlobalMethods.CurWordApp.Selection.PageSetup.FirstPageTray = lFirstPageTray
            GlobalMethods.CurWordApp.Selection.PageSetup.OtherPagesTray = lOtherPagesTray
            On Error GoTo ProcError

            oFPS = GlobalMethods.CurWordApp.Dialogs(WdWordDialog.wdDialogFilePageSetup)
            With oFPS
                'GLOG 8809: previously wdDialogFilePageSetupTabPaperSource in VB6
                .DefaultTab = WdWordDialogTab.wdDialogFilePageSetupTabPaper '150001
                lAction = .Show()

                '       check to see if dialog was cancelled
                If lAction <> 0 Then
                    For Each oSec In oDoc.Sections
                        With oSec.PageSetup
                            .FirstPageTray = oFPS.FirstPage
                            .OtherPagesTray = oFPS.OtherPages
                        End With
                    Next oSec
                    lFirstPageTray = oFPS.FirstPage
                    lOtherPagesTray = oFPS.OtherPages
                Else
                    If bReprotect Then
                        oDoc.Protect(iProtection, True, xProtectionPassword)
                    End If

                    FilePrintFinal = False
                    Exit Function
                End If
            End With

            oDoc.Application.ScreenUpdating = False
            System.Windows.Forms.Application.DoEvents()
            oDoc.Application.ScreenUpdating = True

            '   show Print dialog - need this because iManage takes over Print command
            '   Application.Dialogs(wdDialogFilePrint).Show
            On Error Resume Next
            oButton = GlobalMethods.CurWordApp.CommandBars("File").FindControl(Id:=4)
            oButton.Execute()
            On Error GoTo ProcError

            If bReprotect Then
                oDoc.Protect(iProtection, True, xProtectionPassword)
            End If

            FilePrintFinal = True

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsCompareResultsWindow(ByVal oWindow As Word.Window) As Boolean
            'returns true iff the specified window is a compare results window-
            'we define such a window as one with multiple panes where at least one
            'of the panes contains a different document
            Const mpThisFunction As String = "LMP.Forte.MSWord.WordApp.IsCompareResultsWindow"
            Dim oPane As Word.Pane
            Dim xDocName As String

            On Error GoTo ProcError

            If oWindow.Panes.Count = 1 Then
                '7/14/11 (dm) - added this branch to handle Hide Source Documents option -
                'additionally, the other panes may be closed by the user at some point
                xDocName = oWindow.Document.Name
                If InStr(1, xDocName, "Compar", vbTextCompare) = 1 Then
                    IsCompareResultsWindow = True
                    Exit Function
                End If
            Else
                For Each oPane In oWindow.Panes
                    If xDocName = "" Then
                        xDocName = oPane.Document.Name
                    Else
                        If xDocName <> oPane.Document.Name Then
                            IsCompareResultsWindow = True
                            Exit Function
                        End If
                    End If
                Next oPane

                'GLOG 7949
                If xDocName = "" And InStr(oWindow.Caption, "Compare Result") = 1 Then
                    IsCompareResultsWindow = True
                    Exit Function
                End If
            End If

            IsCompareResultsWindow = False

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function IsAutoSaveEvent() As Boolean
            On Error GoTo ProcError
            IsAutoSaveEvent = GlobalMethods.CurWordApp.WordBasic.IsAutoSaveEvent
            Exit Function
ProcError:
            IsAutoSaveEvent = False
            Exit Function
        End Function

        Public Shared Sub CreateGrailDataDumpDoc(ByVal xGrailData As String)
            GlobalMethods.CurWordApp.ScreenUpdating = False
            GlobalMethods.CurWordApp.Documents.Add()
            GlobalMethods.CurWordApp.ActiveDocument.Content.InsertAfter(xGrailData)

            With GlobalMethods.CurWordApp.Selection
                .HomeKey(Unit:=WdUnits.wdStory)
                .MoveRight(Unit:=Word.WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)
                .Copy()
                .Find.ClearFormatting()
                .Find.Replacement.ClearFormatting()
                With .Find.Replacement.ParagraphFormat
                    .SpaceBeforeAuto = False
                    .SpaceAfterAuto = False
                End With
                With .Find
                    .Text = "�"
                    .Replacement.Text = "^p"
                    .Forward = True
                    .Wrap = WdFindWrap.wdFindContinue
                    .Format = True
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                .MoveDown(Unit:=WdUnits.wdLine, Count:=1)
                .MoveLeft(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                .MoveRight(Unit:=Word.WdUnits.wdCharacter, Count:=12)
                .MoveRight(Unit:=Word.WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)
                .Copy()
                .Find.ClearFormatting()
                .Find.Replacement.ClearFormatting()
                With .Find.Replacement.ParagraphFormat
                    .SpaceBeforeAuto = False
                    .SpaceAfterAuto = False
                End With
                With .Find
                    .Text = "�"
                    .Replacement.Text = "^t"
                    .Forward = True
                    .Wrap = WdFindWrap.wdFindContinue
                    .Format = True
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                If .PageSetup.Orientation = WdOrientation.wdOrientPortrait Then
                    .PageSetup.Orientation = WdOrientation.wdOrientLandscape
                Else
                    .PageSetup.Orientation = WdOrientation.wdOrientPortrait
                End If
                With .PageSetup
                    .LineNumbering.Active = False
                    .Orientation = WdOrientation.wdOrientLandscape
                    .TopMargin = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .BottomMargin = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .LeftMargin = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .RightMargin = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .Gutter = GlobalMethods.CurWordApp.InchesToPoints(0)
                    .HeaderDistance = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .FooterDistance = GlobalMethods.CurWordApp.InchesToPoints(0.5)
                    .PageWidth = GlobalMethods.CurWordApp.InchesToPoints(11)
                    .PageHeight = GlobalMethods.CurWordApp.InchesToPoints(8.5)
                    .FirstPageTray = WdPaperTray.wdPrinterDefaultBin
                    .OtherPagesTray = WdPaperTray.wdPrinterDefaultBin
                    .SectionStart = WdSectionStart.wdSectionNewPage
                    .OddAndEvenPagesHeaderFooter = False
                    .DifferentFirstPageHeaderFooter = False
                    .VerticalAlignment = WdVerticalAlignment.wdAlignVerticalTop
                    .SuppressEndnotes = False
                    .MirrorMargins = False
                    .TwoPagesOnOne = False
                    .BookFoldPrinting = False
                    .BookFoldRevPrinting = False
                    .BookFoldPrintingSheets = 1
                    .GutterPos = WdGutterStyle.wdGutterPosLeft
                End With
                .WholeStory()
                .ConvertToTable(Separator:=WdTableFieldSeparator.wdSeparateByTabs, NumColumns:=2, _
                    NumRows:=66, AutoFitBehavior:=WdAutoFitBehavior.wdAutoFitFixed)
                With .Tables(1)
                    .Style = "Table Grid"
                    .ApplyStyleHeadingRows = True
                    .ApplyStyleLastRow = False
                    .ApplyStyleFirstColumn = True
                    .ApplyStyleLastColumn = False
                End With
                GlobalMethods.CurWordApp.ActiveWindow.ActivePane.VerticalPercentScrolled = 89
                .EndKey(Unit:=WdUnits.wdStory)
                .MoveLeft(Unit:=Word.WdUnits.wdCharacter, Count:=2)
                .MoveLeft(Unit:=WdUnits.wdCell)
                GlobalMethods.CurWordApp.Run(MacroName:="MP10.mdlFc.NextCell")
                .Copy()

                Dim oDoc As Word.Document

                oDoc = GlobalMethods.CurWordApp.Documents.Add(DocumentType:=WdNewDocumentType.wdNewBlankDocument)
                oDoc.Content.PasteAndFormat(WdRecoveryType.wdFormatOriginalFormatting)
                oDoc.Range(0, 0).Select()
                With GlobalMethods.CurWordApp.Selection
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find
                        .Text = "</"
                        .Replacement.Text = "|&~"
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    With .Find
                        .Text = "<"
                        .Replacement.Text = "|~"
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    With .Find
                        .Text = ">"
                        .Replacement.Text = "~|"
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find
                        .Text = "Index=""*"" UNID=""0""~|"
                        .Replacement.Text = "="
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find
                        .Text = "Index=""*"" UNID=""0""~|"
                        .Replacement.Text = "= "
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchAllWordForms = False
                        .MatchSoundsLike = False
                        .MatchWildcards = True
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find
                        .Text = "|&~*~|"
                        .Replacement.Text = ""
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchAllWordForms = False
                        .MatchSoundsLike = False
                        .MatchWildcards = True
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    With .Find
                        .Text = "|~"
                        .Replacement.Text = "^p"
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchAllWordForms = False
                        .MatchSoundsLike = False
                        .MatchWildcards = True
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find
                        .Text = "DEFENDANTNAME1"
                        .Replacement.Text = "^pDEFENDANTNAME1"
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)

                    .HomeKey(Unit:=WdUnits.wdStory)
                    .Delete(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                    .Delete(Unit:=Word.WdUnits.wdCharacter, Count:=1)
                    With GlobalMethods.CurWordApp.ActiveDocument.Styles("Normal").ParagraphFormat
                        .LeftIndent = GlobalMethods.CurWordApp.InchesToPoints(0)
                        .RightIndent = GlobalMethods.CurWordApp.InchesToPoints(0)
                        .SpaceBefore = 0
                        .SpaceBeforeAuto = False
                        .SpaceAfter = 0
                        .SpaceAfterAuto = False
                        .LineSpacingRule = WdLineSpacing.wdLineSpaceSingle
                        .Alignment = WdParagraphAlignment.wdAlignParagraphLeft
                        .WidowControl = True
                        .KeepWithNext = False
                        .KeepTogether = False
                        .PageBreakBefore = False
                        .NoLineNumber = False
                        .Hyphenation = True
                        .FirstLineIndent = GlobalMethods.CurWordApp.InchesToPoints(0)
                        .OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText
                        .CharacterUnitLeftIndent = 0
                        .CharacterUnitRightIndent = 0
                        .CharacterUnitFirstLineIndent = 0
                        .LineUnitBefore = 0
                        .LineUnitAfter = 0
                        .MirrorIndents = False
                        .TextboxTightWrap = WdTextboxTightWrap.wdTightNone
                    End With
                    GlobalMethods.CurWordApp.ActiveDocument.Styles("Normal").NoSpaceBetweenParagraphsOfSameStyle = _
                        False
                    With GlobalMethods.CurWordApp.ActiveDocument.Styles("Normal")
                        .AutomaticallyUpdate = False
                        .BaseStyle = ""
                        .NextParagraphStyle = "Normal"
                    End With
                    .Find.ClearFormatting()
                    .Find.Replacement.ClearFormatting()
                    With .Find.Replacement.ParagraphFormat
                        .SpaceBeforeAuto = False
                        .SpaceAfterAuto = False
                    End With
                    With .Find
                        .Text = "^0013br /~|"
                        .Replacement.Text = ", "
                        .Forward = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Format = True
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    .Find.Execute(Replace:=WdReplace.wdReplaceAll)

                    .WholeStory()
                    .Copy()

                    oDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
                End With
                .EndKey(Unit:=WdUnits.wdStory)
                .MoveLeft(Unit:=Word.WdUnits.wdCharacter, Count:=2)
                .MoveLeft(Unit:=WdUnits.wdCell)
                GlobalMethods.CurWordApp.Run(MacroName:="MP10.mdlFc.NextCell")
                .PasteAndFormat(WdRecoveryType.wdUseDestinationStylesRecovery)
                .HomeKey(Unit:=WdUnits.wdStory)
                .Rows.Delete()

                .Tables(1).Columns(1).SetWidth(ColumnWidth:=221.4, RulerStyle:= _
                    WdRulerStyle.wdAdjustNone)
                .Tables(1).Columns(2).SetWidth(ColumnWidth:=499.5, RulerStyle:= _
                    WdRulerStyle.wdAdjustNone)
                GlobalMethods.CurWordApp.ActiveWindow.ActivePane.VerticalPercentScrolled = 0
            End With

            GlobalMethods.CurWordApp.ScreenUpdating = True
        End Sub
    End Class
End Namespace
