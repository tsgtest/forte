'**********************************************************
'   LMP.Forte.MSWord.Pleading Class
'   created 1/31/07 by Doug Miller
'
'   'contains methods and functions that operate on pleadings
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Pleading
        Public Enum mpCollectionTableItemScopes
            FullRow = 0
            LeftCell = 1
            RightCell = 2
        End Enum

        Public Sub InsertCollectionTableItem(ByVal xXML As String, ByVal oLocation As Word.Range, _
                ByVal bAtCurrentRow As Boolean, ByVal iScope As mpCollectionTableItemScopes, _
                ByVal iItemTypeID As Short, ByVal iCollectionTypeID As Short, _
                ByRef oCollectionTag As Word.XMLNode, ByRef oTags As Tags)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.InsertCollectionTableItem"
            Dim oTable As Word.Table
            Dim oTag As Word.XMLNode
            Dim oRange As Word.Range
            Dim xTypeID As String
            Dim xName As String
            Dim oAttribute As Word.XMLNode
            Dim xAttributes(0, 0) As String
            Dim i As Short
            Dim iAttempts As Short
            Dim lTargetRow As Integer
            Dim iRows As Short
            Dim oDoc As Word.Document
            Dim bTableContainsParentTag
            Dim xText As String
            Dim lTargetColumn As Integer
            Dim iStart As Short
            Dim iEnd As Short
            Dim j As Short
            Dim oRow As Word.Row
            Dim oCellRange As Word.Range
            Dim oCellRange2 As Word.Range
            Dim lLoc As Integer
            Dim oWordDoc As WordDoc
            Dim bShowXML As Boolean
            Dim oSiblingTag As Word.XMLNode
            Dim bTrailingParaIsEmpty As Boolean
            Dim oTestRange As Word.Range
            Dim oStyle As Word.Style
            Dim oNextParaRange As Word.Range
            Dim oCell As Word.Cell
            Dim oConvert As Conversion
            Dim oTargetRng As Word.Range
            Dim oTestTag As Word.XMLNode
            Dim oObjectData As Word.XMLNode
            Dim xObjectData As String
            Dim bFound As Boolean
            Dim bIsOpenXml As Boolean
            Dim bMove As Boolean
            Dim xTag As String 'GLOG 7444 (dm)

            On Error GoTo ProcError

            oConvert = New Conversion
            'GLOG : 7705 : ceh
            oWordDoc = New WordDoc

            If oCollectionTag Is Nothing Then
                'get selected table
                With oLocation
                    If .Information(Word.wdInformation.wdWithInTable) Then
                        oTable = .Tables(1)
                    ElseIf .Start <> 0 Then
                        If .Previous(WdUnits.wdCharacter).Information(Word.wdInformation.wdWithInTable) Then _
                                oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                    End If
                End With

                'check whether selected table is wrapped by a collection tag
                'of the specified type
                If Not oTable Is Nothing Then
                    oCollectionTag = GetCollectionTableTag(oLocation, iCollectionTypeID)
                    If oCollectionTag Is Nothing Then
                        'raise error
                        Err.Raise(mpErrors.mpError_CannotInsertTableIntoTable, , _
                            "<Error_CannotInsertTableIntoTable>")
                    End If
                End If
            Else
                'get existing collection table
                oTable = oCollectionTag.Range.Tables(1)
            End If

            'store attributes of existing collection tag in an array
            BaseMethods.GetAttributesArray(oCollectionTag, xAttributes)

            'GLOG 7444 (dm) - moved this line up
            oDoc = oLocation.Document
            'GLOG 8722
            Dim bShowHidden As Boolean = oDoc.Bookmarks.ShowHidden
            If Not bShowHidden Then
                oDoc.Bookmarks.ShowHidden = True
            End If
            'delete existing collection tag
            If Not oCollectionTag Is Nothing Then
                'GLOG 7444 (dm) - delete bookmark as well to prevent retagging below
                xTag = BaseMethods.GetTag(oCollectionTag)
                If oDoc.Bookmarks.Exists("_" & xTag) Then _
                    oDoc.Bookmarks("_" & xTag).Delete()
                oCollectionTag.Delete()
            End If
            'GLOG 8722
            oDoc.Bookmarks.ShowHidden = bShowHidden

            'set insertion location to existing table
            If Not oTable Is Nothing Then
                'GLOG 4144: Rather than trying to determine whether or not an extra paragraph
                'has been inserted with XML that needs to be cleaned up afterward, we can take
                'control of this by always inserting an extra empty paragraph first, then deleting it afterward

                'if table is followed by an empty paragraph, we don't want to remove
                'the trailing paragraph inserted with the xml below
                '        Set oNextParaRange = oTable.Range.Next(WdUnits.wdParagraph)
                '        If oNextParaRange.Text = vbCr Then
                '            'exclude the final paragraph mark in the document
                '            On Error Resume Next
                '            Set oTestRange = oNextParaRange.Next(WdUnits.wdParagraph)
                '            On Error GoTo ProcError
                '            bTrailingParaIsEmpty = (Not oTestRange Is Nothing)
                '
                '            'preserve style
                '            If bTrailingParaIsEmpty Then _
                '                Set oStyle = oNextParaRange.Style
                '        End If

                'get number of existing rows
                iRows = oTable.Rows.Count

                'if target table is selected and insertion at current row
                'is specified, get current row
                If bAtCurrentRow Then
                    With oLocation
                        'GLOG 4433 (dm) - when recreating the last item in the table,
                        'oLocation may be just after the table, causing the old
                        'conditional to err - test for existence of cell
                        '                If .Cells.Count > 0 Then
                        'GLOG : 8115 : ceh - necessary for side-by-side in replacement mode
                        'because in that case, location is at end of table & cells.Count = 0
                        '                On Error Resume Next
                        '                Set oCell = .Cells(1)
                        '                On Error GoTo ProcError
                        '                If Not oCell Is Nothing Then
                        '                    If .Tables(1).Range.Start = oTable.Range.Start Then
                        '                        lTargetRow = oCell.RowIndex
                        '                    End If
                        '                End If
                        On Error Resume Next
                        lTargetRow = .Information(WdInformation.wdEndOfRangeRowNumber)
                        On Error GoTo ProcError
                    End With
                End If

                '        'if insertion in empty existing cell is specified, get target cell
                '        If bInEmptyExistingCell Then
                '            If bAtCurrentRow Then
                '                'search only target row
                '                iStart = lTargetRow
                '                iEnd = lTargetRow
                '            Else
                '                'search entire table
                '                iStart = 1
                '                iEnd = iRows
                '            End If
                '
                '            'locate first empty cell
                '            For i = iStart To iEnd
                '                Set oRow = oTable.Rows(i)
                '                For j = 1 To oRow.Cells.Count
                '                    If BaseMethods.CellIsEmpty(oRow.Cells(j)) Then
                '                        lTargetRow = i
                '                        lTargetColumn = j
                '                        Exit For
                '                    End If
                '                Next j
                '
                '                If lTargetColumn > 0 Then _
                '                    Exit For
                '            Next i
                '        End If

                If iScope = mpCollectionTableItemScopes.LeftCell Then
                    lTargetColumn = 1
                ElseIf iScope = mpCollectionTableItemScopes.RightCell Then
                    lTargetColumn = 2
                End If

                'reset range to target table
                oLocation = oTable.Range

                If BaseMethods.TableIsEmpty(oTable) Then
                    '1/27/11 (dm) - TableIsEmpty() will miss signatures
                    'containing nothing but a line
                    For Each oTestTag In oTable.Range.XMLNodes
                        If oTestTag.BaseName = "mSEG" Then
                            oObjectData = oTestTag.SelectSingleNode("@ObjectData")
                            If Not oObjectData Is Nothing Then
                                xObjectData = BaseMethods.Decrypt(oObjectData.NodeValue)
                                If BaseMethods.IsCollectionTableItem(xObjectData) Then
                                    bFound = True
                                    Exit For
                                End If
                            End If
                        End If
                    Next oTestTag

                    If Not bFound Then
                        'table is empty
                        If oTable.Range.XMLNodes.Count > 0 Then
                            bTableContainsParentTag = oTable.Range.XMLNodes(1).BaseName = "mSEG"
                        End If

                        'table is empty - delete it
                        oTable.Delete()
                        oTable = Nothing
                        lTargetRow = 0

                        'If table contained top-level mSeg, opening tag might have moved below where table was previously
                        If bTableContainsParentTag Then
                            oLocation.MoveEnd(WdUnits.wdCharacter, 1)
                            'GLOG item #5855 - dcf
                            oLocation.Document.ActiveWindow.View.ShowXMLMarkup = True
                            If (oLocation.XMLNodes.Count >= 1) Then
                                If oLocation.XMLNodes(1).BaseName = "mSEG" Then
                                    ' Make sure insertion is within mSEG
                                    oLocation = oLocation.XMLNodes(1).Range
                                    oLocation.Collapse(WdCollapseDirection.wdCollapseStart)

                                    'add a temporary paragraph mark to ensure
                                    'that insertion executes as expected -
                                    'this fixes GLOG item #5855 scenario 2 - dcf
                                    Dim oTempParaBmk As Bookmark
                                    oLocation.InsertParagraphAfter()
                                    oTempParaBmk = oLocation.Bookmarks.Add("zzmpTempPara")
                                    oLocation.Collapse(WdCollapseDirection.wdCollapseEnd)
                                Else
                                    oLocation.Collapse(WdCollapseDirection.wdCollapseStart)
                                End If
                            End If
                        End If
                    Else
                        'table is not empty
                        oLocation.EndOf()
                    End If
                Else
                    'table is not empty - move to end
                    oLocation.EndOf()
                End If
                'GLOG 7724: Insert paragraph after existing table
                'before inserting XML to avoid layout problem caused by KB2880529
                If Not oTable Is Nothing Then
                    oLocation.InsertBefore(vbCr)
                    oLocation.Collapse(WdCollapseDirection.wdCollapseEnd)
                End If
            End If

            'GLOG 4144: Always insert an extra paragraph for InsertXML
            oLocation.InsertBefore(vbCr)
            oLocation.Collapse(WdCollapseDirection.wdCollapseStart)

            'delete existing bookmarks in range (12/9/10)
            bIsOpenXml = BaseMethods.IsWordOpenXML(xXML)
            oTargetRng = oLocation.Duplicate
            If BaseMethods.IsPostInjunctionWordVersion() Or bIsOpenXml Then
                oTargetRng.WholeStory()
                'GLOG 7444 (dm) - set bSkipmSEGs to true for 10.5
                oConvert.DeleteMPContentBookmarks(oTargetRng, True, True)
            End If

            'insert xml
            oLocation.Select()
            oLocation.InsertXML(xXML)

            'GLOG : 7705 : ceh
            'check for existing, unconverted collection tables
            If (Not oTable Is Nothing) And oLocation.Tables.Count Then
                With oLocation.Tables(1)
                    If (.LeftPadding = 0 And .RightPadding = 0) Then
                        'convert original document table if necessary
                        oWordDoc.AdjustTableMargins(oTable.Range)
                    End If
                End With
            End If

            'GLOG 7724:  Delete intervening paragraph to merge tables
            If Not oTable Is Nothing Then
                oLocation.Previous(WdUnits.wdParagraph, 1).Delete()
            End If

            oLocation.Document.ActiveWindow.View.ShowXMLMarkup = False

            'ensure that content controls are bookmarked (1/19/11)
            If bIsOpenXml Then
                oTargetRng.WholeStory()
                oConvert.AddBookmarksToContentControls(oTargetRng)
            End If

            'add tags if necessary (12/9/10)
            If BaseMethods.IsPostInjunctionWordVersion() Or bIsOpenXml Then
                oTargetRng.WholeStory()
                oConvert.AddTagsToBookmarkedRange(oTargetRng, True)
            End If

            'delete content controls if necessary
            If bIsOpenXml Then
                oTargetRng.WholeStory()
                oConvert.DeleteContentControlsInRange(oTargetRng, False)
            End If

            'get table if necessary
            If oTable Is Nothing Then _
                    oTable = oLocation.Tables(1)

            'delete extra paragraph mark
            '    If Not bTrailingParaIsEmpty Then
            '        If GlobalMethods.CurWordApp.Version = "11.0" Then
            'GLOG 4144: Delete extra paragraph that was added
            oTable.Range.Next(WdUnits.wdCharacter).Delete()
            '        Else
            '            'in 2007, there's no harm deleting entire paragraph and this will
            '            'cover up a former bug in the app whereby pleading table items were
            '            'saved with an extra character
            '            With oTable.Range.Next(WdUnits.wdParagraph)
            '                '2/10/09 - make sure that not deleting segment end tag that is outside the
            '                'table because the segment, e.g. our old generic LSIG, was created before we
            '                'genericized collection functionality
            '                If .XMLNodes.Count = 0 Then
            '                    .Delete
            '                End If
            '            End With
            '        End If
            '    Else
            '        'restore style
            '        oTable.Range.Next(WdUnits.wdParagraph).Style = oStyle
            '    End If

            '3/27/12 (dm) - we now support explicit targeting of newly inserted, non full
            'row item - in this case, the target row will be the last
            If (lTargetColumn > 0) And (lTargetRow = 0) Then _
                lTargetRow = oTable.Rows.Count

            'move newly inserted rows to target location
            If lTargetRow > 0 Then
                oDoc = oLocation.Document

                'store clipboard contents
                Clipboard.SaveClipboard()

                If lTargetColumn > 0 Then
                    'show tags
                    bShowXML = oDoc.ActiveWindow.View.ShowXMLMarkup
                    If Not bShowXML Then _
                        GlobalMethods.SetXMLMarkupState(oDoc, True)

                    'move just cell contents - locate first non-empty cell
                    oRow = oTable.Rows.Last
                    For i = 1 To oRow.Cells.Count
                        If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                            oCellRange = oRow.Cells(i).Range
                            Exit For
                        End If
                    Next i

                    'shrink new item to cell
                    oCellRange.MoveEnd(WdUnits.wdCharacter, -1)

                    '3/27/12 (dm) - validate to ensure that we're shrinking the correct mSEG
                    For i = 1 To oCellRange.XMLNodes.Count
                        oTag = oCellRange.XMLNodes(i)
                        xObjectData = oTag.SelectSingleNode("@ObjectData").NodeValue
                        xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                        If xTypeID = CStr(iItemTypeID) Then _
                            Exit For
                    Next i

                    BaseMethods.MoveWordTag(oTag, oCellRange)

                    '3/27/12 (dm) - move only if item is not already in the correct cell
                    bMove = (oCellRange.Cells(1).RowIndex <> lTargetRow) Or _
                        (oCellRange.Cells(1).ColumnIndex <> lTargetColumn)
                    If bMove Then
                        'shrink existing item in target row to cell if necessary
                        'GLOG : 7705 : ceh
                        'Set oWordDoc = New WordDoc
                        oLocation = oTable.Cell(lTargetRow, lTargetColumn).Range
                        lLoc = BaseMethods.GetTagSafeParagraphStart(oLocation)
                        oLocation.SetRange(lLoc, oLocation.End)
                        oLocation.StartOf()
                        oSiblingTag = oWordDoc.GetParentSegmentTag(oLocation)
                        xTypeID = BaseMethods.GetmSEGObjectDataValue(oSiblingTag, "ObjectTypeID")
                        If xTypeID = CStr(iItemTypeID) Then
                            'target cell is inside another pleading table item
                            oRow = oTable.Rows(lTargetRow)
                            For i = 1 To oRow.Cells.Count
                                If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                                    oCellRange2 = oRow.Cells(i).Range
                                    Exit For
                                End If
                            Next i
                            BaseMethods.MoveWordTag(oSiblingTag, oCellRange2)
                        Else
                            oSiblingTag = Nothing
                        End If

                        'copy cell contents to target location
                        oCellRange = oTag.Range
                        oCellRange.MoveStart(WdUnits.wdCharacter, -1)
                        oCellRange.MoveEnd()
                        oCellRange.Cut()
                        oLocation.Paste()

                        'delete leading/trailing paragraph marks
                        With oLocation.Characters.First
                            If .Text = vbCr Then _
                                .Delete()
                        End With
                        With oLocation.Characters.Last
                            If .Text = vbCr Then _
                                .Delete()
                        End With

                        'delete inserted rows
                        If lTargetRow < oTable.Rows.Count Then
                            oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, _
                                oTable.Range.End)
                            oRange.Rows.Delete()
                        Else
                            'clear original cell
                            oCellRange.Expand(WdUnits.wdCell)
                            oCellRange.Text = ""
                        End If
                    End If

                    'GLOG : 6366 : ceh - Equilize regardless (moved from if Block above)
                    'equalize cell widths
                    EqualizeCellWidths(oTable.Rows(lTargetRow))

                    'restore new segment bookmark
                    oDoc.Bookmarks.Add("mpNewSegment", oDoc.Range(oLocation.Start + 1, _
                        oLocation.Start + 1))

                    'hide tags
                    If Not bShowXML Then _
                        GlobalMethods.SetXMLMarkupState(oDoc, False)
                Else
                    'move entire row(s) - set target location
                    oLocation = oTable.Rows(lTargetRow).Range
                    oLocation.StartOf()

                    'get newly inserted rows
                    oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, oTable.Range.End)

                    'move new rows to target location
                    oRange.Cut()
                    oLocation.Paste()
                End If

                'restore clipboard contents
                Clipboard.RestoreClipboard()
            End If

            'if pleading table item is being added to an existing pleading table,
            'create new collection tag that encompasses the entire table
            If xAttributes(0, 0) <> "" Then
                'create a new tag with the same attributes as the old one
                On Error GoTo TryNodeInsertionAgain
                oTag = oTable.Range.XMLNodes.Add("mSEG", GlobalMethods.mpNamespace)
                On Error GoTo ProcError

                'in Word 11, oTag may now refer to a wrapped tag, not the wrapper;
                'reset if necessary
                While (oTag.BaseName <> "mSEG") Or (oTag.Attributes.Count > 0)
                    oTag = oTag.ParentNode
                End While

                'add attributes
                For i = 0 To UBound(xAttributes)
                    oAttribute = oTag.Attributes.Add(xAttributes(i, 0), "")
                    oAttribute.NodeValue = xAttributes(i, 1)
                Next i

                'update tags collection
                oTags.Update(oTag)

                'update sibling tag if necessary
                If Not oSiblingTag Is Nothing Then _
                    oTags.Update(oSiblingTag)

                'return new tag
                oCollectionTag = oTag
            End If

            'GLOG item #5855 - dcf
            'delete temp bookmark and range if one was created above
            With oLocation.Document.Bookmarks
                If .Exists("zzmpTempPara") Then
                    .Item("zzmpTempPara").Range.Delete()
                End If
            End With

            Exit Sub
TryNodeInsertionAgain:
            MsgBox(Err.Number & ": " & Err.Description)
            'try the node insertion a second time -
            'the second time, inexplicably, always works
            If iAttempts = 0 Then
                iAttempts = iAttempts + 1
                Resume
            Else
                GlobalMethods.RaiseError(mpThisFunction)
                Exit Sub
            End If
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Sub InsertCollectionTableItem_CC(ByVal xXML As String, ByVal oLocation As Word.Range, _
                ByVal bAtCurrentRow As Boolean, ByVal iScope As mpCollectionTableItemScopes, _
                ByVal iItemTypeID As Short, ByVal iCollectionTypeID As Short, _
                ByRef oCollectionCC As Word.ContentControl, ByRef oTags As Tags)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.InsertCollectionTableItem_CC"
            Dim oTable As Word.Table
            Dim oCC As Word.ContentControl
            Dim oRange As Word.Range
            Dim xObjectData As String
            Dim xTypeID As String
            Dim xName As String
            Dim i As Short
            Dim iAttempts As Short
            Dim lTargetRow As Integer
            Dim iRows As Short
            Dim oDoc As Word.Document
            Dim bTableContainsParentCC
            Dim xText As String
            Dim lTargetColumn As Integer
            Dim iStart As Short
            Dim iEnd As Short
            Dim j As Short
            Dim oRow As Word.Row
            Dim oCellRange As Word.Range
            Dim oCellRange2 As Word.Range
            Dim lLoc As Integer
            Dim oWordDoc As WordDoc
            Dim oSiblingCC As Word.ContentControl
            Dim bTrailingParaIsEmpty As Boolean
            Dim oTestRange As Word.Range
            Dim oStyle As Word.Style
            Dim oNextParaRange As Word.Range
            Dim oCell As Word.Cell
            Dim xCTag As String
            Dim oTestCC As Word.ContentControl
            Dim bFound As Boolean
            Dim oConvert As Conversion
            Dim oTargetRng As Word.Range
            Dim bIsOpenXml As Boolean
            Dim bMove As Boolean

            On Error GoTo ProcError

            oConvert = New Conversion
            'GLOG : 7705 : ceh
            oWordDoc = New WordDoc

            If oCollectionCC Is Nothing Then
                'get selected table
                With oLocation
                    If .Information(Word.wdInformation.wdWithInTable) Then
                        oTable = .Tables(1)
                    ElseIf .Start <> 0 Then
                        If .Previous(WdUnits.wdCharacter).Information(Word.wdInformation.wdWithInTable) Then _
                                oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                    End If
                End With

                'check whether selected table is wrapped by a collection tag
                'of the specified type
                If Not oTable Is Nothing Then
                    oCollectionCC = GetCollectionTableContentControl(oLocation, iCollectionTypeID)
                    If oCollectionCC Is Nothing Then
                        'raise error
                        Err.Raise(mpErrors.mpError_CannotInsertTableIntoTable, , _
                            "<Error_CannotInsertTableIntoTable>")
                    End If
                End If
            Else
                'get existing collection table
                oTable = oCollectionCC.Range.Tables(1)
            End If

            'store attributes of existing collection tag in an array
            'BaseMethods.GetAttributesArray oCollectionTag, xAttributes()

            'delete existing collection tag
            If Not oCollectionCC Is Nothing Then
                'Save current Tag from Content Control
                xCTag = oCollectionCC.Tag
                oCollectionCC.Delete()
            End If

            'set insertion location to existing table
            If Not oTable Is Nothing Then
                'GLOG 4144: Rather than trying to determine whether or not an extra paragraph
                'has been inserted with XML that needs to be cleaned up afterward, we can take
                'control of this by always inserting an extra empty paragraph first, then deleting it afterward

                'if table is followed by an empty paragraph, we don't want to remove
                'the trailing paragraph inserted with the xml below
                '        Set oNextParaRange = oTable.Range.Next(WdUnits.wdParagraph)
                '        If oNextParaRange.Text = vbCr Then
                '            'exclude the final paragraph mark in the document
                '            On Error Resume Next
                '            Set oTestRange = oNextParaRange.Next(WdUnits.wdParagraph)
                '            On Error GoTo ProcError
                '            bTrailingParaIsEmpty = (Not oTestRange Is Nothing)
                '
                '            'preserve style
                '            If bTrailingParaIsEmpty Then _
                '                Set oStyle = oNextParaRange.Style
                '        End If

                'get number of existing rows
                iRows = oTable.Rows.Count

                'if target table is selected and insertion at current row
                'is specified, get current row
                If bAtCurrentRow Then
                    With oLocation
                        'GLOG 4433 (dm) - when recreating the last item in the table,
                        'oLocation may be just after the table, causing the old
                        'conditional to err - test for existence of cell
                        '                If .Cells.Count > 0 Then
                        'GLOG : 8115 : ceh - necessary for side-by-side in replacement mode
                        'because in that case, location is at end of table & cells.Count = 0
                        '                On Error Resume Next
                        '                Set oCell = .Cells(1)
                        '                On Error GoTo ProcError
                        '                If Not oCell Is Nothing Then
                        '                    If .Tables(1).Range.Start = oTable.Range.Start Then
                        '                        lTargetRow = oCell.RowIndex
                        '                    End If
                        '                End If
                        On Error Resume Next
                        lTargetRow = .Information(WdInformation.wdEndOfRangeRowNumber)
                        On Error GoTo ProcError
                    End With
                End If

                '        'if insertion in empty existing cell is specified, get target cell
                '        If bInEmptyExistingCell Then
                '            If bAtCurrentRow Then
                '                'search only target row
                '                iStart = lTargetRow
                '                iEnd = lTargetRow
                '            Else
                '                'search entire table
                '                iStart = 1
                '                iEnd = iRows
                '            End If
                '
                '            'locate first empty cell
                '            For i = iStart To iEnd
                '                Set oRow = oTable.Rows(i)
                '                For j = 1 To oRow.Cells.Count
                '                    If BaseMethods.CellIsEmpty(oRow.Cells(j)) Then
                '                        lTargetRow = i
                '                        lTargetColumn = j
                '                        Exit For
                '                    End If
                '                Next j
                '
                '                If lTargetColumn > 0 Then _
                '                    Exit For
                '            Next i
                '        End If

                If iScope = mpCollectionTableItemScopes.LeftCell Then
                    lTargetColumn = 1
                ElseIf iScope = mpCollectionTableItemScopes.RightCell Then
                    lTargetColumn = 2
                End If

                'reset range to target table
                oLocation = oTable.Range

                If BaseMethods.TableIsEmpty(oTable) Then
                    '1/27/11 (dm) - TableIsEmpty() will miss signatures
                    'containing nothing but a line
                    For Each oTestCC In oTable.Range.ContentControls
                        If BaseMethods.GetBaseName(oTestCC) = "mSEG" Then
                            xObjectData = BaseMethods.GetAttributeValue(oTestCC.Tag, "ObjectData")
                            If BaseMethods.IsCollectionTableItem(xObjectData) Then
                                bFound = True
                                Exit For
                            End If
                        End If
                    Next oTestCC

                    If Not bFound Then
                        'table is empty
                        If oTable.Range.ContentControls.Count > 0 Then
                            bTableContainsParentCC = (BaseMethods.GetBaseName(oTable.Range.ContentControls(1)) = "mSEG")
                        End If
                        'table is empty - delete it
                        oTable.Delete()
                        oTable = Nothing
                        lTargetRow = 0
                        'If table contained top-level mSeg, opening tag might have moved below where table was previously
                        'GLOG 5267 (dm) - this was never implemented on cc side
                        If bTableContainsParentCC Then
                            oLocation.MoveEnd(WdUnits.wdCharacter, 1)
                            If (oLocation.ContentControls.Count = 1) Then
                                If BaseMethods.GetBaseName(oLocation.ContentControls(1)) = "mSEG" Then
                                    ' Make sure insertion is within mSEG
                                    oLocation.Collapse(WdCollapseDirection.wdCollapseEnd)
                                Else
                                    oLocation.Collapse(WdCollapseDirection.wdCollapseStart)
                                End If
                            End If
                        End If
                    Else
                        'table is not empty
                        oLocation.EndOf()
                    End If
                Else
                    'table is not empty - move to end
                    oLocation.EndOf()
                End If
                'GLOG 7724: Insert paragraph after existing table
                'before inserting XML to avoid layout problem caused by KB2880529
                If Not oTable Is Nothing Then
                    oLocation.InsertBefore(vbCr)
                    oLocation.Collapse(WdCollapseDirection.wdCollapseEnd)
                End If
            End If

            'If GlobalMethods.CurWordApp.Version = "11.0" Then
            ' In Word 2003, InsertXML will zap any XMLNodes directly following
            ' range, so add an empty paragraph to preserve these
            'If oLocation.Paragraphs(1).Range.XMLNodes.Count > 0 Then
            'GLOG 4144: Always insert an extra paragraph for InsertXML
            oLocation.InsertBefore(vbCr)
            oLocation.Collapse(WdCollapseDirection.wdCollapseStart)
            'End If
            'End If

            'delete existing bookmarks in range (12/9/10)
            bIsOpenXml = BaseMethods.IsWordOpenXml(xXML)
            oTargetRng = oLocation.Duplicate
            If Not bIsOpenXml Then
                oTargetRng.WholeStory()
                'GLOG 7444 (dm) - set bSkipmSEGs to true for 10.5
                oConvert.DeleteMPContentBookmarks(oTargetRng, True, True)
            End If

            'GLOG 7815: Select range to ensure proper column widths
            oLocation.Select()

            'insert xml
            oLocation.InsertXML(xXML)

            'GLOG : 7705 : ceh
            'check for existing, unconverted collection tables
            If (Not oTable Is Nothing) And oLocation.Tables.Count Then
                With oLocation.Tables(1)
                    If (.LeftPadding = 0 And .RightPadding = 0) Then
                        'convert original document table if necessary
                        oWordDoc.AdjustTableMargins(oTable.Range)
                    End If
                End With
            End If

            'GLOG 7724:  Delete intervening paragraph to merge tables
            If Not oTable Is Nothing Then
                oLocation.Previous(WdUnits.wdParagraph, 1).Delete()
            End If

            'replace tags with content controls (dm 2/8/11)
            If Not bIsOpenXml Then
                'delete tags
                If Not BaseMethods.IsPostInjunctionWordVersion() Then
                    With oLocation.Document.XMLSchemaReferences
                        .Item(GlobalMethods.mpNamespace).Delete()
                        .Add(GlobalMethods.mpNamespace)
                    End With
                End If

                'add content controls
                oTargetRng.WholeStory()
                oConvert.AddContentControlsToBookmarkedRange(oTargetRng, True)
            End If

            'get table if necessary
            If oTable Is Nothing Then _
                    oTable = oLocation.Tables(1)

            'delete extra paragraph mark
            '    If Not bTrailingParaIsEmpty Then
            '        If GlobalMethods.CurWordApp.Version = "11.0" Then
            'GLOG 4144: Delete extra paragraph that was added
            oTable.Range.Next(WdUnits.wdCharacter).Delete()
            '        Else
            '            'in 2007, there's no harm deleting entire paragraph and this will
            '            'cover up a former bug in the app whereby pleading table items were
            '            'saved with an extra character
            '            With oTable.Range.Next(WdUnits.wdParagraph)
            '                '2/10/09 - make sure that not deleting segment end tag that is outside the
            '                'table because the segment, e.g. our old generic LSIG, was created before we
            '                'genericized collection functionality
            '                If .XMLNodes.Count = 0 Then
            '                    .Delete
            '                End If
            '            End With
            '        End If
            '    Else
            '        'restore style
            '        oTable.Range.Next(WdUnits.wdParagraph).Style = oStyle
            '    End If

            '3/27/12 (dm) - we now support explicit targeting of newly inserted, non full
            'row item - in this case, the target row will be the last
            If (lTargetColumn > 0) And (lTargetRow = 0) Then _
                lTargetRow = oTable.Rows.Count

            'move newly inserted rows to target location
            If lTargetRow > 0 Then
                oDoc = oLocation.Document

                'store clipboard contents
                Clipboard.SaveClipboard()

                If lTargetColumn > 0 Then
                    'move just cell contents - locate first non-empty cell
                    oRow = oTable.Rows.Last
                    For i = 1 To oRow.Cells.Count
                        If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                            oCellRange = oRow.Cells(i).Range
                            Exit For
                        End If
                    Next i

                    'shrink new item to cell
                    oCellRange.MoveEnd(WdUnits.wdCharacter, -1)

                    '3/27/12 (dm) - validate to ensure that we're shrinking the correct mSEG
                    For i = 1 To oCellRange.ContentControls.Count
                        oCC = oCellRange.ContentControls(i)
                        xObjectData = BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData", oLocation.Document)
                        xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                        If xTypeID = CStr(iItemTypeID) Then _
                            Exit For
                    Next i

                    BaseMethods.MoveContentControl(oCC, oCellRange)

                    '3/27/12 (dm) - move only if item is not already in the correct cell
                    bMove = (oCellRange.Cells(1).RowIndex <> lTargetRow) Or _
                        (oCellRange.Cells(1).ColumnIndex <> lTargetColumn)
                    If bMove Then
                        'shrink existing item in target row to cell if necessary
                        'GLOG : 7705 : ceh
                        'Set oWordDoc = New WordDoc
                        oLocation = oTable.Cell(lTargetRow, lTargetColumn).Range
                        lLoc = BaseMethods.GetCCSafeParagraphStart(oLocation)
                        oLocation.SetRange(lLoc, oLocation.End)
                        'GLOG 5580: Existing Content Control may span both columns
                        If oLocation.ContentControls.Count > 0 Then
                            oSiblingCC = oLocation.ContentControls(1)
                        Else
                            oSiblingCC = oWordDoc.GetParentSegmentContentControl(oLocation)
                        End If
                        oLocation.StartOf()
                        'GLOG 5580: Need to get ObjectData of Sibling Content Control here
                        xObjectData = BaseMethods.GetAttributeValue(oSiblingCC.Tag, "ObjectData", oLocation.Document)
                        xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                        If xTypeID = CStr(iItemTypeID) Then
                            'target cell is inside another pleading table item
                            oRow = oTable.Rows(lTargetRow)
                            For i = 1 To oRow.Cells.Count
                                If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                                    oCellRange2 = oRow.Cells(i).Range
                                    Exit For
                                End If
                            Next i
                            BaseMethods.MoveContentControl(oSiblingCC, oCellRange2)
                        Else
                            oSiblingCC = Nothing
                        End If

                        'copy cell contents to target location
                        oCellRange = oCC.Range
                        oCellRange.MoveStart(WdUnits.wdCharacter, -1)
                        oCellRange.MoveEnd()
                        oCellRange.Cut()
                        oLocation.Paste()

                        'delete leading/trailing paragraph marks
                        With oLocation.Characters.First
                            If .Text = vbCr Then _
                                .Delete()
                        End With
                        With oLocation.Characters.Last
                            If .Text = vbCr Then _
                                .Delete()
                        End With

                        'delete inserted rows
                        If lTargetRow < oTable.Rows.Count Then
                            oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, _
                                oTable.Range.End)
                            oRange.Rows.Delete()
                        Else
                            'clear original cell
                            oCellRange.Expand(WdUnits.wdCell)
                            oCellRange.Text = ""
                        End If
                    End If

                    'GLOG : 6366 : ceh - Equilize regardless (moved from if Block above)
                    'equalize cell widths
                    EqualizeCellWidths(oTable.Rows(lTargetRow))

                    'restore new segment bookmark
                    oDoc.Bookmarks.Add("mpNewSegment", oDoc.Range(oLocation.Start + 1, _
                        oLocation.Start + 1))
                Else
                    'move entire row(s) - set target location
                    oLocation = oTable.Rows(lTargetRow).Range
                    oLocation.StartOf()

                    'get newly inserted rows
                    oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, oTable.Range.End)

                    'move new rows to target location
                    oRange.Cut()
                    oLocation.Paste()
                End If

                'restore clipboard contents
                Clipboard.RestoreClipboard()
            End If

            If xCTag <> "" Then
                oCollectionCC = oTable.Range.ContentControls.Add
                oCollectionCC.Tag = xCTag

                'GLOG 4948 (dm) - update tags collection
                oTags.Update_CC(oCollectionCC)

                'GLOG 4948 (dm) - update sibling tag if necessary
                If Not oSiblingCC Is Nothing Then _
                    oTags.Update_CC(oSiblingCC) 'GLOG 5580
            End If

            Exit Sub
TryNodeInsertionAgain:
            MsgBox(Err.Number & ": " & Err.Description)
            'try the node insertion a second time -
            'the second time, inexplicably, always works
            If iAttempts = 0 Then
                iAttempts = iAttempts + 1
                Resume
            Else
                GlobalMethods.RaiseError(mpThisFunction)
                Exit Sub
            End If
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub InsertCollectionTableItem_Bmk(ByVal xXML As String, ByVal oLocation As Word.Range, _
                ByVal bAtCurrentRow As Boolean, ByVal iScope As mpCollectionTableItemScopes, _
                ByVal iItemTypeID As Short, ByVal iCollectionTypeID As Short, _
                ByRef oCollectionBmk As Word.Bookmark, ByRef oTags As Tags)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.InsertCollectionTableItem_Bmk"
            Dim oTable As Word.Table
            Dim oBmk As Word.Bookmark
            Dim oRange As Word.Range
            Dim xObjectData As String
            Dim xTypeID As String
            Dim xName As String
            Dim i As Short
            Dim iAttempts As Short
            Dim lTargetRow As Integer
            Dim iRows As Short
            Dim oDoc As Word.Document
            Dim bTableContainsParentBmk As Boolean
            Dim xText As String
            Dim lTargetColumn As Integer
            Dim iStart As Short
            Dim iEnd As Short
            Dim j As Short
            Dim oRow As Word.Row
            Dim oCellRange As Word.Range
            Dim oCellRange2 As Word.Range
            Dim lLoc As Integer
            Dim oWordDoc As WordDoc
            Dim oSiblingBmk As Word.Bookmark
            Dim bTrailingParaIsEmpty As Boolean
            Dim oTestRange As Word.Range
            Dim oStyle As Word.Style
            Dim oNextParaRange As Word.Range
            Dim oCell As Word.Cell
            Dim xCTag As String
            Dim oTestBmk As Word.Bookmark
            Dim bFound As Boolean
            Dim oConvert As Conversion
            Dim oTargetRng As Word.Range
            Dim bIsOpenXml As Boolean
            Dim bMove As Boolean
            Dim bShowXML As Boolean
            Dim bContentControls As Boolean
            Dim oParentSegmentBmk As Word.Bookmark
            Dim oBmkRng As Word.Range
            Dim sRightMargin As Single
            Dim oCC As Word.ContentControl 'GLOG 6789 (dm)
            Dim oTag As Word.XMLNode 'GLOG 6789 (dm)
            Dim bEmptyParaBelowTable As Boolean 'GLOG 7724
            Dim xTag As String 'GLOG 6789 (dm)
            Dim sWidth As Single
            Dim bTempParaBelowTable As Boolean 'GLOG 8131 (ceh)

            On Error GoTo ProcError

            oConvert = New Conversion
            bContentControls = BaseMethods.FileFormat(oLocation.Document) = mpFileFormats.OpenXml
            oWordDoc = New WordDoc
            If oCollectionBmk Is Nothing Then
                'get selected table
                With oLocation
                    If .Information(Word.wdInformation.wdWithInTable) Then
                        oTable = .Tables(1)
                    ElseIf .Start <> 0 Then
                        If .Previous(WdUnits.wdCharacter).Information(Word.wdInformation.wdWithInTable) Then _
                                oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                    End If
                End With

                'check whether selected table is wrapped by a collection tag
                'of the specified type
                If Not oTable Is Nothing Then
                    oCollectionBmk = GetCollectionTableBookmark(oLocation, iCollectionTypeID)
                    If oCollectionBmk Is Nothing Then
                        'raise error
                        Err.Raise(mpErrors.mpError_CannotInsertTableIntoTable, , _
                            "<Error_CannotInsertTableIntoTable>")
                    End If
                End If
            Else
                'get existing collection table
                On Error Resume Next
                oTable = oCollectionBmk.Range.Tables(1)
                On Error GoTo ProcError
            End If

            'store attributes of existing collection tag in an array
            'BaseMethods.GetAttributesArray oCollectionTag, xAttributes()

            'delete existing collection bookmark
            If Not oCollectionBmk Is Nothing Then
                'GLOG 6803
                oParentSegmentBmk = oWordDoc.GetParentSegmentBookmark(oCollectionBmk)
                'Save current Tag from bookmark
                xCTag = Mid(oCollectionBmk.Name, 2)
                oCollectionBmk.Delete()
            Else
                oParentSegmentBmk = oWordDoc.GetTopLevelSegmentBookmark(oLocation)
            End If
            'set insertion location to existing table
            If Not oTable Is Nothing Then
                'get number of existing rows
                iRows = oTable.Rows.Count

                'if target table is selected and insertion at current row
                'is specified, get current row
                If bAtCurrentRow Then
                    With oLocation
                        'GLOG 4433 (dm) - when recreating the last item in the table,
                        'oLocation may be just after the table, causing the old
                        'conditional to err - test for existence of cell
                        '                If .Cells.Count > 0 Then
                        'GLOG 6789 (dm) - if row is available, use it
                        'GLOG 6789 (dm) - remmed 2/13/14 - this change turned out to be
                        'unnecessary for side-by-side and problematic for chooser replacement
                        '                On Error Resume Next
                        '                Set oRow = .Rows(1)
                        '                On Error GoTo ProcError
                        '                If Not oRow Is Nothing Then
                        '                    lTargetRow = oRow.Index
                        '                Else
                        'GLOG : 8115 : ceh - necessary for side-by-side in replacement mode
                        'because in that case, location is at end of table & cells.Count = 0
                        '                    On Error Resume Next
                        '                    Set oCell = .Cells(1)
                        '                    On Error GoTo ProcError
                        '                    If Not oCell Is Nothing Then
                        '                        If .Tables(1).Range.Start = oTable.Range.Start Then
                        '                            lTargetRow = oCell.RowIndex
                        '                        End If
                        '                    End If
                        On Error Resume Next
                        lTargetRow = .Information(WdInformation.wdEndOfRangeRowNumber)
                        On Error GoTo ProcError
                        '                End If
                    End With
                End If

                If iScope = mpCollectionTableItemScopes.LeftCell Then
                    lTargetColumn = 1
                ElseIf iScope = mpCollectionTableItemScopes.RightCell Then
                    lTargetColumn = 2
                End If

                'reset range to target table
                oLocation = oTable.Range
                'GLOG 8722
                Dim bShowHidden As Boolean = oTable.Range.Bookmarks.ShowHidden
                If Not bShowHidden Then
                    oTable.Range.Bookmarks.ShowHidden = True
                End If
                If BaseMethods.TableIsEmpty(oTable) Then
                    '1/27/11 (dm) - TableIsEmpty() will miss signatures
                    'containing nothing but a line
                    For Each oTestBmk In oTable.Range.Bookmarks
                        If BaseMethods.GetBaseNameFromTag(oTestBmk.Name) = "mSEG" Then
                            xObjectData = BaseMethods.GetAttributeValue(Mid$(oTestBmk.Name, 2), "ObjectData")
                            If BaseMethods.IsCollectionTableItem(xObjectData) Then
                                bFound = True
                                Exit For
                            End If
                        End If
                    Next oTestBmk

                    If Not bFound Then
                        'table is empty
                        If oTable.Range.Bookmarks.Count > 0 Then
                            bTableContainsParentBmk = (BaseMethods.GetBaseNameFromTag(oTable.Range.Bookmarks(1).Name) = "mSEG")
                        End If
                        'table is empty - delete it
                        oTable.Delete()
                        oTable = Nothing
                        lTargetRow = 0
                        iRows = 0 'GLOG 7991: Make sure all table rows will get tagged if XML Tags are used
                        'If table contained top-level mSeg, opening tag might have moved below where table was previously
                        'GLOG 5267 (dm) - this was never implemented on cc side -
                        'mp11 - this caused problems with the correct reinsertion location when using
                        'bookmarks as bounding objects - likely this is only needed with ccs
                        If bTableContainsParentBmk Then
                            'Debug.Assert False
                            '                    oLocation.MoveEnd WdUnits.wdCharacter, 1
                            '                    If (oLocation.Bookmarks.Count = 1) Then
                            '                        If BaseMethods.GetBaseNameFromTag(oLocation.Bookmarks(1)) = "mSEG" Then
                            '                            ' Make sure insertion is within mSEG
                            '                            oLocation.Collapse wdCollapseDirection.wdCollapseEnd
                            '                        Else
                            '                            oLocation.Collapse wdCollapseDirection.wdCollapseStart
                            '                        End If
                            '                    End If
                        End If
                        'GLOG : 8131 : ceh
                        'add temp paragraph so segment doesn't end up
                        'in first cell of following table
                        Dim iTblCount As Integer = 0
                        On Error Resume Next
                        'GLOG 8741: Avoid error if at end of document
                        iTblCount = oLocation.Next.Tables.Count
                        On Error GoTo ProcError
                        If iTblCount > 0 Then
                            oLocation.InsertAfter(Chr(13))
                            bTempParaBelowTable = True
                            oLocation.StartOf()
                        End If

                    Else
                        'table is not empty
                        oLocation.EndOf()
                    End If
                Else
                    'table is not empty - move to end
                    oLocation.EndOf()
                End If
                'GLOG 7724: Insert paragraph after existing table
                'before inserting XML to avoid layout problem caused by KB2880529
                If Not oTable Is Nothing Then
                    oTable.Range.Bookmarks.ShowHidden = bShowHidden
                    oLocation.InsertBefore(vbCr)
                    oLocation.Collapse(WdCollapseDirection.wdCollapseEnd)
                End If
            End If

            'GLOG 7724: Check if original table is followed by an empty para
            If oLocation.Paragraphs(1).Range.Text = Chr(13) Then
                bEmptyParaBelowTable = True
            Else
                bEmptyParaBelowTable = False
            End If
            'mp11 - this is adding extra paras now that we don't require
            'a trailing paragraph mark in mp11
            'GLOG 4144: Always insert an extra paragraph for InsertXML
            '    On Error Resume Next
            '    oLocation.InsertBefore chr(13)
            '    On Error GoTo ProcError
            '    oLocation.Collapse wdCollapseDirection.wdCollapseStart


            'delete existing bookmarks in range (12/9/10)
            bIsOpenXml = BaseMethods.IsWordOpenXML(xXML)
            oTargetRng = oLocation.Duplicate
            If Not bContentControls And (BaseMethods.IsPostInjunctionWordVersion() Or bIsOpenXml) Then
                oTargetRng.WholeStory()
                'GLOG 7070 (dm) - set bSkipmSEGs to true for 10.5
                oConvert.DeleteMPContentBookmarks(oTargetRng, True, True)
            ElseIf bContentControls And Not bIsOpenXml Then
                oTargetRng.WholeStory()
                'GLOG 7070 (dm) - set bSkipmSEGs to true for 10.5
                oConvert.DeleteMPContentBookmarks(oTargetRng, True, True)
            End If

            '140815 : ceh - there is no need for this now
            '    'GLOG : 7130/7131 : ceh - set right margin to 1" prior to inserting xml
            '    'only needed for Word 2013 or above
            '    If Val(GlobalMethods.CurWordApp.Version) >= 15 Then
            '        sRightMargin = oLocation.Sections(1).PageSetup.RightMargin
            '        oLocation.Sections(1).PageSetup.RightMargin = .Application.InchesToPoints(1)
            '    End If

            'GLOG 7815: Select range to ensure proper column widths
            oLocation.Select()

            'insert xml
            oWordDoc.InsertXMLAtLocation(oLocation, xXML)

            'GLOG : 7705 : ceh
            'check for existing, unconverted collection tables
            If (Not oTable Is Nothing) And oLocation.Tables.Count Then
                With oLocation.Tables(1)
                    If (.LeftPadding = 0 And .RightPadding = 0) Then
                        'convert original document table if necessary
                        oWordDoc.AdjustTableMargins(oTable.Range)
                    End If
                End With
            End If

            'GLOG 7724:  Delete intervening paragraph to merge tables
            If Not oTable Is Nothing Then
                oLocation.Previous(WdUnits.wdParagraph, 1).Delete()
            End If

            '140815 : ceh - there is no need for this now
            '    'GLOG : 7130/7131 : CEH - reset margin
            '    If Val(GlobalMethods.CurWordApp.Version) >= 15 Then
            '        oLocation.Sections(1).PageSetup.RightMargin = sRightMargin
            '    End If

            'get table if necessary
            If oTable Is Nothing Then _
                    oTable = oLocation.Tables(1)

            'GLOG : 8131 : ceh - delete temp paragraph
            'GLOG 7724: If there wasn't an empty paragraph following to start,
            'delete empty paragraph if it's there now
            If (Not bEmptyParaBelowTable Or bTempParaBelowTable) And (oTable.Range.Next(WdUnits.wdParagraph).Text = Chr(13)) Then
                oTable.Range.Next(WdUnits.wdParagraph).Delete()
            End If

            'GLOG 7704: This code was problematic when tables contained merged cells,
            'and does not appear to be necessary after GLOG 7724 fix
            '    'GLOG : 7566 : CEH - reset rightmost cell width
            '    'otherwise, new rows width won't match exactly
            '    With oTable.Rows
            '        'get first row's last cell width
            '        sWidth = .Item(1).Cells(.Item(1).Cells.Count).Width
            '        For i = 2 To .Count
            '            'reset last cell width
            '            .Item(i).Cells(.Item(i).Cells.Count).Width = sWidth
            '        Next i
            '    End With

            'Make sure CollectionTable is entirely contained within parent bookmark
            If Not oParentSegmentBmk Is Nothing Then 'GLOG 6803
                If oParentSegmentBmk.End <= oTable.Range.End Then
                    oBmkRng = oParentSegmentBmk.Range.Duplicate
                    oBmkRng.SetRange(oParentSegmentBmk.Start, oTable.Range.End + 1)
                    oParentSegmentBmk.Range.Bookmarks.Add(oParentSegmentBmk.Name, oBmkRng)
                End If
            End If
            If Not bContentControls Then
                oLocation.Document.ActiveWindow.View.ShowXMLMarkup = False
            End If

            'replace tags with content controls (dm 2/8/11)
            If bContentControls And Not bIsOpenXml Then
                'delete tags
                If Not BaseMethods.IsPostInjunctionWordVersion() Then
                    With oLocation.Document.XMLSchemaReferences
                        .Item(GlobalMethods.mpNamespace).Delete()
                        .Add(GlobalMethods.mpNamespace)
                    End With
                End If

                'add content controls
                oTargetRng.WholeStory()
                oConvert.AddContentControlsToBookmarkedRange(oTargetRng, True)
            ElseIf Not bContentControls And bIsOpenXml Then
                oTargetRng.WholeStory()
                oConvert.AddBookmarksToContentControls(oTargetRng)
            End If

            'add tags if necessary (12/9/10)
            If Not bContentControls And (BaseMethods.IsPostInjunctionWordVersion() Or bIsOpenXml) Then
                '1/10/13: Add Tags only to range of newly inserted item
                'Since mSEG tags no longer exist, adding to entire document will result in
                'Top-level Segment being marked as the New Segment
                If iRows < oTable.Rows.Count Then
                    'get newly inserted rows
                    'GLOG 6927: Avoid accessing oTable.Rows.Item in case there are merged cells
                    oTargetRng = oTargetRng.Document.Range(oTable.Cell(iRows + 1, 1).Range.Start, oTable.Range.End)
                Else
                    oTargetRng = oTable.Range
                End If
                oConvert.AddTagsToBookmarkedRange(oTargetRng, True)
            End If

            'delete content controls if necessary
            If Not bContentControls And bIsOpenXml Then
                oTargetRng.WholeStory()
                oConvert.DeleteContentControlsInRange(oTargetRng, False)
            End If

            'delete extra paragraph mark
            '    If Not bTrailingParaIsEmpty Then
            '        If GlobalMethods.CurWordApp.Version = "11.0" Then
            'GLOG 4144: Delete extra paragraph that was added
            'GLOG 7023 (dm) - remmed the following line -
            'in 10.5, there's no extra paragraph to be deleted
            '            oTable.Range.Next(WdUnits.wdCharacter).Delete
            '        Else
            '            'in 2007, there's no harm deleting entire paragraph and this will
            '            'cover up a former bug in the app whereby pleading table items were
            '            'saved with an extra character
            '            With oTable.Range.Next(WdUnits.wdParagraph)
            '                '2/10/09 - make sure that not deleting segment end tag that is outside the
            '                'table because the segment, e.g. our old generic LSIG, was created before we
            '                'genericized collection functionality
            '                If .XMLNodes.Count = 0 Then
            '                    .Delete
            '                End If
            '            End With
            '        End If
            '    Else
            '        'restore style
            '        oTable.Range.Next(WdUnits.wdParagraph).Style = oStyle
            '    End If

            '3/27/12 (dm) - we now support explicit targeting of newly inserted, non full
            'row item - in this case, the target row will be the last
            If (lTargetColumn > 0) And (lTargetRow = 0) Then _
                lTargetRow = oTable.Rows.Count

            'move newly inserted rows to target location
            If lTargetRow > 0 Then
                oDoc = oLocation.Document

                'store clipboard contents
                Clipboard.SaveClipboard()

                If lTargetColumn > 0 Then
                    If Not bContentControls Then
                        'show tags
                        bShowXML = oDoc.ActiveWindow.View.ShowXMLMarkup
                        If Not bShowXML Then _
                            GlobalMethods.SetXMLMarkupState(oDoc, True)
                    End If

                    'move just cell contents - locate first non-empty cell
                    oRow = oTable.Rows.Last
                    For i = 1 To oRow.Cells.Count
                        If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                            oCellRange = oRow.Cells(i).Range
                            Exit For
                        End If
                    Next i

                    'shrink new item to cell
                    oCellRange.MoveEnd(WdUnits.wdCharacter, -1)

                    If bContentControls Then
                        '3/27/12 (dm) - validate to ensure that we're shrinking the correct mSEG
                        For i = 1 To oCellRange.ContentControls.Count
                            'GLOG 6789 (dm) - the new item will still have mSEG bounding objects
                            oCC = oCellRange.ContentControls(i)
                            xTag = oCC.Tag
                            xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oLocation.Document)
                            '                    Set oBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oCC.Range)
                            '                    xObjectData = BaseMethods.GetAttributeValue(Mid$(oBmk.Name, 2), "ObjectData", oLocation.Document)
                            xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                            If xTypeID = CStr(iItemTypeID) Then _
                                Exit For
                        Next i
                        BaseMethods.MoveContentControl(oCC, oCellRange)
                        '                BaseMethods.MoveBookmark oBmk, oCellRange
                    Else
                        '3/27/12 (dm) - validate to ensure that we're shrinking the correct mSEG
                        For i = 1 To oCellRange.XMLNodes.Count
                            'GLOG 6789 (dm) - the new item will still have mSEG bounding objects
                            oTag = oCellRange.XMLNodes(i)
                            xTag = BaseMethods.GetTag(oTag)
                            xObjectData = oTag.SelectSingleNode("@ObjectData").NodeValue
                            '                    Set oBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oTag.Range)
                            '                    xObjectData = BaseMethods.GetAttributeValue(Mid$(oBmk.Name, 2), "ObjectData", oLocation.Document)
                            xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                            If xTypeID = CStr(iItemTypeID) Then _
                                Exit For
                        Next i

                        BaseMethods.MoveWordTag(oTag, oCellRange)
                        '                BaseMethods.MoveBookmark oBmk, oCellRange
                    End If

                    '3/27/12 (dm) - move only if item is not already in the correct cell
                    bMove = (oCellRange.Cells(1).RowIndex <> lTargetRow) Or _
                        (oCellRange.Cells(1).ColumnIndex <> lTargetColumn)
                    If bMove Then
                        'shrink existing item in target row to cell if necessary
                        oLocation = oTable.Cell(lTargetRow, lTargetColumn).Range
                        If bContentControls Then
                            lLoc = BaseMethods.GetCCSafeParagraphStart(oLocation)
                            oLocation.SetRange(lLoc, oLocation.End)
                            'GLOG 5580: Existing Content Control may span both columns
                            If oLocation.ContentControls.Count > 0 Then
                                oSiblingBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oLocation.ContentControls(1).Range)
                            Else
                                oSiblingBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oLocation)
                            End If
                            oLocation.StartOf()
                            'GLOG : 8103 : ceh
                            If Not (oSiblingBmk Is Nothing) Then
                                'GLOG 5580: Need to get ObjectData of Sibling Content Control here
                                xObjectData = BaseMethods.GetAttributeValue(Mid$(oSiblingBmk.Name, 2), "ObjectData", oLocation.Document)
                                xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                                If xTypeID = CStr(iItemTypeID) Then
                                    'target cell is inside another pleading table item
                                    oRow = oTable.Rows(lTargetRow)
                                    For i = 1 To oRow.Cells.Count
                                        If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                                            oCellRange2 = oRow.Cells(i).Range
                                            Exit For
                                        End If
                                    Next i
                                    BaseMethods.MoveBookmark(oSiblingBmk, oCellRange2)
                                Else
                                    oSiblingBmk = Nothing
                                End If
                            Else
                                oSiblingBmk = Nothing
                            End If
                        Else
                            lLoc = BaseMethods.GetTagSafeParagraphStart(oLocation)
                            oLocation.SetRange(lLoc, oLocation.End)
                            oLocation.StartOf()
                            oSiblingBmk = oWordDoc.GetParentSegmentBookmarkFromRange(oLocation)
                            xObjectData = BaseMethods.GetAttributeValue(Mid$(oSiblingBmk.Name, 2), "ObjectData", oLocation.Document)
                            xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                            If xTypeID = CStr(iItemTypeID) Then
                                'target cell is inside another pleading table item
                                oRow = oTable.Rows(lTargetRow)
                                For i = 1 To oRow.Cells.Count
                                    If Not BaseMethods.CellIsEmpty(oRow.Cells(i)) Then
                                        oCellRange2 = oRow.Cells(i).Range
                                        Exit For
                                    End If
                                Next i
                                BaseMethods.MoveBookmark(oSiblingBmk, oCellRange2)
                            Else
                                oSiblingBmk = Nothing
                            End If
                        End If
                        'copy cell contents to target location
                        'GLOG 6789 (dm) - the new item will still have mSEG bounding objects
                        If bContentControls Then
                            oCellRange = oCC.Range
                        Else
                            oCellRange = oTag.Range
                        End If

                        oCellRange.MoveStart(WdUnits.wdCharacter, -1)
                        oCellRange.MoveEnd()
                        oCellRange.Cut()
                        oLocation.Paste()

                        'delete leading/trailing paragraph marks
                        With oLocation.Characters.First
                            If .Text = vbCr Then _
                                .Delete()
                        End With
                        With oLocation.Characters.Last
                            If .Text = vbCr Then _
                                .Delete()
                        End With


                        'delete inserted rows
                        If lTargetRow < oTable.Rows.Count Then
                            oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, _
                                oTable.Range.End)
                            oRange.Rows.Delete()
                        Else
                            'clear original cell
                            oCellRange.Expand(WdUnits.wdCell)
                            oCellRange.Text = ""
                        End If
                    End If

                    'GLOG : 6366 : ceh - Equilize regardless (moved from if Block above)
                    'equalize cell widths
                    EqualizeCellWidths(oTable.Rows(lTargetRow))

                    'restore new segment bookmark
                    oDoc.Bookmarks.Add("mpNewSegment", oDoc.Range(oLocation.Start + 1, _
                        oLocation.Start + 1))
                    oDoc.Variables.Add("mpNewSegment", xTag)

                    'hide tags
                    If Not bContentControls And Not bShowXML Then _
                        GlobalMethods.SetXMLMarkupState(oDoc, False)
                Else
                    'move entire row(s) - set target location
                    oLocation = oTable.Rows(lTargetRow).Range
                    oLocation.StartOf()

                    'get newly inserted rows
                    oRange = oDoc.Range(oTable.Rows(iRows + 1).Range.Start, oTable.Range.End)

                    'move new rows to target location
                    oRange.Cut()
                    oLocation.Paste()

                    'GLOG 7547 (dm) - make sure table is still contained within parent bookmark
                    If (Not oParentSegmentBmk Is Nothing) And (lTargetRow = 1) Then
                        If oParentSegmentBmk.Start > oTable.Range.Start Then
                            oBmkRng = oParentSegmentBmk.Range.Duplicate
                            oBmkRng.SetRange(oTable.Range.Start, oParentSegmentBmk.End)
                            oParentSegmentBmk.Range.Bookmarks.Add(oParentSegmentBmk.Name, oBmkRng)
                        End If
                    End If
                End If

                'restore clipboard contents
                Clipboard.RestoreClipboard()
            End If

            If xCTag <> "" Then
                If oDoc Is Nothing Then
                    oDoc = oTable.Range.Document
                End If

                oCollectionBmk = oDoc.Bookmarks.Add("_" & xCTag, oTable.Range)

                'GLOG 4948 (dm) - update tags collection
                oTags.Update_Bmk(oCollectionBmk)

                'GLOG 4948 (dm) - update sibling tag if necessary
                If Not oSiblingBmk Is Nothing Then _
                    oTags.Update_Bmk(oSiblingBmk) 'GLOG 5580
            End If

            Exit Sub
TryNodeInsertionAgain:
            MsgBox(Err.Number & ": " & Err.Description)
            'try the node insertion a second time -
            'the second time, inexplicably, always works
            If iAttempts = 0 Then
                iAttempts = iAttempts + 1
                Resume
            Else
                GlobalMethods.RaiseError(mpThisFunction)
                Exit Sub
            End If
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetCollectionTableConfiguration(ByVal oCollectionTag As Word.XMLNode) As String
            'returns a delimited string representing the configuration of the table containing
            'oCollectionTag, in the format 'RowIndex�TagID�TagID|RowIndex�TagID�TagID|'
            'where '�' is the end of a cell, '|' is the end of a row, and TagID is the id
            'of a child mSEG of oCollectionTag
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.GetCollectionTableConfiguration"
            Dim oRange As Word.Range
            Dim oTable As Word.Table
            Dim iRow As Short
            Dim iColumn As Short
            Dim iCells As Short
            Dim i As Short
            Dim j As Short
            Dim xConfig As String
            Dim oRow As Word.Row
            Dim oTag As Word.XMLNode
            Dim iPos As Integer
            Dim iPos2 As Integer
            Dim xTagID As String

            On Error GoTo ProcError

            'get pleading table
            oTable = oCollectionTag.Range.Tables(1)

            'add cell and row markers to the config string
            For i = 1 To oTable.Rows.Count
                oRow = oTable.Rows(i)
                xConfig = xConfig & i & "�" & BaseMethods.CreatePadString(oRow.Cells.Count - 1, "�") & "|"
            Next i

            'strip trailing pipe
            xConfig = Left$(xConfig, Len(xConfig) - 1)

            'insert the tag ids of the pleading table items into the config string
            For i = 1 To oCollectionTag.ChildNodes.Count
                oTag = oCollectionTag.ChildNodes(i)
                If oTag.BaseName = "mSEG" Then
                    'get tag id and starting row and column
                    xTagID = oTag.SelectSingleNode("@TagID").NodeValue
                    oRange = oTag.Range
                    iRow = oRange.Cells(1).RowIndex
                    iColumn = oRange.Cells(1).ColumnIndex

                    'get cell count
                    oRange.EndOf()
                    iCells = oRange.Cells(1).ColumnIndex - iColumn + 1

                    'move to correct row
                    If iRow = 1 Then
                        iPos = 0
                    Else
                        iPos = BaseMethods.GetInstancePosition(xConfig, "|", iRow - 1)
                    End If

                    If iColumn = 1 Then
                        'move past row index
                        iPos = InStr(iPos + 1, xConfig, "�")
                    Else
                        'move to correct column
                        For j = 2 To iColumn
                            iPos = InStr(iPos + 1, xConfig, "�")
                        Next j
                    End If

                    'insert tag id in each cell that item spans
                    For j = 1 To iCells
                        xConfig = Left$(xConfig, iPos) + xTagID + Mid$(xConfig, iPos + 1)
                        iPos = iPos + Len(xTagID) + 1
                    Next j
                End If
            Next i

            GetCollectionTableConfiguration = xConfig

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub ReconfigureCollectionTable(ByRef oCollectionTag As Word.XMLNode, _
                                            ByVal xNewConfig As String)
            'reconfigures the table containing oCollectionTag as specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.ReconfigureCollectionTable"
            Dim oTable As Word.Table
            Dim oAttribute As Word.XMLNode
            Dim xAttributes(,) As String
            Dim xOldConfig As String
            Dim vOldRows As Object
            Dim vOldCells As Object
            Dim vOldCells2 As Object
            Dim vNewRows As Object
            Dim vNewCells As Object
            Dim i As Short
            Dim j As Short
            Dim k As Short
            Dim xOldRow As String
            Dim xNewRow As String
            Dim iPos As Integer
            Dim iRowIndex As Short
            Dim iColIndex As Short
            Dim oRange As Word.Range
            Dim xTagID As String
            Dim iRowCount As Short
            Dim iAttempts As Short
            Dim oTag As Word.XMLNode
            Dim xText As String

            On Error GoTo ProcError

            'get current configuration
            xOldConfig = Me.GetCollectionTableConfiguration(oCollectionTag)

            'exit if no changes needed
            If xNewConfig = xOldConfig Then _
                Exit Sub

            'store clipboard contents
            Clipboard.SaveClipboard()

            'get collection table
            oTable = oCollectionTag.Range.Tables(1)
            iRowCount = oTable.Rows.Count

            'store attributes of collection tag in an array
            BaseMethods.GetAttributesArray(oCollectionTag, xAttributes)

            'delete collection tag
            If Not oCollectionTag Is Nothing Then _
                oCollectionTag.Delete()

            'get arrays of rows
            vOldRows = Split(xOldConfig, "|")
            vNewRows = Split(xNewConfig, "|")

            'cycle through new array of rows
            For i = 0 To UBound(vNewRows)
                'get row
                xNewRow = vNewRows(i)

                'get old row index
                iPos = InStr(xNewRow, "�")
                iRowIndex = CLng(Left$(xNewRow, iPos - 1))

                'copy row to bottom of table
                oRange = oTable.Range
                oRange.EndOf()
                oTable.Rows(iRowIndex).Range.Copy()
                oRange.Paste()

                'determine whether contents of row have changed (if not, we're done)
                xOldRow = vOldRows(iRowIndex - 1)
                If xNewRow <> xOldRow Then
                    'get arrays of cells
                    vNewCells = Split(xNewRow, "�")
                    vOldCells = Split(xOldRow, "�")

                    'cycle through cells, comparing old to new
                    For j = 1 To 2
                        If vNewCells(j) <> vOldCells(j) Then
                            'delete cell contents
                            oTable.Rows.Last.Cells(j).Range.Text = ""

                            'strip row index to get tag id
                            xTagID = vNewCells(j)
                            iPos = InStr(xTagID, "�")
                            If iPos > 0 Then _
                                xTagID = Mid$(xTagID, iPos + 1)

                            If xTagID <> "" Then
                                'look for tag id in old config
                                For k = 0 To UBound(vOldRows)
                                    If InStr(vOldRows(k), xTagID) > 0 Then
                                        vOldCells2 = Split(vOldRows(k), "�")
                                        If InStr(vOldCells2(0), xTagID) > 0 Then
                                            iColIndex = 1
                                        Else
                                            iColIndex = 2
                                        End If

                                        'copy/paste cell contents
                                        oTable.Cell(iRowIndex, iColIndex).Range.Copy()
                                        oTable.Rows.Last.Cells(j).Range.Paste()

                                        Exit For
                                    End If
                                Next k
                            End If
                        End If
                    Next j
                End If
            Next i

            'delete old rows
            For i = 1 To iRowCount
                oTable.Rows.First.Delete()
            Next i

            'create a new collection tag with the same attributes as the old one
            On Error GoTo TryNodeInsertionAgain
            oTag = oTable.Range.XMLNodes.Add("mSEG", GlobalMethods.mpNamespace)
            On Error GoTo ProcError

            'in Word 11, oTag may now refer to a wrapped tag, not the wrapper;
            'reset if necessary
            While (oTag.BaseName <> "mSEG") Or (oTag.Attributes.Count > 0)
                oTag = oTag.ParentNode
            End While

            'add attributes
            For i = 0 To UBound(xAttributes)
                oAttribute = oTag.Attributes.Add(xAttributes(i, 0), "")
                oAttribute.NodeValue = xAttributes(i, 1)
            Next i

            'return new tag
            oCollectionTag = oTag

            'restore clipboard contents
            Clipboard.RestoreClipboard()

            Exit Sub
TryNodeInsertionAgain:
            'try the node insertion a second time -
            'the second time, inexplicably, always works
            If iAttempts = 0 Then
                iAttempts = iAttempts + 1
                Resume
            Else
                GlobalMethods.RaiseError(mpThisFunction)
                Exit Sub
            End If
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetCollectionTableTag(ByVal oLocation As Word.Range, _
                                            ByVal iCollectionTypeID As Short) As Word.XMLNode
            'returns the primary mSEG of the table that contains or is adjacent to oLocation,
            'verifying that it represents a segment of the specified type
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.GetCollectionTableTag"
            Dim oTable As Word.Table
            Dim oTag As Word.XMLNode
            Dim xTypeID As String

            On Error GoTo ProcError

            'look for table
            With oLocation
                .StartOf()
                If .Information(Word.wdInformation.wdWithInTable) Then
                    oTable = .Tables(1)
                ElseIf .Start <> 0 Then
                    If .Previous(WdUnits.wdCharacter).Information(Word.wdInformation.wdWithInTable) Then _
                            oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                End If
            End With

            'check whether selected table is wrapped by a collection tag
            'of the specified type
            If Not oTable Is Nothing Then
                For Each oTag In oTable.Range.XMLNodes
                    If oTag.BaseName = "mSEG" Then
                        'validate segment type id
                        xTypeID = BaseMethods.GetmSEGObjectDataValue(oTag, "ObjectTypeID")
                        If CInt(xTypeID) = iCollectionTypeID Then
                            GetCollectionTableTag = oTag
                            Exit For
                        End If
                    End If
                Next oTag
            End If

            Exit Function

ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function GetCollectionTableContentControl(ByVal oLocation As Word.Range, _
                                            ByVal iCollectionTypeID As Short) As Word.ContentControl
            'returns the primary mSEG of the table that contains or is adjacent to oLocation,
            'verifying that it represents a segment of the specified type
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.GetCollectionTableContentControl"
            Dim oTable As Word.Table
            Dim oCC As Word.ContentControl
            Dim xTypeID As String
            Dim xObjectData As String

            On Error GoTo ProcError

            'look for table
            With oLocation
                .StartOf()
                If .Information(Word.wdInformation.wdWithInTable) Then
                    oTable = .Tables(1)
                ElseIf .Start <> 0 Then
                    If .Previous(WdUnits.wdCharacter).Information(Word.wdInformation.wdWithInTable) Then _
                            oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                End If
            End With

            'check whether selected table is wrapped by a collection tag
            'of the specified type
            If Not oTable Is Nothing Then
                For Each oCC In oTable.Range.ContentControls
                    If BaseMethods.GetBaseName(oCC) = "mSEG" Then
                        'validate segment type id
                        xObjectData = BaseMethods.GetAttributeValue(oCC.Tag, "ObjectData", oLocation.Document)
                        xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                        If CInt(xTypeID) = iCollectionTypeID Then
                            GetCollectionTableContentControl = oCC
                            Exit For
                        End If
                    End If
                Next oCC
            End If

            Exit Function

ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function GetCollectionTableBookmark(ByVal oLocation As Word.Range, _
                                            ByVal iCollectionTypeID As Short) As Word.Bookmark
            'returns the primary mSEG of the table that contains or is adjacent to oLocation,
            'verifying that it represents a segment of the specified type
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.GetCollectionTableBookmark"
            Dim oTable As Word.Table
            Dim oBmk As Word.Bookmark
            Dim xTypeID As String
            Dim xObjectData As String
            Dim xTag As String

            On Error GoTo ProcError

            'look for table
            With oLocation
                .StartOf()
                If .Information(Word.wdInformation.wdWithInTable) Then
                    oTable = .Tables(1)
                ElseIf .Start <> 0 Then
                    If .Previous(WdUnits.wdCharacter).Information(Word.WdInformation.wdWithInTable) Then _
                            oTable = .Previous(WdUnits.wdCharacter).Tables(1)
                End If
            End With

            'check whether selected table is wrapped by a collection tag
            'of the specified type
            If Not oTable Is Nothing Then
                'GLOG 8722
                Dim bShowHidden As Boolean = oTable.Range.Bookmarks.ShowHidden
                If Not bShowHidden Then
                    oTable.Range.Bookmarks.ShowHidden = True
                End If
                For Each oBmk In oTable.Range.Bookmarks
                    xTag = Mid$(oBmk.Name, 2)
                    If BaseMethods.GetBaseNameFromTag(xTag) = "mSEG" Then
                        'validate segment type id
                        xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oLocation.Document)
                        xTypeID = BaseMethods.GetSegmentPropertyValue(xObjectData, "ObjectTypeID")
                        If CInt(xTypeID) = iCollectionTypeID Then
                            GetCollectionTableBookmark = oBmk
                            Exit For
                        End If
                    End If
                Next oBmk
                'GLOG 8722
                oTable.Range.Bookmarks.ShowHidden = bShowHidden
            End If

            Exit Function

ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Sub RetagCollectionTableItemIfNecessary(ByRef oWordTag As Word.XMLNode, _
                                                     ByRef oTags As Tags, _
                                                     ByRef bIsValid As Boolean, _
                                                     ByRef bRefreshRequired As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.RetagCollectionTableItemIfNecessary"
            'if closing mSEG of pleading table item is outside of table, and there's nothing
            'between it and the table, moves it inside - otherwise, just validates
            Dim oRange As Word.Range
            Dim oTable As Word.Table

            On Error GoTo ProcError

            oRange = oWordTag.Range
            bIsValid = False
            bRefreshRequired = False

            With oRange
                If (.Tables.Count <> 1) Or Not .Characters(1).Information(Word.wdInformation.wdWithInTable) Then _
                    Exit Sub
                oTable = .Tables(1)
                .EndOf()
                If Not .Information(Word.wdInformation.wdWithInTable) Then
                    oRange = oTable.Range
                    If oWordTag.Range.End = oRange.End Then
                        oRange.MoveEnd(WdUnits.wdCharacter, -1)
                        oTags.MoveWordTag(oWordTag, oRange)
                        bRefreshRequired = True
                    Else
                        Exit Sub
                    End If
                End If
            End With

            bIsValid = True

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        Public Sub RetagCollectionTableItemIfNecessary_CC(ByRef oCC As Word.ContentControl, _
                                                     ByRef oTags As Tags, _
                                                     ByRef bIsValid As Boolean, _
                                                     ByRef bRefreshRequired As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.RetagCollectionTableItemIfNecessary_CC"
            'if end pleading table item ContentControl is outside of table, and there's nothing
            'between it and the table, moves it inside - otherwise, just validates
            Dim oRange As Word.Range
            Dim oTable As Word.Table

            On Error GoTo ProcError

            oRange = oCC.Range
            bIsValid = False
            bRefreshRequired = False

            With oRange
                If (.Tables.Count <> 1) Or Not .Characters(1).Information(Word.wdInformation.wdWithInTable) Then _
                    Exit Sub
                oTable = .Tables(1)
                .EndOf()
                If Not .Information(Word.wdInformation.wdWithInTable) Then
                    oRange = oTable.Range
                    If oCC.Range.End = oRange.End Then
                        oRange.MoveEnd(WdUnits.wdCharacter, -1)
                        oTags.MoveContentControl(oCC, oRange)
                        bRefreshRequired = True
                    Else
                        Exit Sub
                    End If
                End If
            End With

            bIsValid = True

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub EqualizeCellWidths(ByVal oRow As Word.Row)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.EqualizeCellWidths"
            Dim sWidth As Single
            Dim oCell As Word.Cell

            On Error GoTo ProcError

            'get total width of row
            For Each oCell In oRow.Cells
                sWidth = sWidth + oCell.Width
            Next oCell

            'divide by number of cells
            sWidth = sWidth / oRow.Cells.Count

            'reset width of each cell
            For Each oCell In oRow.Cells
                oCell.Width = sWidth
            Next oCell

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function GetOpenPositionsInTable(ByVal oTable As Word.Table, _
                                                ByVal lItemTypeID As Integer) As String
            'returns delimited string representing the total # of rows and the open positions in the table -
            'cells are numbered as follows: row 1/left = 1, row 1/right = 2, row 2/left = 3, row 2/right = 4, etc.
            'for example, "2|2�4" indicates that this is a 2-row table in which the right side of both
            'rows is available
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.GetOpenPositionsInTable"
            Dim iRow As Short
            Dim xPositions As String
            Dim oRow As Word.Row
            Dim oTag As Word.XMLNode
            Dim xContent As String
            Dim bDisallow As Boolean
            Dim iRowCount As Short
            Dim iCellCount As Short
            Dim xTypeID As String
            Dim xAllowSideBySide As String

            iRowCount = oTable.Rows.Count

            For iRow = 1 To iRowCount
                bDisallow = False
                oRow = oTable.Rows(iRow)
                iCellCount = oRow.Cells.Count

                'determine whether existing items allow side-by-side
                For Each oTag In oRow.Range.XMLNodes
                    If oTag.BaseName = "mSEG" Then
                        xTypeID = BaseMethods.GetmSEGObjectDataValue(oTag, "ObjectTypeID")
                        If xTypeID = CStr(lItemTypeID) Then
                            xAllowSideBySide = BaseMethods.GetmSEGObjectDataValue(oTag, "AllowSideBySide")
                            bDisallow = (UCase(xAllowSideBySide) <> "TRUE")
                            If bDisallow Then _
                                Exit For
                        End If
                    End If
                Next oTag

                If Not bDisallow Then
                    'determine whether left side is available
                    xContent = oRow.Cells(1).Range.Text
                    If (xContent = Chr(7)) Or (xContent = vbCr & Chr(7)) Then
                        xPositions = xPositions & CStr((iRow * 2) - 1) & "�"
                    End If

                    'determine whether right side is available
                    'TODO: currently, right side is only available when there's an empty cell -
                    'this code should be adjusted once we add code to programatically
                    'add a new cell to a single-cell row
                    If iCellCount > 1 Then
                        xContent = oRow.Cells(2).Range.Text
                        If (xContent = Chr(7)) Or (xContent = vbCr & Chr(7)) Then
                            xPositions = xPositions & CStr(iRow * 2) & "�"
                        End If
                    End If
                End If
            Next iRow

            'trim trailing separator
            If xPositions <> "" Then _
                xPositions = Left$(xPositions, Len(xPositions) - 1)

            'prepend row count
            GetOpenPositionsInTable = CStr(iRowCount) & "|" & xPositions

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function SideBySideDesignIsValid(ByVal oCollectionItemTag As Word.XMLNode) As Boolean
            'returns TRUE if pleading table item design is appropriate for SideBySide=True -
            'currently, this is limited to 1-row items with an open cell
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.SideBySideDesignIsValid"
            Dim oRange As Word.Range
            Dim bIsValid As Boolean

            On Error GoTo ProcError

            oRange = oCollectionItemTag.Range

            If BaseMethods.RangeIsMultiCell(oRange) Then
                If oRange.Rows.Count = 1 Then
                    bIsValid = BaseMethods.CellIsEmpty(oRange.Rows(1).Cells(1))
                    If Not bIsValid Then _
                        bIsValid = BaseMethods.CellIsEmpty(oRange.Rows(1).Cells(2))
                End If
            End If

            SideBySideDesignIsValid = bIsValid

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function SideBySideDesignIsValid_CC(ByVal oCollectionItemCC As Word.ContentControl) As Boolean
            'returns TRUE if pleading table item design is appropriate for SideBySide=True -
            'currently, this is limited to 1-row items with an open cell
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.SideBySideDesignIsValid_CC"
            Dim oRange As Word.Range
            Dim bIsValid As Boolean

            On Error GoTo ProcError

            oRange = oCollectionItemCC.Range

            If BaseMethods.RangeIsMultiCell(oRange) Then
                If oRange.Rows.Count = 1 Then
                    bIsValid = BaseMethods.CellIsEmpty(oRange.Rows(1).Cells(1))
                    If Not bIsValid Then _
                        bIsValid = BaseMethods.CellIsEmpty(oRange.Rows(1).Cells(2))
                End If
            End If

            SideBySideDesignIsValid_CC = bIsValid

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function ItemIsFullRow(ByVal oWordTag As Word.XMLNode) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.ItemIsFullRow"
            On Error GoTo ProcError
            ItemIsFullRow = BaseMethods.TagIsFullRow(oWordTag, False)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function ItemIsFullRow_CC(ByVal oCC As Word.ContentControl) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.ItemIsFullRow_CC"
            On Error GoTo ProcError
            ItemIsFullRow_CC = BaseMethods.ContentControlIsFullRow(oCC, False)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Function ItemIsFullRow_Bookmark(ByVal oBmk As Word.Bookmark) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.ItemIsFullRow_Bookmark"
            On Error GoTo ProcError
            ItemIsFullRow_Bookmark = BaseMethods.BookmarkIsFullRow(oBmk, False)
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Function InsertPleadingCollectionTableTag(ByVal oRange As Word.Range, _
                ByVal iType As mpPleadingCollectionTypes) As Word.XMLNode
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.InsertPleadingCollectionTableTag"
            Dim oTag As Word.XMLNode
            Dim xTagID As String
            Dim xObjectData As String

            On Error GoTo ProcError

            'get attributes
            Select Case iType
                Case mpPleadingCollectionTypes.PleadingCounsels
                    xTagID = "PleadingCounsels_1"
                    xObjectData = "SegmentID=959|ObjectTypeID=507|Name=PleadingCounsels|IntendedUse=2|DisplayName=Pleading Counsels|AuthorNodeUILabel=Attorney|MaxAuthors=4|HelpText=&lt;td&gt;Select the counsel type from the available list. &lt;/td&gt;|MenuInsertionOptions=0|DefaultMenuInsertionBehavior=0|DefaultDragLocation=16|DefaultDragBehavior=0|DefaultDoubleClickLocation=16|DefaultDoubleClickBehavior=0|L0=0|L1=0|L2=0|L3=0|L4=0|AuthorControlProperties=AllowManualInput=false�AllowManualInputLeadAuthor=false�DropDownRows=8�DisplayRows=4�ShowColumns=7�ListType=1|ShowChooser=false|LinkAuthorsToParent=|TranslationID=0|ParentTagID=|LinkAuthorsToParentDef=2|IsTransparentDef=true|"
                Case mpPleadingCollectionTypes.PleadingCaptions
                    xTagID = "PleadingCaptions_1"
                    xObjectData = "SegmentID=960|ObjectTypeID=506|Name=PleadingCaptions|IntendedUse=2|DisplayName=Pleading Captions|AuthorNodeUILabel=Attorney|MaxAuthors=|HelpText=|MenuInsertionOptions=0|DefaultMenuInsertionBehavior=0|DefaultDragLocation=16|DefaultDragBehavior=0|DefaultDoubleClickLocation=16|DefaultDoubleClickBehavior=0|L0=0|L1=0|L2=0|L3=0|L4=0|AuthorControlProperties=|ShowChooser=false|LinkAuthorsToParent=|TranslationID=0|ParentTagID=|LinkAuthorsToParentDef=2|IsTransparentDef=true|"
                Case mpPleadingCollectionTypes.PleadingSignatures
                    xTagID = "PleadingSignatures_1"
                    xObjectData = "SegmentID=961|ObjectTypeID=508|Name=PleadingSignatures|IntendedUse=2|DisplayName=Pleading Signatures|AuthorNodeUILabel=Attorney|MaxAuthors=4|HelpText=&lt;td&gt;&lt;font size=&quot;3&quot; face=&quot;Times New Roman, Times, serif&quot;&gt;Select the appropriate &#xA;        signature type from the available list. The options available depend on &#xA;        the pleading type.&lt;o:p&gt;&lt;/o:p&gt;&lt;/font&gt;&lt;/td&gt;|MenuInsertionOptions=0|DefaultMenuInsertionBehavior=0|DefaultDragLocation=16|DefaultDragBehavior=0|DefaultDoubleClickLocation=16|DefaultDoubleClickBehavior=0|L0=0|L1=0|L2=0|L3=0|L4=0|AuthorControlProperties=AllowManualInput=false�AllowManualInputLeadAuthor=false�DropDownRows=8�DisplayRows=4�ShowColumns=3�ListType=1�ListWhereClause=|ShowChooser=false|LinkAuthorsToParent=|TranslationID=0|ParentTagID=|LinkAuthorsToParentDef=2|IsTransparentDef=true|"
                Case Else
                    Exit Function
            End Select

            On Error GoTo TryAgain
            'create new tag - handle failure by trying again-
            'this always works the second time
            oTag = oRange.XMLNodes.Add("mSEG", GlobalMethods.mpNamespace, oRange)
            On Error GoTo ProcError

            'set attributes
            With oTag.Attributes
                .Add("TagID", "").NodeValue = xTagID
                .Add("ObjectData", "").NodeValue = xObjectData
                .Add("PartNumber", "").NodeValue = "1"
            End With

            InsertPleadingCollectionTableTag = oTag

            Exit Function
TryAgain:
            Resume
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Sub InsertTOAFieldCodes(ByVal oLocation As Word.Range, ByVal bUsePassim As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.InsertTOAFieldCodes"
            Dim oField As Word.Field
            Dim iPos1 As Short
            Dim iPos2 As Short
            Dim xCode As String
            Dim xCategory As String
            Dim xCategories As String
            Dim i As Short
            Dim bShowCodes As Boolean
            Dim bShowHidden As Boolean 'GLOG 5115
            Dim oCodeRng As Word.Range
            Dim bShowTags As Boolean
            Dim bShowAll As Boolean

            'GLOG 5115
            bShowAll = oLocation.Document.ActiveWindow.View.ShowAll
            bShowTags = oLocation.Document.ActiveWindow.View.ShowXMLMarkup
            bShowCodes = oLocation.Document.ActiveWindow.View.ShowFieldCodes
            bShowHidden = oLocation.Document.ActiveWindow.View.ShowHiddenText

            If Len(oLocation.Text) > 0 Then
                oLocation.Text = ""
            End If

            'cycle through TA codes getting categories
            xCategories = "|"

            For Each oField In GlobalMethods.CurWordApp.ActiveDocument.Fields
                If oField.Type = wdFieldType.wdFieldTOAEntry Then
                    'GLOG 8503
                    xCode = oField.Code.Text
                    iPos1 = InStr(xCode, "\c ") + Len("\c ")
                    iPos2 = InStr(iPos1 + 1, xCode, " ")

                    If iPos2 > 0 Then
                        xCategory = Mid$(xCode, iPos1, iPos2 - iPos1)
                    Else
                        xCategory = Mid$(xCode, iPos1)
                    End If

                    xCategories = xCategories & xCategory & "|"
                End If
            Next oField

            'GLOG 5115: Make sure all Codes and Hidden Text are off so that pagination is correct
            oLocation.Document.ActiveWindow.View.ShowAll = False
            GlobalMethods.SetXMLMarkupState(oLocation.Document, False)
            oLocation.Document.ActiveWindow.View.ShowHiddenText = False
            oLocation.Document.ActiveWindow.View.ShowFieldCodes = False

            'insert toa field code for each category
            For i = 1 To 16
                If InStr(xCategories, "|" & CStr(i) & "|") > 0 Then
                    'include category in toa - insert field code

                    oField = oLocation.Fields.Add(oLocation, wdFieldType.wdFieldTOA, "\h" & _
                        IIf(bUsePassim, " \p", "") & " \c """ & CStr(i) & """")

                    oLocation = oField.Result
                    oLocation.EndOf()
                    oLocation.Move(WdUnits.wdCharacter, 1)
                End If
            Next i

            'GLOG 7293 (dm) - ensure trailing paragraph -
            'this may be an issue after updating a finished TOA
            Dim oRng As Word.Range
            On Error Resume Next
            oRng = oLocation.Paragraphs(1).Range.Next(WdUnits.wdCharacter)
            On Error GoTo ProcError
            If oRng Is Nothing Then
                oLocation.InsertParagraphAfter()
            ElseIf oRng.Text = Chr(12) Then
                oLocation.InsertParagraphAfter()
            End If

            oLocation.Document.ActiveWindow.View.ShowFieldCodes = bShowCodes
            GlobalMethods.SetXMLMarkupState(oLocation.Document, bShowTags, True) 'JTS 6/4/10
            oLocation.Document.ActiveWindow.View.ShowHiddenText = bShowHidden 'GLOG 5115
            oLocation.Document.ActiveWindow.View.ShowAll = bShowAll
            Exit Sub
ProcError:
            GlobalMethods.SetXMLMarkupState(oLocation.Document, bShowTags, True) 'JTS 6/4/10
            oLocation.Document.ActiveWindow.View.ShowFieldCodes = bShowCodes
            oLocation.Document.ActiveWindow.View.ShowHiddenText = bShowHidden 'GLOG 5115
            oLocation.Document.ActiveWindow.View.ShowAll = bShowAll

            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Function DeleteTOAFieldCodes(ByVal oLocation As Word.Range) As Word.Range
            Const mpThisFunction As String = "LMP.Forte.MSWord.Pleading.DeleteTOAFieldCodes"
            Dim oField As Word.Field
            Dim oTOALocation As Word.Range

            For Each oField In oLocation.Fields
                If oField.Type = wdFieldType.wdFieldTOA Then
                    If oTOALocation Is Nothing Then
                        oTOALocation = oLocation.Document _
                            .Range(oField.Result.Start, oField.Result.Start)
                    End If

                    oField.Delete()
                End If
            Next oField
            'JTS 9/14/10: Set return value
            DeleteTOAFieldCodes = oTOALocation
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
    End Class
End Namespace