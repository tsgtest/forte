'**********************************************************
'   LMP.Forte.MSWord.PleadingPaper Class
'   created 9/17/07 by Jenny Wade
'
'   'contains methods and functions that operate on pleading paper
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class PleadingPaper
        Public Function Has24PtLineSpacing() As Boolean
            'Check if Pleading Paper has 24 pt spacing.
            'document is a pleading.
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.Has24PtLineSpacing"
            Dim bHas24PtLineSpacing As Boolean
            Dim oTextFrame As Word.TextFrame
            Dim oShape As Word.Shape 'GLOG 7009
            Dim oShapeRange As Object
            Dim iCount As Short
            Dim i As Short

            On Error GoTo ProcError
            oShapeRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdFirstPageHeaderStory).ShapeRange
            iCount = oShapeRange.Count

            'cycle through each ShapeRange in first page header
            For i = 1 To iCount
                oShape = oShapeRange(i)
                'check if ShapeRange has a TextFrame
                'GLOG 7009
                If BaseMethods.HasTextFrameText(oShape) Then
                    oTextFrame = oShape.TextFrame
                    'GLOG 4867: Avoid error if textbox is empty
                    If oTextFrame.TextRange.Characters.Count > 1 Then
                        'make sure this is the correct TextFrame
                        If oTextFrame.TextRange.Characters(1).Text = "1" Or oTextFrame.TextRange.Characters(2).Text = "1" Then
                            If oTextFrame.TextRange.ParagraphFormat.LineSpacing = 24 Then
                                bHas24PtLineSpacing = True
                            End If
                        End If
                    End If
                End If
            Next
            Has24PtLineSpacing = bHas24PtLineSpacing
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function


        Public Function HasPleadingPaper() As Boolean
            'Check if Pleading Paper is present.  Numbers formatted in pleading line number style within a textbox
            'document is a pleading.
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.HasPleadingPaper"
            Dim bHasPleadingPaper As Boolean
            Dim oTextFrame As Word.TextFrame
            Dim oShape As Word.Shape 'GLOG 7009
            Dim oShapeRange As Object
            Dim iCount As Short
            Dim i As Short

            'GLOG 7098: there may not be a shape range
            On Error GoTo ExitFunction
            oShapeRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdFirstPageHeaderStory).ShapeRange
            On Error GoTo ProcError
            iCount = oShapeRange.Count

            'cycle through each ShapeRange in first page header
            For i = 1 To iCount
                oShape = oShapeRange(i)
                'check if ShapeRange has a TextFrame
                'GLOG 7009
                If BaseMethods.HasTextFrameText(oShape) Then
                    oTextFrame = oShape.TextFrame
                    'GLOG 4867: Avoid error if textbox is empty
                    If oTextFrame.TextRange.Characters.Count > 1 Then
                        'make sure this is the correct TextFrame
                        If oTextFrame.TextRange.Characters(1).Text = "1" Or oTextFrame.TextRange.Characters(2).Text = "1" Then
                            If HasLineNumbers(oTextFrame) Then
                                bHasPleadingPaper = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
            HasPleadingPaper = bHasPleadingPaper
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
ExitFunction:
            Exit Function
        End Function
        Public Function GetTaglessPleadingBodyRange() As Word.Range
            'is there contiguous text formatted in Body Text style?
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.GetTaglessPleadingBodyRange"
            Dim bHasPleadingPaper As Boolean
            Dim oBodyRange As Range
            Dim vStyle As Object
            Dim iStartPos As Short
            Dim iEndPos As Short
            Dim oPara As Word.Paragraph
            Dim oSel As Range

            On Error GoTo ProcError

            bHasPleadingPaper = HasPleadingPaper()

            If bHasPleadingPaper = False Then
                Exit Function
            End If
            'Does Pleading Signature Style exist? If so, this is probably not an mp10 Pleading.
            vStyle = "Pleading Signature"
            If Not CheckStyleExists(vStyle) Then
                Exit Function
            End If

            'Make sure Body Text style exists
            vStyle = "Body Text"
            If Not CheckStyleExists(vStyle) Then
                Exit Function
            End If

            oSel = GlobalMethods.CurWordApp.Selection.Range

            'Get body text block
            oBodyRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdMainTextStory)
            'find first instance of Body Text paragraph
            With oBodyRange.Find
                .ClearAllFuzzyOptions()
                .ClearFormatting()
                .Wrap = wdfindwrap.wdfindcontinue
                .Style = "Body Text"
                .Execute()
                If .Found Then
                    iStartPos = oBodyRange.Start
                End If
            End With
            oBodyRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdMainTextStory)
            'find first instance of Pleading Signature paragraph
            With oBodyRange.Find
                .ClearAllFuzzyOptions()
                .ClearFormatting()
                .Wrap = wdfindwrap.wdfindcontinue
                .Style = "Pleading Signature"
                .Execute()
                If .Found Then
                    iEndPos = oBodyRange.Start
                End If
            End With

            If iStartPos > 0 And (iEndPos > iStartPos) Then
                oBodyRange.SetRange(Start:=iStartPos, End:=iEndPos)
            Else
                oBodyRange = Nothing
            End If

            oSel.Select()
            GetTaglessPleadingBodyRange = oBodyRange
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Sub AlignText(ByVal oRange As Word.Range)
            Dim iCurrentPos As Short
            Dim iStyleSpaceBefore As Short
            Dim iFirstLinePos As Short
            Dim iDifference As Short
            Dim iPaddingNum As Short
            Dim iMultiplier As Short
            Dim iNextGoodNum As Short
            Dim iSpaceBefore As Short
            Dim oPara As Word.Paragraph
            Dim oSelectionRange As Word.Range
            Dim iCounter As Short
            Dim xPreviousStyle As String = ""
            Dim iPreviousSpaceAfter As Short
            Dim lStart As Integer
            Dim lEnd As Integer
            Dim iRangeType As Short
            Dim oShape As Word.Shape 'GLOG 7009
            Dim i As Short
            Dim iParaCount As Short
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.AlignText"

            On Error GoTo ProcError

            If oRange Is Nothing Then Exit Sub

            oSelectionRange = GlobalMethods.CurWordApp.Selection.Range

            Dim oTextFrame As Word.TextFrame
            Dim oShapeRange As Object
            Dim iCount As Short
            Dim bDiffFirstPage As Boolean
            Dim oSec As Word.Section
            Dim oHeader As Word.HeaderFooter
            Dim xFirstParaOutOfRangeStyle As String = ""

            'GLOG 6352: If Different First Page is not on, use Primary Header as guide
            oSec = oSelectionRange.Sections(1)
            If oSec.PageSetup.DifferentFirstPageHeaderFooter Then
                oShapeRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdFirstPageHeaderStory).ShapeRange
            Else
                oShapeRange = GlobalMethods.CurWordApp.ActiveDocument.StoryRanges(WdStoryType.wdPrimaryHeaderStory).ShapeRange
            End If
            iCount = oShapeRange.Count

            'cycle through each ShapeRange in first page header
            For i = 1 To iCount
                oShape = oShapeRange(i)
                'check if ShapeRange has a TextFrame
                'GLOG 7009
                If BaseMethods.HasTextFrameText(oShape) Then
                    oTextFrame = oShape.TextFrame
                    'GLOG 4867: Avoid error if textbox is empty
                    If oTextFrame.TextRange.Characters.Count > 1 Then
                        'make sure this is the correct TextFrame
                        If oTextFrame.TextRange.Characters(1).Text = "1" Or oTextFrame.TextRange.Characters(2).Text = "1" Then
                            'GLOG 6068:  pleading paper has to have 24 pt spacing
                            If oTextFrame.TextRange.ParagraphFormat.LineSpacing = 24 Then
                                If HasLineNumbers(oTextFrame) Then Exit For
                            End If
                        End If
                    End If
                End If
            Next
            If oTextFrame Is Nothing Then Exit Sub

            'get vertical position of first pleading paper line number
            iFirstLinePos = oTextFrame.TextRange.Characters.First.Information(wdInformation.wdVerticalPositionRelativeToPage)

            If iFirstLinePos = -1 Then
                '        to do:  create proper mpCOM message
                '        MsgBox "Cannot determine first numbered line position.  " & _
                '            "Please run the Forte Pleading Paper macro and try again.", vbOKOnly, My.Application.Info.Title
                Exit Sub
            End If

            GlobalMethods.CurWordApp.ScreenUpdating = False

            If Not (oRange.Previous(WdUnits.wdParagraph, 1) Is Nothing) Then
                'GLOG : 7711 : ceh
                On Error Resume Next
                'GLOG 8561
                xFirstParaOutOfRangeStyle = oRange.Previous(WdUnits.wdParagraph, 1).Style.NameLocal
                On Error GoTo ProcError
            End If

            For Each oPara In oRange.Paragraphs
                'GLOG 8561
                Dim xParaSty As String = ""
                On Error Resume Next
                xParaSty = oPara.Style.NameLocal
                On Error GoTo ProcError
                'GLOG : 3933 : CEH
                If oPara.Range.Information(Word.WdInformation.wdWithInTable) Then
                    'reset space after
                    iPreviousSpaceAfter = 0
                    GoTo NextPara
                End If
                'GLOG : 6693 : JSW
                'adding 12 pt before spacing on paragraph following SepStmt spacing paragraph
                'has no effect -- though 12 pt is added, the paragraph does not
                'change position since the spacer paragraph as 12 pts after
                'if this spacer paragraph is in the paragraph range, remove 12 pt
                'after spacing so following paragraph can be adjusted correctly
                'GLOG 8561
                If xParaSty = "SepStmt" Then
                    If oPara.LineSpacing = 1 Then
                        oPara.SpaceAfter = 0
                    End If
                End If


                iParaCount = iParaCount + 1
                oPara.Range.Select()
                GlobalMethods.CurWordApp.Selection.Collapse(WdCollapseDirection.wdCollapseStart)
                'use selection object to get cursor position,
                'range object doesn't always account for spacebefore padding.
                iCurrentPos = GlobalMethods.CurWordApp.Selection.Information(WdInformation.wdVerticalPositionRelativeToPage)

                If iCurrentPos = -1 Then
                    'selection.information returned a bad value - try range.information
                    iCurrentPos = GlobalMethods.CurWordApp.Selection.Range.Information(WdInformation.wdVerticalPositionRelativeToPage)
                End If

                'this number does not include space before if it's in style!
                'have to subtract that number from cursor position.
                iStyleSpaceBefore = GlobalMethods.CurWordApp.ActiveDocument.Styles(oPara.Style).ParagraphFormat.SpaceBefore
                iCurrentPos = (iCurrentPos - iStyleSpaceBefore)
                'Debug.Print "iCurrentPos:  " & iCurrentPos
                iDifference = iCurrentPos - iFirstLinePos
                'Debug.Print "Difference:  " & iDifference
                'if not divisible by 24 (number of points between paragraphs),
                'space before setting needs to be adjusted
                Dim dMod As Double
                dMod = iDifference Mod 24
                'Debug.Print "Dmod:  " & dMod
                If dMod <> 0 Then
                    'divide by 24, get result as whole number
                    iMultiplier = CDbl(CLng(iDifference / 24 - 0.5))
                    'add one to result, multiply by 24
                    iNextGoodNum = (iMultiplier + 1) * 24
                    'get number of points needed to make divisible by 24
                    iPaddingNum = iNextGoodNum - iDifference
                    'Debug.Print "Padding num:  " & iPaddingNum
                    If iPaddingNum > 4 And iPaddingNum < 20 Then
                        iSpaceBefore = oPara.SpaceBefore
                        'add or remove points to space before setting.
                        'GLOG 6261 JSW:  check to see if the first paragraph above the
                        'is a spacer paragraph, and if the paragraph being
                        'aligned is the first in the alignment range
                        'if so, it's safe to decrement.
                        If xPreviousStyle = "" Then
                            xPreviousStyle = xFirstParaOutOfRangeStyle
                        End If
                        If (iSpaceBefore >= 12 And iPreviousSpaceAfter >= 12) _
                            Or (IsStyleException(xPreviousStyle) And oPara.SpaceBefore >= 12) _
                            Or iSpaceBefore >= 24 Then '*c
                            'decrement
                            oPara.SpaceBefore = iSpaceBefore - 12 'iPaddingNum
                        Else
                            'increment
                            'GLOG 8561
                            If xParaSty <> "SepStmt" Then
                                oPara.SpaceBefore = iSpaceBefore + 12 'iPaddingNum
                            End If
                        End If
                        'getting the correct position requires a screen refresh!
                        GlobalMethods.CurWordApp.ScreenRefresh()
                    End If
                End If
SkipMe:
                xPreviousStyle = xParaSty
                iPreviousSpaceAfter = oPara.SpaceAfter
                'Debug.Print "iPreviousSpaceAfter:  " & iPreviousSpaceAfter
NextPara:
            Next oPara

            oSelectionRange.Select()

            GlobalMethods.CurWordApp.ScreenUpdating = True
            GlobalMethods.CurWordApp.ScreenRefresh()
            Exit Sub
ProcError:
            GlobalMethods.CurWordApp.ScreenUpdating = True
            GlobalMethods.CurWordApp.ScreenRefresh()
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub AlignHeaderToLine1(ByVal oHeader As Word.HeaderFooter)
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.AlignHeaderToLine1"
            Dim iCurrentPos As Short
            Dim iStyleSpaceBefore As Short
            Dim iFirstLinePos As Short
            Dim iDifference As Short
            Dim iSpaceBefore As Short
            Dim oPara1 As Word.Paragraph
            Dim iCounter As Short
            Dim oShape As Word.Shape 'GLOG 7009
            Dim i As Short
            Dim oTextFrame As Word.TextFrame
            Dim oShapeRange As Object
            Dim iCount As Short
            Dim iViewType As WdViewType
            Dim oSec As Word.Section
            Dim bDiffFirstPage As Boolean 'GLOG 4822

            On Error GoTo ProcError

            If oHeader Is Nothing Then Exit Sub
            oSec = oHeader.Range.Sections(1)

            bDiffFirstPage = oSec.PageSetup.DifferentFirstPageHeaderFooter

            'GLOG 4822: Make sure Header is accessible for selecting in Print View
            'by temporarily resetting DifferentFirstPage setting if necessary
            If Not bDiffFirstPage And oHeader.Index = WdHeaderFooterIndex.wdHeaderFooterFirstPage Then
                oSec.PageSetup.DifferentFirstPageHeaderFooter = True
            ElseIf bDiffFirstPage And oHeader.Index = WdHeaderFooterIndex.wdHeaderFooterPrimary Then
                oSec.PageSetup.DifferentFirstPageHeaderFooter = False
            End If

            oPara1 = oHeader.Range.Paragraphs(1)

            oShapeRange = oHeader.Range.ShapeRange

            iCount = oShapeRange.Count

            'cycle through each ShapeRange in first page header
            For i = 1 To iCount
                oShape = oShapeRange(i)
                'GLOG 6275:  Test Shape is a textbox before accessing TextFrame
                If oShape.Type = Microsoft.Office.Core.MsoShapeType.msoTextBox Then
                    'check if ShapeRange has a TextFrame
                    If BaseMethods.HasTextFrameText(oShape) Then
                        oTextFrame = oShape.TextFrame
                        'make sure this is the correct TextFrame
                        If oTextFrame.TextRange.Characters(1).Text = "1" Or _
                            oTextFrame.TextRange.Characters(2).Text = "1" Then
                            If HasLineNumbers(oTextFrame) Then Exit For
                        End If
                    End If
                End If
            Next

            If oTextFrame Is Nothing Then
                With oHeader.Range.ParagraphFormat
                    'set all paragraphs to line up with pleading paper
                    .LineSpacingRule = wdLineSpacing.wdLineSpaceSingle
                    .SpaceBefore = 12
                End With

                Exit Sub
            End If

            'get vertical position of first pleading paper line number
            oTextFrame.TextRange.Characters.First.Select()
            iFirstLinePos = GlobalMethods.CurWordApp.Selection.Information(wdInformation.wdVerticalPositionRelativeToPage)

            If iFirstLinePos = -1 Then
                'try alternate method of retrieving vertical position
                iFirstLinePos = GlobalMethods.CurWordApp.Selection.Range.Information(wdInformation.wdVerticalPositionRelativeToPage)

                If iFirstLinePos = -1 Then
                    Exit Sub
                End If
            End If

            GlobalMethods.CurWordApp.ScreenUpdating = False

            oPara1.Range.Select()
            GlobalMethods.CurWordApp.Selection.Collapse(wdCollapseDirection.wdCollapseStart)

            If GlobalMethods.CurWordApp.ActiveWindow.View.Type <> Word.WdViewType.wdPrintView Then
                iViewType = GlobalMethods.CurWordApp.ActiveWindow.View.Type
                GlobalMethods.CurWordApp.ActiveWindow.View.Type = Word.WdViewType.wdPrintView
            End If

            'use selection object to get cursor position,
            'range object doesn't always account for spacebefore padding.
            iCurrentPos = GlobalMethods.CurWordApp.Selection.Information(wdInformation.wdVerticalPositionRelativeToPage)

            If iCurrentPos = -1 Then
                'selection.information returned a bad value - try range.information
                iCurrentPos = GlobalMethods.CurWordApp.Selection.Range.Information(wdInformation.wdVerticalPositionRelativeToPage)
            End If

            iStyleSpaceBefore = oPara1.SpaceBefore
            iCurrentPos = (iCurrentPos - iStyleSpaceBefore)
            iDifference = iCurrentPos - iFirstLinePos

            With oHeader.Range.ParagraphFormat
                'set all paragraphs to line up with pleading paper
                .LineSpacingRule = wdLineSpacing.wdLineSpaceExactly
                .LineSpacing = 12
                .SpaceBefore = 12
            End With

            'move first para to
            oPara1.SpaceBefore = IIf(iDifference < 0, Math.Abs(iDifference) + 9, 0)

            oSec.PageSetup.DifferentFirstPageHeaderFooter = bDiffFirstPage

            'return to original view if necessary
            If iViewType <> 0 Then '9/7/11 (dm)
                If GlobalMethods.CurWordApp.ActiveWindow.View.Type <> iViewType Then
                    GlobalMethods.CurWordApp.ActiveWindow.View.Type = iViewType
                End If
            End If

            Exit Sub
ProcError:
            GlobalMethods.CurWordApp.ScreenUpdating = True
            GlobalMethods.CurWordApp.ScreenRefresh()
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function HasLineNumbers(oTextFrame As Word.TextFrame) As Boolean
            'returns true if text box contains line numbers from 1 to 23
            Dim i As Short
            Dim vChars As Object
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.HasLineNumbers"

            On Error GoTo ProcError

            vChars = Split(oTextFrame.TextRange.Text, Chr(11))

            If IsArray(vChars) Then
                If UBound(vChars, 1) < 22 Then
                    HasLineNumbers = False
                    Exit Function
                End If
            Else
                HasLineNumbers = False
                Exit Function
            End If

            For i = 0 To 22
                If vChars(i) = i + 1 Then
                    HasLineNumbers = True
                Else
                    HasLineNumbers = False
                    Exit For
                End If
            Next
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub AdjustTOCSection(ByVal oSection As Word.Section)
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.AdjustTOCSection"
            Dim oBookmark As Word.Bookmark
            Dim rngLocation As Word.Range
            Dim rngHeaderFirstPage As Word.Range
            Dim bPreserveBmk As Boolean
            Dim oDeleteScope As DeleteScope

            On Error Resume Next
            oBookmark = oSection.Range.Bookmarks("mpTableOfContents")
            If oBookmark Is Nothing Then
                'check for temporary bookmark used during TOC section insertion
                oBookmark = oSection.Range.Bookmarks("mpTableOfContents_Temp")
            End If
            On Error GoTo ProcError

            If Not oBookmark Is Nothing Then
                'GLOG 5434 (dm) - the new segment bookmark may be in the first page footer if
                'the pleading paper has been inserted from a fully converted db into a .doc
                rngLocation = oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                bPreserveBmk = rngLocation.Bookmarks.Exists("mpNewSegment")

                'GLOG 8393 (dm) - moved the following function from mpCTOC into this class
                AdjustTOCForFortePaper(oSection)

                'GLOG 5247 - Retag controls in pasted content (added 9/7/11 dm)
                Dim iFileFormat As mpFileFormats
                rngLocation.WholeStory()
                iFileFormat = BaseMethods.FileFormat(rngLocation.Document)
                If iFileFormat = mpfileformats.Openxml Then
                    Dim oCC As ContentControl
                    For Each oCC In rngLocation.ContentControls
                        BaseMethods.RetagContentControl(oCC)

                        '9/7/11 (dm) - delete scope xml needs to be retagged as well
                        If BaseMethods.GetBaseName(oCC) = "mSEG" Then
                            If oDeleteScope Is Nothing Then _
                                    oDeleteScope = New DeleteScope
                            oDeleteScope.RetagDeletedScopes(oCC.Tag, oCC.Range.Document)
                        End If

                        'GLOG 6921 (dm) - bookmark retagged content control
                        oCC.Range.Bookmarks.Add("_" & oCC.Tag)
                    Next oCC
                Else
                    Dim oNode As XMLNode
                    For Each oNode In rngLocation.XMLNodes
                        BaseMethods.RetagXMLNode(oNode)

                        'GLOG 6921 (dm) - bookmark retagged xml node
                        oNode.Range.Bookmarks.Add("_" & BaseMethods.GetTag(oNode))
                    Next oNode
                End If

                'GLOG 5434 (dm) - restore bookmark
                If bPreserveBmk Then
                    rngLocation.WholeStory()
                    rngLocation.StartOf()
                    rngLocation.Move()
                    rngLocation.Bookmarks.Add("mpNewSegment")
                End If

                'GLOG 6423 (dm) - the pleading paper may not be configured for different
                'first page, causing "(continued)" to appear on the first page
                If Not oSection.PageSetup.DifferentFirstPageHeaderFooter Then _
                    oSection.PageSetup.DifferentFirstPageHeaderFooter = True

                'GLOG 7634 (dm) - reset right tab stop
                ResetRightTabStop_TOC(oSection)

                'GLOG : 6967 : CEH
                'Reset First Page Header Space After
                rngHeaderFirstPage = oSection.Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                rngHeaderFirstPage.Paragraphs.Last.Range.ParagraphFormat.SpaceAfter = 0
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub AdjustLitigationAddOnSection(ByVal oSection As Word.Section, ByVal bIsTOA As Boolean)
            'makes hardcoded adjustments to TOA and Exhibits pleading paper footers as a temporary
            'solution until we make this configurable
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.AdjustLitigationAddOnSection"
            Dim xText As String
            Dim rngLocation As Word.Range
            Dim bPreserveBmk As Boolean
            Dim oDeleteScope As DeleteScope
            Dim rngHeaderFirstPage As Word.Range

            On Error Resume Next

            Clipboard.SaveClipboard()

            'footers should be identical in TOA - copy primary and paste into first page
            oSection.Footers(wdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Copy()
            rngLocation = oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range

            'GLOG 5434 (dm) - the new segment bookmark may be in the first page footer if
            'the pleading paper has been inserted from a fully converted db into a .doc
            bPreserveBmk = rngLocation.Bookmarks.Exists("mpNewSegment")

            rngLocation.Delete()
            rngLocation.Paste()
            rngLocation.WholeStory()
            rngLocation.Characters.Last.Delete()

            'GLOG 5247 - Retag controls in pasted content
            Dim iFileFormat As mpFileFormats
            iFileFormat = BaseMethods.FileFormat(rngLocation.Document)
            If iFileFormat = mpfileformats.Openxml Then
                Dim oCC As ContentControl
                For Each oCC In rngLocation.ContentControls
                    BaseMethods.RetagContentControl(oCC)

                    '9/7/11 (dm) - delete scope xml needs to be retagged as well
                    If BaseMethods.GetBaseName(oCC) = "mSEG" Then
                        If oDeleteScope Is Nothing Then _
                                oDeleteScope = New DeleteScope
                        oDeleteScope.RetagDeletedScopes(oCC.Tag, oCC.Range.Document)
                    End If
                    'GLOG 6852: Bookmark retagged Content Control
                    oCC.Range.Bookmarks.Add("_" & oCC.Tag)
                Next oCC
            Else
                Dim oNode As XMLNode
                For Each oNode In rngLocation.XMLNodes
                    BaseMethods.RetagXMLNode(oNode)
                    'GLOG 6852: Bookmark retagged XML Node
                    oNode.Range.Bookmarks.Add("_" & BaseMethods.GetTag(oNode))
                Next oNode
            End If

            'GLOG 5434 (dm) - restore bookmark
            If bPreserveBmk Then
                rngLocation.StartOf()
                rngLocation.Move()
                rngLocation.Bookmarks.Add("mpNewSegment")
            End If

            'GLOG 8746
            'set page numbering style
            'If bIsTOA Then
            '    oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers.NumberStyle = _
            '        WdPageNumberStyle.wdPageNumberStyleLowercaseRoman
            'Else
            '    oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers.NumberStyle = _
            '        WdPageNumberStyle.wdPageNumberStyleArabic
            'End If

            'GLOG 6423 (dm) - the pleading paper may not be configured for different
            'first page, causing "(continued)" to appear on the first page
            If bIsTOA And Not oSection.PageSetup.DifferentFirstPageHeaderFooter Then _
                oSection.PageSetup.DifferentFirstPageHeaderFooter = True

            'GLOG 7634 (dm) - adjust right tab stop for new margins by updating field -
            'tab stop is not part of style definition as it is for TOC
            If bIsTOA Then _
                oSection.Range.Fields.Update()


            'GLOG : 6967 : CEH
            'Reset First Page Header Space After
            If bIsTOA Then
                rngHeaderFirstPage = oSection.Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                rngHeaderFirstPage.Paragraphs.Last.Range.ParagraphFormat.SpaceAfter = 0
            End If


            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        '**********************************************************
        '   Internal Procs
        '**********************************************************
        Private Function CheckStyleExists(ByVal vStyle As Object) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.CheckStyleExists"
            Dim oSty As Word.Style
            On Error Resume Next
            oSty = GlobalMethods.CurWordApp.ActiveDocument.Styles(vStyle)
            If oSty Is Nothing Then
                CheckStyleExists = False
            Else
                CheckStyleExists = True
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Private Function IsStyleException(ByVal xStyle As String) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.IsStyleException"
            If xStyle = "Normal" Or xStyle = "Pleading" Or xStyle = "Placeholder" Or xStyle = "" Then
                IsStyleException = True
            Else
                'GLOG : 6509 : jsw
                'Styles with 24 pt line spacing also ok
                If GlobalMethods.CurWordApp.ActiveDocument.Styles(xStyle).ParagraphFormat.LineSpacing = 24 Then
                    IsStyleException = True
                End If
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Private Sub ResetRightTabStop_TOC(secTOC As Word.Section)
            'redefines right tab stop of TOC styles to fit margins - added for GLOG 7634 (dm)
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.ResetRightTabStop_TOC"
            Dim sRightTabPos As Single
            Dim xTOCStyle As String
            Dim i As Short
            Dim styTOC As Word.Style
            Dim lColWidth As Single

            On Error GoTo ProcError

            '   get proper position of page number tab stop
            With secTOC.PageSetup
                If .TextColumns.Count = 1 Then
                    sRightTabPos = .PageWidth - .LeftMargin - _
                        .RightMargin - .Gutter - GlobalMethods.CurWordApp.InchesToPoints(0.05)
                ElseIf .TextColumns.Count = 2 Then
                    lColWidth = BaseMethods.Min(.TextColumns(1).Width, _
                        .TextColumns(2).Width)
                    sRightTabPos = lColWidth - GlobalMethods.CurWordApp.InchesToPoints(0.01)
                Else
                    sRightTabPos = .TextColumns(1).Width - GlobalMethods.CurWordApp.InchesToPoints(0.01)
                End If
            End With

            For i = 1 To 9
                '       modify the last tab position to proper position
                xTOCStyle = xTranslateTOCStyle(i)
                styTOC = GlobalMethods.CurWordApp.ActiveDocument.Styles(xTOCStyle)
                With styTOC.ParagraphFormat.TabStops
                    If .Count > 0 Then 'GLOG 7672 (dm)
                        .Item(.Count).Position = sRightTabPos
                    End If
                End With
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function xTranslateTOCStyle(iLevel As Short) As String
            'gets local name of iLevel TOC style - added for GLOG 7634 (dm)
            With GlobalMethods.CurWordApp.ActiveDocument
                Select Case iLevel
                    Case 1
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC1).NameLocal
                    Case 2
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC2).NameLocal
                    Case 3
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC3).NameLocal
                    Case 4
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC4).NameLocal
                    Case 5
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC5).NameLocal
                    Case 6
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC6).NameLocal
                    Case 7
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC7).NameLocal
                    Case 8
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC8).NameLocal
                    Case 9
                        xTranslateTOCStyle = _
                            .Styles(WdBuiltinStyle.wdStyleTOC9).NameLocal
                End Select
            End With
        End Function

        Private Sub AdjustTOCForFortePaper(ByVal oSection As Word.Section)
            'adjust generic toc headers and footers - this is a temporary patch until we
            'provide a way in mp10 to configure TOC headers/footers for pleadings
            'GLOG 8393 (dm) - moved into Forte from mpCTOC
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.AdjustTOCForFortePaper"
            Dim oHeader As Word.HeaderFooter
            Dim oPara As Word.Paragraph
            Dim rngPara As Word.Range
            Dim rngHeader As Word.Range
            Dim rngLocation As Word.Range
            Dim oNextPara As Word.Paragraph
            Dim bBold As Boolean
            Dim bAllCaps As Boolean
            Dim iUnderline As WdUnderline

            On Error GoTo ProcError

            If bContainsNumberedFortePPaper(oSection) Then
                'move header down to align with line 1
                'GLOG 8687: Make sure this is a postive value
                oSection.PageSetup.HeaderDistance = Math.Abs(oSection.PageSetup.TopMargin)

                'adjust spacing after header
                For Each oHeader In oSection.Headers
                    rngHeader = oHeader.Range
                    For Each oPara In oHeader.Range.Paragraphs
                        rngPara = oPara.Range
                        If Left$(UCase$(rngPara.Text), 4) = "PAGE" Then
                            'preserve font formatting when applying style
                            bBold = rngPara.Bold
                            bAllCaps = rngPara.Font.AllCaps
                            iUnderline = rngPara.Font.Underline
                            rngPara.Style = "TOC Header"
                            rngPara.Bold = bBold
                            rngPara.Font.AllCaps = bAllCaps
                            rngPara.Font.Underline = iUnderline
                            rngPara.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight
                            If rngPara.End = rngHeader.End Then
                                rngPara.ParagraphFormat.SpaceAfter = 6
                            ElseIf rngPara.Next(WdUnits.wdParagraph).Text = vbCr Then
                                'don't delete empty trailing paragraph because it may be
                                'the anchor for textboxes
                                'rngPara.ParagraphFormat.SpaceAfter = 5 'GLOG 5840 (dm)
                                With rngPara.Next(WdUnits.wdParagraph).ParagraphFormat
                                    .LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                                    .LineSpacing = 1
                                End With
                            End If
                            Exit For
                        End If
                    Next oPara
                Next oHeader

                'GLOG 5840 (dm) - align with numbers
                Dim oStyle As Word.Style
                On Error Resume Next
                oStyle = oSection.Parent.Styles("TOC Header")
                On Error GoTo 0
                If Not oStyle Is Nothing Then
                    oStyle.ParagraphFormat.LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                    oStyle.ParagraphFormat.LineSpacing = 12
                End If
            End If

            'footers should be identical in TOC - copy primary and paste into first page
            Clipboard.SaveClipboard()
            oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.Copy()
            rngLocation = oSection.Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
            rngLocation.Delete()
            rngLocation.Paste()
            rngLocation.WholeStory()
            rngLocation.Characters.Last.Delete()
            Clipboard.RestoreClipboard()

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function bContainsNumberedFortePPaper(oSection As Word.Section) As Boolean
            'GLOG 8393 (dm) - moved into Forte from mpCTOC
            Const mpThisFunction As String = "LMP.Forte.MSWord.PleadingPaper.bContainsNumberedFortePPaper"
            Dim i As Integer
            Dim j As Integer
            Dim oShape As Word.Shape
            Dim oGroupItem As Word.Shape
            Dim oTextFrame As Word.TextFrame
            Dim oTag As Word.XMLNode
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            For i = 1 To oSection.Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes.Count
                On Error Resume Next
                oShape = oSection.Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes(i)
                On Error GoTo ProcError

                If Not oShape Is Nothing Then
                    With oShape
                        If (.Anchor.Sections.First.Index = oSection.Index) And _
                               (.Anchor.StoryType = WdStoryType.wdPrimaryHeaderStory) Then
                            If .Type = Microsoft.Office.Core.MsoShapeType.msoGroup Then
                                'group - check individual items
                                For Each oGroupItem In .GroupItems
                                    oTextFrame = Nothing
                                    On Error Resume Next
                                    oTextFrame = oGroupItem.TextFrame
                                    On Error GoTo ProcError
                                    If Not oTextFrame Is Nothing Then
                                        If oTextFrame.HasText Then
                                            For j = 1 To oTextFrame.TextRange.XMLNodes.Count
                                                oTag = oTextFrame.TextRange.XMLNodes(j)
                                                If oTag.BaseName = "mSEG" Then
                                                    'can't directly check segment type because
                                                    'object data may be compressed
                                                    If InStr(oTag.Range.Text, "1" & vbVerticalTab & "2") > 0 Then
                                                        bContainsNumberedFortePPaper = True
                                                        Exit Function
                                                    End If
                                                End If
                                            Next j

                                            'content controls - added 8/11/10
                                            For j = 1 To oTextFrame.TextRange.ContentControls.Count
                                                oCC = oTextFrame.TextRange.ContentControls(j)
                                                If Left$(oCC.Tag, 3) = "mps" Then
                                                    If InStr(oCC.Range.Text, "1" & vbVerticalTab & "2") > 0 Then
                                                        bContainsNumberedFortePPaper = True
                                                        Exit Function
                                                    End If
                                                End If
                                            Next j
                                        End If
                                    End If
                                Next oGroupItem
                            Else
                                oTextFrame = Nothing
                                On Error Resume Next
                                oTextFrame = .TextFrame
                                On Error GoTo ProcError
                                If Not oTextFrame Is Nothing Then
                                    If oTextFrame.HasText Then
                                        For j = 1 To oTextFrame.TextRange.XMLNodes.Count
                                            oTag = oTextFrame.TextRange.XMLNodes(j)
                                            If oTag.BaseName = "mSEG" Then
                                                If InStr(oTag.Range.Text, "1" & vbVerticalTab & "2") > 0 Then
                                                    bContainsNumberedFortePPaper = True
                                                    Exit Function
                                                End If
                                            End If
                                        Next j

                                        'content controls - added 8/11/10
                                        For j = 1 To oTextFrame.TextRange.ContentControls.Count
                                            oCC = oTextFrame.TextRange.ContentControls(j)
                                            If Left$(oCC.Tag, 3) = "mps" Then
                                                If InStr(oCC.Range.Text, "1" & vbVerticalTab & "2") > 0 Then
                                                    bContainsNumberedFortePPaper = True
                                                    Exit Function
                                                End If
                                            End If
                                        Next j
                                    End If
                                End If
                            End If
                        End If
                    End With
                End If
                oShape = Nothing
            Next i

            bContainsNumberedFortePPaper = False

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
    End Class
End Namespace