'**********************************************************
'   PageNumbering Class
'   created 3/6/00 by Dan Fisherman-
'   momshead@earthlink.net

'   edited 5/21/09 by Charlie Homo
'   for use in mp10

'   contains methods and functions that insert Page Numbers
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class PageNumbering
        Private Enum mpPageNumberLocations
            mpPageNumberLocation_Header = 1
            mpPageNumberLocation_Footer = 2
            mpPageNumberLocation_Cursor = 3
        End Enum

        Private Enum mpPageNumberAlignments
            mpPageNumberAlignment_Left = 1
            mpPageNumberAlignment_Center = 2
            mpPageNumberAlignment_Right = 3
        End Enum

        Private Enum mpPageNumbers
            mpPageNumbers_Arabic = 1
            mpPageNumbers_UpperRoman = 2
            mpPageNumbers_LowerRoman = 3
            mpPageNumbers_UpperAlpha = 4
            mpPageNumbers_LowerAlpha = 5
            mpPageNumbers_CardText = 6
        End Enum

        Private Enum mpPageNumberRemoveOptions
            mpPageNumberRemoveOption_AllPages = 1
            mpPageNumberRemoveOption_FirstPage = 2
        End Enum

        Private m_xXofY As String
        Private m_iNum As mpPageNumbers
        Private m_iLocation As mpPageNumberLocations
        Private m_iAlign As mpPageNumberAlignments
        Private m_iNumRemoveOption As mpPageNumberRemoveOptions
        Private m_xTextBefore As String
        Private m_xTextAfter As String
        Private m_bFirstPage As Boolean
        Private m_iSection As Short
        Private m_bResetPrompt As Boolean
        Private m_bRestartNumbering As Boolean
        Private m_iRestartAtNumber As Short
        Private m_bSetupDiffFirstPage As Boolean
        '
        '**********************************************************
        '   Methods
        '**********************************************************
        'GLOG : 6435 : ceh
        Public Sub Insert(ByVal iLocation As Short, _
                          ByVal vTargetedSections As Object, _
                          ByVal bFirstPage As Boolean, _
                          ByVal xXofY As String, _
                          ByVal iAlignment As Short, _
                          ByVal xTextBefore As String, _
                          ByVal xTextAfter As String, _
                          ByVal iNumber As Short, _
                          ByVal bRestartNumbering As Boolean, _
                          ByVal iRestartAtNumber As Short)
            'inserts a page number -prompt is reset to allow
            'user to be prompted in PromptForDeletion
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.Insert"
            Dim oRange As Word.Range
            Dim oSec As Word.Section
            Dim i As Short
            Dim iAnswer As Short
            Dim xMsg As String
            Dim oWordDoc As WordDoc

            On Error GoTo ProcError

            '   initialize module level variables
            m_xTextBefore = xTextBefore
            m_xTextAfter = xTextAfter
            m_xXofY = xXofY
            m_iNum = iNumber
            m_iLocation = iLocation
            m_bFirstPage = bFirstPage
            m_iAlign = iAlignment
            m_bRestartNumbering = bRestartNumbering
            m_iRestartAtNumber = iRestartAtNumber

            oWordDoc = New WordDoc

            If m_iLocation = mpPageNumberLocations.mpPageNumberLocation_Cursor Then
                '       insert page number at cursor
                oRange = GlobalMethods.CurWordApp.Selection.Range

                '       insert number at range
                InsertPageNumber(oRange)
            ElseIf UBound(vTargetedSections) = 0 Then   'only one targeted section
                '*      check page setup
                'GLOG 4748: Don't use PageSetup object to avoid bug when Framed table exists
                If Not (GlobalMethods.CurWordApp.ActiveDocument.Sections(vTargetedSections(0)) _
                   .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Exists) And _
                   Not m_bFirstPage Then
                    xMsg = "You have chosen not to include a page number on the first page of this section, but "
                    xMsg = xMsg & "it is not currently setup to allow for this."
                    xMsg = xMsg & vbCr & vbCr & "Click 'Yes' to allow for a different first page header\footer."
                    xMsg = xMsg & vbCr & "If you choose 'No', the page number will appear on every page."
                    iAnswer = MsgBox(xMsg, vbYesNoCancel + vbInformation, My.Application.Info.Title)
                    Select Case iAnswer
                        Case vbYes
                            m_bSetupDiffFirstPage = True
                        Case vbNo
                            m_bSetupDiffFirstPage = False
                        Case Else
                            GoTo ProcEnd
                    End Select
                Else
                    m_bSetupDiffFirstPage = False
                End If
                InsertPageNumberInHeadersFooters(vTargetedSections(0))
            Else    'there are more than one targeted section
                '*      check page setup
                If oWordDoc.HasSecWithNoDiffFirstPage(vTargetedSections) And _
                   Not m_bFirstPage Then
                    xMsg = "You have chosen not to include a page number on the first page of sections of this document, but "
                    xMsg = xMsg & "some sections are not setup to allow for this."
                    xMsg = xMsg & vbCr & vbCr & "Click 'Yes' to allow for a different first page header\footer on each section of this document."
                    xMsg = xMsg & vbCr & "If you choose 'No', the page number will appear on every page of the sections that are not properly setup."
                    iAnswer = MsgBox(xMsg, vbYesNoCancel + vbInformation, My.Application.Info.Title)
                    Select Case iAnswer
                        Case vbYes
                            m_bSetupDiffFirstPage = True
                        Case vbNo
                            m_bSetupDiffFirstPage = False
                        Case Else
                            GoTo ProcEnd
                    End Select
                Else
                    m_bSetupDiffFirstPage = False
                End If
                '       insert in all sections - cycle
                For i = 0 To UBound(vTargetedSections)
                    InsertPageNumberInHeadersFooters(vTargetedSections(i))
                Next i
            End If

ProcEnd:
            'JTS 5/7/10: Open and close header to clear out empty paragraph mark if present
            GlobalMethods.CurWordApp.WordBasic.ViewHeader()
            GlobalMethods.CurWordApp.WordBasic.ViewHeader()
            m_bResetPrompt = True
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        'GLOG : 6025 : CEH
        Public Sub Remove(ByVal iRemoveOption As Short, _
                          ByVal vTargetedSections As Object)
            'inserts a page number -prompt is reset to allow
            'user to be prompted in PromptForDeletion
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.Remove"

            Dim oRange As Word.Range
            Dim oSec As Word.Section
            Dim i As Short
            Dim iAnswer As Short
            Dim xMsg As String
            Dim oWordDoc As WordDoc

            On Error GoTo ProcError

            '   initialize module level variable for first page
            m_bFirstPage = (iRemoveOption = mpPageNumberRemoveOptions.mpPageNumberRemoveOption_FirstPage)

            oWordDoc = New WordDoc

            If UBound(vTargetedSections) = 0 Then   'only one targeted section
                '*      check page setup
                'GLOG 4748: Don't use PageSetup object to avoid bug when Framed table exists
                If Not (GlobalMethods.CurWordApp.ActiveDocument.Sections(vTargetedSections(0)) _
                   .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Exists) And _
                   m_bFirstPage Then
                    xMsg = "You have chosen to remove the page number from the first page of this section, but "
                    xMsg = xMsg & "it is not currently setup to allow for this."
                    xMsg = xMsg & vbCr & vbCr & "Click 'Ok' to allow for a different first page header\footer."
                    iAnswer = MsgBox(xMsg, vbOKCancel + vbInformation, My.Application.Info.Title)
                    Select Case iAnswer
                        Case vbOK
                            m_bSetupDiffFirstPage = True
                        Case Else
                            GoTo ProcEnd
                    End Select
                Else
                    m_bSetupDiffFirstPage = False
                End If
                RemovePageNumberInHeadersFooters(vTargetedSections(0))
            Else
                '*      check page setup
                If Not (oWordDoc.HasSecWithNoDiffFirstPage(vTargetedSections)) And _
                   m_bFirstPage Then
                    xMsg = "You have chosen to remove the page number from the first page of all sections of this document, but "
                    xMsg = xMsg & "some sections are not setup to allow for this."
                    xMsg = xMsg & vbCr & vbCr & "Click 'Ok' to allow for a different first page header\footer on each section of this document."
                    iAnswer = MsgBox(xMsg, vbOKCancel + vbInformation, My.Application.Info.Title)
                    Select Case iAnswer
                        Case vbOK
                            m_bSetupDiffFirstPage = True
                        Case Else
                            GoTo ProcEnd
                    End Select
                Else
                    m_bSetupDiffFirstPage = False
                End If
                '       insert in all sections - cycle
                For i = 0 To UBound(vTargetedSections)
                    RemovePageNumberInHeadersFooters(vTargetedSections(i))
                Next i
            End If

ProcEnd:
            GlobalMethods.CurWordApp.WordBasic.ViewHeader()
            GlobalMethods.CurWordApp.WordBasic.ViewHeader()
            m_bResetPrompt = True
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub InsertPageNumberInHeadersFooters(ByVal iSection As Short)
            'inserts page number in headers or footers of specified section
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.InsertPageNumberInHeadersFooters"
            Dim oHFs As Word.HeadersFooters
            Dim oHF As Word.HeaderFooter
            Dim bDo As Boolean
            Dim vStyle As Object
            Dim oSec As Word.Section
            Dim xDesc As String
            Dim oNextSec As Word.Section
            Dim xDest As String
            Dim oWordDoc As WordDoc

            With GlobalMethods.CurWordApp.ActiveDocument.Sections
                On Error Resume Next
                oSec = .Item(iSection)
                On Error GoTo ProcError

                '       get next section if it exists
                If iSection < .Count Then
                    oNextSec = .Item(iSection + 1)
                End If
            End With

            If oSec Is Nothing Then
                xDesc = "Section " & iSection & " does not exist in this document."
                Err.Raise(mpErrors.mpError_InvalidMember, , _
                    "<Error_InvalidMember>" & xDesc)
            End If

            oWordDoc = New WordDoc

            '   get headers/footers
            If m_iLocation = mpPageNumberLocations.mpPageNumberLocation_Header Then
                oHFs = oSec.Headers
                If oSec.PageSetup.Orientation = WdOrientation.wdOrientLandscape Then
                    vStyle = "Header Landscape"
                    CheckStyleExists(vStyle)
                Else
                    vStyle = WdBuiltinStyle.wdStyleHeader
                End If

                '       unlink current section headers
                oWordDoc.UnlinkHeaders(oSec)

                If Not (oNextSec Is Nothing) Then
                    '           unlink next section headers
                    oWordDoc.UnlinkHeaders(oNextSec)
                End If

            ElseIf m_iLocation = mpPageNumberLocations.mpPageNumberLocation_Footer Then
                oHFs = oSec.Footers
                If oSec.PageSetup.Orientation = WdOrientation.wdOrientLandscape Then
                    vStyle = "Footer Landscape"
                    CheckStyleExists(vStyle)
                Else
                    vStyle = WdBuiltinStyle.wdStyleFooter
                End If
                '       unlink current section footers
                oWordDoc.UnlinkFooters(oSec)
                If Not (oNextSec Is Nothing) Then
                    '           unlink next section footers
                    oWordDoc.UnlinkFooters(oNextSec)
                End If
            End If

            '   set destination string
            xDest = LCase(GlobalMethods.CurWordApp.ActiveDocument.Styles(vStyle).NameLocal)

            '********* JTS 2/20/02
            Dim xType As String

            '   do primary header/footer
            oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterPrimary)
            xType = "primary"

            '   prompt for deletion of existing
            '   contents if there are any
            bDo = PromptForDeletion(oHF, xType, xDest)

            If bDo Then
                With oHF.PageNumbers
                    ' Set Numberstyle of section so that TOC/TOA entries will
                    ' use the same format displayed on page
                    Select Case m_iNum
                        Case mpPageNumbers.mpPageNumbers_LowerRoman
                            .NumberStyle = WdPageNumberStyle.wdPageNumberStyleLowercaseRoman
                        Case mpPageNumbers.mpPageNumbers_UpperRoman
                            .NumberStyle = WdPageNumberStyle.wdPageNumberStyleUppercaseRoman
                        Case mpPageNumbers.mpPageNumbers_LowerAlpha
                            .NumberStyle = WdPageNumberStyle.wdPageNumberStyleLowercaseLetter
                        Case mpPageNumbers.mpPageNumbers_UpperAlpha
                            .NumberStyle = WdPageNumberStyle.wdPageNumberStyleUppercaseLetter
                        Case Else
                            .NumberStyle = WdPageNumberStyle.wdPageNumberStyleArabic
                    End Select
                    If m_bRestartNumbering Then
                        .RestartNumberingAtSection = True
                        .StartingNumber = m_iRestartAtNumber
                    Else
                        .RestartNumberingAtSection = False
                        .StartingNumber = 1
                    End If
                End With
                '       insert number based on format
                InsertPageNumber(oHF.Range, vStyle, True)
            End If

            ' Also do Even page footers if appropriate
            If oSec.PageSetup.OddAndEvenPagesHeaderFooter Then
                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterEvenPages)
                xType = "even pages"
                '       prompt for deletion of existing
                '       contents if there are any
                bDo = PromptForDeletion(oHF, xType, xDest)
                If bDo Then
                    '               clear header/footer
                    With oHF.Range
                        If Len(.Text) > 1 Then
                            .Text = ""
                            '                        .MoveEnd WdUnits.wdCharacter, -1
                            '                        .Delete
                        End If
                    End With
                    InsertPageNumber(oHF.Range, vStyle, True)
                End If

            End If
            '****************

            '   do first page if needed
            If m_bSetupDiffFirstPage Then
                oSec.PageSetup.DifferentFirstPageHeaderFooter = True
            End If

            If oSec.PageSetup.DifferentFirstPageHeaderFooter Then

                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                '       prompt for deletion of existing
                '       contents if there are any
                bDo = PromptForDeletion(oHF, "first page", xDest)

                If bDo Then
                    If m_bFirstPage Then
                        With oHF.PageNumbers
                            If m_bRestartNumbering Then
                                .RestartNumberingAtSection = True
                                .StartingNumber = m_iRestartAtNumber
                            Else
                                .RestartNumberingAtSection = False
                                .StartingNumber = 1
                            End If
                            '                    .RestartNumberingAtSection = True
                            '                    .StartingNumber = 1
                        End With

                        '               insert page number
                        InsertPageNumber(oHF.Range, vStyle, True)
                    Else
                        '               clear header/footer
                        With oHF.Range
                            If Len(.Text) > 1 Then
                                .Text = ""
                                '                        .MoveEnd WdUnits.wdCharacter, -1
                                '                        .Delete
                            End If
                        End With
                    End If
                End If

            End If

            oWordDoc = Nothing

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub


        'GLOG : 6025 : CEH
        Private Sub RemovePageNumberInHeadersFooters(ByVal iSection As Short)
            'inserts page number in headers or footers of specified section
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.RemovePageNumberInHeadersFooters"
            Dim oHFs As Word.HeadersFooters
            Dim oHF As Word.HeaderFooter
            Dim oSec As Word.Section
            Dim oWordDoc As WordDoc
            Dim xDesc As String

            With GlobalMethods.CurWordApp.ActiveDocument.Sections
                On Error Resume Next
                oSec = .Item(iSection)
                On Error GoTo ProcError

            End With

            If oSec Is Nothing Then
                xDesc = "Section " & iSection & " does not exist in this document."
                Err.Raise(mpErrors.mpError_InvalidMember, , _
                    "<Error_InvalidMember>" & xDesc)
            End If

            If m_bFirstPage Then
                '   remove only from first page
                If m_bSetupDiffFirstPage Then
                    oSec.PageSetup.DifferentFirstPageHeaderFooter = True
                End If

                '   do first page header
                oHFs = oSec.Headers
                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                RemovePageNumber(oHF.Range)

                '   do first page footer
                oHFs = oSec.Footers
                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                RemovePageNumber(oHF.Range)
            Else
                '   do headers
                oHFs = oSec.Headers
                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                RemovePageNumber(oHF.Range)

                '   do first page header if necessary
                If oSec.PageSetup.DifferentFirstPageHeaderFooter Then
                    oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                    RemovePageNumber(oHF.Range)
                End If

                '   do footers
                oHFs = oSec.Footers
                oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                RemovePageNumber(oHF.Range)

                '   do first page footer if necessary
                If oSec.PageSetup.DifferentFirstPageHeaderFooter Then
                    oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                    RemovePageNumber(oHF.Range)
                End If

                '   Also do Even page header/footer if appropriate
                If oSec.PageSetup.OddAndEvenPagesHeaderFooter Then
                    '   do first page header
                    oHFs = oSec.Headers
                    oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterEvenPages)
                    RemovePageNumber(oHF.Range)

                    '   do first page footer
                    oHFs = oSec.Footers
                    oHF = oHFs(WdHeaderFooterIndex.wdHeaderFooterEvenPages)
                    RemovePageNumber(oHF.Range)
                End If

            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Function PromptForDeletion(oHF As Word.HeaderFooter, _
                                           ByVal xType As String, _
                                           ByVal xDestination As String) As Boolean

            'prompts to delete existing content in headers/footers if necessary -
            'sets static var to TRUE to skip subsequent prompts -
            'bReset reset static var to FALSE
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.PromptForDeletion"
            Dim iChoice As Integer
            Dim xMsg As String
            Dim xText As String 'GLOG 7835
            Static bSkip As Boolean
            Static bDo As Boolean

            On Error GoTo ProcError

            'GLOG 7835: If Header/Footer contains only paragraph marks, treat as empty
            xText = BaseMethods.xSubstitute(oHF.Range.Text, vbCr, "")

            If m_bResetPrompt Then
                bSkip = False
            End If

            If bSkip Then
                '       skip prompt - if there is text in HF,
                '       return user choice from previous prompt (bDo),
                '       else return TRUE
                If Len(xText) > 0 Then 'GLOG 7835
                    PromptForDeletion = bDo
                Else
                    PromptForDeletion = True
                End If
                Exit Function
            End If

            If Len(xText) > 0 Then 'GLOG 7835
                '       prompt only if there's text in the range

                xMsg = "The section " & oHF.Range.Sections.First.Index & " " & _
                    xDestination & " is not empty.  The contents " & _
                    "(except for the Trailer/Doc ID) will be deleted if the page number is inserted into this " & _
                    xDestination & "." & vbCr & vbCr & "Do you want to insert page " & _
                    "numbers when a header or footer is not empty?" & _
                    vbCr & vbCr & "Choose No to insert " & _
                    "page numbers only in headers and footers that are empty."

                'GLOG : 6493 : CEH
                GlobalMethods.CurWordApp().ScreenUpdating = True
                iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, My.Application.Info.Title)
                GlobalMethods.CurWordApp.ScreenUpdating = False
                '       bDo is static so that future requests
                '       can use the value stored here
                bDo = (iChoice = vbYes)
                '       set flag to skip prompts in the future
                bSkip = True
                PromptForDeletion = bDo
            Else
                PromptForDeletion = True
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Private Sub InsertPageNumber(oRange As Word.Range, _
                                     Optional ByVal vStyle As Object = "", _
                                     Optional bDoAlignment As Boolean = False)

            'inserts page number at specified location-
            'applies style if specified-
            'applies alignment if specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.InsertPageNumber"
            Dim xFormat As String
            Dim oStyle As Word.Style
            Dim oTS As Word.TabStops
            Dim oPgSet As Word.PageSetup
            Dim sArea As Single
            Dim xMsg As String
            Dim oRangeField As Word.Range
            Dim oNewFld As Word.Field
            Dim xNewText As String
            Dim xSep As String

            On Error GoTo ProcError

            With oRange
                '       delete range if it's not an insertion point
                If Len(.Text) Then
                    If m_iLocation = mpPageNumberLocations.mpPageNumberLocation_Cursor Then
                        .Delete()
                    Else
                        If Len(.Text) > 1 Then
                            .Font.Reset()
                            .Text = ""
                        End If
                    End If
                End If

                '       apply style if specified
                If Len(vStyle) Then
                    On Error Resume Next
                    oStyle = GlobalMethods.CurWordApp.ActiveDocument.Styles(vStyle)
                    On Error GoTo ProcError

                    '           alert that style doesn't exist
                    If (oStyle Is Nothing) Then
                        xMsg = "Could not apply the " & vStyle & " style " & _
                            "to the page number.  The style doesn't exist " & _
                            "in this document."
                        MsgBox(xMsg, vbExclamation, My.Application.Info.Title)
                    Else
                        'apply styles - both specified and character styles
                        'GLOG 8498
                        .Style = oStyle
                    End If

                End If

                '       apply alignment if specified
                If bDoAlignment Then
                    '           set tab stops - center and right
                    'GLOG 8498: Can't compare object to empty string, but can use with Len()
                    If Len(vStyle) Then
                        oTS = GlobalMethods.CurWordApp.ActiveDocument.Styles( _
                            vStyle).ParagraphFormat.TabStops
                    Else
                        oTS = GlobalMethods.CurWordApp.ActiveDocument.Styles( _
                            .Style).ParagraphFormat.TabStops
                    End If

                    oTS.ClearAll()

                    oPgSet = oRange.Sections.First.PageSetup

                    '           get width of area between margins
                    With oPgSet
                        sArea = .PageWidth - (.RightMargin + .LeftMargin + .Gutter)
                    End With
                    oTS.Add(sArea, WdTabAlignment.wdAlignTabRight)
                    oTS.Add(sArea / 2, WdTabAlignment.wdAlignTabCenter)

                    '           insert tabs
                    .InsertAfter(vbTab & vbTab)
                    .Font.Reset()
                    .StartOf()

                    If m_iAlign = mpPageNumberAlignments.mpPageNumberAlignment_Center Then
                        .Move(WdUnits.wdCharacter, 1)
                    ElseIf m_iAlign = mpPageNumberAlignments.mpPageNumberAlignment_Right Then
                        .Move(WdUnits.wdCharacter, 2)
                    End If
                End If

                '       reference start of range
                .StartOf()

                '       GLOG : 6434 : CEH
                '       insert text before if it exists
                If m_xTextBefore <> "" Then
                    Select Case m_iAlign
                        Case mpPageNumberAlignments.mpPageNumberAlignment_Center
                            xNewText = Replace(m_xTextBefore, vbCrLf, Chr(11) & vbTab)
                        Case mpPageNumberAlignments.mpPageNumberAlignment_Right
                            xNewText = Replace(m_xTextBefore, vbCrLf, Chr(11) & vbTab & vbTab)
                        Case Else
                            xNewText = Replace(m_xTextBefore, vbCrLf, Chr(11))
                    End Select
                    .InsertAfter(xNewText)
                    .Collapse(WdCollapseDirection.wdCollapseEnd)
                End If

                '       GLOG : 6434 : CEH
                If m_xTextAfter <> "" Then
                    Select Case m_iAlign
                        Case mpPageNumberAlignments.mpPageNumberAlignment_Center
                            xNewText = Replace(m_xTextAfter, vbCrLf, Chr(11) & vbTab)
                        Case mpPageNumberAlignments.mpPageNumberAlignment_Right
                            xNewText = Replace(m_xTextAfter, vbCrLf, Chr(11) & vbTab & vbTab)
                        Case Else
                            xNewText = Replace(m_xTextAfter, vbCrLf, Chr(11))
                    End Select
                Else
                    'reset var
                    xNewText = ""
                End If

                '       insert text after if it exists
                'GLOG 4820: Character style applied to field also gets applied to following whitespace
                'insert temporary Character to ensure style is limited to page number field only
                'If m_xTextAfter <> "" Then
                .InsertAfter("|" & xNewText)
                .Collapse(WdCollapseDirection.wdCollapseStart)
                'End If

                '       insert number based on format
                Select Case m_iNum
                    '************************ MODIFIED CODE
                    Case mpPageNumbers.mpPageNumbers_CardText
                        ' Need to set in field because this is not
                        ' a native Word format option
                        xFormat = "\* CardText \* FirstCap "
                    Case Else
                        If m_iLocation = mpPageNumberLocations.mpPageNumberLocation_Cursor Then   '"#3915
                            '                    insert number based on format
                            Select Case m_iNum
                                Case mpPageNumbers.mpPageNumbers_Arabic
                                    xFormat = "\* Arabic "
                                Case mpPageNumbers.mpPageNumbers_LowerRoman
                                    xFormat = "\* roman "
                                Case mpPageNumbers.mpPageNumbers_UpperRoman
                                    xFormat = "\* ROMAN "
                                Case mpPageNumbers.mpPageNumbers_CardText
                                    xFormat = "\* CardText \* FirstCap "
                                Case mpPageNumbers.mpPageNumbers_LowerAlpha
                                    xFormat = "\* alphabetic "
                                Case mpPageNumbers.mpPageNumbers_UpperAlpha
                                    xFormat = "\* ALPHABETIC "
                            End Select
                        Else
                            'GLOG 7978: Preserveformatting option adds \* MERGEFORMAT,
                            'so not necessary to specify it separately
                            xFormat = ""
                        End If
                End Select

                '       add page number field
                Dim oFld As Word.Field
                oFld = .Fields.Add(oRange, WdFieldType.wdFieldPage, xFormat, True)
                oFld.Result.Style = WdBuiltinStyle.wdStylePageNumber

                'get view and selection for later return
                Dim iView As WdViewType
                Dim oSelRng As Word.Range

                iView = oRange.Document.ActiveWindow.View.Type
                oSelRng = GlobalMethods.CurWordApp.Selection.Range

                '       check for X of Y format
                'GLOG : 6025 : CEH
                'GLOG : 6435 : ceh
                If m_xXofY <> "none" Then

                    'Extract separator
                    xSep = Replace(m_xXofY, "{PAGE}", "")
                    xSep = Replace(xSep, "{NUMPAGES}", "")
                    xSep = Replace(xSep, "{SECTIONPAGES}", "")

                    oRangeField = oFld.Result
                    With oRangeField
                        .EndOf()
                        .InsertAfter(xSep)
                        .EndOf()
                        Select Case m_iNum
                            Case mpPageNumbers.mpPageNumbers_Arabic
                                xFormat = "\* Arabic "
                            Case mpPageNumbers.mpPageNumbers_LowerRoman
                                xFormat = "\* roman "
                            Case mpPageNumbers.mpPageNumbers_UpperRoman
                                xFormat = "\* ROMAN "
                            Case mpPageNumbers.mpPageNumbers_CardText
                                xFormat = "\* CardText \* FirstCap "
                            Case mpPageNumbers.mpPageNumbers_LowerAlpha
                                xFormat = "\* alphabetic "
                            Case mpPageNumbers.mpPageNumbers_UpperAlpha
                                xFormat = "\* ALPHABETIC "
                        End Select
                        'GLOG : 6435 : ceh
                        'section total or document total
                        If InStr(UCase(m_xXofY), "{SECTIONPAGES}") Then
                            oNewFld = .Fields.Add(oRangeField, WdFieldType.wdFieldSectionPages, xFormat, True)
                        Else
                            oNewFld = .Fields.Add(oRangeField, WdFieldType.wdFieldNumPages, xFormat, True)
                        End If
                    End With
                End If

                'GLOG : 6025 : CEH
                oFld.Select()
                'GLOG item #5574 - dcf - 6/29/11
                GlobalMethods.CurWordApp.Selection.Style = WdBuiltinStyle.wdStylePageNumber  'oRange.Document.Styles(Word.WdBuiltinStyle.wdStylePageNumber).NameLocal
                If Not (oNewFld Is Nothing) Then
                    oNewFld.Select()
                    GlobalMethods.CurWordApp.Selection.Style = WdBuiltinStyle.wdStylePageNumber
                End If

                'GLOG 4820: Delete temporary character following page number
                GlobalMethods.CurWordApp.Selection.Next(WdUnits.wdCharacter, 1).Delete()
                oSelRng.Select()
                GlobalMethods.CurWordApp.Selection.Document.ActiveWindow.View.Type = iView

            End With
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        'GLOG : 6025 : CEH
        Private Sub RemovePageNumber(oRange As Word.Range)
            'removes page number at specified location-
            'leaves preceeding & following tabs in place

            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.RemovePageNumber"
            Dim oField As Word.Field
            Dim oPara As Word.Range
            Dim l As Integer

            On Error GoTo ProcError

            For Each oField In oRange.Fields
                If oField.Type = WdFieldType.wdFieldPage Or _
                oField.Type = WdFieldType.wdFieldNumPages Then
                    'get field range
                    oPara = oField.Result

                    With oPara
                        'GLOG 6762: Also remove tabs before and after number
                        'deal with right side
                        l = .MoveEndUntil(vbTab)
                        If l = 0 Then
                            .MoveEnd(WdUnits.wdParagraph)
                            .MoveEnd(WdUnits.wdCharacter, -1)
                        Else
                            .MoveEndWhile(vbTab)
                        End If

                        'GLOG : 6409 : CEH
                        'deal with left side
                        l = .MoveStartUntil(vbTab, WdConstants.wdBackward)
                        If l = 0 Then
                            .MoveStart(WdUnits.wdParagraph, -1)
                        Else
                            .MoveStartWhile(vbTab, WdConstants.wdBackward)
                        End If

                        'delete range
                        .Delete()

                    End With
                    Exit For
                End If
            Next oField

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub


        '**********************************************************
        '   Internal Procs
        '**********************************************************
        Private Sub CheckStyleExists(ByVal vStyle As Object)
            Const mpThisFunction As String = "LMP.Forte.MSWord.PageNumbering.CheckStyleExists"
            Dim oSty As Word.Style
            On Error Resume Next
            oSty = GlobalMethods.CurWordApp.ActiveDocument.Styles(vStyle)
            If oSty Is Nothing Then
                oSty = GlobalMethods.CurWordApp.ActiveDocument.Styles.Add(vStyle)
                oSty.BaseStyle = WdBuiltinStyle.wdStyleNormal 'GlobalMethods.CurWordApp.ActiveDocument.Styles(Word.wdBuiltInStyle.wdStyleNormal).NameLocal
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
    End Class
End Namespace
