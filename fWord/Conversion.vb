Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord
    Public Class Conversion
        Private m_iIndex As Short
        Private m_bFirstCC As Boolean
        Private Sub DeleteContentControl(ByVal oCC As Word.ContentControl)
            'Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteContentControl"
            Dim oRng As Word.Range
            Dim xTag As String
            Dim oBmks As Word.Bookmarks

            oBmks = oCC.Range.Document.Bookmarks
            oRng = oCC.Range

            xTag = oCC.Tag
            oCC.Delete()

            If Not oBmks.Exists("_" & xTag) Then
                oBmks.Add("_" & xTag, oRng)
            End If
        End Sub
        Public Sub DeleteContentControls(ByVal oDoc As Word.Document)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteContentControls"
            Dim oSec As Section
            Dim oHF As HeaderFooter
            Dim oCC As ContentControl
            Dim i As Short
            Dim oShape As Shape

            For Each oCC In oDoc.ContentControls
                'GLOG 5842 - Skip non-Forte Content Controls
                If BaseMethods.IsForteContentControl(oCC) Then
                    DeleteContentControl(oCC)
                End If
            Next oCC

            For Each oSec In oDoc.Sections
                For Each oHF In oSec.Headers
                    For i = oHF.Range.ContentControls.Count To 1 Step -1
                        'GLOG 5842 - Skip non-Forte Content Controls
                        If BaseMethods.IsForteContentControl(oHF.Range.ContentControls(i)) Then
                            DeleteContentControl(oHF.Range.ContentControls(i))
                        End If
                    Next i

                    For Each oShape In oHF.Shapes
                        'GLOG 5269
                        If BaseMethods.HasTextFrameText(oShape) Then 'GLOG 7009
                            For Each oCC In oShape.TextFrame.TextRange.ContentControls
                                'GLOG 5842 - Skip non-Forte Content Controls
                                If BaseMethods.IsForteContentControl(oCC) Then
                                    DeleteContentControl(oCC)
                                End If
                            Next oCC
                        End If
                    Next oShape
                Next oHF
                For Each oHF In oSec.Footers
                    For i = oHF.Range.ContentControls.Count To 1 Step -1
                        'GLOG 5842 - Skip non-Forte Content Controls
                        If BaseMethods.IsForteContentControl(oHF.Range.ContentControls(i)) Then
                            DeleteContentControl(oHF.Range.ContentControls(i))
                        End If
                    Next i

                    For Each oShape In oHF.Shapes
                        'GLOG 5269
                        If BaseMethods.HasTextFrameText(oShape) Then 'GLOG 7009
                            For Each oCC In oShape.TextFrame.TextRange.ContentControls
                                'GLOG 5842 - Skip non-Forte Content Controls
                                If BaseMethods.IsForteContentControl(oCC) Then
                                    DeleteContentControl(oCC)
                                End If
                            Next oCC
                        End If
                    Next oShape
                Next oHF
            Next oSec
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub DeleteContentControlsInRange(ByVal oRange As Word.Range, ByVal bSegmentControlsOnly As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteContentControlsInRange"
            Dim oSec As Section
            Dim oHF As HeaderFooter
            Dim oCC As ContentControl
            Dim i As Short
            Dim oShape As Word.Shape
            Dim oRng As Word.Range
            Dim xTag As String
            Dim oShapes As Word.Shapes
            Dim oTextRange As Word.Range

            'GLOG 6844 (dm) - this was previously a for each loop, which won't
            'hit everything when you're deleting in process
            For i = oRange.ContentControls.Count To 1 Step -1
                oCC = oRange.ContentControls(i)
                'GLOG 5842 - Skip non-Forte Content Controls
                If BaseMethods.IsForteContentControl(oCC) Then
                    If Not bSegmentControlsOnly Or Left$(oCC.Tag, 3) = "mps" Then
                        oCC.Delete()
                    End If
                End If
            Next i

            'GLOG 6844 (dm) - just noting that the following will hit all textboxes in the
            'target story, including in other section headers/footers - we currently rely on it
            'working this way, but it does make the name of this method a bit of a misnomer
            If oRange.StoryType = wdStoryType.wdMainTextStory Then
                oShapes = oRange.Document.Shapes
            ElseIf oRange.StoryType <> wdStoryType.wdTextFrameStory Then
                oShapes = oRange.Document.Sections(1).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes
            End If

            If Not oShapes Is Nothing Then
                For Each oShape In oShapes
                    If BaseMethods.HasTextFrameText(oShape) Then 'GLOG 7009
                        'GLOG 6844 (dm) - this was previously a for each loop, which won't
                        'hit everything when you're deleting in process
                        oTextRange = oShape.TextFrame.TextRange
                        For i = oTextRange.ContentControls.Count To 1 Step -1
                            oCC = oTextRange.ContentControls(i)
                            'GLOG 5842 - Skip non-Forte Content Controls
                            If BaseMethods.IsForteContentControl(oCC) Then
                                If Not bSegmentControlsOnly Or Left$(oCC.Tag, 3) = "mps" Then
                                    oCC.Delete()
                                End If
                            End If
                        Next i
                    End If
                Next oShape
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub AddContentControlsToBookmarkedDocument(ByVal oDoc As Word.Document, _
                                                          Optional ByVal bInsertNewSegmentBookmark As Boolean = False, _
                                                          Optional ByVal bAsFinished As Boolean = False, _
                                                          Optional ByVal bValidateTopBookmarks As Boolean = False, _
                                                          Optional ByVal bIncludemSEGs As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddContentControlsToBookmarkedDocument"
            Dim oRng As Word.Range
            Dim oHF As Word.HeaderFooter
            Dim oSec As Word.Section
            Dim vHFIndex As Object
            Dim iHFTypes(2) As Short

            oDoc.Application.ScreenUpdating = False

            oRng = oDoc.Range

            AddContentControlsToBookmarkedRange(oRng, bInsertNewSegmentBookmark, _
                bAsFinished, bValidateTopBookmarks, bIncludemSEGs)

            'Convert First Page Headers/Footers first if present
            iHFTypes(0) = 2
            iHFTypes(1) = 1
            iHFTypes(2) = 3
            For Each oSec In oDoc.Sections
                For Each vHFIndex In iHFTypes
                    '2/25/11 (dm) - removed .Exists condition - we insert trailer into the
                    'first page footer regardless of whether different first page is set, so
                    'there may be content there to reconstitute
                    '3/15/11 (dm) - added oSec.Index condition - LinkToPrevious was returning
                    'true in the first section, though interestingly not when running from vb
                    'and not in AddContentControlsToBookmarkedDocument() under the same
                    'circumstances - but I am adding the condition in that method as well
                    oHF = oSec.Headers(vHFIndex)
                    '            If oHF.Exists And Not oHF.LinkToPrevious Then
                    If (oSec.Index = 1) Or Not oHF.LinkToPrevious Then
                        AddContentControlsToBookmarkedRange(oHF.Range, _
                            bInsertNewSegmentBookmark, bAsFinished, bValidateTopBookmarks, _
                            bIncludemSEGs)
                    End If
                    oHF = oSec.Footers(vHFIndex)
                    '            If oHF.Exists And Not oHF.LinkToPrevious Then
                    If (oSec.Index = 1) Or Not oHF.LinkToPrevious Then
                        AddContentControlsToBookmarkedRange(oHF.Range, _
                            bInsertNewSegmentBookmark, bAsFinished, bValidateTopBookmarks, _
                            bIncludemSEGs)
                    End If
                Next vHFIndex
            Next oSec
            oDoc.Application.ScreenUpdating = True

            Exit Sub
ProcError:
            oDoc.Application.ScreenUpdating = True
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
        Friend Sub AddContentControlsToBookmarkedRange(ByVal oRng As Word.Range, _
                                                       ByRef bInsertNewSegmentBookmark As Boolean, _
                                                       Optional ByVal bAsFinished As Boolean = False, _
                                                       Optional ByVal bValidateTopBookmark As Boolean = False, _
                                                       Optional bIncludemSEGs As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddContentControlsToBookmarkedRange"
            On Error GoTo ProcError
            Dim bShowHidden As Boolean
            Dim xBaseName As String
            Dim xTag As String
            Dim xObjectData As String
            Dim oBmkCol As Collection
            Dim oBRange As Word.Range
            Dim iNumBmks As Short
            Dim oCC As Word.ContentControl
            Dim oBmk As Word.Bookmark
            Dim i As Short
            Dim xBMK As String
            Dim xDeletedTags As String
            Dim oFirstSeg As Word.ContentControl
            Dim xTempID As String
            Dim iCount As Short
            Dim iIndex As Short
            Dim xBmks As String
            Dim vBmks As Object
            Dim j As Short
            Dim g As Short
            Dim lStart As Integer
            Dim xTempIDTest As String
            Dim oDoc As Word.Document
            Dim bIsPostInjunction As Boolean
            Dim bDo As Boolean
            Dim oTag As Word.XMLNode
            Dim oTableRange As Word.Range 'GLOG 7706

            oDoc = oRng.Document
            bIsPostInjunction = BaseMethods.IsPostInjunctionWordVersion()

            bShowHidden = oRng.Bookmarks.ShowHidden
            oRng.Bookmarks.ShowHidden = True
            oRng.Bookmarks.DefaultSorting = Word.WdBookmarkSortBy.wdSortByLocation

            iNumBmks = oRng.Bookmarks.Count
            oBmkCol = New Collection

            'get all relevant bookmarks -
            'we do this before conversion to
            'allow us to create additional bookmarks
            'during the conversion process -
            For i = 1 To iNumBmks
                xBmks = ""
                oBmk = oRng.Bookmarks(i)
                xBMK = oBmk.Name

                '10/11/11 (dm) - added option to only reconstitute mSEGs, mDocProps, and mSecProps
                If bAsFinished Then
                    '1/4/13: Don't reconstitute mSEGs
                    bDo = ((xBMK Like "_mps*" And bIncludemSEGs) Or (xBMK Like "_mpp*") Or (xBMK Like "_mpc*"))
                Else
                    bDo = (xBMK Like "_mp*" And Not xBMK Like "_mps*") Or (xBMK Like "_mps*" And bIncludemSEGs)
                End If
                If bDo And oBmk.StoryType = oRng.StoryType And _
                        oBmk.Range.Start >= oRng.Start And oBmk.Range.End <= oRng.End Then
                    'this is a Forte content control related bookmark -
                    '8/30/10 (dm) - limited to bookmarks that are entirely within target range -
                    '11/29/10 (dm) - adjust order of nested adjacent bookmarks if necessary -
                    'index is held in TempID attribute - only nested adjacent bookmarks are indexed
                    xBmks = xBMK & "|" & xBmks
                    If bIsPostInjunction Then
                        'checking indexes is only necessary in post-injunction Word
                        xTag = Mid$(xBMK, 2)
                        xTempID = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)
                        '                If xTempID = "1" Then
                        If IsNumeric(xTempID) Then '4/26/11 (dm)
                            'this is an innermost nested adjacent bookmark - ensure correct order
                            iCount = i
                            lStart = oBmk.Range.Start
                            While (xTempID <> "") And (iCount < iNumBmks)
                                'cycle while indexed and there are remaining bookmarks
                                oBmk = oRng.Bookmarks(iCount + 1)
                                xBMK = oBmk.Name
                                If oBmk.Range.Start <> lStart Then
                                    'no ordering issue - break cycle
                                    xTempID = ""
                                ElseIf xBMK Like "_mp*" And oBmk.StoryType = oRng.StoryType And _
                                        oBmk.Range.Start >= oRng.Start And oBmk.Range.End <= oRng.End Then
                                    iIndex = CInt(xTempID)
                                    xTag = Mid$(xBMK, 2)
                                    xTempIDTest = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)
                                    If IsNumeric(xTempIDTest) Then 'GLOG 6535
                                        '                            If xTempIDTest <> "" Then
                                        If CInt(xTempIDTest) = iIndex + 1 Then
                                            'this bookmark should be before the previous one
                                            'GLOG 7141 (dm) - add mSEGs only if specified
                                            If (Not xBMK Like "_mps*") Or bIncludemSEGs Then _
                                                xBmks = xBMK & "|" & xBmks
                                            iCount = iCount + 1
                                            xTempID = xTempIDTest
                                        ElseIf CInt(xTempIDTest) = iIndex + 2 Then
                                            '4/26/11 (dm) - this to correct for the fact that
                                            'collection table/collection item may be in the
                                            'correct order in an otherwise reversed run of
                                            'nested adjacent bookmarks - this was preventing
                                            'reconstitution of pleadings saved from .doc to .docx
                                            'and of 10.1.2 .docx pleadings - I've opted for a
                                            'narrow fix so as not to cause some new problem
                                            Dim xBMK2 As String
                                            Dim bFound As Boolean
                                            oBmk = oRng.Bookmarks(iCount + 2)
                                            xBMK2 = oBmk.Name
                                            If (oBmk.Range.Start = lStart) And _
                                                    xBMK2 Like "_mp*" And _
                                                    oBmk.StoryType = oRng.StoryType And _
                                                    oBmk.Range.Start >= oRng.Start And _
                                                    oBmk.Range.End <= oRng.End Then
                                                xTag = Mid$(xBMK2, 2)
                                                xTempIDTest = BaseMethods.GetAttributeValue(xTag, _
                                                    "TempID", oDoc)
                                                If IsNumeric(xTempIDTest) Then _
                                                    bFound = (CInt(xTempIDTest) = iIndex + 1)
                                            End If
                                            If bFound Then
                                                'add both bookmarks before the previous one
                                                'GLOG 7141 (dm) - add mSEGs only if specified
                                                If (Not xBMK2 Like "_mps*") Or bIncludemSEGs Then _
                                                    xBmks = xBMK2 & "|" & xBmks
                                                If (Not xBMK Like "_mps*") Or bIncludemSEGs Then _
                                                    xBmks = xBMK & "|" & xBmks
                                                iCount = iCount + 2
                                                xTempID = CStr(iIndex + 2)
                                            Else
                                                'break cycle
                                                xTempID = ""
                                            End If
                                        Else
                                            'no ordering issue - break cycle
                                            xTempID = ""
                                        End If
                                    Else
                                        'this is nested and adjacent at the same level
                                        'GLOG 7141 (dm) - add mSEGs only if specified
                                        If (Not xBMK Like "_mps*") Or bIncludemSEGs Then _
                                            xBmks = xBmks & xBMK & "|"
                                        iCount = iCount + 1
                                    End If
                                Else
                                    'not Forte content or not in target range - just skip
                                    iCount = iCount + 1
                                End If
                            End While
                            i = iCount
                        End If
                    End If

                    'add bookmarks to collection
                    If xBmks <> "" Then
                        xBmks = Left$(xBmks, Len(xBmks) - 1)
                        vBmks = Split(xBmks, "|")
                        For j = 0 To UBound(vBmks)
                            oBmkCol.Add(vBmks(j), vBmks(j))
                        Next j
                    End If
                End If
            Next i

            'GLOG 5866 (dm) - don't reconstitute if top bookmark is inappropriate
            If bValidateTopBookmark And (oBmkCol.Count > 0) Then
                xBMK = oBmkCol(1)
                If Not ((xBMK Like "_mps*") Or (xBMK Like "_mpp*") Or (xBMK Like "_mpc*")) Then _
                        oBmkCol = New Collection
            End If

            'Process bookmarks in reverse order to ensure proper nesting
            For i = oBmkCol.Count To 1 Step -1
                xBMK = oBmkCol(i)
                oBmk = oRng.Bookmarks(xBMK)
                xTag = Mid$(xBMK, 2)
                xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oRng.Document)
                Select Case Left(xTag, 3)
                    Case "mps"
                        xBaseName = "mSEG"
                    Case "mpv"
                        xBaseName = "mVar"
                    Case "mpb"
                        xBaseName = "mBlock"
                    Case "mpu"
                        xBaseName = "mSubVar"
                    Case "mpp"
                        xBaseName = "mDocProps"
                    Case "mpc"
                        xBaseName = "mSecProps"
                    Case "mpd"
                        xBaseName = "mDel"
                    Case Else
                End Select

                'GLOG 5950 (dm) - ensure that this content control doesn't already exist - the
                'pleading paper mSEG was getting mistaken as the target when changing the sidebar
                Dim oTestRng As Word.Range
                Dim oExistingCC As Word.ContentControl
                oTestRng = oBmk.Range
                If oTestRng.Start = 0 Then _
                    oTestRng.MoveStart()
                On Error Resume Next
                oExistingCC = oTestRng.ParentContentControl
                On Error GoTo ProcError
                If Not oExistingCC Is Nothing Then
                    If oExistingCC.Tag = xTag Then
                        'clear base name to skip adding
                        xBaseName = ""
                    End If
                End If

                'GLOG 5576 (dm) - if there's no object data, we shouldn't add the content control
                If (xBaseName <> "") And (xObjectData <> "") Then
                    oBRange = oBmk.Range

                    If Val(oRng.Application.Version) < 15 Then 'GLOG 7134 (dm)
                        '2/17/11 (dm) - in some cases, e.g. when an mSEG starts before a table and
                        'ends inside it, we won't be able to add the content control while the
                        'corresponding xml node is still present
                        'GLOG 6645 (dm, 3/27/13) - this code was previously limited to pre-injunction Word and
                        'mSEGs for performance reasons and because it may cause the parent's bookmark
                        'to get deleted in the case of empty nested tags - it's now applied more broadly
                        'because it was also an issue with the cc table mBlock in O'Melveny's fax
                        'when saving from .doc to .docx in Word 2010
                        bDo = False
                        If xBaseName = "mSEG" Then
                            bDo = True
                        ElseIf oBRange.Characters.Last.Information(Word.WdInformation.wdWithInTable) Then
                            bDo = Not oBRange.Characters.First.Information(Word.WdInformation.wdWithInTable)
                        End If
                        If bDo Then
                            Dim oRngStart As Word.Range
                            Dim xExistingTag As String 'GLOG 6015
                            oRngStart = oBRange.Duplicate
                            oRngStart.StartOf()
                            oTag = oRngStart.XMLParentNode
                            While Not oTag Is Nothing
                                'GLOG 6015 (dm) - correct for 40-character tags
                                xExistingTag = BaseMethods.GetTag(oTag)
                                If Len(xExistingTag) = 40 Then _
                                    xExistingTag = Left$(xExistingTag, 39)
                                If xExistingTag = xTag Then
                                    oTag.Delete()
                                    oTag = Nothing
                                Else
                                    oRngStart.Move()
                                    oTag = oRngStart.XMLParentNode
                                End If
                            End While
                        End If
                    End If

                    If xBaseName = "mSEG" Then
                        xDeletedTags = BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oRng.Document)
                        If xDeletedTags <> "" Then
                            Dim oDel As New DeleteScope
                            'Convert Deleted Scopes XML
                            xDeletedTags = oDel.ConvertToOpenXml(xDeletedTags, oRng.Document)
                            BaseMethods.SetAttributeValue(xTag, "DeletedScopes", xDeletedTags, "", oRng.Document)
                        End If

                        If oBRange.Characters.Last.Tables.Count > 0 Then
                            'Make sure closing mSEG isn't left inside table
                            'if opening mSEG is not in the same table
                            oTableRange = oBRange.Characters.Last.Tables(1).Range 'GLOG 7706 (dm)
                            If oBRange.Characters.First.Tables.Count = 0 Then
                                'GLOG 7706 (dm) - move end only if not already at end of table -
                                'bookmark was expanding to include the next table in the doc
                                If oBRange.End <> oTableRange.End Then _
                                    oBRange.MoveEnd(Word.WdUnits.wdTable)
                                oBRange.MoveEnd(Word.WdUnits.wdCharacter, 1)
                            ElseIf oBRange.Characters.First.Tables(1).Range.Start <> _
                                oBRange.Characters.Last.Tables(1).Range.Start Then
                                'GLOG 7706 (dm) - same as above
                                If oBRange.End <> oTableRange.End Then _
                                    oBRange.MoveEnd(Word.WdUnits.wdTable)
                                oBRange.MoveEnd(Word.WdUnits.wdCharacter, 1)
                            End If
                        ElseIf oBRange.Characters.Last.Text = vbCr Then 'GLOG 8440
                            'Last Character of Bookmark is Paragraph mark - check if there are any
                            'following empty tags that should be enclosed within this mSEG
                            Dim oRngNext As Word.Range
                            Dim ic As Short
                            Dim ix As Short
                            On Error Resume Next
                            oRngNext = oBRange.Next(Word.WdUnits.wdParagraph, 1)
                            On Error GoTo ProcError
                            If Not oRngNext Is Nothing Then
                                With oRngNext
                                    'Current bookmark ends with Paragraph mark and is also followed by an empty paragraph
                                    If .Text = vbCr Then
                                        'Just in case .ContentControls.Count causes an error with no CCs present
                                        On Error Resume Next
                                        ic = .ContentControls.Count
                                        On Error GoTo ProcError
                                        If ic > 0 Then
                                            For ix = 1 To ic
                                                Dim xCCBase As String
                                                xCCBase = BaseMethods.GetBaseName(.ContentControls(ix))
                                                'If Paragraph after bookmark range is empty,
                                                'move closing mSEG past any ContentControls that are not mSEGs or mBlocks
                                                If xCCBase = "mDel" Or xCCBase = "mVar" Or xCCBase = "mSubVar" Then
                                                    oBRange.SetRange(oBRange.Start, .ContentControls(ix).Range.End + 1)
                                                Else
                                                    Exit For
                                                End If
                                            Next ix
                                        Else
                                            'If no ContentControls Just extend range so inserted mSEG will enclose final paragraph mark of current bookmark
                                            'GLOG 6577 (dm, 3/27/13) - the following line was moving the end of
                                            'the letterhead mSEGs two positions instead of one after converting
                                            'a generic letter with a task pane loaded
                                            '                                    oBRange.MoveEnd Word.WdUnits.wdCharacter, 1
                                            oBRange.SetRange(oBRange.Start, oBRange.End + 1)
                                        End If
                                    End If
                                End With
                            End If
                        End If
                        If oBRange.ContentControls.Count > 0 Then
                            'Make sure mSEG is only contained by another mSEG
                            If BaseMethods.GetBaseName(oBRange.ContentControls(1)) <> "mSEG" Then
                                'Move start before any existing content control
                                If oBRange.ContentControls(1).Range.Start <= oBRange.Start Then
                                    oBRange.SetRange(oBRange.ContentControls(1).Range.Start - 1, oBRange.End)
                                End If
                                'move end after any existing content Control
                                If oBRange.ContentControls(oBRange.ContentControls.Count).Range.End >= oBRange.End Then
                                    oBRange.SetRange(oBRange.Start, oBRange.ContentControls(oBRange.ContentControls.Count).Range.End + 1)
                                End If
                            End If
                        End If

                        'GLOG 6015 (dm) - if the range includes a column with vertically merged
                        'cells, it may not span the row, causing an untrappable "rich text controls
                        'cannot be inserted around table columns" error or an abridged content
                        'control - there's always been code in AddTagsToBookmarkedRange() to
                        'expand collection table items to row, so do the same here
                        If BaseMethods.IsCollectionTableItem(xObjectData) Then
                            If UCase(BaseMethods.GetSegmentPropertyValue(xObjectData, _
                                    "AllowSideBySide")) <> "TRUE" Then
                                oBRange.Expand(Word.WdUnits.wdRow)
                            End If
                        End If
                    End If

                    If oBRange.Information(Word.WdInformation.wdWithInTable) Then
                        'GLOG 7141 (dm) - the fix for GLOG 7088 didn't account for vertically merged cells
                        'and, thus, errs when converting segments such as NY captions to .docx -
                        'since adding a cc to multiple cells which are less than a full row will always
                        'produce an untrappable error below, I'm now expanding unconditionally here -
                        'this relies on the assumption that Expand Word.wdUnits.wdRow will never expand to another row
                        If oBRange.Cells.Count > 1 Then
                            oBRange.Expand(Word.WdUnits.wdRow)
                        End If
                        '                If oBRange.Rows.Count > 1 Then
                        '                    'ensure that entire rows are selected -
                        '                    'content controls cannot be inserted around
                        '                    'individual columns
                        '                    oBRange.Expand Word.wdUnits.wdRow
                        '                Else
                        '                    'GLOG 7088 (dm) - a single row may also not be fully encompassed
                        '                    Dim iCells As short
                        '                    iCells = oBRange.Cells.Count
                        '                    If iCells > 1 Then
                        '                        If iCells < oBRange.Rows(1).Cells.Count Then
                        '                            oBRange.Expand Word.wdUnits.wdRow
                        '                        End If
                        '                    End If
                        '                End If
                    End If

                    '4/25/11 (dm) - expand range if it ends before end of child cc
                    'GLOG 6646 (dm, 3/27/13) - "rich text controls cannot be applied" error
                    'was occurring in an O'Melveny Hong Kong letter sample - mDel range
                    'should never be extended
                    If xBaseName <> "mDel" Then
                        Dim oRngEnd As Word.Range
                        Dim oParentCC As Word.ContentControl
                        oRngEnd = oBRange.Duplicate
                        oRngEnd.EndOf()
                        oParentCC = oRngEnd.ParentContentControl
                        If Not oParentCC Is Nothing Then
                            If (oParentCC.Range.Start > oBRange.Start) And _
                                    (oParentCC.Range.End >= oBRange.End) Then
                                oBRange.End = oParentCC.Range.End + 1
                            End If
                        End If
                    End If

                    'GLOG 7017/7019 (dm) - expand range if story contains nothing but a graphic -
                    'a "rich text controls cannot be applied" error was ocurring with Squire's
                    'content when adding ccs in SetupDistributedSegmentSections action
                    If xBaseName = "mSEG" And oBRange.StoryType = wdStoryType.wdTextFrameStory And _
                            oBRange.Text = "/" Then
                        If oRng.Text = "/" & vbCr Then _
                            oBRange.WholeStory()
                    End If

                    oCC = oBRange.ContentControls.Add(Word.WdContentControlType.wdContentControlRichText)
                    oCC.SetPlaceholderText(, , "")
                    oCC.Tag = xTag
                    oCC.LockContentControl = False

                    'ensure that bookmarks are situated within
                    'bounds of content controls
                    'JTS 7/12/10: Setting Bookmark Start and End will not be valid if bookmark is applied to table
                    oCC.Range.Bookmarks.Add(oBmk.Name)
                    '            oBmk.Start = oCC.Range.Start
                    '            oBmk.End = oCC.Range.End
                    If xBaseName = "mSEG" Then
                        If BaseMethods.IsCollectionTableItem(xObjectData) Then
                            'Ensure proper nesting of Collection Items
                            If oCC.Range.ContentControls.Count > 1 Then
                                Dim xChildObjectData As String
                                xChildObjectData = BaseMethods.GetAttributeValue(oCC.Range.ContentControls(2).Tag, "ObjectData")
                                If BaseMethods.IsCollectionTable(xChildObjectData) Then
                                    'Move Collection Table Tag so that it encloses Table Item
                                    BaseMethods.MoveContentControl(oCC.Range.ContentControls(2), oCC.Range.Tables(1).Range)
                                    'Reapply bookmark so it appears after CollectionTable in Location order
                                    oCC.Range.Bookmarks.Add(xBMK)
                                End If
                            End If
                        ElseIf BaseMethods.IsCollectionTable(xObjectData) Then
                            'Ensure proper nesting of Collection Items
                            Dim xChildObjectData As String
                            If oCC.Range.ContentControls.Count > 1 Then
                                xChildObjectData = BaseMethods.GetAttributeValue(oCC.Range.ContentControls(2).Tag, "ObjectData")
                                If BaseMethods.IsCollectionTableItem(xChildObjectData) Then
                                    Dim ocWordDoc As New WordDoc
                                    'Reapply bookmark so it appears after CollectionTable in Location order
                                    oCC.Range.Bookmarks.Add(xBMK)
                                End If
                            End If
                        End If
                        If oCC.Range.Characters.First.Tables.Count > 0 Then
                            'JTS 7/12/10: If ContentControls are nested at the start of a Table, and internal CCs encompass
                            'a full row, the start of bookmark range will be the same for both parent and child CCs.
                            'We need to reapply the bookmarks to contained tags so that they will appear in the correct
                            'order when sorting by location
                            Dim oTestCC As Word.ContentControl
                            Dim c As Short
                            lStart = oCC.Range.Bookmarks(xBMK).Start
                            For c = 2 To oCC.Range.ContentControls.Count 'oCC will be first item included in the count
                                Dim xTestTag As String
                                oTestCC = oCC.Range.ContentControls(c)
                                xTestTag = oTestCC.Tag
                                If xTestTag = "" Then
                                    Exit For
                                ElseIf Not oTestCC.Range.Bookmarks.Exists("_" & xTestTag) Then
                                    Exit For
                                ElseIf oTestCC.Range.Bookmarks("_" & xTestTag).Start = lStart Then
                                    'Bookmarks with identical starting positions appear in order created
                                    oTestCC.Range.Bookmarks.Add("_" & xTestTag)
                                Else
                                    Exit For
                                End If
                            Next c
                        End If
                    End If
                    If xBaseName = "mSEG" And bInsertNewSegmentBookmark = True Then
                        'First mSEG will be last processed
                        oFirstSeg = oCC
                    End If
                End If
            Next i
            'Mark first mSEG with mpNewSegment bookmark if specified
            If bInsertNewSegmentBookmark And Not oFirstSeg Is Nothing Then
                Dim oWordDoc As WordDoc
                oWordDoc = New WordDoc
                oWordDoc.InsertNewSegmentBookmark_CC(oRng.Document, oFirstSeg)
                bInsertNewSegmentBookmark = False
            End If
            oRng.Bookmarks.ShowHidden = bShowHidden
            'Also convert Textboxes in range
            If oRng.StoryType <> wdStoryType.wdTextFrameStory Then
                Dim oShp As Word.Shape
                Dim oItem As Object 'Word.Shape
                For Each oShp In oRng.ShapeRange
                    'GLOG 8424: Conditions were reversed here
                    If oShp.Type = Microsoft.Office.Core.MsoShapeType.msoGroup Then
                        'For Each oItem In oShp.GroupItems
                        For g = 0 To oShp.GroupItems.Count - 1
                            oItem = oShp.GroupItems(g)
                            If BaseMethods.HasTextFrameText(oItem) Then 'GLOG 7009
                                AddContentControlsToBookmarkedRange(oItem.TextFrame.TextRange, _
                                    bInsertNewSegmentBookmark, bAsFinished, bValidateTopBookmark, _
                                    bIncludemSEGs)
                            End If
                        Next g ' oItem
                    ElseIf BaseMethods.HasTextFrameText(oShp) Then 'GLOG 7009
                        AddContentControlsToBookmarkedRange(oShp.TextFrame.TextRange, _
                            bInsertNewSegmentBookmark, bAsFinished, bValidateTopBookmark, _
                            bIncludemSEGs)
                    End If
                Next oShp
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Sub ConvertToContentControls(oDoc As Word.Document)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.ConvertToContentControls"
            Dim oSec As Word.Section
            Dim oStoryRng As Word.Range
            Dim oNodeRng As Word.Range
            Dim oNode As Word.XMLNode
            Dim oTagID As Word.XMLNode
            Dim oCC As ContentControl
            Dim xName As String
            Dim iPos As Short
            Dim iViewType As WdViewType

            On Error GoTo ProcError

            GlobalMethods.CurWordApp.ScreenUpdating = False
            oDoc.ActiveWindow.View.ShowFieldCodes = False
            iViewType = oDoc.ActiveWindow.View.Type

            m_iIndex = 0
            m_bFirstCC = True

            For Each oStoryRng In oDoc.StoryRanges
                ConvertTagsToContentControls(oStoryRng)
            Next oStoryRng

            For Each oSec In oDoc.Sections
                ConvertTagsInShapeRangeToContentControls(oSec.Headers(Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages).Range.ShapeRange)
                ConvertTagsInShapeRangeToContentControls(oSec.Headers(Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range.ShapeRange)
                ConvertTagsInShapeRangeToContentControls(oSec.Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.ShapeRange)
                ConvertTagsInShapeRangeToContentControls(oSec.Footers(Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages).Range.ShapeRange)
                ConvertTagsInShapeRangeToContentControls(oSec.Footers(Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range.ShapeRange)
                ConvertTagsInShapeRangeToContentControls(oSec.Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary).Range.ShapeRange)
            Next oSec

            oDoc.ActiveWindow.View.ShowFieldCodes = True
            oDoc.ActiveWindow.View.ShowFieldCodes = False
            oDoc.ActiveWindow.View.Type = iViewType
            oDoc.Application.ScreenUpdating = True

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Private Sub ConvertTagsToContentControls(oRng As Word.Range)
            For Each oNode In oRng.XMLNodes
                ConvertTagToContentControl(oNode)
            Next oNode
        End Sub
        Private Sub ConvertTagsInShapeRangeToContentControls(oShapeRng As Word.ShapeRange)
            Dim i As Short
            Dim oRng As Word.Range

            For i = 1 To oShapeRng.Count
                If BaseMethods.HasTextFrameText(oShapeRng) Then 'GLOG 7009
                    oRng = oShapeRng(i).TextFrame.TextRange 'GLOG 6669
                    If Not oRng Is Nothing Then
                        ConvertTagsToContentControls(oRng)
                    End If
                End If
            Next i
        End Sub
        Private Sub ConvertTagToContentControl(oNode As Word.XMLNode)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.ConvertTagToContentControl"
            Dim oChildNode As Word.XMLNode
            Dim oControlRng As Word.Range
            Dim oNodeRng As Word.Range
            Dim oCC As ContentControl
            Dim i As Short
            Dim xAttributes As String = ""
            Dim xTag As String = ""
            Dim xObjectDBID As String = ""
            Dim xDocVarID As String = ""
            Dim xElement As String = ""
            Dim oDoc As Word.Document
            Dim xDocVar As String = ""
            Dim xMPOVar As String = ""
            Dim xMPDVar As String = ""
            Dim xAttribute As String = ""
            Dim oDeletedScopes As Word.XMLNode
            Dim oCOM As Application
            Dim xTagID As String = ""
            Dim oTestRng As Word.Range
            Dim xDeletedScopes As String = ""

            oCOM = New Application

            For Each oChildNode In oNode.SelectNodes("child::*")
                ConvertTagToContentControl(oChildNode)
            Next oChildNode

            oNodeRng = oNode.Range.Duplicate
            oDoc = oNodeRng.Document

            'if the closing tag is immediately after a table, the range
            'needs to be expanded for the content control to also encompass
            'the end of the table
            If oNodeRng.Characters.Last.Tables.Count = 1 Then
                On Error Resume Next
                oTestRng = oNodeRng.Characters.Last.Next(Word.WdUnits.wdCharacter)
                On Error GoTo 0
                If Not oTestRng Is Nothing Then
                    If oTestRng.Tables.Count = 0 Then
                        oNodeRng.MoveEnd(Word.WdUnits.wdParagraph)
                    End If
                End If
            End If

            'Application.ScreenUpdating = True

            'construct new tag id
            xElement = oNode.BaseName
            Select Case xElement
                Case "mSEG"
                    xTag = "mps"
                Case "mVar"
                    xTag = "mpv"
                Case "mBlock"
                    xTag = "mpb"
                Case "mDel"
                    xTag = "mpd"
                Case "mSubVar"
                    xTag = "mpu"
                Case "mDocProps"
                    xTag = "mpp"
                Case "mSecProps"
                    xTag = "mpc"
                Case Else
                    Exit Sub
            End Select

            'doc var id
            xDocVarID = GenerateDocVarID()
            xTag = xTag & xDocVarID

            'object db id
            If xElement = "mSEG" Then
                xObjectDBID = GetmSEGObjectDataValue(oNode, "SegmentID")
                If (Len(xObjectDBID) < 19) Then
                    xObjectDBID = BaseMethods.CreatePadString(19 - Len(xObjectDBID), "0") & xObjectDBID
                End If
            Else
                'NOTE: m_iIndex is a dummy value since existing variables and blocks
                'do not yet contain an ObjectDatabaseID property
                If (xElement = "mVar") Or (xElement = "mBlock") Or _
                        (xElement = "mDel") Then
                    m_iIndex = m_iIndex + 1
                    xObjectDBID = CStr(m_iIndex)
                End If

                If (Len(xObjectDBID) < 6) Then
                    xObjectDBID = BaseMethods.CreatePadString(6 - Len(xObjectDBID), "0") & xObjectDBID
                End If
            End If
            xTag = xTag & xObjectDBID

            'bookmark names can only contain alphanumeric characters
            xTag = Replace(xTag, "-", "m")
            xTag = Replace(xTag, ".", "d")

            'pad with trailing zeroes
            xTag = xTag & BaseMethods.CreatePadString(40 - Len(xTag), "0")

            'get mpo doc var name
            xMPOVar = "mpo" & xDocVarID

            'test for conflicts
            On Error Resume Next
            xDocVar = oDoc.Variables(xMPOVar).Name
            On Error GoTo 0
            While oDoc.Bookmarks.Exists(xTag) Or (xDocVar <> "")
                'regenerate doc var id
                xDocVarID = GenerateDocVarID()
                xTag = Left$(xTag, 3) & xDocVarID & Mid$(xTag, 12)
                xMPOVar = Left$(xMPOVar, 3) & xDocVarID
                xDocVar = ""
                On Error Resume Next
                xDocVar = oDoc.Variables(xMPOVar).Name
                On Error GoTo 0
            End While

            'create mpo document variable
            If xElement = "mSubVar" Then
                xTagID = oNode.SelectSingleNode("@Name").NodeValue
            Else
                Dim oTagIDAttr As XMLNode

                oTagIDAttr = oNode.SelectSingleNode("@TagID")

                If Not oTagIDAttr Is Nothing Then
                    xTagID = oTagIDAttr.NodeValue
                End If
            End If
            xTagID = xTagID & "��"

            'the rest of the value is encrypted
            With oNode.Attributes
                For i = 0 To .Count - 1
                    xAttribute = .Item(i).BaseName
                    If (xAttribute <> "TagID") And (xAttribute <> "Name") And _
                            (xAttribute <> "DeletedScopes") Then
                        xAttributes = xAttributes & xAttribute & "=" & _
                            oCOM.Decrypt(.Item(i).NodeValue) & "��"
                    End If
                Next i
            End With

            'strip trailing separator
            If xAttributes <> "" Then _
                xAttributes = Left$(xAttributes, Len(xAttributes) - 2)

            'encrypt
            If (xElement <> "mDel") And (xElement <> "mSubVar") Then _
                xAttributes = oCOM.Encrypt(xAttributes)

            'add variable
            oDoc.Variables.Add(xMPOVar, xTagID & xAttributes)

            'create mpd variable
            If xElement = "mSEG" Then
                oDeletedScopes = oNode.SelectSingleNode("@DeletedScopes")
                If Not oDeletedScopes Is Nothing Then
                    xDeletedScopes = oCOM.ConvertDeletedScopesToOpenXml( _
                        oDeletedScopes.NodeValue, oDoc)
                    xMPDVar = "mpd" & xDocVarID & "01"
                    oDoc.Variables.Add(xMPDVar, xDeletedScopes)
                End If
            End If

            'delete tag
            oNode.Delete()

            'create content control
            oNodeRng.Select()

            If GlobalMethods.CurWordApp.Selection.Information(Word.WdInformation.wdWithInTable) Then
                If GlobalMethods.CurWordApp.Selection.Range.Rows.Count > 1 Then
                    'ensure that entire rows are selected -
                    'content controls cannot be inserted around
                    'individual columns
                    oNodeRng.Expand(Word.WdUnits.wdRow)
                End If
            End If

            If m_bFirstCC Then
                'add dummy cc, then delete -
                'this overcomes a bug whereby the first cc
                'ignores the specified placeholder text if
                'the cc does not contain text
                oCC = oNodeRng.ContentControls.Add(Word.WdContentControlType.wdContentControlText)
                oCC.SetPlaceholderText(, , "")
                oCC.Delete()
                m_bFirstCC = False
            End If
            oCC = oNodeRng.ContentControls.Add(Word.WdContentControlType.wdContentControlRichText)

            oCC.SetPlaceholderText(, , "")
            oCC.Tag = xTag
            oCC.LockContentControl = False

            'create bookmark
            oCC.Range.Bookmarks.Add("_" & xTag)
        End Sub

        Private Function GetmSEGObjectDataValue(ByVal oTag As Word.XMLNode, _
                                               ByVal xProperty As String) As String
            'gets the value of the specified segment property
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.GetmSEGObjectDataValue"
            Dim oObjectData As Word.XMLNode
            Dim xObjectData As String
            Dim iPos1 As Integer
            Dim iPos2 As Integer
            Dim xValue As String
            Dim oCOM As Application

            On Error GoTo ProcError

            oCOM = New Application

            oObjectData = oTag.SelectSingleNode("@ObjectData")
            If oObjectData Is Nothing Then _
                Exit Function

            xObjectData = oCOM.Decrypt(oObjectData.NodeValue)
            iPos1 = InStr(xObjectData, xProperty & "=")
            If iPos1 > 0 Then
                'property found - parse from object data
                iPos1 = iPos1 + Len(xProperty) + 1

                'get delimiting pipe - it may or may not exist
                iPos2 = InStr(iPos1, xObjectData, "|")

                If iPos2 > 0 Then
                    'pipe exists - parse to pipe
                    xValue = Mid$(xObjectData, iPos1, iPos2 - iPos1)
                Else
                    'pipe doesn't exist - parse to end of string
                    xValue = Mid$(xObjectData, iPos1)
                End If
            End If

            GetmSEGObjectDataValue = xValue

            Exit Function
ProcError:
            Exit Function
        End Function

        Private Function GenerateDocVarID() As String
            Dim xDocVarID As String
            Randomize()
            xDocVarID = CStr(CLng(100000000 * Rnd()))
            xDocVarID = BaseMethods.CreatePadString("0", 8 - Len(xDocVarID)) & xDocVarID
            GenerateDocVarID = xDocVarID
        End Function

        Public Sub RefreshTags()
            Dim oTags As Tags
            Dim oCOM As WordDoc
            Dim iRemovedTagType As mpInvalidTagsRemovedTypes

            oCOM = New WordDoc
            oTags = oCOM.RefreshTags(GlobalMethods.CurWordApp.ActiveDocument, True, "", iRemovedTagType)
        End Sub

        Public Sub AddTagsToBookmarkedDocument(ByVal oDoc As Word.Document, _
                                               Optional ByVal bInsertNewSegmentBookmark As Boolean = False, _
                                               Optional ByVal bDeleteContentControls As Boolean = False, _
                                               Optional ByVal bAsFinished As Boolean = False, _
                                               Optional ByVal bValidateTopBookmarks As Boolean = False, _
                                               Optional ByVal bIncludemSEGs As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddTagsToBookmarkedDocument"
            Dim oRng As Word.Range
            Dim oHF As Word.HeaderFooter
            Dim oSec As Word.Section
            Dim vHFIndex As Object
            Dim iHFTypes(2) As Short
            Dim bValidate As Boolean
            Dim oSelRng As Word.Range
            Dim lScroll As Integer
            Dim oSchema As Word.XMLSchemaReference

            'GLOG 5490 (dm) - add schema if necessary
            On Error Resume Next
            oSchema = oDoc.XMLSchemaReferences(GlobalMethods.mpNamespace)
            If oSchema Is Nothing Then
                oSchema = oDoc.XMLSchemaReferences.Add(GlobalMethods.mpNamespace)
                If oSchema Is Nothing Then _
                    Exit Sub
            End If
            On Error GoTo ProcError

            oDoc.Application.ScreenUpdating = False
            oSelRng = oDoc.Application.Selection.Range
            lScroll = oDoc.ActiveWindow.VerticalPercentScrolled

            GlobalMethods.SetXMLMarkupState(oDoc, True)

            'JTS 6/17/10: Turn off Schema validation so that tags can be
            'inserted in reverse order of location.  Required Parent tag
            'may not be present for all
            bValidate = oDoc.XMLSchemaReferences.AutomaticValidation
            'GLOG 5490: This command will work, but generate an error if
            'Attached Template doesnt' exist in the path specified
            On Error Resume Next
            oDoc.XMLSchemaReferences.AutomaticValidation = False
            On Error GoTo ProcError
            oRng = oDoc.Range

            AddTagsToBookmarkedRange(oRng, bInsertNewSegmentBookmark, _
                bDeleteContentControls, bAsFinished, bValidateTopBookmarks, bIncludemSEGs)

            'Convert First Page Headers/Footers first if present
            iHFTypes(0) = 2
            iHFTypes(1) = 1
            iHFTypes(2) = 3
            For Each oSec In oDoc.Sections
                For Each vHFIndex In iHFTypes
                    '2/25/11 (dm) - removed .Exists condition - we insert trailer into the
                    'first page footer regardless of whether different first page is set, so
                    'there may be content there to reconstitute
                    '3/15/11 (dm) - added oSec.Index condition - LinkToPrevious was returning
                    'true in the first section, though interestingly not when running from vb
                    'and not in AddContentControlsToBookmarkedDocument() under the same
                    'circumstances - but I am adding the condition in that method as well
                    oHF = oSec.Headers(vHFIndex)
                    '            If oHF.Exists And Not oHF.LinkToPrevious Then
                    If (oSec.Index = 1) Or Not oHF.LinkToPrevious Then
                        AddTagsToBookmarkedRange(oHF.Range, bInsertNewSegmentBookmark, _
                            bDeleteContentControls, bAsFinished, bValidateTopBookmarks, _
                            bIncludemSEGs)
                    End If
                    oHF = oSec.Footers(vHFIndex)
                    '            If oHF.Exists And Not oHF.LinkToPrevious Then
                    If (oSec.Index = 1) Or Not oHF.LinkToPrevious Then
                        AddTagsToBookmarkedRange(oHF.Range, bInsertNewSegmentBookmark, _
                            bDeleteContentControls, bAsFinished, bValidateTopBookmarks, _
                            bIncludemSEGs)
                    End If
                Next vHFIndex
            Next oSec

            'GLOG 5490: This command will work, but generate an error if
            'Attached Template doesnt' exist in the path specified
            On Error Resume Next
            oDoc.XMLSchemaReferences.AutomaticValidation = bValidate
            On Error GoTo ProcError

            oSelRng.Select()
            oDoc.ActiveWindow.VerticalPercentScrolled = lScroll
            GlobalMethods.SetXMLMarkupState(oDoc, False)
            oDoc.Application.ScreenUpdating = True
            Exit Sub
ProcError:
            oDoc.Application.ScreenUpdating = True
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
        Friend Sub AddTagsToBookmarkedRange(ByVal oRng As Word.Range, _
                                            Optional ByRef bInsertNewSegmentBookmark As Boolean = False, _
                                            Optional ByVal bDeleteContentControls As Boolean = False, _
                                            Optional ByVal bAsFinished As Boolean = False, _
                                            Optional ByVal bValidateTopBookmark As Boolean = False, _
                                            Optional ByVal bIncludemSEGs As Boolean = True)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddTagsToBookmarkedRange"
            Dim bShowHidden As Boolean
            Dim oBmk As Word.Bookmark
            Dim xTag As String
            Dim xTagID As String
            Dim xObjectData As String
            Dim xDeletedScopes As String
            Dim xAuthors As String
            Dim xPart As String
            Dim xName As String
            Dim xBaseName As String
            Dim oNode As Word.XMLNode
            Dim oBRange As Word.Range
            Dim xBookmarksToDelete As String
            Dim xBookmarkArray() As Boolean
            Dim i As Short
            Dim xBMK As String
            Dim oBmkCol As Collection
            Dim iNumBmks As Short
            Dim oFirstSeg As Word.XMLNode
            Dim oTags As Word.XMLNode
            Dim oDoc As Word.Document
            Dim xTempID As String
            Dim iCount As Short
            Dim iIndex As Short
            Dim xBmks As String
            Dim vBmks As Object
            Dim j As Short
            Dim lStart As Integer
            Dim vBookmarksToDelete As Object
            Dim xTempIDTest As String
            Dim bShowXML As Boolean
            Dim oTestBmk As Word.Bookmark
            Dim oTestRange As Word.Range
            Dim bEmptyNested As Boolean
            Dim oTableRange As Word.Range
            Dim bDo As Boolean
            Dim vProps As Object 'GLOG 6959 (dm)
            Dim xBkmks(0, 1) As String 'GLOG 6959 (dm)
            Dim k As Short 'GLOG 7536 (dm)
            Dim lPos As Integer 'GLOG 7536 (dm)
            Dim bCollectionAtStart As Boolean 'GLOG 7444 (dm)

            oDoc = oRng.Document

            '1/5/11 (dm) - I restored the bInsertNewSegmentBookmark parameter in place of
            'the following code due to a strange bug whereby the new segment bookmark is stripped
            'by Word 2010 along with the tags upon insertion of Word-Generated Avery 5164 labels -
            'it's strange because this doesn't happen with pre-printed 5164 (or any other segment)
            'and the bookmark looks identical in the xml - perhaps it's related to the graphic
            '    'determine whether this is a newly inserted segment -
            '    'we can't just check oRng due to the placement of the new
            '    'segment bookmark in some of the collection segments
            '    If oDoc.Bookmarks.Exists("mpNewSegment") Then
            '        bInsertNewSegmentBookmark = _
            '            (oDoc.Bookmarks("mpNewSegment").StoryType = oRng.StoryType)
            '    End If

            bShowHidden = oRng.Bookmarks.ShowHidden
            oRng.Bookmarks.ShowHidden = True
            oRng.Bookmarks.DefaultSorting = Word.WdBookmarkSortBy.wdSortByLocation

            bShowXML = oDoc.ActiveWindow.View.ShowXMLMarkup
            If Not bShowXML Then _
                GlobalMethods.SetXMLMarkupState(oRng.Document, True)

            iNumBmks = oRng.Bookmarks.Count
            oBmkCol = New Collection

            'get all relevant bookmarks -
            'we do this before conversion to
            'allow us to create additional bookmarks
            'during the conversion process -
            For i = 1 To iNumBmks
                bCollectionAtStart = False 'GLOG 7444 (dm)
                xBmks = ""
                oBmk = oRng.Bookmarks(i)
                xBMK = oBmk.Name

                '10/11/11 (dm) - added option to only reconstitute mSEGs, mDocProps, and mSecProps
                If bAsFinished Then
                    '1/4/13: Don't add mSEGs
                    bDo = ((xBMK Like "_mps*" And bIncludemSEGs) Or (xBMK Like "_mpp*") Or (xBMK Like "_mpc*"))
                Else
                    bDo = (xBMK Like "_mp*" And Not xBMK Like "_mps*") Or ((xBMK Like "_mps*") And bIncludemSEGs)

                    'GLOG 7444 (dm) - this is a very specific exception to ensure that the pleading counsels
                    'collection gets tagged when containing segments are updated on design save - it was
                    'being excluded by the conditional below because its bookmark starts before the
                    'start of the target range, which is set to exclude the parent pleading
                    If (xBMK Like "_mps*") And bIncludemSEGs Then
                        If (oBmk.Range.Start = 0) And (oRng.Start = 1) Then
                            xObjectData = BaseMethods.GetAttributeValue(Mid$(xBMK, 2), "ObjectData", oDoc)
                            bCollectionAtStart = BaseMethods.IsCollectionTable(xObjectData)
                        End If
                    End If
                End If

                If bDo And (oBmk.StoryType = oRng.StoryType) And _
                        ((oBmk.Range.Start >= oRng.Start) Or bCollectionAtStart) And _
                        (oBmk.Range.End <= oRng.End) Then
                    'this is a Forte content control related bookmark -
                    '8/30/10 (dm) - limited to bookmarks that are entirely within target range -
                    '11/29/10 (dm) - adjust order of nested adjacent bookmarks if necessary -
                    'index is held in TempID attribute - only nested adjacent bookmarks are indexed
                    'GLOG 6959 (dm) - rewrote the following block to no longer assume that indices
                    'will be encountered sequentially - this will especially be true after temporarily
                    'restoring segment bounding objects before saving segments or during other procesess
                    xTag = Mid$(xBMK, 2)
                    xTempID = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)

                    If IsNumeric(xTempID) Then
                        xBmks = xBMK & "�" & xTempID
                        iCount = i
                        lStart = oBmk.Range.Start
                        While (xTempID <> "") And (iCount < iNumBmks)
                            'cycle while indexed and there are remaining bookmarks
                            oBmk = oRng.Bookmarks(iCount + 1)
                            xBMK = oBmk.Name
                            If bAsFinished Then
                                '1/4/13: Don't add mSEGs
                                bDo = ((xBMK Like "_mps*" And bIncludemSEGs) Or (xBMK Like "_mpp*") Or (xBMK Like "_mpc*"))
                            Else
                                bDo = (xBMK Like "_mp*" And Not xBMK Like "_mps*") Or ((xBMK Like "_mps*") And bIncludemSEGs)
                            End If
                            If oBmk.Range.Start <> lStart Then
                                'no ordering issue - break cycle
                                xTempID = ""
                            ElseIf bDo And oBmk.StoryType = oRng.StoryType And _
                                    oBmk.Range.Start >= oRng.Start And oBmk.Range.End <= oRng.End Then
                                iIndex = CInt(xTempID)
                                xTag = Mid$(xBMK, 2)
                                xTempIDTest = BaseMethods.GetAttributeValue(xTag, "TempID", oDoc)
                                If IsNumeric(xTempIDTest) Then 'GLOG 6535
                                    'GLOG 7141 (dm) - add mSEGs only if specified
                                    If (Not xBMK Like "_mps*") Or bIncludemSEGs Then _
                                        xBmks = xBmks & "|" & xBMK & "�" & xTempIDTest
                                    xTempID = xTempIDTest
                                Else
                                    'break cycle
                                    '                            xTempID = ""
                                    'GLOG 7536 (dm) - adjacent, at same level - don't break cycle -
                                    'the next bookmark not be at same level - add with new separator
                                    lPos = InStrRev(xBmks, "�")
                                    xBmks = Left$(xBmks, lPos - 1) & "�" & xBMK & Mid$(xBmks, lPos)
                                End If
                                iCount = iCount + 1 'GLOG 7536 (dm)
                            Else
                                'not Forte content or not in target range - just skip
                                iCount = iCount + 1
                            End If
                        End While
                        i = iCount

                        'sort bookmarks
                        vBmks = Split(xBmks, "|")
                        ReDim xBkmks(UBound(vBmks), 1)
                        For j = 0 To UBound(vBmks)
                            vProps = Split(vBmks(j), "�")
                            xBkmks(j, 0) = vProps(0)
                            xBkmks(j, 1) = vProps(1)
                        Next j
                        GlobalMethods.CurWordApp.WordBasic.SortArray(xBkmks, 1, 0, UBound(vBmks), 0, 1)

                        'add bookmarks to collection
                        For j = 0 To UBound(vBmks)
                            'GLOG 7536 (dm) - split up adjacent, same-level bookmarks
                            vProps = Split(xBkmks(j, 0), "�")
                            For k = 0 To UBound(vProps)
                                oBmkCol.Add(vProps(k))
                            Next k
                        Next j
                    Else
                        'add bookmark to collection
                        oBmkCol.Add(xBMK, xBMK)
                    End If
                End If
            Next i

            Dim bTagged As Boolean
            Dim bTagsAlreadyInRange As Boolean

            bTagsAlreadyInRange = (oRng.XMLNodes.Count > 0)

            'GLOG 5866 (dm) - don't reconstitute if top bookmark is inappropriate
            If bValidateTopBookmark And (oBmkCol.Count > 0) Then
                xBMK = oBmkCol(1)
                If Not ((xBMK Like "_mps*") Or (xBMK Like "_mpp*") Or (xBMK Like "_mpc*")) Then _
                        oBmkCol = New Collection
            End If

            'Process bookmarks in reverse order to ensure proper nesting
            For i = oBmkCol.Count To 1 Step -1
                xBMK = oBmkCol(i)
                oBmk = oRng.Bookmarks(xBMK)
                oBRange = oBmk.Range

                If bTagsAlreadyInRange Then
                    Dim oParentTag As XMLNode

                    oParentTag = Nothing
                    bTagged = False

                    On Error Resume Next
                    oParentTag = oBRange.XMLParentNode
                    On Error GoTo ProcError

                    'GLOG 5950 (dm) - the pleading paper mSEG bookmark encompasses
                    'the start tag - this was resulting in it getting double tagged and
                    'mistaken as the target segment when inserting a sidebar
                    If (oParentTag Is Nothing) And (oBRange.Start = 0) Then
                        lStart = oBRange.Start 'GLOG 6815 (dm)
                        oBRange.MoveStart()
                        On Error Resume Next
                        oParentTag = oBRange.XMLParentNode
                        On Error GoTo ProcError

                        'GLOG 6815 (dm) - restore range if parent isn't an mSEG - this
                        'may be the case when temporarily retagging mSEGs during a procedure
                        If oParentTag Is Nothing Then
                            oBRange.SetRange(lStart, oBRange.End)
                        ElseIf oParentTag.BaseName <> "mSEG" Then
                            oBRange.SetRange(lStart, oBRange.End)
                        End If
                    End If

                    If Not oParentTag Is Nothing Then
                        Dim xReserved As String

                        On Error Resume Next
                        xReserved = oParentTag.SelectSingleNode("@Reserved").Text
                        On Error GoTo ProcError

                        'GLOG 6481 (dm) - correct for 40-character tags
                        If Len(xReserved) = 40 Then _
                            xReserved = Left$(xReserved, 39)

                        If xReserved = Mid$(xBMK, 2) Then
                            'bookmarked range is already tagged
                            bTagged = True
                        ElseIf oBRange.Information(Word.WdInformation.wdWithInTable) Then
                            'we're in a table - bookmarks will
                            'encapsulate tags if the bookmark
                            'spans table cells - check for tag
                            'in table
                            For Each oTags In oBRange.XMLNodes
                                xReserved = oTags.SelectSingleNode("@Reserved").Text

                                'GLOG 6481 (dm) - correct for 40-character tags
                                If Len(xReserved) = 40 Then _
                                    xReserved = Left$(xReserved, 39)

                                If xReserved = Mid$(xBMK, 2) Then
                                    'bookmarked range is already tagged
                                    bTagged = True
                                    Exit For
                                End If
                            Next oTags
                        End If
                    End If
                Else
                    bTagged = False
                End If

                If Not bTagged Then
                    xBookmarksToDelete = xBookmarksToDelete & oBmk.Name & "|"
                    xDeletedScopes = ""
                    xPart = ""
                    xAuthors = ""
                    xTagID = ""
                    xName = ""
                    xTag = Mid$(oBmk.Name, 2)
                    xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oRng.Document)
                    Select Case Left(xTag, 3)
                        Case "mps"
                            xBaseName = "mSEG"
                        Case "mpv"
                            xBaseName = "mVar"
                        Case "mpb"
                            xBaseName = "mBlock"
                        Case "mpu"
                            xBaseName = "mSubVar"
                        Case "mpp"
                            xBaseName = "mDocProps"
                        Case "mpc"
                            xBaseName = "mSecProps"
                        Case "mpd"
                            xBaseName = "mDel"
                        Case Else

                    End Select

                    'Debug.Print xBMK & ": " & Left$(oBmk.Range.Text, 500)

                    'GLOG 5576 (dm) - if there's no object data, we shouldn't add the tag
                    If (xBaseName <> "") And (xObjectData <> "") Then
                        If xBaseName = "mSubVar" Then
                            xName = BaseMethods.GetAttributeValue(xTag, "Name", oRng.Document)
                        ElseIf xBaseName = "mVar" Or xBaseName = "mSEG" Or xBaseName = "mBlock" Or xBaseName = "mDel" Then
                            xTagID = BaseMethods.GetAttributeValue(xTag, "TagID", oRng.Document)
                        End If
                        If xBaseName = "mSEG" Then
                            xPart = BaseMethods.GetAttributeValue(xTag, "PartNumber", oRng.Document)
                            xAuthors = BaseMethods.GetAttributeValue(xTag, "Authors", oRng.Document)
                            xDeletedScopes = BaseMethods.GetAttributeValue(xTag, "DeletedScopes", oRng.Document)
                        End If

                        If xBaseName = "mSEG" Then
                            '1/19/11 (dm) - if corresponding content control is still present,
                            'it prevents proper placement of end tag in table segments -
                            'limit to mSEGs because this may cause the parent's bookmark
                            'to get deleted in the case of empty nested tags
                            Dim oCC As Word.ContentControl
                            Dim oRngStart As Word.Range
                            oRngStart = oBRange.Duplicate
                            oRngStart.StartOf()
                            oCC = oRngStart.ParentContentControl
                            While Not oCC Is Nothing
                                If oCC.Tag = xTag Then
                                    oCC.Delete()
                                    oCC = Nothing
                                Else
                                    oRngStart.Move()
                                    oCC = oRngStart.ParentContentControl
                                End If
                            End While

                            If oBRange.Characters.Last.Tables.Count > 0 Then
                                'Make sure closing mSEG isn't left inside table
                                'if opening mSEG is not in the same table
                                oTableRange = oBRange.Characters.Last.Tables(1).Range
                                If oBRange.Characters.First.Tables.Count = 0 Then
                                    '2/1/11 (dm) - move end only if not already at end of table -
                                    'bookmark was expanding to include the next table in the doc
                                    If oBRange.End <> oTableRange.End Then _
                                        oBRange.MoveEnd(Word.WdUnits.wdTable)
                                    oBRange.MoveEnd(Word.WdUnits.wdCharacter, 1)
                                ElseIf oBRange.Characters.First.Tables(1).Range.Start <> _
                                        oTableRange.Start Then
                                    'GLOG 7706 (dm) - added conditional in this block as well
                                    If oBRange.End <> oTableRange.End Then _
                                        oBRange.MoveEnd(Word.WdUnits.wdTable)
                                    oBRange.MoveEnd(Word.WdUnits.wdCharacter, 1)
                                End If
                            ElseIf oBRange.Characters.Last.Text = vbCr Then 'GLOG 8440
                                'Last Character of Bookmark is Paragraph mark - check if there are any
                                'following empty tags that should be enclosed within this mSEG
                                Dim oRngNext As Word.Range
                                Dim ic As Short
                                Dim ix As Short
                                On Error Resume Next
                                oRngNext = oBRange.Next(Word.WdUnits.wdParagraph, 1)
                                On Error GoTo ProcError
                                If Not oRngNext Is Nothing Then
                                    With oRngNext
                                        'Current bookmark ends with Paragraph mark and is also followed by an empty paragraph
                                        If .Text = vbCr Then
                                            'Just in case .XMLNodes.Count causes an error with no XML Tags present
                                            On Error Resume Next
                                            ic = .XMLNodes.Count
                                            On Error GoTo ProcError
                                            If ic > 0 Then
                                                For ix = 1 To ic
                                                    'If Paragraph after bookmark range is empty,
                                                    'move closing mSEG past any XMLTags that are not mSEGs or mBlocks
                                                    If .XMLNodes(ix).BaseName = "mDel" Or .XMLNodes(ix).BaseName = "mVar" Or .XMLNodes(ix).BaseName = "mSubVar" Then
                                                        oBRange.SetRange(oBRange.Start, .XMLNodes(ix).Range.End + 1)
                                                    Else
                                                        Exit For
                                                    End If
                                                Next ix
                                            Else
                                                'If no XML Tags Just extend range so inserted mSEG will enclose final paragraph mark of current bookmark
                                                oBRange.MoveEnd(Word.WdUnits.wdCharacter, 1)
                                            End If
                                        End If
                                    End With
                                End If
                            End If

                            '1/20/11 (dm) - ensure that collection tags span full row where appropriate
                            If BaseMethods.IsCollectionTable(xObjectData) Then
                                oBRange.Expand(Word.WdUnits.wdTable)
                            ElseIf BaseMethods.IsCollectionTableItem(xObjectData) Then
                                If UCase(BaseMethods.GetSegmentPropertyValue(xObjectData, _
                                        "AllowSideBySide")) <> "TRUE" Then
                                    oBRange.Expand(Word.WdUnits.wdRow)
                                End If
                            End If
                        End If

                        '12/8/10 (dm) - even with indexing, I determined that the following block is still necessary
                        'under some circumstances, e.g. when inserting a standalone collection table item with an
                        'empty mVar at the start of the table
                        'check insertion range -
                        'if range only partiallly encloses a child node, correct -
                        'this happens if the end of a child node is the same
                        'as the end of the parent - adding a child tag expands the range,
                        'but does not affect the range of the parent - hence,
                        'the parent no longer encloses the child tag fully
                        Dim oRngEnd As Range
                        Dim oParentBNode As XMLNode

                        oRngEnd = oBRange.Duplicate
                        oRngEnd.EndOf()
                        oParentBNode = oRngEnd.XMLParentNode

                        If Not oParentBNode Is Nothing Then
                            If (oParentBNode.Range.Start > oBRange.Start) And _
                                    (oParentBNode.BaseName = "mSEG") Then
                                'an mVar range starts before its containing mSEG - this happens
                                'because Word automatically scrambles bookmark start tag order
                                'when those tags start at the same place - this corrects
                                'some of the issue
                                'GLOG 6314 (dm) - the updating of containing pleadings was erring
                                'after saving modifications to counsel design because the collection
                                'was getting tagged around the pleading mSEG - mSEGS were previously
                                'excluded from the adjustment to the start range below - I added a
                                'narrow exception for collection mSEGs, where the parent is an existing
                                'mSEG that's not a collection table item
                                Dim xParentObjectData As String
                                xParentObjectData = BaseMethods.Decrypt(oParentBNode.SelectSingleNode( _
                                    "@ObjectData").NodeValue)
                                'GLOG 7444 (dm) - removed the Count = 1 condition - this is no
                                'longer appropriate with persisting mSEG bookmarks
                                '                        If (xBaseName <> "mSEG") Or ((oBmkCol.Count = 1) And _
                                '                                mpBase.IsCollectionTable(xObjectData) And _
                                '                                Not mpBase.IsCollectionTableItem(xParentObjectData)) Then
                                If (xBaseName <> "mSEG") Or (BaseMethods.IsCollectionTable(xObjectData) And _
                                        Not BaseMethods.IsCollectionTableItem(xParentObjectData)) Then
                                    oBRange.Start = oParentBNode.Range.Start
                                End If
                            End If

                            If oParentBNode.Range.Start > oBRange.Start And _
                                oParentBNode.Range.End >= oBRange.End Then
                                'the child node (ie. oParentBNode) has an end
                                'that exceeds the end of the parent range
                                oBRange.End = oParentBNode.Range.End + 1
                            ElseIf oParentBNode.Range.Start < oBRange.Start And xBaseName = "mSEG" Then
                                'JTS 7/12/10: If we're inserting an mSEG, it should be the top level Node at this point
                                'This would generally be an issue if both mSEG and parent mSEG are nested inside the same
                                'table and the child mSEG had its bookmark redefined to the table range on a previous pass -
                                'DCF 9/14/10: This is no longer needed, now that we dole out bookmark IDs in descending order -
                                'when bookmarks are written this way, they naturally order correctly,
                                'rendering repositioning superfluous
                                'oBRange.Start = oParentBNode.Range.Start - 1
                            End If
                        End If

                        'GLOG 6959 (dm) - avoid xml error by ensuring that zero-length bookmarks
                        'are inside any block level tags
                        lStart = oBRange.Start
                        If (lStart = oBRange.End) And (lStart = oBRange.Paragraphs(1).Range.Start) Then
                            lStart = BaseMethods.GetTagSafeParagraphStart(oBRange)
                            oBRange.SetRange(lStart, lStart)
                        End If

                        oNode = Nothing

                        On Error Resume Next
                        oNode = oBRange.XMLNodes.Add(xBaseName, GlobalMethods.mpNamespace)
                        On Error GoTo ProcError

                        If oNode Is Nothing Then
                            '1/20/11 - added to deal with runtime conversion of Nixon's letterhead
                            If (xBaseName = "mSEG") And (oBRange.StoryType = wdStoryType.wdTextFrameStory) Then
                                oBRange.WholeStory()
                            ElseIf oBRange.Paragraphs.Count > 1 Then
                                'GLOG 5490 (dm) - if bookmark is multiple paragraphs, expand to
                                'include entirety of paragraphs - a paragraph-level tag cannot
                                'encompass only part of a paragraph
                                oBRange.Expand(Word.WdUnits.wdParagraph)
                            ElseIf Len(oBRange.Paragraphs(1).Range.Text) = 2 And _
                                    (oBRange.Paragraphs(1).Range.InlineShapes.Count > 0) Then
                                '12/16/11 (dm) for Fulbright's logo
                                oBRange.Expand(Word.WdUnits.wdParagraph)
                            End If

                            'try again - some inexplicable bug -
                            'node insertion requires two attempts
                            'for some ranges
                            oNode = oBRange.XMLNodes.Add(xBaseName, GlobalMethods.mpNamespace)
                        End If

                        'redefine bookmark range
                        oNode.Range.Bookmarks.Add(xBMK)

                        '1/31/11 (dm) - when a segment with nothing but empty nested tags in the
                        'body, e.g. an envelope, is inserted at the end of an existing document, all
                        'bookmarks after the first one need to be expanded to preserve nesting
                        If bInsertNewSegmentBookmark And (i = oBmkCol.Count) And (i <> 1) Then
                            lStart = oBRange.Start
                            If lStart > 0 Then
                                oTestBmk = oRng.Bookmarks(oBmkCol(1))
                                If (oTestBmk.Start = lStart) And (oTestBmk.End = lStart) Then
                                    For j = i - 1 To 1 Step -1
                                        oTestBmk = oRng.Bookmarks(oBmkCol(j))
                                        oTestRange = oTestBmk.Range
                                        oTestRange.MoveEnd(Word.WdUnits.wdCharacter, 2)
                                        oTestRange.Bookmarks.Add(oTestBmk.Name)
                                    Next j
                                    bEmptyNested = True
                                End If
                            End If
                        End If

                        '11/23/10 (dm) - ensure that mDel stays inside parent mSEG
                        If (xBaseName = "mDel") And Not bEmptyNested Then
                            oTestRange = oBRange.Duplicate
                            oTestRange.Move(Word.WdUnits.wdCharacter, -1)
                            For Each oTestBmk In oTestRange.Bookmarks
                                If Left$(oTestBmk.Name, 4) = "_mps" Then
                                    If oTestBmk.Range.End = oBRange.End Then
                                        'mDel is at end of mSEG - expand mSEG bookmark
                                        oTestRange = oTestBmk.Range
                                        oTestRange.MoveEnd(Word.WdUnits.wdCharacter, 2)
                                        oTestRange.Bookmarks.Add(oTestBmk.Name)
                                        Exit For
                                    End If
                                End If
                            Next oTestBmk
                        End If

                        With oNode.Attributes
                            If xName <> "" Then _
                                .Add("Name", "").NodeValue = xName
                            If xTagID <> "" Then _
                                .Add("TagID", "").NodeValue = xTagID
                            If xObjectData <> "" Then _
                                .Add("ObjectData", "").NodeValue = xObjectData
                            If xPart <> "" Then _
                                .Add("PartNumber", "").NodeValue = xPart
                            If xBaseName = "mSEG" Then _
                                .Add("DeletedScopes", "").NodeValue = xDeletedScopes
                            If xAuthors <> "" Then _
                                .Add("Authors", "").NodeValue = xAuthors

                            'add tag to reserve value
                            .Add("Reserved", "").NodeValue = xTag
                        End With

                        '12/3/10 (dm) - removing the following block resulted in an error when switching
                        'collection table items - leave it for now
                        If xBaseName = "mSEG" Then
                            If BaseMethods.IsCollectionTableItem(xObjectData) Then
                                'Ensure proper nesting of Collection Items
                                If oNode.ChildNodes.Count > 0 Then
                                    Dim xChildObjectData As String
                                    xChildObjectData = BaseMethods.Decrypt(oNode.ChildNodes(1).SelectSingleNode("@ObjectData").NodeValue)
                                    If BaseMethods.IsCollectionTable(xChildObjectData) Then
                                        'Move Collection Table Tag so that it encloses Table Item
                                        BaseMethods.MoveWordTag(oNode.ChildNodes(1), oNode.Range.Tables(1).Range)
                                        'Reapply bookmark so it appears after CollectionTable in Location order
                                        oNode.Range.Bookmarks.Add(xBMK)
                                    End If

                                End If
                            ElseIf BaseMethods.IsCollectionTable(xObjectData) Then
                                'Ensure proper nesting of Collection Items
                                If oNode.ChildNodes.Count > 0 Then
                                    Dim xChildObjectData As String
                                    xChildObjectData = BaseMethods.Decrypt(oNode.ChildNodes(1).SelectSingleNode("@ObjectData").NodeValue)
                                    If BaseMethods.IsCollectionTableItem(xChildObjectData) Then
                                        Dim ocWordDoc As WordDoc
                                        ocWordDoc = New WordDoc

                                        'Reapply bookmark so it appears after CollectionTable in Location order
                                        ocWordDoc.ApplyBookmarkToNode(oNode.ChildNodes(1))
                                    End If
                                End If
                            End If
                            If oNode.Range.Characters.First.Tables.Count > 0 Then
                                'JTS 7/12/10: If Tags are nested at the start of a Table, and internal tags encompass
                                'a full row, the start of bookmark range will be the same for both parent and child tag.
                                'We need to reapply the bookmark to contained tags so that it will appear in the correct
                                'order when sorting by location
                                Dim oTestNode As Word.XMLNode
                                lStart = oNode.Range.Bookmarks(xBMK).Start
                                oTestNode = oNode
                                Do While oTestNode.ChildNodes.Count > 0
                                    Dim xTestTag As String
                                    oTestNode = oTestNode.ChildNodes(1)
                                    On Error Resume Next
                                    xTestTag = oTestNode.SelectSingleNode("@Reserved").NodeValue
                                    On Error GoTo ProcError
                                    If xTestTag = "" Then
                                        Exit Do
                                        '                            ElseIf oTestNode.Range.Bookmarks("_" & xTestTag).Start = lStart Then
                                    ElseIf oTestNode.Range.Start = lStart Then
                                        'Bookmarks with identical starting positions appear in order created
                                        '1/13/11 (dm) - modified the conditional because child bookmark is not
                                        'always there when tags are inside content controls, i.e. when going
                                        'from open xml data to binary doc
                                        Dim ocWordDoc As WordDoc
                                        ocWordDoc = New WordDoc
                                        ocWordDoc.ApplyBookmarkToNode(oTestNode)
                                    Else
                                        Exit Do
                                    End If
                                Loop
                            End If
                        End If

                        'First mSEG will be last one inserted
                        If xBaseName = "mSEG" And bInsertNewSegmentBookmark = True Then
                            oFirstSeg = oNode
                        End If
                    End If
                End If
            Next i

            '    For i = 1 To oBmkCol.Count
            '        oRng.Document.Bookmarks(oBmkCol(i)).Range.Bookmarks.Add oBmkCol(i)
            '    Next i

            '    'delete mp content bookmarks (12/7/10 dm)
            '    If xBookmarksToDelete <> "" Then
            '        xBookmarksToDelete = Left$(xBookmarksToDelete, Len(xBookmarksToDelete) - 1)
            '        vBookmarksToDelete = Split(xBookmarksToDelete, "|")
            '        For i = 0 To UBound(vBookmarksToDelete)
            '            If oRng.Document.Bookmarks.Exists(vBookmarksToDelete(i)) Then _
            '                oRng.Document.Bookmarks(vBookmarksToDelete(i)).Delete
            '        Next i
            '    End If

            'Mark first mSEG with mpNewSegment bookmark if specified
            If Not oFirstSeg Is Nothing Then
                Dim oWordDoc As WordDoc
                oWordDoc = New WordDoc
                oWordDoc.InsertNewSegmentBookmark(oRng.Document, oNode)
                bInsertNewSegmentBookmark = False
            End If
            'Also convert Textboxes in range
            If oRng.StoryType <> wdStoryType.wdTextFrameStory Then
                Dim oShp As Word.Shape
                Dim oItem As Word.Shape
                Dim c As Short
                Dim d As Short
                Dim bNewSegBookmark As Boolean

                'GLOG : 7371 : CEH
                'switched to 'for loop' to insure we only mark
                'last shape in ShapeRange with New Segment bookmark.
                For d = 1 To oRng.ShapeRange.Count
                    oShp = oRng.ShapeRange(d)
                    bNewSegBookmark = IIf(oRng.ShapeRange.Count = d, bInsertNewSegmentBookmark, False)
                    'GLOG 8424: Conditions were reversed here
                    If oShp.Type = Microsoft.Office.Core.MsoShapeType.msoGroup Then
                        'group - check individual items
                        'GLOG 5842 - Using For Each causes Type Mismatch error with some Word 2010 shapes
                        For c = 1 To oShp.GroupItems.Count
                            oItem = oShp.GroupItems(c)
                            If BaseMethods.HasTextFrameText(oItem) Then 'GLOG 7009
                                AddTagsToBookmarkedRange(oItem.TextFrame.TextRange, _
                                    bNewSegBookmark, bDeleteContentControls, _
                                    bAsFinished, bValidateTopBookmark, bIncludemSEGs)
                            End If
                        Next c
                    ElseIf BaseMethods.HasTextFrameText(oShp) Then 'GLOG 7009
                        AddTagsToBookmarkedRange(oShp.TextFrame.TextRange, _
                            bNewSegBookmark, bDeleteContentControls, _
                            bAsFinished, bValidateTopBookmark, bIncludemSEGs)
                    End If
                Next d
            End If

            '9/28/11 (dm) - delete content controls if specified - they're not
            'deleted automatically on a local save as .doc with Worksite 8.5 SP2
            If bDeleteContentControls Then
                Dim oTargetCC As Word.ContentControl
                For Each oTargetCC In oRng.ContentControls
                    oTargetCC.Delete()
                Next oTargetCC
            End If

            oRng.Document.Bookmarks.ShowHidden = bShowHidden
            If Not bShowXML Then _
                GlobalMethods.SetXMLMarkupState(oRng.Document, False)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub AddTagsToBookmarkedRangeIfNecessary(ByVal oRng As Word.Range, _
                                                       Optional ByRef bInsertNewSegmentBookmark As Boolean = False, _
                                                       Optional ByRef bIncludemSEGs As Boolean = False)
            'GLOG 7120 (dm) - added optional bIncludemSEGs parameter - as used currently,
            'this method should never reconsitute mSEGs
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddTagsToBookmarkedRangeIfNecessary"
            On Error GoTo ProcError
            If oRng.XMLNodes.Count = 0 Then
                AddTagsToBookmarkedRange(oRng, bInsertNewSegmentBookmark, , , , bIncludemSEGs)
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub AddContentControlsToBookmarkedRangeIfNecessary(ByVal oRng As Word.Range, _
                                                                  Optional ByRef bInsertNewSegmentBookmark As Boolean = False, _
                                                                  Optional ByRef bIncludemSEGs As Boolean = False)
            'GLOG 7120 (dm) - added optional bIncludemSEGs parameter
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddContentControlsToBookmarkedRangeIfNecessary"
            On Error GoTo ProcError
            If oRng.ContentControls.Count = 0 Then
                AddContentControlsToBookmarkedRange(oRng, bInsertNewSegmentBookmark, , , bIncludemSEGs)
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub DeleteMPContentBookmarks(ByVal oRange As Word.Range, _
            Optional ByVal bSkipOrphaned As Boolean = False, Optional bSkipmSEGs As Boolean = False)
            'deletes all Forte content bookmarks in oRange
            'GLOG 7070 (dm) - added bSkipmSEGs parameter
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteMPContentBookmarks"
            Dim oBmk As Word.Bookmark
            Dim oDoc As Word.Document
            Dim bShowHidden As Boolean
            Dim i As Short
            Dim bContentControls As Boolean
            Dim oNode As Word.XMLNode
            Dim oNodes As Word.XMLNodes
            Dim oDocXML As Xml.XmlDocument
            Dim xBMK As String 'GLOG 7070 (dm)

            On Error GoTo ProcError
            oDoc = oRange.Document
            bContentControls = BaseMethods.FileFormat(oDoc) = mpFileFormats.OpenXml
            bShowHidden = oRange.Bookmarks.ShowHidden
            oRange.Bookmarks.ShowHidden = True

            oDocXML = New Xml.XmlDocument
            If Not bContentControls Then
                oNodes = oRange.XMLNodes
            End If

            For Each oBmk In oRange.Bookmarks
                'GLOG 7070 (dm) - skip mSEGS if specified
                xBMK = oBmk.Name
                If (xBMK Like "_mp*" And Not xBMK Like "_mps*") Or ((xBMK Like "_mps*") And _
                        Not bSkipmSEGs) And (oBmk.StoryType = oRange.StoryType) Then
                    Dim bDo As Boolean
                    bDo = True
                    If bSkipOrphaned And Not bContentControls Then
                        'GLOG 6961: Default bDo to False so that all Bookmarks will be treated as orphaned if there are no XMLNodes
                        bDo = False
                        For i = 1 To oNodes.Count
                            oNode = oNodes(i).SelectSingleNode("../descendant::x:*[@Reserved='" & Mid(oBmk.Name, 2) & "']", _
                                "xmlns:x='" & GlobalMethods.mpNamespace & "'")
                            If oNode Is Nothing Then
                                bDo = False
                            Else
                                bDo = True
                                Exit For
                            End If
                        Next i
                    End If
                    If bDo Then
                        oBmk.Delete()
                    End If
                End If
            Next oBmk

            oRange.Bookmarks.ShowHidden = bShowHidden

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub IndexNestedAdjacentChildren(ByVal oTag As Word.XMLNode, _
                                               Optional ByRef iLastIndex As Short = 0)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.IndexNestedAdjacentChildren"
            Dim oChildTag As Word.XMLNode
            Dim i As Short
            Dim lStart As Integer
            Dim iCount As Short
            Dim oReserved As Word.XMLNode
            Dim bNestedAdjacent As Boolean

            On Error GoTo ProcError

            iCount = oTag.ChildNodes.Count
            For i = iCount To 1 Step -1
                oChildTag = oTag.ChildNodes(i)

                'GLOG 6954 (dm) - preconvert if necessary
                If BaseMethods.GetTag(oChildTag) = "" Then
                    PreconvertXMLTag(oChildTag)
                End If

                oReserved = oChildTag.SelectSingleNode("@Reserved")
                If Not oReserved Is Nothing Then
                    'index children
                    IndexNestedAdjacentChildren(oChildTag, iLastIndex)

                    'determine whether this tag is a nested adjacent child
                    If i = 1 Then
                        bNestedAdjacent = (oChildTag.Range.Start = oTag.Range.Start + i)
                    End If

                    If (iLastIndex > 0) Or bNestedAdjacent Then
                        'there's a nested adjacent relationship - add index
                        iLastIndex = iLastIndex + 1
                        BaseMethods.SetAttributeValue(oReserved.NodeValue, _
                            "TempID", CStr(iLastIndex), "")

                        'reset index
                        If Not bNestedAdjacent Then _
                            iLastIndex = 0
                    Else
                        'clear out TempID
                        BaseMethods.SetAttributeValue(oReserved.NodeValue, "TempID", "", "")
                    End If
                End If
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub IndexNestedAdjacentChildren_CC(ByVal oCC As Word.ContentControl, _
                                                  Optional ByRef iLastIndex As Short = 0)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.IndexNestedAdjacentChildren_CC"
            Dim oChildCC As Word.ContentControl
            Dim i As Short
            Dim lStart As Integer
            Dim iCount As Short
            Dim bNestedAdjacent As Boolean
            Dim oParentCC As Word.ContentControl

            On Error GoTo ProcError

            iCount = oCC.Range.ContentControls.Count
            For i = iCount To 1 Step -1
                oChildCC = oCC.Range.ContentControls(i)

                '7/11/11 (dm) - added conditional to prevent infinite loop in the
                'event that a content control is copy/pasted inside itself - Morgan
                'Lewis sent a pleading in which this occurred in the footer
                If oChildCC.Tag <> oCC.Tag Then
                    'limit to immediate children
                    oParentCC = oChildCC.ParentContentControl
                    If Not oParentCC Is Nothing Then
                        If oParentCC.Tag = oCC.Tag Then
                            'index children
                            IndexNestedAdjacentChildren_CC(oChildCC, iLastIndex)

                            'determine whether this tag is a nested adjacent child
                            bNestedAdjacent = (oChildCC.Range.Start = oCC.Range.Start + 1)

                            If (iLastIndex > 0) Or bNestedAdjacent Then
                                'there's a nested adjacent relationship - add index
                                iLastIndex = iLastIndex + 1
                                BaseMethods.SetAttributeValue(oChildCC.Tag, _
                                    "TempID", CStr(iLastIndex), "")

                                'reset index
                                If Not bNestedAdjacent Then _
                                    iLastIndex = 0
                            Else
                                'clear out TempID
                                BaseMethods.SetAttributeValue(oChildCC.Tag, "TempID", "", "")
                            End If
                        End If
                    End If
                End If
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub AddBookmarksToContentControls(ByVal oRange As Word.Range)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddBookmarksToContentControls"
            Dim oCC As ContentControl
            Dim oShape As Word.Shape
            Dim xTag As String
            Dim oShapes As Word.Shapes

            On Error GoTo ProcError

            For Each oCC In oRange.ContentControls
                'GLOG 5842 - Don't bookmark non-Forte Content Controls
                If BaseMethods.IsForteContentControl(oCC) Then
                    oCC.Range.Bookmarks.Add("_" & oCC.Tag)
                End If
            Next oCC

            If oRange.StoryType = wdStoryType.wdMainTextStory Then
                oShapes = oRange.Document.Shapes
            ElseIf oRange.StoryType <> wdStoryType.wdTextFrameStory Then
                oShapes = oRange.Document.Sections(1).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes
            End If

            For Each oShape In oShapes
                If BaseMethods.HasTextFrameText(oShape) Then 'GLOG 7009
                    For Each oCC In oShape.TextFrame.TextRange.ContentControls
                        'GLOG 5842 - Don't bookmark non-Forte Content Controls
                        If BaseMethods.IsForteContentControl(oCC) Then
                            oCC.Range.Bookmarks.Add("_" & oCC.Tag)
                        End If
                    Next oCC
                End If
            Next oShape

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Sub AddBookmarks(ByVal oDoc As Word.Document, Optional ByVal bXMLTagsOnly As Boolean = False)
            '3/22/11 (dm) - added bXMLTagsOnly parameter to allow for runtime bookmarking/preconversion
            'of 10.1.2 docx files in preinjunction Word 2007
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddBookmarks"
            Dim i As Short
            Dim j As Short
            Dim g As Short
            Dim oShape As Word.Shape
            Dim oGroupItem As Word.Shape
            Dim bEcho As Boolean
            Dim oWordApp As WordApp
            Dim oTextFrame As Word.TextFrame 'JTS 5/6/11 - GLOG 5495
            Dim lScrolled As Integer
            Dim oRngSelect As Word.Range
            Dim lLeft As Integer
            Dim lTop As Integer
            Dim lTop2 As Integer
            Dim lTop3 As Integer
            Dim lWidth As Integer
            Dim lHeight As Integer
            Dim bScroll As Boolean
            Dim lScroll As Integer

            On Error GoTo ProcError

            'GLOG 6711: Exit immediately if there are no MP variables present
            'GLOG 6954 (dm) - if bXMLTagsOnly, this is getting called on
            'a 10.1.2 doc that has not been preconverted yet
            If Not bXMLTagsOnly Then
                Dim bFound As Boolean
                For i = 1 To oDoc.Variables.Count
                    If InStr(oDoc.Variables(i).Name, "mpo") = 1 Then
                        bFound = True
                        Exit For
                    End If
                Next i
                If Not bFound Then
                    Exit Sub
                End If
            End If

            oWordApp = New WordApp
            bEcho = oWordApp.GetEcho()
            oWordApp.Echo(False)

            'GLOG 7473 (dm) - this code was causing documents to close prematurely
            'because it requires calls to DoEvents below
            '    '8/19/11 (dm) - prevent inadvertent scrolling -
            '    'GetPoint errs if the selection is off the screen
            '    oRngSelect = GlobalMethods.CurWordApp.Selection.Range.Duplicate
            '    On Error Resume Next
            '    oDoc.Application.ActiveWindow.GetPoint lLeft, lTop, lWidth, lHeight, oRngSelect
            '    lScroll = oDoc.Application.ActiveWindow.VerticalPercentScrolled
            '    bScroll = (Err = 0)
            '    On Error GoTo ProcError

            With oDoc
                'body
                AddBookmarksToRange(.Content, bXMLTagsOnly)

                'textboxes in body
                For i = 1 To .Shapes.Count
                    On Error Resume Next
                    oShape = .Shapes(i)
                    On Error GoTo ProcError

                    If Not oShape Is Nothing Then
                        With oShape
                            'GLOG 8424: Conditions were reversed here
                            If .Type = Microsoft.Office.Core.MsoShapeType.msoGroup Then
                                'group - check individual items
                                'For Each oGroupItem In .GroupItems
                                For g = 1 To .GroupItems.Count
                                    oGroupItem = .GroupItems(g)
                                    'GLOG 5495: Test for TextFrame
                                    oTextFrame = BaseMethods.GetTextFrame(oGroupItem)
                                    If Not oTextFrame Is Nothing Then
                                        If oTextFrame.HasText Then
                                            AddBookmarksToRange(oTextFrame.TextRange, bXMLTagsOnly)
                                        End If
                                    End If
                                Next g 'oGroupItem
                            Else
                                'GLOG 5495: Test for TextFrame
                                oTextFrame = BaseMethods.GetTextFrame(oShape)
                                If Not oTextFrame Is Nothing Then
                                    If oTextFrame.HasText Then
                                        AddBookmarksToRange(oTextFrame.TextRange, bXMLTagsOnly)
                                    End If
                                End If
                            End If
                        End With
                    End If
                    oShape = Nothing
                Next i

                'headers/footers
                '3/12/12 (dm) - modified to use StoryRanges collection
                'instead of cycling through every section - first do shapes
                With .Sections(1).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                    For j = 1 To .Shapes.Count
                        On Error Resume Next
                        oShape = .Shapes(j)
                        On Error GoTo ProcError

                        If Not oShape Is Nothing Then
                            With oShape
                                'GLOG 8424: Conditions were reversed here
                                If .Type = Microsoft.Office.Core.MsoShapeType.msoGroup Then
                                    'group - check individual items
                                    'For Each oGroupItem In .GroupItems
                                    For g = 1 To .GroupItems.Count
                                        oGroupItem = .GroupItems(g)
                                        'GLOG 5495: Test for TextFrame
                                        oTextFrame = BaseMethods.GetTextFrame(oGroupItem)
                                        If Not oTextFrame Is Nothing Then
                                            If oTextFrame.HasText Then
                                                AddBookmarksToRange(oTextFrame.TextRange, bXMLTagsOnly)
                                            End If
                                        End If
                                    Next g 'oGroupItem
                                Else
                                    'GLOG 5495: Test for TextFrame
                                    oTextFrame = BaseMethods.GetTextFrame(oShape)
                                    If Not oTextFrame Is Nothing Then
                                        If oTextFrame.HasText Then
                                            AddBookmarksToRange(oTextFrame.TextRange, bXMLTagsOnly)
                                        End If
                                    End If
                                End If
                            End With
                        End If
                        oShape = Nothing
                    Next j
                End With

                Dim oStory As Object
                Dim oStoryRange As Word.Range
                Dim xStories As String
                For Each oStory In .StoryRanges
                    Select Case oStory.StoryType
                        Case WdStoryType.wdPrimaryHeaderStory, WdStoryType.wdPrimaryFooterStory, _
                            WdStoryType.wdFirstPageHeaderStory, WdStoryType.wdFirstPageFooterStory, _
                            WdStoryType.wdEvenPagesFooterStory, WdStoryType.wdEvenPagesHeaderStory
                            oStoryRange = oStory
                            'some stories may be repeated in this cycle for each section in the
                            'document - ensure that we haven't hit this one already
                            If InStr(xStories, "|" & CStr(oStoryRange.StoryType) & "|") = 0 Then
                                xStories = xStories & "|" & CStr(oStoryRange.StoryType) & "|"
                                While Not oStoryRange Is Nothing
                                    AddBookmarksToRange(oStoryRange, bXMLTagsOnly)
                                    oStoryRange = oStoryRange.NextStoryRange
                                End While
                            End If
                        Case Else
                    End Select
                Next oStory

                '        For i = 1 To .Sections.Count
                '            With .Sections(i)
                '                With .Headers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary.wdHeaderFooterIndex.wdHeaderFooterPrimary)
                '                    If i = 1 Then
                '                        'text boxes - there's a shapes single collection
                '                        'for all headers and footers
                '                        For j = 1 To .Shapes.Count
                '                            On Error Resume Next
                '                            oShape = .Shapes(j)
                '                            On Error GoTo ProcError
                '
                '                            If Not oShape Is Nothing Then
                '                                With oShape
                '                                    If .Type = Microsoft.Office.Core.Microsoft.Office.CoreShapeType.Microsoft.Office.CoreGroup Then
                '                                        'group - check individual items
                '                                        'For Each oGroupItem In .GroupItems
                '                                        For g = 1 To .GroupItems.Count
                '                                            oGroupItem = .GroupItems(g)
                '                                            'GLOG 5495: Test for TextFrame
                '                                            oTextFrame = mpBase.GetTextFrame(oGroupItem)
                '                                            If Not oTextFrame Is Nothing Then
                '                                                If oTextFrame.HasText Then
                '                                                    AddBookmarksToRange oTextFrame.TextRange, bXMLTagsOnly
                '                                                End If
                '                                            End If
                '                                        Next g 'oGroupItem
                '                                    Else
                '                                        'GLOG 5495: Test for TextFrame
                '                                        oTextFrame = mpBase.GetTextFrame(oShape)
                '                                        If Not oTextFrame Is Nothing Then
                '                                            If oTextFrame.HasText Then
                '                                                AddBookmarksToRange oTextFrame.TextRange, bXMLTagsOnly
                '                                            End If
                '                                        End If
                '                                    End If
                '                                End With
                '                            End If
                '                            oShape = Nothing
                '                        Next j
                '                    End If
                '
                '                    'primary header
                '                    If (i = 1) Or (Not .LinkToPrevious) Then
                '                        AddBookmarksToRange .Range, bXMLTagsOnly
                '                    End If
                '                End With
                '
                '                 'primary footer
                '                With .Footers(Word.WdHeaderFooterIndex.wdHeaderFooterPrimary.wdHeaderFooterIndex.wdHeaderFooterPrimary)
                '                    If (i = 1) Or (Not .LinkToPrevious) Then
                '                        AddBookmarksToRange .Range, bXMLTagsOnly
                '                    End If
                '                End With
                '
                '                'different first page
                '                If .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Exists Then
                '                    'first page header
                '                    With .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                '                        If (i = 1) Or (Not .LinkToPrevious) Then
                '                            AddBookmarksToRange .Range, bXMLTagsOnly
                '                        End If
                '                    End With
                '                End If
                '                If .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Exists Then
                '                    'first page footer
                '                    With .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                '                        If (i = 1) Or (Not .LinkToPrevious) Then
                '                            AddBookmarksToRange .Range, bXMLTagsOnly
                '                        End If
                '                    End With
                '                End If
                '
                '                'odd/even
                '                If .Headers(WdHeaderFooterIndex.wdHeaderFooterEvenPages).Exists Then
                '                    'first page header
                '                    With .Headers(WdHeaderFooterIndex.wdHeaderFooterEvenPages)
                '                        If (i = 1) Or (Not .LinkToPrevious) Then
                '                            AddBookmarksToRange .Range, bXMLTagsOnly
                '                        End If
                '                    End With
                '                End If
                '                If .Footers(WdHeaderFooterIndex.wdHeaderFooterEvenPages).Exists Then
                '                    'first page footer
                '                    With .Footers(WdHeaderFooterIndex.wdHeaderFooterEvenPages)
                '                        If (i = 1) Or (Not .LinkToPrevious) Then
                '                            AddBookmarksToRange .Range, bXMLTagsOnly
                '                        End If
                '                    End With
                '                End If
                '            End With
                '        Next i
            End With

            'GLOG 6711: If any errors occur after this point, skip to error handler
            'but don't raise an error
            On Error GoTo ScrollError
            'GLOG 5570: Open and close header to clear out empty paragraph mark if present
            'GLOG 6117 (dm) - ViewHeader turns on "show white space"
            'GLOG 7476 (dm) - DisplayPageBoundaries is now preserved in ResetEmptyHeader()
            '    Dim bPageBoundaries As Boolean
            '    bPageBoundaries = GlobalMethods.CurWordApp.ActiveWindow.View.DisplayPageBoundaries

            'GLOG 7473 (dm) - use ResetEmptyHeader, which contains its own code to preserve
            'the scroll percentage - this code is less precise but doesn't require DoEvents
            Dim oWordDoc As WordDoc
            oWordDoc = New WordDoc
            oWordDoc.ResetEmptyHeader()

            '    On Error Resume Next
            '    GlobalMethods.CurWordApp.WordBasic.ViewHeader
            '    GlobalMethods.CurWordApp.WordBasic.ViewHeader
            '
            '    On Error GoTo ScrollError 'GLOG 6711

            'GLOG 6117 (dm)
            '    If Not bPageBoundaries Then _
            '        GlobalMethods.CurWordApp.ActiveWindow.View.DisplayPageBoundaries = False

ScrollError:

            'GLOG 7473 (dm) - this code was causing documents to close prematurely
            'because it requires calls to DoEvents
            '    '8/19/11 (dm) - prevent inadvertent scrolling
            '    If bScroll Then
            '        On Error GoTo ScrollError
            '        oRngSelect.Select
            '        Dim oRngDupe As Word.Range
            '        oRngDupe = oRngSelect.Duplicate
            '        oDoc.Application.ActiveWindow.GetPoint lLeft, lTop2, lWidth, lHeight, oRngSelect
            '        If lTop2 < lTop Then
            '            Do While lTop2 < lTop
            '                lTop3 = lTop2
            '                oDoc.Application.ActiveWindow.SmallScroll , 1
            '                DoEvents
            '                oDoc.Application.ActiveWindow.GetPoint lLeft, lTop2, lWidth, lHeight, oRngDupe
            '                If lTop2 = lTop3 Then Exit Do
            '            Loop
            '        ElseIf lTop2 > lTop Then
            '            Do While lTop2 > lTop
            '                lTop3 = lTop2
            '                oDoc.Application.ActiveWindow.SmallScroll
            '                DoEvents
            '                oDoc.Application.ActiveWindow.GetPoint lLeft, lTop2, lWidth, lHeight, oRngDupe
            '                If lTop2 = lTop3 Then Exit Do
            '            Loop
            '        End If
            '
            '        If Abs(oDoc.Application.ActiveWindow.VerticalPercentScrolled - lScroll) > 5 Then
            '            'the code above did not work correctly - a known issue when the
            '            'selection is far from the scroll position - use the Word
            '            'api directly - calling the property repeatedly ensures that the
            '            'scroll position will be correct within 1% - dcf - 08/25/11
            '            With oDoc.Application.ActiveWindow
            '                .VerticalPercentScrolled = 0
            '                .VerticalPercentScrolled = 100
            '                .VerticalPercentScrolled = lScroll
            '                .VerticalPercentScrolled = lScroll
            '                Debug.Print .VerticalPercentScrolled = lScroll
            '            End With
            '        End If
            '
            '        GoTo ExitScroll
            'ScrollError:
            '        'GLOG 6711: Ignore any error here
            '        On Error Resume Next
            '        oDoc.Application.ActiveWindow.ScrollIntoView oRngSelect, True
            'ExitScroll:
            '    End If

            'GLOG 6711
            On Error Resume Next
            oWordApp.Echo(bEcho)

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            oWordApp.Echo(bEcho)
        End Sub

        Public Sub AddBookmarksToRange(ByVal oRange As Word.Range, Optional ByVal bXMLTagsOnly As Boolean = False)
            '3/22/11 (dm) - added bXMLTagsOnly parameter to allow for runtime bookmarking/preconversion
            'of 10.1.2 docx files in preinjunction Word 2007
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.AddBookmarksToRange"
            Dim oCC As Word.ContentControl
            Dim oTag As Word.XMLNode
            Dim iFormat As mpFileFormats
            Dim xTag As String
            Dim iLastIndex As Short
            Dim oBRange As Word.Range

            On Error GoTo ProcError

            iFormat = BaseMethods.FileFormat(oRange.Document)

            '3/22/11 - removed conditional to allow for preconversion of 10.1.2 .docx files
            '    If iFormat = Binary Then
            For Each oTag In oRange.XMLNodes
                xTag = BaseMethods.GetTag(oTag)

                'preconvert if necessary
                If xTag = "" Then
                    PreconvertXMLTag(oTag, xTag)
                End If

                If xTag <> "" Then
                    'index as necessary
                    If oTag.ParentNode Is Nothing Then
                        iLastIndex = 0
                        IndexNestedAdjacentChildren(oTag, iLastIndex)
                        If iLastIndex > 0 Then
                            iLastIndex = iLastIndex + 1
                            BaseMethods.SetAttributeValue(xTag, "TempID", CStr(iLastIndex), "")
                        End If
                    End If

                    '3/1/11 (dm) - if tag is top-level, expand range to encompass
                    'the start and end tags - this is necessary to avoid a "rich text
                    'can't be applied here" error when attempting to insert the content
                    'control after a .docx save as - do only at runtime, as bookmark
                    'should encompass only the range in the design xml
                    oBRange = oTag.Range
                    If oTag.BaseName = "mSEG" Then
                        If oTag.ParentNode Is Nothing Then
                            oBRange.MoveStart(Word.WdUnits.wdCharacter, -1)
                            oBRange.MoveEnd()
                        End If
                    End If

                    'add bookmark
                    oBRange.Bookmarks.Add("_" & xTag)
                End If
            Next oTag
            '    Else

            If (iFormat = mpFileFormats.OpenXml) And Not bXMLTagsOnly Then
                For Each oCC In oRange.ContentControls
                    'GLOG 5557 (dm) - make sure that this is  Forte content control
                    If BaseMethods.GetBaseName(oCC) <> "" Then
                        'index as necessary
                        If oCC.ParentContentControl Is Nothing Then
                            iLastIndex = 0
                            IndexNestedAdjacentChildren_CC(oCC, iLastIndex)
                            If iLastIndex > 0 Then
                                iLastIndex = iLastIndex + 1
                                BaseMethods.SetAttributeValue(oCC.Tag, "TempID", CStr(iLastIndex), "")
                            End If
                        End If

                        'add bookmark
                        oCC.Range.Bookmarks.Add("_" & oCC.Tag)
                    End If
                Next oCC
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Function ReconstituteIfNecessary(ByVal oDoc As Word.Document, _
                                                ByVal bAsFinished As Boolean, _
                                                ByVal bIncludemSEGs As Boolean) As Boolean
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.ReconstituteIfNecessary"
            Dim oBmk As Word.Bookmark
            Dim oRange As Word.Range
            Dim oSchema As Word.XMLSchemaReference
            Dim bShowHidden As Boolean
            Dim xTag As String
            Dim xObjectData As String
            Dim bTriggerBmkFound As Boolean 'GLOG 6848
            Dim bTargetBmkFound As Boolean 'GLOG 6848
            Dim oChildBmk As Word.Bookmark 'GLOG 7322

            On Error GoTo ProcError

            If oDoc.Path = "" Then _
                Exit Function

            If Val(oDoc.Application.Version) > 14 And BaseMethods.FileFormat(oDoc) = mpFileFormats.Binary Then
                'Word 2013 doesn't support XML Tags, so don't attempt reconstitution
                Exit Function
            End If
            '6/9/11 (dm) - exit if .docx file is in Word 2003 compatibility mode,
            'since this does not support content controls - this property is
            'not in the Word 2007 object model and does not impact the use of
            'content controls in Word 2007
            'GLOG 6567 (dm) - it turns out that compatibility mode does impact Word 2007, it
            'just doesn't prevent reconstitution in that state as it does in Word 2010 -
            'continuing to allow in Word 2007 will enable Convert with DM to work seamlessly
            If Val(oDoc.Application.Version) >= 14 Then
                If oDoc.CompatibilityMode = 11 Then _
                    Exit Function
            End If

            '3/22/11 (dm) - preconvert 10.1.2 .docx file if necessary -
            'batch preconversion is required for Word 2010
            If (Not BaseMethods.IsPostInjunctionWordVersion()) And _
                    (BaseMethods.FileFormat(oDoc) = mpFileFormats.OpenXml) Then
                AddBookmarks(oDoc, True)
            End If

            bShowHidden = oDoc.Bookmarks.ShowHidden
            oDoc.Bookmarks.ShowHidden = True

            'determine whether first mp content bookmark has appropriate bounding object -
            'checking for tags/ccs in story seems safer than checking for range's parent tag/cc
            'because it allows for the possibility that the bookmark is simply mispositioned
            For Each oBmk In oDoc.Bookmarks
                'GLOG 5490 (dm) - do not reconsitute unless an mSEG bookmark is found -
                'we were previously accepting any mp10 content bookmark
                '9/6/11 (dm) - added mSecProps and mDocProps as reconsitution triggers,
                'since these exist independently of mSEGS
                'GLOG 6848 (dm) - in 10.5, mSEGs are no longer reconstituted -
                'make sure that the document contains a target as well as a trigger
                'GLOG 7322 (dm) - only search for a target bookmark in the trigger bookmark's
                'range - the previous code was returning a false positive in segments where
                'some mSEGs contain variables and blocks and others don't
                If (Not bTriggerBmkFound) And ((oBmk.Name Like "_mps*") Or _
                        (oBmk.Name Like "_mpp*") Or (oBmk.Name Like "_mpc*")) Then
                    'verify that bounding objects are missing
                    oRange = oBmk.Range
                    oRange.WholeStory()
                    If BaseMethods.FileFormat(oDoc) = mpFileFormats.Binary Then
                        '.doc format
                        bTriggerBmkFound = (oRange.XMLNodes.Count = 0)
                    Else
                        '.docx format
                        bTriggerBmkFound = (oRange.ContentControls.Count = 0)
                    End If

                    If bTriggerBmkFound Then
                        '6/9/11 (dm) - test .docx for doc var corruption - this may
                        'be caused by the compatibility pack - exit if found
                        'GLOG 6470 (dm) - extended to .doc
                        'GLOG 6524 (dm) - ignore error that may occur if doc var is corrupt
                        xTag = Mid$(oBmk.Name, 2)
                        On Error Resume Next
                        xObjectData = BaseMethods.GetAttributeValue(xTag, "ObjectData", oDoc)
                        On Error GoTo ProcError

                        'GLOG 6644 (dm) - mSEGS and mSecProps/mDocProps require different tests
                        If oBmk.Name Like "_mps*" Then
                            'mSEG
                            If InStr(xObjectData, "SegmentID=") = 0 Then _
                                Exit For

                            'GLOG 6848 (dm) - verify target
                            If Not bTargetBmkFound Then _
                                bTargetBmkFound = bIncludemSEGs
                        Else
                            'mSecProp/mDocProp
                            If (InStr(xObjectData, "|") = 0) Or (InStr(xObjectData, "�") = 0) Then _
                                Exit For

                            'GLOG 6848 (dm) - verify target
                            bTargetBmkFound = True
                        End If

                        If Not bTargetBmkFound Then
                            'GLOG 6848 (dm) - verify target
                            For Each oChildBmk In oRange.Bookmarks
                                bTargetBmkFound = ((oChildBmk.Name Like "_mpv*") Or (oChildBmk.Name Like "_mpb*") Or _
                                    (oChildBmk.Name Like "_mpd*") Or (oChildBmk.Name Like "_mpu*"))
                                If bTargetBmkFound Then _
                                    Exit For
                            Next oChildBmk
                        End If

                        If bTargetBmkFound Then
                            If BaseMethods.FileFormat(oDoc) = LMP.Forte.MSWord.mpFileFormats.Binary Then
                                'reconstitute with tags -
                                '9/28/11 (dm) - added argument to delete content controls -
                                'this doesn't happen automatically with Worksite 8.5 SP2
                                'GLOG 6801 (dm): reversed bValidateTopBookmarks value for 10.5, as
                                'we're no longer reconstituting mSEGs in most contexts and
                                'validation is inappropriate in the others
                                AddTagsToBookmarkedDocument(oDoc, , True, bAsFinished, _
                                    False, bIncludemSEGs)
                            Else
                                'reconstitute with content controls
                                'GLOG 6801 (dm): reversed bValidateTopBookmarks value for 10.5, as
                                'we're no longer reconstituting mSEGs in most contexts and
                                'validation is inappropriate in the others
                                AddContentControlsToBookmarkedDocument(oDoc, , bAsFinished, _
                                    False, bIncludemSEGs)

                                'remove any tags
                                On Error Resume Next
                                oSchema = oDoc.XMLSchemaReferences(GlobalMethods.mpNamespace)
                                On Error GoTo ProcError
                                If Not oSchema Is Nothing Then
                                    oSchema.Delete()
                                    oDoc.XMLSchemaReferences.Add(GlobalMethods.mpNamespace)
                                End If
                            End If

                            'GLOG 5803 (dm) - added return value
                            ReconstituteIfNecessary = True
                            Exit For
                        Else
                            'GLOG 8087 (dm) - clear trigger flag - mSEGs without any
                            'mVars, mBlocks, or mDels were inadvertently terminating
                            'the search for content to reconstitute
                            bTriggerBmkFound = False
                        End If
                    End If
                End If
            Next oBmk

            oDoc.Bookmarks.ShowHidden = bShowHidden

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Function

        Public Sub BookmarkBoundingObjects(ByVal oDoc As Word.Document, _
                                           ByVal vBoundingObjectArray As Object)
            'bookmarks the tags/ccs in the array, as well as their children
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.BookmarkBoundingObjects"
            Dim oRange As Word.Range
            Dim i As Short
            Dim oTag As Word.XMLNode
            Dim oCC As Word.ContentControl

            On Error GoTo ProcError

            For i = 0 To UBound(vBoundingObjectArray)
                If BaseMethods.FileFormat(oDoc) = mpFileFormats.Binary Then
                    oTag = vBoundingObjectArray(i)
                    oRange = oTag.Range
                Else
                    oCC = vBoundingObjectArray(i)
                    oRange = oCC.Range
                End If

                'expand range to include this tag
                oRange.MoveStart(Word.WdUnits.wdCharacter, -1)
                oRange.MoveEnd()

                'bookmark range
                AddBookmarksToRange(oRange, False)
            Next i

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Friend Sub PreconvertXMLTag(ByVal oTag As Word.XMLNode, Optional ByRef xTag As String = "")
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.PreconvertXMLTag"
            Dim xElement As String
            Dim i As Short
            Dim xAttributes As String
            Dim xObjectDBID As String
            Dim xDocVarID As String
            Dim oDoc As Word.Document
            Dim xDocVar As String
            Dim xMPOVar As String
            Dim xMPDVar As String
            Dim xAttribute As String
            Dim oDeletedScopes As Word.XMLNode
            Dim oCOM As Application
            Dim xTagID As String
            Dim xDeletedScopes As String
            Dim oChildTag As Word.XMLNode
            Dim oDeleteScope As DeleteScope

            On Error GoTo ProcError

            oCOM = New Application
            oDoc = oTag.Range.Document

            xElement = oTag.BaseName
            Select Case xElement
                Case "mSEG"
                    xTag = "mps"
                Case "mVar"
                    xTag = "mpv"
                Case "mBlock"
                    xTag = "mpb"
                Case "mDel"
                    xTag = "mpd"
                Case "mSubVar"
                    xTag = "mpu"
                Case "mDocProps"
                    xTag = "mpp"
                Case "mSecProps"
                    xTag = "mpc"
                Case Else
                    Exit Sub
            End Select

            'doc var id
            xDocVarID = GenerateDocVarID()
            xTag = xTag & xDocVarID

            'object db id
            If xElement = "mSEG" Then
                xObjectDBID = GetmSEGObjectDataValue(oTag, "SegmentID")
                If (Len(xObjectDBID) < 19) Then
                    xObjectDBID = BaseMethods.CreatePadString(19 - Len(xObjectDBID), "0") & xObjectDBID
                End If
                xTag = xTag & xObjectDBID

                'bookmark names can only contain alphanumeric characters
                xTag = Replace(xTag, "-", "m")
                xTag = Replace(xTag, ".", "d")
            End If

            'pad with trailing zeroes
            'GLOG 6015 (dm) - we were creating 40-character tags here -
            'front-end preconversion of design xml has correctly been doing
            '39 characters all along, explaining why were seeing certain problems
            'only with existing 10.1.2 documents
            xTag = xTag & BaseMethods.CreatePadString(39 - Len(xTag), "0")

            'add reserved attribute
            oTag.Attributes.Add("Reserved", "").NodeValue = xTag

            'get mpo doc var name
            xMPOVar = "mpo" & xDocVarID

            'test for conflicts
            On Error Resume Next
            xDocVar = oDoc.Variables(xMPOVar).Name
            On Error GoTo 0
            While oDoc.Bookmarks.Exists(xTag) Or (xDocVar <> "")
                'regenerate doc var id
                xDocVarID = GenerateDocVarID()
                xTag = Left$(xTag, 3) & xDocVarID & Mid$(xTag, 12)
                xMPOVar = Left$(xMPOVar, 3) & xDocVarID
                xDocVar = ""
                On Error Resume Next
                xDocVar = oDoc.Variables(xMPOVar).Name
                On Error GoTo 0
            End While

            'create mpo document variable
            If xElement = "mSubVar" Then
                xTagID = oTag.SelectSingleNode("@Name").NodeValue
            Else
                Dim oTagIDAttr As XMLNode

                oTagIDAttr = oTag.SelectSingleNode("@TagID")

                If Not oTagIDAttr Is Nothing Then
                    xTagID = oTagIDAttr.NodeValue
                End If
            End If
            xTagID = xTagID & "��"

            'the rest of the value is encrypted
            With oTag.Attributes
                For i = 1 To .Count
                    xAttribute = .Item(i).BaseName
                    If (xAttribute <> "TagID") And (xAttribute <> "Name") And _
                            (xAttribute <> "DeletedScopes") Then
                        xAttributes = xAttributes & xAttribute & "=" & _
                            oCOM.Decrypt(.Item(i).NodeValue) & "��"
                    End If
                Next i
            End With

            'strip trailing separator
            If xAttributes <> "" Then _
                xAttributes = Left$(xAttributes, Len(xAttributes) - 2)

            'encrypt
            If (xElement <> "mDel") And (xElement <> "mSubVar") Then _
                xAttributes = oCOM.Encrypt(xAttributes)

            'add variable
            oDoc.Variables.Add(xMPOVar, xTagID & xAttributes)

            'element-specific handling
            If xElement = "mSEG" Then
                'preconvert delete scopes
                oDeletedScopes = oTag.SelectSingleNode("@DeletedScopes")
                If Not oDeletedScopes Is Nothing Then
                    'preconvert the xml
                    oDeleteScope = New DeleteScope
                    xDeletedScopes = oCOM.Decrypt(oDeletedScopes.NodeValue)
                    xDeletedScopes = oDeleteScope.Preconvert(xDeletedScopes, oDoc)
                    xDeletedScopes = oCOM.Encrypt(xDeletedScopes)
                    oDeletedScopes.NodeValue = xDeletedScopes

                    'create mpd variable
                    xMPDVar = "mpd" & xDocVarID & "01"
                    oDoc.Variables.Add(xMPDVar, xDeletedScopes)
                End If
            ElseIf xElement = "mVar" Then
                'preconvert sub vars
                For Each oChildTag In oTag.Range.XMLNodes
                    If oChildTag.BaseName = "mSubVar" Then
                        If BaseMethods.GetTag(oChildTag) = "" Then _
                            PreconvertXMLTag(oChildTag)
                    End If
                Next oChildTag
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub DeleteBookmarksAndDocVars(ByVal oDoc As Word.Document)
            'deletes all Forte content bookmarks and associated doc vars
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteBookmarksAndDocVars"
            Dim bShowHidden As Boolean
            Dim oBmk As Word.Bookmark
            Dim oDocVar As Word.Variable
            Dim xName As String
            Dim xValue As String

            On Error GoTo ProcError

            'delete bookmarks
            bShowHidden = oDoc.Bookmarks.ShowHidden
            oDoc.Bookmarks.ShowHidden = True

            For Each oBmk In oDoc.Bookmarks
                If oBmk.Name Like "_mp*" Then
                    oBmk.Delete()
                End If
            Next oBmk

            oDoc.Bookmarks.ShowHidden = bShowHidden

            'delete doc vars
            For Each oDocVar In oDoc.Variables
                xName = oDocVar.Name
                xValue = oDocVar.Value
                If (Left$(xName, 3) = "mpo") And (Len(xName) = 11) Then
                    'mpo doc var - check for attribute separator
                    If InStr(xValue, "��") > 0 Then _
                        oDocVar.Delete()
                ElseIf (Left$(xName, 3) = "mpd") And (Len(xName) = 13) Then
                    'mpd doc var - ensure that it's a DeletedScopes attribute
                    xValue = BaseMethods.Decrypt(xValue)
                    If InStr(xValue, "DeletedTag=") > 0 Then _
                        oDocVar.Delete()
                ElseIf (InStr(xName, "_{") > 0) And ((Left$(xName, 9) = "Finished_") Or _
                        (Left$(xName, 15) = "FinishedConfig_")) Then
                    '10/24/11 (dm) - delete snapshot vars
                    'GLOG 7361 (dm) - restored this branch, which had been remmed out
                    oDocVar.Delete()
                End If
            Next oDocVar

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub

        Public Function DocxIsIn2003CompatibilityMode(ByVal oDocument As Word.Document) As Boolean
            'GLOG 6567 (dm) - returns true if this is a docx file in Word 2003 compatibility mode
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DocxIsIn2003CompatibilityMode"

            On Error GoTo ProcError

            If BaseMethods.FileFormat(oDocument) = mpFileFormats.Binary Then
                DocxIsIn2003CompatibilityMode = False
            ElseIf Val(GlobalMethods.CurWordApp.Version) > 12 Then ' GLOG 6665: Apply to Word 2013 as well
                DocxIsIn2003CompatibilityMode = (oDocument.CompatibilityMode = 11)
            Else
                'Word 2007 doesn't support CompatibilityMode property - use caption
                DocxIsIn2003CompatibilityMode = (InStr(oDocument.ActiveWindow.Caption, _
                    "[Compatibility Mode]") > 0)
            End If

            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Sub DeleteTagsInRange(ByVal oRange As Word.Range, ByVal bSegmentTagsOnly As Boolean)
            'added for GLOG 6844 (dm)
            Const mpThisFunction As String = "LMP.Forte.MSWord.Conversion.DeleteTagsInRange"
            Dim oSec As Section
            Dim oHF As HeaderFooter
            Dim oNode As Word.XMLNode
            Dim i As Short
            Dim oShape As Word.Shape
            Dim oRng As Word.Range
            Dim xTag As String
            Dim oShapes As Word.Shapes
            Dim oTextRange As Word.Range

            For i = oRange.XMLNodes.Count To 1 Step -1
                oNode = oRange.XMLNodes(i)
                If BaseMethods.IsForteTag(oNode) Then
                    If (Not bSegmentTagsOnly) Or (oNode.BaseName = "mSEG") Then
                        oNode.Delete()
                    End If
                End If
            Next i

            'GLOG 6844 (dm) - just noting that the following will hit all textboxes in the
            'target story, including in other section headers/footers - we currently rely on it
            'working this way, but it does make the name of this method a bit of a misnomer
            If oRange.StoryType = wdStoryType.wdMainTextStory Then
                oShapes = oRange.Document.Shapes
            ElseIf oRange.StoryType <> wdStoryType.wdTextFrameStory Then
                oShapes = oRange.Document.Sections(1).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes
            End If

            If Not oShapes Is Nothing Then
                For Each oShape In oShapes
                    If BaseMethods.HasTextFrameText(oShape) Then 'GLOG 7009
                        oTextRange = oShape.TextFrame.TextRange
                        For i = oTextRange.XMLNodes.Count To 1 Step -1
                            oNode = oTextRange.XMLNodes(i)
                            If BaseMethods.IsForteTag(oNode) Then
                                If (Not bSegmentTagsOnly) Or (oNode.BaseName = "mSEG") Then
                                    oNode.Delete()
                                End If
                            End If
                        Next i
                    End If
                Next oShape
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
        End Sub
    End Class
End Namespace