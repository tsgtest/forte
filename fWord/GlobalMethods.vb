Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Forte.MSWord

    'Important note: do not change the order of enumeration below -
    'currently, our other components can only refer to these errors by their numbers
    Public Enum mpErrors
        mpError_NotImplemented = 10 + 512 + 4000
        mpError_FailedDBConnection
        mpError_InvalidFieldCode
        mpError_FailedStoredProcedure
        mpError_FailedSQLStatement
        mpError_ItemNotInCollection
        mpError_MatchingItemNotInCollection
        mpError_InvalidData
        mpError_RecordUpdateFailed
        mpError_MissingObjectID
        mpError_NoUserName
        mpError_InvalidStringArrayElementIndex
        mpError_InvalidStringArrayRecordIndex
        mpError_InvalidStringArrayFieldIndex
        mpError_StringArrayTextNotSet
        mpError_StringArraySeparatorsNotUnique
        mpError_IncorrectParameterCount
        mpError_NamedItemExistsInCollection
        mpError_NumericItemKeyNotAllowed
        mpError_TableKeyViolation
        mpError_InvalidValue
        mpError_OwnerRequired
        mpError_InvalidPersonID
        mpError_InvalidUser
        mpError_EnumerationTypeMismatch
        mpError_InvalidObjectTypeID
        mpError_DocPropertyIDRequired
        mpError_InvalidDocPropertyID
        mpError_ControlIDRequired
        mpError_InvalidControlID
        mpError_UnknownLocaleID
        mpError_ItemNotEditable
        mpError_SaveRequired
        mpError_InvalidControlTypeID
        mpError_ItemCannotBeDeleted
        mpError_InvalidObjectID
        mpError_UserObjectAssignmentExists
        mpError_UserObjectAssignmentDoesNotExist
        mpError_NoPermission
        mpError_PublicPersonRequired
        mpError_InvalidAction
        mpError_InvalidEntityID
        mpError_InvalidKeySet
        mpError_InvalidOrMissingDefinitionObject
        mpError_InvalidAddressID
        mpError_InvalidField
        mpError_NoTranslationExists
        mpError_ItemAlreadyInCollection
        mpError_ApplicationNotRunning
        mpError_NoActiveWordDocument
        mpError_InvalidWordDocumentVariableName
        mpError_InvalidWordStyle
        mpError_InvalidWordBookmarkName
        mpError_NoAuthorsProvided
        mpError_InvalidObjectPropertyName
        mpError_InvalidExpression
        mpError_InvalidArgument
        mpError_InvalidPropertyName
        mpError_ObjectParameterIsNothing
        mpError_InvalidWordDocument
        mpError_InvalidWordPropertyReference
        mpError_InvalidWordObjectReference
        mpError_CallByNameFailed
        mpError_ErrorDetailLost
        mpError_InvalidEXECCode
        mpError_FileDoesNotExist
        mpError_CouldNotExecuteMacro
        mpError_InvalidDirectory
        mpError_BookmarkDoesNotExist
        mpError_CouldNotCreateCOMObject
        mpError_CouldNotRunMethod
        mpError_InvalidDocumentSection
        mpError_CouldNotInsertBoilerplate
        mpError_PropertyCountMismatch
        mpError_NoObjectPropertiesDefined
        mpError_LimitExceeded
        mpError_TypeIDRequired
        mpError_FieldCodeNotValidInCurrentContext
        mpError_ControlDoesNotHaveValue
        mpError_MissingParent
        mpError_ControlDoesNotHaveCaption
        mpError_ControlDoesNotSupportProperty
        mpError_ControlDoesNotSupportAction
        mpError_InvalidList
        mpError_InvalidUnprotectPassword
        mpError_PleadingCaptionsCollectionFull
        mpError_PleadingCounselsCollectionFull
        mpError_PleadingSignaturesCollectionFull
        mpError_InvalidXML
        mpError_CouldNotCreateXMLNode
        mpError_XMLNodeDoesNotExist
        mpError_InvalidOrMissingProgID
        mpError_InvalidAuthorIndex
        mpError_SubObjectDoesNotExist
        mpError_InvalidDocPropertyName
        mpError_InvalidSubObjectReference
        mpError_InvalidKeyword
        mpError_CouldNotSetProperty
        mpError_InvalidPositionInTable
        mpError_InvalidWordDocumentPropertyName
        mpError_CouldNotAddBorder
        mpError_InvalidDeleteScope
        mpError_InsertionLocationRequired
        mpError_Generic
        mpError_CannotInsertTableIntoTable
        mpError_MissingTable
        mpError_InvalidMember
        mpError_TruncatedData
    End Enum

    Public Class GlobalMethods
        Private Declare Function LockWindowUpdate Lib "user32" ( _
            ByVal hwndLock As Integer) As Integer
        Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
            ByVal lpClassName As String, ByVal lpWindowName As String) As Integer

        Public Const mpEndOfValue As String = "�"
        Public Const mpEndOfField As String = "|"
        Public Const mpEndOfRecord As String = "�"
        Public Const mpInsertUnderlinedTab As Short = -1
        Public Const mpNamespace As String = "urn-legalmacpac-data/10"
        Public Const mpStartParaTag As String = "<w:p>"
        Public Const mpEndParaTag As String = "</w:p>"
        Public Const mpEmptyParaTag As String = "<w:p />"
        Public Const mpInternalTagPrefixID As String = "SGI"
        Public Const mpTagPrefixIDSeparator As String = "���"
        Public Const mpTrailerStyleName As String = "MacPac Trailer"


        'used by Tags
        Friend Shared g_oContentXML As Xml.XmlDocument
        Friend Shared g_bSuspendCompression As Boolean
        Friend Shared g_bDisableEncryption As Boolean
        Friend Shared g_xClientTagPrefixID As String
        Friend Shared g_xEncryptionPassword As String
        Friend Shared g_oDocVars As Collection
        Friend Shared g_iIsPostInjunctionWord As Short
        Friend Shared g_bBase64Encoding As Boolean
        Friend Shared g_bTrailerInProgress As Boolean 'GLOG 7496 (dm)
        Friend Shared g_oCurWordApp As Word.Application

        Public Shared Property CurWordApp As Word.Application
            Get
                CurWordApp = g_oCurWordApp
            End Get
            Set(value As Word.Application)
                g_oCurWordApp = value
            End Set
        End Property
        Public Shared Function GetWordObjectFromString(ByVal oTopLevelObject As Object, ByVal xReferenceString As String) As Object
            'returns the Word object referenced by the specified string and
            'top level object- e.g. ParagraphFormat.TabStops.Item(1)
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.GetWordObjectFromString"
            Dim oWordObject As Object
            Dim vElements As Object
            Dim j As Short
            Dim iPos As Integer
            Dim xIndexKey As String
            Dim iIndexKey As Short
            Dim lErr As Integer
            Dim xDesc As String

            On Error GoTo ProcError

            oWordObject = oTopLevelObject

            'replace allowed shortcuts with proper syntax
            'replace all Para. with ParagraphFormat.
            xReferenceString = Replace(xReferenceString, _
                "Para.", "ParagraphFormat.", , , vbTextCompare)

            'split all parts of the instruction field into array elements
            vElements = Split(xReferenceString, ".")

            'cycle through remaining array elements, using
            'call by name to work through the object model
            For j = 0 To UBound(vElements)
                If UCase$(Left$(vElements(j), 5)) = "ITEM(" Then
                    'the element is an Item method - parse index/key
                    iPos = InStr(6, vElements(j), ")")

                    If iPos = 0 Then
                        Err.Raise(mpErrors.mpError_InvalidWordObjectReference, , _
                            "<Error_InvalidWordObjectReference>" & xReferenceString)
                    End If

                    xIndexKey = Mid$(vElements(j), 6, iPos - 6)

                    'get the specified item
                    On Error Resume Next
                    oWordObject = oWordObject.Item(xIndexKey)
                    lErr = Err.Number
                    xDesc = Err.Description

                    On Error GoTo ProcError
                    If lErr = 5941 Then
                        Err.Raise(mpErrors.mpError_InvalidWordObjectReference, , _
                            "<Error_InvalidWordObjectReference>" & xReferenceString)
                    End If
                Else
                    On Error Resume Next
                    oWordObject = CallByName(oWordObject, vElements(j), vbGet)
                    If Err.Number > 0 Then
                        On Error GoTo ProcError
                        Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                            "<Error_CallByNameFailed>" & vElements(j))
                    End If

                    On Error GoTo ProcError
                End If
            Next j

            GetWordObjectFromString = oWordObject
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function

        Public Shared Function GetRangeValue(ByVal oRange As Word.Range, ByVal xProperty As String) As String
            'returns the value of the specified property of a word range
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.GetRangeValue"
            Dim vPropertyName As Object
            Dim i As Short
            Dim oFormatObject As Object
            Dim xIndexKey As String
            Dim iIndexKey As Short
            Dim lErr As Integer
            Dim xDesc As String
            Dim iPos As Integer

            On Error GoTo ProcError

            'GlobalMethods.SendToDebug "xPropertyName=" & xProperty, mpThisFunction

            oFormatObject = oRange

            'split all parts of the field code field into array elements
            vPropertyName = Split(xProperty, ".")

            'cycle through remaining array elements, using
            'call by name to work through the object model
            For i = 0 To UBound(vPropertyName) - 1
                If UCase$(Left$(vPropertyName(i), 5)) = "ITEM(" Then
                    'the element is an Item method - parse index/key
                    iPos = InStr(6, vPropertyName(i), ")")

                    If iPos = 0 Then
                        Err.Raise(mpErrors.mpError_InvalidWordPropertyReference, , _
                            "<Error_InvalidWordPropertyReference>" & xProperty)
                    End If

                    xIndexKey = Mid$(vPropertyName(i), 6, iPos - 6)

                    If Not IsNumeric(xIndexKey) Then
                        Err.Raise(mpErrors.mpError_InvalidWordPropertyReference, , _
                            "<Error_InvalidWordPropertyReference>" & xProperty)
                    End If

                    iIndexKey = CInt(xIndexKey)

                    'get the item specified
                    On Error Resume Next
                    oFormatObject = oFormatObject.Item(iIndexKey)
                    lErr = Err.Number
                    xDesc = Err.Description

                    On Error GoTo ProcError
                    If lErr = 5941 Then
                        Err.Raise(mpErrors.mpError_InvalidWordPropertyReference, , _
                            "<Error_InvalidWordPropertyReference>" & xProperty)
                    End If
                Else
                    On Error Resume Next
                    oFormatObject = CallByName(oFormatObject, vPropertyName(i), vbGet)

                    If Err.Number > 0 Then
                        On Error GoTo ProcError
                        Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                            "<Error_CallByNameFailed>" & vPropertyName(i))
                    End If

                    On Error GoTo ProcError
                End If
            Next i

            On Error Resume Next

            'get the property specified - this is the last item in the array
            GetRangeValue = CallByName(oFormatObject, vPropertyName(UBound(vPropertyName)), vbGet)

            If Err.Number > 0 Then
                On Error GoTo ProcError
                Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & vPropertyName(i))
            End If

            On Error GoTo ProcError
            Exit Function
ProcError:
            If Err.Number = 438 Then
                'object doesn't support the property or method
                Err.Raise(mpErrors.mpError_InvalidFieldCode, mpThisFunction, _
                    "<Error_InvalidFieldCodeParameter>" & "Style" & xProperty)
            Else
                GlobalMethods.RaiseError(mpThisFunction)
            End If
            Exit Function
        End Function

        Public Shared Sub SetWordProperty(ByVal oWordObject As Object, ByVal xPropertyName As String, ByVal xPropertyValue As String)
            'sets the value of the specifed property
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.SetWordProperty"
            Dim xModifier As String
            Dim xPercentModifier As String
            Dim xAmount As String
            Dim xCurValue As String
            Dim bShowTags As Boolean

            On Error GoTo ProcError

            'GlobalMethods.SendToDebug "xPropertyName=" & xPropertyName & _
            '";xPropertyValue=" & xPropertyValue, mpThisFunction

            'get any Forte-specific value modifiers -
            'currently there are 3 - <,>,%
            xModifier = Left$(xPropertyValue, 1)
            xPercentModifier = Right(xPropertyValue, 1)

            If Trim$(UCase$(xPropertyName)) = "NULL" Then
                xPropertyName = ""
            ElseIf xModifier = "<" Or xModifier = ">" Or xPercentModifier = "%" Then
                'increase/decrease existing value by specified amount-
                If xModifier = "<" Or xModifier = ">" Then
                    xAmount = Mid$(xPropertyValue, 2)
                Else
                    xAmount = Left$(xPropertyValue, Len(xPropertyValue) - 1)
                End If

                'check if amount is numeric - if not raise error
                If Not IsNumeric(xAmount) Then
                    Err.Raise(mpErrors.mpError_InvalidValue, , _
                        "<Error_InvalidPropertyValueAssignment>" & _
                        xPropertyName & "=" & xPropertyValue)
                End If

                On Error Resume Next
                'get current value of property
                xCurValue = CallByName(oWordObject, xPropertyName, vbGet)

                If Err.Number > 0 Then
                    On Error GoTo ProcError
                    Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                        "<Error_CallByNameFailed>" & xPropertyName)
                End If

                On Error GoTo ProcError

                'modify value based on modifier
                If xModifier = "<" Then
                    xPropertyValue = CDbl(xCurValue) - CDbl(xAmount)
                ElseIf xModifier = ">" Then
                    xPropertyValue = CDbl(xCurValue) + CDbl(xAmount)
                Else
                    xPropertyValue = xCurValue * (CInt(xAmount) / 100)
                End If
            End If

            On Error Resume Next
            'GLOG 7526: Save initial XML Tag state to be restored afterward
            bShowTags = GlobalMethods.CurWordApp.ActiveDocument.ActiveWindow.View.ShowXMLMarkup
            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, True)

            'set property to new value
            'convert true and false to numeric equivalents
            If UCase$(xPropertyValue) = "TRUE" Then
                CallByName(oWordObject, xPropertyName, vbLet, True)
            ElseIf UCase$(xPropertyValue) = "FALSE" Then
                CallByName(oWordObject, xPropertyName, vbLet, False)
            ElseIf IsNumeric(xPropertyValue) Then
                CallByName(oWordObject, xPropertyName, vbLet, CDbl(xPropertyValue))
            Else
                CallByName(oWordObject, xPropertyName, vbLet, xPropertyValue)
            End If

            If Err.Number > 0 Then
                On Error GoTo ProcError
                Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & xPropertyName & "=" & xPropertyValue)
            End If

            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, bShowTags)  'JTS 6/4/10
            On Error GoTo ProcError
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub CallMethodOnObject(ByVal oObject As Object, ByVal xProcName As String, xArgs As String)
            'calls the method with the specified name - args is a comma delimited
            'string of method arguments - must be in the correct order
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.CallMethodOnObject"
            Dim vArgs As Object
            Dim i As Short
            Dim vArg1 As Object
            Dim vArg2 As Object
            Dim vArg3 As Object
            Dim vArg4 As Object
            Dim vArg5 As Object
            Dim vArg6 As Object
            Dim vArg7 As Object
            Dim vArg8 As Object
            Dim vArg9 As Object
            Dim vArg10 As Object
            Dim vArg11 As Object
            Dim vArg12 As Object
            Dim vArg13 As Object
            Dim vArg14 As Object
            Dim vArg15 As Object
            Dim vArg16 As Object
            Dim vArg17 As Object
            Dim vArg18 As Object
            Dim vArg19 As Object
            Dim vArg20 As Object
            Dim vArg21 As Object
            Dim vArg22 As Object
            Dim vArg23 As Object
            Dim vArg24 As Object
            Dim vArg25 As Object
            Dim vArg26 As Object
            Dim vArg27 As Object
            Dim vArg28 As Object
            Dim vArg29 As Object
            Dim vArg30 As Object
            Dim vArg31 As Object
            Dim bShowTags As Boolean

            On Error GoTo ProcError

            bShowTags = GlobalMethods.CurWordApp.ActiveDocument.ActiveWindow.View.ShowXMLMarkup
            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, True)  'JTS 6/4/10
            GlobalMethods.CurWordApp.ScreenRefresh()

            'GlobalMethods.SendToDebug "xProcName=" & xProcName & _
            '";xArgs=" & xArgs, mpThisFunction

            On Error Resume Next

            If xArgs = "" Then
                CallByName(oObject, xProcName, vbMethod)
            Else
                On Error GoTo ProcError

                'create array of arguments
                vArgs = Split(xArgs, mpEndOfValue)
                vArgs = Split(xArgs, ",")

                'make any necessary adjustments for CallByName,
                'e.g. converting True and False to booleans
                For i = 0 To UBound(vArgs)
                    Select Case i
                        Case 0
                            vArg1 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 1
                            vArg2 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 2
                            vArg3 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 3
                            vArg4 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 4
                            vArg5 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 5
                            vArg6 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 6
                            vArg7 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 7
                            vArg8 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 8
                            vArg9 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 9
                            vArg10 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 10
                            vArg11 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 11
                            vArg12 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 12
                            vArg13 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 13
                            vArg14 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 14
                            vArg15 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 15
                            vArg16 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 16
                            vArg17 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 17
                            vArg18 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 18
                            vArg19 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 19
                            vArg20 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 20
                            vArg21 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 21
                            vArg22 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 22
                            vArg23 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 23
                            vArg24 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 24
                            vArg25 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 25
                            vArg26 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 26
                            vArg27 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 27
                            vArg28 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 28
                            vArg29 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 29
                            vArg30 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                        Case 30
                            vArg31 = BaseMethods.ConvertDataTypeKeywords(vArgs(i))
                    End Select
                Next i

                On Error Resume Next

                'make the appropriate call for the given number of arguments
                Select Case UBound(vArgs)
                    Case 0
                        CallByName(oObject, xProcName, vbMethod, vArg1)
                    Case 1
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2)
                    Case 2
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3)
                    Case 3
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4)
                    Case 4
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5)
                    Case 5
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5, vArg6)
                    Case 6
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5, vArg6, vArg7)
                    Case 7
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8)
                    Case 8
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, _
                            vArg9)
                    Case 9
                        CallByName(oObject, xProcName, vbMethod, vArg1, _
                            vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, _
                            vArg9, vArg10)
                    Case 10
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11)
                    Case 11
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12)
                    Case 12
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13)
                    Case 13
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14)
                    Case 14
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15)
                    Case 15
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16)
                    Case 16
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17)
                    Case 17
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18)
                    Case 18
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19)
                    Case 19
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20)
                    Case 20
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21)
                    Case 21
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22)
                    Case 22
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23)
                    Case 23
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24)
                    Case 24
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25)
                    Case 25
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26)
                    Case 26
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26, vArg27)
                    Case 27
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26, vArg27, vArg28)
                    Case 28
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26, vArg27, vArg28, vArg29)
                    Case 29
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26, vArg27, vArg28, vArg29, vArg30)
                    Case 30
                        CallByName(oObject, xProcName, vbMethod, _
                            vArg1, vArg2, vArg3, vArg4, vArg5, vArg6, vArg7, vArg8, vArg9, vArg10, vArg11, _
                            vArg12, vArg13, vArg14, vArg15, vArg16, vArg17, vArg18, vArg19, vArg20, vArg21, _
                            vArg22, vArg23, vArg24, vArg25, vArg26, vArg27, vArg28, vArg29, vArg30, vArg31)
                End Select
            End If

            If Err.Number > 0 Then
                On Error GoTo ProcError
                Err.Raise(LMP.Forte.MSWord.mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & xProcName & " " & xArgs)
            Else
                On Error GoTo ProcError
            End If

            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, bShowTags)  'JTS 6/4/10
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub CallMethod(ByVal xServer As String, _
                              ByVal xClass As String, _
                              ByVal xProcName As String, _
                              Optional ByVal vParams As Object = Nothing)
            'calls the specified method with the specified arguments
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.CallMethod"
            Dim oObject As Object
            Dim vParam1 As Object
            Dim vParam2 As Object
            Dim vParam3 As Object
            Dim vParam4 As Object
            Dim vParam5 As Object
            Dim vParam6 As Object
            Dim vParam7 As Object
            Dim vParam8 As Object
            Dim vParam9 As Object
            Dim vParam10 As Object
            Dim i As Short
            Dim iParams As Short
            Dim xArgs As String

            On Error GoTo ProcError

            'create object
            On Error Resume Next
            oObject = CreateObject(xServer & "." & xClass)
            On Error GoTo ProcError

            If oObject Is Nothing Then
                Err.Raise(LMP.Forte.MSWord.mpErrors.mpError_CouldNotCreateCOMObject, , _
                    "<Error_CreateCOMObject>" & xServer & "." & xClass)
            End If

            'get number of parameters
            If Not Information.IsNothing(vParams) Then
                On Error Resume Next
                iParams = UBound(vParams) + 1
                On Error GoTo ProcError
            End If

            'make any necessary adjustments for CallByName,
            'e.g. converting True and False to booleans
            For i = 1 To iParams
                Select Case i
                    Case 1
                        vParam1 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 2
                        vParam2 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 3
                        vParam3 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 4
                        vParam4 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 5
                        vParam5 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 6
                        vParam6 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 7
                        vParam7 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 8
                        vParam8 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 9
                        vParam9 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 10
                        vParam10 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                End Select
            Next i

            'execute method
            On Error Resume Next
            Select Case iParams
                Case 0
                    CallByName(oObject, xProcName, vbMethod)
                Case 1
                    CallByName(oObject, xProcName, vbMethod, vParam1)
                Case 2
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2)
                Case 3
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3)
                Case 4
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4)
                Case 5
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5)
                Case 6
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6)
                Case 7
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7)
                Case 8
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8)
                Case 9
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8, vParam9)
                Case 10
                    CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8, vParam9, vParam10)
            End Select

            If Err.Number <> 0 Then
                For i = 1 To iParams
                    xArgs = xArgs & vParams(i - 1) & ", "
                Next i
                If iParams > 0 Then _
                    xArgs = Left(xArgs, Len(xArgs) - 2)
                On Error GoTo ProcError
                Err.Raise(LMP.Forte.MSWord.mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & xProcName & " " & xArgs)
            End If
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub CallMacro(ByVal xMacroName As String, _
                             ByVal xArgs As String, _
                             ByVal xArgDelimiter As String)
            'calls the macro with the specified name - args is a delimited
            'string of macro arguments - must be in the correct order
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.CallMacro"
            Dim vArgs As Object
            Dim i As Short

            On Error GoTo ProcError

            On Error Resume Next

            If xArgs = "" Then
                GlobalMethods.CurWordApp.Run(xMacroName)
            Else
                On Error GoTo ProcError

                'create array of arguments
                vArgs = Split(xArgs, xArgDelimiter)

                On Error Resume Next

                'make the appropriate call for the given number of arguments
                Select Case UBound(vArgs)
                    Case 0
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0))
                    Case 1
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1))
                    Case 2
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2))
                    Case 3
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3))
                    Case 4
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4))
                    Case 5
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5))
                    Case 6
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5), vArgs(6))
                    Case 7
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5), vArgs(6), vArgs(7))
                    Case 8
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5), vArgs(6), vArgs(7), vArgs(8))
                    Case 9
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5), vArgs(6), vArgs(7), vArgs(8), _
                            vArgs(9))
                    Case 10
                        GlobalMethods.CurWordApp.Run(xMacroName, vArgs(0), _
                            vArgs(1), vArgs(2), vArgs(3), vArgs(4), _
                            vArgs(5), vArgs(6), vArgs(7), vArgs(8), _
                            vArgs(9), vArgs(10))
                    Case Else
                        Err.Raise(mpErrors.mpError_CouldNotExecuteMacro, , _
                            "<Error_TooManyParameters>" & xArgs)
                End Select
            End If

            If Err.Number > 0 Then
                On Error GoTo ProcError
                Err.Raise(mpErrors.mpError_CallByNameFailed, , _
                    "<Error_MacroExecutionFailed>" & xMacroName & " " & xArgs)
            End If

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub RaiseError(ByVal xNewSource As String)
            'raises the current error, appending the source
            Static xStack As String
            With Err()
                'clear the stack if this is a
                'new error that's being raised
                If InStr(.Source, ".") = 0 And (.Source <> "mpError") Then
                    xStack = ""
                End If

                'append current source to stack
                xStack = xStack & xNewSource & ":"

                'raise the error with the updated stack
                If .Number <> 0 Then
                    .Raise(.Number, xStack, .Description)
                Else
                    .Raise(LMP.Forte.MSWord.mpErrors.mpError_ErrorDetailLost, xStack, "<Error_ErrorDetailLost>")
                End If
            End With
        End Sub

        Public Shared Function GetFunctionValue(ByVal xServer As String, _
                                         ByVal xClass As String, _
                                         ByVal xProcName As String, _
                                         Optional ByVal vParams As Object = Nothing) As String
            'returns the value of the specified function given the specified arguments
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.GetFunctionValue"
            Dim oObject As Object
            Dim vParam1 As Object
            Dim vParam2 As Object
            Dim vParam3 As Object
            Dim vParam4 As Object
            Dim vParam5 As Object
            Dim vParam6 As Object
            Dim vParam7 As Object
            Dim vParam8 As Object
            Dim vParam9 As Object
            Dim vParam10 As Object
            Dim i As Short
            Dim iParams As Short
            Dim xArgs As String

            On Error GoTo ProcError

            'create object
            On Error Resume Next
            oObject = CreateObject(xServer & "." & xClass)
            On Error GoTo ProcError

            If oObject Is Nothing Then
                Err.Raise(LMP.Forte.MSWord.mpErrors.mpError_CouldNotCreateCOMObject, , _
                    "<Error_CreateCOMObject>" & xServer & "." & xClass)
            End If

            'get number of parameters
            If Not Information.IsNothing(vParams) Then
                iParams = UBound(vParams) + 1
            End If

            'make any necessary adjustments for CallByName,
            'e.g. converting True and False to booleans
            For i = 1 To iParams
                Select Case i
                    Case 1
                        vParam1 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 2
                        vParam2 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 3
                        vParam3 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 4
                        vParam4 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 5
                        vParam5 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 6
                        vParam6 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 7
                        vParam7 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 8
                        vParam8 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 9
                        vParam9 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                    Case 10
                        vParam10 = BaseMethods.ConvertDataTypeKeywords(vParams(i - 1))
                End Select
            Next i

            'execute method
            On Error Resume Next
            Select Case iParams
                Case 0
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod)
                Case 1
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1)
                Case 2
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2)
                Case 3
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3)
                Case 4
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4)
                Case 5
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5)
                Case 6
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6)
                Case 7
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7)
                Case 8
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8)
                Case 9
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8, vParam9)
                Case 10
                    GetFunctionValue = CallByName(oObject, xProcName, vbMethod, vParam1, vParam2, _
                        vParam3, vParam4, vParam5, vParam6, vParam7, vParam8, vParam9, vParam10)
            End Select

            If Err.Number <> 0 Then
                For i = 1 To iParams
                    xArgs = xArgs & vParams(i - 1) & ", "
                Next i
                If iParams > 0 Then _
                    xArgs = Left(xArgs, Len(xArgs) - 2)
                On Error GoTo ProcError
                Err.Raise(LMP.Forte.MSWord.mpErrors.mpError_CallByNameFailed, , _
                    "<Error_CallByNameFailed>" & xProcName & " " & xArgs)
            End If
            Exit Function
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Function
        End Function
        Public Shared Sub SetXMLMarkupState(ByVal oDocument As Word.Document, ByVal bShowXML As Boolean, Optional ByVal bResetShowAll As Boolean = False)
            ' There is a bug in Word 2007 that causes the ShowAll state to be changed when
            ' opening a new document if ShowXMLMarkup is changed to not match ShowAll
            ' Need to toggle ShowAll to reset the correct state
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.GlobalMethods.SetXMLMarkupState"
            Dim lCurTick As Integer

            On Error GoTo ProcError

            With oDocument.ActiveWindow.View
                If .ShowXMLMarkup <> bShowXML Then
                    .ShowXMLMarkup = bShowXML
                    If bResetShowAll Then
                        'JTS 6/4/10 If .ShowXMLMarkup and .ShowAll started out the same,
                        'changing .ShowXMLMarkup will also change Word's point to .ShowAll state
                        'Toggling setting brings it back in sync
                        If .ShowAll <> .ShowXMLMarkup Then
                            .ShowAll = Not .ShowAll
                            .ShowAll = Not .ShowAll
                        End If
                    End If
                End If
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Shared Sub Echo(ByVal bEcho As Boolean)
            Const mpThisFunction As String = "LMP.Forte.MSWord.GlobalMethods.Echo"

            On Error GoTo ProcError

            If Not bEcho Then
                Call LockWindowUpdate(FindWindow("OpusApp", Nothing))
            Else
                Call LockWindowUpdate(0&)
            End If

            GlobalMethods.CurWordApp.ScreenUpdating = bEcho
            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
        ''' <summary>
        ''' Get Handle of Word Window, optionally matching a specific Window title
        ''' </summary>
        ''' <param name="xCaption"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetWordHandle(Optional ByVal xCaption As String = "") As Integer
            If xCaption <> "" Then
                xCaption = xCaption & " - Microsoft Word"
            End If
            GetWordHandle = FindWindow("OpusApp", xCaption)
        End Function
        ''' <summary>
        ''' Get IWin32Window object for active Word window
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetWordWin32Window() As System.Windows.Forms.IWin32Window
            If Not GlobalMethods.CurWordApp.ActiveDocument Is Nothing Then
                GetWordWin32Window = New Win32Window(GetWordHandle(GlobalMethods.CurWordApp.ActiveDocument.ActiveWindow.Caption))
            End If
        End Function

        Private Class Win32Window
            Implements System.Windows.Forms.IWin32Window

            Private _hwnd As System.IntPtr
            Public ReadOnly Property Handle() As System.IntPtr Implements System.Windows.Forms.IWin32Window.Handle
                Get
                    Handle = _hwnd
                End Get
            End Property
            Public Sub New(iHandle As Integer)
                _hwnd = iHandle
            End Sub
        End Class
    End Class
End Namespace