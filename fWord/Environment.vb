'**********************************************************
'   LMP.Forte.MSWord.cEnvironment Class
'   created 2/25/04 by Doug Miller
'
'   'contains methods and functions that get/set Word environment
'**********************************************************
Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

'**********************************************************
Namespace LMP.Forte.MSWord
    Public Class Environment
        Private m_bShowAll As Boolean
        Private m_bShowBookmarks As Boolean
        Private m_bShowHidden As Boolean
        Private m_bShowFieldCodes As Boolean
        Private m_bShowXMLMarkup As Boolean
        Private m_bDisplayHorizontalScrollBar As Boolean
        Private m_bDisplayVerticalScrollBar As Boolean
        Private m_iView As Word.WdViewType
        Private m_sZoom As Integer
        Private m_iBrowserTarget As WdBrowseTarget
        Private m_oSelRange As Word.Range
        Private m_bSmartCutPaste As Boolean 'GLOG 7314 (dm)

        Private Const mpUndefined As Integer = -777777

        Public Sub Test()

        End Sub
        Public Sub SetExecutionState(ByVal oDocument As Word.Document, _
                                     ByVal iShowAll As Integer, _
                                     ByVal iShowHiddenText As Integer, _
                                     ByVal iShowBookmarks As Integer, _
                                     ByVal iShowFieldCodes As Integer, _
                                     ByVal iShowXMLMarkup As Integer, _
                                     ByVal iViewType As Integer)
            'sets Word application environment for Forte end-user functions as specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.cEnvironment.SetExecutionState"

            On Error GoTo ProcError

            'GLOG 15957 (dm) - many Word methods are unavailable with Find and Replace dialog open -
            'this will close it
            oDocument.Application.Dialogs(WdWordDialog.wdDialogEditReplace).Update()

            'set Word environment
            With oDocument.ActiveWindow
                .DisplayHorizontalScrollBar = True
                .DisplayVerticalScrollBar = True
                With .View
                    'GLOG 8056: Make sure we're not in Print Preview mode for editing
                    If iViewType <> mpUndefined Then
                        .Type = iViewType
                    ElseIf .Type = Word.WdViewType.wdPrintPreview Then
                        .Type = Word.WdViewType.wdPrintView
                    End If

                    If iShowAll <> mpUndefined Then
                        .ShowAll = iShowAll
                    End If
                    If iShowHiddenText <> mpUndefined Then
                        .ShowHiddenText = iShowHiddenText
                    End If
                    If iShowBookmarks <> mpUndefined Then
                        .ShowBookmarks = iShowBookmarks
                    End If
                    If iShowFieldCodes <> mpUndefined Then
                        .ShowFieldCodes = iShowFieldCodes
                    End If
                    If iShowXMLMarkup <> mpUndefined Then
                        GlobalMethods.SetXMLMarkupState(oDocument, iShowXMLMarkup = 1)
                        'JTS 6/4/10
                        If .ShowAll <> .ShowXMLMarkup Then
                            .ShowAll = Not .ShowAll
                            .ShowAll = Not .ShowAll
                        End If
                    End If
                End With
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub SaveState(ByVal oDocument As Word.Document)
            'gets values from Word environment
            Const mpThisFunction As String = "LMP.Forte.MSWord.cEnvironment.SaveState"
            On Error GoTo ProcError
            With oDocument.ActiveWindow
                m_bDisplayHorizontalScrollBar = .DisplayHorizontalScrollBar
                m_bDisplayVerticalScrollBar = .DisplayVerticalScrollBar
                On Error Resume Next
                With .View
                    m_iView = .Type
                    m_sZoom = .Zoom.Percentage
                    m_bShowAll = .ShowAll
                    m_bShowBookmarks = .ShowBookmarks
                    m_bShowHidden = .ShowHiddenText
                    m_bShowFieldCodes = .ShowFieldCodes
                    m_bShowXMLMarkup = .ShowXMLMarkup
                End With
            End With

            With oDocument.Application
                On Error Resume Next
                m_iBrowserTarget = .Browser.Target
                On Error GoTo ProcError
                m_oSelRange = .Application.Selection.Range

                'GLOG 7314 (dm)
                m_bSmartCutPaste = .Options.SmartCutPaste
            End With

            Exit Sub
ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub

        Public Sub RestoreState(ByVal oDocument As Word.Document, _
                                Optional ByVal bSelect As Boolean = False, _
                                Optional ByVal bClearUndo As Boolean = True, _
                                Optional ByVal bSeekMain As Boolean = True, _
                                Optional ByVal bApplicationStateOnly As Boolean = False)
            'applies all values to Word environment-
            'ignores doc-specific properties if specified
            Const mpThisFunction As String = "LMP.Forte.MSWord.cEnvironment.RestoreState"
            On Error GoTo ProcError

            If Not bApplicationStateOnly Then
                With oDocument.ActiveWindow
                    On Error Resume Next
                    If .DisplayHorizontalScrollBar <> m_bDisplayHorizontalScrollBar Then _
                        .DisplayHorizontalScrollBar = m_bDisplayHorizontalScrollBar
                    If .DisplayVerticalScrollBar <> m_bDisplayVerticalScrollBar Then _
                        .DisplayVerticalScrollBar = m_bDisplayVerticalScrollBar
                    With .View
                        If bSeekMain Then
                            If .Type <> Word.WdViewType.wdPrintView Then
                                .Type = Word.WdViewType.wdPrintView
                            End If
                            .SeekView = Word.WdSeekView.wdSeekMainDocument
                        End If
                        If .Type <> m_iView Then
                            .Type = m_iView
                        End If
                        If .Zoom.Percentage <> m_sZoom Then _
                            .Zoom.Percentage = m_sZoom
                        If .ShowAll <> m_bShowAll Then _
                            .ShowAll = m_bShowAll
                        If .ShowHiddenText <> m_bShowHidden Then _
                            .ShowHiddenText = m_bShowHidden
                        If .ShowBookmarks <> m_bShowBookmarks Then _
                            .ShowBookmarks = m_bShowBookmarks
                        If .ShowFieldCodes <> m_bShowFieldCodes Then _
                            .ShowFieldCodes = m_bShowFieldCodes
                        If .ShowXMLMarkup <> m_bShowXMLMarkup Then
                            .ShowXMLMarkup = m_bShowXMLMarkup
                            'JTS 6/4/10: This was previously checking for equality instead of inequality.
                            'Details of this bug: If state of .ShowXMLMarkup and .ShowAll are the same
                            'Changing .ShowXMLMarkup will also change Word's memorized setting for
                            '.ShowAll, so that next document opened won't match current document
                            'Toggling .ShowAll back and forth brings this back in sync
                            'fixes a Word bug - GLOG #1109
                            If .ShowAll <> .ShowXMLMarkup Then
                                .ShowAll = Not .ShowAll
                                .ShowAll = Not .ShowAll
                            End If
                        End If
                    End With
                End With

                If bClearUndo Then
                    oDocument.UndoClear()
                End If
            End If

            With oDocument.Application
                On Error Resume Next
                .Browser.Target = m_iBrowserTarget
                On Error GoTo ProcError
                If bSelect Then
                    'GLOG 5669: If original selection is in header/footer,
                    'seek appropriate story before reselecting,
                    'to avoid view switching to Draft view
                    Dim oSelSeek As WdSeekView
                    If .Application.ActiveDocument.ActiveWindow.View.Type = Word.WdViewType.wdPrintView Then
                        Select Case m_oSelRange.StoryType
                            Case Word.WdStoryType.wdFirstPageFooterStory
                                oSelSeek = Word.WdSeekView.wdSeekFirstPageFooter
                            Case Word.WdStoryType.wdFirstPageHeaderStory
                                oSelSeek = Word.WdSeekView.wdSeekFirstPageHeader
                            Case Word.WdStoryType.wdPrimaryFooterStory
                                oSelSeek = Word.WdSeekView.wdSeekPrimaryFooter
                            Case Word.WdStoryType.wdPrimaryHeaderStory
                                oSelSeek = Word.WdSeekView.wdSeekPrimaryHeader
                            Case Word.WdStoryType.wdEvenPagesFooterStory
                                oSelSeek = Word.WdSeekView.wdSeekEvenPagesFooter
                            Case Word.WdStoryType.wdEvenPagesHeaderStory
                                oSelSeek = Word.WdSeekView.wdSeekEvenPagesHeader
                            Case Else
                                oSelSeek = 0
                        End Select
                        If oSelSeek > 0 Then
                            'GLOG item #6551 - dcf
                            On Error Resume Next
                            .Application.ActiveDocument.ActiveWindow.View.SeekView = oSelSeek
                            On Error GoTo ProcError
                        End If
                    End If

                    'GLOG item #6551 - dcf
                    On Error Resume Next
                    m_oSelRange.Select()
                    On Error GoTo ProcError
                End If

                'GLOG 7314 (dm)
                .Options.SmartCutPaste = m_bSmartCutPaste
            End With

            Exit Sub

ProcError:
            GlobalMethods.RaiseError(mpThisFunction)
            Exit Sub
        End Sub
    End Class
End Namespace
