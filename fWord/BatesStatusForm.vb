﻿Public Class BatesStatusForm
    Public JobCanceled As Boolean
    Private m_xDescription As String = "Labels"
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.JobCanceled = True
    End Sub

    Private Sub frmBatesStatus_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.JobCanceled = False
        lblStatus.Text = "Click Cancel Job to halt processing." & vbCr & vbCr & _
               "To create more than 2500 labels it will be faster to run several smaller batches of labels."
        Me.CenterToParent()
    End Sub
    Public Property Description
        Get
            Return m_xDescription
        End Get
        Set(value)
            If value = "" Then
                value = "Labels"
            End If
            m_xDescription = value
            Me.Text = "Creating " & m_xDescription & "..."
        End Set
    End Property
End Class