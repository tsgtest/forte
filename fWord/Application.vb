'**********************************************************
'   LMP.Forte.MSWord.Application Class
'   created 2/26/04 by Doug Miller
'
'   'contains methods and functions that operate in COM
'**********************************************************
Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports System.Runtime.InteropServices

Namespace LMP.Forte.MSWord
    Public Class Application
        <DllImport("User32.dll")> _
        Public Shared Function LockWindowUpdate(ByVal hwndLock As Integer) As Integer
        End Function

        <DllImport("User32.dll")> _
        Public Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As Integer) As Integer
        End Function

        Public Shared Function GetFunctionValue(ByVal xServer As String, _
                                         ByVal xClass As String, _
                                         ByVal xProcName As String, _
                                         Optional ByVal vParams As Object = Nothing) As String
            'returns the value of the specified function given the specified arguments
            GetFunctionValue = GlobalMethods.GetFunctionValue(xServer, xClass, _
                xProcName, vParams)
        End Function

        Public Shared Function Encrypt(ByVal xText As String, Optional ByVal xPassword As String = "") As String
            If xText = "" Then
                Encrypt = xText
            Else
                Encrypt = BaseMethods.Encrypt(xText, xPassword)
            End If
        End Function

        Public Shared Function Decrypt(ByVal xText As String, Optional ByRef xPassword As String = "") As String
            If xText = "" Then
                Decrypt = xText
            ElseIf IsEncrypted(xText) Then
                Decrypt = BaseMethods.Decrypt(xText, xPassword)
            Else
                Decrypt = xText
            End If
        End Function
        Public Shared Function IsEncrypted(xText As String) As Boolean
            'GLOG : 8239 : ceh
            If System.String.IsNullOrEmpty(xText) Then
                Return False
            End If

            If xText.Length < 3 Then
                Return False
            End If

            If xText.Substring(0, 3) = "^`~" Then
                'new encryption marker
                Return True
            End If

            If xText.Substring(0, 1) = ChrW(2514).ToString() Then
                'old encryption markers
                Return ((xText.Substring(1, 1) = ChrW(2524).ToString()) Or _
                    (xText.Substring(1, 1) = ChrW(2518).ToString()))
            Else
                Return False
            End If
        End Function


        Public Shared Sub SuspendCompression(ByVal bSuspend As Boolean)
            GlobalMethods.g_bSuspendCompression = bSuspend
        End Sub

        Public Shared Sub DisableEncryption(ByVal bDisable As Boolean)
            GlobalMethods.g_bDisableEncryption = bDisable
        End Sub

        Public Shared Sub SetClientMetadataValues(ByVal xTagPrefixID As String, ByVal xEncryptionPassword As String)
            GlobalMethods.g_xClientTagPrefixID = xTagPrefixID
            GlobalMethods.g_xEncryptionPassword = xEncryptionPassword
        End Sub

        Public Shared Function ConvertDeletedScopesToOpenXml(ByVal xXML As String, ByVal oDoc As Word.Document) As String
            Dim oDeleteScope As DeleteScope
            oDeleteScope = New DeleteScope
            ConvertDeletedScopesToOpenXml = oDeleteScope.ConvertToOpenXml(xXML, oDoc)
        End Function

        Public Shared Sub SetBase64Encoding(ByVal bOn As Boolean)
            GlobalMethods.g_bBase64Encoding = bOn
        End Sub
        'GLOG 5908
        Public Shared Function ScreenFonts() As Object
            'Returns unsorted list of system fonts
            Dim vFonts As Object
            Dim i As Short
            Dim iFontCount As Short
            ReDim vFonts(0 To System.Drawing.FontFamily.Families.Length)
            iFontCount = -1
            For i = 0 To System.Drawing.FontFamily.Families.Length - 1
                'Omit Vertical font names starting with "@"
                If Left(System.Drawing.FontFamily.Families(i).Name, 1) <> "@" Then
                    iFontCount = iFontCount + 1
                    vFonts(iFontCount) = System.Drawing.FontFamily.Families(i).Name
                End If
            Next i
            ReDim Preserve vFonts(0 To iFontCount)
            ScreenFonts = vFonts
        End Function
        'GLOG 7496 (dm)
        Public Shared Sub SetTrailerInProgressFlag(ByVal bOn As Boolean)
            GlobalMethods.g_bTrailerInProgress = bOn
        End Sub
        Public Shared Sub SelectTaskPane()
            'selects the task pane
            If GlobalMethods.CurWordApp.Version = "11.0" Then
                'Set focus to the currently active TaskPane panel
                On Error Resume Next
                GlobalMethods.CurWordApp.Application.CommandBars("Task Pane").Controls(1).SetFocus()
                On Error GoTo 0
            Else
                System.Windows.Forms.SendKeys.Send("{F10}(^{Tab})")
            End If
        End Sub

        Public Shared Sub ToggleXMLCodes()
            GlobalMethods.SetXMLMarkupState(GlobalMethods.CurWordApp.ActiveDocument, Not GlobalMethods.CurWordApp.ActiveWindow.View.ShowXMLMarkup, True)
        End Sub

        Public Shared Sub NewForteDocument()
            Const mpThisFunction As String = "LMP.Forte.MSWord.cUICommands.NewForteDocument"
            On Error GoTo ProcError
            Dim oWordApp As WordApp
            oWordApp = New WordApp
            oWordApp.CreateForteDocument()
            Exit Sub
ProcError:
            oWordApp.ShowError()
        End Sub
        ''' <summary>
        ''' returns true iff Word is currently running
        ''' </summary>
        Public Shared Function WordIsRunning() As Boolean
            Try
                Return Not LMP.Forte.MSWord.GlobalMethods.CurWordApp Is Nothing
            Catch
                Return False
            End Try
        End Function

        ''' <summary>
        ''' refreshes the Word window
        ''' </summary>
        Public Shared Sub RefreshScreen()
            Dim oApp As Word.Application = LMP.Forte.MSWord.GlobalMethods.CurWordApp
            Dim bFrozen As Boolean = oApp.ScreenUpdating = False

            If bFrozen Then
                LockWindowUpdate(0)
                oApp.ScreenUpdating = True
                oApp.ScreenRefresh()
                oApp.ScreenUpdating = False
                LockWindowUpdate(FindWindow("OpusApp", 0))
            Else
                oApp.ScreenRefresh()
            End If
        End Sub
        Public Shared Function GetActivePrinterName() As String
            Dim oPrinter As LMP.Forte.MSWord.Printer = New LMP.Forte.MSWord.Printer()
            Try
                Return oPrinter.GetPrinterName()
            Catch

            End Try

            'Return empty string if error occurred
            Return ""
        End Function
        Public Shared Sub GetPrinterBinNamesAndIDs(ByRef arTrays As String(), ByRef arIDs As Integer())
            'GLOG 4752: Use shared function for uniform error-handling
            arTrays = {}
            arIDs = {}

            'does not appear to need this conditional, as neither GetBinNames()
            'nor GetBinNumbers() need Word to be open
            'If (WordIsRunning()) Then
            Dim oPrinter As LMP.Forte.MSWord.Printer = New LMP.Forte.MSWord.Printer()
            Try
                oPrinter.GetBinNamesAndNumbers(arTrays, arIDs)
            Catch

            End Try
            'End If
            'GLOG 8642: Show default built-in options if no printer-specific bins returned
            If (arTrays.Count() = 0 Or arIDs.Count() = 0) Then
                'GLOG 4752: If there was an error retrieving printer properties
                'or Word is not running, display Word's built-in Default Bin and Manual Feed options
                arTrays = {"Default", "Manual Feed"}
                arIDs = {1, 4}
            End If
        End Sub

        Public Shared Function GetPrinterBinName(ByVal iBinID As Integer) As String
            Dim arTrays As String() = {}
            Dim arIDs As Integer() = {}

            GetPrinterBinNamesAndIDs(arTrays, arIDs)
            If (arIDs.Count > 0) Then
                For i As Integer = 0 To arIDs.Length - 1
                    If (arIDs(i) = iBinID) Then
                        Return arTrays(i)
                    End If
                Next i
            End If

            'No match found
            Return ""
        End Function

        '''' <summary>
        '''' refreshes the Word window
        '''' </summary>
        'public static void RefreshScreen()
        '{
        '    Word.Application oApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
        '    bool bFrozen = oApp.ScreenUpdating == false;

        '    If (bFrozen) Then
        '    {
        '        LMP.WinAPI.LockWindowUpdate(0);
        '        oApp.ScreenUpdating = true;
        '        oApp.ScreenRefresh();
        '        oApp.ScreenUpdating = false;
        '        LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
        '    }
        '    Else
        '        oApp.ScreenRefresh();
        '}
        'public static void GetPrinterBinNamesAndIDs(ref string[] arTrays, ref int[] arIDs)
        '{
        '    //GLOG 4752: Use shared function for uniform error-handling
        '    arTrays = new string[0];
        '    arIDs = new int[0];
        '        If (WordIsRunning) Then
        '    {
        '        LMP.Forte.MSWord.Printer oPrinter = new LMP.Forte.MSWord.Printer();
        '            Try
        '        {
        '            arTrays = oPrinter.GetBinNames();
        '            arIDs = oPrinter.GetBinNumbers();
        '            return;
        '        }
        '            Catch
        '        {
        '        }
        '    }
        '    //GLOG 4752: If there was an error retrieving printer properties
        '    //or Word is not running, display Word's built-in Default Bin and Manual Feed options
        '    arTrays = new string[] { "Default Bin", "Manual Feed" };
        '    arIDs = new int[] { 1, 4 };
        '}
        'public static string GetPrinterBinName(int iBinID)
        '{
        '    string[] arTrays = null;
        '    int[] arIDs = null;
        '    GetPrinterBinNamesAndIDs(ref arTrays, ref arIDs);
        '    if (arIDs != null)
        '    {
        '        for (int i = 0; i < arIDs.Length; i++)
        '        {
        '            if (arIDs[i] == iBinID)
        '                return arTrays[i];
        '        }
        '    }
        '    //No match found
        '    return "";
        '}
    End Class
End Namespace
