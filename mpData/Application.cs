using System;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using OLEDB = System.Data.OleDb;
using System.IO;
using System.Configuration;
using LMP;
using System.Reflection;    //GLOG : 8819 : ceh
using TSG.CI;
using System.Xml;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;

namespace LMP.Data
{
    #region *********************enumerations*********************
    public enum mpDatabaseTypes : byte
	{
		Jet = 1,
		SQLServer = 2
	}
    public enum mpSegmentFindMode : byte
    {
        MatchSubString = 1,
        MatchStart = 2,
        MatchExact = 3
    }
    [Flags]
    public enum mpSegmentFindTypes
    {
        AdminSegments = 1,
        UserSegments = 2,
        Prefills = 4,
        SegmentPackets = 8,
        UserSegmentPackets = 16,
        StyleSheets = 32,
        UserStyleSheets = 64,
        Preferences = 128,
        MasterData = 256 //GLOG 8037
    }
    [Flags] //GLOG 8037
    public enum mpSegmentFindFields
    {
        Name = 1,
        Description = 2,
        FolderName = 4 //GLOG 8162
    }
    public enum mpLocationTypes
    {
        Countries = 1,
        States = 2,
        Counties = 3
    }
    public enum mpObjectTypes : short
	{
		Letter = 1,
		Memo = 2,
		Fax = 3,
		Architect = 4,
		Pleading = 5,
		Service = 6,
		Notary = 7,
		Verification = 8,
		DepoSummary = 9,
		BatesLabels = 10,
		Agreement = 11,
        MasterData = 95,
        SegmentPacket = 96,
        SavedContent = 97,
        NonMacPacContent = 98,
		UserSegment = 99,
		Segment = 100,
		ContentFolder = 101,
		FavoriteTemplate = 102,
        Segments = 103,
        VariableSet = 104,
		NumberingScheme = 105,
		CIGroup = 106,
        UserFolderMembers = 107,
        UserFolders = 108,
        AdminFolderMembers = 109,
        Person = 200,
		Group = 201,
		Proxy = 202,
		User = 203,
        People = 204,
		Alias = 205,
        Proxies = 206,
		Office = 300,
		Courier = 301,
		Court = 302,
		DateFormat = 303,
		DateFormatGroup = 304,
		OfficeAddressFormat = 305,
        Couriers = 306,
        Courts = 307,
        Offices = 308,
        ExternalConnections = 400,
		Trailer = 401,
		Envelopes = 402,
		Labels = 403,
		Letterhead = 404,
        KeySets = 405,
        Lists = 410,
        Restrictions = 415,
        Translations = 420,
        ValueSets = 430,
        VariableSets = 440,
        Permissions = 450,
        PleadingCaption = 500,
		PleadingCounsel = 501,
		PleadingSignature = 502,
		PleadingPaper = 503,
		PleadingCaptionBorderSet = 504,
		PleadingCoverPage = 505,
		PleadingCaptions = 506,
		PleadingCounsels = 507,
		PleadingSignatures = 508,
        LetterSignature = 509,
        DraftStamp = 510,
        Sidebar = 511,
        ServiceList=512,
        AgreementSignature=513,
        AgreementExhibit=514,
        AgreementTitlePage=515,
        TOA=516,
        PleadingExhibit=517,
        Jurisdictions0 = 520,
        Jurisdictions1 = 521,
        Jurisdictions2 = 522,
        Jurisdictions3 = 523,
        Jurisdictions4 = 524,
        CollectionTableItem = 525,
        CollectionTable = 526,
        LetterSignatures = 527,
        AgreementSignatures = 528,
        PleadingSignatureNonTable = 529,
        LetterSignatureNonTable = 530,
        AgreementSignatureNonTable = 531,
        Addresses = 600,
        AddressFormats = 601,
        Assignments = 610,
        AttorneyLicenses = 620,
        Counties = 630,
        Countries = 631,
        States = 632,
        GroupAssignments = 640,
        FavoritePeople = 641,
        ServiceListSeparatePage = 642, //GLOG 6094
        LitigationBack = 643,    //GLOG 7154
        Aliases = 644,  //GLOG : 8031 : ceh
        NumberedLabels = 645, //GLOG 15750
        //GLOG 15915
        DisclaimerSidebar = 646,
        DisclaimerText = 647,
        LogoSidebar = 648
    }
    public enum mpSegmentIntendedUses
    {
        AsDocument = 1,
        AsDocumentComponent = 2,
        AsParagraphText = 3, 
        AsStyleSheet = 4,
        AsAnswerFile = 5,
        AsSentenceText = 6,
        AsMasterDataForm = 7
    }

    public enum mpControlTypes : int
    {
        None = 0,
        AuthorSelector = 1,
        MultilineCombo = 2,
        DetailGrid = 3,
        ClientMatterSelector = 4,
        SalutationCombo = 5,
        RelineGrid = 6,
        Textbox = 7,
        Checkbox = 8,
        Combo = 9,
        List = 11,
        DetailList = 12,
        MultilineTextbox = 14,
        DropdownList = 15,
        SegmentChooser = 16,
        DateCombo = 17,
        Spinner = 18,
        PeopleDropdown = 19,
        Calendar = 20,
        FontList = 21,
        LocationsDropdown = 22,
        NameValuePairGrid = 23,
        JurisdictionChooser = 24,
        StartingLabelSelector = 25,
        PaperTraySelector = 26
    }

    public enum mpUserDataScopes : byte
	{
		AllItems = 1,
		AccessibleItems = 2,
		OwnedItems = 3
	}

	public enum mpDirectories: byte
	{
		Bin = 1,
		Data = 2,
		Personal = 3,
		WordUser = 4,
		WordWorkgroup = 5,
		WordStartup = 6,
        Templates = 7,
        Help = 8,
        DB = 9,
        UserCache = 10,
        RootDirectory = 11,
        CIData = 12,
        NumberingData = 13,
        SegmentsDB = 14,
        WritableDB = 15
	}

	public enum mpSharedDataItems: byte
	{
		UserSegments = 1,
		VariableSets = 2,
		NumberingSchemes = 3,
		CIGroups = 4
	}

    public enum mpMeasurementUnits : int
    {
        Inches = 0,
        Centimeters = 1,
        Millimeters = 2,
        Points = 3,
        Picas = 4
    }

    public enum mpFrequencyUnits
    {
        Minute = 1,
        Hour = 2,
        Day = 3,
        Week = 4
    }

    public enum mpDayOfWeek //JTS 12/05/11: Modify values to correspond to Date.DayOfWeek enum
    {
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6
    }

    public enum mpCollectionSegmentIDs
    {
        PleadingCounsels = 959,
        PleadingCaptions = 960,
        PleadingSignatures = 961,
        //JTS 4/15/10: Separate IDs for Content Control wrappers
        PleadingCounselsCC = 962,
        PleadingCaptionsCC = 963,
        PleadingSignaturesCC = 964,
        LetterSignaturesCC = 965,
        AgreementSignaturesCC = 966,
        CollectionTableCC = 967,
        CollectionTable = -1,
        LetterSignatures = -2,
        AgreementSignatures = -3,
        PleadingCounsels2003 = 3943,
        PleadingCaptions2003 = 3944,
        PleadingSignatures2003 = 3945
    }

    public enum mpFileFormats
    {
        None = 0,
        Binary = 1,
        OpenXML = 2
    }
    #endregion

    /// <summary>
	/// Summary description for Application.
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public class Application
    {
        #region *********************fields/constants*********************
        const string mpSuperUserID = "mp10SuperUser";
        const string mpSuperUserPassword = "superfish4wade";
		internal static string g_xSysUserID = null;
		internal static LMP.Data.User g_oCurUser = null;
        internal static LMP.Data.User g_oLoginUser = null;
		private static bool g_bAdminMode = false;
        private static ICSession m_oCISession = null;
        private static string m_xDataDir;
        private static string m_xHelpDir;
        private static string m_xDBDir;
        private static string m_xWritableDBDir; //JTS 3/28/13
        private static string m_xSegmentsDBDir;
        private static string m_xCIDataDir;
        private static string m_xNumberingDataDir;
        private static string m_xBinDir;
        private static string m_xWordWorkgroupDir;
        private static string m_xWordUserDir;
        private static string m_xWordStartupDir;
        private static string m_xPersonalDir;
        private static string m_xTemplatesDir;
        private static string m_xUserCacheDir;
        private static string m_xCurrentClientTagPrefixID;
        private static string m_xEncryptionPassword;
        private static string m_xDisableEncryptionPassword;
        private static string m_xSupportedLanguages;
        private static System.Collections.Specialized.ListDictionary m_ldSupportedLanguages = null;
        private static string m_xToolkitTaskPaneSegmentTypes; //GLOG 7103 (dm)
        private static string m_xUserDBName; //GLOG 7892 (dm)
        private static string m_xAdminDBName; //GLOG 7892 (dm)
        private static string m_xPeopleDBName; //GLOG 8220
        #endregion
        #region *********************enumerations*********************
        #endregion
		#region *********************constructors*********************
        public Application(){}
		#endregion
		#region *********************static members*********************
        /// <summary>
        /// returns the ID of the segment mapped to the specified doc type
        /// </summary>
        /// <param name="xLegacyDocTypeName"></param>
        /// <returns></returns>
        public static int GetLegacyMappedSegment(string xLegacyDocTypeName)
        {
            return (int)SimpleDataCollection.GetScalar("spSegmentIDFromLegacyDocType",
                new object[] { xLegacyDocTypeName });
        }
        /// <summary>
        /// returns true iff the last sync failed
        /// </summary>
        public static bool LastSyncFailed
        {
            get
            {
                return LMP.Registry.GetCurrentUserValue(
                   LMP.Data.ForteConstants.mpSyncRegKey, "LastSyncFailed") == "1";
            }
        }
        /// <summary>
        /// returns the xml of the firm normal template
        /// </summary>
        /// <returns></returns>
        public static string GetFirmNormalTemplateXML()
        {
            return SimpleDataCollection.GetScalar("spGetNormalTemplateXML", new object[] { }).ToString();
        }
        /// <summary>
        /// returns the LastEditTime of the firm Normal template XML segment record
        /// </summary>
        /// <returns></returns>
        public static DateTime GetFirmNormalLastEditTime()
        {
            const string SQL = "SELECT LastEditTime FROM Segments WHERE Name = '****MP10NormalTemplate****'";
            string xTime = SimpleDataCollection.GetScalar(SQL).ToString();
            //GLOG 4311: Don't force U.S. English Culture, as this will fail if local date/time uses dd/MM/yy format
            return DateTime.Parse(xTime);
        }
        public static bool AdminDBExists
        {
            get { return File.Exists(LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName); }
        }

        public string [] InterrogatoriesTitles
        {
            get
            {
                ArrayList alxInterrogatoriesTitles = new ArrayList();
                // JAB TODO: Access the interrogatories and populate this array.
                string [] axInterrogatoriesTitles = (string [])alxInterrogatoriesTitles.ToArray();
                return null;
            }
        }
        public static LMP.Data.mpFileFormats GetBoundingObjectType(string xXML)
        {
            DateTime t0 = DateTime.Now;

            LMP.Data.mpFileFormats iType = mpFileFormats.None;

            XmlDocument oDocXML = new XmlDocument();
            oDocXML.LoadXml(xXML);
            XmlNamespaceManager oNSMan = new XmlNamespaceManager(oDocXML.NameTable);

            if (String.IsWordOpenXML(xXML))
            {
                //add namespaces for XPath navigation
                oNSMan.AddNamespace("w", LMP.Data.ForteConstants.WordOpenXMLNamespace);

                //check for content control with mp tag
                bool bContainsMPCC = oDocXML.SelectSingleNode(
                    "descendant::w:sdt/w:sdtPr/w:tag[starts-with(@w:val, \"mp\")]", oNSMan) != null;

                if (bContainsMPCC)
                {
                    iType = mpFileFormats.OpenXML;
                }
                else
                {
                    //check for mp xml tags
                    bool bContainsMPTags = oDocXML.SelectSingleNode(
                        "descendant::w:customXml[@w:uri=\"urn-legalmacpac-data/10\"]", oNSMan) != null;

                    if (bContainsMPTags)
                    {
                        iType = mpFileFormats.Binary;
                    }
                    else
                    {
                        //leave as no bounding type
                    }
                }
            }
            else
            {
                string xPrefix = String.GetNamespacePrefix(xXML, LMP.Data.ForteConstants.MacPacNamespace);

                //add namespaces for XPath navigation
                oNSMan.AddNamespace(xPrefix, LMP.Data.ForteConstants.MacPacNamespace);

                //check for mp xml tags
                bool bContainsMPTags = oDocXML.SelectSingleNode(
                    "descendant::" + xPrefix + ":*", oNSMan) != null;

                if (bContainsMPTags)
                {
                    iType = mpFileFormats.Binary;
                }
                else
                {
                    //leave as no bounding type
                }
            }

            LMP.Benchmarks.Print(t0);
            return iType;
        }
        public static string GetObjectTypeDisplayName(mpObjectTypes iType){
            switch(iType){
                case mpObjectTypes.Architect:
                    return "Segment";
                case mpObjectTypes.AddressFormats:
                    return "Address Formats";
                case mpObjectTypes.AdminFolderMembers:
                    return "Admin Folder Members";
                case mpObjectTypes.AttorneyLicenses:
                    return "Attorney Licenses";
                case mpObjectTypes.BatesLabels:
                    return "Bates Labels";
                case mpObjectTypes.NumberedLabels: //GLOG 15750
                    return "Numbered Labels";
                case mpObjectTypes.AgreementTitlePage:
                    return "Agreement Title Page";
                case mpObjectTypes.ContentFolder:
                    return "Folder";
                case mpObjectTypes.DateFormat:
                    return "Date Format";
                case mpObjectTypes.DateFormatGroup:
                    return "Date Format Group";
                case mpObjectTypes.DepoSummary:
                    return "Deposition Summary";
                case mpObjectTypes.DraftStamp:
                    return "Draft Stamp";
                case mpObjectTypes.ExternalConnections:
                    return "External Connections";
                case mpObjectTypes.FavoriteTemplate:
                    return "Favorite Template";
                case mpObjectTypes.GroupAssignments:
                    return "Group Assignments";
                case mpObjectTypes.LetterSignature:
                    return "Letter Signature";
                case mpObjectTypes.NumberingScheme:
                    return "Numbering Scheme";
                case mpObjectTypes.OfficeAddressFormat:
                    return "Office Address Format";
                case mpObjectTypes.PleadingCaption:
                    return "Pleading Caption";
                case mpObjectTypes.PleadingCaptionBorderSet:
                    return "Pleading Caption Border Set";
                case mpObjectTypes.PleadingCaptions:
                    return "Pleading Captions";
                case mpObjectTypes.PleadingCounsel:
                    return "Pleading Counsel";
                case mpObjectTypes.PleadingCounsels:
                    return "Pleading Counsels";
                case mpObjectTypes.PleadingCoverPage:
                    return "Pleading Cover Page";
                case mpObjectTypes.PleadingPaper:
                    return "Pleading Paper";
                case mpObjectTypes.PleadingSignature:
                    return "Pleading Signature";
                case mpObjectTypes.PleadingSignatures:
                    return "Pleading Signatures";
                case mpObjectTypes.UserFolderMembers:
                    return "User Folder Members";
                case mpObjectTypes.UserFolders:
                    return "User Folders";
                case mpObjectTypes.UserSegment:
                    return "User Segment";
                case mpObjectTypes.VariableSet:
                    return "Prefill";
                case mpObjectTypes.VariableSets:
                    return "Variable Sets";
                case mpObjectTypes.Service:
                    return "POS/COS";
                case mpObjectTypes.ServiceList:
                    return "Service List";
                case mpObjectTypes.ServiceListSeparatePage: //GLOG 6094
                    return "Service List";
                case mpObjectTypes.Trailer:
                    return "Trailer/Doc ID";
                case mpObjectTypes.CollectionTable:
                    return "Collection Table";
                case mpObjectTypes.CollectionTableItem:
                    return "Collection Table Item";
                case mpObjectTypes.LetterSignatures:
                    return "Letter Signatures";
                case mpObjectTypes.AgreementSignatures:
                    return "Agreement Signatures";
                case mpObjectTypes.PleadingSignatureNonTable:
                    return "Pleading Signature Non-Table";
                case mpObjectTypes.LetterSignatureNonTable:
                    return "Letter Signature Non-Table";
                case mpObjectTypes.AgreementSignatureNonTable:
                    return "Agreement Signature Non-Table";
                case mpObjectTypes.TOA:
                    return "Table of Authorities";
                case mpObjectTypes.AgreementSignature:
                    return "Agreement Signature";
                case mpObjectTypes.PleadingExhibit:
                    return "Pleading Exhibit";
                case mpObjectTypes.AgreementExhibit:
                    return "Agreement Exhibit";
                case mpObjectTypes.MasterData:
                    return "Master Data";
                case mpObjectTypes.LitigationBack:  //GLOG : 7154 : ceh
                    return "Litigation Back";
                default:
                    return iType.ToString();
            }
        }
        public static void InstallUpdatedDBIfNecessary()
        {
            InstallUpdatedDBIfNecessary(true);
        }
        /// <summary>
        /// installs the sync db, if it exists, as the local Forte.mdb
        /// </summary>
        public static void InstallUpdatedDBIfNecessary(bool bPrompt)
        {
            try
            {
                string xDataDir = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
                bool bSyncDBExists = File.Exists(xDataDir + @"\ForteSync.mdb");
                bool bSyncCompressedDBExists = File.Exists(xDataDir + @"\ForteSyncCompressed.mdb");
                
                if (bSyncDBExists || bSyncCompressedDBExists)
                {
                    bool bSyncInProgress = false;
                    if (bSyncDBExists)
                    {

                        WriteDBLog("InstallUpdatedDBIfNecessary", xDataDir + @"\ForteSync.mdb"); //GLOG 7546
 
                       System.Data.OleDb.OleDbConnection oSyncDB = new OleDbConnection(
                            @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDataDir +
                            @"\ForteSync.mdb;Jet OLEDB:System database=" +
                            LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"); //GLOG 6718
                        try
                        {
                            oSyncDB.Open();
                            bSyncInProgress = LMP.Data.SimpleDataCollection.GetScalar(oSyncDB,
                               "SELECT Value FROM MetaData WHERE Name=\"SyncInProgress\"").ToString() != "";
                        }
                        catch(OleDbException oDbE)
                        {
                            if (oDbE.Message.Contains("'Databases'"))
                            {
                                //GLOG 4378: If SyncDB has become corrupted during creation,
                                //error message will be:
                                //  The Microsoft Jet database engine could not find the object 'Databases'
                                //Discard corrupted ForteSync.mdb and start over
                                try
                                {
                                    File.Delete(xDataDir + @"\ForteSync.mdb");
                                }
                                catch { }
                                WriteDBLog("Corrupt Synch DB discarded"); //GLOG 7546
                                //Don't display message if called from DesktopSyncProcess
                                if (bPrompt)
                                {
                                    //Notify user about incomplete sync
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_PreviousSyncFailed"),
                                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                throw oDbE;
                            }
                        }
                        finally
                        {
                            oSyncDB.Close();
                        }
                    }
                    else
                    {
                        //Compressed DB exists - could be either because scheduled sync is running 
                        //or previous sync did not complete successfully
                        bSyncInProgress = true;
                    }
                    if (bSyncInProgress)
                    {
                        //GLOG 3684: MetaData indicates Sync is currently in progress
                        //mpDesktopSyncProcess may be running, or a previous Sync could have failed
                        bool bIsRunning = false;
                        //GLOG 3505: GetProcessesByName may return processes running in a different
                        //Terminal Server session, so check SessionID of each Process to see
                        //if there is a match for the Current Process's SessionID (will be 0 in non-TS environment)
                        int iSession = System.Diagnostics.Process.GetCurrentProcess().SessionId;
                        System.Diagnostics.Process[] aProcesses = System.Diagnostics
                            .Process.GetProcessesByName("fDesktopSyncProcess");

                        //Check if Desktop Process is currently running
                        for (int i = 0; i < aProcesses.Length; i++)
                        {
                            if (aProcesses[i].SessionId == iSession)
                            {
                                //Process is already running in this session
                                bIsRunning = true;
                                break;
                            }
                        }
                        if (!bIsRunning)
                        {
                            //sync is not running - clear incomplete ForteSync.mdb and reset Registry key
                            if (bSyncDBExists)
                            {
                                try
                                {
                                    File.Delete(xDataDir + @"\ForteSync.mdb");
                                }
                                catch { }
                            }
                            if (bSyncCompressedDBExists)
                            {
                                try
                                {
                                    File.Delete(xDataDir + @"\ForteSyncCompressed.mdb");
                                }
                                catch { }
                            }
                            LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                            if (bPrompt)
                            {
                                //Notify user about incomplete sync
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_PreviousSyncFailed"),
                                    LMP.ComponentProperties.ProductName, 
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            //Sync process is currently running - 
                            //user will be notified upon completion
                            return;
                        }
                    }
                    else
                    {

                        //GLOG 5919: Delete leftover ForteSyncCompressed.mdb
                        if (bSyncCompressedDBExists)
                        {
                            try
                            {
                                File.Delete(xDataDir + @"\ForteSyncCompressed.mdb");
                            }
                            catch { }
                        }
                        if (File.Exists(xDataDir + @"\" + LMP.Data.Application.UserDBName))
                        {
							//GLOG 7546: Test for readable ForteSync.mdb before final replacement of Forte.mdb
                            WriteDBLog("Before replacement", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                            //delete Forte.mdb
                            File.Move(xDataDir + @"\" + LMP.Data.Application.UserDBName, xDataDir + @"\Fortebak.mdb");
                            try
                            {
                                //rename sync db to Forte.mdb
                                File.Move(xDataDir + @"\ForteSync.mdb", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                            }
                            catch (System.Exception oE)
                            {
                                File.Move(xDataDir + @"\Fortebak.mdb", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                                throw new LMP.Exceptions.FileAccessException("ForteSync.mdb could not be renamed - original " +
                                    LMP.Data.Application.UserDBName + " restored", oE);
                            }
                        }
                        else
                        {
                            //copy sync db to Forte.mdb
                            File.Copy(xDataDir + @"\ForteSync.mdb", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                        }
                        //JTS 8/13/13: Test that renamed Forte.mdb is readable
                        System.Data.OleDb.OleDbConnection oTestDB = new OleDbConnection(
                            @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDataDir +
                            @"\" + LMP.Data.Application.UserDBName + ";Jet OLEDB:System database=" +
                            LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437");
                        try
                        {
                            oTestDB.Open();
                            bSyncInProgress = LMP.Data.SimpleDataCollection.GetScalar(oTestDB,
                               "SELECT Value FROM MetaData WHERE Name=\"SyncInProgress\"").ToString() != "";
                            oTestDB.Close();
                        }
                        catch (System.Exception oE)
                        {
                            oTestDB.Close();
                            //renamed ForteSync.mdb was not readable, restore original Forte.mdb
                            WriteDBLog("Backing out corrupt Sync DB replacement", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                            File.Delete(xDataDir + @"\" + LMP.Data.Application.UserDBName);
                            if (File.Exists(xDataDir + @"\Fortebak.mdb"))
                            {
                                File.Move(xDataDir + @"\Fortebak.mdb", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                                WriteDBLog("Restored original DB", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                            }
                            throw oE;
                        }
                        //New Forte.mdb is readable
                        try
                        {
                            if (File.Exists(xDataDir + @"\Fortebak.mdb"))
                                File.Delete(xDataDir + @"\Fortebak.mdb");
                            if (File.Exists(xDataDir + @"\ForteSync.mdb"))
                                File.Delete(xDataDir + @"\ForteSync.mdb");
                        }
                        catch { }
                        WriteDBLog("After replacement", xDataDir + @"\" + LMP.Data.Application.UserDBName);
                        //GLOG 5919: Reset Executing key if necessary
                        LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                    }
                }
            }
            catch (System.Exception oE)
            {
                Data.Application.WriteDBLog("Error prevented replacement of " +
                    LMP.Data.Application.UserDBName + " - " + oE.Message); //GLOG 7546
                throw new LMP.Exceptions.SyncDBException(
                    LMP.Resources.GetLangString("Error_CouldNotInstallSyncDB"), oE);
            }
        }

        public static void RenameUserDBIfSpecified()
        {
            string xDir = Data.Application.GetDirectory(mpDirectories.WritableDB);

            //if (LMP.Registry.GetMacPac10Value("RenameDB") == "1" &&
            //    File.Exists(xDir + @"\mp10.mdb") &&
            //    !File.Exists(xDir + @"\" + LMP.Data.Application.UserDBName))
            if (File.Exists(xDir + @"\mp10.mdb") &&
                !File.Exists(xDir + @"\" + LMP.Data.Application.UserDBName))
            {
                //Forte.mdb does not exist while an old mp10.mdb does -
                //rename the mp10.mdb for use with Forte
                File.Move(xDir + @"\mp10.mdb", xDir + @"\" + LMP.Data.Application.UserDBName);

                if (LMP.Registry.GetMacPac10Value("SplitUserDB") == "1")
                {
                    string xSegmentsDBDir = Data.Application.GetDirectory(mpDirectories.SegmentsDB);

                    if (!string.IsNullOrEmpty(xSegmentsDBDir) &&
                        File.Exists(xSegmentsDBDir + @"\mp10Segments.mdb"))
                    {
                        File.Move(xSegmentsDBDir + @"\mp10Segments.mdb",
                            xSegmentsDBDir + @"\ForteSegments.mdb");
                    }
                }

                LMP.Data.Application.WriteDBLog("Rename mp10.mdb to " + LMP.Data.Application.UserDBName,
                    xDir + @"\" + LMP.Data.Application.UserDBName); //GLOG 7546
            }
        }

        /// <summary>
        /// returns object data type corresponding to object name. 
        /// </summary>
        /// <param name="xObjectName"></param>
        /// <returns>mpDataType</returns>
        internal static mpObjectTypes GetObjectTypeID(string xObjectName)
        {
            switch (xObjectName)
            {
                case "UserSegment":
                case "UserSegmentDefs":
                    return mpObjectTypes.UserSegment;
                case "Segment":
                case "AdminSegmentDefs":
                    return mpObjectTypes.Segment;
                case "ContentFolder":
                case "Folders":
                    return mpObjectTypes.ContentFolder;
                case "Segments":
                    return mpObjectTypes.Segments;
                case "VariableSet":
                    return mpObjectTypes.VariableSet;
                case "UserFolderMembers":
                    return mpObjectTypes.UserFolderMembers;
                case "UserFolders":
                    return mpObjectTypes.UserFolders;
                case "Person":
                    return mpObjectTypes.Person;
                case "Group":
                case "PeopleGroups":
                    return mpObjectTypes.Group;
                case "Proxy":
                    return mpObjectTypes.Proxy;
                case "User":
                    return mpObjectTypes.User;
                case "People":
                    return mpObjectTypes.People;
                case "Alias":
                    return mpObjectTypes.Alias;
                case "Proxies":
                    return mpObjectTypes.Proxies;
                case "Office":
                    return mpObjectTypes.Office;
                case "Courier":
                    return mpObjectTypes.Courier;
                case "Court":
                    return mpObjectTypes.Court;
                case "OfficeAddressFormat":
                    return mpObjectTypes.OfficeAddressFormat;
                case "Couriers":
                    return mpObjectTypes.Couriers;
                case "Courts":
                    return mpObjectTypes.Courts;
                case "Offices":
                    return mpObjectTypes.Offices;
                case "ExternalConnections":
                    return mpObjectTypes.ExternalConnections;
                case "KeySets":
                    return mpObjectTypes.KeySets;
                case "Lists":
                    return mpObjectTypes.Lists;
                case "Restrictions":
                    return mpObjectTypes.Restrictions;
                case "Translations":
                    return mpObjectTypes.Translations;
                case "ValueSets":
                    return mpObjectTypes.ValueSets;
                case "VariableSets":
                    return mpObjectTypes.VariableSet;
                case "Permissions":
                    return mpObjectTypes.Permissions;
                case "Jurisdictions0":
                    return mpObjectTypes.Jurisdictions0;
                case "Jurisdictions1":
                    return mpObjectTypes.Jurisdictions1;
                case "Jurisdictions2":
                    return mpObjectTypes.Jurisdictions2;
                case "Jurisdictions3":
                    return mpObjectTypes.Jurisdictions3;
                case "Jurisdictions4":
                    return mpObjectTypes.Jurisdictions4;
                case "Addresses":
                    return mpObjectTypes.Addresses;
                case "AddressFormats":
                    return mpObjectTypes.AddressFormats;
                case "Assignments":
                    return mpObjectTypes.Assignments;
                case "AttorneyLicenses":
                    return mpObjectTypes.AttorneyLicenses;
                case "Counties":
                    return mpObjectTypes.Counties;
                case "Countries":
                    return mpObjectTypes.Countries;
                case "States":
                    return mpObjectTypes.States;
                case "GroupAssignments":
                    return mpObjectTypes.GroupAssignments;
                default:
                    return (mpObjectTypes)0;
            }
        }
        /// <summary>
        /// returns the name of the template used to create 
        /// all new MacPac 10 documents - documents can be 
        /// attached to other templates during the segment insertion process,
        /// but all documents are initially created using this template
        /// </summary>
        public static string BaseTemplate
        {
            get
            {
                string xFilename = @"\ForteNormal.dot";

                xFilename += "x";

                //the file can be found in either the
                //templates directory or the personal directory
                if (File.Exists(TemplatesDirectory + xFilename))
                {
                    return TemplatesDirectory + xFilename;
                }
                else
                {
                    string xPersonalDir = GetDirectory(mpDirectories.WordUser);

                    if (!File.Exists(xPersonalDir + xFilename))
                    {
                        throw new LMP.Exceptions.FileException(
                            LMP.Resources.GetLangString("Error_FileDoesNotExist") + xFilename);
                    }
                    else
                        return xPersonalDir + xFilename;
                }
            }
        }
        /// <summary>
        /// returns the email parameters set up in 
        /// the administrator for use in sending emails
        /// </summary>
        /// <returns></returns>
        public static MailServerParameters GetMailServerParameters()
        {
            MailServerParameters oParams = new MailServerParameters();
            FirmApplicationSettings oSettings = LMP.Data.Application.User.FirmSettings;

            oParams.Host = oSettings.MailHost;
            oParams.Password = oSettings.MailPassword;
            oParams.UseNetworkCredentialsToLogin = oSettings.MailUseNetworkCredentialsToLogin;
            oParams.UserID = oSettings.MailUserID;
            oParams.UseSsl = oSettings.MailUseSsl;
            oParams.Port = oSettings.MailPort;

            return oParams;
        }

		/// <summary>
		/// returns the current user of LMP.Data
		/// </summary>
		public static User User
		{
			get{return g_oCurUser;}
		}
        
		/// <summary>
		/// returns whether or not the application is in Admin Mode -
		/// Admin Mode can be set only on Login.
		/// </summary>
		public static bool AdminMode
		{
			get{return g_bAdminMode;}
		}

		/// <summary>
		/// returns the version of the this assembly
		/// </summary>
		public static string AppVersion
		{
			get
			{
				//get location of code of executing assembly
				string xAsmFileName=System.Reflection.Assembly.GetExecutingAssembly().CodeBase;

				//parse URI prefix
				xAsmFileName = xAsmFileName.Replace(@"file:///","");
			
				System.Diagnostics.FileVersionInfo oVersionInfo = 
					System.Diagnostics.FileVersionInfo.GetVersionInfo(xAsmFileName);

				return oVersionInfo.ProductVersion;
			}
		}

        /// <summary>
        /// logs into the mpData component as a specified user
        /// </summary>
        /// <param name="iUserID">MacPac User ID</param>
        /// <param name="xSystemUserID">name of current or last online system user</param>
        /// <param name="bAdminMode">log in in Admin mode</param>
        public static bool Login(string xSystemUserID, bool bAdminMode)
        {
            LocalConnection.ConnectIfNecessary(bAdminMode);

            Trace.WriteNameValuePairs("xSystemUserID", xSystemUserID, 
                "bAdminMode", bAdminMode);

            Person oPerson = null;
            
            //store AdminMode for use
            g_bAdminMode = bAdminMode;


            //GLOG 8316:  Make sure Local People table structure matches Public People structure
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                System.Collections.Generic.List<string> aCustomFlds = PublicPersons.GetPublicPeopleCustomFieldList(LMP.Data.LocalConnection.ConnectionObject);
                LMP.Data.Application.UpdatePeopleTableStructure(LMP.Data.LocalConnection.ConnectionObject.DataSource, aCustomFlds);
            }

            //GLOG : 7998 : ceh
            if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                oPerson = LocalPersons.CreateGuestPerson(xSystemUserID);
            }
            else
            {
                //test that person exists
                oPerson = Person.PersonFromSystemUserName(xSystemUserID);

                if (LMP.MacPac.MacPacImplementation.IsFullLocal && oPerson == null)
                {
                    //no person record having that system user name -
                    //attempt to copy person from People table in FortePublic.mdb
                    try
                    {
                        Person oPersonImport = PublicPersons.ItemFromSystemUserName(xSystemUserID);
                        //Don't copy record if Admin Mode and User doesn't have Admin Access
                        //GLOG 8316
                        if (oPersonImport != null && ( !bAdminMode || Person.HasAdminAccess(oPersonImport.ID1, 0, false))) //GLOG 8289
                        {
                            LocalPersons oLocalPersons = new LocalPersons();
                            oLocalPersons.CopyPublicUser(oPersonImport.ID1, false, false);

                            //test that person was copied
                            oPerson = Person.PersonFromSystemUserName(xSystemUserID);
                        }
                    }
                    catch (LMP.Exceptions.NotInCollectionException)
                    {
                        oPerson = null;
                    }
                }
            }
            //GLOG 2879: no guest person support yet - DF
            if (oPerson == null)
            {
                throw new LMP.Exceptions.InvalidUserException(
                    string.Format(LMP.Resources.GetLangString("Error_InvalidUser"), xSystemUserID));
            }

            //create a user for this person -
            //set the system user id of the current user - this might be
            //the actual os user (if in online mode), or will be the
            //last session system user (if in offline mode)
            g_oCurUser = new User(oPerson);

            //store this person as the login user -
            //the current user may not be the login user,
            //they may be a proxy
            g_oLoginUser = g_oCurUser;

            Trace.WriteNameValuePairs("g_oCurUser.ID", g_oCurUser.ID);

            if (bAdminMode)
            {
                ImportRibbonXMLIfNecessary();
            }

            return true;
        }

        /// <summary>
        /// Changes the current user 
        /// </summary>
        /// <param name="PersonID"></param>
        public static void SetCurrentUser(int iPersonID)
        {
            if (g_oCurUser == null)
                //must login first - alert
                throw new LMP.Exceptions.UserException(
                    LMP.Resources.GetLangString("Error_MustLoginBeforeSettingUser"));

            //get the person with the specified ID
            Person oPerson = LocalPersons.GetPersonFromID(iPersonID.ToString());

            if (oPerson != null)
            {
                g_oCurUser = new User(oPerson);
                //GLOG 3549: Refresh static FavoriteSegments object
                FavoriteSegments.RefreshFavorites();
            }
            else
                throw new LMP.Exceptions.PersonIDException(
                    LMP.Resources.GetLangString("Error_InvalidPersonID") +
                    iPersonID.ToString());
        }

        /// <summary>
        /// logs into the mpData component as a specified user
        /// </summary>
        /// <param name="iUserID">MacPac User ID</param>
        /// <param name="xSystemUserID">name of current or last online system user</param>
        /// <param name="bAdminMode">log in in Admin mode</param>
        public static bool Login(int iUserID, bool bAdminMode)
        {
            Person oPerson = null;

            LocalConnection.ConnectIfNecessary(bAdminMode);

            Trace.WriteNameValuePairs("iUserID", iUserID);

            if (iUserID == ForteConstants.mpGuestPersonID)
                oPerson = LocalPersons.CreateGuestPerson();
            else
            {
                try
                {
                    oPerson = LocalPersons.GetPersonFromIDs(iUserID, 0);
                }
                catch (System.Exception oE)
                {
                    //not a valid user - raise error
                    throw new LMP.Exceptions.UserException(
                        string.Format(LMP.Resources.GetLangString("Error_InvalidUser"), 
                        iUserID.ToString()), oE);
                }

                if (oPerson == null || oPerson.SystemUserID == "")
                {
                    //not a valid user - raise error
                    throw new LMP.Exceptions.UserException(
                        string.Format(LMP.Resources.GetLangString("Error_InvalidUser"), 
                        iUserID.ToString()));
                }
            }

            //create a user for this person
            g_oCurUser = new User(oPerson);

            //store this person as the login user -
            //the current user may not be the login user,
            //they may be a proxy
            g_oLoginUser = g_oCurUser;

            Trace.WriteNameValuePairs("g_oCurUser.ID", g_oCurUser.ID,
                "g_oCurUser.SystemUserID", g_oCurUser.SystemUserID);

            //store AdminMode for later use
            g_bAdminMode = bAdminMode;

            //store system user ID for later use
            g_xSysUserID = oPerson.SystemUserID;

            return true;
        }

        /// <summary>
        /// creates a segment containing the
        /// Admin ribbon xml if the record does
        /// not already exist
        /// </summary>
        private static void ImportRibbonXMLIfNecessary()
        {
            if (LMP.Data.Application.AdminMode && LMP.Utility.InDevelopmentMode)
            {
                string xFile = GetDirectory(mpDirectories.DB) + "\\ForteRibbon.xml";

                if (!File.Exists(xFile))
                    xFile = GetDirectory(mpDirectories.Bin) + "\\ForteRibbon.xml";

                string xXML = File.ReadAllText(xFile);

                //test to see if ForteRibbon.xml segment exists
                int iID = 0;
                
                try{
                    iID = AdminSegmentDefs.GetIDFromName("****RibbonXML****");
                }
                catch{}

                if (iID == 0)
                {
                    //create ribbon xml segment-
                    //read xml from file
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = new AdminSegmentDef();
                    oDef.DisplayName = "RibbonXML";
                    oDef.Name = "****RibbonXML****";
                    oDef.XML = xXML;
                    oDefs.Save(oDef);
                }
                else
                {
                    //check if the ribbon xml has been edited
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

                    if (oDef.XML != xXML)
                    {
                        //xml has changed - import new xml
                        oDef.XML = xXML;
                        oDefs.Save(oDef);
                    }
                }
            }
        }
        public static void PopulateHelpTextTable(bool bAdminDB) //GLOG 8107
        {
            //GLOG 8076:  Creates temporary table linking Segments.HelpText field 
            //to HTML content of external files for use by Advanced Find function
            DateTime t0 = DateTime.Now;
            const string xCONTENTS_TABLE = "SegmentsHelpContents";

            //GLOG 8107
            string xDir = null;
            string xDBName = null;

            xDir = LMP.Data.Application.WritableDBDirectory; //GLOG 8107

            
            if (bAdminDB)
                xDBName = @"\" + LMP.Data.Application.AdminDBName;
            else
                xDBName = @"\" + LMP.Data.Application.UserDBName;

            int iTries = 0;
            while (iTries++ < 2)
            {
                LMP.Trace.WriteNameValuePairs("iTries", iTries);
                try
                {
                    string xFile = "";
                    //GLOG 8107: Use MacPacPersonnel login to ensure sufficient rights
                    using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                        @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        xDir + xDBName + ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //GLOG 6718
                        @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                    {
                        oLocalDBCnn.Open();
                        try
                        {
                            //create a command to be used repeatedly below
                            using (OleDbCommand oCmd = new OleDbCommand())
                            {
                                oCmd.Connection = oLocalDBCnn;
                                //delete and recreate table at start of each session
                                oCmd.CommandText = ("DROP TABLE " + xCONTENTS_TABLE);

                                try
                                {
                                    oCmd.ExecuteNonQuery();
                                }
                                catch { }
                                System.Windows.Forms.Application.DoEvents();
                                oCmd.CommandText = "CREATE TABLE " + xCONTENTS_TABLE + " ([ID] AutoIncrement Primary Key,  [File] Text(255), [Contents] Memo, [FileDate] DateTime)";
                                oCmd.ExecuteNonQuery();
                                System.Windows.Forms.Application.DoEvents();
                                oCmd.CommandText = "INSERT INTO " + xCONTENTS_TABLE + " ( File ) SELECT DISTINCT s.HelpText FROM Segments AS s WHERE (((s.HelpText) Like 'file:///%'))";
                                oCmd.ExecuteNonQuery();
                                System.Windows.Forms.Application.DoEvents();
                                using (OleDbDataAdapter oContents = new OleDbDataAdapter())
                                {
                                    oCmd.CommandText = "SELECT * FROM " + xCONTENTS_TABLE;
                                    oContents.SelectCommand = oCmd;
                                    using (DataTable oData = new DataTable())
                                    {
                                        oContents.UpdateCommand = new OleDbCommandBuilder(oContents).GetUpdateCommand();
                                        oContents.Fill(oData);

                                        foreach (DataRow oRow in oData.Rows)
                                        {
                                            xFile = oRow["File"].ToString();
                                            if (xFile != "")
                                            {
                                                xFile = xFile.Replace("file:///", "");
                                                //If no path specified, assume Help directory
                                                if (!xFile.Contains(@"\"))
                                                    xFile = GetDirectory(mpDirectories.Help) + @"\" + xFile;
                                                if (File.Exists(xFile))
                                                {
                                                    string xContents = File.ReadAllText(xFile, Encoding.UTF8);
                                                    oRow["Contents"] = xContents;
                                                    oRow["FileDate"] = new FileInfo(xFile).LastWriteTimeUtc.ToString();
                                                }
                                            }
                                        }
                                        if (oData.GetChanges() != null)
                                            oContents.Update(oData);
                                    }
                                }
                            }
                        }
                        catch (System.Exception oE)
                        {
                            throw oE;
                        }
                        finally
                        {
                            oLocalDBCnn.Close();
                        }
                        //GLOG 8837: If we got here without error, return
                        return;
                    }
                }
                catch (OleDbException oE)
                {
                    LMP.Trace.WriteError(oE.Message);
                    //if first attempt try one more time
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE); 
                }
                finally
                {
                    LMP.Benchmarks.Print(t0);
                }
            }
        }
        public static void Logout()
        {
			//GLOG 7546:  Log DB size at Logout
            string xDB = "";
            try
            {
                if (LocalConnection.ConnectionObject != null)
                {
                    xDB = LocalConnection.ConnectionObject.DataSource;
                }
                LocalConnection.Close();
            }
            catch { }
            WriteDBLog("Logout", xDB);
        }
        public static bool LocalDBStructureUpdateRequired()
        {
            //Dummy argument - return value ignored
            DateTime oDate;
            return LocalDBStructureUpdateRequired(out oDate);
        }
        /// <summary>
        /// Returns true if there are new structure updates to be applied
        /// </summary>
        /// <returns></returns>
        public static bool LocalDBStructureUpdateRequired(out DateTime oNewFileDate)
        {
            oNewFileDate = new DateTime();
            string xLastUpdate = LMP.Data.Application.GetMetadata("LastUserDBStructureUpdate");
            
            //get file date - 
            //GLOG 8220
            string xSQLFile = "UpdateLocalDB.sql";
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                if (LMP.Data.Application.AdminMode)
                    xSQLFile = "UpdateFullLocalPublicDB.sql";
                else
                    xSQLFile = "UpdateFullLocalUserDB.sql";
            }
            FileInfo oFI = new FileInfo(LMP.Data.Application.AppDirectory + @"\" + xSQLFile);

            //exit if file doesn't exist - nothing to update
            if (!oFI.Exists)
                return false;

            //get last edit time of file
            //JTS 12/15/08 - Use UTC for consistency between time zones and Daylight/Standard time
            oNewFileDate = oFI.LastWriteTimeUtc;

            if (string.IsNullOrEmpty(xLastUpdate))
            {
                return true;
            }
            else
            {
                DateTime oLastUpdate = new DateTime(long.Parse(xLastUpdate));

                if (oLastUpdate.CompareTo(oNewFileDate) == -1)
                {
                    return true;
                }
            }
            return false;
        }
        //updates structure of local db if an updated 
        //set of sql statements exists - 
        //gets last update time from metadata table
        public static void UpdateLocalDBStructureIfNecessary(bool bAdmin)
        {

            DateTime oNewFileDate;
            if (LocalDBStructureUpdateRequired(out oNewFileDate))
            {
                LMP.Data.Application.UpdateLocalDBStructure(bAdmin);

                //write file date as last update time
                LMP.Data.Application.SetMetadata("LastUserDBStructureUpdate",
                    oNewFileDate.Ticks.ToString());
            }
        }

        /// <summary>
        /// updates the structure of the local database - 
        /// reads sql statement from text file in the bin directory -
        /// if bAdminDB is false, the Forte.mdb is updated
        /// </summary>
        public static void UpdateLocalDBStructure(bool bAdminDB)
        {
            //********************************************
            //NOTE: for this method to execute correctly,
            //the UpdateLocalDB.sql file must have entries
            //whose IDs are sequential
            //********************************************
            string xDir = null;
            string xDBName = null;
            string xSQLFile = null;

            try
            {
                DateTime t0 = DateTime.Now;

                if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    xDir = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
                    xSQLFile = @"\UpdateLocalDB.sql";
                }
                else
                {
                    //GLOG 8220
                    if (bAdminDB)
                    {
                        //ForteLocalPublic.mdb
                        xDir = LMP.Data.Application.PublicDataDirectory;
                        xSQLFile = @"\UpdateFullLocalPublicDB.sql";
                    }
                    else
                    {
                        //ForteLocal.mdb
                        xDir = LMP.Data.Application.WritableDBDirectory;
                        xSQLFile = @"\UpdateFullLocalUserDB.sql";
                    }
                }



                //read update file
                FileInfo oFI = new FileInfo(LMP.Data.Application.AppDirectory  + xSQLFile);

                //exit if file doesn't exist - nothing to update
                if (!oFI.Exists)
                    return;
                
                //read
                string xSQL = null;
                using (StreamReader oSR = oFI.OpenText())
                {
                    xSQL = oSR.ReadToEnd();
                    oSR.Close();
                }

                if (string.IsNullOrEmpty(xSQL))
                    return;

                //parse into array
                string[] aCmds = xSQL.Split(new string[]{";;"}, 
                    StringSplitOptions.RemoveEmptyEntries);

                xDBName = @"\" + LMP.Data.Application.UserDBName;

                if (bAdminDB)
                    xDBName = @"\" + LMP.Data.Application.AdminDBName;

                string xBackupDB = xDir + xDBName + ".mpbak";

                if (File.Exists(xBackupDB))
                {
                    MessageBox.Show("Could not update the local database structure.  " +
                        "A backup of the database already exists, which indicates that a previous attempt to update the structure failed.  You should restore this backup before continuing.",
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                //backup db
                File.Copy(xDir + xDBName, xDir + xDBName + ".mpbak");

                string xIDs = GetMetadata("DBStructureUpdateIDs");

                //largest ID executed
                string xLastID = "";

                if (xIDs.Contains("|"))
                {
                    //we're using the old format, which holds
                    //every ID executed - get the last one only
                    xIDs = xIDs.TrimEnd('|');
                    string[] aIDs = xIDs.Split('|');
                    xLastID = aIDs[aIDs.Length - 1];
                }
                else
                {
                    xLastID = xIDs;
                }
                
                if (string.IsNullOrEmpty(xLastID))
                    xLastID = "0";

                LMP.Trace.WriteNameValuePairs("xIDs", xIDs);

                using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDir + xDBName + ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //GLOG 6718
                    @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill")) 
                {
                    //connect to admin db
                    oLocalDBCnn.Open();

                    //cycle through array, executing each sql statement
                    foreach (string xCmd in aCmds)
                    {
                        string xModCmd = xCmd.Replace("\r\n", "");

                        if (xModCmd != "")
                        {
                            //parse out command ID
                            int iPos = xModCmd.IndexOf(".");

                            if (iPos == -1)
                            {
                                //something's wrong - no ID specified -
                                throw new LMP.Exceptions.DataException(
                                    LMP.Resources.GetLangString("Error_InvalidCommandID") + xModCmd);
                            }

                            //get ID
                            string xID = xModCmd.Substring(0, iPos);
                            
                            //check if ID has already been executed on this database
                            bool bAlreadyExecuted = int.Parse(xID)<=int.Parse(xLastID);

                            //skip if this command has already been executed on this database
                            if (bAlreadyExecuted)
                                continue;

                            xModCmd = xModCmd.Substring(iPos + 1);

                            using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                            {
                                try
                                {
                                    LMP.Trace.WriteInfo("Executing sql: " + xModCmd);

                                    //Clear existing records
                                    oCommand.CommandText = xModCmd;
                                    oCommand.CommandType = CommandType.Text;
                                    oCommand.ExecuteNonQuery();

                                    //set greatest id - note that this assumes that
                                    //the commands will have sequential IDs
                                    xLastID = xID;
                                }
                                catch (System.Exception oE)
                                {
                                    //TODO: JTS 12/12/08 - Should we just look for "DROP " in the command
                                    //here?  Not sure if error message would be the same in other language environments
                                    if ((oE.Message.ToLower() == "cannot find table or constraint." ||
                                        oE.Message.ToLower().Contains("does not exist")) &&
                                        xCmd.Contains("DROP "))
                                        continue;
                                    else
                                        throw new LMP.Exceptions.SQLStatementException(
                                            LMP.Resources.GetLangString("Error_CouldNotUpdateLocalDBStructure") + xModCmd, oE);
                                }
                            }
                        }
                    }

                    //write ID of last executed command
                    //back to the metadata table
                    SetMetadata("DBStructureUpdateIDs", xLastID);

                    oLocalDBCnn.Close();

                    //delete backup
                    File.Delete(xDir + xDBName + ".mpbak");

                    LMP.Benchmarks.Print(t0);
                }
            }
            catch (System.Exception oE)
            {
                System.Windows.Forms.MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_CouldNotUpdateLocalDBStructure"), 
                    LMP.ComponentProperties.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Error);

                throw oE;
            }
        }
        /// <summary>
        /// updates the structure of the specified database - 
        /// reads sql statement from specified text file in the bin directory
        /// </summary>
        public static void ApplyStructureUpdatesToDB(string xDB, string xSQLFile)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                //read update file
                FileInfo oFI = new FileInfo(LMP.Data.Application.AppDirectory + @"\" + xSQLFile);

                //exit if file doesn't exist - nothing to update
                if (!oFI.Exists)
                    return;

                //read
                string xSQL = null;
                using (StreamReader oSR = oFI.OpenText())
                {
                    xSQL = oSR.ReadToEnd();
                    oSR.Close();
                }

                if (string.IsNullOrEmpty(xSQL))
                    return;

                //parse into array
                string[] aCmds = xSQL.Split(new string[] { ";;" },
                    StringSplitOptions.RemoveEmptyEntries);

                string xBackupDB = xDB + ".mpbak";

                if (File.Exists(xBackupDB))
                {
                    MessageBox.Show("Could not update the local database structure.  " +
                        "A backup of the database already exists, which indicates that a previous attempt to update the structure failed.  You should restore this backup before continuing.",
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                //backup db
                File.Copy(xDB, xDB + ".mpbak");

                using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDB + ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //GLOG 6718
                    @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                {
                    //connect to admin db
                    oLocalDBCnn.Open();

                    //cycle through array, executing each sql statement
                    foreach (string xCmd in aCmds)
                    {
                        string xModCmd = xCmd.Replace("\r\n", "").Trim();

                        if (xModCmd != "")
                        {
                            using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                            {
                                try
                                {
                                    LMP.Trace.WriteInfo("Executing sql: " + xModCmd);

                                    //Clear existing records
                                    oCommand.CommandText = xModCmd;
                                    oCommand.CommandType = CommandType.Text;
                                    oCommand.ExecuteNonQuery();

                                }
                                catch (System.Exception oE)
                                {
                                    //TODO: JTS 12/12/08 - Should we just look for "DROP " in the command
                                    //here?  Not sure if error message would be the same in other language environments
                                    if ((oE.Message.ToLower() == "cannot find table or constraint." ||
                                        oE.Message.ToLower().Contains("does not exist")) &&
                                        xCmd.Contains("DROP "))
                                        continue;
                                    else
                                        throw new LMP.Exceptions.SQLStatementException(
                                            LMP.Resources.GetLangString("Error_CouldNotUpdateLocalDBStructure") + xModCmd, oE);
                                }
                            }
                        }
                    }

                    oLocalDBCnn.Close();

                    //delete backup
                    File.Delete(xDB + ".mpbak");

                    LMP.Benchmarks.Print(t0);
                }
            }
            catch (System.Exception oE)
            {
                System.Windows.Forms.MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_CouldNotUpdateLocalDBStructure"),
                    LMP.ComponentProperties.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Error);

                throw oE;
            }
        }
        /// <summary>
		/// returns the full path to the directory running this code
		/// </summary>
		public static string AppDirectory
		{
			get
			{
				//get location of code of executing assembly
				string xAsmFileName=System.Reflection.Assembly.GetExecutingAssembly().CodeBase;

				//parse URI prefix
				xAsmFileName = xAsmFileName.Replace(@"file:///","");
			
				//get connection string
				FileInfo oFile = new FileInfo(xAsmFileName);
				return oFile.Directory.FullName;
			}
		}

		/// <summary>
		/// returns the full path to the data directory
		/// </summary>
		public static string DataDirectory
		{
			get
			{
				return GetDirectory(mpDirectories.Data);
			}
		}
        //GLOG - 3459 - CEH
        /// <summary>
        /// returns the full path to the Database directory
        /// </summary>
        public static string PublicDataDirectory
        {
            get
            {
                return GetDirectory(mpDirectories.DB);
            }
        }
        public static string WritableDBDirectory
        {
            get
            {
                return GetDirectory(mpDirectories.WritableDB);
            }
        }
        public static string ForteSegmentsDBDirectory
        {
            get
            {
                return GetDirectory(mpDirectories.SegmentsDB);
            }
        }
        /// <summary>
        /// returns the full path to the Templates directory
        /// </summary>
        public static string TemplatesDirectory
        {
            get
            {
                return GetDirectory(mpDirectories.Templates);
            }
        }

        //GLOG : 6112 : CEH
        /// <summary>
        /// gets the client/tag prefix id for the current client
        /// </summary>
        public static string CurrentClientTagPrefixID
        {
            get
            {
                if (m_xCurrentClientTagPrefixID == null)
                {
                    string xClientID = GetMetadata("TagPrefixID");
                    if (xClientID != null)
                        m_xCurrentClientTagPrefixID = xClientID;
                    else
                        m_xCurrentClientTagPrefixID = "";
                }
                return m_xCurrentClientTagPrefixID;
            }
            set
            {
                m_xCurrentClientTagPrefixID = value;
            }
        }

        //GLOG : 6112 : CEH
        /// <summary>
        /// gets the password used for disabling tag data encryption-
        /// used only by MacPersonel
        /// </summary>
        public static string EncryptionPassword
        {
            get
            {
                if (m_xEncryptionPassword == null)
                {
                    string xPassword = GetMetadata("EncryptionPassword");
                    if (xPassword != null)
                        m_xEncryptionPassword = xPassword;
                    else
                        m_xEncryptionPassword = "";
                }
                return m_xEncryptionPassword;
            }
            set
            {
                m_xEncryptionPassword = value;
            }
        }

        /// <summary>
        /// gets the password used for encrypting tag data
        /// </summary>
        public static string DisableEncryptionPassword
        {
            get
            {
                if (m_xDisableEncryptionPassword == null)
                {
                    string xPassword = GetMetadata("DisableEncryptionPassword");
                    if (xPassword != null)
                        m_xDisableEncryptionPassword = xPassword;
                    else
                        m_xDisableEncryptionPassword = "";
                }
                return m_xDisableEncryptionPassword;
            }
        }

        /// <summary>
        /// returns the user database name for the implementation -
        /// added for GLOG 7892 (dm)
        /// </summary>
        public static string UserDBName
        {
            get
            {
                if (m_xUserDBName == null)
                {
                    if (LMP.MacPac.MacPacImplementation.IsToolkit)
                        m_xUserDBName = "ForteTools.mdb";
                    else if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                        //GLOG 8220 (dm) - renamed db
                        m_xUserDBName = "ForteLocal.mdb";
                    else
                        m_xUserDBName = "Forte.mdb";
                }
                return m_xUserDBName;
            }
        }
        //GLOG 8220
        public static string PeopleDBName
        {
            get
            {
                if (m_xPeopleDBName == null)
                {
                    if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                        m_xPeopleDBName = "ForteLocalPeople.mdb";
                    else if (AdminMode)
                        m_xPeopleDBName = AdminDBName;
                    else
                        m_xPeopleDBName = UserDBName;
                }
                return m_xPeopleDBName;
            }
        
        }

        /// <summary>
        /// returns the admin database name for the implementation -
        /// added for GLOG 7892 (dm)
        /// </summary>
        public static string AdminDBName
        {
            get
            {
                if (m_xAdminDBName == null)
                {
                    if (LMP.MacPac.MacPacImplementation.IsToolkit)
                        m_xAdminDBName = "ForteToolsAdmin.mdb";
                    else if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                        //GLOG 8220 (dm) - renamed db
                        m_xAdminDBName = "ForteLocalPublic.mdb";
                    else
                        m_xAdminDBName = "ForteAdmin.mdb";
                }
                return m_xAdminDBName;
            }
        }

        /// <summary>
		/// returns the full path of the specified directory
		/// </summary>
		/// <param name="bytDirectory">directory whose path this method will return</param>
		/// <returns></returns>
		public static string GetDirectory(mpDirectories bytDirectory)
		{
			//GLOG 5449:  When building the Registry key names for Office
            //need to append ".0" rather than depending on type conversion.
            //This is because regional settings may not be using '.' as the
            //decimal separator
            string xDir = null;

			Trace.WriteNameValuePairs("bytDirectory", bytDirectory);
			
			switch(bytDirectory)
			{
                case mpDirectories.RootDirectory:
                    xDir = Registry.GetMacPac10Value("RootDirectory");
                    break;
				case mpDirectories.WordWorkgroup:
				{
                    if (m_xWordWorkgroupDir == null)
                    {
                        //get version of Word
                        byte bytVer = OS.GetWinwordVersionFromRegistry();
                        //GLOG 5449
                        string xVer = bytVer.ToString() + ".0";
                        m_xWordWorkgroupDir = Registry.GetCurrentUserValue(
                            string.Concat(@"Software\Microsoft\Office\", xVer, @"\Common\General"),
                            "SharedTemplates");
                    }
                    
                    xDir = m_xWordWorkgroupDir;
					break;
				}
				case mpDirectories.WordUser:
				{
                    if (m_xWordUserDir == null)
                    {
                        //get version of Word
                        byte bytVer = OS.GetWinwordVersionFromRegistry();
                        //GLOG 5449
                        string xVer = bytVer.ToString() + ".0";
                        m_xWordUserDir = Registry.GetCurrentUserValue(
                            string.Concat(@"Software\Microsoft\Office\", xVer, @"\Common\General"),
                            "UserTemplates");
					    
                        //GLOG item #4850 - jsw
                        if (m_xWordUserDir == null)
                        {
                            //if no registry value exists, default location has not been changed.
                            //set to default
                            m_xWordUserDir = Environment.GetEnvironmentVariable("APPDATA") + "\\Microsoft\\Templates";
                        }
                    }

                    xDir = m_xWordUserDir;
					break;
				}
				case mpDirectories.WordStartup:
				{
                    if (m_xWordStartupDir == null)
                    {
                        //get version of Word
                        byte bytVer = OS.GetWinwordVersionFromRegistry();
                        //GLOG 5449
                        string xVer = bytVer.ToString() + ".0";
                        m_xWordStartupDir = Registry.GetCurrentUserValue(
                            string.Concat(@"Software\Microsoft\Office\", xVer, @"\Word\Options"),
                            "STARTUP-PATH");
                    }

                    //use out-of-the-box startup directory if reg key
                    //is empty - dcf - 09/13/11
                    if (string.IsNullOrEmpty(m_xWordStartupDir))
                    {
                        m_xWordStartupDir = @"%appdata%\Microsoft\Word\Startup";
                    }

                    xDir = LMP.OS.EvaluateEnvironmentVariables(m_xWordStartupDir);
					break;
				}
				case mpDirectories.Bin:
				{
                    if (m_xBinDir == null)
                    {
                        //get from root directory
                        m_xBinDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\bin";

                        m_xBinDir = LMP.OS.EvaluateEnvironmentVariables(m_xBinDir);
                    }

                    xDir = m_xBinDir;
					break;
				}
            //GLOG - 3459 - CEH
            //JTS 3/28/13: Mandatory location for Forte.xsd only
            case mpDirectories.Data:
                {
                    if (m_xDataDir == null)
                    {
                        //no registry path was specified - get from root directory
                        m_xDataDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "RootDirectory");

                        m_xDataDir = LMP.OS.EvaluateEnvironmentVariables(m_xDataDir) + @"\data";
                    }

                    xDir = m_xDataDir;
                    break;
                }
            //GLOG - 3459 - CEH
            //JTS 3/28/13: Optional location for Shared Data files (ie, Forte.mdb, ForteAdmin.mdb, ForteDMS.ini, ForteRibbon.xml)
            case mpDirectories.DB:
                {
                    if (m_xDBDir == null)
                    {
                        //get path from registry
                        m_xDBDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "DataDirectory");

                        if (System.String.IsNullOrEmpty(m_xDBDir))
                        {
                            //no registry path was specified - get from root directory
                            m_xDBDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\data";
                        }

                        m_xDBDir = LMP.OS.EvaluateEnvironmentVariables(m_xDBDir);
                    }

                    xDir = m_xDBDir;
                    break;
                }
            //JTS 3/28/13: Optional location for User-writable Database files (Forte.mdb, ForteAdmin.mdb)
            case mpDirectories.WritableDB:
                {
                    if (m_xWritableDBDir == null)
                    {
                        //get path from registry
                        m_xWritableDBDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "WritableDataDirectory");

                        if (System.String.IsNullOrEmpty(m_xWritableDBDir))
                        {
                            //no registry path was specified - use DB Dir
                            m_xWritableDBDir = GetDirectory(mpDirectories.DB);
                        }
                        else
                        {
                            m_xWritableDBDir = LMP.OS.EvaluateEnvironmentVariables(m_xWritableDBDir);
                            //check that segments db exists in specified location
                            if (!Directory.Exists(m_xWritableDBDir))
                            {
                                try
                                {
                                    Directory.CreateDirectory(m_xWritableDBDir);
                                }
                                catch
                                {
                                    MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                        "Error_WritableDataDirectoryNotFound"), m_xWritableDBDir),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                }
                            }
                        }
                    }
                    xDir = m_xWritableDBDir;
                    break;
                }
            case mpDirectories.SegmentsDB:
                {
                    if (m_xSegmentsDBDir == null)
                    {
                        //get path for segments db directory from registry
                        m_xSegmentsDBDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "SegmentsDBDirectory");

                        if (System.String.IsNullOrEmpty(m_xSegmentsDBDir))
                        {
                            //no path was specified for segments db - get Forte db directory
                            m_xSegmentsDBDir = GetDirectory(mpDirectories.DB);
                        }

                        m_xSegmentsDBDir = LMP.OS.EvaluateEnvironmentVariables(m_xSegmentsDBDir);

                        if (!System.String.IsNullOrEmpty(m_xSegmentsDBDir))
                        {
                            //check that segments db exists in specified location
                            if (!File.Exists(m_xSegmentsDBDir + "\\ForteSegments.mdb"))
                            {
                                //GLOG item #7818 - dcf
                                //rename mp10.mdb if necessary
                                if (File.Exists(m_xSegmentsDBDir + "\\mp10Segments.mdb"))
                                {
                                    RenameUserDBIfSpecified();
                                }
                                else
                                {
                                    MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                        "Error_SegmentsDBNotFound"), m_xSegmentsDBDir),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                }
                            }
                        }
                    }

                    xDir = m_xSegmentsDBDir;
                    break;
                }
            case mpDirectories.Help:
                {
                    if (m_xHelpDir == null)
                    {
                        //get path from registry
                        m_xHelpDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "HelpDirectory");

                        if (System.String.IsNullOrEmpty(m_xHelpDir))
                        {
                            //no registry path was specified - get from root directory
                            m_xHelpDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\help";
                        }

                        m_xHelpDir = LMP.OS.EvaluateEnvironmentVariables(m_xHelpDir);
                    }

                    xDir = m_xHelpDir;
                    break;
                }
            case mpDirectories.Personal:
				{
                    if (m_xPersonalDir == null)
                    {
                        //directory is a personal - get from keyset
                        KeySet oK = KeySet.GetFirmAppKeys(g_oCurUser.ID);

                        m_xPersonalDir = oK.GetValue("PersonalDirectory");

                        m_xPersonalDir = LMP.OS.EvaluateEnvironmentVariables(m_xPersonalDir);
                    }

                    xDir = m_xPersonalDir;
					break;
				}
                case mpDirectories.Templates:
                {
                    if (m_xTemplatesDir == null)
                    {
                        //get path from registry
                        m_xTemplatesDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "TemplatesDirectory");

                        if (System.String.IsNullOrEmpty(m_xTemplatesDir))
                        {
                            //no registry path was specified - get from root directory
                            m_xTemplatesDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\templates";
                        }

                        m_xTemplatesDir = LMP.OS.EvaluateEnvironmentVariables(m_xTemplatesDir);
                    }

                    xDir = m_xTemplatesDir;
                    break;
                }

				//GLOG item #4850 - jsw
                case mpDirectories.UserCache:
                {
                    if (m_xUserCacheDir == null)
                    {
                        //get path from registry
                        m_xUserCacheDir = Registry.GetLocalMachineValue(
                            LMP.Registry.MacPac10RegistryRoot, "UserCacheDirectory");

                        if (System.String.IsNullOrEmpty(m_xUserCacheDir))
                        {
                            //no registry path was specified - get from root directory
                            m_xUserCacheDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\Tools\UserCache";
                        }

                        m_xUserCacheDir = LMP.OS.EvaluateEnvironmentVariables(m_xUserCacheDir);
                    }

                    xDir = m_xUserCacheDir;
                    break;
                }
                //GLOG : 6112 : CEH
                case mpDirectories.CIData:
                {
                    if (m_xCIDataDir == null)
                    {
                        //get path from registry
                        m_xCIDataDir = Registry.GetLocalMachineValue(
                            LMP.Registry.CIRegistryRoot, "DataDirectory");

                        if (System.String.IsNullOrEmpty(m_xCIDataDir) || !Directory.Exists(m_xCIDataDir))
                        {
                            //no registry path was specified - get from root directory
                            //11-21-14 (dm) - this was referencing "MacPac", not "Forte"
                            //GLOG : 8082 : ceh - deal with multiple instances of the string "Forte"
                            string xRootDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory");
                            int i = xRootDir.LastIndexOf("Forte");
                            if (i >= 0)
                                m_xCIDataDir = xRootDir.Substring(0, i) + "CI\\App";
                            else
                                //something's wrong
                                throw new LMP.Exceptions.DirectoryException(
                                    LMP.Resources.GetLangString("Error_InvalidDirectory") + xRootDir);
                        }

                        m_xCIDataDir = LMP.OS.EvaluateEnvironmentVariables(m_xCIDataDir);
                    }

                    xDir = m_xCIDataDir;
                    break;
                }
                //GLOG : 6112 : CEH
                case mpDirectories.NumberingData:
                {
                    if (m_xNumberingDataDir == null)
                    {
                        //get path from registry
                        m_xNumberingDataDir = Registry.GetLocalMachineValue(
                            LMP.Registry.NumberingRegistryRoot, "DataDirectory");

                        if (System.String.IsNullOrEmpty(m_xNumberingDataDir) || !Directory.Exists(m_xNumberingDataDir))
                        {
                            //no registry path was specified - get from root directory
                            //11-21-14 (dm) - this was referencing "MacPac", not "Forte"
                            //GLOG : 8082 : ceh - deal with multiple instances of the string "Forte"
                            string xRootDir = Registry.GetLocalMachineValue(
                                LMP.Registry.MacPac10RegistryRoot, "RootDirectory");
                            int i = xRootDir.LastIndexOf("Forte");
                            if (i >= 0)
                                m_xNumberingDataDir = xRootDir.Substring(0, i) + "Numbering\\Bin"; //GLOG 8896 (dm)
                            else
                                //something's wrong
                                throw new LMP.Exceptions.DirectoryException(
                                    LMP.Resources.GetLangString("Error_InvalidDirectory") + xRootDir);

                        }

                        m_xNumberingDataDir = LMP.OS.EvaluateEnvironmentVariables(m_xNumberingDataDir);
                    }

                    xDir = m_xNumberingDataDir;
                    break;
                }
            }

            if (!Directory.Exists(xDir))
                //GLOG : 7434 : jsw
                //return null if startup path is invalid
                if (bytDirectory == mpDirectories.WordStartup)
                    xDir = null;
                else
                    throw new LMP.Exceptions.DirectoryException(
                        LMP.Resources.GetLangString("Error_DirectoryDoesNotExist") + xDir);

			return xDir;
		}
        /// <summary>
        /// returns the set of defined date formats
        /// </summary>
        /// <returns></returns>
        public static string[] GetDateFormats()
        {
            ArrayList oFormatsArray = SimpleDataCollection
                .GetArray("spDateFormats", null);
            string[] aFormats = new string[oFormatsArray.Count];
            int j = 0;
            foreach (object[] oFormat in oFormatsArray)
            {
                aFormats[j] = oFormat[0].ToString();
                aFormats[j] = aFormats[j].Replace("\"", "'");
                // Remove the prefix characters used to allow case sensitivite date formats.
                // These prefixes facilitate storing date formats that differ only in 
                // character case (eg, THIS "MMMM, DD, YYYY" vs "this mmmm, dd, yyyy")
                // The date formats are stored in a primary field and would be prevented from
                // being added to the date formats table since Access sees them as a duplicate entry.
                // If theses values exist in the table:
                //      THIS MMMM, DD, YYYY
                //      this mmmm, dd, yyyy 
                //      this MMMM, dd, YYYY
                //
                // they would be stored as:
                //      THIS MMMM, DD, YYYY
                //      �this mmmm, dd, yyyy 
                //      ��this MMMM, dd, YYYY
                while (aFormats[j].StartsWith(LMP.StringArray.mpEndOfSubField.ToString()))
                {
                    aFormats[j] = aFormats[j].Remove(0,1);
                }

                j++;
            }
            return aFormats;
        }
		/// <summary>
		/// returns true iff there is an object with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		internal static bool SegmentExists(int iID)
		{
			int iCount = (int) LMP.Data.SimpleDataCollection.GetScalar(
				"spSegmentsExists", new object[] {iID});
			return (iCount > 0);
		}

		/// <summary>
		/// returns true if there is an object type with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		internal static bool ObjectTypeExists(mpObjectTypes shID)
		{
			return Enum.IsDefined(typeof(mpObjectTypes), shID);
		}
        /// <summary>
        /// Returns true if record exists in VariableSets with specified name
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="iOwnerID"></param>
        /// <returns></returns>
        public static bool NamedPrefillExists(string xName, int iOwnerID)
        {
            int iCount = (int)LMP.Data.SimpleDataCollection.GetScalar(
                "spVariableSetsNameExists", new object[] { iOwnerID, xName });
            return (iCount > 0);
        }
        /// <summary>
        /// Returns true if record exists in UserSegments with specified name
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="iOwnerID"></param>
        /// <returns></returns>
        public static bool NamedContentExists(string xName, int iOwnerID)
        {
            int iCount = (int)LMP.Data.SimpleDataCollection.GetScalar(
                "spUserSegmentsNameExists", new object[] { iOwnerID, xName });
            return (iCount > 0);
        }
		/// splits string ID in format xxxx.xxxxxx into two numeric IDs
		/// </summary>
		/// <param name="xID"></param>
		/// <param name="iID1"></param>
		/// <param name="iID2"></param>
		public static void SplitID(string xID, out int iID1, out int iID2)
		{
            String.SplitID(xID, out iID1, out iID2);
		}

		/// <summary>
		/// builds string ID in format xxxx.xxxxxx from two numeric IDs
		/// </summary>
		/// <param name="iID1"></param>
		/// <param name="iID2"></param>
		/// <returns></returns>
		internal static string GetFormattedID(int iID1, int iID2)
		{
			string xID = iID1.ToString();
			if (iID2 != ForteConstants.mpFirmRecordID)
				xID = System.String.Concat(xID, ".", iID2.ToString());
			return xID;
		}

		/// <summary>
		/// returns a new ID for the specified user person
		/// </summary>
		/// <param name="iID1">ID1 of the specified user person</param>
		/// <param name="iID2">ID2 of the specified user person</param>
		/// <returns></returns>
		internal static string GetNewUserDataItemID(out int iID1, out int iID2)
		{
            //the number of ticks since Apr 19, 2006
            const long START_TICKS = 632810016000000000;
            const long TICKS_PER_DAY = 864000000000;

            iID1 = g_oCurUser.ID;

            long lTicksNow = DateTime.Now.Ticks;

            //get the number of ticks since start time
            long lTicks = lTicksNow - START_TICKS;

            int iNumDays = (int)(lTicks / TICKS_PER_DAY);
            string xNumDays = iNumDays.ToString();

            //ID2 is the first five digits from the 
            //number of days since the start date (this gives us
            //10,000 days (approx. 27.3 years - actually 24.6 years,
            //as we're padding with 9 - 9000/365) of unique 5
            //digit numbers) followed by last four digits
            //from the time portion of the ticks number -
            //this gives us a one in 10000 chance that 
            //we'll get a duplicate ID on the same day
            string xFirst5 = xNumDays.Substring(0, Math.Min(xNumDays.Length, 5)).PadLeft(5, '9');

            //use random number for lower four digits
            System.Threading.Thread.Sleep(1);
            Random oRand = new Random();
            string xNext4 = oRand.Next(9999).ToString().PadLeft(4, '0');

			iID2 = Int32.Parse(xFirst5 + xNext4);
			return GetFormattedID(iID1, iID2);
		}

		/// <summary>
		/// returns the MacPac person ID of the system user;
		/// if system user is not a MacPac person, returns guest user id
		/// </summary>
		/// <returns></returns>
		/// <summary>
		/// fills the supplied var with the aliases for the specified person
		/// </summary>
		/// <param name="oAliases">Persons collection containing all aliases</param>
		/// <param name="iPersonID">ID of the person whose aliases will be returned</param>
		public static void GetAliases(ref LocalPersons oAliases, int iPersonID)
		{
			//build Where statement for new Persons collection
			string xWhere = string.Concat("(LinkedPersonID=",
				iPersonID.ToString(),") AND (UserID is Null)");

			Trace.WriteNameValuePairs("xWhere", xWhere);

			oAliases = new LocalPersons(xWhere);
		}

        /// <summary>
        /// returns true if the file with the specified name is a local admin db
        /// </summary>
        /// <param name="xFile"></param>
        /// <returns></returns>
        public static bool IsAdminDB(string xFile)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xFile", xFile);
            
            if (!File.Exists(xFile))
                return false;

            try
            {
                bool bReestablishConnection = false;
                bool bAdminMode = false;

                //get current connection object for
                //future reset
                //GLOG 8286: Only re-establish if connection is currently open
                if (LMP.Data.LocalConnection.ConnectionObject != null && ((LMP.Data.LocalConnection.ConnectionObject.State & ConnectionState.Open)  == ConnectionState.Open))
                {
                    bReestablishConnection = true;
                    bAdminMode = LMP.Data.Application.AdminMode;
                }

                LocalConnection.ConnectIfNecessary(true);

                object oIsAdminDB = SimpleDataCollection.GetScalar(
                    "spMetadataItemFromID", new object[] { "IsAdminDB" });

                if (bReestablishConnection && !bAdminMode)
                    //reset connection
                    LocalConnection.ConnectIfNecessary(false);

                Trace.WriteNameValuePairs("oIsAdminDB", oIsAdminDB);

                LMP.Benchmarks.Print(t0);
                return Boolean.Parse(oIsAdminDB.ToString());
            }
            catch
            {
                //error occurred - database is not the admin db
                LMP.Benchmarks.Print(t0);
                return false;
            }
        }
        /// <summary>
        /// returns application metadata
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        public static string GetMetadata(string xKey)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xKey", xKey);
            object oData = SimpleDataCollection.GetScalar(
                "spMetadataItemFromID", new object[] { xKey });

            LMP.Benchmarks.Print(t0);

            if (oData != null)
                return oData.ToString();
            else
                return "";
        }

        /// <summary>
        /// sets the specified key value metadata pair
        /// </summary>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        public static void SetMetadata(string xKey, string xValue)
        {
            int iNumRowsAffected = SimpleDataCollection.ExecuteAction(
                "UPDATE Metadata SET [Value] = '" + xValue + "' WHERE [Name] = '" + xKey + "';");

            if (iNumRowsAffected == 0)
            {
                // There was no record to update. Add a record.
                SimpleDataCollection.ExecuteAction(
                    "Insert into Metadata(Name, [Value]) values('" + xKey + "', '" + xValue + "')");
            }
        }
		/// <summary>
		/// returns the set of lists or editable lists
		/// </summary>
		/// <param name="bInternalListsOnly"></param>
		/// <returns></returns>
		public static Lists GetLists(bool bInternalListsOnly)
		{
			Lists oLists = new Lists();
			oLists.InternalListsOnly = bInternalListsOnly;
			return oLists;
		}

		/// <summary>
		/// returns the name of the specified pleading court level item
		/// </summary>
		/// <param name="iLevel"></param>
		/// <param name="iID"></param>
		/// <returns></returns>
		public static string GetPleadingCourtLevelName(byte bytLevel, int iID)
		{
			Trace.WriteNameValuePairs("bytLevel", bytLevel, "iID", iID);

			object oName = LMP.Data.SimpleDataCollection.GetScalar(
				"spPleadingJurisdictions" + bytLevel.ToString() + "NameFromID",
				new object[] {iID});

            if (oName != null)
                return oName.ToString();
            else
                return "";
		}
		/// <summary>
		/// returns array list of folders for the specified user and shared data type - 
		/// there's a folder for every other user that has shared an item of this type
		/// with the current user;
		/// contains the folder's display name (in the form "last name, first name")
		/// and the id of the user to which it corresponds
		/// </summary>
		/// <param name="iPersonID"></param>
		/// <param name="bytType"></param>
		/// <returns></returns>
		public static ArrayList GetSharedDataFolders(int iPersonID,
			LMP.Data.mpSharedDataItems bytType)
		{
			int iObjectType = 0;
			switch (bytType)
			{
				case mpSharedDataItems.UserSegments:
					iObjectType = (int) mpObjectTypes.UserSegment;
					break;
				case mpSharedDataItems.VariableSets:
					iObjectType = (int) mpObjectTypes.VariableSet;
					break;
				case mpSharedDataItems.NumberingSchemes:
					iObjectType = (int) mpObjectTypes.NumberingScheme;
					break;
				case mpSharedDataItems.CIGroups:
					iObjectType = (int) mpObjectTypes.CIGroup;
					break;
				default:
					break;
			}

			if (iObjectType != 0)
			{
				return SimpleDataCollection.GetArray("spSharedDataFolders",
					new object[]{iPersonID, iObjectType});
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// returns array list of folder members for the specified user, owner,
		/// and shared data type - 
		/// contains the object ids, display name, and description
		/// </summary>
		/// <param name="iPersonID"></param>
		/// <param name="iOwnerID"></param>
		/// <param name="bytType"></param>
		/// <returns></returns>
		public static ArrayList GetSharedDataFolderMembers(int iPersonID,
			int iOwnerID, LMP.Data.mpSharedDataItems bytType)
		{
			string xProcName = null;
			switch (bytType)
			{
				case mpSharedDataItems.UserSegments:
					xProcName = "spSharedDataFolderMembersUserSegments";
					break;
				case mpSharedDataItems.VariableSets:
					//TODO: implement variable sets query 
					break;
				case mpSharedDataItems.NumberingSchemes:
					//TODO: implement numbering schemes query 
					break;
				case mpSharedDataItems.CIGroups:
					//TODO: implementci groups query 
					break;
				default:
					break;
			}

			if (xProcName != null)
			{
				return SimpleDataCollection.GetArray(xProcName,
					new object[]{iPersonID, iOwnerID});
			}
			else
			{
				return null;
			}
		}

		//GLOG 8037
        public static ArrayList GetSegmentFindResults(string xFindText, int iPersonID, mpSegmentFindMode iMode, mpSegmentFindTypes iTypes)
        {
            return GetSegmentFindResults(xFindText, iPersonID, iMode, iTypes, mpSegmentFindFields.Name, 0, "");
        }
        /// <summary>
        /// returns array list of segments matching the specified text and user
        /// contains the display name and object ids
        /// </summary>
        /// <param name="xFindText"></param>
        /// <param name="iPersonID"></param>
        /// <param name="iMode"></param>
        /// <returns></returns>
        public static ArrayList GetSegmentFindResults(string xFindText, int iPersonID, mpSegmentFindMode iMode, mpSegmentFindTypes iTypes, mpSegmentFindFields iFields, int iOwnerID, string xFolderIDList)
        {
            //GLOG 8037 (JTS 05/05/15): FUNCTION HAS BEEN SIGNIFICANTLY REWORKED FOR USE WITH CONTENT MANAGER ADVANCED FIND
            //=============================================================================================================
            //GLOG 6028: Allow including only specific types of content in results
            //GLOG 6208: Separate option to include Style Sheets or Segment Packets
            const string FIND_SEGMENTS_SQL = "SELECT DISTINCT aSegs.DisplayName, aSegs.ID, 100 AS FolderType, aSegs.IntendedUse FROM ({2} AS S INNER JOIN Assignments AS a ON S.ID=a.AssignedObjectID WHERE ({0}) AND (a.TargetObjectTypeID=101 AND a.AssignedObjectTypeID=100){1}) AS aSegs";
            //GLOG 8076: Alternate SQL for searching DisplayName only, or including HelpText
            const string FIND_SEGMENTS_DISPLAYNAME_ONLY_SQL = "SELECT s.DisplayName, s.ID, s.IntendedUse FROM Segments";
            const string FIND_SEGMENTS_HELPTEXT_SQL = "SELECT * FROM (SELECT Segs.DisplayName, Segs.ID, Segs.IntendedUse, IIF(H.File is Null, Segs.HelpText, H.Contents) as HelpText FROM Segments AS Segs LEFT JOIN SegmentsHelpContents AS H ON Left(Segs.HelpText, 255)=Left(H.File, 255))";
            //GLOG 8162
            const string FIND_SEGMENTS_FOLDERNAME_SQL = "SELECT DISTINCT aSegs.DisplayName, aSegs.ID, 100 AS FolderType, aSegs.IntendedUse FROM (SELECT s.DisplayName, s.ID, s.IntendedUse FROM Segments AS S INNER JOIN (SELECT TargetObjectID, AssignedObjectID FROM Assignments AS a INNER JOIN Folders f on f.ID=a.TargetObjectID WHERE (({2}) AND (a.TargetObjectTypeID=101 AND a.AssignedObjectTypeID=100){1})) AS af ON S.ID=af.AssignedObjectID{0}) AS aSegs";

            const string FIND_USERSEGMENTS_SQL = "SELECT uSegs.DisplayName, uSegs.ID, 99 AS FolderType, uSegs.IntendedUse FROM (SELECT us.DisplayName, us.ID1 & '.' & us.ID2 AS ID, us.IntendedUse FROM (SELECT DisplayName,ID1, ID2, IntendedUse FROM UserSegments As S WHERE {0}) AS us INNER JOIN (SELECT u.ID1, u.ID2 FROM UserSegments AS u INNER JOIN (SELECT p.ObjectID1, p.ObjectID2 FROM Permissions p WHERE p.PersonID={1} and p.ObjectTypeID=99 and (p.ObjectID1={2} OR {2}=0)) AS p ON (u.ID1=p.ObjectID1) AND (u.ID2=p.ObjectID2) UNION SELECT u.ID1, u.ID2 FROM UserSegments u WHERE ((u.SharedFolderID>0 AND (u.OwnerID1={2} OR {2}=0)) OR ((u.OwnerID1={2} AND {2}={1}) OR (u.OwnerID1={1} AND {2}=0)))) AS SegIDs ON (us.ID1=SegIDs.ID1) AND (us.ID2=SegIDs.ID2)) as uSegs";
            const string FIND_VARIABLE_SETS_SQL = "SELECT vSets.Name, vSets.ID, 104 as FolderType, vSets.IntendedUse FROM (SELECT vs.Name, vs.ID1 & '.' & vs.ID2 AS ID, 0 AS IntendedUse FROM (SELECT Name, ID1, ID2 FROM VariableSets WHERE ({0})) AS vs INNER JOIN (SELECT v.ID1, v.ID2 FROM VariableSets AS v INNER JOIN (SELECT p.ObjectID1, p.ObjectID2 FROM Permissions p WHERE p.PersonID={1} and p.ObjectTypeID = 104 and (p.ObjectID1={2} OR {2}=0)) AS p ON (v.ID1=p.ObjectID1) AND (v.ID2=p.ObjectID2) UNION SELECT v.ID1, v.ID2 FROM VariableSets v WHERE ((v.SharedFolderID>0 AND (v.OwnerID1={2} OR {2}=0)) OR ((v.OwnerID1={2} AND {2}={1}) OR (v.OwnerID1={1} AND {2}=0)))) AS SetIDs ON (vs.ID2=SetIDs.ID2) AND (vs.ID1=SetIDs.ID1)) as vSets";
			//GLOG 8162
            const string FIND_USERSEGMENTS_FOLDERS_SQL = " INNER JOIN (SELECT m.TargetObjectID1, m.TargetObjectID2, m.UserFolderID1, m.UserFolderID2 From UserFolderMembers AS m INNER JOIN UserFolders As u ON (u.ID1=m.UserFolderID1 AND u.ID2=UserFolderID2) WHERE {0}) AS ufm ON uSegs.ID=ufm.TargetObjectID1 & '.' & ufm.TargetObjectID2";
            const string FIND_VARIABLE_SETS_FOLDERS_SQL = " INNER JOIN (SELECT m.TargetObjectID1, m.TargetObjectID2, m.UserFolderID1, m.UserFolderID2 From UserFolderMembers AS m INNER JOIN UserFolders As u ON (u.ID1=m.UserFolderID1 AND u.ID2=UserFolderID2) WHERE {0}) AS ufm ON vSets.ID=ufm.TargetObjectID1 & '.' & ufm.TargetObjectID2";
            const string FIND_PREFERENCES_WHERE_CLAUSE = " AND (S.XML LIKE '<mAuthorPrefs>%')";

            const string xSDotDisplayName = "S.DisplayName";
            const string xSDotHelpText = "S.HelpText";
            const string xUserSegDisplayName = "DisplayName";
            const string xUserSegHelpText = "HelpText";
            const string xVSetName = "Name";
            const string xVSetDescription = "Description";
            //GLOG 8162
            const string xFDotName = "f.Name";
            const string xUDotName = "u.Name";
            
            string xFoldersClause = "";
            string xFieldsClause = ""; //GLOG 8076

            if (iTypes == 0)
                iTypes = mpSegmentFindTypes.AdminSegments | mpSegmentFindTypes.UserSegments | mpSegmentFindTypes.Prefills | mpSegmentFindTypes.StyleSheets | 
                    mpSegmentFindTypes.SegmentPackets | mpSegmentFindTypes.UserStyleSheets | mpSegmentFindTypes.UserSegmentPackets | mpSegmentFindTypes.MasterData; //GLOG 8037

            if (xFindText == "")
                return null;
            else
            {
                if (xFindText == " ")
                    xFindText = "";
                xFindText = xFindText.Replace("'", "''");
                //GLOG 6205: Accommodate wildcard characters used as literals in search text
                xFindText = xFindText.Replace("[", "[[]");
                xFindText = xFindText.Replace("%", "[%]");
                xFindText = xFindText.Replace("_", "[_]");
                string xWHEREClause = "";
                string xBaseSQL = ""; //GLOG 8162
                if (iFields == mpSegmentFindFields.Name)
                {
                    xWHEREClause = GetWhereClause(xSDotDisplayName, xFindText, iMode);
                    xFieldsClause = FIND_SEGMENTS_DISPLAYNAME_ONLY_SQL; //GLOG 8076
                    xBaseSQL = FIND_SEGMENTS_SQL;  //GLOG 8162
                }
                else if (iFields == mpSegmentFindFields.Description)
                {
                    xWHEREClause = GetWhereClause(xSDotHelpText, xFindText, iMode);
                    xFieldsClause = FIND_SEGMENTS_HELPTEXT_SQL; //GLOG 8076
                    xBaseSQL = FIND_SEGMENTS_SQL; //GLOG 8162
                }
                else if (iFields == mpSegmentFindFields.FolderName) //GLOG 8162
                {
                    xWHEREClause = " WHERE true ";
                    xFieldsClause = GetWhereClause(xFDotName, xFindText, iMode);
                    xBaseSQL = FIND_SEGMENTS_FOLDERNAME_SQL;
                }
                else
                {
                    //Search both fields
                    xWHEREClause = GetWhereClause(xSDotDisplayName + "|" + xSDotHelpText, xFindText, iMode);
                    xFieldsClause = FIND_SEGMENTS_HELPTEXT_SQL; //GLOG 8076
                    xBaseSQL = FIND_SEGMENTS_SQL;  //GLOG 8162
                }

                if ((iTypes & mpSegmentFindTypes.Preferences) == mpSegmentFindTypes.Preferences)
                    xWHEREClause = xWHEREClause + FIND_PREFERENCES_WHERE_CLAUSE;
                
                //Include or exclude Segment Packets and Style Sheets
                string xWHEREIntendedUse = "";
                if (((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets) &&
                    ((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets) && 
                    ((iTypes & mpSegmentFindTypes.MasterData) == mpSegmentFindTypes.MasterData))
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == 0)
                        //Only Admin Segments that are Style Sheets, Master Data or Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=7 OR IntendedUse=4 OR IntendedUse=5)";

                }
                else if (((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets) &&
                    ((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets))
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == 0)
                        //Only Admin Segments that are Style Sheets or Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=5 OR IntendedUse=4)";

                }
                else if (((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets) &&
                    ((iTypes & mpSegmentFindTypes.MasterData) == mpSegmentFindTypes.MasterData))
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == 0)
                        //Only Admin Segments that are Segment Packets or MasterData
                        xWHEREIntendedUse = " AND (IntendedUse=7 OR IntendedUse=5)";

                }
                else if (((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets) &&
                    ((iTypes & mpSegmentFindTypes.MasterData) == mpSegmentFindTypes.MasterData))
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == 0)
                        //Only Admin Segments that are Style Sheets or MasterData
                        xWHEREIntendedUse = " AND (IntendedUse=7 OR IntendedUse=4)";

                }
                else if ((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets)
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == mpSegmentFindTypes.AdminSegments)
                        //All not Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse<>4 AND IntendedUse<>7)";
                    else
                        //Only Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=5)";
                }
                else if ((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets)
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == mpSegmentFindTypes.AdminSegments)
                        //Include all that are not Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse<>7 AND IntendedUse<>5)";
                    else
                        //Only Style Sheets
                        xWHEREIntendedUse = " AND (IntendedUse=4)";
                }
                else if ((iTypes & mpSegmentFindTypes.MasterData) == mpSegmentFindTypes.MasterData)
                {
                    if ((iTypes & mpSegmentFindTypes.AdminSegments) == mpSegmentFindTypes.AdminSegments)
                        //All not Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse<>5 AND IntendedUse<>4)";
                    else
                        //Only Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=7)";
                }
                else
                    xWHEREIntendedUse = " AND (IntendedUse<>4 AND IntendedUse<>7 AND IntendedUse<>5)";

                xWHEREClause = xWHEREClause + xWHEREIntendedUse;
                if (xFolderIDList != "")
                {
                    xFoldersClause = " AND (a.TargetObjectID IN (" + xFolderIDList + "))";
                }
				//GLOG 8162
                string xSegmentSql = string.Format(xBaseSQL, xWHEREClause, xFoldersClause, xFieldsClause); //GLOG 8076

                if (iFields == mpSegmentFindFields.Name)
                {
                    xWHEREClause = GetWhereClause(xUserSegDisplayName, xFindText, iMode);
                }
                else if (iFields == mpSegmentFindFields.Description)
                {
                    xWHEREClause = GetWhereClause(xUserSegHelpText, xFindText, iMode);
                }
                else if (iFields == mpSegmentFindFields.FolderName) //GLOG 8162
                {
                    xWHEREClause = "true";
                }
                else
                {
                    //Search both fields
                    xWHEREClause = GetWhereClause(xUserSegDisplayName + "|" + xUserSegHelpText, xFindText, iMode);
                }
                //Include or exclude Segment Packets and Style Sheets
                xWHEREIntendedUse = "";
                if (((iTypes & mpSegmentFindTypes.UserSegmentPackets) == mpSegmentFindTypes.UserSegmentPackets) &&
                    ((iTypes & mpSegmentFindTypes.UserStyleSheets) == mpSegmentFindTypes.UserStyleSheets))
                {
                    if ((iTypes & mpSegmentFindTypes.UserSegments) == 0)
                        //Only Admin Segments that are Style Sheets or Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=5 OR IntendedUse=4)";

                }
                else if ((iTypes & mpSegmentFindTypes.UserSegmentPackets) == mpSegmentFindTypes.UserSegmentPackets)
                {
                    if ((iTypes & mpSegmentFindTypes.UserSegments) == mpSegmentFindTypes.UserSegments)
                        //All not Style Sheets
                        xWHEREIntendedUse = " AND (IntendedUse<>4)";
                    else
                        //Only Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse=5)";
                }
                else if ((iTypes & mpSegmentFindTypes.UserStyleSheets) == mpSegmentFindTypes.UserStyleSheets) //GLOG 8037
                {
                    if ((iTypes & mpSegmentFindTypes.UserSegments) == mpSegmentFindTypes.UserSegments)
                        //Include all that are not Segment Packets
                        xWHEREIntendedUse = " AND (IntendedUse<>5)";
                    else
                        //Only Style Sheets
                        xWHEREIntendedUse = " AND (IntendedUse=4)";
                }
                else
                    xWHEREIntendedUse = " AND (IntendedUse<>4 AND IntendedUse<>5)";
                xWHEREClause = xWHEREClause + xWHEREIntendedUse;
                string xUserSegmentSql = string.Format(FIND_USERSEGMENTS_SQL, xWHEREClause, iPersonID, iOwnerID);
                xWHEREClause = ""; //GLOG 8162
                string xUserFoldersWHEREClause = "";
                string xUserFoldersClause = "";
				//GLOG 8162
                if (xFolderIDList != "" || iFields == mpSegmentFindFields.FolderName)
                {
                    if (iFields == mpSegmentFindFields.FolderName)
                    {
                        xWHEREClause = GetWhereClause(xUDotName, xFindText, iMode);
                    }
                    xUserFoldersWHEREClause = GetUserFoldersWhereClause(xFolderIDList);
                    if (xWHEREClause != "" && xUserFoldersWHEREClause != "")
                    {
                        xUserFoldersWHEREClause = xWHEREClause + " AND (" + xUserFoldersWHEREClause + ")";
                    }
                    else if (xWHEREClause != "")
                    {
                        xUserFoldersWHEREClause = xWHEREClause;
                    }
                    if (xUserFoldersWHEREClause != "")
                        xUserFoldersClause = string.Format(FIND_USERSEGMENTS_FOLDERS_SQL, xUserFoldersWHEREClause);
                    xUserSegmentSql = xUserSegmentSql + xUserFoldersClause;
                }
                xWHEREClause = ""; //GLOG 8162
                if (iFields == mpSegmentFindFields.Name)
                {
                    xWHEREClause = GetWhereClause(xVSetName, xFindText, iMode);
                }
                else if (iFields == mpSegmentFindFields.Description)
                {
                    xWHEREClause = GetWhereClause(xVSetDescription, xFindText, iMode);
                }
                else if (iFields == mpSegmentFindFields.FolderName) //GLOG 8162
                {
                    xWHEREClause = "true";
                }
                else
                {
                    //Search both fields
                    xWHEREClause = GetWhereClause(xVSetName + "|" + xVSetDescription, xFindText, iMode);
                }
                string xVarSetSql = string.Format(FIND_VARIABLE_SETS_SQL, xWHEREClause, iPersonID, iOwnerID);
                if (xUserFoldersWHEREClause != "")
                {
                    xUserFoldersClause = string.Format(FIND_VARIABLE_SETS_FOLDERS_SQL, xUserFoldersWHEREClause);
                    xVarSetSql = xVarSetSql + xUserFoldersClause;
                }
                //string xSql = string.Format(FIND_SQL, xSegmentSql, xUserSegmentSql, xVarSetSql);
                string xSql = "";
                if (((iTypes & mpSegmentFindTypes.AdminSegments) == mpSegmentFindTypes.AdminSegments) || 
                    ((iTypes & mpSegmentFindTypes.Preferences) == mpSegmentFindTypes.Preferences) ||
                    ((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets) ||
                    ((iTypes & mpSegmentFindTypes.MasterData) == mpSegmentFindTypes.MasterData) ||
                    ((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets))
                    xSql = xSql + xSegmentSql;

                if (((iTypes & mpSegmentFindTypes.UserSegments) == mpSegmentFindTypes.UserSegments) ||
                    ((iTypes & mpSegmentFindTypes.UserSegmentPackets) == mpSegmentFindTypes.UserSegmentPackets) ||
                    ((iTypes & mpSegmentFindTypes.UserStyleSheets) == mpSegmentFindTypes.UserStyleSheets))
                {
                    if (xSql != "")
                        xSql = xSql + " UNION ";
                    xSql = xSql + xUserSegmentSql;
                }
                if ((iTypes & mpSegmentFindTypes.Prefills) == mpSegmentFindTypes.Prefills)
                {
                    if (xSql != "")
                        xSql = xSql + " UNION ";
                    xSql = xSql + xVarSetSql;
                }
                if (xSql != "")
                    xSql = xSql + ";";
                else
                    return null;

                ArrayList aResults = SimpleDataCollection.GetArray(xSql);
                // JTS 3/12/10: Remove Admin Segments found only in folders for which User does not have permission
                if (!LMP.Data.Application.AdminMode && ((iTypes & mpSegmentFindTypes.AdminSegments) == mpSegmentFindTypes.AdminSegments) ||
                    ((iTypes & mpSegmentFindTypes.StyleSheets) == mpSegmentFindTypes.StyleSheets) ||
                    ((iTypes & mpSegmentFindTypes.SegmentPackets) == mpSegmentFindTypes.SegmentPackets))
                {
                    DateTime t0 = DateTime.Now;
                    for (int i = aResults.Count - 1; i >= 0; i--)
                    {
                        object[] aItem = (object[])aResults[i];
                        string xID = @aItem[1].ToString();
                        mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)(int.Parse(aItem[2].ToString()));
                        if (shMemberType == mpFolderMemberTypes.AdminSegment &&
                            !LMP.Data.Application.User.HasPermissionForSegment(xID))
                        {
                            aResults.RemoveAt(i);
                        }
                    }
                    LMP.Benchmarks.Print(t0, "Remove non-permitted results");
                }
                //GLOG 8016: Don't show user content no longer assigned to any folder
                if ((iOwnerID == 0 || iOwnerID == iPersonID) && (((iTypes & mpSegmentFindTypes.UserSegmentPackets) == mpSegmentFindTypes.UserSegmentPackets) ||
                    ((iTypes & mpSegmentFindTypes.UserSegments) == mpSegmentFindTypes.UserSegments) ||
                    ((iTypes & mpSegmentFindTypes.UserStyleSheets) == mpSegmentFindTypes.UserStyleSheets) ||
                    ((iTypes & mpSegmentFindTypes.Prefills) == mpSegmentFindTypes.Prefills)))
                {
                    DateTime t0 = DateTime.Now;
                    for (int i = aResults.Count - 1; i >= 0; i--)
                    {
                        object[] aItem = (object[])aResults[i];
                        string xID = @aItem[1].ToString();
                        mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)(int.Parse(aItem[2].ToString()));
                        if (!xID.StartsWith(iPersonID.ToString())) //ignore shared content
                        {
                            continue;
                        }
                        string xPrefix = (shMemberType == mpFolderMemberTypes.VariableSet) ? "P" : "U";
                        if ((shMemberType == mpFolderMemberTypes.UserSegment || 
                            shMemberType == mpFolderMemberTypes.UserSegmentPacket ||
                            shMemberType == mpFolderMemberTypes.VariableSet) &&
                            UserFolder.GetSegmentFullPaths(xPrefix + xID, iPersonID).GetLength(0) == 0)
                        {
                            aResults.RemoveAt(i);
                        }
                    }
                    LMP.Benchmarks.Print(t0, "Remove orphaned user content results");
                }
                if (aResults.Count == 0)
                    return null;
                else
                    return aResults;
            }
        }
        //GLOG 8037: Limit User Content find results to specific folders
        private static string GetUserFoldersWhereClause(string xFolderIDList)
        {
            if (!xFolderIDList.Contains("."))
            {
                //Not User Content IDs
                return "";
            }
            const string xFOLDER_SQL = "(m.UserFolderID1={0} AND m.UserFolderID2={1})"; //GLOG 8162
            string xClause = "";
            string[] aFolders = xFolderIDList.Split(',');
            for (int i = 0; i <= aFolders.GetUpperBound(0); i++)
            {
                string[] aIDs = aFolders[i].Split('.');
                if (xClause != "")
                    xClause = xClause + " OR ";
                xClause = xClause + string.Format(xFOLDER_SQL, aIDs[0], aIDs[1]);
            }
            return xClause;

        }
        //GLOG 8037: reworked to allow searching in multiple fields at once
        private static string GetWhereClause(string xFieldNames, string xFindText, mpSegmentFindMode iMode)
        {
            string[] axFindTextParts = xFindText.Split(' ');
            string[] axFieldNames = xFieldNames.Split('|'); //GLOG 8037
            StringBuilder sbFindText = new StringBuilder();
            string xLeadingWildCard = (iMode == mpSegmentFindMode.MatchSubString && xFindText != "") ? "%" : "";

            if (axFieldNames.Length > 1)
            {
                sbFindText.Append("(");
            }
            ///GLOG 2721 - if second element is empty, then include space following first element
            //GLOG 8037: search multiple fields
            for (int iField = 0; iField < axFieldNames.Length; iField++)
            {
                if (iField > 0)
                {
                    sbFindText.Append(" OR ");
                }
                sbFindText.Append("(");
                if (axFindTextParts.Length == 2 && axFindTextParts[1] == "")
                    sbFindText.Append(axFieldNames[iField] + " LIKE '" + xLeadingWildCard + axFindTextParts[0] + " %'");
                else
                    for (int i = 0; i < axFindTextParts.Length; i++)
                    {
                        if (i > 0)
                        {
                            sbFindText.Append(" AND ");
                        }

                        sbFindText.Append(axFieldNames[iField] + " LIKE '" + xLeadingWildCard + axFindTextParts[i] + "%'");
                    }
                sbFindText.Append(")");
            }
            if (axFieldNames.Length > 1)
            {
                sbFindText.Append(")");
            }
            return sbFindText.ToString();
        }

        /// <summary>
        /// returns the dataset with the items in the value set with the specified ID
        /// </summary>
        /// <param name="iID"></param>
        /// <returns>DataSet</returns>
        internal static DataTable GetValueSet(int iID)
        {
            try
            {
                DataTable oDT = new DataTable();
                OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "Select Value1, Value2, SetID FROM ValueSets WHERE SetID = " + iID.ToString() + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //fill data set using adapter
                    OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);
                    oAdapter.Fill(oDT);
                }
                return oDT;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }

        /// <summary>
        /// returns array list of segments containing
        /// the specified Segment ID in the ChildSegmentIDs field
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public static ArrayList GetContainingSegmentIDs(string xSegmentID)
        {
            const string xProcName = "spSegmentsGetContainingSegmentIDs";
            if (xSegmentID == "")
                return null;
            return SimpleDataCollection.GetArray(xProcName, new object[] { xSegmentID });
        }
        public static int CountAdminSegmentFolderAssignments(int iAdminSegmentID)
        {
            string xProcName = "spSegmentsCountFolderAssignments";
            object oRet = SimpleDataCollection.GetScalar(xProcName, new object[] { iAdminSegmentID });
            return (int)oRet;
        }
        public static int CountUserSegmentFolderAssignments(string xSegmentID)
        {
            if (xSegmentID.IndexOf(".") < 0)
            {
                xSegmentID = xSegmentID + ".0";
            }
            int iID1;
            int iID2;
            LMP.String.SplitID(xSegmentID, out iID1, out iID2);
            string xProcName = "spUserSegmentsCountFolderAssignments";
            object oRet = SimpleDataCollection.GetScalar(xProcName, new object[] { iID1, iID2 });
            return (int)oRet;
        }
        public static int CountVariableSetFolderAssignments(string xSegmentID)
        {
            if (xSegmentID.IndexOf(".") < 0)
            {
                xSegmentID = xSegmentID + ".0";
            }
            int iID1;
            int iID2;
            LMP.String.SplitID(xSegmentID, out iID1, out iID2);
            string xProcName = "spVariableSetsCountFolderAssignments";
            object oRet = SimpleDataCollection.GetScalar(xProcName, new object[] { iID1, iID2 });
            return (int)oRet;
        }
        /// <summary>
        /// returns a CI Session object
        /// </summary>
        public static ICSession CISession
        {
            get{return m_oCISession;}
            set{m_oCISession = value;}
        }
        /// <summary>
        /// starts a new CI session if one doesn't already exist
        /// </summary>
        public static void StartCIIfNecessary()
        {
            if (m_oCISession == null)
            {
                m_oCISession = GetCISession();
                if (!m_oCISession.Initialized)
                {
                    throw new LMP.Exceptions.CIException(
                        LMP.Resources.GetLangString("Error_CouldNotStartCI"));
                }
                else
                    Trace.WriteInfo("CI Launched.");
            }
        }
        /// <summary>
        /// exits CI
        /// </summary>
        public static void QuitCI()
        {
            m_oCISession = null;
        }

        //GLOG : 7469 : ceh - Custom for Orrick: ceh - Symantec's Enterprise Vault Add-in for Outlook 2010 does not work
        //if Outlook is started programatically, ie, with CreateObject("Outlook.Application").  We need to force
        //users to start Outlook
        public static bool AllowCIWithoutOutlookRunning()
        {
            byte bytVer = 0;
            string xRegValue = LMP.Registry.GetMacPac10Value("ForceOutlookLaunch");

            if (xRegValue == "1" && !OS.ProcessIsRunning("Outlook"))
            {
                //get version of Outlook
                bytVer = OS.GetOutlookVersionFromRegistry();

                //if outlook 2010 or later, then
                //prompt user to launch Outlook before using CI
                if (bytVer >= 14)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ForceOutlookLaunch"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// Copy KeySet records from source ObjectID to target ObjectID
        /// </summary>
        /// <param name="iObjectID1"></param>
        /// <param name="iNewObjectID1"></param>
        /// <param name="iOwnerID1"></param>
        /// <param name="iOwnerID2"></param>
        private static void CopyKeySetsForObjectID(int iObjectID1, int iObjectID2, int iNewObjectID1, int iNewObjectID2, int iOwnerID1, int iOwnerID2)
        {
            LMP.Trace.WriteNameValuePairs("iObjectID1", iObjectID1, "iObjectID2", iObjectID2, 
                "iNewObjectID1", iNewObjectID1, "iNewObjectID2", iNewObjectID2,
                "iOwnerID1", iOwnerID1, "iOwnerID2", iOwnerID2);
            LMP.Data.SimpleDataCollection.ExecuteActionSproc("spKeySetsCopyForObjectID", 
                new object[] {iObjectID1, iObjectID2, iNewObjectID1, iNewObjectID2, 
                    iOwnerID1, iOwnerID2, Application.GetCurrentEditTime()});
        }
        /// <summary>
        /// Copy all Firm-wide Keysets (OwnerID1 and OwnerID2 = -999999999) from source ObjectID to target ObjectID
        /// </summary>
        /// <param name="iObjectID1"></param>
        /// <param name="iNewObjectID1"></param>
        public static void CopyFirmKeySetsForObjectID(int iObjectID1, int iNewObjectID1)
        {
            CopyKeySetsForObjectID(iObjectID1, ForteConstants.mpFirmRecordID, 
                iNewObjectID1, ForteConstants.mpFirmRecordID, 
                ForteConstants.mpFirmRecordID, ForteConstants.mpFirmRecordID);
        }

        /// <summary>
        /// Copy all Firm-wide Keysets (OwnerID1 and OwnerID2 = -999999999) from source ObjectID to target ObjectID
        /// </summary>
        /// <param name="iObjectID1"></param>
        /// <param name="iNewObjectID1"></param>
        public static void CopyFirmKeySetsForObjectID(int iObjectID1, int iObjectID2, int iNewObjectID1, int iNewObjectID2)
        {
            CopyKeySetsForObjectID(iObjectID1, iObjectID2, iNewObjectID1, 
                iNewObjectID2, ForteConstants.mpFirmRecordID, ForteConstants.mpFirmRecordID);
        }

        /// <summary>
        /// Copy all assignment records from source object ID to new object ID
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iNewTargetObjectID"></param>
        public static void CopyAssignmentsForObject(mpObjectTypes iObjectTypeID, int iTargetObjectID, int iNewTargetObjectID)
        {
            LMP.Trace.WriteNameValuePairs("iObjectTypeID", iObjectTypeID, "iTargetObjectID", iTargetObjectID,
                "iNewTargetObjectID", iNewTargetObjectID);
            LMP.Data.SimpleDataCollection.ExecuteActionSproc("spAssignmentsCopyForTargetObjectID",
                new object[] { iObjectTypeID, iTargetObjectID, iNewTargetObjectID, Application.GetCurrentEditTime()});
        }
        /// <summary>
        /// Returns ID of AdminSegment with the specified name
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static int GetSegmentIDFromName(string xSegment)
        {
			Trace.WriteNameValuePairs("xSegment", xSegment);

			object oID = LMP.Data.SimpleDataCollection.GetScalar(
				"spSegmentIDFromName", new object[] {xSegment});

            if (oID != null)
                return (int)oID;
            else
                return 0;
        }
        /// <summary>
        /// Returns Display Name of AdminSegment with the specified ID
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string GetSegmentNameFromID(int iSegmentID)
        {
            Trace.WriteNameValuePairs("iSegmentID", iSegmentID);

            object oID = LMP.Data.SimpleDataCollection.GetScalar(
                "spSegmentNameFromID", new object[] { iSegmentID });

            if (oID != null)
                return oID.ToString();
            else
                return "";
        }

        /// <summary>
        /// returns type id of admin segment with the specified id
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <returns></returns>
        public static mpObjectTypes GetSegmentTypeID(int iSegmentID)
        {
            Trace.WriteNameValuePairs("iSegmentID", iSegmentID);

            object oID = LMP.Data.SimpleDataCollection.GetScalar(
                "spSegmentTypeIDFromID", new object[] { iSegmentID });

            if (oID != null)
                return (mpObjectTypes)oID;
            else
                return (mpObjectTypes)0;
        }

        /// <summary>
        /// Returns Display Name of AdminSegment with the specified ID
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string GetUserSegmentNameFromID(int iID1, int iID2)
        {
            Trace.WriteNameValuePairs("iID1", iID1, "iID2", iID2);

            object oID = LMP.Data.SimpleDataCollection.GetScalar(
                "spUserSegmentNameFromID", new object[] { iID1, iID2 });

            if (oID != null)
                return oID.ToString();
            else
                return "";
        }
        /// <summary>
        /// Returns list populated with names, IDs of Persons who have shared objects with the specified Person
        /// </summary>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static ArrayList GetOwnerFoldersForDisplay(int iPersonID)
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "spOwnerFoldersForDisplay";
            //get reader with display fields only
            OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, false, new object[] { iPersonID });

            try
            {
                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }
        /// <summary>
        /// Returns list populated with names, IDs of Persons who have shared objects with the specified Person
        /// </summary>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static ArrayList GetOwnerFoldersMembersForDisplay(int iPersonID, int iOwnerID)
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "spOwnerFolderMembersForDisplay";
            //get reader with display fields only
            OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, false, new object[] { iPersonID, iOwnerID });

            try
            {
                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }
        /// <summary>
        /// Returns All Segments
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static ArrayList GetAllSegments()
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "SELECT id FROM Segments;";
            //get reader with display fields only
            OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL,false);

            try
            {
                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }
        /// <summary>
        /// Delete UserFolderMember records from local DB
        /// that reference non-existent Objects
        /// </summary>
        public static void DeleteOrphanedUserFolderMembers()
        {
            int iRecs = 0;
            try
            {
                //Remove invalid UserSegment UserFolderMembers
                iRecs = LMP.Data.SimpleDataCollection.ExecuteActionSproc("spUserFolderMembersDeleteOrphanedUserSegments", null);
            }
            catch {}
            try
            {
                //Remove invalid AdminSegment UserFolderMembers
                iRecs = iRecs + LMP.Data.SimpleDataCollection.ExecuteActionSproc("spUserFolderMembersDeleteOrphanedSegments", null);
            }
            catch {}
            try
            {
                //Remove invalid VariableSet UserFolderMembers
                iRecs = iRecs + LMP.Data.SimpleDataCollection.ExecuteActionSproc("spUserFolderMembersDeleteOrphanedVariableSets", null);
            }
            catch {}
            if (iRecs > 0)
                LMP.Trace.WriteInfo("Deleted " + iRecs + " orphaned UserFolderMember records.");
        }
        /// <summary>
        /// Return current Date/time as UTC string that can be passed to Access query
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentEditTime()
        {
            return GetCurrentEditTime(false);
        }
        /// <summary>
        /// Return current Date/time as UTC string that can be passed to Access query
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentEditTime(bool bSQLFormat)
        {
            if (bSQLFormat)
                //GLOG 6673: If building SQL String, need to make sure date is US Format
                return System.DateTime.Now.ToUniversalTime().ToString(Culture.USEnglishCulture);
            else
                //GLOG 7004: Format date in universal format for compatibility
                return System.DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
        }
        /// <summary>
        /// returns supported languages for current client
        /// </summary>
        public static string SupportedLanguagesString
        {
            get
            {
                if (m_xSupportedLanguages == null)
                {
                    DataTable oDT = GetValueSet(-3);

                    if (oDT.Rows.Count == 0)
                        m_xSupportedLanguages = "English�1033";
                    else
                    {
                        string[] aLangNames = oDT.Rows[0][0].ToString().Split('|');
                        string[] aLCIDs = oDT.Rows[0][1].ToString().Split('|');

                        for (int i = 0; i < aLangNames.Length; i++)
                        {
                            m_xSupportedLanguages += aLangNames[i] + "�" + aLCIDs[i] + "|";
                        }

                        m_xSupportedLanguages.TrimEnd('|');
                    }
                }
                return m_xSupportedLanguages;
            }
        }

        /// <summary>
        /// returns supported languages for current client
        /// </summary>
        public static DataTable SupportedLanguages
        {
            get
            {
                DataTable oDTLangs = new DataTable();
                oDTLangs.Columns.Add("Display Name");
                oDTLangs.Columns.Add("LCID");

                //get the supported languages value set -
                //this set has only one row, as the names
                //and lcids are stored as delimited strings
                DataTable oDT = GetValueSet(-3);

                if (oDT.Rows.Count == 0)
                    oDTLangs.Rows.Add("English", "1033");
                else
                {
                    string[] aLangNames = oDT.Rows[0][0].ToString().Split('|');
                    string[] aLCIDs = oDT.Rows[0][1].ToString().Split('|');

                    //parse string
                    for (int i = 0; i < aLangNames.Length; i++)
                    {
                        oDTLangs.Rows.Add(aLangNames[i].ToString(), aLCIDs[i].ToString());
                    }
                }

                //save all changes to datatable so
                //code won't attempt to save to db
                oDTLangs.AcceptChanges();
                return oDTLangs;
            }
            set
            {
                string xNames = null;
                string xLCIDs = null;

                if (value.Rows.Count == 0)
                {
                    xNames = "English";
                    xLCIDs = "1033";
                }
                else
                {
                    //create names and IDs strings
                    foreach (DataRow oDR in value.Rows)
                    {
                        xNames += oDR[0].ToString() + "|";
                        xLCIDs += oDR[1].ToString() + "|";
                    }

                    xNames = xNames.TrimEnd('|');
                    xLCIDs = xLCIDs.TrimEnd('|');
                }

                //add each language to the value set
                using (OleDbCommand oCmd = new OleDbCommand())
                {
                    oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                    if (GetValueSet(-3).Rows.Count == 0)
                    {
                        //no value set exists - insert
                        //GLOG 5612: LastEditTime needs to be UTC
                        //GLOG 6673:  Make sure Date in SQL string is US Format
                        oCmd.CommandText = "INSERT INTO ValueSets (Value1, Value2, SetID, LastEditTime) VALUES ('" +
                            xNames + "','" + xLCIDs + "', -3, #" + GetCurrentEditTime(true) + "#)";
                    }
                    else
                    {
                        //GLOG 5612: Need to add Deletions record so that user DBs will be updated correctly after sync
                        SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, -3);
                        //GLOG 5612: LastEditTime needs to be UTC
                        //GLOG 6673:  Make sure Date in SQL string is US Format
                        oCmd.CommandText = "UPDATE ValueSets SET Value1='" + xNames +
                            "', Value2='" + xLCIDs + "', LastEditTime=#" + GetCurrentEditTime(true) + "# WHERE SetID=-3";
                    }

                    oCmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Returns string containing Jurisdiction Text for specified levels
        /// </summary>
        /// <param name="L0"></param>
        /// <param name="L1"></param>
        /// <param name="L2"></param>
        /// <param name="L3"></param>
        /// <param name="L4"></param>
        /// <returns></returns>
        public static string GetCourtText(string L0, string L1, string L2, string L3, string L4)
        {
            StringBuilder oSB = new StringBuilder();
            Jurisdictions oJurs = null;
            Jurisdiction oJur;
            int iL0 = 0;
            int iL1 = 0;
            int iL2 = 0;
            int iL3 = 0;
            int iL4 = 0;

            //Get Level 0 Text
            try
            {
                iL0 = Int32.Parse(L0);
            }
            catch { }
            if (iL0 > 0)
            {
                oJurs = new Jurisdictions(Jurisdictions.Levels.Zero);
                if (oJurs.Count > 0)
                {
                    oJur = ((Jurisdiction)oJurs.ItemFromID(iL0));
                    if (oJur != null)
                        oSB.Append(oJur.Name);
                    else
                        return "";
                }
            }
            else
                return "";

            //Get Level 1 Text
            try
            {
                iL1 = Int32.Parse(L1);
            }
            catch { }
            if (iL1 > 0)
            {
                oJurs = new Jurisdictions(Jurisdictions.Levels.One);
                if (oJurs.Count > 0)
                {
                    oJur = ((Jurisdiction)oJurs.ItemFromID(iL1));
                    if (oJur != null)
                        oSB.Append("/" + oJur.Name);
                    else
                        return oSB.ToString();
                }
            }
            else
                return oSB.ToString();

            //Get Level 2 Text
            try
            {
                iL2 = Int32.Parse(L2);
            }
            catch { }
            if (iL2 > 0)
            {
                oJurs = new Jurisdictions(Jurisdictions.Levels.Two);
                if (oJurs.Count > 0)
                {
                    oJur = ((Jurisdiction)oJurs.ItemFromID(iL2));
                    if (oJur != null)
                        oSB.Append("/" + oJur.Name);
                    else
                        return oSB.ToString();
                }
            }
            else
                return oSB.ToString();

            //Get Level 3 Text
            try
            {
                iL3 = Int32.Parse(L3);
            }
            catch { }
            if (iL3 > 0)
            {
                oJurs = new Jurisdictions(Jurisdictions.Levels.Three);
                if (oJurs.Count > 0)
                {
                    oJur = ((Jurisdiction)oJurs.ItemFromID(iL3));
                    if (oJur != null)
                        oSB.Append("/" + oJur.Name);
                    else
                        return oSB.ToString();
                }
            }
            else
                return oSB.ToString();

            //Get Level 4 Text
            try
            {
                iL4 = Int32.Parse(L4);
            }
            catch { }
            if (iL4 > 0)
            {
                oJurs = new Jurisdictions(Jurisdictions.Levels.Four);
                if (oJurs.Count > 0)
                {
                    oJur = ((Jurisdiction)oJurs.ItemFromID(iL4));
                    if (oJur != null)
                        oSB.Append("/" + oJur.Name);
                }
            }
            return oSB.ToString();
        }

        ///// <summary>
        ///// returns supported languages for current client
        ///// </summary>
        //public static string SupportedLanguages
        //{
        //    get
        //    {
        //        if (m_xSupportedLanguages == null)
        //        {
        //            string xLanguages = User.FirmSettings.SupportedLanguages;
        //            if (!string.IsNullOrEmpty(xLanguages))
        //            {
        //                //this setting was originally held in the metadata table,
        //                //where pipes were fine - now that this is in the key sets
        //                //table, we need to replace these characters
        //                xLanguages = xLanguages.Replace('�', '�');
        //                xLanguages = xLanguages.Replace('$', '|');
        //                m_xSupportedLanguages = xLanguages;
        //            }
        //            else
        //                m_xSupportedLanguages = "English�1033";
        //        }
        //        return m_xSupportedLanguages;
        //    }
        //}

        /// <summary>
        /// converts from languages value to display value
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public static string GetLanguagesDisplayValue(string xValue)
        {
            string xDisplayValue = "";

            //load dictionary if necessary
            if (m_ldSupportedLanguages == null)
            {
                m_ldSupportedLanguages = new System.Collections.Specialized.ListDictionary();
                LMP.StringArray oLanguages = new LMP.StringArray(SupportedLanguagesString);
                string[,] aLanguages = oLanguages.ToArray();
                for (int i = 0; i <= aLanguages.GetUpperBound(0); i++)
                    m_ldSupportedLanguages.Add(aLanguages[i, 1], aLanguages[i, 0]);
            }

            //convert to display value
            string[] aValue = xValue.Split(LMP.StringArray.mpEndOfValue);
            for (int i = 0; i < aValue.Length; i++)
            {
                xDisplayValue += m_ldSupportedLanguages[aValue[i]].ToString();
                if (i < aValue.Length - 1)
                    xDisplayValue += ", ";
            }

            return xDisplayValue;
        }

        public static void CompactAdminDB()
        {
            //GLOG : 8883 : JSW
            if (!Registry.Is64Bit())
            {
                CompactJetDB(Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName); //JTS 3/28/13
            }
            else
            {
                //GLOG : 8883 : jsw
                //Notify user this action not available in 64-bit
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotCompactRepairDB"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        public static void CompactUserDBs()
        {
            CompactJetDB(Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName);

            //GLOG 7727: Don't attempt to compact mp10Segments.mdb
            //if it's shared by multiple users
            if (LMP.Registry.GetMacPac10Value("SplitUserDB") == "1" &&
                LMP.Registry.GetMacPac10Value("DoNotSyncSegments") != "1" &&
                File.Exists(Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb"))
            {
                CompactJetDB(Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb");
            }

            //GLOG 8220 (dm) - renamed db
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                if (File.Exists(Application.PublicDataDirectory + "\\ForteLocalPublic.mdb"))
                {
                    CompactJetDB(Application.PublicDataDirectory + "\\ForteLocalPublic.mdb");
                }
            }
        }

        public static void CompactJetDB(string xDB)
        {
            DateTime t0 = DateTime.Now;
            
            //GLOG : 8666 : JSW
            //JetEngine does not work in 64-bit process
            if (Registry.Is64Bit())
            {
                WriteDBLog("CompactJetDB failed: unable to run in 64-bit process");
                return;
            }

            //JetEngine COM object
            JRO.JetEngine oJRO = new JRO.JetEngine();

            //make sure db file exists
            if (File.Exists(xDB))
            {
				//GLOG 7546: Log problems during compaction process
                try
                {
                    WriteDBLog("CompactJetDB: Before Compact", xDB);
                    //compact the db - while this is not as effective at solving repair issues,
                    //it will deal with many common problems
                    oJRO.CompactDatabase(@"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        xDB, @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDB + ".new");

                    if (File.Exists(xDB + ".new"))
                    {
                        WriteDBLog("CompactJetDB: After Compact", xDB + ".new");
                        //delete original
                        File.Delete(xDB);

                        //rename new db to old name
                        File.Move(xDB + ".new", xDB);
                        WriteDBLog("CompactJetDB: After Compact", xDB);
                    }
                    else
                    {
                        WriteDBLog("CompactJetDB: No compacted DB created", xDB);
                    }
                    LMP.Benchmarks.Print(t0);
                }
                catch (System.Exception oE)
                {
                    Trace.WriteInfo("Compact of the following database failed: " + xDB);
                    WriteDBLog("CompactJetDB: Compact failed", xDB);
                    throw oE;
                }
            }
        }
        public static void RepairUserDBs()
        {

            //GLOG : 6522 : jsw
            //check existence of Forte.mdb here 
            //to avoid misleading error message
            if (File.Exists(Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName)) //JTS 3/28/13
            {
                RepairJetDB(Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName); //JTS 3/28/13

                //GLOG 6655: Don't attempt to repair ForteSegments.mdb if not used
                //GLOG 7727: Don't attempt to repair mp10Segments.mdb
                //if it's shared by multiple users
                if (LMP.Registry.GetMacPac10Value("SplitUserDB") == "1" &&
                    LMP.Registry.GetMacPac10Value("DoNotSyncSegments") != "1" &&
                    File.Exists(Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb"))
                {
                    RepairJetDB(Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb");
                }
            }
        }
        public static void RepairJetDB(string xDB)
        {
            DateTime t0 = DateTime.Now;

            //GLOG : 8666 : JSW
            //JetEngine does not work in 64-bit process
            if (Registry.Is64Bit())
            {
                WriteDBLog("RepairJetDB failed: unable to run in 64-bit process");
                return;
            }

            //JetEngine COM object
            JRO.JetEngine oJRO = new JRO.JetEngine();

            try
            {
                WriteDBLog("RepairJetDB: Before Repair", xDB); //GLOG 7546
                //use the jet comp utility if possible
                string xJetComp = LMP.Data.Application.AppDirectory + "\\JetComp.exe";

                if (File.Exists(xJetComp))
                {
                    System.Diagnostics.Process oProcess = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo oInfo = new System.Diagnostics.ProcessStartInfo(xJetComp,
                        " -src:\"" + xDB + "\" -dest:\"" + xDB + ".new\"");
                    oProcess.StartInfo = oInfo;
                    oProcess.Start();
					//GLOG 7546: Limit time to wait for completion of Jetcomp
                    oProcess.WaitForExit(120000);
                    try
                    {
                        if (!oProcess.HasExited)
                        {
                            WriteDBLog("RepairJetDB: JetComp not completed after 2 minutes");
                            try
                            {
                                oProcess.Kill();
                            }
                            catch { }
                            if (File.Exists(xDB + ".new"))
                                File.Delete(xDB + ".new");
                        }
                    }
                    catch { }

                }
                else
                {
                    //compact the db - while this is not as effective at solving repair issues,
                    //it will deal with many common problems
                    oJRO.CompactDatabase(@"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        xDB, @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDB + ".new");
                }

                if (File.Exists(xDB + ".new"))
                {
                    WriteDBLog("RepairJetDB: After Repair", xDB + ".new"); //GLOG 7546

                    //delete original
                    File.Delete(xDB);

                    //rename new db to old name
                    File.Move(xDB + ".new", xDB);
                }
                else //GLOG 7546
                {
                    WriteDBLog("RepairJetDB: No Repaired DB created", xDB);
                }
            }
            catch
            {
                Trace.WriteInfo("Repair of the following database failed: " + xDB);
                WriteDBLog("RepairJetDB: Repair failed", xDB); //GLOG 7546
            }

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// Update People sync queries with fields names to match current People table definition
        /// </summary>
        /// <param name="oCnn"></param>
        public static void UpdatePeopleRelatedQueries(OleDbConnection oCnn)
        {
            const string mpPeopleUpdateSQL1 = "CREATE PROCEDURE spSyncPeopleUpdate AS " +
                "UPDATE People AS p INNER JOIN SyncPeopleTmp AS t ON (p.ID1=t.ID1) AND (p.ID2=t.ID2) " +
                "SET p.OwnerID = t.OwnerID, p.UsageState = t.UsageState, p.UserID = t.UserID, " +
                "p.DisplayName = t.DisplayName, p.LinkedPersonID = t.LinkedPersonID, p.Prefix = t.Prefix, " +
                "p.LastName = t.LastName, p.FirstName = t.FirstName, p.MI = t.MI, p.Suffix = t.Suffix, " +
                "p.FullName = t.FullName, p.ShortName = t.ShortName, p.OfficeID = t.OfficeID, " +
                "p.Initials = t.Initials, p.IsAttorney = t.IsAttorney, p.IsAuthor = t.IsAuthor, " +
                "p.Title = t.Title, p.Phone = t.Phone, p.Fax = t.Fax, p.EMail = t.EMail, p.AdmittedIn = t.AdmittedIn, " +
                "p.DefaultOfficeRecordID1=t.DefaultOfficeRecordID1, p.OfficeUsageState=t.OfficeUsageState, p.LastEditTime = t.LastEditTime";
            const string mpPeopleUpdateSQL2 = " WHERE t.LastEditTime > p.LastEditTime;";
            
            const string mpPeopleCreateTmpSQL1 = "CREATE PROCEDURE spSyncPeopleTmpCreate AS " + 
                "CREATE TABLE SyncPeopleTmp (ID1 LONG, ID2 LONG, " + 
                "OwnerID LONG, UsageState  BYTE, UserID TEXT(255), DisplayName  TEXT(255),  LinkedPersonID LONG, " +
                "Prefix TEXT(255), LastName TEXT(255), FirstName TEXT(255), MI TEXT(255), Suffix TEXT(255), " +
                "FullName TEXT(255), ShortName TEXT(255), OfficeID LONG, Initials TEXT(255), IsAttorney YESNO, " +
                "IsAuthor YESNO, Title TEXT(255), Phone TEXT(255), Fax TEXT(255), EMail TEXT(255), " +
                "AdmittedIn TEXT(255), DefaultOfficeRecordID1 LONG, OfficeUsageState BYTE,  LastEditTime DATETIME";
            const string mpPeopleCreateTmpSQL2 = ", CONSTRAINT multifieldindex  PRIMARY KEY(ID1, ID2));";
            
            //GLOG 8220
            const string mpSyncPublicPeopleSQL1 = "CREATE PROCEDURE spPeopleSyncPublicPeople AS " +
                "UPDATE People AS p INNER JOIN PeoplePublic AS t ON (p.ID1=t.ID1) AND (p.ID2=t.ID2) " +
                "SET p.OwnerID = t.OwnerID, p.UsageState = t.UsageState, p.UserID = t.UserID, " +
                "p.DisplayName = t.DisplayName, p.LinkedPersonID = t.LinkedPersonID, p.Prefix = t.Prefix, " +
                "p.LastName = t.LastName, p.FirstName = t.FirstName, p.MI = t.MI, p.Suffix = t.Suffix, " +
                "p.FullName = t.FullName, p.ShortName = t.ShortName, p.OfficeID = t.OfficeID, " +
                "p.Initials = t.Initials, p.IsAttorney = t.IsAttorney, p.IsAuthor = t.IsAuthor, " +
                "p.Title = t.Title, p.Phone = t.Phone, p.Fax = t.Fax, p.EMail = t.EMail, p.AdmittedIn = t.AdmittedIn, " +
                "p.DefaultOfficeRecordID1=t.DefaultOfficeRecordID1, p.OfficeUsageState=t.OfficeUsageState, p.LastEditTime = t.LastEditTime";
            const string mpSyncPublicPeopleSQL2 = " WHERE t.LastEditTime > p.LastEditTime;";

            bool bCloseConnection = false;
            try
            {
                if ((oCnn.State & ConnectionState.Open) == 0)
                {
                    oCnn.Open();
                    bCloseConnection = true;
                }
                List<string> aCustomFields = GetPeopleCustomFieldList(oCnn);
                if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    string xSQL = mpPeopleCreateTmpSQL1;
                    for (int i = 0; i < aCustomFields.Count; i++)
                    {
                        xSQL = xSQL + string.Format(", [{0}] TEXT(255)", aCustomFields[i]);
                    }
                    xSQL = xSQL + mpPeopleCreateTmpSQL2;
                    OleDbCommand oCmd = new OleDbCommand();
                    oCmd.Connection = oCnn;
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = "DROP PROCEDURE spSyncPeopleTmpCreate;";
                    try
                    {
                        oCmd.ExecuteNonQuery();
                    }
                    catch { }
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = xSQL;
                    oCmd.ExecuteNonQuery();

                    //error occurs below if both tables used by spSyncPeopleUpdate don't exit
                    //so run query to create SyncPeopleTmp
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandText = "spSyncPeopleTmpCreate;";
                    oCmd.ExecuteNonQuery();

                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = "DROP PROCEDURE spSyncPeopleUpdate;";
                    try
                    {
                        oCmd.ExecuteNonQuery();
                    }
                    catch { }
                    xSQL = mpPeopleUpdateSQL1;
                    for (int i = 0; i < aCustomFields.Count; i++)
                    {
                        xSQL = xSQL + string.Format(", [p].[{0}] = [t].[{0}]", aCustomFields[i]);
                    }
                    xSQL = xSQL + mpPeopleUpdateSQL2;
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = xSQL;
                    oCmd.ExecuteNonQuery();
                    //Delete SyncPeopleTmp now that query is created
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = "DROP TABLE SyncPeopleTmp;";
                    try
                    {
                        oCmd.ExecuteNonQuery();
                    }
                    catch { }
                }
                else
                {
                    OleDbCommand oCmd = new OleDbCommand();
                    oCmd.Connection = oCnn;
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = "DROP PROCEDURE spPeopleSyncPublicPeople;";
                    try
                    {
                        oCmd.ExecuteNonQuery();
                    }
                    catch (System.Exception oE)
                    { }
                    string xSQL = mpSyncPublicPeopleSQL1;
                    for (int i = 0; i < aCustomFields.Count; i++)
                    {
                        xSQL = xSQL + string.Format(", [p].[{0}] = [t].[{0}]", aCustomFields[i]);
                    }
                    xSQL = xSQL + mpSyncPublicPeopleSQL2;
                    oCmd = new OleDbCommand();
                    oCmd.Connection = oCnn;
                    oCmd.CommandType = CommandType.Text;
                    oCmd.CommandText = xSQL;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DBConnectionException(LMP.Resources.GetLangString("Error_CouldNotUpdatePeopleQueries"), oE);
            }
            finally
            {
                //Return connection to initial state
                if (bCloseConnection)
                    oCnn.Close();
            }
        }
        public static List<string> GetPeopleCustomFieldList()
        {
            return GetPeopleCustomFieldList(LMP.Data.LocalConnection.ConnectionObject);
        }
        /// <summary>
        /// Returns List containing names of Custom Fields in local People table
        /// </summary>
        /// <param name="oCnn"></param>
        /// <returns></returns>
        public static List<string> GetPeopleCustomFieldList(OleDbConnection oCnn)
        {
            if ((oCnn.State & ConnectionState.Open) == 0)
            {
                oCnn.Open();
            }
            List<string> aCustomFields = new List<string>();

            OleDbCommand oCmd = new OleDbCommand();
            oCmd.Connection = oCnn;
            oCmd.CommandText = "SELECT TOP 1 * FROM PEOPLE;";

            using (OleDbDataReader oReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly))
            {
                try
                {
                    int iLocalFieldCount = oReader.FieldCount;

                    for (int i = 0; i + 24 < iLocalFieldCount; i++)
                    {
                        string xColumn = oReader.GetName(i + 24);
                        if (!ForteConstants.mpRequiredDBFields.Contains("|" + xColumn + "|"))
                            //this is a custom filed
                            aCustomFields.Add(xColumn);
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }

            return aCustomFields;
        }
        public static bool UpdatePeopleTableStructure(string xDBPath, List<string> aCustomFields)
        {
            try
            {
                bool bStructureChanged = false;
                bool bCloseConnection = false;
                List<string> aLocalFields = new List<string>();

                using (OleDbConnection oCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDBPath + ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                    @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                {
                    if ((oCnn.State & ConnectionState.Open) == 0)
                    {
                        oCnn.Open();
                        bCloseConnection = true;
                    }

                    OLEDB.OleDbDataReader oReader = null;

                    try
                    {
                        oReader = SimpleDataCollection.GetResultSet(oCnn,
                            "SELECT TOP 1 * FROM PEOPLE;", true);

                        for (int i = 24; i < oReader.FieldCount; i++)
                        {
                            aLocalFields.Add(oReader.GetName(i));
                        }
                    }
                    finally
                    {
                        //Need to close Reader before Commands can be executed
                        oReader.Close();
                        oReader.Dispose();
                    }

                    //Delete any local custom fields that are not in the new list
                    foreach (string xField in aLocalFields)
                    {
                        //JTS 1/5/09: This was deleting existing custom fields every time
                        //instead of only those not in the Server list
                        if (!ForteConstants.mpRequiredDBFields.Contains(xField) && !aCustomFields.Contains(xField))
                        {
                            OleDbCommand oCmd = new OleDbCommand();
                            oCmd.Connection = oCnn;
                            oCmd.CommandText = "ALTER TABLE PEOPLE DROP COLUMN [" + xField + "];";
                            oCmd.ExecuteNonQuery();
                            bStructureChanged = true;
                        }
                    }

                    //Add any custom fields that don't currently exist
                    foreach (string xField in aCustomFields)
                    {
                        if (!ForteConstants.mpRequiredDBFields.Contains(xField) && !aLocalFields.Contains(xField))
                        {
                            OleDbCommand oCmd = new OleDbCommand();
                            oCmd.Connection = oCnn;
                            oCmd.CommandText = "ALTER TABLE PEOPLE ADD [" + xField + "] Text(255);";
                            oCmd.ExecuteNonQuery();
                            bStructureChanged = true;
                        }
                    }

                    //If changes have been made, adjust sync queries to match -
                    //we've removed the conditional here to account for existing
                    //clients whose people table structure was manually altered, but
                    //whose queries needed updating.  This will always guarantee that
                    //the queries are always updated.
                    //if (bStructureChanged)
                    //{
                    UpdatePeopleRelatedQueries(oCnn);
                    //}
                    if (bCloseConnection)
                        oCnn.Close();

                    return bStructureChanged;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DBConnectionException(LMP.Resources.GetLangString("Error_CouldNotUpdateLocalPeopleTable"), oE);
            }
        }
        public static bool UpdateNetworkDBPeopleObjects(string xServer, string xDatabase, OleDbConnection oLocalCnn)
        {
            string xCnn = "";
            int iNetworkFieldCount = 0;
            int iTempFieldCount = 0;
            bool bStructureChanged = false;
            List<string> oDeletedCustomFields = new List<string>();
            List<string> oAddedCustomFields = new List<string>();

            List<string> aCustomFields = LMP.Data.Application.GetPeopleCustomFieldList(oLocalCnn);

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;MultipleActiveResultSets=true;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + mpSuperUserID +
                ";password=" + mpSuperUserPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                oCnn.Open();
                SqlCommand oCmd = new SqlCommand();
                oCmd.Connection = oCnn;
                oCmd.CommandText = "SELECT TOP 1 * FROM dbo.People;";
                using (SqlDataReader oNetworkReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly))
                {
                    try
                    {
                        iNetworkFieldCount = oNetworkReader.FieldCount;

                        //get list of Custom fields in Network People table that no longer exist in Admin DB
                        for (int i = iNetworkFieldCount - 1; i >= 24; i--)
                        {
                            string xColumn = (oNetworkReader.GetName(i));
                            if (!ForteConstants.mpRequiredDBFields.Contains("|" + xColumn + "|") &&
                                !aCustomFields.Contains(xColumn))
                            {
                                oDeletedCustomFields.Add(xColumn);
                            }
                        }

                        //get list of added custom fields
                        foreach (string xCustomField in aCustomFields)
                        {
                            int iIndex = -1;
                            try
                            {
                                iIndex = oNetworkReader.GetOrdinal(xCustomField);
                            }
                            catch
                            {
                            }
                            if (iIndex < 0)
                            {
                                oAddedCustomFields.Add(xCustomField);
                            }
                        }
                    }
                    finally
                    {
                        oNetworkReader.Close();
                    }
                }

                //delete fields from network db
                foreach (string xCustomField in oDeletedCustomFields)
                {
                    SqlCommand oCmd2 = new SqlCommand();
                    oCmd2.Connection = oCnn;
                    oCmd2.CommandText = "ALTER TABLE dbo.People DROP COLUMN [" + xCustomField + "];";
                    oCmd2.ExecuteNonQuery();
                    bStructureChanged = true;
                }

                //add fields to network db
                foreach (string xCustomField in oAddedCustomFields)
                {
                    //Add to Network People table any local Custom Fields that don't exist
                    SqlCommand oCmd2 = new SqlCommand();
                    oCmd2.Connection = oCnn;
                    oCmd2.CommandText = "ALTER TABLE dbo.People ADD [" + xCustomField + "] NVARCHAR(255);";
                    oCmd2.ExecuteNonQuery();
                    bStructureChanged = true;
                }

                //clear lists
                oDeletedCustomFields.Clear();
                oAddedCustomFields.Clear();

                oCmd = new SqlCommand();
                oCmd.Connection = oCnn;
                oCmd.CommandText = "SELECT TOP 1 * FROM dbo.SyncPeopleTmp;";
                using (SqlDataReader oTempReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly))
                {
                    try
                    {
                        iTempFieldCount = oTempReader.FieldCount;

                        //Delete any Custom fields in Network SyncPeopleTmp table that no longer exist in Admin DB
                        for (int i = iTempFieldCount - 1; i >= 24; i--)
                        {
                            string xColumn = (oTempReader.GetName(i));

                            if (!ForteConstants.mpRequiredDBFields.Contains("|" + xColumn + "|") &&
                                !aCustomFields.Contains(xColumn))
                            {
                                oDeletedCustomFields.Add(xColumn);
                            }
                        }
                        foreach (string xCustomField in aCustomFields)
                        {
                            int iIndex = -1;
                            try
                            {
                                iIndex = oTempReader.GetOrdinal(xCustomField);
                            }
                            catch
                            {
                            }
                            if (iIndex < 0)
                            {
                                oAddedCustomFields.Add(xCustomField);
                            }
                        }
                    }
                    finally
                    {
                        oTempReader.Close();
                    }
                }

                foreach (string xCustomField in oDeletedCustomFields)
                {
                    SqlCommand oCmd2 = new SqlCommand();
                    oCmd2.Connection = oCnn;
                    oCmd2.CommandText = "ALTER TABLE dbo.SyncPeopleTmp DROP COLUMN [" + xCustomField + "];";
                    oCmd2.ExecuteNonQuery();
                    bStructureChanged = true;
                }

                foreach (string xCustomField in oAddedCustomFields)
                {
                    //Add to Network SyncPeopleTmp table any local Custom Fields that don't exist
                    SqlCommand oCmd2 = new SqlCommand();
                    oCmd2.Connection = oCnn;
                    oCmd2.CommandText = "ALTER TABLE dbo.SyncPeopleTmp ADD [" + xCustomField + "] NVARCHAR(255);";
                    oCmd2.ExecuteNonQuery();
                    bStructureChanged = true;
                }

                if (bStructureChanged)
                {
                    UpdateNetworkPeopleQueries(oCnn, aCustomFields);
                }
                oCnn.Close();
            }
            return bStructureChanged;
        }

        //public static bool UpdateNetworkDBPeopleObjectsOLD(string xServer, string xDatabase, OleDbConnection oLocalCnn)
        //{
        //    string xCnn = "";
        //    SqlDataReader oNetworkReader = null;
        //    SqlDataReader oTempReader = null;
        //    int iNetworkFieldCount;
        //    int iTempFieldCount;
        //    SqlCommand oCmd;
        //    SqlCommand oCmd2;
        //    bool bStructureChanged = false;
        //    List<string> aCustomFields = null;

        //    aCustomFields = LMP.Data.Application.GetPeopleCustomFieldList(oLocalCnn);

        //    //create connection string from parameters
        //    xCnn = "Persist Security Info=false;Integrated Security=false;MultipleActiveResultSets=true;" +
        //        "database=" + xDatabase + ";server=" + xServer + @";user id=" + mpSuperUserID +
        //        ";password=" + mpSuperUserPassword;

        //    //connection object
        //    using (SqlConnection oCnn = new SqlConnection(xCnn))
        //    {
        //        oCnn.Open();
        //        oCmd = new SqlCommand();
        //        oCmd.Connection = oCnn;
        //        oCmd.CommandText = "SELECT TOP 1 * FROM dbo.People;";
        //        oNetworkReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly);
        //        iNetworkFieldCount = oNetworkReader.FieldCount;

        //        //Delete any Custom fields in Network People table that no longer exist in Admin DB
        //        for (int i = iNetworkFieldCount - 1; i >= 26; i--)
        //        {
        //            string xColumn = (oNetworkReader.GetName(i));
        //            if (!aCustomFields.Contains(xColumn))
        //            {
        //                oCmd2 = new SqlCommand();
        //                oCmd2.Connection = oCnn;
        //                oCmd2.CommandText = "ALTER TABLE dbo.People DROP COLUMN [" + xColumn + "];";
        //                oCmd2.ExecuteNonQuery();
        //                bStructureChanged = true;
        //            }
        //        }

        //        foreach (string xCustomField in aCustomFields)
        //        {
        //            int iIndex = -1;
        //            try
        //            {
        //                iIndex = oNetworkReader.GetOrdinal(xCustomField);
        //            }
        //            catch
        //            {
        //            }
        //            if (iIndex < 0)
        //            {
        //                //Add to Network People table any local Custom Fields that don't exist
        //                oCmd2 = new SqlCommand();
        //                oCmd2.Connection = oCnn;
        //                oCmd2.CommandText = "ALTER TABLE dbo.People ADD [" + xCustomField + "] NVARCHAR(255);";
        //                oCmd2.ExecuteNonQuery();
        //                bStructureChanged = true;
        //            }
        //        }
        //        oNetworkReader.Close();

        //        oCmd = new SqlCommand();
        //        oCmd.Connection = oCnn;
        //        oCmd.CommandText = "SELECT TOP 1 * FROM dbo.SyncPeopleTmp;";
        //        oTempReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly);
        //        iTempFieldCount = oTempReader.FieldCount;

        //        //Delete any Custom fields in Network SyncPeopleTmp table that no longer exist in Admin DB
        //        for (int i = iTempFieldCount - 1; i >= 26; i--)
        //        {
        //            string xColumn = (oTempReader.GetName(i));
        //            if (!aCustomFields.Contains(xColumn))
        //            {
        //                oCmd2 = new SqlCommand();
        //                oCmd2.Connection = oCnn;
        //                oCmd2.CommandText = "ALTER TABLE dbo.SyncPeopleTmp DROP COLUMN [" + xColumn + "];";
        //                oCmd2.ExecuteNonQuery();
        //                bStructureChanged = true;
        //            }
        //        }
        //        foreach (string xCustomField in aCustomFields)
        //        {
        //            int iIndex = -1;
        //            try
        //            {
        //                iIndex = oTempReader.GetOrdinal(xCustomField);
        //            }
        //            catch
        //            {
        //            }
        //            if (iIndex < 0)
        //            {
        //                //Add to Network SyncPeopleTmp table any local Custom Fields that don't exist
        //                oCmd2 = new SqlCommand();
        //                oCmd2.Connection = oCnn;
        //                oCmd2.CommandText = "ALTER TABLE dbo.SyncPeopleTmp ADD [" + xCustomField + "] NVARCHAR(255);";
        //                oCmd2.ExecuteNonQuery();
        //                bStructureChanged = true;
        //            }
        //        }
        //        oTempReader.Close();
        //        if (bStructureChanged)
        //        {
        //            UpdateNetworkPeopleQueries(oCnn, aCustomFields);
        //        }
        //        oCnn.Close();
        //        return bStructureChanged;
        //    }
        //}
        /// <summary>
        /// Sets SQL of People-related Stored Procedures to include any defined custom fields
        /// </summary>
        /// <param name="oCnn"></param>
        /// <param name="aFields"></param>
        private static void UpdateNetworkPeopleQueries(SqlConnection oCnn, List<string> aFields)
        {
            const string mpPeopleUpdateSQL1 = "ALTER PROCEDURE dbo.spSyncUpPeopleUpdate AS\r\nUPDATE People " +
                "SET OwnerID = s.OwnerID, UsageState = s.UsageState, UserID = s.UserID, \r\n" +
                "DisplayName = s.DisplayName, LinkedPersonID = s.LinkedPersonID, Prefix = s.Prefix, \r\n" +
                "LastName = s.LastName, FirstName = s.FirstName, MI = s.MI, Suffix = s.Suffix, \r\n" +
                "FullName = s.FullName, ShortName = s.ShortName, OfficeID = s.OfficeID, \r\n" +
                "Initials = s.Initials, IsAttorney = s.IsAttorney, IsAuthor = s.IsAuthor, \r\n" +
                "Title = s.Title, Phone = s.Phone, Fax = s.Fax, EMail = s.EMail, AdmittedIn = s.AdmittedIn, \r\n" +
                "DefaultOfficeRecordID1=s.DefaultOfficeRecordID1, OfficeUsageState=s.OfficeUsageState, LastEditTime = s.LastEditTime";
            const string mpPeopleUpdateSQL2 = " \r\nFROM PEOPLE p INNER JOIN SyncPeopleTmp s ON (p.ID1=s.ID1) AND (p.ID2=s.ID2)";
            const string mpPeopleAddSQL1 = "ALTER PROCEDURE dbo.spSyncUpPeopleAdd AS \r\nINSERT INTO People (ID1, ID2, OwnerID, UsageState, UserID, DisplayName, \r\n" +
                "LinkedPersonID, Prefix, LastName, FirstName, MI, Suffix, FullName, ShortName, OfficeID, \r\n" +
                "Initials, IsAttorney, IsAuthor, Title, Phone, Fax, EMail, AdmittedIn, \r\n" +
                "DefaultOfficeRecordID1, OfficeUsageState, LastEditTime";
            const string mpPeopleAddSQL2 = ") \r\nSELECT s.ID1, s.ID2, s.OwnerID, s.UsageState, s.UserID, s.DisplayName, \r\n" +
                "s.LinkedPersonID, s.Prefix, s.LastName, s.FirstName, s.MI, s.Suffix, s.FullName, s.ShortName, \r\n" +
                "s.OfficeID, s.Initials, s.IsAttorney, s.IsAuthor, s.Title, s.Phone, s.Fax, s.EMail, s.AdmittedIn, \r\n" +
                "s.DefaultOfficeRecordID1, s.OfficeUsageState, s.LastEditTime";
            const string mpPeopleAddSQL3 = " \r\nFROM SyncPeopleTmp s LEFT JOIN People p ON s.ID1 = p.ID1 AND s.ID2 = p.ID2 WHERE p.ID1 is null;";

            //Build Update SQL including Custom fields
            string xSQL = mpPeopleUpdateSQL1;
            for (int i = 0; i < aFields.Count; i++)
            {
                xSQL = xSQL + string.Format(", [{0}] = [s].[{0}]", aFields[i]);
            }
            xSQL = xSQL + mpPeopleUpdateSQL2;
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = oCnn;
            oCmd.CommandText = xSQL;
            oCmd.ExecuteNonQuery();

            //Build Add SQL including Custom fields
            xSQL = mpPeopleAddSQL1;
            for (int i = 0; i < aFields.Count; i++)
            {
                xSQL = xSQL + string.Format(", [{0}]", aFields[i]);
            }
            xSQL = xSQL + mpPeopleAddSQL2;
            for (int i = 0; i < aFields.Count; i++)
            {
                xSQL = xSQL + string.Format(", [s].[{0}]", aFields[i]);
            }
            xSQL = xSQL + mpPeopleAddSQL3;
            oCmd = new SqlCommand();
            oCmd.Connection = oCnn;
            oCmd.CommandText = xSQL;
            oCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// returns TRUE if specified type is a collection table or
        /// collection table item type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public static bool IsCollectionTableType(mpObjectTypes oType)
        {
            switch (oType)
            {
                case mpObjectTypes.LetterSignature:
                case mpObjectTypes.LetterSignatures:
                case mpObjectTypes.AgreementSignature:
                case mpObjectTypes.AgreementSignatures:
                case mpObjectTypes.PleadingSignature:
                case mpObjectTypes.PleadingSignatures:
                case mpObjectTypes.PleadingCaption:
                case mpObjectTypes.PleadingCaptions:
                case mpObjectTypes.PleadingCounsel:
                case mpObjectTypes.PleadingCounsels:
                case mpObjectTypes.CollectionTableItem:
                case mpObjectTypes.CollectionTable:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// returns whether specified segment type should display in doc editor
        /// </summary>
        /// <param name="oSegmentType"></param>
        /// <returns></returns>
        public static bool DisplayInToolkitTaskPane(mpObjectTypes oSegmentType)
        {
            if (m_xToolkitTaskPaneSegmentTypes == null)
            {
                string xTypes = GetMetadata("TaskPaneSegmentTypes");
                if (xTypes != null)
                    m_xToolkitTaskPaneSegmentTypes = '|' + xTypes + '|';
                else
                    m_xToolkitTaskPaneSegmentTypes = "";
            }

            return m_xToolkitTaskPaneSegmentTypes.Contains('|' +
                ((System.Int32)oSegmentType).ToString() + '|');
        }

        //GLOG : 8031 : ceh
        public static string CleanCoreAddress(string xCountry, string xCoreAddress)
        {
            //remove country from CoreAddress if necessary
            if ((xCountry != null) && g_oCurUser.IsUserCountry(xCountry))
            {
                xCoreAddress = xCoreAddress.TrimEnd(("\r\n" + xCountry).ToCharArray());
                xCoreAddress = xCoreAddress.TrimEnd(("\r" + xCountry).ToCharArray());
                xCoreAddress = xCoreAddress.TrimEnd(("\n" + xCountry).ToCharArray());
                xCoreAddress = xCoreAddress.TrimEnd((" " + xCountry).ToCharArray());
            }

            return xCoreAddress;
        }

		//GLOG 7546: Log size of active Database
        public static void WriteDBLog(string xMsg)
        {
            WriteDBLog(xMsg, "");
        }
        public static void WriteDBLog(string xMsg, string xDBPath)
        {
            if (Registry.GetMacPac10Value("EnableDBLogging") != "1")
                return;

            System.IO.StreamWriter oWriter = null;
            try
            {
                string xFormat = "{0} | {1} | {2}";
                string xDB = "";
                string xSize = "";
                string xUser = Environment.UserName;
                    
                if (xDBPath != "")
                {
                    xFormat = xFormat + " | {3}={4}";
                    FileInfo oFI = new FileInfo(xDBPath);
                    xDB = oFI.Name;
                    //exit if file doesn't exist - nothing to update
                    if (!oFI.Exists)
                        xSize = "N/A";
                    else
                        xSize = oFI.Length.ToString();
                }
                //GLOG 15949: Write log to WritableDataDirectory location if specified
                string xPath = GetDirectory(LMP.Data.mpDirectories.WritableDB) + "\\ForteDB_" + Environment.UserName + ".log";
                oWriter = new System.IO.StreamWriter(xPath, true);
                oWriter.Write(string.Format(xFormat, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), xUser, xMsg, xDB, xSize) + "\r\n");
            }
            catch
            {
            }
            finally
            {
                if (oWriter != null)
                {
                    oWriter.Close();
                    oWriter.Dispose();
                }
            }
        }

        //GLOG : 8819 : ceh
        private static ICSession GetCISession()
        {
            ICSession oSession = null;
            Assembly oAsm = null;
            string xAssemblyName = "CI.dll";
            string xClassName = "TSG.CI.CSession";

            string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAssemblyName;

            try
            {
                //load assembly
                oAsm = Assembly.LoadFrom(xAsmDllFullName);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
            }

            try
            {
                //create class instance
                oSession = (ICSession)oAsm.CreateInstance(xClassName, true);

                 if (oSession == null)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
            }

            return oSession;

        }
        #endregion
    }

    public class TwoColumnListObject
    {
        public string DisplayMember{get; set;}
        public string ValueMember{get; set;}
    }

}
