using System;
using System.Data.OleDb;
using System.Collections;

namespace LMP.Data
{
	public enum mpFolderTypes: byte
	{
		Admin = 1,
		User = 2
	}

    public enum mpFolderMemberTypes : short
    {
        UserSegmentPacket = 96,
        UserSegment = 99,
        AdminSegment = 100,
        VariableSet = 104
    }
	/// <summary>
	/// contains the methods and properties that define a MacPac folder member,
	/// an icon that references a segment that is accessible from a folder;
	/// there are two kinds of folders - admin folders and user folders;
	/// an admin folder member is a record in the Assignments table - its id is an internal
	/// construct in the form "FolderID.SegmentID";
	/// the id of a user folder member is the id of its record in the UserFolderMembers table -
	/// derived from StringIDSimpleDataItem - created by Daniel Fisherman 09/04
	/// </summary>
	public class FolderMember:StringIDSimpleDataItem
	{
		private int m_iObjectID1 = 0;
		private int m_iObjectID2 = 0;
        private mpFolderMemberTypes m_shObjectTypeID = 0;
		private string m_xName = null;
		private int m_iFolderID1 = 0;
		private int m_iFolderID2 = 0;
		private mpFolderTypes m_bytFolderType = mpFolderTypes.Admin;

		#region *********************constructors*********************
		/// <summary>
		/// initializes a new user folder member
		/// </summary>
		/// <param name="iFolderID1"></param>
		/// <param name="iFolderID2"></param>
		internal FolderMember(int iFolderID1, int iFolderID2)
		{
			m_bytFolderType = mpFolderTypes.User;
			m_iFolderID1 = iFolderID1;
			m_iFolderID2 = iFolderID2;

			//create and set ID
			int iID1;
			int iID2;
			Application.GetNewUserDataItemID(out iID1, out iID2);
			string xID = string.Concat(iID1.ToString(), ".", iID2.ToString());
			this.SetID(xID);
		}

		/// <summary>
		/// initializes a new admin folder member
		/// </summary>
		/// <param name="iFolderID"></param>
		internal FolderMember(int iFolderID)
		{
			m_bytFolderType = mpFolderTypes.Admin;
            m_shObjectTypeID = mpFolderMemberTypes.AdminSegment;
			m_iFolderID1 = iFolderID;
		}

		#endregion

		#region *********************properties*********************
		/// <summary>
		/// gets the type of folder that contains this member
		/// </summary>
		public mpFolderTypes FolderType
		{
			get{return m_bytFolderType;}
		}

		/// <summary>
		/// gets/sets the ID1 of the folder that contains this member
		/// </summary>
		public int FolderID1
		{
			get{return m_iFolderID1;}
			set{this.SetIntPropertyValue(ref m_iFolderID1, value);}
		}

		/// <summary>
		/// gets/sets the ID2 of the folder that contains this member
		/// </summary>
		public int FolderID2
		{
			get{return m_iFolderID2;}
			set{this.SetIntPropertyValue(ref m_iFolderID2, value);}
		}

		/// <summary>
		/// gets/sets the name of the folder member
		/// read-only for admin folder members -
		/// read/write for user folder members
		/// </summary>
		public string Name
		{
			get{return m_xName;}
			set
			{
				throw new LMP.Exceptions.PropertyException(LMP.Resources
					.GetLangString("Error_ReadOnlySegmentFolderMemberProperty") + "Name");
			}
		}
        /// <summary>
        /// gets/sets FolderMember type ID
		/// read-only for admin folder members -
		/// read/write for user folder members
        /// </summary>
        public mpFolderMemberTypes ObjectTypeID
        {
            get { return m_shObjectTypeID; }
            set 
            {
                if (this.FolderType == mpFolderTypes.User)
                {
                    if (m_shObjectTypeID != value)
                    {
                        m_shObjectTypeID = value;
                        this.IsDirty = true;
                    }
                }
                else
                {
                    throw new LMP.Exceptions.PropertyException(LMP.Resources
                        .GetLangString("Error_ReadOnlySegmentFolderMemberProperty") + "ObjectTypdID");
                }
            }

        }
		/// <summary>
		/// gets/sets the first id of the segment referred to by this folder member
		/// </summary>
		public int ObjectID1
		{
			get{return m_iObjectID1;}
			set{this.SetIntPropertyValue(ref m_iObjectID1, value);}
		}

		/// <summary>
		/// gets/sets the second id of the segment referred to by this folder member -
		/// always 0 for admin folder members
		/// </summary>
		public int ObjectID2
		{
			get{return m_iObjectID2;}
			set{this.SetIntPropertyValue(ref m_iObjectID2, value);}
		}

		#endregion

		#region *********************methods*********************
		internal void SetName(string xName)
		{
			this.SetStringPropertyValue(ref m_xName, xName, 255);
		}
		#endregion

		#region *********************StringIDSimpleDataItem members*********************
		internal override object[] GetAddQueryParameters()
		{
			if (this.FolderType == mpFolderTypes.Admin)
			{
				object[] oProps = new object[3];
				oProps[0] = this.FolderID1;
				oProps[1] = this.ObjectID1;
                oProps[2] = Application.GetCurrentEditTime();
				return oProps;
			}
			else
			{
				int iID1;
				int iID2;
				Application.SplitID(this.ID, out iID1, out iID2);

				object[] oProps = new object[9];
				oProps[0] = iID1;
				oProps[1] = iID2;
				oProps[2] = this.FolderID1;
				oProps[3] = this.FolderID2;
                oProps[4] = this.ObjectTypeID;
				oProps[5] = this.ObjectID1;
				oProps[6] = this.ObjectID2;
                // This property not used for Segment-type FolderMembers
				oProps[7] = null;
                oProps[8] = Application.GetCurrentEditTime();
				return oProps;
			}
		}
        internal override object[] GetUpdateQueryParameters()
        {
            return new object[] {
				this.ID, this.FolderType, this.FolderID1, this.FolderID2,
				this.ObjectTypeID, this.ObjectID1, this.ObjectID2, this.Name, 
                Application.GetCurrentEditTime()};
        }

		/// <summary>
		/// returns true iff the FolderMember is valid
		/// </summary>
		/// <returns></returns>
		internal override bool IsValid()
		{
			bool bIsValid = false;
			if (this.FolderType == mpFolderTypes.Admin)
			{
				bIsValid = (this.FolderID1 != 0) && (this.ObjectID1 != 0);
			}
			else
			{
				bIsValid = (this.FolderID1 != 0) && (this.FolderID2 != 0) &&
					(this.ObjectID1 != 0) && (this.ObjectTypeID != 0);
			}
			return bIsValid;
		}
	
		public override object[] ToArray()
		{
			return new object[] {
				this.ID, this.FolderType, this.FolderID1, this.FolderID2,
				this.ObjectTypeID, this.ObjectID1, this.ObjectID2, this.Name};
		}
		#endregion
	}


	public class FolderMembers: StringIDSimpleDataCollection
	{
		private StringIDUpdateObjectDelegate m_oDel = null;
		private mpFolderTypes m_bytFolderTypeFilter = mpFolderTypes.Admin;
		private int m_iFolderID1Filter = 0;
		private int m_iFolderID2Filter = 0;

		#region *********************constructors*********************
		/// <summary>
		/// initializes a new admin folder members collection
		/// </summary>
		/// <param name="iFolderIDFilter"></param>
		public FolderMembers(int iFolderIDFilter)
		{
			m_bytFolderTypeFilter = mpFolderTypes.Admin;
			m_iFolderID1Filter = iFolderIDFilter;
		}

		/// <summary>
		/// initializes a new user folder members collection
		/// </summary>
		/// <param name="iFolderID1Filter"></param>
		/// <param name="iFolderID2Filter"></param>
		public FolderMembers(int iFolderID1Filter, int iFolderID2Filter)
		{
			m_bytFolderTypeFilter = mpFolderTypes.User;
			m_iFolderID1Filter = iFolderID1Filter;
			m_iFolderID2Filter = iFolderID2Filter;
		}
		#endregion

		#region *********************properties*********************
		public mpFolderTypes FolderTypeFilter
		{
			get{return m_bytFolderTypeFilter;}
		}

		public int FolderID1Filter
		{
			get{return m_iFolderID1Filter;}
		}

		public int FolderID2Filter
		{
			get{return m_iFolderID2Filter;}
		}
		#endregion

		#region *********************methods*********************
		public override void Save(StringIDSimpleDataItem oItem)
		{
			//cast to folder member object
			FolderMember oMember = (FolderMember) oItem;
			if(oMember.IsDirty)
			{
				if(!oMember.IsValid())
				{
					//some data is not valid - alert
					throw new LMP.Exceptions.DataException(string.Concat(
						LMP.Resources.GetLangString("Error_InvalidOrMissingObjectData"), 
						oMember.GetType(),"-\n", oMember.ToString()));
				}

				if(!oMember.IsPersisted)
					AddRecord(oMember);
				else
					UpdateRecord(oMember);
			}
		}

		/// <summary>
		/// delete folder member
		/// </summary>
		/// <param name="xID">for admin folder members, use form "FolderID.ObjectID"</param>
		public override void Delete(string xID)
		{
			Trace.WriteNameValuePairs("xID", xID);

            //parse ID
			string[] aIDs = xID.Split('.');

            // If associated Segment is not assigned to any other folders
            // delete Segment object as well
            string xSegCountSProc = "";
            string xSegDeleteSProc = "";
            string xObjectID = "";
            mpFolderMemberTypes iMemberType = 0;
            object[] oParams = null;

            // GLOG : 2797 : JAB
            // Store the object type which will later be used to
            // remove the sharing association.
            mpObjectTypes iObjectType = 0;

            if (m_bytFolderTypeFilter == mpFolderTypes.Admin)
            {
                iMemberType = mpFolderMemberTypes.AdminSegment;
                xSegCountSProc = "spSegmentsCountFolderAssignments";
                xSegDeleteSProc = "spSegmentsDelete";
                // Segment ID is 2nd part of FolderMember ID
                xObjectID = aIDs[1];
                oParams = new object[] { aIDs[1].ToString() };
                Trace.WriteNameValuePairs("Segment ID", xObjectID);
                //GLOG 7977: Get segment TypeID to delete assignments
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xObjectID));
                iObjectType = oDef.TypeID;
            }
            else
            {
                object[] oValues = null;

                OleDbDataReader oReader = null;

                try
                {
                    // Get ID of associated UserSegment
                    oReader = GetResultSet("spUserFolderMembersGetTypeAndIDFromFolderMemberID", true, aIDs);

                    if (oReader.HasRows)
                    {
                        //get values
                        oReader.Read();
                        oValues = new object[oReader.FieldCount];
                        oReader.GetValues(oValues);
                        xObjectID = oValues[0].ToString() + "." + oValues[1].ToString();
                        iMemberType = (mpFolderMemberTypes)(int)oValues[2];
                        // Use Object IDs for query parameters
                        oParams = new object[] { oValues[0], oValues[0], oValues[1] };
                        Trace.WriteNameValuePairs("Object ID", xObjectID, "MemberType", iMemberType);
                    }
                }
                finally
                {
                    oReader.Close();
                    oReader.Dispose();
                }

                // Get appropriate queries for Member Type
                switch (iMemberType)
                {
                    case mpFolderMemberTypes.AdminSegment:
                        // Don't try to delete Admin Segment
                        xSegCountSProc = "";
                        xSegDeleteSProc = "";
                        break;
                    case mpFolderMemberTypes.UserSegment:
                    case mpFolderMemberTypes.UserSegmentPacket: //GLOG 8295
                        xSegCountSProc = "spUserSegmentsCountFolderAssignments";
                        xSegDeleteSProc = "spUserSegmentsDelete";

                        // GLOG : 2797 : JAB
                        // The sharing association will be apply to a UserSegment.
                        iObjectType = mpObjectTypes.UserSegment;
                        break;
                    case mpFolderMemberTypes.VariableSet:
                        xSegCountSProc = "spVariableSetsCountFolderAssignments";
                        xSegDeleteSProc = "spVariableSetsDelete";

                        // GLOG : 2797 : JAB
                        // The sharing association will be apply to a VariableSet.
                        iObjectType = mpObjectTypes.VariableSet;
                        break;
                }

            }

            //execute query
			int iRecs = ExecuteActionSproc(this.DeleteSprocName, 
				new object[] {aIDs[0], aIDs[1]});
            
            //log deletion
            if (m_bytFolderTypeFilter == mpFolderTypes.Admin)
                SimpleDataCollection.LogDeletions(mpObjectTypes.AdminFolderMembers, xID);
            else if (m_bytFolderTypeFilter == mpFolderTypes.User)
                SimpleDataCollection.LogDeletions(mpObjectTypes.UserFolderMembers, xID);


			if(iRecs == 0)
			{
				//not deleted - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
			}
			else
			{
                //delete item from array if necessary
				if(m_oArray != null)
				{
					//get the index to delete
					int iIndex = GetItemIndex(xID);

					Trace.WriteNameValuePairs("iIndex", iIndex);

					//delete if found in array
					if(iIndex > -1)
						m_oArray.RemoveAt(iIndex);
				}
                if (xSegCountSProc != "")
                {
                    object oRet = GetScalar(xSegCountSProc, oParams);
                    if ((int)oRet == 0)
                    {
                        // GLOG : 2797 : JAB
                        // Remove any folder or user-based sharing association of user segments and
                        // variable set prior to deleting it.

                        if (iMemberType == mpFolderMemberTypes.UserSegment ||
                            iMemberType == mpFolderMemberTypes.VariableSet ||
                            iMemberType == mpFolderMemberTypes.UserSegmentPacket) //GLOG 8295
                        {
                            try
                            {
                                int iObjectID1;
                                int iObjectID2;

                                String.SplitID(xObjectID, out iObjectID1, out iObjectID2);

                                //GLOG 6448: If ID1 of FolderMember and base object is different, this is a shortcut
                                //to a shared item.  Don't delete original object or permissions
                                if (iObjectID1.ToString() == aIDs[0])
                                {
                                    // GLOG : 2797 : JAB
                                    // Remove any folder-based sharing association to this segment prior to deleting it.
                                    if (SharedFolder.GetSharedFolderID(iObjectID1, iObjectID2, iObjectType) > 0)
                                    {
                                        SharedFolder.RemoveSharedFolderAssignment(iObjectID1, iObjectID2, iObjectType);
                                    }

                                    // GLOG : 2797 : JAB
                                    // Remove any user-based sharing association to this segment prior deleting it.
                                    if (UserPermissions.GetPermittedUsers(iObjectType, iObjectID1, iObjectID2).Count > 0)
                                    {
                                        UserPermissions.ClearPermissions(iObjectType, iObjectID1, iObjectID2);
                                    }
                                }
                                else
                                {
                                    //GLOG 6448: Nothing more to delete
                                    return;
                                }
                            }
                            catch (System.Exception oE)
                            {
                                LMP.Error.Show(oE);
                            }
                        }

                        // No other assignments for Segment, then delete Segment record as well
                        int iDel = ExecuteActionSproc(xSegDeleteSProc, new object[] { xObjectID });
                        if (iDel > 0)
                        {
                            if (iMemberType == mpFolderMemberTypes.UserSegment || iMemberType == mpFolderMemberTypes.AdminSegment 
                                || iMemberType == mpFolderMemberTypes.UserSegmentPacket) //GLOG 8295
                            {
                                //log deletion
                                if (iMemberType == mpFolderMemberTypes.UserSegment || iMemberType == mpFolderMemberTypes.UserSegmentPacket) //GLOG 8295
                                {
                                    SimpleDataCollection.LogDeletions(mpObjectTypes.UserSegment, xObjectID);
                                }
                                else if (iMemberType == mpFolderMemberTypes.AdminSegment)
                                {
                                    SimpleDataCollection.LogDeletions(mpObjectTypes.Segment, xObjectID);
                                    //GLOG 7977: Also delete related Keysets and Assignments
                                    int iKeysets = ExecuteActionSproc("spKeySetsDeleteByObjectIDs", new object[] {xObjectID, ForteConstants.mpFirmRecordID.ToString() });
                                    if (iKeysets > 0)
                                        LogDeletions(mpObjectTypes.KeySets, xObjectID);
                                    int iAssign = ExecuteActionSproc("spAssignmentsDeleteByObjectIDs", new object[] { ((int)iObjectType).ToString(), xObjectID});
                                    if (iAssign > 0)
                                        LogDeletions(mpObjectTypes.Assignments, ((int)iObjectType).ToString() + "." +  xObjectID);
                                }
                                Trace.WriteInfo("Deleted Segment Record");
                            }
                            else if (iMemberType == mpFolderMemberTypes.VariableSet)
                            {
                                //log deletion
                                SimpleDataCollection.LogDeletions(mpObjectTypes.VariableSet, xObjectID);
                                Trace.WriteInfo("Deleted Variable Set Record");
                            }
                        }
                    }
                }
            }
		}
        /// <summary>
        /// returns 2-column array with Display Name and ID of each member of collection
        /// </summary>
        /// <returns></returns>
        public ArrayList ToDisplayArray()
        {
            DateTime t0 = DateTime.Now;
            string xSQL;
            if (m_bytFolderTypeFilter == mpFolderTypes.Admin)
            {
                if (LMP.Resources.CultureName == "en-US")
                    xSQL = "spFolderMembersForDisplay";
                else
                    xSQL = "spFolderMembersForDisplayTranslated";
            }
            else
                xSQL = "spUserFolderMembersForDisplay";

            //get reader with display fields only
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                    xSQL, false, this.SelectSprocParameters))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                    LMP.Benchmarks.Print(t0);
                    return oArray;
                }
                finally
                {
                    oReader.Close();
                }
            }
        }

		#endregion

		#region *********************private members*********************
		/// <summary>
		/// adds the specified FolderMember to the db
		/// </summary>
		/// <param name="oMember">the FolderMember to add</param>
		private void AddRecord(FolderMember oMember)
		{
			if(oMember == null)
				//reference to item can't be null - alert
				throw new LMP.Exceptions.NullReferenceException(
					LMP.Resources.GetLangString("Error_ReferenceCantBeNull"));

			//get query parameters
			object[] aParams = oMember.GetAddQueryParameters();

			//add member to folder
			int iRecs = SimpleDataCollection.ExecuteActionSproc(this.AddSprocName, aParams);

			if(iRecs > 0)
			{
				if (oMember.FolderType == mpFolderTypes.Admin)
					//set internal ID of added member
					oMember.SetID(string.Concat(oMember.FolderID1, ".", oMember.ObjectID1));

				//force array to repopulate
				m_oArray = null;

				//update flags
				oMember.IsPersisted = true;
				oMember.IsDirty = false;
			}
		}

		private void UpdateRecord(FolderMember oMember)
		{
			if(oMember == null)
				//reference to item can't be null - alert
				throw new LMP.Exceptions.NullReferenceException(
					LMP.Resources.GetLangString("Error_ReferenceCantBeNull"));

			if(oMember.FolderType == mpFolderTypes.Admin)
			{
				//can't edit admin folder members
				throw new LMP.Exceptions.ActionException(
					LMP.Resources.GetLangString("Error_AdminFolderMembersNotUpdatable"));
			}

			Trace.WriteNameValuePairs("oMember.ID", oMember.ID, "oMember.FolderID1",
				oMember.FolderID1, "oMember.FolderID2", oMember.FolderID2, "oMember.ObjectTypeID", oMember.ObjectTypeID,
                "oMember.ObjectID1", oMember.ObjectID1, "oMember.ObjectID2", oMember.ObjectID2, "oMember.Name",
				oMember.Name);

			//get update sproc parameters
			object[] aParams = oMember.GetAddQueryParameters();

			int iRecs = ExecuteActionSproc("spUserFolderMembersUpdate", aParams);

			if(iRecs > 0)
			{
				//item was saved
				oMember.IsDirty = false;

				if(m_oArray != null)
				{
					//save changes to array -
					//get index of edited item
					int iIndex = GetItemIndex(oMember.ID);

					Trace.WriteNameValuePairs("iIndex", iIndex);

					//update value array if item was found
					if(iIndex > -1)
						base.m_oArray[iIndex] = aParams;
				}
			}
			else
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + oMember.ID);
		}

		/// <summary>
		/// returns a FolderMember having the specified values
		/// </summary>
		/// <param name="oValues"></param>
		/// <returns></returns>
		private StringIDSimpleDataItem UpdateObject(object[] oValues)
		{
			FolderMember oMember;

            DateTime t0 = DateTime.Now;

            if (this.FolderTypeFilter == mpFolderTypes.Admin)
            {
                //admin
                oMember = new FolderMember(m_iFolderID1Filter);
                oMember.SetID(oValues[0].ToString());
                oMember.ObjectID1 = (int)oValues[1];
                oMember.SetName(oValues[2].ToString());
            }
            else
            {
                //user
                oMember = new FolderMember(m_iFolderID1Filter, m_iFolderID2Filter);
                oMember.SetID(oValues[0].ToString());
                oMember.ObjectTypeID = (mpFolderMemberTypes)((int)oValues[1]);
                oMember.ObjectID1 = (int)oValues[2];
                oMember.ObjectID2 = (int)oValues[3];
                oMember.SetName(oValues[4].ToString());
            }

			oMember.IsDirty = false;

            LMP.Benchmarks.Print(t0);

			return oMember;
		}
		#endregion

		#region *********************LongIDSimpleDataCollection members*********************
		protected override StringIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new StringIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
					return "spFolderMembersAdd";
				else
					return "spUserFolderMembersAdd";
			}
		}

		protected override string CountSprocName
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
					return "spFolderMembersCount";
				else
					return "spUserFolderMembersCount";
			}
		}

		protected override string DeleteSprocName
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
					return "spFolderMembersDelete";
				else
					return "spUserFolderMembersDelete";
			}
		}

		protected override string ItemFromIDSprocName
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
				{
					if (LMP.Resources.CultureName == "en-US")
						return "spFolderMembersItemFromID";
					else
						return "spFolderMembersItemFromIDTranslated";
				}
				else
					return "spUserFolderMembersItemFromID";
			}
		}

		protected override string SelectSprocName
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
				{
					if (LMP.Resources.CultureName == "en-US")
						return "spFolderMembers";
					else
						return "spFolderMembersTranslated";
				}
				else
					return "spUserFolderMembers";
			}
		}

		protected override object[] SelectSprocParameters
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
				{
					if (LMP.Resources.CultureName == "en-US")
						return new object[] {this.FolderID1Filter};
					else
						return new object[] {this.FolderID1Filter, LMP.Resources.CultureID};
				}
				else
					return new object[] {this.FolderID1Filter, this.FolderID2Filter};
			}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get
			{
				if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
				{
					if (LMP.Resources.CultureName == "en-US")
						return null;
					else
						return new object[] {LMP.Resources.CultureID};
				}
				else
					return this.SelectSprocParameters;
			}
		}

		protected override object[] CountSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get
			{
				throw new LMP.Exceptions.NotImplementedException(
					LMP.Resources.GetLangString("Error_NotImplemented"));
			}
		}

		public override StringIDSimpleDataItem Create()
		{
			if(m_bytFolderTypeFilter == mpFolderTypes.Admin)
				return new FolderMember(this.FolderID1Filter);
			else
				return new FolderMember(this.FolderID1Filter, this.FolderID2Filter);
		}

		#endregion
	}
}
