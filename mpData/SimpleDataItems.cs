using System;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace LMP.Data
{
	//defines a MacPac SimpleDataItem, an abstract class
	//inherited by many LongID and StringID simple data classes
	abstract public class SimpleDataItem
	{
		private bool m_bIsDirty = false;
		private bool m_bIsPersisted = false;

        public void Export()
        {
            Export(null, true);
        }
        /// <summary>
        /// exports this data item to a .mpd file
        /// </summary>
        public virtual void Export(string xFileName, bool bOverwriteExisting)
        {
            //JTS: I've removed this code to avoid confusion, since this virtual version 
            //is never used - only the specific UserSegmentDef or AdminSegmentDef variants

            //const string EXPORT_REG_VALUE_NAME = "SegmentExportPath"; //GLOG 6647
            //System.Type oType = this.GetType();
            //string xContent = oType.Name + LMP.Data.ForteConstants.mpDataFileFieldSep + 
            //    this.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);

            //if (string.IsNullOrEmpty(xFileName))
            //{
            //    //prompt for filename/location
            //    using (SaveFileDialog oDlg = new SaveFileDialog())
            //    {
            //        oDlg.Title = LMP.Resources.GetLangString("Lbl_ExportItem");
            //        oDlg.Filter = LMP.ComponentProperties.ProductName + " Data File (*.mpd)|*.mpd";
            //        //GLOG 6647: Get last selected path from registry
            //        string xPath = LMP.Registry.GetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME);
            //        //If invalid or not set, default to Writable Data location
            //        if (string.IsNullOrEmpty(xPath) || !Directory.Exists(xPath))
            //            xPath = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
            //        oDlg.InitialDirectory = xPath;
            //        oDlg.CheckPathExists = true;
            //        oDlg.OverwritePrompt = bOverwriteExisting;
            //        if (oDlg.ShowDialog() == DialogResult.OK)
            //        {
            //            //GLOG 6647: Save selected path as default
            //            LMP.Registry.SetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME, Path.GetDirectoryName(oDlg.FileName));
            //            if (bOverwriteExisting)
            //            {
            //                //write to file
            //                File.WriteAllText(oDlg.FileName, xContent);
            //            }
            //            else
            //            {
            //                FileStream oStream = null;

            //                //append to file
            //                if (File.Exists(oDlg.FileName))
            //                    oStream = File.OpenWrite(oDlg.FileName);
            //                else
            //                    oStream = File.Create(oDlg.FileName);

            //                using (oStream)
            //                {
            //                    using (StreamWriter oSW = new StreamWriter(oStream))
            //                    {
            //                        oSW.Write(xContent);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    //file name was provided - use it
            //    FileStream oStream = null;

            //    //append to file
            //    if (File.Exists(xFileName))
            //        oStream = File.OpenWrite(xFileName);
            //    else
            //        oStream = File.Create(xFileName);

            //    using (oStream)
            //    {
            //        using (StreamWriter oSW = new StreamWriter(oStream))
            //        {
            //            oSW.Write(xContent);
            //        }
            //    }
            //}
        }

		/// <summary>
		/// returns the item's data as a delimited string
		/// </summary>
		/// <returns></returns>
		public string ToString(string xSeparator)
		{
			object[] aItemVals = this.ToArray();
			string xVals = null;

			//build string with item values
			foreach(object oVal in aItemVals)
				xVals = xVals + ((oVal == null) ? "NULL" : oVal.ToString() + xSeparator);

			return xVals;
		}

        public override string ToString()
        {
            return ToString(";");
        }
		/// <summary>
		/// sets/returns the edit state of the item
		/// </summary>
		public virtual bool IsDirty
		{
			get{return m_bIsDirty;}
			set{m_bIsDirty = value;}
		}

		/// <summary>
		/// returns true iff the item has been saved to a MacPac data source
		/// </summary>
		internal virtual bool IsPersisted
		{
			get{return m_bIsPersisted;}
			set{m_bIsPersisted = value;}
		}

		/// <summary>
		/// returns the item as an array of values
		/// </summary>
		/// <returns></returns>
		public abstract object[] ToArray();
		/// <summary>
		/// returns an object array of the parameters for the add query
		/// </summary>
		/// <returns></returns>
		internal abstract object[] GetAddQueryParameters();
		/// <summary>
		/// returns an object array of the parameters for the update query
		/// </summary>
		/// <returns></returns>
		internal virtual object[] GetUpdateQueryParameters()
		{
			return ToArray();
		}

		/// <summary>
		/// returns true if the item passes all validation criteria
		/// </summary>
		/// <returns></returns>
		internal abstract bool IsValid();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		/// <param name="iMaxLength">maximum length of string - specify 0 for an unlimited length</param>
		protected void SetStringPropertyValue(ref string xVar, string xVal, int iMaxLength)
		{
			if(xVar != xVal)
			{
				if(iMaxLength > 0 && xVal.Length > iMaxLength)
				{
					throw new LMP.Exceptions.DataException(
						Resources.GetLangString("Error_PropertyCharacterOverflow"));
				}

				xVar = xVal;
				this.IsDirty = true;
			}
		}

		/// <summary>
		/// sets the value of the specified string variable to the specified value, with a maximum length of 255 characters.
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		protected void SetStringPropertyValue(ref string xVar, string xVal)
		{
			SetStringPropertyValue(ref xVar, xVal, 255);
		}

		/// <summary>
		/// sets the value of the specified integer variable to the specified value
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		protected void SetIntPropertyValue(ref int iVar, int iVal)
		{
			if(iVar != iVal)
			{
				iVar = iVal;
				this.IsDirty = true;
			}
		}

		/// <summary>
		/// sets the value of the specified byte variable to the specified value
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		protected void SetBytePropertyValue(ref byte byVar, byte byVal)
		{
			if(byVar != byVal)
			{
				byVar = byVal;
				this.IsDirty = true;
			}
		}

		/// <summary>
		/// sets the value of the specified short variable to the specified value
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		protected void SetShortPropertyValue(ref short shVar, short shVal)
		{
			if(shVar != shVal)
			{
				shVar = shVal;
				this.IsDirty = true;
			}
		}

		/// <summary>
		/// sets the value of the specified boolean variable to the specified value
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		protected void SetBoolPropertyValue(ref bool bVar, bool bVal)
		{
			if(bVar != bVal)
			{
				bVar = bVal;
				this.IsDirty = true;
			}
		}
	}


	/// <summary>
	/// defines a MacPac Simple Data Item that has a Long ID- 
	/// is inherited by many MacPac data classes
	/// </summary>
	abstract public class LongIDSimpleDataItem:SimpleDataItem
	{
		protected int m_iID;

		internal LongIDSimpleDataItem(){}
		internal LongIDSimpleDataItem(int ID)
		{
			m_iID = ID;
		}

		/// <summary>
		/// sets the ID of this item - used internally only
		/// </summary>
		/// <param name="iID"></param>
		public virtual void SetID(int iID)
		{
			if(m_iID != iID)
			{
				m_iID = iID;
				this.IsDirty = true;
			}
		}
	
		/// <summary>
		/// returns the ID of this item
		/// </summary>
		public virtual int ID
		{
			get{return m_iID;}
		}

	}


	/// <summary>
	/// defines a MacPac Simple Data Item that has a String ID
	/// is inherited by many MacPac data classes
	/// </summary>
	abstract public class StringIDSimpleDataItem:SimpleDataItem
	{
		protected string m_xID;

		internal StringIDSimpleDataItem(){}
		internal StringIDSimpleDataItem(string xID)
		{
			m_xID = xID;
		}

		/// <summary>
		/// returns the ID of the item
		/// </summary>
		public virtual string ID{get{return m_xID;}}

		/// <summary>
		/// sets the ID of the item - used internally only
		/// </summary>
		/// <param name="xID"></param>
		internal virtual void SetID(string xID)
		{
			if(xID != m_xID)
			{
				m_xID = xID;
				this.IsDirty = true;
			}
		}

	}
}
