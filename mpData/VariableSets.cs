using System;
using System.Text.RegularExpressions;

namespace LMP.Data
{
    /// <summary>
    /// user segments can be filtered by 
    /// either owner or permitted user
    /// </summary>
    public enum mpVariableSetsFilterFields
    {
        User = 1,
        Owner = 2
    }

    /// <summary>
    /// defines a MacPac Variable Set definition -
    /// a segment defined by the user
    /// </summary>
    public class VariableSetDef : StringIDSimpleDataItem
    {   
        #region *********************fields*********************
        private int m_iOwnerID;
        private string m_xName = "";
        private string m_xDescription = "";
        private string m_xSegmentID = "";
        private string m_xContentString = "";
        #endregion

        #region *********************constructors*********************
        /// <summary>
        /// creates new user segment def and assigns IDs
        /// </summary>
        internal VariableSetDef():base()
        {
            //create and set ID
            int iID1;
            int iID2;

            Application.GetNewUserDataItemID(out iID1, out iID2);
            this.SetID(iID1, iID2);
        }

        /// <summary>
        /// creates new user segment def, assigning specified IDs
        /// </summary>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        internal VariableSetDef(int iID1, int iID2):base()
        {
            //create and set ID
            this.SetID(iID1, iID2);
        }
        #endregion

        #region *********************properties*********************
        public int OwnerID
        {
            get { return m_iOwnerID; }
            set { this.SetIntPropertyValue(ref m_iOwnerID, value); }
        }

        public string Name
        {
            get { return m_xName; }
            set { this.SetStringPropertyValue(ref m_xName, value); }
        }

        public string Description
        {
            get { return m_xDescription; }
            set { this.SetStringPropertyValue(ref m_xDescription, value); }
        }

        public string ContentString
        {
            get { return m_xContentString; }
            set { this.SetStringPropertyValue(ref m_xContentString, value, 0); }
        }

        public string SegmentID
        {
            get { return m_xSegmentID; }
            set { this.SetStringPropertyValue(ref m_xSegmentID, value); }
        }
        #endregion

        #region *********************methods*********************
        /// <summary>
        /// assigns the specified IDs to the user segment
        /// </summary>
        /// <param name="ID1"></param>
        /// <param name="ID2"></param>
        internal void SetID(int ID1, int ID2)
        {
            this.m_xID = ID1.ToString() + "." + ID2.ToString();
            this.IsDirty = true;
        }

        #endregion

        #region *********************StringIDSimpleDataItem members*********************
        internal override object[] GetAddQueryParameters()
        {
            int iID1 = 0;
            int iID2 = 0;

            object[] oParams = new object[9];
            Application.SplitID(this.ID, out iID1, out iID2);

            oParams[0] = iID1;
            oParams[1] = iID2;
            oParams[2] = this.OwnerID;
            oParams[3] = 0;
            oParams[4] = this.Name;
            oParams[5] = this.Description;
            oParams[6] = this.SegmentID;
            oParams[7] = this.ContentString;
            oParams[8] = Application.GetCurrentEditTime();

            return oParams;
        }

        internal override object[] GetUpdateQueryParameters()
        {
            int iID1;
            int iID2;

            object[] oParams = new object[9];
            Application.SplitID(this.ID, out iID1, out iID2);

            oParams[0] = iID1;
            oParams[1] = iID2;
            oParams[2] = this.OwnerID;
            oParams[3] = 0;
            oParams[4] = this.Name;
            oParams[5] = this.Description;
            oParams[6] = this.SegmentID;
            oParams[7] = this.ContentString;
            oParams[8] = Application.GetCurrentEditTime();

            return oParams;
        }

        internal override bool IsValid()
        {
            //returns true if id is of the form number.number,
            //and display name and ContentString are not empty
            return Regex.IsMatch(this.ID, @"\d{1,9}\.\d{1,9}") &
                this.Name != "" & this.Description != "";
        }

        public override object[] ToArray()
        {
            object[] oVals = new object[6];
            oVals[0] = this.ID;
            oVals[1] = this.OwnerID;
            oVals[2] = this.Name;
            oVals[3] = this.Description;
            oVals[4] = this.SegmentID;
            oVals[5] = this.ContentString;

            return oVals;
        }
        #endregion
    }

    public class VariableSetDefs : StringIDSimpleDataCollection
    {
        #region *********************fields*********************
        private mpVariableSetsFilterFields m_iFilterField;
        private int m_iFilterValue;
        private StringIDUpdateObjectDelegate m_oDel;
        #endregion

        #region *********************constructors*********************
        public VariableSetDefs(mpVariableSetsFilterFields iFilterField, int iFilterValue)
        {
            SetFilter(iFilterField, iFilterValue);
        }

        #endregion

        #region *********************methods*********************
        public void SetFilter(mpVariableSetsFilterFields iFilterField, int iFilterValue)
        {
            m_iFilterField = iFilterField;
            m_iFilterValue = iFilterValue;
            m_oArray = null;
        }
        #endregion

        #region *********************StringIDSimpleDataCollection members*********************
        protected override string AddSprocName
        {
            get { return "spVariableSetsAdd"; }
        }

        protected override string CountSprocName
        {
            get
            {
                if (m_iFilterField == mpVariableSetsFilterFields.Owner)
                    return "spVariableSetsCount";
                else
                    return "spVariableSetsByPermissionCount";
            }
        }

        protected override object[] CountSprocParameters
        {
            get { return new object[] { m_iFilterValue }; }
        }

        protected override string DeleteSprocName
        {
            get { return "spVariableSetsDelete"; }
        }

        protected override string ItemFromIDSprocName
        {
            get
            {
                if (m_iFilterField == mpVariableSetsFilterFields.Owner)
                    return "spVariableSetsItemFromID";
                else
                    return "spVariableSetsByPermissionItemFromID";
            }
        }

        protected override object[] ItemFromIDSprocParameters
        {
            get { return this.SelectSprocParameters; }
        }

        protected override string SelectSprocName
        {
            get
            {
                if (m_iFilterField == mpVariableSetsFilterFields.Owner)
                    return "spVariableSets";
                else
                    return "spVariableSetsByPermission";
            }
        }

        protected override object[] SelectSprocParameters
        {
            get
            {
                if (m_iFilterValue == 0)
                    return new object[] { System.DBNull.Value };
                else
                    return new object[] { m_iFilterValue };
            }
        }

        protected override string UpdateSprocName
        {
            get { return "spVariableSetsUpdate"; }
        }

        protected override StringIDUpdateObjectDelegate UpdateObjectDelegate
        {
            get
            {
                if (m_oDel == null)
                    m_oDel = new StringIDUpdateObjectDelegate(this.UpdateObject);

                return m_oDel;
            }
        }

        public override StringIDSimpleDataItem Create()
        {
            return new VariableSetDef();
        }
        #endregion

        #region *********************private members*********************
        private StringIDSimpleDataItem UpdateObject(object[] oVals)
        {
            VariableSetDef oDef = new VariableSetDef(
                Convert.ToInt32(oVals[0]), Convert.ToInt32(oVals[1]));
            oDef.OwnerID = (int)oVals[2];
            oDef.Name = oVals[3] == null ? "" : oVals[3].ToString();
            oDef.Description = oVals[4] == null ? "" : oVals[4].ToString();
            oDef.SegmentID = oVals[5] == null ? "" : oVals[5].ToString();
            oDef.ContentString = oVals[6] == null ? "" : oVals[6].ToString();

            return oDef;
        }
        #endregion
    }
}
