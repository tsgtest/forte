using System;
using System.Data;
using System.Data.OleDb;
using System.Collections;

namespace LMP.Data
{
	public enum mpListColumns: byte
	{
		NotApplicable = 0,
		One = 1,
		Two = 2
	}

	/// <summary>
	/// contains the methods and properties that define a MacPac list
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class List: LongIDSimpleDataItem
	{
		private string m_xName = "";
		private string m_xSQL = "";
		private string m_xConnectionString = "";
		private string m_xWhereClause = "";
		private int m_iCulture = 0;
		private int m_iSetID = 0;
		private mpListColumns m_bytColumnCount = mpListColumns.NotApplicable;
		private mpListColumns m_bytSortColumn = mpListColumns.NotApplicable;
		private ArrayList m_oList = null;
		private DataSet m_oDS = null;

		#region *********************constructors*********************
		internal List(){}
		internal List(int ID): base(ID){}
		#endregion

		#region *********************properties*********************

		/// <summary>
		/// gets/sets the name of the list
		/// </summary>
		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}

		/// <summary>
		/// gets/sets the sql statement used to populate
		/// a lists whose source is a sql db
		/// </summary>
		public string SQL
		{
			get{return m_xSQL;}
			set{this.SetStringPropertyValue(ref m_xSQL, value, 0);} //GLOG 6521: This is a Memo field, so should not use default Max length
		}

		/// <summary>
		/// gets/sets the connection string of the 
		/// sql statement that will populate the tlist
		/// </summary>
		public string ConnectionString
		{
			get{return m_xConnectionString;}
			set{this.SetStringPropertyValue(ref m_xConnectionString, value);}
		}

		/// <summary>
		/// gets/sets the Where clause of the SQL statement
		/// that will be used to populate the list
		/// </summary>
		public string WhereClause
		{
			get{return m_xWhereClause;}
			set
			{
				this.SetStringPropertyValue(ref m_xWhereClause, value);
				m_oList = null;
			}	
		}

		/// <summary>
		/// gets/sets the culture ID of the list
		/// </summary>
		public int Culture
		{
			get
			{
				if(m_iCulture == 0)
				{
					m_iCulture = Language.mpUSEnglishCultureID;
				}
				return m_iCulture;
			}
			set
			{
				if (m_iCulture != value)
				{
					this.SetIntPropertyValue(ref m_iCulture, value);

					// close recordset if open, so it will be updated 
					// with appropriate records at next access
					m_oList = null;
				}
			}
		}

		/// <summary>
		/// gets/sets the ID of the value set
		/// that contains the list items for this list
		/// </summary>
		public int ValueSetID
		{
			get{return m_iSetID;}
			set{this.SetIntPropertyValue(ref m_iSetID, value);}
		}

		/// <summary>
		/// gets/sets the number of columns of the 
		/// value set to include in the list
		/// </summary>
		public mpListColumns ValueSetColumnCount
		{
			get{return m_bytColumnCount;}
			set
			{
				if(m_bytColumnCount != value)
				{
					m_bytColumnCount = value;
					this.IsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/gets the index of the column in the value set 
		/// that will be used to sort the list items
		/// </summary>
		public mpListColumns ValueSetSortColumn
		{
			get{return m_bytSortColumn;}
			set
			{
				if(m_bytSortColumn != value)
				{
					m_bytSortColumn = value;
					this.IsDirty = true;
				}
			}
		}

		/// <summary>
		/// returns the items in the list as an ArrayList
		/// </summary>
		public ArrayList ItemArray
		{
			get
			{
				if(m_oList == null)
					this.Refresh();

				return m_oList;
			}
		}

		/// <summary>
		/// returns the list items as part of a DataSet
		/// </summary>
		public DataSet ItemDataSet
		{
			get
			{
				if(m_oDS == null)
					this.Refresh(out m_oDS, this.Culture);
	
				return m_oDS;
			}
		}

		/// <summary>
		/// returns the list items as a DataTable
		/// </summary>
		public DataTable ItemDataTable
		{
			get{return this.ItemDataSet.Tables["Table"];}
		}

		/// <summary>
		/// returns the number of items in the list
		/// </summary>
		public int ItemCount
		{
			get
			{
				//get list items if necessary
				if (m_oList == null)
					Refresh();

				return m_oList.Count;
			}
		}


		/// <summary>
		/// gets the number of columns in the list
		/// </summary>
		public byte ColumnCount
		{
			get
			{
				if(m_oList==null)
					Refresh();

				if(m_oList.Count > 0)
				{
					//get number of columns from number of array elements in first array item
					object[] oArr = null;
					oArr = (object[]) m_oList[0];
					return (byte) oArr.Length;
				}
				else
					return 0;
			}
		}

		#endregion

		#region *********************methods*********************
		/// <summary>
		/// populates the list with the appropriate list items
		/// </summary>
		public void Refresh()
		{
			Refresh(out m_oList, this.Culture);
		}

		/// <summary>
		/// populates the specified DataSet with the appropriate list items for this list
		/// </summary>
		/// <param name="oList">list to populate</param>
		private void Refresh(out DataSet oDS, int iCultureID)
		{
			OleDbConnection oCn;
			string xSQL;

            switch (this.Name)
            {
                case "SystemFonts":
                    {
                        //TODO: get system fonts as a dataset (oDS)
                        oDS = new DataSet();
                        break;
                    }
                default:
                    {
                        GetRetrievalData(out oCn, out xSQL, iCultureID);

                        //get array list of list items
                        oDS = SimpleDataCollection.GetDataSet(oCn, xSQL);
                        DataTable oDT = oDS.Tables[0];
                        //GLOG 3464: replace %s% token to get around Access Sort Orders not
                        //allowing values that are duplicated except for 'ss' vs. '�'
                        DataColumn oCol = null;
                        try
                        {
                            oCol = oDT.Columns["Value1"];
                        }
                        catch { }
                        if (oCol != null)
                        {
                            foreach (DataRow oRow in oDT.Rows)
                            {
                                if (oRow["Value1"].ToString().Contains("%s%"))
                                    oRow["Value1"] = oRow["Value1"].ToString().Replace("%s%", "�");
                            }
                        }
                        break;
                    }
            }
		}
	
		/// <summary>
		/// populates the specified ArrayList with the appropriate list items for this list
		/// </summary>
		/// <param name="oList">list to populate</param>
		private void Refresh(out ArrayList oList, int iCultureID)
		{
			OleDbConnection oCn;
			string xSQL;

            //GLOG item #6473 - dcf
            //if (this.ValueSetID != 0)
            //{
                switch (this.Name)
                {
                    case "SystemFonts":
                        {
                            //TODO: get system fonts as an arraylist (oList)
                            oList = new ArrayList();
                            break;
                        }
                    default:
                        {
                            //list is an internal list - get items
                            GetRetrievalData(out oCn, out xSQL, iCultureID);

                            //get array list of list items
                            oList = SimpleDataCollection.GetArray(oCn, xSQL);
                            break;
                        }
                }
            //}
            //else
            //    oList = null;
		}

		/// <summary>
		/// returns the Connection and SQL needed to retrieve list items
		/// </summary>
		/// <param name="oCn"></param>
		/// <param name="xSQL"></param>
		private void GetRetrievalData(out OleDbConnection oCn, out string xSQL, int iCultureID)
		{
			string xCn = null;
 
			Trace.WriteNameValuePairs("m_xConnectionString", 
				m_xConnectionString, "m_iSetID", m_iSetID, 
				"iCultureID", iCultureID);

			if(m_xConnectionString == "" || m_xConnectionString == "LocalDBConnection")
			{
				oCn = LocalConnection.ConnectionObject;
			}
			else
			{
				//open new connection
				xCn = m_xConnectionString;
				oCn = new OleDbConnection(xCn);

				//get engine type to which we're connected
				string xUpper = xCn.ToUpper();
			}

			if(m_iSetID != 0)
			{
				//this is a standard MacPac List
				if(iCultureID != Language.mpUSEnglishCultureID)
				{
					//get non-English translation
					xSQL = "SELECT * FROM (SELECT IIf((t.Value1 Is Null),v.Value1,t.Value1) AS Value1";

					if(m_bytColumnCount == mpListColumns.Two)
						//list is two columns - add additional clause
						xSQL += ", IIf((t.Value2 Is Null),v.Value2,t.Value2) AS Value2";

					xSQL = string.Concat(xSQL, " FROM ValueSets AS v LEFT JOIN (SELECT * FROM Translations ",
						"WHERE LocaleID=", iCultureID, ") AS t ON v.TranslationID = t.ID ",
						"WHERE SetID=", m_iSetID, ")");
				}
				else
				{
					//uses us english select statement
					xSQL = "SELECT Value1";

					if(m_bytColumnCount == mpListColumns.Two)
						//list is two columns - add additional field
						xSQL += ", Value2";

					xSQL += " FROM ValueSets WHERE SetID=" + m_iSetID;
				}

				//add sort order
				if((m_bytSortColumn == mpListColumns.Two))
					xSQL += " ORDER BY Value2";
				else
					xSQL += " ORDER BY Value1";
			}
			else
				xSQL = m_xSQL;

			if(m_xWhereClause != "")
			{
				//add filter to last WHERE clause in statement
				int iPos = xSQL.ToUpper().LastIndexOf(" WHERE ");

				if(iPos > 0)
				{
					string xUpToWhere = xSQL.Substring(0, iPos + 6);
					string xAfterWhere = xSQL.Substring(iPos + 7);
					xSQL = xUpToWhere + "(" + m_xWhereClause + ") AND " + xAfterWhere;
				}
				else
					xSQL += " WHERE (" + m_xWhereClause + ")";
			}

			Trace.WriteNameValuePairs("xSQL", xSQL);

		}
		/// <summary>
		/// adds the specified values as the values of a list item
		/// </summary>
		/// <param name="oValues">the values comprising the list item</param>
		public void AddItem(params object[] oValues)
		{
			Trace.WriteNameValuePairs("m_iSetID", m_iSetID);
			Trace.WriteParameters(oValues);

			if(this.ID==0)
			{
				//list has not yet been saved - alert
				throw new LMP.Exceptions.MethodCallException(
					string.Concat(LMP.Resources.GetLangString("Error_ListMustBeSaved"),"(", this.Name, ")"));
			}

			if(this.ValueSetID==0)
			{
				//list is not editable - alert
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_ListIsNotEditable") + this.Name);
			}

			//throw exception if the number of values to be added
			//is not the same as the number of columns in the list -
			//get number of list columns
			LMP.Data.mpListColumns bytNumVals = (oValues.Length==1) ? 
				LMP.Data.mpListColumns.One : LMP.Data.mpListColumns.Two;

			if(bytNumVals != this.ValueSetColumnCount)
			{
				//wrong number of values - raise exception
				throw new LMP.Exceptions.ParameterCountException(
					LMP.Resources.GetLangString("Error_WrongNumberOfValues") + 
					this.ValueSetColumnCount.ToString());
			}
			
			//create parameters array
			object[] oParams = new object[3];

			//first parameter is value set id
			oParams[0] = this.ValueSetID;

			//next is the first value
			oParams[1] = oValues[0];

			//second value may not be supplied,
			//but is required by the sproc - set
			//null if not supplied
			if(oValues.Length==1)
				oParams[2] = System.DBNull.Value;
			else
				oParams[2] = oValues[1];


			SimpleDataCollection.ExecuteActionSproc("spListsItemsAdd", oParams);

			this.m_oList = null;
		}

		/// <summary>
		/// edits the specified values as the values of a list item
		/// </summary>
		/// <param name="xCol1Value">the column 1 value of the list item</param>
		/// <param name="oValues">the values comprising the list item</param>
		public void EditItem(string xCol1Value, params object[] oValues)
		{
			Trace.WriteNameValuePairs("m_iSetID", m_iSetID, "xCol1Value", xCol1Value);
			Trace.WriteParameters(oValues);

			if(this.ID==0)
			{
				//list has not yet been saved - alert
				throw new LMP.Exceptions.MethodCallException(
					string.Concat(LMP.Resources.GetLangString("Error_ListMustBeSaved"),"(", this.Name, ")"));
			}

			if(this.ValueSetID==0)
			{
				//list is not editable - alert
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_ListIsNotEditable") + this.Name);
			}

			//throw exception if the number of values to be edited
			//is not the same as the number of columns in the list -
			//get number of list columns - both column values must be supplied
			LMP.Data.mpListColumns bytNumVals = (oValues.Length==1) ? 
				LMP.Data.mpListColumns.One : LMP.Data.mpListColumns.Two;

			if(bytNumVals != this.ValueSetColumnCount)
			{
				//wrong number of values - raise exception
				throw new LMP.Exceptions.ParameterCountException(
					LMP.Resources.GetLangString("Error_WrongNumberOfValues" + 
					this.ValueSetColumnCount));
			}
			
			//create parameters array
			object[] oParams = new object[4];

			//first parameter is value set id
			oParams[0] = this.ValueSetID;

			//second is column 1 value
			oParams[1] = xCol1Value;

			//next is the first value
			oParams[2] = oValues[0];

			//second value may not be supplied,
			//but is required by the sproc - set
			//null if not supplied
			if(oValues.Length==1)
				oParams[3] = System.DBNull.Value;
			else
				oParams[3] = oValues[1];

			int iRecsAffected = SimpleDataCollection.ExecuteActionSproc(
				"spListsItemsEdit", oParams);

			if(iRecsAffected == 0)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ListItemDoesNotExist") + xCol1Value);

			this.m_oList = null;
		}

		/// <summary>
		/// deletes the specified list item
		/// </summary>
		/// <param name="xCol1Value">the Column 1 value of the list item</param>
		public void DeleteItem(string xCol1Value)
		{
			Trace.WriteNameValuePairs("m_iSetID", m_iSetID, "xCol1Value", xCol1Value);

			if(this.ID==0)
			{
				//list has not yet been saved - alert
				throw new LMP.Exceptions.MethodCallException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ListMustBeSaved"),"(", this.Name, ")"));
			}

			if(this.ValueSetID==0)
			{
				//list is not editable - alert
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_ListIsNotEditable") + this.Name);
			}
			
			//get translation id for later deletion
			int lTranslationID = LMP.Data.Language.GetValueTranslationID(
				this.ValueSetID, xCol1Value);

			//create parameters array
			object[] oParams = new object[2];

			//first parameter is value set id
			oParams[0] = this.ValueSetID;

			//second is column 1 value
			oParams[1] = xCol1Value;

			//delete the value set
			int iRecsAffected = SimpleDataCollection.ExecuteActionSproc(
				"spListsItemsDelete", oParams);

			if(iRecsAffected == 0)
				//no records affected - we're assuming the item wasn't found
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ListItemDoesNotExist") + xCol1Value);

			if(lTranslationID !=0)
				//delete the translation record associated with 
				//this item, if it's not being used elsewhere
				LMP.Data.Language.DeleteTranslationIfUnused("ValueSets", lTranslationID);

			//force list to repopulate
			this.m_oList = null;
		}

		/// <summary>
		/// returns the list item that exists at the specified index in the list
		/// </summary>
		/// <param name="iIndex">0-based index of the specified item</param>
		/// <param name="bytColumn">1-based column index of value to be returned</param>
		/// <param name="iCultureID">id of the culture in which to return the list item value</param>
		/// <returns></returns>
		public string ItemFromIndex(int iIndex, byte bytColumn, int iCultureID)
		{
			ArrayList oList = null;

			Trace.WriteNameValuePairs("iIndex", iIndex, "bytColumn", 
				bytColumn, "iCultureID", iCultureID);

			if(iCultureID == 0)
				iCultureID = Language.mpUSEnglishCultureID;
	
			if(iCultureID == this.Culture)
				//culture requested is current list culture -
				//use module level list
				oList = m_oList;
			
			//get list items if necessary
			if(oList == null)
				this.Refresh(out oList, iCultureID);

			if(oList.Count == 0)
				//list has no items - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_NoListItems") + this.Name);

			//get number of list columns from first list item
			object[] oItem = (object[]) oList[0];

			if(bytColumn > oItem.Length || bytColumn < 1)
				//invalid list column specified - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_InvalidListColumn") + bytColumn);
				
			if(iIndex < 0 || iIndex > oList.Count)
				//requested index is out of range - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

			//if we got here, there is a list item - get it
			oItem = (object[]) oList[iIndex];

			//return value in specified column for that item
			return oItem[bytColumn - 1].ToString();
		}

		/// <summary>
		/// returns the list item that exists at the specified index in the list
		/// </summary>
		/// <param name="iIndex">0-based index of the specified item</param>
		/// <param name="bytColumn">1-based column index of value to be returned</param>
		/// <returns></returns>
		public string ItemFromIndex(int iIndex, byte bytColumn)
		{
			return ItemFromIndex(iIndex, bytColumn, Language.mpUSEnglishCultureID);
		}

		/// <summary>
		/// returns the list item that exists at the specified index in the list
		/// </summary>
		/// <param name="iIndex">0-based index of the specified item</param>
		/// <returns></returns>
		public string ItemFromIndex(int iIndex)
		{
			return ItemFromIndex(iIndex, 1, Language.mpUSEnglishCultureID);
		}

		/// <summary>
		/// returns the value in column iReturnColumn for the first row
		/// that contains the text xSearchValue in column iSearchColumn
		/// </summary>
		/// <param name="xSearchValue">value to search for</param>
		/// <param name="iSearchColumn">column to search in</param>
		/// <param name="iReturnColumn">column of value to return</param>
		/// <param name="iCulture">culture to return the value in</param>
		/// <returns></returns>
		public string Lookup(string xSearchValue, byte iSearchColumn, byte iReturnColumn, int iCultureID)
		{
			ArrayList oList = null;
			string xDefaultLookupValue = "";

			Trace.WriteNameValuePairs("xSearchValue", xSearchValue, "iSearchColumn", 
				iSearchColumn, "iReturnColumn", "iCultureID", iCultureID);

			if(iCultureID == 0)
				iCultureID = Language.mpUSEnglishCultureID;
	
			if(iCultureID == this.Culture)
				//culture requested is current list culture -
				//use module level list
				oList = m_oList;
			
			//get list items if necessary
			if(oList == null)
				this.Refresh(out oList, iCultureID);

			if(oList.Count == 0)
				//list has no items - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_NoListItems") + this.Name);

			//get number of list columns from first list item
			object[] oItem = (object[]) oList[0];

			if(iSearchColumn > oItem.Length || iSearchColumn < 1)
				//invalid list column specified - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_InvalidListColumn") + iSearchColumn);
				
			if(iReturnColumn > oItem.Length || iReturnColumn < 1)
				//invalid list column specified - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_InvalidListColumn") + iReturnColumn);
				
			//cycle through list items
			System.Collections.IEnumerator oEnum = oList.GetEnumerator();

			for(int iCounter = 0; oEnum.MoveNext(); iCounter++)
			{
				oItem =(object[]) oEnum.Current;
				
				string xListValue = ((string)oItem[iSearchColumn - 1]).ToUpper();

				//get index, if value in search column matches search value
				if(xListValue == xSearchValue.ToUpper())
				{
					return (string) oItem[iReturnColumn - 1];
				}
				else if(xListValue == "ELSE")
				{
					xDefaultLookupValue = (string) oItem[iReturnColumn - 1];
				}
			}

			//if we got here, none of the values matched - return default lookup value;
			return xDefaultLookupValue;
		}


		/// <summary>
		/// returns the value in column iReturnColumn for the first row
		/// that contains the text xSearchValue in column 1
		/// </summary>
		/// <param name="xSearchValue">value to search for</param>
		/// <param name="iReturnColumn">column of value to return</param>
		/// <param name="iCulture">culture to return the value in</param>
		/// <returns></returns>
		public string Lookup(string xSearchValue, byte iReturnColumn, int iCulture)
		{
			return Lookup(xSearchValue, 1, iReturnColumn, iCulture);
		}

		/// <summary>
		/// returns the value in column iReturnColumn for the first row
		/// that contains the text xSearchValue in column iSearchColumn
		/// </summary>
		/// <param name="xSearchValue">value to search for</param>
		/// <param name="iSearchColumn">column to search in</param>
		/// <param name="iReturnColumn">column of value to return</param>
		/// <returns></returns>
		public string Lookup(string xSearchValue, byte iSearchColumn, byte iReturnColumn)
		{
			return Lookup(xSearchValue, iSearchColumn, iReturnColumn, Language.mpUSEnglishCultureID);
		}

		/// <summary>
		/// returns the value in column 2 for the first row
		/// that contains the text xSearchValue in column 1
		/// </summary>
		/// <param name="xSearchValue">value to search for</param>
		/// <returns></returns>
		public string Lookup(string xSearchValue)
		{
			return Lookup(xSearchValue,1,2,Language.mpUSEnglishCultureID);
		}

		/// <summary>
		/// returns the value in column 2 for the first row
		/// that contains the text xSearchValue in column 1
		/// </summary>
		/// <param name="xSearchValue">value to search for</param>
		/// <returns></returns>
		public string Lookup(string xSearchValue, int iCulture)
		{
			return Lookup(xSearchValue,1,2,iCulture);
		}

			#endregion

		#region *********************LongIDSimpleDataItem members*********************
		internal override bool IsValid()
		{
			//return true iff required info is present -
			//list is a valid value set list if and only if
			//ValueSetID, ColumnCount and SortColumn are all non-zero, 
			//and all sql fields are blank
			bool bIsValidValueSetList = (this.ValueSetID != 0 && 
				this.ValueSetColumnCount != 0 && this.ValueSetSortColumn != 0) &&
				this.SQL == "";

			//list is a valid sql list if and only if
			//sql statement is specified for either Access or SQL Server,  
			//and all value set fields are blank
			bool bIsValidSQLList = this.SQL != "" &&
				this.ValueSetID == 0 && this.ValueSetColumnCount == 0 && 
				this.ValueSetSortColumn == 0;

			return (this.Name != "") && (bIsValidValueSetList ^ bIsValidSQLList);
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[7];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.ValueSetID;
			oProps[3] = this.ValueSetColumnCount;
			oProps[4] = this.ValueSetSortColumn;
			oProps[5] = this.SQL;
			oProps[6] = this.ConnectionString;

			return oProps;
		}

		internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[7];
			oProps[0] = this.Name;
			oProps[1] = this.ValueSetID;
			oProps[2] = this.ValueSetColumnCount;
			oProps[3] = this.ValueSetSortColumn;
			oProps[4] = this.SQL;
			oProps[5] = this.ConnectionString;
            oProps[6] = Application.GetCurrentEditTime();
			return oProps;
		}
        internal override object[] GetUpdateQueryParameters()
        {
            object[] oProps = new object[8];
            oProps[0] = this.ID;
            oProps[1] = this.Name;
            oProps[2] = this.ValueSetID;
            oProps[3] = this.ValueSetColumnCount;
            oProps[4] = this.ValueSetSortColumn;
            oProps[5] = this.SQL;
            oProps[6] = this.ConnectionString;
            oProps[7] = Application.GetCurrentEditTime();

            return oProps;
        }

        #endregion
	}

	/// <summary>
	/// contains the methods and properties that manage the
	/// collection of MacPac Lists - derived from LongIDSimpleDataCollection -
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Lists: LongIDSimpleDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel = null;
		private bool m_bInternalListsOnly = false;

		#region *********************constructors*********************
		public Lists():base(){}
        public Lists(bool bInternalListsOnly)
            : base()
        {
            this.InternalListsOnly = bInternalListsOnly;
        }
		#endregion

		#region ********************LongIDSimpleDataCollection********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spListsAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spListsCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spListsDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spListsItemFromID";}
		}

		protected override string LastIDSprocName
		{
			get{return "spListsLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spLists";}
		}

		protected override object[] SelectSprocParameters
		{
			get{return new object[] {this.InternalListsOnly};}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return new object[] {this.InternalListsOnly};}
		}

		protected override object[] CountSprocParameters
		{
			get{return new object[] {this.InternalListsOnly};}
		}

		protected override string UpdateSprocName
		{
			get{return "spListsUpdate";}
		}
		#endregion

		#region ************************public members************************

		/// <summary>
		/// creates an editable list - i.e. a value set
		/// </summary>
		/// <returns></returns>
		public LongIDSimpleDataItem CreateEditableList()
		{
			List oList = new List();

			//get last value set id used
			int iLastID = (int) SimpleDataCollection.GetScalar("spValueSetsLastID", null);

			//assign new value set id
			oList.ValueSetID = iLastID++;

			return oList;
		}

		public override LongIDSimpleDataItem Create()
		{
			return new List();
		}

		/// <summary>
		/// returns true iff list with specified name exists
		/// </summary>
		/// <param name="xName">name of list</param>
		/// <returns></returns>
		public bool Exists(string xName)
		{
			//execute query that returns the number of lists 
			//with that name - should be one or zero
			int iNum = (int) GetScalar("spListsExists", new object[] {xName});
			return iNum > 0;
		}

		/// <summary>
		/// returns the List with the specified name as a LongIDSimpleDataItem
		/// </summary>
		/// <param name="xName">name of the list</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromName(string xName)
		{
			DateTime t0 = DateTime.Now;

			Trace.WriteNameValuePairs("xName", xName);

			//change active item only if ID is different than active item
			if((base.m_oItem == null) || (((List) base.m_oItem).Name != xName))
			{
				//save current item if dirty and there are listeners 
				//requesting this - ideally, this should be called
				//after we know that the requested item exists - 
				//unfortunately, we can't save when a DataReader is open
				//so we do it here
				if(base.m_oItem != null && base.m_oItem.IsDirty)
					SaveCurrentItemIfRequested();

                switch (xName)
                {
                    case "SystemFonts":
                        {
                            List oList = new List(-1);
                            oList.Name = xName;
                            oList.ValueSetID = 0;
                            oList.ValueSetColumnCount = mpListColumns.One;
                            oList.ValueSetSortColumn = mpListColumns.One;
                            oList.SQL = "";
                            oList.ConnectionString = "";
                            oList.IsDirty = false;
                            break;
                        }
                    default:
                        {
                            //create new array that contains an extra item for ID
                            object[] oParams = new object[] { this.InternalListsOnly, xName };

                            using (OleDbDataReader oReader = GetResultSet("spListsItemFromName", true, oParams))
                            {

                                try
                                {
                                    if (oReader.HasRows)
                                    {
                                        //get values
                                        oReader.Read();
                                        object[] oValues = new object[oReader.FieldCount];
                                        oReader.GetValues(oValues);

                                        //update object with values
                                        base.m_oItem = UpdateObject(oValues);
                                    }
                                    else
                                    {
                                        //item was not found
                                        throw new LMP.Exceptions.NotInCollectionException(
                                            LMP.Resources.GetLangString(
                                            "Error_ItemWithNameNotInCollection") + xName);
                                    }
                                }
                                finally
                                {
                                    oReader.Close();
                                }
                            }
                            break;
                        }
                }
			}

			LMP.Benchmarks.Print(t0, xName);

			base.m_oItem.IsPersisted = true;
			return base.m_oItem;
		}

		/// <summary>
		/// deletes the list with the specified name
		/// </summary>
		/// <param name="iID"></param>
		public void Delete(string xName)
		{
			//get target list
			List oList = (List) this.ItemFromName(xName);

			if(oList == null)
				//item doesn't exist - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithNameNotInCollection") + xName);

			//delete list
			Delete(oList);
		}

		/// <summary>
		/// deletes the list with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		public override void Delete(int iID)
		{
			//get target list
			List oList = (List) base.ItemFromID(iID);

			if(oList == null)
				//item doesn't exist - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iID);

			//delete list
			Delete(oList);
		}
		#endregion

		#region ************************private members************************
		/// <summary>
		/// sets/returns if the collection should contain only editable lists
		/// </summary>
		internal bool InternalListsOnly
		{
			get{return m_bInternalListsOnly;}
			set
			{
				if(m_bInternalListsOnly != value)
				{
					m_bInternalListsOnly = value;

					//force array to repopulate
					base.m_oArray = null;
				}
			}
		}
		
		/// <summary>
		/// deletes the specified list
		/// </summary>
		/// <param name="oList"></param>
		private void Delete(List oList)
		{
			//get set id before deleting list
			int iSetID = oList.ValueSetID;

			Trace.WriteNameValuePairs("oList.ID", oList.ID, "iSetID", iSetID);

            if (iSetID != 0)
            {
                //list is an internal list - 
                //delete list item translation records - the records in 
                //ValueSets will be deleted in a single query below - 
                //cycle through list items
                foreach (object o in oList.ItemArray.ToArray())
                {
                    //get translation id
                    int iTranslationID = 0;
                    object[] oItem = (object[])o;
                    iTranslationID = Language.GetValueTranslationID(iSetID, oItem[0].ToString());

                    //delete translation with specified id if it's not used elsewhere
                    if (iTranslationID != 0)
                        Language.DeleteTranslationIfUnused("ValueSets", iTranslationID);
                }
            }

			//delete list
			int iRecsAffected = SimpleDataCollection.ExecuteActionSproc(
				"spListsDelete", new object[] {oList.ID});
            
            //log deletion
            SimpleDataCollection.LogDeletions(mpObjectTypes.Lists, oList.ID);
            
			if(iRecsAffected == 0)
				//could not delete record with specified ID - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + oList.ID);

			//delete list items
			SimpleDataCollection.ExecuteActionSproc(
				"spValueSetDelete", new object[] {iSetID});

            //log deletion
            SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, iSetID);

			//force array to repopulate
			base.m_oArray = null;

		}

		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			List oList = new List((int) oValues[0]);
			oList.Name = oValues[1].ToString();
			oList.ValueSetID = (int) oValues[2];
			oList.ValueSetColumnCount = (mpListColumns) oValues[3];
			oList.ValueSetSortColumn = (mpListColumns) oValues[4];
			oList.SQL = oValues[5].ToString();
			oList.ConnectionString = oValues[6].ToString();
			oList.IsDirty = false;

			return oList;
		}
		#endregion
	}
}
