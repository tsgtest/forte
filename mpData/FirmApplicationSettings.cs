using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;

namespace LMP.Data
{
    public class FirmApplicationSettings
    {
        public struct TemplateTrailerOptions
        {
            public string TemplateName;
            public string TrailerIDs;
            public int DefaultTrailerID;
        }
        [Flags]
        public enum mpTrailerIntegrationOptions
        {
            FileOpen = 1,
            FileSave = 2,
            FileSaveAs = 4,
            FileSaveAll = 8,
            FileClose = 16,
            FileCloseAll= 32,
            FilePrint = 64,
            FilePrintDefault = 128,
            FileExit = 256
        }
        private const string PREFNAME_PUBLIC_FOLDER_LABEL = "PublicFolderLabel";
        private const string PREFNAME_MY_FOLDER_LABEL = "MyFolderLabel";
        private const string PREFNAME_SHARED_FOLDER_LABEL = "SharedFolderLabel";
        private const string PREFNAME_EXPAND_SELECTED_CHILD_SEGMENT = "ExpandSelChildSegment";
        private const string PREFNAME_SHOW_SEGMENT_SPECIFIC_RIBBONS = "ShowSegSpecificRibbons";
        private const string PREFNAME_CUSTOM_DATE_FORMATS = "CustomDateFormats";
        private const string PREFNAME_NETWORK_DB_SERVER = "NetDBServer";
        private const string PREFNAME_NETWORK_DB_NAME = "NetDBName";
        private const string PREFNAME_IIS_SERVER_IP_ADDRESS = "IISServerIPAddress";
        private const string PREFNAME_SERVER_APP_NAME = "ServerAppName";
        private const string PREFNAME_DEFAULT_LABEL_SEGMENT = "DefLabelSegment";
        private const string PREFNAME_DEFAULT_ENVELOPE_SEGMENT = "DefEnvelopeSegment";
        //JTS 3/18/10        
        private const string PREFNAME_NETWORK_DISTRIBUTABLE = "NetworkDistributableFolder";
        private const string PREFNAME_SYNC_FREQ_UNIT = "SyncFreqUnit";
        private const string PREFNAME_SYNC_FREQ_INTERVAL = "SyncFreqInterval";
        private const string PREFNAME_SYNC_FREQ_DAYOFWEEK = "SyncFreqDayOfWeek";
        private const string PREFNAME_SYNC_TIME = "SyncTime";
        private const string PREFNAME_DEFAULT_TRAILERS = "Trailers";
        private const string PREFNAME_DEFAULT_DRAFTSTAMPS = "DraftStamps";
        private const string PREFNAME_MAX_PEOPLE = "MaxPeople";
        private const string PREFNAME_PEOPLE_LIST_WARNING_THRESHOLD = "PeopleListWarningThreshold";
        private const string PREFNAME_DEFAULT_TRAILER_ID = "TrailerDefault";
        private const string PREFNAME_DISPLAY_TRAILER_DIALOG = "DisplayTrailerDialog";
        //GLOG 4283
        private const string PREFNAME_SKIP_TRAILER_FOR_TEMP_FILES = "SkipTrailerForTempFiles";
        private const string PREFNAME_DEF_TRAILER_BEHAVIOR = "DefaultTrailerOption";
        private const string PREFNAME_DMS_TYPE_ID = "DMSTypeID";
        private const string PREFNAME_TRAILER_INTEGRATION_OPTIONS = "TrailerIntegrationOptions";
        private const string PREFNAME_PROTECTED_DOCUMENTS_PASSWORD = "ProtectedDocumentsPassword";
        private const string PREFNAME_DMS_FRONTEND_PROFILING = "DMSFrontEndProfiling";
        private const string PREFNAME_DMS_CLOSE_ON_FEP_CANCEL = "DMSCloseOnFEPCancel";
        private const string PREFNAME_CHECKBOX_FONT = "CheckboxFont";
        private const string PREFNAME_CHECKED_CHECKBOX_ASCII = "CheckboxCheckedASCII";
        private const string PREFNAME_UNCHECKED_CHECKBOX_ASCII = "CheckboxUnCheckedASCII";
        private const string PREFNAME_SUPPORTEDLANGUAGES = "SupportedLanguages";
        private const string PREFNAME_ALLOW_NORMAL_STYLE_UPDATES = "AllowNormalStyleUpdates";
        private const string PREFNAME_CLEAR_UNDO_STACK = "ClearUndoStack";
        //glog - 3637 - CEH
        private const string PREFNAME_ALLOW_SHARED_SEGMENTS = "AllowSharedSegments";
        private const string PREFNAME_ALLOW_PRIVATE_PEOPLE = "AllowPrivatePeople";
        private const string PREFNAME_ALLOW_PROXYING = "AllowProxying";
        //GLOG 4488
        private const string PREFNAME_SCHEDULED_SYNC_BEHAVIOR = "ScheduledSyncBehavior";
        //GLOG 5392
        private const string PREFNAME_SYNC_PROMPT_ON_COMPLETION = "SyncPromptOnCompletion";
        
        // GLOG : 3178 : JAB
        // Add user prefs strings for user content limits and warning threshold.
        private const string PREFNAME_USER_CONTENT_LIMIT = "UserContentLimit";
        private const string PREFNAME_USER_CONTENT_WARNING_THRESHOLD = "UserContentWarningThreshold";

        // GLOG : 5358 : JSW
        // Add user prefs strings for saved data limits and warning threshold.
        private const string PREFNAME_SAVED_DATA_LIMIT = "SavedDataLimit";
        private const string PREFNAME_SAVED_DATA_WARNING_THRESHOLD = "SavedDataWarningThreshold";
        
        //GLOG : 3079 : CEH
        private const string PREFNAME_ALLOW_USERS_TO_SET_TRAILER_BEHAVIOR = "DisplayTrailerOptionsUserPrefs";

        //GLOG 6108 (dm)
        private const string PREFNAME_USE_MACPAC9X_STYLE_TRAILER = "UseMacPac9xStyleTrailer";

        //GLOG 6131 (dm)
        private const string PREFNAME_FINISH_DOCUMENTS_UPON_OPEN = "FinishDocumentsUponOpen";

        //GLOG 6192 (dm)
        private const string PREFNAME_PROMPT_BEFORE_REMOVING_LEGACY_TRAILER = "PromptBeforeRemovingLegacyTrailer";

        //GLOG : 8170 : ceh
        private const string PREFNAME_SET_REMOVE_ALL_STAMPS_AS_FIRST_IN_LIST = "SetRemoveAllStampsAsFirstInList";
        
        // GLOG : 3315 : JAB
        // Add firm app setting for help text font and font size.
        private const string PREFNAME_HELP_FONT_SIZE = "HelpFontSize";
        private const string PREFNAME_HELP_FONT_NAME = "HelpFontName";

        // GLOG : 3292 : JAB
        // Add firm app setting for folders help text.
        private const string PREFNAME_PUBLIC_FOLDERS_HELP_TEXT = "PublicFoldersHelpText";
        private const string PREFNAME_MY_FOLDERS_HELP_TEXT = "MyFoldersHelpText";
        private const string PREFNAME_SHARED_FOLDERS_HELP_TEXT = "SharedFoldersHelpText";

        private const string PREFNAME_ALLOW_PAUSE_RESUME = "AllowPause";
        private const string PREFNAME_ALLOW_DOC_TAG_REMOVAL = "AllowDoTagRemoval";
        private const string PREFNAME_ALLOW_SEG_MEMBER_TAG_REMOVAL = "AllowSegMemberTagRemoval";
        private const string PREFNAME_ALLOW_VAR_TAG_REMOVAL = "AllowVarTagRemoval";
        private const string PREFNAME_ALLOW_LEGACY_DOCS = "AllowLegacyDocs";
        private const string PREFNAME_ALLOW_DOC_MEMBER_TAG_REMOVAL = "AllowDocMemberTagRemoval";
        private const string PREFNAME_ALLOW_SAVED_DATA_CREATION_ON_DEAUTOMATION = "AllowSavedDataOnDeautomation";
        private const string PREFNAME_ALERT_USER_ON_FAILED_LOGIN = "AlertFailedLogin";
        private const string PREFNAME_ALERT_UNSTABLE_TAGS_REMOVED = "AlertTagsRemoved";
        private const string PREFNAME_ALERT_WEN_NON_Forte_DOCUMENT_OPENED = "AlertWhenNonMP10DocumentOpened";  //GLOG 6052
        private const string PREFNAME_PROMPT_ABOUT_UNCONVERTEDTABLES_IN_DESIGN = "PromptAboutUnconvertedTablesInDesign";  //GLOG 7873

        private const string PREFNAME_MAIL_HOST = "MailHost";
        private const string PREFNAME_MAIL_USERID = "MailUserID";
        private const string PREFNAME_MAIL_PWD = "MailPwd";
        private const string PREFNAME_MAIL_USE_SSL = "MailUseSsl";
        private const string PREFNAME_MAIL_PORT = "MailPort";
        private const string PREFNAME_MAIL_USE_NETWORK_CREDENTIALS = "MailUseNetCred";
        private const string PREFNAME_MAIL_ADMIN_EMAIL = "MailAdminEMail";
        private const string PREFNAME_BLANK_WORD_DOC_TEMPLATE = "BlankWordDocTemplate";
        private const string PREFNAME_OFFER_MAIL_FOR_ERRORS = "OfferMailForErrors";

        private const string PREFNAME_DEFAULT_USER_FOLDERS = "DefaultUserFolders";
        private const string PREFNAME_USE_BASE64ENCODING = "UseBase64Encoding";

        private const string PREFNAME_DELETE_TEXT_SEGMENT_TAGS = "DeleteTextSegmentTags";

        private const string PREFNAME_LOG_USER_SYNC_SUCCESS = "LogUserSyncSuccess"; //GLOG 5864
        private const string PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_PHONE_PREFIX = "AdvancedAddressBookFormatPhonePrefix";
        private const string PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_FAX_PREFIX = "AdvancedAddressBookFormatFaxPrefix";
        private const string PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_EMAIL_PREFIX = "AdvancedAddressBookFormatEmailPrefix";

        private const string PREFNAME_COUNTRY_ALIASES = "CountryAliases"; //GLOG 8031
        private const string PREFNAME_FINISH_BUTTON_AT_END = "EditorFinishButtonAtEnd"; //GLOG 8511

        private const string PREFNAME_TEMPLATE_TRAILER_DEFAULTS = "TemplateTrailerDefaults"; //GLOG 

        private KeySet m_oFirmAppSettings;
        private User m_oCurUser;
        private int m_iEntityID;
        //private char m_cCustomDateFormatsFieldsKeySetDelimiter = Convert.ToChar(30);
        //private char m_cCustomDateFormatsFieldsDelimiter = '|';
        //private char m_cCustomDateFormatsRecordsDelimiter = '\n';

        /// <summary>
        /// Property to get the prefix for the advanced Address Book format phone
        /// </summary>
        public string AdvancedAddressbookFormatPhonePrefix
        {
            get
            {
                string xPrefix = GetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_PHONE_PREFIX);

                if (xPrefix == null && ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                if (xPrefix == null || xPrefix == "")
                    xPrefix = LMP.Resources.GetLangString("Prefix_AdvancedAddressBookFormatPhone");

                return xPrefix;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_PHONE_PREFIX, value);
            }
        }

        /// <summary>
        /// Property to get the prefix for the advanced Address Book format fax
        /// </summary>
        public string AdvancedAddressbookFormatFaxPrefix
        {
            get
            {
                string xPrefix = GetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_FAX_PREFIX);

                if (xPrefix == null && ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                if (xPrefix == null || xPrefix == "")
                    xPrefix = LMP.Resources.GetLangString("Prefix_AdvancedAddressBookFormatFax");

                return xPrefix;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_FAX_PREFIX, value);
            }
        }

        /// <summary>
        /// Property to get the prefix for the advanced Address Book format email
        /// </summary>
        public string AdvancedAddressbookFormatEmailPrefix
        {
            get
            {
                string xPrefix = GetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_EMAIL_PREFIX);

                if (xPrefix == null && ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                if (xPrefix == null || xPrefix == "")
                    xPrefix = LMP.Resources.GetLangString("Prefix_AdvancedAddressBookFormatEmail");

                return xPrefix;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_ADVANCED_ADDRESSBOOK_FORMAT_EMAIL_PREFIX, value);
            }
        }

        //GLOG : 8031 : ceh
        /// <summary>
        /// Property to get the country aliases
        /// </summary>
        public string CountryAliases
        {
            get
            {
                string xCountryAliases = GetFirmAppSetting(PREFNAME_COUNTRY_ALIASES);

                if (xCountryAliases == null && ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                if (xCountryAliases == null || xCountryAliases == "")
                    xCountryAliases = "";

                return xCountryAliases;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_COUNTRY_ALIASES, value);
            }
        }

        public enum mpScheduledSyncBehavior
        {
            PromptUser = 0,
            AlwaysRun = 1,
            NeverRun = 2
        }

        public FirmApplicationSettings(int iEntityID)
        {
            this.m_iEntityID = iEntityID;
        }

        public string InterrogatoryStyleDefs
        {
            get
            {
                KeySet oKeySet = new KeySet(mpKeySetTypes.UserAppPref, "0", "-999999999", 0);
                return oKeySet.GetValue("RogStyles");
            }
            set
            {
                KeySet oKeySet = new KeySet(mpKeySetTypes.UserAppPref, "0", "-999999999", 0);
                oKeySet.SetValue("RogStyles", value);
            }
        }

        /// <summary>
        /// gets/sets the default document protection password
        /// </summary>
        public string ProtectedDocumentsPassword
        {
            get
            {
                string xPwd = GetFirmAppSetting(PREFNAME_PROTECTED_DOCUMENTS_PASSWORD);
                //GLOG : 6421 : CEH
                //deal with legacy password wich did not contain description field
                if (!xPwd.Contains(LMP.StringArray.mpEndOfSubField.ToString()))
                    xPwd = xPwd + LMP.StringArray.mpEndOfSubField.ToString();
                return xPwd;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_PROTECTED_DOCUMENTS_PASSWORD, value);
            }
        }
        /// <summary>
        /// Property to get the public folder label.
        /// </summary>
        public string PublicFoldersLabel
        {
            get
            {
                string xLabel = GetFirmAppSetting(PREFNAME_PUBLIC_FOLDER_LABEL);
                if (xLabel == "")
                    xLabel = LMP.Resources.GetLangString("Lbl_PublicFolders");

                return xLabel;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_PUBLIC_FOLDER_LABEL, value);
            }
        }
        /// <summary>
        /// Property to get the public folder label.
        /// </summary>
        public LMP.Data.TrailerInsertionOptions DefaultTrailerBehavior
        {
            get
            {
                string xOption = GetFirmAppSetting(PREFNAME_DEF_TRAILER_BEHAVIOR);

                if (xOption == "")
                    return TrailerInsertionOptions.Prompt;
                else
                    return (TrailerInsertionOptions)(int.Parse(xOption));
            }

            set
            {
                SetFirmAppSetting(PREFNAME_DEF_TRAILER_BEHAVIOR,((int)value).ToString());
            }
        }

        bool m_bReturnNullsWhenNoValuePresent = false;

        /// <summary>
        /// Property to set the return value when no value exists for a setting.
        /// </summary>
        public bool ReturnNullsWhenNoValuePresent
        {
            get
            {
                return m_bReturnNullsWhenNoValuePresent;
            }

            set
            {
                m_bReturnNullsWhenNoValuePresent = value;
            }
        }

        /// <summary>
        /// gets/sets the default user folders for the application
        /// </summary>
        public string DefaultUserFolders
        {
            get
            {
                return GetFirmAppSetting(PREFNAME_DEFAULT_USER_FOLDERS);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_DEFAULT_USER_FOLDERS, value);
            }
        }

        /// <summary>
        /// Property to get the label for the My Folders label.
        /// </summary>
        public string MyFoldersLabel
        {
            get
            {
                string xLabel = GetFirmAppSetting(PREFNAME_MY_FOLDER_LABEL);

                if (xLabel == null && ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                if (xLabel == null || xLabel == "")
                    xLabel = LMP.Resources.GetLangString("Lbl_MyFolders");

                return xLabel;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_MY_FOLDER_LABEL, value);
            }
        }

        /// <summary>
        /// Property to get the shared folders label.
        /// </summary>
        public string SharedFoldersLabel
        {
            get
            {
                string xLabel = GetFirmAppSetting(PREFNAME_SHARED_FOLDER_LABEL);
                if (xLabel == "")
                    xLabel = LMP.Resources.GetLangString("Lbl_SharedFolders");

                return xLabel;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SHARED_FOLDER_LABEL, value);
            }
        }

        /// <summary>
        /// gets/sets the checkbox font
        /// </summary>
        public string CheckboxFont
        {
            get
            {
                string xLabel = GetFirmAppSetting(PREFNAME_CHECKBOX_FONT);
                if (xLabel == "")
                    xLabel = "Wingdings";

                return xLabel;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_CHECKBOX_FONT, value);
            }
        }

        /// <summary>
        /// gets/sets the checkbox character
        /// </summary>
        public string CheckedCheckboxASCII
        {
            get
            {
                string xRet = GetFirmAppSetting(PREFNAME_CHECKED_CHECKBOX_ASCII);
                if (xRet == "")
                    xRet = "254";

                return xRet;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_CHECKED_CHECKBOX_ASCII, value);
            }
        }

        /// <summary>
        /// gets/sets the unchcecked checkbox character
        /// </summary>
        public string UnCheckedCheckboxASCII
        {
            get
            {
                string xRet = GetFirmAppSetting(PREFNAME_UNCHECKED_CHECKBOX_ASCII);
                if (xRet == "")
                    xRet = "168";

                return xRet;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_UNCHECKED_CHECKBOX_ASCII, value);
            }
        }

        /// <summary>
        /// gets/sets the uri of the sync server.
        /// </summary>
        public string IISServerIPAddress
        {
            get
            {
                string xURI = GetFirmAppSetting(PREFNAME_IIS_SERVER_IP_ADDRESS);

                if (string.IsNullOrEmpty(xURI))
                {
                    //no value specified as firm setting - get from metadata
                    try
                    {
                        xURI = LMP.Data.Application.GetMetadata("IISServerIPAddress");
                    }
                    catch
                    {
                        xURI = "";
                    }
                }
                
                return xURI;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_IIS_SERVER_IP_ADDRESS, value);
            }
        }

        /// <summary>
        /// gets/sets the app name of the sync server.
        /// </summary>
        public string ServerApplicationName
        {
            get
            {
                string xName = GetFirmAppSetting(PREFNAME_SERVER_APP_NAME);

                if (string.IsNullOrEmpty(xName))
                {
                    //no value specified as firm setting - use "ForteSyncServer"
                    xName = LMP.Data.Application.GetMetadata("ServerApplicationName");
                }

                return xName;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SERVER_APP_NAME, value);
            }
        }

        /// <summary>
        /// gets/sets the app name of the sync server.
        /// </summary>
        public int DefaultLabelSegment
        {
            get
            {
                string xID = GetFirmAppSetting(PREFNAME_DEFAULT_LABEL_SEGMENT);

                if (string.IsNullOrEmpty(xID))
                {
                    return 0;
                }

                return int.Parse(xID);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_DEFAULT_LABEL_SEGMENT, value.ToString());
            }
        }

        /// <summary>
        /// gets/sets the app name of the sync server.
        /// </summary>
        public int DefaultEnvelopeSegment
        {
            get
            {
                string xID = GetFirmAppSetting(PREFNAME_DEFAULT_ENVELOPE_SEGMENT);

                if (string.IsNullOrEmpty(xID))
                {
                    return 0;
                }

                return int.Parse(xID);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_DEFAULT_ENVELOPE_SEGMENT, value.ToString());
            }
        }

        /// <summary>
        /// gets/sets the network database server.
        /// </summary>
        public string NetworkDatabaseServer
        {
            get
            {
                string xServer = GetFirmAppSetting(PREFNAME_NETWORK_DB_SERVER);

                if (string.IsNullOrEmpty(xServer))
                {
                    //no value specified as firm setting - get from metadata
                    try
                    {
                        xServer = LMP.Data.Application.GetMetadata("DefaultServer");
                    }
                    catch
                    {
                        xServer = "";
                    }
                }
                    
                return xServer;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_NETWORK_DB_SERVER, value);
            }
        }

        /// <summary>
        /// gets/sets the name of the network database.
        /// </summary>
        public string NetworkDatabaseName
        {
            get
            {
                string xDB = GetFirmAppSetting(PREFNAME_NETWORK_DB_NAME);

                if (string.IsNullOrEmpty(xDB))
                {
                    //no value specified as firm setting - get from metadata
                    try
                    {
                        xDB = LMP.Data.Application.GetMetadata("DefaultDatabase");
                    }
                    catch
                    {
                        xDB = "";
                    }
                }

                return xDB;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_NETWORK_DB_NAME, value);
            }
        }
        /// <summary>
        /// gets/sets the name of the network database.
        /// </summary>
        public string NetworkDistributableFolder
        {
            get
            {
                return GetFirmAppSetting(PREFNAME_NETWORK_DISTRIBUTABLE);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_NETWORK_DISTRIBUTABLE, value);
            }
        }
        /// <summary>
        /// gets/sets the default trailers for the application
        /// </summary>
        public int[] DefaultTrailerIDs
        {
            get
            {
                string xIDs = GetFirmAppSetting(PREFNAME_DEFAULT_TRAILERS);

                if (xIDs == "")
                    return new int[0];

                string[] aIDs = xIDs.Split(StringArray.mpEndOfSubField);

                //populate integer array with IDs
                int[] aIntIDs = new int[aIDs.Length];

                for(int i=0; i<aIntIDs.Length; i++)
                {
                    aIntIDs[i] = int.Parse(aIDs[i]);
                }

                return aIntIDs;
            }
            set
            {
                StringBuilder oSB = new StringBuilder();

                foreach (int i in value)
                {
                    oSB.AppendFormat("{0}" + StringArray.mpEndOfSubField, i.ToString());
                }

                string xIDs = oSB.ToString();
                xIDs = xIDs.TrimEnd(StringArray.mpEndOfSubField);

                SetFirmAppSetting(PREFNAME_DEFAULT_TRAILERS, xIDs);
            }
        }
        public int DefaultTrailerID
        {
            get
            {
                string xID = GetFirmAppSetting(PREFNAME_DEFAULT_TRAILER_ID);
                if (xID == "" || !String.IsNumericInt32(xID))
                    return 0;
                else
                    return Int32.Parse(xID);
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DEFAULT_TRAILER_ID, value.ToString());
            }
        }
        public bool DisplayTrailerDialog
        {
            get
            {
                bool bShow = true;
                string xShow = GetFirmAppSetting(PREFNAME_DISPLAY_TRAILER_DIALOG);
                if (xShow != "")
                    bShow = bool.Parse(xShow);
                return bShow;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DISPLAY_TRAILER_DIALOG, value.ToString());
            }
        }
        public bool OfferMailForErrors
        {
            get
            {
                bool bOffer = false;
                string xOffer = GetFirmAppSetting(PREFNAME_OFFER_MAIL_FOR_ERRORS);
                if (xOffer != "")
                    bOffer = bool.Parse(xOffer);
                return bOffer;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_OFFER_MAIL_FOR_ERRORS, value.ToString());
            }
        }
        public bool SkipTrailerForTempFiles
        {
            //GLOG 4283
            get
            {
                bool bSkip = false;
                string xSkip = GetFirmAppSetting(PREFNAME_SKIP_TRAILER_FOR_TEMP_FILES);
                if (xSkip != "")
                    bSkip = bool.Parse(xSkip);
                return bSkip;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_SKIP_TRAILER_FOR_TEMP_FILES, value.ToString());
            }
        }
        //GLOG : 3079 : CEH
        public bool AllowUsersToSetTrailerBehavior
        {
            get
            {
                bool bShow = false;
                string xShow = GetFirmAppSetting(PREFNAME_ALLOW_USERS_TO_SET_TRAILER_BEHAVIOR);
                if (xShow != "")
                    bShow = bool.Parse(xShow);
                return bShow;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_USERS_TO_SET_TRAILER_BEHAVIOR, value.ToString());
            }
        }

        //GLOG 6108 (dm)
        public bool UseMacPac9xStyleTrailer
        {
            get
            {
                bool bShow = false;
                string xShow = GetFirmAppSetting(PREFNAME_USE_MACPAC9X_STYLE_TRAILER);
                if (xShow != "")
                    bShow = bool.Parse(xShow);
                return bShow;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_USE_MACPAC9X_STYLE_TRAILER, value.ToString());
            }
        }

        //GLOG 6131 (dm)
        public bool FinishDocumentsUponOpen
        {
            get
            {
                bool bValue = true;
                string xValue = GetFirmAppSetting(PREFNAME_FINISH_DOCUMENTS_UPON_OPEN);
                if (xValue != "")
                    bValue = bool.Parse(xValue);
                return bValue;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_FINISH_DOCUMENTS_UPON_OPEN, value.ToString());
            }
        }

        //GLOG 6192 (dm)
        public bool PromptBeforeRemovingLegacyTrailer
        {
            get
            {
                bool bValue = true;
                string xValue = GetFirmAppSetting(PREFNAME_PROMPT_BEFORE_REMOVING_LEGACY_TRAILER);
                if (xValue != "")
                    bValue = bool.Parse(xValue);
                return bValue;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_PROMPT_BEFORE_REMOVING_LEGACY_TRAILER, value.ToString());
            }
        }

        //GLOG : 8170 : ceh
        //default to true
        public bool SetRemoveAllStampsAsFirstInList
        {
            get
            {
                bool bValue = true;
                string xValue = GetFirmAppSetting(PREFNAME_SET_REMOVE_ALL_STAMPS_AS_FIRST_IN_LIST);
                if (xValue != "")
                    bValue = bool.Parse(xValue);
                return bValue;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_SET_REMOVE_ALL_STAMPS_AS_FIRST_IN_LIST, value.ToString());
            }
        }


        /// <summary>
        /// Get/Set DMS Type in Registry
        /// </summary>
        public int DMSType
        {
            get
            {
                string xDMS = GetFirmAppSetting(PREFNAME_DMS_TYPE_ID);
                if (xDMS == "" || !String.IsNumericInt32(xDMS))
                    return 0;
                else
                    return Int32.Parse(xDMS);
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DMS_TYPE_ID, value.ToString());
            }
        }

        /// <summary>
        /// Get/Set Trailer Integration Options in Registry
        /// </summary>
        public mpTrailerIntegrationOptions TrailerIntegrationOptions
        {
            get
            {
                string xOptions = GetFirmAppSetting(PREFNAME_TRAILER_INTEGRATION_OPTIONS);

                if (xOptions != "" && String.IsNumericInt32(xOptions))
                    return (mpTrailerIntegrationOptions)Int32.Parse(xOptions);
                else
                    return 0;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_TRAILER_INTEGRATION_OPTIONS, ((int)value).ToString());
            }
        }

        /// <summary>
        /// gets/sets the default draft stamps for the application
        /// </summary>
        public int[] DefaultDraftStampIDs
        {
            get
            {
                string xIDs = GetFirmAppSetting(PREFNAME_DEFAULT_DRAFTSTAMPS);

                if (xIDs == "")
                    return new int[0];

                string[] aIDs = xIDs.Split(StringArray.mpEndOfSubField);

                //populate integer array with IDs
                int[] aIntIDs = new int[aIDs.Length];

                for (int i = 0; i < aIntIDs.Length; i++)
                {
                    aIntIDs[i] = int.Parse(aIDs[i]);
                }

                return aIntIDs;
            }
            set
            {
                StringBuilder oSB = new StringBuilder();

                foreach (int i in value)
                {
                    oSB.AppendFormat("{0}" + StringArray.mpEndOfSubField, i.ToString());
                }

                string xIDs = oSB.ToString();
                xIDs = xIDs.TrimEnd(StringArray.mpEndOfSubField);

                SetFirmAppSetting(PREFNAME_DEFAULT_DRAFTSTAMPS, xIDs);
            }
        }
        /// <summary>
        /// gets/sets the unit of frequency for synchronization.
        /// </summary>
        public mpFrequencyUnits SyncFrequencyUnit
        {
            get
            {
                string xUnit = GetFirmAppSetting(PREFNAME_SYNC_FREQ_UNIT);

                if (xUnit == "")
                    return mpFrequencyUnits.Day;
                else
                    return (mpFrequencyUnits)int.Parse(xUnit);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SYNC_FREQ_UNIT, ((int)value).ToString());
            }
        }

        /// <summary>
        /// gets/sets the frequency interval for synchronization.
        /// </summary>
        public int SyncFrequencyInterval
        {
            get
            {
                string xInterval = GetFirmAppSetting(PREFNAME_SYNC_FREQ_INTERVAL);

                if (xInterval == "")
                    return 1;
                else
                    return int.Parse(xInterval);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SYNC_FREQ_INTERVAL, ((int)value).ToString());
            }
        }


        /// <summary>
        /// gets/sets the week day for synchronization.
        /// </summary>
        public mpDayOfWeek SyncFrequencyDayOfWeek
        {
            get
            {
                string xDay = GetFirmAppSetting(PREFNAME_SYNC_FREQ_DAYOFWEEK);

                if (xDay == "")
                    return mpDayOfWeek.Sunday;
                else
                    return (mpDayOfWeek)int.Parse(xDay);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SYNC_FREQ_DAYOFWEEK, ((int)value).ToString());
            }
        }

        /// <summary>
        /// gets/sets the time for synchronization.
        /// </summary>
        public DateTime SyncTime
        {
            get
            {
                string xTime = GetFirmAppSetting(PREFNAME_SYNC_TIME);

                if (xTime == "")
                    return DateTime.Parse("12:00:00 am", LMP.Culture.USEnglishCulture);
                else
                    return DateTime.Parse(xTime, LMP.Culture.USEnglishCulture);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SYNC_TIME, value.ToLongTimeString());
            }
        }

        /// <summary>
        /// Property to determine if the selected child segments should be expanded on selection.
        /// </summary>
        public bool ExpandSelectedChildSegment
        {
            get
            {
                string xExpandChild = GetFirmAppSetting(PREFNAME_EXPAND_SELECTED_CHILD_SEGMENT).ToUpper();
                return (xExpandChild == null) ? false : (xExpandChild == "TRUE");
            }

            set
            {
                SetFirmAppSetting(PREFNAME_EXPAND_SELECTED_CHILD_SEGMENT, value.ToString());
            }
        }
        public bool LogUserSyncSuccess //GLOG 5864
        {
            get
            {
                bool bLog = false;
                string xLog = GetFirmAppSetting(PREFNAME_LOG_USER_SYNC_SUCCESS);
                if (xLog != "")
                    bLog = bool.Parse(xLog);
                return bLog;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_LOG_USER_SYNC_SUCCESS, value.ToString());
            }
        }
        /// <summary>
        /// Property to determine if the segment specific ribbons should be shown.
        /// </summary>
        public bool ShowSegmentSpecificRibbons
        {
            get
            {
                string xShowRibbons = GetFirmAppSetting(PREFNAME_SHOW_SEGMENT_SPECIFIC_RIBBONS).ToUpper();
                return (xShowRibbons == null) ? false : (xShowRibbons == "TRUE");
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SHOW_SEGMENT_SPECIFIC_RIBBONS, value.ToString());
            }
        }

        /// <summary>
        /// Update Firm Application preference for specified Key
        /// </summary>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        private void SetFirmAppSetting(string xKey, string xValue)
        {
            try
            {
                FirmAppSettingKeyset.SetValue(xKey, xValue);
                FirmAppSettingKeyset.Save();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.KeySetException(
                    LMP.Resources.GetLangString("Error_CouldNotSetFirmAppPref") + xKey + ":" + xKey, oE);
            }
        }

        /// <summary>
        /// Get value of specified Firm Application Setting key
        /// </summary>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public string GetFirmAppSetting(string xKey)
        {
            try
            {
                //GLOG 15817: Don't need to call GetDefaultValue separately,
                //since GetValue will do that if there's no match
                string  xValue = FirmAppSettingKeyset.GetValue(xKey);
                return xValue;
            }
            catch
            {
                if (this.ReturnNullsWhenNoValuePresent)
                {
                    return null;
                }

                return "";
            }
        }

        /// <summary>
        /// returns true iff the firm settings are a default
        /// </summary>
        public bool IsDefault
        {
            get { return FirmAppSettingKeyset.IsDefault; } //GLOG 6731
        }

        public mpEntityTypes SettingType
        {
            get { return FirmAppSettingKeyset.EntityType; } //GLOG 6731
        }
        private KeySet FirmAppSettingKeyset
        {
            get
            {
                if (m_oFirmAppSettings == null || m_oCurUser.ID != Application.g_oCurUser.ID)
                {
                    m_oFirmAppSettings = KeySet.GetFirmAppKeys(this.m_iEntityID);

                    //store user for future comparison
                    m_oCurUser = Application.g_oCurUser;
                }

                return m_oFirmAppSettings;
            }
        }

        /// <summary>
        /// deletes this firm app settings record
        /// </summary>
        public void Delete()
        {
            this.m_oFirmAppSettings.Delete();
        }

        /// <summary>
        /// The number of people in the people list that, once reached, will prompt
        /// users to keep the people size as small as possible for performance reasons.
        /// </summary>
        public int PeopleListWarningThreshold
        {
            get
            {
                string xThreshold = GetFirmAppSetting(PREFNAME_PEOPLE_LIST_WARNING_THRESHOLD);
                int iThreshold = string.IsNullOrEmpty(xThreshold) ? 15 : Int32.Parse(xThreshold); //GLOG 7312
                return iThreshold;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_PEOPLE_LIST_WARNING_THRESHOLD, value.ToString());
            }
        }

        /// <summary>
        /// The maximum number of people that can be added to the people list.
        /// </summary>
        public int MaxPeople
        {
            get
            {
                string xMax = GetFirmAppSetting(PREFNAME_MAX_PEOPLE);
                int iMax = string.IsNullOrEmpty(xMax) ? 20 : Int32.Parse(xMax); //GLOG 7312
                return iMax;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_MAX_PEOPLE, value.ToString());
            }
        }

        /// <summary>
        /// GLOG : 3178 : JAB
        /// The user content limit allowed to each user.
        /// </summary>
        public int UserContentLimit
        {
            get
            {
                string xUserContentLimit = GetFirmAppSetting(PREFNAME_USER_CONTENT_LIMIT);
                int iMax = string.IsNullOrEmpty(xUserContentLimit) ? 50 : Int32.Parse(xUserContentLimit); //GLOG 7312
                return iMax;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_USER_CONTENT_LIMIT, value.ToString());
            }
        }
        
        /// <summary>
        /// GLOG : 3178 : JAB
        /// Get/Sets the number of user content items that will trigger warning a
        /// user that they are approaching the number of user content limit.
        /// </summary>
        public int UserContentWarningThreshold
        {
            get
            {
                string xThreshold = GetFirmAppSetting(PREFNAME_USER_CONTENT_WARNING_THRESHOLD);
                int iThreshold = string.IsNullOrEmpty(xThreshold) ? 40 : Int32.Parse(xThreshold);  //GLOG 7312
                return iThreshold;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_USER_CONTENT_WARNING_THRESHOLD, value.ToString());
            }
        }

        /// <summary>
        /// GLOG : 5358 : JSW
        /// The saved data limit allowed to each user.
        /// </summary>
        public int SavedDataLimit
        {
            get
            {
                string xSavedDataLimit = GetFirmAppSetting(PREFNAME_SAVED_DATA_LIMIT);
                int iMax = string.IsNullOrEmpty(xSavedDataLimit) ? 35 : Int32.Parse(xSavedDataLimit); //GLOG 7312
                return iMax;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SAVED_DATA_LIMIT, value.ToString());
            }
        }

        /// <summary>
        /// GLOG : 5358 : JSW
        /// Get/Sets the number of saved data items that will trigger warning a
        /// user that they are approaching the number of saved data limit.
        /// </summary>
        public int SavedDataWarningThreshold
        {
            get
            {
                string xThreshold = GetFirmAppSetting(PREFNAME_SAVED_DATA_WARNING_THRESHOLD);
                int iThreshold = string.IsNullOrEmpty(xThreshold) ? 30 : Int32.Parse(xThreshold); //GLOG 7312
                return iThreshold;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SAVED_DATA_WARNING_THRESHOLD, value.ToString());
            }
        }
        
        /// <summary>
        /// GLOG : 3315 : JAB
        /// Set/get the font name for the help text.
        /// </summary>
        public string HelpFontName
        {
            get
            {
                string xHelpFontName = GetFirmAppSetting(PREFNAME_HELP_FONT_NAME);
                return string.IsNullOrEmpty(xHelpFontName) ? "MS Sans Serif" : xHelpFontName;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_HELP_FONT_NAME, value);
            }
        }

        /// <summary>
        /// GLOG : 3315 : JAB
        /// Set/get the font name for the help text.
        /// </summary>
        public int HelpFontSize
        {
            get
            {
                string xHelpFontSize = GetFirmAppSetting(PREFNAME_HELP_FONT_SIZE);
                int iHelpFontSize = string.IsNullOrEmpty(xHelpFontSize) ? 8 : Int32.Parse(xHelpFontSize);
                return iHelpFontSize;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_HELP_FONT_SIZE, value.ToString());
            }
        }

        // GLOG : 3292 : JAB
        // Save the help text for the folders.
        public string PublicFoldersHelpText
        {
            get
            {
                return GetFirmAppSetting(PREFNAME_PUBLIC_FOLDERS_HELP_TEXT);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_PUBLIC_FOLDERS_HELP_TEXT, value);
            }
        }

        public string MyFoldersHelpText
        {
            get
            {
                return GetFirmAppSetting(PREFNAME_MY_FOLDERS_HELP_TEXT);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_MY_FOLDERS_HELP_TEXT, value);
            }
        }

        public string SharedFoldersHelpText
        {
            get
            {
                return GetFirmAppSetting(PREFNAME_SHARED_FOLDERS_HELP_TEXT);
            }

            set
            {
                SetFirmAppSetting(PREFNAME_SHARED_FOLDERS_HELP_TEXT, value);
            }
        }


        public bool DMSFrontEndProfiling
        {
            get
            {
                bool bUse = false;
                string xSetting = GetFirmAppSetting(PREFNAME_DMS_FRONTEND_PROFILING);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DMS_FRONTEND_PROFILING, value.ToString());
            }
        }
        //GLOG 5316: Setting controls if new Document will be closed after FEP Cancel
        public bool DMSCloseOnFEPCancel
        {
            get
            {
                bool bUse = false;
                string xSetting = GetFirmAppSetting(PREFNAME_DMS_CLOSE_ON_FEP_CANCEL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DMS_CLOSE_ON_FEP_CANCEL, value.ToString());
            }
        }
        public bool AllowLegacyDocumentFunctionality
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_LEGACY_DOCS);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_LEGACY_DOCS, value.ToString());
            }
        }

        public bool AllowPauseResume
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_PAUSE_RESUME);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_PAUSE_RESUME, value.ToString());
            }
        }
        public bool AllowDocumentTagRemoval
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_DOC_TAG_REMOVAL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_DOC_TAG_REMOVAL, value.ToString());
            }
        }
        public bool AllowSegmentMemberTagRemoval
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_SEG_MEMBER_TAG_REMOVAL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_SEG_MEMBER_TAG_REMOVAL, value.ToString());
            }
        }
        public bool AllowDocumentMemberTagRemoval
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_DOC_MEMBER_TAG_REMOVAL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_DOC_MEMBER_TAG_REMOVAL, value.ToString());
            }
        }
        public bool AllowVariableBlockTagRemoval
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_VAR_TAG_REMOVAL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_VAR_TAG_REMOVAL, value.ToString());
            }
        }
        public bool AllowSavedDataCreationOnDeautomation
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_SAVED_DATA_CREATION_ON_DEAUTOMATION);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_SAVED_DATA_CREATION_ON_DEAUTOMATION, value.ToString());
            }
        }
        public bool DeleteTextSegmentTagsOnFinish
        {
            get
            {
                bool bDelete = true;
                string xSetting = GetFirmAppSetting(PREFNAME_DELETE_TEXT_SEGMENT_TAGS);
                if (!string.IsNullOrEmpty(xSetting))
                    bDelete = bool.Parse(xSetting);
                return bDelete;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_DELETE_TEXT_SEGMENT_TAGS, value.ToString());
            }
        }
        //glog - 3637 - CEH
        public bool AllowSharedSegments
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_SHARED_SEGMENTS);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse && !LMP.MacPac.MacPacImplementation.IsFullLocal; //GLOG 8220: Not available in Local mode
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_SHARED_SEGMENTS, value.ToString());
            }
        }

        public bool AllowNormalStyleUpdates
        {
            get
            {
                bool bUse = false;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_NORMAL_STYLE_UPDATES);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_NORMAL_STYLE_UPDATES, value.ToString());
            }
        }

        public bool ClearUndoStack
        {
            get
            {
                bool bUse = false;
                string xSetting = GetFirmAppSetting(PREFNAME_CLEAR_UNDO_STACK);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_CLEAR_UNDO_STACK, value.ToString());
            }
        }

        public bool AllowPrivatePeople
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_PRIVATE_PEOPLE);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_PRIVATE_PEOPLE, value.ToString());
            }
        }

        public bool AllowProxying
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALLOW_PROXYING);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse && !LMP.MacPac.MacPacImplementation.IsFullLocal; //GLOG 8220: Not available in Local mode
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALLOW_PROXYING, value.ToString());
            }
        }

        public bool AlertUnusableTagsRemoved
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_ALERT_UNSTABLE_TAGS_REMOVED);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                else
                    bUse = true;

                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALERT_UNSTABLE_TAGS_REMOVED, value.ToString());
            }
        }

        public bool AlertWhenNonForteDocumentOpened
        {
            get
            {
                bool bAlert = false;
                string xSetting = GetFirmAppSetting(PREFNAME_ALERT_WEN_NON_Forte_DOCUMENT_OPENED);
                if (!string.IsNullOrEmpty(xSetting))
                    bAlert = bool.Parse(xSetting);
                return bAlert;

            }
            set
            {
                SetFirmAppSetting(PREFNAME_ALERT_WEN_NON_Forte_DOCUMENT_OPENED, value.ToString());
            }
        }
		
		//GLOG : 7873 : ceh
        public bool PromptAboutUnconvertedTablesInDesign
        {
            get
            {
                bool bAlert = false;
                string xSetting = GetFirmAppSetting(PREFNAME_PROMPT_ABOUT_UNCONVERTEDTABLES_IN_DESIGN);
                if (!string.IsNullOrEmpty(xSetting))
                    bAlert = bool.Parse(xSetting);
                return bAlert;

            }
            set
            {
                SetFirmAppSetting(PREFNAME_PROMPT_ABOUT_UNCONVERTEDTABLES_IN_DESIGN, value.ToString());
            }
        }

        /// <summary>
        /// gets/sets the supported languages string
        /// </summary>
        public string SupportedLanguages
        {
            get { return GetFirmAppSetting(PREFNAME_SUPPORTEDLANGUAGES); }
            set { SetFirmAppSetting(PREFNAME_SUPPORTEDLANGUAGES, value); }
        }
        public string MailHost
        {
            get { return GetFirmAppSetting(PREFNAME_MAIL_HOST); }
            set { SetFirmAppSetting(PREFNAME_MAIL_HOST, value); }
        }
        public string MailUserID
        {
            get { return GetFirmAppSetting(PREFNAME_MAIL_USERID); }
            set { SetFirmAppSetting(PREFNAME_MAIL_USERID, value); }
        }
        public string MailPassword
        {
            get { return GetFirmAppSetting(PREFNAME_MAIL_PWD); }
            set { SetFirmAppSetting(PREFNAME_MAIL_PWD, value); }
        }
        public string MailAdminEMail
        {
            get { return GetFirmAppSetting(PREFNAME_MAIL_ADMIN_EMAIL); }
            set { SetFirmAppSetting(PREFNAME_MAIL_ADMIN_EMAIL, value); }
        }
        public string BlankWordDocTemplate
        {
            get
            {
                string xName = GetFirmAppSetting(PREFNAME_BLANK_WORD_DOC_TEMPLATE);

                if (string.IsNullOrEmpty(xName))
                {
                    xName = "Normal.dotm";
                }

                return xName;
            }
            set { SetFirmAppSetting(PREFNAME_BLANK_WORD_DOC_TEMPLATE, value); }
        }
        public bool MailUseSsl
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_MAIL_USE_SSL);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_MAIL_USE_SSL, value.ToString());
            }
        }
        public int MailPort
        {
            get
            {
                int iPort = 587;
                string xSetting = GetFirmAppSetting(PREFNAME_MAIL_PORT);
                if (!string.IsNullOrEmpty(xSetting))
                    iPort = int.Parse(xSetting);
                return iPort;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_MAIL_PORT, value.ToString());
            }
        }
        public bool MailUseNetworkCredentialsToLogin
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_MAIL_USE_NETWORK_CREDENTIALS);
                if (!string.IsNullOrEmpty(xSetting))
                    bUse = bool.Parse(xSetting);
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_MAIL_USE_NETWORK_CREDENTIALS, value.ToString());
            }
        }
        //GLOG 4488: Action to take if scheduled sync occurs when Word is running
        public mpScheduledSyncBehavior ScheduledSyncBehavior
        {
            get
            {
                string xAction = GetFirmAppSetting(PREFNAME_SCHEDULED_SYNC_BEHAVIOR);

                if (xAction != "" && String.IsNumericInt32(xAction))
                    return (mpScheduledSyncBehavior)Int32.Parse(xAction);
                else
                    return 0;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_SCHEDULED_SYNC_BEHAVIOR, ((int)value).ToString());
            }
        }
        //GLOG 5392: Option to display message when scheduled sync completes.
        public bool SyncPromptOnCompletion
        {
            get
            {
                bool bUse = true;
                string xSetting = GetFirmAppSetting(PREFNAME_SYNC_PROMPT_ON_COMPLETION);
                if (!string.IsNullOrEmpty(xSetting))
                    //GLOG 5462:  Make sure invalid string doesn't throw error
                    try
                    {
                        bUse = bool.Parse(xSetting);
                    }
                    catch { }
                return bUse;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_SYNC_PROMPT_ON_COMPLETION, value.ToString());
            }
        }

        /// <summary>
        /// gets/sets whether to use Base64 encoding
        /// </summary>
        public bool UseBase64Encoding
        {
            get
            {
                bool bUse = false;
                string xSetting = GetFirmAppSetting(PREFNAME_USE_BASE64ENCODING);
                if (!string.IsNullOrEmpty(xSetting))
                    //GLOG 5462:  Make sure invalid string doesn't throw error
                    try
                    {
                        bUse = bool.Parse(xSetting);
                    }
                    catch { }
                return bUse;
            }

            set
            {
                SetFirmAppSetting(PREFNAME_USE_BASE64ENCODING, value.ToString());
            }
        }
        public bool FinishButtonAtEndOfEditor
        {
            //GLOG 8511
            get
            {
                bool bVal = false;
                string xSetting = GetFirmAppSetting(PREFNAME_FINISH_BUTTON_AT_END);
                if (!string.IsNullOrEmpty(xSetting))
                    //GLOG 5462:  Make sure invalid string doesn't throw error
                    try
                    {
                        bVal = bool.Parse(xSetting);
                    }
                    catch { }
                return bVal;
            }
            set
            {
                SetFirmAppSetting(PREFNAME_FINISH_BUTTON_AT_END, value.ToString());
            }
        }
        public List<TemplateTrailerOptions> TemplateTrailerDefaults
        {
            get
            {
                string xSetting = GetFirmAppSetting(PREFNAME_TEMPLATE_TRAILER_DEFAULTS);
                //Use delimiters that are not valid in file names - ':' and '>'
                string[] aTemplates = xSetting.Split(':');
                Array.Sort(aTemplates);
                List<TemplateTrailerOptions> oTemplateArray = new List<TemplateTrailerOptions>();
                if (aTemplates[0] != "")
                {
                    for (int i = 0; i < aTemplates.GetLength(0); i++)
                    {
                        string[] aOptions = aTemplates[i].Split('>');
                        TemplateTrailerOptions oOptions = new TemplateTrailerOptions();
                        oOptions.TemplateName = aOptions[0];
                        if (aOptions.GetUpperBound(0) > 0)
                            oOptions.TrailerIDs = aOptions[1];
                        else
                            oOptions.TrailerIDs = "";
                        if (aOptions.GetUpperBound(0) > 1)
                            oOptions.DefaultTrailerID = Int32.Parse(aOptions[2]);
                        else
                            oOptions.DefaultTrailerID = 0;
                        oTemplateArray.Add(oOptions);
                    }
                }
                return oTemplateArray;
            }
            set
            {
                string xValue = "";
                for (int i = 0; i < value.Count; i++)
                {
                    xValue += value[i].TemplateName + ">";
                    xValue += value[i].TrailerIDs + ">";
                    xValue += value[i].DefaultTrailerID.ToString();
                    if (i < value.Count - 1)
                        xValue += ":";
                }
                SetFirmAppSetting(PREFNAME_TEMPLATE_TRAILER_DEFAULTS, xValue);
            }
        }

    }
}
