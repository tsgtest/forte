using System;

namespace LMP.Data
{
	/// <summary>
	/// contains the properties and methods of a MacPac TranslatableDataItem,
	/// which is a LongIDSimpleDataItem with translation support
	/// </summary>
	abstract public class TranslatableDataItem: LongIDSimpleDataItem
	{
		private int m_iTranslationID;

		#region *********************constructors*********************
		public TranslatableDataItem(){}
		public TranslatableDataItem(int ID): base(ID){}
		#endregion

		#region *********************properties*********************
		/// <summary>
		/// gets the name of the database table used by this class
		/// </summary>
		internal abstract string TableName{get;}
		/// <summary>
		/// gets the name of the ID field of the table specified in TableName
		/// </summary>
		internal abstract string IDFieldName{get;}
		/// <summary>
		/// gets the number of translation values required by this class
		/// </summary>
		internal abstract byte ValuesCount{get;}

		/// <summary>
		/// returns id of record in Translations table if applicable
		/// </summary>
		public int TranslationID
		{
			get{return m_iTranslationID;}
			set{this.SetIntPropertyValue(ref m_iTranslationID, value);}
				
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// returns array containing all translations of this item's translatable fields
		/// </summary>
		/// <returns></returns>
		public object[] GetTranslations()
		{
			return Language.GetCustomTranslations(this.TranslationID);
		}

		/// <summary>
		/// returns array containing translation of this item's translatable fields for
		/// the specified culture
		/// </summary>
		/// <param name="iCulture"></param>
		/// <returns></returns>
		public object[] GetTranslation(int iCulture)
		{
			return Language.GetCustomTranslation(this.TranslationID, iCulture);
		}

		/// <summary>
		/// sets a single-value translation for the specified culture
		/// </summary>
		/// <param name="iCulture"></param>
		/// <param name="oValue1"></param>
		public void SetTranslation(int iCulture, object oValue1)
		{
			if(this.ValuesCount != 1)
			{
				//alert that this overload cannot be called for this object
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_InvalidSetTranslationOverload") + this.GetType().ToString());
			}

			if (this.TranslationID==0)
				//no translation records yet - create and write ID to this item's record
				this.TranslationID = Language.SetCustomTranslation(this.TableName, this.IDFieldName,
					this.ID, iCulture, oValue1);
			else
				//create/modify translation record for specified culture
				Language.SetCustomTranslation(this.TranslationID, iCulture,
					oValue1);
		}	

		/// <summary>
		/// sets a two-value translation for the specified culture
		/// </summary>
		/// <param name="iCulture"></param>
		/// <param name="oValue1"></param>
		/// <param name="oValue2"></param>
		public void SetTranslation(int iCulture, object oValue1, object oValue2)
		{
			if(this.ValuesCount != 2)
			{
				//alert that this overload cannot be called for this object
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_InvalidSetTranslationOverload") + this.GetType().ToString());
			}
			if (this.TranslationID==0)
				//no translation records yet - create and write ID to this item's record
				Language.SetCustomTranslation(this.TableName, this.IDFieldName,
					this.ID, iCulture, oValue1, oValue2);
			else
				//create/modify translation record for specified culture
				Language.SetCustomTranslation(this.TranslationID, iCulture,
					oValue1, oValue2);
		}	
		/// <summary>
		/// sets a three-value translation for the specified culture
		/// </summary>
		/// <param name="iCulture"></param>
		/// <param name="oValue1"></param>
		/// <param name="oValue2"></param>
		/// <param name="oValue3"></param>
		public void SetTranslation(int iCulture, object oValue1, 
			object oValue2, object oValue3)
		{
			if(this.ValuesCount != 3)
			{
				//alert that this overload cannot be called for this object
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_InvalidSetTranslationOverload") + this.GetType().ToString());
			}
			if (this.TranslationID==0)
				//no translation records yet - create and write ID to this item's record
				Language.SetCustomTranslation(this.TableName, this.IDFieldName,
					this.ID, iCulture, oValue1, oValue2, oValue3);
			else
				//create/modify translation record for specified culture
				Language.SetCustomTranslation(this.TranslationID, iCulture,
					oValue1, oValue2, oValue3);
		}	

		/// <summary>
		/// sets a four-value translation for the specified culture
		/// </summary>
		/// <param name="iCulture"></param>
		/// <param name="oValue1"></param>
		/// <param name="oValue2"></param>
		/// <param name="oValue3"></param>
		/// <param name="oValue4"></param>
		public void SetTranslation(int iCulture, object oValue1, 
			object oValue2, object oValue3, object oValue4)
		{
			if(this.ValuesCount != 4)
			{
				//alert that this overload cannot be called for this object
				throw new LMP.Exceptions.MethodCallException(
					LMP.Resources.GetLangString("Error_InvalidSetTranslationOverload") + this.GetType().ToString());
			}
			if (this.TranslationID==0)
				//no translation records yet - create and write ID to this item's record
				Language.SetCustomTranslation(this.TableName, this.IDFieldName,
					this.ID, iCulture, oValue1, oValue2, oValue3, oValue4);
			else
				//create/modify translation record for specified culture
				Language.SetCustomTranslation(this.TranslationID, iCulture,
					oValue1, oValue2, oValue3, oValue4);
		}	
		#endregion
	}

	abstract public class TranslatableDataCollection: LongIDSimpleDataCollection
	{
		public override void Delete(int iID)
		{
			TranslatableDataItem oItem = null;

			try
			{
				//get the item
				oItem = (TranslatableDataItem) this.ItemFromID(iID);
			}

			catch
			{
				throw new LMP.Exceptions.TypeMismatchException(
					LMP.Resources.GetLangString("Error_CouldNotCastToTranslatableDataItem"));

			}

			//get the translation ID
			int iTranslationID = oItem.TranslationID;

			//delete item
			base.Delete(iID);

			//add deletion to log
			SimpleDataCollection.LogDeletions(mpObjectTypes.Translations, iID);

			if(iTranslationID != 0)
			{
				//if we got here, the item was deleted - 
				//delete the translation record with the appropriate ID
				//if the translation record is not used elsewhere
				Language.DeleteTranslationIfUnused(oItem.TableName, iTranslationID);
			}
		}
	}

}
