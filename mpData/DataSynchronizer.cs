using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Timers;
using LMP.Data;
using System.Diagnostics;
using XML = System.Xml;
using System.Reflection;
using Diag = System.Diagnostics;
//using SMO = Microsoft.SqlServer.Management.Smo;
//using SMOCommon = Microsoft.SqlServer.Management.Common;

namespace LMP.MacPac.Sync.Server
{
    public class DataSynchronizer : MarshalByRefObject, LMP.MacPac.Sync.ISyncServer
    {
        #region **************************constants**************************
        private const string mpUserID = "mp10User";
        private const string mpUserPassword = "fish4wade";
        private const string mpSuperUserID = "mp10SuperUser";
        private const string mpSuperUserPassword = "superfish4wade";
        private const string mpEventLogSource = "MP10Server";
        private const string mpMetadataLastPublishedTime = "LastPublishedTime";
        private const string mpMetadataBlockServers = "BlockedServers";
        private const string mpMetadataPublishInProgress = "PublishInProgress";
        private const string mpSyncRegistryKey = @"SOFTWARE\The Sackett Group\Deca\Sync";
        private const string mpEventLogApplication = "MacPac 10 Synchronizer";
        private const string mpMetadataLastStructureUpdate = "DBStructureUpdateIDs";
        #endregion
        #region **************************fields**************************
        //JTS 1/16/09: PeopleGroups needs to be included in People-related list, 
        //since GroupAssignments has a related field - new Groups may have been
        //added since Distributable DB was created
        private static string[] m_aPeopleRelatedTables = { "Deletions", "People", "FavoritePeople", "PeopleGroups", "GroupAssignments", "Proxies", "Permissions", "KeySets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };
        //JTS 1/16/09: On-Demand tables should include AttyLicenses
        private static string[] m_aOnDemandTables = { "Deletions", "People", "FavoritePeople", "Proxies", "AttyLicenses", "Permissions", "KeySets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };
        private static string[] m_aCachableUserSyncTables = { "Assignments", "AddressFormats", "Addresses", "Offices", "PeopleGroups", "Countries", "States", "Counties", "Couriers", "Courts", "ExternalConnections", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "Lists", "Translations", "ValueSets", "Folders", "Segments" };
        private static string[] m_aNonCachableUserSyncTables = { "Deletions", "People", "FavoritePeople", "Proxies", "AttyLicenses", "GroupAssignments", "Permissions", "KeySets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };
        private static string[] m_aAdminSyncTables = { "Deletions", "Assignments", "AddressFormats", "Addresses", "Offices", "People", "Proxies", "AttyLicenses", "Permissions", "PeopleGroups", "GroupAssignments", "Countries", "States", "Counties", "Couriers", "Courts", "ExternalConnections", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "KeySets", "Lists", "Translations", "ValueSets", "VariableSets", "UserSegments" };
        private static DataSet m_oUserSyncDownCachedData;
        private static string m_xCachedServer = "";
        private static string m_xCachedDatabase = "";
        private static int m_iCommandTimeout = 60;
        private static int m_iConnectionTimeout = 60;
        private static DateTime m_dLastCachedSync;
        private static Timer m_oTimer = null;
        private static int m_iRefreshInterval;
        private static int m_iBenchmarkMode = 0;
        private static bool m_bConstructorRun = false;
        private static long m_dtInitialStart = 0;
        private static int m_iLogLevel = 0;
        private static string m_xLastStructureUpdateIDs = "";
        #endregion
        #region**************************constructors**************************
        static DataSynchronizer()
        {
            ResetStartTime();
            DateTime t0 = DateTime.Now;

            WriteNameValuePairs("AppDomain", AppDomain.CurrentDomain.FriendlyName);
            //make sure static constructor is not running twice
            if (m_bConstructorRun)
                return;
            m_bConstructorRun = true;
            //get main server and db that we're synching with
            try
            {
                m_xCachedDatabase = LMP.Registry.GetLocalMachineValue(
                    mpSyncRegistryKey, "DefaultDatabase");
                WriteNameValuePairs("m_xCachedDatabase", m_xCachedDatabase);

                m_xCachedServer = LMP.Registry.GetLocalMachineValue(
                    mpSyncRegistryKey, "DefaultServer");
                WriteNameValuePairs("m_xCachedServer", m_xCachedServer);

                string xCommandTimeout = LMP.Registry.GetLocalMachineValue(
                    mpSyncRegistryKey, "CommandTimeout");
                string xConnectionTimeout = LMP.Registry.GetLocalMachineValue(
                    mpSyncRegistryKey, "ConnectionTimeout");

                if (!string.IsNullOrEmpty(xCommandTimeout))
                    m_iCommandTimeout = int.Parse(xCommandTimeout);
                WriteNameValuePairs("m_iCommandTimeout", m_iCommandTimeout);

                if (!string.IsNullOrEmpty(xConnectionTimeout))
                    m_iConnectionTimeout = int.Parse(xConnectionTimeout);
                WriteNameValuePairs("m_iConnectionTimeout", m_iConnectionTimeout);
            }
            catch { }

            if (string.IsNullOrEmpty(m_xCachedDatabase) || string.IsNullOrEmpty(m_xCachedServer))
            {
                WriteError(new LMP.Exceptions.ServerException(
                    "Invalid or missing default server/database registry keys."));
                return;
            }
            try
            {
                //get interval for checking if cache needs refreshing
                m_iRefreshInterval = Int32.Parse(LMP.Registry.GetLocalMachineValue(mpSyncRegistryKey, "CacheRefreshInterval"));
            }
            catch
            {
                //default if no registry setting
                m_iRefreshInterval = 60000;
            }
            try
            {
                //get Benchmark Mode: >0=Log Bechmark times
                m_iBenchmarkMode = Int32.Parse(LMP.Registry.GetLocalMachineValue(mpSyncRegistryKey, "BenchmarkMode"));
            }
            catch
            {
                m_iBenchmarkMode = 0;
            }
            try
            {
                //get Log Level: 0=Errors only; >0=All messages
                m_iLogLevel = Int32.Parse(LMP.Registry.GetLocalMachineValue(mpSyncRegistryKey, "LogLevel"));
            } 
            catch
            {
                m_iLogLevel = 0;
            }
            m_dLastCachedSync = MinSyncCompareDate();
            m_oTimer = new Timer(m_iRefreshInterval);
            m_oTimer.Elapsed += new ElapsedEventHandler(oTimer_Elapsed);
            RefreshCacheIfNecessary();
            WriteBenchmark(t0);
        }
        #endregion
        #region**************************public methods**************************
        /// <summary>
        /// returns true iff sync is available for the IIS at the specified address
        /// </summary>
        /// <param name="xServer">the database server containing the database where this information is stored</param>
        /// <param name="xDatabase">the database where this information is stored</param>
        /// <param name="xIISAddress"></param>
        /// <returns></returns>
        public bool SyncAvailable(string xServer, string xDatabase, string xIISAddress)
        {
            try
            {
                string xBlockedServers = GetMetaDataValue(xServer, xDatabase, mpMetadataBlockServers);

                switch (xBlockedServers)
                {
                    case "0":
                        return true;
                    case "1":
                    case "All":
                    case "ALL":
                    case "all":
                        return false;
                    default:
                        return !xBlockedServers.Contains(xIISAddress);
                }
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                //GLOG 4462: return false if an error has occurred
                //This could be due to mp10user password being incorrectly changed.
                //If key doesn't exist in Metadata table, GetMetaDataValue would 
                //just return an empty string, without an error being thrown.
                return false;
            }
        }
        /// <summary>
        /// updates the server database with data supplied
        /// </summary>
        /// <param name="iUserID"></param>
        /// <param name="oSourceData"></param>
        public void PublishUserSyncData(int iUserID, DataSet oSourceData, string xServer, string xDatabase)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                //Remote service automatically adjusts dates based on client timezone
                //We need to undo the adjustment so dates reflect UTC again
                AdjustDataSetTimeZone(oSourceData);

                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
                {
                    //create reusable command object - for use below
                    using (SqlCommand oCmd = new SqlCommand())
                    {

                        //connect to target source
                        oCmd.Connection = oCnn;

                        if ((oCnn.State & ConnectionState.Open) == 0)
                        {
                            try
                            {
                                //open connection
                                oCnn.Open();
                            }
                            catch (System.Exception oE)
                            {
                                WriteError(oE);
                                throw oE;
                            }
                        }

                        oCmd.CommandType = CommandType.StoredProcedure;

                        //cycle through data tables, updating the target for each
                        foreach (DataTable oDT in oSourceData.Tables)
                        {

                            DateTime t1 = DateTime.Now;
                            //clear target dump table
                            try
                            {
                                oCmd.CommandText = "spSyncUpClear" + oDT.TableName + "Tmp";
                                oCmd.ExecuteNonQuery();
                            }
                            catch (System.Exception oE)
                            {
                                WriteError(oE);
                                throw oE;
                            }

                            //populate target dump table with data in data table
                            SqlDataAdapter oA = null;

                            try
                            {
                                using (oA = new SqlDataAdapter())
                                {
                                    SqlCommandBuilder oCB = new SqlCommandBuilder(oA);

                                    //create select command so that command builder
                                    //can generate the insert command
                                    oA.SelectCommand = new SqlCommand(
                                        "SELECT * FROM Sync" + oDT.TableName + "Tmp", oCmd.Connection);
                                    oA.SelectCommand.CommandTimeout = 0;
                                    oA.InsertCommand = oCB.GetInsertCommand();
                                    oA.InsertCommand.CommandTimeout = 0;
                                    oA.UpdateCommand = oCB.GetUpdateCommand();
                                    oA.UpdateCommand.CommandTimeout = 0;

                                    oA.Update(oDT);
                                }
                            }
                            catch (System.Exception oE)
                            {
                                WriteError(oE);
                                throw oE;
                            }

                            try
                            {
                                //transfer data from dump table to target table
                                if (oDT.TableName.ToUpper() != "DELETIONS")
                                {
                                    //JTS 1/05/09: Deletions can be skipped since
                                    //records should never get updated once created
                                    oCmd.CommandText = "spSyncUp" + oDT.TableName + "Update";
                                    oCmd.CommandTimeout = 0;
                                    oCmd.ExecuteNonQuery();
                                }
                                oCmd.CommandText = "spSyncUp" + oDT.TableName + "Add";
                                oCmd.CommandTimeout = 0;
                                oCmd.ExecuteNonQuery();

                                WriteNameValuePairs("oDT.TableName", oDT.TableName);

                                if (oDT.TableName.ToUpper() == "DELETIONS")
                                {
                                    //deleted obsolete records
                                    SyncDeletions(oCnn, false);
                                }
                            }
                            catch (System.Exception oE)
                            {
                                WriteError(oE);
                                throw oE;
                            }
                            finally
                            {
                                //clear out temp table
                                oCmd.CommandText = "spSyncUpClear" + oDT.TableName + "Tmp";
                                oCmd.ExecuteNonQuery();
                                WriteBenchmark(t1, oDT.TableName);
                            }
                        }
                    }
                }

                WriteBenchmark(t0, "UserID=" + iUserID.ToString());
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }

        }
        /// <summary>
        /// returns the set of rows for the specified user that were added/edited/deleted 
        /// in the server db since the last synchronization down
        /// </summary>
        /// <param name="iUserID"></param>
        /// <param name="datLastSyncDown"></param>
        /// <returns></returns>
        public DataSet GetUserSyncPeopleData(int iUserID, string xLastSyncDown, string xServer, string xDatabase)
        {
            DataSet oDS = null;

            try
            {
                DateTime t0 = DateTime.Now;
                DateTime datLastSyncDown = DateTime.Parse(xLastSyncDown, LMP.Culture.USEnglishCulture);
                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
                {
                    oDS = new DataSet("MacPacServerDataSet");

                    //cycle through all tables marked for synchronization
                    for (int i = 0; i < m_aPeopleRelatedTables.Length; i++)
                    {
                        //create a datatable to add to dataset
                        DataTable oDT = oDS.Tables.Add();
                        oDT.TableName = m_aPeopleRelatedTables[i];

                        try
                        {
                            //get all rows edited since the last edit time - create command
                            using (SqlCommand oCmd = new SqlCommand())
                            {
                                oCmd.CommandText = "spUserSyncDownGet" + m_aPeopleRelatedTables[i];
                                oCmd.CommandType = CommandType.StoredProcedure;
                                oCmd.Connection = oCnn; ;


                                //add parameters
                                SqlParameter oParam = new SqlParameter("@LastSyncDown", datLastSyncDown);
                                oCmd.Parameters.Add(oParam);

                                oParam = new SqlParameter("@UserID", iUserID);
                                oCmd.Parameters.Add(oParam);

                                using (SqlDataAdapter oA = new SqlDataAdapter(oCmd))
                                {
                                    //mark all rows as "Added" - this will
                                    //force the subsequent DataSet update
                                    //to add all rows to target data source
                                    oA.AcceptChangesDuringFill = false;

                                    //WriteNameValuePairs("oDT.TableName", oDT.TableName);

                                    DateTime t1 = DateTime.Now;
                                    oA.Fill(oDT);
                                    WriteBenchmark(t1, "Fill " + oDT.TableName + " (" + oDT.Rows.Count + ")");
                                }
                                if (oDT.Rows.Count > 0)
                                {
                                    //JTS 12/08/08: Date Column must be in UTC format
                                    //so that it will remain unchanged during serialization.
                                    //DateTimeMode cannot be changed in populated DataSet,
                                    //so must recreate the LastEditTime column instead
                                    DataColumn oCol = new DataColumn("LastEditTimeNew");
                                    oCol.DataType = System.Type.GetType("System.DateTime");
                                    oCol.DateTimeMode = DataSetDateTime.Unspecified;
                                    oDT.Columns.Add(oCol);
                                    foreach (DataRow oRow in oDT.Rows)
                                    {
                                        oRow["LastEditTimeNew"] = oRow["LastEditTime"];
                                    }
                                    oDT.Columns.Remove("LastEditTime");
                                    oDT.Columns["LastEditTimeNew"].ColumnName = "LastEditTime";
                                }
                            }
                        }
                        catch (System.Exception oE)
                        {
                            throw oE;
                        }
                    }
                }

                WriteBenchmark(t0);
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
            SetDataSetTimeZoneOffset(oDS);
            //JTS: This is to avoid Out Of Memory Errors with very large DataSets
            oDS.RemotingFormat = SerializationFormat.Binary;
            return oDS;
        }
        /// <summary>
        /// returns the set of rows for the specified user that were added/edited/deleted 
        /// in the server db since the last synchronization down
        /// </summary>
        /// <param name="iUserID"></param>
        /// <param name="datLastSyncDown"></param>
        /// <returns></returns>
        public DataSet GetUserSyncData(int iUserID, string xLastSyncDown, string xServer, string xDatabase, bool bOnDemand)
        {
            DataSet oDS = new DataSet("MacPacServerDataSet");

            try
            {
                DateTime t0 = DateTime.Now;

                //Don't access cache if no new Admin data has been published since last sync
                DateTime dLastPublishedTime = GetAdminPublishedTime(xServer, xDatabase);
                DateTime datLastSyncDown = DateTime.Parse(xLastSyncDown, LMP.Culture.USEnglishCulture);
                if (dLastPublishedTime == MinSyncCompareDate())
                    //Admin Publish hasn't occurred yet
                    return oDS;

                WriteNameValuePairs("dLastPublishedTime", dLastPublishedTime.ToString(),
                        "datLastSyncDown", datLastSyncDown.ToString());

                FillWithNonCachedData(oDS, bOnDemand, datLastSyncDown, iUserID, xServer, xDatabase);
                if (datLastSyncDown <= dLastPublishedTime)
                {
                    //JTS 4/1/09: Make sure Cache reflects latest data
                    RefreshCacheIfNecessary();
                    FillWithCachedData(oDS, xServer, xDatabase, datLastSyncDown);
                }

                WriteBenchmark(t0, "UserID=" + iUserID.ToString());
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
            SetDataSetTimeZoneOffset(oDS);
            //JTS: This is to avoid Out Of Memory Errors with very large DataSets
            oDS.RemotingFormat = SerializationFormat.Binary;
            return oDS;
        }
        /// <summary>
        /// returns a data set containing all shared segments
        /// </summary>
        /// <param name="xLastSyncDown"></param>
        /// <param name="xServer"></param>
        /// <param name="xDatabase"></param>
        /// <returns></returns>
        public DataSet GetSharedSegments(string xServer, string xDatabase)
        {
            DateTime t0 = DateTime.Now;
            DataSet oDS = new DataSet();

            using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
            {
                //create a datatable to add to dataset
                DataTable oDT = new DataTable("UserSegments");

                try
                {
                    //get all rows edited since the last edit time - create command
                    using (SqlCommand oCmd = new SqlCommand())
                    {
                        oCmd.CommandText = "SELECT * FROM UserSegments WHERE SharedFolderID<>0";
                        oCmd.CommandType = CommandType.Text;
                        oCmd.Connection = oCnn;

                        using (SqlDataAdapter oA = new SqlDataAdapter(oCmd))
                        {
                            //mark all rows as "Added" - this will
                            //force the subsequent DataSet update
                            //to add all rows to target data source
                            oA.AcceptChangesDuringFill = false;

                            DateTime t1 = DateTime.Now;
                            oA.Fill(oDT);
                            WriteBenchmark(t1, "Fill " + oDT.TableName + " (" + oDT.Rows.Count + ")");
                        }
                    }
                    //Add Table to Dataset only of it contains data
                    if (oDT.Rows.Count > 0)
                    {
                        //JTS 12/08/08: Date Column must be in UTC format
                        //so that it will remain unchanged during serialization.
                        //DateTimeMode cannot be changed in populated DataSet,
                        //so must recreate the LastEditTime column instead
                        DataColumn oCol = new DataColumn("LastEditTimeNew");
                        oCol.DataType = System.Type.GetType("System.DateTime");
                        oCol.DateTimeMode = DataSetDateTime.Unspecified;
                        oDT.Columns.Add(oCol);
                        foreach (DataRow oRow in oDT.Rows)
                        {
                            oRow["LastEditTimeNew"] = oRow["LastEditTime"];
                        }
                        oDT.Columns.Remove("LastEditTime");
                        oDT.Columns["LastEditTimeNew"].ColumnName = "LastEditTime";
                        oDS.Tables.Add(oDT);
                    }
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }

            WriteBenchmark(t0);

            return oDS;
        }
        /// <summary>
        /// updates the server database with data supplied
        /// </summary>
        /// <param name="oSourceData"></param>
        /// <summary>
        /// updates the server database with data supplied
        /// </summary>
        /// <param name="oSourceData"></param>
        public void PublishAdminSyncData(DataSet oSourceData, string xServer, string xDatabase)
        {
            try
            {
                //oTimer.Stop();
                DateTime t0 = DateTime.Now;

                //Remote service automatically adjusts dates based on client timezone
                //We need to undo the adjustment so dates reflect UTC again
                AdjustDataSetTimeZone(oSourceData);

                //get connection object
                using (SqlConnection oCnn = GetConnectionObject(
                    xServer, xDatabase, mpSuperUserID, mpSuperUserPassword))
                {

                    //create reusable command object - for use below
                    using (SqlCommand oCmd = new SqlCommand())
                    {
                        oCmd.Connection = oCnn;

                        if ((oCnn.State & ConnectionState.Open) == 0)
                        {
                            try
                            {
                                //open connection
                                oCnn.Open();
                            }
                            catch (Exception oE)
                            {
                                throw oE;
                            }
                        }

                        oCmd.CommandType = CommandType.StoredProcedure;

                        //cycle through data tables, updating the target for each
                        foreach (DataTable oDT in oSourceData.Tables)
                        {
                            //clear target dump table
                            try
                            {
                                oCmd.CommandText = "spSyncUpClear" + oDT.TableName + "Tmp";
                                oCmd.ExecuteNonQuery();
                            }
                            catch (Exception oE)
                            {
                                throw oE;
                            }

                            //populate target dump table with data in data table
                            SqlDataAdapter oA = null;

                            try
                            {
                                using (oA = new SqlDataAdapter())
                                {

                                    SqlCommandBuilder oCB = new SqlCommandBuilder(oA);

                                    //create select command so that command builder
                                    //can generate the insert command
                                    oA.SelectCommand = new SqlCommand(
                                        "SELECT * FROM Sync" + oDT.TableName + "Tmp", oCnn);
                                    oA.SelectCommand.CommandTimeout = 0;
                                    oA.InsertCommand = oCB.GetInsertCommand();
                                    oA.InsertCommand.CommandTimeout = 0;
                                    oA.UpdateCommand = oCB.GetUpdateCommand();
                                    oA.UpdateCommand.CommandTimeout = 0;

                                    oA.Update(oDT);

                                }
                            }
                            catch (Exception oEx)
                            {
                                throw oEx;
                            }
                            try
                            {
                                //transfer data from dump table to target table
                                if (oDT.TableName.ToUpper() != "DELETIONS")
                                {
                                    //JTS 1/05/09: Deletions can be skipped since
                                    //records should never get updated once created
                                    oCmd.CommandText = "spSyncUp" + oDT.TableName + "Update";
                                    oCmd.CommandTimeout = 0;
                                    oCmd.ExecuteNonQuery();
                                }

                                oCmd.CommandText = "spSyncUp" + oDT.TableName + "Add";
                                oCmd.CommandTimeout = 0;
                                oCmd.ExecuteNonQuery();

                                try
                                {
                                    WriteNameValuePairs("oDT.TableName", oDT.TableName);
                                }
                                catch
                                {
                                }

                                //JTS 1/7/09: If this is initial sync to this database, historical Deletions
                                //are added, but deletions are not processed on the server.
                                //This is to prevent the Deletions accidentally showing up and getting
                                //processed at some later time.  This could occur if an Admin publish were 
                                //performed with no LastSync or Last
                                if (oDT.TableName.ToUpper() == "DELETIONS" && 
                                    GetAdminPublishedTime(xServer, xDatabase).Date > MinSyncCompareDate().Date)
                                {
                                    //deleted obsolete records
                                    SyncDeletions(oCnn, true);
                                }
                            }
                            finally
                            {
                                //clear out temp table
                                oCmd.CommandText = "spSyncUpClear" + oDT.TableName + "Tmp";
                                oCmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
                WriteBenchmark(t0);
            }
            catch (Exception oE)
            {
                WriteError(oE);
            }
            finally
            {
                //oTimer.Start();
            }
        }
        /// <summary>
        /// returns the set of rows for the specified user that were added/edited/deleted 
        /// in the server db since the last synchronization down
        /// </summary>
        /// <param name="datLastSyncDown"></param>
        /// <returns></returns>
        public DataSet GetAdminSyncData(string xLastSyncDown, string xServer, string xDatabase)
        {
            DataSet oDS = null;

            // oException = null;

            try
            {
                DateTime t0 = DateTime.Now;
                DateTime datLastSyncDown = DateTime.Parse(xLastSyncDown, LMP.Culture.USEnglishCulture);

                using (SqlConnection oCnn = GetConnectionObject(
                    xServer, xDatabase, mpSuperUserID, mpSuperUserPassword))
                {

                    using (oDS = new DataSet("MacPacServerDataSet"))
                    {
                        //cycle through all tables marked for synchronization
                        for (int i = 0; i < m_aAdminSyncTables.Length; i++)
                        {
                            //create a datatable to add to dataset
                            DataTable oDT = new DataTable(m_aAdminSyncTables[i]);

                            try
                            {
                                //get all rows edited since the last edit time - create command
                                using (SqlCommand oCmd = new SqlCommand())
                                {
                                    oCmd.CommandText = "spAdminSyncDownGet" + m_aAdminSyncTables[i];
                                    oCmd.CommandType = CommandType.StoredProcedure;
                                    oCmd.Connection = oCnn;

                                    //add parameters
                                    SqlParameter oParam = new SqlParameter("@LastSyncDown", datLastSyncDown);
                                    oCmd.Parameters.Add(oParam);

                                    using (SqlDataAdapter oA = new SqlDataAdapter(oCmd))
                                    {
                                        //mark all rows as "Added" - this will
                                        //force the subsequent DataSet update
                                        //to add all rows to target data source
                                        oA.AcceptChangesDuringFill = false;

                                        //WriteNameValuePairs("oDT.TableName", oDT.TableName);
                                        DateTime t1 = DateTime.Now;
                                        oA.Fill(oDT);
                                        WriteBenchmark(t1, "Fill " + oDT.TableName + " (" + oDT.Rows.Count + ")");
                                    }
                                }
                                //Add Table to Dataset only of it contains data
                                if (oDT.Rows.Count > 0)
                                {
                                    //JTS 12/08/08: Date Column must be in UTC format
                                    //so that it will remain unchanged during serialization.
                                    //DateTimeMode cannot be changed in populated DataSet,
                                    //so must recreate the LastEditTime column instead
                                    DataColumn oCol = new DataColumn("LastEditTimeNew");
                                    oCol.DataType = System.Type.GetType("System.DateTime");
                                    oCol.DateTimeMode = DataSetDateTime.Unspecified;
                                    oDT.Columns.Add(oCol);
                                    foreach (DataRow oRow in oDT.Rows)
                                    {
                                        oRow["LastEditTimeNew"] = oRow["LastEditTime"];
                                    }
                                    oDT.Columns.Remove("LastEditTime");
                                    oDT.Columns["LastEditTimeNew"].ColumnName = "LastEditTime";
                                    oDS.Tables.Add(oDT);
                                }
                            }
                            catch (System.Exception oE)
                            {
                                throw oE;
                            }
                        }
                    }
                }

                WriteBenchmark(t0);
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
            SetDataSetTimeZoneOffset(oDS);

            //JTS: This is to avoid Out Of Memory Errors with very large DataSets
            oDS.RemotingFormat = SerializationFormat.Binary;
            return oDS;
        }
        /// <summary>
        /// returns the earliest client version that is compatible with the sync server
        /// </summary>
        /// <returns></returns>
        public string GetRequiredMinClientVersion(string xServer, string xDatabase)
        {
            try
            {
                return GetMetaDataValue(xServer, xDatabase, "RequiredClientMinVersion");
            }
            catch (System.Exception oE)
            {
                //GLOG 4462: If error occurred returning Metadata value, return null to indicate error to client
                WriteError(oE);
                return null;
            }
        }
        //JTS: 03/18/10
        public string GetServerGUID(string xServer, string xDatabase)
        {
            try
            {
                return GetMetaDataValue(xServer, xDatabase, "ServerGUID");
            }
            catch (System.Exception oE)
            {
                //GLOG 4462: If error occurred returning Metadata value, return null to indicate error to client
                WriteError(oE);
                return null;
            }
        }
        /// <summary>
        /// Copies all table data from source db to destination db.
        /// </summary>
        /// 
        public void CopyNetworkDB(string xSourceServer, string xSourceDatabase,
            string xDestServer, string xDestDatabase, string xDestLogin, string xDestPassword)
        {
            try
            {
                //SMOCommon.ServerConnection oSourceCn = new SMOCommon.ServerConnection(xSourceServer, xDestLogin, xDestPassword);
                //SMO.Server oSourceServer = new SMO.Server(oSourceCn);
                //SMO.Database oSourceDB = oSourceServer.Databases[xSourceDatabase];
                //SMOCommon.ServerConnection oTargetCn = new SMOCommon.ServerConnection(xDestServer, xDestLogin, xDestPassword);
                //SMO.Server oTargetServer = new SMO.Server(oTargetCn);
                //SMO.Database oTargetDB = new SMO.Database(oTargetServer, xDestDatabase);
                //SMO.Transfer oTransfer = new SMO.Transfer(oSourceDB);
                
                //oTransfer.CopyAllObjects = true;
                //oTransfer.CopySchema = true;
                //oTransfer.CopyData = true;
                //oTransfer.DestinationServer = xDestServer;
                //oTransfer.DestinationDatabase = xDestDatabase;
                //oTransfer.Options.IncludeIfNotExists = true;

                //oTransfer.TransferData();
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
        }
        ///// <summary>
        ///// Copies all table data from source db to destination db.
        ///// </summary>
        ///// 
        //public void CopyNetworkDB(string xSourceServer, string xSourceDatabase,
        //    string xDestServer, string xDestDatabase, string xDestLogin, string xDestPassword)
        //{
        //    try
        //    {
        //        DateTime t0 = DateTime.Now;
        //        string xSourceCnn = "Persist Security Info=false;Integrated Security=false;" +
        //            "database=" + xSourceDatabase + ";server=" + xSourceServer + ";user id=" +
        //            mpSuperUserID + ";password=" + mpSuperUserPassword;

        //        string xDestCnn = "Persist Security Info=false;Integrated Security=false;" +
        //           "database=" + xSourceDatabase + ";server=" + xSourceServer + ";user id=" + xDestLogin +
        //           ";password=" + xDestPassword;

        //        //create source and destination connections
        //        using (SqlConnection oSourceCnn = new SqlConnection(xSourceCnn))
        //        {
        //            using (SqlConnection oDestCnn = new SqlConnection(xDestCnn))
        //            {

        //                //open connections
        //                oSourceCnn.Open();
        //                oDestCnn.Open();

        //                string xSQL = "";
        //                using (SqlCommand oCmd = new SqlCommand())
        //                {
        //                    oCmd.Connection = oSourceCnn;
        //                    IDataReader oDR;
        //                    SqlBulkCopy oCopy = new SqlBulkCopy(xDestCnn);

        //                    //cycle through all admin tables marked for synchronization
        //                    for (int i = 0; i < m_aAdminSyncTables.Length; i++)
        //                    {
        //                        xSQL = "SELECT * FROM " + m_aAdminSyncTables[i];
        //                        oCmd.CommandText = xSQL;

        //                        //exectute reader
        //                        oDR = oCmd.ExecuteReader();
        //                        oCopy.DestinationTableName = m_aAdminSyncTables[i];

        //                        //set long timeout for the larger tables
        //                        oCopy.BulkCopyTimeout = 500;

        //                        //write data to source db
        //                        oCopy.WriteToServer(oDR);

        //                        //close reader
        //                        oDR.Close();
        //                    }
        //                }
        //                oSourceCnn.Close();
        //                oDestCnn.Close();
        //            }
        //        }
        //        WriteBenchmark(t0);
        //    }
        //    catch (System.Exception oE)
        //    {
        //        WriteError(oE);
        //    }
        //}
        /// <summary>
        /// returns the office ID of the MacPac user associated with the specified system login
        /// </summary>
        /// <param name="xSystemLoginID"></param>
        /// <param name="xServer"></param>
        /// <param name="xDatabase"></param>
        /// <returns></returns>
        public int GetUserOfficeID(string xSystemLoginID, string xServer, string xDatabase)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase, "xSystemLoginID", xSystemLoginID);
                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpSuperUserID, mpSuperUserPassword))
                {
                    //create reusable command object - for use below
                    using (SqlCommand oCmd = new SqlCommand())
                    {
                        //connect to target source
                        oCmd.Connection = oCnn;

                        if ((oCnn.State & ConnectionState.Open) == 0)
                        {
                            try
                            {
                                //open connection
                                oCnn.Open();
                            }
                            catch (System.Exception oE)
                            {
                                throw oE;
                            }
                        }

                        oCmd.CommandType = CommandType.Text;
                        oCmd.CommandText = "SELECT OfficeID FROM People WHERE UserID='" + xSystemLoginID + "'";
                        object oID = oCmd.ExecuteScalar();

                        if (oID != null)
                            return (int)oID;
                        else
                            return 0;
                    }
                }
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                return 0;
            }
        }
        public int GetUserID(string xSystemLoginID, string xServer, string xDatabase)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase, "xSystemLoginID", xSystemLoginID);
                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpSuperUserID, mpSuperUserPassword))
                {
                    //create reusable command object - for use below
                    using (SqlCommand oCmd = new SqlCommand())
                    {
                        //connect to target source
                        oCmd.Connection = oCnn;

                        if ((oCnn.State & ConnectionState.Open) == 0)
                        {
                            try
                            {
                                //open connection
                                oCnn.Open();
                            }
                            catch (System.Exception oE)
                            {
                                throw oE;
                            }
                        }

                        oCmd.CommandType = CommandType.Text;
                        //JTS 11/05/09: Need to escape any apostrophes used in the SQL
                        xSystemLoginID = xSystemLoginID.Replace("'", "''");
                        oCmd.CommandText = "SELECT ID1 FROM People WHERE UserID='" + 
                            xSystemLoginID + "' AND UsageState=1"; 
                        object oID = oCmd.ExecuteScalar();

                        if (oID != null)
                            return (int)oID;
                        else
                            return LMP.Data.MP10Constants.mpGuestPersonID;
                    }
                }
                WriteBenchmark(t0);
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                return 0;
            }
        }
        /// <summary>
        /// Gets/Sets last published time stored on Server
        /// </summary>
        public DateTime GetAdminPublishedTime(string xServer, string xDatabase)
        {
            try
            {
                string xLastTime = GetMetaDataValue(xServer, xDatabase, mpMetadataLastPublishedTime);
                if (xLastTime != "")
                {
                    if (!xLastTime.ToUpper().Contains("Z"))
                        xLastTime = String.ConvertDateToISO8601(xLastTime);
                    return DateTime.Parse(xLastTime, LMP.Culture.USEnglishCulture).ToUniversalTime();
                }
                else
                    return MinSyncCompareDate();
            }
            catch (System.Exception oE)
            {
                //GLOG 4462: There was an error logging into Server or executing the SQL query. 
                //Indicate that by returning Maximum Date Value
                WriteError(oE);
                return DateTime.MaxValue;
            }
        }
        public void SetAdminPublishedTime(string xServer, string xDatabase, string xDate)
        {
            try
            {
                if (!xDate.ToUpper().Contains("Z"))
                    xDate = String.ConvertDateToISO8601(xDate);
                SetMetaDataValue(xServer, xDatabase, mpMetadataLastPublishedTime, xDate);
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
        }
        public bool AdminPublishInProgress(string xServer, string xDatabase)
        {
            try
            {
                return GetMetaDataValue(xServer, xDatabase, mpMetadataPublishInProgress) == "1";
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                //GLOG 4462: If error has occurred, return true, to prevent sync from ocurring
                return true;
            }
        }
        public void StartAdminPublish(string xServer, string xDatabase)
        {
            try
            {
                SetMetaDataValue(xServer, xDatabase, mpMetadataPublishInProgress, "1");
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
            finally
            {
                if(m_oTimer != null)
                    m_oTimer.Stop();
            }
        }
        public void EndAdminPublish(string xServer, string xDatabase)
        {
            try
            {
                SetMetaDataValue(xServer, xDatabase, mpMetadataPublishInProgress, "0");
            }
            catch(System.Exception oE)
            {
                WriteError(oE);
            }
            finally
            {
                if(m_oTimer != null)
                    m_oTimer.Start();
            }
        }
        /// <summary>
        /// builds the data cache - called by admin to ensure
        /// that the cache is built before the first user
        /// request to use the cache
        /// </summary>
        public void BuildCache()
        {
            RefreshCache();
        }
        #endregion
        #region**************************private methods**************************
        /// <summary>
        /// connects to the specified data source if the connection doesn't exist
        /// </summary>
        /// 
        private static SqlConnection GetConnectionObject(string xServer, string xDatabase, string xLogin, string xPassword)
        {
            try
            {
                //create connection string from parameters
                string xCnn = "Persist Security Info=false;Integrated Security=false;" +
                    "database=" + xDatabase + ";server=" + xServer + @";user id=" + xLogin +
                    ";password=" + xPassword + ";Connect Timeout=" + m_iConnectionTimeout ;

                return new SqlConnection(xCnn);
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                return null;
            }
        }
        /// <summary>
        /// refreshes the existing cache
        /// </summary>
        private static void RefreshCache()
        {
            if(m_oTimer != null)
                m_oTimer.Stop();

            try
            {
                DateTime t0 = DateTime.Now;

                //refresh cache
                DataSet oOldCache = m_oUserSyncDownCachedData;
                DataSet oNewCache = GetCache(m_xCachedServer, m_xCachedDatabase);

                if (oNewCache != null)
                {
                    //a new cache was retrieved - put the new one in place,
                    //and discard the old, if there is one
                    m_oUserSyncDownCachedData = oNewCache;

                    DateTime dLastPublishedTime = MinSyncCompareDate();
                    string xLast = GetMetaDataValue(m_xCachedServer, m_xCachedDatabase, mpMetadataLastPublishedTime);
                    if (xLast != "")
                    {
                        if (!xLast.ToUpper().Contains("Z"))
                            xLast = String.ConvertDateToISO8601(xLast);
                        dLastPublishedTime = DateTime.Parse(xLast, LMP.Culture.USEnglishCulture).ToUniversalTime();
                    }
                    //Store most recent pubished date in memory
                    m_dLastCachedSync = dLastPublishedTime;
                    //JTS 12/10/08: Track Server Metadata indicating that a structure update has occurred
                    m_xLastStructureUpdateIDs = GetMetaDataValue(m_xCachedServer, m_xCachedDatabase, mpMetadataLastStructureUpdate);

                    if (oOldCache != null)
                    {
                        //dispose of old cache
                        oOldCache.Dispose();
                    }
                }
                WriteBenchmark(t0);

                if(m_oTimer != null)
                    m_oTimer.Start();
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
        }
        /// <summary>
        /// Refreshes Cache if admin data has been published since last refresh
        /// </summary>
        private static void RefreshCacheIfNecessary()
        {
            try
            {
                DateTime dLastPublishedTime = MinSyncCompareDate();
                string xLast = GetMetaDataValue(m_xCachedServer, m_xCachedDatabase, mpMetadataLastPublishedTime);
                //JTS 12/10/08: Check if Metadata indicating a structure update has changed, and refresh if so
                string xStructureUpdateIDs = GetMetaDataValue(m_xCachedServer, m_xCachedDatabase, mpMetadataLastStructureUpdate);
                if (xLast != "")
                {
                    if (!xLast.ToUpper().Contains("Z"))
                        xLast = String.ConvertDateToISO8601(xLast);
                    dLastPublishedTime = DateTime.Parse(xLast, 
                        LMP.Culture.USEnglishCulture).ToUniversalTime();
                }
                WriteNameValuePairs("dLastPublishedTime", dLastPublishedTime.ToString(),
                    "m_dLastCachedSync", m_dLastCachedSync.ToString(), 
                    "xStructureUpdateIDs", xStructureUpdateIDs,
                    "m_xLastStructureUpdateIDs", m_xLastStructureUpdateIDs);
                if ((dLastPublishedTime > MinSyncCompareDate()) && 
                    ((m_dLastCachedSync < dLastPublishedTime) || 
                    (m_xLastStructureUpdateIDs != GetMetaDataValue(m_xCachedServer, m_xCachedDatabase, mpMetadataLastStructureUpdate))))
                    RefreshCache();
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
            }
        }
        /// <summary>
        /// updates the memory cache
        /// </summary>
        private static DataSet GetCache(string xServer, string xDatabase)
        {
            DataSet oDS = new DataSet("MacPacDataCache");

            try
            {
                DateTime t0 = DateTime.Now;

                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
                {
                    //cycle through all tables marked for caching
                    for (int i = 0; i < m_aCachableUserSyncTables.Length; i++)
                    {
                        //create a datatable to add to dataset
                        DataTable oDT = oDS.Tables.Add();
                        oDT.TableName = m_aCachableUserSyncTables[i];

                        try
                        {
                            //get all rows in the table
                            using (SqlCommand oCmd = new SqlCommand())
                            {
                                oCmd.CommandText = "SELECT * FROM " + m_aCachableUserSyncTables[i];
                                oCmd.CommandType = CommandType.Text;
                                oCmd.Connection = oCnn;
                                oCmd.CommandTimeout = m_iCommandTimeout;

                                using (SqlDataAdapter oA = new SqlDataAdapter(oCmd))
                                {
                                    //mark all rows as "Added" - this will
                                    //force the subsequent DataSet update
                                    //to add all rows to target data source
                                    oA.AcceptChangesDuringFill = false;

                                    //WriteNameValuePairs("oDT.TableName", oDT.TableName);

                                    //benchmark the fill
                                    DateTime t1 = DateTime.Now;
                                    oA.Fill(oDT);
                                    WriteBenchmark(t1, "Fill " + oDT.TableName + " (" + oDT.Rows.Count + ")");
                                }
                            }
                        }
                        catch (System.Exception oE)
                        {
                            throw oE;
                        }
                    }

                    WriteBenchmark(t0);

                    //return new cache
                    return oDS;
                }

            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                return null;
            }
        }
        /// <summary>
        /// fills the specified dataset with data from the specified cache
        /// </summary>
        /// <param name="oDS"></param>
        /// <param name="xServer"></param>
        /// <param name="xDatabase"></param>
        private static void FillWithCachedData(DataSet oDS, string xServer, string xDatabase, DateTime datLastSyncDown)
        {
            DateTime t0 = DateTime.Now;

            DataSet oCache = null;
            DataTable oCachedTable;

            if (xServer.ToUpper() == m_xCachedServer.ToUpper() && xDatabase.ToUpper() == m_xCachedDatabase.ToUpper())
            {
                //use the main cache - 
                oCache = m_oUserSyncDownCachedData;
            }
            else
            {
                //we're synching with a different database -
                //get a temporary cache
                oCache = GetCache(xServer, xDatabase);
            }

            //cycle through all specified tables
            for (int i = 0; i < m_aCachableUserSyncTables.Length; i++)
            {
                string xTableName = m_aCachableUserSyncTables[i];

                //get cached table
                oCachedTable = oCache.Tables[xTableName];

                try
                {
                    //get all rows edited since the last edit time - 
                    DataView oView = new DataView(oCachedTable, "LastEditTime > #" + 
                        datLastSyncDown.ToString() + "#", "LastEditTime", DataViewRowState.CurrentRows);
                    
                    //add filtered view as table to dataset
                    DataTable oDT = oView.ToTable(xTableName);
                    //Add Table to Dataset only if there are changed records
                    if (oDT.Rows.Count > 0)
                    {
                        //JTS 12/08/08: Date Column must be in UTC format
                        //so that it will remain unchanged during serialization.
                        //DateTimeMode cannot be changed in populated DataSet,
                        //so must recreate the LastEditTime column instead
                        DataColumn oCol = new DataColumn("LastEditTimeNew");
                        oCol.DataType = System.Type.GetType("System.DateTime");
                        oCol.DateTimeMode = DataSetDateTime.Unspecified;
                        oDT.Columns.Add(oCol);
                        foreach (DataRow oRow in oDT.Rows)
                        {
                            oRow["LastEditTimeNew"] = oRow["LastEditTime"];
                        }
                        oDT.Columns.Remove("LastEditTime");
                        oDT.Columns["LastEditTimeNew"].ColumnName = "LastEditTime";
                        oDS.Tables.Add(oDT);
                    }
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }

            WriteBenchmark(t0);
        }
        /// <summary>
        /// fills the specified dataset with data from the non-cachable tables
        /// </summary>
        /// <param name="oDS"></param>
        /// <param name="bOnDemand"></param>
        private static void FillWithNonCachedData(DataSet oDS, bool bOnDemand, DateTime datLastSyncDown, int iUserID, string xServer, string xDatabase)
        {
            DateTime t0 = DateTime.Now;

            string[] aUserSyncDownTables;

            if (bOnDemand)
                aUserSyncDownTables = m_aOnDemandTables;
            else
                aUserSyncDownTables = m_aNonCachableUserSyncTables;

            using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
            {
                //cycle through all specified tables
                for (int i = 0; i < aUserSyncDownTables.Length; i++)
                {
                    //create a datatable to add to dataset
                    DataTable oDT = new DataTable(aUserSyncDownTables[i]);

                    try
                    {
                        //get all rows edited since the last edit time - create command
                        using (SqlCommand oCmd = new SqlCommand())
                        {
                            oCmd.CommandText = "spUserSyncDownGet" + aUserSyncDownTables[i];
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.Connection = oCnn;

                            //add parameters
                            SqlParameter oParam = new SqlParameter("@LastSyncDown", datLastSyncDown);
                            oParam.DbType = DbType.Date;
                            oCmd.Parameters.Add(oParam);
                            oParam = new SqlParameter("@UserID", iUserID);
                            oCmd.Parameters.Add(oParam);

                            using (SqlDataAdapter oA = new SqlDataAdapter(oCmd))
                            {
                                //mark all rows as "Added" - this will
                                //force the subsequent DataSet update
                                //to add all rows to target data source
                                oA.AcceptChangesDuringFill = false;

                                DateTime t1 = DateTime.Now;
                                oA.Fill(oDT);
                                WriteBenchmark(t1, "Fill " + oDT.TableName + " (" + oDT.Rows.Count + ")");
                            }
                        }
                        //Add Table to Dataset only of it contains data
                        if (oDT.Rows.Count > 0)
                        {
                            //JTS 12/08/08: Date Column must be in UTC format
                            //so that it will remain unchanged during serialization.
                            //DateTimeMode cannot be changed in populated DataSet,
                            //so must recreate the LastEditTime column instead
                            DataColumn oCol = new DataColumn("LastEditTimeNew");
                            oCol.DataType = System.Type.GetType("System.DateTime");
                            oCol.DateTimeMode = DataSetDateTime.Unspecified;
                            oDT.Columns.Add(oCol);
                            foreach (DataRow oRow in oDT.Rows)
                            {
                                oRow["LastEditTimeNew"] = oRow["LastEditTime"];
                            }
                            oDT.Columns.Remove("LastEditTime");
                            oDT.Columns["LastEditTimeNew"].ColumnName = "LastEditTime";
                            oDS.Tables.Add(oDT);
                        }
                    }
                    catch (System.Exception oE)
                    {
                        throw oE;
                    }
                }
            }

            WriteBenchmark(t0);
        }
        /// <summary>
        /// delete records specified in Deletions table 
        /// </summary>
        /// <returns></returns>
        private static void SyncDeletions(SqlConnection oCnn, bool bAdmin)
        {
            DateTime t0 = DateTime.Now;

            mpObjectTypes oObjectTypeID;
            string xObjectID;

            //get last download date
            // has to be passed from SyncClient, I think
            //
            //get records from deletion that are more recent than last download date
            DataSet oDS = GetDeletionsDataSet(oCnn);

            foreach (DataRow oRow in oDS.Tables[0].Rows)
            {
                oObjectTypeID = (mpObjectTypes)(int)oRow["ObjectTypeID"];
                xObjectID = oRow["ObjectID"].ToString();
                //JTS 4/1/09: Make sure deletions of Admin objects don't get processed
                //during a user sync, even if date has somehow been changed
                if (!bAdmin)
                {
                    switch (oObjectTypeID)
                    {
                        case mpObjectTypes.AttorneyLicenses:
                        case mpObjectTypes.People:
                        case mpObjectTypes.Proxy:
                        case mpObjectTypes.Proxies:
                        case mpObjectTypes.Permissions:
                        case mpObjectTypes.KeySets:
                        case mpObjectTypes.UserFolderMembers:
                        case mpObjectTypes.UserFolders:
                        case mpObjectTypes.UserSegment:
                        case mpObjectTypes.VariableSet:
                        case mpObjectTypes.FavoritePeople:
                            break;
                        default:
                            //If not Admin Sync, don't process non-user deletions - go to next deletion record
                            continue;
                    }
                }

                //create command object 
                using (SqlCommand oCmd = GetDeleteCommand(oObjectTypeID, xObjectID, oCnn))
                {
                    try
                    {
                        if (oCmd.CommandText != null && oCmd.CommandText != "" &&
                                oObjectTypeID != mpObjectTypes.Addresses)
                            oCmd.ExecuteNonQuery();
                    }
                    catch (System.Exception oE)
                    {
                        WriteError(oE);
                    }
                }
            }

            WriteBenchmark(t0);
        }
        //private void SyncDeletionsOLD()
        //{
        //    mpObjectTypes oObjectTypeID;
        //    string xObjectID;
        //    SqlCommand oCmd = new SqlCommand();

        //    oCmd.Connection = m_oSQLCnn;

        //    //get last download date
        //    // has to be passed from SyncClient, I think
        //    //
        //    //get records from deletion that are more recent than last download date
        //    DataSet oDS = GetDeletionsDataSet();

        //    foreach (DataRow oRow in oDS.Tables[0].Rows)
        //    {
        //        oObjectTypeID = (mpObjectTypes)(int)oRow["ObjectTypeID"];
        //        xObjectID = oRow["ObjectID"].ToString();

        //        //create command object 
        //        oCmd = GetDeleteCommand(oObjectTypeID, xObjectID);

        //        try
        //        {
        //            if (oCmd.CommandText != null && oCmd.CommandText != "" &&
        //                    oObjectTypeID != mpObjectTypes.Addresses)
        //                oCmd.ExecuteNonQuery();
        //        }
        //        catch (SoapException oE)
        //        {
        //            throw oE;
        //        }
        //        finally
        //        {
        //            oCmd.Dispose();
        //        }
        //    }
        //}

        /// <summary>
        /// returns DataSet from Deletions table 
        /// </summary>
        /// <returns>DataSet</returns>
        private static DataSet GetDeletionsDataSet(SqlConnection oCnn)
        {
            DataSet oDS = new DataSet("DeletionsDataSet");

            //create command object 
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = oCnn;

            //The records in the SyncDeletionsTmp table
            //show what has been deleted since the last SyncUp.
            //These records have also been added to the Deletions table,
            //but using them would require passing along a LastSync parameter
            //from the client through several methods.  
            oCmd.CommandText = "SELECT * FROM SyncDeletionsTmp";

            //create data adapter from command
            using (SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd))
            {
                oAdapter.Fill(oDS);
            }
            return oDS;
        }
        /// <summary>
        /// builds command object to delete obsolete records 
        /// </summary>
        /// <param name="oObjectTypeID"></param>
        /// <param name="xObjectID"></param>
        /// <returns>SqlCommand</returns>
        private static SqlCommand GetDeleteCommand(mpObjectTypes oObjectTypeID, string xObjectID, SqlConnection oCnn)
        {
            SqlCommand oCmd = new SqlCommand();
            string[] aParams;
            char cSep = '.';
            WriteNameValuePairs("oObjectTypeID", (int)oObjectTypeID, "xObjectID", xObjectID);
            SqlParameter oParam = new SqlParameter();

            oCmd.Connection = oCnn;
            oCmd.CommandType = CommandType.StoredProcedure;

            switch (oObjectTypeID)
            {
                case mpObjectTypes.UserSegment:
                    oCmd.CommandText = "spUserSegmentsDelete";
                    //note for local, it's 1 param.
                    //2 params.
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@ID1", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ID2", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Segment:
                case mpObjectTypes.Segments:
                    oCmd.CommandText = "spSegmentsDelete";
                    oParam = new SqlParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.ContentFolder:
                    oCmd.CommandText = "spFoldersDelete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.VariableSet:
                case mpObjectTypes.VariableSets:
                    oCmd.CommandText = "spVariableSetsDelete";
                    //note:  local has 1 param.
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@ID1", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ID2", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.UserFolderMembers:
                    oCmd.CommandText = "spUserFolderMembersDelete";
                    //2 params.
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@ID1", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ID2", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.AdminFolderMembers:
                    oCmd.CommandText = "spFolderMembersDelete";
                    //2 params.
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@FolderID", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@SegmentID", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.UserFolders:
                    //1 param in local.
                    oCmd.CommandText = "spUserFoldersDelete";
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@ID1", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ID2", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Person:
                case mpObjectTypes.People:
                    //do nothing, table has a usage state field.
                    break;
                case mpObjectTypes.Group:
                    oCmd.CommandText = "spPeopleGroupsDelete";
                    oParam = new SqlParameter("@ID", Int32.Parse(xObjectID));
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Proxy:
                case mpObjectTypes.Proxies:
                    //im here
                    oCmd.CommandText = "spProxyDelete";
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@User", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@Proxy", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.User:
                    break;
                case mpObjectTypes.Alias:
                    break;
                case mpObjectTypes.Office:
                case mpObjectTypes.Offices:
                    //do nothing, table has a usage state field.
                    break;
                case mpObjectTypes.Courier:
                case mpObjectTypes.Couriers:
                    oCmd.CommandText = "spCouriersDelete";
                    oParam = new SqlParameter("@CourierID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Court:
                case mpObjectTypes.Courts:
                    oCmd.CommandText = "spCourtsDelete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.OfficeAddressFormat:
                case mpObjectTypes.AddressFormats:
                    oCmd.CommandText = "spAddressFormatsDelete";
                    oParam = new SqlParameter("@ID", Int32.Parse(xObjectID));
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.ExternalConnections:
                    oCmd.CommandText = "spExternalConnectionsDelete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.KeySets:
                    //GLOG 4937: Individual Keyset deletions also need to be handled
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 5)
                    {
                        oCmd.CommandText = "spKeySetDelete";
                        oParam = new SqlParameter("@ScopeID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@ScopeID2", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@OwnerID1", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@OwnerID2", aParams[3]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@Type", aParams[4]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@Culture", aParams[5]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else
                    {
                        oCmd.CommandText = "spKeySetsDeleteByEntityIDs";
                        //Only a single parameter
                        if (aParams.GetUpperBound(0) == 0)
                        {
                            aParams = new string[2];
                            aParams[0] = xObjectID;
                            aParams[1] = "0";
                        }
                        oParam = new SqlParameter("@ID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@ID2", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;

                case mpObjectTypes.Lists:
                    oCmd.CommandText = "spListsDelete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Restrictions:
                    break;
                case mpObjectTypes.Translations:
                    oCmd.CommandText = "spTranslationsDeleteByID";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.ValueSets:
                    oCmd.CommandText = "spValueSetDelete";
                    oParam = new SqlParameter("@SetID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Permissions:
                    //check local query, some of the field names are wrong.
                    oCmd.CommandText = "spPermissionsDeleteByObjectIDs";
                    //3 params
                    aParams = xObjectID.Split(cSep);
                    oParam = new SqlParameter("@ObjectTypeID", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ObjectID1", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ObjectID2", aParams[2]);
                    oCmd.Parameters.Add(oParam);
                    //JTS 12/31/08: Optional 4th Parameter for PersonID
                    if (aParams.GetUpperBound(0) > 2)
                        oParam = new SqlParameter("@PersonID", aParams[3]);
                    else
                        oParam = new SqlParameter("@PersonID", DBNull.Value);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions0:
                    oCmd.CommandText = "spJurisdictions0Delete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions1:
                    oCmd.CommandText = "spJurisdictions1Delete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions2:
                    oCmd.CommandText = "spJurisdictions2Delete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions3:
                    oCmd.CommandText = "spJurisdictions3Delete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions4:
                    oCmd.CommandText = "spJurisdictions4Delete";
                    oParam = new SqlParameter("@ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Addresses:
                    oCmd.CommandText = "spAddressesDelete";
                    oParam = new SqlParameter("@AddressID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Assignments:
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 2)
                    {
                        //3 params - Group Permissions
                        oCmd.CommandText = "spObjectPermissionRevoke";
                        oParam = new SqlParameter("@AssignedObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@AssignedObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@Entity", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else if (aParams.GetUpperBound(0) == 1)
                    {
                        //2 params
                        oCmd.CommandText = "spAssignmentsDeleteByObjectIDs";
                        oParam = new SqlParameter("@ObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@ObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else if (aParams.GetUpperBound(0) == 3)
                    {
                        //4 params
                        oCmd.CommandText = "spAssignmentsDelete";
                        oParam = new SqlParameter("@TargetObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@TargetObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@AssignedObjectTypeID", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@AssignedObjectID", aParams[3]);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.AttorneyLicenses:
                    //GLOG 3691: Handle deletion normally
                    oCmd.CommandText = "spAttyLicensesDelete";
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 1)
                    {
                        oParam = new SqlParameter("@ID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@ID2", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.Counties:
                    break;
                case mpObjectTypes.Countries:
                    break;
                case mpObjectTypes.States:
                    break;
                case mpObjectTypes.GroupAssignments:
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 1)
                    {
                        //2 params
                        oCmd.CommandText = "spPeopleGroupMembersDelete";
                        oParam = new SqlParameter("@PersonID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@GroupID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else
                    {
                        oCmd.CommandText = "spPeopleGroupAssignmentsDeleteByGroupID";
                        oParam = new SqlParameter("@GroupID", xObjectID);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.FavoritePeople:
                    //GLOG 3544: Delete Favorite Person record
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 1)
                    {
                        //2 params
                        oCmd.CommandText = "spFavoritePeopleDelete";
                        oParam = new SqlParameter("@OwnerID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@OwnerID2", "0");
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@PersonID1", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new SqlParameter("@PersonID2", "0");
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
            }

            return oCmd;
        }
        public void LogEvent(string xMessage)
        {
            LogEvent(xMessage, EventLogEntryType.Information);
        }
        /// <summary>
        /// Creates an entry in the Application Event Log
        /// </summary>
        /// <param name="xMessage"></param>
        /// <param name="iType"></param>
        private static void LogEvent(string xMessage, EventLogEntryType iType)
        {
            try
            {
                EventLogPermission eventLogPermission = new EventLogPermission(EventLogPermissionAccess.Administer, ".");
                eventLogPermission.PermitOnly();
                //Using arbitrary value of 1000 for Event ID
                //With mscoree.dll as the EventMessageFile, this ensures that only
                //the specified text is logged, without "Description for Event ID ... cannot be found"
                EventLog.WriteEntry(mpEventLogApplication, xMessage, iType, 1000);
            }
            catch { }
        }

        private static void WriteError(System.Exception oE)
        {
            WriteError(oE, 0);
        }
        private static void WriteError(System.Exception oE, int iUserID)
        {
            string xUserID = (iUserID == 0 ? "N/A" : iUserID.ToString());
            string xError = oE.Message + "\r\nUserID=" + xUserID + "\r\n" + oE.StackTrace;
            LogEvent(xError, EventLogEntryType.Error);
        }
        private static void WriteNameValuePairs(params object[] oNameValuePairs)
        {
            if (LogLevel > 0)
            {
                StringBuilder oSB = new StringBuilder();

                for (int i = 0; i < oNameValuePairs.Length; i += 2)
                {
                    try
                    {
                        //first of two is the name
                        string xName = oNameValuePairs[i].ToString();

                        //convert object value to string
                        string xValue = oNameValuePairs[i + 1].ToString();

                        //append name/value pair to string
                        oSB.Append(string.Concat(xName, "=", xValue));

                        //append separator
                        if (i < oNameValuePairs.Length - 2)
                            oSB.Append(", ");
                    }

                    //ignore if name/value pair can't be added to string - 
                    //we're tracing because we're examining a different error
                    catch { }
                }
                //set StackFrame of function that called this method - 
                //always has index of 1
                Diag.StackTrace oST = new Diag.StackTrace();
                Diag.StackFrame oStackFrame = oST.GetFrame(1);

                //get StackFrame method
                MethodBase oCaller = oStackFrame.GetMethod();

                LogEvent(oCaller.Name + ":\t" + oSB.ToString(), EventLogEntryType.Information);
            }
        }
        /// <summary>
        /// Tracks elapsed time in the Application Event Log
        /// </summary>
        /// <param name="dtStart"></param>
        private static void WriteBenchmark(DateTime dtStart)
        {
            WriteBenchmark(dtStart, null);
        }
        private static void WriteBenchmark(DateTime dtStart, string xLabel)
        {
            if (BenchmarkMode > 0)
            {
                //get end time
                DateTime dtEnd = DateTime.Now;

                string xElapsedTime = "";

                //get time xElapsedTime
                TimeSpan oElapsedTime =
                    new TimeSpan(dtEnd.Ticks - dtStart.Ticks);

                if (oElapsedTime.TotalSeconds < .04)
                    xElapsedTime = "  " + oElapsedTime.TotalSeconds.ToString("0.000");
                else
                    xElapsedTime = "* " + oElapsedTime.TotalSeconds.ToString("0.000");

                //get total xRunning time
                string xRunning;
                if (m_dtInitialStart == 0)
                {
                    //there is no previous time recorded
                    xRunning = xElapsedTime;

                    //set last end time for next iteration
                    m_dtInitialStart = dtStart.Ticks;
                }
                else
                {
                    //get time xElapsedTime since last recorded time
                    TimeSpan xRunningTime = new TimeSpan(
                        dtEnd.Ticks - m_dtInitialStart);
                    xRunning = xRunningTime.TotalSeconds.ToString("0.00");
                }

                //pad to force decimal alignment
                int iPos = xElapsedTime.IndexOf(".");
                string xPadBefore = new string(' ', 6 - iPos + 1);
                string xPadAfter = new string(' ', 6 - (xElapsedTime.Length - iPos));

                iPos = xRunning.IndexOf(".");
                string xRunningPadBefore = new string(' ', 6 - iPos + 1);
                string xRunningPadAfter = new string(' ', 6 - (xRunning.Length - iPos));

                //get xLabels, separate with ";" -
                //first label is the member name - 
                //set StackFrame of function that called this method - 
                //always has index of 1
                Diag.StackTrace oST = new Diag.StackTrace();
                Diag.StackFrame oStackFrame = oST.GetFrame(1);

                //get StackFrame method
                MethodBase oCaller = oStackFrame.GetMethod();

                if (oCaller.Name == "WriteBenchmark")
                {
                    //called from an overload of this method - 
                    //get stack previous to caller
                    oStackFrame = oST.GetFrame(2);
                    oCaller = oStackFrame.GetMethod();
                }

                //add member name
                StringBuilder oSB = new StringBuilder();
                oSB.AppendFormat("{0}",
                    oCaller.Name);

                if (!string.IsNullOrEmpty(xLabel))
                {
                    oSB.AppendFormat(" {0}", xLabel);
                }

                string xLabelString = oSB.ToString();

                //build text to write
                oSB.Remove(0, oSB.Length);
                oSB.AppendFormat("{0}{1}    {2}{3}    {4}", xPadBefore, xElapsedTime,
                    xRunningPadBefore, xRunning, xLabelString);
                LogEvent(oSB.ToString(), EventLogEntryType.Information);
            }
        }
        /// <summary>
        /// Get/Set BenchmarkMode - value > 0 turns on Benchmarking
        /// </summary>
        private static int BenchmarkMode
        {
            get { return m_iBenchmarkMode; }
            set { m_iBenchmarkMode = value; }
        }
        /// <summary>
        /// Get/Set Logging Level - value > 0 logs informational messages
        /// </summary>
        private static int LogLevel
        {
            get { return m_iLogLevel; }
            set { m_iLogLevel = value; }
        }
        /// <summary>
        /// resets the start of the running time counter
        /// </summary>
        private static void ResetStartTime()
        {
            m_dtInitialStart = 0;
        }
        private static string GetMetaDataValue(string xServer, string xDatabase, string xName)
        {
            using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
            {
                if ((oCnn.State & ConnectionState.Open) == 0)
                {
                    try
                    {
                        //open connection
                        oCnn.Open();
                    }
                    catch (System.Exception oE)
                    {
                        throw oE;
                    }
                }

                using (SqlCommand oCmd = new SqlCommand())
                {
                    try
                    {
                        oCmd.CommandText = "SELECT [Value] FROM Metadata WHERE [Name] = '" + xName + "';";
                        oCmd.CommandType = CommandType.Text;
                        oCmd.Connection = oCnn;
                        object oRet = oCmd.ExecuteScalar();
                        if (oRet != null)
                            return (string)oRet;
                        else
                            return "";
                    }
                    catch
                    {
                        return "";
                    }
                }
            }
        }
        /// <summary>
        /// Returns arbitrary far past date that is compatible with SQL Server limits (minimum 1753)
        /// </summary>
        private static DateTime MinSyncCompareDate()
        {
            return new DateTime(1800, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        }
        /// <summary>
        /// Adds Extended Property to Dataset with local system's time offset from UTC
        /// </summary>
        /// <param name="oDS"></param>
        /// <returns></returns>
        private static long SetDataSetTimeZoneOffset(DataSet oDS)
        {
            string xTimeZoneOffsetTicks = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks.ToString();
            oDS.ExtendedProperties["UTCDifference"] = xTimeZoneOffsetTicks;
            return long.Parse(xTimeZoneOffsetTicks);
        }
        /// <summary>
        /// Adjusts DateTime values in Dataset back to original value passed from client
        /// (Code from MSKB842545)
        /// </summary>
        /// <param name="dataSet"></param>
        private static void AdjustDataSetTimeZone(DataSet dataSet)
        {
            // Obtains the time difference on the sender computer that
            //remoted this dataset to the Web service.
            string sourceTicksString = "";
            try
            {
                sourceTicksString = dataSet.ExtendedProperties["UTCDifference"].ToString();
                if (string.IsNullOrEmpty(sourceTicksString))
                    return;
            }
            catch
            {
                return;
            }
            long sourceTicks = long.Parse(sourceTicksString);
            // Obtain the UTC offset for the remote computer.
            DateTime baseUTC = DateTime.Now;
            long UtcTickslocal = TimeZone.CurrentTimeZone.GetUtcOffset(baseUTC).Ticks;
            // Obtain the time difference between the sender computer and the remote computer.
            //Do nothing if client timezone is same as web server
            if (sourceTicks == UtcTickslocal)
                return;
            long ticksDifference = sourceTicks - UtcTickslocal;
            TimeSpan timespan = new TimeSpan(ticksDifference);
            WriteNameValuePairs("timespan", timespan.ToString());
            // The following code iterates through each table, and find all the columns that are 
            // DateTime columns. After identifying the columns that have to be adjusted,
            // it traverses the data in the table and adjusts the DateTime columns back to their 
            // original values. You must leave the RowState of the DataRow in the same state 
            //after making the adjustments.
            foreach (DataTable table in dataSet.Tables)
            {
                DataColumnCollection columns = table.Columns;
                int[] ColumnNumbers = new int[columns.Count];
                int ColumnNumbersIndex = 0;
                for (int i = 0; i < columns.Count; i++)
                {
                    DataColumn col = columns[i];
                    if (col.DataType == typeof(DateTime))
                    {
                        ColumnNumbers[ColumnNumbersIndex] = i;
                        ColumnNumbersIndex++;
                    }
                }
                foreach (DataRow row in table.Rows)
                {
                    switch (row.RowState)
                    {
                        case DataRowState.Unchanged:
                            AdjustDateTimeValues(row, ColumnNumbers,
                            ColumnNumbersIndex, timespan);
                            row.AcceptChanges();	// This is to make sure that the
                            // row appears to be unchanged again.
                            Debug.Assert(row.RowState == DataRowState.Unchanged);
                            break;
                        case DataRowState.Added:
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // The row is still in a DataRowState.Added state.
                            Debug.Assert(row.RowState == DataRowState.Added);
                            break;
                        case DataRowState.Modified:
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // The row is a still DataRowState.Modified.
                            Debug.Assert(row.RowState == DataRowState.Modified);
                            break;
                        case DataRowState.Deleted:
                            //   This is to make sure that you obtain the right results if 
                            //the .RejectChanges()method is called.
                            row.RejectChanges();	// This is to "undo" the delete.
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // To adjust the datatime values.
                            // The row is now in DataRowState.Modified state.
                            Debug.Assert(row.RowState == DataRowState.Modified);
                            row.AcceptChanges();	// This is to mark the changes as permanent.
                            Debug.Assert(row.RowState == DataRowState.Unchanged);
                            row.Delete();
                            // Delete the row. Now, it has the same state as it started.
                            Debug.Assert(row.RowState == DataRowState.Deleted);
                            break;
                        default:
                            throw new ApplicationException
                            ("You must add a case statement that handles the new version of the dataset.");
                    }
                }
            }
        }
        private static void AdjustDateTimeValues(DataRow row, int[] ColumnNumbers, int columnCount, TimeSpan timespan)
        {
            for (int i = 0; i < columnCount; i++)
            {
                int columnIndex = ColumnNumbers[i];
                DateTime original = (DateTime)row[columnIndex];
                DateTime modifiedDateTime = original.Add(timespan);
                row[columnIndex] = modifiedDateTime;
            }
        }
        private static void SetMetaDataValue(string xServer, string xDatabase, string xName, string xValue)
        {
            try
            {
                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
                {
                    if ((oCnn.State & ConnectionState.Open) == 0)
                    {
                        //open connection
                        oCnn.Open();
                    }

                    using (SqlCommand oCmd = new SqlCommand())
                    {
                        oCmd.CommandText = "UPDATE Metadata SET [Value] = '" + xValue
                            + "' WHERE [Name]='" + xName + "'";
                        oCmd.CommandType = CommandType.Text;
                        oCmd.Connection = oCnn;
                        int iNumRowsAffected = oCmd.ExecuteNonQuery();
                        if (iNumRowsAffected == 0)
                        {
                            oCmd.CommandText = "INSERT INTO Metadata ([Name], [Value]) Values('" +
                                xName + "', '" + xValue + "');";
                            iNumRowsAffected = oCmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                throw oE;
            }
        }
        public string[] PeopleCustomFieldList(string xServer, string xDatabase)
        {
            try
            {
                List<string> aList = null;
                using (SqlConnection oCnn = GetConnectionObject(xServer, xDatabase, mpUserID, mpUserPassword))
                {
                    //create reusable command object - for use below
                    using (SqlCommand oCmd = new SqlCommand())
                    {

                        //connect to target source
                        oCmd.Connection = oCnn;

                        if ((oCnn.State & ConnectionState.Open) == 0)
                        {
                            try
                            {
                                //open connection
                                oCnn.Open();
                            }
                            catch (System.Exception oE)
                            {
                                WriteError(oE);
                                throw oE;
                            }
                        }
                        oCmd.CommandText = "SELECT TOP 1 * FROM PEOPLE;";
                        SqlDataReader oReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly);
                        for (int i = 0; i < oReader.FieldCount - 24; i++)
                        {
                            string xField = oReader.GetName(i + 24);
                            if (!MP10Constants.mpRequiredDBFields.Contains(xField))
                            {
                                //this is a custom field

                                if (aList == null)
                                    //create list if necessary
                                    aList = new List<string>();

                                //add to list
                                aList.Add(xField);
                            }
                        }
                        oReader.Close();
                    }
                }
                //JTS 3/19/09: Convert List to string array for serialization
                if (aList != null && aList.Count > 0)
                    return (string[])aList.ToArray();
                else
                    return null;
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                //GLOG 4462: shouldn't throw error when remoting
                return null;
            }

        }
        #endregion
        #region**************************event handlers**************************
        static void oTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                RefreshCacheIfNecessary();
            }
            catch (System.Exception oE)
            {
                WriteError(oE);
                m_oTimer.Stop();
            }
        }
        #endregion
        #region *************************helper classes*************************
        #endregion
    }
}
