using System;
using System.IO;
using System.Windows.Forms;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a
	/// MacPac Object Type definition - derived from TranslatableDataItem -
	/// created by Daniel Fisherman - 09/04
	/// </summary>
	public sealed class AdminSegmentDef: TranslatableDataItem, ISegmentDef
	{
        #region *********************fields*********************
        private string m_xName;
		private string m_xDisplayName;
		private mpObjectTypes m_shTypeID;
		private string m_xXML = null;
		private int m_iL0;
		private int m_iL1;
		private int m_iL2;
		private int m_iL3;
		private int m_iL4;
		private short m_shMenuInsertionOptions;
        private short m_shDefaultMenuInsertionBehavior;
        //initialize default drag location to Insert At Selection -
        //16 (from Architect.Segment.InsertionLocations)
        private short m_shDefaultDragLocation = 16;
        private short m_shDefaultDoubleClickLocation = 16;
        private short m_shDefaultDragBehavior;
        private short m_shDefaultDoubleClickBehavior;
        private string m_xHelpText;
        private string m_xChildSegmentIDs;
        private mpSegmentIntendedUses m_iIntendedUse;
        private bool m_bListInAuthorPrefsManager;
        #endregion
        #region *********************constructors*********************
        internal AdminSegmentDef():base(){}
		internal AdminSegmentDef(int iID):base()
		{
			base.SetID(iID);
		}
		#endregion
		#region *********************enumerations*********************
        /// <summary>
        /// provides choices for the intended use of a segment
        /// </summary>
        #endregion
        #region *********************properties*********************

        public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value, 255);}
		}

		public string DisplayName
		{
			get{return m_xDisplayName;}
			set{this.SetStringPropertyValue(ref m_xDisplayName, value, 255);}
		}

		public mpObjectTypes TypeID
		{
			get{return m_shTypeID;}
			set
			{
				if(m_shTypeID != value)
				{
					m_shTypeID = value;
					this.IsDirty = true;
				}
			}
		}

        public string XML
        {
            get
            {
                    return m_xXML;
            }
            set{this.SetStringPropertyValue(ref m_xXML, value, 0);}
        }
        
        /// <summary>
        /// returns true iff this segment definition
        /// is being edited by a content designer
        /// </summary>
        public bool EditedByContentDesigner
        {
            get
            {
                return LMP.Data.Application.User.IsContentDesigner && !LMP.Data.Application.AdminMode;
            }
        }

        public bool ContainsAuthorPreferences
        {
            get
            {
                string xXML = this.XML.ToUpper();
                return xXML.Contains("[AUTHORPREFERENCE_") || xXML.Contains("[LEADAUTHORPREFERENCE_") ||
                    xXML.Contains("[AUTHORPARENTPREFERENCE") || xXML.Contains("[LEADAUTHORPARENTPREFERENCE_");
            }
        }

		public int L0
		{
			get{return m_iL0;}
			set{this.SetIntPropertyValue(ref m_iL0, value);}
		}

		public int L1
		{
			get{return m_iL1;}
			set{this.SetIntPropertyValue(ref m_iL1, value);}
		}

		public int L2
		{
			get{return m_iL2;}
			set{this.SetIntPropertyValue(ref m_iL2, value);}
		}

		public int L3
		{
			get{return m_iL3;}
			set{this.SetIntPropertyValue(ref m_iL3, value);}
		}

		public int L4
		{
			get{return m_iL4;}
			set{this.SetIntPropertyValue(ref m_iL4, value);}
		}

		public short MenuInsertionOptions
		{
			get{return m_shMenuInsertionOptions;}
			set
			{
				if(m_shMenuInsertionOptions != value)
				{
					m_shMenuInsertionOptions = value;
					this.IsDirty = true;
				}
			}
		}

        public short DefaultMenuInsertionBehavior
        {
            get { return m_shDefaultMenuInsertionBehavior; }
            set
            {
                if (m_shDefaultMenuInsertionBehavior != value)
                {
                    m_shDefaultMenuInsertionBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDragLocation
        {
            get { return m_shDefaultDragLocation; }
            set
            {
                if (m_shDefaultDragLocation != value)
                {
                    m_shDefaultDragLocation = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDragBehavior
        {
            get { return m_shDefaultDragBehavior; }
            set
            {
                if (m_shDefaultDragBehavior != value)
                {
                    m_shDefaultDragBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDoubleClickLocation
        {
            get { return m_shDefaultDoubleClickLocation; }
            set
            {
                if (m_shDefaultDoubleClickLocation != value)
                {
                    m_shDefaultDoubleClickLocation = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDoubleClickBehavior
        {
            get { return m_shDefaultDoubleClickBehavior; }
            set
            {
                if (m_shDefaultDoubleClickBehavior != value)
                {
                    m_shDefaultDoubleClickBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

		public string HelpText
		{
			get{return m_xHelpText;}
			set{this.SetStringPropertyValue(ref m_xHelpText, value, 0);}
		}
        public string ChildSegmentIDs
        {
            get { return m_xChildSegmentIDs; }
            set { this.SetStringPropertyValue(ref m_xChildSegmentIDs, value, 255); }
        }
        public mpSegmentIntendedUses IntendedUse
        {
            get { return m_iIntendedUse; }
            set { m_iIntendedUse = value; }
        }
        public bool ListInAuthorPrefsManager
        {
            get { return m_bListInAuthorPrefsManager; }
            set { m_bListInAuthorPrefsManager = value; }
        }
        /// <summary>
        /// returns the bounding object type used by the segment definition
        /// </summary>
        public LMP.Data.mpFileFormats BoundingObjectType
        {
            get { return LMP.Data.Application.GetBoundingObjectType(this.XML); }
        }

        //these properties are used to allow an admin segment def
        //that was created from a user segment def (ie a segment
        //that was created by a content designer) to be saved
        //back as a user segment def.  without them we
        //don't know a)that the segment needs to be saved
        //back to a user segment, and b)what the user ID
        //of the segment needs to be -

        //gets/sets the ID1 of the user segment that was
        //used to create this admin segment
        internal int UserSegID1 { get; set; }

        //gets/sets the ID2 of the user segment that was
        //used to create this admin segment
        internal int UserSegID2 { get; set; }

        public bool IsPaperType
        {
            get { return this.TypeID == mpObjectTypes.Letterhead || this.TypeID == mpObjectTypes.PleadingPaper; }
        }

        public bool IsTableCollectionItemType
        {
            get
            {
                return this.TypeID == mpObjectTypes.PleadingCaption ||
                    this.TypeID == mpObjectTypes.PleadingCounsel ||
                    this.TypeID == mpObjectTypes.PleadingSignature ||
                    this.TypeID == mpObjectTypes.CollectionTableItem ||
                    this.TypeID == mpObjectTypes.AgreementSignature ||
                    this.TypeID == mpObjectTypes.LetterSignature;
            }
        }
        public bool IsTableCollectionType
        {
            get
            {
                return this.TypeID == mpObjectTypes.PleadingCaptions ||
                    this.TypeID == mpObjectTypes.PleadingCounsels ||
                    this.TypeID == mpObjectTypes.PleadingSignatures ||
                    this.TypeID == mpObjectTypes.CollectionTable ||
                    this.TypeID == mpObjectTypes.AgreementSignatures ||
                    this.TypeID == mpObjectTypes.LetterSignatures;
            }
        }

        #endregion
		#region *********************methods*********************
        /// <summary>
        /// returns an AdminSegmentDef containing
        /// the same values as this UserSegmentDef -
        /// sets ID of AdminSegmentDef if supplied
        /// value is non-zero
        /// </summary>
        /// <returns></returns>
        public static AdminSegmentDef CreateFromUserSegment(UserSegmentDef oUserDef, int iID, bool bAssignUserSegmentDefID)
        {
            AdminSegmentDef oAdminDef = new AdminSegmentDef();
            if (iID != 0)
            {
                oAdminDef.SetID(iID);
                oAdminDef.IsPersisted = true;
            }
            else if(bAssignUserSegmentDefID)
            {
                int iUserID1;
                int iUserID2;

                LMP.Data.Application.SplitID(oUserDef.ID, out iUserID1, out iUserID2);

                oAdminDef.UserSegID1 = iUserID1;
                oAdminDef.UserSegID2 = iUserID2;
            }

            oAdminDef.IntendedUse = oUserDef.IntendedUse;
            oAdminDef.ChildSegmentIDs = oUserDef.ChildSegmentIDs;
            oAdminDef.DefaultDoubleClickBehavior = oUserDef.DefaultDoubleClickBehavior;
            oAdminDef.DefaultDoubleClickLocation = oUserDef.DefaultDoubleClickLocation;
            oAdminDef.DefaultDragBehavior = oUserDef.DefaultDragBehavior;
            oAdminDef.DefaultDragLocation = oUserDef.DefaultDragLocation;
            oAdminDef.DefaultMenuInsertionBehavior = oUserDef.DefaultMenuInsertionBehavior;
            oAdminDef.DisplayName = oUserDef.DisplayName;
            oAdminDef.HelpText = oUserDef.HelpText;
            oAdminDef.L0 = oUserDef.L0;
            oAdminDef.L1 = oUserDef.L1;
            oAdminDef.L2 = oUserDef.L2;
            oAdminDef.L3 = oUserDef.L3;
            oAdminDef.L4 = oUserDef.L4;
            oAdminDef.MenuInsertionOptions = oUserDef.MenuInsertionOptions;
            oAdminDef.Name = oUserDef.Name;
            oAdminDef.TypeID = oUserDef.TypeID == mpObjectTypes.UserSegment ? mpObjectTypes.Architect : oUserDef.TypeID;
            oAdminDef.XML = oUserDef.XML;

            return oAdminDef;
        }
        /// <summary>
        /// returns an AdminSegmentDef containing
        /// the same values as this UserSegmentDef -
        /// sets ID of AdminSegmentDef if supplied
        /// value is non-zero
        /// </summary>
        /// <returns></returns>
        public  UserSegmentDef ConvertToUserSegment()
        {
            UserSegmentDef oUserDef = new UserSegmentDef();
            if (this.UserSegID1 != 0 && this.UserSegID2!=0)
            {
                oUserDef.SetID(this.UserSegID1.ToString() + "." + this.UserSegID2.ToString());
                oUserDef.IsPersisted = true;
            }

            oUserDef.OwnerID = LMP.Data.Application.User.ID;
            oUserDef.IntendedUse = this.IntendedUse;
            oUserDef.ChildSegmentIDs = this.ChildSegmentIDs;
            oUserDef.DefaultDoubleClickBehavior = this.DefaultDoubleClickBehavior;
            oUserDef.DefaultDoubleClickLocation = this.DefaultDoubleClickLocation;
            oUserDef.DefaultDragBehavior = this.DefaultDragBehavior;
            oUserDef.DefaultDragLocation = this.DefaultDragLocation;
            oUserDef.DefaultMenuInsertionBehavior = this.DefaultMenuInsertionBehavior;
            oUserDef.DisplayName = this.DisplayName;
            oUserDef.HelpText = this.HelpText;
            oUserDef.L0 = this.L0;
            oUserDef.L1 = this.L1;
            oUserDef.L2 = this.L2;
            oUserDef.L3 = this.L3;
            oUserDef.L4 = this.L4;
            oUserDef.MenuInsertionOptions = this.MenuInsertionOptions;
            oUserDef.Name = this.Name;
            oUserDef.TypeID = this.TypeID == mpObjectTypes.UserSegment ? mpObjectTypes.Architect : oUserDef.TypeID;

            string xXML = this.XML;
            oUserDef.XML = String.GetSegmentXmlFromDefString(xXML);

            return oUserDef;
        }
        /// <summary>
        /// saves this admin segment as a user segment
        /// </summary>
        public UserSegmentDef SaveAsUserSegmentDef(string xName, string xDisplayName)
        {
            int iID1;
            int iID2;
            bool bIsPersisted = false;

            if (this.UserSegID1 == 0 || this.UserSegID2 == 0)
            {
                LMP.Data.Application.GetNewUserDataItemID(out iID1, out iID2);

                if (this.UserSegID1 != 0)
                    iID1 = this.ID;
            }
            else
            {
                iID1 = this.UserSegID1;
                iID2 = this.UserSegID2;
                bIsPersisted = true;
            }

            UserSegmentDefs oDefs = new UserSegmentDefs();
            UserSegmentDef oUserDef = new UserSegmentDef(iID1, iID2);

            oUserDef.IsPersisted = bIsPersisted;
            oUserDef.OwnerID = LMP.Data.Application.User.ID;
            oUserDef.ChildSegmentIDs = this.ChildSegmentIDs;
            oUserDef.DefaultDoubleClickBehavior = this.DefaultDoubleClickBehavior;
            oUserDef.DefaultDoubleClickLocation = this.DefaultDoubleClickLocation;
            oUserDef.DefaultDragBehavior = this.DefaultDragBehavior;
            oUserDef.DefaultDragLocation = this.DefaultDragLocation;
            oUserDef.DefaultMenuInsertionBehavior = this.DefaultMenuInsertionBehavior;
            oUserDef.DisplayName = xDisplayName;
            oUserDef.HelpText = this.HelpText;
            oUserDef.L0 = this.L0;
            oUserDef.L1 = this.L1;
            oUserDef.L2 = this.L2;
            oUserDef.L3 = this.L3;
            oUserDef.L4 = this.L4;
            oUserDef.MenuInsertionOptions = this.MenuInsertionOptions;
            oUserDef.Name = xName;
            oUserDef.OwnerID = LMP.Data.Application.User.ID;
            oUserDef.TypeID = this.TypeID;

            string xXML = this.XML;
            oUserDef.XML = String.GetSegmentXmlFromDefString(xXML);

            oUserDef.IntendedUse = this.IntendedUse;

            oDefs.Save((StringIDSimpleDataItem)oUserDef);

            oUserDef.XML = String.ReplaceSegmentXML(oUserDef.XML, this.ID.ToString(), oUserDef.ID,
                this.DisplayName, oUserDef.DisplayName, this.Name, oUserDef.Name, "0");

            oDefs.Save((StringIDSimpleDataItem)oUserDef);

            //GLOG item #4475 - dcf
            if (KeySet.Exists(mpKeySetTypes.AuthorPref, this.ID.ToString(), 
                ForteConstants.mpFirmRecordID.ToString()) &&
                !KeySet.Exists(mpKeySetTypes.AuthorPref,
                iID1.ToString() + "." + iID2.ToString(), LMP.Data.Application.User.ID.ToString()))
            {
                //create firm keyset record
                LMP.Data.Application.CopyFirmKeySetsForObjectID(
                    this.ID, ForteConstants.mpFirmRecordID, iID1, iID2);
            }

            return oUserDef;
        }
        public static AdminSegmentDef FromString(string xDef)
        {
            if (string.IsNullOrEmpty(xDef))
                return null;

            string[] aSeps = new string[]{LMP.Data.ForteConstants.mpDataFileFieldSep};
            string[] aDef = xDef.Split(aSeps, StringSplitOptions.None);
            AdminSegmentDef oDef = AdminSegmentDefs.GetDefFromValues(aDef);

            return oDef;
        }
        
        /// <summary>
        /// exports this data item to a .mpd file
        /// </summary>
        public override void Export(string xFileName, bool bOverwriteExisting)
        {
            const string EXPORT_REG_VALUE_NAME = "SegmentExportPath"; //GLOG 6647
            DialogResult iRes = DialogResult.OK;

            if (string.IsNullOrEmpty(xFileName))
            {
                //prompt for filename/location
                using (SaveFileDialog oDlg = new SaveFileDialog())
                {
                    oDlg.Title = LMP.Resources.GetLangString("Lbl_ExportItem");
                    oDlg.Filter = LMP.ComponentProperties.ProductName + " Data File (*.mpd)|*.mpd";
                    //GLOG 4844: Remove any illegal filename characters
                    oDlg.FileName = LMP.String.RemoveInvalidFileNameCharacters(this.DisplayName) + ".mpd";
                    //GLOG 6647: Get last selected path from registry
                    string xPath = LMP.Registry.GetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME);
                    //If invalid or not set, default to Writable Data location
                    if (string.IsNullOrEmpty(xPath) || !Directory.Exists(xPath))
                        xPath = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
                    oDlg.InitialDirectory = xPath;
                    oDlg.CheckPathExists = true;

                    iRes = oDlg.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    xFileName = oDlg.FileName;
                    //GLOG 6647: Get Path of selected file
                    xPath = Path.GetDirectoryName(xFileName);
                    //GLOG 6647: Save selected path as new default
                    if (iRes == DialogResult.OK)
                        LMP.Registry.SetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME, xPath);
                }
            }

            if (iRes == DialogResult.OK)
            {
                if (bOverwriteExisting)
                {
                    FileStream oFS = File.Create(xFileName);
                    oFS.Close();
                    oFS.Dispose();
                }

                if (this.ChildSegmentIDs != "")
                {
                    //write all non-admin child segments to file
                    string xChildIDs = this.ChildSegmentIDs;
                    string[] aChildIDs = xChildIDs.Split('|');

                    foreach (string xID in aChildIDs)
                    {
                        //get the segment if it's a user segment
                        if (xID.IndexOf('.') > -1 && !xID.EndsWith(".0"))
                        {
                            //get the UserSegment
                            UserSegmentDefs oDefs = new UserSegmentDefs(
                                mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                            UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                            //export it to the same file
                            oDef.Export(xFileName, false);
                        }
                        else
                        {
                            string xChildID = xID;
                            //GLOG 6966: Make sure string can be parsed as a Double even if
                            //the Decimal separator is something other than '.' in Regional Settings
                            if (xChildID.EndsWith(".0"))
                            {
                                xChildID = xChildID.Replace(".0", "");
                            }
                            //child is an admin segment
                            AdminSegmentDefs oAdminDefs = new AdminSegmentDefs();
                            AdminSegmentDef oDef = (AdminSegmentDef)oAdminDefs.ItemFromID((int)double.Parse(xChildID)); //GLOG 6966

                            oDef.Export(xFileName, false);
                        }
                    }
                }

                System.Type oType = this.GetType();
                string xContent = oType.Name + LMP.Data.ForteConstants.mpDataFileFieldSep + 
                    this.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep) + 
                    LMP.Data.ForteConstants.mpDataFileEntitySep;

                //append segment to file
                File.AppendAllText(xFileName, xContent);

                //add all segment assignments
                Assignments oAssignments = new Assignments(this.TypeID, this.ID, 0);
                File.AppendAllText(xFileName, "Assignments" + ForteConstants.mpDataFileFieldSep +
                    oAssignments.ToString("|") + ForteConstants.mpDataFileEntitySep);

                //GLOG 6148: Get all Firm, Office or Group Author Preferences Keysets records for all languages
                string xPreferences = "";
                string xPrefSql = "SELECT [OwnerID1], [Values], [Culture] FROM Keysets WHERE ScopeID1=" + this.ID
                    + " AND OwnerID2=" + ForteConstants.mpFirmRecordID.ToString() + " AND [Type]=" + ((int)mpKeySetTypes.AuthorPref).ToString();

                System.Collections.ArrayList alPrefs = LMP.Data.SimpleDataCollection.GetArray(xPrefSql);
                
                for (int i = 0; i < alPrefs.Count; i++)
                {
                    string xKeyset = "";
                    object[] oKeyset = (object[])alPrefs[i];
                    for (int o = 0; o < oKeyset.GetLength(0); o++)
                    {
                        if (oKeyset[o] is int)
                            xKeyset += ((int)oKeyset[o]).ToString() + StringArray.mpEndOfSubValue;
                        else
                            xKeyset += (string)oKeyset[o] + StringArray.mpEndOfSubValue;

                    }
                    xKeyset = xKeyset.TrimEnd(StringArray.mpEndOfSubValue);
                    xPreferences += xKeyset + StringArray.mpEndOfSubField;
                }
                xPreferences = xPreferences.TrimEnd(StringArray.mpEndOfSubField);
                if (xPreferences != "")
                {
                    File.AppendAllText(xFileName, "Preferences" + ForteConstants.mpDataFileFieldSep  + this.ID + ForteConstants.mpDataFileFieldSep +
                        xPreferences + ForteConstants.mpDataFileEntitySep);
                }
            }
        }
        
		#endregion
		#region *********************TranslatableDataItem members*********************
		internal override string IDFieldName
		{
			get{return "ID";}
		}
	
		internal override string TableName
		{
			get{return "Segments";}
		}
	
		internal override byte ValuesCount
		{
			//3 values - Display Name, Name, Help Text
			get{return 3;}
		}
	
		internal override object[] GetAddQueryParameters()
		{
			object[] oVals = new object[20];
            oVals[0] = this.DisplayName;
			oVals[1] = this.Name;
			oVals[2] = this.TranslationID;
			oVals[3] = this.TypeID;
			oVals[4] = this.XML;
			oVals[5] = this.L0;
			oVals[6] = this.L1;
			oVals[7] = this.L2;
			oVals[8] = this.L3;
			oVals[9] = this.L4;
			oVals[10] = this.MenuInsertionOptions;
            oVals[11] = this.DefaultMenuInsertionBehavior;
            oVals[12] = this.DefaultDragLocation;
            oVals[13] = this.DefaultDragBehavior;
            oVals[14] = this.DefaultDoubleClickLocation;
            oVals[15] = this.DefaultDoubleClickBehavior;
            oVals[16] = this.HelpText;
            oVals[17] = this.ChildSegmentIDs;
            oVals[18] = this.IntendedUse;
            oVals[19] = Application.GetCurrentEditTime();

			return oVals;
		}
        internal override object[] GetUpdateQueryParameters()
        {
            object[] oVals = new object[21];
            oVals[0] = this.ID;
            oVals[1] = this.DisplayName;
            oVals[2] = this.Name;
            oVals[3] = this.TranslationID;
            oVals[4] = this.TypeID;
            //we use the private var here because the XML property
            //only returns either the production xml or the non-production
            //xml.  but we need to save both, which is stored in the
            oVals[5] = m_xXML;
            oVals[6] = this.L0;
            oVals[7] = this.L1;
            oVals[8] = this.L2;
            oVals[9] = this.L3;
            oVals[10] = this.L4;
            oVals[11] = this.MenuInsertionOptions;
            oVals[12] = this.DefaultMenuInsertionBehavior;
            oVals[13] = this.DefaultDragLocation;
            oVals[14] = this.DefaultDragBehavior;
            oVals[15] = this.DefaultDoubleClickLocation;
            oVals[16] = this.DefaultDoubleClickBehavior;
            oVals[17] = this.HelpText;
            oVals[18] = this.ChildSegmentIDs;
            oVals[19] = this.IntendedUse;
            oVals[20] = Application.GetCurrentEditTime();

            return oVals;
        }
	
		internal override bool IsValid()
		{
			//always return true, as no fields are required
			return true;
		}
	
        /// <summary>
        /// returns true iff the definition is for external content
        /// </summary>
        public bool IsExternalContent
        {
            get
            {
                string xFile = this.XML;
                System.Xml.XmlDocument oDoc = new System.Xml.XmlDocument();
                try
                {
                    oDoc.LoadXml(this.XML);
                }
                catch
                {
                    return true;
                }
                return false;
            }
        }
		public override object[] ToArray()
		{
			object[] oVals = new object[20];
			oVals[0] = this.ID;
            oVals[1] = this.DisplayName;
			oVals[2] = this.Name;
			oVals[3] = this.TranslationID;
			oVals[4] = this.TypeID;
			oVals[5] = this.XML;
			oVals[6] = this.L0;
			oVals[7] = this.L1;
			oVals[8] = this.L2;
			oVals[9] = this.L3;
			oVals[10] = this.L4;
            oVals[11] = this.MenuInsertionOptions;
            oVals[12] = this.DefaultMenuInsertionBehavior;
            oVals[13] = this.DefaultDragLocation;
            oVals[14] = this.DefaultDragBehavior;
            oVals[15] = this.DefaultDoubleClickLocation;
            oVals[16] = this.DefaultDoubleClickBehavior;
            oVals[17] = this.HelpText;
            oVals[18] = this.ChildSegmentIDs;
            oVals[19] = this.IntendedUse;

			return oVals;
		}
		#endregion
		#region *********************private functions*********************
		#endregion
	}

	/// contains the methods and properties that manage the
	/// collection of MacPac object types - derived from TranslatableDataCollection -
	/// created by Daniel Fisherman - 09/04
	public sealed class AdminSegmentDefs: TranslatableDataCollection
	{
		private LMP.Data.LongIDUpdateObjectDelegate m_oDel = null;
		string m_xPersonIDFilter = null;
		mpObjectTypes m_shTypeIDFilter = 0;
        private int m_iL0Filter = 0;
        private int m_iL1Filter = 0;
        private int m_iL2Filter = 0;
        private int m_iL3Filter = 0;
        private int m_iL4Filter = 0;
        private bool m_bOnlyThoseAvailableToUser;

		#region *********************constructors*********************
		public AdminSegmentDefs(): base(){}
        public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID,
            int iL0, int iL1, int iL2, int iL3, int iL4, bool bOnlyThoseAvailableToUser)
        {
            this.SetFilter(shTypeID, xPersonID, iL0, iL1, iL2, iL3, iL4, bOnlyThoseAvailableToUser);
        }

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID, 
			int iL0, int iL1, int iL2, int iL3, int iL4): base()
		{
			this.SetFilter(shTypeID, xPersonID, iL0, iL1, iL2, iL3, iL4, false);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID, 
			int iL0, int iL1, int iL2, int iL3): base()
		{
			this.SetFilter(shTypeID, xPersonID, iL0, iL1, iL2, iL3);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID, 
			int iL0, int iL1, int iL2): base()
		{
			this.SetFilter(shTypeID, xPersonID, iL0, iL1, iL2);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID, 
			int iL0, int iL1): base()
		{
			this.SetFilter(shTypeID, xPersonID, iL0, iL1);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID, 
			int iL0): base()
		{
			this.SetFilter(shTypeID, xPersonID, iL0);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID, string xPersonID): base()
		{
			this.SetFilter(shTypeID, xPersonID);
		}

		public AdminSegmentDefs(mpObjectTypes shTypeID): base()
		{
			this.SetFilter(shTypeID);
		}
		#endregion

		#region *********************properties*********************
		public string PersonIDFilter
		{
			get
			{
				return m_xPersonIDFilter;
			}
		}
		public mpObjectTypes TypeIDFilter
		{
			get
			{
				return m_shTypeIDFilter;
			}
		}
		public int L0Filter
		{
			get{return m_iL0Filter;}
		}

		public int L1Filter
		{
			get{return m_iL1Filter;}
		}

		public int L2Filter
		{
			get{return m_iL2Filter;}
		}

		public int L3Filter
		{
			get{return m_iL3Filter;}
		}

		public int L4Filter
		{
			get{return m_iL4Filter;}
		}

		#endregion

		#region *********************methods*********************
		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		/// <param name="iL1">level 1 id on which to filter</param>
		/// <param name="iL2">level 2 id on which to filter</param>
		/// <param name="iL3">level 3 id on which to filter</param>
		/// <param name="iL4">level 4 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID, int iL0, 
			int iL1, int iL2, int iL3, int iL4, bool bOnlyThoseAvailableToUser)
		{
			m_shTypeIDFilter = shTypeID;
			m_xPersonIDFilter = xPersonID;
			m_iL0Filter = iL0;
			m_iL1Filter = iL1;
			m_iL2Filter = iL2;
			m_iL3Filter = iL3;
			m_iL4Filter = iL4;
            m_bOnlyThoseAvailableToUser = bOnlyThoseAvailableToUser;
            m_oArray = null;
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		/// <param name="iL1">level 1 id on which to filter</param>
		/// <param name="iL2">level 2 id on which to filter</param>
		/// <param name="iL3">level 3 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID, int iL0, 
			int iL1, int iL2, int iL3)
		{
			SetFilter(shTypeID, xPersonID, iL0, iL1, iL2, iL3, 0, false);
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		/// <param name="iL1">level 1 id on which to filter</param>
		/// <param name="iL2">level 2 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID, int iL0, 
			int iL1, int iL2)
		{
			SetFilter(shTypeID, xPersonID, iL0, iL1, iL2, 0, 0, false);
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		/// <param name="iL1">level 1 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID, int iL0, 
			int iL1)
		{
			SetFilter(shTypeID, xPersonID, iL0, iL1, 0, 0, 0, false);
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID, int iL0)
		{
			SetFilter(shTypeID, xPersonID, iL0, 0, 0, 0, 0, false);
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		/// <param name="xPersonID">id of the user on which to filter</param>
		/// <param name="iL0">level 0 id on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID, string xPersonID)
		{
			SetFilter(shTypeID, xPersonID, 0, 0, 0, 0, 0, false);
		}

		/// <summary>
		/// sets the filter to the specified values
		/// </summary>
		/// <param name="iObjectID">id of the object on which to filter</param>
		public void SetFilter(mpObjectTypes shTypeID)
		{
			SetFilter(shTypeID, "", 0, 0, 0, 0, 0, false);
		}

        public override void Save(LongIDSimpleDataItem oItem)
        {
            AdminSegmentDef oDef = (AdminSegmentDef)oItem;

            //if this segment was originally converted from
            //a user segment (ie the user segment ids are populated),
            //save as a user segment
            if (oDef.UserSegID1 != 0 && oDef.UserSegID2 != 0)
            {
                oDef.SaveAsUserSegmentDef(oDef.Name, oDef.DisplayName);
            }
            else
            {
                base.Save(oItem);
            }
        }
		/// <summary>
		/// returns the first object type def that matches the specified levels and object id
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <param name="iL1">level 1 criterion</param>
		/// <param name="iL2">level 2 criterion</param>
		/// <param name="iL3">level 3 criterion</param>
		/// <param name="iL4">level 4 criterion</param>
		/// <param name="shTypeID">object id criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, 
			int iL2, int iL3, int iL4, mpObjectTypes shTypeID)
		{
			object[] oParams = null;
			string xSprocName = null;
			
			Trace.WriteNameValuePairs("iL0", iL0,"iL1", iL1,
				"iL2", iL2,"iL3", iL3, "iL4", iL4);
  
			if((this.L0Filter != 0 && iL0 != 0 && this.L0Filter != iL0) ||
				(this.L1Filter != 0 && iL1 != 0 && this.L1Filter != iL1) ||
				(this.L2Filter != 0 && iL2 != 0 && this.L2Filter != iL2) ||
				(this.L3Filter != 0 && iL3 != 0 && this.L3Filter != iL3) ||
				(this.L4Filter != 0 && iL4 != 0 && this.L4Filter != iL1) ||
				(this.TypeIDFilter != 0 && shTypeID != 0 && this.TypeIDFilter != shTypeID))
			{
				//supplied filter is different from current 
				//filter on collection - return nothing
				return null;
			}
			
			//get resultset containing the filtered set of object type defs
			if(this.PersonIDFilter != "" && this.PersonIDFilter != null)
			{
				Person oPerson = LocalPersons.GetPersonFromID(this.PersonIDFilter);
				string xGroupIDs = oPerson.GetGroupIDs();
				
				//filter by personID
				if(Language.CultureID == Language.mpUSEnglishCultureID)
				{
					xSprocName = "spSegmentsByGroup";
					oParams = new object[7];

					//add group IDs
					oParams[6] = xGroupIDs;
				}
				else
				{
					oParams = new object[8];

					//add group IDs
					oParams[6] = xGroupIDs;

					//add culture filter
					oParams[7] = Language.CultureID;

					xSprocName = "spSegmentsByGroupTranslated";
				}
			}
			else
			{
				if(Language.CultureID == Language.mpUSEnglishCultureID)
				{
					oParams = new object[6];
					xSprocName = "spSegments";
				}
				else
				{
					oParams = new object[7];
					oParams[6] = Language.CultureID;
					xSprocName = "spSegmentsTranslated";
				}
			}

			//get filter params - use supplied unless empty, then collection filter value
			oParams[0] = shTypeID!=0 ? shTypeID : this.TypeIDFilter;
			oParams[1] = iL0!=0 ? iL0 : this.L0Filter;
			oParams[2] = iL1!=0 ? iL1 : this.L1Filter;
			oParams[3] = iL2!=0 ? iL2 : this.L2Filter;
			oParams[4] = iL3!=0 ? iL3 : this.L3Filter;
			oParams[5] = iL4!=0 ? iL4 : this.L4Filter;

			System.Data.OleDb.OleDbDataReader oReader = null;
			try
			{
				oReader = SimpleDataCollection.GetResultSet(xSprocName, true, oParams);
			
				if (oReader.HasRows)
				{
					//save current item if dirty and there are listeners 
					//requesting this - ideally, this should be called
					//after we know that the requested item exists - 
					//unfortunately, we can't save when a DataReader is open
					//so we do it here
					if(base.m_oItem != null && base.m_oItem.IsDirty)
					{
						SaveCurrentItemIfRequested();
					}

					oReader.Read();

					//get values at current row
					object[] oValues = new object[oReader.FieldCount];

					oReader.GetValues(oValues);


					//update object with values
					base.m_oItem = this.UpdateObjectDelegate(oValues);
					base.m_oItem.IsPersisted = true;
					base.m_oItem.IsDirty = false;
					return m_oItem;
				}
				else
					return null;
			}
			finally
			{
				oReader.Close();
                oReader.Dispose();
			}

		}

		/// <summary>
		/// returns the first object type def that matches the specified levels
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <param name="iL1">level 1 criterion</param>
		/// <param name="iL2">level 2 criterion</param>
		/// <param name="iL3">level 3 criterion</param>
		/// <param name="iL4">level 4 criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, 
			int iL2, int iL3, int iL4)
		{
			return ItemFromLevels(iL0, iL1, iL2, iL3, iL4, 0);
		}

		/// <summary>
		/// returns the first object type def that matches the specified levels
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <param name="iL1">level 1 criterion</param>
		/// <param name="iL2">level 2 criterion</param>
		/// <param name="iL3">level 3 criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, int iL2, int iL3)
		{
			return ItemFromLevels(iL0, iL1, iL2, iL3, 0, 0);
		}

		/// <summary>
		/// returns the first object type def that matches the specified levels
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <param name="iL1">level 1 criterion</param>
		/// <param name="iL2">level 2 criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, int iL2)
		{
			return ItemFromLevels(iL0, iL1, iL2, 0, 0, 0);
		}

		/// <summary>
		/// returns the first object type def that matches the specified levels
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <param name="iL1">level 1 criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1)
		{
			return ItemFromLevels(iL0, iL1, 0, 0, 0, 0);
		}

		/// <summary>
		/// returns the first object type def that matches the specified levels and object id
		/// </summary>
		/// <param name="iL0">level 0 criterion</param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0)
		{
			return ItemFromLevels(iL0, 0, 0, 0, 0, 0);
		}

        public static bool Exists(string xName)
        {
            return SimpleDataCollection.GetResultSet(
                "spSegmentIDFromName", true, new object[] { xName }).HasRows;
        }
        /// <summary>
        /// returns the ID of the admin segment with the specified name -
        /// returns 0 if no segment with the specified name exists
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public static int GetIDFromName(string xName)
        {
            object oID = SimpleDataCollection.GetScalar("spSegmentIDFromName", new object[] { xName });
            if (oID != null)
                return (int)oID;
            else
                return 0;
        }
		#endregion

		#region *********************TranslatableDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spSegmentsAdd";}
		}

		protected override string SelectSprocName
		{
			get
			{
				if(Language.CultureID == Language.mpUSEnglishCultureID)
				{
					//current culture is US English
                    if (m_bOnlyThoseAvailableToUser)
                    {
                        return "spSegmentsAvailableToUser";
                    }
					else if((m_xPersonIDFilter == null) || (m_xPersonIDFilter == ""))
						return "spSegments";
					else
						//return only those templates available 
						//to the person's groups
						return "spSegmentsByGroup";
				}
				else
				{
					//culture is not US English - translate
					if((m_xPersonIDFilter == null) || m_xPersonIDFilter == "")
						return "spSegmentsTranslated";
					else
					{
						//return only those templates available 
						//to the person's groups
						return "spSegmentsByGroupTranslated";
					}
				}
			}
		}

		protected override string DeleteSprocName
		{
			get{return "spSegmentsDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get
			{
				if(Language.CultureID == Language.mpUSEnglishCultureID)
					return "spSegmentsItemFromID";
				else
					return "spSegmentsTranslatedItemFromID";
			}
		}

		protected override string LastIDSprocName
		{
			get{return "spSegmentsLastID";}
		}

		protected override string CountSprocName
		{
			get
			{
				if((m_xPersonIDFilter == null) || (m_xPersonIDFilter == ""))
					return "spSegmentsCount";
				else
					//return only those templates available 
					//to the person's groups
					return "spSegmentsByGroupCount";
			}
		}

		protected override object[] SelectSprocParameters
		{
			get
			{
                if (m_bOnlyThoseAvailableToUser)
                {
                    //get params for spSegments sproc
                    return new object[] {this.TypeIDFilter, this.L0Filter, 
							this.L1Filter, this.L2Filter, this.L3Filter, this.L4Filter, this.PersonIDFilter};
                }
				if(this.PersonIDFilter != "" && this.PersonIDFilter != null)
				{
					//get group IDs from Person specified by filter
					Person oPerson = LocalPersons.GetPersonFromID(this.PersonIDFilter);
					string xGroupIDs = oPerson.GetGroupIDs();
				
					if(Language.CultureID == Language.mpUSEnglishCultureID)
					{
						//get params for spSegmentsByGroup sproc
						return new object[] {this.TypeIDFilter, this.L0Filter, 
							this.L1Filter, this.L2Filter, this.L3Filter, this.L4Filter, xGroupIDs};
					}
					else
					{
						//get params for spSegmentsByGroupTranslated sproc
						return new object[] {this.TypeIDFilter, this.L0Filter, 
							this.L1Filter, this.L2Filter, this.L3Filter, this.L4Filter, xGroupIDs, Language.CultureID};
					}
				}
				else
				{
					if(Language.CultureID == Language.mpUSEnglishCultureID)
					{
						//get params for spSegments sproc
						return new object[] {this.TypeIDFilter, this.L0Filter, 
							this.L1Filter, this.L2Filter, this.L3Filter, this.L4Filter};
					}
					else
					{
						//get params for spSegmentsByGroup sproc
						return new object[] {this.TypeIDFilter, this.L0Filter, 
							this.L1Filter, this.L2Filter, this.L3Filter, this.L4Filter, Language.CultureID};
					}
				}
			}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get
			{
				if(Language.CultureID == Language.mpUSEnglishCultureID)
					return null;
				else
					return new object[] {System.Threading.Thread.CurrentThread.CurrentUICulture.LCID};
			}
		}

		protected override object[] CountSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get{return "spSegmentsUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			if(m_shTypeIDFilter != 0)
			{
				//create object type def with the obj type 
				//filter used as the value for the obj type id
				AdminSegmentDef oDef = (AdminSegmentDef) this.Create(m_shTypeIDFilter);
				return oDef;
			}
			else
				//object type id is required for creation, and none is supplied -
				//alert
				throw new LMP.Exceptions.MethodExecutionException(
					LMP.Resources.GetLangString("Error_CreateRequiresSetFilter"));

		}

		public LongIDSimpleDataItem Create(mpObjectTypes shTypeID)
		{
			if(!Application.ObjectTypeExists(shTypeID))
			{
				//invalid object ID - alert
				throw new LMP.Exceptions.ObjectIDException(
					LMP.Resources.GetLangString("Error_InvalidObjectTypeID") + shTypeID);
			}
			
			//create new Object Type
			AdminSegmentDef oDef = new AdminSegmentDef();

			//assign object ID
			oDef.TypeID = shTypeID;

			return oDef;
		}

		#endregion
		
		#region *********************private functions*********************
		/// <summary>
		/// sets the person id filter
		/// </summary>
		/// <param name="xPersonID"></param>
		private void SetPersonIDFilter(string xPersonID)
		{
			if(m_xPersonIDFilter != xPersonID)
			{
				Trace.WriteNameValuePairs("xPersonID", xPersonID, 
					"m_xPersonIDFilter", m_xPersonIDFilter);
				
				//set to null if empty string
				if(xPersonID == "")
					xPersonID = null;

				if(xPersonID != null)
				{
					//alert if supplied ID is not the ID of a MacPac person
					if(!LocalPersons.Exists(xPersonID))
						throw new LMP.Exceptions.PersonIDException(
							LMP.Resources.GetLangString("Error_InvalidPersonID") + xPersonID);
				}

				//set filter var
				m_xPersonIDFilter = xPersonID;

				//clear internal array to force repopulation
				this.m_oArray = null;
			}
		}

		/// <summary>
		/// sets the object ID filter to the specified Object ID
		/// </summary>
		/// <param name="shTypeID"></param>
		private void SetTypeIDFilter(mpObjectTypes shTypeID)
		{
			if(m_shTypeIDFilter != shTypeID)
			{
				if(shTypeID != 0 && !Application.ObjectTypeExists(shTypeID))
					//object ID is invalid
					throw new LMP.Exceptions.ObjectIDException(
						LMP.Resources.GetLangString("Error_InvalidObjectID") + 
						shTypeID.ToString());
					
				//set object ID filter
				m_shTypeIDFilter = shTypeID;

				//clear internal array to force repopulation
				this.m_oArray = null;
			}
		}

		/// <summary>
		/// sets the levels filter
		/// </summary>
		/// <param name="iL0">level 0 filter</param>
		/// <param name="iL1">level 1 filter</param>
		/// <param name="iL2">level 2 filter</param>
		/// <param name="iL3">level 3 filter</param>
		/// <param name="iL4">level 4 filter</param>
		private void SetLevelsFilter(int iL0, int iL1, int iL2, int iL3, int iL4)
		{
			bool bChanged = false;

			if(m_iL0Filter != iL0)
			{
				m_iL0Filter = iL0;
				bChanged = true;
			}
		
			if(m_iL1Filter != iL1)
			{
				m_iL1Filter = iL1;
				bChanged = true;
			}

			if(m_iL2Filter != iL2)
			{
				m_iL2Filter = iL2;
				bChanged = true;
			}

			if(m_iL3Filter != iL3)
			{
				m_iL3Filter = iL3;
				bChanged = true;
			}

			if(m_iL4Filter != iL4)
			{
				m_iL4Filter = iL4;
				bChanged = true;
			}
		
			//clear internal array to force repopulation
			if(bChanged)
				this.m_oArray = null;
		}

		private string GetDefaultPropertyValues(mpObjectTypes shTypeID)
		{
			//run sproc that returns the default property values for the specified object
			object oPropValues = SimpleDataCollection.GetScalar(
				"spGetDefaultObjectPropertyValues", new object[]{(short) shTypeID});

			return (string) oPropValues;
		}

        internal static AdminSegmentDef GetDefFromValues(object[] oVals)
        {
            AdminSegmentDef oDef = new AdminSegmentDef((int)oVals[0]);
            oDef.DisplayName = oVals[1].ToString();
            oDef.Name = oVals[2].ToString();
            oDef.TranslationID = (oVals[3] == System.DBNull.Value ? 0 : (int)oVals[3]);
            oDef.TypeID = (oVals[4] == System.DBNull.Value ? 0 : (mpObjectTypes)oVals[4]);
            oDef.IntendedUse = (mpSegmentIntendedUses)(int.Parse(oVals[5].ToString()));

            string xXML = oVals[6].ToString();
            oDef.XML = String.GetSegmentXmlFromDefString(xXML);

            oDef.L0 = (oVals[7] == System.DBNull.Value ? 0 : (int)oVals[7]);
            oDef.L1 = (oVals[8] == System.DBNull.Value ? 0 : (int)oVals[8]);
            oDef.L2 = (oVals[9] == System.DBNull.Value ? 0 : (int)oVals[9]);
            oDef.L3 = (oVals[10] == System.DBNull.Value ? 0 : (int)oVals[10]);
            oDef.L4 = (oVals[11] == System.DBNull.Value ? 0 : (int)oVals[11]);
            oDef.MenuInsertionOptions = (oVals[12] == System.DBNull.Value ? (short)0 : (short)oVals[12]);
            oDef.DefaultMenuInsertionBehavior = (oVals[13] == System.DBNull.Value ? (short)0 : (short)oVals[13]);
            oDef.DefaultDragLocation = (oVals[14] == System.DBNull.Value ? (short)0 : (short)oVals[14]);
            oDef.DefaultDragBehavior = (oVals[15] == System.DBNull.Value ? (short)0 : (short)oVals[15]);
            oDef.DefaultDoubleClickLocation = (oVals[16] == System.DBNull.Value ? (short)0 : (short)oVals[16]);
            oDef.DefaultDoubleClickBehavior = (oVals[17] == System.DBNull.Value ? (short)0 : (short)oVals[17]);
            oDef.HelpText = oVals[18].ToString();
            oDef.ChildSegmentIDs = oVals[19].ToString();
            return oDef;
        }

        internal static AdminSegmentDef GetDefFromValues(string[] oVals)
        {
            AdminSegmentDef oDef = new AdminSegmentDef(int.Parse(oVals[0]));
            oDef.DisplayName = oVals[1];
            oDef.Name = oVals[2];
            oDef.TranslationID = int.Parse(oVals[3]);
            oDef.TypeID = (mpObjectTypes)(Enum.Parse(typeof(mpObjectTypes),oVals[4]));
            oDef.IntendedUse = (mpSegmentIntendedUses)(Enum.Parse(typeof(mpSegmentIntendedUses),oVals[19]));

            string xXML = oVals[5];
            oDef.XML = String.GetSegmentXmlFromDefString(xXML);

            oDef.L0 = int.Parse(oVals[6]);
            oDef.L1 = int.Parse(oVals[7]);
            oDef.L2 = int.Parse(oVals[8]);
            oDef.L3 = int.Parse(oVals[9]);
            oDef.L4 = int.Parse(oVals[10]);
            oDef.MenuInsertionOptions = short.Parse(oVals[11]);
            oDef.DefaultMenuInsertionBehavior = short.Parse(oVals[12]);
            oDef.DefaultDragLocation = short.Parse(oVals[13]);
            oDef.DefaultDragBehavior = short.Parse(oVals[14]);
            oDef.DefaultDoubleClickLocation = short.Parse(oVals[15]);
            oDef.DefaultDoubleClickBehavior = short.Parse(oVals[16]);
            oDef.HelpText = oVals[17];
            oDef.ChildSegmentIDs = oVals[18];
            oDef.IsPersisted = true;
            oDef.IsDirty = true;
            return oDef;
        }

        private LongIDSimpleDataItem UpdateObject(object[] oVals)
        {
            return GetDefFromValues(oVals);
        }
		#endregion
	}
}