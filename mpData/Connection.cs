using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Data.Sql;

namespace LMP.Data
{
    public enum mpSQLServerAuthentication
    {
        WindowsNT = 0,
        SQLServer = 1,
    }
    /// <summary>
	/// contains the methods and properties that define a local MacPac connection
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class LocalConnection
	{
		private static OleDbConnection m_oCnn = null;
        private static bool m_bAdminMode = false;
        private static StateChangeEventHandler m_oStateChangeHandler;
        private static string m_xConnectionProvider; //GLOG 8500

		#region *********************static members*********************
		/// <summary>
		/// returns the local database connection
		/// </summary>
		public static OleDbConnection ConnectionObject {get{return m_oCnn;}}
        /// <summary>
        /// returns the connection provider for either 64 or 32-bit
        /// </summary>
        /// GLOG : 8500 : jsw
        public static string ConnectionProvider
        {
            get { return m_xConnectionProvider; }
            set { m_xConnectionProvider = value; }
        }
        //GLOG : 8500 : jsw
        public static void SetConnectionProvider()
        {
            if (Registry.Is64Bit())
                LocalConnection.ConnectionProvider = "Microsoft.ACE.OLEDB.12.0";
            else
                LocalConnection.ConnectionProvider = "Microsoft.Jet.OLEDB.4.0";

        }
		/// <summary>
		/// connects to the mpLocal database
		/// </summary>
        public static void ConnectToDB(bool bAdminMode)
        {
            ConnectToDB(bAdminMode, false);
        }
		
        public static void ConnectToDB(bool bAdminMode, bool bLocalPeopleMode)
		{
			DateTime t0 = DateTime.Now;

            string xCnStr = "";
            string xDir = Application.WritableDBDirectory; //JTS 3/28/13
            //GLOG 8220: ForteLocalPublic.mdb and ForteLocalPeople.mdb will be in Public Data folder
            if ((bAdminMode || bLocalPeopleMode) && LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                xDir = Application.PublicDataDirectory;
            }
            string xPrintableCnStr = "";
            string xWorkgroupFile = "";

            if (File.Exists(xDir + "\\Forte.mdw"))
                xWorkgroupFile = xDir + "\\Forte.mdw";
            else if (File.Exists(Application.PublicDataDirectory + "\\Forte.mdw"))
                xWorkgroupFile = Application.PublicDataDirectory + "\\Forte.mdw"; //JTS 3/28/13

            if (xWorkgroupFile == "")
            {
                //GLOG 6655: Forte.mdw not found
                throw new LMP.Exceptions.DBConnectionException(
                    string.Format(Resources.GetLangString("Error_WorkgroupFileNotFound"), Application.PublicDataDirectory));
            }

            //GLOG : 8500 : jsw
            SetConnectionProvider();

            if (bLocalPeopleMode)
            {
                LMP.Data.Application.WriteDBLog("Login as Admin", xDir + @"\" + LMP.Data.Application.PeopleDBName); //GLOG 7546
                //connect to local admin db
                xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDir + @"\" + LMP.Data.Application.PeopleDBName + ";Jet OLEDB:System database=" +
                    xWorkgroupFile + ";User ID=mp10ProgrammaticUser;Password=Fish4Mill"; //WorkgroupFile ID = 4437
                xPrintableCnStr = xCnStr.Replace("Fish4Mill", "********");
            }
            else if (bAdminMode)
            {
                LMP.Data.Application.WriteDBLog("Login as Admin", xDir + @"\" + LMP.Data.Application.AdminDBName); //GLOG 7546
                //connect to local admin db
                xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDir + @"\" + LMP.Data.Application.AdminDBName + ";Jet OLEDB:System database=" +
                    xWorkgroupFile + ";User ID=mp10ProgrammaticUser;Password=Fish4Mill"; //WorkgroupFile ID = 4437
                xPrintableCnStr = xCnStr.Replace("Fish4Mill", "********");
            }
            else
            {
                LMP.Data.Application.WriteDBLog("Login as User", xDir + @"\" + LMP.Data.Application.UserDBName); //GLOG 7546

                if (!LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8316
                    LMP.Data.Application.RenameUserDBIfSpecified();

                //connect to local user db
                xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    xDir + @"\" + LMP.Data.Application.UserDBName + ";Jet OLEDB:System database=" +
                    xWorkgroupFile + ";User ID=mp10User;Password=Sackett@4437";
                xPrintableCnStr = xCnStr.Replace("Sackett@4437", "********");
            }

            Trace.WriteNameValuePairs("xCnStr", xPrintableCnStr);

            if (m_oCnn == null)
                m_oCnn = new OleDbConnection();

            //return if current connection is
            //the same as the connection requested
            if (m_oCnn.State == ConnectionState.Open && 
                xCnStr.Contains(m_oCnn.ConnectionString))
                return;

            if (m_oCnn.State == ConnectionState.Open)
            {
                //unsubscribe previous handler
                if (m_oStateChangeHandler != null)
                    m_oCnn.StateChange -= m_oStateChangeHandler;

                //close previous connection
                m_oCnn.Close();
            }

            //set new connection string
			m_oCnn.ConnectionString = xCnStr;

			//open connection
            try
            {
                m_oCnn.Open();
            }
            catch (System.Exception e)
            {
                throw new LMP.Exceptions.DBConnectionException(
                    Resources.GetLangString("Error_CouldNotConnectToDB") + xPrintableCnStr, e);
            }

            if (!bLocalPeopleMode)
            {
                if (LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220 (dm)
                {
                    //this is a fully local version of MacPac, which has
                    //a separate public db file - refresh the link if necessary
                    RefreshPublicDBLinksIfNecessary(m_oCnn, xWorkgroupFile, !bAdminMode); //GLOG 8220
                }
                else
                {
                    //this version of mp is configured to use a network db -
                    //there might be a separate segments db - refreh the link if necessary
                    RefreshSegmentsLinkIfNecessary(m_oCnn, xWorkgroupFile);
                }
            }
            Trace.WriteNameValuePairs("oCn.State", m_oCnn.State);

            //subscribe new handler to connection StateChange event
            m_oStateChangeHandler =
                new StateChangeEventHandler(m_oCnn_StateChange);
            m_oCnn.StateChange += m_oStateChangeHandler;

            LMP.Benchmarks.Print(t0);
		}

        //GLOG 8220
        /// <summary>
        /// refreshes the links in ForteLocal.mdb to the tables in ForteLocalPublic.mdb
        /// </summary>
        /// <param name="oCn"></param>
        /// <param name="xWorkgroupFile"></param>
        public static void RefreshPublicDBLinksIfNecessary(OleDbConnection oCn, string xWorkgroupFile, bool bIsUserDB)
        {
            //GLOG 8220 (dm) - renamed db
            string xPublicDB = LMP.Data.Application.PublicDataDirectory + "\\ForteLocalPublic.mdb";
            string xPeopleDB = LMP.Data.Application.PublicDataDirectory + "\\ForteLocalPeople.mdb";

            if (!LMP.MacPac.MacPacImplementation.IsFullLocal || !File.Exists(xPublicDB))
                return;

            DateTime t0 = DateTime.Now;
            Trace.WriteNameValuePairs("oCn", oCn, "xWorkgroupFile", xWorkgroupFile);

            string xDBName = "";

            if (oCn == null)
            {
                if (bIsUserDB)
                {
                    xDBName = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName;
                }
                else
                {
                    xDBName = LMP.Data.Application.PublicDataDirectory + "\\" + LMP.Data.Application.AdminDBName;
                }

                using (oCn = new OleDbConnection(
                     @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                      xDBName + ";Jet OLEDB:System database=" +
                     LMP.Data.Application.PublicDataDirectory +
                     @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                {
                    //connect to admin db
                    oCn.Open();
                }

            }
            else
            {
                xDBName = oCn.DataSource;
            }

            string[] aPublicTables = { "Addresses", "AddressFormats", "Assignments", "AttyLicensesPublic", "Counties", "Countries", "Couriers", "Courts", 
                                         "ExternalConnections", "Folders", "GroupAssignments", "Jurisdictions0", "Jurisdictions1", 
                                         "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "KeysetsPublic", "Lists", "Offices", 
                                         "PeopleGroups", "PeoplePublic", "Reserved", "Restrictions", "Segments", "States", "Translations", "ValueSets" };
            
            if (!bIsUserDB)
            {
                aPublicTables = new string [] { "AttyLicensesPublic", "PeoplePublic" };
            }

            foreach (string xTable in aPublicTables)
            {
                OleDbCommand oCmd = new OleDbCommand("SELECT Top 1 * FROM " + xTable);
                oCmd.Connection = oCn;

                object o = null;

                try
                {
                    o = oCmd.ExecuteScalar();
                }
                catch { }

                if (o == null)
                {
                    //we need a connection using MacPacPersonnel to give us permission
                    //to delete and create queries - the regular logon has user permissions only
					//GLOG 8289
                    using (OleDbConnection oCnMpPersonnel = new OleDbConnection(
                            @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                            xDBName + ";Jet OLEDB:System database=" +
                            LMP.Data.Application.PublicDataDirectory +
                            @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                    {
                        oCnMpPersonnel.Open();

                        oCmd.Connection = oCnMpPersonnel;

                        //add query that selects all data in the
                        //corresponding table of the FortePublic.mdb

                        oCmd.CommandText = "DROP PROCEDURE " + xTable;

                        try
                        {
                            oCmd.ExecuteNonQuery();
                        }
                        catch (System.Exception oE)
                        { }

                        //source table name does not include "Public"
                        string xSourceName = xTable.Replace("Public", "");
                        string xCurDB = xPublicDB;
                        if (xSourceName == "People" || xSourceName == "AttyLicenses")
                            xCurDB = xPeopleDB;

                        oCmd.CommandText = "CREATE PROCEDURE " + xTable + " AS SELECT * FROM " + xSourceName + " IN '" + xCurDB + "';";
                        oCmd.ExecuteNonQuery();
                        oCnMpPersonnel.Close();
                    }
                }
            }

            LMP.Benchmarks.Print(t0);
        }
        
        /// <summary>
        /// refreshes the link to the segments table if the
        /// segments table is not available.
        /// </summary>
        /// <param name="oCn"></param>
        /// <param name="xWorkgroupFile"></param>
        public static void RefreshSegmentsLinkIfNecessary(OleDbConnection oCn, string xWorkgroupFile)
        {
            if (m_bAdminMode)
                return;

            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("oCn", oCn, "xWorkgroupFile", xWorkgroupFile);

            if (oCn == null)
            {
                using (oCn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                    LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName + ";Jet OLEDB:System database=" +  //JTS 3/28/13
                    LMP.Data.Application.PublicDataDirectory +
                    @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill")) //JTS 3/28/13
                {
                    //connect to admin db
                    oCn.Open();
                }
            }

            OleDbCommand oCmd = new OleDbCommand("SELECT Top 1 ID FROM Segments");
            oCmd.Connection = oCn;
            object o = null;

            try
            {
                o = oCmd.ExecuteScalar();
            }
            catch {}

            if (o == null)
            {
                //add Segments query that selects all data in the
                //Segments table of the Segments.mdb
                string xSegmentsDB = null;
                if (File.Exists(LMP.Data.Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb"))
                    //link to the db in the db directory - should be the case on the desktop
                    xSegmentsDB = LMP.Data.Application.ForteSegmentsDBDirectory + "\\ForteSegments.mdb";
                else
                    //link to the db in the data directory - should be the case on a virtual desktop
                    xSegmentsDB = LMP.Data.Application.PublicDataDirectory + "\\ForteSegments.mdb"; //GLOG 6655

                oCmd.CommandText = "DROP PROCEDURE Segments";

                try
                {
                    oCmd.ExecuteNonQuery();
                }
                catch { }

                oCmd.CommandText = "CREATE PROCEDURE Segments AS SELECT * FROM Segments IN '" + xSegmentsDB + "';";
                oCmd.ExecuteNonQuery();

                ////link needs to be refreshed or created
                //dao.DBEngineClass oDBEngine = new dao.DBEngineClass();

                //if (string.IsNullOrEmpty(xWorkgroupFile))
                //{
                //    xWorkgroupFile = LMP.Data.Application.DataDirectory + "\\Forte.mdw";
                //}

                //oDBEngine.SystemDB = xWorkgroupFile;
                //dao.Workspace oWSpace = oDBEngine.CreateWorkspace(
                //    "", "MacPacPersonnel", "fish4mill", dao.WorkspaceTypeEnum.dbUseJet);
                //dao.Database oDB = oWSpace.OpenDatabase(
                //    LMP.Data.Application.DBDirectory + "\\" + LMP.Data.Application.UserDBName, false, false, null);

                //dao.TableDef oTD = null;

                //try
                //{
                //    oTD = oDB.TableDefs["Segments"];
                //}
                //catch { }

                //try
                //{
                //    if (oTD == null)
                //    {
                //        //neither the table nor the link exists -
                //        //create the link
                //        oTD = oDB.CreateTableDef("Segments", 0, "Segments", ";DATABASE=" + xSegmentsDB);
                //        oDB.TableDefs.Append(oTD);
                //        oDB.TableDefs.Refresh();
                //    }
                //    else
                //    {
                //        //refresh the link
                //        oDB.TableDefs["Segments"].Connect = ";DATABASE=" + xSegmentsDB;
                //        oDB.TableDefs["Segments"].RefreshLink();
                //    }
                //}
                //catch (System.Exception oE)
                //{
                //    LMP.Error.Show(oE);
                //}
            }

            LMP.Benchmarks.Print(t0);
        }
		/// <summary>
		/// connects to the local database if
		/// connection doesn't already exist
		/// </summary>
		public static void ConnectIfNecessary(bool bAdminMode)
		{
            bool bModeChanged = LocalConnection.m_bAdminMode != bAdminMode;
            bool bConnect = false;

            LocalConnection.m_bAdminMode = bAdminMode;

            if (m_oCnn == null)
            {
                m_oCnn = new System.Data.OleDb.OleDbConnection();
                bConnect = true;
            }
            else if (bModeChanged)
                bConnect = true;

            //open connection if closed
            if (((m_oCnn.State & ConnectionState.Open) != ConnectionState.Open) || bConnect)
                ConnectToDB(bAdminMode);
		}

        /// <summary>
        /// closes the database connection
        /// </summary>
        public static void Close()
        {
            if(m_oCnn != null && m_oCnn.State != ConnectionState.Closed)
                m_oCnn.Close();
        }

		/// <summary>
		/// handles a change in the state of the local db connection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="oArgs"></param>
		private static void m_oCnn_StateChange(object sender, System.Data.StateChangeEventArgs oArgs)
		{
            //if ((oArgs.CurrentState & ConnectionState.Closed) == ConnectionState.Closed)
            //    //db connection has been broken or otherwise closed - alert
            //    throw new LMP.Exceptions.DBConnectionException(
            //        LMP.Resources.GetLangString("Error_LocalDBConnectionNotOpen"));
		}

        /// <summary>
        /// returns true iff the connection is to the admin db
        /// </summary>
        public static bool AdminConnection
        {
            get { return m_bAdminMode; }
        }
		#endregion
	}

    /// <summary>
    /// contains the methods and properties that define a connection
    /// to the MacPac network database -
    /// created by Daniel Fisherman - 07/04
    /// </summary>
    public sealed class NetworkConnection:IDisposable
    {
        #region *********************fields*********************
        private string m_xDBServer;
        private string m_xDBName;
        private OleDbConnection m_oCnn;
        private bool m_bIsDisposed;
        #endregion
        #region *********************constructors*********************
        /// <summary>
        /// creates the object with a connection 
        /// to the user's specified network database
        /// </summary>
        public NetworkConnection()
        {
            m_oCnn = new OleDbConnection();

            this.m_xDBName = LMP.Data.Application.User.FirmSettings.NetworkDatabaseName;
            this.m_xDBServer = LMP.Data.Application.User.FirmSettings.NetworkDatabaseServer;

            if (string.IsNullOrEmpty(this.m_xDBName) || string.IsNullOrEmpty(this.m_xDBServer))
                throw new LMP.Exceptions
                    .DBConnectionException(LMP.Resources.GetLangString(
                    "Error_NoConnectionParametersSet"));

            m_oCnn.ConnectionString = this.ConnectionString;
        }
        /// <summary>
        /// creates the object wiht a connection
        /// to the specified database
        /// </summary>
        /// <param name="xDatabaseServer"></param>
        /// <param name="xDatabaseName"></param>
        public NetworkConnection(string xDatabaseServer, string xDatabaseName)
        {
            m_oCnn = new OleDbConnection();

            this.m_xDBServer = xDatabaseServer;
            this.m_xDBName = xDatabaseName;

            if (string.IsNullOrEmpty(this.m_xDBName) || string.IsNullOrEmpty(this.m_xDBServer))
                throw new LMP.Exceptions
                    .DBConnectionException(LMP.Resources.GetLangString(
                    "Error_NoConnectionParametersSet"));

            m_oCnn.ConnectionString = this.ConnectionString;
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// returns the local database connection
        /// </summary>
        public OleDbConnection ConnectionObject { get { return m_oCnn; } }
        /// <summary>
        /// returns proper string to create an OleDB connection to SQL database
        /// </summary>
        /// <returns></returns>
        public string ConnectionString
        {
            get
            {
                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    //connect to local user db
                    //GLOG 8220 (dm) - renamed db
                    return "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        LMP.Data.Application.PublicDataDirectory + @"\ForteLocalPublic.mdb;Jet OLEDB:System database=" +
                        LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw" + ";User ID=mp10User;Password=Sackett@4437";
                }
                else
                {
                    return "Provider=SQLOLEDB;Data Source=" + this.m_xDBServer +
                        ";Initial Catalog=" + this.m_xDBName +
                        ";user id=mp10User;password=fish4wade";
                }
            }
        }
        #endregion
        #region *********************methods*********************
        ~NetworkConnection()
        {
            if (!m_bIsDisposed)
                DisposeObjects();
        }

        /// <summary>
        /// connects to the mpLocal database
        /// </summary>
        public void ConnectToDB()
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("this.ConnectionString", this.ConnectionString);

            //return if current connection is
            //the same as the connection requested
            if (m_oCnn.State == ConnectionState.Open &&
                this.ConnectionString.Contains(m_oCnn.ConnectionString))
                return;

            if (m_oCnn.State == ConnectionState.Open)
            {
                //close previous connection
                m_oCnn.Close();
            }

            //set new connection string
            m_oCnn.ConnectionString = this.ConnectionString;

            if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                //alert if a network connection is not available
                if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    throw new LMP.Exceptions.NetworkException(
                        LMP.Resources.GetLangString("Error_NetworkNotAccessible"));
            }

            // GLOG 2728 : JAB 
            // Check that the server tcp connection is established.
            // The data source specifies the host and port in the following format:
            //      host:port
            // if the port is not specified the default port of 1433 is used.
            int iPort;
            string xHost;

            LMP.Network.GetConnectionParameters(m_oCnn, out xHost, out iPort);

            // GLOG 2728 : JAB : Specify a 3 second timeout to connect.
            if (!LMP.Network.IsServerConnectionAvailable(xHost, iPort, 3000))
            {
                throw new LMP.Exceptions.NetworkException(
                    LMP.Resources.GetLangString("Error_NetworkNotAccessible"));
            }

            //open connection
            try { m_oCnn.Open(); }

            catch (System.Exception e)
            {
                string xConnectionDetail = "Server=" + this.m_xDBServer + ", Database=" + this.m_xDBName;
                throw new LMP.Exceptions.DBConnectionException(
                    Resources.GetLangString("Error_CouldNotConnectToDB") + xConnectionDetail, e);
            }

            Trace.WriteNameValuePairs("oCn.State", m_oCnn.State);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// connects to the local database if
        /// connection doesn't already exist
        /// </summary>
        internal void ConnectIfNecessary()
        {
            //open connection if closed
            if ((m_oCnn.State & ConnectionState.Closed) == ConnectionState.Closed)
                ConnectToDB();
        }

        /// <summary>
        /// closes the database connection
        /// </summary>
        public void Close()
        {
            m_oCnn.Close();
        }
        /// <summary>
        /// disposes contained, disposable objects
        /// </summary>
        private void DisposeObjects()
        {
            if (!m_bIsDisposed)
            {
                if (m_oCnn != null)
                {
                    if ((m_oCnn.State & ConnectionState.Open) == ConnectionState.Open)
                        m_oCnn.Close();

                    m_oCnn.Dispose();
                }
            }
        }

        #endregion
        #region IDisposable Members

        public void Dispose()
        {
            DisposeObjects();
            m_bIsDisposed = true;
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
