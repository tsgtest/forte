using System;
using System.Data.OleDb;

namespace LMP.Data
{
	public delegate void KeySetChangeHandler(object sender, KeySetChangeEventArgs e);

	public class KeySetChangeEventArgs: System.EventArgs
	{
		private KeySet m_oKS = null;

		public KeySetChangeEventArgs(KeySet oKS)
		{
			m_oKS = oKS;
		}

		public KeySet KeySetObject
		{
			get {return m_oKS;}
		}
	}

	public enum mpKeySetTypes: byte
	{
		FirmApp = 1,
		AuthorPref = 2,
		UserObjectPref = 3,
		UserTypePref = 4,
		UserApp = 5,
		UserAppPref = 6
	}

	public enum mpPreferenceSetTypes: byte
	{
		Author = 2,
		UserObject = 3,
		UserType = 4,
		UserApp = 6
	}

	public enum mpEntityTypes: byte
	{
		Firm = 0,
		Group = 1,
		Office = 2,
		Person = 3,
        Alias = 4
	}

	/// <summary>
	/// Contains the methods and properties that define a MacPac KeySet.
	/// </summary>
	public class KeySet
	{
		private string m_xEntityID = "";
		private string m_xOriginalEntityID = "";
		private mpKeySetTypes m_iKeySetType = mpKeySetTypes.FirmApp;
		private mpEntityTypes m_iEntityType = mpEntityTypes.Firm;
		private mpEntityTypes m_iOriginalEntityType = mpEntityTypes.Firm;
		private string m_xScopeID = "";
		private int m_iOfficeID = 0;
		private string m_xValues = "";
		private string m_xDefValues = "";
		private LMP.StringArray m_oString;
		private LMP.StringArray m_oDefaultString;
		private bool m_bIsDirty = false;
        private static Person m_oCachedPerson;
        private static KeySet m_oCachedDefPrefSet;
        private int m_iCulture = 1033;

		public static event KeySetChangeHandler KeySetChanged;
		
		#region constructors
		public KeySet(mpKeySetTypes iKeySetType, string xScopeID, string xEntityID,
			int iOfficeID)
		{
			Initialize(iKeySetType, xScopeID, xEntityID, iOfficeID, 1033);
		}

		public KeySet(mpKeySetTypes iKeySetType, string xScopeID, string xEntityID)
		{
			Initialize(iKeySetType, xScopeID, xEntityID, 0, 1033);
		}

		public KeySet(mpPreferenceSetTypes iPreferenceType, string xScopeID, string xEntityID,
			int iOfficeID)
		{
			Initialize((mpKeySetTypes) iPreferenceType, xScopeID, xEntityID, iOfficeID, 1033);
		}

		public KeySet(mpPreferenceSetTypes iPreferenceType, string xScopeID, string xEntityID)
		{
			Initialize((mpKeySetTypes) iPreferenceType, xScopeID, xEntityID, 0, 1033);
		}
        public KeySet(mpKeySetTypes iKeySetType, string xScopeID, string xEntityID,
            int iOfficeID, int iCulture)
        {
            Initialize(iKeySetType, xScopeID, xEntityID, iOfficeID, iCulture);
        }

        public KeySet(mpPreferenceSetTypes iPreferenceType, string xScopeID, string xEntityID,
            int iOfficeID, int iCulture)
        {
            Initialize((mpKeySetTypes)iPreferenceType, xScopeID, xEntityID, iOfficeID, iCulture);
        }
        #endregion

		#region Properties
		public string Scope
		{
			get{return m_xScopeID;}
		}

		public string EntityID
		{
			get{return m_xEntityID;}
		}

		public mpKeySetTypes KeySetType
		{
			get{return m_iKeySetType;}
		}

		public mpEntityTypes EntityType
		{
			get{return m_iEntityType;}
		}

		private string Values
		{
			get{return m_xValues;}
			set
			{
				if (m_xValues != value)
				{
					m_xValues = value;
					m_bIsDirty = true;
				}
			}
		}

		private string DefaultValues
		{
			get{return m_xDefValues;}
			set
			{
				if (m_xDefValues != value)
				{
					m_xDefValues = value;
					m_bIsDirty = true;
				}
			}
		}

        public bool IsDefault
        {
            get { return m_iEntityType != m_iOriginalEntityType; }
        }

        public int Culture
        {
            get{return m_iCulture;}
        }
		#endregion

		#region Methods
		/// <summary>
		/// loads keyset with specified parameters
		/// </summary>
		/// <param name="iKeySetType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		/// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        private void LoadKeys(mpKeySetTypes iKeySetType, string xScopeID, string xEntityID,
			int iOfficeID, int iCulture)
		{
			int iID1;
			int iID2;
            int iScopeID1;
            int iScopeID2;
			string xSproc = "";
			object[] oParams;

			Trace.WriteNameValuePairs("iKeySetType", iKeySetType, "xScopeID", xScopeID,
				"xEntityID", xEntityID, "iOfficeID", iOfficeID, "iCulture", iCulture);

			//initialize keyset with requested parameters
			m_oString = null;
			m_oDefaultString = null;
			m_iKeySetType = iKeySetType;
            if (xScopeID == "")
                xScopeID = "0";
			m_xScopeID = xScopeID;
            Application.SplitID(xScopeID, out iScopeID1, out iScopeID2);

			Application.SplitID(xEntityID, out iID1, out iID2);
			if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
				iID2 = ForteConstants.mpPublicPersonID2;
			SetEntity(Application.GetFormattedID(iID1, iID2));

			m_xOriginalEntityID = m_xEntityID;
			m_iOriginalEntityType = m_iEntityType;
			m_iOfficeID = iOfficeID;
            m_iCulture = iCulture;

			//set appropriate query and parameters for requested entity
			if (m_iOriginalEntityType == mpEntityTypes.Person ||
                m_iOriginalEntityType == mpEntityTypes.Alias)
			{
				//person
				oParams = new object[7];
				oParams[0] = iScopeID1;
                oParams[1] = iScopeID2;
				oParams[2] = iID1;
				oParams[3] = iID2;
				if (m_iOfficeID == 0)
				{
					//get person's office if not supplied
					object[] oSubParams = new object[] {iID1, iID2};
					object oRet = SimpleDataCollection.GetScalar(
						"spOfficeIDByPersonID", oSubParams);
					m_iOfficeID = (int) oRet;
				}
				oParams[4] = m_iOfficeID;
				oParams[5] = (byte) iKeySetType;
                oParams[6] = m_iCulture;

                //if this is an alias, we'll use a query that returns the linked person's
                //keyset as the top default, ahead of the group, office or firm records
                if (m_iOriginalEntityType == mpEntityTypes.Alias)
                    xSproc = "spKeySetSelectForAlias";
                else
				    xSproc = "spKeySetSelectForPerson";
			}
			else
			{
				//office, group, or firm
				oParams = new object[5];
				oParams[0] = iScopeID1;
                oParams[1] = iScopeID2;
				oParams[2] = iID1;
				oParams[3] = (byte) iKeySetType;
                oParams[4] = iCulture;
				xSproc = "spKeySetSelectForGroupOrOffice";
			}

			//execute query
			OleDbDataReader oReader = SimpleDataCollection.GetResultSet
				(xSproc, false, oParams);

			try
			{
				if(oReader.HasRows)
				{
					//get values
					for(int i=0; oReader.Read();i++)
					{
						object[] oValues = new object[oReader.FieldCount];
						oReader.GetValues(oValues);

						//queries will return two records, unless there's only a firm-wide
						//record for the requested keyset or the firm-wide record was
						//specifically requested
						if (i == 0)
						{
							//first record;
							//reset entity type in case query needed to move up
							//a level to find a record for the requested entity
							iID1 = (int) oValues[2];
							iID2 = (int) oValues[3];
							SetEntity(Application.GetFormattedID(iID1, iID2));
							m_xDefValues = oValues[6].ToString();
							if (((m_iOriginalEntityType == mpEntityTypes.Person ||
                                m_iOriginalEntityType == mpEntityTypes.Alias) && 
								(m_iEntityType != m_iOriginalEntityType)) || 
								(m_iOriginalEntityType == mpEntityTypes.Firm))
							{
								//clear out values if no specific record exists for the
								//requested person, in which case the entity will have changed,
								//or if the firm-wide record was specifically requested
								m_xValues = "";
							}
							else
								//set values
								m_xValues = oValues[6].ToString();
						}
						else if (m_iEntityType == m_iOriginalEntityType)
							//second record is the next level up, i.e. default values
							m_xDefValues = oValues[6].ToString();
					}

					m_bIsDirty = false;
				}
				else
				{
					//there's no record, so this is not a valid keyset
					throw new LMP.Exceptions.KeySetException(System.String.Concat(
						LMP.Resources.GetLangString("Error_InvalidKeySet"),
						"(", m_xScopeID, ":", m_iKeySetType.ToString(), ")"));
				}
			}

			finally
			{
				oReader.Close();
                oReader.Dispose();
			}
		}

		/// <summary>
		/// sets the entity ID and Type
		/// </summary>
		/// <param name="xID"></param>
		private void SetEntity(string xID)
		{
			int iID1;
			int iID2;

    		Trace.WriteNameValuePairs("xID", xID);

			//ensure correct id format
			if (xID.IndexOf('.') != -1)
			{
				Application.SplitID(xID, out iID1, out iID2);
				if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
					iID2 = ForteConstants.mpPublicPersonID2;
				xID = Application.GetFormattedID(iID1, iID2);
			}

			//set entity id and type
			if (m_xEntityID != xID)
			{
				m_xEntityID = xID;
				Application.SplitID(m_xEntityID, out iID1, out iID2);
				if (iID1 > 0)
                {
                    if ((iID2 > 1) && m_oCachedPerson.IsAlias)
                        m_iEntityType = mpEntityTypes.Alias;
                    else
					m_iEntityType = mpEntityTypes.Person;
                }
				else if ((iID1 > -2000001) && (iID2 < 0))
					m_iEntityType = mpEntityTypes.Office;
				else if (iID1 > ForteConstants.mpFirmRecordID)
					m_iEntityType = mpEntityTypes.Group;
				else
					m_iEntityType = mpEntityTypes.Firm;
			}	  
		}

		/// <summary>
		/// fills module-level string array with entity-specific values
		/// </summary>
		private void LoadArray()
		{
			Trace.WriteNameValuePairs("m_xValues", m_xValues);

			m_oString = new LMP.StringArray(m_xValues);
		}

		/// <summary>
		/// fills module-level string array with default values
		/// </summary>
		private void LoadDefaultArray()
		{
			Trace.WriteNameValuePairs("m_xDefValues", m_xDefValues);

			m_oDefaultString = new LMP.StringArray(m_xDefValues);
		}

		/// <summary>
		/// returns the entity-specific value of the specified key
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <returns></returns>
		private string GetEntitySpecificValue(string xKeyName)
		{
			Trace.WriteNameValuePairs("xKeyName", xKeyName);

			if (m_xValues != "")
			{
				if (m_oString == null)
					LoadArray();

				for (int i = 0; i < m_oString.FieldCount(); i++)
				{
					if (m_oString[i, 0].ToUpper() == xKeyName.ToUpper())
					{
						return m_oString[i, 1];
					}
				}
			}
			return null;
		}

		/// <summary>
		/// returns the value of the specified key
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <returns></returns>
		public string GetValue(string xKeyName)
		{
			bool bIsDefault;
			return GetValue(xKeyName, out bIsDefault);
		}

		/// <summary>
		/// returns the value of the specified key and whether it's the default
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <param name="bIsDefault"></param>
		/// <returns></returns>
		public string GetValue(string xKeyName, out bool bIsDefault)
		{
			string xValue = "";

			Trace.WriteNameValuePairs("xKeyName", xKeyName);

			//get value for specified entity
			if (m_xValues != "")
			{
				xValue = GetEntitySpecificValue(xKeyName);
				if (xValue != null)
				{
					bIsDefault = false;
					return xValue;
				}
			}

			//no user value, get default
			xValue = GetDefaultValue(xKeyName);
			bIsDefault = true;	
			return xValue;
		}

		/// <summary>
		/// returns the default value of the specified key
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <returns></returns>
		public string GetDefaultValue(string xKeyName)
		{
			Trace.WriteNameValuePairs("xKeyName", xKeyName);

			if (m_oDefaultString == null)
				LoadDefaultArray();

			for (int i = 0; i < m_oDefaultString.FieldCount(); i++)
			{
				if (m_oDefaultString[i, 0].ToUpper() == xKeyName.ToUpper())
				{
					return m_oDefaultString[i, 1];
				}
			}
            //GLOG 4938: If Office Keyset is missing specified key,
            //check Firm keyset for value
            if (m_iOriginalEntityType == mpEntityTypes.Person)
            {
                string xFirmDef = null;
                try
                {
                    //GLOG 15817: Make sure we're getting KeySet matching current type
                    if (m_oCachedDefPrefSet == null)
                    {
                        DateTime t0 = DateTime.Now;

                        m_oCachedDefPrefSet = GetPreferenceSet((mpPreferenceSetTypes)m_iKeySetType,
                            this.m_xScopeID, -999999999, this.m_iOfficeID, m_iCulture);

                        LMP.Benchmarks.Print(t0, "GetPreferenceSet");
                    }

                    xFirmDef = m_oCachedDefPrefSet.GetValue(xKeyName);
                    //xFirmDef = KeySet.GetKeyValue(xKeyName, mpKeySetTypes.AuthorPref, this.m_xScopeID,
                    //    -999999999, this.m_iOfficeID, m_iCulture);
                }
                catch
                {
                }
                //key doesn't exist - raise exception
                if (xFirmDef != null)
                    return xFirmDef;
            }
            throw new LMP.Exceptions.NotInCollectionException(LMP.Resources
                .GetLangString("Error_KeyNotFound") + xKeyName);
		}

		/// <summary>
		/// returns the number of keys in the keyset
		/// </summary>
		/// <returns></returns>
		public int GetValueCount()
		{
			if (m_oDefaultString == null)
				LoadDefaultArray();
			return m_oDefaultString.FieldCount();
		}

		/// <summary>
		/// returns the name of the key with the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public string GetValueName(int iIndex)
		{
			if (m_oDefaultString == null)
				LoadDefaultArray();
			//field name is in the first column of the values array
			return m_oDefaultString[(iIndex - 1), 0];
		}

		/// <summary>
		/// sets value of specified key
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <param name="xValue"></param>
		public void SetValue(string xKeyName, string xValue)
		{
			Trace.WriteNameValuePairs("xKeyName", xKeyName, "xValue", xValue);

			if (m_iOriginalEntityType != mpEntityTypes.Firm)
			{
				//update entity-specific key set
				if (m_oString == null)
					LoadArray();
				for (int i=0; i < m_oString.FieldCount(); i++)
				{
					if (m_oString[i, 0].ToUpper() == xKeyName.ToUpper())
					{
						try
						{
							string xCurValue = m_oString[i,1];
						}
						catch
						{
							//matrix item does not exist - create
							m_oString[i,1] = null;
						}

						if (m_oString[i, 1] != xValue)
						{
							//value has changed
							m_oString[i, 1] = xValue;
							Values = m_oString.TextString;
    						Trace.WriteNameValuePairs("m_xValues", m_xValues);
							if (KeySetChanged != null)
								KeySetChanged(this, new KeySetChangeEventArgs(this));
						}
						return;
					}
				}

				//entity-specific key not found - check for default
				if (m_oDefaultString == null)
					LoadDefaultArray();
				for (int i=0; i < m_oDefaultString.FieldCount(); i++)
				{
					if (m_oDefaultString[i, 0].ToUpper() == xKeyName.ToUpper())
					{
						//default key found - add to entity-specific set
						m_oString.InsertField();
						int iIndex = System.Math.Max(m_oString.FieldCount() - 1, 0);
						m_oString[iIndex, 0] = xKeyName;
						m_oString[iIndex, 1] = xValue;
						Values = m_oString.TextString;
    					Trace.WriteNameValuePairs("m_xValues", m_xValues);
						if (KeySetChanged != null)
							KeySetChanged(this, new KeySetChangeEventArgs(this));
						return;
					}
				}

				//if we got here, no key was found - add key if keyset
				//type is user preference - these should be written on the fly
				if(this.KeySetType == mpKeySetTypes.UserAppPref ||
					this.KeySetType == mpKeySetTypes.UserObjectPref ||
					this.KeySetType == mpKeySetTypes.UserTypePref ||
                    this.KeySetType == mpKeySetTypes.UserApp ||
                    this.KeySetType == mpKeySetTypes.FirmApp)
				{
					//no key was found - add key
					m_oString.InsertField();
					int iIndex = System.Math.Max(m_oString.FieldCount() - 1, 0);
					m_oString[iIndex, 0] = xKeyName;
					m_oString[iIndex, 1] = xValue;
					Values = m_oString.TextString;
					Trace.WriteNameValuePairs("m_xValues", m_xValues);
					if (KeySetChanged != null)
						KeySetChanged(this, new KeySetChangeEventArgs(this));
				}
				else
				{
					//default key not found - raise exception
				    throw new LMP.Exceptions.NotInCollectionException(LMP.Resources
					    .GetLangString("Error_ItemWithNameNotInCollection") + xKeyName);
				}
			}
			else
			{
				//update firm-wide key set
				if (m_oDefaultString == null)
					LoadDefaultArray();
				for (int i=0; i < m_oDefaultString.FieldCount(); i++)
				{
					if (m_oDefaultString[i, 0].ToUpper() == xKeyName.ToUpper())
					{
						if (m_oDefaultString[i, 1] != xValue)
						{
							//value has changed
							m_oDefaultString[i, 1] = xValue;
							DefaultValues = m_oDefaultString.TextString;
							Trace.WriteNameValuePairs("m_xDefValues", m_xDefValues);
							if (KeySetChanged != null)
								KeySetChanged(this, new KeySetChangeEventArgs(this));
						}
						return;
					}
				}

				//firm-wide key not found - add it
				m_oDefaultString.InsertField();
				int iIndex = System.Math.Max(m_oDefaultString.FieldCount() - 1, 0);
				m_oDefaultString[iIndex, 0] = xKeyName;
				m_oDefaultString[iIndex, 1] = xValue;
				DefaultValues = m_oDefaultString.TextString;
				Trace.WriteNameValuePairs("m_xDefValues", m_xDefValues);
				if (KeySetChanged != null)
					KeySetChanged(this, new KeySetChangeEventArgs(this));
			}
		}

		/// <summary>
		/// saves changes to keyset record; creates new record if necessary
		/// </summary>
		public void Save()
		{
			if (m_bIsDirty)
			{
				int iID1;
				int iID2;
                int iScopeID1;
                int iScopeID2;

				Trace.WriteNameValuePairs("m_xScopeID", m_xScopeID, "m_xOriginalEntityID",
					m_xOriginalEntityID, "m_iKeySetType", m_iKeySetType, "m_iCulture",
                    m_iCulture);

                //split Scope ID
                Application.SplitID(m_xScopeID, out iScopeID1, out iScopeID2);

                //split entity id
				Application.SplitID(m_xOriginalEntityID, out iID1, out iID2);
				if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
					iID2 = ForteConstants.mpPublicPersonID2;

				//set query parameters
				object[] oParams = new object[8];
				oParams[0] = iScopeID1;
                oParams[1] = iScopeID2;
				oParams[2] = iID1;
				oParams[3] = iID2;
				oParams[4] = (byte) m_iKeySetType;
                oParams[5] = m_iCulture;
				if (m_iOriginalEntityType != mpEntityTypes.Firm)
					oParams[6] = m_xValues;
				else
					oParams[6] = m_xDefValues;
                oParams[7] = Application.GetCurrentEditTime();

                if (m_iEntityType == m_iOriginalEntityType)
				{
                    //update existing record
					SimpleDataCollection.ExecuteActionSproc("spKeySetUpdate", oParams);
				}
				else
				{
                    //add new record
				    SimpleDataCollection.ExecuteActionSproc("spKeySetAdd", oParams);

					//reset entity, since there's now a record for it
					m_iEntityType = m_iOriginalEntityType;
                    //GLOG 4632
                    m_xEntityID = m_xOriginalEntityID;
				}

				m_bIsDirty = false;
			}
			else
			{
    			Trace.WriteInfo("Key set is not dirty");
			}
		}

		/// <summary>
		/// deletes entity-specific keyset record
		/// </summary>
		public void Delete()
		{
			int iID1;
			int iID2;
            int iScopeID1;
            int iScopeID2;

			if (m_iEntityType != m_iOriginalEntityType)
			{
				//raise error if keyset belongs to higher-level entity
				throw new LMP.Exceptions.ItemDeleteException(LMP.Resources
					.GetLangString("Error_KeySetDelete"));
			}

			Trace.WriteNameValuePairs("m_xScopeID", m_xScopeID, "m_xEntityID",
				m_xEntityID, "m_iKeySetType", m_iKeySetType, "m_iCulture", m_iCulture);

			//split scope ID
            Application.SplitID(m_xScopeID, out iScopeID1, out iScopeID2);

            //split entity id
			Application.SplitID(m_xEntityID, out iID1, out iID2);
			if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
				iID2 = ForteConstants.mpPublicPersonID2;
            
			//set query parameters
			object[] oParams = new object[6];
			oParams[0] = iScopeID1;
            oParams[1] = iScopeID2;
			oParams[2] = iID1;
			oParams[3] = iID2;
			oParams[4] = (byte) m_iKeySetType;
            oParams[5] = m_iCulture;

            //execute query
			SimpleDataCollection.ExecuteActionSproc("spKeySetDelete", oParams);
            //GLOG 4937: Keyset Deletion needs to be logged for sync
            string xDelObjID = iScopeID1.ToString() + "." + iScopeID2.ToString() + "." + iID1.ToString() + "." + iID2.ToString() + "." + ((byte)m_iKeySetType).ToString() + "." + m_iCulture.ToString();
            SimpleDataCollection.LogDeletions(mpObjectTypes.KeySets, xDelObjID);
		}

		/// <summary>
		/// returns array containing key name, value, and a third column to
		/// hold "0" if the value is entity-specific or "1" if it is the default
		/// </summary>
		/// <returns></returns>
		public string[,] ToArray()
		{
			
			if (m_oDefaultString == null)
				LoadDefaultArray();

			if (m_oDefaultString.TextString != "")
			{
				string xValue = "";

				//get default array
				int iFields = m_oDefaultString.FieldCount();
				string[,] oArray = m_oDefaultString.ToArray();

				//create new array with third column
				string[,] oArrayMod = new string[iFields, 3];

				//cycle through default array
				for (int i = 0; i < iFields; i++)
				{
					//copy default name and value
					oArrayMod[i,0] = oArray[i,0];
					oArrayMod[i,1] = oArray[i,1];

					//update with entity-specic value if it exists;
					//otherwise, mark as default
					if (m_xValues != "")
					{
						xValue = GetEntitySpecificValue(oArray[i,0]);
						if (xValue != null)
						{
							oArrayMod[i,1] = xValue;
							oArrayMod[i,2] = "0";
						}
						else
							oArrayMod[i,2] = "1";
					}
					else
						oArrayMod[i,2] = "1";
				}

				//return new array
				return oArrayMod;
			}

			//return empty array
			return new string[1, 3];
		}

		#endregion

		#region Static Members
		/// <summary>
		/// returns firm application settings for xEntityID
		/// </summary>
		/// <param name="xEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetFirmAppKeys(string xEntityID)
		{
			Trace.WriteNameValuePairs("xEntityID", xEntityID);
			KeySet oKS = new KeySet(mpKeySetTypes.FirmApp, "", xEntityID);
			return oKS;
		}

		/// <summary>
		/// returns firm application settings for iEntityID
		/// </summary>
		/// <param name="iEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetFirmAppKeys(int iEntityID)
		{
			return GetFirmAppKeys(iEntityID.ToString());
		}

		/// <summary>
		/// returns user application settings for xEntityID
		/// </summary>
		/// <param name="xEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetUserAppKeys(string xEntityID)
		{
			Trace.WriteNameValuePairs("xEntityID", xEntityID);
			KeySet oKS = new KeySet(mpKeySetTypes.UserApp, "", xEntityID);
			return oKS;
		}

		/// <summary>
		/// returns user application settings for iEntityID
		/// </summary>
		/// <param name="iEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetUserAppKeys(int iEntityID)
		{
			return GetUserAppKeys(iEntityID.ToString());
		}

		/// <summary>
		/// returns preference set of iType and xScopeID for xEntityID
		/// </summary>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		/// <param name="iOfficeID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
			iType, string xScopeID, string xEntityID, int iOfficeID)
		{
			Trace.WriteNameValuePairs("iType", iType, "xScopeID", xScopeID,
				"xEntityID", xEntityID, "iOfficeID", iOfficeID);

            KeySet oKS = new KeySet(iType, xScopeID, xEntityID, iOfficeID);
			return oKS;
		}

		/// <summary>
		/// returns preference set of iType and xScopeID for iEntityID
		/// </summary>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		/// <param name="iOfficeID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
			iType, string xScopeID, int iEntityID, int iOfficeID)
		{
			return GetPreferenceSet(iType, xScopeID, iEntityID.ToString(), iOfficeID);
		}

		/// <summary>
		/// returns preference set of iType and xScopeID for xEntityID
		/// </summary>
		/// <param name="iType"></param>
		/// <param name="iScope"></param>
		/// <param name="xEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
			iType, string xScopeID, string xEntityID)
		{
			Trace.WriteNameValuePairs("iType", iType, "xScopeID", xScopeID,
				"xEntityID", xEntityID);

            KeySet oKS = new KeySet(iType, xScopeID, xEntityID);
			return oKS;
		}

		/// <summary>
		/// returns preference set of iType and xScopeID for iEntityID
		/// </summary>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		/// <returns></returns>
		public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
			iType, string xScopeID, int iEntityID)
		{
			return GetPreferenceSet(iType, xScopeID, iEntityID.ToString());
		}

        /// <summary>
        /// returns preference set of iType and xScopeID and iCulture for xEntityID
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="xEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
            iType, string xScopeID, string xEntityID, int iOfficeID, int iCulture)
        {
            Trace.WriteNameValuePairs("iType", iType, "xScopeID", xScopeID,
                "xEntityID", xEntityID, "iOfficeID", iOfficeID, "iCulture", iCulture);

            KeySet oKS = new KeySet(iType, xScopeID, xEntityID, iOfficeID, iCulture);
            return oKS;
        }

        /// <summary>
        /// returns preference set of iType and xScopeID and iCulture for iEntityID
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="iEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public static LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes
            iType, string xScopeID, int iEntityID, int iOfficeID, int iCulture)
        {
            return GetPreferenceSet(iType, xScopeID, iEntityID.ToString(),
                iOfficeID, iCulture);
        }

        /// <summary>
		/// returns value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		/// <param name="iOfficeID"></param>
		/// <returns></returns>
		public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
			string xScopeID, string xEntityID, int iOfficeID)
		{
			KeySet oKS;

			Trace.WriteNameValuePairs("xName", xName, "iType", iType, "xScopeID",
				xScopeID, "xEntityID", xEntityID, "iOfficeID", iOfficeID);

			if (iType == mpKeySetTypes.FirmApp)
				oKS = GetFirmAppKeys(xEntityID);
			else if (iType == mpKeySetTypes.UserApp)
				oKS = GetUserAppKeys(xEntityID);
			else
				oKS = GetPreferenceSet((mpPreferenceSetTypes) iType, xScopeID,
					xEntityID, iOfficeID);

			return oKS.GetValue(xName);
		}

		/// <summary>
		/// returns value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		/// <param name="iOfficeID"></param>
		/// <returns></returns>
		public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
			string xScopeID, int iEntityID, int iOfficeID)
		{
			return GetKeyValue(xName, iType, xScopeID, iEntityID.ToString(), iOfficeID);
		}

		/// <summary>
		/// returns value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		/// <returns></returns>
		public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
			string xScopeID, string xEntityID)
		{
			return GetKeyValue(xName, iType, xScopeID, xEntityID, 0);
		}

		/// <summary>
		/// returns value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		/// <returns></returns>
		public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
			string xScopeID, int iEntityID)
		{
			return GetKeyValue(xName, iType, xScopeID, iEntityID.ToString(), 0);
		}

        /// <summary>
        /// returns value of specified key
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="xEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
            string xScopeID, string xEntityID, int iOfficeID, int iCulture)
        {
            KeySet oKS;

            Trace.WriteNameValuePairs("xName", xName, "iType", iType, "xScopeID",
                xScopeID, "xEntityID", xEntityID, "iOfficeID", iOfficeID,
                "iCulture", iCulture);

            if (iType == mpKeySetTypes.FirmApp)
                oKS = GetFirmAppKeys(xEntityID);
            else if (iType == mpKeySetTypes.UserApp)
                oKS = GetUserAppKeys(xEntityID);
            else
                oKS = GetPreferenceSet((mpPreferenceSetTypes)iType, xScopeID,
                    xEntityID, iOfficeID, iCulture);

            return oKS.GetValue(xName);
        }

        /// <summary>
        /// returns value of specified key
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="iEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public static string GetKeyValue(string xName, LMP.Data.mpKeySetTypes iType,
            string xScopeID, int iEntityID, int iOfficeID, int iCulture)
        {
            return GetKeyValue(xName, iType, xScopeID, iEntityID.ToString(),
                iOfficeID, iCulture);
        }

        /// <summary>
		/// sets value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		/// <param name="iOfficeID"></param>
		public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
			string xScopeID, string xEntityID, int iOfficeID)
		{
			KeySet oKS = null;

			Trace.WriteNameValuePairs("xName", xName, "xValue", xValue, "iType", iType,
				"xScopeID", xScopeID, "xEntityID", xEntityID, "iOfficeID", iOfficeID);

			if (iType == mpKeySetTypes.FirmApp)
				oKS = GetFirmAppKeys(xEntityID);
			else if (iType == mpKeySetTypes.UserApp)
				oKS = GetUserAppKeys(xEntityID);
			else
				oKS = GetPreferenceSet((mpPreferenceSetTypes) iType, xScopeID,
					xEntityID, iOfficeID);

			oKS.SetValue(xName, xValue);
			oKS.Save();
		}

		/// <summary>
		/// sets value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		/// <param name="iOfficeID"></param>
		public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
			string xScopeID, int iEntityID, int iOfficeID)
		{
			SetKeyValue(xName, xValue, iType, xScopeID, iEntityID.ToString(), iOfficeID);
		}

		/// <summary>
		/// sets value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xEntityID"></param>
		public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
			string xScopeID, string xEntityID)
		{
			SetKeyValue(xName, xValue, iType, xScopeID, xEntityID, 0);
		}

		/// <summary>
		/// sets value of specified key
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="iEntityID"></param>
		public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
			string xScopeID, int iEntityID)
		{
			SetKeyValue(xName, xValue, iType, xScopeID, iEntityID.ToString(), 0);
		}

        /// <summary>
        /// sets value of specified key
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="xEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
            string xScopeID, string xEntityID, int iOfficeID, int iCulture)
        {
            KeySet oKS = null;

            Trace.WriteNameValuePairs("xName", xName, "xValue", xValue, "iType", iType,
                "xScopeID", xScopeID, "xEntityID", xEntityID, "iOfficeID", iOfficeID,
                "iCulture", iCulture);

            if (iType == mpKeySetTypes.FirmApp)
                oKS = GetFirmAppKeys(xEntityID);
            else if (iType == mpKeySetTypes.UserApp)
                oKS = GetUserAppKeys(xEntityID);
            else
                oKS = GetPreferenceSet((mpPreferenceSetTypes)iType, xScopeID,
                    xEntityID, iOfficeID, iCulture);

            oKS.SetValue(xName, xValue);
            oKS.Save();
        }

        /// <summary>
        /// sets value of specified key
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <param name="iEntityID"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="iCulture"></param>
        public static void SetKeyValue(string xName, string xValue, LMP.Data.mpKeySetTypes iType,
            string xScopeID, int iEntityID, int iOfficeID, int iCulture)
        {
            SetKeyValue(xName, xValue, iType, xScopeID, iEntityID.ToString(),
                iOfficeID, iCulture);
        }

        /// <summary>
		/// edits all group and office key sets to reflect a new default value as specified;
		/// raises error if firm record not found
		/// </summary>
		/// <param name="iType"></param>
		/// <param name="xScopeID"></param>
		/// <param name="xName"></param>
		/// <param name="xNewValue"></param>
		public static void UpdateGroupKeySets(LMP.Data.mpKeySetTypes iType, string xScopeID,
			string xName, string xNewValue, int iCulture)
		{
			Trace.WriteNameValuePairs("iType", iType, "xScopeID", xScopeID, "xName", xName,
				"xNewValue", xNewValue, "iCulture", iCulture);
            int iScopeID1;
            int iScopeID2;
            //split Scope ID
            Application.SplitID(xScopeID, out iScopeID1, out iScopeID2);

			//set query parameters
			object[] oParams = new object[7];
			oParams[0] = iScopeID1;
            oParams[1] = iScopeID2;
			oParams[2] = iType;
            oParams[3] = iCulture;
			oParams[4] = xName;
			oParams[5] = xNewValue;
            oParams[6] = Application.GetCurrentEditTime();

			//execute
			int iRecs = SimpleDataCollection.ExecuteActionSproc("spKeySetsUpdateGroups", oParams);

			if (iRecs == 0)
			{
				//for speed reasons, query edits firm record as well
				throw new LMP.Exceptions.DataException(LMP.Resources
					.GetLangString("Error_NoDefaultKeySet"));
			}
		}

        /// <summary>
        /// adds author preference record
        /// </summary>
        /// <param name="xScopeID"></param>
        /// <param name="xEntityID"></param>
        /// <param name="aNameValuePairs"></param>
        public static void AddAuthorPrefRecord(string xScopeID, string xEntityID,
            int iCulture, System.Collections.ArrayList aNameValuePairs)
        {
            int iID1;
            int iID2;
            int iScopeID1;
            int iScopeID2;

            //split Scope ID
            Application.SplitID(xScopeID, out iScopeID1, out iScopeID2);

            //split entity id
            Application.SplitID(xEntityID, out iID1, out iID2);
            if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
                iID2 = ForteConstants.mpPublicPersonID2;

            //set query parameters
            object[] aParams = new object[8];
            Application.SplitID(xScopeID, out iScopeID1, out iScopeID2);
            aParams[0] = iScopeID1;
            aParams[1] = iScopeID2;
            aParams[2] = iID1;
            aParams[3] = iID2;
            aParams[4] = (byte)mpKeySetTypes.AuthorPref;
            aParams[5] = iCulture;

            //construct values parameter
            string xValues = "";
            if (aNameValuePairs != null)
            {
                for (int i = 0; i < aNameValuePairs.Count; i++)
                {
                    string[] aKey = (string[])aNameValuePairs[i];
                    xValues += aKey[0] + StringArray.mpEndOfValue + aKey[1] +
                        StringArray.mpEndOfField;
                }
                xValues = xValues.TrimEnd(StringArray.mpEndOfField);
            }
            aParams[6] = xValues;
            aParams[7] = Application.GetCurrentEditTime();

            //add record
            SimpleDataCollection.ExecuteActionSproc("spKeySetAdd", aParams);
        }

        public static bool Exists(LMP.Data.mpKeySetTypes iType, string xScopeID, string xEntityID)
        {
            KeySet oFirmKeyset = null;

            try
            {
                oFirmKeyset = new KeySet(iType, xScopeID, xEntityID);
            }
            catch { }

            return oFirmKeyset != null;
        }
		#endregion

		#region *********************private methods*********************
		private void Initialize(mpKeySetTypes iKeySetType, string xScopeID, string xEntityID,
			int iOfficeID, int iCulture)
		{
            int iID1;
            int iID2;

            //split entity ID
            Application.SplitID(xEntityID, out iID1, out iID2);
            if ((iID1 > 0) && (iID2 == ForteConstants.mpFirmRecordID))
                iID2 = ForteConstants.mpPublicPersonID2;

            //update cached person if necessary - we use this to distinguish
            //between an alias and a private author
            if (iID1 > 0 && iID2 > 0)
            {
                if (m_oCachedPerson == null || m_oCachedPerson.ID != xEntityID)
                    m_oCachedPerson = LocalPersons.GetPersonFromID(xEntityID);
            }

            //load key set
            if (xScopeID == "")
                xScopeID = "0";
            try
            {
                LoadKeys(iKeySetType, xScopeID, xEntityID, iOfficeID, iCulture);
            }
            catch (System.Exception oE)
            {
                //record doesn't exist in DB
                if (this.KeySetType == mpKeySetTypes.UserAppPref ||
                    this.KeySetType == mpKeySetTypes.UserObjectPref ||
                    this.KeySetType == mpKeySetTypes.UserTypePref || 
                    this.KeySetType == mpKeySetTypes.UserApp ||
                    this.KeySetType == mpKeySetTypes.FirmApp)
                {
                    //create user preference now
                    int iScopeID1;
                    int iScopeID2;
                    Application.SplitID(xScopeID, out iScopeID1, out iScopeID2);

                    //set entity
                    SetEntity(Application.GetFormattedID(iID1, iID2));

                    //add new record
                    SimpleDataCollection.ExecuteActionSproc("spKeySetAdd",
                        new object[] { iScopeID1, iScopeID2, iID1, iID2, iKeySetType,
                            iCulture, "", Application.GetCurrentEditTime()});

                    LoadKeys(iKeySetType, xScopeID, xEntityID, iOfficeID, iCulture);
                }
                else
                    //other types of keysets should not be created transparently
                    throw oE;
            }
        }
		#endregion
	}
}
