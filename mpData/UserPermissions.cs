using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;

namespace LMP.Data
{
    public class UserPermissions
    {
        /// <summary>
        /// returns true if the specified person has permission to the specified object
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static bool IsPersonPermitted(mpObjectTypes iObjectType, int iObjectID1, int iObjectID2, int iPersonID)
        {
            int iCount = (int)LMP.Data.SimpleDataCollection.GetScalar(
                "spUserPermissionsExists", new object[] { iObjectType, iObjectID1, iObjectID2, iPersonID });
            return (iCount > 0);
        }
        /// <summary>
        /// Grant specified Group or Office permission to view an Object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID"></param>
        /// <param name="iEntityID"></param>
        public static void GrantPermission(mpObjectTypes iObjectType, int iObjectID1, int iObjectID2, int iPersonID)
        {
            SimpleDataCollection.ExecuteActionSproc("spUserPermissionsGrant", new object[] { (int)iObjectType, iObjectID1, iObjectID2, iPersonID, Application.GetCurrentEditTime()});
        }
        /// <summary>
        /// Revoke permission of specified Person to view an Object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID"></param>
        /// <param name="iEntityID"></param>
        public static void RevokePermission(mpObjectTypes iObjectType, int iObjectID1, int iObjectID2, int iPersonID)
        {
            SimpleDataCollection.ExecuteActionSproc("spUserPermissionsRevoke", new object[] { (int)iObjectType, iObjectID1, iObjectID2, iPersonID });
            //Log deletion of Permissions records
            SimpleDataCollection.LogDeletions(mpObjectTypes.Permissions, ((int)iObjectType).ToString() + "." + iObjectID1.ToString() + "." + iObjectID2.ToString() + "." + iPersonID.ToString());
        }
        /// <summary>
        /// Returns ArrayList containg Display Name and ID of users with permission to the selected object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID1"></param>
        /// <param name="iObjectID2"></param>
        /// <returns></returns>
        public static ArrayList GetPermittedUsers(mpObjectTypes iObjectType, int iObjectID1, int iObjectID2)
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "spUserPermissionsGetPermittedUsers";
            //get reader with display fields only
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, false, new object[] { iObjectType, iObjectID1, iObjectID2 }))
            {

                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                    LMP.Benchmarks.Print(t0);
                    return oArray;
                }
                finally
                {
                    oReader.Close();
                }
            }
        }
        /// <summary>
        /// Remove all permissions for the selected object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectiD1"></param>
        /// <param name="iObjectID2"></param>
        public static void ClearPermissions(mpObjectTypes iObjectType, int iObjectID1, int iObjectID2)
        {
            SimpleDataCollection.ExecuteActionSproc("spUserPermissionsClearAll", new object[] { (int)iObjectType, iObjectID1, iObjectID2 });
            //JTS 12/31/08: Log deletion of Permissions records
            SimpleDataCollection.LogDeletions(mpObjectTypes.Permissions, ((int)iObjectType).ToString() + "." + iObjectID1.ToString() + "." + iObjectID2.ToString());
        }
    }
}
