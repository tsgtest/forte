using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace LMP.Data
{
    public static class FavoriteSegments
    {
        #region *********************fields**********************
        private static DataTable m_dtFaveSegments = new DataTable();

        private static int FAVORITE_SEGMENT_NAME_INDEX = 0;
        private static int FAVORITE_SEGMENT_ID_INDEX = 1;
        private static int FAVORITE_SEGMENT_TYPE_INDEX = 2;
        private static int FAVORITE_SEGMENT_DISPLAY_NAME_INDEX = 3;
        private static int FAVORITE_SEGMENT_INSERT_LOCATION_INDEX = 4;
        private static int FAVORITE_SEGMENT_INTENDED_USE_INDEX = 5;
        private static bool m_IsInitialized;

        //GLOG : 6221 : CEH
        public static int FAVORITE_SEGMENT_MAX_FAVORITES = 35;

        #endregion
        #region *********************constructors**********************
        static FavoriteSegments()
        {
            m_IsInitialized = false;

            Load(LMP.Data.Application.g_oCurUser.UserSettings.FavoriteSegments);
        }
        #endregion
        #region *********************methods**********************

        public static void RefreshFavorites()
        {
            Load(LMP.Data.Application.g_oCurUser.UserSettings.FavoriteSegments);
        }

        public static void Remove(int iSegmentIndex)
        {
            ToDataTable().Rows.RemoveAt(iSegmentIndex);
        }
        
        //GLOG : 7276 : jsw
        public static void Remove (string xSegmentName)
        {
            for (int i = 0; i < m_dtFaveSegments.Rows.Count; i++)
            {
                //check segment display name
                if (m_dtFaveSegments.Rows[i]["SegmentDisplayName"].ToString() == xSegmentName)
                {
                    Remove(i);
                    return;
                }   
            }
        }

        public static void Move(int iRowIndex, int iOffset)
        {
            if (iRowIndex < ToDataTable().Rows.Count)
            {
                DataTable oDT = ToDataTable();
                DataRow drSegmentRow = ToDataTable().Rows[iRowIndex];
                DataRow oNewRow = oDT.NewRow();

                for (int i = 0; i < oDT.Columns.Count; i++)
                {
                    oNewRow[i] = drSegmentRow[i];
                }

                oDT.Rows.Remove(drSegmentRow);
                oDT.Rows.InsertAt(oNewRow, iRowIndex + iOffset);
            }
        }

        /// <summary>
        /// Add to the collection favorite segments.
        /// </summary>
        /// <param name="xFavoriteSegmentName"></param>
        /// <returns></returns>
        public static bool Add(string xSegmentName, 
            string xSegmentID, LMP.Data.mpFolderMemberTypes xSegmentFolderMemberType, 
            string xSegmentDisplayName, LMP.Data.mpSegmentIntendedUses xSegmentIntendedUse)
        {
            DataRow[] adrFoundSegments = null;
            try
            {
                // Add the segment only if it is not currently in the collection.
                adrFoundSegments = m_dtFaveSegments.Select("SegmentName = " + xSegmentName);
            }
            catch
            {
                // The segment name was not found.
            }

            if (adrFoundSegments == null || adrFoundSegments.Length == 0)
            {
                DataRow drFavoriteSegmentRow = m_dtFaveSegments.NewRow();

                drFavoriteSegmentRow["SegmentName"] = xSegmentName;
                drFavoriteSegmentRow["SegmentID"] = xSegmentID;
                drFavoriteSegmentRow["SegmentType"] = xSegmentFolderMemberType;
                drFavoriteSegmentRow["SegmentDisplayName"] = xSegmentDisplayName;
                drFavoriteSegmentRow["SegmentIntendedUse"] = xSegmentIntendedUse;

                m_dtFaveSegments.Rows.Add(drFavoriteSegmentRow);
                // return a boolean indicating that the addition was made successfully.
                return true;
            }

            // return a boolean indicating that the addition failed due to the 
            // item previously existing in the collection.
            return false;
        }

        /// <summary>
        /// Get the number of favorite segments.
        /// </summary>
        public static DataTable ToDataTable()
        {
            return m_dtFaveSegments;
        }

        public static bool IsInitialized
        {
            get
            {
                return m_IsInitialized;
            }
        }

        private static void Load(DataTable oFavoriteSegmentsDataTable)
        {
            m_IsInitialized = true;
            m_dtFaveSegments = oFavoriteSegmentsDataTable;
        }

        public static void Save()
        {
            LMP.Data.Application.g_oCurUser.UserSettings.FavoriteSegments = ToDataTable();
        }

        /// <summary>
        /// Return the segment name.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static string ItemFromIndex(int iSegmentIndex)
        {
            return (string)ToDataTable().Rows[iSegmentIndex][0];
        }

        /// <summary>
        /// Get the number of favorite segments.
        /// </summary>
        public static int Count
        {
            get
            {
                return ToDataTable().Rows.Count;
            }
        }

        /// <summary>
        /// Get the favorite segment's id.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static string SegmentID(int iSegmentIndex)
        {
            return (string)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_ID_INDEX];
        }

        /// <summary>
        /// Get the favorite segment's name.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static string SegmentName(int iSegmentIndex)
        {
            return (string)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_NAME_INDEX];
        }

        /// <summary>
        /// Get the favorite segment's display name.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static string SegmentDisplayName(int iSegmentIndex)
        {
            return (string)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_DISPLAY_NAME_INDEX];
        }

        /// <summary>
        /// Get the favorite segment's type.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static LMP.Data.mpFolderMemberTypes SegmentFolderMemberType(int iSegmentIndex)
        {
            return (LMP.Data.mpFolderMemberTypes)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_TYPE_INDEX];
        }


        /// <summary>
        /// Get the favorite segment's insertion location.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static int SegmentInsertionLocation(int iSegmentIndex)
        {
            return (int)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_INSERT_LOCATION_INDEX];
        }
        #endregion
		//GLOG : 6303 : jsw
		//add intended use icon to favorites 
        /// <summary>
        /// Get the favorite segment's Intended Use type.
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        /// <returns></returns>
        public static LMP.Data.mpSegmentIntendedUses SegmentIntendedUses(int iSegmentIndex)
        {
            try
            {
                return (LMP.Data.mpSegmentIntendedUses)ToDataTable().Rows[iSegmentIndex][FAVORITE_SEGMENT_INTENDED_USE_INDEX];
            }
            catch
            {
                return mpSegmentIntendedUses.AsDocument;
            }
        }
    }
}
