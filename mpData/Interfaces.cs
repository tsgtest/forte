using System;
using System.Collections;

namespace LMP.Data
{
	public interface ILongIDSimpleDataCollectionReader
	{
		int Count{get;}
		LongIDSimpleDataItem ItemFromID(int iID);
		LongIDSimpleDataItem ItemFromIndex(int iIndex);
		Array ToArray();
		Array ToArray(params int[] aFieldIndexes);
		System.Data.DataSet ToDataSet();
	}

	public interface ILongIDSimpleDataCollectionWriter
	{
		LongIDSimpleDataItem Create();
		void Delete(int iID);
		void Save();
		void Save(LongIDSimpleDataItem oItem);
	}

	public interface IStringIDSimpleDataCollectionReader
	{
		int Count{get;}
		StringIDSimpleDataItem ItemFromID(string xID);
		StringIDSimpleDataItem ItemFromIndex(int iIndex);
		Array ToArray();
		Array ToArray(params int[] aFieldIndexes);
		System.Data.DataSet ToDataSet();
	}

	public interface IStringIDSimpleDataCollectionWriter
	{
		StringIDSimpleDataItem Create();
		void Delete(string xID);
		void Save();
		void Save(StringIDSimpleDataItem oItem);
	}

    public interface ISegmentDef
    {
        string Name {get;set;}
        string DisplayName {get;set;}
        mpObjectTypes TypeID { get;set;}
        mpSegmentIntendedUses IntendedUse { get;set;}
        string XML { get; set; }
        int L0 { get;set;}
        int L1 { get;set;}
        int L2 { get;set;}
        int L3 { get;set;}
        int L4 { get;set;}
        short MenuInsertionOptions { get;set;}
        short DefaultMenuInsertionBehavior { get;set;}
        short DefaultDragLocation { get;set;}
        short DefaultDragBehavior { get;set;}
        short DefaultDoubleClickLocation { get;set;}
        short DefaultDoubleClickBehavior { get;set;}
        string HelpText {get;set;}
        string ChildSegmentIDs { get;set;}
        LMP.Data.mpFileFormats BoundingObjectType { get; }
    }
    public interface IFolder
    {
    }
}
