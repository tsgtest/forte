using System;

namespace LMP.Data
{
	public enum mpFirmNameFormats: byte
	{
		None = 0,
		ProperCase = 1,
		UpperCase = 2
	}

	/// <summary>
	/// contains the methods/properties that define a MacPac Address Format
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class AddressFormat:LongIDSimpleDataItem
	{
		private string m_xName = "";
		private bool m_bShowInMacro = false;
		private mpFirmNameFormats m_iFirmNameFormat = LMP.Data.mpFirmNameFormats.None;
		private bool m_bIncludeSlogan = false;
		private string m_xAddressTokenString = "";
		private string m_xDelimiterReplacement = "";

		#region *********************constructors*********************
		internal AddressFormat(){}
		internal AddressFormat(int ID):base(ID){}
		#endregion

		#region *********************properties*********************
		public string AddressTokenString
		{
			get{return m_xAddressTokenString;}
			set{this.SetStringPropertyValue(ref m_xAddressTokenString, value);}
		}

		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}

		public bool ShowInMacro
		{
			get{return m_bShowInMacro;}
			set{this.SetBoolPropertyValue(ref m_bShowInMacro, value);}
		}

		public mpFirmNameFormats FirmNameFormat
		{
			get{return m_iFirmNameFormat;}
			set
			{
				if(m_iFirmNameFormat != value)
				{
					m_iFirmNameFormat = value;
					this.IsDirty = true;
				}
			}
		}

		public bool IncludeSlogan
		{
			get{return m_bIncludeSlogan;}
			set{this.SetBoolPropertyValue(ref m_bIncludeSlogan, value);}
		}

		public string DelimiterReplacement
		{
			get{return m_xDelimiterReplacement;}
			set{this.SetStringPropertyValue(ref m_xDelimiterReplacement, value);}
		}

		#endregion

		#region *********************LongIDSimpleDataItem members*********************
		internal override bool IsValid()
		{
			//return true iff name and address token string are not empty
			return (m_xName != "") && (m_xAddressTokenString != "");
		}

		internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[7];
			oProps[0] = this.Name;
			oProps[1] = this.ShowInMacro;
			oProps[2] = this.FirmNameFormat;
			oProps[3] = this.IncludeSlogan;
			oProps[4] = this.AddressTokenString;
			oProps[5] = this.DelimiterReplacement;
            oProps[6] = Application.GetCurrentEditTime();
			return oProps;
		}
	
		public override object[] ToArray()
		{
			object[] oProps = new object[8];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.ShowInMacro;
			oProps[3] = this.FirmNameFormat;
			oProps[4] = this.IncludeSlogan;
			oProps[5] = this.AddressTokenString;
			oProps[6] = this.DelimiterReplacement;
            oProps[7] = Application.GetCurrentEditTime();
            return oProps;
		}
		#endregion
	}

	/// <summary>
	/// contains the methods and properties that define the 
	/// collection of MacPac address formats - derived from LongIDSimpleDataCollection
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class AddressFormats:LongIDSimpleDataCollection

	{
		private LongIDUpdateObjectDelegate m_oDel;

		#region *********************constructors*********************
		public AddressFormats():base(){}
		#endregion

		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spAddressFormatsAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spAddressFormatsCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spAddressFormatsDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spAddressFormatsItemFromID";}
		}
		protected override string LastIDSprocName
		{
			get{return "spAddressFormatsLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spAddressFormats";}
		}

		protected override object[] SelectSprocParameters
		{
			get{return null;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return null;}
		}

		protected override object[] CountSprocParameters
		{
			get{return null;}
		}

		protected override string UpdateSprocName
		{
			get{return "spAddressFormatsUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new AddressFormat();
		}
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			AddressFormat oFormat = new AddressFormat((int) oValues[0]);
			oFormat.Name = oValues[1].ToString();
			oFormat.ShowInMacro = (bool) oValues[2];
			oFormat.FirmNameFormat = (LMP.Data.mpFirmNameFormats) oValues[3];
			oFormat.IncludeSlogan = (bool) oValues[4];
			oFormat.AddressTokenString = oValues[5].ToString();
			oFormat.DelimiterReplacement = oValues[6].ToString();
			oFormat.IsDirty = false;
			return oFormat;
		}
		#endregion
	}
}
