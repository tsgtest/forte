using System;
using System.Collections;
using System.Text;
using System.Data.OleDb;

namespace LMP.Data
{
    /// <summary>
    /// there are two types of admin folders - 
    /// public and shared
    /// </summary>
    public enum mpAdminFolderTypes
    {
        Public = 1,
        Shared = 2
    }

	/// <summary>
	/// Contains the properties and methods that define a MacPac Folder
	/// created by Jeffrey Sweetland 10/29/04
	/// </summary>
	public class Folder: TranslatableDataItem, IFolder
	{
		private const mpFolderTypes m_bytType = mpFolderTypes.Admin;
		private int m_iParentFolderID = 0;
		private string m_xDescription = null;
		private string m_xName = null;
		private string m_xXML = null;
        private int m_iIndex = 0;

		#region *********************constructors*********************
		internal Folder(){}
		internal Folder(int ID):base(ID){}

		#endregion

		#region *********************properties*********************
		public int ParentFolderID
		{
			get{return m_iParentFolderID;}
			set{this.SetIntPropertyValue(ref m_iParentFolderID, value);}
		}
        public int Index
        {
            get { return m_iIndex; }
            set { this.SetIntPropertyValue(ref m_iIndex, value); }

        }
		public string Description
		{
			get{return m_xDescription;}
			set{this.SetStringPropertyValue(ref m_xDescription, value, 255);}
		}
		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value, 255);}
		}
		public string XML
		{
			get{return m_xXML;}
            set { this.SetStringPropertyValue(ref m_xXML, value, 0); }
        }
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// return FolderMembers collection for this folder
		/// </summary>
		/// <returns></returns>
		public FolderMembers GetMembers()
		{
			if (m_iID == 0)
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_FolderSaveRequired"));
		
			return new FolderMembers(m_iID);
		}
		/// <summary>
		/// Assigns permission to access folder to specified Entity (Person or Group)
		/// </summary>
		/// <param name="iEntityID"></param>
		public void GrantPermission(int iEntityID)
		{
			if (m_iID == 0)
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_FolderSaveRequired"));

			SimpleDataCollection.ExecuteActionSproc("spFoldersGrantPermission", new object[] {m_iID, iEntityID, Application.GetCurrentEditTime()});
		}
		/// <summary>
		/// Revokes permission to access folder for specified Entity (Person or Group)
		/// </summary>
		/// <param name="iEntityID"></param>
		public void RevokePermission(int iEntityID)
		{
			if (m_iID == 0)
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_FolderSaveRequired"));

			SimpleDataCollection.ExecuteActionSproc("spFoldersRevokePermission", 
				new object[] {m_iID, iEntityID});
            //Log deletion of Assignments records
            SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, "101." +
                m_iID.ToString() + "." + iEntityID.ToString());
        }
		/// <summary>
		/// Returns ArrayList containing IDs of entities with permission
		/// to access this folder
		/// </summary>
		/// <returns></returns>
		public ArrayList Permissions()
		{
			if (m_iID == 0)
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_FolderSaveRequired"));

			return SimpleDataCollection.GetArray("spFoldersPermissions", new object[] {m_iID});
		}

        /// <summary>
        /// Get the series of paths to a segment. A segment can exist in several folders
        /// therefore several paths can exist for a single segment.
        /// </summary>
        /// <param name="xSegmentId"></param>
        /// <returns></returns>
        public static string[] GetSegmentPaths(string xSegmentId)
        {
            // Find out what folders contain this segment.
            string [] axSegmentParentFolderIds = GetSegmentParentFolderIds(xSegmentId);
            string [] axSegmentPaths = new string[axSegmentParentFolderIds.Length];

            for (int i = 0; i < axSegmentParentFolderIds.Length; i++)
            {
                string xParentFolderId = axSegmentParentFolderIds[i];
                string xSegmentPath = GetFolderPath(xParentFolderId);
                axSegmentPaths[i] = xSegmentPath;
            }

            return axSegmentPaths;
        }





        /// <summary>
        /// Get the details for the series of paths to a segment. A segment can exist in several folders
        /// therefore several paths can exist for a single segment.
        /// </summary>
        /// <param name="xSegmentId"></param>
        /// <returns></returns>
        public static object [] GetSegmentPathsDetails(string xSegmentId)
        {
            // Find out what folders contain this segment.
            string[] axSegmentParentFolderIds = GetSegmentParentFolderIds(xSegmentId);
            object [] aoSegmentPathsDetails = new object[axSegmentParentFolderIds.Length];

            for (int i = 0; i < axSegmentParentFolderIds.Length; i++)
            {
                string xParentFolderId = axSegmentParentFolderIds[i];
                // Get the details of the path of the parent folder. Recall that the detail is an array of
                // each path segment's name/key value combination. The name/key value combination is stored
                // as an array of strings.
                object [] aoFolderPathDetails = GetFolderPathDetails(xParentFolderId);
                aoSegmentPathsDetails[i] = aoFolderPathDetails;
            }

            return aoSegmentPathsDetails;
        }
        /// <summary>
        /// Get an array of id's for the parent folder's of this segment. A segment can
        /// exist in many folders, ie parent folders. Getting the id's of all these parent
        /// folders is the first step in determining the path of the segment.
        /// </summary>
        /// <param name="xSegmentId"></param>
        /// <returns></returns>
        public static string [] GetSegmentParentFolderIds(string xSegmentId)
        {
            //JTS 3/13/10: Hard-coded SQL to speed up execution
            const string FIND_SQL = "SELECT Assignments.TargetObjectID FROM Assignments WHERE " +
                "Assignments.TargetObjectTypeID=101 And " + 
                "Assignments.AssignedObjectTypeID=100 And Assignments.AssignedObjectID={0}";

            // The admin segment id has this format: A1233
            // Remove the A prefix which designate this as an admin segment.
            xSegmentId = xSegmentId.Substring(1);
            int iSegmentId = int.Parse(xSegmentId);

                //string xProcName = "spAdminSegmentParentFolderId";
                //ArrayList aliParentFolderIds = SimpleDataCollection.GetArray(xProcName, new object[] { iSegmentId });
                string xSql = string.Format(FIND_SQL, iSegmentId);

            ArrayList aliParentFolderIds = SimpleDataCollection.GetArray(xSql);
                //JTS 3/13/10: Remove any Admin Folders that User does not permission to display
                if (!LMP.Data.Application.AdminMode)
                {
                    for (int i = aliParentFolderIds.Count - 1; i >= 0; i--)
                    {
                        if (!LMP.Data.Application.User.HasPermissionForFolder(((object[])aliParentFolderIds[i])[0].ToString()))
                            aliParentFolderIds.RemoveAt(i);
                    }
                }

            string[] axParentFolderIds = new string[aliParentFolderIds.Count];

            int iFolderIdIndex = 0;

            foreach (object[] aoParentFolderIds in aliParentFolderIds)
            {
                axParentFolderIds[iFolderIdIndex++] = ((int)aoParentFolderIds[0]).ToString();
            }

            return axParentFolderIds;
        }

        /// <summary>
        /// Get the path of a given folder which is usually the parent folder of a segment.
        /// </summary>
        /// <param name="xParentFolderId"></param>
        /// <returns></returns>
        private static string GetFolderPath(string xParentFolderId)
        {
            StringBuilder sbPath = new StringBuilder("");

            // Store the path in an arraylist which will grow as the parent folders are determined.
            ArrayList alPaths = new ArrayList();

            do
            {
                // Get the parent folder's name and the id of its parent.
                string[] axParentFolderDetails = GetFolderDetails(xParentFolderId);

                // Set the parent folder id to the id of this parent folder's parent folder for the next iteration in this loop.
                xParentFolderId = axParentFolderDetails[0];

                // Get the name of the parent folder.
                string xParentFolderName = axParentFolderDetails[1];

                // Add the parent folder name to the list of parent folders list.
                alPaths.Add(xParentFolderName);

            } while (xParentFolderId.Length > 0);

            for (int i = alPaths.Count - 1; i >= 0; i--)
            {
                sbPath.Append(alPaths[i] + "/");
            }

            return sbPath.ToString();
        }



        /// <summary>
        /// Get the path details of a given folder which is usually the parent folder of a segment.
        /// </summary>
        /// <param name="xParentFolderId"></param>
        /// <returns></returns>
        private static object [] GetFolderPathDetails(string xParentFolderId)
        {
            // Store the path in an arraylist which will grow as the parent folders are determined.
            ArrayList alPaths = new ArrayList();
            do
            {
                // Get the parent folder's name and the id of its parent.
                string[] axParentFolderDetails = GetFolderDetails(xParentFolderId);

                // Add the parent folder name to the list of parent folders list.
                alPaths.Add(new object[] { axParentFolderDetails[1], xParentFolderId });

                // Set the parent folder id to the id of this parent folder's parent folder for the next iteration in this loop.
                xParentFolderId = axParentFolderDetails[0];
            } while (xParentFolderId.Length > 0);

            object[] aoPathDetails = new object[alPaths.Count];

            for (int i = 0; i < alPaths.Count; i++)
            {
                aoPathDetails[i] = alPaths[alPaths.Count - 1 - i];
            }

            return aoPathDetails;
        }

        /// <summary>
        /// Get the name and the parent folder id of the folder corresponding to the folder id param.
        /// Getting the name facilitates building the path string of a segment. Getting the parent
        /// folder id facilitates getting the name of next folder in the paths heiarchy.
        /// </summary>
        /// <param name="xFolderId"></param>
        /// <returns></returns>
        internal static string[] GetFolderDetails(string xFolderId)
        {
            //JTS 3/13/10: Hard-coded SQL to speed up execution
            const string FIND_SQL = "SELECT Folders.ParentFolderID, Folders.Name FROM Folders " + 
                    "WHERE Folders.ID={0}";
            
            //string xProcName = "spAdminSegmentParentFolderId";
            //ArrayList aliParentFolderIds = SimpleDataCollection.GetArray(xProcName, new object[] { iSegmentId });

            string xRootFolderId = "0";

            if (xFolderId == xRootFolderId)
            {
                return new string[] { "", new FirmApplicationSettings(LMP.Data.Application.User.ID).PublicFoldersLabel};
            }

            //string xProcName = "spAdminFolderParentFolderId";
            //int iFolderId = int.Parse(xFolderId);
            //ArrayList alFolderDetails = SimpleDataCollection.GetArray(xProcName, new object[] { iFolderId });
            
            string xSql = string.Format(FIND_SQL, xFolderId);
            ArrayList alFolderDetails = SimpleDataCollection.GetArray(xSql);

            if (alFolderDetails.Count > 0)
            {
                int iFolderParentId = (int)((object[])alFolderDetails[0])[0];
                string xFolderParentId = iFolderParentId.ToString();
                string xFolderName = (string)((object[])alFolderDetails[0])[1];
                return new string[] { xFolderParentId, xFolderName };
            }
            // We should never get here.
            return new string[] { "", "[NOT FOUND]" };
        }

		#endregion

		#region *********************TranslatableDataItem members*********************
		internal override string TableName
		{
			get{return "Folders";}
		}

		internal override string IDFieldName
		{
			get{return "ID";}
		}

		internal override byte ValuesCount
		{
			get{return 2;}
		}

		internal override bool IsValid()
		{
			//return true iff required info is present
			return (this.Name != "" && this.Name != null && 
				this.Description != "" && this.Description != null);
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[6];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.Description;
			oProps[3] = this.ParentFolderID;
            oProps[4] = this.Index;
			oProps[5] = this.XML;
			return oProps;
		}

		internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[6];
			oProps[0] = this.Name;
			oProps[1] = this.Description;
			oProps[2] = this.ParentFolderID;
            oProps[3] = this.Index;
			oProps[4] = this.XML;
            oProps[5] = Application.GetCurrentEditTime();
			return oProps;
		}
		internal override object[] GetUpdateQueryParameters()
		{
			object[] oProps = new object[7];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.Description;
			oProps[3] = this.ParentFolderID;
            oProps[4] = this.Index;
			oProps[5] = this.XML;
            oProps[6] = Application.GetCurrentEditTime();
			return oProps;
		}
		#endregion
	}
	/// <summary>
	/// contains the methods and properties that manage the 
	/// collection of MacPac Folder definitions - derived from TranslatableDataCollection -
	/// created by Jeffrey Sweetland - 11/03/04
	/// </summary>
	public sealed class Folders: TranslatableDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel = null;
		private int m_iParentFolderIDFilter = -1;
		private int m_iPersonIDFilter = 0;
        
		#region *********************constructors*********************
		public Folders():base(){}

		/// <summary>
		/// Constructor for Folders collection with filter set 
		/// on PersonID
		/// </summary>
		/// <param name="iPersonID"></param>
		/// <param name="iParentFolderID"></param>
		public Folders(int iPersonID):base()
		{
			this.SetFilter(iPersonID);
		}

		/// <summary>
		/// Constructor for Folders collection with filter set 
		/// on PersonID and ParentFolderID
		/// </summary>
		/// <param name="iPersonID"></param>
		/// <param name="iParentFolderID"></param>
		public Folders(int iPersonID, int iParentFolderID):base()
		{
			this.SetFilter(iPersonID, iParentFolderID);
		}

		#endregion

		#region *********************TranslatableDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spFoldersAdd";}
		}

		protected override string CountSprocName
		{
			get
			{
				// Separate query is used if filtered by Group permissions
				if (this.PersonIDFilter == 0)
					return "spFoldersCount";
				else
					return "spFoldersCountByPermission";
			}
		}

		protected override string DeleteSprocName
		{
			get{return "spFoldersDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get
			{
				if (LMP.Resources.CultureName == "en-US")
					return "spFoldersItemFromID";
				else
					return "spFoldersItemFromIDTranslated";
			}
		}

		protected override string LastIDSprocName
		{
			get{return "spFoldersLastID";}
		}

		protected override string SelectSprocName
		{
			get
			{
				if (LMP.Resources.CultureName == "en-US")
					if (this.PersonIDFilter == 0)
						return "spFolders";
					else
						return "spFoldersByPermission";
				else
					if (this.PersonIDFilter == 0)
						return "spFoldersTranslated";
					else
						return "spFoldersTranslatedByPermission";
			}
		}

		protected override object[] SelectSprocParameters
		{
			get 
			{
				int iParams = 1;

				if (this.PersonIDFilter != 0)
					//by permission
					iParams++;

				if (LMP.Resources.CultureName != "en-US")
					//translated
					iParams++;

				object[] oParams = new object[iParams];

				//all queries take parent folder parameter
				if (this.ParentFolderIDFilter == -1)
					oParams[0] = System.DBNull.Value;
				else
					oParams[0] = (int) this.ParentFolderIDFilter;

				//by permission
				if (this.PersonIDFilter > 0)
				{
					// Get Group list for specified Person
					StringBuilder oBldr = new StringBuilder();

                    //append people groups
					object[] aIDs = LocalPersons.GetPeopleGroupIDs(this.PersonIDFilter);
					foreach(object o in aIDs)
						oBldr.AppendFormat(",{0}", ((object[]) o)[0].ToString());

                    //append offices
                    aIDs = LocalPersons.GetOfficeIDs(this.PersonIDFilter);
                    foreach (object o in aIDs)
                        oBldr.AppendFormat(",{0}", ((object[])o)[0].ToString());

					oParams[1] = oBldr.ToString();
				}
				else if (this.PersonIDFilter < 0)
					// ID specifies a Group or Office
					oParams[1] = this.PersonIDFilter.ToString();

				//translated queries require locale
				if (LMP.Resources.CultureName != "en-US")
					oParams[iParams - 1] = LMP.Resources.CultureID;

				return oParams; 
			}
		}

		protected override object[] CountSprocParameters
		{
			get {return this.SelectSprocParameters;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get
			{
				//no need to filter
				if (Language.CultureID == 1033)
					return null;
				else
					return new object[] {LMP.Resources.CultureID};
			}
		}

		protected override string UpdateSprocName
		{
			get{return "spFoldersUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new Folder();
		}

		public LongIDSimpleDataItem Create(int iParentFolderID)
		{
			LMP.Data.Folder oFolder = new Folder();
			oFolder.ParentFolderID = iParentFolderID;
			return oFolder;
		}

		public override void Delete(int iID)
		{
			// Delete child folders recursively
			Folders oChildFolders = new Folders();
			oChildFolders.m_iParentFolderIDFilter = iID;
			for (int i = oChildFolders.Count; i > 0; i--)
				oChildFolders.Delete(oChildFolders.ItemFromIndex(i).ID);
			// delete existing foldermembers - this will also delete unused Segments
            FolderMembers oMembers = new FolderMembers(iID);
            for (int i = oMembers.Count; i > 0; i--)
                oMembers.Delete(oMembers.ItemFromIndex(i).ID);
            base.Delete(iID);
            // remove existing permissions
			TranslatableDataCollection.ExecuteActionSproc("spFoldersRevokePermission", 
				new object[] {iID, System.DBNull.Value});
            // Clear SharedFolderID field of any shared objects
            TranslatableDataCollection.ExecuteActionSproc("spSharedFolderDeleteUserSegments",
                new object[] { iID, Application.GetCurrentEditTime() });
            TranslatableDataCollection.ExecuteActionSproc("spSharedFolderDeleteVariableSets",
                new object[] { iID, Application.GetCurrentEditTime() });
        }
		#endregion

		#region *********************properties*********************
		public int PersonIDFilter
		{
			get{return m_iPersonIDFilter;}
		}
		public int ParentFolderIDFilter
		{
			get{return m_iParentFolderIDFilter;}
		}
		#endregion

        #region *********************static members*********************
        /// <summary>
        /// returns an array list of folder ids that
        /// contain the specified segment
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <returns></returns>
        public static ArrayList GetSegmentFolderIDs(int iSegmentID){
            return SimpleDataCollection.GetArray("spSegmentFolders", new object[] { iSegmentID });
        }
        #endregion

        #region *********************methods*********************
        public void SetFilter(int iPersonID, int iParentFolderID)
		{
			Trace.WriteNameValuePairs("iPersonID", 
				iPersonID, "iParentFolderID", iParentFolderID);

			if(iPersonID != this.PersonIDFilter
				|| iParentFolderID != this.ParentFolderIDFilter)
			{
				//filter has changed - 
				//alert if new value is invalid
				if((iPersonID != 0) && !LocalPersons.Exists(iPersonID))
				{
					//person with this ID does not exist
					throw new LMP.Exceptions.PersonIDException(
						LMP.Resources.GetLangString("Error_InvalidPersonID") + iPersonID);
				}
				m_iPersonIDFilter = iPersonID;
				m_iParentFolderIDFilter = iParentFolderID;
                m_oArray = null;
			}
		}

		public void SetFilter(int iPersonID)
		{
			SetFilter(iPersonID, 0);
		}
        /// <summary>
        /// returns 2-column array with Name and ID of each member of collection
        /// </summary>
        /// <returns></returns>
        public ArrayList ToDisplayArray()
        {
            DateTime t0 = DateTime.Now;
            string xSQL;
            if (LMP.Resources.CultureName == "en-US")
                if (this.PersonIDFilter == 0)
                    xSQL = "spFoldersForDisplay";
                else
                    xSQL =  "spFoldersForDisplayByPermission";
            else
                if (this.PersonIDFilter == 0)
                    xSQL = "spFoldersForDisplayTranslated";
                else
                    xSQL= "spFoldersForDisplayTranslatedByPermission";

            OleDbDataReader oReader = null;

            try
            {
                //get reader with display fields only
                oReader = SimpleDataCollection.GetResultSet(xSQL, false, this.SelectSprocParameters);

                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
            }
        }
        /// <summary>
        /// Returns High Index among Folders in this collection
        /// </summary>
        /// <returns></returns>
        public int GetHighIndex()
        {
            object oRet = GetScalar("spFoldersLastIndex", new object[] { this.ParentFolderIDFilter });
            if (oRet != null && oRet != DBNull.Value)
                return (int)oRet;
            else
                return 0;
        }
        #endregion

		#region *********************private functions*********************
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			Folder oDef = new Folder((int) oValues[0]);
			oDef.Name = oValues[1].ToString();
			oDef.Description = oValues[2].ToString();
			if (oValues[3]!=DBNull.Value)
				oDef.TranslationID = (int) oValues[3];
			oDef.ParentFolderID  = (int) oValues[4];
            if (oValues[5] != DBNull.Value)
                oDef.Index = (int) oValues[5];
            oDef.XML = oValues[6].ToString();
            oDef.IsDirty = false;
			return oDef;
		}

		#endregion
	}
}
