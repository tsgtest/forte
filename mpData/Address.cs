using System;
using System.Collections;
using System.Data.OleDb;
using LMP;
using LMP.Data;
using LMP.Exceptions;

namespace LMP.Data
{
	/// <summary>
	/// Contains the properties and methods of a MacPac Address
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Address: LongIDSimpleDataItem
	{
		string m_xLine1 = "";
		string m_xLine2 = "";
		string m_xLine3 = "";
		string m_xPreCity = "";
		string m_xCity = "";
		string m_xPostCity = "";
		string m_xCounty = "";
		string m_xState = "";
		string m_xStateAbbr = "";
		string m_xCountry = "";
		string m_xZip = "";
		string m_xTemplate = "";
		string m_xPhone1 = "";
		string m_xPhone2 = "";
		string m_xPhone3 = "";
		string m_xFax1 = "";
		string m_xFax2 = "";

		#region *********************constructors*********************
		internal Address():base(){}
		internal Address(int ID):base(ID){}
		#endregion

		#region *********************properties*********************
		public string Line1
		{
			get{return m_xLine1;}
			set{this.SetStringPropertyValue(ref m_xLine1, value);}
		}

		public string Line2
		{
			get{return m_xLine2;}
			set{this.SetStringPropertyValue(ref m_xLine2, value);}
		}

		public string Line3
		{
			get{return m_xLine3;}
			set{this.SetStringPropertyValue(ref m_xLine3, value);}
		}

		public string PreCity
		{
			get{return m_xPreCity;}
			set{this.SetStringPropertyValue(ref m_xPreCity, value);}
		}
		
		public string City
		{
			get{return m_xCity;}
			set{this.SetStringPropertyValue(ref m_xCity, value);}
		}

		public string PostCity
		{
			get{return m_xPostCity;}
			set{this.SetStringPropertyValue(ref m_xPostCity, value);}
		}

		public string County
		{
			get{return m_xCounty;}
			set
			{
				if(m_xCounty != value)
				{
					if (value.Length > 255)
					{
						throw new LMP.Exceptions.DataException(
							Resources.GetLangString("Error_PropertyCharacterOverflow"));
					}

					m_xCounty = value;
					this.IsDirty = true;
				}
			}
		}
	
		public string State
		{
			get{return m_xState;}
			set{this.SetStringPropertyValue(ref m_xState, value);}
		}

		public string StateAbbr
		{
			get{return m_xStateAbbr;}
			set{this.SetStringPropertyValue(ref m_xStateAbbr, value);}
		}

		public string Country
		{
			get{return m_xCountry;}
			set{this.SetStringPropertyValue(ref m_xCountry, value);}
		}

		public string Zip
		{
			get{return m_xZip;}
			set{this.SetStringPropertyValue(ref m_xZip, value);}
		}

		public string Template
		{
			get{return m_xTemplate;}
			set{this.SetStringPropertyValue(ref m_xTemplate, value);}
		}

		public string Phone1
		{
			get{return m_xPhone1;}
			set{this.SetStringPropertyValue(ref m_xPhone1, value);}
		}

		public string Phone2
		{
			get{return m_xPhone2;}
			set{this.SetStringPropertyValue(ref m_xPhone2, value);}
		}

		public string Phone3
		{
			get{return m_xPhone3;}
			set{this.SetStringPropertyValue(ref m_xPhone3, value);}
		}

		public string Fax1
		{
			get{return m_xFax1;}
			set{this.SetStringPropertyValue(ref m_xFax1, value);}
		}

		public string Fax2
		{
			get{return m_xFax2;}
			set{this.SetStringPropertyValue(ref m_xFax2, value);}
		}

		#endregion

		#region *********************LongIDSimleDataItemMembers*********************
		public override object[] ToArray()
		{
			object[] oProps = new object[18];
			oProps[0] = this.ID;
			oProps[1] = this.Line1;
			oProps[2] = this.Line2;
			oProps[3] = this.Line3;
			oProps[4] = this.PreCity;
			oProps[5] = this.City;
			oProps[6] = this.PostCity;
			oProps[7] = this.County;
			oProps[8] = this.State;
			oProps[9] = this.StateAbbr;
			oProps[10] = this.Country;
			oProps[11] = this.Zip;
			oProps[12] = this.Template;
			oProps[13] = this.Phone1;
			oProps[14] = this.Phone2;
			oProps[15] = this.Phone3;
			oProps[16] = this.Fax1;
			oProps[17] = this.Fax2;

			return oProps;
		}

        internal override object[] GetUpdateQueryParameters()
        {
            object[] oProps = new object[19];
            oProps[0] = this.ID;
            oProps[1] = this.Line1;
            oProps[2] = this.Line2;
            oProps[3] = this.Line3;
            oProps[4] = this.PreCity;
            oProps[5] = this.City;
            oProps[6] = this.PostCity;
            oProps[7] = this.County;
            oProps[8] = this.State;
            oProps[9] = this.StateAbbr;
            oProps[10] = this.Country;
            oProps[11] = this.Zip;
            oProps[12] = this.Template;
            oProps[13] = this.Phone1;
            oProps[14] = this.Phone2;
            oProps[15] = this.Phone3;
            oProps[16] = this.Fax1;
            oProps[17] = this.Fax2;
            oProps[18] = Application.GetCurrentEditTime();

            return oProps;
        }
        internal override object[] GetAddQueryParameters()
		{
			object[] oParams = new object[18];

			oParams[0] = this.Line1;
			oParams[1] = this.Line2;
			oParams[2] = this.Line3;
			oParams[3] = this.PreCity;
			oParams[4] = this.City;
			oParams[5] = this.PostCity;
			oParams[6] = this.County;
			oParams[7] = this.State;
			oParams[8] = this.StateAbbr;
			oParams[9] = this.Country;
			oParams[10] = this.Zip;
			oParams[11] = this.Template;
			oParams[12] = this.Phone1;
			oParams[13] = this.Phone2;
			oParams[14] = this.Phone3;
			oParams[15] = this.Fax1;
			oParams[16] = this.Fax2;
            oParams[17] = Application.GetCurrentEditTime();
			return oParams;
		}

		internal override bool IsValid()
		{
			//item is valid if a template is specified
			return (m_xTemplate != "");
		}
		#endregion

		#region *********************methods*********************
		public string ToString(string xAddressTokenString, string xDelimiterReplacement)
		{
			if(xAddressTokenString == "")
				xAddressTokenString ="[StandardAddress]";

			//process character field codes first
			string xModAddr = LMP.String.ReplaceCharFieldCodes(xAddressTokenString);

            //replace standard address field code with address template
            xModAddr = xModAddr.Replace("[StandardAddress]", this.Template);

            //place in StringBuilder for efficiency
			//TODO: We may need to provide culture info objects as IFormatProvider to 
			//StringBuilder.AppendFormat -let's keep this in mind.
			System.Text.StringBuilder oSB = new System.Text.StringBuilder();
			oSB.Append(xModAddr);

			//replace all other tokens
			oSB.Replace("[Line1]", this.Line1);
			oSB.Replace("[Line2]", this.Line2);
			oSB.Replace("[Line3]", this.Line3);
			oSB.Replace("[PreCity]", this.PreCity);
			oSB.Replace("[City]", this.City);
			oSB.Replace("[PostCity]", this.PostCity);
			oSB.Replace("[County]", this.County);
			oSB.Replace("[State]", this.State);
			oSB.Replace("[StateAbbr]", this.StateAbbr);
			oSB.Replace("[Zip]", this.Zip);
			oSB.Replace("[Country]", this.Country);
			oSB.Replace("[Phone1]", this.Phone1);
			oSB.Replace("[Phone2]", this.Phone2);
			oSB.Replace("[Phone3]", this.Phone3);
			oSB.Replace("[Fax1]", this.Fax1);
			oSB.Replace("[Fax2]", this.Fax2);
			
			//country if foreign
            if (xModAddr.IndexOf("[CountryIfForeign]") > -1)
            {
                Address oUserAddress = Application.User.Office.GetAddress();
                if (oUserAddress.Country != this.Country)
                    oSB.Replace("[CountryIfForeign]", this.Country);
                else
                {
                    //try and remove extraneous chars before country field code
                    oSB.Replace(", [CountryIfForeign]", "");
                    oSB.Replace("  [CountryIfForeign]", "");
                    oSB.Replace(" [CountryIfForeign]", "");
                    oSB.Replace("[CountryIfForeign]", "");
                }
            }

            //remove extraneous pipes that may exist due to empty address info
            oSB.Replace("|||", "|");
            oSB.Replace("||", "|");

            //convert back to string and trim leading and trailing pipes
            xModAddr = oSB.ToString();
            xModAddr = xModAddr.Trim('|');
			    
            //replace pipes with specified delimiter
            if (xDelimiterReplacement == null)
                xDelimiterReplacement = "\v";
            xModAddr = xModAddr.Replace("|", xDelimiterReplacement);

			return xModAddr;
		}
		#endregion
	}

	/// <summary>
	/// contains the methods and properties that define a MacPac 
	/// Address collection - derived from LongIDSimpleDataCollection
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Addresses: LongIDSimpleDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel;

		#region *********************constructors*********************
		public Addresses():base(){}
		#endregion

		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spAddressesAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spAddressesCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spAddressesDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spAddressesItemFromID";}
		}
		protected override string LastIDSprocName
		{
			get{return "spAddressesLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spAddresses";}
		}

		protected override object[] SelectSprocParameters
		{
			get{return null;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return null;}
		}

		protected override object[] CountSprocParameters
		{
			get{return null;}
		}

		protected override string UpdateSprocName
		{
			get{return "spAddressesUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new Address();
		}

		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			Address oAddress = new Address((int) oValues[0]);
			oAddress.Line1 = oValues[1].ToString();
			oAddress.Line2 = oValues[2].ToString();
			oAddress.Line3 = oValues[3].ToString();
			oAddress.PreCity = oValues[4].ToString();
			oAddress.City = oValues[5].ToString();
			oAddress.PostCity = oValues[6].ToString();
			oAddress.County = oValues[7].ToString();
			oAddress.State = oValues[8].ToString();
			oAddress.StateAbbr = oValues[9].ToString();
			oAddress.Country = oValues[10].ToString();
			oAddress.Zip = oValues[11].ToString();
			oAddress.Template = oValues[12].ToString();
			oAddress.Phone1 = oValues[13].ToString();
			oAddress.Phone2 = oValues[14].ToString();
			oAddress.Phone3 = oValues[15].ToString();
			oAddress.Fax1 = oValues[16].ToString();
			oAddress.Fax2 = oValues[17].ToString();
			oAddress.IsDirty = false;

			return oAddress;
		}
		#endregion
	}
}
