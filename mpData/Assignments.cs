using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.OleDb;

namespace LMP.Data
{
    public class Assignment
    {
        #region *********************fields*********************
        private mpObjectTypes m_iTargetObjectType;
        private mpObjectTypes m_iAssignedObjectType;
        private int m_iTargetObjectID;
        private int m_iAssignedObjectID;
        #endregion
        #region *********************constructors*********************
        internal Assignment() { }
        #endregion
        #region *********************properties*********************
        public mpObjectTypes TargetObjectType
        {
            get { return m_iTargetObjectType; }
            set { m_iTargetObjectType = value; }
        }
        public mpObjectTypes AssignedObjectType
        {
            get { return m_iAssignedObjectType; }
            set { m_iAssignedObjectType = value; }
        }
        public int TargetObjectID
        {
            get { return m_iTargetObjectID; }
            set { m_iTargetObjectID = value; }
        }
        public int AssignedObjectID
        {
            get { return m_iAssignedObjectID; }
            set { m_iAssignedObjectID = value; }
        }
        #endregion
        #region *********************methods*********************
        public override string ToString()
        {
            return ToString("|");
        }
        public string ToString(string xSeparator)
        {
            return ((int)this.TargetObjectType).ToString() + xSeparator + 
                this.TargetObjectID.ToString() + xSeparator + 
                ((int)this.AssignedObjectType).ToString() + xSeparator + 
                this.AssignedObjectID.ToString();
        }
        #endregion
    }

    public class Assignments
    {
        #region *********************fields*********************
        private mpObjectTypes m_iTargetObjectType;
        private mpObjectTypes m_iAssignedObjectType;
        private int m_iTargetObjectID;
        private ArrayList m_oAssignments;
        #endregion
        #region *********************constructors*********************
        /// <summary>
        /// creates a new assignments class
        /// </summary>
        /// <param name="iTargetObjectType"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iAssignedObjectType"></param>
        public Assignments(mpObjectTypes iTargetObjectType, int iTargetObjectID,
            mpObjectTypes iAssignedObjectType)
        {
            this.m_iTargetObjectType = iTargetObjectType;
            this.m_iAssignedObjectType = iAssignedObjectType;
            this.m_iTargetObjectID = iTargetObjectID;
        }
        #endregion
        #region *********************properties*********************
        public mpObjectTypes TargetObjectType
        {
            get { return m_iTargetObjectType; }
            set { m_iTargetObjectType = value; }
        }
        public mpObjectTypes AssignedObjectType
        {
            get { return m_iAssignedObjectType; }
            set { m_iAssignedObjectType = value; }
        }
        public int TargetObjectID
        {
            get { return m_iTargetObjectID; }
            set { m_iTargetObjectID = value; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// adds new assignment to the collection, returns new object
        /// </summary>
        /// <param name="iAssignedObjectID"></param>
        /// <returns></returns>
        public Assignment Add(int iAssignedObjectID)
        {
            try
            {
                //save new assignment to db
                SimpleDataCollection.ExecuteActionSproc("spAssignmentsAdd",
                    new object[] {(int)this.m_iTargetObjectType, this.m_iTargetObjectID, 
                    (int)this.m_iAssignedObjectType, iAssignedObjectID});
            }
            catch (System.Exception oE)
            {
                //alert that assignment could not be added
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotAddAssignment") + 
                    iAssignedObjectID.ToString(), oE);
            }

            //clear assignments arraylist to force a refresh
            m_oAssignments = null;

            //create new object
            Assignment oA = new Assignment();
            oA.TargetObjectID = this.m_iTargetObjectID;
            oA.AssignedObjectID = iAssignedObjectID;
            oA.TargetObjectType = this.m_iTargetObjectType;
            oA.AssignedObjectType = this.m_iAssignedObjectType;

            return oA;
        }

        /// <summary>
        /// adds a new assignment
        /// </summary>
        /// <param name="iTargetObjectType"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iAssignedObjectType"></param>
        /// <param name="iAssignedObjectID"></param>
        /// <returns></returns>
        public static Assignment Add(mpObjectTypes iTargetObjectType, int iTargetObjectID,
            mpObjectTypes iAssignedObjectType, int iAssignedObjectID)
        {
            //create new object
            Assignment oA = new Assignment();
            oA.TargetObjectID = iTargetObjectID;
            oA.AssignedObjectID = iAssignedObjectID;
            oA.TargetObjectType = iTargetObjectType;
            oA.AssignedObjectType = iAssignedObjectType;

            //save to db
            SimpleDataCollection.ExecuteActionSproc("spAssignmentsAdd",
                new object[] { (int)iTargetObjectType, iTargetObjectID, 
                    (int)iAssignedObjectType, iAssignedObjectID,
                    Application.GetCurrentEditTime()});

            return oA;
        }

        /// <summary>
        /// deletes the assignment with the 
        /// specified iAssignedObjectID from the collection
        /// </summary>
        /// <param name="iAssignedObjectID"></param>
        public void Delete(int iAssignedObjectID)
        {
            try
            {
                //delete
                SimpleDataCollection.ExecuteActionSproc("spAssignmentsDelete",
                    new object[] {(int)this.m_iTargetObjectType, this.m_iTargetObjectID, 
                    (int)this.m_iAssignedObjectType, iAssignedObjectID});

                //log deletion
                SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, iAssignedObjectID);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotDeleteAssignment") + iAssignedObjectID, oE);
            }

            //force refresh of internal array
            m_oAssignments = null;
            
        }

        /// <summary>
        /// deletes the specified assignment
        /// </summary>
        /// <param name="iTargetObjectType"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iAssignedObjectType"></param>
        /// <param name="iAssignedObjectID"></param>
        /// <returns></returns>
        public static void Delete(mpObjectTypes iTargetObjectType, int iTargetObjectID,
            mpObjectTypes iAssignedObjectType, int iAssignedObjectID)
        {
            //delete from db
            SimpleDataCollection.ExecuteActionSproc("spAssignmentsDelete",
                new object[] { (int)iTargetObjectType, iTargetObjectID, (int)iAssignedObjectType, iAssignedObjectID });
            //GLOG 3959: Log individual Assignment deletion
            SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, ((int)iTargetObjectType).ToString() + "." +
                iTargetObjectID.ToString() + "." + ((int)iAssignedObjectType).ToString() + "." + iAssignedObjectID.ToString());
        }
        
        /// <summary>
        /// returns the assignment with the specified ID
        /// </summary>
        /// <param name="iObject2ID"></param>
        /// <returns></returns>
        public Assignment ItemFromID(int iObject2ID)
        {
            OleDbDataReader oReader = null;
            try
            {
                if (Language.CultureID == Language.mpUSEnglishCultureID)
                    //get the item in US English
                    oReader = SimpleDataCollection.GetResultSet("spAssignmentsItemFromID", true,
                        new object[] {(int)this.m_iTargetObjectType, this.m_iTargetObjectID, 
                    (int)this.m_iAssignedObjectType, iObject2ID});
                else
                    //get the translated item
                    oReader = SimpleDataCollection.GetResultSet("spAssignmentsTranslatedItemFromID",
                        true, new object[] {(int)this.m_iTargetObjectType, this.m_iTargetObjectID, 
                    (int)this.m_iAssignedObjectType, Language.CultureID, iObject2ID});

                //alert if no item found
                if (!oReader.HasRows)
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iObject2ID);

                //read returned row
                oReader.Read();
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }

            //build assignment
            Assignment oA = new Assignment();

            oA.TargetObjectID = this.m_iTargetObjectID;
            oA.TargetObjectType = this.m_iTargetObjectType;
            oA.AssignedObjectType = this.m_iAssignedObjectType;
            oA.AssignedObjectID = (int)oReader.GetValue(0);

            return oA;
        }

        /// <summary>
        /// returns the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public Assignment ItemFromIndex(int iIndex)
        {
            //populate array if necessary
            if (m_oAssignments == null)
                PopulateArrayList();

            //raise error if index is out of bounds
            if(iIndex < 0 || iIndex > m_oAssignments.Count - 1)
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString(
                    "Error_ItemAtIndexNotInCollection") + iIndex.ToString());

            //build assignment
            Assignment oA = new Assignment();
            oA.TargetObjectType = this.m_iTargetObjectType;
            oA.AssignedObjectType = (mpObjectTypes)((object[])m_oAssignments[iIndex])[2];
            oA.TargetObjectID = this.m_iTargetObjectID;
            oA.AssignedObjectID = (int)((object[])m_oAssignments[iIndex])[0];

            //return assignment
            return oA;
        }
            
        /// <summary>
        /// returns the number of assignments in the collection
        /// </summary>
        public int Count
        {
            get
            {
                return (int)SimpleDataCollection.GetScalar("spAssignmentsCount",
                new object[] { (int)this.m_iTargetObjectType, this.m_iTargetObjectID, (int)this.m_iAssignedObjectType });
            }
        }

        /// <summary>
        /// returns an ArrayList of assignments
        /// </summary>
        /// <returns></returns>
        public ArrayList ToArrayList()
        {
            //retrieve assignments array if not yet retrieved
            if (m_oAssignments == null)
                PopulateArrayList();

            return m_oAssignments;
        }
        public string ToString(string xSeparator)
        {
            StringBuilder oSB = new StringBuilder();

            for (int i = 0; i < this.Count; i++)
            {
                Assignment oAssignment = (Assignment)this.ItemFromIndex(i);

                //append segment to file
                if (i == this.Count - 1)
                    //appending last assignment - don't
                    //add trailing separator
                    oSB.Append(oAssignment.ToString(xSeparator));
                else
                    oSB.Append(oAssignment.ToString(xSeparator) + xSeparator);
            }

            return oSB.ToString();
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// populates the ArrayList field
        /// </summary>
        private void PopulateArrayList()
        {
            if (Language.CultureID == Language.mpUSEnglishCultureID)
                //return assignments in English
                m_oAssignments = SimpleDataCollection.GetArray("spAssignments",
                    new object[] { (int)this.m_iTargetObjectType, 
                        this.m_iTargetObjectID, (int)this.m_iAssignedObjectType });
            else
                //return translated assignments
                m_oAssignments = SimpleDataCollection.GetArray("spAssignmentsTranslated",
                    new object[] { (int)this.m_iTargetObjectType, this.m_iTargetObjectID, 
                        (int)this.m_iAssignedObjectType, Language.CultureID });
        }

        #endregion
        #region *********************static methods*********************
        public static DataTable GetAvailableAssignments(mpObjectTypes iTargetObjectType,
            int iTargetObjectID, mpObjectTypes iAssignedObjectType)
        {
            object[] aParams = { iTargetObjectType, iTargetObjectID, iAssignedObjectType };

            if ((int)iAssignedObjectType < 101 || (int)iAssignedObjectType > 400)
                //get segment-type assignments - there's a 
                //query to retrieve all segment-type assignments
                return SimpleDataCollection.GetDataSet(
                    "spAssignmentsAvailableSegments", aParams).Tables[0];
            else
            {
                //get other types of assignments - other queries needed
                //TODO: implement this branch
                return null;
            }
        }
        //GLOG : 3870 : JSW
        //query to retrieve parent assignemts for child segments
        public static DataTable GetParentAssignments(int iChildObjectID,
            mpObjectTypes iChildObjectType)
        {
            object[] aParams = { iChildObjectID, iChildObjectType};

                return SimpleDataCollection.GetDataSet(
                    "spAssignmentsParentSegments", aParams).Tables[0];
        }        
        #endregion
    }
}
