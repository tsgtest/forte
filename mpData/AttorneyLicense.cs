using System;

namespace LMP.Data
{
	/// <summary>
	/// contains the properties and methods of a MacPac AttorneyLicense object -
	/// derived from StringIDSimpleDataItem - created by Doug Miller 10/4/04
	/// </summary>
	public class AttorneyLicense: StringIDSimpleDataItem
	{
		#region constructors
		internal AttorneyLicense():base(){}
		#endregion

		#region properties
		private string m_xOwnerID = null;
		private string m_xDescription = null;
		private string m_xLicense = null;
		private int m_iL0 = 0;
		private int m_iL1 = 0;
		private int m_iL2 = 0;
		private int m_iL3 = 0;
		private int m_iL4 = 0;
        private DateTime m_dLastEditTime;

		public string OwnerID
		{
			get{return m_xOwnerID;}
		}

		public string Description
		{
			get{return m_xDescription;}
			set{this.SetStringPropertyValue(ref m_xDescription, value);}
		}

		public string License
		{
			get{return m_xLicense;}
			set{this.SetStringPropertyValue(ref m_xLicense, value);}
		}

		public int L0
		{
			get{return m_iL0;}
			set
			{
				if (value < 0)
				{
					throw new LMP.Exceptions.ValueException(Resources
						.GetLangString("Error_PropertyNegativeValue"));
				}
				this.SetIntPropertyValue(ref m_iL0, value);
			}
		}

		public int L1
		{
			get{return m_iL1;}
			set
			{
				if (value < 0)
				{
					throw new LMP.Exceptions.ValueException(Resources
						.GetLangString("Error_PropertyNegativeValue"));
				}
				this.SetIntPropertyValue(ref m_iL1, value);
			}
		}

		public int L2
		{
			get{return m_iL2;}
			set
			{
				if (value < 0)
				{
					throw new LMP.Exceptions.ValueException(Resources
						.GetLangString("Error_PropertyNegativeValue"));
				}
				this.SetIntPropertyValue(ref m_iL2, value);
			}
		}

		public int L3
		{
			get{return m_iL3;}
			set
			{
				if (value < 0)
				{
					throw new LMP.Exceptions.ValueException(Resources
						.GetLangString("Error_PropertyNegativeValue"));
				}
				this.SetIntPropertyValue(ref m_iL3, value);
			}
		}

		public int L4
		{
			get{return m_iL4;}
			set
			{
				if (value < 0)
				{
					throw new LMP.Exceptions.ValueException(Resources
						.GetLangString("Error_PropertyNegativeValue"));
				}
				this.SetIntPropertyValue(ref m_iL4, value);
			}
		}
        internal DateTime LastEditTime
        {
            get { return m_dLastEditTime; }
            set { m_dLastEditTime = value; }
        }
		#endregion

		#region methods
		internal void SetOwnerID(string xOwnerID)
		{
			this.SetStringPropertyValue(ref m_xOwnerID, xOwnerID);
		}

		internal override bool IsValid()
		{
			return ((this.OwnerID != null) &&
				(this.License != "") && (this.License != null));
		}

		internal override object[] GetAddQueryParameters()
		{
			int iID1 = 0;
			int iID2 = 0;
			object[] oParams = new object[12];

			Application.SplitID(this.ID, out iID1, out iID2);
			oParams[0] = iID1;
			oParams[1] = iID2;

			LocalPersons.SplitID(this.OwnerID, out iID1, out iID2);
			oParams[2] = iID1;
			oParams[3] = iID2;

			oParams[4] = this.Description;
			oParams[5] = this.License;
			oParams[6] = this.L0;
			oParams[7] = this.L1;
			oParams[8] = this.L2;
			oParams[9] = this.L3;
			oParams[10] = this.L4;
            //GLOG 3691: Don't change date if copying Network licenses
            oParams[11] = LastEditTime.Year < 1900 ? Application.GetCurrentEditTime() : this.LastEditTime.ToString();
			return oParams;
		}

		public override object[] ToArray()
		{
			object[] oVals = new object[9];
			oVals[0] = this.ID;
			oVals[1] = this.OwnerID;
			oVals[2] = this.Description;
			oVals[3] = this.License;
			oVals[4] = this.L0;
			oVals[5] = this.L1;
			oVals[6] = this.L2;
			oVals[7] = this.L3;
			oVals[8] = this.L4;

			return oVals;
		}
	
		internal override object[] GetUpdateQueryParameters()
		{
			int iID1;
			int iID2;

			Application.SplitID(this.ID, out iID1, out iID2);

			object[] oVals = new object[10];
			oVals[0] = iID1;
			oVals[1] = iID2;
			oVals[2] = this.Description;
			oVals[3] = this.License;
			oVals[4] = this.L0;
			oVals[5] = this.L1;
			oVals[6] = this.L2;
			oVals[7] = this.L3;
			oVals[8] = this.L4;
            //GLOG 3691: Don't change date if copying Network licenses
            oVals[9] = this.LastEditTime.Year < 1900 ? Application.GetCurrentEditTime() : this.LastEditTime.ToString();

			return oVals;
		}
		#endregion
	}

	/// <summary>
	/// contains the properties and methods that 
	/// manage the MacPac AttorneyLicense collection - 
	/// derived from StringIDSimpleDataCollection - created by Doug Miller 10/4/04
	/// </summary>
	public class AttorneyLicenses:StringIDSimpleDataCollection
	{
		private StringIDUpdateObjectDelegate m_oDel = null;
		private string m_xOwnerUserIDFilter = null;
        private int m_iL0 = 0;
        private int m_iL1 = 0;
        private int m_iL2 = 0;
        private int m_iL3 = 0;
        private int m_iL4 = 0;
        private bool m_bLocalPublicPeople = false;  //8316

		#region *********************constructors*********************
		public AttorneyLicenses():base(){}
        //GLOG 8316
        public AttorneyLicenses(string xOwnerIDFilter, bool bLocalPublicPeople) : base()
        {
            //initialize filter
            m_xOwnerUserIDFilter = xOwnerIDFilter;
            m_bLocalPublicPeople = bLocalPublicPeople;
        }
        public AttorneyLicenses(string xOwnerIDFilter)
            : base()
		{
			//initialize filter
			m_xOwnerUserIDFilter= xOwnerIDFilter;
            m_bLocalPublicPeople = false;
		}
        //GLOG 8316
        public AttorneyLicenses(string xOwnerIDFilter, int L0, int L1, int L2, int L3, int L4, bool bLocalPublicPeople)
            : base()
        {
            //initialize filter
            m_xOwnerUserIDFilter = xOwnerIDFilter;
            m_bLocalPublicPeople = bLocalPublicPeople;
            if (L0 < 0)
                L0 = 0;
            m_iL0 = L0;
            if (L1 < 0)
                L1 = 0;
            m_iL1 = L1;
            if (L2 < 0)
                L2 = 0;
            m_iL2 = L2;
            if (L3 < 0)
                L3 = 0;
            m_iL3 = L3;
            if (L4 < 0)
                L4 = 0;
            m_iL4 = L4;
        }
        public AttorneyLicenses(string xOwnerIDFilter, int L0, int L1, int L2, int L3, int L4)
            : base()
        {
            //initialize filter
            m_xOwnerUserIDFilter = xOwnerIDFilter;
            if (L0 < 0)
                L0 = 0;
            m_iL0 = L0;
            if (L1 < 0)
                L1 = 0;
            m_iL1 = L1;
            if (L2 < 0)
                L2 = 0;
            m_iL2 = L2;
            if (L3 < 0)
                L3 = 0;
            m_iL3 = L3;
            if (L4 < 0)
                L4 = 0;
            m_iL4 = L4;
        }

		#endregion

		#region *********************properties*********************
		/// <summary>
		/// returns the Owner ID filter
		/// </summary>
		public string OwnerIDFilter
		{
			get{return m_xOwnerUserIDFilter;}
		}
        /// <summary>
        /// Returns first license, if only one in collection
        /// </summary>
        public AttorneyLicense Default
        {
            get
            {
                if (this.Count == 1)
                    return ((AttorneyLicense)this.ItemFromIndex(1));
                else
                    return null;
            }
        }
		#endregion

		#region *********************methods*********************

		/// <summary>
		/// returns the license of the filter owner that
		/// most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <param name="iL3"></param>
		/// <param name="iL4"></param>
		/// <returns></returns>
		public StringIDSimpleDataItem ItemFromLevels(int iL0, int iL1,
			int iL2, int iL3, int iL4)
		{
			object[] oParams = null;

			Trace.WriteNameValuePairs("OwnerIDFilter", this.OwnerIDFilter,
				"iL0", iL0, "iL1", iL1, "iL2", iL2, "iL3", iL3, "iL4", iL4);

			if(this.OwnerIDFilter == null)
			{
				oParams = new object[]{System.DBNull.Value, System.DBNull.Value,
					iL0, iL1, iL2, iL3, iL4};
			}
			else
			{
				int iID1;
				int iID2;
				LocalPersons.SplitID(this.OwnerIDFilter, out iID1, out iID2);
				oParams = new object[]{iID1, iID2, iL0, iL1, iL2, iL3, iL4};
			}

			System.Data.OleDb.OleDbDataReader oReader = null;

			try
			{
                //GLOG 8316
                string xQueryItemFromLevels = "spAttyLicensesItemFromLevels";
                if (m_bLocalPublicPeople)
                    xQueryItemFromLevels = "spAttyLicensesPublicItemFromLevels";
                oReader = SimpleDataCollection.GetResultSet(xQueryItemFromLevels,
					true, oParams);
			
				if (oReader.HasRows)
				{
					//save current item if dirty and there are listeners 
					//requesting this - ideally, this should be called
					//after we know that the requested item exists - 
					//unfortunately, we can't save when a DataReader is open
					//so we do it here
					if(base.m_oItem != null && base.m_oItem.IsDirty)
					{
						SaveCurrentItemIfRequested();
					}

					oReader.Read();

					//get values at current row
					object[] oValues = new object[oReader.FieldCount];

					oReader.GetValues(oValues);


					//update object with values
					base.m_oItem = this.UpdateObjectDelegate(oValues);
					base.m_oItem.IsPersisted = true;
					base.m_oItem.IsDirty = false;
					return m_oItem;
				}
				else
				{
					//item was not found
					throw new LMP.Exceptions.NotInCollectionException(System.String.Concat(
						LMP.Resources.GetLangString("Error_ItemWithLevelsNotInCollection"),
						", ", iL0, ", ", iL1, ", ", iL2, ", ", iL3, ", ", iL4));
				}
			}
			finally
			{
				oReader.Close();
                oReader.Dispose();
			}
		}

		/// <summary>
		/// returns the license of the filter owner that
		/// most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <param name="iL3"></param>
		/// <returns></returns>
		public StringIDSimpleDataItem ItemFromLevels(int iL0, int iL1,
			int iL2, int iL3)
		{
			return ItemFromLevels(iL0, iL1, iL2, iL3, 0);
		}

		/// <summary>
		/// returns the license of the filter owner that
		/// most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <returns></returns>
		public StringIDSimpleDataItem ItemFromLevels(int iL0, int iL1,
			int iL2)
		{
			return ItemFromLevels(iL0, iL1, iL2, 0, 0);
		}

		/// <summary>
		/// returns the license of the filter owner that
		/// most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <returns></returns>
		public StringIDSimpleDataItem ItemFromLevels(int iL0, int iL1)
		{
			return ItemFromLevels(iL0, iL1, 0, 0, 0);
		}

		/// <summary>
		/// returns the license of the filter owner that
		/// most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <returns></returns>
		public StringIDSimpleDataItem ItemFromLevels(int iL0)
		{
			return ItemFromLevels(iL0, 0, 0, 0, 0);
		}

		#endregion

		#region *********************StringIDSimpleDataCollection members*********************
		protected override StringIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new StringIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                    return "spAttyLicensesPublicAdd";
                else
                    return "spAttyLicensesAdd";
            }
		}

		protected override string CountSprocName
		{
			get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                {
                    if (m_iL0 > 0)
                        return "spAttyLicensesPublicByLevelsCount";
                    else
                        return "spAttyLicensesPublicCount";
                }
                else
                {
                    if (m_iL0 > 0)
                        return "spAttyLicensesByLevelsCount";
                    else
                        return "spAttyLicensesCount";
                }

            }
		}

		protected override string DeleteSprocName
		{
			get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                    return "spAttyLicensesPublicDelete";
                else
                    return "spAttyLicensesDelete";

            }
		}

		protected override string ItemFromIDSprocName
		{
			get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                    return "spAttyLicensesPublicItemFromID";
                else
                    return "spAttyLicensesItemFromID";
            }
		}

		protected override string SelectSprocName
		{
            get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                {
                    if (m_iL0 > 0)
                        return "spAttyLicensesPublicByLevels";
                    else
                        return "spAttyLicensesPublic";
                }
                else
                {
                    if (m_iL0 > 0)
                        return "spAttyLicensesByLevels";
                    else
                        return "spAttyLicenses";
                }
            }
        
        }
		protected override object[] SelectSprocParameters
		{
			get
			{
                if (m_iL0 > 0)
                {
                    if (this.OwnerIDFilter == null)
                        return new object[] { System.DBNull.Value, System.DBNull.Value, m_iL0, m_iL1, m_iL2, m_iL3, m_iL4 };
                    else
                    {
                        int iID1;
                        int iID2;
                        LocalPersons.SplitID(this.OwnerIDFilter, out iID1, out iID2);
                        return new object[] { iID1, iID2, m_iL0, m_iL1, m_iL2, m_iL3, m_iL4};
                    }
                }
                else
                {
                    if (this.OwnerIDFilter == null)
                        return new object[] { System.DBNull.Value, System.DBNull.Value, m_iL0, m_iL1, m_iL2, m_iL3, m_iL4 };
                    else
                    {
                        int iID1;
                        int iID2;
                        LocalPersons.SplitID(this.OwnerIDFilter, out iID1, out iID2);
                        return new object[] { iID1, iID2, m_iL0, m_iL1, m_iL2, m_iL3, m_iL4 };
                    }
                }
			}
		}

		protected override object[] ItemFromIDSprocParameters
		{
            get
            {
                //GLOG 3691: Should only be returning extra parameters for query other than item ID
                if (string.IsNullOrEmpty(this.OwnerIDFilter))
                    return new object[] { DBNull.Value, DBNull.Value };
                else
                {
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.Data.Persons.SplitID(this.OwnerIDFilter, out iID1, out iID2);
                    return new object[] { iID1, iID2 };
                }
            }
		}

		protected override object[] CountSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get
            {
                //GLOG 8316
                if (m_bLocalPublicPeople)
                    return "spAttyLicensesPublicUpdate";
                else
                    return "spAttyLicensesUpdate";

            }
		}

		public override StringIDSimpleDataItem Create()
		{
			if(this.OwnerIDFilter == null)
			{
				throw new LMP.Exceptions.OwnerRequiredException(
					LMP.Resources.GetLangString("Error_OwnerRequired"));
			}
			
			return Create(this.OwnerIDFilter);
		}

		public StringIDSimpleDataItem Create(string xUserPersonID)
		{
            //GLOG 8316
            if ((m_bLocalPublicPeople && !PublicPersons.Exists(xUserPersonID)) ||  
                (!m_bLocalPublicPeople && !LocalPersons.Exists(xUserPersonID)))
			{
				//person with this ID does not exist
				throw new LMP.Exceptions.PersonIDException(
					LMP.Resources.GetLangString("Error_InvalidPersonID") + xUserPersonID);
			}

			//create new license
			AttorneyLicense oLicense = new AttorneyLicense();
            if (LMP.Data.Application.g_oCurUser != null) //GLOG 8220
            {
                //create and set ID
                int iID1;
                int iID2;
                Application.GetNewUserDataItemID(out iID1, out iID2);
                string xID = string.Concat(iID1.ToString(), ".", iID2.ToString());
                oLicense.SetID(xID);
            }
			//set owner id
			oLicense.SetOwnerID(xUserPersonID);

			return oLicense;
		}
		#endregion

		private StringIDSimpleDataItem UpdateObject(object[] oValues)
		{
			AttorneyLicense oLicense = new AttorneyLicense();
			oLicense.SetID(oValues[0].ToString());
			oLicense.SetOwnerID(oValues[1].ToString());
			oLicense.Description = oValues[2].ToString();
			oLicense.License = oValues[3].ToString();
			if (oValues[4] != System.DBNull.Value)
				oLicense.L0 = (int) oValues[4];
			else
				oLicense.L0 = 0;
			if (oValues[5] != System.DBNull.Value)
				oLicense.L1 = (int) oValues[5];
			else
				oLicense.L1 = 0;
			if (oValues[6] != System.DBNull.Value)
				oLicense.L2 = (int) oValues[6];
			else
				oLicense.L2 = 0;
			if (oValues[7] != System.DBNull.Value)
				oLicense.L3 = (int) oValues[7];
			else
				oLicense.L3 = 0;
			if (oValues[8] != System.DBNull.Value)
				oLicense.L4 = (int) oValues[8];
			else
				oLicense.L4 = 0;
			oLicense.IsDirty = false;

			return oLicense;
		}
	}
}
