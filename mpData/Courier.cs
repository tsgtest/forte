using System;
using System.Collections;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a MacPac Courier
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Courier: LongIDSimpleDataItem
	{
		int m_iOfficeID = 0;
		int m_iAddressID = 0;
		string m_xName = "";
		Address m_oAddress = null;

		#region *********************constructors*********************
		internal Courier(){}
		internal Courier(int ID):base(ID){}
		#endregion

		#region *********************properties*********************
		public int OfficeID
		{
			get{return m_iOfficeID;}
			set{this.SetIntPropertyValue(ref m_iOfficeID, value);}
		}

		public int AddressID
		{
			get{return m_iAddressID;}
			set{this.SetIntPropertyValue(ref m_iAddressID, value);}
		}

		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// returns the address of the courier
		/// </summary>
		/// <returns></returns>
		public Address GetAddress()
		{
			if(m_oAddress == null)
			{
				//address has not yet been retrieved - retrieve
				Addresses oAddresses = new Addresses();

				//assign address to field
				m_oAddress = (Address) oAddresses.ItemFromID(m_iAddressID);
			}
			
			return m_oAddress;
		}

		/// <summary>
		/// returns the value of the specified property
		/// </summary>
		/// <param name="xPropertyName"></param>
		/// <returns></returns>
		public string GetPropertyValue(string xPropertyName)
		{
			Trace.WriteNameValuePairs("xPropertyName", xPropertyName);

			//compare upper case version
			string xUpperName = xPropertyName.ToUpper();
			string xVal = "";

			//return appropriate property
			switch(xUpperName)
			{
				case "ID":
					xVal = this.ID.ToString();
					break;
				case "OFFICEID":
					xVal = this.OfficeID.ToString();
					break;
				case "ADDRESSID":
					xVal = this.AddressID.ToString();
					break;
				case "NAME":
					xVal = this.Name;
					break;
				case "CITY":
					xVal = this.GetAddress().City;
					break;
				case "COUNTRY":
					xVal = this.GetAddress().Country;
					break;
				case "COUNTY":
					xVal = this.GetAddress().County;
					break;
				case "FAX1":
					xVal = this.GetAddress().Fax1;
					break;
				case "FAX2":
					xVal = this.GetAddress().Fax2;
					break;
				case "LINE1":
					xVal = this.GetAddress().Line1;
					break;
				case "LINE2":
					xVal = this.GetAddress().Line2;
					break;
				case "LINE3":
					xVal = this.GetAddress().Line3;
					break;
				case "PHONE1":
					xVal = this.GetAddress().Phone1;
					break;
				case "PHONE2":
					xVal = this.GetAddress().Phone2;
					break;
				case "PHONE3":
					xVal = this.GetAddress().Phone3;
					break;
				case "POSTCITY":
					xVal = this.GetAddress().PostCity;
					break;
				case "PRECITY":
					xVal = this.GetAddress().PreCity;
					break;
				case "STATE":
					xVal = this.GetAddress().State;
					break;
				case "STATEABBR":
					xVal = this.GetAddress().StateAbbr;
					break;
				case "ZIP":
					xVal = this.GetAddress().Zip;
					break;
				default:
					//invalid property
					throw new LMP.Exceptions.PropertyNameException(
						LMP.Resources.GetLangString(
						"Error_InvalidPropertyName") + xPropertyName);
			}

			return xVal;
		}
																	 
		#endregion

		#region *********************LongIDSimpleDataItem members*********************
		internal override bool IsValid()
		{
			//return true iff required info is present
			return (m_xName != "") && (m_iAddressID > 0) && (m_iOfficeID != 0);
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[4];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.OfficeID;
			oProps[3] = this.AddressID;

			return oProps;
		}

        internal override object[] GetUpdateQueryParameters()
        {
            object[] oProps = new object[5];
            oProps[0] = this.ID;
            oProps[1] = this.Name;
            oProps[2] = this.OfficeID;
            oProps[3] = this.AddressID;
            oProps[4] = Application.GetCurrentEditTime();

            return oProps;
        }
        internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[4];
			oProps[0] = this.Name;
			oProps[1] = this.OfficeID;
			oProps[2] = this.AddressID;
            oProps[3] = Application.GetCurrentEditTime();
			return oProps;
		}
		#endregion
	}

	/// <summary>
	/// contains the methods and properties that manage the MacPac
	/// collection of Courier - derived from LongIDSimpleDataCollection -
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Couriers: LongIDSimpleDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel = null;
		private int m_iOfficeFilterID = 0;

		#region *********************constructors*********************
		public Couriers(int OfficeFilterID)
		{
			//initialize filter
			m_iOfficeFilterID = OfficeFilterID;
		}

		public Couriers(): base(){}

		#endregion

		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spCouriersAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spCouriersCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spCouriersDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spCouriersItemFromID";}
		}

		protected override string LastIDSprocName
		{
			get{return "spCouriersLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spCouriers";}
		}

		protected override object[] SelectSprocParameters
		{
			get
			{
				if(this.OfficeIDFilter > 0)
					return new object[] {this.OfficeIDFilter};
				else
					return new object[] {DBNull.Value};
			}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override object[] CountSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get{return "spCouriersUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new Courier();
		}
		#endregion

		#region *********************methods*********************
		public int OfficeIDFilter
		{
			get{return m_iOfficeFilterID;}
			set{m_iOfficeFilterID = value;}
		}
	
		#endregion

		#region *********************private functions*********************
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			Courier oCourier = new Courier((int) oValues[0]);
			oCourier.Name = oValues[1].ToString();
			oCourier.OfficeID = (int) oValues[2];
			oCourier.AddressID = (int) oValues[3];
			oCourier.IsDirty = false;

			return oCourier;
		}
	
		#endregion
	}
}
