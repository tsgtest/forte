using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.Data

{
    public class Jurisdiction : LongIDSimpleDataItem
    {
        #region *********************fields*********************
        private string m_xName = "";
        private int m_iParentID = 0;
        private Jurisdictions.Levels m_iLevel;
        #endregion
		#region *********************constructors*********************
        internal Jurisdiction(Jurisdictions.Levels iLevel)
        {
            this.m_iLevel = iLevel;
        }
        internal Jurisdiction(Jurisdictions.Levels iLevel, int iID)
            : base(iID)
        {
            this.m_iLevel = iLevel;
        }
		#endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the name of the Jurisdiction
        /// </summary>
        public string Name
        {
            get { return m_xName; }
            set { this.SetStringPropertyValue(ref m_xName, value); }
        }

        /// <summary>
        /// gets/sets the ParentID of the Jurisdiction
        /// </summary>
        public int ParentID
        {
            get { return m_iParentID; }
            set { this.SetIntPropertyValue(ref m_iParentID, value); }
        }

        /// <summary>
        /// gets the level of the Jurisdiction
        /// </summary>
        public Jurisdictions.Levels Level
        {
            get { return m_iLevel; }
        }
        #endregion
        #region *********************LongIDSimpleDataItem members*********************
        /// <summary>
        /// returns true if the Jurisdiction is valid
        /// </summary>
        /// <returns></returns>
        internal override bool IsValid()
        {
            //return true iff required info is present -
            //if not level 0, there must be a non-zero Parent ID
            return this.Name != "" && this.Level != 0 ? this.ParentID != 0 : true;
        }

        /// <summary>
        /// returns the Jurisdiction as an array
        /// </summary>
        /// <returns></returns>
        public override object[] ToArray()
        {
            //there will be no parent ID for level0
            int iCols = this.Level == Jurisdictions.Levels.Zero ? 2 : 3;

            object[] aProps = new object[iCols];

            aProps[0] = this.ID;
            aProps[1] = this.Name;

            //get parent id if not level 0
            if (this.Level != Jurisdictions.Levels.Zero)
                aProps[2] = this.ParentID;

            return aProps;
        }
        /// <summary>
        /// returns the parameters for the Jurisdiction add query
        /// </summary>
        /// <returns></returns>
        internal override object[] GetAddQueryParameters()
        {
            object[] aProps = null;

            if (this.Level == Jurisdictions.Levels.Zero)
            {
                aProps = new object[3];
                aProps[2] = Application.GetCurrentEditTime();
            }
            else
            {
                //get array that includes parent id
                aProps = new object[4];
                aProps[2] = this.ParentID;
                aProps[3] = Application.GetCurrentEditTime();
            }
               
            //get new ID for Jurisdiction - necessary
            //because Jurisdictions do not have autonumber IDs
            aProps[0] = ((int)SimpleDataCollection.GetScalar(
                "spJurisdictions" + (int)this.Level + "LastID", new object[0])) + 1;

            aProps[1] = this.Name;

            return aProps;
        }
        internal override object[] GetUpdateQueryParameters()
        {
            //there will be no parent ID for level0
            int iCols = this.Level == Jurisdictions.Levels.Zero ? 2 : 3;

            object[] aProps = new object[iCols];

            aProps[0] = this.ID;
            aProps[1] = this.Name;

            //get parent id if not level 0
            if (this.Level != Jurisdictions.Levels.Zero)
            {
                aProps[2] = this.ParentID;
                aProps[3] = Application.GetCurrentEditTime();
            }
            else
            {
                aProps[2] = Application.GetCurrentEditTime();
            }

            return aProps;
        }
        #endregion
    }

    public class Jurisdictions:LongIDSimpleDataCollection
    {
        #region *********************enumerations*********************
        public enum Levels
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4
        }
        #endregion
        #region *********************fields*********************
        private LongIDUpdateObjectDelegate m_oDel = null;
        private Jurisdictions.Levels m_iLevel;
        private int m_iParentID;
        #endregion
        #region *********************constructors*********************
        public Jurisdictions(Jurisdictions.Levels iLevel)
        {
            this.m_iLevel = iLevel;
        }
        public Jurisdictions(Jurisdictions.Levels iLevel, int iParentID)
        {
            this.m_iLevel = iLevel;
            this.m_iParentID = iParentID;
        }
        #endregion
        #region *********************LongIDSimpleDataCollection methods*********************
        protected override string AddSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "Add"; }
        }

        protected override string CountSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "Count"; }
        }

        protected override object[] CountSprocParameters
        {
            get { return new object[] { this.m_iParentID }; }
        }

        protected override string DeleteSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "Delete"; }
        }

        protected override string ItemFromIDSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "ItemFromID"; }
        }

        protected override object[] ItemFromIDSprocParameters
        {
            get
            {
                if (m_iLevel == 0)
                    return new object[] { };
                else
                    return new object[] { this.m_iParentID };
            }
        }

        protected override string LastIDSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "LastID"; }
        }

        protected override string SelectSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString(); }
        }

        protected override object[] SelectSprocParameters
        {
            get
            {
                if (m_iLevel == 0)
                    return new object[] { };
                else
                    return new object[] { this.m_iParentID };
            }
        }

        protected override string UpdateSprocName
        {
            get { return "spJurisdictions" + ((int)m_iLevel).ToString() + "Update"; }
        }

        public override LongIDSimpleDataItem Create()
        {
            Jurisdiction oLevel = new Jurisdiction(m_iLevel);

            if (m_iParentID != 0)
                oLevel.ParentID = m_iParentID;

            return oLevel;
        }
            
        protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
        {
            get
            {
                if (m_oDel == null)
                    m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

                return m_oDel;
            }
        }
        #endregion
        #region *********************private functions*********************
        private LongIDSimpleDataItem UpdateObject(object[] aValues)
        {
            Jurisdiction oLevel = new Jurisdiction(m_iLevel, (int)aValues[0]);

            oLevel.Name = aValues[1].ToString();

            if(aValues.Length == 3)
                //array contains parent id
                oLevel.ParentID = (int)aValues[2];

            oLevel.IsDirty = false;

            return oLevel;
        }
        #endregion
    }
}
