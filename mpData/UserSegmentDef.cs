using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace LMP.Data
{
	/// <summary>
	/// user segments can be filtered by 
	/// either owner or permitted user
	/// </summary>
	public enum mpUserSegmentsFilterFields
	{
		User = 1,
		Owner = 2
	}

	/// <summary>
	/// defines a MacPac User Segment definition -
	/// a segment defined by the user
	/// </summary>
	public class UserSegmentDef: StringIDSimpleDataItem, ISegmentDef
	{
		#region *********************fields*********************
		private int m_iOwnerID;
		private string m_xName = "";
		private string m_xDisplayName = "";
        private mpObjectTypes m_shTypeID = mpObjectTypes.UserSegment;
        private string m_xXML = "";
        private int m_iL0;
        private int m_iL1;
        private int m_iL2;
        private int m_iL3;
        private int m_iL4;
        private short m_shMenuInsertionOptions;
        private short m_shDefaultMenuInsertionBehavior;
        //initialize default drag location to Insert At Selection -
        //16 (from Architect.Segment.InsertionLocations)
        private short m_shDefaultDragLocation = 16;
        private short m_shDefaultDoubleClickLocation = 16;
        private short m_shDefaultDragBehavior;
        private short m_shDefaultDoubleClickBehavior;
        private string m_xHelpText = "";
        private string m_xChildSegmentIDs;
        private mpSegmentIntendedUses m_iIntendedUse;
		#endregion
		#region *********************constructors*********************
		/// <summary>
		/// creates new user segment def and assigns IDs
		/// </summary>
		internal UserSegmentDef()
		{			
			//create and set ID
			int iID1;
			int iID2;

			Application.GetNewUserDataItemID(out iID1, out iID2);
			this.SetID(iID1, iID2);
		}

		/// <summary>
		/// creates new user segment def, assigning specified IDs
		/// </summary>
		/// <param name="iID1"></param>
		/// <param name="iID2"></param>
		internal UserSegmentDef(int iID1, int iID2)
		{			
			//create and set ID
			this.SetID(iID1, iID2);
		}
		#endregion
		#region *********************properties*********************
		public int OwnerID
		{
			get{return m_iOwnerID;}
			set{this.SetIntPropertyValue(ref m_iOwnerID, value);}
		}

		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}

		public string DisplayName
		{
			get{return m_xDisplayName;}
			set{this.SetStringPropertyValue(ref m_xDisplayName, value);}
		}

        public mpObjectTypes TypeID
        {
            get { return m_shTypeID; }
            set
            {
                if (m_shTypeID != value)
                {
                    m_shTypeID = value;
                    this.IsDirty = true;
                }
            }
        }

		public string XML
		{
			get{return m_xXML;}
			set{this.SetStringPropertyValue(ref m_xXML, value, 0);}
		}

        public int L0
        {
            get { return m_iL0; }
            set { this.SetIntPropertyValue(ref m_iL0, value); }
        }

        public int L1
        {
            get { return m_iL1; }
            set { this.SetIntPropertyValue(ref m_iL1, value); }
        }

        public int L2
        {
            get { return m_iL2; }
            set { this.SetIntPropertyValue(ref m_iL2, value); }
        }

        public int L3
        {
            get { return m_iL3; }
            set { this.SetIntPropertyValue(ref m_iL3, value); }
        }

        public int L4
        {
            get { return m_iL4; }
            set { this.SetIntPropertyValue(ref m_iL4, value); }
        }

        public short MenuInsertionOptions
        {
            get { return m_shMenuInsertionOptions; }
            set
            {
                if (m_shMenuInsertionOptions != value)
                {
                    m_shMenuInsertionOptions = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultMenuInsertionBehavior
        {
            get { return m_shDefaultMenuInsertionBehavior; }
            set
            {
                if (m_shDefaultMenuInsertionBehavior != value)
                {
                    m_shDefaultMenuInsertionBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDragLocation
        {
            get { return m_shDefaultDragLocation; }
            set
            {
                if (m_shDefaultDragLocation != value)
                {
                    m_shDefaultDragLocation = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDragBehavior
        {
            get { return m_shDefaultDragBehavior; }
            set
            {
                if (m_shDefaultDragBehavior != value)
                {
                    m_shDefaultDragBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDoubleClickLocation
        {
            get { return m_shDefaultDoubleClickLocation; }
            set
            {
                if (m_shDefaultDoubleClickLocation != value)
                {
                    m_shDefaultDoubleClickLocation = value;
                    this.IsDirty = true;
                }
            }
        }

        public short DefaultDoubleClickBehavior
        {
            get { return m_shDefaultDoubleClickBehavior; }
            set
            {
                if (m_shDefaultDoubleClickBehavior != value)
                {
                    m_shDefaultDoubleClickBehavior = value;
                    this.IsDirty = true;
                }
            }
        }

        /// <summary>
        /// returns the bounding object type used by the segment definition
        /// </summary>
        public LMP.Data.mpFileFormats BoundingObjectType
        {
            get { return LMP.Data.Application.GetBoundingObjectType(this.XML); }
        }

        public string HelpText
		{
			get{return m_xHelpText;}
			set{this.SetStringPropertyValue(ref m_xHelpText, value, 0);}
		}

        public string ChildSegmentIDs
        {
            get { return m_xChildSegmentIDs; }
            set { this.SetStringPropertyValue(ref m_xChildSegmentIDs, value, 255); }
        }

        public mpSegmentIntendedUses IntendedUse
        {
            get { return m_iIntendedUse; }
            set { m_iIntendedUse = value; }
        }

        public bool ContainsAuthorPreferences
        {
            get
            {
                string xXML = this.XML.ToUpper();
                return xXML.Contains("[AUTHORPREFERENCE_") || xXML.Contains("[LEADAUTHORPREFERENCE_") ||
                    xXML.Contains("[AUTHORPARENTPREFERENCE") || xXML.Contains("[LEADAUTHORPARENTPREFERENCE_");
            }
        }
        #endregion
		#region *********************methods*********************
		/// <summary>
		/// assigns the specified IDs to the user segment
		/// </summary>
		/// <param name="ID1"></param>
		/// <param name="ID2"></param>
		internal void SetID(int ID1, int ID2)
		{
			this.m_xID = ID1.ToString() + "." + ID2.ToString();
			this.IsDirty = true;
		}

        /// <summary>
        /// returns an AdminSegmentDef containing
        /// the same values as this UserSegmentDef -
        /// sets ID of AdminSegmentDef if supplied
        /// value is non-zero
        /// </summary>
        /// <returns></returns>
        public AdminSegmentDef ConvertToAdminSegmentDef(int iExistingID, bool bIncludeUserSegmentDefIDsInPrivateAdminDefFields)
        {
            return AdminSegmentDef.CreateFromUserSegment(this, iExistingID, bIncludeUserSegmentDefIDsInPrivateAdminDefFields);
        }

        /// <summary>
        /// returns a UserSegmentDef that contains the specified values
        /// </summary>
        /// <param name="xDef"></param>
        /// <returns></returns>
        public static UserSegmentDef FromString(string xDef)
        {
            if (string.IsNullOrEmpty(xDef))
                return null;

            string[] aSeps = new string[] { LMP.Data.ForteConstants.mpDataFileFieldSep };
            string[] aDef = xDef.Split(aSeps, StringSplitOptions.None);
            UserSegmentDef oDef = UserSegmentDefs.GetDefFromValues(aDef);

            return oDef;
        }
        public override void Export(string xFileName, bool bOverwriteExisting)
        {
            const string EXPORT_REG_VALUE_NAME = "SegmentExportPath";
            DialogResult iRes = DialogResult.OK;

            if (string.IsNullOrEmpty(xFileName))
            {
                //prompt for filename/location
                using (SaveFileDialog oDlg = new SaveFileDialog())
                {
                    oDlg.Title = LMP.Resources.GetLangString("Lbl_ExportItem");
                    oDlg.Filter = LMP.ComponentProperties.ProductName + " Data File (*.mpd)|*.mpd";
                    //GLOG 4844: Remove invalid filename characters from Display Name
                    oDlg.FileName = LMP.String.RemoveInvalidFileNameCharacters(this.DisplayName) + ".mpd";
                    //GLOG 6647: Get last selected path from registry
                    string xPath = LMP.Registry.GetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME);
                    //If invalid or not set, default to Writable Data location
                    if (string.IsNullOrEmpty(xPath) || !Directory.Exists(xPath))
                        xPath = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
                    oDlg.InitialDirectory = xPath;
                    oDlg.CheckPathExists = true;

                    iRes = oDlg.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    xFileName = oDlg.FileName;
                    //GLOG 6647: Get Path of selected file
                    xPath = Path.GetDirectoryName(xFileName);
                    //GLOG 6647: Save selected path as new default
                    if (iRes == DialogResult.OK)
                        LMP.Registry.SetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME, xPath);

                }
            }

            if (iRes == DialogResult.OK)
            {
                if (bOverwriteExisting)
                {
                    FileStream oFS = File.Create(xFileName);
                    oFS.Close();
                    oFS.Dispose();
                }

                if (this.ChildSegmentIDs != "")
                {
                    //write all non-admin child segments to file
                    string xChildIDs = this.ChildSegmentIDs;
                    string[] aChildIDs = xChildIDs.Split('|');

                    foreach (string xID in aChildIDs)
                    {
                        //get the segment if it's a user segment
                        if (xID.IndexOf('.') > -1 && !xID.EndsWith(".0"))
                        {
                            //get the UserSegment
                            UserSegmentDefs oDefs = new UserSegmentDefs(
                                mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                            UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                            //export it to the same file
                            oDef.Export(xFileName, false);
                        }
                    }
                }

                System.Type oType = this.GetType();
                string xContent = oType.Name + LMP.Data.ForteConstants.mpDataFileFieldSep + this.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep) + LMP.Data.ForteConstants.mpDataFileEntitySep;

                //append segment to file
                File.AppendAllText(xFileName, xContent);

                //GLOG 6148: If typeid is not UserSegment it's a Segment Designer segment
                if (this.TypeID != mpObjectTypes.UserSegment)
                {
                    string xPreferences = "";
                    int ID1 = 0;
                    int ID2 = 0;
                    LMP.Data.Application.SplitID(this.ID, out ID1, out ID2);
                    //GLOG 6148: Get all Firm, Office or Group Author Preferences Keysets records for all languages
                    string xPrefSql = "SELECT [OwnerID1], [Values], [Culture] FROM Keysets WHERE ScopeID1=" + ID1 + " AND ScopeID2=" + ID2 
                        + " AND OwnerID2=" + ForteConstants.mpFirmRecordID.ToString() + " AND [Type]=" + ((int)mpKeySetTypes.AuthorPref).ToString();

                    System.Collections.ArrayList alPrefs = LMP.Data.SimpleDataCollection.GetArray(xPrefSql);

                    for (int i = 0; i < alPrefs.Count; i++)
                    {
                        string xKeyset = "";
                        object[] oKeyset = (object[])alPrefs[i];
                        for (int o = 0; o < oKeyset.GetLength(0); o++)
                        {
                            if (oKeyset[o] is int)
                                xKeyset += ((int)oKeyset[o]).ToString() + StringArray.mpEndOfSubValue;
                            else
                                xKeyset += (string)oKeyset[o] + StringArray.mpEndOfSubValue;

                        }
                        xKeyset = xKeyset.TrimEnd(StringArray.mpEndOfSubValue);
                        xPreferences += xKeyset + StringArray.mpEndOfSubField;
                    }
                    xPreferences = xPreferences.TrimEnd(StringArray.mpEndOfSubField);
                    if (xPreferences != "")
                    {
                        File.AppendAllText(xFileName, "Preferences" + ForteConstants.mpDataFileFieldSep + this.ID + ForteConstants.mpDataFileFieldSep +
                            xPreferences + ForteConstants.mpDataFileEntitySep);
                    }
                }
            }
        }
		#endregion
		#region *********************StringIDSimpleDataItem members*********************
		internal override object[] GetAddQueryParameters()
		{
			int iID1 = 0;
			int iID2 = 0;

			object[] oParams = new object[24];
			Application.SplitID(this.ID, out iID1, out iID2);

			oParams[0] = iID1;
			oParams[1] = iID2;
			oParams[2] = this.OwnerID;
			oParams[3] = 0;
            oParams[4] = this.DisplayName;
			oParams[5] = this.Name;
            oParams[6] = this.IntendedUse;
            oParams[7] = 0;
            oParams[8] = (short)this.TypeID;
            oParams[9] = this.XML;
            oParams[10] = this.L0;
            oParams[11] = this.L1;
            oParams[12] = this.L2;
            oParams[13] = this.L3;
            oParams[14] = this.L4;
            oParams[15] = (short)this.MenuInsertionOptions;
            oParams[16] = (short)this.DefaultMenuInsertionBehavior;
            oParams[17] = (short)this.DefaultDragLocation;
            oParams[18] = (short)this.DefaultDragBehavior;
            oParams[19] = (short)this.DefaultDoubleClickLocation;
            oParams[20] = (short)this.DefaultDoubleClickBehavior;
            oParams[21] = this.HelpText;
            oParams[22] = this.ChildSegmentIDs;
            oParams[23] = Application.GetCurrentEditTime();
			return oParams;
		}
	
		internal override object[] GetUpdateQueryParameters()
		{
			int iID1;
			int iID2;

			object[] oParams = new object[24];
			Application.SplitID(this.ID, out iID1, out iID2);

            oParams[0] = iID1;
            oParams[1] = iID2;
            oParams[2] = this.OwnerID;
            oParams[3] = 0;
            oParams[4] = this.DisplayName;
            oParams[5] = this.Name;
            oParams[6] = this.IntendedUse;
            oParams[7] = 0;
            oParams[8] = (short)this.TypeID;
            oParams[9] = this.XML;
            oParams[10] = this.L0;
            oParams[11] = this.L1;
            oParams[12] = this.L2;
            oParams[13] = this.L3;
            oParams[14] = this.L4;
            oParams[15] = (short)this.MenuInsertionOptions;
            oParams[16] = (short)this.DefaultMenuInsertionBehavior;
            oParams[17] = (short)this.DefaultDragLocation;
            oParams[18] = (short)this.DefaultDragBehavior;
            oParams[19] = (short)this.DefaultDoubleClickLocation;
            oParams[20] = (short)this.DefaultDoubleClickBehavior;
            oParams[21] = this.HelpText;
            oParams[22] = this.ChildSegmentIDs;
            oParams[23] = Application.GetCurrentEditTime();
			return oParams;
		}

        internal override bool IsValid()
        {
            //returns true if id is of the form number.number,
            //and display name and xml are not empty
            return Regex.IsMatch(this.ID, @"\d{1,9}\.\d{1,9}") &
                this.DisplayName != "";
        }
	
		public override object[] ToArray()
		{
            object[] oVals = new object[21];
            int iID1;
            int iID2;

            LMP.Data.Application.SplitID(this.ID, out iID1, out iID2);

            oVals[0] = iID1;
            oVals[1] = iID2;
            oVals[2] = this.OwnerID;
            oVals[3] = this.DisplayName;
            oVals[4] = this.Name;
            oVals[5] = this.TypeID;
            oVals[6] = this.XML;
            oVals[7] = this.L0;
            oVals[8] = this.L1;
            oVals[9] = this.L2;
            oVals[10] = this.L3;
            oVals[11] = this.L4;
            oVals[12] = this.MenuInsertionOptions;
            oVals[13] = this.DefaultMenuInsertionBehavior;
            oVals[14] = this.DefaultDragLocation;
            oVals[15] = this.DefaultDragBehavior;
            oVals[16] = this.DefaultDoubleClickLocation;
            oVals[17] = this.DefaultDoubleClickBehavior;
            oVals[18] = this.HelpText;
            oVals[19] = this.ChildSegmentIDs;
            oVals[20] = this.IntendedUse;

			return oVals;
		}
		#endregion
	}

	public class UserSegmentDefs:StringIDSimpleDataCollection
	{
		#region *********************fields*********************
		private mpUserSegmentsFilterFields m_iFilterField;
		private int m_iFilterValue;
		private StringIDUpdateObjectDelegate m_oDel;
		#endregion
		#region *********************constructors*********************
		public UserSegmentDefs(mpUserSegmentsFilterFields iFilterField, int iFilterValue)
		{
			SetFilter(iFilterField, iFilterValue);
		}
        public UserSegmentDefs()
        {
            //JTS 4/22/10: Doesn't make sense for default Filter Field to be User
            //User filter with ID of 0 will never return any items
            SetFilter(mpUserSegmentsFilterFields.Owner, 0);
        }
		#endregion
		#region *********************methods*********************
		public void SetFilter(mpUserSegmentsFilterFields iFilterField, int iFilterValue)
		{
			m_iFilterField = iFilterField;
			m_iFilterValue = iFilterValue;
            m_oArray = null;
		}

        internal static UserSegmentDef GetDefFromValues(string[] oVals)
        {
            UserSegmentDef oDef = new UserSegmentDef(int.Parse(oVals[0]), int.Parse(oVals[1]));
            oDef.OwnerID = int.Parse(oVals[2]);
            oDef.DisplayName = oVals[3];
            oDef.Name = oVals[4];
            oDef.TypeID = (mpObjectTypes)(Enum.Parse(typeof(mpObjectTypes), oVals[5]));
            oDef.IntendedUse = (mpSegmentIntendedUses)(Enum.Parse(typeof(mpSegmentIntendedUses), oVals[20]));

            string xXML = oVals[6];
            oDef.XML = String.GetSegmentXmlFromDefString(xXML);

            oDef.L0 = int.Parse(oVals[7]);
            oDef.L1 = int.Parse(oVals[8]);
            oDef.L2 = int.Parse(oVals[9]);
            oDef.L3 = int.Parse(oVals[10]);
            oDef.L4 = int.Parse(oVals[11]);
            oDef.MenuInsertionOptions = short.Parse(oVals[12]);
            oDef.DefaultMenuInsertionBehavior = short.Parse(oVals[13]);
            oDef.DefaultDragLocation = short.Parse(oVals[14]);
            oDef.DefaultDragBehavior = short.Parse(oVals[15]);
            oDef.DefaultDoubleClickLocation = short.Parse(oVals[16]);
            oDef.DefaultDoubleClickBehavior = short.Parse(oVals[17]);
            oDef.HelpText = oVals[18];
            oDef.ChildSegmentIDs = oVals[19];
            oDef.IsPersisted = true;
            oDef.IsDirty = true;
            return oDef;
        }

        public static bool Exists(int iOwner, string xID)
        {
            int iID1;
            int iID2;

            if (xID.EndsWith(".0"))
                return false;

            LMP.Data.Application.SplitID(xID, out iID1, out iID2);

            return SimpleDataCollection.GetResultSet(
                "spUserSegmentsItemFromID", true, new object[] {iOwner, iID1, iID2 }).HasRows;
        }

		#endregion
		#region *********************StringIDSimpleDataCollection members*********************
		protected override string AddSprocName
		{
			get{return "spUserSegmentsAdd";}
		}
	
		protected override string CountSprocName
		{
			get
			{
				if(m_iFilterField == mpUserSegmentsFilterFields.Owner)
					return "spUserSegmentsCount";
				else
					return "spUserSegmentsByPermissionCount";
			}
		}
	
		protected override object[] CountSprocParameters
		{
			get{return new object[] {m_iFilterValue};}
		}
	
		protected override string DeleteSprocName
		{
			get{return "spUserSegmentsDelete";}
		}
	
		protected override string ItemFromIDSprocName
		{
			get
			{
				if(m_iFilterField == mpUserSegmentsFilterFields.Owner)
					return "spUserSegmentsItemFromID";
				else
					return "spUserSegmentsByPermissionItemFromID";
			}
		}
	
		protected override object[] ItemFromIDSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}
	
		protected override string SelectSprocName
		{
			get
			{
				if(m_iFilterField == mpUserSegmentsFilterFields.Owner)
					return "spUserSegments";
				else
					return "spUserSegmentsByPermission";
			}
		}
	
		protected override object[] SelectSprocParameters
		{
			get
			{
                if (m_iFilterValue == 0)
                    return new object[]{System.DBNull.Value};
                else
                    return new object[] { m_iFilterValue };
			}
		}
	
		protected override string UpdateSprocName
		{
			get{return "spUserSegmentsUpdate";}
		}
	
		protected override StringIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new StringIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}
	
		public override StringIDSimpleDataItem Create()
		{
			return new UserSegmentDef();
		}
		#endregion
		#region *********************private members*********************
		private StringIDSimpleDataItem UpdateObject(object[] oVals)
		{
			UserSegmentDef oDef = new UserSegmentDef(
				Convert.ToInt32(oVals[0]), Convert.ToInt32(oVals[1]));
			oDef.OwnerID = (int) oVals[2];
            oDef.DisplayName = oVals[3] == null ? "" : oVals[3].ToString();
			oDef.Name = oVals[4] == null ? "" : oVals[4].ToString();
            oDef.IntendedUse = ((mpSegmentIntendedUses)int.Parse(oVals[5].ToString()));
            oDef.TypeID = (oVals[7] == System.DBNull.Value ? 0 : (mpObjectTypes)oVals[7]);
			oDef.XML = oVals[8].ToString();
            oDef.L0 = (oVals[9] == System.DBNull.Value ? 0 : (int)oVals[9]);
            oDef.L1 = (oVals[10] == System.DBNull.Value ? 0 : (int)oVals[10]);
            oDef.L2 = (oVals[11] == System.DBNull.Value ? 0 : (int)oVals[11]);
            oDef.L3 = (oVals[12] == System.DBNull.Value ? 0 : (int)oVals[12]);
            oDef.L4 = (oVals[13] == System.DBNull.Value ? 0 : (int)oVals[13]);
            oDef.MenuInsertionOptions = (oVals[14] == System.DBNull.Value ? (short)0 : (short)oVals[14]);
            oDef.DefaultMenuInsertionBehavior = (oVals[15] == System.DBNull.Value ? (short)0 : (short)oVals[15]);
            oDef.DefaultDragLocation = (oVals[16] == System.DBNull.Value ? (short)0 : (short)oVals[16]);
            oDef.DefaultDragBehavior = (oVals[17] == System.DBNull.Value ? (short)0 : (short)oVals[17]);
            oDef.DefaultDoubleClickLocation = (oVals[18] == System.DBNull.Value ? (short)0 : (short)oVals[18]);
            oDef.DefaultDoubleClickBehavior = (oVals[19] == System.DBNull.Value ? (short)0 : (short)oVals[19]);
            oDef.HelpText = oVals[20] == null ? "" : oVals[20].ToString();
            oDef.ChildSegmentIDs = oVals[21].ToString();
            return oDef;
		}
		#endregion
	}
}
