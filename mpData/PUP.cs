﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Reflection;
using System.Data.OleDb; //GLOG 8220
using Access = Microsoft.Office.Interop.Access;

namespace LMP.Data
{
    static public class PUP
    {
        const string SPACER = "     ";

        static string xPupLog = LMP.Data.Application.PublicDataDirectory + @"\fPUP.log"; //JTS 3/28/13 //GLOG 7881
        static string xXMLSource = LMP.Data.Application.PublicDataDirectory + @"\PUPSource.xml"; //JTS 3/28/13
        static string xDBSource = LMP.Data.Application.PublicDataDirectory + @"\PUPSource.mdb"; //JTS 3/28/13

        public static void Execute(bool bUpdateLicenses, string xSourceServer, string xSourceDatabase)
        {
            bool bIgnore = false;
            Execute(bUpdateLicenses, xSourceServer, xSourceDatabase, out bIgnore, false);
        }
        public static void Execute(bool bUpdateLicenses, string xSourceServer, string xSourceDatabase, out bool bError, bool bAdminUI)
        {
            PupHelper oHelper = null;
            OleDbConnection oCn = null;
            string xTargetDB = "";
            string xDB = "";
            string xDir = "";
            
            bError = false;

            //GLOG item #5460 - dcf
            if (File.Exists(xPupLog))
                File.Delete(xPupLog);

            //GLOG 8220
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                xTargetDB = LMP.Data.Application.PeopleDBName;
                xDir = LMP.Data.Application.PublicDataDirectory;

            }
            else
            {
                xTargetDB = LMP.Data.Application.AdminDBName;
                xDir = LMP.Data.Application.WritableDBDirectory;
            }
            xDB = xDir + @"\" + xTargetDB;
            string xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                xDB + ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory +
                @"\Forte.mdw" + ";User ID=mp10ProgrammaticUser;Password=Fish4Mill"; //WorkgroupFile ID = 4437
            using (oCn = new OleDbConnection(xCnStr))
            {
                oCn.Open();
                //JTS 3/30/10:  Create new Stream for each Execution, otherwise it will only work once per session
                using (FileStream oLog = File.OpenWrite(xPupLog))
                {
                    using (StreamWriter oLogWriter = new StreamWriter(oLog))
                    {
                        //write start time
                        LogText(oLogWriter, "Start");

                        //backup db
                        File.Copy(xDB, xDB + ".bak");
                        LogText(oLogWriter, "Backed up database " + xTargetDB +
                            " to " + xTargetDB + ".bak.");  //GLOG 7881

                        oHelper = GetPupHelper(oLogWriter);

                        if (oHelper != null)
                        {
                            oHelper.ExecutePrePup(oLogWriter);
                            LogText(oLogWriter, "Executed pre-PUP method.");
                        }
                        else
                        {
                            LogText(oLogWriter, "oHelper is null.");
                        }

                        try
                        {
                            //delete the temp tables if they exist
                            try
                            {
                                LMP.Data.SimpleDataCollection.ExecuteAction(oCn, "DROP TABLE PeopleTmp"); //GLOG 8220
                            }
                            catch { }
                            try
                            {
                                LMP.Data.SimpleDataCollection.ExecuteAction(oCn, "DROP TABLE AttyLicensesTmp"); //GLOG 8220
                            }
                            catch { }

                            //update people - formerly qryGetHubPeople - creates PeopleTmp table-
                            //queries below add/edit/delete records based on the records in PeopleTmp - 
                            //thus spCreatePeopleTmp should create PeopleTmp with all the fields necessary
                            //for table creation except OwnerID, UsageState, LinkedPersonID, 
                            //DefaultOfficeRecordID1, OfficeUsageState, and LastEditTime.
                            //ID2 must be present in the temp table, and all values for this field should be 0.
                            //Note that field names in the temp table must be identical to those in the People table.
                            int iPeopleTmp = LMP.Data.SimpleDataCollection.ExecuteActionSproc(oCn, "spCreatePeopleTmp", new object[] { });
                            LogText(oLogWriter, "Created updated people temp table");

                            //GLOG 5332: If no records found in People Source,
                            //do nothing, instead of marking all existing People as deleted
                            if (iPeopleTmp == 0)
                            {
                                LogText(oLogWriter, "No People records found in Source Table");
                                return;
                            }

                            //update values of existing records -
                            //cycle through fields of temp table to create sql statement,
                            //assuming that field names will match in People table
                            string xSQL = "UPDATE People p INNER JOIN PeopleTmp t ON p.ID1=t.ID1 AND p.ID2=t.ID2 SET ";
                            DataTable oDT = LMP.Data.SimpleDataCollection.GetDataSet(oCn, "SELECT * FROM PeopleTmp WHERE ID1=0;").Tables[0];
                            StringBuilder oSBSet = new StringBuilder();
                            StringBuilder oSBWhere = new StringBuilder();

                            //cycle through each column, building sql
                            foreach (DataColumn oCol in oDT.Columns)
                            {
                                string xColName = oCol.ColumnName;
                                oSBSet.AppendFormat("p.{0}=t.{0},", xColName);
                                oSBWhere.AppendFormat("p.{0}<>t.{0} OR ", xColName);
                            }

                            string xSetClause = oSBSet.ToString();
                            string xWhereClause = oSBWhere.ToString();
                            string xNow = "#" + LMP.Data.Application.GetCurrentEditTime(true) + "#";

                            //JTS 3/30/10: Indicate Table Name to avoid possible ambiguity
                            xSetClause = xSetClause + "p.LastEditTime=" + xNow + " ";
                            xWhereClause = xWhereClause.Substring(0, xWhereClause.Length - 4);

                            //update changed records
                            xSQL += xSetClause + " WHERE " + xWhereClause;
                            int iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220

                            //add new records
                            //GLOG 5656: Make sure new records use UTC for LastEditTime
                            xSQL = "INSERT INTO People SELECT t.*, " + xNow +
                                " as LastEditTime FROM PeopleTmp t LEFT JOIN People p ON (t.ID1 = p.ID1) AND (t.ID2 = p.ID2) WHERE  p.ID1 is null;";
                            iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220
                            LogText(oLogWriter, "Added new records to people table");

                            ////set field defaults
                            xSQL = "UPDATE PeopleTmp t LEFT JOIN People p ON (t.ID1 = p.ID1) AND (t.ID2 = p.ID2) SET p.ID2=0, p.OwnerID=0, p.UsageState=1, p.LinkedPersonID=0, p.OfficeUsageState=1, p.LinkedExternally=-1, p.LastEditTime = " + xNow + " WHERE  p.ID1 is null;";
                            iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220
                            LogText(oLogWriter, "Set new field defaults");

                            //"delete" records - set usage state of missing records to 0
                            xSQL = "UPDATE People p LEFT JOIN PeopleTmp t ON (p.ID1 = t.ID1) AND (p.ID2 = t.ID2) SET p.UsageState=0, p.LastEditTime = " + xNow + " WHERE t.ID1 Is Null AND p.OwnerID=0 AND p.UsageState<>0 AND p.LinkedExternally=-1;";
                            iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220
                            LogText(oLogWriter, "Updated records");

                            //GLOG 8220: Moved this command 
                            //delete the temp table if it exists
                            try
                            {
                                LMP.Data.SimpleDataCollection.ExecuteAction(oCn, "DROP TABLE PeopleTmp"); //GLOG 8220
                                LogText(oLogWriter, "Deleted temp table");
                            }
                            catch { }
                            if (bUpdateLicenses)
                            {
                                ////create AttyLicensesTmp - this query needs to generate a value for ID2
                                ////in addition to getting the values for the following fields:
                                ////OwnerID1, Description, License, L0, L1, L2, L3, L4.
                                ////the value for ID2 should be unique for each record each time
                                ////the query is executed.  The field is used only by the "Add" query
                                ////to provide a unique ID2 value.
                                ////FYI, uniqueness is defined by the add/update/delete queries by the following
                                ////fields: OwnerID1, License, L0, L1, L2, L3, L4.  This allows an attorney
                                ////to have multiple licenses in a jurisdiction.  I'm not sure if this will
                                ////ever be used, but since the db table allows for it, I figured the pup queries
                                ////should support it. - DF
                                LMP.Data.SimpleDataCollection.ExecuteActionSproc(oCn, "spCreateAttyLicensesTmp", new object[] { }); //GLOG 8220
                                LogText(oLogWriter, "Created updated licenses temp table");

                                //JTS 3/30/10: LastEditTime should be set using UTC Time stored in xNow variable
                                //update changed records
                                xSQL = "UPDATE AttyLicensesTmp AS t INNER JOIN AttyLicenses AS a ON (t.L4 = a.L4) AND (t.L3 = a.L3) AND (t.L2 = a.L2) AND (t.L1 = a.L1) AND (t.L0 = a.L0) AND (t.OwnerID1 = a.OwnerID1) AND (t.License = a.License) SET a.Description = t.Description, a.LastEditTime = " + xNow + " WHERE (a.Description <> t.Description) AND a.OwnerID2=0;";
                                iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220

                                //GLOG item #6737
                                //delete licenses - set usage state of missing records to 0
                                xSQL = "UPDATE AttyLicenses AS a LEFT JOIN AttyLicensesTmp AS t ON (t.OwnerID1 = a.OwnerID1) AND (t.L0 = a.L0) AND (t.L1 = a.L1) AND (t.L2 = a.L2) AND (t.L3 = a.L3) AND (t.L4 = a.L4) AND (t.License = a.License) SET a.UsageState = 0, a.LastEditTime = " + xNow + " WHERE ((t.L0 Is Null) AND (a.OwnerID2=0) AND (a.UsageState<>0));";
                                iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL);

                                //add new licenses
                                xSQL = "INSERT INTO AttyLicenses ( ID1, ID2, OwnerID1, OwnerID2, Description, License, L0, L1, L2, L3, L4, UsageState, LastEditTime ) SELECT t.OwnerID1, t.ID2, t.OwnerID1, 0, t.Description, t.License, t.L0, t.L1, t.L2, t.L3, t.L4, 1, " + xNow + " FROM AttyLicenses AS a RIGHT JOIN AttyLicensesTmp AS t ON (a.L4 = t.L4) AND (a.L3 = t.L3) AND (a.L2 = t.L2) AND (a.L1 = t.L1) AND (a.L0 = t.L0) AND (a.OwnerID1 = t.OwnerID1) AND (t.License = a.License) WHERE a.OwnerID1 Is Null;";
                                iRet = LMP.Data.SimpleDataCollection.ExecuteAction(oCn, xSQL); //GLOG 8220
                                //GLOG 8220: Delete AttyLicensesTmp table
                                //delete the temp table if it exists
                                try
                                {
                                    LMP.Data.SimpleDataCollection.ExecuteAction(oCn, "DROP TABLE AttyLicensesTmp");
                                    LogText(oLogWriter, "Deleted temp table");
                                }
                                catch { }
                            }
                        }
                        catch (System.Exception oE)
                        {
                            bError = true;
                            oCn.Close(); //GLOG 8220
                            //GLOG 8149
                            int iUserID = 0;
                            if (LMP.Data.Application.g_oCurUser != null)
                                iUserID = LMP.Data.Application.g_oCurUser.ID;
                            //GLOG 8149: Need to logout before attempting to restore original DB
                            if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                                LMP.Data.Application.Logout();

                            //restore original db
                            File.Delete(xDB);

                            //rename bak db to ForteAdmin.mdb  //GLOG 7881
                            File.Move(xDB + ".bak", xDB);
                            LogText(oLogWriter, "Restored backup database");

                            //GLOG 8149: Log back in after replacing DB
                            if (bAdminUI && !LMP.MacPac.MacPacImplementation.IsFullLocal && iUserID > 0)
                                LMP.Data.Application.Login(iUserID, true);

                            LogText(oLogWriter, "ERROR: " + LMP.Error.GetErrorDetail(oE));

                            //GLOG 7881
                            if (bAdminUI) //GLOG 8149: Don't display message if running PUP.exe
                                MessageBox.Show("Could not update " + LMP.ComponentProperties.ProductName +
                                    " people with data in external source.  Please check fPUP.log for more information.",
                                    LMP.ComponentProperties.CompanyName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                            return;
                        }
                        finally
                        {
                            //GLOG 4865: Backup won't exist if error was trapped above
                            if (File.Exists(xDB + ".bak"))
                            {
                                oCn.Close();
                                try
                                {
                                    //delete the backup - the update of the local db was successful
                                    File.Delete(xDB + ".bak");
                                    LogText(oLogWriter, "Removed database backup");
                                }
                                catch (System.Exception oE)
                                {
                                    LogText(oLogWriter, "Unable to remove database backup: " + LMP.Error.GetErrorDetail(oE));
                                }
                                LogText(oLogWriter, "Local people records updated successfully");
                            }

                            if (oHelper != null)
                            {
                                oHelper.ExecutePostPup(oLogWriter);
                                LogText(oLogWriter, "Executed post-PUP method.");
                            }

                            //Close StreamWriter last
                            oLogWriter.Flush();
                            oLogWriter.Close();
                        }
                    }
                }
            }
        }


        public static PupHelper GetPupHelper(StreamWriter oLogWriter)
        {
            PupHelper oPupHelper = null;

            //retrieve assembly and class name of object to create
            string xAsmName = LMP.Registry.GetMacPac10Value("PupHelperAssemblyName");
            string xClassName = LMP.Registry.GetMacPac10Value("PupHelperClassName");

            //MessageBox.Show("PupHelperAssemblyName=" + (xAsmName==null ? "" : xAsmName));
            //MessageBox.Show("PupHelperClassName=" + (xClassName==null ? "" : xClassName));

            if (string.IsNullOrEmpty(xAsmName) && string.IsNullOrEmpty(xClassName))
            {
                //no helper has been specified
                return null;
            }
            else if (string.IsNullOrEmpty(xAsmName))
            {
                throw new LMP.Exceptions.RegistryKeyException(
                    LMP.Resources.GetLangString("Msg_MissingPupHelperAsmNameRegKey"));
            }
            else if (string.IsNullOrEmpty(xClassName))
            {
                throw new LMP.Exceptions.RegistryKeyException(
                    LMP.Resources.GetLangString("Msg_MissingPupHelrperClassNameRegKey"));
            }

            string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAsmName;

            Assembly oAsm = null;

            try
            {
                //load assembly
                oAsm = Assembly.LoadFrom(xAsmDllFullName);
                //MessageBox.Show("Loaded assembly " + xAsmDllFullName);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
            }

            try
            {
                //create class instance
                oPupHelper = (PupHelper)oAsm.CreateInstance(xClassName);
                //MessageBox.Show("oPupHelper==null:  " + (oPupHelper==null).ToString());

                 if (oPupHelper == null)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ResourceException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
            }

            //MessageBox.Show("Done");
            return oPupHelper;
        }

        private static void LogText(StreamWriter oWriter, string xText)
        {
            oWriter.WriteLine(DateTime.Now.ToString(LMP.Culture.USEnglishCulture) +
                                    SPACER + xText);
            oWriter.Flush();
        }
    }
}
