﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LMP.Data
{
    abstract public class PupHelper
    {
        abstract public void ExecutePrePup(StreamWriter oSW);
        abstract public void ExecutePostPup(StreamWriter oSW);
    }
}
