using System;
using System.Data.OleDb;
using System.Collections;
using System.Text;

namespace LMP.Data
{

	/// <summary>
	/// Contains the properties and methods that define a MacPac UserFolder member
	/// created by Jeffrey Sweetland 10/29/04
	/// </summary>
	public class UserFolder: StringIDSimpleDataItem, IFolder
	{
		private const mpFolderTypes m_bytType = mpFolderTypes.User;
		private int m_iParentFolderID1 = 0;
		private int m_iParentFolderID2 = 0;
		private int m_iOwnerID = 0;
        private int m_iIndex = 0;
		private string m_xDescription = null;
		private string m_xName = null;
		private string m_xXML = null;

		#region *********************constructors*********************
		internal UserFolder():base()
		{
			//create and set ID
			int iID1;
			int iID2;

			Application.GetNewUserDataItemID(out iID1, out iID2);
			string xID = string.Concat(iID1.ToString(), ".", iID2.ToString());
			this.SetID(xID);
		}

		internal UserFolder(int iOwnerID, int iParentFolderID1,
			int iParentFolderID2):base()
		{
			this.OwnerID = iOwnerID;
			this.ParentFolderID1 = iParentFolderID1;
			this.ParentFolderID2 = iParentFolderID2;

			//create and set ID
			int iID1;
			int iID2;

			Application.GetNewUserDataItemID(out iID1, out iID2);
			string xID = string.Concat(iID1.ToString(), ".", iID2.ToString());
			this.SetID(xID);
		}

		internal UserFolder(string xID):base()
		{
			this.SetID(xID);
		}

		#endregion

		#region *********************properties*********************
		public int ParentFolderID1
		{
			get{return m_iParentFolderID1;}
			set{this.SetIntPropertyValue(ref m_iParentFolderID1, value);}
		}

		public int ParentFolderID2
		{
			get{return m_iParentFolderID2;}
			set{this.SetIntPropertyValue(ref m_iParentFolderID2, value);}
		}
        public int Index
        {
            get { return m_iIndex; }
            set { this.SetIntPropertyValue(ref m_iIndex, value); }
        }

		public string Description
		{
			get{return m_xDescription;}
			set{this.SetStringPropertyValue(ref m_xDescription, value, 255);}
		}

		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value, 255);}
		}

        public string XML
        {
            get { return m_xXML; }
            set { this.SetStringPropertyValue(ref m_xXML, value, 0); }
        }

		public int OwnerID
		{
			get{return m_iOwnerID;}
			set{this.SetIntPropertyValue(ref m_iOwnerID, value);}
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// return FolderMembers collection for this folder
		/// </summary>
		/// <returns></returns>
		public FolderMembers GetMembers()
		{
			if (m_xID == null || m_xID == "")
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_FolderSaveRequired"));
		
			int iID1;
			int iID2;
			Application.SplitID(m_xID, out iID1, out iID2);
			return new FolderMembers(iID1, iID2);
		}

        /// <summary>
        /// Get the series of paths strings for this segment. Segments can exist in several
        /// folders therefore severals paths can exist.
        /// </summary>
        /// <param name="xSegmentId"></param>
        /// <returns></returns>
        public static string [] GetSegmentFullPaths(string xSegmentId, int iUserId)
        {
            ArrayList alSegmentPathDetailsLists = GetSegmentPathIds(xSegmentId, iUserId);
            StringBuilder sbSegmentPath = new StringBuilder("");

            ArrayList alxSegmentPaths = new ArrayList();

            foreach (ArrayList alSegmentPathDetails in alSegmentPathDetailsLists)
            {
                sbSegmentPath.Remove(0, sbSegmentPath.ToString().Length);

                for (int i = alSegmentPathDetails.Count - 1; i >= 0; i--)
                {
                    object[] oSegmentPathDetail = (object[])alSegmentPathDetails[i];
                    string xFolderName = (string)oSegmentPathDetail[2];
                    sbSegmentPath.Append(xFolderName + "/");
                }

                alxSegmentPaths.Add(sbSegmentPath.ToString());
            }

            string[] axSegmentPaths = new string[alxSegmentPaths.Count];

            for(int iPathIndex = 0; iPathIndex < axSegmentPaths.Length; iPathIndex++)
            {
                axSegmentPaths[iPathIndex] = (string)alxSegmentPaths[iPathIndex];
            }

            return axSegmentPaths;
        }

        /// <summary>
        /// Get the series of paths id's for this segment. Segments can exist in several
        /// folders therefore severals paths can exist.
        /// </summary>
        /// <param name="xUserSegmentId"></param>
        /// <returns></returns>
        public static ArrayList GetSegmentPathIds(string xUserSegmentId, int iUserId)
        {
            // The user segment id has this format: U9400.995241152 
            // Remove the U prefix which designate this as a user segment.
            string xUserIdSegmentId = xUserSegmentId.Substring(1);

            // We now have the user id and the segment id in this format: 9400.995241152
            // Split this to get the individual user id and segment id.
            string[] axUserSegmentParts = xUserIdSegmentId.Split('.');

            // Get the user id and the segment id.
            string xFolderId1;
            string xSegmentId;

            if (axUserSegmentParts.Length < 2)
            {
                // When an admin segment is in a user folder the field that is 
                // the segment owner  user id field (TargetObjectID1) contains the segment id and 
                // the segment id field (TargetObkectID2)contains a zero.
                xFolderId1 = axUserSegmentParts[0];
                xSegmentId = "0";
            }
            else
            {
                xFolderId1 = axUserSegmentParts[0];
                xSegmentId = axUserSegmentParts[1];
            }

            ArrayList alPaths = new ArrayList();
            string xProcName = "spUserFolderMemberParentFolderId";
            int iFolderId1 = int.Parse(xFolderId1);
            ArrayList alParentFolderDetailsList = SimpleDataCollection.GetArray(xProcName, new object[] { iFolderId1, int.Parse(xSegmentId) });

            foreach (object[] aSegmentParentDetails in alParentFolderDetailsList)
            {
                string xParentId1 = aSegmentParentDetails[0].ToString();
                string xParentId2 = aSegmentParentDetails[1].ToString();
                ArrayList aloFolderPathIds = GetFolderPathIds(int.Parse(xParentId1), xParentId2, iUserId);
                
                if (aloFolderPathIds.Count > 0)
                {
                    alPaths.Add(aloFolderPathIds);
                }
            }

            return alPaths;
        }

        public static ArrayList GetFolderPathIds(int iFolderId1, string xFolderId2, int iUserId)
        {
            string xUserFoldersRootId = "0";
            string xRootFolderName = new FirmApplicationSettings(LMP.Data.Application.User.ID).MyFoldersLabel;

            string xProcName = "spUserFolderParentFolderId";
            ArrayList alParentFolderDetailsList = SimpleDataCollection.GetArray(xProcName, new object[] { iFolderId1, int.Parse(xFolderId2), iUserId });

            if (alParentFolderDetailsList.Count > 0)
            {
                object[] oFolderParentDetails = (object[])alParentFolderDetailsList[0];
                string xParentId1 = oFolderParentDetails[0].ToString();
                string xParentId2 = oFolderParentDetails[1].ToString();
                //Array returned from query contains Parent Folder IDs and Current Folder Name
                //Save Parent IDs, then replace with child folder IDs in array
                oFolderParentDetails[0] = iFolderId1.ToString();
                oFolderParentDetails[1] = xFolderId2;
                alParentFolderDetailsList[0] = oFolderParentDetails;
                //xProcName = "spUserFoldersItemFromID";
                //ArrayList alFolderDetails = SimpleDataCollection.GetArray(xProcName, new object[] { iUserId, int.Parse(xParentId1), int.Parse(xParentId2), iFolderId1, int.Parse(xFolderId2) });
                //if (alFolderDetails.Count > 0)
                //{
                //    iFolderId1 = int.Parse(((object[])alFolderDetails[0])[0].ToString());
                //    xFolderId2 = ((object[])alFolderDetails[0])[1].ToString();
                //    string xDisplayName = ((object[])alFolderDetails[0])[2].ToString();
                //    ((object[])alParentFolderDetailsList[0])[0] = iFolderId1.ToString();
                //    ((object[])alParentFolderDetailsList[0])[1] = xFolderId2;
                //}
                if (xParentId1 == xUserFoldersRootId)
                {
                    alParentFolderDetailsList.Add(new object[] { "0", "0", xRootFolderName });
                }
                else
                {
                    ArrayList alGrandParentPathDetailsList = GetFolderPathIds(int.Parse(xParentId1), xParentId2, iUserId);
                    //ArrayList alGrandParentPathDetailsList = GetFolderPathIds(iFolderId1, xFolderId2, iUserId);
                    foreach (object oGrandParentPathDetails in alGrandParentPathDetailsList)
                    {
                        alParentFolderDetailsList.Add(oGrandParentPathDetails);
                    }
                }
            }

            return alParentFolderDetailsList;
        }
		#endregion

		#region *********************StringIDSimpleDataItem members*********************

		internal override bool IsValid()
		{
            //GLOG item #4545 - dcf
			//return true iff required info is present
            //return (this.Name != "" && this.Name != null && 
            //    this.Description != "" && this.Description != null);
            return (this.Name != "" && this.Name != null);
        }

		public override object[] ToArray()
		{
			object[] oProps = new object[8];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.Description;
			oProps[3] = this.ParentFolderID1;
			oProps[4] = this.ParentFolderID2;
            oProps[5] = this.Index;
			oProps[6] = this.XML;
			oProps[7] = this.OwnerID;
			return oProps;
		}

		internal override object[] GetAddQueryParameters()
		{
			int iID1;
			int iID2;
			Application.SplitID(this.ID, out iID1, out iID2);

			object[] oProps = new object[10];
			oProps[0] = iID1;
			oProps[1] = iID2;
			oProps[2] = this.Name;
			oProps[3] = this.Description;
			oProps[4] = this.ParentFolderID1;
			oProps[5] = this.ParentFolderID2;
            oProps[6] = this.Index;
			oProps[7] = this.XML;
			oProps[8] = this.OwnerID;
            oProps[9] = Application.GetCurrentEditTime();
			return oProps;
		}
		internal override object[] GetUpdateQueryParameters()
		{
			int iID1;
			int iID2;
			Application.SplitID(this.ID, out iID1, out iID2);

			object[] oProps = new object[9];
			oProps[0] = iID1;
			oProps[1] = iID2;
			oProps[2] = this.Name;
			oProps[3] = this.Description;
			oProps[4] = this.ParentFolderID1;
			oProps[5] = this.ParentFolderID2;
            oProps[6] = this.Index;
			oProps[7] = this.XML;
            oProps[8] = Application.GetCurrentEditTime();
			return oProps;
		}
		#endregion
	}
	/// <summary>
	/// contains the methods and properties that manage the 
	/// collection of MacPac UserFolder definitions -
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class UserFolders: StringIDSimpleDataCollection
	{
		private StringIDUpdateObjectDelegate m_oDel = null;
		private int m_iParentFolderID1Filter = -1;
		private int m_iParentFolderID2Filter = -1;
		private int m_iOwnerIDFilter = 0;

		#region *********************constructors*********************
		public UserFolders():base(){}

		/// <summary>
		/// Constructor that sets the filter on OwnerID
		/// </summary>
		/// <param name="iOwnerIDFilter"></param>
		public UserFolders(int iOwnerIDFilter):base()
		{
			this.SetFilter(iOwnerIDFilter);
		}
		/// <summary>
		/// Constructor that sets the filter on OwnerID and ParentFolderID
		/// </summary>
		/// <param name="iOwnerIDFilter"></param>
		/// <param name="iParentFolderID"></param>
		public UserFolders(int iOwnerIDFilter, int iParentFolderID1,
			int iParentFolderID2):base()
		{
			this.SetFilter(iOwnerIDFilter, iParentFolderID1, iParentFolderID2);
		}

		#endregion

		#region *********************StringIDSimpleDataCollection members*********************
		protected override StringIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new StringIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spUserFoldersAdd";}
		}

		protected override string CountSprocName
		{
			get	{ return "spUserFoldersCount"; }
		}

		protected override string DeleteSprocName
		{
			get{return "spUserFoldersDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get	{return "spUserFoldersItemFromID";}
		}

		protected override string SelectSprocName
		{
			get { return "spUserFolders"; }
		}

		protected override object[] SelectSprocParameters
		{
			get 
			{
				object[] oParams = new object[3];
				if(this.OwnerIDFilter == 0)
					oParams[0] = System.DBNull.Value;
				else
					oParams[0] = this.OwnerIDFilter;
				if (this.ParentFolderID1Filter == -1)
					oParams[1] = System.DBNull.Value;
				else
					oParams[1] = this.ParentFolderID1Filter;
				if (this.ParentFolderID2Filter == -1)
					oParams[2] = System.DBNull.Value;
				else
					oParams[2] = this.ParentFolderID2Filter;
				return oParams; 
			}
		}

		protected override object[] CountSprocParameters
		{
			get {return this.SelectSprocParameters;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get{return "spUserFoldersUpdate";}
		}

		public override StringIDSimpleDataItem Create()
		{
			return this.Create(m_iOwnerIDFilter, m_iParentFolderID1Filter,
				m_iParentFolderID2Filter);
		}

		public StringIDSimpleDataItem Create(int iOwnerID)
		{
			return this.Create(iOwnerID, m_iParentFolderID1Filter,
				m_iParentFolderID2Filter) ;
		}

		public StringIDSimpleDataItem Create(int iOwnerID, int iParentFolderID1,
			int iParentFolderID2)
		{
			if(!LocalPersons.Exists(iOwnerID))
			{
				//person with this ID does not exist
				throw new LMP.Exceptions.PersonIDException(
					LMP.Resources.GetLangString("Error_InvalidPersonID") + iOwnerID);
			}

			return new UserFolder(iOwnerID, iParentFolderID1, iParentFolderID2);
		}

		public override void Delete(string xID)
		{
			int iID1;
			int iID2;
			Application.SplitID(xID, out iID1, out iID2);

			// Delete child folders recursively
			UserFolders oChildFolders = new UserFolders();
			oChildFolders.m_iParentFolderID1Filter = iID1;
			oChildFolders.m_iParentFolderID2Filter = iID2;
			for (int i = oChildFolders.Count; i > 0; i--)
				oChildFolders.Delete(oChildFolders.ItemFromIndex(i).ID);
            // Delete FolderMembers - including contained UserSegments
            FolderMembers oMembers = new FolderMembers(iID1, iID2);
            for (int i = oMembers.Count; i > 0; i--)
                oMembers.Delete(oMembers.ItemFromIndex(i).ID);
			base.Delete(xID);
		}
        #endregion

        #region *********************properties*********************
        public int OwnerIDFilter
		{
			get{return m_iOwnerIDFilter;}
		}

		public int ParentFolderID1Filter
		{
			get{return m_iParentFolderID1Filter;}
		}

		public int ParentFolderID2Filter
		{
			get{return m_iParentFolderID2Filter;}
		}

		#endregion

		#region *********************methods*********************
		public void SetFilter(int iOwnerID, int iParentFolderID1, int iParentFolderID2)
		{
			Trace.WriteNameValuePairs("iOwnerID", iOwnerID, "iParentFolderID1",
				iParentFolderID1, "iParentFolderID2", iParentFolderID2);

			if(iOwnerID != this.OwnerIDFilter ||
				iParentFolderID1 != this.ParentFolderID1Filter ||
				iParentFolderID2 != this.ParentFolderID2Filter)
			{
				//filter has changed - 
				//alert if new value is invalid
				if(!LocalPersons.Exists(iOwnerID))
				{
					//person with this ID does not exist
					throw new LMP.Exceptions.PersonIDException(
						LMP.Resources.GetLangString("Error_InvalidPersonID") + iOwnerID);
				}
				m_iOwnerIDFilter = iOwnerID;
				m_iParentFolderID1Filter = iParentFolderID1;
				m_iParentFolderID2Filter = iParentFolderID2;
                m_oArray = null;
			}
		}

		public void SetFilter(int iOwnerID)
		{
			SetFilter(iOwnerID, 0, 0);
		}
        /// <summary>
        /// Returns High Index among Folders in this collection
        /// </summary>
        /// <returns></returns>
        public int GetHighIndex()
        {
            object oRet = GetScalar("spUserFoldersLastIndex", 
                new object[] { this.OwnerIDFilter, this.ParentFolderID1Filter, this.ParentFolderID2Filter });
            if (oRet != null && oRet != DBNull.Value)
                return (int)oRet;
            else
                return 0;
        }

        /// <summary>
        /// returns 2-column array with Name and ID of each member of collection
        /// </summary>
        /// <returns></returns>
        public ArrayList ToDisplayArray()
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "spUserFoldersForDisplay";

            //get reader with display fields only
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                xSQL, false, this.SelectSprocParameters))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                    LMP.Benchmarks.Print(t0);
                    return oArray;
                }
                finally
                {
                    oReader.Close();
                }
            }
        }

		#endregion

		#region *********************private functions*********************
		private StringIDSimpleDataItem UpdateObject(object[] oValues)
		{
			UserFolder oDef = new UserFolder(oValues[0].ToString() + "." + oValues[1].ToString());
			oDef.Name = oValues[2].ToString();
			oDef.Description = oValues[3].ToString();
			oDef.ParentFolderID1 = (int) oValues[4];
			oDef.ParentFolderID2 = (int) oValues[5];
            if (oValues[6] != DBNull.Value)
                oDef.Index = (int)oValues[6];
			oDef.XML = oValues[7].ToString();
			oDef.OwnerID = (int) oValues[8];
			oDef.IsDirty = false;
			return oDef;
		}
		#endregion
	}
}
