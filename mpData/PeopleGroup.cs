using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a
	/// MacPac PeopleGroup
	/// created by Jeffrey Sweetland 10.14.04
	/// </summary>
	public sealed class PeopleGroup: TranslatableDataItem
	{
        #region *********************fields*********************
        private string m_xName = null;
		private string m_xDescription = null;
		private bool m_bIsVisible = false;
		private bool m_bIsCIGroup = false;
		private ArrayList m_oArray = null;
        #endregion
        #region *********************constructors*********************
        internal PeopleGroup(){}
		internal PeopleGroup(int ID):base(ID){}
		#endregion
		#region *********************properties*********************
		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}

		public string Description
		{
			get{return m_xDescription;}
			set{this.SetStringPropertyValue(ref m_xDescription, value);}
		}

		public bool IsVisible
		{
			get{return m_bIsVisible;}
			set{this.SetBoolPropertyValue(ref m_bIsVisible, value);}
		}

		public bool IsCIGroup
		{
			get{return m_bIsCIGroup;}
			set{this.SetBoolPropertyValue(ref m_bIsCIGroup, value);}
		}

        public new int ID
        {
            get { return base.ID; }
            set { base.SetID(value); }
        }
		#endregion
		#region *********************methods*********************
		/// <summary>
		/// Returns Person object for 
		/// </summary>
		/// <param name="shIndex"></param>
		/// <returns></returns>
		public Person GetMember(short shIndex)
		{
			LoadMembers();
			if (m_oArray.Count < shIndex)
				throw new LMP.Exceptions.NotInCollectionException(Resources
					.GetLangString("Error_ItemAtIndexNotInCollection") + shIndex);
			object[] oMems = (object[]) m_oArray[shIndex];
			return LocalPersons.GetPersonFromID(oMems[0].ToString());
		}
		/// <summary>
		/// Returns the number of Persons assigned to this group
		/// </summary>
		public int MemberCount()
		{
			return (int) SimpleDataCollection.GetScalar(
				"spPeopleGroupMembersCount", new object[] {m_iID} );
		}
		/// <summary>
		/// Adds Group assignment for the specified Person
		/// </summary>
		/// <param name="iPersonID"></param>
		public void AddMember(int iPersonID)
		{
			AddMember(System.String.Concat(iPersonID.ToString(), ".0"));
		}
        /// <summary>
        /// Adds Group assignment for the specified Person
        /// </summary>
        /// <param name="xPersonID"></param>
        public void AddMember(string xPersonID)
        {
            int iID1;
            int iID2;
            if (m_iID == 0)
                throw new LMP.Exceptions.SaveRequiredException(Resources
                    .GetLangString("Error_GroupSaveRequired"));
            if (LocalPersons.Exists(xPersonID) == false)
                throw new LMP.Exceptions.PersonIDException(Resources
                    .GetLangString("Error_InvalidPersonID") + xPersonID);
            LocalPersons.SplitID(xPersonID, out iID1, out iID2);
            // must be public person
            if (iID2 != ForteConstants.mpPublicPersonID2)
                throw new LMP.Exceptions.PublicPersonRequiredException(Resources
                    .GetLangString("Error_PublicPersonRequired") + xPersonID);

            //execute add query
            SimpleDataCollection.ExecuteActionSproc(
                "spPeopleGroupMembersAdd", new object[] { iID1, m_iID, Application.GetCurrentEditTime() });

            //reset array object
            m_oArray = null;
        }
        /// <summary>
        /// Adds Group assignment for the specified Person
        /// </summary>
        /// <param name="xPersonIDs">comma delimited string of Person IDs (ID1)</param>
        public void AddMembers(string xPersonIDs)
        {
            if (m_iID == 0)
                throw new LMP.Exceptions.SaveRequiredException(Resources
                    .GetLangString("Error_GroupSaveRequired"));

            //execute add query
            SimpleDataCollection.ExecuteActionSproc(
                "spPeopleGroupMembersAddRange", new object[] {xPersonIDs, m_iID, Application.GetCurrentEditTime() });

            //reset array object
            m_oArray = null;
        }
		/// <summary>
		/// Removes group assignment for the specified Person
		/// </summary>
		/// <param name="iPersonID"></param>
		public void DeleteMember(int iPersonID)
		{
			DeleteMember(iPersonID.ToString() + ".0");
		}
		/// <summary>
		/// Removes group assignment for the specified Person
		/// </summary>
		/// <param name="xPersonID"></param>
		public void DeleteMember(string xPersonID)
		{
			int iID1;
			int iID2;
			if (m_iID == 0)
				throw new LMP.Exceptions.SaveRequiredException(Resources
					.GetLangString("Error_GroupSaveRequired"));
			LocalPersons.SplitID(xPersonID, out iID1, out iID2);

			//execute delete query
			int iRecordsAffected = SimpleDataCollection.ExecuteActionSproc(
				"spPeopleGroupMembersDelete", new object[] {iID1, m_iID});

			//write deletion to log
			string xID = iID1.ToString() + "." + m_iID.ToString();
			SimpleDataCollection.LogDeletions(mpObjectTypes.GroupAssignments, xID);

			if (iRecordsAffected == 0)
			{
			throw new LMP.Exceptions.NotInCollectionException(Resources
			.GetLangString("Error_PersonNotInGroup") + xPersonID);
            }

            m_oArray = null;
            

		}
		/// <summary>
		/// Returns 2-column array: ID and Display Name
		/// for all People assigned to this group
		/// </summary>
		/// <returns></returns>
		public Array ToMemberArray()
		{
			LoadMembers();
			//copy internal array to new array and return -
			//we do this so that the internal array can't be
			//modified by a client object
			Array oArray = new object[m_oArray.Count];
			m_oArray.CopyTo(0, oArray, 0, m_oArray.Count);
			return oArray;
		}
		/// <summary>
		/// Returns string which can be appended to the WHERE clause
		/// in an SQL statement to include records matching the IDs of group members
		/// </summary>
		/// <returns></returns>
		public string ToMemberSQLString()
		{
			int iID1;
			object[] o;
			StringBuilder oBldr = new StringBuilder();
			LoadMembers();
			for (int i = 0; i < m_oArray.Count; i++)
			{
				o = (object[]) m_oArray[i];
				iID1 = (int) o[0];
				if (i > 0)
					oBldr.Append(" OR ");
				oBldr.AppendFormat("([ID1]={0} And [ID2]=0)", iID1);
			}
			return oBldr.ToString();
		}
        /// <summary>
        /// Returns a DataSet of the members of the group.
        /// </summary>
        /// <returns></returns>
        public System.Data.DataSet ToMemberDataSet()
        {
            OleDbDataAdapter oAdapter = null;
            System.Data.DataSet oDS = null;

            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LocalConnection.ConnectionObject;
                oCmd.CommandText = "spPeopleGroupMembersByGroupID";
                oCmd.CommandType = CommandType.StoredProcedure;


                //add parameters to command
                oCmd.Parameters.Add(new OleDbParameter(this.ID.ToString(), this.ID));

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oDS = new DataSet("PeopleGroupMembersDataSet");
                    oAdapter.Fill(oDS);
                }

                return oDS;
            }

            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.StoredProcedureException(
                    Resources.GetLangString("Error_StoredProcedureFailed") +
                    "spPeopleGroupMembersByGroupID", oE);
            }
        }
		#endregion
		#region *********************TranslatableDataItem members*********************
		internal override string TableName
		{
			get{return "PeopleGroups";}
		}

		internal override string IDFieldName
		{
			get{return "ID";}
		}

		internal override byte ValuesCount
		{
			get{return 2;}
		}

		internal override bool IsValid()
		{
			//return true iff required info is present
			return (this.Name != "");
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[5];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.Description;
			oProps[3] = this.IsVisible;
			oProps[4] = this.IsCIGroup;

			return oProps;
		}

		internal override object[] GetAddQueryParameters()
		{
            object[] oProps = new object[6];
            oProps[0] = this.ID;
            oProps[1] = this.Name;
            oProps[2] = this.Description;
            oProps[3] = this.IsVisible;
            oProps[4] = this.IsCIGroup;
            oProps[5] = Application.GetCurrentEditTime();

            return oProps;
		}
        internal override object[] GetUpdateQueryParameters()
        {
            return this.GetAddQueryParameters();
        }
		#endregion
		#region **************************private members**************************
		private void LoadMembers()
		{
			//fill internal array if necessary
			if(m_oArray == null)
			{
				m_oArray = SimpleDataCollection.GetArray("spPeopleGroupMembersByGroupID",
					new object[] {m_iID} );
			}
		}
		#endregion
	}

	public class PeopleGroups: TranslatableDataCollection
    {
        #region *********************constants*********************
        public const int ADMIN_GROUP =  -2000001;
        public const int USER_DESIGNER_GROUP = -2000002;
        public const int MIN_CUSTOM_GROUP_ID = -2000003;
        #endregion
        #region *********************fields*********************
        private string m_xPersonFilter = null;
        #endregion
        #region *********************constructors*********************
        public PeopleGroups():base(){}

		public PeopleGroups(string xPersonFilter):base()
		{
			//initialize filter
			if (LocalPersons.Exists(xPersonFilter)== false)
				throw new LMP.Exceptions.PersonIDException(Resources
					.GetLangString("Error_InvalidPersonID") + m_xPersonFilter);
			m_xPersonFilter= xPersonFilter;
		}
		public PeopleGroups(int iPersonFilter):base()
		{
			//initialize filter
			//GLOG 8220
            if (LMP.Data.Application.AdminMode && LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                if (!PublicPersons.Exists(iPersonFilter))
                throw new LMP.Exceptions.PersonIDException(Resources
                    .GetLangString("Error_InvalidPersonID") + iPersonFilter.ToString());
            }
            else if (LocalPersons.Exists(iPersonFilter)== false)
				throw new LMP.Exceptions.PersonIDException(Resources
					.GetLangString("Error_InvalidPersonID") + iPersonFilter.ToString());
			m_xPersonFilter = System.String.Concat(iPersonFilter.ToString(), ".0");
		}

		#endregion
		#region *********************properties*********************
		/// <summary>
		/// returns the Owner ID filter
		/// </summary>
		public string PersonFilter
		{
			get{return m_xPersonFilter;}
		}
		#endregion
        #region *********************methods*********************
        public override int GetLastID()
        {
            return Math.Min(base.GetLastID(), MIN_CUSTOM_GROUP_ID + 1);
        }
        #endregion
        #region ************************TranslatableDataCollection members************************
        protected override string AddSprocName
		{
			get{return "spPeopleGroupsAdd";}
		}
	
		protected override string CountSprocName
		{
			get
			{
				//use different count sprocs for different filters
				if(m_xPersonFilter != null)
					return "spPeopleGroupsCountFiltered";
				else
					return "spPeopleGroupsCount";
			}
		}
	
		protected override string DeleteSprocName
		{
			get{return "spPeopleGroupsDelete";}
		}
	
		protected override string ItemFromIDSprocName
		{
			get
			{
				if(Language.CultureID == 1033)
					return "spPeopleGroupsItemFromID";
				else
					return "spPeopleGroupsItemFromIDTranslated";
			}
		}
	
		protected override string LastIDSprocName
		{
			get{return "spPeopleGroupsLastID";}
		}
	
		protected override string SelectSprocName
		{
			get
			{
				//build sproc name
				StringBuilder oSB = new StringBuilder("spPeopleGroups");
				if(this.PersonFilter != null)
					oSB.Append("Filtered");
				if (Language.CultureID != 1033)
					oSB.Append("Translated");
				return oSB.ToString();
			}
		}
	
		protected override object[] SelectSprocParameters
		{
			get
			{
				if(Language.CultureID != 1033)
				{
					if (m_xPersonFilter != null)
						return new object[] {this.GroupFilterString, Language.CultureID};
					else
						return new object[] {Language.CultureID};
				}
				else
				{
					if (m_xPersonFilter != null)
						return new object[] {this.GroupFilterString};
					else
						return null;
				}
			}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get
			{
				//no need to filter
				if (Language.CultureID == 1033)
					return null;
				else
					return new object[] {LMP.Resources.CultureID};
			}
		}

		protected override object[] CountSprocParameters
		{
			get
			{
				//no need to translate
				if(m_xPersonFilter != null)
					return new object[] {this.GroupFilterString};
				else
					return null;
			}
		}

		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				return new LMP.Data.LongIDUpdateObjectDelegate(this.UpdateObject);
			}
		}
	
		protected override string UpdateSprocName
		{
			get{return "spPeopleGroupsUpdate";}
		}
		
		public override LongIDSimpleDataItem Create()
		{
			return new PeopleGroup();
		}

		public override void Delete(int iID)
		{
            //Can't delete admin or power user groups
            if (iID == ADMIN_GROUP || iID == USER_DESIGNER_GROUP)
                throw new LMP.Exceptions.GroupsException(
                    LMP.Resources.GetLangString("Error_CantDeleteBuiltInGroup"));

			base.Delete (iID);

			// delete existing Group assignments
			SimpleDataCollection.ExecuteActionSproc(
				"spPeopleGroupAssignmentsDeleteByGroupID", new object[] {iID});

			//log deletion
			SimpleDataCollection.LogDeletions(mpObjectTypes.GroupAssignments, iID);
            
			//delete keysets
			SimpleDataCollection.ExecuteActionSproc(
				"spKeySetsDeleteByEntityIDs", new object[] {iID, ForteConstants.mpFirmRecordID});

			//log deletion
			SimpleDataCollection.LogDeletions(mpObjectTypes.KeySets, iID.ToString() + "." + ForteConstants.mpFirmRecordID.ToString());
            
			//delete relevant object assignments
			SimpleDataCollection.ExecuteActionSproc(
				"spAssignmentsDeleteByObjectIDs", new object[] {(int)mpObjectTypes.Group,iID});
            
			//log deletion
			SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, (int)mpObjectTypes.Group + "." + iID.ToString());

        }

		#endregion
		#region **************************private members**************************
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			PeopleGroup oPeopleGroup = new PeopleGroup((int) oValues[0]);
			oPeopleGroup.Name = oValues[1].ToString();
			oPeopleGroup.Description = oValues[2].ToString();
			if (oValues[3]!=DBNull.Value)
				oPeopleGroup.TranslationID = (int) oValues[3];
			oPeopleGroup.IsVisible = (bool) oValues[4];
			oPeopleGroup.IsCIGroup = (bool) oValues[5];
			oPeopleGroup.IsDirty = false;

			return oPeopleGroup;
		}
		private string GroupFilterString
		{
			// return comma-delimited list of groups for current owner filter
			get
			{
				System.Text.StringBuilder oBldr = null;
				if (m_xPersonFilter != null)
				{
					oBldr = new StringBuilder();
					int iID1;
					int iID2;
					LocalPersons.SplitID(m_xPersonFilter, out iID1, out iID2);
					System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
						"spPeopleGroupIDsByPersonID", new object[] {iID1});
					//append array items to string
					object[] aIDs = oArrayList.ToArray();
					foreach(object o in aIDs)
						oBldr.AppendFormat(",{0}", ((object[]) o)[0].ToString());
					return oBldr.ToString();
				}
				else
					return null;
			}
		}


		#endregion
	}
}
