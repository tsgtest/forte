using System;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace LMP.Data

{
	public delegate void PersonChangeHandler(object sender, PersonChangeEventArgs e);
	public delegate void BeforeGuestPersonCreated(object sender, PersonChangeEventArgs e);

	public class PersonChangeEventArgs: System.EventArgs
	{
		private Person m_oPerson = null;

		public PersonChangeEventArgs(Person oPerson)
		{
			m_oPerson = oPerson;
		}

		public Person PersonObject
		{
			get {return m_oPerson;}
		}
	}

	public enum mpPeopleListTypes: byte
	{
        AllPeopleInDatabase = 0,
        AllActivePeopleInDatabase = 1,
		AllPeople = 2,
		Public = 3,
		Private = 4,
        Users = 5,
        LocalPublicPeople = 6 //GLOG 8220
	}

	public enum mpPeopleNameFormats: byte
	{
		FullFormal = 0,
		FirstLast = 1,
		FirstMidLast = 2,
		LastFirst = 3,
		LastFirstMid = 4
	}

    public enum UsageStates
    {
        Inactive = 0,
        Active = 1,
        OfficeActive = 2
    }
	/// <summary>
	/// contains the methods and properties that define a MacPac Person
	/// created by Doug Miller - 07/04
	/// </summary>
	public class Person
	{
        #region ***********************************fields***********************************
		private string m_xID;
		private byte m_iUsageState;
		private byte m_iOfficeUsageState;
		private int m_iOwnerID;
		private int m_iLinkedPersonID;
		private string m_xSystemUserID;
		private bool m_bIsAuthor;
		private bool m_bIsAttorney;
		private string m_xDisplayName;
		private string m_xFullName;
		private string m_xLastName;
		private string m_xFirstName;
		private string m_xMiddleName;
		private string m_xNamePrefix;
		private string m_xNameSuffix;
        private int m_iOfficeID;
        private int m_iDefaultOfficeID;
        private string m_xGroupIDs;
		private bool m_bIsDirty = false;
		private bool m_bIsPersisted = false;
		private Hashtable m_oProperties;
		private LMP.Data.Office m_oOffice = null;
		private LMP.Data.AttorneyLicenses m_oLicenses = null;
        internal static Hashtable m_oPropertiesBaseHashtable;
        private Person m_oAliasBase = null;
        private const string ALIAS_LINKED_DATA = "[LINKED]";
        private bool m_bSavingRecord = false;
        private DateTime m_dLastEditTime;
        private bool m_bLinkedExternally = false;
        #endregion
		#region ***********************************constructors***********************************
		internal Person(string[,] oFields)
		{
			//create hashtable for custom fields 
			SetupProperties(oFields);
		}
		#endregion
		#region ***********************************properties***********************************

		/// <summary>
		/// sets/returns the edit state of the item
		/// </summary>
		public bool IsDirty
		{
			get{return m_bIsDirty;}
			set{m_bIsDirty = value;}
		}

		/// <summary>
		/// returns true if person is private
		/// </summary>
		public bool IsPrivate
		{
			get{return m_iOwnerID != 0 && m_iLinkedPersonID == 0;}
		}

		/// <summary>
		/// returns true if person is an alias
		/// </summary>
		public bool IsAlias
		{
			get{return ((m_iLinkedPersonID > 0) && (m_xSystemUserID == "") && m_iOwnerID > 0 );}
		}

        /// <summary>
        /// returns true iff this person is the "base" person
        /// </summary>
        public bool IsBasePerson
        {
            get { return this.LinkedPersonID == 0 && this.ID2 == ForteConstants.mpFirmRecordID; }
        }

		/// <summary>
		/// returns true iff the item has been saved to a MacPac data source
		/// </summary>
		internal virtual bool IsPersisted
		{
			get{return m_bIsPersisted;}
			set{m_bIsPersisted = value;}
		}

		/// <summary>
		/// returns the ID of this item
		/// </summary>
		public string ID
		{
			get{return m_xID;}
		}

        /// <summary>
        /// sets/returns the usage state of the item
        /// </summary>
        public byte UsageState
        {
            get { return m_iUsageState; }

            set
            {
                if (value != m_iUsageState)
                {
                    m_iUsageState = value;
                    m_bIsDirty = true;
                }
            }
        }

        /// <summary>
        /// sets/returns the usage state for this office record
        /// </summary>
        public byte OfficeUsageState
        {
            get { return m_iOfficeUsageState; }

            set
            {
                if (value != m_iOfficeUsageState)
                {
                    m_iOfficeUsageState = value;
                    m_bIsDirty = true;
                }
            }
        }
        internal DateTime LastEditTime
        {
            get { return m_dLastEditTime; }
            set { m_dLastEditTime = value; }
        }
        /// <summary>
        /// sets/returns the default office id of the user record
        /// </summary>
        public int DefaultOfficeRecordID
        {
            get
            {
                //will return 0 if not applicable -
                //only applicable to user records
                return m_iDefaultOfficeID;
            }

            set
            {
                if (this.LinkedPersonID != 0)
                {
                    //person is not a user - default office ID
                    //is not applicable
                    return;
                }

                if (value != m_iDefaultOfficeID)
                {
                    m_iDefaultOfficeID = value;
                    m_bIsDirty = true;
                }
            }
        }
        /// <summary>
        /// sets/returns the LinkedExternally property of the item
        /// </summary>
        public bool LinkedExternally
        {
            //GLOG 3768
            get { return m_bLinkedExternally; }
            set { m_bLinkedExternally = value; }
        }
        /// <summary>
        /// sets/returns the owner id of the item
        /// </summary>
        public int OwnerID
        {
            get { return m_iOwnerID; }

            set
            {
                if (value != m_iOwnerID)
                {
                    m_iOwnerID = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the linked person id of the item
		/// </summary>
		public int LinkedPersonID
		{
			get{return m_iLinkedPersonID;}

			set
			{
				if (value != m_iLinkedPersonID)
				{
					m_iLinkedPersonID = value;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/returns the system user id of the item
		/// </summary>
		public string SystemUserID
		{
			get{return m_xSystemUserID;}

			set
			{
				if (value != m_xSystemUserID)
				{
					m_xSystemUserID = value;
					m_bIsDirty = true;
				}
			}
		}

        /// <summary>
        /// returns true iff the person is a user
        /// </summary>
        public bool IsUser
        {
            get { return (this.SystemUserID != null) && 
                (this.SystemUserID != string.Empty); }
        }

		/// <summary>
		/// sets/returns whether this person is an author
		/// </summary>
		public bool IsAuthor
		{
			get{return m_bIsAuthor;}

			set
			{
				if (value != m_bIsAuthor)
				{
					m_bIsAuthor = value;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/returns whether this person is an attorney
		/// </summary>
		public bool IsAttorney
		{
			get{return m_bIsAttorney;}

			set
			{
				if (value != m_bIsAttorney)
				{
					m_bIsAttorney = value;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/returns the display name of the person
		/// </summary>
		public string DisplayName
		{
			get{return m_xDisplayName;}

			set
			{
				if (value != m_xDisplayName)
				{
					m_xDisplayName = value;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/returns the full name of the person
		/// </summary>
		public string FullName
		{
			get
            {
                if (this.IsAlias && m_xFullName == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("FullName");
                else
                    return m_xFullName;
            }

			set
			{
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("FullName");
                    if (value != xBaseValue && value != m_xFullName)
                    {
                        m_xFullName = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xFullName != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xFullName = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xFullName)
				{
					m_xFullName = value;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// sets/returns the last name of the person
		/// </summary>
		public string LastName
		{
            get
            {
                if (this.IsAlias && m_xLastName == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("LastName");
                else
                    return m_xLastName;
            }

            set
            {
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("LastName");

                    if (value != xBaseValue && value != m_xLastName)
                    {
                        m_xLastName = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xLastName != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xLastName = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xLastName)
                {
                    m_xLastName = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the first name of the person
		/// </summary>
		public string FirstName
		{
            get
            {
                if (this.IsAlias && m_xFirstName == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("FirstName");
                else
                    return m_xFirstName;
            }

            set
            {
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("FirstName");

                    if (value != xBaseValue && value != m_xFirstName)
                    {
                        m_xFirstName = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xFirstName != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xFirstName = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xFirstName)
                {
                    m_xFirstName = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the middle name of the person
		/// </summary>
		public string MiddleName
		{
            get
            {
                if (this.IsAlias && m_xMiddleName == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("MiddleName");
                else
                    return m_xMiddleName;
            }

            set
            {
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("MiddleName");

                    if (value != xBaseValue && value != m_xMiddleName)
                    {
                        m_xMiddleName = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xMiddleName != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xMiddleName = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xMiddleName)
                {
                    m_xMiddleName = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the name prefix of the person
		/// </summary>
		public string NamePrefix
		{
            get
            {
                if (this.IsAlias && m_xNamePrefix == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("NamePrefix");
                else
                    return m_xNamePrefix;
            }

            set
            {
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("NamePrefix");

                    if (value != xBaseValue && value != m_xNamePrefix)
                    {
                        m_xNamePrefix = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xNamePrefix != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xNamePrefix = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xNamePrefix)
                {
                    m_xNamePrefix = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the name suffix of the person
		/// </summary>
		public string NameSuffix
		{
            get
            {
                if (this.IsAlias && m_xNameSuffix == ALIAS_LINKED_DATA && !m_bSavingRecord)
                    //Return value from linked object
                    return GetBasePropertyValue("NameSuffix");
                else
                    return m_xNameSuffix;
            }

            set
            {
                if (this.IsAlias)
                {
                    string xBaseValue = GetBasePropertyValue("NameSuffix");

                    if (value != xBaseValue && value != m_xNameSuffix)
                    {
                        m_xNameSuffix = value;
                        m_bIsDirty = true;
                    }
                    else if (value == xBaseValue && m_xNameSuffix != ALIAS_LINKED_DATA)
                    {
                        //Mark as linked if same as base object value
                        m_xNameSuffix = ALIAS_LINKED_DATA;
                        m_bIsDirty = true;
                    }
                }
                else if (value != m_xNameSuffix)
                {
                    m_xNameSuffix = value;
                    m_bIsDirty = true;
                }
            }
        }

		/// <summary>
		/// sets/returns the office id of the person
		/// </summary>
		public int OfficeID
		{
			get{return m_iOfficeID;}

			set
			{
				if (value != m_iOfficeID)
				{
					m_iOfficeID = value;
					m_oOffice = null;
					m_bIsDirty = true;
				}
			}
		}

		/// <summary>
		/// returns the hashtable containing the names and values of
		/// all custom person fields
		/// </summary>
		public Hashtable Properties
		{
			get{return m_oProperties;}
		}

        /// <summary>
        /// Returns the Default Bar ID for the specified Jurisdiction levels
        /// </summary>
        public string DefaultBarID(int L0, int L1, int L2, int L3, int L4)
        {
            AttorneyLicense oLic = this.GetLicensesForLevel(L0, L1, L2, L3, L4).Default;
            if (oLic != null)
            {
                return oLic.License;
            }
            else
                return "";
        }

        internal bool SavingRecord
        {
            get { return m_bSavingRecord; }
            set { m_bSavingRecord = value; }
        }

        /// <summary>
        /// returns the person to whom this person is linked -
        /// returns the person if not linked
        /// </summary>
        public Person BasePerson
        {
            get
            {
                if (this.LinkedPersonID != 0)
                {
                    Persons oPeople;
                    //GLOG 8220
                    if (LMP.Data.Application.AdminMode && LMP.MacPac.MacPacImplementation.IsFullLocal)
                        oPeople = new LocalPersons(mpPeopleListTypes.LocalPublicPeople);
                    else
                        oPeople = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase);
                    Person oBasePerson = oPeople.ItemFromID(this.LinkedPersonID.ToString() + ".0");
                    return oBasePerson;
                }
                else
                    return this;
            }
        }

        /// <summary>
        /// returns ID1 of the person
        /// </summary>
        public int ID1
        {
            get
            {
                int iID1 = 0;
                int iID2 = 0;

                String.SplitID(this.ID, out iID1, out iID2);

                return iID1;
            }
        }

        /// <summary>
        /// returns ID2 of the person
        /// </summary>
        public int ID2
        {
            get
            {
                int iID1 = 0;
                int iID2 = 0;

                String.SplitID(this.ID, out iID1, out iID2);

                return iID2;
            }
        }
        public bool IsDefaultOffice
        {
            get
            {
                return this.GetDefaultOfficeID() == this.OfficeID;
            }
        }
        #endregion
		#region ***********************************methods***********************************
		/// <summary>
		/// sets this item's id property
		/// </summary>
		/// <param name="xID"></param>
		internal void SetID(string xID)
		{
			if (xID != m_xID)
			{
				m_xID = xID;
				m_bIsDirty = true;
			}
		}

		/// <summary>
		/// creates the internal hashtable for custom field names and values
		/// </summary>
		/// <param name="oFields"></param>
		private void SetupProperties(string[,] oFields)
		{
			Trace.WriteValues(oFields);

            if (m_oPropertiesBaseHashtable == null)
            {
                m_oProperties = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();

                //cycle through field names, adding the custom ones to the hashtable
                for (int i=0; i<=oFields.GetUpperBound(0); i++)
                {
                    string xField = oFields[i,0];

                    switch (xField.ToUpper())
                    {
                        case "ID1":
                        case "ID2":
                        case "OWNERID":
                        case "USAGESTATE":
                        case "USERID":
                        case "FULLNAME":
                        case "DISPLAYNAME":
                        case "LINKEDPERSONID":
                        case "LASTNAME":
                        case "FIRSTNAME":
                        case "MI":
                        case "PREFIX":
                        case "SUFFIX":
                        case "OFFICEID":
                        case "ISAUTHOR":
                        case "ISATTORNEY":
                        case "LASTEDITTIME":
                        case "LINKEDEXTERNALLY":
                            break;
                        default:
                            //this is a custom field
                            if (oFields[i, 1] == "System.Boolean")
                                m_oProperties.Add(xField, false);
                            else if (oFields[i, 1] == "System.Int32" || oFields[i,1]=="System.Byte")
                                m_oProperties.Add(xField, 0);
                            else
                                m_oProperties.Add(xField, null);
                            break;
                    }
                }

                //set up base for future use
                m_oPropertiesBaseHashtable = (Hashtable) m_oProperties.Clone();
            }
            else
            {
                //create properties hashtable from existing base
                m_oProperties = (Hashtable) m_oPropertiesBaseHashtable.Clone();
            }
		}

		/// <summary>
		/// sets value of custom property
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="oValue"></param>
		public void SetPropertyValue(string xName, object oValue)
		{
			if (!m_oProperties.ContainsKey(xName))
			{
				//property isn't in hashtable
				throw new LMP.Exceptions.PropertyNameException
					(Resources.GetLangString("Error_PropertyDoesNotExist") + xName);
			}
            if (this.IsAlias)
            {
                bool bIsBool=false;
                bool bTest;
                bool bIsInteger=false;
                int iTest;
                if (m_oProperties[xName] != null)
                {
                    bIsBool = bool.TryParse(m_oProperties[xName].ToString(), out bTest);
                    bIsInteger = int.TryParse(m_oProperties[xName].ToString(), out iTest);
                }
                if (bIsBool || bIsInteger)
                    m_oProperties[xName] = oValue;
                else
                {
                    if (oValue.ToString() == GetBasePropertyValue(xName))
                        //Mark as linked if same as base object value
                        m_oProperties[xName] = ALIAS_LINKED_DATA;
                    else
                        m_oProperties[xName] = oValue;
                }
            }
            else
            {
                m_oProperties[xName] = oValue;
            }
            m_bIsDirty = true;
        }

		/// <summary>
		/// returns value of custom property
		/// </summary>
		/// <param name="xName"></param>
		/// <returns></returns>
		public object GetPropertyValue(string xName)
		{
			object oVal = null;
			switch(xName.ToUpper())
			{
				case "DISPLAYNAME":
					oVal = this.DisplayName;
					break;
				case "FIRSTNAME":
					oVal = this.FirstName;
					break;
				case "FULLNAME":
					oVal = this.FullName;
					break;
				case "ID":
					oVal = this.ID;
					break;
				case "ISALIAS":
					oVal = this.IsAlias;
					break;
				case "ISAUTHOR":
					oVal = this.IsAuthor;
					break;
				case "ISATTORNEY":
					oVal = this.IsAttorney;
					break;
				case "ISPRIVATE":
					oVal = this.IsPrivate;
					break;
				case "LASTNAME":
					oVal = this.LastName;
					break;
				case "LINKEDPERSONID":
					oVal = this.LinkedPersonID;
					break;
				case "MIDDLENAME":
					oVal = this.MiddleName;
					break;
				case "NAMEPREFIX":
					oVal = this.NamePrefix;
					break;
				case "NAMESUFFIX":
					oVal = this.NameSuffix;
					break;
				case "OFFICEID":
					oVal = this.OfficeID;
					break;
				case "OWNERID":
					oVal = this.OwnerID;
					break;
                case "USERID":
                    oVal = this.SystemUserID;
                    break;
                case "OFFICEUSAGESTATE":
                    oVal = this.OfficeUsageState;
                    break;
                case "DEFAULTOFFICERECORDID1":
                    oVal = this.DefaultOfficeRecordID;
                    break;
                case "LINKEDEXTERNALLY":
                    //GLOG 3768: Use object property
                    oVal = this.LinkedExternally;
                    break;
                default:
				{
					//check for value of custom property
					if (!m_oProperties.ContainsKey(xName))
					{
						//property isn't in hashtable
						throw new LMP.Exceptions.PropertyNameException
							(Resources.GetLangString("Error_PropertyDoesNotExist") + xName);
					}
                    if (m_oProperties[xName] != null)
                    {
                        //If alias and this value is unassigned, return linked object value
                        if (this.IsAlias && m_oProperties[xName].ToString() == ALIAS_LINKED_DATA && !m_bSavingRecord)
                            oVal = GetBasePropertyValue(xName);
                        else
                            oVal = m_oProperties[xName];
                    }
                    else
                    {
                        //if value hasn't been set, oVal = a null object
                        oVal = new object();
                    }
					break;
				}
			}
			return oVal;
		}

        /// <summary>
        /// Return property value from linked object of alias
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        private string GetBasePropertyValue(string xName)
        {
            if (!this.IsAlias)
                return null;

            if (m_oAliasBase == null)
            {
                //GLOG 4645 (JTS 3/21/10): Make sure additional office records are included
                //LocalPersons oPersons = new LocalPersons(UsageStates.OfficeActive);
                //dcf - 8/11/11 - don't restrict the collection at all, as we
                //want to get the base person regardless of usage state
                LocalPersons oPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                m_oAliasBase = (Person)oPersons.ItemFromID(this.LinkedPersonID);
            }
            return m_oAliasBase.GetPropertyValue(xName).ToString();
        }

        /// <summary>
		/// returns array with property names and values for this person
		/// </summary>
		/// <returns></returns>
		public object[,] ToArray()
		{
			int iLength = 14 + m_oProperties.Count;
			object[,] oProps = new object[iLength, 2];

			//built-in properties
			oProps[0,0] = "ID";
			oProps[0,1] = this.ID;
			oProps[1,0] = "Display Name";
			oProps[1,1] = this.DisplayName;
			oProps[2,0] = "System User ID";
			oProps[2,1] = this.SystemUserID;
			oProps[3,0] = "Owner ID";
			oProps[3,1] = this.OwnerID;
			oProps[4,0] = "Is Author";
			oProps[4,1] = this.IsAuthor;
			oProps[5,0] = "Is Attorney";
			oProps[5,1] = this.IsAttorney;
			oProps[6,0] = "Linked Person ID";
			oProps[6,1] = this.LinkedPersonID;
			oProps[7,0] = "Full Name";
			oProps[7,1] = this.FullName;
			oProps[8,0] = "Prefix";
			oProps[8,1] = this.NamePrefix;
			oProps[9,0] = "First Name";
			oProps[9,1] = this.FirstName;
			oProps[10,0] = "Middle Name";
			oProps[10,1] = this.MiddleName;
			oProps[11,0] = "Last Name";
			oProps[11,1] = this.LastName;
			oProps[12,0] = "Suffix";
			oProps[12,1] = this.NameSuffix;
			oProps[13,0] = "Office ID";
			oProps[13,1] = this.OfficeID;

			//custom properties
			int i = 14;
			ICollection oKeys = m_oProperties.Keys;
			foreach (string oKey in oKeys)
			{
				oProps[i, 0] = oKey;
                if (this.IsAlias && m_oProperties[oKey].ToString() == ALIAS_LINKED_DATA)
                    //Return value from linked object
                    oProps[i, 1] = this.GetBasePropertyValue(oKey.ToString());
                else
                    oProps[i, 1] = m_oProperties[oKey];
				i++;
			}

			return oProps;
		}

		/// <summary>
		/// returns a delimited string of the group IDs to which this person belongs
		/// </summary>
		/// <returns></returns>
		public string GetGroupIDs()
		{
			DateTime t0 = DateTime.Now;
			System.Text.StringBuilder oBldr = null;

			if(m_xGroupIDs == null)
			{
				oBldr = new StringBuilder();
				int iID1;
				int iID2;

				//all users members of firm group
				oBldr.Append(ForteConstants.mpFirmRecordID);

				if(this.IsPrivate)
					//person can be assigned only to one office
					oBldr.AppendFormat(",{0}", m_iOfficeID.ToString());
				else
				{
					Application.SplitID(m_xID, out iID1, out iID2);

					//get list of office groups for person
					object[] aIDs = LocalPersons.GetOfficeIDs(
						this.LinkedPersonID != 0 ? this.LinkedPersonID : iID1);

					//append array items to string
					foreach(object o in aIDs)
						oBldr.AppendFormat(",{0}", ((object[]) o)[0].ToString());

					//get assigned groups
					aIDs = LocalPersons.GetPeopleGroupIDs(iID1);

					//append array items to string
					foreach(object o in aIDs)
						oBldr.AppendFormat(",{0}", ((object[]) o)[0].ToString());
				}
				m_xGroupIDs = oBldr.ToString();
			}
			Benchmarks.Print(t0);
			return m_xGroupIDs;
		}

        /// <summary>
        /// returns the default office ID o
        /// </summary>
        /// <returns></returns>
        public int GetDefaultOfficeID()
        {
            Person oBase = this.BasePerson;

            int iDefaultRecordID = oBase.DefaultOfficeRecordID;

            if ((iDefaultRecordID == 0) || (oBase.ID1 == oBase.DefaultOfficeRecordID))
                return oBase.OfficeID;
            else
            {
                try
                {
                    //get the secondary office person record
                    //specified by the default office record ID
                    Persons oPeople = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase);
                    Person oPerson = oPeople.ItemFromID(iDefaultRecordID.ToString() + ".0");
                    return oPerson.OfficeID;
                }
                catch
                {
                    return oBase.OfficeID;
                }
            }
        }
		/// <summary>
		/// returns string with name fields combined in the format specified
		/// </summary>
		/// <param name="iFormat"></param>
		/// <returns></returns>
		public string ToFormattedName(LMP.Data.mpPeopleNameFormats iFormat)
		{
			Trace.WriteNameValuePairs("iFormat", iFormat);

			StringBuilder oSB = new StringBuilder();

			//insert prefix
			if ((iFormat==mpPeopleNameFormats.FullFormal) && (m_xNamePrefix != ""))
				oSB.AppendFormat("{0} ", m_xNamePrefix);

			switch (iFormat)
			{
				case mpPeopleNameFormats.FirstLast:
				case mpPeopleNameFormats.FirstMidLast:
				case mpPeopleNameFormats.FullFormal:
					//first name first
					if (m_xFirstName != "")
						oSB.AppendFormat("{0} ", m_xFirstName);
					if ((iFormat != mpPeopleNameFormats.FirstLast) && (m_xMiddleName != ""))
						oSB.AppendFormat("{0} ", m_xMiddleName);
					if (m_xLastName != "")
						oSB.AppendFormat("{0} ", m_xLastName);
					break;
				case mpPeopleNameFormats.LastFirst:
				case mpPeopleNameFormats.LastFirstMid:
					//last name first
					oSB.Append(m_xLastName);
					if (m_xFirstName != "")
						oSB.AppendFormat(", {0}", m_xFirstName);
					if ((iFormat == mpPeopleNameFormats.LastFirstMid) && (m_xMiddleName != ""))
						oSB.AppendFormat(" {0}", m_xMiddleName);
					break;
				default:
					oSB.Append(m_xDisplayName);
					break;
			}

			//insert suffix
			if ((iFormat==mpPeopleNameFormats.FullFormal) && (m_xNameSuffix != ""))
				oSB.AppendFormat(" {0}", m_xNameSuffix);

			return oSB.ToString();
		}

        /// <summary>
        /// returns preference set of specified type and scope for this Person
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <returns></returns>
        public LMP.Data.KeySet PreferenceSet(LMP.Data.mpPreferenceSetTypes iType, string xScopeID)
        {
            string xEntityID;

            //if secondary record, get preferences for primary record
            if (m_iLinkedPersonID > 0)
                xEntityID = m_iLinkedPersonID.ToString();
            else
                xEntityID = m_xID;

            return KeySet.GetPreferenceSet(iType, xScopeID, xEntityID, m_iOfficeID);
        }

		/// <summary>
		/// returns number of custom person fields
		/// </summary>
		/// <returns></returns>
		public int GetPropertyCount()
		{
			return m_oProperties.Count;
		}

		/// <summary>
		/// returns delimited string with property names and values
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.ToString(LMP.StringArray.mpEndOfValue, LMP.StringArray.mpEndOfField);
		}

		/// <summary>
		/// returns delimited string with property names and values using
		/// specified delimiters
		/// </summary>
		/// <param name="xColumnSeparator"></param>
		/// <param name="xRowSeparator"></param>
		/// <returns></returns>
		public string ToString(char xColumnSeparator, char xRowSeparator)
		{
			StringBuilder oSB = new StringBuilder();
			oSB.AppendFormat("ID{0}{1}{2}", xColumnSeparator, m_xID, xRowSeparator);
			oSB.AppendFormat("Display Name{0}{1}{2}", xColumnSeparator,
				m_xDisplayName, xRowSeparator);
			if (m_xSystemUserID != "")
			{
				oSB.AppendFormat("System User ID{0}{1}{2}", xColumnSeparator,
					m_xSystemUserID, xRowSeparator);
			}
			if (m_iOwnerID != 0)
			{
				oSB.AppendFormat("Owner ID{0}{1}{2}", xColumnSeparator,
					m_iOwnerID, xRowSeparator);
			}
			oSB.AppendFormat("Is Author{0}{1}{2}", xColumnSeparator, m_bIsAuthor, xRowSeparator);
			oSB.AppendFormat("Is Attorney{0}{1}{2}", xColumnSeparator, m_bIsAttorney, xRowSeparator);
			if (m_iLinkedPersonID != 0)
			{
				oSB.AppendFormat("Linked Person ID{0}{1}{2}", xColumnSeparator,
					m_iLinkedPersonID, xRowSeparator);
			}
			oSB.AppendFormat("Full Name{0}{1}{2}", xColumnSeparator, m_xFullName, xRowSeparator);
			oSB.AppendFormat("Prefix{0}{1}{2}", xColumnSeparator, m_xNamePrefix, xRowSeparator);
			oSB.AppendFormat("First Name{0}{1}{2}", xColumnSeparator, m_xFirstName, xRowSeparator);
			oSB.AppendFormat("Middle Name{0}{1}{2}", xColumnSeparator, m_xMiddleName, xRowSeparator);
			oSB.AppendFormat("Last Name{0}{1}{2}", xColumnSeparator, m_xLastName, xRowSeparator);
			oSB.AppendFormat("Suffix{0}{1}{2}", xColumnSeparator, m_xNameSuffix, xRowSeparator);
			oSB.AppendFormat("Office ID{0}{1}{2}", xColumnSeparator, m_iOfficeID, xRowSeparator);

			//custom properties
			ICollection oKeys = m_oProperties.Keys;
			foreach (string oKey in oKeys)
			{
				oSB.AppendFormat("{0}{1}{2}{3}", oKey, xColumnSeparator,
					m_oProperties[oKey], xRowSeparator);
			}

			return oSB.ToString();
		}

		/// <summary>
		/// returns comma-delimited string containing ids of folders belonging to
		/// this person in specified parent folder
		/// </summary>
		/// <param name="iParentFolder"></param>
		/// <returns></returns>
		public string GetUserFolderIDs(int iParentFolderID1, int iParentFolderID2)
		{
			int iID1;
			int iID2;

			//no user folders for private person
			if (this.IsPrivate)
				return "";
			
			StringBuilder oSB = new StringBuilder();

			LocalPersons.SplitID(m_xID, out iID1, out iID2);
			object[] oIDs = LocalPersons.GetUserFolderIDs(iID1, iParentFolderID1,
				iParentFolderID2);

			//append array items to string
			foreach(object o in oIDs)
				oSB.AppendFormat("{0},", ((object[]) o)[0].ToString());

			//strip trailing comma
			if (oSB.Length > 0)
				oSB.Remove(oSB.Length - 1, 1);

			return oSB.ToString();
		}

		/// <summary>
		/// returns this person's office
		/// </summary>
		/// <returns></returns>
		public LMP.Data.Office GetOffice()
		{
			if (m_oOffice == null)
			{
				m_oOffice = (LMP.Data.Office) new Offices().ItemFromID(m_iOfficeID);
			}
			return m_oOffice;
		}

        /// <summary>
        /// returns all the person objects for this person
        /// </summary>
        /// <returns></returns>
        public LocalPersons RelatedPeople{
            get
            {
                Person oBase = this.BasePerson;

                if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    return new LocalPersons(
                        mpPeopleListTypes.LocalPublicPeople,
                        "((LinkedPersonID = " + oBase.ID1 + " AND ID2 = 0) OR (ID1 = " +
                        oBase.ID1 + " AND ID2 = 0 AND (OfficeUsageState = 1 OR IsNull(OfficeUsageState))))");
                }
                else
                {
                    return new LocalPersons(
                        mpPeopleListTypes.AllActivePeopleInDatabase,
                        "((LinkedPersonID = " + oBase.ID1 + " AND ID2 = 0) OR (ID1 = " +
                        oBase.ID1 + " AND ID2 = 0 AND (OfficeUsageState = 1 OR IsNull(OfficeUsageState))))");
                }
            }
        }
        /// <summary>
        /// returns all secondary office "people" for this person
        /// </summary>
        /// <returns></returns>
        public Persons GetAllSecondaryRecords()
        {
            //GLOG 4942: should include all active office records
            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                return new LocalPersons(mpPeopleListTypes.LocalPublicPeople, "LinkedPersonID=" + this.BasePerson.ID1 + " AND ID2=0");
            }
            else
            {
                return new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase, "LinkedPersonID=" + this.BasePerson.ID1 + " AND ID2=0");
            }
        }
        /// <summary>
        /// returns all aliases for this person
        /// </summary>
        /// <returns></returns>
        public Persons GetAliases()
        {
            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                return new LocalPersons(mpPeopleListTypes.LocalPublicPeople, "LinkedPersonID=" + this.ID1 + " AND ID2<>0");
            }
            else
            {
                return new LocalPersons("LinkedPersonID=" + this.ID1 + " AND ID2<>0");
            }
        }
        public void DeleteAliases()
        {
            Persons oAliases = GetAliases();

            for (int i = oAliases.Count; i>0; i--)
            {
                Person oAlias = (Person)oAliases.ItemFromIndex(i);
                oAliases.Delete(oAlias.ID);
            }
        }
        /// <summary>
        /// removes the specified office of the
        /// specified person - 
        /// </summary>
        /// <param name="iOfficeID"></param>
        public void RemoveOffice(int iOfficeID)
        {
            Persons oPersons = null;
            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                oPersons = new LocalPersons(mpPeopleListTypes.LocalPublicPeople);
            }
            else
            {
                oPersons = new LocalPersons();
            }
            Person oBasePerson = this.BasePerson;
            
            if (oBasePerson.OfficeID == iOfficeID)
            {
                //remove the office from the base person record
                oBasePerson.OfficeUsageState = 0;

                //save
                oPersons.Save(oBasePerson);
            }
            else if (this.OfficeID == iOfficeID)
            {
                //this person record is assigned to the
                //office that we're removing - set the
                //usage state to "delete" the record
                this.UsageState = 0;
                oPersons.Save(this);
            }
            else
            {
                //find the person record assigned to
                //the specified office
                Persons oSecondaryRecords = this.BasePerson.GetAllSecondaryRecords();
                int iNumRecs = oSecondaryRecords.Count;
                for (int i = 0; i < iNumRecs; i++)
                {
                    Person oPerson = oSecondaryRecords.ItemFromIndex(i);
                    if (oPerson.OfficeID == iOfficeID)
                    {
                        oPersons.Save(oPerson);
                        break;
                    }
                }
            }
        }
		/// <summary>
		/// returns this person's attorney licenses
		/// </summary>
		/// <returns></returns>
		public LMP.Data.AttorneyLicenses GetLicenses()
		{
			if (m_oLicenses == null)
			{
				m_oLicenses = new AttorneyLicenses(m_xID);
			}
			return m_oLicenses;
		}
        public LMP.Data.AttorneyLicenses GetLicensesForLevel(int L0, int L1, int L2, int L3, int L4)
        {
            //Get collection of licenses with nearest match to selected levels
            AttorneyLicenses oLic = new AttorneyLicenses(m_xID, L0, L1, L2, L3, L4);
            return oLic;
        }
		#endregion
        #region ***********************************static members***********************************
        /// <summary>
        /// returns the ID1 of the person with the specified system user id-
        /// returns a guest person if there is no person with the
        /// specified ID
        /// </summary>
        /// <param name="xSystemUserID"></param>
        /// <returns></returns>
        public static int PersonIDFromSystemUserName(string xSystemUserID)
        {
            try
            {
                return (int)SimpleDataCollection.GetScalar(
                    "spPersonIDFromSystemUserID", new object[] { xSystemUserID });
            }
            catch
            {
                return ForteConstants.mpGuestPersonID;
            }
        }

        /// <summary>
        /// returns the person with the specified system user id-
        /// returns a guest person if there is no person with the
        /// specified ID
        /// </summary>
        /// <param name="xSystemUserID"></param>
        /// <returns></returns>
        public static Person PersonFromSystemUserName(string xSystemUserID)
        {
            int iID;

            try
            {
                iID = (int)SimpleDataCollection.GetScalar(
                    "spPersonIDFromSystemUserID", new object[] { xSystemUserID });

                LocalPersons oPersons = new LocalPersons(iID);
                return oPersons.ItemFromID(iID);
            }
            catch(System.Exception oE)
            {
                //no person with the specified system user id exists
                return null;
            }

        }

        /// <summary>
        /// returns the person associated with the current system user-
        /// returns a guest person if there is no associated person
        /// </summary>
        /// <returns></returns>
        public static Person PersonFromSystemUserName()
        {
            return PersonFromSystemUserName(Environment.UserName);
        }

        public static bool IsGroupMember(int iPersonID1, int iPersonID2, int iGroupID)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("iPersonID1", iPersonID1.ToString(), "iPersonID2",
                iPersonID2.ToString(), "iGroupID", iGroupID);

            object oRes = SimpleDataCollection.GetScalar(
                "spPeopleGroupPersonIsMember", new object[] { iPersonID1, iPersonID2, iGroupID });

            bool bIsMember = oRes.ToString() == "-1";


            LMP.Benchmarks.Print(t0);

            return bIsMember;
        }

        /// <summary>
        /// returns true iff the person with the 
        /// specified ID is a MacPac 10 administrator
        /// </summary>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static bool HasAdminAccess(int iPersonID1, int iPersonID2)
        {
            return HasAdminAccess(iPersonID1, iPersonID2, true);
        }
        public static bool HasAdminAccess(int iPersonID1, int iPersonID2, bool bAllowForEmptyGroup)
        {
            DateTime t0 = DateTime.Now;

            PeopleGroups oGroups = new PeopleGroups();
            PeopleGroup oAdminGroup = (PeopleGroup)oGroups.ItemFromID(-2000001);

            //person has admin access if they're a member of the group
            //or if nobody has been assigned as an admin
            //GLOG 8289: Don't allow Admin login if Local Admin Group is empty unless specified
            bool bHasAccess = IsGroupMember(iPersonID1, 
                iPersonID2, PeopleGroups.ADMIN_GROUP) || (oAdminGroup.MemberCount() == 0 && bAllowForEmptyGroup);

            Trace.WriteNameValuePairs("iPersonID1", iPersonID1, 
                "iPersonID2", iPersonID2, "bHasAccess", bHasAccess);

            return bHasAccess;
        }

        /// <summary>
        /// returns true iff the person with the 
        /// specified ID full access to the user designer
        /// </summary>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static bool IsContentDesigner(int iUserID)
        {
            DateTime t0 = DateTime.Now;

            bool bIsContentDesigner = IsGroupMember(iUserID, 0, PeopleGroups.USER_DESIGNER_GROUP);

            Trace.WriteNameValuePairs("iUserID", iUserID, "bIsContentDesigner", bIsContentDesigner);

            return bIsContentDesigner;
        }

        ///// <summary>
        ///// returns true iff the person with the 
        ///// specified ID is a MacPac 10 administrator
        ///// </summary>
        ///// <param name="iPersonID"></param>
        ///// <returns></returns>
        //public static bool IsAdmin(int iPersonID)
        //{
        //    DateTime t0 = DateTime.Now;

        //    LocalConnection.ConnectIfNecessary(true);

        //    string xAdminIDs = SimpleDataCollection.GetScalar(
        //        "spMetadataItemFromID", new object[] { "AdminIDs" }).ToString();

        //    xAdminIDs += ",";

        //    Trace.WriteNameValuePairs("iPersonID", iPersonID, "xAdminIDs", xAdminIDs);

        //    LMP.Benchmarks.PrintBenchmark(t0);

        //    //return true iff the specified IDs are in the admin id string
        //    return xAdminIDs.IndexOf(iPersonID.ToString() + ",") > -1;
        //}

        /// <summary>
        /// returns true if the person with the 
        /// specified system user ID is a MacPac 10 administrator
        /// </summary>
        /// <param name="xSystemUserID"></param>
        /// <returns></returns>
        public static bool HasAdminAccess(string xSystemUserID)
        {

            return HasAdminAccess(xSystemUserID, true);
        }
        //GLOG 8289: Specify whether Admin access should be allowed when Admin Group is empty
        public static bool HasAdminAccess(string xSystemUserID, bool bAllowForEmptyGroup)
        {
            Trace.WriteNameValuePairs("xSystemUserID", xSystemUserID);

            return HasAdminAccess(PersonIDFromSystemUserName(xSystemUserID), 0, bAllowForEmptyGroup);
        }
        #endregion
    }
    abstract public class Persons
    {
        #region ***********************************fields***********************************
        protected int m_iCount = -1;
        protected static string[,] m_oFields;
        protected LMP.Data.mpPeopleListTypes m_iListTypeFilter;
        protected int m_iUserIDFilter;
        protected LMP.mpTriState m_iIsAttorneyFilter;
        protected LMP.mpTriState m_iIsAuthorFilter;
        protected UsageStates m_iUsageStateFilter;
        protected int m_iOfficeIDFilter;
        protected string m_xWhereFilter;
		protected ArrayList m_oArray;
        protected Person m_oPerson = null;
        #endregion
        #region ***********************************properties***********************************
        #endregion
        #region ***********************************methods***********************************
        /// <summary>
        /// builds where clause from filters applied to collection
        /// </summary>
        /// <returns></returns>
        protected string GetWhereClause()
        {
            //limit to active records
            
            StringBuilder oSB = new StringBuilder();
            if (m_iListTypeFilter != mpPeopleListTypes.AllPeopleInDatabase)
            {
                if (m_iUsageStateFilter == UsageStates.Active)
                {
                    oSB.AppendFormat("(p.UsageState=1)");
                }
                else if (m_iUsageStateFilter == UsageStates.Inactive)
                {
                    oSB.AppendFormat("(p.UsageState=0)");
                }
                else if (m_iUsageStateFilter == UsageStates.OfficeActive)
                    //GLOG 4552: Ensure only Active People appear in list
                    oSB.AppendFormat("((p.LinkedPersonID=0 AND p.ID2=0 AND p.UsageState=1 AND " +
                        "(p.OfficeUsageState=1 OR IsNull(p.OfficeUsageState))) " +
                        "OR (p.LinkedPersonID<>0 AND p.ID2=0 AND p.UsageState=1) OR " +
                        "(p.LinkedPersonID=0 AND p.ID2<>0 AND p.UsageState=1) OR " + 
                        "(p.LinkedPersonID<>0 AND p.ID2<>0 AND p.UsageState=1))");
            }

            //apply list type filter
            if (m_iListTypeFilter == mpPeopleListTypes.Public)
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("(p.ID2={0})", ForteConstants.mpPublicPersonID2);
            }
            else if (m_iListTypeFilter == mpPeopleListTypes.Private)
            {
                oSB.Append(" AND ");
                oSB.AppendFormat("(p.ID2<>{0})", ForteConstants.mpPublicPersonID2);
            }
            else if (m_iListTypeFilter == mpPeopleListTypes.Users)
            {
                oSB.Append(" AND ");
                oSB.AppendFormat("(p.ID2={0} AND p.LinkedPersonID=0)", ForteConstants.mpPublicPersonID2);
            }

            //apply user filter
            if (m_iUserIDFilter != 0)
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("(p.OwnerID={0} OR p.OwnerID = 0)", m_iUserIDFilter);
            }

            //apply office filter
            if (m_iOfficeIDFilter != 0)
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("(p.OfficeID={0})", m_iOfficeIDFilter);
            }

            //apply attorney filter
            if (m_iIsAttorneyFilter != mpTriState.Undefined)
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("(p.IsAttorney={0})", m_iIsAttorneyFilter == mpTriState.True ? -1 : 0);
            }

            //apply author filter
            if (m_iIsAuthorFilter != mpTriState.Undefined)
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("(p.IsAuthor={0})", m_iIsAuthorFilter == mpTriState.True ? -1 : 0);
            }

            //apply additional filter
            if (!string.IsNullOrEmpty(m_xWhereFilter))
            {
                if (oSB.Length > 0)
                    oSB.Append(" AND ");
                oSB.AppendFormat("({0})", m_xWhereFilter);
            }

            return oSB.ToString();
        }
        protected void UpdateObject(object[] oValues)
        {
            int iID1 = 0;
            int iID2 = 0;
            int iOffset = 0;
            int iValuesIndex = 0;

            Trace.WriteValues(oValues);

            //create new person, passing in array of field names
            m_oPerson = new Person(m_oFields);

            //cycle through values, updating properties - field names
            //array and values array are in the same order
            for (int i = 0; i <= m_oFields.GetUpperBound(0); i++)
            {
                iValuesIndex = i + iOffset;
                switch (m_oFields[i, 0].ToUpper())
                {
                    case "ID1":
                        iID1 = (int)oValues[iValuesIndex];
                        break;
                    case "ID2":
                        iID2 = (int)oValues[iValuesIndex];
                        break;
                    case "DISPLAYNAME":
                        m_oPerson.DisplayName = oValues[iValuesIndex].ToString();
                        break;
                    case "USERID":
                        m_oPerson.SystemUserID = oValues[iValuesIndex].ToString();
                        break;
                    case "LINKEDPERSONID":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            m_oPerson.LinkedPersonID = (int)oValues[iValuesIndex];
                        else
                            m_oPerson.LinkedPersonID = 0;
                        break;
                    case "FULLNAME":
                        m_oPerson.FullName = oValues[iValuesIndex].ToString();
                        break;
                    case "LASTNAME":
                        m_oPerson.LastName = oValues[iValuesIndex].ToString();
                        break;
                    case "FIRSTNAME":
                        m_oPerson.FirstName = oValues[iValuesIndex].ToString();
                        break;
                    case "MI":
                        m_oPerson.MiddleName = oValues[iValuesIndex].ToString();
                        break;
                    case "PREFIX":
                        m_oPerson.NamePrefix = oValues[iValuesIndex].ToString();
                        break;
                    case "SUFFIX":
                        m_oPerson.NameSuffix = oValues[iValuesIndex].ToString();
                        break;
                    case "OFFICEID":
                        m_oPerson.OfficeID = (int)oValues[iValuesIndex];
                        break;
                    case "DEFAULTOFFICERECORDID1":
                        if (oValues[iValuesIndex] != DBNull.Value)
                            m_oPerson.DefaultOfficeRecordID = (int)oValues[iValuesIndex];
                        break;
                    case "ISAUTHOR":
                        m_oPerson.IsAuthor = (bool)oValues[iValuesIndex];
                        break;
                    case "ISATTORNEY":
                        m_oPerson.IsAttorney = (bool)oValues[iValuesIndex];
                        break;
                    case "OWNERID":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            m_oPerson.OwnerID = (int)oValues[iValuesIndex];
                        else
                            m_oPerson.OwnerID = 0;
                        break;
                    case "USAGESTATE":
                        m_oPerson.UsageState = byte.Parse(oValues[iValuesIndex].ToString());
                        break;
                    case "OFFICEUSAGESTATE":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            m_oPerson.OfficeUsageState = byte.Parse(oValues[iValuesIndex].ToString());
                        else
                            m_oPerson.OfficeUsageState = 1;
                        break;
                    case "LASTEDITTIME":
                        m_oPerson.LastEditTime = DateTime.Parse(oValues[iValuesIndex].ToString());
                        break;
                    case "LINKEDEXTERNALLY":
                        string xVal = null;
                        try
                        {
                            //ignore error here - we're not concerned with
                            //this value except when set by pup
                            xVal = oValues[iValuesIndex].ToString();
                        }
                        catch { }

                        if (!string.IsNullOrEmpty(xVal))
                        {
                            //GLOG 3768: Need property for this to enable copying from Network to Local list
                            try
                            {
                                //field is an int for a local person (that's how it's returned from Access)
                                m_oPerson.LinkedExternally = (bool)oValues[iValuesIndex];
                            }
                            catch
                            {
                                //field is a string for a network person (that's how it's returned from SQL Server)
                                m_oPerson.LinkedExternally = bool.Parse(oValues[iValuesIndex].ToString());
                            }

                        }
                        break;
                    default:
                        //custom property
                        if (iValuesIndex <= oValues.GetUpperBound(0))
                            m_oPerson.SetPropertyValue(m_oFields[i, 0], oValues[iValuesIndex]);
                        else
                            m_oPerson.SetPropertyValue(m_oFields[i, 0], null);
                        break;
                }
            }

            //fill ID property
            m_oPerson.SetID(GetFormattedID(iID1, iID2));

            m_oPerson.IsDirty = false;
            m_oPerson.IsPersisted = true;
        }

        /// <summary>
        /// fills static array with names of all fields in People table
        /// </summary>
        protected static void GetFields()
        {
            OleDbDataReader oReader = null;
            OleDbCommand oCmd = new System.Data.OleDb.OleDbCommand();

            using (oCmd)
            {
                try
                {
                    //setup command
                    oCmd.Connection = LocalConnection.ConnectionObject;
                    oCmd.CommandText = "SELECT * FROM People";
                    oCmd.CommandType = CommandType.Text;

                    //execute
                    oReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly);
                    DataTable oDT = oReader.GetSchemaTable();

                    //load array
                    int iFields = oReader.FieldCount;
                    m_oFields = new string[iFields, 2];
                    for (int i = 0; i < iFields; i++)
                    {
                        m_oFields[i, 0] = oDT.Rows[i]["ColumnName"].ToString();
                        m_oFields[i, 1] = oDT.Rows[i]["DataType"].ToString();
                    }
                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.SQLStatementException(
                        Resources.GetLangString("Error_SQLStatementFailed") +
                        oCmd.CommandText, e);
                }

                finally
                {
                    oReader.Close();
                    oReader.Dispose();
                }
            }
        }
        /// <summary>
        /// builds string ID in format xxxx.xxxxxx from two numeric IDs
        /// </summary>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        /// <returns></returns>
        public static string GetFormattedID(int iID1, int iID2)
        {
            string xID = iID1.ToString();
            if (iID2 != ForteConstants.mpFirmRecordID)
                xID = System.String.Concat(xID, ".", iID2.ToString());
            return xID;
        }
        /// <summary>
        /// returns public person ID in xxxx.xxxxxx format
        /// </summary>
        /// <param name="iID1"></param>
        /// <returns></returns>
        public static string GetFormattedID(int iID1)
        {
            string xID = System.String.Concat(iID1.ToString(), ".",
                ForteConstants.mpPublicPersonID2.ToString());
            return xID;
        }
        /// <summary>
        /// updates existing item in internal array list after a save
        /// </summary>
        /// <param name="oPerson"></param>
        protected void UpdateArrayElement(LMP.Data.Person oPerson)
        {
            //get index of edited item
            int iIndex = GetItemIndex(oPerson.ID);

            Trace.WriteNameValuePairs("iIndex", iIndex);

            //update value array if item was found
            if (iIndex > -1)
            {
                int iValuesIndex = 0;
                int iID1;
                int iID2;

                SplitID(oPerson.ID, out iID1, out iID2);

                //populate an array with all fields - can't
                //use People.ToArray because that function doesn't
                //include all fields or know their order
                int iFields = m_oFields.GetUpperBound(0);

                object[] oArray = new object[iFields + 1];

                //JTS 5/11/09: Need <= since iFields is Upperbound
                for (int i = 0; i <= iFields; i++)
                {
                    iValuesIndex = i;
                    switch (m_oFields[i, 0].ToUpper())
                    {
                        case "ID1":
                            oArray[iValuesIndex] = iID1;
                            break;
                        case "ID2":
                            oArray[iValuesIndex] = iID2;
                            break;
                        case "DISPLAYNAME":
                            oArray[iValuesIndex] = oPerson.DisplayName;
                            break;
                        case "USERID":
                            oArray[iValuesIndex] = oPerson.SystemUserID;
                            break;
                        case "LINKEDPERSONID":
                            oArray[iValuesIndex] = oPerson.LinkedPersonID;
                            break;
                        case "FULLNAME":
                            oArray[iValuesIndex] = oPerson.FullName;
                            break;
                        case "LASTNAME":
                            oArray[iValuesIndex] = oPerson.LastName;
                            break;
                        case "FIRSTNAME":
                            oArray[iValuesIndex] = oPerson.FirstName;
                            break;
                        case "MI":
                            oArray[iValuesIndex] = oPerson.MiddleName;
                            break;
                        case "PREFIX":
                            oArray[iValuesIndex] = oPerson.NamePrefix;
                            break;
                        case "SUFFIX":
                            oArray[iValuesIndex] = oPerson.NameSuffix;
                            break;
                        case "OFFICEID":
                            oArray[iValuesIndex] = oPerson.OfficeID;
                            break;
                        case "ISAUTHOR":
                            oArray[iValuesIndex] = oPerson.IsAuthor;
                            break;
                        case "ISATTORNEY":
                            oArray[iValuesIndex] = oPerson.IsAttorney;
                            break;
                        case "OWNERID":
                            oArray[iValuesIndex] = oPerson.OwnerID;
                            break;
                        case "USAGESTATE":
                            oArray[iValuesIndex] = oPerson.UsageState;
                            break;
                        case "LASTEDITTIME":
                            oArray[iValuesIndex] = oPerson.LastEditTime;
                            break;
                        case "LINKEDEXTERNALLY":
                            //don't do anything - only pup can modify this field
                            //oArray[iValuesIndex] = oPerson.LinkedExternally;
                            break;
                        default:
                            oArray[iValuesIndex] = oPerson.GetPropertyValue(m_oFields[i, 0]);
                            break;
                    }
                }

                //update array list element with new array
                m_oArray[iIndex] = oArray;
            }
        }
        /// <summary>
        /// splits string ID in format xxxx.xxxxxx into two numeric IDs
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        public static void SplitID(string xID, out int iID1, out int iID2)
        {
            string[] aSplit = xID.Split('.');
            iID1 = Convert.ToInt32(aSplit[0]);
            if (aSplit.Length == 1)
                iID2 = ForteConstants.mpPublicPersonID2;
            else
            {
                if ((aSplit[1] == "0") || (aSplit[1] == ""))
                    iID2 = ForteConstants.mpPublicPersonID2;
                else
                    iID2 = Convert.ToInt32(aSplit[1]);
            }
        }
        /// <summary>
        /// creates and returns a new Person
        /// </summary>
        /// <returns></returns>
        public LMP.Data.Person Create()
        {
            //create new person
            m_oPerson = new Person(m_oFields);
            return m_oPerson;
        }
        /// <summary>
        /// deletes Person with specified ID
        /// </summary>
        /// <param name="xID"></param>
        public void Delete(string xID)
        {
            Delete(xID, false);
        }
        /// <summary>
        /// applies specified filters to collection
        /// </summary>
        /// <param name="iListType"></param>
        /// <param name="iUserID"></param>
        /// <param name="iIsAttorney"></param>
        /// <param name="iIsAuthor"></param>
        /// <param name="iOfficeID"></param>
        /// <param name="xWhere"></param>
        protected void SetFilters(LMP.Data.mpPeopleListTypes iListType, int iUserID,
            LMP.mpTriState iIsAttorney, LMP.mpTriState iIsAuthor, int iOfficeID, string xWhere, UsageStates iUsageState)
        {
            Trace.WriteNameValuePairs("iListType", iListType, "iUserID", iUserID,
                "iIsAttorney", iIsAttorney, "iIsAuthor", iIsAuthor, "iOfficeID", iOfficeID,
                "xWhere", xWhere);

            m_iListTypeFilter = iListType;
            m_iUserIDFilter = iUserID;
            m_iIsAttorneyFilter = iIsAttorney;
            m_iIsAuthorFilter = iIsAuthor;
            m_iOfficeIDFilter = iOfficeID;
            m_xWhereFilter = xWhere;
            m_iUsageStateFilter = iUsageState;
            m_oArray = null;
        }
        #endregion
        #region ***********************************abstract members***********************************
        /// <summary>
		/// returns the person at the specified collection index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
        abstract public Person ItemFromIndex(int iIndex);
        /// <summary>
        /// returns the person with the specified ID, from the local or network People table
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="bUseNetworkConnection"></param>
        /// <returns></returns>
        abstract public Person ItemFromID(string xID);
        abstract public void Save(LMP.Data.Person oPerson);
		/// <summary>
		/// saves current Person
		/// </summary>
        abstract public void Save();
        /// <summary>
        /// returns the collection index of the specified item
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        abstract public int GetItemIndex(string xID);
		/// <summary>
		/// returns this collection as a DataSet
		/// </summary>
		/// <returns></returns>
        abstract public System.Data.DataSet ToDataSet(bool bDisplayFieldsOnly);
        /// <summary>
        /// returns the display fields of this collection as a dataset -
        /// includes the office name as part of the display name, if specified
        /// </summary>
        /// <param name="bIncludeOfficeName"></param>
        /// <returns></returns>
        abstract public System.Data.DataSet ToDisplayFieldsDataSet(bool bIncludeOfficeName);
        /// <summary>
		/// returns the collection as an array - array contains all fields
		/// </summary>
		/// <returns></returns>
        abstract public Array ToArray();
		/// <summary>
		/// returns the collection as an array - array contains only the specified fields
		/// </summary>
		/// <param name="aFieldIndexes"></param>
		/// <returns></returns>
        abstract public Array ToArray(params int[] aFieldIndexes);
        abstract public void Delete(string xID, bool bUpdateFavorite);
        /// <summary>
		/// returns the number of items in the collection
		/// </summary>
        abstract public int Count{get;}
        #endregion
    }
	/// <summary>
	/// contains the methods and properties that manage a
	/// collection of MacPac Persons
	/// </summary>
	public class LocalPersons : Persons, IEnumerable
    {
        #region ***********************************fields***********************************
        //GLOG - 3691 - ceh
        private ArrayList m_oNetworkLicenses = null;
        private ArrayList m_oNetworkKeysets = null; //JTS 3/21/10
        private static Person m_oGuestPerson = null;
        public static event PersonChangeHandler PrivatePersonChanged;
        public event BeforeLostEditsHandler BeforeLostEdits;
        //GLOG 5033
        private const int MIN_NEW_PERSON_ID = 100000000;
        #endregion
        #region ***********************************constructors***********************************
        static LocalPersons()
		{
			GetFields();
		}

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney, LMP.mpTriState iIsAuthor, int iOfficeID, string xWhere)
        {
            SetFilters(iListType, iUserID, iIsAttorney, iIsAuthor, iOfficeID, xWhere, UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney)
        {
            SetFilters(iListType, iUserID, iIsAttorney, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, LMP.mpTriState iIsAttorney)
        {
            SetFilters(iListType, 0, iIsAttorney, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }

        public LocalPersons(LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType,
            int iUserID)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined, iIsAuthor, 0, "", UsageStates.Active);
        }

        public LocalPersons(LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, iIsAuthor, 0, "", UsageStates.Active);
        }

        public LocalPersons(int iOfficeID, LMP.Data.mpPeopleListTypes iListType, int iUserID)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", UsageStates.Active);
        }

        public LocalPersons(int iOfficeID, LMP.Data.mpPeopleListTypes iListType)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, string xWhere)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, string xWhere)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
        }
        
        public LocalPersons(string xWhere)
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }

        public LocalPersons(int iUserID)
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }

        public LocalPersons()
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
        }



        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney, LMP.mpTriState iIsAuthor, int iOfficeID, string xWhere, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, iIsAttorney, iIsAuthor, iOfficeID, xWhere, iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, iIsAttorney, LMP.mpTriState.Undefined, 0, "", iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, LMP.mpTriState iIsAttorney, UsageStates iUsageState)
        {
            SetFilters(iListType, 0, iIsAttorney, LMP.mpTriState.Undefined, 0, "", iUsageState);
        }

        public LocalPersons(LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType,
            int iUserID, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined, iIsAuthor, 0, "", iUsageState);
        }

        public LocalPersons(LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType, UsageStates iUsageState)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, iIsAuthor, 0, "", iUsageState);
        }

        public LocalPersons(int iOfficeID, LMP.Data.mpPeopleListTypes iListType, int iUserID, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", iUsageState);
        }

        public LocalPersons(int iOfficeID, LMP.Data.mpPeopleListTypes iListType, UsageStates iUsageState)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, string xWhere, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, string xWhere, UsageStates iUsageState)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, iUsageState);
        }

        public LocalPersons(string xWhere, UsageStates iUsageState)
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, int iUserID, UsageStates iUsageState)
        {
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", iUsageState);
        }

        public LocalPersons(int iUserID, UsageStates iUsageState)
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", iUsageState);
        }

        public LocalPersons(LMP.Data.mpPeopleListTypes iListType, UsageStates iUsageState)
        {
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, LMP.mpTriState.Undefined, 0, "", iUsageState);
        }

        public LocalPersons(UsageStates iUsageState)
        {
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", iUsageState);
        }
        #endregion
		#region ***********************************properties***********************************
		/// <summary>
		/// returns the list type filter that has been applied
		/// </summary>
		public LMP.Data.mpPeopleListTypes ListTypeFilter
		{
			get{return m_iListTypeFilter;}
		}
		/// <summary>
		/// returns the user id filter that has been applied
		/// </summary>
		public int UserIDFilter
		{
			get{return m_iUserIDFilter;}
		}
		/// <summary>
		/// returns the office id filter that has been applied
		/// </summary>
		public int OfficeIDFilter
		{
			get{return m_iOfficeIDFilter;}
		}
		/// <summary>
		/// returns the 'is attorney' filter that has been applied
		/// </summary>
		public LMP.mpTriState IsAttorneyFilter
		{
			get{return m_iIsAttorneyFilter;}
		}
		/// <summary>
		/// returns the 'is author' filter that has been applied
		/// </summary>
		public LMP.mpTriState IsAuthorFilter
		{
			get{return m_iIsAuthorFilter;}
		}
		/// <summary>
		/// returns the where clause filter that has been applied
		/// </summary>
		public string WhereFilter
		{
			get{return m_xWhereFilter;}
		}
        public UsageStates UsageStateFilter
        {
            get { return m_iUsageStateFilter; }
        }
        /// <summary>
        /// returns the number of items in the collection
        /// </summary>
        public override int Count
        {
            get
            {
                //we may already have count
                if (m_iCount != -1)
                    return m_iCount;

                //we may already have array - use it
                if (m_oArray != null)
                    return m_oArray.Count;

                string xSQL = "SELECT Count(*) FROM (" + this.GetSQL(false) + ")";
                OleDbCommand oCmd = new System.Data.OleDb.OleDbCommand();

                using (oCmd)
                {
                    try
                    {
                        //setup command
                        oCmd.Connection = LocalConnection.ConnectionObject;
                        oCmd.CommandText = xSQL;
                        oCmd.CommandType = CommandType.Text;

                        //execute and return
                        m_iCount = (int)oCmd.ExecuteScalar();
                        return m_iCount;
                    }

                    catch (System.Exception e)
                    {
                        throw new LMP.Exceptions.SQLStatementException(
                            Resources.GetLangString("Error_SQLStatementFailed") +
                            xSQL, e);
                    }
                }
            }
        }
		#endregion
		#region ***********************************methods***********************************
        /// <summary>
        ///  returns the person with the specified ID
        /// </summary>
        /// <param name="iID"></param>
        /// <returns></returns>
        public Person ItemFromID(int iID)
        {
            if (iID == ForteConstants.mpGuestPersonID)
                return m_oGuestPerson;
            else
                return ItemFromID(System.String.Concat(iID.ToString(), ".0"));
        }
        /// <summary>
        ///  returns the person with the specified ID
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public override Person ItemFromID(string xID)
        {
			Trace.WriteNameValuePairs("xID", xID);

			//requested item may already be the current one
			if ((m_oPerson != null) && (xID == m_oPerson.ID))
					return m_oPerson;

			//save current item if dirty and there are listeners requesting this
			if((m_oPerson != null) && (m_oPerson.IsDirty))
				SaveCurrentItemIfRequested();

			//set parameters
			int iID1;
			int iID2;
			string xSQL;

			SplitID(xID, out iID1, out iID2);

			xSQL = "SELECT * FROM (" + this.GetSQL(false) + ") as P2 WHERE (P2.ID1=" + iID1.ToString() +
                " AND P2.ID2=" + iID2.ToString() + ");";

			//execute query
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, true))
            {
                try
                {
                    if (oReader.HasRows)
                    {
                        //get row values
                        oReader.Read();
                        object[] oValues = new object[oReader.FieldCount];
                        oReader.GetValues(oValues);
                        UpdateObject(oValues);
                    }
                    else
                    {
                        //item was not found
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString("Error_ItemNotInCollection"));
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }

			return m_oPerson;
		}		
        /// <summary>
        /// Copy record for specified from Network to Local DB and return new Person object
        /// </summary>
        /// <param name="iID"></param>
        /// <returns></returns>
        public Person CopyNetworkUser(int iID)
        {
            return CopyNetworkUser(iID, false);
        }
        public Person CopyNetworkUser(int iID, bool bAddFavoriteRecord)
        {
            //get person from network db
            Person oPerson = null;
            Person oRelated = null;

            if (this.IsInCollection(iID))
            {
                throw new LMP.Exceptions.AlreadyInCollectionException(
                    LMP.Resources.GetLangString("Error_PersonAlreadyInLocalDB") + oPerson.DisplayName);
            }
            //GLOG - 3691 - ceh
            //GLOG 4380: Copy main person record and additional office records
            using (NetworkPersons oNetPersons = new NetworkPersons(null, null, mpPeopleListTypes.Public, 
                "(p.ID1=" + iID.ToString() + ") OR (p.LinkedPersonID=" + iID.ToString() + ")"))
            {
                oPerson = oNetPersons.ItemFromID(iID.ToString() + ".0");
                //GLOG 6943: If ID is for additional office record, return main person record
                if (oPerson.LinkedPersonID != 0)
                    return CopyNetworkUser(oPerson.LinkedPersonID, bAddFavoriteRecord);
                m_oNetworkLicenses = oNetPersons.GetLicenses(iID);
                //GLOG 4649 (JTS 3/21/10): Copy Keysets belonging to Network Person
                m_oNetworkKeysets = oNetPersons.GetKeysets(iID); 
                if (oPerson != null)
                {
                    oPerson.IsDirty = true;
                    oPerson.UsageState = 1;
                    //Record may already be in local DB
                    LocalPersons oAllPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                    oPerson.IsPersisted = oAllPersons.IsInCollection(iID);
                    Save(oPerson, false);
                }
                m_oNetworkLicenses = null;
                m_oNetworkKeysets = null; //GLOG 4649
                for (int i = 1; i <= oNetPersons.Count; i++)
                {
                    oRelated = oNetPersons.ItemFromIndex(i);
                    if (oRelated.ID != oPerson.ID)
                    {
                        //GLOG 4649 (JTS 3/21/10): Copy Keysets for additional offices
                        m_oNetworkKeysets = oNetPersons.GetKeysets(oRelated.ID1);
                        oRelated.IsDirty = true;
                        oRelated.UsageState = 1;
                        //Record may already be in local DB
                        LocalPersons oAllPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                        oRelated.IsPersisted = oAllPersons.IsInCollection(oRelated.ID1);
                        Save(oRelated, false);
                        m_oNetworkKeysets = null; //GLOG 4649
                    }
                }

                if (bAddFavoriteRecord)
                {
                    //GLOG 5721: Don't add Favorite if it already exists
                    //This generally wouldn't come up in practice -
                    //only if a previously deleted Favorite Firm Author
                    //has been 'resurrected' by manually changing UsageState to 1
                    if (!Application.User.GetAuthors().IsInCollection(iID))
                        AddFavoritePerson(Application.User.ID, iID);
                }
                return oPerson;
            }


        }
        /// <summary>
        /// Create record in network FavoritePeople table 
        /// for specified Owner and Person
        /// </summary>
        /// <param name="iOwnerID"></param>
        /// <param name="iPersonID"></param>
        public void AddFavoritePerson(int iOwnerID, int iPersonID)
        {
            SimpleDataCollection.ExecuteActionSproc("spFavoritePeopleAdd", 
                new object[] { iOwnerID, 0, iPersonID, 0, Application.GetCurrentEditTime()});
        }

        /// <summary>
        /// Remove record corresponding to specified Owner and 
        /// Person from network FavoritePeople table
        /// </summary>
        /// <param name="iOwnerID"></param>
        /// <param name="iPersonID"></param>
        public void DeleteFavoritePerson(int iOwnerID, int iPersonID)
        {
            SimpleDataCollection.ExecuteActionSproc("spFavoritePeopleDelete", 
                new object[] { iOwnerID, 0, iPersonID, 0 });
            //GLOG 3544: Log deletion so that Proxies will see changes
            SimpleDataCollection.LogDeletions(mpObjectTypes.FavoritePeople, iOwnerID.ToString() + "." + iPersonID.ToString());
        }
        public Person CopyPublicUser(int iID, bool bAddFavoriteRecord, bool bCheckForExisting)
        {
            //get person from public db
            Person oPerson = null;
            Person oRelated = null;

            if (bCheckForExisting && this.IsInCollection(iID))
            {
                throw new LMP.Exceptions.AlreadyInCollectionException(
                    LMP.Resources.GetLangString("Error_PersonAlreadyInLocalDB") + oPerson.DisplayName);
            }

            //GLOG 4380: Copy main person record and additional office records
            PublicPersons oPersons = new PublicPersons();
            oPerson = oPersons.ItemFromID(iID);

            if (oPerson != null)
            {
                oPerson.IsDirty = true;
                oPerson.UsageState = 1;
                //Record may already be in local DB
                LocalPersons oAllPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                oPerson.IsPersisted = oAllPersons.IsInCollection(iID);
                Save(oPerson, false);
            }

            m_oNetworkLicenses = oPersons.GetLicenses(iID);
            //GLOG 8220
            if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                m_oNetworkKeysets = oPersons.GetKeysets(iID);

            this.UpdateLicenses(oPerson);
            //GLOG 8220
            if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                this.UpdateKeysets(oPerson);

            ArrayList oRelatedPeopleIDs = SimpleDataCollection.GetArray("SELECT ID1 FROM PeoplePublic WHERE ID2=0 AND LinkedPersonID = " + iID.ToString());

            for (int i = 0; i < oRelatedPeopleIDs.Count; i++)
            {
                m_oNetworkLicenses = null;
                m_oNetworkKeysets = null;

                object[] oIDs = (object[])oRelatedPeopleIDs[i];

                oRelated = oPersons.ItemFromID(oIDs[0].ToString());
                if (oRelated.ID != oPerson.ID)
                {
                    //GLOG 8220
                    if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                    {
                        //GLOG 4649 (JTS 3/21/10): Copy Keysets for additional offices
                        m_oNetworkKeysets = oPersons.GetKeysets(oRelated.ID1);
                    }
                    oRelated.IsDirty = true;
                    oRelated.UsageState = 1;
                    //Record may already be in local DB
                    LocalPersons oAllPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                    oRelated.IsPersisted = oAllPersons.IsInCollection(oRelated.ID1);
                    Save(oRelated, false);

                    this.UpdateLicenses(oPerson);
                    //GLOG 8220
                    if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                    {
                        this.UpdateKeysets(oPerson);
                    }
                }
            }

            if (bAddFavoriteRecord)
            {
                //GLOG 5721: Don't add Favorite if it already exists
                //This generally wouldn't come up in practice -
                //only if a previously deleted Favorite Firm Author
                //has been 'resurrected' by manually changing UsageState to 1
                if (!Application.User.GetAuthors().IsInCollection(iID))
                    AddFavoritePerson(Application.User.ID, iID);
            }
            return oPerson;
        }
        public void SyncPublicPeople(OleDbConnection oCnn)
        {
            string xSQL = "spPeopleSyncPublicPeople";
            try
            {
                LMP.Data.SimpleDataCollection.ExecuteActionSproc(oCnn, xSQL, null);
            }
            catch { }
        }
#if false
        /// <summary>
        /// Create record in network FavoritePeople table for specified Owner and Person
        /// </summary>
        /// <param name="iOwnerID"></param>
        /// <param name="iPersonID"></param>
        public void AddFavoritePersonOLD(int iOwnerID, int iPersonID)
        {
            object[] oParams;
            oParams = new object[] { iOwnerID, 0, iPersonID, 0 };
            using (NetworkConnection oCn = new NetworkConnection())
            {
                oCn.ConnectToDB();
                try
                {
                    SimpleDataCollection.ExecuteActionSproc(
                        oCn.ConnectionObject, "spFavoritePeopleAdd", oParams);
                }
                catch { }
                finally
                {
                    oCn.Close();
                }
            }
        }

        /// <summary>
        /// Remove record corresponding to specified Owner and Person from network FavoritePeople table
        /// </summary>
        /// <param name="iOwnerID"></param>
        /// <param name="iPersonID"></param>
        public void DeleteFavoritePersonOLD(int iOwnerID, int iPersonID)
        {
            object[] oParams;
            oParams = new object[] { iOwnerID, 0, iPersonID, 0 };

            using (NetworkConnection oCn = new NetworkConnection())
            {
                try
                {
                    SimpleDataCollection.ExecuteActionSproc(oCn.ConnectionObject,
                        "spFavoritePeopleDelete", oParams);
                }
                catch { }
                finally
                {
                    oCn.Close();
                }
            }
        }
#endif
        /// <summary>
        /// Delete corresponding record in People table
        /// 'Delete' method just sets UsageState=0
        /// </summary>
        /// <param name="xID"></param>
        public void DeleteRecordFromDB(string xID)
        {
            int iID1 = 0;
            int iID2 = 0;
            SplitID(xID, out iID1, out iID2);
            //First perform normal delete to remove from collection
            try
            {
                this.Delete(xID);
            }
            catch 
            { 
                //No record with this ID - exit
                return; 
            }
            //Run Delete query to remove Person record
            int iRecs = SimpleDataCollection.ExecuteActionSproc("spPersonDeleteRecord", new object[] { iID1, iID2 });

        }
        public void DeleteRecordFromDB(int iID)
        {
            DeleteRecordFromDB(iID.ToString() + ".0");
        }
		/// <summary>
		/// returns the person at the specified collection index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public override Person ItemFromIndex(int iIndex)
		{
			Trace.WriteNameValuePairs("iIndex", iIndex);

			//ensure that index is not less than 1
			if(iIndex < 1)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (",iIndex, ")"));

			//fill internal array if necessary
			if(m_oArray == null)
				FillArray();

			//ensure that index is not beyond upper bound
			if(iIndex > m_oArray.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemNotInCollection")," (",iIndex, ")"));

			object[] oValues = (object[]) m_oArray[iIndex - 1];

			//save current item if dirty and there are listeners requesting this
			if((m_oPerson != null) && (m_oPerson.IsDirty))
				SaveCurrentItemIfRequested();

			UpdateObject(oValues);
			return m_oPerson;
		}
        /// <summary>
        /// returns the SQL statement used to retrieve people
        /// </summary>
        /// <param name="bDisplayFieldsOnly"></param>
        /// <returns></returns>
        private string GetSQL(bool bDisplayFieldsOnly)
        {
            return GetSQL(bDisplayFieldsOnly, false, false);
        }
        /// <summary>
        /// returns the SQL statement used to retrieve people
        /// </summary>
        /// <param name="bDisplayFieldsOnly"></param>
        /// <returns></returns>
        private string GetSQL(bool bDisplayFieldsOnly, bool bIncludeOfficeName)
        {
            return GetSQL(bDisplayFieldsOnly, bIncludeOfficeName, false);
        }        
		/// <summary>
        /// returns the SQL statement used to retrieve people
        /// </summary>
        /// <param name="bDisplayFieldsOnly"></param>
        /// <returns></returns>
        private string GetSQL(bool bDisplayFieldsOnly, bool bIncludeOfficeName, bool bIncludeLoginID)
        {
            string xPeopleTbl = "People";
            if (this.ListTypeFilter == mpPeopleListTypes.LocalPublicPeople && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                xPeopleTbl = "PeoplePublic";

            //build SQL statement
            StringBuilder oSB = new StringBuilder("SELECT ");

            if (bDisplayFieldsOnly && bIncludeOfficeName)
                //get display name and ID - display name should include office name
                //oSB.Append("p.DisplayName & iif(p.ID2=0 AND p.LinkedPersonID<>0, ' (' & o.DisplayName & ')', '') AS DisplayName, (p.ID1 & '.' & p.ID2) AS ID");
                oSB.Append("p.DisplayName & '  (' & o.DisplayName & ')' AS DisplayName, (p.ID1 & '.' & p.ID2) AS ID");
            // GLOG : 6027 : CEH
            else if(bDisplayFieldsOnly && bIncludeLoginID)
                //get display name and ID - display name should include office name
                oSB.Append("p.DisplayName AS DisplayName, (p.ID1 & '.' & p.ID2) AS ID, p.UserID AS LoginID");
            else if (bDisplayFieldsOnly)
                //get display name and ID - display name should include office name
                oSB.Append("p.DisplayName AS DisplayName, (p.ID1 & '.' & p.ID2) AS ID");
            else
            {
                //get all fields in people table
                for (int i = 0; i <= m_oFields.GetUpperBound(0); i++)
                {
                    if (m_oFields[i, 0].ToUpper() == "LASTEDITTIME")
                        oSB.Append("p.LastEditTime as LastEditTime, ");
                    else
                    {
                        string xField = m_oFields[i, 0];

                        //custom property fieldnames might contain spaces or other characters
                        //that affect the sql. Use brackets to prevent such issues.
                        xField = "[" + xField + "]";

                        oSB.AppendFormat("p.{0}, ", xField);
                    }
                }
                oSB.Remove(oSB.Length - 2, 2);
            }

            if (this.ListTypeFilter == mpPeopleListTypes.AllActivePeopleInDatabase ||
                this.ListTypeFilter == mpPeopleListTypes.AllPeopleInDatabase || 
                this.ListTypeFilter == mpPeopleListTypes.LocalPublicPeople)
            {
                oSB.Append(" FROM " + xPeopleTbl + " p INNER JOIN Offices o ON p.OfficeID = o.ID ");
            }
            else
            {
                //this base sql statement retrieves all people that should be displayed
                //for the current user - i.e. the user, his/her secondary offices, private people, and
                //people for whom the user can proxy - we need this because there may be people
                //in the people table that shouldn't be displayed - people that are in the db
                //because other users logged into macpac and ran user syncs

                string xID = "";
                if (this.UserIDFilter == 0)
                    //GLOG 8015/8021 (dm) - for Tools, use guest person id
                    if (MacPac.MacPacImplementation.IsToolkit || MacPac.MacPacImplementation.IsToolsAdministrator)
                        xID = ForteConstants.mpGuestPersonID.ToString();
                    else
                        xID = LMP.Data.Application.User.ID.ToString();
                else
                    xID = this.UserIDFilter.ToString();

                //GLOG 4403: Don't display Proxy Users if Proxying is disabled
                FirmApplicationSettings oFirmApp = new FirmApplicationSettings(Int32.Parse(xID));
                bool bIncludeProxies = oFirmApp.AllowProxying;
                //GLOG 4380: Include alternate Office records if UsageState=OfficeActive
                oSB.Append(" FROM (SELECT * FROM ((SELECT * FROM " + xPeopleTbl + " p LEFT JOIN (SELECT PersonID1, " +
                    "PersonID2, PersonID1 is not null as IsFavorite FROM FavoritePeople WHERE OwnerID1=" +
                    //JTS 3/21/10: Only include FavoritePeople Records that correspond to Base Person record in People
                    //JTS 3/26/10: This has been restructured to avoid issues with Jet Query engine when Proxying is disabled
                    xID + " AND PersonID1 IN (SELECT ID1 FROM " + xPeopleTbl + " WHERE LinkedPersonID=0)) AS f " + 
                    "ON ((f.PersonID1=p.ID1 AND f.PersonID2=p.ID2)" + 
                    (m_iUsageStateFilter == UsageStates.OfficeActive 
                    //JTS 3/26/10: Alternate JOIN ON conditions must have same fields defined
                    ? " OR (f.PersonID1=p.LinkedPersonID AND p.ID2=0)" : "") + 
                    ")) AS a" +

                    //GLOG 4403: This SQL included only when Proxying is enabled
                    (bIncludeProxies ? " LEFT JOIN (SELECT x.UserID, x.UserID2, x.ProxyID FROM Proxies x " +
                    "WHERE x.ProxyID=" + xID + ") AS b ON (a.ID1=b.UserID AND a.ID2=b.UserID2)" : "") + 
                    //****end optional SQL

                    ") WHERE f.IsFavorite OR (a.ID1=" + xID + " AND a.ID2=0) OR a.LinkedPersonID = " + xID +
                    //GLOG item #5588 - dcf -  needed to included aliases of secondary offices
                    " OR (a.LinkedPersonID IN (SELECT ID1 from " + xPeopleTbl + " AS p2 WHERE p2.ID2=0 And p2.UsageState=1 AND p2.LinkedPersonID = " + xID + "))" +
                    //GLOG 4403: Don't display an Alias unless it is for the current User or 
                    //a defined Favorite or Proxy User
                    " OR (a.OwnerID=" + xID + " AND (a.LinkedPersonID = 0 OR " + 
                    "(a.LinkedPersonID IN (SELECT PersonID1 FROM FavoritePeople fp INNER JOIN " + xPeopleTbl + " p ON p.ID1=fp.PersonID1 AND p.ID2=fp.PersonID2 WHERE fp.OwnerID1 = " + xID + " AND p.UsageState=1)) OR " +
                    //GLOG 4645 (JTS 3/21/10): Include Aliases of Public People Secondary Office records
                    //GLOG 5721: But only if both Office Record and Base Record have UsageState of 1
                    "(a.LinkedPersonID IN (SELECT ID1 from " + xPeopleTbl + " AS p2 WHERE p2.ID2=0 And p2.UsageState=1 AND p2.LinkedPersonID IN " + 
                    "(SELECT PersonID1 FROM FavoritePeople fp INNER JOIN " + xPeopleTbl + " p ON p.ID1=fp.PersonID1 AND p.ID2=fp.PersonID2 WHERE fp.OwnerID1 = " + xID + " AND p.UsageState=1)))" +

                    //GLOG 4403: This SQL included only when Proxying is enabled
                    //GLOG 5614: Make sure aliases for Proxies with separate Default Office Records are included
                    //GLOG 8449: Omit Firm People who have been deleted
                    (bIncludeProxies ? " OR (a.LinkedPersonID IN (SELECT px.UserID FROM Proxies px INNER JOIN " + xPeopleTbl + " px2 ON px2.ID1=px.UserID AND px2.ID2=px.UserID2 " + 
                    "WHERE px.ProxyID = " + xID + " AND px2.UsageState=1)) " +
                    "OR (a.LinkedPersonID IN (SELECT ppx.ID1 From " + xPeopleTbl + " ppx WHERE ppx.ID2=0 AND ppx.UsageState=1 AND ppx.LinkedPersonID IN " +
                    "(SELECT UserID FROM Proxies WHERE ProxyID=" + xID + ")))" : "") + 
                    //****end optional SQL
                    "))" +

                    //GLOG 4403: This SQL included only when Proxying is enabled
                    //GLOG 5614: Make sure Proxies with separate Default Office Records are included
                    //GLOG 8449: Omit Firm People who have been deleted
                    (bIncludeProxies ? " OR (a.OwnerID=0 AND a.LinkedPersonID IN (SELECT px.UserID FROM Proxies px INNER JOIN " + xPeopleTbl + " px2 ON px2.ID1=px.UserID AND px2.ID2=px.UserID2 " + 
                    "WHERE px.ProxyID = " + xID + " AND px2.UsageState=1)) " + 
                    "OR b.UserID is not null" : "") + 
                    //****end optional SQL

                    ") AS c INNER JOIN Offices AS o ON c.OfficeID = o.ID");
            }

            //add WHERE clause
            string xWhere = GetWhereClause();
            if (xWhere != "")
                oSB.AppendFormat(" WHERE ({0})", xWhere);

            //add ORDER BY clause
            oSB.Append(" ORDER BY ");
            if (xWhere.ToUpper().IndexOf("USERID=") != -1)
                oSB.Append("p.LinkedPersonID, ");
            oSB.Append("p.DisplayName, p.LinkedPersonID=0, o.DisplayName");

            return oSB.ToString();
        }
        /// <summary>
		/// fills the internal array containing all persons and fields in the collection
		/// </summary>
		private void FillArray()
		{
			//get reader with all fields
            using (OleDbDataReader oReader = GetPeople(false))
            {
                try
                {
                    //fill array
                    m_oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                }
                finally
                {
                    oReader.Close();
                }
            }
		}

		/// <summary>
		/// returns a data reader containing people as per applied filters -
		/// if bDisplayFieldsOnly=true, returns only Display Name and ID
		/// fields; otherwise, returns all fields
		/// </summary>
		/// <param name="bDisplayFieldsOnly"></param>
		/// <returns></returns>
		private OleDbDataReader GetPeople(bool bDisplayFieldsOnly)
		{
			string xSQL = GetSQL(bDisplayFieldsOnly);

			OleDbDataReader oReader = SimpleDataCollection
				.GetResultSet(xSQL, false);
			return oReader;
		}

		/// <summary>
		/// saves the current item dirty and there
		/// are listeners requesting this
		/// </summary>
		private void SaveCurrentItemIfRequested()
		{
			//check for listeners
			if(this.BeforeLostEdits != null)
			{
				//there are listeners - cycle through each
				//invoking their handlers
				foreach(BeforeLostEditsHandler oHandler in 
					this.BeforeLostEdits.GetInvocationList())
				{
					BeforeLostEditsEventArgs e = new BeforeLostEditsEventArgs();

					//invoke event handler
					oHandler(this, e);

					//handler responded that current item should be saved - save
					if(e.SaveCurrentItem)
					{
						Save(m_oPerson);
						return;
					}
				}
			}
		}
        public override void Save(LMP.Data.Person oPerson)
        {
            Save(oPerson, true);
        }
		/// <summary>
		/// saves the specified Person
		/// </summary>
		/// <param name="oPerson"></param>
		private void Save(LMP.Data.Person oPerson, bool bUpdateLastEditTime)
		{
			Trace.WriteNameValuePairs("oPerson.IsDirty", oPerson.IsDirty);

			if(oPerson.IsDirty)
			{
				//JTS 3/21/10: Avoid unhandled error if ID has not yet been assigned
                if (oPerson.ID != null)
                    Trace.WriteNameValuePairs("oPerson.ID", oPerson.ID);

				if (oPerson.IsPersisted)
				{
					//update people table
					UpdateRecord(oPerson, bUpdateLastEditTime);
                    UpdateLicenses(oPerson);
                    //GLOG 8220: Don't copy keysets in Local mode
                    if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                        UpdateKeysets(oPerson);

					if(m_oArray != null)
					{
						//save changes to array
						UpdateArrayElement(oPerson);
					}

					//if person is private, raise changed event -
					//allows subscribers to update their static person objects
					if ((oPerson.IsPrivate) && (PrivatePersonChanged != null))
						PrivatePersonChanged(this, new PersonChangeEventArgs(oPerson));
				}
				else
				{
					//add record to people table
					AddRecord(oPerson, bUpdateLastEditTime);
                    UpdateLicenses(oPerson);
                    UpdateKeysets(oPerson);

					if(m_oArray != null)
					{
						//force array to repopulate upon next request
						m_oArray = null;
					}
				}
				
				oPerson.IsPersisted = true;
				oPerson.IsDirty = false;
			}
		}

		/// <summary>
		/// saves current Person
		/// </summary>
		public override void Save()
		{
			Save(m_oPerson, true);
		}
        private void UpdateRecord(LMP.Data.Person oPerson)
        {
            UpdateRecord(oPerson, true);
        }
		/// <summary>
		/// updates an existing record in people table
		/// </summary>
		/// <param name="oPerson"></param>
		private void UpdateRecord(LMP.Data.Person oPerson, bool bUpdateLastEditTime)
		{
			int iID1;
			int iID2;

            string xPeopleTbl = "People";
            if (this.ListTypeFilter == mpPeopleListTypes.LocalPublicPeople && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                xPeopleTbl = "PeoplePublic";
            SplitID(oPerson.ID, out iID1, out iID2);

            //Return unchanged Alias values with [LINKED] text
            if (oPerson.IsAlias)
                oPerson.SavingRecord = true;

            string xNamePrefix = oPerson.NamePrefix;
            string xLastName = oPerson.LastName;
            string xFirstName = oPerson.FirstName;
            string xMiddleName = oPerson.MiddleName;
            string xNameSuffix = oPerson.NameSuffix;

            if (xNamePrefix != null)
                xNamePrefix = xNamePrefix.Replace("\"", "\"\"");
            if (xLastName != null)
                xLastName = xLastName.Replace("\"", "\"\"");
            if (xFirstName != null)
                xFirstName = xFirstName.Replace("\"", "\"\"");
            if (xMiddleName != null)
                xMiddleName = xMiddleName.Replace("\"", "\"\"");
            if (xNameSuffix != null)
                xNameSuffix = xNameSuffix.Replace("\"", "\"\"");

			//construct update query
			StringBuilder oSB = new StringBuilder("UPDATE " + xPeopleTbl + " AS p SET ");
			oSB.AppendFormat(@"p.OwnerID = {0}, p.UsageState = {1}, p.UserID = ""{2}"", ",
				oPerson.OwnerID, oPerson.UsageState, oPerson.SystemUserID);
			oSB.AppendFormat(@"p.DisplayName = ""{0}"", p.LinkedPersonID = {1}, p.Prefix = ""{2}"", ",
                oPerson.DisplayName.Replace("\"", "\"\""), oPerson.LinkedPersonID, xNamePrefix);
			oSB.AppendFormat(@"p.LastName = ""{0}"", p.FirstName = ""{1}"", p.MI = ""{2}"", ",
               xLastName, xFirstName, xMiddleName);
			oSB.AppendFormat(@"p.Suffix = ""{0}"", p.FullName = ""{1}"", p.OfficeID = {2}, ",
                xNameSuffix, oPerson.FullName.Replace("\"", "\"\""), oPerson.OfficeID);
			oSB.AppendFormat("p.IsAttorney = {0}, p.IsAuthor = {1}, p.LinkedExternally = {2}, ",
				System.Convert.ToInt32(oPerson.IsAttorney),
				System.Convert.ToInt32(oPerson.IsAuthor),
                System.Convert.ToInt32(oPerson.LinkedExternally));

			//custom fields
			ICollection oKeys = oPerson.Properties.Keys;
			foreach (string oKey in oKeys)
			{
				//get value
                object oValue = oPerson.GetPropertyValue(oKey);
				//get data type
				string xType = oValue.GetType().ToString();
				//if value is still null, omit from query
				if (xType != "System.DBNull")
				{
					//custom field name might contain space
                    //bracket field name so query will run
                    //string xKey = oKey;
                    //if (xKey.Contains(" "))
                    //    xKey = "[" + xKey + "]";
                    
                    oSB.AppendFormat(@"p.[{0}] = ", oKey);
					switch (xType)
					{
                        case "System.Int32":
                        case "System.Byte":
                        case "System.Boolean":
							oSB.AppendFormat("{0}, ", oValue);
							break;
						default:
                            oSB.AppendFormat(@"""{0}"", ", oValue.ToString().Replace("\"", "\"\""));
							break;
					}
				}
			}

			//GLOG 3691: Don't change LastEditTime when copying Network person
            //GLOG 6673:  Make sure Date in SQL string can be interpreted as US Format
            if (bUpdateLastEditTime)
                oSB.AppendFormat("p.LastEditTime = #{0}# ", Application.GetCurrentEditTime(true));
            else
                oSB.AppendFormat("p.LastEditTime = #{0}# ", oPerson.LastEditTime.ToString(Culture.USEnglishCulture));
			oSB.AppendFormat("WHERE (((p.ID1)={0}) AND ((p.ID2)={1}));", iID1, iID2);

            if (oPerson.IsAlias)
                oPerson.SavingRecord = false;

            //execute query
			string xSQL = oSB.ToString();
			SimpleDataCollection.ExecuteAction(xSQL);
		}
        private void AddRecord(LMP.Data.Person oPerson)
        {
            AddRecord(oPerson, true);
        }
		/// <summary>
		/// adds a new record to people table
		/// </summary>
		/// <param name="oPerson"></param>
		private void AddRecord(LMP.Data.Person oPerson, bool bUpdateLastEditTime)
		{
			int iID1;
			int iID2;

            string xPeopleTbl = "People";
            if (this.ListTypeFilter == mpPeopleListTypes.LocalPublicPeople && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                xPeopleTbl = "PeoplePublic";

            //generate id
			if (oPerson.OwnerID != 0)
				//record belongs to user - id1 is user's id1 and
				//id2 is the current time
				Application.GetNewUserDataItemID(out iID1, out iID2);
			else if (!System.String.IsNullOrEmpty(oPerson.ID))
            {
                LMP.String.SplitID(oPerson.ID, out iID1, out iID2);
                if (iID2 == ForteConstants.mpFirmRecordID)
                    iID2 = ForteConstants.mpPublicPersonID2;
            }
            else
			{
				//record belongs to firm - get next id
				iID1 = (int) SimpleDataCollection.GetScalar(
					"spPeopleLastID", null) + 1;
                //GLOG 5033: Manually added people get IDs >= 100000000
                if (iID1 < MIN_NEW_PERSON_ID)
                    iID1 = MIN_NEW_PERSON_ID;
				//id2 is mpPublicPersonID2
				iID2 = ForteConstants.mpPublicPersonID2;
			}

            //Return unchanged Alias values with [LINKED] text
            if (oPerson.IsAlias)
                oPerson.SavingRecord = true;

			//construct query
			StringBuilder oSB1 = new StringBuilder("INSERT INTO " + xPeopleTbl + " ( ID1, ID2, OwnerID, ");
			oSB1.Append("UsageState, UserID, DisplayName, LinkedPersonID, Prefix, LastName, ");
			oSB1.Append("FirstName, MI, Suffix, FullName, OfficeID, ");
			oSB1.Append("IsAttorney, IsAuthor, LinkedExternally, LastEditTime");

			StringBuilder oSB2 = new StringBuilder(@"SELECT ");
            //GLOG 8024/GLOG 8026: Replace any quotes in string values with double quotes
            string xDisplay = oPerson.DisplayName != null ? oPerson.DisplayName.Replace("\"", "\"\"") : null;
            string xPrefix = oPerson.NamePrefix != null ? oPerson.NamePrefix.Replace("\"", "\"\"") : null;
            string xSuffix = oPerson.NameSuffix != null ? oPerson.NameSuffix.Replace("\"", "\"\"") : null;
            string xLast = oPerson.LastName != null ? oPerson.LastName.Replace("\"", "\"\"") : null;
            string xFirst = oPerson.FirstName != null ? oPerson.FirstName.Replace("\"", "\"\"") : null;
            string xMiddle = oPerson.MiddleName != null ? oPerson.MiddleName.Replace("\"", "\"\"") : null;
            string xFull = oPerson.FullName != null ? oPerson.FullName.Replace("\"", "\"\"") : null;

            oSB2.AppendFormat(@"{0}, {1}, {2}, {3}, ""{4}"", ""{5}"", ", iID1, iID2,
                oPerson.OwnerID, oPerson.UsageState, oPerson.SystemUserID, xDisplay);
            oSB2.AppendFormat(@"{0}, ""{1}"", ""{2}"", ""{3}"", ""{4}"", ""{5}"", ", oPerson.LinkedPersonID, xPrefix, xLast, xFirst, xMiddle, xSuffix);
            //GLOG 6673:  Make sure Date in SQL string can be interpreted as US Format
            oSB2.AppendFormat(@"""{0}"", {1}, {2}, {3}, {4}, #{5}#", xFull, oPerson.OfficeID, System.Convert.ToInt32(oPerson.IsAttorney),
                System.Convert.ToInt32(oPerson.IsAuthor), System.Convert.ToInt32(oPerson.LinkedExternally),
                bUpdateLastEditTime ? Application.GetCurrentEditTime(true) : oPerson.LastEditTime.ToString(Culture.USEnglishCulture));

			//custom fields
			ICollection oKeys = oPerson.Properties.Keys;
			foreach (string oKey in oKeys)
			{
				//get value
				object oValue = oPerson.GetPropertyValue(oKey);
				//get data type
				string xType = oValue.GetType().ToString();
				//if value of property hasn't been set, omit from query
				if (xType != "System.Object")
				{
					oSB1.AppendFormat(@", [{0}]", oKey);
					switch (xType)
					{
						case "System.Int32":
						case "System.Boolean":
							oSB2.AppendFormat(", {0}", oValue);
							break;
						default:
                            oSB2.AppendFormat(@", ""{0}""", oValue.ToString().Replace("\"", "\"\"")); //GLOG 8024
							break;
					}
				}
			}

            if (oPerson.IsAlias)
                oPerson.SavingRecord = false;
            
            //execute query
			string xSQL = System.String.Concat(oSB1.ToString(), ") ", oSB2.ToString(), ";");
			SimpleDataCollection.ExecuteAction(xSQL);
			oPerson.SetID(System.String.Concat(iID1.ToString(), ".", iID2.ToString()));
		}

        /// <summary>
        /// updates licenses for local person
        /// </summary>
        /// <param name="oPerson"></param>
         
        //GLOG - 3691 - ceh
        private void UpdateLicenses(LMP.Data.Person oPerson)
        {
            //GLOG - 3875 - ceh
            //there are no Network Licences to update
            if (m_oNetworkLicenses == null)
                return;

            string xPreviousID = "";

            //get licenses for local person
            AttorneyLicenses oLicenses = new AttorneyLicenses();

            //JTS 5/6/09: Don't delete - record will be updated or added based on IsPersisted property
            //int iCount = oLicenses.Count;

            ////delete existing local licenses
            //for (int i = 0; i < iCount; i++)
            //{
            //    oLicenses.Delete(oLicenses[1].ID.ToString());
            //}

            //create new local licenses from network
            int iCount = m_oNetworkLicenses.Count;

            for (int i = 0; i < iCount; i++)
            {
                object[] aVals = (Object[])m_oNetworkLicenses[i];
                string xNetworkID = aVals[0] + "." + aVals[1];
                //Create a new license
                AttorneyLicense oNewLicense = (AttorneyLicense)oLicenses.Create(oPerson.ID);

                StringIDSimpleDataItem oTest = null;
                try
                {
                    oTest = oLicenses.ItemFromID(xNetworkID);
                }
                catch { }
                oNewLicense.IsPersisted = (oTest != null);
                //Set license properties
                oNewLicense.SetID(xNetworkID);
                oNewLicense.Description = aVals[4].ToString();
                oNewLicense.License = aVals[5].ToString();
                oNewLicense.L0 = (int)aVals[6];
                try
                {
                    oNewLicense.L1 = (int)aVals[7];
                }
                catch { }

                try
                {
                    oNewLicense.L2 = (int)aVals[8];
                }
                catch { }

                try
                {
                    oNewLicense.L3 = (int)aVals[9];
                }
                catch { }

                try
                {
                    oNewLicense.L4 = (int)aVals[10];
                }
                catch { }

                oNewLicense.LastEditTime = DateTime.Parse(aVals[12].ToString());
                //Save the new license
                oLicenses.Save(oNewLicense);

                //Save previous license ID
                xPreviousID = oNewLicense.ID;
            }
        }


        private void UpdateKeysets(LMP.Data.Person oPerson)
        {
            //there are no Network Keysets to update
            if (m_oNetworkKeysets == null || m_oNetworkKeysets.Count == 0)
                return;

            //JTS 5/6/09: Don't delete - record will be updated or added based on IsPersisted property
            //int iCount = oLicenses.Count;

            ////delete existing local licenses
            //for (int i = 0; i < iCount; i++)
            //{
            //    oLicenses.Delete(oLicenses[1].ID.ToString());
            //}

            //create new local licenses from network
            int iCount = m_oNetworkKeysets.Count;

            for (int i = 0; i < iCount; i++)
            {
                object[] aVals = (Object[])m_oNetworkKeysets[i];
                string xScopeID = aVals[0] + "." + aVals[1];
                string xOwnerID = aVals[2] + "." + aVals[3];
                mpKeySetTypes iType = (mpKeySetTypes)short.Parse(aVals[4].ToString());
                int iCulture = Int32.Parse(aVals[5].ToString()); //GLOG 7633
                StringArray aValues = new StringArray(aVals[6].ToString()); //GLOG 7633
                //Create a new license
                KeySet oNewKeySet = new KeySet(iType, xScopeID, xOwnerID, 0, iCulture);

                for (int f = 0; f < aValues.FieldCount(); f++)
                {
                    string xField = "";
                    string xValue = "";
                    try
                    {
                        //JTS 4/01/10: Deal with Value strings with extra or missing final delimiters
                        xField = aValues[f, 0];
                        if (aValues.ValueCount(f) > 1)
                            xValue = aValues[f, 1];
                    }
                    catch { }
                    if (xField != "")
                    {
                        //JTS 4/6/10: Ignore error if string contains non-existent keyset field
                        try
                        {
                            oNewKeySet.SetValue(xField, xValue);
                        }
                        catch { }
                    }
                }
                //Save the new Keyset record
                oNewKeySet.Save();
            }
        }

        /// <summary>
        /// returns the collection index of the specified item
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public override int GetItemIndex(string xID)
        {
            int iID1;
            int iID2;

            //fill internal array if necessary
            if (m_oArray == null)
                FillArray();

            SplitID(xID, out iID1, out iID2);

            object[] oArray = m_oArray.ToArray();

            //cycle through array items, 
            //looking for match for both ID1 and ID2
            for (int i = 0; i < m_oArray.Count; i++)
            {
                object[] oValues = (object[])oArray[i];
                if (((int)oValues[0] == iID1) && ((int)oValues[1] == iID2))
                {
                    return i;
                }
            }

            return -1;
        }
		public override void Delete(string xID, bool bUpdateFavorite)
		{
			int iID1;
			int iID2;
            bool bIsBasePerson = false; //GLOG 5721

			SplitID(xID, out iID1, out iID2);
			object[] oIDs = new object[]{iID1, iID2};

			Trace.WriteNameValuePairs("iID1", iID1, "iID2", iID2);

            if (iID2 == 0)
            {
                Person oPerson = this.ItemFromID(xID);
                oPerson.DeleteAliases();
                if (oPerson.LinkedPersonID == 0)
                {
                    //GLOG 5721: This is not a related Office record
                    bIsBasePerson = true;
                    if (!bUpdateFavorite)
                    {
                        //GLOG 4546: Delete any People Group assignments and Log Deletions
                        PeopleGroups oGroups = new PeopleGroups(iID1);
                        for (int i = 1; i <= oGroups.Count; i++)
                        {
                            ((PeopleGroup)oGroups.ItemFromIndex(i)).DeleteMember(iID1);
                        }
                    }
                    //GLOG 5271: Also Delete Related Office records
                    Persons oRelatedPeople = oPerson.RelatedPeople;
                    for (int i = oRelatedPeople.Count; i > 0; i--)
                    {
                        Person oRelatedPerson = oRelatedPeople.ItemFromIndex(i);
                        if (oRelatedPerson.ID != oPerson.ID)
                        {
                            oRelatedPeople.Delete(oRelatedPerson.ID, bUpdateFavorite);
                        }

                    }
                }
            }

            //execute query
            //GLOG 3544: If a Firm Favorite is being deleted, we don't want to 
            //remove any related records, since they may still be used by other Logins
            //Just delete record from FavoritePeople
            if (iID2 != 0 || !bUpdateFavorite)
            {
			    //GLOG 3544: Need to Update LastEditDate when UsageState is changed
                if (m_iListTypeFilter == mpPeopleListTypes.LocalPublicPeople)
                {
                    string xSQL = "UPDATE PeoplePublic SET UsageState=0, LastEditTime=#" + LMP.Data.Application.GetCurrentEditTime() + "# WHERE ID1=" + iID1 + " AND ID2=" + iID2 + ";";
                    int iRecsAffected = SimpleDataCollection.ExecuteAction(xSQL);
                    if (iRecsAffected == 0)
                    {
                        //not deleted - alert
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
                    }
                }
                else
                {
                    int iRecsAffected = SimpleDataCollection
                        .ExecuteActionSproc("spPersonDelete", new object[] { iID1, iID2, LMP.Data.Application.GetCurrentEditTime() });
                    if (iRecsAffected == 0)
                    {
                        //not deleted - alert
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
                    }
                }
                //delete keysets
                SimpleDataCollection.ExecuteActionSproc("spKeySetsDeleteByEntityIDs", oIDs);

                //delete permissions
                SimpleDataCollection.ExecuteActionSproc("spPermissionsDeleteByPersonIDs", oIDs);

                //delete records with this person as OwnerID or LinkedPersonID if Admin Delete
                if (iID2 == 0)
                {
                    if (!LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8258
                    {
                        //GLOG 5271: spPeopleDeleteRelatedByID has been updated to not include related Office records
                        //since these should also be deleted by having the UsageState set to 0
                        SimpleDataCollection.ExecuteActionSproc(
                            "spPeopleDeleteRelatedByID", new object[] { iID1 });
                    }
                    //GLOG 4546: Delete Group Assignments for this ID
                    SimpleDataCollection.ExecuteActionSproc(
                        "spPeopleGroupAssignmentsDeleteByPersonID", new object[] { iID1 });
                }
            }

			//delete item from array if necessary
			if (m_oArray != null)
			{
                //remove aliases from array
				//get the index to delete
				int iIndex = GetItemIndex(xID);

				Trace.WriteNameValuePairs("iIndex", iIndex);

				//delete if found in array
				if(iIndex > -1)
					m_oArray.RemoveAt(iIndex);
			}

            if (bUpdateFavorite)
            {
                //Deleted from User Preferences
                //also delete corresponding record from network FavoritePeople table
                if (bIsBasePerson) //GLOG 5721: Only Base Person ID will be Favorite
                    DeleteFavoritePerson(Application.User.ID, iID1);
            }
            else
            {
                //Deleted from Administrator.exe
                //log deletion
                SimpleDataCollection.LogDeletions(mpObjectTypes.Person, xID);

                //log deletion
                SimpleDataCollection.LogDeletions(mpObjectTypes.KeySets, xID);
            }
		}

        /// <summary>
        /// write deletion to the deletions log 
        /// </summary>
        private void LogDeletions(string xObjectID)
        {
            //GLOG 8220: Don't Log Deletions in Local Mode
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                return;
            int iObjectTypeID = (int)mpObjectTypes.People;

            Trace.WriteParameters("iObjectTypeID", iObjectTypeID, "xObjectID", xObjectID);

            object[] oParams = { iObjectTypeID, xObjectID, Application.GetCurrentEditTime() };

            //execute delete
            int iRecsAffected = SimpleDataCollection.ExecuteActionSproc("spSyncDeletions", oParams);

            if (iRecsAffected == 0)
            {
                //not deleted - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xObjectID);
            }
        }

		/// <summary>
		/// returns the collection as an array - array contains all fields
		/// </summary>
		/// <returns></returns>
		public override Array ToArray()
		{
			DateTime t0 = DateTime.Now;

			if (m_oArray == null)
				FillArray();

			//copy internal array to new array and return -
			//we do this so that the internal array can't be
			//modified by a client object
			Array oArray = new object[m_oArray.Count];
			m_oArray.CopyTo(0, oArray, 0, m_oArray.Count);

			LMP.Benchmarks.Print(t0);
			return oArray;
		}

		/// <summary>
		/// returns the collection as an array - array contains only the specified fields
		/// </summary>
		/// <param name="aFieldIndexes"></param>
		/// <returns></returns>
		public override Array ToArray(params int[] aFieldIndexes)
		{
			//get reader with all fields
            using (OleDbDataReader oReader = GetPeople(false))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader,
                        aFieldIndexes);
                    return oArray.ToArray();
                }
                finally
                {
                    oReader.Close();
                }
            }
		}

		/// <summary>
		/// returns 2-column array with Display Name, and ID
		/// </summary>
		/// <returns></returns>
		public virtual ArrayList ToDisplayArray()
		{
			DateTime t0 = DateTime.Now;

			//get reader with display fields only
            using (OleDbDataReader oReader = GetPeople(true))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                    LMP.Benchmarks.Print(t0);
                    return oArray;
                }
                finally
                {
                    oReader.Close();
                }
            }
		}

		/// <summary>
		/// returns 2-column array with Display Name and ID
		/// </summary>
		/// <returns></returns>
		public virtual string ToXML(bool bDisplayFieldsOnly)
		{
			DateTime t0 = DateTime.Now;

			//get reader with display fields only
            using (OleDbDataReader oReader = GetPeople(bDisplayFieldsOnly))
            {
                try
                {
                    //fill array
                    string xXML = SimpleDataCollection.DataReaderToXML(oReader);
                    LMP.Benchmarks.Print(t0);
                    return xXML;
                }
                finally
                {
                    oReader.Close();
                }
            }
		}

        /// <summary>
        /// returns this collection as a DataSet
        /// </summary>
        /// <returns></returns>
        public override DataSet ToDataSet(bool bDisplayFieldsOnly)
        {
            DateTime t0 = DateTime.Now;
            DataSet oDS = SimpleDataCollection.GetDataSet(GetSQL(bDisplayFieldsOnly, false));
            LMP.Benchmarks.Print(t0);
            return oDS;
        }

        /// <summary>
        /// returns the display fields of this collection -
        /// includes the office name as part of the display name, if specified
        /// </summary>
        /// <returns></returns>
        public override DataSet ToDisplayFieldsDataSet(bool bIncludeOfficeName)
        {
            DateTime t0 = DateTime.Now;
            DataSet oDS = SimpleDataCollection.GetDataSet(GetSQL(true, bIncludeOfficeName));
            LMP.Benchmarks.Print(t0);
            return oDS;
        }

        // GLOG : 6027 : CEH
        /// <summary>
        /// returns the display name, ID & Login ID
        /// </summary>
        /// <returns></returns>
        public DataSet ToPeopleManagerDataSet()
        {
            DateTime t0 = DateTime.Now;
            DataSet oDS = SimpleDataCollection.GetDataSet(GetSQL(true, false,true));
            LMP.Benchmarks.Print(t0);
            return oDS;
        }

		/// <summary>
		/// returns true if collection contains person with xID
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public bool IsInCollection(string xID)
		{
			Trace.WriteNameValuePairs("xID", xID);
			return (this.GetItemIndex(xID) != -1);
		}

		/// <summary>
		/// returns true if collection contains person with iID
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		public bool IsInCollection(int iID)
		{
			return IsInCollection(GetFormattedID(iID));
		}
		#endregion
		#region ***********************************static members***********************************

		/// <summary>
		/// returns true iff the specified IDs are the IDs of a MacPac person
		/// </summary>
		/// <param name="iID1"></param>
		/// <param name="iID2"></param>
		/// <returns></returns>
		public static bool Exists(int iID1, int iID2)
		{
			//return true if ID is the MacPac Guest Person ID
            if (iID1 == ForteConstants.mpGuestPersonID)
				return true;

			//if ID2 is 0, use the public person ID2
			if(iID2 == 0)
				iID2 = ForteConstants.mpPublicPersonID2;

			//build sql to count number of records with specified IDs
			string xSQL = string.Concat("SELECT COUNT(*) FROM People p WHERE p.ID1=",
				iID1, " AND p.ID2=", iID2, " AND p.UsageState = 1");

			Trace.WriteNameValuePairs("xSQL", xSQL);

			//get number of records
			int iCount = (int) SimpleDataCollection.GetScalar(xSQL); //GLOG 8258
	
			//return true iff there are records
			return iCount > 0;
		}
        
		/// <summary>
		/// returns true iff the specified ID is the ID of a MacPac public person
		/// </summary>
		/// <param name="iID1"></param>
		/// <returns></returns>
		public static bool Exists(int iID1)
		{
			return Exists(iID1, ForteConstants.mpPublicPersonID2);
		}

		/// <summary>
		/// returns true iff the specified ID is the ID of a MacPac person
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public static bool Exists(string xID)
		{
			int iID1;
			int iID2;

			SplitID(xID, out iID1, out iID2);

			return Exists(iID1, iID2);
		}

		/// <summary>
		/// returns people from delimited string of ids
		/// </summary>
		/// <param name="xIDs"></param>
		/// <param name="cDelimiter"></param>
		/// <returns></returns>
		public static LMP.Data.LocalPersons GetPeopleFromIDString(string xIDs, char cDelimiter)
		{
			int iID1;
			int iID2;

			if (xIDs == "")
			{
				//raise exception if id string is empty
				throw new LMP.Exceptions.ArgumentException(Resources
					.GetLangString("Error_EmptyArgument") + "xIDs");
			}

			//split ids into an array
			string[] oIDs = xIDs.Split(new char[]{cDelimiter});

			//build where filter
			StringBuilder oSB = new StringBuilder();
			foreach (string xID in oIDs)
			{
				SplitID(xID, out iID1, out iID2);
				if (oSB.Length > 0)
					oSB.Append(" OR ");
				oSB.AppendFormat("(p.ID1={0} AND p.ID2={1})", iID1, iID2);
			}

			return new LocalPersons(oSB.ToString());
		}

		/// <summary>
		/// returns people from comma delimited string of ids
		/// </summary>
		/// <param name="xIDs"></param>
		/// <returns></returns>
		public static LMP.Data.LocalPersons GetPeopleFromIDString(string xIDs)
		{
			return GetPeopleFromIDString(xIDs, ',');
		}

		/// <summary>
		/// returns array containing ids of offices to which requested person belongs
		/// </summary>
		/// <param name="iUserID"></param>
		/// <returns></returns>
		public static object[] GetOfficeIDs(int iUserID)
		{
			System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
				"spOfficeIDsByPersonID", new object[] {iUserID});
			return oArrayList.ToArray();
		}

		/// <summary>
		/// returns array containing ids of groups to which requested person belongs
		/// </summary>
		/// <param name="iUserID"></param>
		/// <returns></returns>
		public static object[] GetPeopleGroupIDs(int iUserID)
		{
			System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
				"spPeopleGroupIDsByPersonID", new object[] {iUserID});
			return oArrayList.ToArray();
		}

		/// <summary>
		/// returns array containing ids of folders belonging to specified person
		/// in specified parent folder
		/// </summary>
		/// <param name="iUserID"></param>
		/// <param name="iParentFolder"></param>
		/// <returns></returns>
		public static object[] GetUserFolderIDs(int iUserID, int iParentFolderID1,
			int iParentFolderID2)
		{
			Trace.WriteNameValuePairs("iUserID", iUserID, "iParentFolderID1",
				iParentFolderID1, "iParentFolderID2", iParentFolderID2);

			System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
				"spUserFolderIDs", new object[] {iUserID, iParentFolderID1, iParentFolderID2});
			return oArrayList.ToArray();
		}

		/// <summary>
		/// returns person with supplied ids
		/// </summary>
		/// <param name="iID1"></param>
		/// <param name="iID2"></param>
		/// <returns></returns>
		public static LMP.Data.Person GetPersonFromIDs(int iID1, int iID2)
		{
			 return GetPersonFromID(string.Concat(iID1, ".", iID2));
		}

		/// <summary>
		/// returns person with supplied id
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public static LMP.Data.Person GetPersonFromID(string xID)
		{
			Person oPerson = null;
            LocalPersons oPersons = null;
            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                oPersons = new LocalPersons(mpPeopleListTypes.LocalPublicPeople);
            }
            else
            {
                oPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
            }
			try
			{
				//attempt to get person - return null if xID is not a valid ID
				oPerson = oPersons.ItemFromID(xID);
			}
			catch{}

			return oPerson;
		}

        /// <summary>
        /// returns the single MacPac guest person using
        /// the system user id of the current OS user
        /// </summary>
        /// <returns></returns>
        public static Person CreateGuestPerson()
        {
            if (m_oGuestPerson == null)
                return CreateGuestPerson(Environment.UserName);
            else
                return m_oGuestPerson;
        }

		/// <summary>
		/// returns the single MacPac guest person
		/// </summary>
		/// <returns></returns>
		public static Person CreateGuestPerson(string xSystemUserID)
		{
			if(m_oGuestPerson == null)
			{
                //guest person has not yet been created - create
				LocalPersons oPersons = new LocalPersons();

                //GLOG : 7998 : always create transient record
                //GLOG 8021: reversed GLOG 7998 changes to this routine - stored procedures depend on the
                //guest record being in the db, and I can't reproduce the original error that prompted
                //these changes - it's true, that we're always creating a transient user in any case,
                //but that doesn't seem to be causing any problems
                try
                {
                    m_oGuestPerson = oPersons.ItemFromID(ForteConstants.mpGuestPersonID + ".0");
                }
                catch
                {
                    m_oGuestPerson = oPersons.Create();
                    m_oGuestPerson.SetID(ForteConstants.mpGuestPersonID.ToString() + ".0");
                    m_oGuestPerson.DisplayName = "Guest";
                    m_oGuestPerson.FullName = "Guest";

                    //GLOG : 7119 : jsw
                    //Assign 1st office in offices to Guest
                    //JTS 2/21/14: .GetItemIndex(0) actually looks for an record with 0 as the ID, so will not match and return -1
                    m_oGuestPerson.OfficeID = new Offices().ItemFromIndex(1).ID;
                    m_oGuestPerson.SystemUserID = xSystemUserID;
                    m_oGuestPerson.IsAuthor = true;
                    m_oGuestPerson.IsAttorney = true;
                    m_oGuestPerson.UsageState = 1;
                    m_oGuestPerson.OfficeUsageState = 1;

                    //GLOG 7551: Only save in Toolkit mode, otherwise this
                    //should be a transient ID for the current session only
                    //GLOG 8021: restore save and do for administrator as well
                    if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                        oPersons.Save(m_oGuestPerson);
                }
			}
			return m_oGuestPerson;
		}

        public static void ReloadCustomFields()
        {
            GetFields();
            Person.m_oPropertiesBaseHashtable = null;
        }
		#endregion
		#region ***********************************IEnumerable Members***********************************
		public IEnumerator GetEnumerator()
		{
			return this.GetEnumerator();
		}
		#endregion
	}
    /// <summary>
    /// contains the methods and properties that manage a
    /// collection of MacPac network persons
    /// </summary>
    public class NetworkPersons : Persons, IDisposable
    {
        #region ***********************************fields***********************************
        private string m_xDBServer;
        private string m_xDBName;
        private NetworkConnection m_oCn;
        private bool m_bDisposed;
        public event BeforeLostEditsHandler BeforeLostEdits;
        #endregion
        #region ***********************************constructors***********************************
        static NetworkPersons()
        {
            GetFields();
        }
        public NetworkPersons()
        {
            if (LMP.Data.Application.AdminMode)
            {
                throw new LMP.Exceptions.NotImplementedException(
                    LMP.Resources.GetLangString("Error_MethodNotUsableInAdminMode"));
            }

            FirmApplicationSettings oSettings = LMP.Data.Application.User.FirmSettings;
            this.m_xDBServer = oSettings.NetworkDatabaseServer;
            this.m_xDBName = oSettings.NetworkDatabaseName;
            m_oCn = new NetworkConnection(this.m_xDBServer, this.m_xDBName);
            m_oCn.ConnectIfNecessary();
        }
        public NetworkPersons(string xDatabaseServer, string xDatabaseName)
        {
            //GLOG 4380: Default to Firm Settings if arguments are empty
            FirmApplicationSettings oSettings = LMP.Data.Application.User.FirmSettings;
            if (string.IsNullOrEmpty(xDatabaseServer))
                xDatabaseServer = oSettings.NetworkDatabaseServer;
            if (string.IsNullOrEmpty(xDatabaseName))
                xDatabaseName = oSettings.NetworkDatabaseName;
            this.m_xDBServer = xDatabaseServer;
            this.m_xDBName = xDatabaseName;
            m_oCn = new NetworkConnection(xDatabaseServer, xDatabaseName);
            m_oCn.ConnectIfNecessary();
        }
        public NetworkPersons(string xDatabaseServer, string xDatabaseName, 
            LMP.Data.mpPeopleListTypes iListType, string xWhere)
            :this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
				LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName, 
            LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney, LMP.mpTriState iIsAuthor, int iOfficeID, string xWhere)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, iIsAttorney, iIsAuthor, iOfficeID, xWhere, UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName, 
            LMP.Data.mpPeopleListTypes iListType, int iUserID, LMP.mpTriState
            iIsAttorney): this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, iIsAttorney, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName,
            LMP.Data.mpPeopleListTypes iListType, LMP.mpTriState iIsAttorney)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, 0, iIsAttorney, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName, 
            LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType,
            int iUserID)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined, iIsAuthor, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName,
            LMP.mpTriState iIsAuthor, LMP.Data.mpPeopleListTypes iListType)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, iIsAuthor, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName,
            int iOfficeID, LMP.Data.mpPeopleListTypes iListType, int iUserID)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName,
            int iOfficeID, LMP.Data.mpPeopleListTypes iListType)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, iOfficeID, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName,
            LMP.Data.mpPeopleListTypes iListType, int iUserID, string xWhere)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
		}
        public NetworkPersons(string xDatabaseServer, string xDatabaseName, string xWhere)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, 0, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, xWhere, UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName, 
            LMP.Data.mpPeopleListTypes iListType, int iUserID)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, string xDatabaseName, int iUserID)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(LMP.Data.mpPeopleListTypes.AllPeople, iUserID, LMP.mpTriState.Undefined,
                LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
		}

        public NetworkPersons(string xDatabaseServer, 
            string xDatabaseName, LMP.Data.mpPeopleListTypes iListType)
            : this(xDatabaseServer, xDatabaseName)
		{
            SetFilters(iListType, 0, LMP.mpTriState.Undefined, LMP.mpTriState.Undefined, 0, "", UsageStates.Active);
		}
        #endregion
        #region ***********************************properties***********************************
        /// <summary>
		/// returns the number of items in the collection
		/// </summary>
        public override int Count
        {
            get
            {
                //we may already have count
                if (m_iCount != -1)
                    return m_iCount;

                //we may already have array - use it
                if (m_oArray != null)
                    return m_oArray.Count;

                //build SQL statement
                StringBuilder oSB = new StringBuilder("SELECT Count(*) FROM People as p");

                //add Where clause
                string xWhere = GetWhereClause();
                if (xWhere != "")
                    oSB.AppendFormat(" WHERE ({0})", xWhere);

                string xSQL = oSB.ToString();
                OleDbCommand oCmd = new System.Data.OleDb.OleDbCommand();

                using (oCmd)
                {
                    try
                    {
                        //setup command
                        oCmd.Connection = m_oCn.ConnectionObject;
                        oCmd.CommandText = xSQL;
                        oCmd.CommandType = CommandType.Text;

                        //execute and return
                        m_iCount = (int)oCmd.ExecuteScalar();
                        return m_iCount;
                    }

                    catch (System.Exception e)
                    {
                        throw new LMP.Exceptions.SQLStatementException(
                            Resources.GetLangString("Error_SQLStatementFailed") +
                            xSQL, e);
                    }
                }
            }
        }
        #endregion
        #region ***********************************methods***********************************
        ~NetworkPersons()
        {
            if (!m_bDisposed)
                DisposeObjects();
        }
        private void DisposeObjects()
        {
            if (m_oCn != null)
            {
                m_oCn.Dispose();
                m_bDisposed = true;
            }
        }
		/// <summary>
		/// fills the internal array containing all persons and fields in the collection
		/// </summary>
		private void FillArray()
		{
			//get reader with all fields
            using (OleDbDataReader oReader = GetPeople(false))
            {
                try
                {
                    //fill array
                    m_oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                }
                finally
                {
                    oReader.Close();
                }
            }
		}
		/// <summary>
		/// returns a data reader containing people as per applied filters -
		/// if bDisplayFieldsOnly=true, returns only Display Name and ID
		/// fields; otherwise, returns all fields
		/// </summary>
		/// <param name="bDisplayFieldsOnly"></param>
		/// <returns></returns>
		private OleDbDataReader GetPeople(bool bDisplayFieldsOnly)
		{
			string xSQL = GetSQL(bDisplayFieldsOnly);

			OleDbDataReader oReader = SimpleDataCollection
				.GetResultSet(m_oCn.ConnectionObject, xSQL, false);
			return oReader;
		}
        /// <summary>
        /// returns the person at the specified collection index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public override Person ItemFromIndex(int iIndex)
        {
            Trace.WriteNameValuePairs("iIndex", iIndex);

            //ensure that index is not less than 1
            if (iIndex < 1)
                throw new LMP.Exceptions.NotInCollectionException(
                    string.Concat(LMP.Resources.GetLangString(
                    "Error_ItemNotInCollection"), " (", iIndex, ")"));

            //fill internal array if necessary
            if (m_oArray == null)
                FillArray();

            //ensure that index is not beyond upper bound
            if (iIndex > m_oArray.Count)
                throw new LMP.Exceptions.NotInCollectionException(
                    string.Concat(LMP.Resources.GetLangString(
                    "Error_ItemNotInCollection"), " (", iIndex, ")"));

            object[] oValues = (object[])m_oArray[iIndex - 1];

            //save current item if dirty and there are listeners requesting this
            if ((m_oPerson != null) && (m_oPerson.IsDirty))
                SaveCurrentItemIfRequested();

            UpdateObject(oValues);
            return m_oPerson;
        }
        /// <summary>
        /// saves the current item dirty and there
        /// are listeners requesting this
        /// </summary>
        private void SaveCurrentItemIfRequested()
        {
            //check for listeners
            if (this.BeforeLostEdits != null)
            {
                //there are listeners - cycle through each
                //invoking their handlers
                foreach (BeforeLostEditsHandler oHandler in
                    this.BeforeLostEdits.GetInvocationList())
                {
                    BeforeLostEditsEventArgs e = new BeforeLostEditsEventArgs();

                    //invoke event handler
                    oHandler(this, e);

                    //handler responded that current item should be saved - save
                    if (e.SaveCurrentItem)
                    {
                        Save(m_oPerson);
                        return;
                    }
                }
            }
        }
		/// <summary>
		/// saves the specified Person
		/// </summary>
		/// <param name="oPerson"></param>
		public override void Save(LMP.Data.Person oPerson)
		{
			Trace.WriteNameValuePairs("oPerson.IsDirty", oPerson.IsDirty);

			if(oPerson.IsDirty)
			{
				Trace.WriteNameValuePairs("oPerson.ID", oPerson.ID);

				if (oPerson.IsPersisted)
				{
					//update people table
					UpdateRecord(oPerson);

					if(m_oArray != null)
					{
						//save changes to array
						UpdateArrayElement(oPerson);
					}
				}
				else
				{
					//add record to people table
					AddRecord(oPerson);

					if(m_oArray != null)
					{
						//force array to repopulate upon next request
						m_oArray = null;
					}
				}
				
				oPerson.IsPersisted = true;
				oPerson.IsDirty = false;
			}
		}
		/// <summary>
		/// saves current Person
		/// </summary>
		public override void Save()
		{
			Save(m_oPerson);
		}
        /// <summary>
        /// returns the collection index of the specified item
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public override int GetItemIndex(string xID)
        {
            int iID1;
            int iID2;

            //fill internal array if necessary
            if (m_oArray == null)
                FillArray();

            SplitID(xID, out iID1, out iID2);

            object[] oArray = m_oArray.ToArray();

            //cycle through array items, 
            //looking for match for both ID1 and ID2
            for (int i = 0; i < m_oArray.Count; i++)
            {
                object[] oValues = (object[])oArray[i];
                if (((int)oValues[0] == iID1) && ((int)oValues[1] == iID2))
                {
                    return i;
                }
            }

            return -1;
        }
        /// <summary>
        /// returns this collection as a DataSet
        /// </summary>
        /// <returns></returns>
        public override System.Data.DataSet ToDataSet(bool bDisplayFieldsOnly)
        {
            return SimpleDataCollection.GetDataSet(m_oCn.ConnectionObject, GetSQL(bDisplayFieldsOnly));
        }

        /// <summary>
        /// returns the display fields of this collection -
        /// includes the office name as part of the display name, if specified
        /// </summary>
        /// <returns></returns>
        public override DataSet ToDisplayFieldsDataSet(bool bIncludeOfficeName)
        {
            DateTime t0 = DateTime.Now;
            DataSet oDS = SimpleDataCollection.GetDataSet(GetSQL(true, bIncludeOfficeName));
            LMP.Benchmarks.Print(t0);
            return oDS;
        }
        /// <summary>
        /// returns the collection as an array - array contains all fields
        /// </summary>
        /// <returns></returns>
        public override Array ToArray()
        {
            DateTime t0 = DateTime.Now;

            if (m_oArray == null)
                FillArray();

            //copy internal array to new array and return -
            //we do this so that the internal array can't be
            //modified by a client object
            Array oArray = new object[m_oArray.Count];
            m_oArray.CopyTo(0, oArray, 0, m_oArray.Count);

            LMP.Benchmarks.Print(t0);
            return oArray;
        }
        /// <summary>
        /// returns the collection as an array - array contains only the specified fields
        /// </summary>
        /// <param name="aFieldIndexes"></param>
        /// <returns></returns>
        public override Array ToArray(params int[] aFieldIndexes)
        {
            //get reader with all fields
            using (OleDbDataReader oReader = GetPeople(false))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader,
                        aFieldIndexes);
                    return oArray.ToArray();
                }
                finally
                {
                    oReader.Close();
                }
            }
        }

        //GLOG - 3691 - ceh

        /// <summary>
        /// returns a network person's attorney licenses
        /// </summary>
        /// <returns></returns>
        public ArrayList GetLicenses(int iOwnerID)
        {
            OleDbDataReader oReader = null;

            DateTime t0 = DateTime.Now;

            string xSQL = "SELECT * FROM AttyLicenses WHERE OwnerID1 =" + iOwnerID;

            try
            {
                //get licenses for Network person
                oReader = SimpleDataCollection.GetResultSet(m_oCn.ConnectionObject,xSQL, false);

                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }

        /// <summary>
        /// returns a network person's keyset records
        /// </summary>
        /// <returns></returns>
        public ArrayList GetKeysets(int iOwnerID)
        {
            //GLOG 4649
            OleDbDataReader oReader = null;

            DateTime t0 = DateTime.Now;

            string xSQL = "SELECT * FROM Keysets WHERE OwnerID1="+ iOwnerID + " AND OwnerID2=0";

            try
            {
                //get Keysets for Network person
                oReader = SimpleDataCollection.GetResultSet(m_oCn.ConnectionObject, xSQL, false);

                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }
        /// <summary>
        /// updates an existing record in people table
        /// </summary>
        /// <param name="oPerson"></param>
        private void UpdateRecord(LMP.Data.Person oPerson)
        {
            int iID1;
            int iID2;

            SplitID(oPerson.ID, out iID1, out iID2);

            //construct update query
            StringBuilder oSB = new StringBuilder("UPDATE People AS p SET ");
            oSB.AppendFormat(@"p.OwnerID = {0}, p.UsageState = {1}, p.UserID = ""{2}"", ",
                oPerson.OwnerID, oPerson.UsageState, oPerson.SystemUserID);
            oSB.AppendFormat(@"p.DisplayName = ""{0}"", p.LinkedPersonID = {1}, p.Prefix = ""{2}"", ",
                oPerson.DisplayName, oPerson.LinkedPersonID, oPerson.NamePrefix);
            oSB.AppendFormat(@"p.LastName = ""{0}"", p.FirstName = ""{1}"", p.MI = ""{2}"", ",
                oPerson.LastName, oPerson.FirstName, oPerson.MiddleName);
            oSB.AppendFormat(@"p.Suffix = ""{0}"", p.FullName = ""{1}"", p.OfficeID = {2}, ",
                oPerson.NameSuffix, oPerson.FullName, oPerson.OfficeID);
            oSB.AppendFormat("p.IsAttorney = {0}, p.IsAuthor = {1}, ",
                System.Convert.ToInt32(oPerson.IsAttorney),
                System.Convert.ToInt32(oPerson.IsAuthor));

            //custom fields
            ICollection oKeys = oPerson.Properties.Keys;
            foreach (string oKey in oKeys)
            {
                //get value
                object oValue = oPerson.GetPropertyValue(oKey);
                //get data type
                string xType = oValue.GetType().ToString();
                //if value is still null, omit from query
                if (xType != "System.DBNull")
                {
                    //custom field name might contain space
                    //bracket field name so query will run
                    //string xKey = oKey;
                    //if (xKey.Contains(" "))
                    //    xKey = "[" + xKey + "]";

                    oSB.AppendFormat(@"p.[{0}] = ", oKey);
                    switch (xType)
                    {
                        case "System.Int32":
                        case "System.Boolean":
                            oSB.AppendFormat("{0}, ", oValue);
                            break;
                        default:
                            oSB.AppendFormat(@"""{0}"", ", oValue);
                            break;
                    }
                }
            }

            //GLOG 6673:  Dates in SQL Strings are assumed to be US Format
            oSB.AppendFormat("p.LastEditTime = #{0}# ", Application.GetCurrentEditTime(true));
            oSB.AppendFormat("WHERE (((p.ID1)={0}) AND ((p.ID2)={1}));", iID1, iID2);

            //execute query
            string xSQL = oSB.ToString();
            SimpleDataCollection.ExecuteAction(xSQL);
        }
        /// <summary>
        /// adds a new record to people table
        /// </summary>
        /// <param name="oPerson"></param>
        private void AddRecord(LMP.Data.Person oPerson)
        {
            int iID1;
            int iID2;

            //generate id
            if (oPerson.OwnerID != 0)
                //record belongs to user - id1 is user's id1 and
                //id2 is the current time
                Application.GetNewUserDataItemID(out iID1, out iID2);
            else if (!System.String.IsNullOrEmpty(oPerson.ID))
            {
                LMP.String.SplitID(oPerson.ID, out iID1, out iID2);
                if (iID2 == ForteConstants.mpFirmRecordID)
                    iID2 = ForteConstants.mpPublicPersonID2;
            }
            else
            {
                //record belongs to firm - get next id
                iID1 = (int)SimpleDataCollection.GetScalar(
                    "spPeopleLastID", null) + 1;
                //id2 is mpPublicPersonID2
                iID2 = ForteConstants.mpPublicPersonID2;
            }

            //construct query
            StringBuilder oSB1 = new StringBuilder("INSERT INTO People ( ID1, ID2, OwnerID, ");
            oSB1.Append("UsageState, UserID, DisplayName, LinkedPersonID, Prefix, LastName, ");
            oSB1.Append("FirstName, MI, Suffix, FullName, OfficeID, ");
            oSB1.Append("IsAttorney, IsAuthor, LastEditTime");

            StringBuilder oSB2 = new StringBuilder(@"SELECT ");
            oSB2.AppendFormat(@"{0}, {1}, {2}, {3}, ""{4}"", ""{5}"", ", iID1, iID2,
                oPerson.OwnerID, oPerson.UsageState, oPerson.SystemUserID, oPerson.DisplayName);
            oSB2.AppendFormat(@"{0}, ""{1}"", ""{2}"", ""{3}"", ""{4}"", ""{5}"", ",
                oPerson.LinkedPersonID, oPerson.NamePrefix, oPerson.LastName, oPerson.FirstName,
                oPerson.MiddleName, oPerson.NameSuffix);
            oSB2.AppendFormat(@"""{0}"", {1}, {2}, {3}, #{4}#", oPerson.FullName,
                oPerson.OfficeID, System.Convert.ToInt32(oPerson.IsAttorney),
                System.Convert.ToInt32(oPerson.IsAuthor), Application.GetCurrentEditTime(true)); //GLOG 6673: Use US Format Date for SQL String

            //custom fields
            ICollection oKeys = oPerson.Properties.Keys;
            foreach (string oKey in oKeys)
            {
                //get value
                object oValue = oPerson.GetPropertyValue(oKey);
                //get data type
                string xType = oValue.GetType().ToString();
                //if value of property hasn't been set, omit from query
                if (xType != "System.Object")
                {
                    oSB1.AppendFormat(@", [{0}]", oKey);
                    switch (xType)
                    {
                        case "System.Int32":
                        case "System.Boolean":
                            oSB2.AppendFormat(", {0}", oValue);
                            break;
                        default:
                            oSB2.AppendFormat(@", ""{0}""", oValue);
                            break;
                    }
                }
            }

            //execute query
            string xSQL = System.String.Concat(oSB1.ToString(), ") ", oSB2.ToString(), ";");
            SimpleDataCollection.ExecuteAction(m_oCn.ConnectionObject, xSQL);
            oPerson.SetID(System.String.Concat(iID1.ToString(), ".", iID2.ToString()));
        }
        /// <summary>
        /// returns the person with the specified ID, from the local or network People table
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="bUseNetworkConnection"></param>
        /// <returns></returns>
        public override Person ItemFromID(string xID)
        {
            Trace.WriteNameValuePairs("xID", xID);

            //requested item may already be the current one
            if ((m_oPerson != null) && (xID == m_oPerson.ID))
                return m_oPerson;

            //save current item if dirty and there are listeners requesting this
            if ((m_oPerson != null) && (m_oPerson.IsDirty))
                SaveCurrentItemIfRequested();

            //set parameters
            int iID1;
            int iID2;
            object[] oParams;

            SplitID(xID, out iID1, out iID2);

            oParams = new object[] { iID1, iID2 };

            StringBuilder oSB = new StringBuilder();

            for(int i=0; i<=m_oFields.GetUpperBound(0); i++)
                oSB.AppendFormat("[p].[{0}],", m_oFields[i,0]);

            string xFields = oSB.ToString();
            xFields = xFields.TrimEnd(',');

            string xSQL = "SELECT " + xFields + " FROM People AS p WHERE p.ID1=" + iID1 + 
                " And (p.ID2=" + iID2 + " Or (" + iID2 + " Is Null And p.ID2=0))";

            //GLOG : 6482 : ceh - apply UsageStateFilter
            if (m_iUsageStateFilter == UsageStates.Active)
                xSQL = xSQL + " And p.UsageState=1";
           
            //execute query
            using (OleDbDataReader oReader = SimpleDataCollection
                .GetResultSet(m_oCn.ConnectionObject, xSQL, true))
            {
                try
                {
                    if (oReader.HasRows)
                    {
                        //get row values
                        oReader.Read();
                        object[] oValues = new object[oReader.FieldCount];
                        oReader.GetValues(oValues);
                        UpdateObject(oValues);
                    }
                    else
                    {
                        //item was not found
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString("Error_ItemNotInCollection"));
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }

            return m_oPerson;
        }
        /// <summary>
        ///  returns the person with the specified ID
        /// </summary>
        /// <param name="iID"></param>
        /// <returns></returns>
        public Person ItemFromID(int iID)
        {
            return ItemFromID(iID.ToString() + ".0");
        }
        public override void Delete(string xID, bool bUpdateFavorite)
        {
            int iID1;
            int iID2;

            SplitID(xID, out iID1, out iID2);
            object[] oIDs = new object[] { iID1, iID2 };

            Trace.WriteNameValuePairs("iID1", iID1, "iID2", iID2);

            //execute query
            int iRecsAffected = SimpleDataCollection
                .ExecuteActionSproc(m_oCn.ConnectionObject,"spPersonDelete", oIDs);

            if (iRecsAffected == 0)
            {
                //not deleted - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
            }
            else
            {
                //delete item from array if necessary
                if (m_oArray != null)
                {
                    //get the index to delete
                    int iIndex = GetItemIndex(xID);

                    Trace.WriteNameValuePairs("iIndex", iIndex);

                    //delete if found in array
                    if (iIndex > -1)
                        m_oArray.RemoveAt(iIndex);
                }
            }

            //delete keysets
            SimpleDataCollection.ExecuteActionSproc(m_oCn.ConnectionObject, "spKeySetsDeleteByEntityIDs", oIDs);

            //delete permissions
            SimpleDataCollection.ExecuteActionSproc(m_oCn.ConnectionObject, "spPermissionsDeleteByPersonIDs", oIDs);
            //log deletion
            SimpleDataCollection.LogDeletions(mpObjectTypes.KeySets, xID);

            //delete records with this person as OwnerID or LinkedPersonID
            if (iID2 == ForteConstants.mpPublicPersonID2)
            {
                SimpleDataCollection.ExecuteActionSproc(m_oCn.ConnectionObject,
                    "spPeopleDeleteRelatedByID", new object[] { iID1 });
            }
            if (bUpdateFavorite)
            {
                //also delete corresponding record from network FavoritePeople table
                DeleteFavoritePerson(Application.User.ID, iID1);
            }
        }
        /// <summary>
        /// Remove record corresponding to specified Owner and Person from network FavoritePeople table
        /// </summary>
        /// <param name="iOwnerID"></param>
        /// <param name="iPersonID"></param>
        public void DeleteFavoritePerson(int iOwnerID, int iPersonID)
        {
            object[] oParams;
            oParams = new object[] { iOwnerID, 0, iPersonID, 0 };

            using (NetworkConnection oCn = new NetworkConnection())
            {
                try
                {
                    SimpleDataCollection.ExecuteActionSproc(m_oCn.ConnectionObject,
                        "spFavoritePeopleDelete", oParams);
                }
                catch { }
                finally
                {
                    oCn.Close();
                }
            }
        }
        /// <summary>
        /// returns the SQL statement used to retrieve people
        /// </summary>
        /// <param name="bDisplayFieldsOnly"></param>
        /// <returns></returns>
        private string GetSQL(bool bDisplayFieldsOnly)
        {
            return GetSQL(bDisplayFieldsOnly, false);
        }
        private string GetSQL(bool bDisplayFieldsOnly, bool bIncludeOfficeName)
        {
            //build SQL statement
            StringBuilder oSB = new StringBuilder("SELECT ");

            if (bDisplayFieldsOnly && bIncludeOfficeName)
                //get display name and ID - display name should include
                //office name if record is a secondary office
                //oSB.Append("p.DisplayName & iif(p.ID2=0 AND p.LinkedPersonID<>0, ' (' & o.DisplayName & ')', '') AS DisplayName, (p.ID1 & '.' & p.ID2) AS ID");
                oSB.Append("p.DisplayName + ' (' + o.DisplayName + ')' AS DisplayName, (cast(p.ID1 as varchar) + '.' + cast(p.ID2 as varchar)) AS ID");
            else if (bDisplayFieldsOnly)
            {
                oSB.Append("p.DisplayName AS DisplayName, (cast(p.ID1 as varchar) + '.' + cast(p.ID2 as varchar)) AS ID");

            }
            else
            {
                //get all fields in people table
                for (int i = 0; i <= m_oFields.GetUpperBound(0); i++)
                {
                    if (m_oFields[i, 0].ToUpper() == "LASTEDITTIME")
                        oSB.Append("p.LastEditTime as LastEditTime, ");
                    else
                    {
                        string xField = m_oFields[i, 0];

                        //custom property fieldnames might contain spaces or other characters
                        //that affect the sql. Use brackets to prevent such issues.
                        xField = "[" + xField + "]";

                        oSB.AppendFormat("p.{0}, ", xField);
                    }
                }
                oSB.Remove(oSB.Length - 2, 2);
            }

            oSB.Append(" FROM People as p INNER JOIN Offices o on p.OfficeID=o.ID");

            //add WHERE clause
            string xWhere = GetWhereClause();
            if (xWhere != "")
                oSB.AppendFormat(" WHERE ({0})", xWhere);

            //add ORDER BY clause
            oSB.Append(" ORDER BY ");
            if (xWhere.ToUpper().IndexOf("USERID=") != -1)
                oSB.Append("p.LinkedPersonID, ");
            //GLOG 4380: using p.LinkedPersonID=0 in the ORDER BY clause is not valid for SQL Server
            oSB.Append("p.DisplayName, p.LinkedPersonID, o.DisplayName;");

            return oSB.ToString();
        }
        #endregion
        #region IDisposable Members

        public void Dispose()
        {
            DisposeObjects();
            GC.SuppressFinalize(this);
        }
        #endregion
    }
    /// <summary>
    /// contains the methods and properties that manage a
    /// collection of MacPac public persons
    /// </summary>
    public class PublicPersons : IEnumerable
    {
        #region ***********************************fields***********************************
        protected static string[,] m_oFields;
        protected static OleDbConnection m_oCnn;
        #endregion
        #region ***********************************constructor***********************************
        static PublicPersons()
        {
            //GetFields();
        }

        #endregion
        #region ***********************************methods***********************************
        /// <summary>
        ///  returns the person with the specified ID
        /// </summary>
        /// <param name="iID"></param>
        /// <returns></returns>
        public Person ItemFromID(int iID1)
        {
            return ItemFromID(iID1.ToString());
        }
        /// <summary>
        ///  returns the person with the specified ID
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public Person ItemFromID(string xID1)
        {
            Person oPerson = null;

            Trace.WriteNameValuePairs("xID1", xID1);

            string xSQL = "SELECT * FROM PeoplePublic as p WHERE p.ID1=" + xID1 +
                " AND p.ID2=0;";
            //execute query
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, true))
            {
                try
                {
                    if (oReader.HasRows)
                    {
                        //get row values
                        oReader.Read();
                        object[] oValues = new object[oReader.FieldCount];
                        oReader.GetValues(oValues);
                        oPerson = CreatePersonFromValues(oValues);
                    }
                    else
                    {
                        //item was not found
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString("Error_ItemNotInCollection"));
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }

            return oPerson;
        }
        /// <summary>
        /// returns the person with the 
        /// specified system user name
        /// </summary>
        /// <param name="xSystemUserName"></param>
        /// <returns></returns>
        public static Person ItemFromSystemUserName(string xSystemUserName) //GLOG 8289
        {
            Person oPerson = null;

            Trace.WriteNameValuePairs("xSystemUserName", xSystemUserName);

            string xSQL = "SELECT * FROM PeoplePublic WHERE UserID Is Not Null  And UserID = '" +
                xSystemUserName + "' and LinkedPersonID = 0 And UsageState=1";

            //execute query
            bool bTryAgain = false;
            do
            {
                try
                {
                    using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, true))
                    {
                        try
                        {
                            if (oReader.HasRows)
                            {
                                //get row values
                                oReader.Read();
                                object[] oValues = new object[oReader.FieldCount];
                                oReader.GetValues(oValues);
                                oPerson = CreatePersonFromValues(oValues);
                            }
                            else
                            {
                                //item was not found
                                throw new LMP.Exceptions.NotInCollectionException(
                                    LMP.Resources.GetLangString("Error_ItemNotInCollection"));
                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                    bTryAgain = false;
                }
                catch
                {
                    //If PeoplePublic link was just created, query may not 
                    //work first time - try once more
                    if (!bTryAgain)
                        bTryAgain = true;
                    else
                        return null;
                }
            } while (bTryAgain);
            return oPerson;
        }

        /// <summary>
        /// returns this collection as a DataSet
        /// </summary>
        /// <returns></returns>
        public DataSet ToDataSet(bool bDisplayFieldsOnly)
        {
            DateTime t0 = DateTime.Now;
            string xSQL = null;

            if (bDisplayFieldsOnly)
                xSQL = "SELECT DisplayName, ID1 FROM PeoplePublic";
            else
                xSQL = "SELECT * FROM PeoplePublic";
            DataSet oDS = SimpleDataCollection.GetDataSet(xSQL);
            LMP.Benchmarks.Print(t0);
            return oDS;
        }
        protected static Person CreatePersonFromValues(object[] oValues) //GLOG 8289
        {
            int iID1 = 0;
            int iID2 = 0;
            int iOffset = 0;
            int iValuesIndex = 0;

            Trace.WriteValues(oValues);
            if (m_oFields == null)
                GetFields();

            //create new person, passing in array of field names
            Person oPerson = new Person(m_oFields);

            //cycle through values, updating properties - field names
            //array and values array are in the same order
            for (int i = 0; i <= m_oFields.GetUpperBound(0); i++)
            {
                iValuesIndex = i + iOffset;
                switch (m_oFields[i, 0].ToUpper())
                {
                    case "ID1":
                        iID1 = (int)oValues[iValuesIndex];
                        break;
                    case "ID2":
                        iID2 = (int)oValues[iValuesIndex];
                        break;
                    case "DISPLAYNAME":
                        oPerson.DisplayName = oValues[iValuesIndex].ToString();
                        break;
                    case "USERID":
                        oPerson.SystemUserID = oValues[iValuesIndex].ToString();
                        break;
                    case "LINKEDPERSONID":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            oPerson.LinkedPersonID = (int)oValues[iValuesIndex];
                        else
                            oPerson.LinkedPersonID = 0;
                        break;
                    case "FULLNAME":
                        oPerson.FullName = oValues[iValuesIndex].ToString();
                        break;
                    case "LASTNAME":
                        oPerson.LastName = oValues[iValuesIndex].ToString();
                        break;
                    case "FIRSTNAME":
                        oPerson.FirstName = oValues[iValuesIndex].ToString();
                        break;
                    case "MI":
                        oPerson.MiddleName = oValues[iValuesIndex].ToString();
                        break;
                    case "PREFIX":
                        oPerson.NamePrefix = oValues[iValuesIndex].ToString();
                        break;
                    case "SUFFIX":
                        oPerson.NameSuffix = oValues[iValuesIndex].ToString();
                        break;
                    case "OFFICEID":
                        oPerson.OfficeID = (int)oValues[iValuesIndex];
                        break;
                    case "DEFAULTOFFICERECORDID1":
                        if (oValues[iValuesIndex] != DBNull.Value)
                            oPerson.DefaultOfficeRecordID = (int)oValues[iValuesIndex];
                        break;
                    case "ISAUTHOR":
                        oPerson.IsAuthor = (bool)oValues[iValuesIndex];
                        break;
                    case "ISATTORNEY":
                        oPerson.IsAttorney = (bool)oValues[iValuesIndex];
                        break;
                    case "OWNERID":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            oPerson.OwnerID = (int)oValues[iValuesIndex];
                        else
                            oPerson.OwnerID = 0;
                        break;
                    case "USAGESTATE":
                        oPerson.UsageState = byte.Parse(oValues[iValuesIndex].ToString());
                        break;
                    case "OFFICEUSAGESTATE":
                        if (oValues[iValuesIndex] != System.DBNull.Value)
                            oPerson.OfficeUsageState = byte.Parse(oValues[iValuesIndex].ToString());
                        else
                            oPerson.OfficeUsageState = 1;
                        break;
                    case "LASTEDITTIME":
                        oPerson.LastEditTime = DateTime.Parse(oValues[iValuesIndex].ToString());
                        break;
                    case "LINKEDEXTERNALLY":
                        string xVal = null;
                        try
                        {
                            //ignore error here - we're not concerned with
                            //this value except when set by pup
                            xVal = oValues[iValuesIndex].ToString();
                        }
                        catch { }

                        if (!string.IsNullOrEmpty(xVal))
                        {
                            //GLOG 3768: Need property for this to enable copying from Network to Local list
                            try
                            {
                                //field is an int for a local person (that's how it's returned from Access)
                                oPerson.LinkedExternally = (bool)oValues[iValuesIndex];
                            }
                            catch
                            {
                                //field is a string for a network person (that's how it's returned from SQL Server)
                                oPerson.LinkedExternally = bool.Parse(oValues[iValuesIndex].ToString());
                            }

                        }
                        break;
                    default:
                        //custom property
                        if (iValuesIndex <= oValues.GetUpperBound(0))
                            oPerson.SetPropertyValue(m_oFields[i, 0], oValues[iValuesIndex]);
                        else
                            oPerson.SetPropertyValue(m_oFields[i, 0], null);
                        break;
                }
            }

            //fill ID property
            oPerson.SetID(iID1.ToString() + ".0");

            oPerson.IsDirty = false;
            oPerson.IsPersisted = true;

            return oPerson;
        }

        /// <summary>
        /// fills static array with names of all fields in People table
        /// </summary>
        protected static void GetFields()
        {
            OleDbDataReader oReader = null;
            OleDbCommand oCmd = new System.Data.OleDb.OleDbCommand();

            if (LocalConnection.ConnectionObject == null)
            {
                return;
            }
            using (oCmd)
            {
                try
                {
                    //setup command
                    oCmd.Connection = LocalConnection.ConnectionObject;
                    oCmd.CommandText = "SELECT * FROM PeoplePublic";
                    oCmd.CommandType = CommandType.Text;

                    //execute
                    oReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly);
                    DataTable oDT = oReader.GetSchemaTable();

                    //load array
                    int iFields = oReader.FieldCount;
                    m_oFields = new string[iFields, 2];
                    for (int i = 0; i < iFields; i++)
                    {
                        m_oFields[i, 0] = oDT.Rows[i]["ColumnName"].ToString();
                        m_oFields[i, 1] = oDT.Rows[i]["DataType"].ToString();
                    }
                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.SQLStatementException(
                        Resources.GetLangString("Error_SQLStatementFailed") +
                        oCmd.CommandText, e);
                }

                finally
                {
                    oReader.Close();
                    oReader.Dispose();
                }
            }
        }

        /// <summary>
        /// returns a network person's attorney licenses
        /// </summary>
        /// <returns></returns>
        public ArrayList GetLicenses(int iOwnerID)
        {
            OleDbDataReader oReader = null;

            DateTime t0 = DateTime.Now;

            string xSQL = "SELECT * FROM AttyLicensesPublic WHERE OwnerID1 =" + iOwnerID;

            try
            {
                //get licenses for Network person
                oReader = SimpleDataCollection.GetResultSet(LMP.Data.LocalConnection.ConnectionObject, xSQL, false);

                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }

        /// <summary>
        /// returns a network person's keyset records
        /// </summary>
        /// <returns></returns>
        public ArrayList GetKeysets(int iOwnerID)
        {
            //GLOG 4649
            OleDbDataReader oReader = null;

            DateTime t0 = DateTime.Now;

            string xSQL = "SELECT * FROM KeysetsPublic WHERE OwnerID1=" + iOwnerID + " AND OwnerID2=0";

            try
            {
                //get Keysets for public person
                oReader = SimpleDataCollection.GetResultSet(LMP.Data.LocalConnection.ConnectionObject, xSQL, false);

                //fill array
                ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                LMP.Benchmarks.Print(t0);
                return oArray;
            }
            finally
            {
                oReader.Close();
                oReader.Dispose();
            }
        }
        /// <summary>
        /// Returns List containing names of Custom Fields in Public People table
        /// </summary>
        /// <param name="oCnn"></param>
        /// <returns></returns>
        public static List<string> GetPublicPeopleCustomFieldList(OleDbConnection oCnn)
        {
            List<string> aCustomFields = new List<string>();

            OleDbCommand oCmd = new OleDbCommand();
            oCmd.Connection = oCnn;
            oCmd.CommandText = "SELECT TOP 1 * FROM PEOPLEPUBLIC;";

            using (OleDbDataReader oReader = oCmd.ExecuteReader(CommandBehavior.SchemaOnly))
            {
                try
                {
                    int iLocalFieldCount = oReader.FieldCount;

                    for (int i = 0; i + 24 < iLocalFieldCount; i++)
                    {
                        string xColumn = oReader.GetName(i + 24);
                        if (!ForteConstants.mpRequiredDBFields.Contains("|" + xColumn + "|"))
                            //this is a custom filed
                            aCustomFields.Add(xColumn);
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }
            return aCustomFields;
        }
        public static OleDbConnection PeopleDBConnection
        {
            get
            {
                if (m_oCnn == null)
                {
                    string xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                   LMP.Data.Application.PublicDataDirectory  + @"\" + LMP.Data.Application.PeopleDBName + ";Jet OLEDB:System database=" +
                    LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10ProgrammaticUser;Password=Fish4Mill";
                   m_oCnn = new OleDbConnection(xCnStr);
                }
                return m_oCnn;
            }

        }
        //GLOG 8258
        /// <summary>
        /// returns true iff the specified IDs are the IDs of a MacPac person
        /// </summary>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        /// <returns></returns>
        public static bool Exists(int iID1, int iID2)
        {
            //return true if ID is the MacPac Guest Person ID
            if (iID1 == ForteConstants.mpGuestPersonID)
                return true;

            //if ID2 is 0, use the public person ID2
            if (iID2 == 0)
                iID2 = ForteConstants.mpPublicPersonID2;

            //build sql to count number of records with specified IDs
            string xSQL = string.Concat("SELECT COUNT(*) FROM PeoplePublic p WHERE p.ID1=",
                iID1, " AND p.ID2=", iID2, " AND p.UsageState = 1");
                
            Trace.WriteNameValuePairs("xSQL", xSQL);

            //get number of records
            int iCount = (int)SimpleDataCollection.GetScalar(xSQL);

            //return true iff there are records
            return iCount > 0;
        }

        /// <summary>
        /// returns true iff the specified ID is the ID of a MacPac public person
        /// </summary>
        /// <param name="iID1"></param>
        /// <returns></returns>
        public static bool Exists(int iID1)
        {
            return Exists(iID1, ForteConstants.mpPublicPersonID2);
        }

        /// <summary>
        /// returns true iff the specified ID is the ID of a MacPac person
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public static bool Exists(string xID)
        {
            int iID1;
            int iID2;

            Persons.SplitID(xID, out iID1, out iID2);

            return Exists(iID1, iID2);
        }
        //GLOG 8289: Specify whether Admin access should be allowed when Admin Group is empty
        public static bool HasAdminAccess(string xSystemUserID, bool bAllowForEmptyGroup)
        {
            Trace.WriteNameValuePairs("xSystemUserID", xSystemUserID);
            Person oPerson = ItemFromSystemUserName(xSystemUserID);
            if (oPerson != null)
                return Person.HasAdminAccess(oPerson.ID1, 0, bAllowForEmptyGroup);
            else
                return false;
        }
        #endregion
        #region ***********************************IEnumerable Members***********************************
        public IEnumerator GetEnumerator()
        {
            return this.GetEnumerator();
        }
        #endregion
    }
}
