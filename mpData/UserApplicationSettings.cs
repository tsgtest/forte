using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace LMP.Data
{
    public enum TrailerInsertionOptions
    {
        Manual = 1,
        Silent = 2,
        Prompt = 3
    }

    //GLOG : 6262 : CEH
    public enum PageNumberScopes
    {
        Section = 0,
        Document = 1,
        Multiple = 2
    }

    public class UserApplicationSettings
    {
        #region****************************enumerations****************************
        [Flags]
        public enum FavoriteSegmentsInsertionLocations
        {
            InsertAtStartOfCurrentSection = 1,
            InsertAtEndOfCurrentSection = 2,
            InsertAtStartOfAllSections = 4,
            InsertAtEndOfAllSections = 8,
            InsertAtSelection = 16,
            InsertAtStartOfDocument = 32,
            InsertAtEndOfDocument = 64,
            InsertInNewDocument = 128,
            Default = 0
        }
        #endregion
        #region****************************constants****************************
        private const string PREFNAME_ROG_STYLES = "RogStyles";
        private const string PREFNAME_FIRST_PAGE_PRINT_TRAY = "Page1Tray";
        private const string PREFNAME_OTHER_PAGES_PRINT_TRAY = "OtherPagesTray";
        private const string PREFNAME_SHOW_XMLTAGS = "ShowXMLTags";
        private const string PREFNAME_USER_SEGMENTS_CONVERTED = "UserSegmentsConverted";
        private const string PREFNAME_UPDATE_ZOOM = "UpdateZoom";
        private const string PREFNAME_SHOW_HELP = "ShowQuickHelp";
        private const string PREFNAME_SHOW_TASKPANE_MPDOC = "ShowTP_MPDoc";
        private const string PREFNAME_SHOW_TASKPANE_NON_MPDOC = "ShowTP_NonMPDoc";
        private const string PREFNAME_HELP_WINDOW_HEIGHT = "HelpWindowHeight";
        private const string PREFNAME_SHOW_LOCATION_TOOLTIPS = "ShowLocationTooltips";
        private const string PREFNAME_TASK_PANE_BACKGROUND_COLOR = "TPBackColor";
        private const string PREFNAME_USE_GRADIENT = "UseGradient";
        private const string PREFNAME_FAVORITE_SEGMENTS = "FavoriteSegments";
        private const string PREFNAME_FIT_TO_PAGE_WIDTH = "FitToPageWidth";
        private const string PREFNAME_CI_DIALOG_LOCATION = "CIDialogLocation";
        private const string PREFNAME_DEFAULT_PREFILL_FOLDER = "DefaultPrefillFolder";
        private const string PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB = "ActivateMPRibbonTab";
        private const string PREFNAME_TASK_PANE_WIDTH = "TPWidth";
        private const string PREFNAME_DEFAULT_DESIGNER_FOLDER = "DefDesignerFolder";
        private const string PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB_ONOPEN = "ActivateMPRibbonTabOnOpen";
        private const string PREFNAME_SHOW_TASKPANE_ON_BLANK_NEW_DOC = "ShowTP_BlankNew";
        private const string PREFNAME_SHOW_TASKPANE_WHEN_NO_VARIABLES = "ShowTP_NoVariables";
        private const string PREFNAME_CLOSE_TASKPANE_ON_FINISH = "CloseTP_OnFinish";
        private const string PREFNAME_WARN_ON_FINISH = "Warn_OnFinish";
        private const string PREFNAME_AUTHOR_PREFS_WINDOW_SIZE = "AuthorPrefsWinSize";
        private const string PREFNAME_ALTERNATE_ROGS_Q_AND_R = "RogsAltQR";
        private const string PREFNAME_BATES_FIELD_VALUES = "BatesValues";
        //GLOG : 8308 : JSW
        private const string PREFNAME_EXPAND_FIND_RESULTS = "ExpandFindResults";

        //GLOG 8162
        private const string PREFNAME_SEARCH_OPTIONS_TEXT = "SearchOptionsText";
        private const string PREFNAME_SEARCH_OPTIONS_TYPE = "SearchOptionsType";
        private const string PREFNAME_SEARCH_OPTIONS_LOCATION = "SearchOptionsLocation";
        private const string PREFNAME_SEARCH_OPTIONS_OWNER = "SearchOptionsOwner";
        private const string PREFNAME_SEARCH_OPTIONS_FIELD = "SearchOptionsField";

        //GLOG : 3079 : CEH
        private const string PREFNAME_TRAILER_DISPLAY_OPTIONS = "TrailerDisplayOptions";

        //GLOG : 5759 : CEH
        private const string PREFNAME_DEFAULT_PAGE_ZOOM_PERCENTAGE = "DefaultPageZoomPercentage";


        //Pagenumber constants
        private const string PREFNAME_PAGE_NUMBER_ALIGNMENT = "PgNumAlign";
        private const string PREFNAME_PAGE_NUMBER_SCOPE = "PgNumScope";
        //GLOG : 6262 : CEH
        private const string PREFNAME_PAGE_NUMBER_REMOVE_SCOPE = "PgNumRemoveScope";
        private const string PREFNAME_PAGE_NUMBER_ON_FIRST_PAGE = "PgNumPg1";
        private const string PREFNAME_PAGE_NUMBER_LOCATION = "PgNumLoc";
        private const string PREFNAME_PAGE_NUMBER_FORMAT = "PgNumFormat";
        private const string PREFNAME_PAGE_NUMBER_TEXT_BEFORE = "PgNumPreText";
        private const string PREFNAME_PAGE_NUMBER_TEXT_AFTER = "PgNumPostText";
        private const string PREFNAME_SHOW_REVIEW_DATA_MSG = "ShowReviewDataMsg";
        private const string PREFNAME_PAGE_NUMBER_REMOVE_OPTION = "PgNumRemoveOption";
        private const string PREFNAME_PAGE_NUMBER_RESTART_NUMBERING = "PgNumRestartNumbering";
        private const string PREFNAME_PAGE_NUMBER_START_AT_NUMBER = "PgNumStartAtNumber";
        private const string PREFNAME_PAGE_NUMBER_TOTAL_FORMAT = "PgNumTotalFormat";	//GLOG : 6435 : ceh

        //GLOG : 7606 : CEH
        //Label/Envelope type
        private const string PREFNAME_LABEL_TYPE = "LabelType";
        private const string PREFNAME_ENVELOPE_TYPE = "EnvelopeType";

        //Page number default constants
        //Center
        private const int DEFAULT_PAGE_NUMBER_ALIGNMENT = 2;
        //Footer
        private const int DEFAULT_PAGE_NUMBER_LOCATION = 2;
        //Arabic
        private const int DEFAULT_PAGE_NUMBER_FORMAT = 1;

        private const int DEFAULT_PAGE_NUMBER_REMOVE_OPTION = 1;

        private const int DEFAULT_PAGE_NUMBER_START_AT_NUMBER = 1;

        private const int DEFAULT_HELP_WINDOW_HEIGHT = 87;

        //GLOG : 5759 : CEH
        private const int DEFAULT_PAGE_ZOOM_PERCENTAGE = 100;

        //5-13-11 (dm)
        private const string PREFNAME_WARN_BEFORE_DEACTIVATION = "WarnBeforeDeactivation";

        //GLOG 6266: Section Break macro
        private const string PREFNAME_SECTION_BREAK_LOCATION = "SecBrkLoc";
        private const string PREFNAME_SECTION_BREAK_RESTART_NUMBERING = "SecBrkRestart";
        private const string PREFNAME_SECTION_BREAK_HEADER1_OPTION = "SecBrkHeader1";
        private const string PREFNAME_SECTION_BREAK_HEADER2_OPTION = "SecBrkHeader2";
        private const string PREFNAME_SECTION_BREAK_PAGENUMBER1_OPTION = "SecBrkPgNum1";
        private const string PREFNAME_SECTION_BREAK_PAGENUMBER2_OPTION = "SecBrkPgNum2";


        #endregion
        #region****************************fields****************************
        private int m_iEntityID;
        //GLOG 4775: define [Char_XX] fieldcode replacements for reserved separator characters
        private const char FAV_SEGMENTS_DETAILS_SEP = ',';
        private const char FAV_SEGMENTS_INSTANCE_SEP = ';';
        private const string FAV_SEGMENTS_DETAILS_SEP_REPLACEMENT = "[Char_44]";
        private const string FAV_SEGMENTS_INSTANCE_SEP_REPLACEMENT = "[Char_59]";
        private static KeySet m_oCachedKeyset;
        #endregion
        #region****************************constructors****************************
        public UserApplicationSettings(int iEntityID)
        {
            m_iEntityID = iEntityID;
            m_oCachedKeyset = null; //GLOG 6731
        }
        //public UserApplicationSettings() { }
        #endregion
        #region****************************properties****************************
        /// <summary>
        public int FirstPagePrinterTray
        {
            get
            {
                string xTray = GetUserAppPref(PREFNAME_FIRST_PAGE_PRINT_TRAY);

                if (xTray == "")
                {
                    return 0;
                }
                else
                {
                    return int.Parse(xTray);
                }
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_FIRST_PAGE_PRINT_TRAY, value.ToString());
            }
        }

        public int OtherPagesPrinterTray
        {
            get
            {
                string xTray = GetUserAppPref(PREFNAME_OTHER_PAGES_PRINT_TRAY);

                if (xTray == "")
                {
                    return 0;
                }
                else
                {
                    return int.Parse(xTray);
                }
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_OTHER_PAGES_PRINT_TRAY, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not xml tags will be 
        /// displayed when editing a document with the editor
        /// </summary>
        public bool ShowXMLTags
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_XMLTAGS).ToUpper();
                return (xShow == null) ? false : (xShow == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_XMLTAGS, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets if user segments have been
        /// converted to use content controls
        /// </summary>
        public bool UserSegmentsConverted
        {
            get
            {
                string xConverted = GetUserAppPref(PREFNAME_USER_SEGMENTS_CONVERTED).ToUpper();
                return (xConverted == null) ? false : (xConverted == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_USER_SEGMENTS_CONVERTED, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets if user segments have been
        /// converted to use content controls
        /// </summary>
        public bool UpdateZoom
        {
            get
            {
                string xConverted = GetUserAppPref(PREFNAME_UPDATE_ZOOM).ToUpper();
                return (xConverted == null) ? false : (xConverted == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_UPDATE_ZOOM, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets lasts user value of Rogs Alternate Q and R
        /// </summary>
        public bool AlternateBetweenRogsQAndR
        {
            get
            {
                string xAlternate = GetUserAppPref(PREFNAME_ALTERNATE_ROGS_Q_AND_R).ToUpper();
                return (xAlternate == null) ? false : (xAlternate == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_ALTERNATE_ROGS_Q_AND_R, value.ToString());
            }
        }

        /// <summary>
        /// gets/sets the default folder used
        /// in the document designer
        /// </summary>
        public string DefaultDesignerFolderID
        {
            get { return GetUserAppPref(PREFNAME_DEFAULT_DESIGNER_FOLDER); }
            set { SetUserApplicationPreference(PREFNAME_DEFAULT_DESIGNER_FOLDER, value.ToString()); }
        }

        /// <summary>
        /// gets/sets whether or not the task pane will
        /// display when a MacPac document is opened
        /// </summary>
        public bool ShowTaskPaneWhenOpeningMPDoc
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_TASKPANE_MPDOC).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_TASKPANE_MPDOC, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the MacPac
        /// Ribbon Tab will be activate when the task pane is set up
        /// </summary>
        public bool ActivateMacPacRibbonTab
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the MacPac
        /// Ribbon Tab will be activated when a MacPac document is opened
        /// </summary>
        //GLOG : 2620 : CEH
        public bool ActivateMacPacRibbonTabOnOpen
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB_ONOPEN).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_ACTIVATE_MACPAC_RIBBON_TAB_ONOPEN, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the task pane will
        /// display when a non-MacPac document is opened 
        /// </summary>
        public bool ShowTaskPaneWhenOpeningNonMPDoc
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_TASKPANE_NON_MPDOC).ToUpper();
                return (xShow == null) ? false : (xShow == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_TASKPANE_NON_MPDOC, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the task pane will
        /// display when a Forte document is opened 
        /// </summary>
        public bool ShowTaskPaneWhenCreatingBlankMPDocument
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_TASKPANE_ON_BLANK_NEW_DOC).ToUpper();
                return (xShow == null) ? false : (xShow == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_TASKPANE_ON_BLANK_NEW_DOC, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the task pane will
        /// display when a Forte document with no UI elements is opened 
        /// </summary>
        public bool ShowTaskPaneWhenNoVariables
        {
            //GLOG 15933
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_TASKPANE_WHEN_NO_VARIABLES).ToUpper();
                return string.IsNullOrEmpty(xShow) ? true : (xShow == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_TASKPANE_WHEN_NO_VARIABLES, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not the Task Pane
        /// closes on clicking the Finish button
        /// </summary>
        //GLOG : 6049 : JSW
        public bool CloseTaskPaneOnFinish
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_CLOSE_TASKPANE_ON_FINISH).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_CLOSE_TASKPANE_ON_FINISH, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not to expand find results by default
        /// </summary>
        //GLOG : 8308 : JSW
        public bool ExpandFindResults
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_EXPAND_FIND_RESULTS).ToUpper();
                return (string.IsNullOrEmpty(xShow) ? false : (xShow != "FALSE"));
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_EXPAND_FIND_RESULTS, value.ToString());
            }
        }
        /// <summary>
        /// gets/sets whether or not to prompt the user
        /// when clicking the Finish button
        /// </summary>
        //GLOG : 6641 : JSW
        public bool WarnOnFinish
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_WARN_ON_FINISH).ToUpper();
                return (string.IsNullOrEmpty(xShow) ? false : (xShow != "FALSE"));
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_WARN_ON_FINISH, value.ToString());
            }
        }       /// <summary>
        /// gets/sets whether or not the Review Data
        /// message will show on data functions
        /// </summary>
        public bool ShowReviewDataMessage
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_REVIEW_DATA_MSG).ToUpper();
                return (string.IsNullOrEmpty(xShow)) ? true : (xShow.ToUpper() == "TRUE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_REVIEW_DATA_MSG, value.ToString());
            }
        }

        public bool ShowHelp
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_SHOW_HELP).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_HELP, value.ToString());
            }
        }

        public bool ShowLocationTooltips
        {
            get
            {
                string xShowLocationTooltips = GetUserAppPref(PREFNAME_SHOW_LOCATION_TOOLTIPS).ToUpper();
                return (xShowLocationTooltips == "") ? false : (xShowLocationTooltips != "FALSE");
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_SHOW_LOCATION_TOOLTIPS, value.ToString());
            }
        }

        public bool FitToPageWidth
        {
            get
            {
                string xFit = GetUserAppPref(PREFNAME_FIT_TO_PAGE_WIDTH).ToUpper();
                return (xFit == null) ? false : (xFit == "TRUE");
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_FIT_TO_PAGE_WIDTH, value.ToString());
            }
        }

        public int HelpWindowHeight
        {
            get
            {
                string xWindowHeight = GetUserAppPref(PREFNAME_HELP_WINDOW_HEIGHT);
                try
                {
                    //GLOG 8547: Apply scaling factor
                    return Convert.ToInt32(LMP.OS.GetScalingFactor() * float.Parse(xWindowHeight));
                }
                catch
                {
                    //GLOG 8547: Apply scaling factor
                    // The height was set to 87 prior to allowing the user to set this value.
                    return Convert.ToInt32(LMP.OS.GetScalingFactor() * DEFAULT_HELP_WINDOW_HEIGHT);
                }
            }

            set
            {

                //GLOG 8547: Make sure saved value is for 100% scaling factor
                SetUserApplicationPreference(PREFNAME_HELP_WINDOW_HEIGHT, Convert.ToString(value / LMP.OS.GetScalingFactor()));
            }
        }

        public string TaskPaneBackgroundEntry
        {
            get
            {
                return GetUserAppPref(PREFNAME_TASK_PANE_BACKGROUND_COLOR);
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_TASK_PANE_BACKGROUND_COLOR, value);
            }
        }

        public string TaskPaneWidth
        {
            get
            {
                try
                {
                    //GLOG 8547: Apply scaling factor
                    return Convert.ToString(Convert.ToInt32(GetUserAppPref(PREFNAME_TASK_PANE_WIDTH)) * LMP.OS.GetScalingFactor());
                }
                catch
                {
                    return "";
                }
            }

            set
            {
                //GLOG 8547: Make sure saved value is for 100% scaling factor
                SetUserApplicationPreference(PREFNAME_TASK_PANE_WIDTH, Convert.ToString(Int32.Parse(value) / LMP.OS.GetScalingFactor()));
            }
        }

        public Size AuthorPreferencesDialogSize
        {
            get
            {
                string xSize = GetUserAppPref(PREFNAME_AUTHOR_PREFS_WINDOW_SIZE);
                try
                {
                    if (string.IsNullOrEmpty(xSize))
                    {
                        return new Size(0, 0);
                    }
                    else
                    {
                        string[] aSize = xSize.Split(',');

                        //GLOG 8547: Apply scaling factor
                        return new Size(Convert.ToInt32(LMP.OS.GetScalingFactor() * float.Parse(aSize[0])), Convert.ToInt32(LMP.OS.GetScalingFactor() * float.Parse(aSize[1])));
                    }
                }
                catch
                {
                    return new Size(0, 0);
                }
            }
            set
            {
                //GLOG 8547: Make sure saved value is for 100% scaling factor
                SetUserApplicationPreference(PREFNAME_AUTHOR_PREFS_WINDOW_SIZE,
                    Convert.ToString(value.Width / LMP.OS.GetScalingFactor()) + "," +  Convert.ToString(value.Height / LMP.OS.GetScalingFactor()));
            }
        }

        public System.Drawing.Color TaskPaneBackgroundColor
        {
            get
            {
                DateTime t0 = DateTime.Now;

                string xColor = this.TaskPaneBackgroundEntry;
                string xInvalidColorEntry;
                System.Drawing.Color oColor = LMP.ColorUtil.ColorFromString(
                    xColor, out xInvalidColorEntry);

                LMP.Benchmarks.Print(t0);
                return oColor;
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_TASK_PANE_BACKGROUND_COLOR, value.R + ", " + value.G + ", " + value.B);
            }
        }

        public bool UseGradientInTaskPane
        {
            get
            {
                string xUseGradientSetting = GetUserAppPref(PREFNAME_USE_GRADIENT);

                try
                {
                    return Boolean.Parse(xUseGradientSetting);
                }
                catch
                {
                    LMP.Trace.WriteInfo("Invalid UseGradientInTaskPane get value: " + xUseGradientSetting);

                    // The use gradient setting was either not specified or was invalid. Revert to 
                    // the value used prior to allowing the user to customize this setting.
                    return false;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_USE_GRADIENT, Convert.ToString(value));
            }
        }

        public DataTable FavoriteSegments
        {
            // The list of quick access segment items are stored as a UserApplicationSetting with 
            // the segment names delimited by a � char. Letter�157�Memo�158�Fax�159�California 

            set
            {
                StringBuilder sbQuickAccessSegments = new StringBuilder();
                DataTable dtQuickAccessSegments = value;

                for (int i = 0; i < dtQuickAccessSegments.Rows.Count; i++)
                {
                    if(i > 0)
                    {
                        sbQuickAccessSegments.Append(FAV_SEGMENTS_INSTANCE_SEP); 
                    }

                    //GLOG 4775: Replace any occurrences of the defined separator characters
                    //within the Name or DisplayName of the items
                    string xSegmentDetails = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}",
                                    dtQuickAccessSegments.Rows[i]["SegmentName"].ToString()
                                        .Replace(FAV_SEGMENTS_DETAILS_SEP.ToString(), FAV_SEGMENTS_DETAILS_SEP_REPLACEMENT)
                                        .Replace(FAV_SEGMENTS_INSTANCE_SEP.ToString(), FAV_SEGMENTS_INSTANCE_SEP_REPLACEMENT), 
                                        FAV_SEGMENTS_DETAILS_SEP,
                                    dtQuickAccessSegments.Rows[i]["SegmentID"], FAV_SEGMENTS_DETAILS_SEP,
                                    dtQuickAccessSegments.Rows[i]["SegmentType"], FAV_SEGMENTS_DETAILS_SEP,
                                    dtQuickAccessSegments.Rows[i]["SegmentDisplayName"].ToString()
                                        .Replace(FAV_SEGMENTS_DETAILS_SEP.ToString(), FAV_SEGMENTS_DETAILS_SEP_REPLACEMENT)
                                        .Replace(FAV_SEGMENTS_INSTANCE_SEP.ToString(), FAV_SEGMENTS_INSTANCE_SEP_REPLACEMENT),
                                        FAV_SEGMENTS_DETAILS_SEP,
                                    dtQuickAccessSegments.Rows[i]["InsertionLocation"], FAV_SEGMENTS_DETAILS_SEP,
                                    dtQuickAccessSegments.Rows[i]["SegmentIntendedUse"]);

                    sbQuickAccessSegments.Append(xSegmentDetails);
                }

                SetUserApplicationPreference(PREFNAME_FAVORITE_SEGMENTS, sbQuickAccessSegments.ToString());
            }

            get
            {
                DataTable oFaveSegments = new DataTable();

                DataColumn oNameCol = new DataColumn("SegmentName", typeof(string));
                DataColumn oSegmentIDCol = new DataColumn("SegmentID", typeof(string));
                DataColumn oSegmentTypeCol = new DataColumn("SegmentType", typeof(LMP.Data.mpFolderMemberTypes));
                DataColumn oDisplayNameCol = new DataColumn("SegmentDisplayName", typeof(string));
               
                // Glog : 2475 Introduce insert location for favorite segments.
                DataColumn oInsertionLocation = new DataColumn("InsertionLocation", typeof(int));
				//GLOG : 6303 : jsw
				//add intended use icon to favorites 
                DataColumn oSegmentIntendedUse = new DataColumn("SegmentIntendedUse", typeof(LMP.Data.mpSegmentIntendedUses));
                
                // The insertion location is a new feature implemented after the introduction of
                // favorite segments. As a result, favorite segments will exist without insert 
                // location and require a default value. Set the value to be the segments default
                // insert location.
                oInsertionLocation.DefaultValue = FavoriteSegmentsInsertionLocations.Default;

                oFaveSegments.Columns.Add(oNameCol);
                oFaveSegments.Columns.Add(oSegmentIDCol);
                oFaveSegments.Columns.Add(oSegmentTypeCol);
                oFaveSegments.Columns.Add(oDisplayNameCol);
                oFaveSegments.Columns.Add(oInsertionLocation);
                oFaveSegments.Columns.Add(oSegmentIntendedUse);

                string xFaveSegments = GetUserAppPref(PREFNAME_FAVORITE_SEGMENTS);

                string[] aFaveSegments = xFaveSegments.Split(FAV_SEGMENTS_INSTANCE_SEP);

                for (int i = 0; i < aFaveSegments.Length; i++)
                {
                    if (aFaveSegments[i].Length > 0)
                    {
                        bool bBadData = false;
                        string[] aFaveSegmentDetails = aFaveSegments[i].Split(FAV_SEGMENTS_DETAILS_SEP);
                        //GLOG 4775: Skip if data is not valid
                        if (aFaveSegmentDetails.Length == 6 || aFaveSegmentDetails.Length == 5)
                        {
                            DataRow oRow = oFaveSegments.NewRow();

                            for (int iColumnIndex = 0; iColumnIndex < aFaveSegmentDetails.Length && iColumnIndex < oFaveSegments.Columns.Count; iColumnIndex++)
                            {
                                try
                                {
                                    if (oRow.Table.Columns[iColumnIndex].DataType ==  System.Type.GetType("System.String"))
                                    {
                                        string xText = aFaveSegmentDetails[iColumnIndex].ToString();
                                        //GLOG 4775: Restore reserved characters before adding to DataTable
                                        xText = xText.Replace(FAV_SEGMENTS_DETAILS_SEP_REPLACEMENT, FAV_SEGMENTS_DETAILS_SEP.ToString());
                                        xText = xText.Replace(FAV_SEGMENTS_INSTANCE_SEP_REPLACEMENT, FAV_SEGMENTS_INSTANCE_SEP.ToString());
                                        oRow[iColumnIndex] = xText;
                                    }
                                    else
                                        oRow[iColumnIndex] = aFaveSegmentDetails[iColumnIndex];
                                }
                                catch
                                {
                                    //GLOG 4775: Ignore Favorites entries with bad data
                                    bBadData = true;
                                    break;
                                }
                            }
                            if (!bBadData)
                                oFaveSegments.Rows.Add(oRow);
                        }
                    }
                }
                
                return oFaveSegments;
            }
        }

        /// <summary>
        /// gets/sets the various trailer insertion options 
        /// </summary>
        //GLOG : 3079 : CEH
        public TrailerInsertionOptions TrailerInsertionOption
        {
            get
            {
                string xOption = GetUserAppPref(PREFNAME_TRAILER_DISPLAY_OPTIONS);
                if (xOption == "")
                    return 0;
                else
                    return (TrailerInsertionOptions) Enum.Parse(typeof(TrailerInsertionOptions), xOption);
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_TRAILER_DISPLAY_OPTIONS, ((int)value).ToString());
            }
        }

        /// <summary>
        /// gets if default zoom is set
        /// </summary>
        //GLOG : 6517 : JSW
        public bool DefaultPageZoomPercentageIsSet
        {
            get
            {
                string xZoom = GetUserAppPref(PREFNAME_DEFAULT_PAGE_ZOOM_PERCENTAGE);
                if(string.IsNullOrEmpty(xZoom))
                    return false;
                
                else
                    return true;

            }
        }        
        /// <summary>
        /// gets/sets the default zoom 
        /// </summary>
        //GLOG : 5759 : CEH
        public int DefaultPageZoomPercentage
        {
            get
            {
                string xZoom = GetUserAppPref(PREFNAME_DEFAULT_PAGE_ZOOM_PERCENTAGE);
                try
                {
                    return Int32.Parse(xZoom);
                }
                catch
                {
                    //no value set
                    return DEFAULT_PAGE_ZOOM_PERCENTAGE;
                }
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_DEFAULT_PAGE_ZOOM_PERCENTAGE, Convert.ToString(value));
            }
        }

        public int PageNumberAlignment
        {
            get
            {
                string xPageNumberAlignment = GetUserAppPref(PREFNAME_PAGE_NUMBER_ALIGNMENT);
                try
                {
                    return Int32.Parse(xPageNumberAlignment);
                }
                catch
                {
                    // The height was set to 87 prior to allowing the user to set this value.
                    return DEFAULT_PAGE_NUMBER_ALIGNMENT;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_ALIGNMENT, Convert.ToString(value));
            }
        }

        //GLOG : 6262 : CEH
        public PageNumberScopes PageNumberScope
        {
            get
            {
                string xPageNumberScope = GetUserAppPref(PREFNAME_PAGE_NUMBER_SCOPE);
                if (xPageNumberScope == "")
                    return PageNumberScopes.Document;
                else
                    return (PageNumberScopes) (int.Parse(xPageNumberScope));
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_SCOPE, ((int)value).ToString());
            }
        }

        //GLOG : 6262 : CEH
        public PageNumberScopes PageNumberRemoveScope
        {
            get
            {
                string xPageNumberRemoveScope = GetUserAppPref(PREFNAME_PAGE_NUMBER_REMOVE_SCOPE);
                if (xPageNumberRemoveScope == "")
                    return PageNumberScopes.Document;
                else
                    return (PageNumberScopes)(int.Parse(xPageNumberRemoveScope));
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_REMOVE_SCOPE, ((int)value).ToString());
            }
        }

        public int PageNumberLocation
        {
            get
            {
                string xPageNumberLocation = GetUserAppPref(PREFNAME_PAGE_NUMBER_LOCATION);
                try
                {
                    return Int32.Parse(xPageNumberLocation);
                }
                catch
                {
                    return DEFAULT_PAGE_NUMBER_LOCATION;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_LOCATION, Convert.ToString(value));
            }
        }

        //GLOG : 7606 : CEH
        public int LabelType
        {
            get
            {
                string xLabelType = GetUserAppPref(PREFNAME_LABEL_TYPE);
                try
                {
                    return Int32.Parse(xLabelType);
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_LABEL_TYPE, Convert.ToString(value));
            }
        }

        public int EnvelopeType
        {
            get
            {
                string xEnvelopeType = GetUserAppPref(PREFNAME_ENVELOPE_TYPE);
                try
                {
                    return Int32.Parse(xEnvelopeType);
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_ENVELOPE_TYPE, Convert.ToString(value));
            }
        }


        public bool PageNumberRestartNumbering
        {
            get
            {
                string xRestart = GetUserAppPref(PREFNAME_PAGE_NUMBER_RESTART_NUMBERING).ToUpper();
                return (xRestart == null) ? true : (xRestart != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_RESTART_NUMBERING, value.ToString());
            }
        }

        public int PageNumberStartAtNumber
        {
            get
            {
                string xPageNumberStartAtNumber = GetUserAppPref(PREFNAME_PAGE_NUMBER_START_AT_NUMBER);
                try
                {
                    return Int32.Parse(xPageNumberStartAtNumber);
                }
                catch
                {
                    return DEFAULT_PAGE_NUMBER_START_AT_NUMBER;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_START_AT_NUMBER, Convert.ToString(value));
            }
        }

        public bool PageNumberOnFirstPage
        {
            get
            {
                string xFit = GetUserAppPref(PREFNAME_PAGE_NUMBER_ON_FIRST_PAGE).ToUpper();
                return (xFit == null) ? false : (xFit == "TRUE");
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_ON_FIRST_PAGE, value.ToString());
            }
        }

		//GLOG : 6435 : ceh
        public string PageNumberTotalFormat
        {
            get
            {
                string xPageNumberTotalFormat = GetUserAppPref(PREFNAME_PAGE_NUMBER_TOTAL_FORMAT);
                if (xPageNumberTotalFormat == "")
                {
                    return "none";
                }
                else
                    return xPageNumberTotalFormat;
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_TOTAL_FORMAT, value.ToString());
            }
        }

        public int PageNumberFormat
        {
            get
            {
                string xPageNumberFormat = GetUserAppPref(PREFNAME_PAGE_NUMBER_FORMAT);
                try
                {
                    return Int32.Parse(xPageNumberFormat);
                }
                catch
                {
                    return DEFAULT_PAGE_NUMBER_FORMAT;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_FORMAT, Convert.ToString(value));
            }
        }

        public int PageNumberRemoveOption
        {
            get
            {
                string xPageNumberRemoveOption = GetUserAppPref(PREFNAME_PAGE_NUMBER_REMOVE_OPTION);
                try
                {
                    switch (xPageNumberRemoveOption)
                    {
                        case "3":
                        case "4":
                            return Int32.Parse("2");
                        default:
                            return Int32.Parse(xPageNumberRemoveOption);
                    }
                }
                catch
                {
                    return DEFAULT_PAGE_NUMBER_REMOVE_OPTION;
                }
            }

            set
            {
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_REMOVE_OPTION, Convert.ToString(value));
            }
        }

        public string PageNumberTextBefore
        {
            get
            {
                string xPageNumberTextBefore = GetUserAppPref(PREFNAME_PAGE_NUMBER_TEXT_BEFORE);
                return xPageNumberTextBefore;
            }

            set
            {
                string xPageNumberTextBefore = value;
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_TEXT_BEFORE, xPageNumberTextBefore);
            }
        }

        public string PageNumberTextAfter
        {
            get
            {
                string xPageNumberTextAfter = GetUserAppPref(PREFNAME_PAGE_NUMBER_TEXT_AFTER);
                return xPageNumberTextAfter;
            }

            set
            {
                string xPageNumberTextAfter = value;
                SetUserApplicationPreference(PREFNAME_PAGE_NUMBER_TEXT_AFTER, xPageNumberTextAfter);
            }
        }

        //GLOG 8329: HelpTextAddSelectedText setting removed

        /// <summary>
        /// gets/sets the interrogatory styles for the application
        /// </summary>
        public string InterrogatoryStyles
        {
            get
            {
                string xRogStyles = GetUserAppPref(PREFNAME_ROG_STYLES);

                if (xRogStyles == "")
                {
                    //get style definitions from the firm record - this
                    //code is necessary here because this preference was
                    //added after production started - hence, some clients
                    //might have office app preferences that don't include
                    //this key value
                    if (this.m_iEntityID == -999999999)
                        return "QuestionHeading,Discovery No,2,0,0,0,True,True,False,True;QuestionText,Discovery Text,2,.5,0,0,False,False,False,False;ResponseHeading,Response No,2,0,0,0,True,True,False,True;ResponseText,Response Text,2,.5,0,0,False,False,False,False;RunInDiscovery,RunIn Question,2,0,0,0,True,True,False,False;RunInResponse,RunIn Response,2,0,0,0,False,False,False,False";
                    else
                    {
                        LMP.Data.UserApplicationSettings oSettings = new UserApplicationSettings(-999999999);
                        return oSettings.InterrogatoryStyles;
                    }
                }
                else
                    return xRogStyles;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_ROG_STYLES, value);
            }
        }

        /// <summary>
        /// gets/sets the string of values for Bates Labels creation
        /// </summary>
        public string BatesFieldValues
        {
            get
            {
                string xValues = GetUserAppPref(PREFNAME_BATES_FIELD_VALUES);
                return xValues;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_BATES_FIELD_VALUES, value);
            }
        }

        /// <summary>
        /// gets/sets the sticky Search Text value
        /// </summary>
        public string SearchOptionsText //GLOG 8162
        {
            get
            {
                string xValue = GetUserAppPref(PREFNAME_SEARCH_OPTIONS_TEXT);
                return xValue;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SEARCH_OPTIONS_TEXT, value);
            }
        }
        /// <summary>
        /// gets/sets the sticky Search Owner value
        /// </summary>
        public string SearchOptionsOwner //GLOG 8162
        {
            get
            {
                string xValue = GetUserAppPref(PREFNAME_SEARCH_OPTIONS_OWNER);
                return xValue;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SEARCH_OPTIONS_OWNER, value);
            }
        }
        /// <summary>
        /// gets/sets the sticky Search Content Type value
        /// </summary>
        public string SearchOptionsType //GLOG 8162
        {
            get
            {
                string xValue = GetUserAppPref(PREFNAME_SEARCH_OPTIONS_TYPE);
                return xValue;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SEARCH_OPTIONS_TYPE, value);
            }
        }
        /// <summary>
        /// gets/sets the sticky Search Field value
        /// </summary>
        public string SearchOptionsField //GLOG 8162
        {
            get
            {
                string xValue = GetUserAppPref(PREFNAME_SEARCH_OPTIONS_FIELD);
                return xValue;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SEARCH_OPTIONS_FIELD, value);
            }
        }
        /// <summary>
        /// gets/sets the sticky Search Location value
        /// </summary>
        public string SearchOptionsLocation //GLOG 8162
        {
            get
            {
                string xValue = GetUserAppPref(PREFNAME_SEARCH_OPTIONS_LOCATION);
                return xValue;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SEARCH_OPTIONS_LOCATION, value);
            }
        }

        // Accessor to the coordinates to location of the CI Dialog box.
        public Point CIDlgLocation
        {
            get
            {
                // Get the string representing the location of the CI Dialog. The format is: 100, 200
                string xCIDlgLocation = GetUserAppPref(PREFNAME_CI_DIALOG_LOCATION);
                
                int iX;
                int iY;

                if (IsValidCIDlgLocation(xCIDlgLocation))
                {
                    string [] aCoordinates = xCIDlgLocation.Split(',');
                    //GLOG 8547: Apply scaling factor
                    //GLOG : 15751 : ceh
                    iX = Convert.ToInt32(float.Parse(aCoordinates[0], LMP.Culture.USEnglishCulture) * LMP.OS.GetScalingFactor());
                    iY = Convert.ToInt32(float.Parse(aCoordinates[1], LMP.Culture.USEnglishCulture) * LMP.OS.GetScalingFactor());
                }
                else
                {
                    iX = 0;
                    iY = 0;
                }

                return new Point(iX, iY); 
            }

            set
            {
                Point oCIDlgLocationPoint = value;
                //GLOG 8547: Make sure saved value is for 100% scaling factor
                //GLOG : 15751 : ceh
                string xCIDlgLocation = Convert.ToString(oCIDlgLocationPoint.X / LMP.OS.GetScalingFactor(), LMP.Culture.USEnglishCulture) + ", " + 
                                        Convert.ToString(oCIDlgLocationPoint.Y / LMP.OS.GetScalingFactor(),LMP.Culture.USEnglishCulture);

                SetUserApplicationPreference(PREFNAME_CI_DIALOG_LOCATION, xCIDlgLocation);
            }
        }

        private bool IsValidCIDlgLocation(string xCIDlgLocation)
        {
            return (xCIDlgLocation != null && xCIDlgLocation.Length > 0 && xCIDlgLocation.Contains(","));
        }

        private bool IsValidCIDlgLocationCoordinateX(int iCIDLocationCoordinateX)
        {
            // Check if the coordinate is within the screen width.

            int iScreenWidth = Screen.PrimaryScreen.WorkingArea.Width;
            return (iCIDLocationCoordinateX >= 0 && iCIDLocationCoordinateX < iScreenWidth);
        }

        private bool IsValidCIDlgLocationCoordinateY(int iCIDLocationCoordinateY)
        {
            // Check if the coordinate is valid within the screen height.

            int iScreenHeight = Screen.PrimaryScreen.WorkingArea.Height;
            return (iCIDLocationCoordinateY >= 0 && iCIDLocationCoordinateY < iScreenHeight);
        }

        public string DefaultPrefillFolder
        {
            get
            {                
                string xDefaultPrefillFolder = GetUserAppPref(PREFNAME_DEFAULT_PREFILL_FOLDER);
                return xDefaultPrefillFolder;
            }

            set
            {
                string xDefaultPrefillFolder = value;
                SetUserApplicationPreference(PREFNAME_DEFAULT_PREFILL_FOLDER, xDefaultPrefillFolder);
            }
        }

        /// <summary>
        /// gets/sets whether or not to warn user that a manual
        /// deletion will result in segment deactivation
        /// </summary>
        public bool WarnBeforeDeactivation
        {
            get
            {
                string xShow = GetUserAppPref(PREFNAME_WARN_BEFORE_DEACTIVATION).ToUpper();
                return (xShow == null) ? true : (xShow != "FALSE");
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_WARN_BEFORE_DEACTIVATION, value.ToString());
            }
        }
        public int SectionBreakLocation
        {
            get
            {
                string xLoc = GetUserAppPref(PREFNAME_SECTION_BREAK_LOCATION);
                if (!string.IsNullOrEmpty(xLoc))
                    return int.Parse(xLoc);
                else
                    return 0;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_LOCATION, value.ToString());
            }
        }
        public int SectionBreakHeader1Option
        {
            get
            {
                string xOpt = GetUserAppPref(PREFNAME_SECTION_BREAK_HEADER1_OPTION);
                if (!string.IsNullOrEmpty(xOpt))
                    return int.Parse(xOpt);
                else
                    return 1;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_HEADER1_OPTION, value.ToString());
            }
        }
        public int SectionBreakHeader2Option
        {
            get
            {
                string xOpt = GetUserAppPref(PREFNAME_SECTION_BREAK_HEADER2_OPTION);
                if (!string.IsNullOrEmpty(xOpt))
                    return int.Parse(xOpt);
                else
                    return 1;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_HEADER2_OPTION, value.ToString());
            }
        }
        public int SectionBreakPageNumber1Option
        {
            get
            {
                string xOpt = GetUserAppPref(PREFNAME_SECTION_BREAK_PAGENUMBER1_OPTION);
                if (!string.IsNullOrEmpty(xOpt))
                    return int.Parse(xOpt);
                else
                    return 0;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_PAGENUMBER1_OPTION, value.ToString());
            }
        }
        public int SectionBreakPageNumber2Option
        {
            get
            {
                string xOpt = GetUserAppPref(PREFNAME_SECTION_BREAK_PAGENUMBER2_OPTION);
                if (!string.IsNullOrEmpty(xOpt))
                    return int.Parse(xOpt);
                else
                    return 0;
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_PAGENUMBER2_OPTION, value.ToString());
            }
        }
        public bool SectionBreakRestartNumbering
        {
            get
            {
                return GetUserAppPref(PREFNAME_SECTION_BREAK_RESTART_NUMBERING).ToUpper() == "TRUE";
            }
            set
            {
                SetUserApplicationPreference(PREFNAME_SECTION_BREAK_RESTART_NUMBERING, value.ToString());
            }
        }
        #endregion
        #region****************************methods****************************
        /// <summary>
        /// Update User Application preference for specified Key
        /// </summary>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        private void SetUserApplicationPreference(string xKey, string xValue)
        {
            try
            {
                KeySet oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "",
                    this.m_iEntityID.ToString());
                oPrefs.SetValue(xKey, xValue);
                oPrefs.Save();

                //clear cached keyset to force values to update
                m_oCachedKeyset = null;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.KeySetException(
                    LMP.Resources.GetLangString("Error_CouldNotSetUserAppPref") + xKey + ":" + xKey, oE);
            }
        }
        /// <summary>
        /// Get value of specified User Application Setting key
        /// </summary>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private string GetUserAppPref(string xKey)
        {
            DateTime t0 = DateTime.Now;

            if (string.IsNullOrEmpty(xKey))
            {
                return null;
            }

            KeySet oPrefs = null;

            //GLOG 6966: Make sure string can be parsed as a Double even if
            //the Decimal separator is something other than '.' in Regional Settings
            if (m_oCachedKeyset != null)
            {
                string xCachedEntityID = m_oCachedKeyset.EntityID;
                if (xCachedEntityID.EndsWith(".0"))
                    xCachedEntityID = xCachedEntityID.Replace(".0", "");
                if (double.Parse(xCachedEntityID) == (double)this.m_iEntityID)
                {
                    //use cached keyset
                    oPrefs = m_oCachedKeyset;
                }
            }
            if (oPrefs == null)
            {
                oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "",
                    this.m_iEntityID.ToString());

                //cache keyset for faster retrieval in the future
                m_oCachedKeyset = oPrefs;
            }

            try
            {
                string xPref = oPrefs.GetValue(xKey);

                LMP.Benchmarks.Print(t0, xKey);

                return xPref;
            }
            catch
            {
                LMP.Benchmarks.Print(t0, xKey);
                return "";
            }
        }

        /// <summary>
        /// deletes this user app settings record
        /// </summary>
        public void Delete()
        {
            KeySet oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "",
                this.m_iEntityID.ToString());

            oPrefs.Delete();
        }
        /// <summary>
        /// returns true if the user app settings are a default
        /// </summary>
        public bool IsDefault //GLOG 6731
        {
            get 
            {
                if (m_oCachedKeyset != null)
                    return m_oCachedKeyset.IsDefault;
                else
                {
                    KeySet oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "", this.m_iEntityID.ToString());
                    return oPrefs.IsDefault;
                }
            } 
        }

        #endregion
    }
}
