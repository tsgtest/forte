using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.OleDb;

namespace LMP.Data
{
    public class SharedFolder: IFolder
    {
        /// <summary>
        /// Returns list populated with Names and IDs of UserSegments and VariableSets in Shared Folder
        /// </summary>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static ArrayList GetSharedFolderMembersForDisplay(int iSharedFolderID)
        {
            DateTime t0 = DateTime.Now;
            string xSQL = "spSharedFolderMembersForDisplay";
            //get reader with display fields only
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                xSQL, false, new object[] { iSharedFolderID }))
            {
                try
                {
                    //fill array
                    ArrayList oArray = SimpleDataCollection.DataReaderToArrayList(oReader);
                    LMP.Benchmarks.Print(t0);
                    return oArray;
                }
                finally
                {
                    oReader.Close();
                }
            }
        }
        /// <summary>
        /// Share UserSegment or VariableSet by setting SharedFolderID field
        /// </summary>
        /// <param name="iSharedFolderID"></param>
        /// <param name="iObjectID1"></param>
        /// <param name="iObjectID2"></param>
        /// <param name="iType"></param>
        public static void AssignToSharedFolder(int iSharedFolderID, int iObjectID1, int iObjectID2, mpObjectTypes iType)
        {
            string xSProc = "";

            if (iType == mpObjectTypes.UserSegment)
            {
                xSProc = "spUserSegmentsSetSharedFolder";
            }
            else if (iType == mpObjectTypes.VariableSet)
            {
                xSProc = "spVariableSetsSetSharedFolder";
            }
            int iNum = LongIDSimpleDataCollection.ExecuteActionSproc(xSProc,
                new object[] { iSharedFolderID, iObjectID1, iObjectID2, Application.GetCurrentEditTime()});
            if (iNum == 0)
            {
                //Update query did nothing
                throw new LMP.Exceptions.StoredProcedureException(LMP.Resources.GetLangString(
                    "Error_CouldNotAddSharedFolderMember") + iObjectID1.ToString() + "." + iObjectID2.ToString());
            }
        }
        /// <summary>
        /// Remove UserSegment or VariableSet from Shared Folder by setting SharedFolderID to 0
        /// </summary>
        /// <param name="iObjectID1"></param>
        /// <param name="iObjectID2"></param>
        /// <param name="iType"></param>
        public static void RemoveSharedFolderAssignment(int iObjectID1, int iObjectID2, mpObjectTypes iType)
        {
            string xSProc = "";

            if (iType == mpObjectTypes.UserSegment)
            {
                xSProc = "spUserSegmentsSetSharedFolder";
            }
            else if (iType == mpObjectTypes.VariableSet)
            {
                xSProc = "spVariableSetsSetSharedFolder";
            }
            int iNum = LongIDSimpleDataCollection.ExecuteActionSproc(xSProc,
                new object[] { 0, iObjectID1, iObjectID2, Application.GetCurrentEditTime() });
            //JTS 12/11/08: Add deletion record to remove from other local DBs when syncing
            SimpleDataCollection.LogDeletions(iType, iObjectID1.ToString() + "." + iObjectID2.ToString());
        }

        public static int GetSharedFolderID(int iObjectiD1, int iObjectID2, mpObjectTypes iType)
        {
            string xSProc = "";
            if (iType == mpObjectTypes.VariableSet)
                xSProc = "spVariableSetsGetSharedFolderID";
            else
                xSProc = "spUserSegmentsGetSharedFolderID";

            object oID = SimpleDataCollection.GetScalar(xSProc, new object[] { iObjectiD1, iObjectID2 });
            if (oID != null)
            {
                return (int)oID;
            }
            else
                return 0;
        }
		//GLOG 8037
        public static string GetSharedContentFullPath(string xMemberId, int iUserID)
        {
            int iID1 = 0;
            int iID2 = 0;
            string  xFolderPath = "";
            mpObjectTypes iObjectType = 0;
            if (xMemberId.StartsWith("P"))
            {
                string xVarSetID = xMemberId.Substring(1);
                string[] aIDs = xVarSetID.Split('.');
                iID1 = Int32.Parse(aIDs[0]);
                iID2 = Int32.Parse(aIDs[1]);
                iObjectType = mpObjectTypes.VariableSet;
            }
            else if (xMemberId.StartsWith("U"))
            {
                string xUSegID = xMemberId.Substring(1);
                string[] aIDs = xUSegID.Split('.');
                iID1 = Int32.Parse(aIDs[0]);
                iID2 = Int32.Parse(aIDs[1]);
                iObjectType = mpObjectTypes.UserSegment;
            }
            int iSharedFolderID = SharedFolder.GetSharedFolderID(iID1, iID2, iObjectType);
            if (iSharedFolderID > 0)
            {
                xFolderPath = GetFolderPath(iSharedFolderID.ToString());
            }
            else if (UserPermissions.IsPersonPermitted(iObjectType, iID1, iID2, iUserID))
            {
                LocalPersons oPersons = new LocalPersons(iUserID);
                try
                {
                    Person oPerson = oPersons.ItemFromID(iID1);
                    if (!(oPerson == null))
                    {
                        xFolderPath = new FirmApplicationSettings(iUserID).SharedFoldersLabel + "/" + oPerson.DisplayName;
                    }
                }
                catch { }
            }
            return xFolderPath;
        }
        /// <summary>
        /// Get the name and the parent folder id of the folder corresponding to the folder id param.
        /// Getting the name facilitates building the path string of a segment. Getting the parent
        /// folder id facilitates getting the name of next folder in the paths heiarchy.
        /// </summary>
        /// <param name="xFolderId"></param>
        /// <returns></returns>
        private static string[] GetFolderDetails(string xFolderId)
        {
            //JTS 3/13/10: Hard-coded SQL to speed up execution
            const string FIND_SQL = "SELECT Folders.ParentFolderID, Folders.Name FROM Folders " +
                    "WHERE Folders.ID={0}";

            //string xProcName = "spAdminSegmentParentFolderId";
            //ArrayList aliParentFolderIds = SimpleDataCollection.GetArray(xProcName, new object[] { iSegmentId });

            string xRootFolderId = "-2";

            if (xFolderId == xRootFolderId)
            {
                return new string[] { "", new FirmApplicationSettings(LMP.Data.Application.User.ID).SharedFoldersLabel };
            }

            string xSql = string.Format(FIND_SQL, xFolderId);
            ArrayList alFolderDetails = SimpleDataCollection.GetArray(xSql);

            if (alFolderDetails.Count > 0)
            {
                int iFolderParentId = (int)((object[])alFolderDetails[0])[0];
                string xFolderParentId = iFolderParentId.ToString();
                string xFolderName = (string)((object[])alFolderDetails[0])[1];
                return new string[] { xFolderParentId, xFolderName };
            }
            // We should never get here.
            return new string[] { "", "[NOT FOUND]" };
        }
        /// <summary>
        /// Get the path of a given folder which is usually the parent folder of a segment.
        /// </summary>
        /// <param name="xParentFolderId"></param>
        /// <returns></returns>
        private static string GetFolderPath(string xParentFolderId)
        {
            StringBuilder sbPath = new StringBuilder("");

            // Store the path in an arraylist which will grow as the parent folders are determined.
            ArrayList alPaths = new ArrayList();

            do
            {
                // Get the parent folder's name and the id of its parent.
                string[] axParentFolderDetails = GetFolderDetails(xParentFolderId);

                // Set the parent folder id to the id of this parent folder's parent folder for the next iteration in this loop.
                xParentFolderId = axParentFolderDetails[0];

                // Get the name of the parent folder.
                string xParentFolderName = axParentFolderDetails[1];

                // Add the parent folder name to the list of parent folders list.
                alPaths.Add(xParentFolderName);

            } while (xParentFolderId.Length > 0);

            for (int i = alPaths.Count - 1; i >= 0; i--)
            {
                sbPath.Append(alPaths[i] + "/");
            }

            return sbPath.ToString();
        }
    }
}
