using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a
	/// MacPac Office
	/// created by Daniel Fisherman - 09/04
	/// </summary>
	public sealed class Office: LongIDSimpleDataItem
	{
		private byte m_bytUsageState = 0;
		private string m_xDisplayName = null;
		private string m_xName = null;
		private string m_xAbbr = null;
		private int m_iAddressID = 0;
		private string m_xFirmName = null;
		private string m_xFirmNameUpperCase = null;
		private string m_xSlogan = null;
		private string m_xFirmID = null;
		private Address m_oAddress = null;
		private Hashtable m_oCustomProps = null;
		private static string[] m_aCustomPropNames = null;
        private string m_xURL = "";

		#region *********************constructors*********************
		internal Office(){
			//create case-insensitive hashtable
			m_oCustomProps = CollectionsUtil.CreateCaseInsensitiveHashtable();

			SetupCustomPropertiesCollection();
		}
		internal Office(int ID): base(ID){
			//create case-insensitive hashtable
			m_oCustomProps = CollectionsUtil.CreateCaseInsensitiveHashtable();
		
			SetupCustomPropertiesCollection();
		}
		#endregion

		#region *********************properties*********************
		public byte UsageState
		{
			get{return m_bytUsageState;}
			set{this.SetBytePropertyValue(ref m_bytUsageState, value);}
		}

		public string DisplayName
		{
			get{return m_xDisplayName;}
			set{this.SetStringPropertyValue(ref m_xDisplayName, value);}
		}

		public string Name
		{
			get {return m_xName;}
			set {this.SetStringPropertyValue(ref m_xName,value);}
		}

		public string Abbr
		{
			get {return m_xAbbr;}
			set {this.SetStringPropertyValue(ref m_xAbbr,value);}
		}
		public int AddressID
		{
			get{return m_iAddressID;}
			set{this.SetIntPropertyValue(ref m_iAddressID, value);}
		}

		public string FirmName
		{
			get {return m_xFirmName;}
			set {this.SetStringPropertyValue(ref m_xFirmName, value);}
		}
		public string FirmNameUpperCase
		{
			get {return m_xFirmNameUpperCase;}
			set {this.SetStringPropertyValue(ref m_xFirmNameUpperCase, value);}
		}
		public string Slogan
		{
			get {return m_xSlogan;}
			set {this.SetStringPropertyValue(ref m_xSlogan, value);}
		}
		public string FirmID
		{
			get {return m_xFirmID;}
			set {this.SetStringPropertyValue(ref m_xFirmID, value);}
		}
		public int CustomPropertyCount
		{
			get{return m_oCustomProps.Count;}
		}
		public string[] CustomPropertyNames
		{
			get{return m_aCustomPropNames;}
		}
        public string URL
        {
            get { return m_xURL; }
            set { this.SetStringPropertyValue(ref m_xURL, value); }
        }

		#endregion
	
		#region *********************methods*********************
		/// <summary>
		/// returns the value of the specified property
		/// </summary>
		/// <param name="xPropName"></param>
		/// <returns></returns>
		public object GetProperty(string xPropName)
		{
			object oPropValue;
			switch(xPropName.ToUpper())
			{
				case "ABBR":
					oPropValue = this.Abbr;
					break;
				case "ADDRESSID":
					oPropValue = this.AddressID;
					break;
				case "DISPLAYNAME":
					oPropValue = this.DisplayName;
					break;
				case "FIRMNAME":
					oPropValue = this.FirmName;
					break;
                case "FIRMNAMEUPPERCASE":
                    //GLOG 3783
                    oPropValue = this.FirmNameUpperCase;
                    break;
                case "FIRMID":
                    oPropValue = this.FirmID;
                    break;
                case "ID":
					oPropValue = this.ID;
					break;
				case "NAME":
					oPropValue = this.Name;
					break;
				case "CITY":
					oPropValue = this.GetAddress().City;
					break;
				case "COUNTRY":
					oPropValue = this.GetAddress().Country;
					break;
				case "COUNTY":
					oPropValue = this.GetAddress().County;
					break;
				case "FAX1":
					oPropValue = this.GetAddress().Fax1;
					break;
				case "FAX2":
					oPropValue = this.GetAddress().Fax2;
					break;
				case "LINE1":
					oPropValue = this.GetAddress().Line1;
					break;
				case "LINE2":
					oPropValue = this.GetAddress().Line2;
					break;
				case "LINE3":
					oPropValue = this.GetAddress().Line3;
					break;
				case "PHONE1":
					oPropValue = this.GetAddress().Phone1;
					break;
				case "PHONE2":
					oPropValue = this.GetAddress().Phone2;
					break;
				case "PHONE3":
					oPropValue = this.GetAddress().Phone3;
					break;
				case "POSTCITY":
					oPropValue = this.GetAddress().PostCity;
					break;
				case "PRECITY":
					oPropValue = this.GetAddress().PreCity;
					break;
				case "STATE":
					oPropValue = this.GetAddress().State;
					break;
				case "STATEABBR":
					oPropValue = this.GetAddress().StateAbbr;
					break;
				case "ZIP":
					oPropValue = this.GetAddress().Zip;
					break;
                case "URL":
                    oPropValue = this.URL;
                    break;
                case "SLOGAN":
                    oPropValue = this.Slogan;
                    break;
				default:
					//attempt to get property from properties collection
					try
					{
						oPropValue = m_oCustomProps[xPropName];
						break;
					}
					catch(System.Exception e)
					{
						throw new LMP.Exceptions.NotInCollectionException(
							LMP.Resources.GetLangString("Error_PropertyDoesNotExist") + xPropName, e);
					}
			}
			return oPropValue;
		}

		/// <summary>
		/// sets the specified custom property to the specified value
		/// </summary>
		/// <param name="xName"></param>
		/// <param name="oValue"></param>
		public void SetCustomProperty(string xName, object oValue)
		{
            if (m_oCustomProps.ContainsKey(xName))
            {
                m_oCustomProps[xName] = oValue;
                this.IsDirty = true;
            }
            else
                //property does not exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_OfficePropertyDoesNotExist") + xName);
		}

		/// <summary>
		/// returns the address of this office
		/// </summary>
		/// <returns></returns>
		public Address GetAddress()
		{
			if(m_oAddress == null)
				m_oAddress = (Address) new Addresses().ItemFromID(m_iAddressID);

			return m_oAddress;
		}

        /// <summary>
        /// returns the keyset of the specified type and preference for this office
        /// </summary>
        /// <param name="bytKeySetType"></param>
        /// <param name="xPreferenceScope"></param>
        /// <returns></returns>
        public KeySet GetKeySet(LMP.Data.mpKeySetTypes bytKeySetType, string xPreferenceScope)
        {
            KeySet oKeySet = new KeySet(bytKeySetType, xPreferenceScope, this.ID.ToString(), 0);
            return oKeySet;
        }
		#endregion

		#region *********************LongIDSimpleDataItem members*********************
		internal override object[] GetAddQueryParameters()
		{
			//not used by collection class Save method
			throw new LMP.Exceptions.NotImplementedException(
				LMP.Resources.GetLangString("Error_OverloadNotImplemented"));
		}
	
		internal override object[] GetUpdateQueryParameters()
		{
			//not used by collection class Save method
			throw new LMP.Exceptions.NotImplementedException(
				LMP.Resources.GetLangString("Error_OverloadNotImplemented"));
		}
	
		public override object[] ToArray()
		{
			//get number of custom properties
			int iNumCustProps = m_aCustomPropNames.Length;

			//there are 10 built in props - create appropriate storage space
			object[] oValues = new object[10 + iNumCustProps];

			oValues[0] = this.ID;
			oValues[1] = this.UsageState;
			oValues[2] = this.Name;
			oValues[3] = this.Abbr;
			oValues[4] = this.DisplayName;
			oValues[5] = this.FirmName;
			oValues[6] = this.FirmNameUpperCase;
			oValues[7] = this.Slogan;
			oValues[8] = this.FirmID;
			oValues[9] = this.AddressID;

			for(int i=0; i < iNumCustProps; i++)
				//get value
				oValues[10 + i] = GetProperty(m_aCustomPropNames[i]);

			return oValues;
		}

		internal override bool IsValid()
		{
			//office data is valid if name, display name, firm name, and address ID have been supplied
			return (this.Name != null && this.Name != "" && this.DisplayName != null && 
				this.DisplayName != "" && this.FirmName != null && this.FirmName != "" &&
				this.AddressID != 0);
			;
		}
		#endregion

		#region *********************private functions*********************
		private void SetupCustomPropertiesCollection()
		{
			DateTime t0 = DateTime.Now;
			DataTable oTable = null;
			int iNumFields = 0;

			if(m_aCustomPropNames == null)
			{
				//get custom prop name array - the array is static, 
				//hence it will only be retrieved once a session
                using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                    "spOffices", true, null))
                {

                    try
                    {
                        iNumFields = oReader.FieldCount;

                        //create array for custom properties - there are 12 built-in properties
                        m_aCustomPropNames = new string[iNumFields - 12];

                        //get column names from schema data table
                        oTable = oReader.GetSchemaTable();
                    }
                    finally
                    {
                        oReader.Close();
                    }
                }

				//cycle through first column of schema table, which contains
				//the field names - skip built-in fields
				for(int i = 11; i < iNumFields - 1; i++)
				{
					//add field name to array of custom property names
					m_aCustomPropNames[i - 11] = oTable.Rows[i].ItemArray[0].ToString();
				}
			}

			//fill hashtable for this object
			foreach(string xPropName in m_aCustomPropNames)
			{
				m_oCustomProps.Add(xPropName, null);
			}

			Benchmarks.Print(t0);
		}
		#endregion
	}


	/// contains the methods and properties that manage the
	/// collection of MacPac offices-
	/// created by Daniel Fisherman - 09/04
	public sealed class Offices: LongIDSimpleDataCollection
	{
		LMP.Data.LongIDUpdateObjectDelegate m_oDel = null;

		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return null;}
		}

		protected override string CountSprocName
		{
			get{return "spOfficesCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spOfficesDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spOfficesItemFromID";}
		}
		protected override string LastIDSprocName
		{
			get{return "spOfficesLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spOffices";}
		}

		protected override object[] SelectSprocParameters
		{
			get{return null;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return null;}
		}

		protected override object[] CountSprocParameters
		{
			get{return null;}
		}

		protected override string UpdateSprocName
		{
			get{return null;}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new Office();
		}

		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			Office oOffice = new Office();
			oOffice.SetID((int) oValues[0]);
			oOffice.UsageState = (byte) oValues[1];
			oOffice.Name = oValues[2].ToString();
			oOffice.Abbr = oValues[3].ToString();
			oOffice.DisplayName = oValues[4].ToString();
			oOffice.FirmName = oValues[5].ToString();
			oOffice.FirmNameUpperCase = oValues[6].ToString();
			oOffice.Slogan = oValues[7].ToString();
			oOffice.FirmID = oValues[8].ToString();
			oOffice.AddressID = (int) oValues[9];
            oOffice.URL = oValues[10].ToString();
			
			if(oValues.Length > 11)
			{
				//there are custom properties -
				//add custom properties to custom properties collection
				for(int i = 11; i < oValues.Length - 1;i++)
				{
					string xPropName = oOffice.CustomPropertyNames[i-11];
					oOffice.SetCustomProperty(xPropName, oValues[i]);
				}
			}

			return oOffice;
		}

		public override void Save(LongIDSimpleDataItem oItem)
		{
			Trace.WriteNameValuePairs("oItem.IsDirty", oItem.IsDirty);

			if(oItem.IsDirty)
			{
				Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

				//test if item has valid data to be saved
				if(!oItem.IsValid())
				{
					//some data is not valid - alert
					throw new LMP.Exceptions.DataException(string.Concat(
						LMP.Resources.GetLangString("Error_InvalidOrMissingObjectData"), 
						oItem.GetType(),"-\n", oItem.ToString()));
				}
						
				if(oItem.IsPersisted)
				{
					//item have been previously saved - update item
					string xSQL = GetUpdateSQL((Office) oItem);
					int iRet = ExecuteAction(xSQL);

					if(iRet==0)
						throw new LMP.Exceptions.SQLStatementException(
							LMP.Resources.GetLangString("Error_SQLStatementFailed") + xSQL);

					if(m_oArray != null)
					{
						//save changes to array -
						//get index of edited item
						int iIndex = GetItemIndex(oItem.ID);

						Trace.WriteNameValuePairs("iIndex", iIndex);

						//update value array if item was found
						if(iIndex > -1)
							m_oArray[iIndex] = oItem.ToArray();
					}
				}

				else
				{
					//item has no ID - it's not been previously saved - add item
					string xSQL = GetAddSQL((Office) oItem);

					int iRet = ExecuteAction(xSQL);

					if(iRet==0)
						throw new LMP.Exceptions.SQLStatementException(
							LMP.Resources.GetLangString("Error_SQLStatementFailed") + xSQL);

					Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

					if(m_oArray != null)
						//TODO: In the future, we may want to simply null out the 
						//array, forcing it to repopulate - this would put the newly
						//added item at it's 'proper' index, instead of at the end
						//append to array -
						m_oArray.Insert(m_oArray.Count,oItem.ToArray());
				}

				oItem.IsPersisted = true;
				oItem.IsDirty = false;
			}
		}

		public override void Delete(int iID)
		{
            //GLOG 7590:  Can't use base.Delete since it doesn't update LastEditTime
            int iRecsAffected = SimpleDataCollection
                .ExecuteActionSproc(this.DeleteSprocName, new object[] { iID, LMP.Data.Application.GetCurrentEditTime() });
            if (iRecsAffected == 0)
            {
                //not deleted - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iID.ToString());
            }
            //base.Delete(iID);

			//delete relevant keysets
			SimpleDataCollection.ExecuteActionSproc("spKeySetsDeleteByEntityIDs", 
				new object[] {iID, ForteConstants.mpFirmRecordID});

			//log deletion
			string xID = iID.ToString() + "." + ForteConstants.mpFirmRecordID.ToString();
			SimpleDataCollection.LogDeletions(mpObjectTypes.KeySets, xID);

			//delete relevant object assignments
			SimpleDataCollection.ExecuteActionSproc("spAssignmentsDeleteByObjectIDs", 
				new object[] {201,iID});

            //log deletion
            SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, iID.ToString() + "." + ForteConstants.mpFirmRecordID.ToString());

		}

		#endregion

		#region *********************private functions*********************
		/// <summary>
		/// returns the sql for the update query
		/// </summary>
		/// <param name="oOffice">office that will get updated by query</param>
		/// <returns></returns>
		private string GetUpdateSQL(Office oOffice)
		{
			//build update sql - start with built-in fields
			StringBuilder oSB = new StringBuilder(string.Concat("UPDATE Offices SET ",
				"UsageState=", oOffice.UsageState, ",Name=\"", oOffice.Name, 
				"\",Abbr=\"", oOffice.Abbr, "\",DisplayName=\"", oOffice.DisplayName,
				"\",FirmName=\"", oOffice.FirmName, "\",FirmNameUpperCase=\"", oOffice.FirmNameUpperCase,
				"\",Slogan=\"", oOffice.Slogan, "\",FirmID=\"", oOffice.FirmID,
				"\",AddressID=", oOffice.AddressID,
                ",URL=\"", oOffice.URL, "\"", ",LastEditTime=#", Application.GetCurrentEditTime(true), "#"));

			//append custom fields to SQL
			foreach(string xCustProp in oOffice.CustomPropertyNames)
			{
				//get value
				object oValue = oOffice.GetProperty(xCustProp);

				//append to UPDATE statement
				if(oValue == null || oValue == System.DBNull.Value)
					oSB.AppendFormat(",{0}={1}", xCustProp, "Null");
				else
				{
					//append to UPDATE statement
					if(oValue is System.String)
						//append with enclosing quotes
						oSB.AppendFormat(",{0}='{1}'", xCustProp, oValue.ToString());
					else
						//append without enclosing quotes
						oSB.AppendFormat(",{0}={1}", xCustProp, oValue.ToString());
				}
			}

			//finish building sql
			oSB.AppendFormat(" WHERE Offices.ID={0}", oOffice.ID);
	
			return oSB.ToString();
		}


		/// <summary>
		/// returns the sql for the add query
		/// </summary>
		/// <param name="oOffice">office that will get added by the query</param>
		/// <returns></returns>
		private string GetAddSQL(Office oOffice)
		{
			if(oOffice.ID == 0)
				//item id was not previously assigned- assign now
				oOffice.SetID(GetLastID() - 1);

			//build update sql - start with built-in fields
			StringBuilder oSB = new StringBuilder(string.Concat(
				"INSERT INTO Offices (ID,UsageState,Name,Abbr,",
				"DisplayName,FirmName,FirmNameUpperCase,Slogan,FirmID,AddressID,URL,LastEditTime"));

			//append custom fields names to SQL
			foreach(string xCustProp in oOffice.CustomPropertyNames)
				oSB.AppendFormat(",{0}", xCustProp);

			//append built-in values to SQL
			oSB.AppendFormat(") SELECT {0},{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9},'{10}', #{11}#",
				oOffice.ID, oOffice.UsageState, oOffice.Name, oOffice.Abbr, 
				oOffice.DisplayName, oOffice.FirmName, oOffice.FirmNameUpperCase, 
				oOffice.Slogan, oOffice.FirmID, oOffice.AddressID, oOffice.URL, Application.GetCurrentEditTime(true));

			//append custom field values to SQL
			foreach(string xCustProp in oOffice.CustomPropertyNames)
			{
				//get value
				object oValue = oOffice.GetProperty(xCustProp);

				//append to UPDATE statement
				if(oValue == null || oValue == System.DBNull.Value)
					oSB.AppendFormat(",{0}", "Null");
				else
				{
					if(oValue is System.String)
						//append with enclosing quotes
						oSB.AppendFormat(",'{0}'", oValue.ToString());
					else
						//append without enclosing quotes
						oSB.AppendFormat(",{0}", oValue.ToString());
				}
			}

			return oSB.ToString();
		}
		#endregion
	}
}
