using System;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a MacPac court
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Court: LongIDSimpleDataItem
	{
		int m_iAddressID = 0;
		string m_xJudgeName = "";

		//initialze to -1 because 0 is a valid value for levels
		int m_iL0 = -1;
		int m_iL1 = -1;
		int m_iL2 = -1;
		int m_iL3 = -1;
		int m_iL4 = -1;
		Address m_oAddress = null;

		#region *********************constructors*********************
		internal Court(){}
		internal Court(int iID): base(iID){}
		#endregion

		#region *********************properties*********************
		public int AddressID
		{
			get{return m_iAddressID;}
			set{this.SetIntPropertyValue(ref m_iAddressID, value);}
		}

		public string JudgeName
		{
			get{return m_xJudgeName;}
			set{this.SetStringPropertyValue(ref m_xJudgeName,value);}
		}

		public int L0
		{
			get{return m_iL0;}
			set{this.SetIntPropertyValue(ref m_iL0, value);}
		}

		public int L1
		{
			get{return m_iL1;}
			set{this.SetIntPropertyValue(ref m_iL1, value);}
		}

		public int L2
		{
			get{return m_iL2;}
			set{this.SetIntPropertyValue(ref m_iL2, value);}
		}

		public int L3
		{
			get{return m_iL3;}
			set{this.SetIntPropertyValue(ref m_iL3, value);}
		}

		public int L4
		{
			get{return m_iL4;}
			set{this.SetIntPropertyValue(ref m_iL4, value);}
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// returns the address of the court
		/// </summary>
		/// <returns></returns>
		public Address GetAddress()
		{
			if(m_oAddress == null)
			{
				//address has not yet been retrieved - retrieve
				Addresses oAddresses = new Addresses();

				//assign address to field
				m_oAddress = (Address) oAddresses.ItemFromID(m_iAddressID);
			}
			
			return m_oAddress;
		}

		/// <summary>
		/// returns the value of the specified property
		/// </summary>
		/// <param name="xPropertyName"></param>
		/// <returns></returns>
		public string GetPropertyValue(string xPropertyName)
		{
			Trace.WriteNameValuePairs("xPropertyName", xPropertyName);

			//compare upper case version
			string xUpperName = xPropertyName.ToUpper();
			string xVal = "";

			//return appropriate property
			switch(xUpperName)
			{
				case "ID":
					xVal = this.ID.ToString();
					break;
				case "JUDGENAME":
					xVal = this.JudgeName;
					break;
				case "L0":
					xVal = this.L0.ToString();
					break;
				case "L1":
					xVal = this.L1.ToString();
					break;
				case "L2":
					xVal = this.L2.ToString();
					break;
				case "L3":
					xVal = this.L3.ToString();
					break;
				case "L4":
					xVal = this.L4.ToString();
					break;
				case "CITY":
					xVal = this.GetAddress().City;
					break;
				case "COUNTRY":
					xVal = this.GetAddress().Country;
					break;
				case "COUNTY":
					xVal = this.GetAddress().County;
					break;
				case "FAX1":
					xVal = this.GetAddress().Fax1;
					break;
				case "FAX2":
					xVal = this.GetAddress().Fax2;
					break;
				case "LINE1":
					xVal = this.GetAddress().Line1;
					break;
				case "LINE2":
					xVal = this.GetAddress().Line2;
					break;
				case "LINE3":
					xVal = this.GetAddress().Line3;
					break;
				case "PHONE1":
					xVal = this.GetAddress().Phone1;
					break;
				case "PHONE2":
					xVal = this.GetAddress().Phone2;
					break;
				case "PHONE3":
					xVal = this.GetAddress().Phone3;
					break;
				case "POSTCITY":
					xVal = this.GetAddress().PostCity;
					break;
				case "PRECITY":
					xVal = this.GetAddress().PreCity;
					break;
				case "STATE":
					xVal = this.GetAddress().State;
					break;
				case "STATEABBR":
					xVal = this.GetAddress().StateAbbr;
					break;
				case "ZIP":
					xVal = this.GetAddress().Zip;
					break;
                case "LEVELSXML":
                    //Returns levels in format usable by JurisdictionChooser control
                    string xTemplate = "<L{0}>{1}</L{0}>";
                    xVal = string.Format(xTemplate, "0", this.L0.ToString());
                    xVal = xVal + string.Format(xTemplate, "1", this.L1.ToString());
                    xVal = xVal + string.Format(xTemplate, "2", this.L2.ToString());
                    xVal = xVal + string.Format(xTemplate, "3", this.L3.ToString());
                    xVal = xVal + string.Format(xTemplate, "4", this.L4.ToString());
                    break;
				default:
					//invalid field code
					throw new LMP.Exceptions.PropertyNameException(
						LMP.Resources.GetLangString(
						"Error_InvalidPropertyName") + xPropertyName);
			}

			return xVal;
		}
																	 
		#endregion

		#region *********************LongIDSimpleDataItem members*********************
		internal override bool IsValid()
		{
			//return true iff required info is present
			return (m_iL0 > -1) && (m_iL1 > -1) && (m_iL2 > -1) && 
				(m_iL3 > -1) && (m_iL4 > -1);
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[8];
			oProps[0] = this.ID;
			oProps[1] = this.L0;
			oProps[2] = this.L1;
			oProps[3] = this.L2;
			oProps[4] = this.L3;
			oProps[5] = this.L4;
			oProps[6] = this.AddressID;
			oProps[7] = this.JudgeName;

			return oProps;
		}

        internal override object[] GetUpdateQueryParameters()
        {
            object[] oProps = new object[9];
            oProps[0] = this.ID;
            oProps[1] = this.L0;
            oProps[2] = this.L1;
            oProps[3] = this.L2;
            oProps[4] = this.L3;
            oProps[5] = this.L4;
            oProps[6] = this.AddressID;
            oProps[7] = this.JudgeName;
            oProps[8] = Application.GetCurrentEditTime();
            return oProps;
        }
        internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[8];
			oProps[0] = this.L0;
			oProps[1] = this.L1;
			oProps[2] = this.L2;
			oProps[3] = this.L3;
			oProps[4] = this.L4;
			oProps[5] = this.AddressID;
			oProps[6] = this.JudgeName;
            oProps[7] = Application.GetCurrentEditTime();
			return oProps;
		}
		#endregion
	}

	/// <summary>
	/// contains the methods and properties that manage the 
	/// MacPac collection of Courts - derived from LongIDSimpleDataCollection -
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public sealed class Courts: LongIDSimpleDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel = null;

		#region *********************constructors*********************
		public Courts():base(){}

		#endregion

		#region *********************methods*********************

		/// <summary>
		/// returns the court that most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <param name="iL3"></param>
		/// <param name="iL4"></param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, int iL2,
			int iL3, int iL4)
		{
			Trace.WriteNameValuePairs("iL0", iL0, "iL1", iL1,
				"iL2", iL2, "iL3", iL3, "iL4", iL4);

			//if current item already matches, don't change it
			if (m_oItem != null)
			{
				Court oCourt = (Court) m_oItem;
				if ((oCourt.L0 == iL0) && (oCourt.L1 == iL1) && (oCourt.L2 == iL2) &&
					(oCourt.L3 == iL3) && (oCourt.L4 == iL4))
					return m_oItem;
			}

			object[] oParams = new object[]{iL0, iL1, iL2, iL3, iL4};

			System.Data.OleDb.OleDbDataReader oReader = null;
			try
			{
				oReader = SimpleDataCollection.GetResultSet("spCourtsItemFromLevels",
					true, oParams);
			
				if (oReader.HasRows)
				{
					//save current item if dirty and there are listeners 
					//requesting this - ideally, this should be called
					//after we know that the requested item exists - 
					//unfortunately, we can't save when a DataReader is open
					//so we do it here
					if(base.m_oItem != null && base.m_oItem.IsDirty)
					{
						SaveCurrentItemIfRequested();
					}

					oReader.Read();

					//get values at current row
					object[] oValues = new object[oReader.FieldCount];

					oReader.GetValues(oValues);


					//update object with values
					base.m_oItem = this.UpdateObjectDelegate(oValues);
					base.m_oItem.IsPersisted = true;
					base.m_oItem.IsDirty = false;
					return m_oItem;
				}
				else
				{
					//item was not found
					throw new LMP.Exceptions.NotInCollectionException(System.String.Concat(
						LMP.Resources.GetLangString("Error_ItemWithLevelsNotInCollection"),
						", ", iL0, ", ", iL1, ", ", iL2, ", ", iL3, ", ", iL4));
				}
			}
			finally
			{
				oReader.Close();
                oReader.Dispose();
			}
		}

		/// <summary>
		/// returns the court that most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <param name="iL3"></param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, int iL2,
			int iL3)
		{
			return ItemFromLevels(iL0, iL1, iL2, iL3, 0);
		}

		/// <summary>
		/// returns the court that most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <param name="iL2"></param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1, int iL2)
		{
			return ItemFromLevels(iL0, iL1, iL2, 0, 0);
		}

		/// <summary>
		/// returns the court that most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <param name="iL1"></param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0, int iL1)
		{
			return ItemFromLevels(iL0, iL1, 0, 0, 0);
		}

		/// <summary>
		/// returns the court that most closely matches the specified levels
		/// </summary>
		/// <param name="iL0"></param>
		/// <returns></returns>
		public LongIDSimpleDataItem ItemFromLevels(int iL0)
		{
			return ItemFromLevels(iL0, 0, 0, 0, 0);
		}

		#endregion

		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spCourtsAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spCourtsCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spCourtsDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spCourtsItemFromID";}
		}

		protected override string LastIDSprocName
		{
			get{return "spCourtsLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spCourts";}
		}

		protected override object[] SelectSprocParameters
		{
			get{return null;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return null;}
		}

		protected override object[] CountSprocParameters
		{
			get{return null;}
		}

		protected override string UpdateSprocName
		{
			get{return "spCourtsUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new Court();
		}

		#endregion

		#region *********************private functions*********************
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			Court oCourt = new Court((int) oValues[0]);
			oCourt.L0 = (int) oValues[1];
			oCourt.L1 = (int) oValues[2];
			oCourt.L2 = (int) oValues[3];
			oCourt.L3 = (int) oValues[4];
			oCourt.L4 = (int) oValues[5];
			oCourt.AddressID = (int) oValues[6];
			oCourt.JudgeName = oValues[7].ToString();
			oCourt.IsDirty = false;

			return oCourt;
		}
		#endregion
	}
}
