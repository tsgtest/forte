using System;
using System.Data.OleDb;
using System.Globalization;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a MacPac language
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public class Language
	{
		public const int mpUSEnglishCultureID = 1033;

		#region *****************constructors*****************
		public Language(){}
		#endregion

		#region *****************static members*****************
		/// <summary>
		/// sets/returns the ID of the current MacPac culture, which, in
		/// turn, sets/returns the current thread's UI culture
		/// </summary>
		public static int CultureID
		{
			get{return LMP.Resources.CultureID;}
			set{LMP.Resources.SetCulture(value);}
		}
		#endregion

		#region *****************methods*****************
		/// <summary>
		/// returns the full name of the language with the specified ID
		/// </summary>
		/// <param name="iLanguageID"></param>
		/// <returns></returns>
		public static string GetLanguageName(int iLanguageID)
		{
			CultureInfo oCulture= new CultureInfo(iLanguageID);
			return oCulture.EnglishName;
		}


		/// <summary>
		/// returns a string array of the IDs of the languages supported by the firm
		/// </summary>
		/// <returns></returns>
		public static string[] GetFirmLanguages()
		{
			return GetLanguages(ForteConstants.mpFirmRecordID);
		}


		/// <summary>
		/// returns a string array of the IDs of the languages supported by the office
		/// </summary>
		/// <param name="iOfficeID">ID of the Office</param>
		/// <returns></returns>
		public static string[] GetOfficeLanguages(int iOfficeID)
		{
			return GetLanguages(iOfficeID);
		}


		/// <summary>
		/// deletes the translation with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		public static void DeleteTranslation(int iID)
		{
			Trace.WriteNameValuePairs("iID", iID);

			SimpleDataCollection.ExecuteActionSproc(
				"spTranslationsDeleteByID",new object[]{iID});
            SimpleDataCollection.LogDeletions(mpObjectTypes.Translations, iID);
		}

		/// <summary>
		/// deletes the translation with the specified
		/// ID if it is no longer being used
		/// </summary>
		/// <param name="xTableName">the name of the table in which the 
		/// deleted translation is being currently used</param>
		/// <param name="iTranslationID">ID of the translation</param>
		public static void DeleteTranslationIfUnused(string xTableName, int iTranslationID)
		{
			Trace.WriteNameValuePairs("xTableName", xTableName, "iID", iTranslationID);

			if(IsSafeToDeleteTranslation(iTranslationID, xTableName))
			{
				//translation with specifed ID is not used elsewhere - delete
				SimpleDataCollection.ExecuteActionSproc(
					"spTranslationsDeleteByID", new object[] {iTranslationID});
                SimpleDataCollection.LogDeletions(mpObjectTypes.Translations, iTranslationID);
				//remove the existing reference to the deleted translation if it exists
                //GLOG 6673:  Make sure Date in SQL string can be interpreted as US Format
                string xSQL = string.Concat("UPDATE ", xTableName, 
					" SET TranslationID = Null, LastEditTime =#", Application.GetCurrentEditTime(true), 
                    "# WHERE TranslationID = ", iTranslationID);

				SimpleDataCollection.ExecuteAction(xSQL);
			}
		}


		/// <summary>
		/// returns the translation for the specified
		/// translation ID and locale ID as a variant array
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <param name="iCulture"></param>
		/// <returns></returns>
		internal static object[] GetCustomTranslation(int iTranslationID, int iCulture)
		{
			Trace.WriteNameValuePairs("iTranslationID", 
				iTranslationID, "iCulture", iCulture);
			
			if(iTranslationID == 0)
				return null;
			
			object[] aParams = new object[] {iTranslationID, iCulture};
			int[] aFields = new int[] {0,1,2,3,4,5};

			//get an arry of translations containing all but the 
			//sixth column of the translations table
			System.Collections.ArrayList oArr = SimpleDataCollection.GetArray(
					aFields, "spTranslations", aParams);

			return oArr.ToArray();
		}


		/// <summary>
		/// returns the current culture translations for the specified
		/// translation ID as a variant array
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <returns></returns>
		internal static object[] GetCustomTranslation(int iTranslationID)
		{
			return GetCustomTranslation(iTranslationID, Language.CultureID);
		}


		/// <summary>
		/// returns the translations for the specified translation ID as an object array
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <returns></returns>
		public static object[] GetCustomTranslations(int iTranslationID)
		{
			Trace.WriteNameValuePairs("iTranslationID", iTranslationID);
			
			if(iTranslationID == 0)
				return null;

			object[] aParams = new object[] {iTranslationID, DBNull.Value};
			int[] iFields = new int[] {0,1,2,3,4,5};

			//get an array of translations containing all but the 
			//sixth column of the translations table
			System.Collections.ArrayList oArr = SimpleDataCollection.GetArray(
				iFields, "spTranslations", aParams);

			return oArr.ToArray();
		}


		/// <summary>
		/// returns the translation id for the specified record
		/// </summary>
		/// <param name="xTableName">name of table containing the record</param>
		/// <param name="xIDFieldName">name of field containing the value</param>
		/// <param name="oFieldValue">name of the value to translate</param>
		/// <returns></returns>
		internal static int GetTranslationID(string xTableName, string xIDFieldName, object oFieldValue)
		{
			int iTranslationID = 0;

			Trace.WriteNameValuePairs("xTableName", xTableName, 
				"xIDFieldName", xIDFieldName, "oFieldValue", oFieldValue);

			///build sql string
			string xSQL = string.Concat("SELECT TranslationID FROM ", xTableName, 
				" WHERE ",xIDFieldName, "=", oFieldValue.ToString());

			//get set of matching translation ids - should be only one record
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(xSQL, true))
            {
                try
                {
                    if (oReader.HasRows)
                    {
                        //a translation ID record was found for the specified value - return it
                        oReader.Read();
                        iTranslationID = (int)oReader.GetValue(0);
                        return (int)iTranslationID;
                    }
                    else
                    {
                        //no translation ID record was found
                        throw new LMP.Exceptions.NotInCollectionException(LMP.Resources.GetLangString(
                            "Error_ItemWithIDNotInCollection") + oFieldValue.ToString());
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }
		}


		/// <summary>
		/// returns the translation id for the specified ValueSets record
		/// </summary>
		/// <param name="iValueSetID"></param>
		/// <param name="xValue"></param>
		/// <returns></returns>
		internal static int GetValueTranslationID(int iValueSetID, string xValue)
		{
			int iTranslationID = 0;

			Trace.WriteNameValuePairs("iValueSetID", iValueSetID, "xValue", xValue);

			//get the set of translation IDs for the specified 
			//value - should be only a single record
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                "spValueTranslationID", true, new object[] { iValueSetID, xValue }))
            {
                if (oReader.HasRows)
                {
                    //a translation ID record was found for the specified value - return it
                    oReader.Read();
                    object oVal = oReader.GetValue(0);
                    oReader.Close();

                    if (oVal is System.DBNull)
                        //no translationID
                        return 0;
                    else
                        iTranslationID = (int)oVal;

                    return iTranslationID;
                }
                else
                {
                    oReader.Close();
                    return 0;
                }
            }
		}
				

		/// <summary>
		/// creates a translation record with the values supplied and 
		/// assigns the translation to the specified record
		/// </summary>
		/// <param name="xTableName">table name containing the record that will refer to this translation</param>
		/// <param name="xIDFieldName">field name containing the ID of the record that will refer to this translation</param>
		/// <param name="oRecordID">ID of the record that will refer to this translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		internal static int SetCustomTranslation(string xTableName, string xIDFieldName, 
			object oRecordID, int iCulture, object oValue1)
		{
			return SetCustomTranslation(xTableName, xIDFieldName, oRecordID, 
				iCulture, oValue1, DBNull.Value, DBNull.Value, DBNull.Value);
		}


		/// <summary>
		/// creates a translation record with the values supplied and 
		/// assigns the translation to the specified record
		/// </summary>
		/// <param name="xTableName">table name containing the record that will refer to this translation</param>
		/// <param name="xIDFieldName">field name containing the ID of the record that will refer to this translation</param>
		/// <param name="oRecordID">ID of the record that will refer to this translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		internal static int SetCustomTranslation(string xTableName, string xIDFieldName, 
			object oRecordID, int iCulture, object oValue1, object oValue2)
		{
			return SetCustomTranslation(xTableName, xIDFieldName, oRecordID, 
				iCulture, oValue1, oValue2, DBNull.Value, DBNull.Value);
		}


		/// <summary>
		/// creates a translation record with the values supplied and 
		/// assigns the translation to the specified record
		/// </summary>
		/// <param name="xTableName">table name containing the record that will refer to this translation</param>
		/// <param name="xIDFieldName">field name containing the ID of the record that will refer to this translation</param>
		/// <param name="oRecordID">ID of the record that will refer to this translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		internal static int SetCustomTranslation(string xTableName, 
			string xIDFieldName, object oRecordID, int iCulture, object oValue1, 
			object oValue2, object oValue3)
		{
			return SetCustomTranslation(xTableName, xIDFieldName, oRecordID, 
				iCulture, oValue1, oValue2, oValue3, DBNull.Value);
		}


		/// <summary>
		/// creates a translation record with the values supplied and 
		/// assigns the translation to the specified record
		/// </summary>
		/// <param name="xTableName">table name containing the record that will refer to this translation</param>
		/// <param name="xIDFieldName">field name containing the ID of the record that will refer to this translation</param>
		/// <param name="oRecordID">ID of the record that will refer to this translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		/// <param name="oValue4">fourth translation in record</param>
		internal static int SetCustomTranslation(string xTableName, 
			string xIDFieldName, object oRecordID, int iCulture, object oValue1, 
			object oValue2, object oValue3, object oValue4)
		{
			Trace.WriteNameValuePairs("xTableName", xTableName, 
				"oRecordID", oRecordID.ToString(),
				"xIDFieldName", xIDFieldName, "iCulture", iCulture, "oValue1",
				oValue1, "oValue2", oValue2, "oValue3", oValue3, "oValue4", oValue4);
	  
			//add translation record - get last inserted ID
			int iNextID = (int) SimpleDataCollection.GetScalar("spTranslationsLastID",null);

			//get next available ID
			iNextID = iNextID + 1;

			Trace.WriteNameValuePairs("iNextID", iNextID);

			object[] aParams = new object[] 
				{iNextID, iCulture, oValue1, oValue2, oValue3, oValue4};

			SimpleDataCollection.ExecuteActionSproc("spTranslationAdd", aParams);

			//if we're here, the record was added -
			//write translation id back to appropriate record
			SetTranslationID(iNextID, xTableName, oRecordID, xIDFieldName);
			return iNextID;
		}


		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		internal static void SetCustomTranslation(int iTranslationID, int iCulture,object oValue1)
		{
			SetCustomTranslation(iTranslationID, iCulture, oValue1, DBNull.Value, DBNull.Value, DBNull.Value);
		}


		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		internal static void SetCustomTranslation(int iTranslationID, int iCulture,
			object oValue1, object oValue2)
		{
			SetCustomTranslation(iTranslationID, iCulture, oValue1, oValue2, DBNull.Value, DBNull.Value);
		}


		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		internal static void SetCustomTranslation(int iTranslationID, int iCulture,
			object oValue1, object oValue2, object oValue3)
		{
			SetCustomTranslation(iTranslationID, iCulture, oValue1, oValue2, oValue3, DBNull.Value);
		}


		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		/// <param name="oValue4">fourth translation in record</param>
		internal static void SetCustomTranslation(int iTranslationID, int iCulture,
			object oValue1, object oValue2, object oValue3, object oValue4)
		{
			Trace.WriteNameValuePairs("iTranslationID", iTranslationID, 
				"iCulture", iCulture, "oValue1", oValue1, "oValue2", oValue2,
				"oValue3", oValue3, "oValue4", oValue4);

            string xTranslationSproc = null;
	  
			//see if record with the supplied ids exists
            using (OleDbDataReader oReader = SimpleDataCollection.GetResultSet(
                "spTranslations", true, new object[] { iTranslationID, iCulture }))
            {
                if (oReader.HasRows)
                {
                    //record exists - update record
                    oReader.Close();

                    Trace.WriteInfo("Edit translation record.");

                    xTranslationSproc = "spTranslationUpdate";
                }
                else
                {
                    oReader.Close();

                    //record does not exist - add record
                    Trace.WriteInfo("Add new translation record.");

                    xTranslationSproc = "spTranslationAdd";
                }
            }

			//get array of parameters for the add and edit query,
			//one of which will be executed below
			object[] aParams = new object[] 
				{iTranslationID, iCulture, oValue1, oValue2, oValue3, oValue4};
			
			SimpleDataCollection.ExecuteActionSproc(xTranslationSproc, aParams);
		}

		
		/// <summary>
		/// returns true if there is no more than one reference to lTranslationID 
		/// in xTableName and no references to it elsewhere in the db
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <param name="xTableName"></param>
		/// <returns></returns>
		internal static bool IsSafeToDeleteTranslation(int iTranslationID, string xTableName)
		{
			///not safe if there are multiple records using this ID
			if(TranslationIDExistsInTable(iTranslationID, xTableName, true))
				return false;
			
			//build array of names of all tables that support translations
			string[] aTables = new string[] {"Folders",
				"Segments", "PeopleGroups", "ValueSets"};

			//cycle through all other tables that support translation
			foreach(string x in aTables)
			{
				if((x.ToUpper() != xTableName.ToUpper()) && 
					TranslationIDExistsInTable(iTranslationID, x, false))
					//translation record exists in table - return false
					return false;
			}
			return true;
		}


		/// <summary>
		/// creates a translation for the specified ValueSet item.
		/// </summary>
		/// <param name="iSetID">ID of the value set</param>
		/// <param name="xColumn1Value">value of the item in the value set that will refer to the new translation</param>
		/// <param name="iCulture">culture of the new translation</param>
		/// <param name="oValue1">first value of the translation</param>
		/// <param name="oValue2">second value of the translation</param>
		internal static void SetValueSetTranslation(int iSetID, string xColumn1Value, 
			int iCulture, object oValue1)
		{
			SetValueSetTranslation(iSetID, xColumn1Value, iCulture, oValue1, DBNull.Value);
		}


		/// <summary>
		/// creates a translation for the specified ValueSet item.
		/// </summary>
		/// <param name="iSetID">ID of the value set</param>
		/// <param name="xColumn1Value">value of the item in the value set that will refer to the new translation</param>
		/// <param name="iCulture">culture of the new translation</param>
		/// <param name="oValue1">first value of the translation</param>
		/// <param name="oValue2">second value of the translation</param>
		internal static void SetValueSetTranslation(int iSetID, string xColumn1Value, 
			int iCulture, object oValue1, object oValue2)
		{
			Trace.WriteNameValuePairs("iSetID", iSetID, "xColumn1Value", xColumn1Value, 
				"iCulture", iCulture, "oValue1", oValue1, "oValue2", oValue2);
	  
			//add translation record - get last inserted ID
			int iNextID = (int) SimpleDataCollection.GetScalar("spTranslationsLastID",null);

			//get next available ID
			iNextID = iNextID + 1;

			Trace.WriteNameValuePairs("iNextID", iNextID);

			object[] aParams = new object[] 
				{iNextID, iCulture, oValue1, oValue2, DBNull.Value, DBNull.Value, Application.GetCurrentEditTime()};

			//create translation in db
			SimpleDataCollection.ExecuteActionSproc("spTranslationAdd", aParams);

			//if we're here, the record was added -
			//write translation id back to appropriate record
			aParams = new object[] {iSetID, xColumn1Value, iNextID, Application.GetCurrentEditTime()};
			SimpleDataCollection.ExecuteActionSproc("spValueSetTranslationID", aParams);
		}


		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		/// <param name="oValue4">fourth translation in record</param>
		internal static void SetValueSetTranslation(int iTranslationID, int iCulture,object oValue1)
		{
			SetValueSetTranslation(iTranslationID, iCulture, oValue1, DBNull.Value);
		}

		
		/// <summary>
		/// edits a translation record with the values supplied-
		/// if a record is found with the specified parameters, record is edited-
		/// if record is not found, or lTranslationID is empty, record is created
		/// </summary>
		/// <param name="iTranslationID">ID of the translation record to be edited - 0 to add a translation</param>
		/// <param name="iCulture">culture of the translation</param>
		/// <param name="oValue1">first translation in record</param>
		/// <param name="oValue2">second translation in record</param>
		/// <param name="oValue3">third translation in record</param>
		/// <param name="oValue4">fourth translation in record</param>
		internal static void SetValueSetTranslation(int iTranslationID, int iCulture,
			object oValue1, object oValue2)
		{
			SetCustomTranslation(iTranslationID, iCulture,oValue1, oValue2, DBNull.Value, DBNull.Value);
		}



		#endregion
		
		#region *****************internal functions*****************
		/// <summary>
		/// returns the IDs of the Languages
		///	available to the entity as an array of strings
		/// </summary>
		/// <param name="iEntityID">ID of the entity</param>
		/// <returns></returns>
		private static string[] GetLanguages(int iEntityID)
		{
			//get all languages for this entity - get appropriate
			//keyset, which stores the languages available to the entity
			KeySet oKeySet = KeySet.GetFirmAppKeys(iEntityID);

			//get all languages other than the default language
			string xLangs = oKeySet.GetValue("OtherLanguages");

			Trace.WriteNameValuePairs("xLangs", xLangs);

			//prepend default language
			xLangs += string.Concat(oKeySet.GetValue("DefaultLanguage"), ",");

			//convert languages to array, then return
			return xLangs.Split(',');
		}
		
		/// <summary>
		/// if bMultipleOnly=False, returns true if there is any reference to lTranslationID in xTableName;
		/// if bMultipleOnly=True, returns true only if there are multiple references to the id in the table
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <param name="xTableName"></param>
		/// <param name="bMultipleOnly"></param>
		/// <returns></returns>
		private static bool TranslationIDExistsInTable(int iTranslationID, 
			string xTableName, bool bMultipleOnly)
		{
			
			Trace.WriteNameValuePairs("iTranslationID", iTranslationID,
				"xTableName", xTableName, "bMultipleOnly", bMultipleOnly);

			//build sql that will count the translations for the specified table
			string xSQL = string.Concat("SELECT COUNT(*) FROM ", xTableName, 
				" t WHERE t.TranslationID=", iTranslationID);
			
			//get count
			int iCount = (int) SimpleDataCollection.GetScalar(xSQL);

			if(bMultipleOnly)
				return (iCount > 1);
			else
				return (iCount > 0);
		}


		/// <summary>
		/// writes the specified translation ID to the specified record in the
		/// specified table - assumes translation id field is named TranslationID
		/// </summary>
		/// <param name="iTranslationID"></param>
		/// <param name="xTableName"></param>
		/// <param name="oRecordID"></param>
		/// <param name="xIDFieldName"></param>
		private static void SetTranslationID(int iTranslationID, string xTableName, 
			object oRecordID, string xIDFieldName)
		{
			//build sql string
			string xSQL = string.Concat("UPDATE ",xTableName, " SET TranslationID=", 
				iTranslationID, " WHERE ", xIDFieldName, "=", oRecordID.ToString());
		
			Trace.WriteNameValuePairs("xSQL", xSQL);

			//execute the update
			int iRecs = SimpleDataCollection.ExecuteAction(xSQL);

			//alert if no records were updated - record must not have been found
			if(iRecs == 0)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_CouldNotSetTranslationID") + xSQL);
		}

		#endregion
	}
}
