using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.Data
{
    public class Author
    {
        #region *********************fields*********************
        private string m_xBarID = "";
        private string m_xID  = "";
        private string m_xFullName = "";
        private bool m_bIsManualAuthor = false;
        private Person m_oPersonObject = null;
        #endregion
        #region *********************constructors*********************
        public Author(Person oPerson)
        {
            SetPersonObject(oPerson);
        }
        public Author()
        {
            SetPersonObject(null);
        }
        #endregion
        #region *********************private methods*********************
        private void SetPersonObject(LMP.Data.Person oPerson)
        {
            //object
            m_oPersonObject = oPerson;
            if (m_oPersonObject != null)
            {
                m_xID = m_oPersonObject.ID;
                m_xFullName = m_oPersonObject.FullName;
                m_xBarID = "";
                m_bIsManualAuthor = false;
            }
            else
            {
                m_xID = "";
                m_xFullName = "";
                m_xBarID = "";
                m_bIsManualAuthor = true;
            }
        }
        #endregion
        #region *********************properties*********************
        public string BarID
        {
            get { return m_xBarID; }
            set { m_xBarID = value; }
        }
        public string FullName
        {
            get 
            {
                return m_xFullName;
            }
            set 
            { 
                if (m_bIsManualAuthor)
                    m_xFullName = value; 
            }
        }
        public string ID
        {
            get 
            {
                return m_xID;
            }
            set
            {
                if (m_bIsManualAuthor)
                    m_xID = value;
            }
        }
        public bool IsManualAuthor
        {
            get { return m_bIsManualAuthor; }
        }
        public Person PersonObject
        {
            get { return m_oPersonObject; }
        }
        #endregion
    }
}
