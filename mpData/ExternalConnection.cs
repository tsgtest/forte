using System;
using System.Data.OleDb;
using System.Collections;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a MacPac ExternalConnection
	/// created by Doug Miller - 09/05
	/// </summary>

    public sealed class ExternalConnection: LongIDSimpleDataItem
	{
		string m_xName = "";
		DatabaseTypes m_iDatabaseType = DatabaseTypes.SQLServer;
		string m_xServer = "";
		string m_xDatabase = "";
		string m_xUserID = "";
		string m_xPassword = "";

		#region *********************enumerations*********************
        public enum DatabaseTypes{
            SQLServer = 1,
            Oracle = 2,
            Access = 3
        }
        #endregion
        #region *********************constructors*********************
		internal ExternalConnection(){}
		internal ExternalConnection(int ID):base(ID){}
		#endregion
		#region *********************properties*********************
		public string Name
		{
			get{return m_xName;}
			set{this.SetStringPropertyValue(ref m_xName, value);}
		}

		public DatabaseTypes DatabaseType
		{
			get{return m_iDatabaseType;}
			set{m_iDatabaseType = value;}
		}

		public string Server
		{
			get{return m_xServer;}
			set{this.SetStringPropertyValue(ref m_xServer, value);}
		}

		public string Database
		{
			get{return m_xDatabase;}
			set{this.SetStringPropertyValue(ref m_xDatabase, value);}
		}

		public string UserID
		{
			get{return m_xUserID;}
			set{this.SetStringPropertyValue(ref m_xUserID, value);}
		}

		public string Password
		{
			get{return m_xPassword;}
			set{this.SetStringPropertyValue(ref m_xPassword, value);}
		}

		#endregion
        #region *********************Methods*********************
        /// <summary>
        /// Create OleDbConnection based on internal object properties
        /// </summary>
        /// <returns></returns>
        public OleDbConnection CreateConnection()
        {
            return CreateConnection(this.DatabaseType, this.Server, this.Database, this.UserID, this.Password);
        }
        
        /// <summary>
        /// Create an OleDbConnection by specifying Connection properties
        /// </summary>
        /// <param name="oDBType"></param>
        /// <param name="xServer"></param>
        /// <param name="xDatabase"></param>
        /// <param name="UID"></param>
        /// <param name="PWD"></param>
        /// <returns></returns>
        static OleDbConnection CreateConnection(DatabaseTypes oDBType, string xServer,
                string xDatabase, string UID, string PWD)
        {
            DateTime t0 = DateTime.Now;
            string xCnStr = "";
            switch (oDBType)
            {
                case DatabaseTypes.Access:
                    xCnStr = "Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        xDatabase + ";User ID=" + UID + ";Password=" + PWD;
                    break;
                case DatabaseTypes.SQLServer:
                    xCnStr = "Provider=SQLOLEDB;Data Source=" + xServer + ";Initial Catalog=" + xDatabase +
                        ";User ID=" + UID + ";Password=" + PWD;
                    break;
                case DatabaseTypes.Oracle:
                    xCnStr = "Provider=OraOLEDB.Oracle;Data Source=" + xServer + 
                        ";User ID=" + UID + ";Password=" + PWD;
                    break;
            }
            Trace.WriteNameValuePairs("xCnStr", xCnStr);

            
            //open connection
            try 
            {
                OleDbConnection oCon = new OleDbConnection(xCnStr);
                oCon.Open();
                LMP.Benchmarks.Print(t0);
                return oCon;
            }

            catch (System.Exception e)
            {
                throw new LMP.Exceptions.DBConnectionException(
                    Resources.GetLangString("Error_CouldNotConnectToDB") + xCnStr, e);
            }
        }
        public System.Data.DataSet GetDataSet(string xSprocName, object[] oParameters)
        {
            return SimpleDataCollection.GetDataSet(this.CreateConnection(), xSprocName, oParameters);
        }
        #endregion
        #region *********************LongIDSimpleDataItem members*********************
        internal override bool IsValid()
		{
			//return true iff required info is present
			return (m_xName != "") && (m_xDatabase != "");
		}

		public override object[] ToArray()
		{
			object[] oProps = new object[7];
			oProps[0] = this.ID;
			oProps[1] = this.Name;
			oProps[2] = this.DatabaseType;
			oProps[3] = this.Server;
			oProps[4] = this.Database;
			oProps[5] = this.UserID;
			oProps[6] = this.Password;
			return oProps;
		}

        internal override object[] GetUpdateQueryParameters()
        {
            object[] oProps = new object[8];
            oProps[0] = this.ID;
            oProps[1] = this.Name;
            oProps[2] = this.DatabaseType;
            oProps[3] = this.Server;
            oProps[4] = this.Database;
            oProps[5] = this.UserID;
            oProps[6] = this.Password;
            oProps[7] = Application.GetCurrentEditTime();
            return oProps;
        }
        internal override object[] GetAddQueryParameters()
		{
			object[] oProps = new object[7];
			oProps[0] = this.Name;
			oProps[1] = this.DatabaseType;
			oProps[2] = this.Server;
			oProps[3] = this.Database;
			oProps[4] = this.UserID;
			oProps[5] = this.Password;
            oProps[6] = Application.GetCurrentEditTime();
			return oProps;
		}
		#endregion
	}

	/// <summary>
	/// contains the methods and properties that manage the MacPac
	/// collection of ExternalConnection - derived from LongIDSimpleDataCollection -
	/// created by Doug Miller - 09/05
	/// </summary>
	public sealed class ExternalConnections: LongIDSimpleDataCollection
	{
		private LongIDUpdateObjectDelegate m_oDel = null;

		#region *********************constructors*********************
		public ExternalConnections(): base(){}
		#endregion
		#region *********************LongIDSimpleDataCollection members*********************
		protected override LongIDUpdateObjectDelegate UpdateObjectDelegate
		{
			get
			{
				if(m_oDel == null)
					m_oDel = new LongIDUpdateObjectDelegate(this.UpdateObject);

				return m_oDel;
			}
		}

		protected override string AddSprocName
		{
			get{return "spExternalConnectionsAdd";}
		}

		protected override string CountSprocName
		{
			get{return "spExternalConnectionsCount";}
		}

		protected override string DeleteSprocName
		{
			get{return "spExternalConnectionsDelete";}
		}

		protected override string ItemFromIDSprocName
		{
			get{return "spExternalConnectionsItemFromID";}
		}

		protected override string LastIDSprocName
		{
			get{return "spExternalConnectionsLastID";}
		}

		protected override string SelectSprocName
		{
			get{return "spExternalConnections";}
		}

		protected override object[] SelectSprocParameters
		{
//			get{return new object[] {DBNull.Value};}
			get{return null;}
		}

		protected override object[] ItemFromIDSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override object[] CountSprocParameters
		{
			get{return this.SelectSprocParameters;}
		}

		protected override string UpdateSprocName
		{
			get{return "spExternalConnectionsUpdate";}
		}

		public override LongIDSimpleDataItem Create()
		{
			return new ExternalConnection();
		}
		#endregion
		#region *********************private functions*********************
		private LongIDSimpleDataItem UpdateObject(object[] oValues)
		{
			ExternalConnection oExternalConnection = new ExternalConnection((int) oValues[0]);
			oExternalConnection.Name = oValues[1].ToString();
			oExternalConnection.DatabaseType = (ExternalConnection.DatabaseTypes)((int)oValues[2]);
			oExternalConnection.Server = oValues[3].ToString();
			oExternalConnection.Database = oValues[4].ToString();
			oExternalConnection.UserID = oValues[5].ToString();
			oExternalConnection.Password = oValues[6].ToString();
			oExternalConnection.IsDirty = false;

			return oExternalConnection;
		}
		#endregion
	}
}
