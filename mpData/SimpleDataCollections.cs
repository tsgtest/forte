using System;
using LMP.Data;
using System.Data.OleDb;
using System.Data;
using System.Collections;

namespace LMP.Data
{

	public delegate LongIDSimpleDataItem LongIDUpdateObjectDelegate(object[] oValues);
	public delegate StringIDSimpleDataItem StringIDUpdateObjectDelegate(object[] oValues);
	public delegate void BeforeLostEditsHandler(object sender, BeforeLostEditsEventArgs e);

	/// <summary>
	/// defines the event args for the BeforeLostEdits event
	/// </summary>
	public class BeforeLostEditsEventArgs: System.EventArgs
	{
		private bool m_bSaveCurrentItem = true;

		public bool SaveCurrentItem
		{
			get{return m_bSaveCurrentItem;}
			set{m_bSaveCurrentItem = value;}
		}
	}

    abstract public class SimpleDataCollection
    {
        protected ArrayList m_oArray;

        abstract protected string SelectSprocName { get;}
        abstract protected string ItemFromIDSprocName { get;}
        abstract protected string CountSprocName { get;}
        abstract protected string DeleteSprocName { get;}
        abstract protected string UpdateSprocName { get;}
        abstract protected string AddSprocName { get;}
        abstract protected object[] SelectSprocParameters { get;}
        abstract protected object[] ItemFromIDSprocParameters { get;}
        abstract protected object[] CountSprocParameters { get;}


        /// <summary>
        /// fills the internal m_oArray if not yet populated
        /// </summary>
        protected virtual void FillInternalArrayIfNecessary()
        {
            //fill internal array if necessary
            if (m_oArray == null)
                m_oArray = GetArray(this.SelectSprocName,
                    this.SelectSprocParameters);
        }


        #region *********************************static members*********************************
        /// <summary>
        /// converts a data reader into an array list containing all fields
        /// </summary>
        /// <param name="oReader"></param>
        /// <returns></returns>
        internal static ArrayList DataReaderToArrayList(OleDbDataReader oReader)
        {
            ArrayList oArray = new ArrayList();

            //move to starting row
            for (int i = 0; oReader.Read(); i++)
            {
                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //append values to array end
                oArray.Insert(i, oValues);
            }

            return oArray;
        }

        /// <summary>
        /// converts a data reader into an array list containing
        /// only fields with specified indexes
        /// </summary>
        /// <param name="oReader"></param>
        /// <param name="aFieldIndexes"></param>
        /// <returns></returns>
        internal static ArrayList DataReaderToArrayList(OleDbDataReader oReader,
            int[] aFieldIndexes)
        {
            ArrayList oArray = new ArrayList();

            //move to starting row
            for (int i = 0; oReader.Read(); i++)
            {
                //get values at current row
                object[] oValues = new object[aFieldIndexes.Length];

                //add value for each specified field index to array
                for (int j = 0; j < aFieldIndexes.Length; j++)
                {
                    oValues[j] = oReader.GetValue(aFieldIndexes[j]);
                }

                //append values to array end
                oArray.Insert(i, oValues);
            }

            return oArray;
        }

        /// <summary>
        /// converts a data reader into an array list containing
        /// only fields with specified indexes
        /// </summary>
        /// <param name="oReader"></param>
        /// <param name="aFieldIndexes"></param>
        /// <returns></returns>
        internal static ArrayList DataReaderToTwoColArrayList(OleDbDataReader oReader,
            int iDisplayFieldIndex, int iValueFieldIndex)
        {
            ArrayList oArray = new ArrayList();

            //move to starting row
            for (int i = 0; oReader.Read(); i++)
            {
                //add value for each specified field index to array
                TwoColumnListObject o = new TwoColumnListObject();
                o.DisplayMember = oReader.GetValue(iDisplayFieldIndex).ToString();
                o.ValueMember = oReader.GetValue(iValueFieldIndex).ToString();

                //append object to array end
                oArray.Insert(i, o);
            }

            return oArray;
        }
        /// <summary>
        /// returns an ArrayList filled with results of specified sproc
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        internal static void AppendToArray(string xSprocName,
            ref ArrayList oArray, object[] oParameters)
        {
            int iStartRow = 0;
            AppendToArray(xSprocName, ref oArray, ref iStartRow, -1, oParameters);
        }


        /// <summary>
        /// appends to the specified ArrayList with the results of the specified sproc
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oArray">ref argument that specifies the array to fill</param>
        /// <param name="iStartRow">ref argument that specifies the first result row to read</param>
        /// <param name="iNumRows"></param>
        /// <param name="oParameters"></param>
        internal static void AppendToArray(string xSprocName,
            ref ArrayList oArray, ref int iStartRow, int iNumRows, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xSprocName",
                xSprocName, "iStartRow", iStartRow, "iNumRows", iNumRows);
            Trace.WriteParameters(oParameters);

            if (oArray == null)
                oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(xSprocName, false, oParameters))
            {
                try
                {
                    //move to starting row
                    for (int i = 0; i < iStartRow; i++)
                    {
                        oReader.Read();
                    }

                    //read until end is reached or specified number of rows 
                    //have been added - read all rows if iNumRows = -1
                    for (int iNumAdded = 0;
                        oReader.Read() && ((iNumAdded < iNumRows) || (iNumRows == -1));
                        iNumAdded++)
                    {
                        //get values at current row
                        object[] oValues = new object[oReader.FieldCount];
                        oReader.GetValues(oValues);

                        //append values to array end
                        oArray.Insert(oArray.Count, oValues);
                    }
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0);
        }


        /// <summary>
        /// executes the specified action sproc - returns the number of records affected
        /// </summary>
        public static int ExecuteActionSproc(string xSprocName, object[] oParameters)
        {
            return ExecuteActionSproc(LocalConnection.ConnectionObject, xSprocName, oParameters);
        }
        public static int ExecuteActionSproc(OleDbConnection oCn, string xSprocName, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSprocName", xSprocName);
            Trace.WriteParameters(oParameters);

            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    //set up command
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSprocName;
                    oCmd.CommandType = CommandType.StoredProcedure;

                    if (oParameters != null)
                    {
                        //add parameters to command
                        foreach (object oParam in oParameters)
                        {
                            OleDbParameter oOleDbParam = new OleDbParameter();
                            oOleDbParam.Value = (oParam == null) ? System.DBNull.Value : oParam;
                            oCmd.Parameters.Add(oOleDbParam);
                        }
                    }

                    //execute command
                    int iRecsAffected = oCmd.ExecuteNonQuery();

                    LMP.Benchmarks.Print(t0, xSprocName);
                    return iRecsAffected;
                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.StoredProcedureException(
                        Resources.GetLangString("Error_StoredProcedureFailed") +
                        xSprocName, e);
                }
            }
        }
        /// <summary>
        /// executes the specified sql statement - returns the number of records affected
        /// </summary>
        public static int ExecuteAction(string xSQL)
        {
            return ExecuteAction(LocalConnection.ConnectionObject, xSQL);
        }
        /// <summary>
        /// executes the specified sql statement - returns the number of records affected
        /// </summary>
        public static int ExecuteAction(OleDbConnection oCn, string xSQL)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSQL", xSQL);

            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    //set up command
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSQL;
                    oCmd.CommandType = CommandType.Text;

                    //execute command
                    int iRecsAffected = oCmd.ExecuteNonQuery();

                    LMP.Benchmarks.Print(t0, xSQL);

                    return iRecsAffected;
                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.StoredProcedureException(
                        Resources.GetLangString("Error_SQLStatementFailed") +
                        xSQL, e);
                }
            }
        }

        /// <summary>
        /// returns a reader containing the results of the specified sproc, with the specified parameters
        /// </summary>
        /// <param name="oCn">the connection to be used to execute the specified sql statement</param>
        /// <param name="xSQL">sql statement to execute</param>
        /// <param name="iRecsAffected">number of records affected</param>
        /// <param name="bSingleRow">specifies whether or not sql statement should be returning a single row or not</param>
        /// <returns></returns>
        public static OleDbDataReader GetResultSet(string xSQL, bool bSingleRow)
        {
            //get connection from connection type
            System.Data.OleDb.OleDbConnection oCn = LocalConnection.ConnectionObject;

            return GetResultSet(oCn, xSQL, bSingleRow);
        }
        /// <summary>
        /// returns a reader containing the results of the specified sproc, with the specified parameters
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static OleDbDataReader GetResultSet(
            System.Data.OleDb.OleDbConnection oCn, string xSQL, bool bSingleRow)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("oCn.ConnectionString", oCn.ConnectionString,
                "xSQL", xSQL, "bSingleRow", bSingleRow);

            OleDbDataReader oReader = null;
            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSQL;
                    oCmd.CommandType = CommandType.Text;

                    //execute command
                    if (bSingleRow == true)
                        oReader = oCmd.ExecuteReader(CommandBehavior.SingleRow);
                    else
                        oReader = oCmd.ExecuteReader(CommandBehavior.SingleResult);

                    LMP.Benchmarks.Print(t0, xSQL);
                    return oReader;
                }

                catch (System.Exception e)
                {
                    if (oReader != null)
                    {
                        oReader.Close();
                        oReader.Dispose();
                    }
                    throw new LMP.Exceptions.StoredProcedureException(
                        Resources.GetLangString("Error_SQLStatementFailed") +
                        xSQL, e);
                }
            }
        }
        public static OleDbDataReader GetResultSet(string xSprocName, bool bSingleRow, object[] oParameters)
        {
            //get connection from connection type
            System.Data.OleDb.OleDbConnection oCn = LocalConnection.ConnectionObject;

            return GetResultSet(oCn, xSprocName, bSingleRow, oParameters);

        }
        /// <summary>
        /// returns a reader containing the results of the specified sproc, with the specified parameters
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static OleDbDataReader GetResultSet(OleDbConnection oCn, string xSprocName, bool bSingleRow, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSprocName",
                xSprocName, "bSingleRow", bSingleRow);
            Trace.WriteParameters(oParameters);

            OleDbDataReader oReader = null;
            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSprocName;
                    oCmd.CommandType = CommandType.StoredProcedure;

                    if (oParameters != null)
                    {
                        //add parameters to command
                        foreach (object oParam in oParameters)
                        {
                            oCmd.Parameters.Add(new OleDbParameter(oParam.ToString(), oParam));
                        }
                    }

                    //execute command
                    if (bSingleRow == true)
                    {
                        oReader = oCmd.ExecuteReader(CommandBehavior.SingleRow);
                    }
                    else
                    {
                        oReader = oCmd.ExecuteReader(CommandBehavior.SingleResult);
                    }

                    LMP.Benchmarks.Print(t0, xSprocName);
                    return oReader;
                }

                catch (System.Exception e)
                {
                    if (oReader != null && !oReader.IsClosed)
                        oReader.Close();
                    throw new LMP.Exceptions.StoredProcedureException(
                        Resources.GetLangString("Error_StoredProcedureFailed") +
                        xSprocName, e);
                }
            }
        }

        /// <summary>
        /// returns the first value of the specified sproc -
        /// connects to the local database
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static object GetScalar(string xSprocName, object[] oParameters)
        {
            return GetScalar(LocalConnection.ConnectionObject, xSprocName, oParameters);
        }
        /// <summary>
        /// returns the first value of the specified sproc
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static object GetScalar(OleDbConnection oCn, string xSprocName, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xSprocName", xSprocName);
            Trace.WriteParameters(oParameters);

            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    //setup command
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSprocName;
                    oCmd.CommandType = CommandType.StoredProcedure;

                    if (oParameters != null)
                    {
                        //add parameters to command
                        foreach (object oParam in oParameters)
                        {
                            OleDbParameter oOleDbParam = new OleDbParameter();
                            oOleDbParam.Value = oParam;
                            oCmd.Parameters.Add(oOleDbParam);
                        }
                    }

                    //execute
                    object oRet = oCmd.ExecuteScalar();

                    LMP.Benchmarks.Print(t0, xSprocName);

                    //execute and return
                    return oRet;

                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.StoredProcedureException(
                        Resources.GetLangString("Error_StoredProcedureFailed") +
                        xSprocName, e);
                }
            }
        }
        /// <summary>
        /// returns the first value of the specified SQL statement -
        /// connects to the local database
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static object GetScalar(string xSQL)
        {
            return GetScalar(LocalConnection.ConnectionObject, xSQL);
        }

        /// <summary>
        /// returns the first value of the specified SQL statement
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static object GetScalar(OleDbConnection oCn, string xSQL)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xSQL", xSQL);

            OleDbCommand oCmd = new OleDbCommand();

            using (oCmd)
            {
                try
                {
                    //setup command
                    oCmd.Connection = oCn;
                    oCmd.CommandText = xSQL;
                    oCmd.CommandType = CommandType.Text;

                    //execute and return
                    object oRet = oCmd.ExecuteScalar();

                    LMP.Benchmarks.Print(t0);
                    return oRet;
                }

                catch (System.Exception e)
                {
                    throw new LMP.Exceptions.SQLStatementException(
                        Resources.GetLangString("Error_SQLStatementFailed") + xSQL, e);
                }
            }
        }

        /// <summary>
        /// returns an xml string containg the contents of the specified DataReader
        /// </summary>
        /// <param name="oReader"></param>
        /// <returns></returns>
        internal static string DataReaderToXML(OleDbDataReader oReader)
        {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            DateTime t0 = DateTime.Now;

            try
            {
                oSB.Append("<Table1>");

                //move to starting row
                for (int i = 0; oReader.Read(); i++)
                {
                    oSB.AppendFormat("<Row{0}>", i);

                    //get values at current row
                    object[] oValues = new object[oReader.FieldCount];
                    oReader.GetValues(oValues);

                    //append values end of xml string
                    for (int j = 0; j < oValues.Length; j++)
                    {
                        oSB.AppendFormat("<Column{0}>{1}</Column{0}>",
                            j, oValues[j]);
                    }

                    oSB.AppendFormat("</Row{0}>", i);
                }

                oSB.Append("</Table1>");
            }
            finally
            {
                oReader.Close();
            }

            LMP.Benchmarks.Print(t0);
            return oSB.ToString();
        }

        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains all fields in resultset
        /// </summary>
        /// <param name="xSQL">sql statement to use</param>
        /// <returns></returns>
        public static ArrayList GetArray(string xSQL)
        {
            System.Data.OleDb.OleDbConnection oCn = LocalConnection.ConnectionObject;

            return GetArray(oCn, xSQL);
        }


        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains all fields in resultset
        /// </summary>
        /// <param name="oCn">database connection to use</param>
        /// <param name="xSQL">sql statement to use</param>
        /// <returns></returns>
        public static ArrayList GetArray(System.Data.OleDb.OleDbConnection oCn, string xSQL)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("oCn.ConnectionString", oCn.ConnectionString, "xSQL", xSQL);

            ArrayList oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(oCn, xSQL, false))
            {

                try
                {
                    //fill array
                    oArray = DataReaderToArrayList(oReader);
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0, xSQL);
            return oArray;
        }


        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains all fields in resultset -
        /// connects to the local database
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetArray(string xSprocName, object[] oParameters)
        {
            return GetArray(LocalConnection.ConnectionObject, xSprocName, oParameters);
        }
        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains all fields in resultset
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetArray(OleDbConnection oCn, string xSprocName, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSprocName", xSprocName);
            Trace.WriteParameters(oParameters);

            ArrayList oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(xSprocName, false, oParameters))
            {
                try
                {
                    //fill array
                    oArray = DataReaderToArrayList(oReader);
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0, xSprocName);
            return oArray;
        }


        /// <summary>
        /// returns an array list containing the results of the specified sql statement,
        /// with the specified parameters - array list contains specified fields
        /// </summary>
        /// <param name="xSQL">sql statement to use</param>
        /// <param name="aFieldIndexes">array of indexes of the fields to retrieve</param>
        /// <returns></returns>
        public static ArrayList GetArray(int[] aFieldIndexes, string xSQL)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSQL", xSQL);

            ArrayList oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(xSQL, false))
            {
                try
                {
                    //fill array
                    oArray = DataReaderToArrayList(oReader, aFieldIndexes);
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0, xSQL);
            return oArray;
        }

        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains specified fields -
        /// connects to the local database
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetTwoColumnArray(int iCol1Index, int iCol2Index, string xSprocName, object[] oParameters)
        {
            return GetTwoColumnArray(LocalConnection.ConnectionObject, iCol1Index, iCol2Index, xSprocName, oParameters);
        }
        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains specified fields -
        /// connects to the local database
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetArray(int[] aFieldIndexes, string xSprocName, object[] oParameters)
        {
            return GetArray(LocalConnection.ConnectionObject, aFieldIndexes, xSprocName, oParameters);
        }
        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains specified fields
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetArray(OleDbConnection oCn, int[] aFieldIndexes, string xSprocName, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSprocName", xSprocName);
            Trace.WriteParameters(oParameters);

            ArrayList oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(oCn, xSprocName, false, oParameters))
            {
                try
                {
                    //fill array
                    oArray = DataReaderToArrayList(oReader, aFieldIndexes);
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0, xSprocName);
            return oArray;
        }
        /// <summary>
        /// returns an array list containing the results of the specified sproc,
        /// with the specified parameters - array list contains specified fields
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="iRecsAffected"></param>
        /// <param name="bSingleRow"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static ArrayList GetTwoColumnArray(OleDbConnection oCn, int iCol1Index, int iCol2Index, string xSprocName, object[] oParameters)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteValues("xSprocName", xSprocName);
            Trace.WriteParameters(oParameters);

            ArrayList oArray = new ArrayList();

            //get results of sproc as a reader
            using (OleDbDataReader oReader = GetResultSet(oCn, xSprocName, false, oParameters))
            {
                try
                {
                    //fill array
                    oArray = DataReaderToTwoColArrayList(oReader, iCol1Index, iCol2Index);
                }
                finally
                {
                    oReader.Close();
                }
            }

            LMP.Benchmarks.Print(t0, xSprocName);
            return oArray;
        }

        /// <summary>
        /// returns a DataSet for the specified sproc, and parameters
        /// </summary>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static System.Data.DataSet GetDataSet(string xSprocName, object[] oParameters)
        {
            OleDbDataAdapter oAdapter = null;
            System.Data.DataSet oDS = null;

            try
            {
                DateTime t0 = DateTime.Now;

                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LocalConnection.ConnectionObject;
                oCmd.CommandText = xSprocName;
                oCmd.CommandType = CommandType.StoredProcedure;

                if (oParameters != null)
                {
                    //add parameters to command
                    foreach (object oParam in oParameters)
                    {
                        oCmd.Parameters.Add(new OleDbParameter(oParam.ToString(), oParam));
                    }
                }

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oDS = new DataSet("SimpleDataCollectionDataSet");
                    oAdapter.Fill(oDS);
                }
                LMP.Benchmarks.Print(t0, xSprocName);

                return oDS;
            }

            catch (System.Exception e)
            {
                throw new LMP.Exceptions.StoredProcedureException(
                    Resources.GetLangString("Error_StoredProcedureFailed") +
                    xSprocName, e);
            }
        }

        /// <summary>
        /// returns a DataSet for the specified connection, sproc, and parameters
        /// </summary>
        /// <param name="oCn"></param>
        /// <param name="xSprocName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        public static System.Data.DataSet GetDataSet(OleDbConnection oCn,
            string xSprocName, object[] oParameters)
        {
            OleDbDataAdapter oAdapter = null;
            System.Data.DataSet oDS = null;

            try
            {
                DateTime t0 = DateTime.Now;

                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = oCn;
                oCmd.CommandText = xSprocName;
                oCmd.CommandType = CommandType.StoredProcedure;

                if (oParameters != null)
                {
                    //add parameters to command
                    foreach (object oParam in oParameters)
                    {
                        oCmd.Parameters.Add(new OleDbParameter(oParam.ToString(), oParam));
                    }
                }

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oDS = new DataSet("SimpleDataCollectionDataSet");
                    oAdapter.Fill(oDS);
                }
                LMP.Benchmarks.Print(t0, xSprocName);

                return oDS;
            }

            catch (System.Exception e)
            {
                throw new LMP.Exceptions.StoredProcedureException(
                    Resources.GetLangString("Error_StoredProcedureFailed") +
                    xSprocName, e);
            }
        }

        /// <summary>
        /// returns a DataSet for the specified sql statement
        /// </summary>
        /// <param name="xSQL"></param>
        /// <returns></returns>
        public static System.Data.DataSet GetDataSet(string xSQL)
        {
            OleDbDataAdapter oAdapter = null;
            System.Data.DataSet oDS = null;

            try
            {
                DateTime t0 = DateTime.Now;

                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LocalConnection.ConnectionObject;
                oCmd.CommandText = xSQL;
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oDS = new DataSet("SimpleDataCollectionDataSet");
                    oAdapter.Fill(oDS);
                }

                LMP.Benchmarks.Print(t0, xSQL);
                return oDS;
            }

            catch (System.Exception e)
            {
                throw new LMP.Exceptions.SQLStatementException(
                    Resources.GetLangString("Error_SQLStatementFailed") + xSQL, e);
            }
        }

        /// <summary>
        /// returns a DataSet for the specified connection and sql statement
        /// </summary>
        /// <param name="oCn"></param>
        /// <param name="xSQL"></param>
        /// <returns></returns>
        public static System.Data.DataSet GetDataSet(OleDbConnection oCn, string xSQL)
        {
            OleDbDataAdapter oAdapter = null;
            System.Data.DataSet oDS = null;

            try
            {
                DateTime t0 = DateTime.Now;

                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = oCn;
                oCmd.CommandText = xSQL;
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oDS = new DataSet("SimpleDataCollectionDataSet");

                    oAdapter.Fill(oDS);
                }
                LMP.Benchmarks.Print(t0, xSQL);
                return oDS;
            }

            catch (System.Exception e)
            {
                throw new LMP.Exceptions.SQLStatementException(
                    Resources.GetLangString("Error_SQLStatementFailed") + xSQL, e);
            }
        }
        #endregion

        /// <summary>
        /// write deletion to the deletions log 
        /// </summary>
        public static void LogDeletions(mpObjectTypes oObjectTypeID, int iObjectID)
        {
            //GLOG 8220: Don't Log Deletions in Local Mode
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                return;
            int iObjectTypeID = (int)oObjectTypeID;

            Trace.WriteParameters("iObjectTypeID", iObjectTypeID, "iObjectID", iObjectID);

            object[] oParams = { iObjectTypeID, iObjectID, Application.GetCurrentEditTime() };

            //execute delete
            int iRecsAffected = SimpleDataCollection.ExecuteActionSproc("spSyncDeletions", oParams);

            if (iRecsAffected == 0)
            {
                //not deleted - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iObjectID);
            }
        }
        /// <summary>
        /// write deletion to the deletions log 
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="xObjectID"></param>
        /// <returns></returns>
        public static void LogDeletions(mpObjectTypes oObjectTypeID, string xObjectID)
        {
            //GLOG 8220: Don't Log Deletions in Local Mode
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                return;

            int iObjectTypeID = (int)oObjectTypeID;

            Trace.WriteParameters("iObjectTypeID", iObjectTypeID, "xObjectID", xObjectID);

            object[] oParams = { iObjectTypeID, xObjectID, Application.GetCurrentEditTime() };

            //execute delete
            int iRecsAffected = SimpleDataCollection.ExecuteActionSproc("spSyncDeletions", oParams);

            if (iRecsAffected == 0)
            {
                //not deleted - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xObjectID);
            }
        }
    }
	/// <summary>
	/// base class for data collections whose items have a long ID
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public abstract class LongIDSimpleDataCollection: SimpleDataCollection,
		ILongIDSimpleDataCollectionReader, ILongIDSimpleDataCollectionWriter
	{
		protected LongIDSimpleDataItem m_oItem;
		public event BeforeLostEditsHandler BeforeLostEdits;
		abstract protected LongIDUpdateObjectDelegate UpdateObjectDelegate{get;}

		abstract protected string LastIDSprocName{get;}

		/// <summary>
		/// returns the collection index of the specified item
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		internal int GetItemIndex(int iID)
		{
			FillInternalArrayIfNecessary();

			object[] oArray =  m_oArray.ToArray();

			//cycle through array items, 
			//looking for ID match in col 1
			for(int i = 0; i < m_oArray.Count; i++)
			{
				object[] oValues =  (object[]) oArray[i];
				if((int) oValues[0] == iID)
				{
					return i;
				}
			}

			return -1;
		}


		/// <summary>
		/// saves the current item dirty and there
		/// are listeners requesting this
		/// </summary>
		protected void SaveCurrentItemIfRequested()
		{
			//check for listeners
			if(this.BeforeLostEdits != null)
			{
				//there are listeners - cycle through each
				//invoking their handlers
				foreach(BeforeLostEditsHandler oHandler in 
					this.BeforeLostEdits.GetInvocationList())
				{
					BeforeLostEditsEventArgs e = new BeforeLostEditsEventArgs();

					//invoke event handler
					oHandler(this, e);

					//handler responded that current item should be saved - save
					if(e.SaveCurrentItem)
					{
						Save(m_oItem);
						return;
					}
				}
			}
		}


		/// <summary>
		/// returns the ID of the last inserted record
		/// </summary>
		/// <returns></returns>
		public virtual int GetLastID()
		{
            object oLastID = GetScalar(this.LastIDSprocName, null);

            return (oLastID != null) ? (int)oLastID : 0;
		}


		public LongIDSimpleDataItem this[int iIndex]
		{
			get{return this.ItemFromIndex(iIndex);}
		}

		#region ILongIDSimpleDataCollectionReader Members

		/// <summary>
		/// returns the number of items in the collection
		/// </summary>
		/// <returns></returns>
		public virtual int Count
		{
			get
			{
				if(m_oArray == null)
				{
					//no array has been retrieved - get count from query
					object oRet = GetScalar(this.CountSprocName, this.CountSprocParameters);

					return (int) oRet;
				}

				else
				{
					//array has been retrieved - get count from array
					return (int) m_oArray.Count;
				}
			}
		}


		/// <summary>
		/// returns the item with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		public virtual LongIDSimpleDataItem ItemFromID(int iID)
		{
			Trace.WriteNameValuePairs("iID", iID);

			//change active item only if ID is different than active item
			if((m_oItem == null) || (m_oItem.ID != iID))
			{
				//save current item if dirty and there are listeners 
				//requesting this - ideally, this should be called
				//after we know that the requested item exists - 
				//unfortunately, we can't save when a DataReader is open
				//so we do it here
				if(m_oItem != null && m_oItem.IsDirty)
				{
					SaveCurrentItemIfRequested();
				}

				//create new array that contains an extra item for ID
				object[] oParams = this.ItemFromIDSprocParameters;
				object[] oParamsMod;
				if(oParams != null)
				{
					oParamsMod = new object[oParams.Length + 1];
					this.ItemFromIDSprocParameters.CopyTo(oParamsMod,0);
				}
				else
				{
					oParamsMod = new object[1];
				}

				oParamsMod[oParamsMod.Length - 1] = iID;

                object[] oValues = null;

                using (OleDbDataReader oReader = GetResultSet(this.ItemFromIDSprocName, true, oParamsMod))
                {
                    try
                    {
                        if (oReader.HasRows)
                        {
                            //get values
                            oReader.Read();
                            oValues = new object[oReader.FieldCount];
                            oReader.GetValues(oValues);
                        }
                        else
                        {
                            //item was not found
                            throw new LMP.Exceptions.NotInCollectionException(
                                LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iID);
                        }
                    }
                    finally
                    {
                        oReader.Close();
                    }
                }

				//update object with values
				m_oItem = this.UpdateObjectDelegate(oValues);
			}

			m_oItem.IsPersisted = true;
			m_oItem.IsDirty = false;
			return m_oItem;
		}


		/// <summary>
		/// returns the item at the specified collection index
		/// </summary>
		/// <param name="Index"></param>
		/// <returns></returns>
		public virtual LongIDSimpleDataItem ItemFromIndex(int iIndex)
		{
			//TODO: consider using a hashtable or string dictionary instead of the 
			//ArrayList, as ItemFromID could then retrieve from a populated hasthtable			
			Trace.WriteNameValuePairs("iIndex", iIndex);
			
			//ensure that index is not less than 0
			if(iIndex < 1)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemAtIndexNotInCollection")," (",iIndex, ")"));

			//fill internal array if necessary
			if(m_oArray == null)
				m_oArray = GetArray(this.SelectSprocName,this.SelectSprocParameters);

			//ensure that index is not beyond upper bound
			if(iIndex > m_oArray.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemAtIndexNotInCollection")," (",iIndex, ")"));

			object[] oValues = (object[]) m_oArray[iIndex - 1];

			//item exists at this index - save current item if dirty
			//and there are listeners requesting this
			if(m_oItem != null && m_oItem.IsDirty)
			{
				SaveCurrentItemIfRequested();
			}
		
			//call UpdateObject method of derived class
			m_oItem = this.UpdateObjectDelegate(oValues);

			m_oItem.IsPersisted = true;
			m_oItem.IsDirty = false;
			return m_oItem;
		}


		/// <summary>
		/// returns the collection as an array - array contains all fields
		/// </summary>
		/// <returns></returns>
		public virtual Array ToArray()
		{
			DateTime t0 = DateTime.Now;

			FillInternalArrayIfNecessary();

			//copy internal array to new array and return -
			//we do this so that the internal array can't be
			//modified by a client object
			Array oArray = new object[m_oArray.Count];
			m_oArray.CopyTo(0, oArray, 0, m_oArray.Count);
            
			LMP.Benchmarks.Print(t0);
			return oArray;
		}


		/// <summary>
		/// returns the collection as an array - array contains only the specified fields
		/// </summary>
		/// <param name="aFieldIndexes"></param>
		/// <returns></returns>
		public virtual Array ToArray(params int[] aFieldIndexes)
		{
            return ToArrayList(aFieldIndexes).ToArray();
		}

        /// <summary>
        /// returns the collection as an ArrayList
        /// </summary>
        /// <returns></returns>
        public ArrayList ToArrayList()
        {
            FillInternalArrayIfNecessary();
            return m_oArray;
        }

        public ArrayList ToArrayList(params int[] aFieldIndexes)
        {
            DateTime t0 = DateTime.Now;
            ArrayList oArray = GetArray(aFieldIndexes,
                this.SelectSprocName, this.SelectSprocParameters);

            LMP.Benchmarks.Print(t0);

            return oArray;
        }

        public ArrayList ToTwoColArrayList(int iCol1Index, int iCol2Index)
        {
            DateTime t0 = DateTime.Now;
            ArrayList oArray = GetTwoColumnArray(iCol1Index, iCol2Index,
                this.SelectSprocName, this.SelectSprocParameters);

            LMP.Benchmarks.Print(t0);

            return oArray;
        }

		/// <summary>
		/// returns this collection as a DataSet
		/// </summary>
		/// <returns></returns>
		public virtual System.Data.DataSet ToDataSet()
		{
			return SimpleDataCollection.GetDataSet(this.SelectSprocName, this.SelectSprocParameters);
		}
		#endregion	

		#region ILongIDSimpleDataCollectionWriter Members

		/// <summary>
		/// returns a new long id simple data item
		/// </summary>
		abstract public LongIDSimpleDataItem Create();

		/// <summary>
		/// deletes the item with the specified ID
		/// </summary>
		/// <param name="ID"></param>
		public virtual void Delete(int iID)
		{
			DateTime t0 = DateTime.Now;

			Trace.WriteParameters("iID", iID);

			object[] oParams = {iID};

			//execute delete
			int iRecsAffected = ExecuteActionSproc(this.DeleteSprocName, oParams);

			if(iRecsAffected == 0)
			{
				//not deleted - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + iID.ToString());
			}
			else
			{
				//delete item from array if necessary
				if(m_oArray != null)
				{
					//get the index to delete
					int iIndex = GetItemIndex(iID);

					Trace.WriteNameValuePairs("iIndex", iIndex);

					//delete if found in array
					if(iIndex > -1)
						m_oArray.RemoveAt(iIndex);
				}

                this.m_oItem = null;
			}
            LogDeletions(iID);

			LMP.Benchmarks.Print(t0);
		}

        /// <summary>
        /// writes the Object ID and Object type to the Deletions table.
        /// </summary>
        /// <param name="ID"></param>
 
        private void LogDeletions(int iObjectID)
        {
            string xObjectName = this.GetType().Name;
            if (xObjectName == "Jurisdictions")
            {
                //this.DeleteSprocName
                string xLevel = this.DeleteSprocName.ToString().Substring(15, 1);
                xObjectName = xObjectName + xLevel;
                
            }
            mpObjectTypes oObjectTypeID = LMP.Data.Application.GetObjectTypeID(xObjectName);

            //don't write log entry for Offices, AttorneyLicenses and People deletions.
            if ((oObjectTypeID != mpObjectTypes.Offices) && 
                (oObjectTypeID != mpObjectTypes.AttorneyLicenses) && (oObjectTypeID != mpObjectTypes.People))
            {
                SimpleDataCollection.LogDeletions(oObjectTypeID, iObjectID);
            }
        }
		
		/// <summary>
		/// deprecated method
		/// </summary>
		/// 
		public virtual void Save()
		{
			//TODO: decide whether or not to get rid of this overload-
			//creates the bug whereby user gets the collection, creates new,
			//edits the properties of new, then save().  this will generate
			//an error, as the internal object is null.
			Save(m_oItem);
		}


		/// <summary>
		/// saves the specified item
		/// </summary>
		/// <param name="oItem"></param>
		public virtual void Save(LongIDSimpleDataItem oItem)
		{
			if(oItem == null)
				//reference to item can't be null - alert
				throw new LMP.Exceptions.NullReferenceException(
					LMP.Resources.GetLangString("Error_ReferenceCantBeNull"));

			Trace.WriteNameValuePairs("oItem.IsDirty", oItem.IsDirty);

			if(oItem.IsDirty)
			{
				Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

				//prevent save and alert if item to be saved supports
				//translation and the current application language is 
				//not US English - translatable items must be edited in English
				if((oItem is TranslatableDataItem) && (Language.CultureID != Language.mpUSEnglishCultureID))
					throw new LMP.Exceptions.ActionException(
						LMP.Resources.GetLangString("Error_CantSaveTranslatableItem"));

				//test if item has valid data to be saved
				if(!oItem.IsValid())
				{
					//some data is not valid - alert
					throw new LMP.Exceptions.DataException(string.Concat(
						LMP.Resources.GetLangString("Error_InvalidOrMissingObjectData"), 
						oItem.GetType(),"-\n", oItem.ToString()));
				}
						
				if(oItem.IsPersisted)
				{
					//item has an ID - it's been saved before - update
					object[] oUpdateParams = oItem.GetUpdateQueryParameters();
                    int i = ExecuteActionSproc(this.UpdateSprocName, oUpdateParams);

					if(m_oArray != null)
					{
						//save changes to array -
						//get index of edited itemn
						int iIndex = GetItemIndex(oItem.ID);

						Trace.WriteNameValuePairs("iIndex", iIndex);

						//update value array if item was found
						if(iIndex > -1)
							m_oArray[iIndex] = oUpdateParams;
					}
				}

				else
				{
					//item has no ID - it's not been previously saved - add
					ExecuteActionSproc(this.AddSprocName,oItem.GetAddQueryParameters());

                    if (oItem.ID == 0)
                    {
                        int iLastID = GetLastID();

                        //item id was not previously assigned- assign now
                        oItem.SetID(iLastID);
                    }

					Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

					if(m_oArray != null)
						//TODO: In the future, we may want to simply null out the 
						//array, forcing it to repopulate - this would put the newly
						//added item at it's 'proper' index, instead of at the end - 
						//append to array -
						m_oArray.Insert(m_oArray.Count,oItem.GetUpdateQueryParameters());
				}

				oItem.IsPersisted = true;
				oItem.IsDirty = false;
			}
		}
		#endregion
	}

	/// <summary>
	/// base class for data collections whose items have a long ID
	/// created by Daniel Fisherman - 07/04
	/// </summary>
	public abstract class StringIDSimpleDataCollection: SimpleDataCollection,
		IStringIDSimpleDataCollectionReader, IStringIDSimpleDataCollectionWriter
	{
		protected StringIDSimpleDataItem m_oItem;
		abstract protected StringIDUpdateObjectDelegate UpdateObjectDelegate{get;}

		public event BeforeLostEditsHandler BeforeLostEdits;

		/// <summary>
		/// returns the collection index of the specified item
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		internal int GetItemIndex(string xID)
		{
			FillInternalArrayIfNecessary();

			object[] oArray =  m_oArray.ToArray();

			//cycle through array items, 
			//looking for ID match in col 1
			for(int i = 0; i < m_oArray.Count; i++)
			{
				object[] oValues =  (object[]) oArray[i];
				if(oValues[0].ToString() == xID)
				{
					return i;
				}
			}

			return -1;
		}


		/// <summary>
		/// saves the current item dirty and there
		/// are listeners requesting this
		/// </summary>
		protected void SaveCurrentItemIfRequested()
		{
			//check for listeners
			if(this.BeforeLostEdits != null)
			{
				//there are listeners - cycle through each
				//invoking their handlers
				foreach(BeforeLostEditsHandler oHandler in 
					this.BeforeLostEdits.GetInvocationList())
				{
					BeforeLostEditsEventArgs e = new BeforeLostEditsEventArgs();

					//invoke event handler
					oHandler(this, e);

					//handler responded that current item should be saved - save
					if(e.SaveCurrentItem)
					{
						Save(m_oItem);
						return;
					}
				}
			}
		}

		public StringIDSimpleDataItem this[int iIndex]
		{
			get{return this.ItemFromIndex(iIndex);}
		}

		public StringIDSimpleDataItem this[string xID]
		{
			get{return this.ItemFromID(xID);}
		}

		#region IStringIDSimpleDataCollectionReader Members

		/// <summary>
		/// returns the number of items in the collection
		/// </summary>
		/// <returns></returns>
		public virtual int Count
		{
			get
			{
				DateTime t0 = DateTime.Now;

				if(m_oArray == null)
				{
					//no array has been retrieved - get count from query
					object oRet = GetScalar(this.CountSprocName, this.CountSprocParameters);

					LMP.Benchmarks.Print(t0);
					return (int) oRet;
				}

				else
				{
					//array has been retrieved - get count from array
					LMP.Benchmarks.Print(t0);
					return (int) m_oArray.Count;
				}
			}
		}


		/// <summary>
		/// returns the item with the specified ID
		/// </summary>
		/// <param name="iID"></param>
		/// <returns></returns>
		public virtual StringIDSimpleDataItem ItemFromID(string xID)
		{
			Trace.WriteNameValuePairs("xID", xID);

			DateTime t0 = DateTime.Now;

			//change active item only if ID is different than active item
			if((m_oItem == null) || (m_oItem.ID != xID))
			{
				//save current item if dirty and there are listeners 
				//requesting this - ideally, this should be called
				//after we know that the requested item exists - 
				//unfortunately, we can't save when a DataReader is open
				//so we do it here
				if(m_oItem != null && m_oItem.IsDirty)
				{
					SaveCurrentItemIfRequested();
				}

				//create new array that contains extra items for IDs
                object[] oParams = this.ItemFromIDSprocParameters;
                object[] oParamsMod;

				if(oParams != null)
				{
					oParamsMod = new object[oParams.Length + 2];
					this.ItemFromIDSprocParameters.CopyTo(oParamsMod,0);
				}
				else
				{
					oParamsMod = new object[2];
				}

				//split ID into 2 integers
				int iID1;
				int iID2;

				Application.SplitID(xID, out iID1, out iID2);

				//add IDs as final two parameters
				oParamsMod[oParamsMod.Length - 2] = iID1;
				oParamsMod[oParamsMod.Length - 1] = iID2;

                object[] oValues = null;

                using (OleDbDataReader oReader = GetResultSet(this.ItemFromIDSprocName, true, oParamsMod))
                {
                    try
                    {
                        if (oReader.HasRows)
                        {
                            //get values
                            oReader.Read();
                            oValues = new object[oReader.FieldCount];
                            oReader.GetValues(oValues);
                        }
                        else
                        {
                            //item was not found
                            throw new LMP.Exceptions.NotInCollectionException(
                                LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
                        }
                    }
                    finally
                    {
                        oReader.Close();
                    }
                }

				//update object with values
				m_oItem = this.UpdateObjectDelegate(oValues);
			}

			LMP.Benchmarks.Print(t0);

			m_oItem.IsPersisted = true;
			m_oItem.IsDirty = false;
			return m_oItem;
		}


		/// <summary>
		/// returns the item at the specified collection index
		/// </summary>
		/// <param name="Index"></param>
		/// <returns></returns>
		public virtual StringIDSimpleDataItem ItemFromIndex(int iIndex)
		{
			//TODO: consider using a hashtable or string dictionary instead of the 
			//ArrayList, as ItemFromID could then retrieve from a populated hasthtable
			DateTime t0 = DateTime.Now;
			
			Trace.WriteNameValuePairs("iIndex", iIndex);
			
			//ensure that index is not less than 0
			if(iIndex < 1)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemAtIndexNotInCollection")," (",iIndex, ")"));

			//fill internal array if necessary
			if(m_oArray == null)
				m_oArray = GetArray(this.SelectSprocName,this.SelectSprocParameters);

			//ensure that index is not beyond upper bound
			if(iIndex > m_oArray.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					string.Concat(LMP.Resources.GetLangString(
					"Error_ItemAtIndexNotInCollection")," (",iIndex, ")"));

			object[] oValues = (object[]) m_oArray[iIndex - 1];

			//item exists at this index - save current item if dirty
			//and there are listeners requesting this
			if(m_oItem != null && m_oItem.IsDirty)
			{
				SaveCurrentItemIfRequested();
			}
		
			//call UpdateObject method of derived class
			m_oItem = this.UpdateObjectDelegate(oValues);

			LMP.Benchmarks.Print(t0);

			m_oItem.IsPersisted = true;
			m_oItem.IsDirty = false;
			return m_oItem;
		}


		/// <summary>
		/// returns the collection as an array - array contains all fields
		/// </summary>
		/// <returns></returns>
		public virtual Array ToArray()
		{
			DateTime t0 = DateTime.Now;

			FillInternalArrayIfNecessary();

			//copy internal array to new array and return -
			//we do this so that the internal array can't be
			//modified by a client object
			Array oArray = new object[m_oArray.Count];
			m_oArray.CopyTo(0, oArray, 0, m_oArray.Count);

			LMP.Benchmarks.Print(t0);
			return oArray;
		}


		/// <summary>
		/// returns the collection as an array - array contains only the specified fields
		/// </summary>
		/// <param name="aFieldIndexes"></param>
		/// <returns></returns>
		public virtual Array ToArray(params int[] aFieldIndexes)
		{
			DateTime t0 = DateTime.Now;
			ArrayList oArray = GetArray(aFieldIndexes,
				this.SelectSprocName, this.SelectSprocParameters);

			LMP.Benchmarks.Print(t0);

			return oArray.ToArray();
		}

		/// <summary>
		/// returns this collection as a DataSet
		/// </summary>
		/// <returns></returns>
		public virtual System.Data.DataSet ToDataSet()
		{
			return SimpleDataCollection.GetDataSet(this.SelectSprocName, this.SelectSprocParameters);
		}
		#endregion	

		#region IStringIDSimpleDataCollectionWriter Members

		/// <summary>
		/// returns a new long id simple data item
		/// </summary>
		abstract public StringIDSimpleDataItem Create();

		/// <summary>
		/// deletes the item with the specified ID
		/// </summary>
		/// <param name="ID"></param>
		public virtual void Delete(string xID)
		{
			DateTime t0 = DateTime.Now;

			Trace.WriteParameters("xID", xID);

			object[] oParams = {xID};

			//execute delete
			int iRecsAffected = ExecuteActionSproc(this.DeleteSprocName, oParams);

			if(iRecsAffected == 0)
			{
				//not deleted - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID);
			}
			else
			{
				//delete item from array if necessary
				if(m_oArray != null)
				{
					//get the index to delete
					int iIndex = GetItemIndex(xID);

					Trace.WriteNameValuePairs("iIndex", iIndex);

					//delete if found in array
					if(iIndex > -1)
						m_oArray.RemoveAt(iIndex);
				}

                m_oItem = null;
			}

            this.LogDeletions(xID);
			LMP.Benchmarks.Print(t0);
		}
        /// <summary>
        /// writes the Object ID and Object type to the Deletions table.
        /// </summary>
        /// <param name="ID"></param>
        private void LogDeletions(string xObjectID)
        {
            string xObjectName = this.GetType().Name;
            mpObjectTypes oObjectTypeID = LMP.Data.Application.GetObjectTypeID(xObjectName);

            SimpleDataCollection.LogDeletions(oObjectTypeID, xObjectID);
        }
		
		/// <summary>
		/// deprecated method
		/// </summary>
		/// 
		public virtual void Save()
		{
			//TODO: decide whether or not to get rid of this overload-
			//creates the bug whereby user gets the collection, creates new,
			//edits the properties of new, then save().  this will generate
			//an error, as the internal object is null.
			Save(m_oItem);
		}


		/// <summary>
		/// saves the specified item
		/// </summary>
		/// <param name="oItem"></param>
		public virtual void Save(StringIDSimpleDataItem oItem)
		{
			if(oItem == null)
				//reference to item can't be null - alert
				throw new LMP.Exceptions.NullReferenceException(
					LMP.Resources.GetLangString("Error_ReferenceCantBeNull"));

			Trace.WriteNameValuePairs("oItem.IsDirty", oItem.IsDirty);
			
			if(oItem.IsDirty)
			{
				Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

				//test if item has valid data to be saved
				if(!oItem.IsValid())
				{
					//some data is not valid - alert
					throw new LMP.Exceptions.DataException(string.Concat(
						LMP.Resources.GetLangString("Error_InvalidOrMissingObjectData"), 
						oItem.GetType(),"-\n", oItem.ToString()));
				}
						
				if(oItem.IsPersisted)
				{
					//item has an ID - it's been saved before - update
					object[] oUpdateParams = oItem.GetUpdateQueryParameters();
					ExecuteActionSproc(this.UpdateSprocName,oUpdateParams);

					if(m_oArray != null)
					{
						//save changes to array -
						//get index of edited item
						int iIndex = GetItemIndex(oItem.ID);

						Trace.WriteNameValuePairs("iIndex", iIndex);

						//update value array if item was found
						if(iIndex > -1)
							m_oArray[iIndex] = oUpdateParams;
					}
				}

				else
				{
					//item has no ID - it's not been previously saved - add
					ExecuteActionSproc(this.AddSprocName,oItem.GetAddQueryParameters());
					
					Trace.WriteNameValuePairs("oItem.ID", oItem.ID);

					//force array to repopulate - see remarks below
					m_oArray = null;

//					if(m_oArray != null)
//						//TODO: In the future, we may want to simply null out the 
//						//array, forcing it to repopulate - this would put the newly
//						//added item at it's 'proper' index, instead of at the end
//						//append to array - this would also guarantee that we have
//						//the data that's been saved - with the call below, 
//						//it's impossible to retrieve any data that's been inserted
//						//automatically by the query itself - like CreationTime, which
//						//is added using Now() in the SQL itself.
//						m_oArray.Insert(m_oArray.Count,oItem.ToArray());
				}

				oItem.IsPersisted = true;
				oItem.IsDirty = false;
			}
		}
		#endregion

	}

}
