using System;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;

namespace LMP.Data
{
	/// <summary>
	/// contains the methods and properties that define a MacPac User
	/// </summary>
	public class User
	{
		private int m_iID = 0;
		private string m_xSysUserID;
		private Person m_oUserPerson;
		private Person m_oAuthorPerson;
		private int m_iPrimaryID;
		private KeySet m_oFirmSettings;
		private KeySet m_oUserSettings;
        private UserApplicationSettings m_oUserAppSettings;
        private FirmApplicationSettings m_oFirmAppSettings;
        private List<string> m_axAllowedFolders = null; //JTS 3/13/10
        private List<string> m_axDisallowedFolders = null; //JTS 3/13/10

		#region *********************constructors*********************
		public User(LMP.Data.Person oPerson)
		{
			SetPersonObject(oPerson);

            if(m_xSysUserID == null)
			    //get system user id from os
			    m_xSysUserID = Environment.UserName;
            m_axAllowedFolders = new List<string>(); //JTS 3/13/10
            m_axDisallowedFolders = new List<string>(); //JTS 3/13/10
            //subscribe
			SubscribeToEvents();
		}
		#endregion

		#region *********************properties*********************

		public string Name
		{
			get{return m_oUserPerson.FullName;}
		}

		public int ID
		{
			get{return m_iID;}
		}

		public Person PersonObject
		{
			get{return m_oUserPerson;}
		}

		public int PrimaryID
		{
			get{return m_iPrimaryID;}
		}

		public string SystemUserID
		{
			get{return m_xSysUserID;}
		}

		public Person DefaultAuthor
		{
			get
			{
				if (m_oAuthorPerson == null)
					return m_oUserPerson;
				else
					return m_oAuthorPerson;
			}
			set{m_oAuthorPerson = value;}
		}

		public Office Office
		{
			get{return m_oUserPerson.GetOffice();}
		}

        public UserApplicationSettings UserSettings
        {
            get
            {
                if (this.m_oUserAppSettings == null)
                    this.m_oUserAppSettings = new UserApplicationSettings(this.ID);

                return this.m_oUserAppSettings;
            }
        }

        public FirmApplicationSettings FirmSettings
        {
            get
            {
                if (this.m_oFirmAppSettings == null)
                    this.m_oFirmAppSettings = new FirmApplicationSettings(this.ID);

                return this.m_oFirmAppSettings;
            }
        }
        /// <summary>
        /// returns true iff the user is a content designer
        /// </summary>
        public bool IsContentDesigner
        {
            get { return Person.IsContentDesigner(this.ID); }
        }

        /// <summary>
        /// GLOG : 3138 : JAB
        /// Get the number of User content items for this user.
        /// </summary>
        public int NumberOfOwnedSegments
        {
            get
            {
                try
                {
                    UserSegmentDefs oSegs = 
                        new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, this.ID);
                    return oSegs.Count;
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }

                return 0;
            }
        }

        /// <summary>
        /// GLOG : 3138 : JAB
        /// Get the maximum number of user content allowed.
        /// </summary>
        public int OwnedSegmentLimit
        {
            get
            {
                return this.FirmSettings.UserContentLimit;
            }
        }

        /// <summary>
        /// GLOG : 3138 : JAB
        /// Get the number of user content that will trigger warning the
        /// user that they are approaching the number of user content limit.
        /// </summary>
        public int OwnedSegmentThreshold
        {
            get
            {
                return this.FirmSettings.UserContentWarningThreshold;
            }
        }

        /// <summary>
        /// GLOG : 5358 : JSW
        /// Get the maximum number of saved data allowed.
        /// </summary>
        public int SavedDataLimit
        {
            get
            {
                return this.FirmSettings.SavedDataLimit;
            }
        }

        /// <summary>
        /// GLOG : 5358 : JSW
        /// Get the number of saved data that will trigger warning the
        /// user that they are approaching the number of saved data limit.
        /// </summary>
        public int SavedDataThreshold
        {
            get
            {
                return this.FirmSettings.SavedDataWarningThreshold;
            }
        }

		#endregion

		#region *********************methods*********************

		/// <summary>
		/// returns the value of the firm app key with the specified name
		/// </summary>
		/// <param name="xKeyName"></param>
		/// <returns></returns>
		public string GetFirmAppKey(string xKeyName)
		{
			return GetFirmAppKeys().GetValue(xKeyName);
		}

		private void SetPersonObject(LMP.Data.Person oPerson)
		{
			int iID1;
			int iID2;

			//object
			m_oUserPerson = oPerson;

			//id
			LocalPersons.SplitID(m_oUserPerson.ID, out iID1, out iID2);
			m_iID = iID1;

			//primary id
			if (m_oUserPerson.LinkedPersonID == 0)
			{
				//this person's id is the primary id
				m_iPrimaryID = m_iID;
                //GLOG 4838: Set DefaultAuthor to Default Office Record Person
                if (m_oUserPerson.DefaultOfficeRecordID > 0)
                {
                    try
                    {
                        this.DefaultAuthor = LocalPersons.GetPersonFromIDs(m_oUserPerson.DefaultOfficeRecordID, 0);
                    }
                    catch { }
                }
			}
			else
			{
				//this is a secondary office person - the
				//linked id is this person's primary id
				m_iPrimaryID = m_oUserPerson.LinkedPersonID;
			}
		}

		/// <summary>
		/// returns the collection of people who can proxy for this user
		/// </summary>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetProxies()
		{
			//execute proxies query
			System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
				"spProxiesForUser", new object[] {m_iPrimaryID});

			//if no proxies exist, return an empty persons collection
			if (oArrayList.Count == 0)
				return new LocalPersons("ID1 is null");

			//build where filter for persons collection
			StringBuilder oSB = new StringBuilder("ID1 IN (");

			//append array items to string
			foreach(object[] o in oArrayList)
				oSB.AppendFormat("{0},", o[0]);

			//strip trailing comma
			if (oSB.Length > 0)
				oSB.Remove(oSB.Length - 1, 1);

    		Trace.WriteNameValuePairs("Proxies", oSB);

			oSB.Append(") AND ID2=0");

            return new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase, oSB.ToString());
		}

		/// <summary>
		/// returns collection of people for whom this user can proxy
		/// </summary>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetProxiableUsers()
		{
			//execute proxies query
			System.Collections.ArrayList oArrayList = SimpleDataCollection.GetArray(
				"spUsersForProxy", new object[] {m_iPrimaryID});

			//if no proxy rights exist, return an empty persons collection
			if (oArrayList.Count == 0)
				return new LocalPersons("ID1 is null");

			//build where filter for persons collection
			StringBuilder oSB = new StringBuilder("ID1 IN (");

			//append array items to string
			foreach(object[] o in oArrayList)
				oSB.AppendFormat("{0},", o[0]);

			//strip trailing comma
			if (oSB.Length > 0)
				oSB.Remove(oSB.Length - 1, 1);

			Trace.WriteNameValuePairs("Proxiable Users", oSB);

			oSB.Append(") AND ID2=0");

			return new LocalPersons(mpPeopleListTypes.AllPeople, m_iPrimaryID, oSB.ToString());
		}

		/// <summary>
		/// adds the specified person as an alias of the user
		/// </summary>
		/// <param name="oPerson"></param>
		public void AddAlias(LMP.Data.Person oPerson)
		{
			//raise exception if person is a user
			if (oPerson.SystemUserID != "")
			{
				throw new LMP.Exceptions.ValueException(Resources.GetLangString
					("Error_AddUserAlias") + oPerson.ToString());
			}

			//link oPerson to the user
			oPerson.LinkedPersonID = m_iPrimaryID;

			//get an empty people collection - we need a collection to add the alias, but
			//we want an empty one for speed considerations
			LocalPersons oPersons = new LocalPersons("ID1 is null");

			//add as person
			oPersons.Save(oPerson);
		}

		/// <summary>
		/// allows person with ID=iPersonID to proxy for this user
		/// </summary>
		/// <param name="iPersonID"></param>
		public void GiveProxyRights(int iPersonID)
		{
    		Trace.WriteNameValuePairs("iPersonID", iPersonID);

			int iRecs = SimpleDataCollection.ExecuteActionSproc(
				"spProxyAdd", new object[] {m_iPrimaryID, iPersonID, Application.GetCurrentEditTime()});

			if (iRecs == 0)
			{
				throw new LMP.Exceptions.StoredProcedureException(Resources.GetLangString
					("Error_GiveProxyRights") + iPersonID.ToString());
			}
		}

		/// <summary>
		/// removes the specified person as a proxy for the user
		/// </summary>
		/// <param name="iPersonID"></param>
		public void RemoveProxyRights(int iPersonID)
		{
			Trace.WriteNameValuePairs("iPersonID", iPersonID);

			int iRecs = SimpleDataCollection.ExecuteActionSproc(
				"spProxyDelete", new object[] {m_iPrimaryID, iPersonID});

			//write deletion to log
			string xID = m_iPrimaryID.ToString() + "." + iPersonID.ToString();
			SimpleDataCollection.LogDeletions(mpObjectTypes.Proxies, xID);
            
			if (iRecs == 0)
			{
				throw new LMP.Exceptions.StoredProcedureException(Resources.GetLangString
					("Error_RemoveProxyRights") + iPersonID.ToString());
			}
		}

		/// <summary>
		/// returns true iff user can proxy for the person with ID = lPersonID
		/// </summary>
		/// <param name="iPersonID"></param>
		/// <returns></returns>
		public bool CanProxyFor(int iPersonID)
		{
			Trace.WriteNameValuePairs("iPersonID", iPersonID);

			//user can always proxy for himself
			if (iPersonID == m_iPrimaryID)
				return true;

			//get collection of people that user can proxy for
			LocalPersons oPersons = this.GetProxiableUsers();

			//return whether specified person is in collection
			return oPersons.IsInCollection(iPersonID);
		}

		/// <summary>
		///returns true iff the current system user is
		///proxying for the current MacPac user
		/// </summary>
		/// <returns></returns>
		public bool IsProxiedFor()
		{
			return (m_xSysUserID != this.PersonObject.SystemUserID);
		}

		/// <summary>
		/// returns this user's private authors
		/// </summary>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetPrivateAuthors()
		{
			return new LocalPersons(mpPeopleListTypes.Private, m_iPrimaryID);
		}

		/// <summary>
		/// returns complete author list for this user
		/// </summary>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetAuthors()
		{
			return new LocalPersons(mpTriState.True, mpPeopleListTypes.AllPeople, m_iPrimaryID, UsageStates.OfficeActive);
		}

		/// <summary>
		/// returns a delimited string containing those group ids to which user belongs
		/// </summary>
		/// <returns></returns>
		public string GetGroupIDs()
		{
			return this.PersonObject.GetGroupIDs();
		}

		/// <summary>
		/// returns aliases for this user
		/// </summary>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetAliases()
		{
			//set where filter
			return new LocalPersons(System.String.Concat("(LinkedPersonID=", m_iPrimaryID.ToString(),
				@") AND (UserID is Null OR UserID="""")"));
		}

        /// <summary>
        /// fills a collection of user's preferences of the specified Type and Scope
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xScopeID"></param>
        /// <returns></returns>
        public LMP.Data.KeySet GetPreferenceSet(LMP.Data.mpPreferenceSetTypes iType, string xScopeID)
        {
            Trace.WriteNameValuePairs("iType", iType, "xScopeID", xScopeID);
            return this.PersonObject.PreferenceSet(iType, xScopeID);
        }

		/// <summary>
		/// fills a collection of all user application settings for this user
		/// </summary>
		/// <returns></returns>
		public LMP.Data.KeySet GetUserAppKeys()
		{
			Trace.WriteNameValuePairs("m_iPrimaryID", m_iPrimaryID);
			if (m_oUserSettings == null)
				m_oUserSettings = KeySet.GetUserAppKeys(m_iPrimaryID);
			return m_oUserSettings;
		}

		/// <summary>
		/// fills a collection of all firm application settings for this user
		/// </summary>
		/// <returns></returns>
		public LMP.Data.KeySet GetFirmAppKeys()
		{
			Trace.WriteNameValuePairs("m_iPrimaryID", m_iPrimaryID);
			if (m_oFirmSettings == null)
				m_oFirmSettings = KeySet.GetFirmAppKeys(m_iPrimaryID);
			return m_oFirmSettings;
		}

		/// <summary>
		/// returns true if user is a member of the specified group
		/// </summary>
		/// <param name="iGroupID"></param>
		/// <returns></returns>
		public bool IsMemberOf(int iGroupID)
		{
			Trace.WriteNameValuePairs("iGroupID", iGroupID);

			//convert id to string and append commas to beginning and end
			string xID = System.String.Concat(",", iGroupID.ToString(), ",");

			//get comma-delimited list of groups to which user belongs and
			//append commas to beginning and end
			string xGroups = System.String.Concat(",", this.GetGroupIDs(), ",");

			Trace.WriteNameValuePairs("xGroups", xGroups);

			//return whether id is in list
			return (xGroups.IndexOf(xID) != -1);
		}

        /// <summary>
        /// returns true if xCountry is user's country or alias
        /// </summary>
        /// <param name="xCountry"></param>
        /// <returns></returns>
        public bool IsUserCountry(string xCountry)
        {
            //GLOG : 8031 : ceh
            Trace.WriteNameValuePairs("xCountry", xCountry);

            string xUserOffice = m_oUserPerson.GetOffice().GetProperty("Country").ToString();

            Trace.WriteNameValuePairs("xUserOffice", xUserOffice);

            //GLOG : 8175 : ceh
            //user office is missing
            if (xUserOffice == "")
                return false;

            FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(ForteConstants.mpFirmRecordID);

            if (xCountry.ToUpper() == xUserOffice.ToUpper())
                return true;
            else if (xCountry != "")
            {
                //check Aliases - test for existing entry in keyset
                string xAliases = null;

                xAliases = oFirmSettings.CountryAliases;

                if (xAliases == null)
                    xAliases = "";

                string[] aAliases = xAliases.Split('�');
                
                for (int i = 0; i < aAliases.Length; i = i + 2)
                {
                    if (aAliases[i] == xUserOffice)
                    {
                        if (aAliases[i + 1] == xCountry)
                            return true;
                    }
                }
            }
            return false;
        }

		/// <summary>
		/// returns user's default authors for specified object type
		/// </summary>
		/// <param name="iObjectTypeID"></param>
		/// <returns></returns>
		public LMP.Data.LocalPersons GetDefaultAuthors(int iObjectTypeID)
		{
			string xIDs = "";

			Trace.WriteNameValuePairs("iObjectTypeID", iObjectTypeID);

			//get list of ids
			try
			{
				xIDs = GetDefaultAuthorIDs(iObjectTypeID);
			}
			catch{}

			Trace.WriteNameValuePairs("xIDs", xIDs);

			if (xIDs != "")
				//get people collection
				return LocalPersons.GetPeopleFromIDString(xIDs);
			else
				//return empty collection of people
				return new LocalPersons("ID1 is null");
		}

		/// <summary>
		/// returns list of default author ids for specified object type
		/// </summary>
		/// <param name="iObjectTypeID"></param>
		/// <returns></returns>
		public string GetDefaultAuthorIDs(int iObjectTypeID)
		{
			Trace.WriteNameValuePairs("iObjectTypeID", iObjectTypeID);

			return KeySet.GetKeyValue("DefaultAuthors", mpKeySetTypes.UserTypePref,
				iObjectTypeID.ToString(), m_iID);
		}

		private void SubscribeToEvents()
		{
			LMP.Data.KeySet.KeySetChanged += new LMP.Data.KeySetChangeHandler(KeySetChanged);
		}

		/// <summary>
		/// updates privately held settings objects after settings have been edited
		/// </summary>
		/// <param name="oKS"></param>
		/// <param name="e"></param>
		private void KeySetChanged(object oKS, LMP.Data.KeySetChangeEventArgs e)
		{
			//do only if edited set belongs to this user
			if (e.KeySetObject.EntityID == LocalPersons.GetFormattedID(m_iID))
			{
				if (e.KeySetObject.KeySetType == mpKeySetTypes.FirmApp)
					m_oFirmSettings = e.KeySetObject;
				else if (e.KeySetObject.KeySetType == mpKeySetTypes.UserApp)
					m_oUserSettings = e.KeySetObject;
			}
		}
        /// <summary>
        /// JTS 3/13/10: returns whether User has permission to access the specified Admin Folder
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public bool HasPermissionForFolder(string xFolderID)
        {
            //Check if Folder is already in allowed or disallowed list to
            //avoid running additional query
            if (m_axAllowedFolders.Contains(xFolderID))
                return true;
            else if (m_axDisallowedFolders.Contains(xFolderID))
                return false;

            string xParentID = xFolderID;
            while (xParentID != "" && xParentID != "0")
            {
                //If Permission has been removed for any Folder from starting point 
                //up to Top level, user should not have access
                if (!AdminPermissions.FolderPermissionExists(int.Parse(xParentID), this.ID))
                {
                    m_axDisallowedFolders.Add(xFolderID);
                    return false;
                }
                //Get next Parent Folder
                string[] xDetails = Folder.GetFolderDetails(xParentID);
                xParentID = xDetails[0];
            }
            //User has permission to all levels
            m_axAllowedFolders.Add(xFolderID);
            return true;
        }
        /// <summary>
        /// JTS 3/13/10: returns whether the user has Permission to any folder containing the specified Admin Segment
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public bool HasPermissionForSegment(string xSegmentID)
        {
            return Folder.GetSegmentParentFolderIds(@"A" + xSegmentID).GetLength(0) > 0;
        }
		#endregion

		#region *********************static members*********************
		internal static int SystemUserPersonID(string xSystemUserID)
		{
			try
			{
				return (int) SimpleDataCollection.GetScalar(
					"spPersonIDFromSystemUserID", new object[]{xSystemUserID});
			}
			catch
			{
                return ForteConstants.mpGuestPersonID;
			}
		}

		internal int SystemUserPersonID()
		{
			return SystemUserPersonID(Environment.UserName);
		}

		/// <summary>
		/// fills oUserPeople with a persons collection containing those MacPac people that have
		/// that system user name - if this collection contains multiple items, each item represents
		/// a different office for the same person
		/// </summary>
		/// <param name="xSystemUser">system name of possible users</param>
		/// <param name="oPossibleUsers">Persons collection of possible users with specified system name</param>
		public static void GetPossibleUsers(string xSystemUser, ref LocalPersons oPossibleUsers)
		{
			Trace.WriteNameValuePairs("xSystemUser", xSystemUser);

			if((xSystemUser != null) ||(xSystemUser !=""))
				oPossibleUsers = new LocalPersons(string.Concat("UserID='", xSystemUser, "'"));
			else
				//return empty collection of users
				oPossibleUsers = new LocalPersons("ID1 Is Null");
		}

		/// <summary>
		/// populates the supplied collection of all users
		/// </summary>
		/// <param name="oPossibleUsers"></param>
		public static void GetUsers(ref LocalPersons oUsers)
		{
			oUsers = new LocalPersons("UserID Is Not Null and LinkedPersonID = 0");
		}

        /// <summary>
        /// returns a list of DisplayNames and IDs for MacPac 10 users
        /// </summary>
        /// <returns></returns>
        public static DataTable GetNetworkUserList()
        {
            DataTable oNetworkPeopleDT = new DataTable();

            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                string xSQL = "SELECT DisplayName, ID1 FROM PeoplePublic WHERE UserID Is Not Null  And UserID <> '''' and LinkedPersonID = 0 And UsageState=1 ORDER BY DisplayName";
                OleDbCommand oCmd = new OleDbCommand(xSQL, LMP.Data.LocalConnection.ConnectionObject);
                oCmd.CommandType = CommandType.Text;
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                oAdapter.Fill(oNetworkPeopleDT);
            }
            else
            {
                using (NetworkConnection oCn = new NetworkConnection())
                {
                    oCn.ConnectToDB();

                    OleDbCommand oCmd = new OleDbCommand("spGetUserNames", oCn.ConnectionObject);
                    oCmd.CommandType = CommandType.StoredProcedure;
                    OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                    oAdapter.Fill(oNetworkPeopleDT);
                }
            }
            return oNetworkPeopleDT;
        }

		#endregion
	}
}
