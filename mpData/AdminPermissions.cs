using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LMP.Data
{
    public class AdminPermissions
    {
        /// <summary>
        /// returns array list containing id and display name of all users who
        /// are authors and who belong to an office or group with permission
        /// to see the specified folder
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <returns></returns>
        public static ArrayList GetFolderPermissionsForAuthorUsers(int iFolderID)
        {
            string xSelectProc;
            object[] oParams = null;
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) mpObjectTypes.ContentFolder, iFolderID});
            if (oArrayList.Count > 0)
                //folder is assigned to entire firm
                xSelectProc = "spPeopleAuthorUsersIDAndDisplayName";
            else
            {
                //folder is not assigned to entire firm
                xSelectProc = "spFolderPermissionsAuthorUsers";
                oParams = new object[] { iFolderID };
            }

            return SimpleDataCollection.GetArray(xSelectProc, oParams);
        }
        /// <summary>
        /// returns array list containing id and display name of all offices
        /// with permission to see the specified folder
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <returns></returns>
        public static ArrayList GetFolderPermissionsForOffices(int iFolderID)
        {
            string xSelectProc;
            object[] oParams = null;
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) mpObjectTypes.ContentFolder, iFolderID});
            if (oArrayList.Count > 0)
                xSelectProc = "spOfficesIDAndDisplayNameOnly";
            else
            {
                xSelectProc = "spFolderPermissionsOffices";
                oParams = new object[] { iFolderID };
            }

            return SimpleDataCollection.GetArray(xSelectProc, oParams);
        }
        /// <summary>
        /// returns array list containing id and display name of all people groups
        /// with permission to see the specified folder
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <returns></returns>
        public static ArrayList GetFolderPermissionsForGroups(int iFolderID)
        {
            string xSelectProc;
            object[] oParams = null;
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) mpObjectTypes.ContentFolder, iFolderID});
            if (oArrayList.Count > 0)
            {
                //Assigned to 'Everyone'
                if (Language.CultureID == Language.mpUSEnglishCultureID)
                    xSelectProc = "spPeopleGroupsIDAndNameOnly";
                else
                {
                    xSelectProc = "spPeopleGroupsIDAndNameOnlyTranslated";
                    oParams = new object[] { Language.CultureID };
                }
            }
            else
            {
                if (Language.CultureID == Language.mpUSEnglishCultureID)
                {
                    xSelectProc = "spFolderPermissionsGroups";
                    oParams = new object[] { iFolderID };
                }
                else
                {
                    xSelectProc = "spFolderPermissionsGroupsTranslated";
                    oParams = new object[] { iFolderID, Language.CultureID };
                }
            }

            return SimpleDataCollection.GetArray(xSelectProc, oParams);
        }
        /// <summary>
        /// returns true if the specified person has permission to the specified folder
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static bool FolderPermissionExists(int iFolderID, int iPersonID)
        {
            //check whether entire firm has permission to folder
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) mpObjectTypes.ContentFolder, iFolderID});
            if (oArrayList.Count > 0)
                return true;

            //check whether specified person is a member of an office or group with
            //permission to folder
            //JTS 3/12/10: Prevent error if no matching records
            object oID = LMP.Data.SimpleDataCollection.GetScalar(
                "spFolderPermissionExists", new object[] { iFolderID, iPersonID });
            if (oID != null)
                return (int)oID > 0;
            else
                return false;
        }
        /// <summary>
        /// returns array list containing id and display name of all offices
        /// with permission to see the specified object
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <returns></returns>
        public static ArrayList GetPermittedOffices(mpObjectTypes iObjectType, int iObjectID)
        {
            string xSelectProc;
            object[] oParams = null;
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) iObjectType, iObjectID});
            if (oArrayList.Count > 0)
                //Assigned to 'Everyone'
                xSelectProc = "spOfficesIDAndDisplayNameOnly";
            else
            {
                xSelectProc = "spObjectPermissionOffices";
                oParams = new object[] { iObjectType, iObjectID };
            }

            return SimpleDataCollection.GetArray(xSelectProc, oParams);
        }
        /// <summary>
        /// returns array list containing id and display name of all people groups
        /// with permission to see the specified object
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <returns></returns>
        public static ArrayList GetPermittedGroups(mpObjectTypes iObjectType, int iObjectID)
        {
            string xSelectProc;
            object[] oParams = null;
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) iObjectType, iObjectID});
            if (oArrayList.Count > 0)
            {
                if (Language.CultureID == Language.mpUSEnglishCultureID)
                    xSelectProc = "spPeopleGroupsIDAndNameOnly";
                else
                {
                    xSelectProc = "spPeopleGroupsIDAndNameOnlyTranslated";
                    oParams = new object[] { Language.CultureID };
                }
            }
            else
            {
                if (Language.CultureID == Language.mpUSEnglishCultureID)
                {
                    xSelectProc = "spObjectPermissionGroups";
                    oParams = new object[] { (int)iObjectType, iObjectID };
                }
                else
                {
                    xSelectProc = "spObjectPermissionGroupsTranslated";
                    oParams = new object[] { (int)iObjectType, iObjectID, Language.CultureID };
                }
            }

            return SimpleDataCollection.GetArray(xSelectProc, oParams);
        }
        /// <summary>
        /// returns true iff everyone is permitted to use the specified object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID"></param>
        /// <returns></returns>
        public static bool IsEveryonePermitted(mpObjectTypes iObjectType, int iObjectID)
        {
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) iObjectType, iObjectID});
            if (oArrayList.Count > 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// returns true if the specified person has permission to the specified object
        /// </summary>
        /// <param name="iFolderID"></param>
        /// <param name="iPersonID"></param>
        /// <returns></returns>
        public static bool IsPersonPermitted(mpObjectTypes iObjectType, int iObjectID, int iPersonID)
        {
            //check whether entire firm has permission to folder
            ArrayList oArrayList = SimpleDataCollection.GetArray("spAssignmentsItemFromIDNew",
                new object[] {(int) mpObjectTypes.Group, ForteConstants.mpFirmRecordID,
                (int) iObjectType, iObjectID});
            if (oArrayList.Count > 0)
                return true;

            //check whether specified person is a member of an office or group with
            //permission to folder
            int iCount = (int)LMP.Data.SimpleDataCollection.GetScalar(
                "spObjectPermissionExists", new object[] { iObjectType, iObjectID, iPersonID });
            return (iCount > 0);
        }
        /// <summary>
        /// Grant specified Group or Office permission to view an Object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID"></param>
        /// <param name="iEntityID"></param>
        public static void GrantPermission(mpObjectTypes iObjectType, int iObjectID, int iEntityID)
        {
            SimpleDataCollection.ExecuteActionSproc("spObjectPermissionGrant", new object[] { (int)iObjectType, iObjectID, iEntityID, Application.GetCurrentEditTime() });
        }
        /// <summary>
        /// Revoke permission of specified Group or Office to view an Object
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <param name="iObjectID"></param>
        /// <param name="iEntityID"></param>
        public static void RevokePermission(mpObjectTypes iObjectType, int iObjectID, int iEntityID)
        {
            //If iEntityID is zero, permissions will be revoked for all Groups and Offices
            object[] oParams = new object[3];
            oParams[0] = iObjectType;
            oParams[1] = iObjectID;
            if (iEntityID == 0)
                oParams[2] = null;
            else
                oParams[3] = iEntityID;
            SimpleDataCollection.ExecuteActionSproc("spObjectPermissionRevoke", oParams);
            //Log deletion of Assignments records
            SimpleDataCollection.LogDeletions(mpObjectTypes.Assignments, ((int)iObjectType).ToString() + "." +
                iObjectID.ToString() + "." + iEntityID.ToString());
        }
    }
}
