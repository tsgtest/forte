﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LMP.Utilities
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Application.Run(new LMP.MacPac.SessionInfoForm(true));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                Application.Exit();
            }
        }
    }
}
