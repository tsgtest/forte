﻿namespace LMP.UserContentManager
{
    partial class frmManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManager));
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tbtnConnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDisconnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblConnection = new System.Windows.Forms.ToolStripLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmbUsers = new System.Windows.Forms.ComboBox();
            this.lblUsers = new System.Windows.Forms.Label();
            this.grdPeople = new System.Windows.Forms.DataGridView();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.tabContent = new System.Windows.Forms.TabPage();
            this.grdUserContent = new System.Windows.Forms.DataGridView();
            this.mnuCommands = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ItemDeleteContent = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemReassignContent = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemUnshareContent = new System.Windows.Forms.ToolStripMenuItem();
            this.tabKeysets = new System.Windows.Forms.TabPage();
            this.grdKeysets = new System.Windows.Forms.DataGridView();
            this.ToolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.TabControl1.SuspendLayout();
            this.tabContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdUserContent)).BeginInit();
            this.mnuCommands.SuspendLayout();
            this.tabKeysets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdKeysets)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.ToolStrip1.CanOverflow = false;
            this.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnConnect,
            this.toolStripSeparator2,
            this.tbtnDisconnect,
            this.toolStripSeparator1,
            this.lblConnection});
            this.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(879, 57);
            this.ToolStrip1.TabIndex = 0;
            this.ToolStrip1.Text = "toolStrip1";
            // 
            // tbtnConnect
            // 
            this.tbtnConnect.AutoSize = false;
            this.tbtnConnect.Image = ((System.Drawing.Image)(resources.GetObject("tbtnConnect.Image")));
            this.tbtnConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnConnect.Name = "tbtnConnect";
            this.tbtnConnect.Size = new System.Drawing.Size(70, 54);
            this.tbtnConnect.Text = "Connect";
            this.tbtnConnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnConnect.ToolTipText = "Connect to Forte database on SQL Server";
            this.tbtnConnect.Click += new System.EventHandler(this.bbtnConnect_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 57);
            // 
            // tbtnDisconnect
            // 
            this.tbtnDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDisconnect.Image")));
            this.tbtnDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDisconnect.Name = "tbtnDisconnect";
            this.tbtnDisconnect.Size = new System.Drawing.Size(70, 54);
            this.tbtnDisconnect.Text = "Disconnect";
            this.tbtnDisconnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnDisconnect.ToolTipText = "Disconnect from SQL Server";
            this.tbtnDisconnect.Click += new System.EventHandler(this.tbtnDisconnect_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 57);
            // 
            // lblConnection
            // 
            this.lblConnection.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblConnection.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnection.Margin = new System.Windows.Forms.Padding(0, 1, 20, 2);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(117, 54);
            this.lblConnection.Text = "(Not connected)";
            this.lblConnection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 57);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmbUsers);
            this.splitContainer1.Panel1.Controls.Add(this.lblUsers);
            this.splitContainer1.Panel1.Controls.Add(this.grdPeople);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(879, 495);
            this.splitContainer1.SplitterDistance = 256;
            this.splitContainer1.TabIndex = 2;
            // 
            // cmbUsers
            // 
            this.cmbUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUsers.FormattingEnabled = true;
            this.cmbUsers.Items.AddRange(new object[] {
            "All",
            "Active Only",
            "Inactive Only"});
            this.cmbUsers.Location = new System.Drawing.Point(77, 5);
            this.cmbUsers.Name = "cmbUsers";
            this.cmbUsers.Size = new System.Drawing.Size(166, 21);
            this.cmbUsers.TabIndex = 1;
            this.cmbUsers.SelectedIndexChanged += new System.EventHandler(this.cmbUsers_SelectedIndexChanged);
            // 
            // lblUsers
            // 
            this.lblUsers.AutoSize = true;
            this.lblUsers.Location = new System.Drawing.Point(5, 8);
            this.lblUsers.Name = "lblUsers";
            this.lblUsers.Size = new System.Drawing.Size(67, 13);
            this.lblUsers.TabIndex = 0;
            this.lblUsers.Text = "Show Users:";
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.AllowUserToResizeRows = false;
            this.grdPeople.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdPeople.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdPeople.Location = new System.Drawing.Point(0, 32);
            this.grdPeople.MultiSelect = false;
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.ReadOnly = true;
            this.grdPeople.RowHeadersVisible = false;
            this.grdPeople.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdPeople.ShowEditingIcon = false;
            this.grdPeople.Size = new System.Drawing.Size(256, 463);
            this.grdPeople.TabIndex = 2;
            this.grdPeople.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdPeople_DataBindingComplete);
            this.grdPeople.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPeople_RowEnter);
            this.grdPeople.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdPeople_KeyDown);
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.tabContent);
            this.TabControl1.Controls.Add(this.tabKeysets);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl1.Location = new System.Drawing.Point(0, 0);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(619, 495);
            this.TabControl1.TabIndex = 2;
            // 
            // tabContent
            // 
            this.tabContent.Controls.Add(this.grdUserContent);
            this.tabContent.Location = new System.Drawing.Point(4, 22);
            this.tabContent.Name = "tabContent";
            this.tabContent.Padding = new System.Windows.Forms.Padding(3);
            this.tabContent.Size = new System.Drawing.Size(611, 469);
            this.tabContent.TabIndex = 1;
            this.tabContent.Text = "User Content";
            this.tabContent.UseVisualStyleBackColor = true;
            // 
            // grdUserContent
            // 
            this.grdUserContent.AllowUserToAddRows = false;
            this.grdUserContent.AllowUserToDeleteRows = false;
            this.grdUserContent.AllowUserToResizeRows = false;
            this.grdUserContent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdUserContent.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdUserContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdUserContent.ContextMenuStrip = this.mnuCommands;
            this.grdUserContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdUserContent.Location = new System.Drawing.Point(3, 3);
            this.grdUserContent.Name = "grdUserContent";
            this.grdUserContent.ReadOnly = true;
            this.grdUserContent.RowHeadersVisible = false;
            this.grdUserContent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdUserContent.ShowEditingIcon = false;
            this.grdUserContent.Size = new System.Drawing.Size(605, 463);
            this.grdUserContent.TabIndex = 1;
            this.grdUserContent.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdUserContent_CellMouseDown);
            // 
            // mnuCommands
            // 
            this.mnuCommands.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ItemDeleteContent,
            this.ItemReassignContent,
            this.ItemUnshareContent});
            this.mnuCommands.Name = "mnuCommands";
            this.mnuCommands.Size = new System.Drawing.Size(232, 70);
            this.mnuCommands.Opening += new System.ComponentModel.CancelEventHandler(this.mnuCommands_Opening);
            // 
            // ItemDeleteContent
            // 
            this.ItemDeleteContent.Name = "ItemDeleteContent";
            this.ItemDeleteContent.Size = new System.Drawing.Size(231, 22);
            this.ItemDeleteContent.Text = "Delete Content";
            this.ItemDeleteContent.Click += new System.EventHandler(this.ItemDeleteContent_Click);
            // 
            // ItemReassignContent
            // 
            this.ItemReassignContent.Name = "ItemReassignContent";
            this.ItemReassignContent.Size = new System.Drawing.Size(231, 22);
            this.ItemReassignContent.Text = "Assign Content to Active User";
            this.ItemReassignContent.Click += new System.EventHandler(this.ItemReassignContent_Click);
            // 
            // ItemUnshareContent
            // 
            this.ItemUnshareContent.Name = "ItemUnshareContent";
            this.ItemUnshareContent.Size = new System.Drawing.Size(231, 22);
            this.ItemUnshareContent.Text = "Remove from Shared Folder";
            this.ItemUnshareContent.Click += new System.EventHandler(this.ItemUnshareContent_Click);
            // 
            // tabKeysets
            // 
            this.tabKeysets.Controls.Add(this.grdKeysets);
            this.tabKeysets.Location = new System.Drawing.Point(4, 22);
            this.tabKeysets.Name = "tabKeysets";
            this.tabKeysets.Size = new System.Drawing.Size(611, 469);
            this.tabKeysets.TabIndex = 3;
            this.tabKeysets.Text = "Preferences";
            this.tabKeysets.UseVisualStyleBackColor = true;
            // 
            // grdKeysets
            // 
            this.grdKeysets.AllowUserToAddRows = false;
            this.grdKeysets.AllowUserToDeleteRows = false;
            this.grdKeysets.AllowUserToResizeRows = false;
            this.grdKeysets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdKeysets.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdKeysets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdKeysets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdKeysets.Location = new System.Drawing.Point(0, 0);
            this.grdKeysets.Name = "grdKeysets";
            this.grdKeysets.ReadOnly = true;
            this.grdKeysets.RowHeadersVisible = false;
            this.grdKeysets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdKeysets.ShowEditingIcon = false;
            this.grdKeysets.Size = new System.Drawing.Size(611, 469);
            this.grdKeysets.TabIndex = 2;
            // 
            // frmManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(879, 552);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.ToolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmManager_Load);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.TabControl1.ResumeLayout(false);
            this.tabContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdUserContent)).EndInit();
            this.mnuCommands.ResumeLayout(false);
            this.tabKeysets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdKeysets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolStrip1;
        private System.Windows.Forms.ToolStripButton tbtnConnect;
        private System.Windows.Forms.ToolStripButton tbtnDisconnect;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel lblConnection;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView grdPeople;
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tabContent;
        private System.Windows.Forms.TabPage tabKeysets;
        private System.Windows.Forms.DataGridView grdUserContent;
        private System.Windows.Forms.DataGridView grdKeysets;
        private System.Windows.Forms.ComboBox cmbUsers;
        private System.Windows.Forms.Label lblUsers;
        private System.Windows.Forms.ContextMenuStrip mnuCommands;
        private System.Windows.Forms.ToolStripMenuItem ItemDeleteContent;
        private System.Windows.Forms.ToolStripMenuItem ItemReassignContent;
        private System.Windows.Forms.ToolStripMenuItem ItemUnshareContent;

    }
}

