﻿namespace LMP.UserContentManager
{
    partial class frmChooseOwner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPeople = new System.Windows.Forms.Panel();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.lblLookup = new System.Windows.Forms.Label();
            this.grdPeople = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlPeople.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPeople
            // 
            this.pnlPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPeople.Controls.Add(this.txtFind);
            this.pnlPeople.Controls.Add(this.lblLookup);
            this.pnlPeople.Controls.Add(this.grdPeople);
            this.pnlPeople.Location = new System.Drawing.Point(0, 0);
            this.pnlPeople.Name = "pnlPeople";
            this.pnlPeople.Size = new System.Drawing.Size(305, 420);
            this.pnlPeople.TabIndex = 0;
            // 
            // txtFind
            // 
            this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFind.Location = new System.Drawing.Point(77, 9);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(220, 20);
            this.txtFind.TabIndex = 5;
            this.txtFind.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFind_KeyUp);
            // 
            // lblLookup
            // 
            this.lblLookup.AutoSize = true;
            this.lblLookup.Location = new System.Drawing.Point(9, 12);
            this.lblLookup.Name = "lblLookup";
            this.lblLookup.Size = new System.Drawing.Size(66, 13);
            this.lblLookup.TabIndex = 4;
            this.lblLookup.Text = "Find Person:";
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.AllowUserToResizeRows = false;
            this.grdPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPeople.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdPeople.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.Location = new System.Drawing.Point(0, 39);
            this.grdPeople.MultiSelect = false;
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.ReadOnly = true;
            this.grdPeople.RowHeadersVisible = false;
            this.grdPeople.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdPeople.ShowEditingIcon = false;
            this.grdPeople.Size = new System.Drawing.Size(305, 379);
            this.grdPeople.TabIndex = 3;
            this.grdPeople.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdPeople_KeyDown);
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelect.Location = new System.Drawing.Point(135, 431);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 23);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "&Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(218, 431);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // frmChooseOwner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 461);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.pnlPeople);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChooseOwner";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select new Content Owner";
            this.Shown += new System.EventHandler(this.frmChooseOwner_Shown);
            this.pnlPeople.ResumeLayout(false);
            this.pnlPeople.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPeople;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.Label lblLookup;
        private System.Windows.Forms.DataGridView grdPeople;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnCancel;
    }
}