using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.UserContentManager
{
    public partial class frmConnectToNetworkDB : Form
    {
        
        public frmConnectToNetworkDB()
        {
            InitializeComponent();
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        public string LoginID
        {
            get { return this.txtLoginID.Text; }
            set { this.txtLoginID.Text = value; }
        }

        public string Password
        {
            get { return this.txtPassword.Text; }
            set { this.txtPassword.Text = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string xMsg = "";
            if (this.txtServer.Text == "")
            {
                xMsg = "Server Name is required information.";
                this.txtServer.Focus();
            }
            else if (this.txtDatabase.Text == "")
            {
                xMsg = "Database Name is required information.";
                this.txtDatabase.Focus();
            }
            else if (this.txtLoginID.Text == "")
            {
                xMsg = "Login ID is required information.";
                this.txtLoginID.Focus();
            }
            else if (this.txtPassword.Text == "")
            {
                xMsg = "Password is required information.";
                this.txtPassword.Focus();
            }
            if (xMsg != "")
            {
                MessageBox.Show(xMsg, this.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Properties.Settings.Default.ServerName = this.txtServer.Text;
            Properties.Settings.Default.DatabaseName = this.txtDatabase.Text;
            Properties.Settings.Default.Save();
        }

        private void frmConnectToNetworkDB_Load(object sender, EventArgs e)
        {
            this.txtServer.Text = Properties.Settings.Default.ServerName;
            this.txtDatabase.Text = Properties.Settings.Default.DatabaseName;

        }
    }
}