﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMP.UserContentManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmManager());
            Application.Exit();
        }
        /// <summary>
        /// Select the first record in grdPeople matching
        /// the text typed into ttxtFindPerson by the user
        /// </summary>
        internal static void SelectMatchingRecord(DataGridView oGrid, string xSearchString, string xColName)
        {
            // GLOG : 6027 : CEH
            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                //search by Display Name
                int iMatchIndex = FindRecord(oGrid, xSearchString, xColName, false);

                if (iMatchIndex > -1)
                {
                    //Match found

                    //Select the appropriate record
                    oGrid.CurrentCell =
                        oGrid.Rows[iMatchIndex].Cells[xColName];
                }
            }
        }
        internal static int FindRecord(DataGridView oGrid, string xSearchString, string xColName, bool bCaseSensitive, bool bUnOrdered)
        {
            if (!bUnOrdered)
                return FindRecord(oGrid, xSearchString, xColName, bCaseSensitive, 0, oGrid.Rows.Count - 1);
            else
                return FindRecordUnOrdered(oGrid, xSearchString, xColName, bCaseSensitive);
        }
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <returns></returns>
        internal static int FindRecord(DataGridView oGrid, string xSearchString, string xColName, bool bCaseSensitive)
        {
            return FindRecord(oGrid, xSearchString, xColName, bCaseSensitive, 0, oGrid.Rows.Count - 1);
        }
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <param name="iCellCount"></param>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        private static int FindRecord(DataGridView oGrid, string xSearchString, string xColName, bool bCaseSensitive, int iLeft, int iRight)
        {
            if (iRight < iLeft)
                //No matches found
                return -1;

            //Get the index of the midpoint of the current
            //range of cells.
            int iMid = ((iRight - iLeft) / 2) + iLeft;

            //Get the text of the midpoint cell.
            string xCurCellText = oGrid.Rows[iMid].Cells[xColName].FormattedValue.ToString();

            if (bCaseSensitive && xCurCellText.StartsWith(xSearchString))
            {
                //Case sensative match found

                //Find the first instance of the string
                do
                {
                    if (iMid == 0)
                        //If the first cell contains the string,
                        //stop testing.
                        return iMid;

                    //Get the text of the previous cell.
                    iMid--;
                    xCurCellText = oGrid.Rows[iMid].Cells[xColName].FormattedValue.ToString();
                }
                while (xCurCellText.StartsWith(xSearchString));

                //Return the index of the first match.
                return iMid + 1;
            }

            else if (!bCaseSensitive && xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()))
            {
                //Non-case sensative match found

                //Find the first instance of the string
                do
                {
                    if (iMid == 0)
                        //The first cell contains the string,
                        //stop testing.
                        return iMid;

                    //Get the text of the previous cell.
                    iMid--;
                    xCurCellText = oGrid.Rows[iMid].Cells[xColName].FormattedValue.ToString();
                }
                while (xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()));

                //Return the index of the first match.
                return iMid + 1;
            }

            else
            {
                //Compare the search string with the text 
                //of the current cell.
                int iCompare = xSearchString.CompareTo(xCurCellText);

                if (iCompare < 0)
                    //Search string is before the current 
                    //cell text alphabetically
                    return FindRecord(oGrid, xSearchString, xColName, bCaseSensitive, iLeft, iMid - 1);

                else
                    //Search string is after the current 
                    //cell text alphabetically
                    return FindRecord(oGrid, xSearchString, xColName, bCaseSensitive, iMid + 1, iRight);

            }
        }

        // GLOG : 6027 : CEH
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <param name="iCellCount"></param>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        private static int FindRecordUnOrdered(DataGridView oGrid, string xSearchString, string xColName, bool bCaseSensitive)
        {
            if (oGrid.Rows.Count == 0)
                //No matches found
                return -1;

            for (int i = 0; i < oGrid.Rows.Count; i++)
            {
                //get text of cell #i
                string xCurCellText = oGrid.Rows[i].Cells[xColName].FormattedValue.ToString();

                if (bCaseSensitive && xCurCellText.StartsWith(xSearchString))
                    //Case sensitive match found
                    //Return the index of the first match.
                    return i;

                else if (!bCaseSensitive && xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()))
                    //match found
                    //Return the index of the first match.
                    return i;
            }

            //not found
            return -1;
        }
        internal static void ShowError(System.Exception oError)
        {
            ShowError(oError, "");
        }
        internal static void ShowError(System.Exception oError, string xExtraText)
        {
            if (xExtraText == "")
            {
                xExtraText = "An Error occurred:";
            }
            xExtraText += "\r\n\r\n";
            MessageBox.Show(xExtraText + oError.HResult.ToString() + " - " + oError.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

    }
}
