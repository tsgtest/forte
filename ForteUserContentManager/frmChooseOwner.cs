﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LMP.UserContentManager
{
    public partial class frmChooseOwner : Form
    {
        SqlConnection m_oCnn = null;
        int m_iPersonID = 0;
        public frmChooseOwner()
        {
            InitializeComponent();
        }
        public SqlConnection Connection
        {
            get
            {
                return m_oCnn;
            }
            set
            {
                m_oCnn = value;
            }
        }
        public int PersonID
        {
            get
            {
                return m_iPersonID;
            }
        }
        private void txtFind_KeyUp(object sender, KeyEventArgs e)
        {
            
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //don't attempt to set current cell
                    //if grids are empty, i.e. there are no records
                    if (grdPeople.Rows.Count == 0)
                        return;
                    grdPeople.Focus();
                }
                else
                    //Select the first record matching
                    //the search string entered by the user
                    Program.SelectMatchingRecord(grdPeople, this.txtFind.Text, "Name");
            }
            catch (System.Exception oE)
            {
                throw oE;
            }

        }

        private void grdPeople_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                {
                    //Select the first record matching
                    //the search string entered by the user
                    Program.SelectMatchingRecord(grdPeople, e.KeyCode.ToString(), "Name");
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }

        }

        private void frmChooseOwner_Shown(object sender, EventArgs e)
        {
            try
            {
                const string xPeopleSQL = "SELECT DISTINCT p.ID1 as [ID], p.DisplayName as [Name], o.DisplayName as [Office] FROM People p INNER JOIN Offices o on p.OfficeID=o.ID " +  
                    "WHERE p.ID2=0 AND p.LinkedPersonID=0 AND p.UsageState=1 ORDER BY p.DisplayName";
                SqlCommand oCmd = new SqlCommand();
                if (m_oCnn == null)
                    return;
                if (m_oCnn.State != ConnectionState.Open)
                    m_oCnn.Open();
                oCmd.Connection = m_oCnn;
                oCmd.CommandText = xPeopleSQL;
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    DataSet oPeopleDS = new DataSet("PeopleDataSet");
                    oAdapter.Fill(oPeopleDS);
                    grdPeople.DataSource = oPeopleDS.Tables[0];
                }
                grdPeople.Columns["ID"].Visible = false;
                grdPeople.Focus();
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdPeople.SelectedRows.Count > 0)
                    m_iPersonID = Int32.Parse(grdPeople.SelectedRows[0].Cells["ID"].Value.ToString());
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }
    }
}
