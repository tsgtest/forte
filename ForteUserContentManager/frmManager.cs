﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Collections;

namespace LMP.UserContentManager
{
    public partial class frmManager : Form
    {
        const string APPNAME = "Forte User Content Manager";
        string m_xServer = "";
        string m_xDB = "";
        string m_xLogin = "";
        string m_xPwd = "";
        bool m_bReadOnly = false;
        bool m_bLoadingPeople = false;
        Random m_oRand = new Random();
        PeopleActiveState m_iState = PeopleActiveState.All;
        SqlTransaction m_oTransaction = null;

        private enum PeopleActiveState
        {
            All = 0,
            Active = 1,
            Inactive = 2
        }
        SqlConnection m_oCnn = null;
        public frmManager()
        {
            InitializeComponent();
        }

        private void frmManager_Load(object sender, EventArgs e)
        {
            this.Text = APPNAME;
            this.splitContainer1.Enabled = false;
            this.TabControl1.Enabled = false;
            this.cmbUsers.SelectedIndex = 0;
            this.TabControl1.TabPages.Remove(tabKeysets);
        }
        private bool ConnectToDB()
        {
            frmConnectToNetworkDB oFormConnect = new frmConnectToNetworkDB();
            oFormConnect.Server = m_xServer;
            oFormConnect.Database = m_xDB;
            oFormConnect.LoginID = m_xLogin;
            oFormConnect.Password = m_xPwd;
            DialogResult oRes = oFormConnect.ShowDialog();
            if (oRes == DialogResult.OK)
            {
                string xConnectionString = "";
                m_xServer = oFormConnect.Server;
                m_xDB = oFormConnect.Database;
                m_xLogin = oFormConnect.LoginID;
                m_xPwd = oFormConnect.Password;

                xConnectionString = "Persist Security Info=false;Integrated Security=false;" +
                    "database=" + m_xDB + ";server=" + m_xServer + @";user id=" + m_xLogin +
                    ";password=" + m_xPwd;

                if (m_oCnn != null && ((m_oCnn.State & ConnectionState.Open) == ConnectionState.Open))
                {
                    m_oCnn.Close();
                }
                m_oCnn = new SqlConnection(xConnectionString);
                try
                {
                    m_oCnn.Open();
                    string xLabel = "Connected to " + m_xDB + " on " + m_xServer;
                    SqlCommand oCmd = new SqlCommand("SELECT permission_name FROM fn_my_permissions (NULL, 'DATABASE') WHERE permission_name='UPDATE'");
                    oCmd.Connection = m_oCnn;
                    object oVal = oCmd.ExecuteScalar();
                    if (oVal == null)
                    {
                        MessageBox.Show("Login does not have Update permissions in database. Records can be viewed only.", APPNAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        m_bReadOnly = true;
                        xLabel = xLabel + " (Read-only mode)";
                    }
                    else
                        m_bReadOnly = false;
                    this.lblConnection.Text = xLabel;
                    this.TabControl1.Visible = true;
                }
                catch (System.Exception oE)
                {
                    //GLOG 8325
                    string xMsg = "Unable to create a SQL Server connection using the specified settings.\r\n\r\nThe following error occurred:";

                    Program.ShowError(oE, xMsg);
                    return false;
                }
                return true;
            }
            else
                return false;
        }

        private void bbtnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConnectToDB())
                {
                    this.splitContainer1.Enabled = true;
                    this.TabControl1.Enabled = true;
                    PopulatePeople(m_iState);
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
                return;
            }

        }
        private void PopulatePeople(PeopleActiveState iState)
        {
            try
            {
                m_bLoadingPeople = true;
                const string xPeopleSQL = "SELECT DISTINCT p.ID1 as [ID], p.DisplayName as [Name], o.DisplayName as [Office], p.UsageState " +
                    "AS [Active] FROM People p INNER JOIN Offices o on p.OfficeID=o.ID INNER JOIN UserSegments u ON u.OwnerID1 = p.ID1 AND u.OwnerID2=p.ID2 WHERE p.ID2=0 AND p.LinkedPersonID=0{0} " + 
                    "UNION SELECT DISTINCT p.ID1 as [ID], p.DisplayName as [Name], o.DisplayName as [Office], p.UsageState " +
                    "AS [Active] FROM People p INNER JOIN Offices o on p.OfficeID=o.ID INNER JOIN VariableSets u ON u.OwnerID1 = p.ID1 AND u.OwnerID2=p.ID2 WHERE p.ID2=0 AND p.LinkedPersonID=0{0} ORDER BY p.DisplayName";
                SqlCommand oCmd = new SqlCommand();
                if (m_oCnn == null)
                    return;
                if (m_oCnn.State != ConnectionState.Open)
                    m_oCnn.Open();
                oCmd.Connection = m_oCnn;
                string xUsageStateSQl = "";
                switch (iState)
                {
                    case PeopleActiveState.Inactive:
                        xUsageStateSQl = " AND p.UsageState=0";
                        break;
                    case PeopleActiveState.Active:
                        xUsageStateSQl = " AND p.UsageState=1";
                        break;
                }
                oCmd.CommandText = string.Format(xPeopleSQL, xUsageStateSQl);
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd);

                using (oAdapter)
                {
                    //create and fill data set using adapter
                    DataSet oPeopleDS = new DataSet("PeopleDataSet");
                    oAdapter.Fill(oPeopleDS);
                    grdPeople.DataSource = oPeopleDS.Tables[0];
                }
                grdPeople.Columns["ID"].Visible = false;
                grdPeople.Columns["Active"].Visible = false;
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
            finally
            {
                m_bLoadingPeople = false;
                if (grdPeople.Rows.Count > 0)
                {
                    LoadContent(grdPeople.Rows[0].Cells["ID"].Value.ToString());
                    grdPeople.Focus();
                }
            }
        }
        private void tbtnDisconnect_Click(object sender, EventArgs e)
        {
            DisconnectFromDB();
        }
        private void DisconnectFromDB()
        {
            if (m_oCnn != null && ((m_oCnn.State & ConnectionState.Open) == ConnectionState.Open))
            {
                m_oCnn.Close();
                m_oCnn = null;
                this.lblConnection.Text = "(Not connected)";
                this.grdPeople.DataSource = null;
                this.grdUserContent.DataSource = null;
                //this.grdSavedData.DataSource = null;
                this.grdKeysets.DataSource = null;
                this.splitContainer1.Enabled = false;

            }
        }

        private void grdPeople_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow oRow in grdPeople.Rows)
                {
                    if (oRow.Cells["Active"].Value.ToString() != "1")
                    {
                        oRow.DefaultCellStyle.BackColor = Color.LightPink;
                    }
                }
            }
            catch { }
        }
        private void grdPeople_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!m_bLoadingPeople && e.RowIndex < grdPeople.Rows.Count)
                {
                    DataGridViewRow oRow = grdPeople.Rows[e.RowIndex];
                    string xID = oRow.Cells["ID"].Value.ToString();
                    LoadUserData(xID);
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }
        private void LoadUserData(string xID)
        {
            LoadContent(xID);
        }
        private void LoadContent(string xID)
        {
            //const string xSQL = "SELECT u.ID1 as [ID1], u.ID2 as [ID2], u.DisplayName as [Content Name], f.Name as [Shared Folder], u.LastEditTime as [Edit Time] FROM UserSegments u " +
            //    "Left Join Folders f on u.SharedFolderID = f.ID WHERE u.OwnerID1={0} AND u.OwnerID2=0 ORDER BY u.DisplayName;";
            const string xSQL = "SELECT u.ID1 as [ID1], u.ID2 as [ID2], u.DisplayName as [Content Name], " +
                "CASE WHEN u.TypeID=97 THEN 'Segment with Data' WHEN u.TypeID=96 THEN 'Segment Packet' ELSE 'Segment' END as [Content Type], " +
                "f.Name as [Shared Folder], u.LastEditTime as [Edit Time] FROM UserSegments u " +
                "Left Join Folders f on u.SharedFolderID = f.ID WHERE u.OwnerID1={0} AND u.OwnerID2=0 UNION " +
                "SELECT v.ID1 as [ID1], v.ID2 as [ID2], v.Name as [Content Name], 'Saved Data' as [Content Type], f.Name as [Shared Folder], v.LastEditTime as [Edit Time] FROM VariableSets v " +
                "Left Join Folders f on v.SharedFolderID = f.ID WHERE v.OwnerID1={0} AND v.OwnerID2=0 ORDER BY [Content Type], u.DisplayName;";
            SqlCommand oCmd = new SqlCommand();
            if (m_oCnn == null)
                return;
            if (m_oCnn.State != ConnectionState.Open)
                m_oCnn.Open();
            oCmd.Connection = m_oCnn;
            oCmd.CommandText = string.Format(xSQL, xID);
            oCmd.CommandType = CommandType.Text;

            //create data adapter from command
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd);
            int iCount = 0;
            using (oAdapter)
            {
                //create and fill data set using adapter
                DataSet oSegmentsDS = new DataSet("SegmentsDataSet");
                oAdapter.Fill(oSegmentsDS);
                grdUserContent.DataSource = oSegmentsDS.Tables[0];
            }
            iCount = grdUserContent.Rows.Count;
            grdUserContent.Columns["ID1"].Visible = false;
            grdUserContent.Columns["ID2"].Visible = false;
            tabContent.Text = "User Content  (" + iCount.ToString() + ")";
            grdUserContent.Columns["Content Name"].Width = grdUserContent.Width / 3;
        }

        private void cmbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (cmbUsers.SelectedIndex)
                {
                    case 1:
                        m_iState = PeopleActiveState.Active;
                        break;
                    case 2:
                        m_iState = PeopleActiveState.Inactive;
                        break;
                    default:
                        m_iState = PeopleActiveState.All;
                        break;
                }
                if (m_oCnn != null && m_oCnn.State == ConnectionState.Open)
                {
                    PopulatePeople(m_iState);
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }

        private void grdUserContent_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

            }
            catch { }
        }

        private void mnuCommands_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (m_bReadOnly)
                {
                    e.Cancel = true;
                    return;
                }
                ItemUnshareContent.Visible = true;
                ItemReassignContent.Visible = true;
                if (grdUserContent.SelectedRows.Count < 1)
                {
                    e.Cancel = true;
                    return;
                }
                if (grdPeople.SelectedRows[0].Cells["Active"].Value.ToString() != "0")
                {
                    this.ItemReassignContent.Visible = false;
                }
                foreach (DataGridViewRow oRow in grdUserContent.SelectedRows)
                {
                    if (oRow.Cells["Shared Folder"].Value.ToString() == "")
                    {
                        this.ItemUnshareContent.Visible = false;
                        return;
                    }
                }
            }
            catch { }
        }

        private void ItemDeleteContent_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedRowCollection oRows = grdUserContent.SelectedRows;
                if (oRows.Count > 0)
                {
                    DialogResult iRet = MessageBox.Show("Delete selected Content?", ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iRet != System.Windows.Forms.DialogResult.Yes)
                    {
                        return;
                    }
                    foreach (DataGridViewRow oRow in oRows)
                    {
                        int iID1 = Int32.Parse(oRow.Cells["ID1"].Value.ToString());
                        int iID2 = Int32.Parse(oRow.Cells["ID2"].Value.ToString());
                        string xType = oRow.Cells["Content Type"].Value.ToString();
                        m_oTransaction = m_oCnn.BeginTransaction("DeleteContent");
                        try
                        {
                            DeleteUserContent(iID1, iID2, xType == "Saved Data", false);
                            grdUserContent.Rows.Remove(oRow);
                            m_oTransaction.Commit();
                        }
                        catch (System.Exception oE)
                        {
                            m_oTransaction.Rollback();
                            Program.ShowError(oE, "An Error occurred attempting to delete records for " + oRow.Cells["Content Name"].Value.ToString() + ".  No changes have been made to the database.");
                        }
                    }
                }

            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }
        private void DeleteUserContent(int iID1, int iID2, bool bVariableSet, bool bSkipContent)
        {
            try
            {
                if (m_oCnn == null || m_bReadOnly)
                {
                    return;
                }
                string xTable = "UserSegments";
                string xObjectType = "99";
                if (bVariableSet)
                {
                    xTable = "VariableSets";
                    xObjectType = "104";
                }
                const string xDELETE_CONTENT_SQL = "DELETE FROM {0} WHERE ID1={1} AND ID2={2};";
                const string xLOG_DELETION_CONTENT_SQL = "INSERT INTO DELETIONS ([ObjectTypeID], [ObjectID], [LastEditTime]) " +
                    "SELECT {0} as [ObjectTypeID], CONCAT({1}, '.' , {2}) as [ObjectID], GETUTCDATE();";
                const string xDELETE_UFM_SQL = "DELETE m FROM UserFolderMembers m INNER JOIN {0} u ON m.TargetObjectID1=u.ID1 AND m.TargetObjectID2=u.ID2 " +
                    "WHERE m.TargetObjectID1={1} AND m.TargetObjectID2={2};";
                const string xLOG_DELETION_UFM_SQL = "INSERT INTO DELETIONS ([ObjectTypeID], [ObjectID], [LastEditTime]) " +
                    "SELECT 107 AS [ObjectTypeID], CONCAT(m.ID1, '.', m.ID2) as [ObjectID], GETUTCDATE() FROM UserFolderMembers m WHERE m.TargetObjectID1={0} and m.TargetObjectID2={1} " +
                    "AND m.TargetObjectTypeID{2}104;";
                const string xDELETE_PERMISSIONS_SQL = "DELETE FROM Permissions WHERE ObjectID1={0} AND ObjectID2={1} AND ObjectTypeID={2};";
                const string xLOG_DELETION_PERMISSIONS_SQL = "INSERT INTO DELETIONS ([ObjectTypeID], [ObjectID], [LastEditTime]) " +
                    "SELECT 640 as [ObjectTypeID], CONCAT({0}, '.' , {1}, '.', {2}) as [ObjectID], GETUTCDATE();";
                if ((m_oCnn.State & ConnectionState.Open) != ConnectionState.Open)
                {
                    m_oCnn.Open();
                }
                try
                {
                    SqlCommand oCmd = new SqlCommand();
                    oCmd.Connection = m_oCnn;
                    oCmd.Transaction = m_oTransaction;
                    //First delete any User Permissions
                    oCmd.CommandText = string.Format(xDELETE_PERMISSIONS_SQL, iID1.ToString(), iID2.ToString(), xObjectType);
                    oCmd.CommandType = CommandType.Text;
                    int iRecs = oCmd.ExecuteNonQuery();
                    if (iRecs > 0)
                    {
                        //If Permissions records were deleted, add Deletions record
                        oCmd.CommandText = string.Format(xLOG_DELETION_PERMISSIONS_SQL, xObjectType, iID1.ToString(), iID2.ToString());
                        oCmd.CommandType = CommandType.Text;
                        oCmd.ExecuteNonQuery();
                    }
                    //Next delete UserFolderMember records
                    //Insert Deletions records first, since there may be more than one related UserFolderMembers record
                    oCmd.CommandText = string.Format(xLOG_DELETION_UFM_SQL, iID1.ToString(), iID2.ToString(), bVariableSet ? "=" : "<>");
                    oCmd.CommandType = CommandType.Text;
                    iRecs = oCmd.ExecuteNonQuery();
                    if (iRecs > 0)
                    {
                        oCmd.CommandText = string.Format(xDELETE_UFM_SQL, xTable, iID1.ToString(), iID2.ToString());
                        oCmd.CommandType = CommandType.Text;
                        oCmd.ExecuteNonQuery();
                    }
                    if (!bSkipContent)
                    {
                        //Now delete actual UserSegments or VariableSets record
                        oCmd.CommandText = string.Format(xDELETE_CONTENT_SQL, xTable, iID1.ToString(), iID2.ToString());
                        oCmd.CommandType = CommandType.Text;
                        iRecs = oCmd.ExecuteNonQuery();
                    }
                    //Create Deletions record for Content
                    oCmd.CommandText = string.Format(xLOG_DELETION_CONTENT_SQL, xObjectType, iID1.ToString(), iID2.ToString());
                    oCmd.CommandType = CommandType.Text;
                    oCmd.ExecuteNonQuery();
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void UnshareContent(int iID1, int iID2, bool bVariableSet)
        {
            try
            {
                if (m_oCnn == null || m_bReadOnly)
                {
                    return;
                }
                string xTable = "UserSegments";
                string xObjectTypeID = "99";
                if (bVariableSet)
                {
                    xTable = "VariableSets";
                    xObjectTypeID = "104";
                }
                const string xUPDATE_SQL = "Update {0} SET SharedFolderID=0, LastEditTime=GETUTCDATE() WHERE ID1={1} AND ID2={2};";
                const string xLOG_DELETION_CONTENT_SQL = "INSERT INTO DELETIONS ([ObjectTypeID], [ObjectID], [LastEditTime]) " +
                    "SELECT {0} as [ObjectTypeID], CONCAT({1}, '.' , {2}) as [ObjectID], GETUTCDATE();";
                if ((m_oCnn.State & ConnectionState.Open) != ConnectionState.Open)
                {
                    m_oCnn.Open();
                }
                SqlCommand oCmd = new SqlCommand();
                oCmd.Connection = m_oCnn;
                oCmd.Transaction = m_oTransaction;
                //First delete any User Permissions
                oCmd.CommandText = string.Format(xUPDATE_SQL, xTable, iID1.ToString(), iID2.ToString());
                oCmd.CommandType = CommandType.Text;
                int iRecs = oCmd.ExecuteNonQuery();
                if (iRecs > 0)
                {
                    oCmd.CommandText = string.Format(xLOG_DELETION_CONTENT_SQL, xObjectTypeID, iID1, iID2);
                    oCmd.CommandType = CommandType.Text;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }

        private void ItemUnshareContent_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedRowCollection oRows = grdUserContent.SelectedRows;
                foreach (DataGridViewRow oRow in oRows)
                {
                    int iID1 = Int32.Parse(oRow.Cells["ID1"].Value.ToString());
                    int iID2 = Int32.Parse(oRow.Cells["ID2"].Value.ToString());
                    string xType = oRow.Cells["Content Type"].Value.ToString();
                    m_oTransaction = m_oCnn.BeginTransaction("UnshareContent");
                    try
                    {
                        UnshareContent(iID1, iID2, xType=="Saved Data");
                        oRow.Cells["Shared Folder"].Value = "";
                        m_oTransaction.Commit();
                    }
                    catch (System.Exception oE)
                    {
                        m_oTransaction.Rollback();
                        Program.ShowError(oE, "An Error occurred attempting to delete records for " + oRow.Cells["Content Name"].Value.ToString() + ".  No changes have been made to the database.");
                    }
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }

        }

        private void grdPeople_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                {
                    //Select the first record matching
                    //the search string entered by the user
                    Program.SelectMatchingRecord(grdPeople, e.KeyCode.ToString(), "Name");
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }

        }

        private void ItemReassignContent_Click(object sender, EventArgs e)
        {
            if (m_oCnn == null || m_bReadOnly)
            {
                return;
            }
            try
            {
                frmChooseOwner oForm = new frmChooseOwner();
                oForm.Connection = m_oCnn;
                DialogResult iRet = oForm.ShowDialog();
                int iPersonID = 0;
                if (iRet == System.Windows.Forms.DialogResult.OK)
                {
                    iPersonID = oForm.PersonID;
                }
                if (iPersonID == 0)
                    return;
                DataGridViewSelectedRowCollection oRows = grdUserContent.SelectedRows;
                foreach (DataGridViewRow oRow in oRows)
                {
                    int iID1 = Int32.Parse(oRow.Cells["ID1"].Value.ToString());
                    int iID2 = Int32.Parse(oRow.Cells["ID2"].Value.ToString());
                    string xType = oRow.Cells["Content Type"].Value.ToString();
                    string xOldUser = grdPeople.SelectedRows[0].Cells["Name"].Value.ToString();
                    m_oTransaction = m_oCnn.BeginTransaction("ReassignContent");
                    try
                    {
                        ReassignContent(iPersonID, xOldUser, iID1, iID2, xType == "Saved Data");
                        grdUserContent.Rows.Remove(oRow);
                        m_oTransaction.Commit();
                    }
                    catch (System.Exception oE)
                    {
                        m_oTransaction.Rollback();
                        Program.ShowError(oE, "An Error occurred attempting to reassign content " + oRow.Cells["Content Name"].Value.ToString() + ".  No changes have been made to the database.");
                    }
                }
            }
            catch (System.Exception oE)
            {
                Program.ShowError(oE);
            }
        }
        private void ReassignContent(int iNewPersonID, string xOldUser, int iID1, int iID2, bool bVariableSet)
        {
            const string xUSERCONTENT_UPDATE_SQL = "UPDATE [{0}] SET [ID1]={1}, [ID2]={2}, [OwnerID1]={3},[OwnerID2]=0,[LastEditTime]=GETUTCDATE() WHERE [ID1]={4} AND [ID2]={5}";
            const string xNEW_FOLDERMEMBER_INSERT_SQL = "INSERT INTO [UserFolderMembers] ([ID1],[ID2],[UserFolderID1],[UserFolderID2],[TargetObjectTypeID],[TargetObjectID1],[TargetObjectID2],[Name],[LastEditTime]) " +
                            "SELECT {0} AS [ID1], {1} AS [ID2] , {2} AS [UserFolderID1], {3} AS [UserFolderID2], {4} AS [TargetObjectTypeID], " + 
                            "u.[ID1] as [TargetObjectID1], u.[ID2] as [TargetObjectID2], u.[{5}] AS [Name], GETUTCDATE() FROM {6} u WHERE u.[ID1]={7} AND u.[ID2]={8};";
            
            try
            {
                int iFolderID1 = 0;
                int iFolderID2 = 0;
                if (GetNewUserFolder(iNewPersonID, xOldUser, out iFolderID1, out iFolderID2))
                {
                    SqlCommand oCmd = new SqlCommand();
                    oCmd.Connection = m_oCnn;
                    oCmd.Transaction = m_oTransaction;

                    string xTable = "UserSegments";
                    string xObjectType = "99";
                    string xFieldName = "DisplayName";
                    if (bVariableSet)
                    {
                        xTable = "VariableSets";
                        xObjectType = "104";
                        xFieldName = "Name";
                    }
                    int iNewID1 = iNewPersonID;
                    int iNewID2 = 0;
                    int iNewMemberID1 = iNewPersonID;
                    int iNewMemberID2 = 0;
                    if (GetNewUserContentID(iNewPersonID, out iNewID1, out iNewID2, xTable) && GetNewUserContentID(iNewPersonID, out iNewMemberID1, out iNewMemberID2, "UserFolderMembers"))
                    {
                        DeleteUserContent(iID1, iID2, bVariableSet, true);
                        oCmd.CommandText = string.Format(xUSERCONTENT_UPDATE_SQL, xTable, iNewID1, iNewID2, iNewPersonID, iID1, iID2);
                        oCmd.CommandType = CommandType.Text;
                        oCmd.ExecuteNonQuery();
                        oCmd.CommandText = string.Format(xNEW_FOLDERMEMBER_INSERT_SQL, iNewMemberID1, iNewMemberID2, iFolderID1, iFolderID2, xObjectType, xFieldName, xTable, iNewID1, iNewID2);
                        oCmd.CommandType = CommandType.Text;
                        oCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private bool GetNewUserFolder(int iPersonID, string xName, out int iFolderID1, out int iFolderID2)
        {
            const string xLAST_INDEX_SQL = "SELECT TOP 1 [Index] FROM [UserFolders] WHERE [OwnerID]={0} AND [ParentFolderID1]=0 ORDER BY [Index] DESC;";
            const string xNEW_FOLDER_INSERT_SQL = "INSERT INTO UserFolders ([ID1],[ID2],[Name],[Description],[ParentFolderID1],[ParentFolderID2],[Index],[OwnerID],[OwnerID2],[LastEditTime]) " +
                            "VALUES({0},{1},'{2}','{3}',0,0,{4},{0},0,GETUTCDATE());";
            const string xSELECT_FOLDER_SQL = "SELECT [ID2] FROM [UserFolders] WHERE [ID1]={0} AND [OwnerID]={0} AND [Name]='{1}';";

            int iNewIndex = 1;
            iFolderID1 = iPersonID;
            iFolderID2 = 0;
            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = m_oCnn;
            oCmd.Transaction = m_oTransaction;
            oCmd.CommandType = CommandType.Text;
            oCmd.CommandText = string.Format(xSELECT_FOLDER_SQL, iPersonID, xName);
            object oVals = oCmd.ExecuteScalar();
            if (oVals != null)
            {
                iFolderID2 = Convert.ToInt32(oVals);
                return true;
            }
            oCmd.CommandText = string.Format(xLAST_INDEX_SQL, iPersonID);
            oCmd.CommandType = CommandType.Text;
            oVals = oCmd.ExecuteScalar();
            if (oVals != null)
            {
                iNewIndex = Convert.ToInt32(oVals) + 1;
            }
            string xDescription = "Contains content reassigned from " + xName;
            bool bSuccess = false;
            int iTries = 0;
            do
            {
                try
                {
                    iTries++;
                    iFolderID2 = m_oRand.Next(900000000, 999999999);
                    oCmd.CommandText = string.Format(xNEW_FOLDER_INSERT_SQL, iPersonID, iFolderID2, xName, xDescription, iNewIndex);
                    oCmd.CommandType = CommandType.Text;
                    oCmd.ExecuteNonQuery();
                    bSuccess = true;
                    break;
                }
                catch (System.Exception oE)
                {
                    //Couldn't insert new record due to key violation - try again with new ID2
                    if (iTries >= 20)
                    {
                        Program.ShowError(oE, "Error creating new UserFolders record for OwnerID=" + iPersonID.ToString());
                        return false;
                    }
                }
            } while (iTries < 20);
            if (bSuccess)
            {
                return true;
            }
            else
            {
                MessageBox.Show("Could not create new UserFolders record for OwnerID=" + iPersonID.ToString());
                return false;
            }
        }
        private bool GetNewUserContentID(int iPersonID, out int iID1, out int iID2, string xTable)
        {
            const string xCONTENT_SELECT_SQL = "SELECT TOP 1 [ID1], [ID2] FROM {0} WHERE [ID1]={1} AND [ID2]={2}";
            iID1 = iPersonID;
            iID2 = 0;
            bool bSuccess = false;
            int iTries = 0;

            SqlCommand oCmd = new SqlCommand();
            oCmd.Connection = m_oCnn;
            oCmd.Transaction = m_oTransaction;
           
            do
            {
                try
                {
                    iTries++;
                    iID2 = m_oRand.Next(900000000, 999999999);
                    oCmd.CommandText = string.Format(xCONTENT_SELECT_SQL, xTable, iID1, iID2);
                    oCmd.CommandType = CommandType.Text;
                    object oVal =  oCmd.ExecuteScalar();
                    if (oVal == null)
                    {
                        //No conflict with existing records
                        bSuccess = true;
                        break;
                    }
                }
                catch (System.Exception oE)
                {
                    //Couldn't insert new record due to key violation - try again with new ID2
                    if (iTries >= 20)
                    {
                        Program.ShowError(oE, "Error creating new " + xTable + " record for OwnerID=" + iPersonID.ToString());
                        return false;
                    }
                }
            } while (iTries < 20);
            return bSuccess;

        }
    }
}