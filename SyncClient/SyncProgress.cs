using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac.Sync
{
    public partial class SyncProgress : Form
    {
        public SyncProgress()
        {
            InitializeComponent();
        }
        public void AddRow(string xDataObject)
        {
            this.lstSyncTables.Items.Add(xDataObject);
            this.lstSyncTables.SelectedIndex = this.lstSyncTables.Items.Count - 1;
            Application.DoEvents();
        }

        public void UpdateProgressLabel(string xText)
        {
            this.lblProgress.Text = xText;
            this.Refresh();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        public int RowCount
        {
            get { return this.lstSyncTables.Items.Count; }
        }
    }
}