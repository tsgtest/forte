using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Reflection;
using LMP.Data;
using System.Web;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Serialization.Formatters.Binary;

namespace LMP.MacPac.Sync
{
    #region OLD AdminPeopleSyncClient Class - functionality has been folded into AdminSyncClient
    //public sealed class AdminPeopleSyncClient : SyncClient
    //{
    //    private string[] m_PeopleUpTables = { "People", "AttyLicenses" };

    //    /// <summary>
    //    /// posts latest ForteAdmin.mdb people data to the network database
    //    /// </summary>
    //    public void Publish(string xServer, string xDatabase)
    //    {
    //        t0 = DateTime.Now;

    //        try
    //        {
    //            DateTime datNewSyncTime = DateTime.Now.ToUniversalTime();
    //            //return a dataset of all people and license data
    //            DataSet oEdits = GetUnpostedEdits(m_PeopleUpTables, false, "1/1/1999", datNewSyncTime.ToString());

    //            foreach (DataTable oTable in oEdits.Tables)
    //            {
    //                using (DataSet oDS = new DataSet("PeopleSyncUpDS"))
    //                {
    //                    oDS.Tables.Add(oTable.Copy());
    //                    //JTS: This is to avoid Out Of Memory Errors with very large DataSets
    //                    oDS.RemotingFormat = SerializationFormat.Binary;
    //                    SetDataSetTimeZoneOffset(oDS);
    //                    if (oTable.Rows.Count > 0)
    //                    {
    //                        try
    //                        {
    //                            m_oSyncServer.PublishAdminSyncData(oDS, xServer, xDatabase);
    //                        }
    //                        catch (System.Web.Services.Protocols.SoapException oSoapE)
    //                        {
    //                            throw new LMP.Exceptions.ServerException(
    //                                LMP.Resources.GetLangString("Error_WebServiceException"), oSoapE);
    //                        }
    //                        catch (System.Exception oE)
    //                        {
    //                            //other unexpected error
    //                            throw oE;
    //                        }
    //                    }
    //                }
    //            }
    //            LMP.Benchmarks.Print(t0, "AdminPeopleSyncUp");
    //        }
    //        catch (System.Exception oE)
    //        {
    //            WriteErrorToLog("Could not synchronize upstream.", oE);
    //            throw;
    //        }
    //        finally
    //        {
    //        }
    //    }
    //}
    #endregion
    /// <summary>
    /// manages admin synchronization with the network database
    /// </summary>
    public sealed class AdminSyncClient : SyncClient
    {
        #region *************************constructors*************************
        public AdminSyncClient(string xIISServer, string xServerAppName)
        {
            try
            {
                this.m_xIISServer = xIISServer;
                this.m_xServerAppName = xServerAppName;

                //alert if no server uri is specified
                string xServerUri = this.ServerUri;

                //create transperent proxy object
                m_oSyncServer = (ISyncServer)Activator.GetObject(
                    typeof(ISyncServer), xServerUri);

                SetLocalConnection();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        #endregion
        #region *************************enumerations*************************
        enum mpAdminSyncDirection
        {
            Up = 1,
            Down = 2
        }
        #endregion
        #region *************************events*************************
        #endregion
        #region *************************fields*************************
        private string m_xIISServer;
        private string m_xServerAppName;
        //GLOG 5484:  Permissions and VariableSets should not be included in Admin Publish
        private string[] m_AdminUpTables = { "Deletions", "Assignments", "AddressFormats", "Addresses", "Offices", "People", "Proxies", "AttyLicenses", "PeopleGroups", "GroupAssignments", "Countries", "States", "Counties", "Couriers", "Courts", "ExternalConnections", "Folders", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "KeySets", "Lists", "Restrictions", "Segments", "Translations", "ValueSets"};
        private string[] m_AdminUpPeopleTables = { "Deletions", "People", "AttyLicenses" };
        #endregion
        #region *************************properties*************************
        /// <summary>
        /// returns the sync server uri
        /// </summary>
        public string ServerUri
        {
            get
            {
                //string xIP = LMP.Registry.GetLocalMachineValue(ForteConstants.mpSyncRegKey, "ServerIP");
                if (string.IsNullOrEmpty(m_xIISServer) || string.IsNullOrEmpty(m_xServerAppName))
                {
                    throw new LMP.Exceptions.ServerException("Invalid server address.");
                }
                else
#if UseConsoleServer
                return "tcp://localhost:65300/DataSynchronizer";
#else
                return "http://" + m_xIISServer + "/" + m_xServerAppName + "/DataSynchronizer.rem";
#endif
            }
        }
        #endregion
        #region *************************methods*************************
        //returns the datetime of the 
        //last admin sync for the current Server and Database
        private DateTime LastSync(string xServer, string xDatabase, mpAdminSyncDirection iDirection)
        {
            try
            {
                string xDirection = "";
                if (iDirection == mpAdminSyncDirection.Down)
                    xDirection = "Down";
                else if (iDirection == mpAdminSyncDirection.Up)
                    xDirection = "Up";

                Trace.WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase, "xDirection", xDirection);
                string xName = "LastSync" + xDirection + "|" + xServer + "|" + xDatabase;
                m_oLocalDBCnn.Open();
                using (OleDbCommand oCommand = m_oLocalDBCnn.CreateCommand())
                {
                    oCommand.CommandText = "SELECT Value FROM Metadata AS m WHERE m.Name='" + xName + "';";
                    oCommand.CommandType = CommandType.Text;
                    string xResult = "";
                    try
                    {
                        xResult = oCommand.ExecuteScalar().ToString();
                    }
                    catch { }
                    if (!string.IsNullOrEmpty(xResult))
                    {
                        if (!xResult.Contains("Z"))
                            xResult = String.ConvertDateToISO8601(xResult);
                        return DateTime.Parse(xResult).ToUniversalTime();
                    }
                    else
                    {
                        if (iDirection == mpAdminSyncDirection.Up)
                        {
                            //JTS 12/08/08: If No LastSyncUp value found for Server,
                            //Also check for LastDeliveryDate metadata
                            oCommand.CommandText = "SELECT Value FROM Metadata AS m WHERE m.Name='LastDeliveryDate';";
                            oCommand.CommandType = CommandType.Text;
                            string xResult2 = oCommand.ExecuteScalar().ToString();
                            if (!string.IsNullOrEmpty(xResult2))
                            {
                                if (!xResult2.Contains("Z"))
                                    xResult2 = String.ConvertDateToISO8601(xResult);
                                return DateTime.Parse(xResult2).ToUniversalTime();
                            }

                        }
                        //JTS 12/08/08: Return Min Date + 1 day so we can distinguish
                        //between initial publish and subsequent publish where LastSync is not available
                        return this.MinSyncCompareDate + new TimeSpan(1, 0, 0, 0);
                    }
                }
            }
            catch
            {
                //return arbitary past date
                return this.MinSyncCompareDate + new TimeSpan(1, 0, 0, 0);
            }
            finally
            {
                m_oLocalDBCnn.Close();
            }
        }
        /// <summary>
        /// writes last sync time to ForteAdmin.mdb
        /// </summary>
        private void WriteLastSyncTime(string xServer, string xDatabase, DateTime datLastSync, mpAdminSyncDirection iDirection)
        {
            string xDirection = "";
            if (iDirection == mpAdminSyncDirection.Down)
                xDirection = "Down";
            else if (iDirection == mpAdminSyncDirection.Up)
                xDirection = "Up";

            string xName = "LastSync" + xDirection + "|" + xServer + "|" + xDatabase;
            DateTime t0 = DateTime.Now;

            if ((m_oLocalDBCnn.State & ConnectionState.Open) == 0)
                m_oLocalDBCnn.Open();

            int iNumRowsAffected = 0;
            try
            {
                using (OleDbCommand oCommand = m_oLocalDBCnn.CreateCommand())
                {
                    oCommand.CommandText = "UPDATE Metadata SET [Value] = '" +
                        XmlConvert.ToString(datLastSync, XmlDateTimeSerializationMode.Utc) +
                        "' WHERE [Name] = '" + xName + "';";
                    oCommand.CommandType = CommandType.Text;
                    iNumRowsAffected = oCommand.ExecuteNonQuery();

                    if (iNumRowsAffected == 0)
                    {
                        // There was no record to update. Add a record.
                        oCommand.CommandText = "Insert into Metadata(Name, [Value]) values('" + xName + "', '"
                            + XmlConvert.ToString(datLastSync, XmlDateTimeSerializationMode.Utc) + "')";
                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_oLocalDBCnn.Close();

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// transfer admin data set to new db
        /// </summary>
        public bool SyncDown(string xServer, string xDatabase)
        {
            t0 = DateTime.Now;

            try
            {
                bool bAdminPublishInProgress = m_oSyncServer.AdminPublishInProgress(xServer, xDatabase, Environment.UserName, Environment.MachineName); //GLOG 5863
                //GLOG 4462: notify if Metadata value could not be retrieved
                DateTime datAdminPublish = m_oSyncServer.GetAdminPublishedTime(xServer, xDatabase, Environment.UserName, Environment.MachineName); //GLOG 5863
                if (datAdminPublish.Year == DateTime.MaxValue.Year)
                {
                    //GLOG 4462: notify if Metadata value could not be retrieved
                    throw new LMP.Exceptions.ServerException(LMP.Resources.GetLangString("Error_ServerLoginFailed"));
                }
                else if (bAdminPublishInProgress)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_DownloadNotAllowedDuringAdminSync"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else if (datAdminPublish == this.MinSyncCompareDate)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_DownloadNotAllowedBeforePublish"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                //get last sync time from metadata
                DateTime datLastSyncTime = this.LastSync(xServer, xDatabase, mpAdminSyncDirection.Down);

                DataSet oDS = null;

                //get new time of last sync down -
                //we put the time right before the sync to 
                //guarantee that changes that are posted
                //during sync down are downloaded in the subsequent sync
                DateTime datNewLastDownTime = DateTime.Now.ToUniversalTime();
                LMP.Trace.WriteNameValuePairs("LastSyncTime", 
                    datLastSyncTime.ToString(), "NewSyncTime", 
                    datNewLastDownTime.ToString());

                try
                {
                    //GLOG 8174: Enforce M/d/yyyy date format even if US English short date
                    //has been changed at system level
                    //GLOG 8412: Make sure AM/PM uses English characters
                    string xLastSyncTime = datLastSyncTime.ToString("M/d/yyyy h:mm:ss tt", Culture.USEnglishCulture);
                    Trace.WriteNameValuePairs("xLastSyncTime", xLastSyncTime);
                    //GLOG 4310: Server expects U.S. English date string
                    oDS = m_oSyncServer.GetAdminSyncData(xLastSyncTime, xServer, xDatabase, Environment.UserName, Environment.MachineName); //GLOG 5863
                }
                catch (System.Web.Services.Protocols.SoapException oE)
                {
                    throw new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_WebServiceException"), oE);
                }
                catch (System.Exception oE)
                {
                    //other unexpected error
                    throw oE;
                }
                ProgressForm = new SyncProgress();
                ProgressForm.Show();
                //JTS 12/09/08: Make sure People table structure matches Server
                string[] aNetFields = m_oSyncServer.PeopleCustomFieldList(xServer, xDatabase, Environment.UserName, Environment.MachineName); //GLOG 5863
                if (aNetFields != null)
                {
                    List<string> aList = new List<string>(aNetFields);
                    if (LMP.Data.Application.UpdatePeopleTableStructure(m_oLocalDBCnn.DataSource, aList))
                    {
                        ProgressForm.AddRow(LMP.Resources.GetLangString("Prompt_LocalPeopleStructureUpdated"));
                    }
                }
                //update local db with new/edited server data
                this.IncorporateServerEdits(oDS, datLastSyncTime, mpSyncType.Admin);

                WriteLastSyncTime(xServer, xDatabase, datNewLastDownTime, mpAdminSyncDirection.Down);

                LMP.Benchmarks.Print(t0, "AdminSyncDown");
                return true;
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not synchronize downstream.", oE);
                throw;
            }
        }
        /// <summary>
        /// Publishes People and Attorney License tables to the network db
        /// </summary>
        /// <param name="xServer"></param>
        /// <param name="xDatabase"></param>
        /// <returns></returns>
        public string SyncPeopleUp(string xServer, string xDatabase)
        {
            return SyncUp(xServer, xDatabase, true); //JTS 3/30/10
        }
        /// <summary>
        /// posts latest ForteAdmin.mdb data to the network database
        /// </summary>
        public string SyncUp(string xServer, string xDatabase)
        {
            return SyncUp(xServer, xDatabase, false); //JTS 3/30/10
        }
        /// <summary>
        /// posts latest ForteAdmin.mdb data in the specified tables to the network database -
        /// if successful, returns a GUID that identifies the publish - 
        /// the GUID is written to both server and local db metadata tables, else returns null
        /// </summary>
        private string SyncUp(string xServer, string xDatabase, bool bPeopleRelatedOnly) //JTS 3/30/10
        {
            string xPublishingID = null;
            bool bFirstSync = true;
            string xLastSyncTime;
            //GLOG 5863
            string xLoginID = Environment.UserName;
            string xMachine = Environment.MachineName;

            //compare last publish id from server and local dbs -
            string xLocalLastPublishID = LMP.Data.Application.GetMetadata("LastPublishID");
            string xServerLastPublishID = m_oSyncServer.GetLastPublishID(xServer, xDatabase, xLoginID, xMachine);

            //GLOG 8161: Don't display this message if called from PUP
            if (!bPeopleRelatedOnly && !string.IsNullOrEmpty(xServerLastPublishID) && !string.IsNullOrEmpty(xLocalLastPublishID))
            {
                //server has been published to -
                //alert if source is not the same as previous publish
                if (xLocalLastPublishID != xServerLastPublishID)
                {
                    string xMsg = LMP.Resources.GetLangString("Prompt_DifferentSourcePublishDB").Replace("ForteAdmin.mdb",
                        LMP.Data.Application.AdminDBName);
                    DialogResult iRes = MessageBox.Show(string.Format(xMsg, xServerLastPublishID),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    System.Windows.Forms.Application.DoEvents();

                    if (iRes == DialogResult.No)
                    {
                        return null;
                    }
                }
            }

            bool bAdminPublishInProgress = m_oSyncServer.AdminPublishInProgress(xServer, xDatabase, xLoginID, xMachine);
            //GLOG 4462: notify if Metadata value could not be retrieved
            DateTime datAdminPublish = m_oSyncServer.GetAdminPublishedTime(xServer, xDatabase, xLoginID, xMachine);
            if (datAdminPublish.Year == DateTime.MaxValue.Year)
            {
                throw new LMP.Exceptions.ServerException(LMP.Resources.GetLangString("Error_ServerLoginFailed"));
            }
            else if (bAdminPublishInProgress)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_PublishNotAllowedDuringAdminSync"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Windows.Forms.Application.DoEvents();
                return null;
            }
            else if (datAdminPublish.Date == this.MinSyncCompareDate.Date)
            {
                //JTS 3/30/10: Force full publish in case PUP has been run before first Publish
                bPeopleRelatedOnly = false;
                bFirstSync = true;
                //Network Database has never been published to.  Prompt before performing Full Sync
                DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_ProceedWithFullAdminSync"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                System.Windows.Forms.Application.DoEvents();
                if (iRet != DialogResult.Yes)
                    return null;
                else
                    //GLOG 6077: Date String needs to be in MM/dd/yyyy format for SQL
                    xLastSyncTime = this.MinSyncCompareDate.ToString(LMP.Culture.USEnglishCulture);
            }
            else
            {
                //GLOG 6008: Confirm new Server/Database values before proceeding
                DateTime dLastSync = this.LastSync(xServer, xDatabase, mpAdminSyncDirection.Up);
                if (dLastSync.Date == this.MinSyncCompareDate.Date + new TimeSpan(1, 0, 0, 0))
                {
                    //GLOG 6008: Local Database has never been published to specified database.  Prompt before performing Full Sync
                    DialogResult iRet = MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FirstPublishToNewDatabase"), xDatabase, xServer),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    System.Windows.Forms.Application.DoEvents();
                    if (iRet != DialogResult.Yes)
                        return null;
                }
                //GLOG 6077: Date String needs to be in MM/dd/yyyy format for SQL
                xLastSyncTime = dLastSync.ToString(LMP.Culture.USEnglishCulture);
            }

            //JTS 3/30/10: Differentiate between regular Admin Publish and PUP Publish
            string[] aTables;
            if (bPeopleRelatedOnly)
            {
                aTables = m_AdminUpPeopleTables;
            }
            else
            {
                aTables = m_AdminUpTables;
            }

            t0 = DateTime.Now;
            ProgressForm = new SyncProgress();

            try
            {
                int iCount = 1;
                int iTableTotal = 0;
                string xTableName = null;

                bool bIsUser = false;

                if (LMP.Data.Application.UpdateNetworkDBPeopleObjects(xServer, xDatabase, m_oLocalDBCnn))
                {
                    ProgressForm.AddRow(LMP.Resources.GetLangString("Prompt_NetworkPeopleStructureUpdated"));
                    iTableTotal = 1;
                }
                ////mark new last sync time before running sync
                ////to ensure we catch all edited records in subsequent syncs
                DateTime datNewLastSyncTime = DateTime.Now.ToUniversalTime();

                //return a dataset of unposted edits
                //GLOG 8412: Make sure AM/PM uses English characters
                //GLOG 15922: If U.S. English Short Date format is customized use custom setting, since Jet will expect that
                //GLOG 15929: Use UTC format for compatibility with all regions and Jet
                DataSet oEdits = GetUnpostedEdits(aTables, bIsUser, xLastSyncTime, datNewLastSyncTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), 
                    bPeopleRelatedOnly); //JTS 3/30/10
                LMP.Trace.WriteNameValuePairs("LastSyncTime", xLastSyncTime, "NewSyncTime", datNewLastSyncTime.ToString());
                m_oSyncServer.StartAdminPublish(xServer, xDatabase, xLoginID, xMachine);
                iTableTotal = iTableTotal + oEdits.Tables.Count;
                ProgressForm.Show();
                if (iTableTotal == 0)
                    //There are no changes to post since last publish
                    ProgressForm.AddRow("Nothing to do.");

                foreach (DataTable oTable in oEdits.Tables)
                {
                    using (DataSet oDS = new DataSet("SyncUpDS"))
                    {
                        //open network db form, 
                        //which displays data objects as they are created
                        xTableName = oTable.TableName;
                        ProgressForm.AddRow("Synchronizing table " + xTableName);
                        ProgressForm.UpdateProgressLabel("Executing " + iCount.ToString() + " of " + iTableTotal.ToString());
                        iCount++;
                        ProgressForm.Refresh();
                        System.Windows.Forms.Application.DoEvents();
                        if (oTable.Rows.Count > 0)
                        {
                            //GLOG 5523: JTS 5/25/11 - If Segments table becomes too large,
                            //eventually OutOfMemoryException will occur when attempting to 
                            //serialize.  Instead of checking the data size,
                            //just always publish Segments in multiple passes.
                            //************************************************
                            ////JTS 11/03/10: Get Serialized content of Table
                            //MemoryStream oStream = new MemoryStream();
                            //BinaryFormatter oFormatter = new BinaryFormatter();
                            //oFormatter.Serialize(oStream, oTable);
                            //oStream.Seek(0, 0);
                            //byte[] oBytes = oStream.ToArray();
                            //oStream.Close();
                            //oStream = null;
                            int iCurRow;
                            ////If Serialized size is greater than 20M, post data
                            ////100 rows at a time.  This is to avoid 404 error when
                            ////initially publishing large Segments table using
                            ////64-bit IIS Server
                            if (oTable.TableName == "Segments") // (oBytes.Length > 20000000)
                            {
                                iCurRow = 0;
                                //Copy table structure only;
                                oDS.Tables.Add(oTable.Clone());
                            }
                            else
                            {
                                iCurRow = -1;
                                oDS.Tables.Add(oTable.Copy());
                            }

                            do
                            {
                                if (iCurRow > -1)
                                {
                                    //Empty rows from previous pass
                                    oDS.Tables[0].Rows.Clear();
                                    for (int r = 0; (r < 100) && (iCurRow < oTable.Rows.Count); r++)
                                    {
                                        //Import rows from source table into dataset
                                        oDS.Tables[0].ImportRow(oTable.Rows[iCurRow++]);
                                    }
                                }
                                //JTS: This is to avoid Out Of Memory Errors with very large DataSets
                                oDS.RemotingFormat = SerializationFormat.Binary;
                                SetDataSetTimeZoneOffset(oDS);
                                try
                                {
                                    m_oSyncServer.PublishAdminSyncData(oDS, xServer, xDatabase, xLoginID, xMachine);
                                }
                                catch (System.Web.Services.Protocols.SoapException oSoapE)
                                {
                                    throw new LMP.Exceptions.ServerException(
                                        LMP.Resources.GetLangString("Error_WebServiceException"), oSoapE);
                                }
                                //this block catches timeout errors
                                catch (System.Net.WebException oWebE)
                                {
                                    if (oWebE.Message == "The operation has timed out")
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        throw new LMP.Exceptions.ServerException(
                                            LMP.Resources.GetLangString("Error_WebServiceException"), oWebE);
                                    }
                                }
                                catch (System.Exception oE)
                                {
                                    //other unexpected error
                                    throw oE;
                                }
                            } while (iCurRow > -1 && iCurRow < oTable.Rows.Count);
                        }
                    }
                }
                //JTS 3/30/10: Don't update Last Published Time if SyncUp called from PUP
                if (!bPeopleRelatedOnly)
                {
                    //Update Metadata field on server
                    //GLOG 6941: Use Date format for string that will be recognized regardless of system regional settings
                    string xDate = datNewLastSyncTime.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    m_oSyncServer.SetAdminPublishedTime(xServer, xDatabase, xDate, xLoginID, xMachine);
                    WriteLastSyncTime(xServer, xDatabase, datNewLastSyncTime.ToUniversalTime(), mpAdminSyncDirection.Up);
                    if (bFirstSync)
                    {
                        //JTS 12/11/08: Also set LastDown value so Download Data won't retrieve everything first time
                        WriteLastSyncTime(xServer, xDatabase, datNewLastSyncTime.ToUniversalTime(), mpAdminSyncDirection.Down);
                    }
                }
                LMP.Benchmarks.Print(t0, "AdminSyncUp");
                if (bPeopleRelatedOnly) //JTS 3/30/10
                {
                    m_oSyncServer.LogEvent("Admin People Publish completed by " + Environment.UserName + " on " + Environment.MachineName + ".");
                }
                else
                {
                    m_oSyncServer.LogEvent("Admin Publish completed by " + Environment.UserName + " on " + Environment.MachineName + ".");
                }

                //write GUID to both local and network db
                //so that on subsequent publish, admin can be
                //alerted if the local source admin db is
                //different than the source admin db of the
                //previous publish
                //GLOG 8161: Don't change local DB Metadata if called from PUP
                if (!bPeopleRelatedOnly)
                {
                    xPublishingID = Guid.NewGuid().ToString().Replace("-", "");

                    //write locally - ID gets written to server in DataSynchronizer.EndAdminPublish() below
                    LMP.Data.Application.SetMetadata("LastPublishID", xPublishingID);
                }
                else
                {
                    //GLOG 8161: If called from PUP, leave Server Metadata at original value
                    xPublishingID = xServerLastPublishID;
                }
                return xPublishingID;
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not synchronize upstream.", oE);
                throw;
            }
            finally
            {
                //Mark completion of publish to unlock network db
                m_oSyncServer.EndAdminPublish(xServer, xDatabase, xPublishingID, xLoginID, xMachine);
                ProgressForm.Controls["btnOk"].Enabled = true;
            }
        }
        /// <summary>
        /// returns true if Word is running
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Copies source db to destination db
        /// </summary>
        public void CopyNetworkDB(string xSourceServer, string xSourceDatabase,
            string xDestServer, string xDestDatabase, string xDestLogin, string xDestPassword)
        {
            t0 = DateTime.Now;
            try
            {
                try
                {
                    m_oSyncServer.CopyNetworkDB(xSourceServer, xSourceDatabase,
                        xDestServer, xDestDatabase, xDestLogin, xDestPassword, Environment.UserName, Environment.MachineName); //GLOG 5863
                }
                catch (System.Web.Services.Protocols.SoapException oE)
                {
                    throw new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_WebServiceException"), oE);
                }
                catch (System.Exception oE)
                {
                    //other unexpected error
                    throw oE;
                }
                LMP.Benchmarks.Print(t0, "CopyNetworkDB");

            }
            catch (Exception oE)
            {
                //write error to log
                WriteErrorToLog("Could not copy network db.", oE);
                throw;
            }
            finally
            {
            }
        }
        /// <summary>
        /// sets the connection to the local access db
        /// </summary>
        private void SetLocalConnection()
        {
            //get location of local db
            string xDBPath = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13

            //open connection to local db
            m_oLocalDBCnn = new OleDbConnection(
                @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDBPath +
                @"\" + LMP.Data.Application.AdminDBName + ";Jet OLEDB:System database=" +
                LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"); //GLOG 6718
        }
        public void BuildCache(string xIISServerIP, string xIISServerApplicationName)
        {
            ISyncServer oServer = null;
            try
            {
                oServer = (ISyncServer)Activator.GetObject(
                    typeof(ISyncServer), "http://" + xIISServerIP + "/" + xIISServerApplicationName + "/DataSynchronizer.rem");
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not build the cache on " + xIISServerIP, oE);
            }

            oServer.BuildCache();
        }
        public bool UpdateSharedSegments(string xServer, string xDatabase)
        {
            t0 = DateTime.Now;

            try
            {
                if (m_oSyncServer.AdminPublishInProgress(xServer, xDatabase, Environment.UserName, Environment.MachineName))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_DownloadNotAllowedDuringAdminSync"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else if (m_oSyncServer.GetAdminPublishedTime(xServer, xDatabase, Environment.UserName, Environment.MachineName) == this.MinSyncCompareDate)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_DownloadNotAllowedBeforePublish"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                ////get last sync time from metadata
                //DateTime datLastSyncTime = this.MinSyncCompareDate + new TimeSpan(1, 0, 0, 0);
                //string xLastSyncTime = LMP.Data.Application.GetMetadata("LastSharedSegmentSync|" + 
                //    xServer + "|" + xDatabase);
                //if (!string.IsNullOrEmpty(xLastSyncTime))
                //{
                //    if (!xLastSyncTime.Contains("Z"))
                //        xLastSyncTime = String.ConvertDateToISO8601(xLastSyncTime);
                //    datLastSyncTime = DateTime.Parse(xLastSyncTime).ToUniversalTime();
                //}

                ////get new time of last sync down -
                ////we put the time right before the sync to 
                ////guarantee that changes that are posted
                ////during sync down are downloaded in the subsequent sync
                //DateTime datNewLastDownTime = DateTime.Now.ToUniversalTime();
                //LMP.Trace.WriteNameValuePairs("LastSyncTime", datLastSyncTime.ToString(), 
                //    "NewSyncTime", datNewLastDownTime.ToString());

                DataSet oDS = null;

                try
                {
                    oDS = m_oSyncServer.GetSharedSegments(xServer, xDatabase, Environment.UserName, Environment.MachineName);
                }
                catch (System.Web.Services.Protocols.SoapException oE)
                {
                    throw new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_WebServiceException"), oE);
                }
                catch (System.Exception oE)
                {
                    //other unexpected error
                    throw oE;
                }

                //update local db with new/edited server data
                this.IncorporateSharedSegments(oDS);

                LMP.Benchmarks.Print(t0, "UpdateSharedSegments");
                return true;
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not download shared segments.", oE);
                throw;
            }
        }
        #endregion
    }

    /// <summary>
    /// manages user synchronization with the network database
    /// </summary>
    public sealed class UserSyncClient : SyncClient
    {
        #region *************************constants*************************
        public const string mpSyncWarningValueName = "SyncDBWarningShown";
        public const string mpSyncInProgressMetadata = "SyncInProgress";
        public const string mpSyncPromptSessionID = "ScheduledSyncPromptSessionID";
        public const string mpSyncLastServerGUIDMetadata = "LastServerGUID";
        private const string mpLastUserSyncValueName = "LastUserSync"; //GLOG 6254
        #endregion
        #region *************************fields*************************
        private int m_iUserID;
        private System.Timers.Timer m_oSyncTimer;
        private string m_xDBServer;
        private string m_xDBName;
        private string m_xIISIPAddress;
        private string m_xServerAppName;
        private string m_xDateCreated;
        private string m_xLocalDataDir;
        private string m_xSystemLoginID; //JTS 2/8/12
        #endregion
        #region *************************enums*************************
        #endregion
        #region *************************constructors*************************
        public UserSyncClient(string xSystemLoginID, string xServer, string xDatabase,
            string xIISIPAddress, string xServerAppName, string xDateCreated):
            this(xSystemLoginID, xServer, xDatabase, xIISIPAddress, xServerAppName,
                xDateCreated, LMP.Data.Application.WritableDBDirectory) //JTS 3/28/13
        {
        }
        public UserSyncClient(string xSystemLoginID, string xServer, string xDatabase, 
            string xIISIPAddress, string xServerAppName, string xDateCreated, string xLocalDataDirectory)
        {
            try
            {
                Trace.WriteInfo("Executing UserSyncClient()");

                this.DatabaseServer = xServer;
                this.DatabaseName = xDatabase;
                this.IISIPAddress = xIISIPAddress;
                this.ServerAppName = xServerAppName;
                this.DateCreated = xDateCreated;
                this.LocalDataDirectory = xLocalDataDirectory;
                m_xSystemLoginID = xSystemLoginID;
                Trace.WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase,
                    "xIISIPAddress", xIISIPAddress, "xServerAppName", xServerAppName,
                    "xDateCreated", xDateCreated, "xLocalDataDirectory", xLocalDataDirectory);

                //rename mp10.mdb to Forte.mdb if necessary and specified
                LMP.Data.Application.RenameUserDBIfSpecified();

                //JTS: 3/31/10
                //Always initialize connection string to point to Forte.mdb so 
                //that GetLocalMetaData will use correct DB prior to ExecuteSync
                SetLocalConnection(true);

                //JTS 2/8/12: Initialization of Server Application object has been moved out of constructor 
                if (Registry.GetLocalMachineValue(ForteConstants.mpSyncRegKey, "UseLegacyInit") == "1") //GLOG 6491: Fix specific to Orrick
                {
                    //JTS 2/8/12: Initialization of Server Application object has been moved out of constructor 
#if UseConsoleServer
                m_oSyncServer = (LMP.MacPac.Sync.ISyncServer)RemotingServices.Connect(
                    typeof(LMP.MacPac.Sync.ISyncServer), "tcp://localhost:" + 65300 + "/DataSynchronizer");
#else
                    m_oSyncServer = (ISyncServer)Activator.GetObject(
                        typeof(ISyncServer), "http://" + this.IISIPAddress + "/" + this.ServerAppName + "/DataSynchronizer.rem");
#endif

                    //check that we can connect to the server IP
                    if (!this.ServerIsAvailable())
                    {
                        //GLOG 4227 - Throw WebException for proper handling by DesktopSyncProcess
                        System.Net.WebException oE = new System.Net.WebException(LMP.Resources.GetLangString(
                            "Error_CouldNotConnectToServerApp"));

                        WriteErrorToLog(LMP.Resources.GetLangString(
                            "Error_CouldNotConnectToServerApp"), oE);

                        throw oE;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public UserSyncClient(int iUserID, string xServer, string xDatabase, string xIISIPAddress,
            string xServerAppName, string xDateCreated, bool bOnDemand)
            : this(iUserID, xServer, xDatabase, xIISIPAddress, xServerAppName, xDateCreated, bOnDemand,
                LMP.Data.Application.WritableDBDirectory) //JTS 3/28/13
        {
        }
        public UserSyncClient(int iUserID, string xServer, string xDatabase, string xIISIPAddress, 
            string xServerAppName, string xDateCreated, bool bOnDemand, string xLocalDataDirectory)
        {
            try
            {
                this.m_iUserID = iUserID;
                this.DatabaseServer = xServer;
                this.DatabaseName = xDatabase;
                this.IISIPAddress = xIISIPAddress;
                this.ServerAppName = xServerAppName;
                this.DateCreated = xDateCreated;
                this.LocalDataDirectory = xLocalDataDirectory;

                Trace.WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase,
                    "xIISIPAddress", xIISIPAddress, "xServerAppName", xServerAppName,
                    "xDateCreated", xDateCreated, "xLocalDataDirectory", xLocalDataDirectory);

                //JTS: 3/31/10
                //Always initialize connection string to point to Forte.mdb so 
                //that GetLocalMetaData will use correct DB prior to ExecuteSync
                SetLocalConnection(true);
                //JTS 2/8/12: Initialization of Server Application object has been moved out of constructor 
                if (Registry.GetLocalMachineValue(ForteConstants.mpSyncRegKey, "UseLegacyInit") == "1") //GLOG 6491: Fix specific to Orrick
                {
                    try
                    {
                        //JTS 2/8/12: Initialization of Server Application object has been moved out of constructor 
                        // transparent proxy
#if UseConsoleServer
                m_oSyncServer = (LMP.MacPac.Sync.ISyncServer)RemotingServices.Connect(
                    typeof(LMP.MacPac.Sync.ISyncServer), "tcp://localhost:" + 65300 + "/DataSynchronizer");
#else
                        m_oSyncServer = (ISyncServer)Activator.GetObject(
                            typeof(ISyncServer), "http://" + this.IISIPAddress + "/" + this.ServerAppName + "/DataSynchronizer.rem");
#endif

                        //will throw System.Net.WebException if unable to connect to remove server
                        Trace.WriteNameValuePairs("m_oSyncServer=", m_oSyncServer.ToString());
                    }
                    catch (System.Net.WebException oWebE)
                    {
                        //JTS 5/7/09: Show different error for On-Demand sync
                        if (bOnDemand)
                            throw new LMP.Exceptions.ServerException(
                                LMP.Resources.GetLangString("Error_OnDemandSyncCouldNotBeExecuted"), oWebE);
                        else
                            //GLOG 4227: throw WebException for proper handling by DesktopSyncProcess
                            throw new System.Net.WebException(
                                LMP.Resources.GetLangString("Error_SyncCouldNotBeCompleted"), oWebE);

                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        #endregion
        #region *************************properties*************************
        /// <summary>
        /// gets/sets the ID of the user that
        /// the sync targets
        /// </summary>
        public int UserID
        {
            get { return this.m_iUserID; }
            private set { this.m_iUserID = value; }
        }
        public string LastSuccessfulUserSync //GLOG 6254
        {
            get
            {
                try
                {
                    string xLastSync = "";
                    string xTemp = GetLocalMetaData("LastSync" + this.UserID.ToString());
                    if (string.IsNullOrEmpty(xTemp))
                        return "";
                    else
                    {
                        int iPos = xTemp.IndexOf("|");
                        if (iPos > -1)
                            xLastSync = xTemp.Substring(0, iPos);
                        else
                            xLastSync = xTemp;
                        if (!xLastSync.Contains("Z"))
                            xLastSync = String.ConvertDateToISO8601(xLastSync);
                        return String.ConvertDateToISO8601(DateTime.Parse(xLastSync).ToUniversalTime().ToString());
                    }
                }
                catch
                {
                    return LMP.Registry.GetCurrentUserValue(LMP.Data.ForteConstants.mpSyncRegKey, mpLastUserSyncValueName);
                }
            }
        }
        //returns the datetime of the 
        //last sync for the current user
        private DateTime LastSync
        {
            get
            {
                //JTS: 3/18/10 - Use GetLocalMetaData instead of connecting directly to Forte.mdb
                //So that ForteSync.mdb will be used if appropriate

                try
                {
                    string xLastSync = "";
                    string xTemp = GetLocalMetaData("LastSync" + this.UserID.ToString());
                    if (string.IsNullOrEmpty(xTemp))
                        return MinSyncCompareDate;
                    else
                    {
                        //parse out time from value -
                        //we've changed this value to contain both the 
                        //time and the server name - old values will
                        //only have the time. - dcf - 4/27/11
                        int iPos = xTemp.IndexOf("|");

                        if (iPos > -1)
                            xLastSync = xTemp.Substring(0, iPos);
                        else
                            xLastSync = xTemp;

                            if (!xLastSync.Contains("Z"))
                            xLastSync = String.ConvertDateToISO8601(xLastSync);

                        //GLOG 4310: Don't force U.S. English cultuer
                        return DateTime.Parse(xLastSync).ToUniversalTime();
                    }
                }
                catch
                {
                    //return arbitary past date
                    return this.MinSyncCompareDate;
                }
                //finally
                //{
                //    oCnn.Close();
                //}
            }
        }
        //returns the datetime of the 
        //last sync for the current user
        public DateTime LastOnDemandSync
        {
            get
            {
                //JTS: 3/18/10 - Use GetLocalMetaData instead of connecting directly to Forte.mdb
                //So that ForteSync.mdb will be used if appropriate
                //OleDbConnection oCnn = new OleDbConnection(
                //    @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.LocalDataDirectory +
                //    @"\Forte.mdb;Jet OLEDB:System database=" +
                //    this.LocalDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437");

                try
                {
                    string xLastSync = "";
                    string xTemp = GetLocalMetaData("LastSyncOnDemand" + this.UserID.ToString());

                    if (string.IsNullOrEmpty(xTemp))
                        return MinSyncCompareDate;
                    else
                    {
                        //parse out time from value -
                        //we've changed this value to contain both the 
                        //time and the server name - old values will
                        //only have the time - dcf - 4/27/11
                        int iPos = xTemp.IndexOf("|");

                        if (iPos > -1)
                            xLastSync = xTemp.Substring(0, iPos);
                        else
                            xLastSync = xTemp;

                        if (!xLastSync.Contains("Z"))
                            xLastSync = String.ConvertDateToISO8601(xLastSync);

                        return DateTime.Parse(xLastSync).ToUniversalTime();
                    }
                }
                catch
                {
                    //return arbitary past date
                    return this.MinSyncCompareDate;
                }
            }
        }
        /// <summary>
        /// Return Metadata List of Proxies that have been included in previous syncs for this user
        /// </summary>
        private string ProxySyncList
        {
            get
            {
                try
                {
                    Trace.WriteNameValuePairs("this.UserID", this.UserID);
                    string xProxyList = this.GetLocalMetaData("ProxySync" + this.UserID);
                    return xProxyList;
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                SetLocalMetaData("ProxySync" + this.UserID, value);
            }
        }
        /// <summary>
        /// Return delimited list of proxiable IDs from Proxies table
        /// </summary>
        internal string CurrentProxyList
        {
            get
            {
                //Read from Proxies table populated by User sync
                try
                {
                    string xProxyList = "";
                    m_oLocalDBCnn.Open();
                    using (OleDbCommand oCommand = m_oLocalDBCnn.CreateCommand())
                    {
                        oCommand.CommandText = "SELECT UserID FROM Proxies WHERE ProxyID=" + this.UserID + ";";
                        oCommand.CommandType = CommandType.Text;
                        using (OleDbDataReader oReader = oCommand.ExecuteReader())
                        {
                            try
                            {
                                while (oReader.Read())
                                {
                                    string xProxyID = oReader.GetValue(0).ToString();
                                    xProxyList += xProxyID + "|";
                                }
                            }
                            finally
                            {
                                oReader.Close();
                            }
                        }
                    }

                    xProxyList = xProxyList.TrimEnd('|');
                    return xProxyList;
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
                finally
                {
                    m_oLocalDBCnn.Close();
                }
            }
        }
        public DateTime NextScheduledSync
        {
            get
            {
                DateTime datLastSync = this.LastSync;

                if (datLastSync == this.MinSyncCompareDate)
                {
                    return DateTime.Now.ToUniversalTime();
                }
                else
                {
                    FirmApplicationSettings oSettings = null;
                    try
                    {
                        try
                        {
                            bool bRet = (LMP.Data.Application.Login(this.UserID, false));
                        }
                        catch(LMP.Exceptions.UserException)
                        {
                            //could not log on - there may not be any people in the local db yet
                            return DateTime.Now.ToUniversalTime();
                        }

                        oSettings = new FirmApplicationSettings(this.m_iUserID);
                        DateTime oNewDate;
                        string xNewDate;
                        switch (oSettings.SyncFrequencyUnit)
                        {
                            case mpFrequencyUnits.Week: //GLOG 5859
                                oNewDate = datLastSync.AddDays(7 * (double)oSettings.SyncFrequencyInterval).ToLocalTime();
                                //Increment Date to next Day matching day of week setting
                                //JTS 2/3/12:  Use a counter to prevent possibility of endless loop
                                int iCounter = 1;
                                while ((oNewDate.DayOfWeek != (DayOfWeek)oSettings.SyncFrequencyDayOfWeek) && (iCounter < 7))
                                {
                                    //JTS 2/3/12: Increment oNewDate by 1 day until configured day of the week is reached
                                    oNewDate = oNewDate.AddDays(1);
                                    iCounter++;
                                }
                                xNewDate = oNewDate.ToShortDateString() + " " + oSettings.SyncTime.ToLongTimeString();
                                //JTS 12/08/08: Make sure date is in UTC format for comparison
                                //GLOG 4310: Don't force U.S. English culture
                                return DateTime.Parse(xNewDate).ToUniversalTime();
                            case mpFrequencyUnits.Day:
                                oNewDate = datLastSync.AddDays((double)oSettings.SyncFrequencyInterval).ToLocalTime();
                                xNewDate = oNewDate.ToShortDateString() + " " + oSettings.SyncTime.ToLongTimeString();
                                //JTS 12/08/08: Make sure date is in UTC format for comparison
                                //GLOG 4310: Don't force U.S. English culture
                                return DateTime.Parse(xNewDate).ToUniversalTime();
                            case mpFrequencyUnits.Hour:
                                return datLastSync.AddHours((double)oSettings.SyncFrequencyInterval);
                            case mpFrequencyUnits.Minute:
                            default:
                                return datLastSync.AddMinutes((double)oSettings.SyncFrequencyInterval);
                        }
                    }
                    finally
                    {
                        oSettings = null;
                        LMP.Data.Application.Logout();
                    }
                }
            }
        }
        public string DatabaseServer
        {
            get { return m_xDBServer; }
            set { m_xDBServer = value; }
        }
        public string DatabaseName
        {
            get { return m_xDBName; }
            set { m_xDBName = value; }
        }
        public string IISIPAddress
        {
            get { return m_xIISIPAddress; }
            set { m_xIISIPAddress = value; }
        }
        public string ServerAppName
        {
            get { return string.IsNullOrEmpty(m_xServerAppName) ? "ForteSyncServer" : m_xServerAppName; }
            set { m_xServerAppName = value; }
        }
        public string DateCreated
        {
            get { return m_xDateCreated; }
            set { m_xDateCreated = value; }
        }
        public string LocalDataDirectory
        {
            get { return m_xLocalDataDir; }
            set { m_xLocalDataDir = value; }
        }
        /// <summary>
        /// returns true iff the sync for this IIS box is available
        /// </summary>
        public bool ServerIsSyncEnabled
        {
            get
            {
                try
                {
                    return m_oSyncServer.SyncAvailable(
                        this.DatabaseServer, this.DatabaseName, this.IISIPAddress, Environment.UserName, Environment.MachineName); //GLOG 5863
                }
                catch (System.Exception oE)
                {
                    //JTS 12/10/08: throw error here
                    //SyncAvailable already handles any error on the server side, so
                    //this would indicate some unexpected condition
                    throw oE;
                }
            
            }
        }
        #endregion
        #region *************************methods*************************
        private bool MeetsMinimumVersionRequirements
        {
            get
            {
                //get server version
                string xRequiredMinimumVersion = m_oSyncServer.GetRequiredMinClientVersion(m_xDBServer, m_xDBName, Environment.UserName, Environment.MachineName); //GLOG 5863

                //no minimum requirements if no value is specified
                if (xRequiredMinimumVersion == "")
                    return true;
                else if (xRequiredMinimumVersion == null)
                {
                    //GLOG 4462: null return value indicates problem retrieving data from server
                    //Log error and don't continue with sync
                    LMP.Exceptions.ServerException oE = new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_ServerLoginFailed"));
                    //GLOG 6254
                    WriteErrorToLog("Could not retrieve Server Required Minimum Version.", oE, this.LastSuccessfulUserSync);
                    return false;
                }

                //parse
                string[] aRequiredMinimumVersionElements = xRequiredMinimumVersion.Split('.');

                //get client version
                string xClientVersion = GetVersion();

                //parse
                string[] aClientVersionElements = xClientVersion.Split('.');

                //compare elements
                for (int i = 0; i < 4; i++)
                {
                    int iRequiredElement = int.Parse(aRequiredMinimumVersionElements[i]);
                    int iClientElement = int.Parse(aClientVersionElements[i]);

                    //since we're moving in order of version elements,
                    //for any element, if the client number is greater
                    //than the required number, the client version is newer.
                    //if the number is the same, we go to the next element.
                    //and if the number is less, the client version is older.
                    if (iClientElement > iRequiredElement)
                        return true;
                    else if (iClientElement == iRequiredElement)
                        continue;
                    else
                        return false;
                }

                return true;
            }
        }
        private string GetVersion()
        {
            //get location of code of executing assembly
            string xAsmFileName = Assembly.GetExecutingAssembly().CodeBase;

            //parse URI prefix
            xAsmFileName = xAsmFileName.Replace(@"file:///", "");

            FileVersionInfo oVersionInfo = FileVersionInfo.GetVersionInfo(xAsmFileName);

            return oVersionInfo.ProductVersion;
        }

        /// <summary>
        /// returns true iff the MacPac server application is available
        /// </summary>
        /// <returns></returns>
        public bool ServerIsAvailable()
        {
            int iPort;

#if UseConsoleServer
            return true; 
#else

            string[] axConnParams = this.IISIPAddress.Split(':');
            string xAddress = axConnParams[0];

            if (axConnParams.Length > 1)
            {
                // Get the specified port.
                iPort = int.Parse(axConnParams[1]);
            }
            else
            {
                iPort = 80; // This is the default port for IIS.
            }

            if (xAddress == "localhost")
                return true;
            else
                return LMP.Network.IsServerConnectionAvailable(xAddress, iPort, 3000);
#endif
        }

        /// <summary>
        /// synchronizes local MacPac data with server data
        /// </summary>
        /// <param name="datLastSyncTime"></param>
        /// <param name="xUserID"></param>
        private void SyncDown(DateTime datLastSyncTime, int iUserID)
        {
            SyncDown(false, datLastSyncTime, iUserID);
        }
        /// <summary>
        /// synchronizes local MacPac data with server data
        /// </summary>
        /// <param name="datLastSyncTime"></param>
        private void SyncDown(DateTime datLastSyncTime)
        {
            SyncDown(false, datLastSyncTime, this.UserID);
        }
        /// <summary>
        /// synchronizes local MacPac data with server data
        /// </summary>
        /// <param name="bOnDemand"></param>
        /// <param name="datLastSyncTime"></param>
        private void SyncDown(bool bOnDemand, DateTime datLastSyncTime)
        {
            SyncDown(bOnDemand, datLastSyncTime, this.UserID);
        }
        /// <summary>
        /// synchronizes local MacPac data with server data -
        /// returns true iff edits were brought down from the server
        /// </summary>
        /// <param name="bOnDemand"></param>
        /// <param name="datLastSyncTime"></param>
        /// <param name="xUserID"></param>
        private bool SyncDown(bool bOnDemand, DateTime datLastSyncTime, int iUserID)
        {
            t0 = DateTime.Now;

            try
            {
                if (m_oSyncTimer != null)
                    m_oSyncTimer.Stop();

                DataSet oDS = null;
                bool bIsProxy = iUserID != this.UserID;
                DateTime t1 = DateTime.Now;
                //GLOG 8174: Enforce M/d/yyyy date format even if US English short date
                //has been changed at system level
                //GLOG 8412: Make sure AM/PM uses English characters
                string xLastSyncTime = datLastSyncTime.ToString("M/d/yyyy h:mm:ss tt", Culture.USEnglishCulture);
                Trace.WriteNameValuePairs("xLastSyncTime", xLastSyncTime);
                //GLOG 4310: Server expects U.S. English date string
                oDS = m_oSyncServer.GetUserSyncData(iUserID, 
                    xLastSyncTime, 
                    this.DatabaseServer, this.DatabaseName, bOnDemand,
                    Environment.UserName, Environment.MachineName);

                Trace.WriteNameValuePairs("oDS.Tables.Count", oDS.Tables.Count);

                LMP.Benchmarks.Print(t1, "GetUserSyncData");

                //check to see if any records exist in the data - 
                //there might not be any need to execute further code
                Boolean bEditsExist = false;
                foreach (DataTable oDT in oDS.Tables)
                {
                    if (oDT.Rows.Count > 0)
                    {
                        bEditsExist = true;
                        break;
                    }
                }

                if (bEditsExist)
                {
                    //update local db with new/edited server data
                    t1 = DateTime.Now;

                    this.IncorporateServerEdits(oDS, datLastSyncTime, 
                        bIsProxy ? mpSyncType.Proxy : mpSyncType.User );

                    LMP.Benchmarks.Print(t1, "IncorporateServerEdits");
                }

                LMP.Benchmarks.Print(t0, "UserSyncDown for " + iUserID.ToString());

                //return tru iff there were edits to incorporate
                return bEditsExist;
            }
            catch (Exception oE)
            {
                //write error to log
                WriteErrorToLog("Could not synchronize downstream - User=" + iUserID.ToString() +
                    " LastSyncTime=" + datLastSyncTime.ToString() + " LogonUser=" + this.UserID +
                    " DBName=" + this.DatabaseName + " DBServer=" + this.DatabaseServer + 
                    "OnDemand=" + bOnDemand, oE);

                throw oE;
            }
        }
        /// <summary>
        /// synchronizes local MacPac data with server data
        /// </summary>
        private void SyncDownPeopleRelated(DateTime datLastSyncTime)
        {
            t0 = DateTime.Now;

            try
            {
                if (m_oSyncTimer != null)
                    m_oSyncTimer.Stop();

                string xUserID = this.m_iUserID.ToString();

                DataSet oDS = null;

                //GLOG 8174: Enforce M/d/yyyy date format even if US English short date
                //has been changed at system level
                //GLOG 8412: Make sure AM/PM uses English characters
                string xLastSyncTime = datLastSyncTime.ToString("M/d/yyyy h:mm:ss tt", Culture.USEnglishCulture);
                Trace.WriteNameValuePairs("xLastSyncTime", xLastSyncTime);
                //GLOG 4310: Server expects U.S. English date string
                oDS = m_oSyncServer.GetUserSyncPeopleData(
                    this.m_iUserID, xLastSyncTime, this.DatabaseServer, this.DatabaseName,
                    Environment.UserName, Environment.MachineName);

                Trace.WriteNameValuePairs("oDS.Tables.Count", oDS.Tables.Count);

                //update local db with new/edited server data
                this.IncorporateServerEdits(oDS, datLastSyncTime, mpSyncType.User);

                LMP.Benchmarks.Print(t0, "UserSyncDown for " + this.UserID);
            }
            catch (Exception oE)
            {
                //write error to log
                WriteErrorToLog("Could not synchronize downstream - " +
                    " DBName=" + this.DatabaseName + " DBServer=" + this.DatabaseServer +
                    " UserID=" + this.m_iUserID.ToString() + " LastSyncTime=" + datLastSyncTime.ToString(), oE);
                throw oE;
            }
        }
        /// <summary>
        /// synchronizes server database with local MacPac data-
        /// synchronization occurs asynchronously
        /// </summary>
        /// <param name="xLastEditTime"></param>
        private void SyncUp(DateTime datLastSyncTime)
        {
            t0 = DateTime.Now;

            try
            {
                bool bIsUser = true;

                string xUserID = this.m_iUserID.ToString();

                //return a dataset of unposted edits
                //JTS 08/20/2009:  Convert LastSyncTime to U.S. English Culture, since this is required for Jet SQL text
                //GLOG 15922: If U.S. English Short Date format is customized use custom setting for new time, since Jet will expect that
                //GLOG 15929: Use UTC format for compatibility with all regions and Jet
                DataSet oEdits = GetUnpostedEdits(m_aUserUpTables, bIsUser, datLastSyncTime.ToString(LMP.Culture.USEnglishCulture),
                    DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff"));

                foreach (DataTable oTable in oEdits.Tables)
                {
                    if (oTable.Rows.Count > 0)
                    {
                        using (DataSet oDS = new DataSet("SyncUpDS"))
                        {
                            oDS.Tables.Add(oTable.Copy());
                            //JTS: This is to avoid Out Of Memory Errors with very large DataSets
                            oDS.RemotingFormat = SerializationFormat.Binary;
                            SetDataSetTimeZoneOffset(oDS);
                            m_oSyncServer.PublishUserSyncData(this.m_iUserID, oDS,
                                this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                        }
                    }
                }

                LMP.Benchmarks.Print(t0, "UserSyncUp for " + this.UserID);
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not synchronize upstream- " + 
                    " DBName=" + this.DatabaseName + " DBServer=" + this.DatabaseServer +
                    " UserID=" + this.m_iUserID.ToString() + " LastSyncTime=" + datLastSyncTime.ToString(), oE);

                throw oE;
            }
        }
        /// <summary>
        /// executes a full user synchronization
        /// </summary>
        public void ExecuteSync()
        {
            ExecuteSync(true, false);
        }
        public void ExecuteSync(bool bCheckLocalDBStructure)
        {
            ExecuteSync(bCheckLocalDBStructure, false);
        }
        /// <summary>
        /// executes a full user synchronization
        /// </summary>
        private void ExecuteSync(bool bCheckLocalDBStructure, bool bCalledFromOnDemand) //GLOG 4573
        {
            if (!LMP.MacPac.MacPacImplementation.IsFullWithNetworkSupport)
            {
                return;
            }

            //GLOG 4462
            DateTime datAdminPublish;
            //JTS: 03/18/10
            string xNetworkServerGUID = "";
            string xLocalServerGUID = "";
            bool bNewServerDatabase = false;
            FirmApplicationSettings.mpScheduledSyncBehavior iStartPrompt = 0;
            bool bPromptOnCompletion = true;
            bool bLogSuccess = false;

            //check that we can connect to the server IP
            if (!this.ServerIsAvailable())
            {
                //GLOG 4227 - Throw WebException for proper handling by DesktopSyncProcess
                System.Net.WebException oE = new System.Net.WebException(LMP.Resources.GetLangString(
                    "Error_CouldNotConnectToServerApp"));

                WriteErrorToLog(LMP.Resources.GetLangString(
                    "Error_CouldNotConnectToServerApp"), oE);

                throw oE;
            }
            try
            {
                if (m_oSyncServer == null)
                {
                    // transparent proxy
#if UseConsoleServer
                m_oSyncServer = (LMP.MacPac.Sync.ISyncServer)RemotingServices.Connect(
                    typeof(LMP.MacPac.Sync.ISyncServer), "tcp://localhost:" + 65300 + "/DataSynchronizer");
#else
                    m_oSyncServer = (ISyncServer)Activator.GetObject(
                        typeof(ISyncServer), "http://" + this.IISIPAddress + "/" + this.ServerAppName + "/DataSynchronizer.rem");
#endif

                    //will throw System.Net.WebException if unable to connect to remote server
                    Trace.WriteNameValuePairs("m_oSyncServer=", m_oSyncServer.ToString());
                }
            }
            catch (System.Net.WebException oWebE)
            {
                //JTS 5/7/09: Show different error for On-Demand sync
                if (bCalledFromOnDemand)
                    throw new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_OnDemandSyncCouldNotBeExecuted"), oWebE);
                else
                    //GLOG 4227: throw WebException for proper handling by DesktopSyncProcess
                    throw new System.Net.WebException(
                        LMP.Resources.GetLangString("Error_SyncCouldNotBeCompleted"), oWebE);

            }

            if (!string.IsNullOrEmpty(m_xSystemLoginID) && m_iUserID == 0) //This is an initial sync - determine correct User ID
            {
                //get user id from system login name
                int iUserID = m_oSyncServer.GetUserID(m_xSystemLoginID, m_xDBServer, m_xDBName, Environment.MachineName); //GLOG 5863

                if (iUserID == 0)
                    throw new LMP.Exceptions.UserException("Error_CouldNotRetrieveUserIDFromSystemLoginName");

                this.m_iUserID = iUserID;

                LMP.Trace.WriteNameValuePairs("m_xSystemLoginID", m_xSystemLoginID, "m_iUserID", m_iUserID);
            }
            //check that the local db meets the minimum
            //server-specified requirements for synchronization-
            //was originally called from UserSyncClient constructor,
            //but has been moved here to avoid unnecessary network chatter - dcf 01/07/11
            if (!this.MeetsMinimumVersionRequirements)
            {
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                LMP.Exceptions.SyncRequirementsException oE = new LMP.Exceptions.SyncRequirementsException(
                    LMP.Resources.GetLangString("Error_SyncClientVersionIsTooOld"));

                WriteErrorToLog("", oE);

                return;
            }

            if (!this.ServerIsSyncEnabled)
            {
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                return;
            }
            //JTS 3/31/10: Reset connection to use ForteSync.mdb
            SetLocalConnection(false);
            //delete previous bum sync file, if necessary
            if (File.Exists(this.LocalDataDirectory + @"\ForteSyncErr.mdb"))
                File.Delete(this.LocalDataDirectory + @"\ForteSyncErr.mdb");

            LMP.Trace.WriteNameValuePairs("m_iUserID", m_iUserID);

            if (m_iUserID == LMP.Data.ForteConstants.mpGuestPersonID)
            {
                //Sync not allowed for Guest user
                LMP.Trace.WriteInfo("Synch not allowed for guest user");
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                return;
            }
            else if (LMP.Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing") == "1")
            {
                //Another user sync is already in progress
                LMP.Trace.WriteInfo("User Synch Already In Progress");
                return;
            }
            else
            {
                //GLOG 4462: log error if Server Metadata value can not be retrieved
                bool bAdminPublishInProgress = m_oSyncServer.AdminPublishInProgress(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                datAdminPublish = m_oSyncServer.GetAdminPublishedTime(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                if (datAdminPublish.Year == DateTime.MaxValue.Year)
                {
                    LMP.Exceptions.ServerException oE = new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_ServerLoginFailed"));
                    WriteErrorToLog("Could not retrieve Server Last Published Time.", oE, this.LastSuccessfulUserSync);
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                    return;
                }
                //An Admin sync to the Network DB is currently in progress or has not yet occurred
                if (bAdminPublishInProgress)
                {
                    LMP.Trace.WriteInfo("Admin synch is currently in progress.");
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                    return;
                }
                else if (datAdminPublish.Date == this.MinSyncCompareDate.Date)
                {
                    LMP.Trace.WriteInfo("Cannot execute a user synch.  No admin synch to this database has ever executed");
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                    return;
                }
            }

            datAdminPublish = DateTime.Now;
            string xSyncDB = null;
            bool bFirstSync = false;

			//GLOG 7546
            string xLogMethod = "Desktop Synch: ";
            if (bCalledFromOnDemand)
                xLogMethod = "On Demand Synch: ";
            Data.Application.WriteDBLog(xLogMethod + " Start");
            try
            {
                //store current published time - check this hastn't changed at end of user sync
                datAdminPublish = m_oSyncServer.GetAdminPublishedTime(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName);

                Trace.WriteNameValuePairs("datAdminPublish", datAdminPublish.ToString());

                //mark that a sync is occurring
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "1");

                xSyncDB = this.LocalDataDirectory + @"\ForteSync.mdb";

                if (File.Exists(xSyncDB))
                {
                    if (GetLocalMetaData(mpSyncInProgressMetadata) != "")
                    {
                        //GLOG 3684: A previous Sync did not complete successfully
                        //Discard existing ForteSync.mdb and start over
                        File.Delete(this.LocalDataDirectory + @"\ForteSync.mdb");
                        //Also delete ForteSyncCompressed.mdb if it exists
                        if (File.Exists(this.LocalDataDirectory + @"\ForteSyncCompressed.mdb"))
                        {
                            File.Delete(this.LocalDataDirectory + @"\ForteSyncCompressed.mdb");
                        }
                    }
                    else
                    {
                        //JTS 12/06/08: Get Handle to Word Window for displaying Messagebox
                        IntPtr iHandle = LMP.OS.GetWordHandle();
                        if (iHandle != IntPtr.Zero)
                        {
                            LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                            if (Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName) != "1")
                            {
                                //message that previous sync hasn't been installed
                                //has not been displayed - we show this only once
                                //per executed sync - show now
                                //sync file still exists - sync can't execute
                                MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Error_PreviousSyncNotInstalled"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //Set flag so message won't keep being displayed
                                Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName, "1");
                            }
                            return;
                        }
                        else
                        {
                            //sync is complete - set sync db as Forte.mdb
                            try
                            {
                                LMP.Data.Application.InstallUpdatedDBIfNecessary(false);
                            }
                            catch (System.Exception oE)
                            {
                                if (oE.InnerException.Message.Contains("'Databases'"))
                                {
                                    //GLOG 4378: If unreadable ForteSync.mdb from previous sync was 
                                    //encountered and deleted, log error and continue
                                    WriteErrorToLog("Corrupted ForteSync.mdb has been discarded.", oE.InnerException);
                                }
                                else
                                {
                                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                                    //Forte.mdb could not be replaced for some reason
                                    //GLOG 6254
                                    WriteErrorToLog("Could not replace " + LMP.Data.Application.UserDBName + ".", oE, this.LastSuccessfulUserSync);
                                    LMP.Error.Show(oE, LMP.Resources.GetLangString(
                                        "Error_SyncCouldNotBeCompleted"));
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //JTS 12/05/11:  This block moved here so that Firm application settings will
                    //be read even if an existing ForteSyncCompressed.mdb has been deleted
                    if (File.Exists(this.LocalDataDirectory + @"\ForteSyncCompressed.mdb"))
                    {
                            //GLOG 3684: A previous Sync did not complete successfully
                            //Discard existing ForteSyncCompressed.mdb and start over
                            File.Delete(this.LocalDataDirectory + @"\ForteSyncCompressed.mdb");
                    }
                    //Reset in case value has gotten out of sync
                    Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName, "0");

                    //GLOG 4488: Firm Application setting controls which action to take 
                    //if scheduled sync is occurring when Word is active
                    //Need to login to access Keyset
                    try
                    {

                        //GLOG 5473:  Don't check Firm application settings for Prompt options
                        //until after current user's ID already exists in the DB, since relations
                        //to Offices and PeopleGroups will be required to get the appropriate values
                        LMP.Data.Application.Login(this.m_iUserID, false);
                        FirmApplicationSettings oSettings = new FirmApplicationSettings(this.m_iUserID);
                        iStartPrompt = oSettings.ScheduledSyncBehavior;
                        bPromptOnCompletion = oSettings.SyncPromptOnCompletion;
                        bLogSuccess = oSettings.LogUserSyncSuccess; //GLOG 5864
                    }
                    catch { }
                    finally
                    {
                        LMP.Data.Application.Logout();
                    }

                    //JTS 12/06/08: Get Handle to Word Window for displaying Messagebox
                    IntPtr iHandle = LMP.OS.GetWordHandle();
                    //GLOG 4573: Skip this check if called from On-Demand Sync -
                    //User has already been prompted to allow the Sync
                    if (iHandle != IntPtr.Zero && !bCalledFromOnDemand)
                    {


                        switch (iStartPrompt)
                        {
                            case FirmApplicationSettings.mpScheduledSyncBehavior.PromptUser:
                                //GLOG 4488: Display prompt to allow scheduled sync only once per Word session
                                if (Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey,
                                    mpSyncPromptSessionID) != iHandle.ToString())
                                {
                                    DialogResult iRet = MessageBox.Show(new WindowWrapper(iHandle),
                                        LMP.Resources.GetLangString("Prompt_AllowScheduledUserSync"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                    if (iRet != DialogResult.Yes)
                                    {
                                        LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                                        Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncPromptSessionID, iHandle.ToString());
                                        return;
                                    }
                                }
                                else
                                {
                                    //Prompt was previously shown and 'No' selected - don't show again
                                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                                    return;
                                }

                                break;
                            case FirmApplicationSettings.mpScheduledSyncBehavior.NeverRun:
                                //Clear previous Session ID value if present
                                Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncPromptSessionID, "");

                                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                                //don't run scheduled sync
                                return;
                            default:
                                //continue to run scheduled sync
                                break;
                        }
                    }
                }

                //reset flag to show warning message that
                //previous sync hasn't been installed
                Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName, "0");
                //GLOG 4488: Clear previous Session ID value if present
                Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncPromptSessionID, "");

                if (m_oSyncTimer != null)
                    m_oSyncTimer.Stop();

                DateTime t1 = DateTime.Now;

                if (bCheckLocalDBStructure)
                {
                    //JTS 12/12/08: Make sure Local DB has latest structure - Login as Guest,
                    //since People table may not yet be populated
                    LMP.Data.Application.Login(LMP.Data.ForteConstants.mpGuestPersonID, false);
                    try
                    {
                        LMP.Data.Application.UpdateLocalDBStructureIfNecessary(false);
                    }
                    catch (System.Exception oE)
                    {
                        //Don't continue if update fails
                        LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                        throw oE;
                    }
                    finally
                    {
                        LMP.Data.Application.Logout();
                    }
                }

                CreateSyncDB();

                //JTS: 03/18/10
                xNetworkServerGUID = m_oSyncServer.GetServerGUID(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                xLocalServerGUID = this.GetLocalMetaData(mpSyncLastServerGUIDMetadata);

                bNewServerDatabase = (xNetworkServerGUID != "" && xLocalServerGUID != "" &&
                        xNetworkServerGUID != xLocalServerGUID);

                //JTS: 03/18/10 - If Source Server Database has been changed, sync existing User Data
                //to new server, then replace with new empty Distributable for Server
                if (bNewServerDatabase)
                {
                    //Get Network Distributable location from user's Firm Application Settings Keyset
                    string xDistributablePath = "";
                    LMP.Data.Application.Login(ForteConstants.mpGuestPersonID, false);
                    try
                    {
                        FirmApplicationSettings oSettings = new FirmApplicationSettings(this.m_iUserID);
                        xDistributablePath = oSettings.NetworkDistributableFolder;
                    }
                    catch { }
                    finally
                    {
                        LMP.Data.Application.Logout();
                    }
                    //Switch to new Distributable only if Location is specified in Application Settings
                    //and file exists
                    if (xDistributablePath != "" && File.Exists(xDistributablePath + @"\" + LMP.Data.Application.UserDBName))
                    {
                        string xSourcePath = xDistributablePath + @"\" + LMP.Data.Application.UserDBName;
                        string xTargetPath = this.LocalDataDirectory + @"\ForteDistributable.mdb";
                        try
                        {
                            File.Copy(xSourcePath, xTargetPath, true);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                            WriteErrorToLog("Could not copy new Distributable DB - " +
                                " Source Path=" + xSourcePath + "; Target Path=" + xTargetPath, oE);
                            throw oE;
                        }
                        //Select all existing User Data to Post to new Server
                        SyncUp(this.MinSyncCompareDate);

                        //Backup existing Fortesync.mdb
                        File.Move(this.LocalDataDirectory + @"\ForteSync.mdb", this.LocalDataDirectory + @"\Forte{" + xLocalServerGUID + @"}.mdb");
                        //Replace Fortesync.mdb with new Distributable
                        File.Move(xTargetPath, this.LocalDataDirectory + @"\ForteSync.mdb");
                        //Delete backup of previous Fortesync.mdb
                        File.Delete(this.LocalDataDirectory + @"\Forte{" + xLocalServerGUID + @"}.mdb");
                        //Reset Date Created to Metadata in new Distributable
                        this.DateCreated = GetLocalMetaData("DateCreated");
                    }
                }

                //GLOG 3684: Set Metadata indicating Sync is not completed
                SetLocalMetaData(mpSyncInProgressMetadata, "True");
                //get last sync time from metadata
                DateTime datLastSyncTime = this.LastSync;

                bFirstSync = (datLastSyncTime.Date == this.MinSyncCompareDate.Date);

                Trace.WriteNameValuePairs("bFirstSync", bFirstSync);

                //get new time of last sync down -
                //we put the time right before the sync to 
                //guarantee that changes that are posted
                //during sync down are downloaded in the subsequent sync
                DateTime datNewLastSyncTime = DateTime.Now.ToUniversalTime();
                LMP.Trace.WriteNameValuePairs("LastSync", datLastSyncTime.ToString(), "NewLastSync", datNewLastSyncTime.ToString());

                //JTS 12/09/08: Make sure People table structure matches Server
                //GLOG 5415: May need to update local People table structure, even if no Custom Fields exist on the Server
                List<string> aList = new List<string>();
                string[] aNetFields = m_oSyncServer.PeopleCustomFieldList(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                if (aNetFields != null)
                    aList = new List<string>(aNetFields);
                List<string> aCustomFields = LMP.Data.Application.GetPeopleCustomFieldList(m_oLocalDBCnn);
                //release connection
                m_oLocalDBCnn.Close();
                if (aList.Count > 0 || aCustomFields.Count > 0)
                {
                    LMP.Data.Application.UpdatePeopleTableStructure(m_oLocalDBCnn.DataSource, aList);
                }

                if (bFirstSync)
                {
                    //User already has an up-to date db, except for 
                    //People table and related records,
                    //so no need to sync up.

                    //GLOG 8240: Back to syncing People-related first
                    //Do an on-demand sync, which retrieves
                    //only data from the People table and
                    //related records
                    SyncDownPeopleRelated(datLastSyncTime);

                    string xLast = this.DateCreated;
                    xLast = String.ConvertDateToISO8601(xLast);
                    DateTime datDBCreationTime = DateTime.Parse(xLast).ToUniversalTime();
                    SyncDown(datDBCreationTime);

                    //Populate Metadata with IDs of Proxiable Users
                    this.ProxySyncList = this.CurrentProxyList;
                }
                else
                {
                    //JTS 1/05/09: If On-Demand Sync has occurred since last Scheduled Sync,
                    //use that time as the cut-off for SyncUp
                    DateTime datLastSyncUpTime = (this.LastOnDemandSync > datLastSyncTime) 
                        ? this.LastOnDemandSync : datLastSyncTime;

                    //sync up
                    SyncUp(datLastSyncUpTime);

                    //GLOG 5450:  Make Creation Date of Forte.mdb cut-off for Last Sync Time padding
                    string xCreated = this.DateCreated;
                    xCreated = String.ConvertDateToISO8601(xCreated);
                    DateTime datCreationTime = DateTime.Parse(xCreated).ToUniversalTime();

                    //Pad last sync time 5 mins to account for other simultaneous user syncs
                    datLastSyncTime = datLastSyncTime - new TimeSpan(0, 5, 0);
                    if (datLastSyncTime < datCreationTime)
                        datLastSyncTime = datCreationTime;

                    //sync down
                    SyncDown(datLastSyncTime);

                    SyncForProxies();
                }

                if (m_oSyncServer.AdminPublishInProgress(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName) ||
                    m_oSyncServer.GetAdminPublishedTime(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName) > datAdminPublish) //GLOG 5863
                {
                    //Admin published data while user sync was in progress
                    //discard SyncDB as all data may not have be current
                    if (File.Exists(this.LocalDataDirectory + @"\ForteSync.mdb"))
                        File.Delete(this.LocalDataDirectory + @"\ForteSync.mdb");
                }
                else
                {
                    //sync up was successful - write last sync time to ForteSync.mdb;
                    WriteLastSyncTime(datNewLastSyncTime, false, this.DatabaseServer, this.DatabaseName);

                    //JTS 3/18/10: Set Last Server GUID metadata
                    SetLocalMetaData(mpSyncLastServerGUIDMetadata, xNetworkServerGUID);
                    //compact ForteSync
					//GLOG 7546
                    try
                    {
                        CompactSyncDB();
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Data.Application.WriteDBLog("Error compacting DB: " + oE.Message, this.LocalDataDirectory + @"\" + LMP.Data.Application.UserDBName);
                        throw oE;
                    }

                    //GLOG 3684: Clear Metadata to indicate all Sync steps have completed
                    SetLocalMetaData(mpSyncInProgressMetadata, "");
                    //JTS 12/06/08: Get Handle to Word Window for displaying Messagebox
                    IntPtr iHandle = LMP.OS.GetWordHandle();
                    if (iHandle != IntPtr.Zero)
                    {
                        //GLOG 5392: Don't display message if turned off in Application Settings
                        if (bPromptOnCompletion)
                        {
                            //alert user that sync is complete
                            MessageBox.Show(new WindowWrapper(iHandle),
                                LMP.Resources.GetLangString("Msg_UserSyncComplete"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        //Set flag so additional messages won't appear this session
                        Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName, "1");
                    }
                    else
                    {
                        LMP.Data.Application.InstallUpdatedDBIfNecessary(false);
                    }
                }

                UpdateRibbonXMLIfNecessary();

                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                LMP.Benchmarks.Print(t1, "Synchronization for user " + this.UserID);
                //GLOG 5864: Log only if specified in Application Settings
                if (bLogSuccess)
                    m_oSyncServer.LogEvent("Scheduled synchronization for user " + this.UserID + " completed.");
            }
            catch (System.Exception oE)
            {
                //mark that a sync is no longer occurring
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                //cleanup bum sync db
                if (File.Exists(this.LocalDataDirectory + @"\ForteSync.mdb"))
                {
                    LMP.Data.Application.WriteDBLog("Renaming ForteSync.mdb to ForteSyncErr.mdb", this.LocalDataDirectory + @"\ForteSync.mdb"); //GLOG 7546
                    File.Move(this.LocalDataDirectory + @"\ForteSync.mdb",
                        this.LocalDataDirectory + @"\ForteSyncErr.mdb");
                }

                Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, mpSyncWarningValueName, "0");

                WriteErrorToLog("Could not synchronize data- " +
                    " DBName=" + this.DatabaseName + " DBServer=" + this.DatabaseServer +
                    " UserID=" + this.m_iUserID.ToString() + " datAdminPublish=" + datAdminPublish.ToString() +
                    " xSyncDB=" + xSyncDB + " bCheckLocalDBStructure=" + bCheckLocalDBStructure + 
                    " bFirstSync=" + bFirstSync + " datLastSync=" + this.LastSync, oE, this.LastSuccessfulUserSync);
                throw oE;
            }
            finally
            {
                //mark that a sync is no longer occurring
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                Trace.WriteInfo("Sync completed successfuly.");
                Data.Application.WriteDBLog(xLogMethod + "Sync complete", this.LocalDataDirectory + @"\" + LMP.Data.Application.UserDBName); //GLOG 7546
                //GLOG 6044: Trim entries older than 2 weeks if log size is greater than 1Meg
                TrimErrorLog(1024000, DateTime.Now.AddDays(-14));
                if (m_oSyncTimer != null)
                    m_oSyncTimer.Start();

            }
        }

        /// <summary>
        /// updates ForteRibbon.xml with the latest
        /// ribbon xml from the administrator
        /// </summary>
        private void UpdateRibbonXMLIfNecessary()
        {
            if (LMP.Utility.InDevelopmentMode)
            {
                LMP.Data.LocalConnection.ConnectToDB(false);

                //get xml and last edit time of ribbon xml segment
                string xSQL = "SELECT XML, LastEditTime FROM Segments WHERE Name='****RibbonXML****'";
                System.Collections.ArrayList oList = SimpleDataCollection.GetArray(xSQL);

                //if segment does not exist, exit
                if (oList.Count == 0)
                    return;

                DateTime dRibbonXMLEditTime = DateTime.Parse((((object[])oList[0])[1]).ToString());
                string xXML = (((object[])oList[0])[0]).ToString();

                //check if ribbon file exists in DB directory
                FileInfo oFile = new FileInfo(LMP.Data.Application.PublicDataDirectory + "\\ForteRibbon.xml"); //JTS 3/28/13

                DateTime dFileEditTime = DateTime.Parse("1/1/2000");

                //if so, get last edit time of file
                if (oFile.Exists)
                    dFileEditTime = oFile.LastWriteTime;

                //if last edit time of ribbon xml is after
                //last edit time of file or file does not exist,
                //write ribbon xml to file
                if (dRibbonXMLEditTime > dFileEditTime)
                {
                    using (StreamWriter oSW = oFile.CreateText())
                    {
                        oSW.Write(xXML);
                        oSW.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Check if there are new proxiable users that require sync
        /// </summary>
        private void SyncForProxies()
        {
            string xCurProxyList = null;
            string xNewProxyList = null;

            //JTS 9/22/09: Do not sync proxies if disabled in application settings,
            //and clear out any previously saved Proxies from Metadata
            FirmApplicationSettings oSettings = new FirmApplicationSettings(this.m_iUserID);
            if (!oSettings.AllowProxying)
            {
                if (this.ProxySyncList != "")
                    this.ProxySyncList = "";
                return;
            }

            //check that we can connect to the server IP
            if (!this.ServerIsAvailable())
            {
                //GLOG 4227 - Throw WebException for proper handling by DesktopSyncProcess
                System.Net.WebException oE = new System.Net.WebException(LMP.Resources.GetLangString(
                    "Error_CouldNotConnectToServerApp"));

                throw oE;
            }

            //check that the local db meets the minimum
            //server-specified requirements for synchronization-
            //was originally called from UserSyncClient constructor,
            //but has been moved here to avoid unnecessary network chatter - dcf 01/07/11
            if (!this.MeetsMinimumVersionRequirements)
            {
                LMP.Exceptions.SyncRequirementsException oE = new LMP.Exceptions.SyncRequirementsException(
                    LMP.Resources.GetLangString("Error_SyncClientVersionIsTooOld"));

                throw oE;
            }

            try
            {
                xCurProxyList = this.ProxySyncList;
                xNewProxyList = this.CurrentProxyList;
                if (xNewProxyList != "")
                {
                    string[] aNewProxies = xNewProxyList.Split('|');
                    foreach (string xProxyID in aNewProxies)
                    {
                        //If Proxiable user ID has been added,
                        //need to do a full sync for that user to get
                        //all appropriate records
                        if (!xCurProxyList.Contains(xProxyID))
                        {
                            //JTS 8/11/09: Include all dates, since user records may pre-date creation of Distributable DB
                            SyncDown(this.MinSyncCompareDate, Int32.Parse(xProxyID));
                        }
                    }
                }
                //Update metadata with current list
                this.ProxySyncList = xNewProxyList;
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not perform proxy syncUser=" + m_iUserID.ToString() +
                    " LogonUser=" + this.UserID + " DBName=" + this.DatabaseName + 
                    " DBServer=" + this.DatabaseServer, oE);
                throw oE;
            }
        }
        public void ExecuteOnDemandSync()
        {
            ExecuteOnDemandSync(false);
        }
        // <summary>
        // execute a user synchronization with user tables only.
        // </summary>
        public void ExecuteOnDemandSync(Boolean bUploadOnly)
        {
            try
            {
                bool bLogSuccess = false;
                //check that we can connect to the server IP
                IntPtr iHandle = LMP.OS.GetWordHandle();

                //check that we can connect to the server IP
                if (!this.ServerIsAvailable())
                {
                    //GLOG 4227 - Throw WebException for proper handling by DesktopSyncProcess
                    System.Net.WebException oE = new System.Net.WebException(LMP.Resources.GetLangString(
                        "Error_CouldNotConnectToServerApp"));

                    throw oE;
                }

                //JTS 2/8/12:  Moved from Constructor
                try
                {
                    if (m_oSyncServer == null)
                    {
                        // transparent proxy
#if UseConsoleServer
                m_oSyncServer = (LMP.MacPac.Sync.ISyncServer)RemotingServices.Connect(
                    typeof(LMP.MacPac.Sync.ISyncServer), "tcp://localhost:" + 65300 + "/DataSynchronizer");
#else
                        m_oSyncServer = (ISyncServer)Activator.GetObject(
                            typeof(ISyncServer), "http://" + this.IISIPAddress + "/" + this.ServerAppName + "/DataSynchronizer.rem");
#endif

                        //will throw System.Net.WebException if unable to connect to remove server
                        Trace.WriteNameValuePairs("m_oSyncServer=", m_oSyncServer.ToString());
                    }
                }
                catch (System.Net.WebException oWebE)
                {
                    //JTS 5/7/09: Show different error for On-Demand sync
                    throw new LMP.Exceptions.ServerException(
                        LMP.Resources.GetLangString("Error_OnDemandSyncCouldNotBeExecuted"), oWebE);
                }

                if (!this.ServerIsSyncEnabled)
                {
                    //GLOG 3632: Don't display message if Word is exiting
                    if (!bUploadOnly)
                    {
                        MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Prompt_SyncNotAvailable"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    return;
                }

                //check that the local db meets the minimum
                //server-specified requirements for synchronization-
                //was originally called from UserSyncClient constructor,
                //but has been moved here to avoid unnecessary network chatter - dcf 01/07/11
                if (!this.MeetsMinimumVersionRequirements)
                {
                    //GLOG 5996
                    if (!bUploadOnly)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Error_SyncClientVersionIsTooOld"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    return;
                }

                if (m_iUserID == LMP.Data.ForteConstants.mpGuestPersonID)
                {
                    //Sync not allowed for Guest user
                    return;
                }
                else if (LMP.Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing") == "1")
                {
                    //GLOG 3632: Don't display message if Word is exiting
                    if (!bUploadOnly)
                    {
                        //Another sync is already in progress
                        //An Admin Publish action is currently in progess
                        MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Prompt_OnDemandSyncNotAllowedDuringUserSync"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    return;
                }
                else
                {
                    //GLOG 4462: if Server Metadata value can not be retrieved, notify user that
                    //Sync is not available
                    bool bAdminPublishInProgress = m_oSyncServer.AdminPublishInProgress(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863
                    DateTime datAdminPublish = m_oSyncServer.GetAdminPublishedTime(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName); //GLOG 5863);
                    if (datAdminPublish.Year == DateTime.MaxValue.Year)
                    {
                        if (!bUploadOnly)
                        {
                            MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Prompt_SyncNotAvailable"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    }
                    //GLOG 3632: Don't display message if Word is exiting
                    if (!bUploadOnly && (bAdminPublishInProgress || datAdminPublish.Date == this.MinSyncCompareDate.Date))
                    {
                        //An Admin Publish action is currently in progess
                        MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Prompt_OnDemandSyncNotAllowedDuringAdminSync"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                if ((m_oSyncServer.GetAdminPublishedTime(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName) > this.LastSync) //GLOG 5863
                        //JTS: 03/18/10 - Treat changed Server GUID same as new Admin Publish
                        || ((m_oSyncServer.GetServerGUID(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName) != "" && 
                            (this.GetLocalMetaData(mpSyncLastServerGUIDMetadata) != "") &&
                            (m_oSyncServer.GetServerGUID(this.DatabaseServer, this.DatabaseName, Environment.UserName, Environment.MachineName) //GLOG 5863
                            != this.GetLocalMetaData(mpSyncLastServerGUIDMetadata)))))
                {
                    DialogResult iRet = DialogResult.No;
                    //GLOG 3623: Don't prompt if Word is exiting - just return
                    if (!bUploadOnly)
                    {
                        //JTS 1/5/09: New Admin Data has been published
                        //Don't run On-Demand sync until this new data has been retrieved
                        iRet = MessageBox.Show(new WindowWrapper(iHandle), LMP.Resources.GetLangString("Prompt_OnDemandSyncNotAllowedBeforeUserSync"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    if (iRet == DialogResult.Yes)
                    {
                        //Reset ConnectionString to use ForteSync.mdb
                        this.SetLocalConnection(false);
                        //GLOG 4192: Don't need to recheck local DB structure since this will have
                        //already been done at start of session
                        ExecuteSync(false, true); //GLOG 4573
                        return;
                    }
                    else
                    {
                        return;
                    }
                }

                DateTime datLastSyncTime = DateTime.MinValue;

                try
                {
                    if (m_oSyncTimer != null)
                        m_oSyncTimer.Stop();

                    DateTime t1 = DateTime.Now;
                    //mark that a sync is occurring
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "1");

                    //get last sync time from metadata
                    //JTS 1/5/09: Use either LastSync or LastOnDemandSync as the cut-off, whichever is later
                    datLastSyncTime = this.LastOnDemandSync > this.LastSync ? this.LastOnDemandSync : this.LastSync;
                    LMP.Trace.WriteNameValuePairs("LastSync", datLastSyncTime.ToString());
                    //synch up
					//GLOG 7546
                    string xSyncType = "On Demand Synch";
                    if (bUploadOnly)
                        xSyncType = "On Demand Upload";
                    LMP.Data.Application.WriteDBLog("Start " + xSyncType, m_oLocalDBCnn.DataSource);
                    SyncUp(datLastSyncTime);

                    //GLOG 3623: Skip for Upload-only sync
                    if (!bUploadOnly)
                    {
                        //sync down
                        SyncDown(true, datLastSyncTime);
                        //GLOG 3523: Perform Proxy Sync for any newly-added Proxiable users
                        SyncForProxies();
                        //sync was successful - write last on-demand sync time to Forte.mdb;
                        WriteLastSyncTime(DateTime.Now.ToUniversalTime(), true, this.DatabaseServer, this.DatabaseName);
                    }
                    string xElapsedTime = LMP.Benchmarks.ElapsedTime(t0);

                    LMP.Benchmarks.Print(t1, "Synchronization for " + this.UserID);
                    //GLOG 3623
                    FirmApplicationSettings oSettings = new FirmApplicationSettings(this.UserID);
                    //GLOG 5864: Log only if specified in Application Settings
                    if (oSettings.LogUserSyncSuccess)
                    {
                        if (bUploadOnly)
                            m_oSyncServer.LogEvent("On Demand upload for user " + this.UserID + " completed.");
                        else
                            m_oSyncServer.LogEvent("On Demand synchronization for user " + this.UserID + " completed.");
                    }
                    LMP.Data.Application.WriteDBLog(xSyncType + " completed", m_oLocalDBCnn.DataSource); //GLOG 7546
                }
                catch (System.Exception oE)
                {
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                    WriteErrorToLog("Could not synchronize data - User=" + m_iUserID.ToString() +
                    " LastSyncTime=" + datLastSyncTime.ToString() + " LogonUser=" + this.UserID +
                    " DBName=" + this.DatabaseName + " DBServer=" + this.DatabaseServer, oE);
                    throw oE;
                }
                finally
                {
                    //mark that a sync is occurring
                    LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");

                    if (m_oSyncTimer != null)
                        m_oSyncTimer.Start();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                throw oE;
            }
        }
        /// <summary>
        /// sets a timer that will cause a 
        /// synchronization to occur periodically
        /// </summary>
        public void StartPeriodicSync()
        {
            try
            {
                if (m_oSyncTimer == null)
                {
                    //setup timer to perform synchronization
                    m_oSyncTimer = new System.Timers.Timer(60000);

                    //synchronize on timer interval
                    m_oSyncTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnSyncTimerElapsed);
                }

                m_oSyncTimer.Start();
            }
            catch (System.Exception oE)
            {
                WriteErrorToLog("Could not start periodic synchronization.", oE);
                throw oE;
            }
        }
        /// <summary>
        /// stops periodic synchronization
        /// </summary>
        public void StopPeriodicUserSync()
        {
            if (m_oSyncTimer != null)
                m_oSyncTimer.Stop();
        }
        /// <summary>
        /// sets the connection to the local access db
        /// </summary>
        private void SetLocalConnection(bool bUseForteDB) //JTS 3/31/10
        {
            //get location of local db
            string xDBPath = this.LocalDataDirectory;
            
            //open connection to local db
            if (bUseForteDB)
            {
                m_oLocalDBCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDBPath +
                    @"\" + LMP.Data.Application.UserDBName + ";Jet OLEDB:System database=" +
                    LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"); //GLOG 6718
            }
            else //using ForteSync.mdb
            {
                m_oLocalDBCnn = new OleDbConnection(
                    @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xDBPath +
                    @"\ForteSync.mdb;Jet OLEDB:System database=" +
                    LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"); //GLOG 6718
            }
        }
        /// <summary>
        /// creates a copy of the Forte.mdb 
        /// that is the target of the sync
        /// </summary>
        private void CreateSyncDB()
        {
            t0 = DateTime.Now;

            Trace.WriteInfo("Creating synch database: " + this.LocalDataDirectory + @"\ForteSync.mdb");
            Data.Application.WriteDBLog("CreateSyncDB: Before Synch DB creation", this.LocalDataDirectory + @"\" + LMP.Data.Application.UserDBName); //GLOG 7546
            //create copy of db as the target of the sync -
            FileInfo oF = new FileInfo(this.LocalDataDirectory + @"\" + LMP.Data.Application.UserDBName);
            oF.CopyTo(this.LocalDataDirectory + @"\ForteSync.mdb", true);
            Data.Application.WriteDBLog("CreateSyncDB: After Synch DB creation", this.LocalDataDirectory + @"\ForteSync.mdb"); //GLOG 7546

            LMP.Benchmarks.Print(t0);
        }
        private void CompactSyncDB()
        {
            DateTime t0 = DateTime.Now;

            //GLOG : 8666 : JSW
            //JetEngine does not work in 64-bit process
            JRO.JetEngine oJRO = null;

            //make sure ForteSync.mdb exists
            if (File.Exists(this.LocalDataDirectory + @"\ForteSync.mdb"))
            {
                Data.Application.WriteDBLog("CompactSyncDB: Before compaction", this.LocalDataDirectory + @"\ForteSync.mdb"); //GLOG 7546
                //compress the db
                if (Registry.Is64Bit())
                {
                    //copy db with no compression
                    File.Copy(this.LocalDataDirectory + @"\ForteSync.mdb", this.LocalDataDirectory + @"\ForteSyncCompressed.mdb", true);
                    Data.Application.WriteDBLog("CopySyncDB: After copy", this.LocalDataDirectory + @"\ForteSyncCompressed.mdb"); 
                }
                else
                {
                    oJRO = new JRO.JetEngine();
                    oJRO.CompactDatabase(@"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        this.LocalDataDirectory +
                        @"\ForteSync.mdb", @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" +
                        this.LocalDataDirectory + @"\ForteSyncCompressed.mdb");
                    Data.Application.WriteDBLog("CompactSyncDB: After compaction", this.LocalDataDirectory + @"\ForteSyncCompressed.mdb"); //GLOG 7546
                }
                //delete original
                File.Delete(this.LocalDataDirectory + @"\ForteSync.mdb");
                Data.Application.WriteDBLog("CompactSyncDB: After deletion", this.LocalDataDirectory + @"\ForteSync.mdb"); 

                //rename new db to old name
                File.Move(this.LocalDataDirectory + @"\ForteSyncCompressed.mdb", 
                    this.LocalDataDirectory + @"\ForteSync.mdb");
                Data.Application.WriteDBLog("CompactSyncDB: After replacement of Sync DB", this.LocalDataDirectory + @"\ForteSync.mdb"); //GLOG 7546

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// writes last sync time to ForteSync.mdb
        /// </summary>
        private void WriteLastSyncTime(DateTime datLastSync, bool bOnDemand, string xServerName, string xDatabaseName)
        {
            string xDateString = String.ConvertDateToISO8601(datLastSync.ToString());
            if (bOnDemand)
                SetLocalMetaData("LastSyncOnDemand" + this.UserID.ToString(),
                    xDateString + "|" + xServerName + "|" + xDatabaseName);
            else
            {
                SetLocalMetaData("LastSync" + this.UserID.ToString(),
                    xDateString + "|" + xServerName + "|" + xDatabaseName);
                LMP.Registry.SetCurrentUserValue(LMP.Data.ForteConstants.mpSyncRegKey, mpLastUserSyncValueName, xDateString); 
            }
        }
        private void SetLocalMetaData(string xKeyName, string xValue)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                m_oLocalDBCnn.Open();
                using (OleDbCommand oCommand = m_oLocalDBCnn.CreateCommand())
                {
                    int iNumRowsAffected = 0;
                    oCommand.CommandText = "UPDATE Metadata SET [Value] = '" +
                        xValue +
                        "' WHERE [Name] = '" + xKeyName + "';";
                    oCommand.CommandType = CommandType.Text;
                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                    if (iNumRowsAffected == 0)
                    {
                        // There was no record to update. Add a record.
                        oCommand.CommandText = "Insert into Metadata(Name, [Value]) values('" + xKeyName + "', '"
                            + xValue + "')";
                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                m_oLocalDBCnn.Close();
                LMP.Benchmarks.Print(t0);
            }

        }
        /// <summary>
        /// Get matching Value from Forte.mdb Metadata table
        /// </summary>
        /// <param name="xKeyName"></param>
        /// <returns></returns>
        private string GetLocalMetaData(string xKeyName)
        {
            try
            {
                Trace.WriteNameValuePairs("xKeyName", xKeyName);
                m_oLocalDBCnn.Open();
                using (OleDbCommand oCommand = m_oLocalDBCnn.CreateCommand())
                {
                    oCommand.CommandText = "SELECT Value FROM Metadata AS m WHERE m.Name='" + xKeyName + "';";
                    oCommand.CommandType = CommandType.Text;
                    string xResult = oCommand.ExecuteScalar().ToString();
                    if (!string.IsNullOrEmpty(xResult))
                        return xResult;
                    else
                        return "";
                }
            }
            catch(System.Exception oE)
            {
                return "";
            }
            finally
            {
                m_oLocalDBCnn.Close();
            }
        }
        #endregion
        #region *************************event handlers*************************
        private void OnSyncTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //prevent timer from refiring until 
                //we have processed this event handler
                this.m_oSyncTimer.Stop();

                //run if scheduled time is now or has past
                if (this.NextScheduledSync <= DateTime.Now.ToUniversalTime())
                    this.ExecuteSync();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                //resume timer
                this.m_oSyncTimer.Start();
            }
        }
        #endregion
    }
    /// <summary>
    /// abstract class used to derive 
    /// instantiable admin and user sync classes
    /// </summary>
    public abstract class SyncClient:IDisposable
    {
        #region *************************constants*************************
        protected const string mpEventLogSource = "MacPac10Synchronizer";
        protected static SyncProgress m_oProgressForm = null;
        protected const string mpSyncErrorLogRoot = "SyncErrors_";
        #endregion
        #region *************************enumerations*************************
        protected enum mpSyncType
        {
            Admin,
            User,
            Proxy,
            SharedSegment
        }
        #endregion
        #region *************************fields*************************
        protected DateTime t0;
        protected OleDbConnection m_oLocalDBCnn;
        protected string m_xNewLastSyncTime;
        protected ISyncServer m_oSyncServer;
        private bool m_bIsDisposed;

        protected string[] m_aUserUpTables = { "Deletions", "Assignments", "AddressFormats", "Addresses", "Offices", "People", "FavoritePeople", "Proxies", "AttyLicenses", "Permissions", "PeopleGroups", "GroupAssignments", "Countries", "States", "Counties", "Couriers", "Courts", "ExternalConnections", "Folders", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "KeySets", "Lists", "Restrictions", "Segments", "Translations", "ValueSets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };
        //JTS 4/1/2009: AttyLicenses should not be included in list for Download only, since private people may have licenses assigned
        protected string[] m_aUserDownTables = { "Assignments", "PeopleGroups", "GroupAssignments", "Addresses", "AddressFormats", "Offices", "Counties", "Countries", "Couriers", "Courts", "ExternalConnections", "Folders", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "Lists", "Restrictions", "Segments", "States", "Translations", "ValueSets" };
        protected string[] m_aSyncTablesOrdered = { "Deletions", "Assignments", "AddressFormats", "Addresses", "Offices", "People", "FavoritePeople", "Proxies", "AttyLicenses", "Permissions", "PeopleGroups", "GroupAssignments", "Countries", "States", "Counties", "Couriers", "Courts", "ExternalConnections", "Folders", "Jurisdictions0", "Jurisdictions1", "Jurisdictions2", "Jurisdictions3", "Jurisdictions4", "KeySets", "Lists", "Segments", "Translations", "ValueSets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };
        protected string[] m_aProxySyncTables = { "People", "FavoritePeople", "Proxies", "AttyLicenses", "Permissions", "GroupAssignments", "KeySets", "UserFolders", "UserFolderMembers", "UserSegments", "VariableSets" };

        #endregion
        #region *************************constructors*************************
        public SyncClient(){
        }
        #endregion
        #region *************************finalizer*************************
        ~SyncClient()
        {
            DisposeObjects();
        }
        #endregion
        #region *************************properties*************************
        /// <summary>
        /// gets whether the sync client
        /// has been disposed
        /// </summary>
        public bool IsDisposed
        {
            get { return m_bIsDisposed; }
        }

        #endregion
        #region *************************methods*************************
        /// <summary>
        /// delete records specified in Deletions table 
        /// </summary>
        /// <returns></returns>
        public void SyncDeletions()
        {
            SyncDeletions(false);
        }
        public void SyncDeletions(bool bFirstUserSync) //GLOG 7607
        {
            mpObjectTypes oObjectTypeID;
            string xObjectID;
            System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

            DataSet oDS = GetDeletionsDataSet();

            foreach (DataRow oRow in oDS.Tables[0].Rows)
            {
                oObjectTypeID = (mpObjectTypes)(int)oRow["ObjectTypeID"];
                xObjectID = oRow["ObjectID"].ToString();

                //GLOG 7607: If this is initial Sync of People-related data,
                //don't skip related Admin tables
                if (!bFirstUserSync ||
                    (oObjectTypeID == mpObjectTypes.Addresses ||
                    oObjectTypeID == mpObjectTypes.Office ||
                    oObjectTypeID == mpObjectTypes.Group))
                {
                    //create command object 
                    oCmd = GetDeleteCommand(oObjectTypeID, xObjectID);
                }

                try
                {
                    if (oCmd.CommandText != null && oCmd.CommandText != ""
                        && oObjectTypeID != mpObjectTypes.Addresses)
                        oCmd.ExecuteNonQuery();
                }
                catch (System.Exception oE)
                {
                    //write error to log
                    WriteErrorToLog("Could not delete item: " + oObjectTypeID + "." + xObjectID, oE);
                    throw oE;
                }
                finally
                {
                    oCmd.Dispose();
                }
            }
        }
        #endregion
        #region *************************private methods*************************
        /// <summary>
        /// returns DataSet from Deletions table 
        /// </summary>
        /// <returns>DataSet</returns>
        private DataSet GetDeletionsDataSet()
        {
            DataSet oDS = new DataSet("DeletionsDataSet");
            
            //create command object 
            using (System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand())
            {
                oCmd.Connection = m_oLocalDBCnn;

                //The records in the SyncDeletionsTmp table
                //show what has been deleted since the last SyncUp.
                //These records have also been added to the Deletions table,
                //but using them would require passing along a LastSync parameter
                //from the client through several methods.  

                oCmd.CommandText = "SELECT * FROM SyncDeletionsTmp";

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                using (oAdapter)
                {
                    oAdapter.Fill(oDS);
                }
            }
            return oDS;
        }
        /// <summary>
        /// incorporates the changes from the server
        /// </summary>
        protected void IncorporateServerEdits(DataSet oDS, DateTime dLastSync, mpSyncType iSyncType)
        {

            int iCount = 1;
            int iTableTotal = 0;
            string[] aSyncTables;

            DataTable oSchemaTable;
            AdjustDataSetTimeZone(oDS);
            //JTS 12/11/08: Display progress form for Admin download
            if (iSyncType == mpSyncType.Admin && ProgressForm != null)
            {
                iCount = iCount + ProgressForm.RowCount;
                iTableTotal = ProgressForm.RowCount + oDS.Tables.Count;
                if (iTableTotal == 0)
                    //There are no changes to post since last publish
                    ProgressForm.AddRow("Nothing to do.");
                System.Windows.Forms.Application.DoEvents();
            }
            //create a command to be used repeatedly below
            using (OleDbCommand oCmd = new OleDbCommand())
            {
                DataTable oDT = null;
                try
                {
                    m_oLocalDBCnn.Open();

                    oCmd.Connection = m_oLocalDBCnn;
                    oCmd.CommandType = CommandType.StoredProcedure;

                    if (iSyncType == mpSyncType.Proxy)
                    {
                        //GLOG 3523: For Proxy sync, we can skip cached tables, since
                        //these will already have been handled by main User sync
                        aSyncTables = m_aProxySyncTables;
                    }
                    else
                    {
                        //JTS 12/08/08: Process tables in fixed order to avoid 
                        //key violations caused by missing related values
                        aSyncTables = m_aSyncTablesOrdered;
                    }

                    for (int i = 0; i < aSyncTables.Length; i++)
                    {
                        oDT = null;
                        if (oDS.Tables.Contains(aSyncTables[i]))
                        {
                            oDT = oDS.Tables[aSyncTables[i]];
                        }
                        else
                            continue;

                        //Shouldn't repeat Deletions when performing sync for Proxiable user
                        //GLOG 8240 - Supersedes GLOGs 7607 and 8235
                        if (oDT.TableName.ToUpper() == "DELETIONS" && iSyncType == mpSyncType.Proxy)
                            continue;
                        else if (oDT.TableName.ToUpper() == "SEGMENTS" &&
                            Registry.GetMacPac10Value("DoNotSyncSegments") == "1")
                        {
                            //skip sync of the segments table -
                            //this should be the case when on a virtual desktop
                            continue;
                        }

                        //JTS 12/11/08: Update progress for Admin sync
                        if (iSyncType == mpSyncType.Admin && ProgressForm != null)
                        {
                            ProgressForm.AddRow("Synchronizing table " + oDT.TableName);
                            ProgressForm.UpdateProgressLabel("Executing " + iCount.ToString() + " of " + iTableTotal.ToString());
                            iCount++;
                            ProgressForm.Refresh();
                            System.Windows.Forms.Application.DoEvents();
                        }

                        Console.WriteLine(oDT.TableName + ": " + oDT.Rows.Count.ToString());

                        //check if tmp table exists
                        oSchemaTable = m_oLocalDBCnn.GetOleDbSchemaTable(
                            System.Data.OleDb.OleDbSchemaGuid.Columns,
                            new Object[] { null, null, "Sync" + oDT.TableName + "Tmp" });

                        if (oSchemaTable.Rows.Count > 0)
                        {
                            //tmp table exists, delete and recreate.
                            //delete it if it does
                            oCmd.CommandText = "spSync" + oDT.TableName + "TmpDelete";
                            oCmd.ExecuteNonQuery();
                        }
                        //create tmp table
                        oCmd.CommandText = "spSync" + oDT.TableName + "TmpCreate";
                        oCmd.ExecuteNonQuery();

                        //create adapter to populate dump tables in target
                        using (OleDbDataAdapter oA = new OleDbDataAdapter())
                        {
                            using (OleDbCommandBuilder oCB = new OleDbCommandBuilder(oA))
                            {
                                //use brackets to prevent any problems 
                                //with reserved keywords as field names
                                oCB.QuotePrefix = "[";
                                oCB.QuoteSuffix = "]";
                                try
                                {
                                    //create commands for adapter to populate dump tables
                                    oA.SelectCommand = new OleDbCommand("SELECT * FROM Sync" +
                                        oDT.TableName + "Tmp", m_oLocalDBCnn);
                                    oA.InsertCommand = oCB.GetInsertCommand();
                                    oA.UpdateCommand = oCB.GetUpdateCommand();

                                    //populate dump table
                                    oA.Update(oDT);

                                    //update target table with data in dump - first 
                                    //update those records that have been edited
                                    if (oDT.TableName.ToUpper() != "DELETIONS")
                                    {
                                        //JTS 1/05/09: Deletions can be skipped since
                                        //records should never get updated once created
                                        oCmd.CommandText = "spSync" + oDT.TableName + "Update";
                                        oCmd.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        if (iSyncType == mpSyncType.User && dLastSync.Date == this.MinSyncCompareDate.Date)
                                        {
                                            try
                                            {
                                                //GLOG 8240: For initial user sync, ensure only Office, Address
                                                //and Group deletions are processed and added to Deletions table
                                                oCmd.CommandText = "spSyncDeletionsRemoveNonPeopleRelated";
                                                oCmd.ExecuteNonQuery();
                                            }
                                            catch { }
                                        }
                                        try
                                        {
                                            //GLOG 5927: This query will delete records from SyncDeletionsTmp
                                            //That already exist in Deletions, to prevent re-executing previous
                                            //Deletions when multiple users share the same Forte.mdb
                                            oCmd.CommandText = "spSyncDeletionsDeleteDupes";
                                            oCmd.ExecuteNonQuery();
                                        }
                                        catch { }
                                    }

                                    //add new records
                                    oCmd.CommandText = "spSync" + oDT.TableName + "Add";
                                    int iRet = oCmd.ExecuteNonQuery();

                                    LMP.Trace.WriteNameValuePairs("oDT.TableName",
                                        oDT.TableName + " (" + oDT.Rows.Count.ToString() + ")");

                                    if (oDT.TableName.ToUpper() == "DELETIONS")
                                    {
                                        //deleted obsolete records
                                        //GLOG 7607
                                        //GLOG 8240
                                        SyncDeletions(iSyncType == mpSyncType.User && dLastSync.Date == this.MinSyncCompareDate.Date);
                                    }
                                }
                                catch (System.Exception oE)
                                {
                                    //write server data to file for troubleshooting purposes
                                    oDS.WriteXml(LMP.Data.Application.WritableDBDirectory + "\\ServerData.log", //JTS 3/28/13
                                        XmlWriteMode.IgnoreSchema);

                                    //write error to log
                                    WriteErrorToLog("Could not incorporate server edits- TableName=" +
                                        oDT.TableName + " CommandText=" + oCmd.CommandText + ".", oE);
                                    throw oE;
                                }
                            }
                        }
                    }

                    //if we got here, all server edits were successfully incorporated-
                    //delete temp tables - we delete after all data incorporation
                    //so that temp tables don't get deleted if an error occurs while
                    //incorporating data.  this gives us an additional troubleshooting
                    //tool, as we can see the data that was dumped into the temp tables
                    foreach (DataTable oTable in oDS.Tables)
                    {
                        //check if tmp table exists
                        oSchemaTable = m_oLocalDBCnn.GetOleDbSchemaTable(
                            System.Data.OleDb.OleDbSchemaGuid.Columns,
                            new Object[] { null, null, "Sync" + oTable.TableName + "Tmp" });

                        if (oSchemaTable.Rows.Count > 0)
                        {
                            //delete it if it does
                            oCmd.CommandText = "spSync" + oTable.TableName + "TmpDelete";
                            oCmd.ExecuteNonQuery();
                        }
                    }
                }
                finally
                {
                    m_oLocalDBCnn.Close();
                    if (iSyncType == mpSyncType.Admin)
                    {
                        ProgressForm.Controls["btnOk"].Enabled = true;
                    }
                }
            }
        }
        protected void IncorporateSharedSegments(DataSet oDS)
        {

            DataTable oSchemaTable;
            AdjustDataSetTimeZone(oDS);

            //create a command to be used repeatedly below
            using (OleDbCommand oCmd = new OleDbCommand())
            {
                DataTable oDT = null;
                try
                {
                    m_oLocalDBCnn.Open();

                    oCmd.Connection = m_oLocalDBCnn;

                    //clear user segments table
                    oCmd.CommandText = "DELETE * FROM UserSegments";
                    oCmd.CommandType = CommandType.Text;
                    oCmd.ExecuteNonQuery();

                    if(oDS.Tables.Count > 0)
                    {
                        //the rest of the commands are stored procedures
                        oCmd.CommandType = CommandType.StoredProcedure;

                        oDT = oDS.Tables[0];

                        Console.WriteLine(oDT.TableName + ": " + oDT.Rows.Count.ToString());

                        //check if tmp table exists
                        oSchemaTable = m_oLocalDBCnn.GetOleDbSchemaTable(
                            System.Data.OleDb.OleDbSchemaGuid.Columns,
                            new Object[] { null, null, "SyncUserSegmentsTmp" });

                        if (oSchemaTable.Rows.Count > 0)
                        {
                            //tmp table exists, delete and recreate.
                            //delete it if it does
                            oCmd.CommandText = "spSyncUserSegmentsTmpDelete";
                            oCmd.ExecuteNonQuery();
                        }
                        //create tmp table
                        oCmd.CommandText = "spSyncUserSegmentsTmpCreate";
                        oCmd.ExecuteNonQuery();

                        //create adapter to populate dump tables in target
                        using (OleDbDataAdapter oA = new OleDbDataAdapter())
                        {
                            using (OleDbCommandBuilder oCB = new OleDbCommandBuilder(oA))
                            {
                                //use brackets to prevent any problems 
                                //with reserved keywords as field names
                                oCB.QuotePrefix = "[";
                                oCB.QuoteSuffix = "]";
                                try
                                {
                                    //create commands for adapter to populate dump tables
                                    oA.SelectCommand = new OleDbCommand(
                                        "SELECT * FROM SyncUserSegmentsTmp", m_oLocalDBCnn);
                                    oA.InsertCommand = oCB.GetInsertCommand();
                                    oA.UpdateCommand = oCB.GetUpdateCommand();

                                    //populate dump table
                                    oA.Update(oDT);

                                    //add new records
                                    oCmd.CommandText = "spSyncUserSegmentsAdd";
                                    int iRet = oCmd.ExecuteNonQuery();

                                    LMP.Trace.WriteNameValuePairs("oDT.TableName",
                                        oDT.TableName + " (" + oDT.Rows.Count.ToString() + ")");
                                }
                                catch (System.Exception oE)
                                {
                                    //write server data to file for troubleshooting purposes
                                    oDS.WriteXml(LMP.Data.Application.WritableDBDirectory + "\\ServerData.log", //JTS 3/28/13
                                        XmlWriteMode.IgnoreSchema);

                                    //write error to log
                                    WriteErrorToLog("Could not incorporate server edits- TableName=" +
                                        oDT.TableName + " CommandText=" + oCmd.CommandText + ".", oE);
                                    throw oE;
                                }
                            }
                        }
                    }

                    //if we got here, all server edits were successfully incorporated-
                    //delete temp tables - we delete after all data incorporation
                    //so that temp tables don't get deleted if an error occurs while
                    //incorporating data.  this gives us an additional troubleshooting
                    //tool, as we can see the data that was dumped into the temp tables -

                    //check if tmp table exists
                    oSchemaTable = m_oLocalDBCnn.GetOleDbSchemaTable(
                        System.Data.OleDb.OleDbSchemaGuid.Columns,
                        new Object[] { null, null, "SyncUserSegmentsTmp" });

                    if (oSchemaTable.Rows.Count > 0)
                    {
                        //delete it if it does
                        oCmd.CommandText = "spSyncUserSegmentsTmpDelete";
                        oCmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    m_oLocalDBCnn.Close();
                }
            }
        }
        /// <summary>
        /// builds command object to delete obsolete records 
        /// </summary>
        /// <param name="oObjectTypeID"></param>
        /// <param name="xObjectID"></param>
        /// <returns>SqlCommand</returns>
        private OleDbCommand GetDeleteCommand(mpObjectTypes oObjectTypeID, string xObjectID)
        {
            System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
            string[] aParams;
            char cSep = '.';
            LMP.Trace.WriteNameValuePairs("oObjectTypeID", (int)oObjectTypeID, "xObjectID", xObjectID);
            OleDbParameter oParam = new OleDbParameter();

            oCmd.Connection = m_oLocalDBCnn;
            oCmd.CommandType = CommandType.StoredProcedure;

            switch (oObjectTypeID)
            {
                case mpObjectTypes.UserSegment:
                    oCmd.CommandText = "spUserSegmentsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Segment:
                case mpObjectTypes.Segments:
                    //GLOG 6458: Don't process Segment deletions if Segments are not being synced
                    if (Registry.GetMacPac10Value("DoNotSyncSegments") != "1")
                    {
                        oCmd.CommandText = "spSegmentsDelete";
                        oParam = new OleDbParameter("ID", xObjectID);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.ContentFolder:
                    oCmd.CommandText = "spFoldersDelete";
                    oParam = new OleDbParameter("FolderID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.VariableSet:
                case mpObjectTypes.VariableSets:
                    oCmd.CommandText = "spVariableSetsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.UserFolderMembers:
                    oCmd.CommandText = "spUserFolderMembersDelete";
                    //2 params.
                    aParams = xObjectID.Split(cSep);
                    oParam = new OleDbParameter("ID1", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("ID2", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.AdminFolderMembers:
                    oCmd.CommandText = "spFolderMembersDelete";
                    //2 params.
                    aParams = xObjectID.Split(cSep);
                    oParam = new OleDbParameter("FolderID", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("SegmentID", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.UserFolders:
                    oCmd.CommandText = "spUserFoldersDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Person:
                case mpObjectTypes.People:
                    //do nothing, table has a usage state field.
                    break;
                case mpObjectTypes.Group:
                    oCmd.CommandText = "spPeopleGroupsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Proxy:
                case mpObjectTypes.Proxies:
                    //im here
                    oCmd.CommandText = "spProxyDelete";
                    aParams = xObjectID.Split(cSep);
                    oParam = new OleDbParameter("User", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("Proxy", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.User:
                    break;
                case mpObjectTypes.Alias:
                    break;
                case mpObjectTypes.Office:
                case mpObjectTypes.Offices:
                    //do nothing, table has a usage state field.
                    break;
                case mpObjectTypes.Courier:
                case mpObjectTypes.Couriers:
                    oCmd.CommandText = "spCouriersDelete";
                    oParam = new OleDbParameter("CourierID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Court:
                case mpObjectTypes.Courts:
                    oCmd.CommandText = "spCourtsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.OfficeAddressFormat:
                case mpObjectTypes.AddressFormats:
                    oCmd.CommandText = "spAddressFormatsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.ExternalConnections:
                    oCmd.CommandText = "spExternalConnectionsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.KeySets:
                    //GLOG 4937: Individual Keyset deletions also need to be handled
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 5)
                    {
                        oCmd.CommandText = "spKeySetDelete";
                        oParam = new OleDbParameter("ScopeID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("ScopeID2", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("OwnerID1", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("OwnerID2", aParams[3]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("Type", aParams[4]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("Culture", aParams[5]);
                        oCmd.Parameters.Add(oParam);

                    }
                    else if (aParams.GetUpperBound(0) == 0)
                    {
                        //GLOG 7977: Single Parameter will be a Segment ID
                        oCmd.CommandText = "spKeySetsDeleteByObjectIDs";
                        oParam = new OleDbParameter("ScopeID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("ScopeID2", ForteConstants.mpFirmRecordID.ToString());
                        oCmd.Parameters.Add(oParam);
                    }
                    else
                    {
                        oCmd.CommandText = "spKeySetsDeleteByEntityIDs";
                        //GLOG 7977: Office or Person ID will always be logged with 2 parameters
                        oParam = new OleDbParameter("ID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("ID2", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.Lists:
                    oCmd.CommandText = "spListsDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Restrictions:
                    break;
                case mpObjectTypes.Translations:
                    oCmd.CommandText = "spTranslationsDeleteByID";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.ValueSets:
                    oCmd.CommandText = "spValueSetDelete";
                    oParam = new OleDbParameter("SetID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Permissions:
                    //check local query, some of the field names are wrong.
                    oCmd.CommandText = "spPermissionsDeleteByObjectIDs";
                    aParams = xObjectID.Split(cSep);
                    oParam = new OleDbParameter("ObjectTypeID", aParams[0]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("ObjectID1", aParams[1]);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("ObjectID2", aParams[2]);
                    oCmd.Parameters.Add(oParam);
                    //JTS 12/31/08:  PersonID may not be included, but parameter is required for query
                    if (aParams.GetUpperBound(0) == 3)
                        oParam = new OleDbParameter("PersonID", aParams[3]);
                    else
                        oParam = new OleDbParameter("PersonID", DBNull.Value);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions0:
                    oCmd.CommandText = "spJurisdictions0Delete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions1:
                    oCmd.CommandText = "spJurisdictions1Delete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions2:
                    oCmd.CommandText = "spJurisdictions2Delete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions3:
                    oCmd.CommandText = "spJurisdictions3Delete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Jurisdictions4:
                    oCmd.CommandText = "spJurisdictions4Delete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Addresses:
                    oCmd.CommandText = "spAddressesDelete";
                    oParam = new OleDbParameter("AddressID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Assignments:
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 2)
                    {
                        //3 params - Group Permissions
                        oCmd.CommandText = "spObjectPermissionRevoke";
                        oParam = new OleDbParameter("AssignedObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("AssignedObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        if (aParams[2] == "0")
                            oParam = new OleDbParameter("Entity", DBNull.Value);
                        else
                            oParam = new OleDbParameter("Entity", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else if (aParams.GetUpperBound(0) == 1)
                    {
                        //2 params
                        oCmd.CommandText = "spAssignmentsDeleteByObjectIDs";
                        oParam = new OleDbParameter("ObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("ObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else if (aParams.GetUpperBound(0) == 3)
                    {
                        //4 params
                        oCmd.CommandText = "spAssignmentsDelete";
                        oParam = new OleDbParameter("TargetObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("TargetObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("AssignedObjectTypeID", aParams[2]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("AssignedObjectID", aParams[3]);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.AttorneyLicenses:
                    //GLOG 3691: Handle deletion normally
                    oCmd.CommandText = "spAttyLicensesDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Counties:
                    //GLOG 7678
                    oCmd.CommandText = "spCountiesDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.Countries:
                    //GLOG 7678
                    oCmd.CommandText = "spCountriesDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.States:
                    //GLOG 7678
                    oCmd.CommandText = "spStatesDelete";
                    oParam = new OleDbParameter("ID", xObjectID);
                    oCmd.Parameters.Add(oParam);
                    break;
                case mpObjectTypes.GroupAssignments:
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 1)
                    {
                        //2 params
                        oCmd.CommandText = "spPeopleGroupMembersDelete";
                        oParam = new OleDbParameter("@ObjectTypeID", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("@ObjectID", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                    }
                    else
                    {
                        oCmd.CommandText = "spPeopleGroupAssignmentsDeleteByGroupID";
                        oParam = new OleDbParameter("@GroupID", xObjectID);
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
                case mpObjectTypes.FavoritePeople:
                    //GLOG 3544: Delete Favorite Person record
                    aParams = xObjectID.Split(cSep);
                    if (aParams.GetUpperBound(0) == 1)
                    {
                        //4 params
                        oCmd.CommandText = "spFavoritePeopleDelete";
                        oParam = new OleDbParameter("@OwnerID1", aParams[0]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("@OwnerID2", "0");
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("@PersonID1", aParams[1]);
                        oCmd.Parameters.Add(oParam);
                        oParam = new OleDbParameter("@PersonID2", "0");
                        oCmd.Parameters.Add(oParam);
                    }
                    break;
            }

            return oCmd;
        }
        //JTS 3/30/10
        protected DataSet GetUnpostedEdits(string[] aTablesUp, bool bIsUser, string xLastSyncTime, string xNewSyncTime)
        {
            return GetUnpostedEdits(aTablesUp, bIsUser, xLastSyncTime, xNewSyncTime, false);
        }
        /// <summary>
        /// returns a dataset containing records with unposted edits
        /// </summary>
        /// <returns></returns>
        protected DataSet GetUnpostedEdits(string[] aTablesUp, bool bIsUser, string xLastSyncTime, string xNewSyncTime, 
            bool bPeopleRelatedOnly) //JTS 3/30/10
        {
            //get last synch time from config file - don't get
            //user-specific sync time, as we a)always want all
            //data posted, and b)edits from previous sessions (ie
            //potentially from other users) will have already been
            //posted

            if (string.IsNullOrEmpty(xLastSyncTime))
            {
                //no last sync exists - get last edit time of access file
                System.IO.FileInfo oFI = new System.IO
                    .FileInfo(m_oLocalDBCnn.DataSource);
                //GLOG 6077: Date format should be MM/dd/yyyy for SQL
                xLastSyncTime = oFI.LastWriteTime.ToUniversalTime().ToString(LMP.Culture.USEnglishCulture);
            }
            LMP.Trace.WriteNameValuePairs("xLastSyncTime", xLastSyncTime, "xNewSyncTime", xNewSyncTime);

            //create data set with data for server
            using (DataSet oDS = new DataSet("SyncUp"))
            {
                //Need to add Extended Property with TZ offset, so that Web Server can adjust
                using (OleDbDataAdapter oA = new OleDbDataAdapter())
                {
                    oA.AcceptChangesDuringFill = false;

                    //cycle through tables that need upstream sync
                    for (int i = 0; i < aTablesUp.Length; i++)
                    {
                        if (bIsUser)
                        {
                            bool bDownloadOnly = false;

                            //User shouldn't upload any data from the tables listed
                            //in the aUserTablesDownOnly array, so skip them.
                            for (int y = 0; y < m_aUserDownTables.Length; y++)
                                if (aTablesUp[i] == m_aUserDownTables[y])
                                {
                                    bDownloadOnly = true;
                                    break;
                                }

                            if (bDownloadOnly == true)
                                continue;
                        }
                        //JTS 1/7/09: Deletions will now be posted on initial full sync,
                        //but not be processed on the server side
                        //else if (xLastSyncTime == this.MinSyncCompareDate.ToString() && 
                        //    aTablesUp[i].ToUpper() == "DELETIONS")
                        //{
                        //    //skip deletions table on initial full sync
                        //    continue;
                        //}

                        //JTS 7/30/13: Limit Keysets, UserSegments and VariableSets records to those related to the current User
                        //and any People for which the current User is a Proxy
                        //GLOG 7053: Moved since it may be used for multiple tables - 8/23/13: Only for User Sync
                        string xSyncIDList = bIsUser ? ((UserSyncClient)this).UserID + "," + ((UserSyncClient)this).CurrentProxyList : "";
                        xSyncIDList = xSyncIDList.Replace("|", ",");
                        xSyncIDList = xSyncIDList.TrimEnd(',');

                        string xSQL;
                        if (!bIsUser && aTablesUp[i].ToUpper() == "KEYSETS")
                            //Don't include User Keysets in Admin Publish
                            xSQL = "SELECT * FROM " + aTablesUp[i] +
                                " WHERE (LastEditTime > #" + xLastSyncTime + "# AND OwnerID1 < 0)";
                        else if (bIsUser && aTablesUp[i].ToUpper() == "KEYSETS")
                        {
                            //Don't include Firm Keysets in User sync up
                            //GLOG 5867:  Include Keysets corresponding to additional office records for IDs in the list
                            xSQL = "SELECT k.* FROM KEYSETS AS k INNER JOIN PEOPLE AS p ON k.OwnerID1=p.ID1 AND k.OwnerID2=p.ID2 " +
                                "WHERE ((k.LastEditTime > #" + xLastSyncTime + "#) AND (p.UsageState=1) AND ((p.ID1 in (" + xSyncIDList + ")) OR (p.ID2 = 0 AND p.LinkedPersonID IN (" + xSyncIDList + "))))";
                        }
                        else if (bIsUser && (aTablesUp[i].ToUpper() == "USERSEGMENTS" || aTablesUp[i].ToUpper() == "VARIABLESETS"))
                        {
                            //GLOG 7053: Don't include shared items not owned by current User or Proxy
                            xSQL = "SELECT u.* FROM " + aTablesUp[i] + " AS u INNER JOIN PEOPLE AS p ON u.OwnerID1=p.ID1 AND u.OwnerID2=p.ID2 " +
                                "WHERE ((u.LastEditTime > #" + xLastSyncTime + "#) AND ((p.ID1 in (" + xSyncIDList + ")) OR (p.ID2 = 0 AND p.LinkedPersonID IN (" + xSyncIDList + "))))";
                        }
                        else if (bIsUser && aTablesUp[i].ToUpper() == "PEOPLE")
                        {
                            //Don't repost public people records
                            xSQL = "SELECT * FROM " + aTablesUp[i] +
                                //JTS 3/21/10: Check only ID2, not LinkedPersonID when selecting records,
                                //to ensure additional office records of public people are not included
                                " WHERE (LastEditTime > #" + xLastSyncTime + "# AND (ID2 > 0))";
                        }
                        else if (bPeopleRelatedOnly && aTablesUp[i].ToUpper() == "DELETIONS")
                        {
                            //JTS 3/30/10: If publishing after PUP, only Deletions records corresponding to
                            //People, Licenses, and Group Assignments should be included
                            xSQL = "SELECT * FROM " + aTablesUp[i] +
                            " WHERE LastEditTime > #" + xLastSyncTime + "# AND ObjectTypeID IN (200, 640, 620)";
                        }
                        else
                            xSQL = "SELECT * FROM " + aTablesUp[i] +
                            " WHERE LastEditTime > #" + xLastSyncTime + "#";

                        OleDbCommand oCmd = new OleDbCommand(xSQL, m_oLocalDBCnn);
                        oA.SelectCommand = oCmd;
                        DataTable oDT = new DataTable(aTablesUp[i]);
                        oA.Fill(oDT);
                        //Add Table to Dataset only if it contains data
                        if (oDT.Rows.Count > 0)
                        {
                            //JTS 12/08/08: Need to set DateTimeMode to Unspecified
                            //to avoid offset during serialization
                            DataColumn oCol = new DataColumn("LastPublishedTime");
                            oCol.DataType = System.Type.GetType("System.DateTime");
                            oCol.DateTimeMode = DataSetDateTime.Unspecified;
                            oCol.DefaultValue = "#" + xNewSyncTime + "#";
                            oDT.Columns.Add(oCol);
                            oDT.Columns.Remove("LastEditTime");
                            oDT.Columns["LastPublishedTime"].ColumnName = "LastEditTime";
                            LMP.Trace.WriteNameValuePairs("oDT.TableName", oDT.TableName, "oDT.Rows.Count", oDT.Rows.Count, "xLastSyncTime", xLastSyncTime, "xNewSyncTime", xNewSyncTime);
                            oDS.Tables.Add(oDT);
                        }
                    }
                }

                return oDS;
            }
        }
        public void WriteErrorToLog(string xIntro, System.Exception oE)
        {
            WriteErrorToLog(xIntro, oE, "");
        }
        public void WriteErrorToLog(string xIntro, System.Exception oE, string xLastUserSync)
        {
            //We're bypassing Event Log to avoid 'log is full' messages
            bool bUseEventLog = false;
            try
            {
                if (!bUseEventLog)
                    WriteErrorToLogFile(xIntro, oE, xLastUserSync);
                else
                    WriteErrorToEventLog(xIntro, oE);
            }
            catch { }
        }
        /// <summary>
        /// writes the specified error to the application log
        /// </summary>
        /// <param name="oE"></param>
        private void WriteErrorToEventLog(string xIntro, System.Exception oE)
        {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            oSB.AppendFormat("{0}  The following error occurred:\r\n Type: {1}\r\nDescription: {2}\r\nStack:{3}",
                xIntro, oE.ToString(), oE.Message, oE.StackTrace);
            try
            {
                EventLogPermission eventLogPermission = new EventLogPermission(EventLogPermissionAccess.Administer, ".");
                eventLogPermission.PermitOnly(); 
                EventLog.WriteEntry(mpEventLogSource, oSB.ToString(), EventLogEntryType.Error);
            }
            catch (System.Exception oLogOE)
            {
                if (oLogOE.Message == "The event log is full")
                { }
                else
                {
                    throw oLogOE;
                }
            }
        }
        //GLOG 6440: Trim SyncErrors.log by Size or Date
        public void TrimErrorLog(long lMaxSize, DateTime dMinDate)
        {
            try
            {
                string xLine;
                DateTime dCurrent;

                string xLogFile = LMP.Data.Application.WritableDBDirectory + "\\" + mpSyncErrorLogRoot + System.Environment.UserName + ".log"; //JTS 3/28/13

                FileInfo oFile = new FileInfo(xLogFile);
                if (!oFile.Exists)
                    return;
                else if (lMaxSize > 0 && oFile.Length < lMaxSize)
                    //If size of log is less than max size, there's nothing to do
                    return;
                else if (dMinDate != null)
                {
                    StreamReader oSR = new StreamReader(xLogFile);
                    //If first date in log is not before cut-off, there's nothing to do
                    xLine = oSR.ReadLine();
                    try
                    {
                        if (xLine != null && DateTime.TryParse(xLine, out dCurrent))
                        {
                            if (dCurrent > dMinDate)
                                return;
                        }
                    }
                    finally
                    {
                        //release file
                        oSR.Close();
                    }
                }
                int iStart = 0;
                //Read all lines into array
                string[] aLines = File.ReadAllLines(xLogFile);
                //Check for date lines starting from end
                for (int i = aLines.GetUpperBound(0)-1; i >= 0; i--)
                {
                    xLine = aLines[i];
                    //look for separator line - line following that will be the date
                    if (xLine.StartsWith(new string('_', 100)))
                    {
                        xLine = aLines[i + 1];
                        if (DateTime.TryParse(xLine, out dCurrent))
                        {
                            if (dCurrent < dMinDate)
                            {
                                break;
                            }
                            else
                            {
                                //Date is within range - mark as starting point
                                iStart = i + 1;
                            }
                        }
                        else if (xLine != "")
                        {
                            break;
                        }
                    }
                }
                if (iStart == 0)
                {
                    //All log entries are older than cut-off - delete log
                    File.Delete(xLogFile);
                    return;
                }
                StringBuilder oSB = new StringBuilder();
                for (int i = iStart; i <= aLines.GetUpperBound(0); i++)
                {
                    oSB.AppendLine(aLines[i]);
                }
                StreamWriter oSW = new StreamWriter(xLogFile);
                oSW.WriteLine(oSB.ToString());
                oSW.Flush();
                oSW.Close();
            }
            finally
            {
                //Ignore errors - just leave log unchanged
            }
        }
        private void WriteErrorToLogFile(string xIntro, System.Exception oE, string xLastUserSync)
        {
            //optional
            //GLOG 6440: Maintain separate sync logs per user
            string xLogFile = LMP.Data.Application.WritableDBDirectory + "\\" + mpSyncErrorLogRoot + System.Environment.UserName + ".log"; //JTS 3/28/13

            //get method detail
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat("{0}\r\n\t{1}\r\n\t{2}\r\n\t{3}\r\n{4}",
                System.DateTime.Now,
                xIntro,
                oE.Message,
                oE.StackTrace,
                new string('_', 100));
            if (!string.IsNullOrEmpty(xLastUserSync))
            {
                oSB.AppendFormat("\r\n{0}\r\n\t{1}\r\n{2}",
                    System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), //GLOG 6440
                    string.Format(LMP.Resources.GetLangString("Msg_LastSuccessfulUserSync"), Environment.UserName, xLastUserSync), 
                    new string('_', 100));
            }

            try
            {
                //write to bench.log file
                FileInfo oFile = new FileInfo(xLogFile);

                FileStream oFS;
                StreamWriter oSW;
                if (oFile.Exists)
                    oSW = oFile.AppendText();
                else
                {
                    try
                    {
                        //create new file
                        oFS = oFile.Create();
                    }
                    catch (System.Exception e)
                    {
                        throw new LMP.Exceptions.FileException(
                            Resources.GetLangString("Error_CouldNotCreateFile") +
                            oFile.FullName, e);
                    }

                    //open write stream
                    oSW = new StreamWriter(oFS);
                }

                //write to file
                oSW.WriteLine(oSB.ToString());
                oSW.Close();
            }
            catch (System.Exception oLogOE)
            {
                throw oLogOE;
            }
        }
        /// <summary>
        /// Adds Extended Property to Dataset with local system's time offset from UTC
        /// </summary>
        /// <param name="oDS"></param>
        /// <returns></returns>
        protected long SetDataSetTimeZoneOffset(DataSet oDS)
        {
            string xTimeZoneOffsetTicks = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks.ToString();
            oDS.ExtendedProperties["UTCDifference"] = xTimeZoneOffsetTicks;
            return long.Parse(xTimeZoneOffsetTicks);
        }

        /// <summary>
        /// Adjusts DateTime values in Dataset back to original value passed from client
        /// (Code from MSKB842545)
        /// </summary>
        /// <param name="dataSet"></param>
        protected void AdjustDataSetTimeZone(DataSet dataSet)
        {
            // Obtains the time difference on the sender computer that
            //remoted this dataset to the Web service.
            string sourceTicksString = "";
            try
            {
                sourceTicksString = dataSet.ExtendedProperties["UTCDifference"].ToString();
                if (string.IsNullOrEmpty(sourceTicksString))
                    return;
            }
            catch
            {
                return;
            }
            long sourceTicks = long.Parse(sourceTicksString);
            // Obtain the UTC offset for the remote computer.
            DateTime baseUTC = DateTime.Now;
            long UtcTickslocal = TimeZone.CurrentTimeZone.GetUtcOffset(baseUTC).Ticks;
            // Obtain the time difference between the sender computer and the remote computer.
            //Do nothing if client timezone is same as web server
            if (sourceTicks == UtcTickslocal)
                return;
            long ticksDifference = sourceTicks - UtcTickslocal;
            TimeSpan timespan = new TimeSpan(ticksDifference);
            LMP.Trace.WriteNameValuePairs("timespan", timespan.ToString());
            // The following code iterates through each table, and find all the columns that are 
            // DateTime columns. After identifying the columns that have to be adjusted,
            // it traverses the data in the table and adjusts the DateTime columns back to their 
            // original values. You must leave the RowState of the DataRow in the same state 
            //after making the adjustments.
            foreach (DataTable table in dataSet.Tables)
            {
                DataColumnCollection columns = table.Columns;
                int[] ColumnNumbers = new int[columns.Count];
                int ColumnNumbersIndex = 0;
                for (int i = 0; i < columns.Count; i++)
                {
                    DataColumn col = columns[i];
                    if (col.DataType == typeof(DateTime))
                    {
                        ColumnNumbers[ColumnNumbersIndex] = i;
                        ColumnNumbersIndex++;
                    }
                }
                foreach (DataRow row in table.Rows)
                {
                    switch (row.RowState)
                    {
                        case DataRowState.Unchanged:
                            AdjustDateTimeValues(row, ColumnNumbers,
                            ColumnNumbersIndex, timespan);
                            row.AcceptChanges();	// This is to make sure that the
                            // row appears to be unchanged again.
                            Debug.Assert(row.RowState == DataRowState.Unchanged);
                            break;
                        case DataRowState.Added:
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // The row is still in a DataRowState.Added state.
                            Debug.Assert(row.RowState == DataRowState.Added);
                            break;
                        case DataRowState.Modified:
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // The row is a still DataRowState.Modified.
                            Debug.Assert(row.RowState == DataRowState.Modified);
                            break;
                        case DataRowState.Deleted:
                            //   This is to make sure that you obtain the right results if 
                            //the .RejectChanges()method is called.
                            row.RejectChanges();	// This is to "undo" the delete.
                            AdjustDateTimeValues(row, ColumnNumbers, ColumnNumbersIndex, timespan);
                            // To adjust the datatime values.
                            // The row is now in DataRowState.Modified state.
                            Debug.Assert(row.RowState == DataRowState.Modified);
                            row.AcceptChanges();	// This is to mark the changes as permanent.
                            Debug.Assert(row.RowState == DataRowState.Unchanged);
                            row.Delete();
                            // Delete the row. Now, it has the same state as it started.
                            Debug.Assert(row.RowState == DataRowState.Deleted);
                            break;
                        default:
                            throw new ApplicationException
                            ("You must add a case statement that handles the new version of the dataset.");
                    }
                }
            }
        }
        protected void AdjustDateTimeValues(DataRow row, int[] ColumnNumbers, int columnCount, TimeSpan timespan)
        {
            for (int i = 0; i < columnCount; i++)
            {
                int columnIndex = ColumnNumbers[i];
                DateTime original = (DateTime)row[columnIndex];
                DateTime modifiedDateTime = original.Add(timespan);
                row[columnIndex] = modifiedDateTime;
            }
        }
        private void DisposeObjects()
        {
            if (!this.IsDisposed)
            {
                try
                {
                    if (m_oSyncServer != null)
                        m_oSyncServer = null;

                    if (m_oLocalDBCnn != null)
                    {
                        //JTS 3/30/10: Prevent unhandled exception
                        m_oLocalDBCnn.Dispose();
                        //JTS 9/05/12: Release object
                        m_oLocalDBCnn = null;
                    }
                }
                catch { }
                m_bIsDisposed = true;
            }
        }
        /// <summary>
        /// Returns arbitrary far past date that is compatible with SQL Server limits (minimum 1753)
        /// </summary>
        internal DateTime MinSyncCompareDate
        {
            get { return new DateTime(1800, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); }
        }
        protected static SyncProgress ProgressForm
        {
            get { return m_oProgressForm; }
            set { m_oProgressForm = value; }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            DisposeObjects();
            GC.SuppressFinalize(this);
            m_bIsDisposed = true;
        }
        #endregion
    }
}
