﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Reg = Microsoft.Win32.Registry;
using System.Reflection;

namespace mpConvert
{
    public enum mpPreconversionResults : int
    {
        Successful = 0,
        NoContent = 1,
        PriorPreconversion = 2,
        NotDocx = 3,
        Error = 4
    }

    internal static class Preconversion
    {
        private const string MacPacNamespace = "urn-legalmacpac-data/10";
        private const string WordNamespace = "http://schemas.microsoft.com/office/word/2003/wordml";
        private const string WordOpenXMLNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        private const string AmlNamespace = "http://schemas.microsoft.com/aml/2001/core";
        private static Random m_oRandom = null;
        private static object m_oCOM = null;
        private static Type m_oCOMType = null;
        internal static mpPreconversionResults PreconvertFile(string xFile, ref string xMessage)
        {
            return PreconvertFile(xFile, ref xMessage, "");
        }
        internal static mpPreconversionResults PreconvertFile(string xFile, ref string xMessage, string xLogFile)
        {

            FileInfo oLog = null;
            StreamWriter oSW = null;
            bool bLog = !string.IsNullOrEmpty(xLogFile);
            if (bLog)
            {
                oLog = new FileInfo(xLogFile);
                if (oLog.Exists)
                {
                    oSW = oLog.AppendText();
                }
                else
                {
                    FileStream oFS = null;
                    try
                    {
                        oFS = oLog.Create();
                    }
                    catch
                    {
                        System.Windows.Forms.MessageBox.Show(
                            "Could not create \"" + xLogFile + "\"",
                            "MacPac Preconversion Utility",
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Error);
                        return mpPreconversionResults.Error;
                    }
                    oSW = new StreamWriter(oFS);
                }
            }
            //turn on encryption - existing password will be used
            SetClientMetadataValues("", "x");

            //preconvert
            mpPreconversionResults iResult = mpPreconversionResults.NotDocx;
            string xErrorDescription = null;
            string xExt = Path.GetExtension(xFile).ToLower();
            if (xExt == ".docx")
            {
                iResult = PreconvertFileIfNecessary(xFile,
                    ref xErrorDescription);
            }

            //log result
            xMessage = null;
            switch (iResult)
            {
                case mpPreconversionResults.Successful:
                    xMessage = "Preconversion was successful.";
                    break;
                case mpPreconversionResults.NoContent:
                    xMessage = "Nothing to do. No MacPac content found.";
                    break;
                case mpPreconversionResults.NotDocx:
                    xMessage = "Not .docx file format. No preconversion necessary.";
                    break;
                case mpPreconversionResults.PriorPreconversion:
                    xMessage = "This document has already been preconverted.";
                    break;
                case mpPreconversionResults.Error:
                    xMessage = "Error: " + xErrorDescription;
                    break;
            }
            if (bLog)
            {
                xMessage = DateTime.Now + " - " + xFile + " - " + xMessage;
                oSW.WriteLine(xMessage);
                oSW.Close();
            }
            //release Random number generator
            m_oRandom = null;
            return iResult;
        }
        private static void CreateCOMObjectIfNecessary()
        {
            if (m_oCOM == null)
            {
                //Create late-bound mpCOM.cApplication object
                m_oCOMType = Type.GetTypeFromProgID("mpCOM.cApplication");
                m_oCOM = Activator.CreateInstance(m_oCOMType);
            }
        }
        private static void AddBookmarkAndDocVarsForNode(XmlNode oMPNode,
            XmlDocument oTargetXML, XmlNamespaceManager oNSMan, string xPrefix,
            XmlNode oDocVarsNode, XmlDocument oSettingsXML,
            XmlNamespaceManager oSettingsNSMan, ref int iBmkID,
            string xEncryptionPassword)
        {
            try
            {
                string xPwd = "";

                //do children first
                XmlNodeList oChildNodes = oMPNode.SelectNodes("descendant::" + xPrefix + ":*", oNSMan);
                foreach (XmlNode oChildNode in oChildNodes)
                {
                    XmlNodeList oParentMPNodes = oChildNode.SelectNodes(
                        "ancestor::" + xPrefix + ":*", oNSMan);

                    XmlNode oParentMPNode = null;

                    if (oParentMPNodes.Count > 0)
                    {
                        oParentMPNode = oParentMPNodes[oParentMPNodes.Count - 1];
                    }

                    if (oParentMPNode == oMPNode)
                    {
                        //this is a child of
                        AddBookmarkAndDocVarsForNode(oChildNode, oTargetXML, oNSMan,
                            xPrefix, oDocVarsNode, oSettingsXML, oSettingsNSMan,
                            ref iBmkID, xEncryptionPassword);
                    }
                }

                //get basename
                string xBaseName = oMPNode.Name.Replace(xPrefix + ":", "");

                string xObjectData = oMPNode.SelectSingleNode("@ObjectData").Value;

                //construct new tag id - get basename prefix
                string xBaseNamePrefix = null;

                switch (xBaseName)
                {
                    case "mSEG":
                        xBaseNamePrefix = "mps";
                        break;
                    case "mVar":
                        xBaseNamePrefix = "mpv";
                        break;
                    case "mBlock":
                        xBaseNamePrefix = "mpb";
                        break;
                    case "mDel":
                        xBaseNamePrefix = "mpd";
                        break;
                    case "mSubVar":
                        xBaseNamePrefix = "mpu";
                        break;
                    case "mDocProps":
                        xBaseNamePrefix = "mpp";
                        break;
                    case "mSecProps":
                        xBaseNamePrefix = "mpc";
                        break;
                }

                string xDocVarID = GenerateDocVarID();
                string xBookmarkName = xBaseNamePrefix + xDocVarID;
                string xObjectDBID = null;

                //object db id
                if (xBaseName == "mSEG")
                {
                    xObjectDBID = GetmSEGObjectDataValue(xObjectData, "SegmentID");
                    if (xObjectDBID.Length < 19)
                    {
                        xObjectDBID = xObjectDBID.PadLeft(19, '0');
                    }
                    xBookmarkName += xObjectDBID;
                }

                //bookmark names can only contain alphanumeric characters
                xBookmarkName = xBookmarkName.Replace('-', 'm');
                xBookmarkName = xBookmarkName.Replace('.', 'd');

                //pad to 40 characters
                xBookmarkName = "_" + xBookmarkName.PadRight(39, '0');

                //get mpo doc var name
                string xMPOVar = "mpo" + xDocVarID;

                //test for conflicts with existing bookmarks 
                //and doc vars - regenerate ID if necessary
                bool bDocVarExists = oSettingsXML.SelectSingleNode(
                    @"//w:docVar[@w:name='" + xMPOVar + "']", oSettingsNSMan) != null;

                while (bDocVarExists)
                {
                    //conflict exists, regenerate id
                    xDocVarID = GenerateDocVarID();
                    //include beginning '_'
                    xBookmarkName = xBookmarkName.Substring(0, 4) + xDocVarID + xBookmarkName.Substring(12);
                    xMPOVar = xMPOVar.Substring(0, 3) + xDocVarID;
                    xDocVarID = null;

                    //test again with new ID
                    bDocVarExists = oSettingsXML.SelectSingleNode(
                        @"//w:docVar[@w:name='" + xMPOVar + "']", oSettingsNSMan) != null;
                }

                //06/22/11: Set XMLNode Reserved Attribute to Bookmark Name without first character
                string xReserved = xBookmarkName.Substring(1);
                if (oMPNode.SelectSingleNode("@Reserved") == null)
                {
                    XmlAttribute oMPReserved = oTargetXML.CreateAttribute("Reserved");
                    oMPNode.Attributes.Append(oMPReserved);
                }
                oMPNode.SelectSingleNode("@Reserved").Value = xReserved;

                //11-29-10 (dm) - use TempID attribute to index nested adjacent tags
                XmlAttribute oTempIDAttr = oMPNode.Attributes["TempID"];
                if (oTempIDAttr != null)
                    oMPNode.Attributes.Remove(oTempIDAttr);
                if (HasAdjacentChild(oMPNode.InnerXml))
                {
                    int iTempID = 1;
                    XmlNode oChildNode = oMPNode.SelectSingleNode(
                        @".//" + xPrefix + ":*", oNSMan);
                    XmlNode oChildNodeTempID = oChildNode.SelectSingleNode("@TempID");
                    if (oChildNodeTempID == null)
                    {
                        //append TempID attribute to child's mpo doc var
                        xReserved = oChildNode.SelectSingleNode("@Reserved").Value;
                        string xChildDocVar = "mpo" + xReserved.Substring(3, 8);
                        XmlNode oChildDocVar = oSettingsXML.SelectSingleNode(
                            @"//w:docVar[@w:name='" + xChildDocVar + "']", oSettingsNSMan);
                        XmlAttribute oChildDocVarValue = oChildDocVar.Attributes["w:val"];
                        string xChildDocVarValue = oChildDocVarValue.Value;
                        int iPos = xChildDocVarValue.IndexOf("ÌÍ") + 2;
                        string xChildTagID = xChildDocVarValue.Substring(0, iPos);
                        string xEncryptedAttrs =  Decrypt(xChildDocVarValue.Substring(iPos),
                            ref xPwd);
                        xEncryptedAttrs = Encrypt(xEncryptedAttrs + "ÌÍTempID=1", xPwd);
                        oChildDocVarValue.Value = xChildTagID + xEncryptedAttrs;
                    }
                    else
                        iTempID = System.Int32.Parse(oChildNodeTempID.Value);
                    iTempID++;
                    oTempIDAttr = oTargetXML.CreateAttribute("TempID");
                    oTempIDAttr.Value = iTempID.ToString();
                    oMPNode.Attributes.Append(oTempIDAttr);
                }

                //create mpo docvar
                string xTagID = null;
                if (xBaseName == "mSubVar")
                {
                    xTagID = oMPNode.SelectSingleNode("@Name").Value;
                }
                else
                {
                    xTagID = oMPNode.SelectSingleNode("@TagID").Value;
                }

                //the rest of the value is encrypted
                string xAttrName = null;
                string xAttrs = null;

                foreach (XmlAttribute oAttr in oMPNode.Attributes)
                {
                    xAttrName = oAttr.Name;

                    if (xAttrName != "TagID" && xAttrName != "Name" && xAttrName != "DeletedScopes")
                    {
                        xAttrs += xAttrName + "=" + Decrypt(oAttr.Value, ref xPwd) + "ÌÍ";
                    }
                }

                //trim trailing separator
                xAttrs = xAttrs.TrimEnd('Ì', 'Í');

                //encrypt - this method, as used in this utility, is creating doc vars
                //only for tags embedded in delete scopes - while the delete scopes
                //attribute itself is encrypted, the attributes of the tags
                //within it are not - thus, the encryption password has been supplied
                if ((xBaseName != "mDel") && (xBaseName != "mSubVar"))
                    xAttrs = Encrypt(xAttrs, xEncryptionPassword);

                //add variable
                string xValue = xTagID + "ÌÍ" + xAttrs;

                //create doc var element
                XmlElement oNewDocVarElement = oSettingsXML.CreateElement("w:docVar",
                    WordOpenXMLNamespace);

                //create name & value attributes for element
                XmlAttribute oVarAttr = oSettingsXML.CreateAttribute("w:name",
                    WordOpenXMLNamespace);
                oVarAttr.Value = xMPOVar;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                oVarAttr = oSettingsXML.CreateAttribute("w:val",
                    WordOpenXMLNamespace);
                oVarAttr.Value = xValue;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                //add element to doc vars element
                oDocVarsNode.AppendChild(oNewDocVarElement);

                //create mpd variable
                if (xBaseName == "mSEG")
                {
                    XmlNode oDeletedScopes = oMPNode.SelectSingleNode("@DeletedScopes");
                    if (oDeletedScopes != null && !string.IsNullOrEmpty(oDeletedScopes.Value))
                    {
                        string xDeletedScopes = oDeletedScopes.Value;
                        xDeletedScopes = Decrypt(xDeletedScopes, ref xPwd);

                        //add bookmarks and doc vars for tags in deleted scopes
                        AddBookmarksAndDocVarsForDeletedScopes(oSettingsXML,
                            oSettingsNSMan, oDocVarsNode, ref iBmkID,
                            ref xDeletedScopes, xEncryptionPassword);

                        //update deleted scopes attribute
                        oDeletedScopes.Value = xDeletedScopes;

                        //encrypt
                        xDeletedScopes = Encrypt(xDeletedScopes, xEncryptionPassword);

                        int iDelPart = 0;
                        //create multiple mpd variables if necessary
                        do
                        {
                            iDelPart++;
                            string xMPDVar = "mpd" + xDocVarID + iDelPart.ToString().PadLeft(2, '0');

                            //create doc var element
                            oNewDocVarElement = oSettingsXML.CreateElement("w:docVar",
                                WordOpenXMLNamespace);

                            //create name & value attributes for element
                            oVarAttr = oSettingsXML.CreateAttribute("w:name",
                                WordOpenXMLNamespace);
                            oVarAttr.Value = xMPDVar;
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oVarAttr = oSettingsXML.CreateAttribute("w:val",
                                WordOpenXMLNamespace);
                            oVarAttr.Value = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oDocVarsNode.AppendChild(oNewDocVarElement);
                            if (xDeletedScopes.Length > 65000)
                                xDeletedScopes = xDeletedScopes.Substring(65000);
                            else
                                break;
                        } while (xDeletedScopes != "");
                    }
                }

                //create bookmark start element
                XmlElement oBookmarkStart = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

                //create name & value attributes for element
                oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkStart.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
                oVarAttr.Value = "Word.Bookmark.Start";
                oBookmarkStart.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
                oVarAttr.Value = xBookmarkName;
                oBookmarkStart.Attributes.Append(oVarAttr);

                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                bool bIsInline = (oMPNode.SelectSingleNode(
                        "descendant::w:p", oNSMan) == null);
                if (!bIsInline)
                {
                    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
                    oVarAttr.Value = "prev";
                    oBookmarkStart.Attributes.Append(oVarAttr);
                }

                //create bookmark end element
                XmlElement oBookmarkEnd = oTargetXML.CreateElement("aml:annotation", AmlNamespace);

                //create name & value attributes for element
                oVarAttr = oTargetXML.CreateAttribute("aml:id", AmlNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkEnd.Attributes.Append(oVarAttr);

                oVarAttr = oTargetXML.CreateAttribute("w:type", WordNamespace);
                oVarAttr.Value = "Word.Bookmark.End";
                oBookmarkEnd.Attributes.Append(oVarAttr);

                //oVarAttr = oTargetXML.CreateAttribute("w:name", WordNamespace);
                //oVarAttr.Value = xBookmarkName;
                //oBookmarkEnd.Attributes.Append(oVarAttr);

                //GLOG 5268 (dm) - not adding w:displacedBySDT was problematic
                //in the fax recipients table
                //if (oTargetNode.InnerXml.Contains("<w:p "))
                //{
                //Omit this attribute if using alternate method above
                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                if (!bIsInline)
                {
                    oVarAttr = oTargetXML.CreateAttribute("w:displacedBySDT", WordNamespace);
                    oVarAttr.Value = "next";
                    oBookmarkEnd.Attributes.Append(oVarAttr);
                }

                //11-29-10 (dm) - add bookmark start and end tags
                bool bIsEmpty = (oMPNode.ChildNodes.Count == 0);
                if (bIsEmpty)
                {
                    oMPNode.PrependChild(oBookmarkEnd);
                    oMPNode.PrependChild(oBookmarkStart);
                }
                else
                {
                    oMPNode.PrependChild(oBookmarkStart);
                    oMPNode.AppendChild(oBookmarkEnd);
                }

                //set next aml id
                iBmkID--;
            }
            catch (System.Exception oE)
            {
                throw new System.Exception(oE.Message, oE);
            }
        }

        private static void AddBookmarksAndDocVarsForDeletedScopes(XmlDocument oSettingsXML,
            XmlNamespaceManager oSettingsNSMan, XmlNode oDocVarsNode, ref int iBmkID,
            ref string xDeletedScopes, string xEncryptionPassword)
        {
            //get namespace prefix
            string xPrefix = GetNamespacePrefix(xDeletedScopes, MacPacNamespace);

            //create new xml document
            XmlDocument oEmbeddedXML = new XmlDocument();

            //add namespaces for XPath navigation
            XmlNamespaceManager oEmbeddedNSMan = new XmlNamespaceManager(oEmbeddedXML.NameTable);
            oEmbeddedNSMan.AddNamespace(xPrefix, MacPacNamespace);
            oEmbeddedNSMan.AddNamespace("w", WordNamespace);
            oEmbeddedNSMan.AddNamespace("aml", AmlNamespace);

            //cycle though delete scopes
            int iPos = xDeletedScopes.IndexOf("¬w:wordDocument ");
            while (iPos > -1)
            {
                //get body node
                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>", iPos) + 9;
                string xBody = xDeletedScopes.Substring(iPos, iPos2 - iPos);

                //restore XML character
                xBody = xBody.Replace("¬", "<");

                //append w:wordDocument closing tag
                xBody = xBody + "</w:wordDocument>";

                //load xml
                oEmbeddedXML.LoadXml(xBody);

                //cycle through MacPac tags, adding bookmarks and doc vars -
                //bookmark and reserved attribute get added to oEmbeddedXML,
                //while doc vars get added to oDocXML
                //get all macpac tags
                XmlNodeList oMacPacNodes = oEmbeddedXML.SelectNodes(@"//" +
                    xPrefix + ":*", oEmbeddedNSMan);

                foreach (XmlNode oNode in oMacPacNodes)
                {
                    XmlNode oParentMPNode = oNode.SelectSingleNode("ancestor::" +
                        xPrefix + ":*", oEmbeddedNSMan);

                    if (oParentMPNode == null)
                    {
                        AddBookmarkAndDocVarsForNode(oNode, oEmbeddedXML,
                            oEmbeddedNSMan, xPrefix, oDocVarsNode, oSettingsXML,
                            oSettingsNSMan, ref iBmkID, xEncryptionPassword);
                    }
                }

                //replace body node, removing w:wordDocument closing tag
                xBody = oEmbeddedXML.OuterXml;
                xDeletedScopes = xDeletedScopes.Substring(0, iPos) + xBody.Substring(0,
                    xBody.Length - 17) + xDeletedScopes.Substring(iPos2);

                //search for next scope
                iPos = xDeletedScopes.IndexOf("¬w:wordDocument ");
            }

            //replace XML character
            xDeletedScopes = xDeletedScopes.Replace("<", "¬");
        }

        /// <summary>
        /// returns a new docvar id
        /// </summary>
        /// <returns></returns>
        private static string GenerateDocVarID()
        {
            //hold in static variable - otherwise if clock has not advanced enough from last pass
            //number generated by NextDouble may be a duplicate
            if (m_oRandom == null)
                m_oRandom = new Random();
            string xDocVarID = m_oRandom.NextDouble().ToString();
            xDocVarID = xDocVarID.Substring(2, 8);
            return xDocVarID;
        }

        /// <summary>
        /// gets the value of the specified segment property
        /// </summary>
        /// <param name="xObjectData"></param>
        /// <param name="xProperty"></param>
        /// <returns></returns>
        private static string GetmSEGObjectDataValue(string xObjectData, string xProperty)
        {
            string xPwd = "";

            xObjectData = Decrypt(xObjectData, ref xPwd);

            int iPos1 = xObjectData.IndexOf(xProperty + "=");

            if (iPos1 > -1)
            {
                //property found - parse from object data
                iPos1 += xProperty.Length + 1;

                //get delimiting pipe - it may or may not exist
                int iPos2 = xObjectData.IndexOf("|", iPos1);

                string xValue = null;

                if (iPos2 > -1)
                {
                    //pipe exists - parse to pipe
                    xValue = xObjectData.Substring(iPos1, iPos2 - iPos1);
                }
                else
                {
                    //pipe doesn't exist - parse to end of string
                    xValue = xObjectData.Substring(iPos1);
                }

                return xValue;
            }

            return null;
        }

        private static bool HasAdjacentChild(string xInnerXML)
        {
            string xPrefix = GetNamespacePrefix(xInnerXML,
                MacPacNamespace);
            if (xPrefix == "")
                return false;

            //prefix is sometimes returned with extra characters
            int iPos = xPrefix.IndexOf('=');
            if (iPos > -1)
                xPrefix = xPrefix.Substring(0, iPos);

            //search for first child tag
            iPos = xInnerXML.IndexOf('<' + xPrefix + ':');
            if (iPos == -1)
                return false;

            //if run or end of paragraph occurs before child,
            //child is not adjacent
            int iPos2 = xInnerXML.IndexOf("<w:r>");
            int iPos3 = xInnerXML.IndexOf("<w:p />");
            int iPos4 = xInnerXML.IndexOf("</w:p>");
            if (((iPos2 > -1) && (iPos2 < iPos)) ||
                ((iPos3 > -1) && (iPos3 < iPos)) ||
                ((iPos4 > -1) && (iPos4 < iPos)))
                return false;

            //child is adjacent
            return true;
        }

        private static mpPreconversionResults PreconvertFileIfNecessary(string xFullName,
            ref string xErrorDescription)
        {
            int iBmkID = 800;
            bool bPreconverted = false;
            int imSEGsInPart = 0;
            bool bmSEGsFound = false;

            try
            {
                using (WordprocessingDocument wordDocument = WordprocessingDocument.Open(
                    xFullName, true))
                {
                    MainDocumentPart oDocPart = wordDocument.MainDocumentPart;
                    DocumentSettingsPart oSettingsPart =
                        oDocPart.GetPartsOfType<DocumentSettingsPart>().First();

                    //load settings xml
                    XmlDocument oSettingsXML = new XmlDocument();
                    oSettingsXML.Load(oSettingsPart.GetStream());
                    XmlNamespaceManager oSettingsNSMan = new XmlNamespaceManager(
                        oSettingsXML.NameTable);
                    oSettingsNSMan.AddNamespace("w", WordOpenXMLNamespace);

                    //get doc vars node
                    XmlNode oDocVarsNode = oSettingsXML.SelectSingleNode(@"//w:docVars",
                        oSettingsNSMan);
                    if (oDocVarsNode == null)
                    {
                        //no doc vars in xml - add doc vars node
                        XmlElement oDocVarsElement = oSettingsXML.CreateElement(
                            "w:docVars", WordOpenXMLNamespace);
                        XmlNode oParentNode = oSettingsXML.SelectSingleNode(
                            "/w:settings", oSettingsNSMan);
                        oParentNode.AppendChild(oDocVarsElement);
                        oDocVarsNode = oDocVarsElement;
                    }
                    else
                    {
                        //exit if already preconverted
                        XmlNode oMPDocVar = oDocVarsNode.SelectSingleNode(
                            "child::w:docVar[starts-with(@w:name, 'mpo') and " +
                            "contains(@w:val, 'ÌÍ')]", oSettingsNSMan);
                        if (oMPDocVar != null)
                            return mpPreconversionResults.PriorPreconversion;
                    }

                    //preconvert the main document part
                    PreconvertPackagePart(oDocPart, oDocVarsNode, oSettingsXML,
                        oSettingsNSMan, ref iBmkID, ref bPreconverted, ref imSEGsInPart);
                    if (bPreconverted)
                        return mpPreconversionResults.PriorPreconversion;
                    else if (imSEGsInPart > 0)
                        bmSEGsFound = true;

                    //preconvert the headers
                    foreach (HeaderPart oHeaderPart in oDocPart.HeaderParts)
                    {
                        PreconvertPackagePart(oHeaderPart, oDocVarsNode, oSettingsXML,
                            oSettingsNSMan, ref iBmkID, ref bPreconverted, ref imSEGsInPart);
                        if (bPreconverted)
                            return mpPreconversionResults.PriorPreconversion;
                        else if (imSEGsInPart > 0)
                            bmSEGsFound = true;
                    }

                    //preconvert the footers
                    foreach (FooterPart oFooterPart in oDocPart.FooterParts)
                    {
                        PreconvertPackagePart(oFooterPart, oDocVarsNode, oSettingsXML,
                            oSettingsNSMan, ref iBmkID, ref bPreconverted, ref imSEGsInPart);
                        if (bPreconverted)
                            return mpPreconversionResults.PriorPreconversion;
                        else if (imSEGsInPart > 0)
                            bmSEGsFound = true;
                    }

                    //save the settings part
                    if (bmSEGsFound)
                    {
                        oSettingsXML.Save(oSettingsPart.GetStream(FileMode.Create,
                            FileAccess.Write));
                        return mpPreconversionResults.Successful;
                    }
                    else
                        return mpPreconversionResults.NoContent;
                }
            }
            catch (System.Exception oE)
            {
                xErrorDescription = oE.Message;
                return mpPreconversionResults.Error;
            }
        }

        private static void PreconvertPackagePart(OpenXmlPart oPart,
            XmlNode oDocVarsNode, XmlDocument oSettingsXML,
            XmlNamespaceManager oSettingsNSMan, ref int iBmkID,
            ref bool bPreconverted, ref int imSEGsInPart)
        {
            XmlDocument oXML = new XmlDocument();
            oXML.Load(oPart.GetStream());

            //add namespace for XPath navigation
            XmlNamespaceManager oNSMan = new XmlNamespaceManager(oXML.NameTable);
            oNSMan.AddNamespace("w", WordOpenXMLNamespace);

            //add bookmarks and doc vars for each top-level mSEG
            //get all macpac segment tags
            XmlNodeList oMacPacNodes = oXML.SelectNodes(
                ".//w:customXml[@w:element=\"mSEG\"]", oNSMan);
            imSEGsInPart = oMacPacNodes.Count;
            if (imSEGsInPart > 0)
            {
                foreach (XmlNode oNode in oMacPacNodes)
                {
                    XmlNode oParentMPNode = oNode.SelectSingleNode(
                        "ancestor::w:customXml[@w:uri=\"" + MacPacNamespace + "\"]", oNSMan);
                    if (oParentMPNode == null)
                    {
                        //exit if already preconverted
                        XmlNode oCustomXMLPrNode = oNode.SelectSingleNode(
                            "child::w:customXmlPr", oNSMan);
                        XmlNode oReserved = oCustomXMLPrNode.SelectSingleNode(
                            "child::w:attr[@w:name=\"Reserved\"]", oNSMan);
                        if ((oReserved != null) &&
                            (oReserved.Attributes["w:val"].Value.Length == 39))
                        {
                            bPreconverted = true;
                            return;
                        }

                        //preconvert
                        AddBookmarkAndDocVarsForOpenXMLNode(oNode, oXML, oNSMan,
                            oDocVarsNode, oSettingsXML, oSettingsNSMan, ref iBmkID);
                    }
                }

                //save the part
                oXML.Save(oPart.GetStream(FileMode.Create, FileAccess.Write));
            }
        }

        private static void AddBookmarkAndDocVarsForOpenXMLNode(XmlNode oMPNode,
            XmlDocument oPartXML, XmlNamespaceManager oPartNSMan, XmlNode oDocVarsNode,
            XmlDocument oSettingsXML, XmlNamespaceManager oSettingsNSMan, ref int iBmkID)
        {
            try
            {
                string xPwd = "";

                //do children first
                XmlNodeList oChildNodes = oMPNode.SelectNodes("descendant::" +
                    "w:customXml[@w:uri=\"" + MacPacNamespace + "\"]", oPartNSMan);
                foreach (XmlNode oChildNode in oChildNodes)
                {
                    XmlNodeList oParentMPNodes = oChildNode.SelectNodes(
                        "ancestor::w:customXml[@w:uri=\"" + MacPacNamespace + "\"]",
                        oPartNSMan);

                    XmlNode oParentMPNode = null;

                    if (oParentMPNodes.Count > 0)
                    {
                        oParentMPNode = oParentMPNodes[oParentMPNodes.Count - 1];
                    }

                    if (oParentMPNode == oMPNode)
                    {
                        //this is a child
                        AddBookmarkAndDocVarsForOpenXMLNode(oChildNode, oPartXML,
                            oPartNSMan, oDocVarsNode, oSettingsXML, oSettingsNSMan,
                            ref iBmkID);
                    }
                }

                //get basename
                string xBaseName = oMPNode.SelectSingleNode("@w:element",
                    oPartNSMan).Value;

                //get properties node
                XmlNode oCustomXMLPrNode = oMPNode.SelectSingleNode(
                    "child::w:customXmlPr", oPartNSMan);

                //construct new tag id - get basename prefix
                string xBaseNamePrefix = null;

                switch (xBaseName)
                {
                    case "mSEG":
                        xBaseNamePrefix = "mps";
                        break;
                    case "mVar":
                        xBaseNamePrefix = "mpv";
                        break;
                    case "mBlock":
                        xBaseNamePrefix = "mpb";
                        break;
                    case "mDel":
                        xBaseNamePrefix = "mpd";
                        break;
                    case "mSubVar":
                        xBaseNamePrefix = "mpu";
                        break;
                    case "mDocProps":
                        xBaseNamePrefix = "mpp";
                        break;
                    case "mSecProps":
                        xBaseNamePrefix = "mpc";
                        break;
                }

                string xDocVarID = GenerateDocVarID();
                string xBookmarkName = xBaseNamePrefix + xDocVarID;

                //object db id
                if (xBaseName == "mSEG")
                {
                    string xObjectData = oCustomXMLPrNode.SelectSingleNode(
                        "child::w:attr[@w:name=\"ObjectData\"]",
                        oPartNSMan).Attributes["w:val"].Value;
                    string xObjectDBID = GetmSEGObjectDataValue(
                        xObjectData, "SegmentID");
                    if (xObjectDBID.Length < 19)
                    {
                        xObjectDBID = xObjectDBID.PadLeft(19, '0');
                    }
                    xBookmarkName += xObjectDBID;
                }

                //bookmark names can only contain alphanumeric characters
                xBookmarkName = xBookmarkName.Replace('-', 'm');
                xBookmarkName = xBookmarkName.Replace('.', 'd');

                //pad to 40 characters
                xBookmarkName = "_" + xBookmarkName.PadRight(39, '0');

                //get mpo doc var name
                string xMPOVar = "mpo" + xDocVarID;

                //test for conflicts with existing doc vars -
                //regenerate ID if necessary
                bool bDocVarExists = oSettingsXML.SelectSingleNode(
                    @"//w:docVar[@w:name='" + xMPOVar + "']", oSettingsNSMan) != null;

                while (bDocVarExists)
                {
                    //conflict exists, regenerate id
                    xDocVarID = GenerateDocVarID();
                    //include beginning '_'
                    xBookmarkName = xBookmarkName.Substring(0, 4) + xDocVarID + xBookmarkName.Substring(12);
                    xMPOVar = xMPOVar.Substring(0, 3) + xDocVarID;
                    xDocVarID = null;

                    //test again with new ID
                    bDocVarExists = oSettingsXML.SelectSingleNode(
                        @"//w:docVar[@w:name='" + xMPOVar + "']", oSettingsNSMan) != null;
                }

                XmlElement oNewAttrElement = null;
                XmlAttribute oAttrAttr = null;
                //06/22/11: Set XMLNode Reserved Attribute to Bookmark Name without first character
                string xReserved = xBookmarkName.Substring(1);
                if (oCustomXMLPrNode.SelectSingleNode("child::w:attr[@w:name=\"Reserved\"]",
                    oPartNSMan) == null)
                {
                    //create attribute element
                    oNewAttrElement = oPartXML.CreateElement("w:attr", WordOpenXMLNamespace);

                    //create name & value attributes for element
                    oAttrAttr = oPartXML.CreateAttribute("w:name", WordOpenXMLNamespace);
                    oAttrAttr.Value = "Reserved";
                    oNewAttrElement.Attributes.Append(oAttrAttr);

                    oAttrAttr = oPartXML.CreateAttribute("w:val", WordOpenXMLNamespace);
                    oNewAttrElement.Attributes.Append(oAttrAttr);

                    //add element to customXmlPr element
                    oCustomXMLPrNode.AppendChild(oNewAttrElement);
                }
                XmlNode oAttrNode = oCustomXMLPrNode.SelectSingleNode(
                   "child::w:attr[@w:name=\"Reserved\"]", oPartNSMan);
                oAttrNode.Attributes["w:val"].Value = xReserved;

                //11-29-10 (dm) - use TempID attribute to index nested adjacent tags
                bool bHasAdjacentChild = false;
                XmlNode oTempIDNode = oCustomXMLPrNode.SelectSingleNode(
                    "child::w:attr[@w:name=\"TempID\"]", oPartNSMan);
                if (oTempIDNode != null)
                    oCustomXMLPrNode.RemoveChild(oTempIDNode);

                //search for first child tag
                string xInnerXML = oMPNode.InnerXml;
                int iPos = xInnerXML.IndexOf("<w:customXml w:uri=\"" +
                    MacPacNamespace + "\"");
                if (iPos > -1)
                {
                    //if run or end of paragraph occurs before child,
                    //child is not adjacent
                    int iPos2 = xInnerXML.IndexOf("<w:r>");
                    int iPos3 = xInnerXML.IndexOf("<w:p />");
                    int iPos4 = xInnerXML.IndexOf("</w:p>");
                    bHasAdjacentChild = (!(((iPos2 > -1) && (iPos2 < iPos)) ||
                        ((iPos3 > -1) && (iPos3 < iPos)) ||
                        ((iPos4 > -1) && (iPos4 < iPos))));
                }

                if (bHasAdjacentChild)
                {
                    int iTempID = 1;
                    XmlNode oChildNode = oMPNode.SelectSingleNode(
                        "descendant::w:customXml[@w:uri=\"" + MacPacNamespace + "\"]", oPartNSMan);
                    XmlNode oChildPrNode = oChildNode.SelectSingleNode(
                        "child::w:customXmlPr", oPartNSMan);
                    XmlNode oChildNodeTempID = oChildPrNode.SelectSingleNode(
                        "child::w:attr[@w:name=\"TempID\"]", oPartNSMan);
                    if (oChildNodeTempID == null)
                    {
                        //append TempID attribute to child's mpo doc var
                        xReserved = oChildPrNode.SelectSingleNode(
                            "child::w:attr[@w:name=\"Reserved\"]",
                            oPartNSMan).Attributes["w:val"].Value;
                        string xChildDocVar = "mpo" + xReserved.Substring(3, 8);
                        XmlNode oChildDocVar = oSettingsXML.SelectSingleNode(
                            @"//w:docVar[@w:name='" + xChildDocVar + "']", oSettingsNSMan);
                        XmlAttribute oChildDocVarValue = oChildDocVar.Attributes["w:val"];
                        string xChildDocVarValue = oChildDocVarValue.Value;
                        iPos = xChildDocVarValue.IndexOf("ÌÍ") + 2;
                        string xChildTagID = xChildDocVarValue.Substring(0, iPos);
                        string xEncryptedAttrs = Decrypt(xChildDocVarValue.Substring(iPos),
                            ref xPwd);
                        xEncryptedAttrs = Encrypt(xEncryptedAttrs + "ÌÍTempID=1", xPwd);
                        oChildDocVarValue.Value = xChildTagID + xEncryptedAttrs;
                    }
                    else
                        iTempID = System.Int32.Parse(oChildNodeTempID.Attributes["w:val"].Value);
                    iTempID++;

                    //create attribute element
                    oNewAttrElement = oPartXML.CreateElement("w:attr", WordOpenXMLNamespace);

                    //create name & value attributes for element
                    oAttrAttr = oPartXML.CreateAttribute("w:name", WordOpenXMLNamespace);
                    oAttrAttr.Value = "TempID";
                    oNewAttrElement.Attributes.Append(oAttrAttr);

                    oAttrAttr = oPartXML.CreateAttribute("w:val", WordOpenXMLNamespace);
                    oAttrAttr.Value = iTempID.ToString();
                    oNewAttrElement.Attributes.Append(oAttrAttr);

                    //add element to customXmlPr element
                    oCustomXMLPrNode.AppendChild(oNewAttrElement);
                }

                //create mpo docvar
                string xTagID = null;
                if (xBaseName == "mSubVar")
                {
                    xTagID = oCustomXMLPrNode.SelectSingleNode("child::w:attr[@w:name=\"Name\"]",
                        oPartNSMan).Attributes["w:val"].Value;
                }
                else
                {
                    xTagID = oCustomXMLPrNode.SelectSingleNode("child::w:attr[@w:name=\"TagID\"]",
                        oPartNSMan).Attributes["w:val"].Value;
                }

                //the rest of the value is encrypted
                string xAttrName = null;
                string xAttrs = null;

                XmlNodeList oAttributeNodes = oCustomXMLPrNode.SelectNodes(
                    "child::w:attr", oPartNSMan);
                foreach (XmlNode oAttr in oAttributeNodes)
                {
                    xAttrName = oAttr.Attributes["w:name"].Value;

                    if (xAttrName != "TagID" && xAttrName != "Name" && xAttrName != "DeletedScopes")
                    {
                        xAttrs += xAttrName + "=" + Decrypt(
                            oAttr.Attributes["w:val"].Value, ref xPwd) + "ÌÍ";
                    }
                }

                //trim trailing separator
                xAttrs = xAttrs.TrimEnd('Ì', 'Í');

                //encrypt
                if ((xBaseName != "mDel") && (xBaseName != "mSubVar"))
                    xAttrs = Encrypt(xAttrs, xPwd);

                //add variable
                string xValue = xTagID + "ÌÍ" + xAttrs;

                //create doc var element
                XmlElement oNewDocVarElement = oSettingsXML.CreateElement("w:docVar",
                    WordOpenXMLNamespace);

                //create name & value attributes for element
                XmlAttribute oVarAttr = oSettingsXML.CreateAttribute("w:name",
                    WordOpenXMLNamespace);
                oVarAttr.Value = xMPOVar;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                oVarAttr = oSettingsXML.CreateAttribute("w:val", WordOpenXMLNamespace);
                oVarAttr.Value = xValue;
                oNewDocVarElement.Attributes.Append(oVarAttr);

                //add element to doc vars element
                oDocVarsNode.AppendChild(oNewDocVarElement);

                //create mpd variable
                if (xBaseName == "mSEG")
                {
                    XmlNode oDeletedScopes = oCustomXMLPrNode.SelectSingleNode(
                        "child::w:attr[@w:name=\"DeletedScopes\"]", oPartNSMan);
                    if ((oDeletedScopes != null) && (!string.IsNullOrEmpty(
                        oDeletedScopes.Attributes["w:val"].Value)))
                    {
                        string xDeletedScopes = oDeletedScopes.Attributes["w:val"].Value;
                        xDeletedScopes = Decrypt(xDeletedScopes, ref xPwd);

                        //add bookmarks and doc vars for tags in deleted scopes
                        AddBookmarksAndDocVarsForDeletedScopes(oSettingsXML, oSettingsNSMan,
                            oDocVarsNode, ref iBmkID, ref xDeletedScopes, xPwd);

                        //update deleted scopes attribute
                        xDeletedScopes = Encrypt(xDeletedScopes, xPwd);
                        oDeletedScopes.Attributes["w:val"].Value = xDeletedScopes;

                        int iDelPart = 0;
                        //create multiple mpd variables if necessary
                        do
                        {
                            iDelPart++;
                            string xMPDVar = "mpd" + xDocVarID + iDelPart.ToString().PadLeft(2, '0');

                            //create doc var element
                            oNewDocVarElement = oSettingsXML.CreateElement("w:docVar", WordOpenXMLNamespace);

                            //create name & value attributes for element
                            oVarAttr = oSettingsXML.CreateAttribute("w:name", WordOpenXMLNamespace);
                            oVarAttr.Value = xMPDVar;
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oVarAttr = oSettingsXML.CreateAttribute("w:val", WordOpenXMLNamespace);
                            oVarAttr.Value = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
                            oNewDocVarElement.Attributes.Append(oVarAttr);

                            oDocVarsNode.AppendChild(oNewDocVarElement);
                            if (xDeletedScopes.Length > 65000)
                                xDeletedScopes = xDeletedScopes.Substring(65000);
                            else
                                break;
                        } while (xDeletedScopes != "");
                    }
                }

                //create bookmark start element
                XmlElement oBookmarkStart = oPartXML.CreateElement("w:bookmarkStart",
                    WordOpenXMLNamespace);

                //create name & value attributes for element
                oVarAttr = oPartXML.CreateAttribute("w:id", WordOpenXMLNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkStart.Attributes.Append(oVarAttr);

                oVarAttr = oPartXML.CreateAttribute("w:name", WordOpenXMLNamespace);
                oVarAttr.Value = xBookmarkName;
                oBookmarkStart.Attributes.Append(oVarAttr);

                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                bool bIsInline = (oMPNode.SelectSingleNode(
                        "descendant::w:p", oPartNSMan) == null);
                if (!bIsInline)
                {
                    oVarAttr = oPartXML.CreateAttribute("w:displacedByCustomXml",
                        WordOpenXMLNamespace);
                    oVarAttr.Value = "prev";
                    oBookmarkStart.Attributes.Append(oVarAttr);
                }

                //create bookmark end element
                XmlElement oBookmarkEnd = oPartXML.CreateElement("bookmarkEnd",
                    WordOpenXMLNamespace);

                //create name & value attributes for element
                oVarAttr = oPartXML.CreateAttribute("w:id", WordOpenXMLNamespace);
                oVarAttr.Value = iBmkID.ToString();
                oBookmarkEnd.Attributes.Append(oVarAttr);

                //11-23-10 (dm) - add w:displacedBySDT attribute to
                //paragraph-level tags only
                if (!bIsInline)
                {
                    oVarAttr = oPartXML.CreateAttribute("w:displacedByCustomXml",
                        WordOpenXMLNamespace);
                    oVarAttr.Value = "next";
                    oBookmarkEnd.Attributes.Append(oVarAttr);
                }

                //11-29-10 (dm) - add bookmark start and end tags
                bool bIsEmpty = (oMPNode.ChildNodes.Count == 0);
                if (bIsEmpty)
                {
                    oMPNode.PrependChild(oBookmarkEnd);
                    oMPNode.PrependChild(oBookmarkStart);
                }
                else
                {
                    oMPNode.PrependChild(oBookmarkStart);
                    oMPNode.AppendChild(oBookmarkEnd);
                }

                //set next bookmark id
                iBmkID--;
            }
            catch (System.Exception oE)
            {
                throw new System.Exception(oE.Message, oE);
            }
        }

        /// <summary>
        /// returns the prefix assigned to the specified namespace
        /// in the specified xml - if namespace isn't found,
        /// returns an empty string
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xNamespace"></param>
        /// <returns></returns>
        private static string GetNamespacePrefix(string xXML, string xNamespace)
        {
            int iPos = xXML.IndexOf(xNamespace);
            if (iPos > -1)
            {
                int iPos2 = xXML.LastIndexOf("xmlns:", iPos);
                return xXML.Substring(iPos2 + 6, iPos - iPos2 - 8);
            }
            return "";
        }
        private static string Decrypt(string xInput, ref string xPwd)
        {
            CreateCOMObjectIfNecessary();
            //Set paramaters for late binding
            object[] oParam = new object[2];
            oParam[0] = xInput;
            oParam[1] = xPwd;
            ParameterModifier oParamMod = new ParameterModifier(2);
            //Mark 2nd parameter as ByRef
            oParamMod[1] = true;
            ParameterModifier[] oMods = { oParamMod };
            string xDecrypted = m_oCOMType.InvokeMember("Decrypt", BindingFlags.InvokeMethod, null, m_oCOM, oParam, oMods, null, null).ToString();
            //Set Password to value passed ByRef
            xPwd = oParam[1].ToString();
            return xDecrypted;
        }
        private static string Encrypt(string xInput, string xPwd)
        {
            CreateCOMObjectIfNecessary();
            //Set paramaters for late binding
            object[] oParam = new object[2];
            oParam[0] = xInput;
            oParam[1] = xPwd;
            return m_oCOMType.InvokeMember("Encrypt", BindingFlags.InvokeMethod, null, m_oCOM, oParam).ToString();
        }
        private static void SetClientMetadataValues(string xPrefix, string xPwd)
        {
            CreateCOMObjectIfNecessary();
            //Set paramaters for late binding
            object[] oParam = new object[2];
            oParam[0] = xPrefix;
            oParam[1] = xPwd;
            m_oCOMType.InvokeMember("SetClientMetadataValues", BindingFlags.InvokeMethod | BindingFlags.IgnoreReturn, null, m_oCOM, oParam);
        }
    }
}
