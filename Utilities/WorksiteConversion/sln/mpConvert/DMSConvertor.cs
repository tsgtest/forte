﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word=Microsoft.Office.Interop.Word;
using System.IO;
using Reg = Microsoft.Win32.Registry;
using IManage;
using IMANEXTLib;

namespace mpConvert
{
    public abstract class DMSConvertor
    {
        public abstract void Preconvert(Word.Document oDoc);

        internal static mpPreconversionResults PreconvertLocalDocument(Word.Document oDoc, ref string xNewPath)
        {
            //Copy last saved version of file on disk to temp location and run preconversion routine on it
            if (!oDoc.Saved)
            {
                DialogResult iRes = MessageBox.Show("In order to convert the document on disk, the Active Document will need to be closed without saving changes. " +
                        "If you want to save current edits, answer 'No' to prompt, then save as new document.  Original document can then " +
                        "be opened and converted to Content Controls.\r\rContinue with conversion without saving changes?", "MacPac", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iRes != DialogResult.Yes)
                    return mpPreconversionResults.NoContent;
            }
            string xPath = oDoc.FullName;
            //Preserve extension in copy
            xNewPath = Path.GetTempPath() + oDoc.Name;
            File.Copy(xPath, xNewPath, true);
            string xMessage = "";
            mpPreconversionResults iResult = Preconversion.PreconvertFile(xNewPath, ref xMessage);
            if (iResult != mpPreconversionResults.Successful)
            {
                MessageBox.Show(xMessage, "MacPac", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                File.Delete(xNewPath);
            }
            return iResult;

        }
        internal static void BackupLocalDocument(Word.Document oDoc)
        {
            BackupLocalDocument(oDoc, "", "");
        }
        internal static void BackupLocalDocument(Word.Document oDoc, string xLibrary, string xAuthorID)
        {
            //Create Backup copy of original unconverted document
            try
            {
                string xUser = xAuthorID;
                if (string.IsNullOrEmpty(xUser))
                    xUser = Environment.UserName;
                string xBackupDir = GetBackupDirectory();
                if (xLibrary != "")
                    xBackupDir = xBackupDir + "\\" + xLibrary;
                xBackupDir = xBackupDir + "\\" + xUser;
                if (!Directory.Exists(xBackupDir))
                {
                    Directory.CreateDirectory(xBackupDir);
                }
                File.Copy(oDoc.FullName, xBackupDir + "\\" + oDoc.Name, true);
            }
            catch (System.Exception oE)
            {
                throw new Exception("Unable to create backup for original document.", oE);
            }
            
        }
        internal static string GetBackupDirectory()
        {
            try
            {
                string xBackupDir = "";
                try
                {
                    xBackupDir = Reg.GetValue("HKEY_LOCAL_MACHINE\\Software\\The Sackett Group\\Deca", "ConvertedBackupDirectory", null).ToString();
                }
                catch { }

                if (xBackupDir == "")
                {
                    string xDatabaseDir = "";
                    //get path from registry
                    try
                    {
                        xDatabaseDir = Reg.GetValue("HKEY_LOCAL_MACHINE\\Software\\The Sackett Group\\Deca", "DataDirectory", null).ToString();
                    }
                    catch { }

                    if (string.IsNullOrEmpty(xDatabaseDir))
                    {
                        //no registry path was specified - get from root directory
                        xDatabaseDir = Reg.GetValue("Software\\The Sackett Group\\Deca", "RootDirectory", null).ToString() + "\\Data";
                    }

                    xDatabaseDir = EvaluateEnvironmentVariables(xDatabaseDir);
                    return xDatabaseDir + "\\Converted";
                }
                else
                    return xBackupDir;
            }
            catch
            {
                throw new Exception("Unable to determine Backup Directory location from Registry.");
            }
        }
        /// <summary>
        /// returns the specified string after evaluating
        /// its environment variables
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        internal static string EvaluateEnvironmentVariables(string xText)
        {

            //evaluate path for environment variables
            int iPos = xText.IndexOf('%');
            while (iPos > -1)
            {
                int iPos2 = xText.IndexOf('%', iPos + 1);

                if (iPos2 == -1)
                {
                    //something's wrong
                    throw new Exception("Invalid Directory name specified in Registry.");
                }

                string xEnvVar = xText.Substring(iPos + 1, iPos2 - iPos - 1);

                //evaluate
                xEnvVar = System.Environment.GetEnvironmentVariable(xEnvVar);

                xText = xText.Substring(0, iPos) + xEnvVar + xText.Substring(iPos2 + 1);

                iPos = xText.IndexOf('%', iPos + 1);
            }

            return xText;
        }
    }

    public class WorksiteConvertor : DMSConvertor
    {
        //Preconversion routines specific to Worksite
        WorkSiteAddinInterfaces.iManageExtensibility m_oExt;
        IManDMS m_oDMS = null;
        ContextItems m_oContext = null;
        Word.Application m_oWordApp = null;
        public WorksiteConvertor()
        {

        }
        public override void Preconvert(Word.Document oDoc)
        {
            InitializeWorksiteObjects(oDoc);

            string xNewPath = "";
            mpPreconversionResults iResult = PreconvertLocalDocument(oDoc, ref xNewPath);
            if (iResult == mpPreconversionResults.Successful)
            {
                NRTDocument oNRTDoc = m_oExt.GetDocumentFromPath(oDoc.FullName);
                int iDocNum = oNRTDoc.Number;
                int iDocVer = oNRTDoc.Version;
                string xLibrary = oNRTDoc.Database.Name;
                string xAuthorID = oNRTDoc.Author.Name;
                BackupLocalDocument(oDoc, xLibrary, xAuthorID);
                oDoc.Saved = true;
                object oFalse = false;
                object oNull = null;
                oDoc.Close(ref oFalse, ref oNull, ref oNull);
                IManDatabase oDB = m_oDMS.Sessions.ItemByIndex(1).Databases.ItemByName(xLibrary);
                IManDocument IManDoc = oDB.GetDocument(iDocNum, iDocVer);
                OpenConvertedDoc(IManDoc, xNewPath);
                //Delete temp file
                File.Delete(xNewPath);
            }
        }
        private void OpenConvertedDoc(IManDocument oDoc, string xNewPath)
        {
            NRTDocument oNRTDoc = (NRTDocument)oDoc;
            OpenCmd oCmd = new OpenCmd();
            ContextItems oContext = new ContextItems();
            NRTDocument[] oDocArray = new NRTDocument[] { oNRTDoc };
            object oValue = oDocArray;
            oContext.Add("SelectedNRTDocuments", oValue);
            oValue = true;
            oContext.Add("IManExt.OpenCmd.NoCmdUI", oValue);
            oContext.Add("IManExt.OpenCmd.Integration", oValue);
            oContext.Add("IManExt.CheckOutCmd.NoVersionPrompts", oValue);
            oCmd.Initialize(oContext);
            oCmd.Update();
            if (oCmd.Status == (oCmd.Status & 0))
            {
                oCmd.Execute();
                string xPath = oContext.Item("IManExt.OpenCmd.ING.FileLocation").ToString();
                if (File.Exists(xPath))
                {
                    File.Copy(xNewPath, xPath, true);
                    object oMissing = System.Reflection.Missing.Value;
                    object oPath = xPath;
                    oDoc.HistoryList.Add(imHistEvent.imHistoryModify, 0, 0, "MacPac", "Convert to Content Controls", "", "", "", null, null, null);
                    m_oWordApp.Documents.Open(ref oPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                }
            }
            //Remove context items
            oContext.Remove("IManExt.OpenCmd.NoCmdUI");
            oContext.Remove("IManExt.OpenCmd.Integration");
            oContext.Remove("IManExt.CheckoutCmd.NoVersionPrompts");
        }
        private void InitializeWorksiteObjects(Word.Document oDoc)
        {
            try
            {
                //already initialized
                if (m_oExt != null)
                    return;

                m_oWordApp = oDoc.Application;

                //Addin Name for Worksite 8.5 SP3 Update 2 and later
                object oAddin = "WorkSiteOffice2007Addins.Connect";
                try
                {
                    m_oExt = (WorkSiteAddinInterfaces.iManageExtensibility)m_oWordApp.COMAddIns.Item(ref oAddin).Object;
                }
                catch { }
                //Addin Name for Worksite 8.5 SP2 or earlier
                oAddin = "oUTR02K.Connect";
                if (m_oExt == null)
                {
                    try
                    {
                        m_oExt = (WorkSiteAddinInterfaces.iManageExtensibility)m_oWordApp.COMAddIns.Item(ref oAddin).Object;
                    }
                    catch { }
                }
                if (m_oExt == null)
                {
                    return;
                }
                m_oContext = m_oExt.Context;
                m_oDMS = (IManDMS)m_oContext.Item("NRTDMS");
            }
            catch (System.Exception oE)
            {
                throw new Exception("Unable to initialize Worksite objects.", oE);
            }
        }
    }
    public class WordConvertor : DMSConvertor
    {
        public override void Preconvert(Word.Document oDoc)
        {
            Word.Application oWordApp = oDoc.Application;
            string xPath = "";
            string xNewPath = "";
            mpPreconversionResults iResult = PreconvertLocalDocument(oDoc, ref xNewPath);
            if (iResult == mpPreconversionResults.Successful)
            {
                xPath = oDoc.FullName;
                BackupLocalDocument(oDoc);
                oDoc.Saved = true;
                object oFalse = false;
                object oNull = null;
                oDoc.Close(ref oFalse, ref oNull, ref oNull);
                File.Copy(xNewPath, xPath, true);
                object oPath = xPath;
                object oMissing = System.Reflection.Missing.Value;
                oWordApp.Documents.Open(ref oPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                //Delete temp file
                File.Delete(xNewPath);
            }
        }
    }
}
