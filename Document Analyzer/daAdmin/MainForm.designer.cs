namespace LMP.DocAnalyzer.Admin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAddElement = new System.Windows.Forms.ToolStripButton();
            this.tbtnDeleteElement = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblElements = new System.Windows.Forms.Label();
            this.grdElements = new System.Windows.Forms.DataGridView();
            this.ElementName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnVisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NoMatch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParseSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Template = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SortOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbDocType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlParseInstruction = new System.Windows.Forms.Panel();
            this.grpParseSource = new System.Windows.Forms.GroupBox();
            this.rdbtnElementMatchRange = new System.Windows.Forms.RadioButton();
            this.rdbtnElementText = new System.Windows.Forms.RadioButton();
            this.lblTemplate = new System.Windows.Forms.Label();
            this.txtTemplate = new System.Windows.Forms.TextBox();
            this.lblMatchInstructions = new System.Windows.Forms.Label();
            this.lblParseInstruction = new System.Windows.Forms.Label();
            this.pnlMatchInstructions = new System.Windows.Forms.Panel();
            this.lblInstructionIndexes = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRDName = new System.Windows.Forms.TextBox();
            this.cmbRDUnit = new System.Windows.Forms.ComboBox();
            this.cmbRDDocObjType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMCValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMCName = new System.Windows.Forms.TextBox();
            this.cmbMCDocObjType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblMatchObjectType = new System.Windows.Forms.Label();
            this.grpMatchDomain = new System.Windows.Forms.GroupBox();
            this.lblRangeEnd = new System.Windows.Forms.Label();
            this.lblRangeStart = new System.Windows.Forms.Label();
            this.lblStory = new System.Windows.Forms.Label();
            this.cmbStory = new System.Windows.Forms.ComboBox();
            this.cmbRPPosition = new System.Windows.Forms.ComboBox();
            this.chkWholeRange = new System.Windows.Forms.CheckBox();
            this.cmbRPPosition2 = new System.Windows.Forms.ComboBox();
            this.txtRPValue2 = new System.Windows.Forms.TextBox();
            this.txtRPName2 = new System.Windows.Forms.TextBox();
            this.cmbRPOperator2 = new System.Windows.Forms.ComboBox();
            this.cmbRPDocObjType2 = new System.Windows.Forms.ComboBox();
            this.cmbRPSection2 = new System.Windows.Forms.ComboBox();
            this.txtRPValue = new System.Windows.Forms.TextBox();
            this.txtRPName = new System.Windows.Forms.TextBox();
            this.cmbRPOperator = new System.Windows.Forms.ComboBox();
            this.cmbRPDocObjType = new System.Windows.Forms.ComboBox();
            this.cmbRPSection = new System.Windows.Forms.ComboBox();
            this.Position = new System.Windows.Forms.Label();
            this.Value = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblObject = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lstInstructionIndexes = new System.Windows.Forms.ListBox();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.tbtnAddDocType = new System.Windows.Forms.ToolStripButton();
            this.tbtnDeleteDocType = new System.Windows.Forms.ToolStripButton();
            this.tbtnImportElements = new System.Windows.Forms.ToolStripButton();
            this.tbtnAddInstruction = new System.Windows.Forms.ToolStripButton();
            this.tbtnDeleteInstruction = new System.Windows.Forms.ToolStripButton();
            this.tbtnEditDeterminants = new System.Windows.Forms.ToolStripButton();
            this.tbtnManageEncryptionSeed = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdElements)).BeginInit();
            this.pnlParseInstruction.SuspendLayout();
            this.grpParseSource.SuspendLayout();
            this.pnlMatchInstructions.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpMatchDomain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnAddDocType,
            this.tbtnDeleteDocType,
            this.toolStripSeparator2,
            this.tbtnImportElements,
            this.tbtnAddElement,
            this.tbtnDeleteElement,
            this.toolStripSeparator5,
            this.tbtnAddInstruction,
            this.tbtnDeleteInstruction,
            this.toolStripSeparator3,
            this.tbtnEditDeterminants,
            this.toolStripSeparator1,
            this.tbtnManageEncryptionSeed});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1263, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnAddElement
            // 
            this.tbtnAddElement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAddElement.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAddElement.Name = "tbtnAddElement";
            this.tbtnAddElement.Size = new System.Drawing.Size(88, 22);
            this.tbtnAddElement.Text = "Add Element...";
            this.tbtnAddElement.Click += new System.EventHandler(this.tbtnAddElement_Click);
            // 
            // tbtnDeleteElement
            // 
            this.tbtnDeleteElement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteElement.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteElement.Name = "tbtnDeleteElement";
            this.tbtnDeleteElement.Size = new System.Drawing.Size(99, 22);
            this.tbtnDeleteElement.Text = "Delete Element...";
            this.tbtnDeleteElement.Click += new System.EventHandler(this.tbtnDeleteElement_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnMoveDown);
            this.splitContainer1.Panel1.Controls.Add(this.btnMoveUp);
            this.splitContainer1.Panel1.Controls.Add(this.lblElements);
            this.splitContainer1.Panel1.Controls.Add(this.grdElements);
            this.splitContainer1.Panel1.Controls.Add(this.cmbDocType);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlParseInstruction);
            this.splitContainer1.Panel2.Controls.Add(this.lblMatchInstructions);
            this.splitContainer1.Panel2.Controls.Add(this.lblParseInstruction);
            this.splitContainer1.Panel2.Controls.Add(this.pnlMatchInstructions);
            this.splitContainer1.Size = new System.Drawing.Size(1263, 795);
            this.splitContainer1.SplitterDistance = 433;
            this.splitContainer1.TabIndex = 2;
            // 
            // lblElements
            // 
            this.lblElements.AutoSize = true;
            this.lblElements.Location = new System.Drawing.Point(3, 49);
            this.lblElements.Name = "lblElements";
            this.lblElements.Size = new System.Drawing.Size(53, 13);
            this.lblElements.TabIndex = 0;
            this.lblElements.Text = "Elements:";
            // 
            // grdElements
            // 
            this.grdElements.AllowUserToAddRows = false;
            this.grdElements.AllowUserToDeleteRows = false;
            this.grdElements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdElements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdElements.BackgroundColor = System.Drawing.Color.White;
            this.grdElements.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdElements.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdElements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ElementName,
            this.DisplayName,
            this.ColumnVisible,
            this.NoMatch,
            this.ID,
            this.ParseSource,
            this.Template,
            this.SortOrder});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdElements.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdElements.Location = new System.Drawing.Point(3, 65);
            this.grdElements.MultiSelect = false;
            this.grdElements.Name = "grdElements";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdElements.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdElements.RowHeadersWidth = 25;
            this.grdElements.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdElements.Size = new System.Drawing.Size(392, 659);
            this.grdElements.TabIndex = 1;
            this.grdElements.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdElements_CellEndEdit);
            this.grdElements.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdElements_CellEnter);
            this.grdElements.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdElements_CellValidating);
            this.grdElements.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdElements_CellValueChanged);
            this.grdElements.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdElements_DataError);
            this.grdElements.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdElements_RowEnter);
            this.grdElements.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdElements_RowLeave);
            this.grdElements.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdElements_UserDeletingRow);
            this.grdElements.Click += new System.EventHandler(this.grdElements_Click);
            this.grdElements.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdElements_KeyDown);
            // 
            // ElementName
            // 
            this.ElementName.DataPropertyName = "Name";
            this.ElementName.FillWeight = 132.2875F;
            this.ElementName.HeaderText = "Name";
            this.ElementName.Name = "ElementName";
            this.ElementName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // DisplayName
            // 
            this.DisplayName.DataPropertyName = "DisplayName";
            this.DisplayName.FillWeight = 230.8699F;
            this.DisplayName.HeaderText = "Display Name";
            this.DisplayName.Name = "DisplayName";
            // 
            // Visible
            // 
            this.ColumnVisible.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnVisible.DataPropertyName = "Visible";
            this.ColumnVisible.FillWeight = 63F;
            this.ColumnVisible.HeaderText = "Show";
            this.ColumnVisible.Name = "Visible";
            this.ColumnVisible.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnVisible.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnVisible.Width = 40;
            // 
            // NoMatch
            // 
            this.NoMatch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NoMatch.FillWeight = 50F;
            this.NoMatch.HeaderText = "If No Data, Return...";
            this.NoMatch.Name = "NoMatch";
            this.NoMatch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NoMatch.Width = 70;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.ID.Visible = false;
            // 
            // ParseSource
            // 
            this.ParseSource.DataPropertyName = "ParsingSource";
            this.ParseSource.HeaderText = "ParseSource";
            this.ParseSource.Name = "ParseSource";
            this.ParseSource.Visible = false;
            // 
            // Template
            // 
            this.Template.DataPropertyName = "Template";
            this.Template.HeaderText = "Template";
            this.Template.Name = "Template";
            this.Template.Visible = false;
            // 
            // SortOrder
            // 
            this.SortOrder.DataPropertyName = "SortOrder";
            this.SortOrder.HeaderText = "SortOrder";
            this.SortOrder.Name = "SortOrder";
            this.SortOrder.Visible = false;
            // 
            // cmbDocType
            // 
            this.cmbDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocType.FormattingEnabled = true;
            this.cmbDocType.Location = new System.Drawing.Point(3, 20);
            this.cmbDocType.Name = "cmbDocType";
            this.cmbDocType.Size = new System.Drawing.Size(241, 21);
            this.cmbDocType.TabIndex = 0;
            this.cmbDocType.SelectedIndexChanged += new System.EventHandler(this.cmbDocType_SelectedIndexChanged);
            this.cmbDocType.Leave += new System.EventHandler(this.cmbDocType_Leave);
            this.cmbDocType.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbDocType_MouseClick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Document Types:";
            // 
            // pnlParseInstruction
            // 
            this.pnlParseInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlParseInstruction.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlParseInstruction.Controls.Add(this.grpParseSource);
            this.pnlParseInstruction.Controls.Add(this.lblTemplate);
            this.pnlParseInstruction.Controls.Add(this.txtTemplate);
            this.pnlParseInstruction.Location = new System.Drawing.Point(3, 457);
            this.pnlParseInstruction.Name = "pnlParseInstruction";
            this.pnlParseInstruction.Size = new System.Drawing.Size(814, 331);
            this.pnlParseInstruction.TabIndex = 2;
            // 
            // grpParseSource
            // 
            this.grpParseSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpParseSource.Controls.Add(this.rdbtnElementMatchRange);
            this.grpParseSource.Controls.Add(this.rdbtnElementText);
            this.grpParseSource.Location = new System.Drawing.Point(15, 86);
            this.grpParseSource.Name = "grpParseSource";
            this.grpParseSource.Size = new System.Drawing.Size(790, 48);
            this.grpParseSource.TabIndex = 89;
            this.grpParseSource.TabStop = false;
            this.grpParseSource.Text = "Parse Source";
            this.grpParseSource.Visible = false;
            // 
            // rdbtnElementMatchRange
            // 
            this.rdbtnElementMatchRange.AutoSize = true;
            this.rdbtnElementMatchRange.Location = new System.Drawing.Point(183, 19);
            this.rdbtnElementMatchRange.Name = "rdbtnElementMatchRange";
            this.rdbtnElementMatchRange.Size = new System.Drawing.Size(131, 17);
            this.rdbtnElementMatchRange.TabIndex = 5;
            this.rdbtnElementMatchRange.TabStop = true;
            this.rdbtnElementMatchRange.Text = "Element Match Range";
            this.rdbtnElementMatchRange.UseVisualStyleBackColor = true;
            this.rdbtnElementMatchRange.CheckedChanged += new System.EventHandler(this.rdbtnElementMatchRange_CheckedChanged);
            // 
            // rdbtnElementText
            // 
            this.rdbtnElementText.AutoSize = true;
            this.rdbtnElementText.Location = new System.Drawing.Point(48, 19);
            this.rdbtnElementText.Name = "rdbtnElementText";
            this.rdbtnElementText.Size = new System.Drawing.Size(87, 17);
            this.rdbtnElementText.TabIndex = 4;
            this.rdbtnElementText.TabStop = true;
            this.rdbtnElementText.Text = "Element Text";
            this.rdbtnElementText.UseVisualStyleBackColor = true;
            this.rdbtnElementText.Click += new System.EventHandler(this.rdbtnElementText_Click);
            // 
            // lblTemplate
            // 
            this.lblTemplate.AutoSize = true;
            this.lblTemplate.Location = new System.Drawing.Point(16, 18);
            this.lblTemplate.Name = "lblTemplate";
            this.lblTemplate.Size = new System.Drawing.Size(54, 13);
            this.lblTemplate.TabIndex = 1;
            this.lblTemplate.Text = "&Template:";
            // 
            // txtTemplate
            // 
            this.txtTemplate.Location = new System.Drawing.Point(19, 34);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(599, 20);
            this.txtTemplate.TabIndex = 0;
            this.txtTemplate.TextChanged += new System.EventHandler(this.txtTemplate_TextChanged);
            // 
            // lblMatchInstructions
            // 
            this.lblMatchInstructions.AutoSize = true;
            this.lblMatchInstructions.Location = new System.Drawing.Point(3, 5);
            this.lblMatchInstructions.Name = "lblMatchInstructions";
            this.lblMatchInstructions.Size = new System.Drawing.Size(94, 13);
            this.lblMatchInstructions.TabIndex = 4;
            this.lblMatchInstructions.Text = "Match Instructions";
            // 
            // lblParseInstruction
            // 
            this.lblParseInstruction.AutoSize = true;
            this.lblParseInstruction.Location = new System.Drawing.Point(0, 441);
            this.lblParseInstruction.Name = "lblParseInstruction";
            this.lblParseInstruction.Size = new System.Drawing.Size(86, 13);
            this.lblParseInstruction.TabIndex = 3;
            this.lblParseInstruction.Text = "Parse Instruction";
            // 
            // pnlMatchInstructions
            // 
            this.pnlMatchInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMatchInstructions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMatchInstructions.Controls.Add(this.lblInstructionIndexes);
            this.pnlMatchInstructions.Controls.Add(this.groupBox2);
            this.pnlMatchInstructions.Controls.Add(this.groupBox1);
            this.pnlMatchInstructions.Controls.Add(this.grpMatchDomain);
            this.pnlMatchInstructions.Controls.Add(this.lstInstructionIndexes);
            this.pnlMatchInstructions.Location = new System.Drawing.Point(3, 21);
            this.pnlMatchInstructions.Name = "pnlMatchInstructions";
            this.pnlMatchInstructions.Size = new System.Drawing.Size(812, 409);
            this.pnlMatchInstructions.TabIndex = 1;
            // 
            // lblInstructionIndexes
            // 
            this.lblInstructionIndexes.AutoSize = true;
            this.lblInstructionIndexes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstructionIndexes.Location = new System.Drawing.Point(6, 5);
            this.lblInstructionIndexes.Name = "lblInstructionIndexes";
            this.lblInstructionIndexes.Size = new System.Drawing.Size(14, 13);
            this.lblInstructionIndexes.TabIndex = 15;
            this.lblInstructionIndexes.Text = "#";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtRDName);
            this.groupBox2.Controls.Add(this.cmbRDUnit);
            this.groupBox2.Controls.Add(this.cmbRDDocObjType);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(37, 295);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(768, 105);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Return Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(462, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "List Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(260, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Measurement Unit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Object Type";
            // 
            // txtRDName
            // 
            this.txtRDName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRDName.Location = new System.Drawing.Point(465, 49);
            this.txtRDName.Name = "txtRDName";
            this.txtRDName.Size = new System.Drawing.Size(182, 20);
            this.txtRDName.TabIndex = 2;
            this.txtRDName.TextChanged += new System.EventHandler(this.txtRDName_TextChanged);
            // 
            // cmbRDUnit
            // 
            this.cmbRDUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRDUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRDUnit.FormattingEnabled = true;
            this.cmbRDUnit.Location = new System.Drawing.Point(260, 49);
            this.cmbRDUnit.Name = "cmbRDUnit";
            this.cmbRDUnit.Size = new System.Drawing.Size(184, 21);
            this.cmbRDUnit.TabIndex = 1;
            this.cmbRDUnit.SelectedIndexChanged += new System.EventHandler(this.cmbRDUnit_SelectedIndexChanged);
            // 
            // cmbRDDocObjType
            // 
            this.cmbRDDocObjType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRDDocObjType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRDDocObjType.FormattingEnabled = true;
            this.cmbRDDocObjType.Location = new System.Drawing.Point(9, 49);
            this.cmbRDDocObjType.Name = "cmbRDDocObjType";
            this.cmbRDDocObjType.Size = new System.Drawing.Size(238, 21);
            this.cmbRDDocObjType.TabIndex = 0;
            this.cmbRDDocObjType.SelectedIndexChanged += new System.EventHandler(this.cmbRDDocObjType_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtMCValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtMCName);
            this.groupBox1.Controls.Add(this.cmbMCDocObjType);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblMatchObjectType);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(37, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(768, 100);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Match Criteria";
            // 
            // txtMCValue
            // 
            this.txtMCValue.Location = new System.Drawing.Point(465, 45);
            this.txtMCValue.Name = "txtMCValue";
            this.txtMCValue.Size = new System.Drawing.Size(182, 20);
            this.txtMCValue.TabIndex = 2;
            this.txtMCValue.TextChanged += new System.EventHandler(this.txtMCValue_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(462, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Value";
            // 
            // txtMCName
            // 
            this.txtMCName.Location = new System.Drawing.Point(263, 46);
            this.txtMCName.Name = "txtMCName";
            this.txtMCName.Size = new System.Drawing.Size(184, 20);
            this.txtMCName.TabIndex = 1;
            this.txtMCName.TextChanged += new System.EventHandler(this.txtMCName_TextChanged);
            // 
            // cmbMCDocObjType
            // 
            this.cmbMCDocObjType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMCDocObjType.FormattingEnabled = true;
            this.cmbMCDocObjType.Location = new System.Drawing.Point(9, 45);
            this.cmbMCDocObjType.Name = "cmbMCDocObjType";
            this.cmbMCDocObjType.Size = new System.Drawing.Size(238, 21);
            this.cmbMCDocObjType.TabIndex = 0;
            this.cmbMCDocObjType.SelectedIndexChanged += new System.EventHandler(this.cmbMCDocObjType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(260, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Name";
            // 
            // lblMatchObjectType
            // 
            this.lblMatchObjectType.AutoSize = true;
            this.lblMatchObjectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatchObjectType.Location = new System.Drawing.Point(6, 29);
            this.lblMatchObjectType.Name = "lblMatchObjectType";
            this.lblMatchObjectType.Size = new System.Drawing.Size(65, 13);
            this.lblMatchObjectType.TabIndex = 0;
            this.lblMatchObjectType.Text = "Object Type";
            // 
            // grpMatchDomain
            // 
            this.grpMatchDomain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpMatchDomain.Controls.Add(this.lblRangeEnd);
            this.grpMatchDomain.Controls.Add(this.lblRangeStart);
            this.grpMatchDomain.Controls.Add(this.lblStory);
            this.grpMatchDomain.Controls.Add(this.cmbStory);
            this.grpMatchDomain.Controls.Add(this.cmbRPPosition);
            this.grpMatchDomain.Controls.Add(this.chkWholeRange);
            this.grpMatchDomain.Controls.Add(this.cmbRPPosition2);
            this.grpMatchDomain.Controls.Add(this.txtRPValue2);
            this.grpMatchDomain.Controls.Add(this.txtRPName2);
            this.grpMatchDomain.Controls.Add(this.cmbRPOperator2);
            this.grpMatchDomain.Controls.Add(this.cmbRPDocObjType2);
            this.grpMatchDomain.Controls.Add(this.cmbRPSection2);
            this.grpMatchDomain.Controls.Add(this.txtRPValue);
            this.grpMatchDomain.Controls.Add(this.txtRPName);
            this.grpMatchDomain.Controls.Add(this.cmbRPOperator);
            this.grpMatchDomain.Controls.Add(this.cmbRPDocObjType);
            this.grpMatchDomain.Controls.Add(this.cmbRPSection);
            this.grpMatchDomain.Controls.Add(this.Position);
            this.grpMatchDomain.Controls.Add(this.Value);
            this.grpMatchDomain.Controls.Add(this.label11);
            this.grpMatchDomain.Controls.Add(this.label10);
            this.grpMatchDomain.Controls.Add(this.lblObject);
            this.grpMatchDomain.Controls.Add(this.label8);
            this.grpMatchDomain.Enabled = false;
            this.grpMatchDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpMatchDomain.Location = new System.Drawing.Point(37, 128);
            this.grpMatchDomain.Name = "grpMatchDomain";
            this.grpMatchDomain.Size = new System.Drawing.Size(768, 161);
            this.grpMatchDomain.TabIndex = 12;
            this.grpMatchDomain.TabStop = false;
            this.grpMatchDomain.Text = "Search Range";
            // 
            // lblRangeEnd
            // 
            this.lblRangeEnd.AutoSize = true;
            this.lblRangeEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRangeEnd.Location = new System.Drawing.Point(6, 115);
            this.lblRangeEnd.Name = "lblRangeEnd";
            this.lblRangeEnd.Size = new System.Drawing.Size(29, 13);
            this.lblRangeEnd.TabIndex = 25;
            this.lblRangeEnd.Text = "End:";
            // 
            // lblRangeStart
            // 
            this.lblRangeStart.AutoSize = true;
            this.lblRangeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRangeStart.Location = new System.Drawing.Point(6, 91);
            this.lblRangeStart.Name = "lblRangeStart";
            this.lblRangeStart.Size = new System.Drawing.Size(29, 13);
            this.lblRangeStart.TabIndex = 24;
            this.lblRangeStart.Text = "Start";
            // 
            // lblStory
            // 
            this.lblStory.AutoSize = true;
            this.lblStory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStory.Location = new System.Drawing.Point(6, 36);
            this.lblStory.Name = "lblStory";
            this.lblStory.Size = new System.Drawing.Size(34, 13);
            this.lblStory.TabIndex = 23;
            this.lblStory.Text = "Story:";
            // 
            // cmbStory
            // 
            this.cmbStory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStory.FormattingEnabled = true;
            this.cmbStory.Location = new System.Drawing.Point(48, 33);
            this.cmbStory.Name = "cmbStory";
            this.cmbStory.Size = new System.Drawing.Size(121, 21);
            this.cmbStory.TabIndex = 0;
            this.cmbStory.SelectedIndexChanged += new System.EventHandler(this.cmbStory_SelectedIndexChanged);
            // 
            // cmbRPPosition
            // 
            this.cmbRPPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPPosition.FormattingEnabled = true;
            this.cmbRPPosition.Location = new System.Drawing.Point(582, 88);
            this.cmbRPPosition.Name = "cmbRPPosition";
            this.cmbRPPosition.Size = new System.Drawing.Size(121, 21);
            this.cmbRPPosition.TabIndex = 7;
            this.cmbRPPosition.SelectedIndexChanged += new System.EventHandler(this.cmbRPPosition_SelectedIndexChanged);
            // 
            // chkWholeRange
            // 
            this.chkWholeRange.AutoSize = true;
            this.chkWholeRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkWholeRange.Location = new System.Drawing.Point(208, 36);
            this.chkWholeRange.Name = "chkWholeRange";
            this.chkWholeRange.Size = new System.Drawing.Size(92, 17);
            this.chkWholeRange.TabIndex = 1;
            this.chkWholeRange.Text = "Whole Range";
            this.chkWholeRange.UseVisualStyleBackColor = true;
            this.chkWholeRange.CheckedChanged += new System.EventHandler(this.chkWholeRange_CheckedChanged);
            // 
            // cmbRPPosition2
            // 
            this.cmbRPPosition2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPPosition2.FormattingEnabled = true;
            this.cmbRPPosition2.Location = new System.Drawing.Point(582, 112);
            this.cmbRPPosition2.Name = "cmbRPPosition2";
            this.cmbRPPosition2.Size = new System.Drawing.Size(121, 21);
            this.cmbRPPosition2.TabIndex = 13;
            this.cmbRPPosition2.SelectedIndexChanged += new System.EventHandler(this.cmbRPPosition2_SelectedIndexChanged);
            // 
            // txtRPValue2
            // 
            this.txtRPValue2.Location = new System.Drawing.Point(481, 112);
            this.txtRPValue2.Name = "txtRPValue2";
            this.txtRPValue2.Size = new System.Drawing.Size(100, 20);
            this.txtRPValue2.TabIndex = 12;
            this.txtRPValue2.TextChanged += new System.EventHandler(this.txtRPValue2_TextChanged);
            // 
            // txtRPName2
            // 
            this.txtRPName2.Location = new System.Drawing.Point(380, 112);
            this.txtRPName2.Name = "txtRPName2";
            this.txtRPName2.Size = new System.Drawing.Size(100, 20);
            this.txtRPName2.TabIndex = 11;
            this.txtRPName2.TextChanged += new System.EventHandler(this.txtRPName2_TextChanged);
            // 
            // cmbRPOperator2
            // 
            this.cmbRPOperator2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPOperator2.FormattingEnabled = true;
            this.cmbRPOperator2.Location = new System.Drawing.Point(239, 112);
            this.cmbRPOperator2.Name = "cmbRPOperator2";
            this.cmbRPOperator2.Size = new System.Drawing.Size(139, 21);
            this.cmbRPOperator2.TabIndex = 10;
            this.cmbRPOperator2.SelectedIndexChanged += new System.EventHandler(this.cmbRPOperator2_SelectedIndexChanged);
            // 
            // cmbRPDocObjType2
            // 
            this.cmbRPDocObjType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPDocObjType2.FormattingEnabled = true;
            this.cmbRPDocObjType2.Location = new System.Drawing.Point(116, 111);
            this.cmbRPDocObjType2.Name = "cmbRPDocObjType2";
            this.cmbRPDocObjType2.Size = new System.Drawing.Size(121, 21);
            this.cmbRPDocObjType2.TabIndex = 9;
            this.cmbRPDocObjType2.SelectedIndexChanged += new System.EventHandler(this.cmbRPDocObjType2_SelectedIndexChanged);
            // 
            // cmbRPSection2
            // 
            this.cmbRPSection2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPSection2.FormattingEnabled = true;
            this.cmbRPSection2.Location = new System.Drawing.Point(48, 112);
            this.cmbRPSection2.Name = "cmbRPSection2";
            this.cmbRPSection2.Size = new System.Drawing.Size(66, 21);
            this.cmbRPSection2.TabIndex = 8;
            this.cmbRPSection2.SelectedIndexChanged += new System.EventHandler(this.cmbRPSection2_SelectedIndexChanged);
            // 
            // txtRPValue
            // 
            this.txtRPValue.Location = new System.Drawing.Point(481, 89);
            this.txtRPValue.Name = "txtRPValue";
            this.txtRPValue.Size = new System.Drawing.Size(100, 20);
            this.txtRPValue.TabIndex = 6;
            this.txtRPValue.TextChanged += new System.EventHandler(this.txtRPValue_TextChanged);
            // 
            // txtRPName
            // 
            this.txtRPName.Location = new System.Drawing.Point(380, 89);
            this.txtRPName.Name = "txtRPName";
            this.txtRPName.Size = new System.Drawing.Size(100, 20);
            this.txtRPName.TabIndex = 5;
            this.txtRPName.TextChanged += new System.EventHandler(this.txtRPName_TextChanged);
            // 
            // cmbRPOperator
            // 
            this.cmbRPOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPOperator.FormattingEnabled = true;
            this.cmbRPOperator.Location = new System.Drawing.Point(239, 89);
            this.cmbRPOperator.Name = "cmbRPOperator";
            this.cmbRPOperator.Size = new System.Drawing.Size(139, 21);
            this.cmbRPOperator.TabIndex = 4;
            this.cmbRPOperator.SelectedIndexChanged += new System.EventHandler(this.cmbRPOperator_SelectedIndexChanged);
            // 
            // cmbRPDocObjType
            // 
            this.cmbRPDocObjType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPDocObjType.FormattingEnabled = true;
            this.cmbRPDocObjType.Location = new System.Drawing.Point(116, 89);
            this.cmbRPDocObjType.Name = "cmbRPDocObjType";
            this.cmbRPDocObjType.Size = new System.Drawing.Size(121, 21);
            this.cmbRPDocObjType.TabIndex = 3;
            this.cmbRPDocObjType.SelectedIndexChanged += new System.EventHandler(this.cmbRPDocObjType_SelectedIndexChanged);
            // 
            // cmbRPSection
            // 
            this.cmbRPSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRPSection.FormattingEnabled = true;
            this.cmbRPSection.Location = new System.Drawing.Point(48, 89);
            this.cmbRPSection.Name = "cmbRPSection";
            this.cmbRPSection.Size = new System.Drawing.Size(66, 21);
            this.cmbRPSection.TabIndex = 2;
            this.cmbRPSection.SelectedIndexChanged += new System.EventHandler(this.cmbRPSection_SelectedIndexChanged);
            // 
            // Position
            // 
            this.Position.AutoSize = true;
            this.Position.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Position.Location = new System.Drawing.Point(583, 75);
            this.Position.Name = "Position";
            this.Position.Size = new System.Drawing.Size(44, 13);
            this.Position.TabIndex = 13;
            this.Position.Text = "Position";
            // 
            // Value
            // 
            this.Value.AutoSize = true;
            this.Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Value.Location = new System.Drawing.Point(481, 75);
            this.Value.Name = "Value";
            this.Value.Size = new System.Drawing.Size(34, 13);
            this.Value.TabIndex = 12;
            this.Value.Text = "Value";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(380, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(238, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Operator";
            // 
            // lblObject
            // 
            this.lblObject.AutoSize = true;
            this.lblObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObject.Location = new System.Drawing.Point(116, 75);
            this.lblObject.Name = "lblObject";
            this.lblObject.Size = new System.Drawing.Size(38, 13);
            this.lblObject.TabIndex = 2;
            this.lblObject.Text = "Object";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Section";
            // 
            // lstInstructionIndexes
            // 
            this.lstInstructionIndexes.FormattingEnabled = true;
            this.lstInstructionIndexes.Location = new System.Drawing.Point(4, 19);
            this.lstInstructionIndexes.Name = "lstInstructionIndexes";
            this.lstInstructionIndexes.Size = new System.Drawing.Size(21, 381);
            this.lstInstructionIndexes.Sorted = true;
            this.lstInstructionIndexes.TabIndex = 11;
            this.lstInstructionIndexes.SelectedIndexChanged += new System.EventHandler(this.lstInstructions_SelectedIndexChanged);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveDown.Image = global::LMP.DocAnalyzer.Admin.Properties.Resources.DownArrow;
            this.btnMoveDown.Location = new System.Drawing.Point(401, 98);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(26, 27);
            this.btnMoveDown.TabIndex = 6;
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMoveUp.Image = global::LMP.DocAnalyzer.Admin.Properties.Resources.UpArrow;
            this.btnMoveUp.Location = new System.Drawing.Point(401, 65);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(26, 27);
            this.btnMoveUp.TabIndex = 5;
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // tbtnAddDocType
            // 
            this.tbtnAddDocType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAddDocType.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAddDocType.Image")));
            this.tbtnAddDocType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAddDocType.Name = "tbtnAddDocType";
            this.tbtnAddDocType.Size = new System.Drawing.Size(71, 22);
            this.tbtnAddDocType.Text = "Add Type...";
            this.tbtnAddDocType.Click += new System.EventHandler(this.toolStripbtnAddDocType_Click);
            // 
            // tbtnDeleteDocType
            // 
            this.tbtnDeleteDocType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteDocType.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteDocType.Image")));
            this.tbtnDeleteDocType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteDocType.Name = "tbtnDeleteDocType";
            this.tbtnDeleteDocType.Size = new System.Drawing.Size(82, 22);
            this.tbtnDeleteDocType.Text = "Delete Type...";
            this.tbtnDeleteDocType.Click += new System.EventHandler(this.toolStripBtnDeleteDocType_Click);
            // 
            // tbtnImportElements
            // 
            this.tbtnImportElements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnImportElements.Image = ((System.Drawing.Image)(resources.GetObject("tbtnImportElements.Image")));
            this.tbtnImportElements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnImportElements.Name = "tbtnImportElements";
            this.tbtnImportElements.Size = new System.Drawing.Size(107, 22);
            this.tbtnImportElements.Text = "Import Elements...";
            this.tbtnImportElements.Click += new System.EventHandler(this.toolStripBtnImportElements_Click);
            // 
            // tbtnAddInstruction
            // 
            this.tbtnAddInstruction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAddInstruction.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAddInstruction.Image")));
            this.tbtnAddInstruction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAddInstruction.Name = "tbtnAddInstruction";
            this.tbtnAddInstruction.Size = new System.Drawing.Size(93, 22);
            this.tbtnAddInstruction.Text = "Add Instruction";
            this.tbtnAddInstruction.Click += new System.EventHandler(this.toolStripBtnAddInstruction_Click);
            // 
            // tbtnDeleteInstruction
            // 
            this.tbtnDeleteInstruction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteInstruction.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteInstruction.Image")));
            this.tbtnDeleteInstruction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteInstruction.Name = "tbtnDeleteInstruction";
            this.tbtnDeleteInstruction.Size = new System.Drawing.Size(113, 22);
            this.tbtnDeleteInstruction.Text = "Delete Instruction...";
            this.tbtnDeleteInstruction.Click += new System.EventHandler(this.toolStripBtnDeleteInstruction_Click);
            // 
            // tbtnEditDeterminants
            // 
            this.tbtnEditDeterminants.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnEditDeterminants.Enabled = false;
            this.tbtnEditDeterminants.Image = ((System.Drawing.Image)(resources.GetObject("tbtnEditDeterminants.Image")));
            this.tbtnEditDeterminants.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnEditDeterminants.Name = "tbtnEditDeterminants";
            this.tbtnEditDeterminants.Size = new System.Drawing.Size(114, 22);
            this.tbtnEditDeterminants.Text = "Edit Determinants...";
            this.tbtnEditDeterminants.Click += new System.EventHandler(this.tbtnEditDeterminants_Click);
            // 
            // tbtnManageEncryptionSeed
            // 
            this.tbtnManageEncryptionSeed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnManageEncryptionSeed.Image = ((System.Drawing.Image)(resources.GetObject("tbtnManageEncryptionSeed.Image")));
            this.tbtnManageEncryptionSeed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnManageEncryptionSeed.Name = "tbtnManageEncryptionSeed";
            this.tbtnManageEncryptionSeed.Size = new System.Drawing.Size(159, 22);
            this.tbtnManageEncryptionSeed.Text = "MacPac 9 Encryption Seed...";
            this.tbtnManageEncryptionSeed.ToolTipText = "Manage Encryption Seed...";
            this.tbtnManageEncryptionSeed.Click += new System.EventHandler(this.tbtnManageEncryptionSeed_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 820);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document Analyzer Administrator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdElements)).EndInit();
            this.pnlParseInstruction.ResumeLayout(false);
            this.pnlParseInstruction.PerformLayout();
            this.grpParseSource.ResumeLayout(false);
            this.grpParseSource.PerformLayout();
            this.pnlMatchInstructions.ResumeLayout(false);
            this.pnlMatchInstructions.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpMatchDomain.ResumeLayout(false);
            this.grpMatchDomain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cmbDocType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton tbtnAddDocType;
        private System.Windows.Forms.ToolStripButton tbtnDeleteDocType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbtnImportElements;
        private System.Windows.Forms.ToolStripButton tbtnAddInstruction;
        private System.Windows.Forms.ToolStripButton tbtnDeleteInstruction;
        private System.Windows.Forms.Label lblElements;
        private System.Windows.Forms.DataGridView grdElements;
        private System.Windows.Forms.ToolStripButton tbtnEditDeterminants;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel pnlMatchInstructions;
        private System.Windows.Forms.Label lblInstructionIndexes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRDName;
        private System.Windows.Forms.ComboBox cmbRDUnit;
        private System.Windows.Forms.ComboBox cmbRDDocObjType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMCValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMCName;
        private System.Windows.Forms.ComboBox cmbMCDocObjType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblMatchObjectType;
        private System.Windows.Forms.GroupBox grpMatchDomain;
        private System.Windows.Forms.Label lblRangeEnd;
        private System.Windows.Forms.Label lblRangeStart;
        private System.Windows.Forms.Label lblStory;
        private System.Windows.Forms.ComboBox cmbStory;
        private System.Windows.Forms.ComboBox cmbRPPosition;
        private System.Windows.Forms.CheckBox chkWholeRange;
        private System.Windows.Forms.ComboBox cmbRPPosition2;
        private System.Windows.Forms.TextBox txtRPValue2;
        private System.Windows.Forms.TextBox txtRPName2;
        private System.Windows.Forms.ComboBox cmbRPOperator2;
        private System.Windows.Forms.ComboBox cmbRPDocObjType2;
        private System.Windows.Forms.ComboBox cmbRPSection2;
        private System.Windows.Forms.TextBox txtRPValue;
        private System.Windows.Forms.TextBox txtRPName;
        private System.Windows.Forms.ComboBox cmbRPOperator;
        private System.Windows.Forms.ComboBox cmbRPDocObjType;
        private System.Windows.Forms.ComboBox cmbRPSection;
        private System.Windows.Forms.Label Position;
        private System.Windows.Forms.Label Value;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblObject;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lstInstructionIndexes;
        private System.Windows.Forms.Label lblMatchInstructions;
        private System.Windows.Forms.Label lblParseInstruction;
        private System.Windows.Forms.Panel pnlParseInstruction;
        private System.Windows.Forms.GroupBox grpParseSource;
        private System.Windows.Forms.RadioButton rdbtnElementMatchRange;
        private System.Windows.Forms.RadioButton rdbtnElementText;
        private System.Windows.Forms.Label lblTemplate;
        private System.Windows.Forms.TextBox txtTemplate;
        private System.Windows.Forms.ToolStripButton tbtnDeleteElement;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tbtnAddElement;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ElementName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnVisible;
        private System.Windows.Forms.DataGridViewComboBoxColumn NoMatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParseSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn Template;
        private System.Windows.Forms.DataGridViewTextBoxColumn SortOrder;
        private System.Windows.Forms.ToolStripButton tbtnManageEncryptionSeed;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}