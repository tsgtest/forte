using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace LMP.DocAnalyzer.Admin
{
    public partial class DeterminantsForm : Form
    {
        # region Declarations

        public List<Determinant> m_oDeterminantsList;
        public int m_iDocTypeVal;
        private DocType m_oDocType;
        private DataTable m_oDeterminantsListTable;
        bool m_bAllowDelete = false;
        # endregion

        # region Form Events

        public DeterminantsForm(string xTitle)
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(xTitle))
                this.Text = xTitle;
            InitializeDataTable();
        }

        private void DeterminantsForm_Load(object sender, EventArgs e)
        {
            //PopulateDocTypes();
            PopulateTypes();
            PopulateDeterminants(m_oDeterminantsList);
        }

        # endregion

        # region Private Methods
        //Method to Populate the Combo Type
        private void PopulateTypes()
        {
            colType.DataSource = Definitions.GetTypesList();
            colType.ValueMember = "Value";
            colType.DisplayMember = "Key";
            colType.DataPropertyName = "Type";

        }

        private void InitializeDataTable()
        {
            m_oDeterminantsListTable = new DataTable();
            //Initialize the Determinantslist DataTable
            m_oDeterminantsListTable.Columns.Add(new DataColumn("Type", typeof(int)));
            m_oDeterminantsListTable.Columns.Add(new DataColumn("Key", typeof(string)));
            m_oDeterminantsListTable.Columns.Add(new DataColumn("Value", typeof(string)));
            m_oDeterminantsListTable.Columns.Add(new DataColumn("ID", typeof(int)));


        }

        //Method to populate Determinants Grid
        private void PopulateDeterminants(List<Determinant> xDeterminantsList)
        {
            //Get the List of Determinants
            List<Determinant> oDeterminantsList = new List<Determinant>();
            oDeterminantsList = xDeterminantsList;
            m_oDeterminantsListTable.Rows.Clear();


            for (int i = 0; i < oDeterminantsList.Count; i++)
            {
                m_oDeterminantsListTable.Rows.Add(m_oDeterminantsListTable.NewRow());
                m_oDeterminantsListTable.Rows[i][0] = oDeterminantsList[i].Type;
                m_oDeterminantsListTable.Rows[i][1] = oDeterminantsList[i].Key;
                m_oDeterminantsListTable.Rows[i][2] = oDeterminantsList[i].Value;
                m_oDeterminantsListTable.Rows[i][3] = oDeterminantsList[i].DocTypeID;

            }

            //Set the Datasource for the Determinants Grid
            grdDeterminants.AutoGenerateColumns = false;
            grdDeterminants.DataSource = m_oDeterminantsListTable;


        }

        //Method to Save the changes in the Determinants Grid
        private void SaveDeterminants(int iRowIndex)
        {
            m_oDocType = new DocType(m_iDocTypeVal);
            bool bSuccessSave = false;
            int iDeterminantType = 0;
            string xDeterminantKey = string.Empty;
            string xDeterminantValue = string.Empty;
            int iDeterminantId = 0;
            bool bIsNewDeterminant = false;

            //iDeterminantType = (int)grdDeterminants.Rows[iRowIndex].Cells["ElementName"].Value;

            if (!string.IsNullOrEmpty(grdDeterminants.Rows[iRowIndex].Cells[0].Value.ToString()))
            {
                iDeterminantType = Convert.ToInt32(grdDeterminants.Rows[iRowIndex].Cells[0].Value.ToString());
            }

            if (!string.IsNullOrEmpty(grdDeterminants.Rows[iRowIndex].Cells[1].FormattedValue.ToString()))
            {
                xDeterminantKey = grdDeterminants.Rows[iRowIndex].Cells[1].FormattedValue.ToString();
            }
            if (!string.IsNullOrEmpty(grdDeterminants.Rows[iRowIndex].Cells[2].FormattedValue.ToString()))
            {
                xDeterminantValue = grdDeterminants.Rows[iRowIndex].Cells[2].FormattedValue.ToString();
            }
            if (!string.IsNullOrEmpty(grdDeterminants.Rows[iRowIndex].Cells[3].Value.ToString()))
            {
                iDeterminantId = Convert.ToInt32(grdDeterminants.Rows[iRowIndex].Cells[3].Value.ToString());
                bIsNewDeterminant = false;
            }
            else
            {
                xDeterminantValue = string.Empty;
                bIsNewDeterminant = true;
            }


            bSuccessSave = m_oDocType.UpdateDeterminant(iDeterminantType, xDeterminantKey, xDeterminantValue, iDeterminantId, bIsNewDeterminant);
            m_oDocType = new DocType(m_iDocTypeVal);
            PopulateDeterminants(m_oDocType.Determinants);

        }

        # endregion

        # region Determinants Grid Events

        private void grdDeterminants_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (grdDeterminants.IsCurrentRowDirty)
            {
                if (e.ColumnIndex == 0 && e.RowIndex != -1)
                {
                    if (grdDeterminants.IsCurrentCellInEditMode)
                    {
                        if (grdDeterminants.IsCurrentCellDirty)
                        {
                            SaveDeterminants(e.RowIndex);
                        }
                    }
                }
                else if (e.ColumnIndex == 1 && e.RowIndex != -1)
                {
                    if (grdDeterminants.IsCurrentCellInEditMode)
                    {
                        if (grdDeterminants.IsCurrentCellDirty)
                        {
                            SaveDeterminants(e.RowIndex);
                        }
                    }
                }
                else if (e.ColumnIndex == 2 && e.RowIndex != -1)
                {
                    if (grdDeterminants.IsCurrentCellInEditMode)
                    {
                        if (grdDeterminants.IsCurrentCellDirty)
                        {
                            SaveDeterminants(e.RowIndex);
                        }
                    }
                }


            }
        }

        private void grdDeterminants_KeyDown(object sender, KeyEventArgs e)
        {
            m_oDocType = new DocType(m_iDocTypeVal);
            Boolean bSuccessDelete = false;
            int oDeleteDeterminantId = 0;
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dr = MessageBox.Show("Delete this determinant? ", "Confirm deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    m_bAllowDelete = true;
                    if (!string.IsNullOrEmpty(grdDeterminants.Rows[grdDeterminants.CurrentRow.Index].Cells[3].FormattedValue.ToString()))
                    {
                        oDeleteDeterminantId = Convert.ToInt32(grdDeterminants.Rows[grdDeterminants.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                        bSuccessDelete = m_oDocType.DeleteDeterminant(oDeleteDeterminantId);
                    }
                }
                else
                {
                    m_bAllowDelete = false;
                }

            }
        }

        private void grdDeterminants_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (m_bAllowDelete)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }

        }

        # endregion

        # region Button Click Events

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        # endregion        

     
    }
}