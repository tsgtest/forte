﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.DocAnalyzer.Admin
{
    public partial class EncryptionSeedForm : Form
    {
        #region *********************fields*********************
        private bool m_bCancel = true;
        private string m_xKey;
        private string m_xMessage = LMP.Resources.GetLangString("Dialog_ControlPropertyEnterEncryptionSeed");
        #endregion
        
        public EncryptionSeedForm()
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(this.Message))
            {
                this.lblEncryptionSeed.Text = this.Message;
            }

            this.Key = LMP.Registry.GetLocalMachineValue(
              LMP.DocAnalyzer.Application.REGISTRY_ROOT, "MacPacEncryptionSeed");

            this.txtEncryptionSeed.Text = this.Key;
            //GLOG : 8002 : JSW 2/20/15
            //this.btnOK.Enabled = false;
        }
        #region *********************properties*********************
        public bool Cancelled
        {
            get { return m_bCancel; }
        }
        public string Key
        {
            get { return m_xKey; }
            set { m_xKey = value; }
        }
        public string Message
        {
            get { return m_xMessage; }
            set { m_xMessage = value; }

        }
        #endregion
        #region *********************events*********************
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = true;
                m_xKey = null;
                this.Close();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = false;
                m_xKey = this.txtEncryptionSeed.Text;
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        //GLOG : 8002 : JSW 2/20/15
        private void txtEncryptionSeed_TextChanged(object sender, EventArgs e)
        {
            //if (this.txtEncryptionSeed.Text == "")
            //{
            //    this.btnOK.Enabled = false;
            //}
            //else
            //{
            //    this.btnOK.Enabled = true;
            //}
        }
    }
}
