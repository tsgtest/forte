namespace LMP.DocAnalyzer.Admin
{
    partial class CreateDocTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDocType = new System.Windows.Forms.TextBox();
            this.lblDocType = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpDeterminant = new System.Windows.Forms.GroupBox();
            this.txtDeterminantValue = new System.Windows.Forms.TextBox();
            this.lblDeterminantKey = new System.Windows.Forms.Label();
            this.txtDeterminantKey = new System.Windows.Forms.TextBox();
            this.lblDeterminantType = new System.Windows.Forms.Label();
            this.cmbDeterminantType = new System.Windows.Forms.ComboBox();
            this.lblDeterminantValue = new System.Windows.Forms.Label();
            this.grpDeterminant.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtDocType
            // 
            this.txtDocType.Location = new System.Drawing.Point(24, 25);
            this.txtDocType.Name = "txtDocType";
            this.txtDocType.Size = new System.Drawing.Size(222, 20);
            this.txtDocType.TabIndex = 0;
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocType.Location = new System.Drawing.Point(22, 10);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(38, 13);
            this.lblDocType.TabIndex = 0;
            this.lblDocType.Text = "&Name:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(100, 231);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAddDocType_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(181, 231);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpDeterminant
            // 
            this.grpDeterminant.Controls.Add(this.txtDeterminantValue);
            this.grpDeterminant.Controls.Add(this.lblDeterminantKey);
            this.grpDeterminant.Controls.Add(this.txtDeterminantKey);
            this.grpDeterminant.Controls.Add(this.lblDeterminantType);
            this.grpDeterminant.Controls.Add(this.cmbDeterminantType);
            this.grpDeterminant.Location = new System.Drawing.Point(12, 59);
            this.grpDeterminant.Name = "grpDeterminant";
            this.grpDeterminant.Size = new System.Drawing.Size(244, 160);
            this.grpDeterminant.TabIndex = 4;
            this.grpDeterminant.TabStop = false;
            this.grpDeterminant.Text = "Determinant";
            // 
            // txtDeterminantValue
            // 
            this.txtDeterminantValue.Location = new System.Drawing.Point(12, 121);
            this.txtDeterminantValue.Name = "txtDeterminantValue";
            this.txtDeterminantValue.Size = new System.Drawing.Size(222, 20);
            this.txtDeterminantValue.TabIndex = 3;
            // 
            // lblDeterminantKey
            // 
            this.lblDeterminantKey.AutoSize = true;
            this.lblDeterminantKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeterminantKey.Location = new System.Drawing.Point(11, 60);
            this.lblDeterminantKey.Name = "lblDeterminantKey";
            this.lblDeterminantKey.Size = new System.Drawing.Size(28, 13);
            this.lblDeterminantKey.TabIndex = 4;
            this.lblDeterminantKey.Text = "&Key:";
            // 
            // txtDeterminantKey
            // 
            this.txtDeterminantKey.Location = new System.Drawing.Point(12, 76);
            this.txtDeterminantKey.Name = "txtDeterminantKey";
            this.txtDeterminantKey.Size = new System.Drawing.Size(222, 20);
            this.txtDeterminantKey.TabIndex = 2;
            // 
            // lblDeterminantType
            // 
            this.lblDeterminantType.AutoSize = true;
            this.lblDeterminantType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeterminantType.Location = new System.Drawing.Point(12, 18);
            this.lblDeterminantType.Name = "lblDeterminantType";
            this.lblDeterminantType.Size = new System.Drawing.Size(34, 13);
            this.lblDeterminantType.TabIndex = 2;
            this.lblDeterminantType.Text = "&Type:";
            // 
            // cmbDeterminantType
            // 
            this.cmbDeterminantType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeterminantType.FormattingEnabled = true;
            this.cmbDeterminantType.Location = new System.Drawing.Point(12, 33);
            this.cmbDeterminantType.Name = "cmbDeterminantType";
            this.cmbDeterminantType.Size = new System.Drawing.Size(222, 21);
            this.cmbDeterminantType.TabIndex = 1;
            this.cmbDeterminantType.SelectedIndexChanged += new System.EventHandler(this.cmbDeterminantType_SelectedIndexChanged);
            // 
            // lblDeterminantValue
            // 
            this.lblDeterminantValue.AutoSize = true;
            this.lblDeterminantValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeterminantValue.Location = new System.Drawing.Point(24, 165);
            this.lblDeterminantValue.Name = "lblDeterminantValue";
            this.lblDeterminantValue.Size = new System.Drawing.Size(37, 13);
            this.lblDeterminantValue.TabIndex = 6;
            this.lblDeterminantValue.Text = "&Value:";
            // 
            // CreateDocTypeForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(270, 266);
            this.Controls.Add(this.lblDeterminantValue);
            this.Controls.Add(this.grpDeterminant);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblDocType);
            this.Controls.Add(this.txtDocType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateDocTypeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Document Type";
            this.Load += new System.EventHandler(this.CreateDocTypeForm_Load);
            this.grpDeterminant.ResumeLayout(false);
            this.grpDeterminant.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDocType;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox grpDeterminant;
        private System.Windows.Forms.TextBox txtDeterminantValue;
        private System.Windows.Forms.Label lblDeterminantKey;
        private System.Windows.Forms.TextBox txtDeterminantKey;
        private System.Windows.Forms.Label lblDeterminantType;
        private System.Windows.Forms.ComboBox cmbDeterminantType;
        private System.Windows.Forms.Label lblDeterminantValue;
    }
}