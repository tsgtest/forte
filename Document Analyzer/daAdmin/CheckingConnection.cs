using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using daCOM;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.DocAnalyzer.Admin
{
    public partial class CheckingConnection : Form
    {
        CInstruction oInstruction = new CInstruction();
        List<Element> elementresultlist;
        List<Determinant> determinantresultlist;
        List<CInstruction> instructionresultlist;
        public CheckingConnection()
        {
            InitializeComponent();
        }

        private void btnforTesting_Click(object sender, EventArgs e)
        {
            DocType res = new DocType();
            Connection cn = new Connection();
            Word.Document adoc=null;
            res = cn.GetDocType();
            textBox1.Text = res.Name;

            res = new DocType(12, adoc);
            elementresultlist = new List<Element>();
            elementresultlist = res.GetElements();
            bindingSource1.DataSource = elementresultlist;
            dataGridView1.DataSource = bindingSource1;


            determinantresultlist = new List<Determinant>();
            determinantresultlist = res.GetDeterminants();
            bindingSource2.DataSource = determinantresultlist;
            dataGridView2.DataSource = bindingSource2;
            string elementname = string.Empty;
            string elementvalue = string.Empty;
            
            for (int j = 0; j < elementresultlist.Count; j++)
            {
                Element element = new Element(elementresultlist[j].ID, elementresultlist[j].Name, elementresultlist[j].ID, elementresultlist[j].DoParsing, elementresultlist[j].NoMatch, adoc);

                instructionresultlist = new List<CInstruction>();
                //Get a list of instructions for each element
                instructionresultlist = element.GetInstructions();
              
                for (int k = 0; k < instructionresultlist.Count; k++)
                {
                    listBox1.Items.Add(instructionresultlist[k].ElementID);
                    listBox1.Items.Add(instructionresultlist[k].ExecutionIndex);
                    listBox1.Items.Add(instructionresultlist[k].Story);
                    if(!(instructionresultlist[k].Range==null))
                    {
                    listBox1.Items.Add(instructionresultlist[k].Range);
                    }
                    if (!(instructionresultlist[k].Text == null))
                    {
                        listBox1.Items.Add(instructionresultlist[k].Text);
                    }
                   


                }
                if (instructionresultlist.Count > 0)
                {
                    //Element Name
                    elementname = elementresultlist[j].Name;
                    //Method to return value of the element
                    //elementvalue = element.GetValue();

                    break;
                }
            }

         
            //Form the XML 
            //string gettingxml = res.AnalyzeDocument(elementname, oInstruction);
            //string gettingxml = res.AnalyzeDocument(elementname, elementvalue);

           // MessageBox.Show(gettingxml);
        }

      
    }
}