namespace LMP.DocAnalyzer.Admin
{
    partial class Tester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnalyze = new System.Windows.Forms.Button();
            this.btnAnalyzeString = new System.Windows.Forms.Button();
            this.chkShowESForm = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnAnalyze
            // 
            this.btnAnalyze.Location = new System.Drawing.Point(95, 42);
            this.btnAnalyze.Name = "btnAnalyze";
            this.btnAnalyze.Size = new System.Drawing.Size(100, 38);
            this.btnAnalyze.TabIndex = 0;
            this.btnAnalyze.Text = "Analyze Document - XML";
            this.btnAnalyze.UseVisualStyleBackColor = true;
            this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
            // 
            // btnAnalyzeString
            // 
            this.btnAnalyzeString.Location = new System.Drawing.Point(96, 84);
            this.btnAnalyzeString.Name = "btnAnalyzeString";
            this.btnAnalyzeString.Size = new System.Drawing.Size(100, 38);
            this.btnAnalyzeString.TabIndex = 1;
            this.btnAnalyzeString.Text = "Analyze Document - String";
            this.btnAnalyzeString.UseVisualStyleBackColor = true;
            this.btnAnalyzeString.Click += new System.EventHandler(this.btnAnalyzeString_Click);
            // 
            // chkShowESForm
            // 
            this.chkShowESForm.AutoSize = true;
            this.chkShowESForm.Checked = true;
            this.chkShowESForm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowESForm.Location = new System.Drawing.Point(95, 153);
            this.chkShowESForm.Name = "chkShowESForm";
            this.chkShowESForm.Size = new System.Drawing.Size(167, 17);
            this.chkShowESForm.TabIndex = 2;
            this.chkShowESForm.Text = "Show Element Selection Form";
            this.chkShowESForm.UseVisualStyleBackColor = true;
            // 
            // Tester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 207);
            this.Controls.Add(this.chkShowESForm);
            this.Controls.Add(this.btnAnalyzeString);
            this.Controls.Add(this.btnAnalyze);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Tester";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAnalyze;
        private System.Windows.Forms.Button btnAnalyzeString;
        private System.Windows.Forms.CheckBox chkShowESForm;
    }
}