using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using WinForms = System.Windows.Forms;
using System.Data.OleDb;
using System.Reflection;
using LMP.DocumentAnalyzer.MSWord;

namespace LMP.DocAnalyzer.Admin
{
    public partial class MainForm : Form
    {
        #region***********************fields****************************
        DataTable m_dtElementslist;

        DocType m_oDocType;
        List<Instruction> m_oInstructions;
        bool m_bAllowDelete = false;
        bool m_bCurInstructionDirty = false;

        bool m_bInstructionWholeRange;
        bool m_bNewElement=false;
        bool m_bDeleteElement = false;
        bool m_bCurParsingInstructionDirty = false;
        bool m_bLoadingInstructions = false;
        
        int m_iSelectedElementRow = -1;
        int m_iRowEnter = -1;
        int m_iParseElementRowIndex = -1;
        
        private Definitions.daParseSource m_iParsingSource = Definitions.daParseSource.daElementValue;
        private ParsingInstruction m_oCurrentParsingInstruction;
        private Instruction m_oCurrentInstruction = new Instruction();

        #endregion***********************fields****************************

        #region***********************private functions****************************

        public MainForm()
        {
            InitializeComponent();
            InitializeDataTable();

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                PopulateDocTypes();
                m_bCurInstructionDirty = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //Initialize the DataTable
        private void InitializeDataTable()
        {

            m_dtElementslist = new DataTable();

            //Initialize the Elementslist DataTable
            m_dtElementslist.Columns.Add(new DataColumn("Name", typeof(string)));
            m_dtElementslist.Columns.Add(new DataColumn("DisplayName", typeof(string)));
            m_dtElementslist.Columns.Add(new DataColumn("Visible", typeof(int)));
            m_dtElementslist.Columns.Add(new DataColumn("NoMatch", typeof(int)));
            m_dtElementslist.Columns.Add(new DataColumn("ID", typeof(int)));
            m_dtElementslist.Columns.Add(new DataColumn("ParsingSource", typeof(int)));
            m_dtElementslist.Columns.Add(new DataColumn("Template", typeof(string)));
            m_dtElementslist.Columns.Add(new DataColumn("SortOrder", typeof(int)));

            LoadInstructionsPanelComboBoxes();
        }

        //Method to add a default item to a doctype combo
        private void AddItem(IList oList, Type oType, string xValueMember, string xDisplayMember, string xDisplayText)
        {
            //Creates an instance of the specified type using the constructor that best matches the specified parameters.
            Object obj = Activator.CreateInstance(oType);

            // Gets the Display Property Information
            PropertyInfo oDisplayProperty = oType.GetProperty(xDisplayMember);

            // Sets the required text into the display property
            oDisplayProperty.SetValue(obj, xDisplayText, null);

            // Gets the Value Property Information
            PropertyInfo oValueProperty = oType.GetProperty(xValueMember);

            // Sets the required value into the value property
            oValueProperty.SetValue(obj, -1, null);

            // Insert the new object on the list
            oList.Insert(0, obj);
        }

        //Method to populate DocTypes Combo and Comboxes within the Grid
        private void PopulateDocTypes()
        {
            cmbDocType.DataSource = null;
            Definitions.m_bDocOpen = false;
            //Get DocType List and populate the comboBox
            List<DocType> oDocTypeList = new List<DocType>();
            oDocTypeList = Definitions.GetDocTypesList();
            AddItem(oDocTypeList, typeof(DocType), "ID", "Name", "< Select Document Type >");
            cmbDocType.DataSource = oDocTypeList;
            cmbDocType.DisplayMember = "Name";
            cmbDocType.ValueMember = "ID";
            cmbDocType.SelectedIndex = 0;
            PopulateMatchList();
            cmbDocType.Focus();

        }

        //Method to Populate the Combo NoMatch
        private void PopulateMatchList()
        {
            string[] names = Enum.GetNames(typeof(Element.daNoMatch));
            List<KeyValuePair<string, int>> xMatchList = new List<KeyValuePair<string, int>>();
            for (int i = 1; i <= names.Length; i++)
            {
                xMatchList.Add(new KeyValuePair<string, int>(Enum.GetName(typeof(Element.daNoMatch), i), i));
            }
            this.NoMatch.DataSource = xMatchList;
            this.NoMatch.DisplayMember = "Key";
            this.NoMatch.ValueMember = "Value";
            this.NoMatch.DataPropertyName = "NoMatch";

        }

        private void PopulateElements(List<Element> oElementsList)
        {
            try
            {
                //Get the List of Elements 
                m_dtElementslist.Rows.Clear();

                for (int i = 0; i < oElementsList.Count; i++)
                {

                    m_dtElementslist.Rows.Add(m_dtElementslist.NewRow());
                    m_dtElementslist.Rows[i][0] = oElementsList[i].Name;
                    m_dtElementslist.Rows[i][1] = oElementsList[i].DisplayName;
                    m_dtElementslist.Rows[i][2] = oElementsList[i].Visible;
                    m_dtElementslist.Rows[i][3] = oElementsList[i].NoMatch;
                    m_dtElementslist.Rows[i][4] = oElementsList[i].ID;
                    m_dtElementslist.Rows[i][5] = oElementsList[i].ParsingInstructions.ParseSource;
                    m_dtElementslist.Rows[i][6] = oElementsList[i].ParsingInstructions.Template;
                    m_dtElementslist.Rows[i][7] = oElementsList[i].SortOrder;


                }
                if (oElementsList.Count == 0)
                {
                    m_dtElementslist.Clear();
                }
                grdElements.AutoGenerateColumns = false;
                grdElements.DataSource = m_dtElementslist;
                if (m_iSelectedElementRow > -1)
                {

                    grdElements.Rows[m_iSelectedElementRow].Selected = true;
                    grdElements.CurrentCell = grdElements[0, m_iSelectedElementRow];
                }
                else
                {
                    m_iRowEnter = -1;
                    //m_iRowEnter = 0;
                    //grdElements.Rows[0].Selected = true;
                    //grdElements.CurrentCell = grdElements[0, 0];
                }

                //hide this column until code is in place.
                grdElements.Columns["Visible"].Visible = true;
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }

        }

        //Method for populating Combo boxes
        private void LoadInstructionsPanelComboBoxes()
        {
            # region Populate Combo story
            this.cmbStory.Items.Clear();
            this.cmbStory.Items.Insert(0, "None");
            this.cmbStory.Items.Insert(1, "Main");
            this.cmbStory.Items.Insert(2, "Header");
            this.cmbStory.Items.Insert(3, "Footer");
            this.cmbStory.Items.Insert(4, "All");
            this.cmbStory.SelectedIndex = 1;
            # endregion

            # region Populate Combo Position

            this.cmbRPPosition.Items.Clear();

            this.cmbRPPosition.Items.Insert(0, "Start");
            this.cmbRPPosition.Items.Insert(1, "End");
            this.cmbRPPosition.Items.Insert(2, "Whole");
            this.cmbRPPosition.SelectedIndex = 0;
            # endregion

            # region Populate Combo Position
            this.cmbRPPosition2.Items.Clear();

            this.cmbRPPosition2.Items.Insert(0, "Start");
            this.cmbRPPosition2.Items.Insert(1, "End");
            this.cmbRPPosition2.SelectedIndex = 0;

            # endregion

            # region Populate Combo Parameter
            //this.cmbRangeParameter.Items.Clear();
            //this.cmbRangeParameter.Items.Insert(0, "Whole Range");
            //this.cmbRangeParameter.Items.Insert(1, "Start Range");
            //this.cmbRangeParameter.SelectedIndex = 1;
            //# endregion

            //# region Populate Combo Parameter2
            //this.cmbRangeParameter2.Items.Clear();

            //this.cmbRangeParameter2.Items.Insert(0, "End Range");
            //this.cmbRangeParameter2.SelectedIndex = 0;
            # endregion

            # region Populate Combo Section
            this.cmbRPSection.Items.Clear();
            this.cmbRPSection.Items.Insert(0, "All");
            this.cmbRPSection.Items.Insert(1, "First");
            this.cmbRPSection.Items.Insert(2, "Last");
            this.cmbRPSection.Items.Insert(3, "Number");
            this.cmbRPSection.SelectedIndex = 0;
            # endregion

            # region Populate Combo Section2
            this.cmbRPSection2.Items.Clear();
            this.cmbRPSection2.Items.Insert(0, "All");
            this.cmbRPSection2.Items.Insert(1, "First");
            this.cmbRPSection2.Items.Insert(2, "Last");
            this.cmbRPSection2.Items.Insert(3, "Number");
            this.cmbRPSection2.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPOperator
            this.cmbRPOperator.Items.Clear();
            this.cmbRPOperator.Items.Insert(0, "Whose name is");
            this.cmbRPOperator.Items.Insert(1, "That is");
            this.cmbRPOperator.Items.Insert(2, "That contains the text");
            this.cmbRPOperator.Items.Insert(3, "Whose index is");
            this.cmbRPOperator.Items.Insert(4, "That contains the style");
            this.cmbRPOperator.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPOperator2
            this.cmbRPOperator2.Items.Clear();
            this.cmbRPOperator2.Items.Insert(0, "Whose name is");
            this.cmbRPOperator2.Items.Insert(1, "That is");
            this.cmbRPOperator2.Items.Insert(2, "That contains the text");
            this.cmbRPOperator2.Items.Insert(3, "Whose index is");
            this.cmbRPOperator2.Items.Insert(4, "That contains the style");
            this.cmbRPOperator2.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPDocObjType
            this.cmbRPDocObjType.Items.Clear();
            this.cmbRPDocObjType.Items.Insert(0, "Bookmark");
            this.cmbRPDocObjType.Items.Insert(1, "First Instance of Text");
            this.cmbRPDocObjType.Items.Insert(2, "Last Instance of Text");
            this.cmbRPDocObjType.Items.Insert(3, "Style");
            this.cmbRPDocObjType.Items.Insert(4, "Paragraph");
            this.cmbRPDocObjType.Items.Insert(5, "Table");
            this.cmbRPDocObjType.Items.Insert(6, "Row");
            this.cmbRPDocObjType.Items.Insert(7, "Cell");
            this.cmbRPDocObjType.Items.Insert(8, "Keyword");
            this.cmbRPDocObjType.Items.Insert(9, "Element");
            this.cmbRPDocObjType.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPDocObjType2
            this.cmbRPDocObjType2.Items.Clear();
            this.cmbRPDocObjType2.Items.Insert(0, "Bookmark");
            this.cmbRPDocObjType2.Items.Insert(1, "First Instance of Text");
            this.cmbRPDocObjType2.Items.Insert(2, "Last Instance of Text");
            this.cmbRPDocObjType2.Items.Insert(3, "Style");
            this.cmbRPDocObjType2.Items.Insert(4, "Paragraph");
            this.cmbRPDocObjType2.Items.Insert(5, "Table");
            this.cmbRPDocObjType2.Items.Insert(6, "Row");
            this.cmbRPDocObjType2.Items.Insert(7, "Cell");
            this.cmbRPDocObjType2.Items.Insert(8, "Keyword");
            this.cmbRPDocObjType2.Items.Insert(9, "Element");
            this.cmbRPDocObjType2.SelectedIndex = 0;
            # endregion

            # region Load DocType combo Box

            this.cmbMCDocObjType.Items.Clear();
            cmbMCDocObjType.Items.Insert(0, "Nothing");
            cmbMCDocObjType.Items.Insert(1, "Bookmark");
            cmbMCDocObjType.Items.Insert(2, "Document Variable");
            cmbMCDocObjType.Items.Insert(3, "Document Property");
            cmbMCDocObjType.Items.Insert(4, "First Instance of Style");
            cmbMCDocObjType.Items.Insert(5, "Last Instance of Style");
            cmbMCDocObjType.Items.Insert(6, "All Instances of Style");
            cmbMCDocObjType.Items.Insert(7, "First Instance of Text");
            cmbMCDocObjType.Items.Insert(8, "Last Instance of Text");
            cmbMCDocObjType.Items.Insert(9, "Paragraph Containing Text");
            cmbMCDocObjType.Items.Insert(10, "Paragraph Whose Index Is");
            cmbMCDocObjType.Items.Insert(11, "Table Containing Text");
            cmbMCDocObjType.Items.Insert(12, "Table Containing Style");
            cmbMCDocObjType.Items.Insert(13, "Cell Containing Text");
            cmbMCDocObjType.Items.Insert(14, "Cell Containing Style");
            cmbMCDocObjType.Items.Insert(15, "Keyword");
            cmbMCDocObjType.Items.Insert(16, "Selection");
            cmbMCDocObjType.SelectedIndex = 0;

            # endregion

            # region Load RDDocType combo Box

            this.cmbRDDocObjType.Items.Clear();
            this.cmbRDDocObjType.Items.Insert(0, "The value");
            this.cmbRDDocObjType.Items.Insert(1, "Nothing");
            this.cmbRDDocObjType.Items.Insert(2, "True");
            this.cmbRDDocObjType.Items.Insert(3, "False");
            this.cmbRDDocObjType.Items.Insert(4, "From Start Of X to Matched Text");
            this.cmbRDDocObjType.Items.Insert(5, "From Matched Text to End of X");
            this.cmbRDDocObjType.Items.Insert(6, "From End of Matched Text to End of X");
            this.cmbRDDocObjType.Items.Insert(7, "Next X");
            this.cmbRDDocObjType.Items.Insert(8, "Previous X");
            this.cmbRDDocObjType.Items.Insert(9, "Current X");
            this.cmbRDDocObjType.Items.Insert(10, "Mapped Value");
            this.cmbRDDocObjType.SelectedIndex = 0;

            # endregion

            # region Load Measurement Unit

            this.cmbRDUnit.Items.Clear();
            this.cmbRDUnit.Items.Insert(0, "Character");
            this.cmbRDUnit.Items.Insert(1, "Word");
            this.cmbRDUnit.Items.Insert(2, "Line");
            this.cmbRDUnit.Items.Insert(3, "Sentence");
            this.cmbRDUnit.Items.Insert(4, "Paragraph");
            this.cmbRDUnit.Items.Insert(5, "Table");
            this.cmbRDUnit.Items.Insert(6, "Cell");
            this.cmbRDUnit.Items.Insert(7, "Row");
            this.cmbRDUnit.Items.Insert(8, "Nothing");
            this.cmbRDUnit.SelectedIndex = 8;

            # endregion

            txtMCName.Text = string.Empty;
            txtMCValue.Text = string.Empty;
            txtRPName.Text = string.Empty;
            txtRPName2.Text = string.Empty;
            txtRPValue.Text = string.Empty;
            txtRPValue2.Text = string.Empty;
        }

        //Method for adding new Element
        private void AddElements()
        {

            string xElementName = string.Empty;
            int iElementId = 0;
            int iNewElementID = 0;
            int iElementSource = -1;
            string xElementTemplate = string.Empty;
            string xDisplayName = "New Element"; 
            int iSortOrder = this.grdElements.Rows.Count;
            bool bVisible = true;

            iNewElementID = m_oDocType.UpdateElement("New Element",
                1, iElementId, true, iElementSource, xElementTemplate, xDisplayName, iSortOrder, bVisible);

            Element oElement = new Element(iNewElementID);

            bool bInsertInstructions = oElement.SaveInstruction(1, 1, "0", "0", "0", "0", "0�8", true, 2);
            tbtnAddInstruction.Enabled = true;
            tbtnDeleteInstruction.Enabled = true;

            m_bDeleteElement = false;
            //m_oDocType = new DocType((int)cmbDocType.SelectedValue);
            DocType oDocType = (DocType)cmbDocType.SelectedItem;
            m_oDocType = new DocType(oDocType.ID);
            //m_oDocType = (DocType)cmbDocType.SelectedItem;
            PopulateElements(m_oDocType.Elements);
            PopulateInstructionsListBox();
            DataGridViewColumn oColumn = new DataGridViewColumn();
            oColumn = this.grdElements.Columns["SortOrder"];
            //grdElements.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

            grdElements.Sort(oColumn, ListSortDirection.Ascending);

            //select new row
            m_iSelectedElementRow = this.grdElements.Rows.Count - 1;
            grdElements.Rows[m_iSelectedElementRow].Selected = true;
            grdElements.CurrentCell = grdElements[0, m_iSelectedElementRow];
            this.pnlMatchInstructions.Enabled = true;
            this.pnlParseInstruction.Enabled = true;
        }

        //Method to Save the changes in the Elements Grid
        private void SaveElements(int iRowIndex)
        {
            try
            {
                string xElementName = string.Empty;
                //bool xParsing = false;
                int iNMatch = 0;
                int iElementId = 0; ;
                bool bNewElement = false;
                //bool bNewInstruction = false;
                //int iNewExceutionIndex = 0;
                int iNewElementID = 0;
                int iElementSource = -1;
                string xElementTemplate = string.Empty;
                string xDisplayName = string.Empty;
                int iSortOrder = 0;
                bool bVisible = false;

                if (m_oCurrentParsingInstruction != null)
                {
                    iElementSource = (int)m_oCurrentParsingInstruction.ParseSource;
                    xElementTemplate = m_oCurrentParsingInstruction.Template;
                }

                DataGridViewCellCollection oCells = grdElements.Rows[iRowIndex].Cells;

                if (!string.IsNullOrEmpty(oCells["ElementName"].FormattedValue.ToString()))
                {
                    xElementName = oCells["ElementName"].FormattedValue.ToString();
                }
                else
                {
                    xElementName = string.Empty;
                    MessageBox.Show("Please enter the name for Element", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    grdElements.Rows[iRowIndex].Selected = true;
                    grdElements.CurrentCell = grdElements[0, iRowIndex];                   
                    return;
                }

                if (!string.IsNullOrEmpty(oCells["NoMatch"].FormattedValue.ToString()))
                {
                    iNMatch = (int)oCells["NoMatch"].Value;
                }
                else
                {
                    iNMatch = 1;
                }
                if (!string.IsNullOrEmpty(oCells["ID"].FormattedValue.ToString()))
                {
                    iElementId = Convert.ToInt32(oCells["ID"].FormattedValue.ToString());
                    m_oCurrentInstruction.ElementID = (short)iElementId;
                    bNewElement = false;
                }
                if (!string.IsNullOrEmpty(oCells["DisplayName"].FormattedValue.ToString()))
                {
                    xDisplayName = oCells["DisplayName"].FormattedValue.ToString();
                }
                if (!string.IsNullOrEmpty(oCells["SortOrder"].FormattedValue.ToString()))
                {
                    iSortOrder = (int)oCells["SortOrder"].Value;
                }
                if (!string.IsNullOrEmpty(oCells["Visible"].FormattedValue.ToString()))
                {
                    if (oCells["Visible"].FormattedValue.ToString().ToLower() == "false")
                        bVisible = false;
                    else
                    {
                        bVisible = true;
                    }
                }
                
                //else
                //{
                //    bNewElement = true;
                //    bNewInstruction = true;
                //    iNewExceutionIndex = CheckInstructionsList();
                //    if (iNewExceutionIndex == 0)
                //    {
                //        iNewExceutionIndex = iNewExceutionIndex + 1;
                //    }
                //}

                iNewElementID = m_oDocType.UpdateElement(xElementName, 
                    iNMatch, iElementId, bNewElement, iElementSource, xElementTemplate, xDisplayName, iSortOrder, bVisible);

                //if (bNewInstruction)
                //{
                //    Element oElement = new Element(iNewElementID);

                //    bool bInsertInstructions = oElement.SaveInstruction(1, iNewExceutionIndex, "0", "0", "0", "0", "0�8", true);
                //    tbtnAddInstruction.Enabled = true;
                //    tbtnDeleteInstruction.Enabled = true;
                //}

                m_bDeleteElement = false;
                //m_oDocType = new DocType((int)cmbDocType.SelectedValue);
                //m_oDocType = new DocType(this.cmbDocType.Text);
                DocType oDocType = (DocType)cmbDocType.SelectedItem;
                m_oDocType = new DocType(oDocType.ID);
                //PopulateElements(m_oDocType.Elements);
               // grdElements.Rows[iRowIndex].Cells["Template"].FormattedValue = xElementName.Template;
                PopulateInstructionsListBox();
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }

        /// <summary>
        /// returns the ExecutionIndex of the element 
        ///Check whether the element has any existing Execution index in the Instructions
        ///if present add the new one taking the last value plus one.
        ///If not present start the ExecutionIndex from 1.
        /// </summary>
        private int CheckInstructionsList()
        {
            int iExecutionIndex = 0;
            if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString()))
            {
                if (lstInstructionIndexes.Items.Count > 0)
                {
                    iExecutionIndex = (Convert.ToInt32(lstInstructionIndexes.Items[lstInstructionIndexes.Items.Count - 1].ToString())) + 1;
                    lstInstructionIndexes.Items.Add(iExecutionIndex);
                    lstInstructionIndexes.Refresh();
                }
                else
                {
                    iExecutionIndex = 1;
                    lstInstructionIndexes.Items.Clear();
                    lstInstructionIndexes.Items.Add(iExecutionIndex);
                    lstInstructionIndexes.Refresh();
                }
            }
            return iExecutionIndex;
        }

        //Method to Load the Instructions based on the selection of ExecutionIndex on the ListBox
        private void LoadInstruction(int iExecutionIndex)
        {
            try
            {
                ClearInstructions();
                //create new instruction if necessary
                if (iExecutionIndex > m_oInstructions.Count)
                    AddNewInstruction();
                else
                    m_oCurrentInstruction = m_oInstructions[iExecutionIndex - 1];

                cmbStory.SelectedIndex = m_oCurrentInstruction.Story;
                if ((!string.IsNullOrEmpty(m_oCurrentInstruction.WholeRangeName)) || 
                    (!string.IsNullOrEmpty(m_oCurrentInstruction.WholeRangeValue)))
                {
                    chkWholeRange.Checked = true;
                }
                else
                    chkWholeRange.Checked = false;
                
                
                if (chkWholeRange.Checked)
                {
                    cmbRPSection.SelectedIndex = (int)m_oCurrentInstruction.WholeRangeSection;
                    cmbRPDocObjType.SelectedIndex = (int)m_oCurrentInstruction.WholeRangeType;
                    cmbRPOperator.SelectedIndex = (int)m_oCurrentInstruction.WholeRangeOperator;
                    
                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.WholeRangeName))
                    {
                        if (m_oCurrentInstruction.WholeRangeName != "0")
                            txtRPName.Text = m_oCurrentInstruction.WholeRangeName;
                        else
                            txtRPName.Text = string.Empty;
                    }
                    else
                        txtRPName.Text = string.Empty;

                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.WholeRangeValue))
                    {
                        if (m_oCurrentInstruction.WholeRangeValue != "0")
                            txtRPValue.Text = m_oCurrentInstruction.WholeRangeValue;
                        else
                            txtRPValue.Text = string.Empty;
                    }
                    else
                        txtRPValue.Text = string.Empty;

                }
                else
                {
                    cmbRPSection.SelectedIndex = (int)m_oCurrentInstruction.StartRangeSection;
                    cmbRPDocObjType.SelectedIndex = (int)m_oCurrentInstruction.StartRangeType;
                    cmbRPOperator.SelectedIndex = (int)m_oCurrentInstruction.StartRangeOperator;
                    cmbRPPosition.SelectedIndex = (int)m_oCurrentInstruction.StartRangePosition;
                    
                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.StartRangeName))
                    {
                        if (m_oCurrentInstruction.StartRangeName != "0")
                            txtRPName.Text = m_oCurrentInstruction.StartRangeName;
                        else
                            txtRPName.Text = string.Empty;
                    }
                    else
                        txtRPName.Text = string.Empty;

                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.StartRangeValue))
                    {
                        if (m_oCurrentInstruction.StartRangeValue != "0")
                            txtRPValue.Text = m_oCurrentInstruction.StartRangeValue;
                        else
                            txtRPValue.Text = string.Empty;
                    }
                    else
                        txtRPValue.Text = string.Empty;

                    cmbRPSection2.SelectedIndex = (int)m_oCurrentInstruction.EndRangeSection;
                    cmbRPDocObjType2.SelectedIndex = (int)m_oCurrentInstruction.EndRangeType;
                    cmbRPOperator2.SelectedIndex = (int)m_oCurrentInstruction.EndRangeOperator;
                    cmbRPPosition2.SelectedIndex = (int)m_oCurrentInstruction.EndRangePosition;
                    
                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.EndRangeName))
                    {
                        if (m_oCurrentInstruction.EndRangeName != "0")
                            txtRPName2.Text = m_oCurrentInstruction.EndRangeName;
                        else
                            txtRPName2.Text = string.Empty;
                    }
                    else
                        txtRPName2.Text = string.Empty;

                    if (!string.IsNullOrEmpty(m_oCurrentInstruction.EndRangeValue))
                    {
                        if (m_oCurrentInstruction.EndRangeValue != "0")
                            txtRPValue2.Text = m_oCurrentInstruction.EndRangeValue;
                        else
                            txtRPValue2.Text = string.Empty;
                    }
                    else
                        txtRPValue2.Text = string.Empty;
                }

                cmbMCDocObjType.SelectedIndex = m_oCurrentInstruction.MatchCriteriaType;
                txtMCName.Text = m_oCurrentInstruction.MatchCriteriaName;
                txtMCValue.Text = m_oCurrentInstruction.MatchCriteriaValue;
                cmbRDDocObjType.SelectedIndex = (int)m_oCurrentInstruction.ReturnDataType;
                cmbRDUnit.SelectedIndex = (int)m_oCurrentInstruction.ReturnDataUnit;
                txtRDName.Text = m_oCurrentInstruction.ReturnDataName;
                m_bCurInstructionDirty = false;
                m_bCurParsingInstructionDirty = false;
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }

        }

        //Method to Import the Elements and Save
        private void SavingImportElements(int iRowIndex, bool bNewElement)
        {
            List<Instruction> oInstructions;
            try
            {
                string xElementName = string.Empty;
                // bool oParsing = false;
                int iNMatch = 0;
                int iElementId = 0;
                bool bNewInstruction = false;
                int iNewExecutionIndex = 0;
                int iElementSource = -1;
                string xElementTemplate = string.Empty;
                string xDisplayName = string.Empty;
                int iSortOrder = 0;
                bool bVisible = false;

                if (rdbtnElementText.Checked)
                {
                    iElementSource = 1;
                }
                else if (rdbtnElementMatchRange.Checked)
                {
                    iElementSource = 2;
                }
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["ElementName"].FormattedValue.ToString()))
                {
                    xElementName = grdElements.Rows[iRowIndex].Cells["ElementName"].FormattedValue.ToString();
                }
                else
                {
                    xElementName = string.Empty;
                }

                //GLOG : 8144 : jsw
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["Template"].FormattedValue.ToString()))
                {
                    xElementTemplate = grdElements.Rows[iRowIndex].Cells["Template"].FormattedValue.ToString();
                }

                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["NoMatch"].FormattedValue.ToString()))
                {
                    iNMatch = (int)grdElements.Rows[iRowIndex].Cells["NoMatch"].Value;
                }
                else
                {
                    iNMatch = 1;
                }
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["ID"].FormattedValue.ToString()))
                {
                    iElementId = Convert.ToInt32(grdElements.Rows[iRowIndex].Cells["ID"].FormattedValue.ToString());
                    bNewElement = true;
                    bNewInstruction = true;
                    iNewExecutionIndex = 1;

                }
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["DisplayName"].FormattedValue.ToString()))
                {
                    xDisplayName = grdElements.Rows[iRowIndex].Cells["DisplayName"].FormattedValue.ToString();
                }
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["SortOrder"].FormattedValue.ToString()))
                {
                    iSortOrder = (int)grdElements.Rows[iRowIndex].Cells["SortOrder"].Value;
                }
                if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["Visible"].FormattedValue.ToString()))
                {
                    if (grdElements.Rows[iRowIndex].Cells["Visible"].FormattedValue.ToString().ToLower() == "false")
                        bVisible = false;
                    else
                        bVisible = true;
                }

                //m_oDocType = new DocType((int)cmbDocType.SelectedValue);
                DocType oDocType = (DocType)cmbDocType.SelectedItem;
                m_oDocType = new DocType(oDocType.ID);
                //m_oDocType = new DocType(cmbDocType.Text);
                //m_oDocType = (DocType)cmbDocType.SelectedItem;

                int iNewElementId = m_oDocType.UpdateElement(xElementName, iNMatch, iElementId, bNewElement, iElementSource, xElementTemplate, xDisplayName, iSortOrder, bVisible);

                if (bNewInstruction)
                {
                    Element oElement = new Element(iNewElementId);
                    bool bInsertInstructions = oElement.SaveInstruction(1, iNewExecutionIndex, "0", "0", "0", "0", "0�8", true, 0);
                    tbtnAddInstruction.Enabled = true;
                    tbtnDeleteInstruction.Enabled = true;
                }

                Element oInstructionsElement = new Element(iElementId);
                oInstructions = oInstructionsElement.Instructions;
                string xWholeRange = string.Empty;
                string xStartRange = string.Empty;
                string xEndRange = string.Empty;
                string xMatchCriteria = string.Empty;
                string xReturnData = string.Empty;
                int iMatchRangeType = 0;
                for (int i = 0; i < oInstructions.Count; i++)
                {

                    xWholeRange = GetRangeValues(oInstructions[i].WholeRangeType, oInstructions[i].WholeRangeName, oInstructions[i].WholeRangeOperator, oInstructions[i].WholeRangeValue, oInstructions[i].WholeRangeSection, oInstructions[i].WholeRangePosition);
                    xStartRange = GetRangeValues(oInstructions[i].StartRangeType, oInstructions[i].StartRangeName, oInstructions[i].StartRangeOperator, oInstructions[i].StartRangeValue, oInstructions[i].StartRangeSection, oInstructions[i].StartRangePosition);
                    xEndRange = GetRangeValues(oInstructions[i].EndRangeType, oInstructions[i].EndRangeName, oInstructions[i].EndRangeOperator, oInstructions[i].EndRangeValue, oInstructions[i].EndRangeSection, oInstructions[i].EndRangePosition);
                    xMatchCriteria = GetMatchCriteriaInstructions(oInstructions[i].MatchCriteriaType, oInstructions[i].MatchCriteriaName, oInstructions[i].MatchCriteriaValue);
                    xReturnData = GetReturnDataInstructions(oInstructions[i]);
                    oInstructionsElement = new Element(iNewElementId);
                    iMatchRangeType = oInstructions[i].MatchRangeType;
                    bool bUpdateInstructions = oInstructionsElement.SaveInstruction(oInstructions[i].Story, oInstructions[i].ExecutionIndex, xWholeRange, xStartRange, xEndRange, xMatchCriteria, xReturnData, false, iMatchRangeType);
                }

            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }

        }

        private string GetReturnDataInstructions(Instruction oInstruction)
        {
            string xResultReturnData = "0";
            int iRType = 0;
            int iRUnit = 0;
            iRType = oInstruction.ReturnDataType;
            iRUnit = oInstruction.ReturnDataUnit;
            xResultReturnData = iRType + "�" + iRUnit;
            return xResultReturnData;
        }

        private string GetMatchCriteriaInstructions(int iMatchInstructionType, string xMatchInstructionName, string xMatchInstructionValue)
        {
            string xResultMatchCriteria = "0";

            xResultMatchCriteria = Convert.ToString(iMatchInstructionType);
            if (!string.IsNullOrEmpty(xMatchInstructionName) || !string.IsNullOrEmpty(xMatchInstructionValue))
            {
                xResultMatchCriteria = iMatchInstructionType + "�" + xMatchInstructionName;
                if (!string.IsNullOrEmpty(xMatchInstructionValue))
                {

                    xResultMatchCriteria = iMatchInstructionType + "�" + xMatchInstructionName + "�" + xMatchInstructionValue;
                }
            }

            return xResultMatchCriteria;
        }

        //For the Whole Range CheckBox
        private void CheckState()
        {
            
            if (chkWholeRange.Checked)
            {
                m_bInstructionWholeRange = true;
                cmbRPSection.SelectedIndex = 0;
                cmbRPSection2.SelectedIndex = 0;
                cmbRPDocObjType.SelectedIndex = 0;
                cmbRPDocObjType2.SelectedIndex = 0;
                cmbRPOperator.SelectedIndex = 0;
                cmbRPOperator2.SelectedIndex = 0;
                cmbRPPosition.SelectedIndex = 0;
                cmbRPPosition2.SelectedIndex = 1;
                //Start range controls
                cmbRPSection.Enabled = false;
                cmbRPDocObjType.Enabled = false;
                cmbRPOperator.Enabled = false;
                cmbRPPosition.Enabled = false;
                txtRPName.Enabled = false;
                txtRPValue.Enabled = false;
                //End Range controls
                cmbRPSection2.Enabled = false;
                cmbRPDocObjType2.Enabled = false;
                cmbRPOperator2.Enabled = false;
                cmbRPPosition2.Enabled = false;
                txtRPName2.Enabled = false;
                txtRPValue2.Enabled = false;
                cmbRPPosition.SelectedIndex = 2;
                cmbRPSection.SelectedIndex = 0;
               
                //Return data controls
                txtRPName.Text = string.Empty;
                txtRPName2.Text = string.Empty;
                txtRPValue.Text = string.Empty;
                txtRPValue2.Text = string.Empty;
              
                this.cmbRDUnit.SelectedIndex = 8;
                this.cmbRDDocObjType.SelectedIndex = 0;

                m_oCurrentInstruction.MatchRangeType = 1;
            }
            else
            {
                txtRPName.Text = string.Empty;
                txtRPName2.Text = string.Empty;
                txtRPValue.Text = string.Empty;
                txtRPValue2.Text = string.Empty;
                cmbRPSection.SelectedIndex = 0;
                cmbRPSection2.SelectedIndex = 0;
                cmbRPDocObjType.SelectedIndex = 0;
                cmbRPDocObjType2.SelectedIndex = 0;
                cmbRPOperator.SelectedIndex = 0;
                cmbRPOperator2.SelectedIndex = 0;
                cmbRPPosition.SelectedIndex = 0;
                cmbRPPosition2.SelectedIndex = 1;
                m_bInstructionWholeRange = false;
                //Start range controls
                cmbRPSection.Enabled = true;
                cmbRPDocObjType.Enabled = true;
                cmbRPOperator.Enabled = true;
                cmbRPPosition.Enabled = true;
                //End Range controls
                cmbRPSection2.Enabled = true;
                cmbRPDocObjType2.Enabled = true;
                cmbRPOperator2.Enabled = true;
                cmbRPPosition2.Enabled = true;
                string xSelectedItem = cmbMCDocObjType.SelectedItem.ToString();

                if (xSelectedItem == "Document Variable" || xSelectedItem == "Document Property")
                    //no range type
                    m_oCurrentInstruction.MatchRangeType = 0;
                else
                    //StartToEnd range type
                    m_oCurrentInstruction.MatchRangeType = 2;
 
                PopulateStartRangeDocType();
                PopulateEndRangeDocType();
                PopulateStartRangeNameValues();
                PopulateEndRangeNameValues();
              
            }
           
        }

        //Actions to perform on Click of Elements Grid
        private void PopulateInstructionsListBox()
        {
            try
            {
                if (m_bCurInstructionDirty)
                {
                    if (m_oCurrentInstruction == null)
                        AddNewInstruction();
                    
                    SaveCurrentInstruction();
                }

                if (grdElements.Rows.Count > 0)
                {
                    int iCurrentRowIndex = -1;
                    if (grdElements.CurrentRow != null)
                    {
                        if (grdElements.CurrentRow.Index > -1)
                        {
                            if (!m_bDeleteElement)
                            {
                                iCurrentRowIndex = grdElements.CurrentRow.Index;
                            }
                            else
                            {
                                iCurrentRowIndex = grdElements.CurrentRow.Index - 1;
                            }
                        }
                        else
                        {
                            iCurrentRowIndex = grdElements.CurrentRow.Index;
                        }
                        
                        m_iRowEnter = iCurrentRowIndex;
                        int iElementID = 0;
                        string xElementName = string.Empty;
                        
                        int iNMatch = 0;
                        m_oInstructions = new List<Instruction>();
                        if (!string.IsNullOrEmpty(grdElements.Rows[iCurrentRowIndex].Cells["ElementName"].FormattedValue.ToString()))
                        {
                            xElementName = grdElements.Rows[iCurrentRowIndex].Cells["ElementName"].FormattedValue.ToString();
                        }

                        if (!string.IsNullOrEmpty(grdElements.Rows[iCurrentRowIndex].Cells["NoMatch"].FormattedValue.ToString()))
                        {
                            iNMatch = (int)grdElements.Rows[iCurrentRowIndex].Cells["NoMatch"].Value;
                        }
                        else
                        {
                            iNMatch = 1;
                        }
                        if (!string.IsNullOrEmpty(grdElements.Rows[iCurrentRowIndex].Cells["ID"].FormattedValue.ToString()))
                        {
                            iElementID = (int)grdElements.Rows[iCurrentRowIndex].Cells["ID"].Value;
                            if (iElementID != 0)
                            {
                                //Element xelement = new Element(iElementID, xelementname, (int)cmbDocType.SelectedValue, xparsing, iNMatch);
                                Element oElement = new Element(iElementID, xElementName, (int)cmbDocType.SelectedValue, iNMatch);
                                m_oInstructions = oElement.Instructions;
                                lstInstructionIndexes.Items.Clear();
                                if (m_oInstructions != null)
                                {
                                    if (m_oInstructions.Count > 0)
                                    {
                                        lstInstructionIndexes.Enabled = true;

                                        for (int k = 0; k < m_oInstructions.Count; k++)
                                        {
                                            lstInstructionIndexes.Items.Add(m_oInstructions[k].ExecutionIndex);

                                        }
                                         lstInstructionIndexes.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        LoadInstructionsPanelComboBoxes();
                                        lstInstructionIndexes.Enabled = false;

                                    }
                                }
                                if (m_bNewElement)
                                {
                                    chkWholeRange.Checked = true;
                                }
                            }
                           
                        }
                        else
                        {
                            lstInstructionIndexes.Items.Clear();
                        }
                       
                    }
                }
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }

        private void PopulateInstructionsListBox(int iIndex)
        {
            int iNMatch = 0;
            string xElementName = "";
            int iElementID = 0;
            m_oInstructions = new List<Instruction>();

            if (!string.IsNullOrEmpty(grdElements.Rows[iIndex].Cells["ElementName"].FormattedValue.ToString()))
            {
                xElementName = grdElements.Rows[iIndex].Cells["ElementName"].FormattedValue.ToString();
            }

            if (!string.IsNullOrEmpty(grdElements.Rows[iIndex].Cells["NoMatch"].FormattedValue.ToString()))
            {
                iNMatch = (int)grdElements.Rows[iIndex].Cells["NoMatch"].Value;
            }
            else
            {
                iNMatch = 1;
            }
            if (!string.IsNullOrEmpty(grdElements.Rows[iIndex].Cells["ID"].FormattedValue.ToString()))
            {
                iElementID = (int)grdElements.Rows[iIndex].Cells["ID"].Value;
                if (iElementID != 0)
                {
                    //Element xelement = new Element(iElementID, xelementname, (int)cmbDocType.SelectedValue, xparsing, iNMatch);
                    Element oElement = new Element(iElementID, xElementName, (int)cmbDocType.SelectedValue, iNMatch);
                    m_oInstructions = oElement.Instructions;
                    lstInstructionIndexes.Items.Clear();
                    if (m_oInstructions != null)
                    {
                        if (m_oInstructions.Count > 0)
                        {
                            lstInstructionIndexes.Enabled = true;

                            for (int k = 0; k < m_oInstructions.Count; k++)
                            {
                                lstInstructionIndexes.Items.Add(m_oInstructions[k].ExecutionIndex);

                            }
                            lstInstructionIndexes.SelectedIndex = 0;
                        }
                        else
                        {
                            LoadInstructionsPanelComboBoxes();
                            lstInstructionIndexes.Enabled = false;

                        }
                        if (iIndex == 0)
                            btnMoveUp.Enabled = false;
                        else
                            btnMoveUp.Enabled = true;
                        if (iIndex == (grdElements.Rows.Count - 1))
                            btnMoveDown.Enabled = false;
                        else
                            btnMoveDown.Enabled = true;
                        
                    }
                    if (m_bNewElement)
                    {
                        chkWholeRange.Checked = true;
                    }
                }

            }
        }
        /// <summary>
        /// creates an new instruction instance, populated with required default values
        /// </summary>
        private void AddNewInstruction()
        {
            m_oCurrentInstruction = new Instruction();
            m_oCurrentInstruction.MatchCriteriaType = (short)daMatchCriteria.None;
            m_oCurrentInstruction.StartRangeType = 0;
            m_oCurrentInstruction.EndRangeType = 0;

            m_oCurrentInstruction.StartRangeName = "0";
            m_oCurrentInstruction.StartRangeOperator = 0;
            m_oCurrentInstruction.StartRangeValue = "0";

            m_oCurrentInstruction.EndRangeName = "0";
            m_oCurrentInstruction.EndRangeOperator = 0;
            m_oCurrentInstruction.EndRangeValue = "0";

        }
              

        //Method to Delete Instructions
        private void DeleteInstruction(int iElementID, int iExecutionIndex)
        {
            try
            {
                Element oElement = new Element(iElementID);
                bool bDeleteInstructions = oElement.DeleteInstruction(iExecutionIndex);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }

        }

        private void SaveCurrentInstruction()
        {
            try
            {
                //exit if no current instruction exists
                if (m_oCurrentInstruction == null)
                    return;
                
                int iElementID = 0;
                Element oElement = new Element(iElementID);

                if (m_bCurInstructionDirty)
                {
                    iElementID = m_oCurrentInstruction.ElementID;// m_iElementID;
                    oElement = new Element(iElementID);
                    string xStartRange = "0";
                    string xWholeRange = "0";
                    string xEndRange = "0";
                    string xMatchCriteria = string.Empty;
                    xMatchCriteria = GetMatchCriteriaInstructions(
                        m_oCurrentInstruction.MatchCriteriaType, 
                        m_oCurrentInstruction.MatchCriteriaName, 
                        m_oCurrentInstruction.MatchCriteriaValue);
                    
                    string xReturnData = m_oCurrentInstruction.ReturnDataType + "�" + m_oCurrentInstruction.ReturnDataUnit;
                    
                    if (!m_bInstructionWholeRange)
                    {

                        xStartRange = GetRangeValues(m_oCurrentInstruction.StartRangeType,
                            m_oCurrentInstruction.StartRangeName, m_oCurrentInstruction.StartRangeOperator,
                            m_oCurrentInstruction.StartRangeValue, m_oCurrentInstruction.StartRangeSection,
                            m_oCurrentInstruction.StartRangePosition);
                        
                        
                        xEndRange = GetRangeValues(m_oCurrentInstruction.EndRangeType,
                           m_oCurrentInstruction.EndRangeName, m_oCurrentInstruction.EndRangeOperator,
                           m_oCurrentInstruction.EndRangeValue, m_oCurrentInstruction.EndRangeSection,
                           m_oCurrentInstruction.EndRangePosition);
                       
                        
                        xWholeRange = "0";
                    }
                    else
                    {
                        xStartRange = "0";
                        xEndRange = "0";

                        xWholeRange = GetRangeValues(m_oCurrentInstruction.StartRangeType,
                            m_oCurrentInstruction.StartRangeName, m_oCurrentInstruction.StartRangeOperator,
                            m_oCurrentInstruction.StartRangeValue, m_oCurrentInstruction.StartRangeSection,
                            m_oCurrentInstruction.StartRangePosition);
                    }

                    if (xWholeRange == "-1")
                    {
                        xWholeRange = "0";
                    }

                    if (m_oCurrentInstruction.ExecutionIndex  != 0)
                    {
                        bool bUpdateInstructions = oElement.SaveInstruction(m_oCurrentInstruction.Story, 
                            m_oCurrentInstruction.ExecutionIndex, xWholeRange, xStartRange,
                            xEndRange, xMatchCriteria, xReturnData, false, m_oCurrentInstruction.MatchRangeType);
                    }

                    m_bCurInstructionDirty = false;
                    ClearInstructions();
                    if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString()))
                    {
                        iElementID = Convert.ToInt32(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString());
                        oElement = new Element(iElementID);
                        m_oInstructions = oElement.Instructions;
                    }

                    //update interface
                    //this.LoadInstruction(m_oCurrentInstruction.ExecutionIndex);
                }
                else
                {
                    ClearInstructions();
                }

            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }

        //Method to clear the Cached Instructions
        private void ClearInstructions()
        {
            m_oCurrentInstruction = null;
            
        }

        //Method to clear the Cached Parse Instructions
        private void ClearCurrentParseInstructions()
        {
            m_oCurrentParsingInstruction = null;
            m_bCurParsingInstructionDirty = false;
            m_iParseElementRowIndex = -1;
            //rdbtnElementMatchRange.Checked = false;
            //rdbtnElementText.Checked = false;
        }

        //Method to save the Cached Parse Instructions
        private void SaveCurrentParseInstruction()
        {
            if (m_bCurParsingInstructionDirty)
            {
                m_bCurParsingInstructionDirty = false;
                if (grdElements.CurrentRow != null &&
                    m_iParseElementRowIndex != -1 &&
                    m_iParseElementRowIndex <= grdElements.Rows.Count)
                {
                    SaveElements(m_iParseElementRowIndex);
                    grdElements.Rows[m_iParseElementRowIndex].Cells["Template"].Value = txtTemplate.Text;

                }
            }
            //clear and refresh parse instructions
            ClearCurrentParseInstructions();
            LoadParseInstruction(grdElements.CurrentRow.Index);
        }

        //Method to populate  Parse Instruction on Click of Elements Grid 
        private void LoadParseInstruction(int iRowIndex)
        {
           
            if (!string.IsNullOrEmpty(grdElements.Rows[iRowIndex].Cells["Template"].FormattedValue.ToString()))
            {
                txtTemplate.Text = grdElements.Rows[iRowIndex].Cells["Template"].FormattedValue.ToString();
            }
            else
            {
                m_bLoadingInstructions = true;
                txtTemplate.Text = string.Empty;
                m_bLoadingInstructions = false;
            }
            if (!string.IsNullOrEmpty(lstInstructionIndexes.Text.ToString()))
            {
                LoadInstruction(Convert.ToInt32(lstInstructionIndexes.Text.ToString()));
            }
            else
            {
                LoadInstructionsPanelComboBoxes();
                //CheckState();
                if (m_oCurrentInstruction.MatchCriteriaType == 1)
                    this.chkWholeRange.Checked = true;
                else
                    this.chkWholeRange.Checked = false;
            }
        }

        //Method to delete the Elements on the Click of the Tool strip Button Delete Elements
        private void DeleteElements()
        {
            Boolean bSuccessDelete = false;
            int iDeleteElementId = 0;
            int iSortOrder = 0;
            int iCurrentIndex = grdElements.CurrentRow.Index;

            DialogResult dr = MessageBox.Show("Delete this element? ", "Confirm deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                m_bAllowDelete = true;
                if (!string.IsNullOrEmpty(grdElements.Rows[iCurrentIndex].Cells["ID"].FormattedValue.ToString()))
                {

                    iDeleteElementId = Convert.ToInt32(grdElements.Rows[iCurrentIndex].Cells["ID"].FormattedValue.ToString());

                    iSortOrder = Convert.ToInt32(grdElements.Rows[iCurrentIndex].Cells["SortOrder"].FormattedValue.ToString());
                    if (iCurrentIndex < (grdElements.Rows.Count - 1))
                    {
                        //have to renumber sort order value for all following elements.
                        for (int i = iCurrentIndex; i < (grdElements.Rows.Count - 1); i++)
                        {
                            iCurrentIndex++;

                            grdElements.Rows[iCurrentIndex].Cells["SortOrder"].Value = iSortOrder;
                            SaveElements(iCurrentIndex);

                            iSortOrder++;
                        }
                    }

                    bSuccessDelete = m_oDocType.DeleteElement(iDeleteElementId);
                    DocType oDocType = (DocType)cmbDocType.SelectedItem;
                    m_oDocType = new DocType(oDocType.ID);

                    m_iSelectedElementRow = grdElements.CurrentRow.Index-1;
                    //Populate Elements Grid
                    if (m_oDocType.Elements.Count > 0)
                    {
                        PopulateElements(m_oDocType.Elements);
                    }


                    lstInstructionIndexes.Items.Clear();
                    if (m_oInstructions != null)
                    {
                        foreach (Instruction oInstruction in m_oInstructions)
                        {
                            if (oInstruction.ElementID != iDeleteElementId)
                            {

                                lstInstructionIndexes.Items.Add(oInstruction.ExecutionIndex);
                            }
                        }
                        if (lstInstructionIndexes != null)
                        {
                            PopulateInstructionsListBox();
                        }
                    }
                    if (grdElements.SelectedRows[0].Index == 0)
                        btnMoveUp.Enabled = false;
                    else
                        btnMoveUp.Enabled = true;
                    if (grdElements.SelectedRows[0].Index == (grdElements.Rows.Count - 1))
                        btnMoveDown.Enabled = false;
                    else
                        btnMoveDown.Enabled = true;
                }
            }
            else
            {
                m_bAllowDelete = false;
            }
        }

        //set default operator for chosen Range parameter object type
        private void PopulateStartRangeDocType()
        {
             switch (this.cmbRPDocObjType.SelectedIndex)
            {
                case (int)daRDDocObjType.Bookmark:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.FirstInstanceOfText:
                case (int)daRDDocObjType.LastInstanceOfText:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.ThatIs;
                    break;
                case (int)daRDDocObjType.Style:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.Paragraph:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Table:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Cell:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Row:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Keyword:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.Element:
                    this.cmbRPOperator.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
            }
        }

        //set default operator for chosen Range parameter object type
        private void PopulateEndRangeDocType()
        {
            //set default operator for chosen Range parameter object type
            switch (this.cmbRPDocObjType2.SelectedIndex)
            {
                case (int)daRDDocObjType.Bookmark:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.FirstInstanceOfText:
                case (int)daRDDocObjType.LastInstanceOfText:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.ThatIs;
                    break;
                case (int)daRDDocObjType.Style:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.Paragraph:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Table:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Cell:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Row:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.ThatContainsTheText;
                    break;
                case (int)daRDDocObjType.Keyword:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
                case (int)daRDDocObjType.Element:
                    this.cmbRPOperator2.SelectedIndex = (int)daRDOperator.WhoseNameIs;
                    break;
            }
        }

        // enable and disable name and value txt boxes according to which range parameter operator is selected
        private void PopulateStartRangeNameValues()
        {
            switch (this.cmbRPOperator.SelectedIndex)
            {
                case (int)daRDOperator.WhoseNameIs:
                    this.txtRPName.Enabled = true;
                    this.txtRPValue.Enabled = false;
                    this.txtRPValue.Text = "";
                    break;
                case (int)daRDOperator.ThatIs:
                    this.txtRPName.Enabled = false; ;
                    this.txtRPName.Text = "";
                    txtRPValue.Text = "";
                    this.txtRPValue.Enabled = true;
                    break;
                case (int)daRDOperator.ThatContainsTheText:
                    this.txtRPName.Enabled = false;
                    this.txtRPName.Text = "";
                    this.txtRPValue.Enabled = true;
                    txtRPValue.Text = "";
                    break;
                case (int)daRDOperator.WhoseIndexIs:
                    this.txtRPName.Enabled = false;
                    this.txtRPName.Text = "";
                    this.txtRPValue.Enabled = true;
                    txtRPValue.Text = "1";
                    break;
                case (int)daRDOperator.ThatContainsTheStyle:
                    this.txtRPName.Enabled = true;
                    this.txtRPValue.Enabled = true;
                    txtRPValue.Text = "";
                    break;
            }
        }

        // enable and disable name and value txt boxes according to which range parameter operator is selected
        private void PopulateEndRangeNameValues()
        {
            switch (this.cmbRPOperator2.SelectedIndex)
            {
                case (int)daRDOperator.WhoseNameIs:
                    this.txtRPName2.Enabled = true;
                    this.txtRPValue2.Enabled = false;
                    this.txtRPValue2.Text = "";
                    break;
                case (int)daRDOperator.ThatIs:
                    this.txtRPName2.Enabled = false;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true;
                    txtRPValue2.Text = "";
                    break;
                case (int)daRDOperator.ThatContainsTheText:
                    this.txtRPName2.Enabled = false;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true;
                    txtRPValue2.Text = "";
                    break;
                case (int)daRDOperator.WhoseIndexIs:
                    this.txtRPName2.Enabled = false;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true;
                    txtRPValue2.Text = "1";
                    break;
                case (int)daRDOperator.ThatContainsTheStyle:
                    this.txtRPName2.Enabled = true;
                    this.txtRPValue2.Enabled = true;
                    txtRPValue2.Text = "";
                    break;
            }
        }

        //Event fired when the Index is changes on the DocType combo
        private void cmbDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDocType();
        }

        private void cmbDocType_Leave(object sender, EventArgs e)
        {
            try
            {
                if (cmbDocType.SelectedIndex == 0)
                {
                    cmbDocType.Focus();

                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdElements.IsCurrentRowDirty)
                {
                    tbtnAddInstruction.Enabled = true;
                    tbtnDeleteInstruction.Enabled = true;
                    if (e.ColumnIndex == 0 && e.RowIndex != -1)
                    {
                        if (grdElements.IsCurrentCellInEditMode)
                        {
                            if (grdElements.IsCurrentCellDirty)
                            {
                                SaveElements(e.RowIndex);
                            }
                        }
                    }
                    else if (e.ColumnIndex == 1 && e.RowIndex != -1)
                    {
                        if (grdElements.IsCurrentCellInEditMode)
                        {
                            if (grdElements.IsCurrentCellDirty)
                            {
                                SaveElements(e.RowIndex);
                            }
                        }
                    }
                    else if (e.ColumnIndex == 2 && e.RowIndex != -1)
                    {
                        if (grdElements.IsCurrentCellInEditMode)
                        {
                            if (grdElements.IsCurrentCellDirty)
                            {
                                SaveElements(e.RowIndex);
                            }
                        }
                    }
                    else if (e.ColumnIndex == 3 && e.RowIndex != -1)
                    {
                        if (grdElements.IsCurrentCellInEditMode)
                        {
                            if (grdElements.IsCurrentCellDirty)
                            {
                                SaveElements(e.RowIndex);
                            }
                        }
                    }
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_Click(object sender, EventArgs e)
        {

            try
            {
                if (cmbDocType.SelectedIndex > 0)
                {
                    grdElements.Enabled = true;
                    tbtnEditDeterminants.Enabled = true;
                    m_iSelectedElementRow = grdElements.CurrentRow.Index;
                    if (grdElements.CurrentRow.IsNewRow)
                    {
                        ClearCurrentParseInstructions();
                        m_bNewElement = true;

                    }
                    else
                    {
                        m_bNewElement = false;
                    }
                    m_bDeleteElement = false;
                    PopulateInstructionsListBox();
                    LoadParseInstruction(grdElements.CurrentRow.Index);
                    if (m_iSelectedElementRow == 0)
                        this.btnMoveUp.Enabled = false;
                    else
                        this.btnMoveUp.Enabled = true;

                    if (m_iSelectedElementRow == (grdElements.Rows.Count - 1))
                        this.btnMoveDown.Enabled = false;
                    else
                        this.btnMoveDown.Enabled = true;
                }
                else
                {
                    grdElements.Enabled = false;
                    tbtnEditDeterminants.Enabled = false;
                    this.tbtnAddElement.Enabled = false;
                    this.tbtnDeleteElement.Enabled = false;
                    MessageBox.Show("Please select a document type", "Select Document Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbDocType.Focus();
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    Boolean bSuccessDelete = false;
                    int oDeleteElementId = 0;

                    DialogResult dr = MessageBox.Show("Delete this element? ", "Confirm deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        m_bAllowDelete = true;
                        if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString()))
                        {

                            oDeleteElementId = Convert.ToInt32(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString());
                            bSuccessDelete = m_oDocType.DeleteElement(oDeleteElementId);
                            m_bDeleteElement = true;
                            lstInstructionIndexes.Items.Clear();
                            if (m_oInstructions != null)
                            {
                                foreach (Instruction oInstruction in m_oInstructions)
                                {
                                    if (oInstruction.ElementID != oDeleteElementId)
                                    {
                                        lstInstructionIndexes.Items.Add(oInstruction.ExecutionIndex);
                                    }
                                }
                                if (lstInstructionIndexes != null)
                                {
                                    PopulateInstructionsListBox();
                                    //if (lstInstructionIndexes.Items.Count == 0)
                                    //{
                                    //    LoadCmbDocType();
                                    //    CheckState();
                                    //}
                                }
                            }
                        }
                    }
                    else
                    {
                        m_bAllowDelete = false;
                    }
                }
                else if (e.KeyCode == Keys.Tab)
                {
                    string xElementName = string.Empty;

                    if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ElementName"].FormattedValue.ToString()))
                    {
                        xElementName = grdElements.Rows[grdElements.CurrentRow.Index].Cells["ElementName"].FormattedValue.ToString();
                    }
                    if (string.IsNullOrEmpty(xElementName))
                    {
                        // MessageBox.Show("Please enter the name for Element", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        grdElements.CurrentCell = grdElements[0, grdElements.CurrentRow.Index];
                        grdElements.Rows[grdElements.CurrentRow.Index].Selected = true;
                    }
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (m_bAllowDelete)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    if (e.RowIndex != grdElements.Rows.Count - 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
                            {
                                MessageBox.Show("Please enter the name for Element", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                            }
                            else
                            {
                                e.Cancel = false;
                            }
                        }
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        private void grdElements_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // Clear the row error in case the user presses ESC. 
                grdElements.Rows[e.RowIndex].ErrorText = string.Empty;
                ////m_bDeleteElement = true;
                PopulateInstructionsListBox();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_iRowEnter > -1)
                {
                    if (e.RowIndex != m_iRowEnter)
                    {

                        m_bDeleteElement = false;
                        PopulateInstructionsListBox();
                    }
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdElements_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //m_iRowEnter = e.RowIndex;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        # endregion

        private void toolStripbtnAddDocType_Click(object sender, EventArgs e)
        {

            try
            {
                CreateDocTypeForm oForm = new CreateDocTypeForm();
                oForm.ShowDialog();

                if (!oForm.Cancelled)
                {
                    cmbDocType.DataSource = null;
                    AddItem(oForm.NewDocTypeList, typeof(DocType), "ID", "Name", "< Select Document Type >");
                    cmbDocType.DataSource = oForm.NewDocTypeList;
                    cmbDocType.DisplayMember = "Name";
                    cmbDocType.ValueMember = "ID";
                    if (oForm.bAddDocType)
                    {
                        int iIndex = -1;
                        for (int i = 0; i < oForm.NewDocTypeList.Count; i++)
                        {
                            if (oForm.NewDocTypeList[i].Name == oForm.DocTypeName)
                            {
                                iIndex = i;
                                break;
                            }
                        }
                        cmbDocType.SelectedIndex = iIndex;
                        grdElements.Focus();

                        if (cmbDocType.SelectedIndex > 0 && grdElements.Rows.Count > 0)
                        {
                            grdElements.Rows[0].Selected = true;
                            grdElements.CurrentCell = grdElements[0, 0];
                            grdElements.Enabled = true;
                            tbtnEditDeterminants.Enabled = true;
                            tbtnImportElements.Enabled = true;
                            chkWholeRange.Checked = false;
                        }
                        else
                        {
                            grdElements.Enabled = false;
                            tbtnEditDeterminants.Enabled = false;
                            tbtnImportElements.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void toolStripBtnDeleteDocType_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult oDR = MessageBox.Show("Delete the selected document type?", "Confirm deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (oDR == DialogResult.Yes)
                {
                    int iDeleteDoctype;
                    if (cmbDocType.SelectedIndex > 0)
                    {

                        DocType oDocType = (DocType)cmbDocType.SelectedItem;
                        iDeleteDoctype = oDocType.ID;
                        if (iDeleteDoctype != 0)
                        {
                            oDocType = new DocType();

                            oDocType.Delete(iDeleteDoctype);
                            List<DocType> oDocTypesList = new List<DocType>();
                            oDocTypesList = Definitions.GetDocTypesList();
                            AddItem(oDocTypesList, typeof(DocType), "ID", "Name", "< Select Document Type >");
                            cmbDocType.DataSource = null;
                            cmbDocType.DataSource = oDocTypesList;

                            cmbDocType.DisplayMember = "Name";
                            cmbDocType.ValueMember = "ID";

                            grdElements.DataSource = null;
                            if (cmbDocType.SelectedIndex > 0)
                            {
                                tbtnImportElements.Enabled = true;
                            }
                        }

                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void toolStripBtnAddInstruction_Click(object sender, EventArgs e)
        {
            try
            {
                if (!lstInstructionIndexes.Enabled)
                {
                    lstInstructionIndexes.Enabled = true;
                    lstInstructionIndexes.Items.Clear();

                }

                int iExecutionIndex = 0;
                if (lstInstructionIndexes.Items.Count > 0)
                {
                    iExecutionIndex = (Convert.ToInt32(lstInstructionIndexes.Items[lstInstructionIndexes.Items.Count - 1].ToString())) + 1;
                    lstInstructionIndexes.Items.Add(iExecutionIndex);
                    lstInstructionIndexes.Refresh();
                }
                else
                {
                    iExecutionIndex = 1;
                    lstInstructionIndexes.Items.Clear();
                    lstInstructionIndexes.Items.Add(iExecutionIndex);
                    lstInstructionIndexes.Refresh();
                }

                //SaveInstruction(iExecutionIndex, true);
                int iElementID = 0;

                if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString()))
                {
                    iElementID = Convert.ToInt32(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString());
                }
                Element oElement = new Element(iElementID);
                bool bInsertInstructions = oElement.SaveInstruction(1, iExecutionIndex, "0", "0", "0", "0", "0�8", true, 0);


                lstInstructionIndexes.SelectedIndex = lstInstructionIndexes.Items.Count - 1;

                m_oCurrentInstruction = new Instruction();
                LoadInstructionsPanelComboBoxes();
                cmbStory.Focus();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void toolStripBtnDeleteInstruction_Click(object sender, EventArgs e)
        {
            try
            {
                int iExecutionIndex = 0;
                int iElementID = 0;
                int iListCount = 0;
                iListCount = lstInstructionIndexes.Items.Count;
                if (iListCount > 1)
                {
                    DialogResult oDR = MessageBox.Show("Delete this instruction? ", "Confirm deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.Yes)
                    {
                        if (!string.IsNullOrEmpty(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString()))
                        {
                            iElementID = Convert.ToInt32(grdElements.Rows[grdElements.CurrentRow.Index].Cells["ID"].FormattedValue.ToString());
                        }
                        if (lstInstructionIndexes.SelectedIndex > -1)
                        {
                            iExecutionIndex = Convert.ToInt32(lstInstructionIndexes.SelectedItem);
                        }
                        m_bDeleteElement = false;
                        DeleteInstruction(iElementID, iExecutionIndex);
                        PopulateInstructionsListBox();
                    }
                }
                else
                {
                    MessageBox.Show("Each element should have at least one instruction.You cannot delete this instruction", "Delete Instruction", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void toolStripBtnImportElements_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocTypeID = 0;

                DocTypesForm oForm = new DocTypesForm("Import Elements", "&Import Elements From:");

                oForm.ShowDialog();

                iDocTypeID = oForm.m_SelectDocType;
                if (iDocTypeID > -1)
                {
                    m_oDocType = new DocType(iDocTypeID);
                    PopulateElements(m_oDocType.Elements);

                    int iRowIndex;
                    int iElementsCount = m_oDocType.Elements.Count;
                    for (iRowIndex = 0; iRowIndex < iElementsCount; iRowIndex++)
                    {
                        SavingImportElements(iRowIndex, true);
                    }
                    LoadDocType();
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnEditDeterminants_Click(object sender, EventArgs e)
        {
            try
            {
                DeterminantsForm oForm = new DeterminantsForm("Edit Determinants");
                if (cmbDocType.SelectedIndex > 0)
                {
                    grdElements.Enabled = true;
                    tbtnEditDeterminants.Enabled = true;
                    //m_oDocType = new DocType((int)cmbDocType.SelectedValue);
                    //m_oDocType = new DocType(cmbDocType.Text);
                    //m_oDocType = (DocType)cmbDocType.SelectedItem;
                    // oForm.m_iDocTypeVal = (int)cmbDocType.SelectedValue;
                    DocType oDocType = (DocType)cmbDocType.SelectedItem;
                    m_oDocType = new DocType(oDocType.ID);

                    oForm.m_iDocTypeVal = m_oDocType.ID;
                    oForm.m_oDeterminantsList = m_oDocType.Determinants;
                    oForm.ShowDialog();
                }
                else
                {
                    grdElements.Enabled = false;
                    tbtnEditDeterminants.Enabled = false;
                    MessageBox.Show("Please select a document type", "Select Document Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbDocType.Focus();
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnDeleteElement_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteElements();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnAddElement_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddElements();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        private void lstInstructions_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (lstInstructionIndexes.SelectedIndex > -1)
                {

                    if (m_bCurInstructionDirty)
                    {
                        SaveCurrentInstruction();
                    }

                    if (m_bCurParsingInstructionDirty)
                        SaveCurrentParseInstruction();

                    if (!string.IsNullOrEmpty(Convert.ToString(lstInstructionIndexes.SelectedItem)))
                    {
                        LoadInstruction(Convert.ToInt32(lstInstructionIndexes.SelectedItem));

                    }
                }
                else
                {
                    m_oCurrentInstruction.ExecutionIndex = 0;
                    LoadInstructionsPanelComboBoxes();
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private string GetRangeValues(int iInstructionType, string xInstructionName, int iInstructionOperator, string xInstructionValue, int iInstructionSection, int iInstructionPosition)
        {
            string xResultRange = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(xInstructionName))
                {
                    xResultRange = Convert.ToString(iInstructionType);
                    xInstructionName = "0";
                }
                xResultRange = iInstructionType + "�" + xInstructionName;
                if (iInstructionOperator > -1)
                {
                    xResultRange = iInstructionType + "�" + xInstructionName + "�" + iInstructionOperator;
                    if (!string.IsNullOrEmpty(xInstructionValue))
                    {
                        xResultRange = xResultRange + "�" + xInstructionValue;

                    }
                    else
                    {
                        xResultRange = xResultRange + "�" + "0";
                    }
                    if (!(iInstructionSection < -1))
                    {
                        xResultRange = xResultRange + "�" + iInstructionSection;
                        if (iInstructionPosition > -1)
                        {
                            xResultRange = xResultRange + "�" + iInstructionPosition;
                        }
                    }
                }
                else
                {
                    xResultRange = iInstructionType + "�" + xInstructionName;
                }

                if (xResultRange == "-1")
                {
                    xResultRange = "0";
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            return xResultRange;
        }
        private void chkWholeRange_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckState();

                ////////if (!chkWholeRange.Checked)
                ////////{
                ////////    m_oCurrentInstruction.StartRangeType = (short)(Convert.ToInt32((int)cmbRPDocObjType.SelectedIndex));
                ////////    m_oCurrentInstruction.StartRangeName = txtRPName.Text;
                ////////    m_oCurrentInstruction.StartRangeOperator = (short)Convert.ToInt32((int)cmbRPOperator.SelectedIndex);
                ////////    m_oCurrentInstruction.StartRangeValue = txtRPValue.Text;
                ////////    m_oCurrentInstruction.StartRangeSection = (short)Convert.ToInt32((int)cmbRPSection.SelectedIndex);
                ////////    m_oCurrentInstruction.StartRangePosition = (short)Convert.ToInt32((int)cmbRPPosition.SelectedIndex);

                ////////    m_oCurrentInstruction.EndRangeType = (short)Convert.ToInt32((int)cmbRPDocObjType2.SelectedIndex);
                ////////    m_oCurrentInstruction.EndRangeName = txtRPName2.Text;
                ////////    m_oCurrentInstruction.EndRangeOperator = (short)Convert.ToInt32((int)cmbRPOperator2.SelectedIndex);
                ////////    m_oCurrentInstruction.EndRangeValue = txtRPValue2.Text;
                ////////    m_oCurrentInstruction.EndRangeSection = (short)Convert.ToInt32((int)cmbRPSection.SelectedIndex);
                ////////    m_oCurrentInstruction.EndRangePosition = (short)Convert.ToInt32((int)cmbRPPosition.SelectedIndex);

                ////////    m_oCurrentInstruction.WholeRangeValue = 0;
                ////////}
                ////////else
                ////////{
                ////////    m_oCurrentInstruction.WholeRangeType = (short)(Convert.ToInt32((int)cmbRPDocObjType.SelectedIndex));
                ////////    m_oCurrentInstruction.WholeRangeName = txtRPName.Text;
                ////////    m_oCurrentInstruction.WholeRangeOperator = (short)Convert.ToInt32((int)cmbRPOperator.SelectedIndex);
                ////////    m_oCurrentInstruction.WholeRangeValue = txtRPValue.Text;
                ////////    m_oCurrentInstruction.WholeRangeSection = (short)Convert.ToInt32((int)cmbRPSection.SelectedIndex);
                ////////    m_oCurrentInstruction.WholeRangePosition = (short)Convert.ToInt32((int)cmbRPPosition.SelectedIndex);

                ////////    m_oCurrentInstruction.StartRangeValue = "0";
                ////////    m_oCurrentInstruction.EndRangeValue = "0";
                ////////}

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbMCDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string xSelectedItem = cmbMCDocObjType.SelectedItem.ToString();
            try
            {
                short iMatchCriteriaDocType = 0;
                if (cmbMCDocObjType.SelectedIndex != -1)
                    iMatchCriteriaDocType = (short)Convert.ToInt32((int)cmbMCDocObjType.SelectedIndex);

                m_oCurrentInstruction.MatchCriteriaType = iMatchCriteriaDocType;
                m_bCurInstructionDirty = true;
                
                //disable search range group if object type is 
                //Document Property or Document Variable
                //disable Match Criteria Name or Value txt
                //boxes according to object type.
                switch(xSelectedItem)
                {
                    case "Document Variable":
                    case "Document Property":
                        //disable search criteria
                        this.grpMatchDomain.Enabled = false;
                        this.chkWholeRange.Checked = false;
                        this.txtMCValue.Enabled = false;
                        this.txtMCName.Enabled = true;
                        m_oCurrentInstruction.MatchRangeType = 0;
                        break;
                    case "Bookmark":
                    case "First Instance of Style":
                    case "Last Instance of Style":
                    case "All Instances of Style":
                    case "Keyword":
                    case "Table Containing Style":
                    case "Cell Containing Style":
                        this.grpMatchDomain.Enabled = true;
                        this.txtMCValue.Enabled = false;
                        this.txtMCName.Enabled = true;
                        if (this.chkWholeRange.Checked == true)
                            m_oCurrentInstruction.MatchRangeType = 1;
                        else
                            m_oCurrentInstruction.MatchRangeType = 2;
                        break;
                    case "First Instance of Text":
                    case "Last Instance of Text":
                    case "Paragraph Whose Index Is":
                    case "Paragraph Containing Text":
                    case "Table Containing Text":
                    case "Cell Containing Text":
                        this.grpMatchDomain.Enabled = true;
                        this.txtMCValue.Enabled = true;
                        this.txtMCName.Enabled = false;
                        if (this.chkWholeRange.Checked == true)
                            m_oCurrentInstruction.MatchRangeType = 1;
                        else
                            m_oCurrentInstruction.MatchRangeType = 2;
                        break;
                    case "Selection":
                        this.txtMCValue.Enabled = false;
                        this.txtMCName.Enabled = false;
                        this.grpMatchDomain.Enabled = false;
                        this.chkWholeRange.Checked = false;
                        m_oCurrentInstruction.MatchRangeType = 0;
                        break;
                    case "Nothing":
                        this.txtMCValue.Enabled = true;
                        this.txtMCName.Enabled = true;
                        this.grpMatchDomain.Enabled = true;
                        if (this.chkWholeRange.Checked == true)
                            m_oCurrentInstruction.MatchRangeType = 1;
                        else
                            m_oCurrentInstruction.MatchRangeType = 2;
                        break;
                    default:
                        this.txtMCValue.Enabled = true;
                        this.txtMCName.Enabled = true;
                        this.grpMatchDomain.Enabled = true;
                        if (this.chkWholeRange.Checked == true)
                            m_oCurrentInstruction.MatchRangeType = 1;
                        else
                            m_oCurrentInstruction.MatchRangeType = 2;
                        break;
                }

               
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbStory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionStory = 0;
                if (cmbStory.SelectedIndex != -1)
                    sInstructionStory = (short)Convert.ToInt32((int)cmbStory.SelectedIndex);

                m_oCurrentInstruction.Story = (short)sInstructionStory;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionStartDocType = 0;
                if (cmbRPDocObjType.SelectedIndex != -1)
                    sInstructionStartDocType = (short)Convert.ToInt32((int)cmbRPDocObjType.SelectedIndex);

                m_oCurrentInstruction.StartRangeType = sInstructionStartDocType;
                m_bCurInstructionDirty = true;

                PopulateStartRangeDocType();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRDDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionReturnDataType = 0;
                if (cmbRDDocObjType.SelectedIndex != -1)
                    sInstructionReturnDataType = (short)Convert.ToInt32((int)cmbRDDocObjType.SelectedIndex);

                m_oCurrentInstruction.ReturnDataType = sInstructionReturnDataType;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRDUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionReturnDataUnit = 0;
                if (cmbRDUnit.SelectedIndex != -1)
                    sInstructionReturnDataUnit = (short)Convert.ToInt32((int)cmbRDUnit.SelectedIndex);

                m_oCurrentInstruction.ReturnDataUnit = sInstructionReturnDataUnit;
                m_bCurInstructionDirty = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPDocObjType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionEndDocType = 0;
                if (cmbRPDocObjType2.SelectedIndex != -1)
                    sInstructionEndDocType = (short)Convert.ToInt32((int)cmbRPDocObjType2.SelectedIndex);

                m_oCurrentInstruction.EndRangeType = sInstructionEndDocType;
                m_bCurInstructionDirty = true;

                PopulateEndRangeDocType();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                m_bCurInstructionDirty = true;
                short sInstructionStartSection = 1;
                if (cmbRPSection.SelectedIndex != -1)
                    sInstructionStartSection = (short)Convert.ToInt32((int)cmbRPSection.SelectedIndex);

                m_oCurrentInstruction.StartRangeSection = sInstructionStartSection;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPSection2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionEndSection = 1;
                if (cmbRPSection2.SelectedIndex != -1)
                    sInstructionEndSection = (short)Convert.ToInt32((int)cmbRPSection2.SelectedIndex);

                m_oCurrentInstruction.EndRangeSection = sInstructionEndSection;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionStartPosition = 1;

                if (cmbRPPosition.SelectedIndex != -1)
                    sInstructionStartPosition = (short)Convert.ToInt32((int)cmbRPPosition.SelectedIndex);

                m_oCurrentInstruction.StartRangePosition = sInstructionStartPosition;

                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPPosition2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionEndPosition = 0;
                if (cmbRPPosition2.SelectedIndex != -1)
                    sInstructionEndPosition = (short)Convert.ToInt32((int)cmbRPPosition2.SelectedIndex);

                m_oCurrentInstruction.EndRangePosition = sInstructionEndPosition;

                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtRPName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.StartRangeName = txtRPName.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtRDName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.ReturnDataName = txtRDName.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtMCName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.MatchCriteriaName = txtMCName.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtMCValue_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.MatchCriteriaValue = txtMCValue.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtRPName2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.EndRangeName = txtRPName2.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtRPValue_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.StartRangeValue = txtRPValue.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtRPValue2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_oCurrentInstruction.EndRangeValue = txtRPValue2.Text;
                m_bCurInstructionDirty = true;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                m_bCurInstructionDirty = true;
                short sInstructionStartOperator = 0;
                if (cmbRPOperator.SelectedIndex != -1)
                    sInstructionStartOperator = (short)Convert.ToInt32((int)cmbRPOperator.SelectedIndex);

                m_oCurrentInstruction.StartRangeOperator = (short)sInstructionStartOperator;
                PopulateStartRangeNameValues();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbRPOperator2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                short sInstructionEndOperator = 0;
                if (cmbRPOperator2.SelectedIndex != -1)
                    sInstructionEndOperator = (short)Convert.ToInt32((int)cmbRPOperator2.SelectedIndex);

                m_oCurrentInstruction.EndRangeOperator = sInstructionEndOperator;
                m_bCurInstructionDirty = true;
                PopulateEndRangeNameValues();

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cbMCDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //reset matching group enabled prop
                this.grpMatchDomain.Enabled = true;

                // enable or disable name and value for Match Criteria, according to which match criteria object type is selected
                switch (cmbMCDocObjType.SelectedIndex)
                {
                    case (int)daMatchCriteria.None:
                        this.txtMCName.Enabled = false;
                        this.txtMCName.Text = "";
                        this.txtMCValue.Enabled = false;
                        this.txtMCValue.Text = "";
                        break;
                    case (int)daMatchCriteria.DocVar:
                    case (int)daMatchCriteria.DocProp:
                    case (int)daMatchCriteria.Bookmark:
                        this.txtMCName.Enabled = true;
                        this.txtMCValue.Enabled = false;
                        this.txtMCValue.Text = "";
                        this.grpMatchDomain.Enabled = false;
                        break;
                    case (int)daMatchCriteria.Keyword:
                        this.txtMCName.Enabled = true;
                        this.txtMCValue.Enabled = false;
                        this.txtMCValue.Text = "";
                        break;
                    case (int)daMatchCriteria.FirstInstanceOfStyle:
                    case (int)daMatchCriteria.LastInstanceOfStyle:
                    case (int)daMatchCriteria.AllInstancesOfStyle:
                        this.txtMCName.Enabled = true;
                        this.txtMCValue.Enabled = true;
                        this.grpMatchDomain.Enabled = false;
                        break;
                    case (int)daMatchCriteria.TableContainingStyle:
                    case (int)daMatchCriteria.CellContainingStyle:
                        this.txtMCName.Enabled = true;
                        this.txtMCValue.Enabled = true;
                        break;
                    case (int)daMatchCriteria.FirstInstanceOfText:
                    case (int)daMatchCriteria.LastInstanceOfText:
                    case (int)daMatchCriteria.TableContainingText:
                    case (int)daMatchCriteria.CellContainingText:
                    case (int)daMatchCriteria.ParagraphContainingText:
                    case (int)daMatchCriteria.ParagraphWhoseIndexIs:
                        {
                            this.txtMCName.Enabled = false;
                            this.txtMCName.Text = "";
                            this.txtMCValue.Enabled = true;
                            break;
                        }
                    default:
                        {
                            this.txtMCName.Enabled = false;
                            this.txtMCName.Text = "";
                            this.txtMCValue.Enabled = false;
                            this.txtMCValue.Text = "";
                        }
                        break;
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// resets template property of current instance of parseinstruction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTemplate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                m_iParseElementRowIndex = grdElements.CurrentRow.Index;

                if (m_oCurrentParsingInstruction == null)
                    m_oCurrentParsingInstruction = new ParsingInstruction(txtTemplate.Text, m_iParsingSource);
                else
                {
                    m_oCurrentParsingInstruction.Template = txtTemplate.Text;
                   //grdElements.Rows[m_iParseElementRowIndex].Cells["Template"].Value = txtTemplate.Text;
                }
                if (m_iParsingSource > 0 && !m_bLoadingInstructions)
                {
                    m_bCurParsingInstructionDirty = true;
                }
            }
            catch (Exception oE)
            {
                //LMP.Error.Show(oE);
            }
        }        
        /// <summary>
        /// sets the parse source prop of the current parsing instruction
        /// instance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbtnElementText_Click(object sender, EventArgs e)
        {
            try
            {
                this.m_iParsingSource = Definitions.daParseSource.daElementValue;
                m_iParseElementRowIndex = grdElements.CurrentRow.Index;

                //create a new parsing instruction instance if null;
                //otherwise reset parsesource property
                if (m_oCurrentParsingInstruction == null)
                    m_oCurrentParsingInstruction = new ParsingInstruction(txtTemplate.Text, m_iParsingSource);
                else
                    m_oCurrentParsingInstruction.ParseSource = Definitions.daParseSource.daElementValue;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// sets the parse source prop of the current parsing instruction
        /// instance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbtnElementMatchRange_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.m_iParsingSource = Definitions.daParseSource.daElementMatchRange;

                //create a new parsing instruction instance if null;
                //otherwise reset parsesource property
                if (m_oCurrentParsingInstruction == null)
                    m_oCurrentParsingInstruction = new ParsingInstruction(txtTemplate.Text, m_iParsingSource);
                else
                    m_oCurrentParsingInstruction.ParseSource = Definitions.daParseSource.daElementMatchRange;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles errors in elements data grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdElements_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                //return if Index out of range; we're just adding a new record to the grid
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LMP.DocAnalyzer.Application.AnalyzeCurrentDocument(DocType.OutputFormats.DelimitedString, true, false);
        }

        private void cmbDocType_MouseClick(object sender, MouseEventArgs e)
        {
            //cmbDocType.Sorted = true;

        }

        private void grdElements_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (this.m_bCurInstructionDirty)
                SaveCurrentInstruction();
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            int iCurrentRowIndex = grdElements.SelectedRows[0].Index;
            int iRowAboveIndex = iCurrentRowIndex - 1;

            if (iCurrentRowIndex > 0)
            {
                int iOrder = Convert.ToInt32(grdElements.Rows[iCurrentRowIndex].Cells["SortOrder"].Value);
                this.grdElements.Rows[iCurrentRowIndex].Cells["SortOrder"].Value = (iOrder - 1);
                SaveElements(grdElements.Rows[iCurrentRowIndex].Index);
                //set sort order to 1 less the original value
                this.grdElements.Rows[iRowAboveIndex].Cells["SortOrder"].Value = iOrder;
                //wierdly, this assignment does not always take.  If the value
                //has not changed, assign it again.
                if (Convert.ToInt32(this.grdElements.Rows[iRowAboveIndex].Cells["SortOrder"].Value) != iOrder)
                    this.grdElements.Rows[iRowAboveIndex].Cells["SortOrder"].Value = iOrder;
                //set sortorder for element one row above to orginal value of targeted element.
                this.grdElements.Rows[iRowAboveIndex].Cells["SortOrder"].Value = iOrder;
                SaveElements(grdElements.Rows[iRowAboveIndex].Index);
                //resort
                PopulateElements(m_oDocType.Elements);
                //select current row
                //GLOG : 8143 : jsw
                this.grdElements.CurrentCell = this.grdElements.Rows[iRowAboveIndex].Cells[0];
                tbtnEditDeterminants.Enabled = true;
                //display  
                PopulateInstructionsListBox(iRowAboveIndex);
                //GLOG : 8143 : jsw
                this.grdElements.Refresh();

            }
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            int iCurrentRowIndex = grdElements.SelectedRows[0].Index;
            int iRowBelowIndex = iCurrentRowIndex + 1;
            int iLastRowIndex = grdElements.Rows.Count - 1;

            if (iCurrentRowIndex < iLastRowIndex)
            {
                int iOrder = Convert.ToInt32(grdElements.Rows[iCurrentRowIndex].Cells["SortOrder"].Value);
                this.grdElements.Rows[iCurrentRowIndex].Cells["SortOrder"].Value = (iOrder + 1);
                SaveElements(grdElements.Rows[iCurrentRowIndex].Index);
                this.grdElements.Rows[iRowBelowIndex].Cells["SortOrder"].Value = iOrder;
                //wierdly, this assignment does not always take.  If the value
                //has not changed, assign it again.
                if (Convert.ToInt32(this.grdElements.Rows[iRowBelowIndex].Cells["SortOrder"].Value) != iOrder)
                    this.grdElements.Rows[iRowBelowIndex].Cells["SortOrder"].Value = iOrder;
                SaveElements(grdElements.Rows[iRowBelowIndex].Index);
                PopulateElements(m_oDocType.Elements);
                this.grdElements.Rows[iRowBelowIndex].Selected = true;
                //GLOG : 8143 : jsw
                this.grdElements.CurrentCell = this.grdElements.Rows[iRowBelowIndex].Cells[0]; 
                PopulateInstructionsListBox(iRowBelowIndex);
                //GLOG : 8143 : jsw
                this.grdElements.Refresh();
            }
        }

        private void tbtnManageEncryptionSeed_Click(object sender, EventArgs e)
        {

            bool bIsValid;
            
            EncryptionSeedForm oForm = new EncryptionSeedForm();

            oForm.ShowDialog();

            if (!oForm.Cancelled)
            {
                
                //is there a way to test the validity of the encryption seed?

                //test for validity
                //bIsValid = (d == DateTime.Parse("4/1/2099", LMP.Culture.USEnglishCulture) ||
                //    (d > DateTime.Now && oTSpan.Days < 183));

                bIsValid = true;

                //write to registry if valid
                //put in a try block
                    if (bIsValid)
                    {
                        //GLOG 7962 JSW 1/5/15
                        //this may produce an error if .exe is not run as admin
                        //display error message in friendly format
                        try
                        {
                            LMP.Registry.SetLocalMachineValue(
                            LMP.DocAnalyzer.Application.REGISTRY_ROOT, "MacPacEncryptionSeed", oForm.Key);
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message + ".\n\nPlease run daAdmin.exe as Administrator for write access." , LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                        //set message for next time
                        oForm.Message = LMP.Resources.GetLangString(
                            "Dialog_ControlPropertyInvalidEncryptionSeed");
            }
        }
        private void LoadDocType()
        {
            try {

                if (cmbDocType.SelectedIndex > 0)
                {
                    m_iSelectedElementRow = -1;
                    grdElements.Enabled = true;
                    tbtnEditDeterminants.Enabled = true;
                    lstInstructionIndexes.Items.Clear();

                    //save current parsing instruction before reloading
                    //elements grid
                    if (m_bCurParsingInstructionDirty)
                        SaveCurrentParseInstruction();

                    DocType oDType = (DocType)cmbDocType.SelectedItem;
                    //LoadCmbDocType();
                    //m_oDocType = new DocType(cmbDocType.Text);
                    //m_oDocType = new (DocType)oDType.ID;
                    //Populate Elements Grid
                    DocType oDocType = (DocType)cmbDocType.SelectedItem;
                    m_oDocType = new DocType(oDocType.ID);
                    PopulateElements(m_oDocType.Elements);
                    cmbDocType.Focus();
                    tbtnImportElements.Enabled = true;
                    tbtnDeleteElement.Enabled = true;
                    tbtnAddElement.Enabled = true;
                    if (grdElements.Rows.Count > 0)
                    {
                        tbtnAddInstruction.Enabled = true;
                        tbtnDeleteInstruction.Enabled = true;
                        pnlMatchInstructions.Enabled = true;
                        pnlParseInstruction.Enabled = true;
                        m_bDeleteElement = false;
                        PopulateInstructionsListBox();
                        LoadParseInstruction(0);

                        pnlMatchInstructions.Enabled = true;
                        pnlParseInstruction.Enabled = true;

                        DataGridViewColumn oColumn = new DataGridViewColumn();
                        oColumn = this.grdElements.Columns["SortOrder"];
                        grdElements.Sort(oColumn, ListSortDirection.Ascending);
                        if (grdElements.Rows.Count > 2)
                        {
                            btnMoveDown.Enabled = true;
                            btnMoveUp.Enabled = true;
                        }
                        if (grdElements.SelectedRows[0].Index == 0)
                            btnMoveUp.Enabled = false;
                        if (grdElements.SelectedRows[0].Index == (grdElements.Rows.Count - 1))
                            btnMoveDown.Enabled = false;
                    }
                    else
                    {
                        tbtnAddInstruction.Enabled = false;
                        tbtnDeleteInstruction.Enabled = false;
                        pnlMatchInstructions.Enabled = false;
                        pnlParseInstruction.Enabled = false;
                        btnMoveDown.Enabled = false;
                        btnMoveUp.Enabled = false;
                    }
                }
                else if (cmbDocType.SelectedIndex == 0)
                {
                    pnlMatchInstructions.Enabled = false;
                    pnlParseInstruction.Enabled = false;
                    tbtnDeleteElement.Enabled = false;
                    tbtnAddElement.Enabled = false;
                    grdElements.Enabled = false;
                    grdElements.DataSource = null;
                    lstInstructionIndexes.Items.Clear();
                    txtTemplate.Text = "";
                    tbtnEditDeterminants.Enabled = false;
                    LoadInstructionsPanelComboBoxes();
                    tbtnAddInstruction.Enabled = false;
                    tbtnDeleteInstruction.Enabled = false;
                    tbtnImportElements.Enabled = false;
                    cmbDocType.Focus();
                    btnMoveDown.Enabled = false;
                    btnMoveUp.Enabled = false;
                    //rdbtnElementMatchRange.Checked = false;
                    //rdbtnElementText.Checked = false;
                }
            }
            catch{}
        }

    }
}

