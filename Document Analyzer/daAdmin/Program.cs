using System;
using System.Collections.Generic;
using WinForms = System.Windows.Forms;

namespace LMP.DocAnalyzer.Admin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            WinForms.Application.EnableVisualStyles();
            WinForms.Application.SetCompatibleTextRenderingDefault(false);
           // WinForms.Application.Run(new Tester());
            WinForms.Application.Run(new MainForm());
        }
    }
}