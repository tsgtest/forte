using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.DocAnalyzer;
using System.Runtime.InteropServices;
using daCOM;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.DocAnalyzer.Admin
{
    public partial class DATester : Form
    {
        # region Declarations
        # endregion

        # region Form Load Events

        public DATester()
        {
            InitializeComponent();
        }

        private void DATester_Load(object sender, EventArgs e)
        {
            LoadCmbDocType();

        }

        # endregion

        # region Methods

        private CInstruction CreateInstruction(Word.Document oDoc)
        {
            short eid = 1;
            short execindex = 1;
            CInstruction oInstruction = new CInstruction();
            oInstruction.Story = (short)this.cbStory.SelectedIndex;
            oInstruction.ElementID = eid;
            oInstruction.ExecutionIndex = execindex;
            oInstruction.WordDocument = oDoc;


            short RPSect = 0;

            if ((string.IsNullOrEmpty(this.txtRPName.Text)) && (string.IsNullOrEmpty(this.txtRPValue.Text)))
            {
                oInstruction.MatchRangeType = (short)((int)DocAnalyzer.Application.daMatchRangeType.daMatchRangeType_None);

            }
            else
            {
                if (this.cbRangeParameter.SelectedIndex == 0)
                {
                    //set whole range

                    oInstruction.MatchRangeType = (short)((int)DocAnalyzer.Application.daMatchRangeType.daMatchRangeType_Whole);
                    oInstruction.WholeRangeName = this.txtRPName.Text;
                    oInstruction.WholeRangeOperator = (short)this.cbRPOperator.SelectedIndex;
                    oInstruction.WholeRangePosition = (short)this.cbRPPosition.SelectedIndex;

                    switch (this.cbRPSection.SelectedIndex)
                    {
                        case 0:
                            //oInstruction.StartRangeSection = 0;
                            oInstruction.StartRangeSection = RPSect;
                            break;
                        case 1:
                            RPSect = 1;
                            // oInstruction.StartRangeSection = 1;
                            oInstruction.StartRangeSection = RPSect;
                            break;
                        case 2:
                            RPSect = -1;
                            // oInstruction.StartRangeSection = -1;
                            oInstruction.StartRangeSection = RPSect;
                            break;
                        case 3:
                            RPSect = 0;
                            //oInstruction.StartRangeSection = 0;
                            oInstruction.StartRangeSection = RPSect;
                            break;
                        default:
                            RPSect = 0;
                            oInstruction.StartRangeSection = RPSect;
                            // oInstruction.StartRangeSection = 0;
                            break;
                    }

                    oInstruction.WholeRangeType = (short)this.cbRPDocObjType.SelectedIndex;

                    if (!string.IsNullOrEmpty(this.txtRPValue.Text))
                    {
                        oInstruction.WholeRangeValue = this.txtRPValue.Text;
                    }
                }
                else // set start and end ranges.
                {
                    oInstruction.MatchRangeType = (short)((int)DocAnalyzer.Application.daMatchRangeType.daMatchRangeType_StartAndEnd);
                    oInstruction.StartRangeName = this.txtRPName.Text;
                    oInstruction.StartRangeOperator = (short)this.cbRPOperator.SelectedIndex;
                    oInstruction.StartRangePosition = (short)this.cbRPPosition.SelectedIndex;

                    switch (this.cbRPSection.SelectedIndex)
                    {
                        case 0:
                            // oInstruction.StartRangeSection = 0;
                            RPSect = 0;
                            oInstruction.StartRangeSection = RPSect;
                            break;
                        case 1:
                            RPSect = 1;
                            oInstruction.StartRangeSection = RPSect;
                            // oInstruction.StartRangeSection = 1;
                            break;
                        case 2:
                            RPSect = -1;
                            oInstruction.StartRangeSection = RPSect;
                            // oInstruction.StartRangeSection = -1;
                            break;
                        case 3:
                            RPSect = 0;
                            oInstruction.StartRangeSection = RPSect;
                            // oInstruction.StartRangeSection = 0;
                            break;
                    }

                    oInstruction.StartRangeType = (short)this.cbRPDocObjType.SelectedIndex;
                    oInstruction.StartRangeValue = this.txtRPValue.Text;
                    oInstruction.EndRangeName = this.txtRPName2.Text;
                    oInstruction.EndRangeOperator = (short)this.cbRPOperator2.SelectedIndex;
                    oInstruction.EndRangePosition = (short)this.cbRPPosition2.SelectedIndex;

                    switch (this.cbRPSection2.SelectedIndex)
                    {
                        case 0:
                            RPSect = 0;
                            oInstruction.EndRangeSection = RPSect;
                            // oInstruction.EndRangeSection = 0;
                            break;
                        case 1:
                            RPSect = 1;
                            oInstruction.EndRangeSection = RPSect;
                            // oInstruction.EndRangeSection = 1;
                            break;
                        case 2:
                            RPSect = -1;
                            oInstruction.EndRangeSection = RPSect;
                            //oInstruction.EndRangeSection = -1;
                            break;
                        case 3:
                            RPSect = -1;
                            oInstruction.EndRangeSection = RPSect;
                            // oInstruction.EndRangeSection = 0;
                            break;
                    }


                    oInstruction.EndRangeType = (short)this.cbRPDocObjType2.SelectedIndex;
                    oInstruction.EndRangeValue = this.txtRPValue2.Text;
                }
            }
            //assign criteria values

            oInstruction.MatchCriteriaName = this.txtMCName.Text;
            oInstruction.MatchCriteriaType = (short)this.cbMCDocObjType.SelectedIndex;
            oInstruction.MatchCriteriaValue = this.txtMCValue.Text;

            //assign return data values
            oInstruction.ReturnDataType = (short)this.cbRDDocObjType.SelectedIndex;
            oInstruction.ReturnDataUnit = (short)this.cbRDUnit.SelectedIndex;
            oInstruction.ReturnDataName = this.txtRDName.Text;


            return oInstruction;
        }

        //Method for populating Combo boxes
        private void LoadCmbDocType()
        {
            # region Populate Combo story
            this.cbStory.Items.Clear();
            this.cbStory.Items.Insert(0, "None");
            this.cbStory.Items.Insert(1, "Main");
            this.cbStory.Items.Insert(2, "Header");
            this.cbStory.Items.Insert(3, "Footer");
            this.cbStory.Items.Insert(4, "All");
            this.cbStory.SelectedIndex = 1;
            # endregion

            # region Populate Combo Position

            this.cbRPPosition.Items.Clear();

            this.cbRPPosition.Items.Insert(0, "Start");
            this.cbRPPosition.Items.Insert(1, "End");
            this.cbRPPosition.Items.Insert(2, "Whole");
            this.cbRPPosition.SelectedIndex = 0;
            # endregion

            # region Populate Combo Position
            this.cbRPPosition2.Items.Clear();

            this.cbRPPosition2.Items.Insert(0, "Start");
            this.cbRPPosition2.Items.Insert(1, "End");
            this.cbRPPosition2.SelectedIndex = 0;

            # endregion
            # region Populate Combo Parameter
            this.cbRangeParameter.Items.Clear();
            this.cbRangeParameter.Items.Insert(0, "Whole Range");
            this.cbRangeParameter.Items.Insert(1, "Start Range");
            this.cbRangeParameter.SelectedIndex = 1;
            # endregion

            # region Populate Combo Parameter2
            this.cbRangeParameter2.Items.Clear();

            this.cbRangeParameter2.Items.Insert(0, "End Range");
            this.cbRangeParameter2.SelectedIndex = 0;
            # endregion

            # region Populate Combo Section
            this.cbRPSection.Items.Clear();
            this.cbRPSection.Items.Insert(0, "All");
            this.cbRPSection.Items.Insert(1, "First");
            this.cbRPSection.Items.Insert(2, "Last");
            this.cbRPSection.Items.Insert(3, "Number");
            this.cbRPSection.SelectedIndex = 0;
            # endregion

            # region Populate Combo Section2
            this.cbRPSection2.Items.Clear();
            this.cbRPSection2.Items.Insert(0, "All");
            this.cbRPSection2.Items.Insert(1, "First");
            this.cbRPSection2.Items.Insert(2, "Last");
            this.cbRPSection2.Items.Insert(3, "Number");
            this.cbRPSection2.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPOperator
            this.cbRPOperator.Items.Clear();
            this.cbRPOperator.Items.Insert(0, "WhoseNameIs");
            this.cbRPOperator.Items.Insert(1, "ThatIs");
            this.cbRPOperator.Items.Insert(2, "ThatContainsTheText");
            this.cbRPOperator.Items.Insert(3, "WhoseIndexIs");
            this.cbRPOperator.Items.Insert(4, "ThatContainsTheStyle");
            this.cbRPOperator.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPOperator2
            this.cbRPOperator2.Items.Clear();
            this.cbRPOperator2.Items.Insert(0, "WhoseNameIs");
            this.cbRPOperator2.Items.Insert(1, "ThatIs");
            this.cbRPOperator2.Items.Insert(2, "ThatContainsTheText");
            this.cbRPOperator2.Items.Insert(3, "WhoseIndexIs");
            this.cbRPOperator2.Items.Insert(4, "ThatContainsTheStyle");
            this.cbRPOperator2.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPDocObjType
            this.cbRPDocObjType.Items.Clear();
            this.cbRPDocObjType.Items.Insert(0, "Bookmark");
            this.cbRPDocObjType.Items.Insert(1, "First Instance of Text");
            this.cbRPDocObjType.Items.Insert(2, "Last Instance of Text");
            this.cbRPDocObjType.Items.Insert(3, "Style");
            this.cbRPDocObjType.Items.Insert(4, "Paragraph");
            this.cbRPDocObjType.Items.Insert(5, "Table");
            this.cbRPDocObjType.Items.Insert(6, "Row");
            this.cbRPDocObjType.Items.Insert(7, "Cell");
            this.cbRPDocObjType.Items.Insert(8, "Keyword");
            this.cbRPDocObjType.Items.Insert(9, "Element");
            this.cbRPDocObjType.SelectedIndex = 0;
            # endregion

            # region Populate Combo RPDocObjType2
            this.cbRPDocObjType2.Items.Clear();
            this.cbRPDocObjType2.Items.Insert(0, "Bookmark");
            this.cbRPDocObjType2.Items.Insert(1, "First Instance of Text");
            this.cbRPDocObjType2.Items.Insert(2, "Last Instance of Text");
            this.cbRPDocObjType2.Items.Insert(3, "Style");
            this.cbRPDocObjType2.Items.Insert(4, "Paragraph");
            this.cbRPDocObjType2.Items.Insert(5, "Table");
            this.cbRPDocObjType2.Items.Insert(6, "Row");
            this.cbRPDocObjType2.Items.Insert(7, "Cell");
            this.cbRPDocObjType2.Items.Insert(8, "Keyword");
            this.cbRPDocObjType2.Items.Insert(9, "Element");
            this.cbRPDocObjType2.SelectedIndex = 0;
            # endregion

            # region Load DocType combo Box

            this.cbMCDocObjType.Items.Clear();
            cbMCDocObjType.Items.Insert(0, "Nothing");
            cbMCDocObjType.Items.Insert(1, "Bookmark");
            cbMCDocObjType.Items.Insert(2, "DocVar");
            cbMCDocObjType.Items.Insert(3, "DocProp");
            cbMCDocObjType.Items.Insert(4, "Style");
            cbMCDocObjType.Items.Insert(5, "FirstInstanceOfText");
            cbMCDocObjType.Items.Insert(6, "LastInstanceOfText");
            cbMCDocObjType.Items.Insert(7, "ParagraphContainingText");
            cbMCDocObjType.Items.Insert(8, "ParagraphWhoseIndexIs");
            cbMCDocObjType.Items.Insert(9, "TableContainingText");
            cbMCDocObjType.Items.Insert(10, "TableContainingStyle");
            cbMCDocObjType.Items.Insert(11, "CellContainingText");
            cbMCDocObjType.Items.Insert(12, "CellContainingStyle");
            cbMCDocObjType.Items.Insert(13, "Keyword");
            cbMCDocObjType.SelectedIndex = 0;

            # endregion

            # region Load RDDocType combo Box

            this.cbRDDocObjType.Items.Clear();
            this.cbRDDocObjType.Items.Insert(0, "The value");
            this.cbRDDocObjType.Items.Insert(1, "Nothing");
            this.cbRDDocObjType.Items.Insert(2, "true;;");
            this.cbRDDocObjType.Items.Insert(3, "false;");
            this.cbRDDocObjType.Items.Insert(4, "FromStartOfXToMatchedText");
            this.cbRDDocObjType.Items.Insert(5, "FromMatchedTextToEndOfX");
            this.cbRDDocObjType.Items.Insert(6, "FromEndOfMatchedTextToEndOfX");
            this.cbRDDocObjType.Items.Insert(7, "NextX");
            this.cbRDDocObjType.Items.Insert(8, "PreviousX");
            this.cbRDDocObjType.Items.Insert(9, "CurrentX");
            this.cbRDDocObjType.Items.Insert(10, "MappedValue");
            this.cbRDDocObjType.SelectedIndex = 0;

            # endregion

            # region Load Measurement Unit

            this.cbRDUnit.Items.Clear();
            this.cbRDUnit.Items.Insert(0, "Character");
            this.cbRDUnit.Items.Insert(1, "Word");
            this.cbRDUnit.Items.Insert(2, "Line");
            this.cbRDUnit.Items.Insert(3, "Sentence");
            this.cbRDUnit.Items.Insert(4, "Paragraph");
            this.cbRDUnit.Items.Insert(5, "Table");
            this.cbRDUnit.Items.Insert(6, "Cell");
            this.cbRDUnit.Items.Insert(7, "Row");
            this.cbRDUnit.Items.Insert(8, "Nothing");
            this.cbRDUnit.SelectedIndex = 8;

            # endregion

        }


        #endregion

        # region Button Click Events

        private void btnTest_Click(object sender, EventArgs e)
        {
            bool bGotRange;
            //Instruction oInstruction;

            //if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            //{
            string output = string.Empty;
            // object filename = openFileDialog1.FileName;
            object filename = "C:\\Sample Docs\\Letter sample_created by mp971.doc";
            object readOnly = false;
            object isVisible = true;
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document aDoc = Application.WordGlobals.Documents.Open(
                ref filename, ref missing, ref readOnly, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing, ref isVisible,
                ref missing, ref missing, ref missing, ref missing);
            aDoc.Activate();

            //validation here
            //if anything's selected that required value, make sure it has a value or nathis.
            //make sure operators match docobj types.
            //Make sure name or value is entered for Match Criteria

            switch (cbMCDocObjType.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Nothing:
                    break;

                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_DocVar:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_DocProp:
                    if (string.IsNullOrEmpty(this.txtMCName.Text))
                    {
                        MessageBox.Show("Please enter a name for Match Criteria type.");
                        this.txtMCName.Focus();
                        return;
                    }
                    switch (this.cbRDDocObjType.SelectedIndex)
                    {
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_FromStartOfXToMatchedText:
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_FromMatchedTextToEndOfX:
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_FromEndOfMatchedTextToEndOfX:
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_NextX:
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_PreviousX:
                        case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_CurrentX:
                            MessageBox.Show("Invalid return data type", "ERROR");
                            this.cbRDDocObjType.Focus();
                            break;
                        default:
                            break;

                    }
                    break;
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Keyword:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Bookmark:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_TableContainingStyle:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_CellContainingStyle:
                    if (string.IsNullOrEmpty(this.txtMCName.Text))
                    {
                        MessageBox.Show("Please enter a name for Match Criteria type.");
                        this.txtMCName.Focus();
                        break;
                    }

                    break;
                default:
                    if (string.IsNullOrEmpty(this.txtMCValue.Text))
                    {
                        MessageBox.Show("Please enter a value for Match Criteria type.");
                        this.txtMCValue.Focus();
                        break;
                    }
                    break;

            }

            // if mapped value is the Return Data type, make sure there's a list name entered.
            switch (this.cbRDDocObjType.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daReturnDataType.daReturnDataType_MappedValue:
                    if (string.IsNullOrEmpty(this.txtRDName.Text))
                    {
                        MessageBox.Show("Please enter a List Name for the mapped value.");
                        this.txtRDName.Focus();
                        break;
                    }
                    break;
            }
            CInstruction oInstruction = CreateInstruction(aDoc);
            bGotRange = oInstruction.GetRawValue();

            if (bGotRange)
            {

                MessageBox.Show(oInstruction.Text, "Result");

            }
            else
            {
                MessageBox.Show("Couldn't find match");
            }
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        # endregion

        # region ComboBox Events

        private void cbRPDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //set default operator for chosen Range parameter object type
            switch (this.cbRPDocObjType.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Bookmark:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_FirstInstanceOfText:
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_LastInstanceOfText:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Style:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Paragraph:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Table:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Cell:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Row:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Keyword:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Element:
                    this.cbRPOperator.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
            }

        }

        private void cbRPDocObjType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //set default operator for chosen Range parameter object type
            switch (this.cbRPDocObjType2.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Bookmark:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_FirstInstanceOfText:
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_LastInstanceOfText:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Style:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Paragraph:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Table:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Cell:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Row:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Keyword:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
                case (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Element:
                    this.cbRPOperator2.SelectedIndex = (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs;
                    break;
            }

        }

        private void cbRPOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            // enable and disable name and value txt boxes according to which range parameter operator is selected
            switch (this.cbRPOperator.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs:
                    this.txtRPName.Enabled = true; ; ;
                    if (this.cbRPDocObjType.SelectedIndex == (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Style)
                    {
                        this.txtRPValue.Enabled = true; ; ;
                    }
                    else
                    {
                        this.txtRPValue.Enabled = false; ;
                        this.txtRPValue.Text = "";

                    }
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatIs:
                    this.txtRPName.Enabled = false; ;
                    this.txtRPName.Text = "";
                    this.txtRPValue.Enabled = true; ; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText:
                    this.txtRPName.Enabled = false; ;
                    this.txtRPName.Text = "";
                    this.txtRPValue.Enabled = true; ; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseIndexIs:
                    this.txtRPName.Enabled = false; ;
                    this.txtRPName.Text = "";
                    this.txtRPValue.Enabled = true; ; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheStyle:
                    this.txtRPName.Enabled = true; ; ;
                    this.txtRPValue.Enabled = true; ; ;
                    break;
            }

        }

        private void cbRPOperator2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // enable and disable name and value txt boxes according to which range parameter operator is selected
            switch (this.cbRPOperator2.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseNameIs:
                    this.txtRPName2.Enabled = true; ;
                    if (this.cbRPDocObjType2.SelectedIndex == (int)DocAnalyzer.Application.daRDDocObjType.daRDDocObjType_Style)
                    {
                        this.txtRPValue2.Enabled = true; ;
                    }
                    else
                    {
                        this.txtRPValue2.Enabled = false; ;
                        this.txtRPValue2.Text = "";
                    }
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatIs:
                    this.txtRPName2.Enabled = false; ;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheText:
                    this.txtRPName2.Enabled = false; ;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_WhoseIndexIs:
                    this.txtRPName2.Enabled = false; ;
                    this.txtRPName2.Text = "";
                    this.txtRPValue2.Enabled = true; ;
                    break;
                case (int)DocAnalyzer.Application.daRDOperator.daRDOperator_ThatContainsTheStyle:
                    this.txtRPName2.Enabled = true; ;
                    this.txtRPValue2.Enabled = true; ;
                    break;
            }
        }

        private void cbMCDocObjType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // enable or disable name and value for Match Criteria, according to which match criteria object type is selected
            switch (cbMCDocObjType.SelectedIndex)
            {
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Nothing:
                    this.txtMCName.Enabled = false; 
                    this.txtMCName.Text = "";

                    this.txtMCValue.Enabled = false;
                    this.txtMCValue.Text = "";
                    break;
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_DocVar:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_DocProp:
                    this.txtMCName.Enabled = true;
                    this.txtMCValue.Enabled = false;
                    this.txtMCValue.Text = "";
                    break;
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Bookmark:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Keyword:
                    this.txtMCName.Enabled = true;
                    this.txtMCValue.Enabled = false;
                    this.txtMCValue.Text = "";
                    break;
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_Style:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_TableContainingStyle:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_CellContainingStyle:
                    this.txtMCName.Enabled = true;
                    this.txtMCValue.Enabled = true;
                    break;
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_FirstInstanceOfText:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_LastInstanceOfText:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_TableContainingText:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_CellContainingText:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_ParagraphContainingText:
                case (int)DocAnalyzer.Application.daMatchCriteria.daMatchCriteria_ParagraphWhoseIndexIs:
                    {
                        this.txtMCName.Enabled = false;
                        this.txtMCName.Text = "";
                        this.txtMCValue.Enabled = true;
                        break;
                    }
                default:
                    {
                        this.txtMCName.Enabled = false;
                        this.txtMCName.Text = "";
                        this.txtMCValue.Enabled = false;
                        this.txtMCValue.Text = "";
                    }
                    break;

            }
        }

        private void cbRangeParameter_SelectedIndexChanged(object sender, EventArgs e)
        {
            //disable 2nd range parameter if "Whole" range is selected
            if (this.cbRangeParameter.SelectedIndex == 0)
            {
                this.cbRangeParameter2.Enabled = false;
                this.cbRPDocObjType2.Enabled = false;
                this.cbRPPosition2.Enabled = false;
                this.cbRPSection2.Enabled = false;
                this.cbRPOperator2.Enabled = false;
                this.txtRPName2.Enabled = false;
                this.txtRPValue2.Enabled = false;
                this.cbRPPosition.SelectedIndex = 2;
            }
            else
            {
                this.cbRangeParameter2.Enabled = true;
                this.cbRPDocObjType2.Enabled = true;
                this.cbRPPosition2.Enabled = true;
                this.cbRPSection2.Enabled = true;
                this.cbRPOperator2.Enabled = true;
                this.txtRPName2.Enabled = true;
                this.txtRPValue2.Enabled = true;
                this.cbRPPosition.SelectedIndex = 0;

            }
        }

        # endregion


        //Pass the DoctypeId and get the result in XML format.

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            string xXML = string.Empty;

            try
            {
                //To get the DocTypeId 
                DocType oDocType = Application.GetDocType(Application.WordGlobals.ActiveDocument);

                if(oDocType != null)
                    xXML = oDocType.AnalyzeDocument();
                else
                    MessageBox.Show("Doc Type could not be determined", 
                        "Document Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }


    }
}