using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace LMP.DocAnalyzer.Admin
{
    public partial class Tester : Form
    {
        public Tester()
        {
            InitializeComponent();
        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            try
            {
                Application.AnalyzeCurrentDocument(DocType.OutputFormats.XML,
                    this.chkShowESForm.CheckState == CheckState.Checked, true);

            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }

        private void btnAnalyzeString_Click(object sender, EventArgs e)
        {
            try
            {
                Application.AnalyzeCurrentDocument(DocType.OutputFormats.DelimitedString,
                    this.chkShowESForm.CheckState == CheckState.Checked, true);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }
    }
}