using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;

namespace LMP.DocAnalyzer.Admin
{
    public partial class CreateDocTypeForm : Form
    {
        # region Declarations
        public bool bAddDocType;
        private bool m_bCancelled;
        private string m_xName;

        public List<DocType> NewDocTypeList
        {
            get { return m_oDocTypesList; }

        }
        public bool Cancelled
        {
            get { return m_bCancelled; }
        }
        public string DocTypeName
        {
            get { return m_xName; }
        }
        List<DocType> m_oDocTypesList;
        DocType m_NewDocType;
        int m_DoctypeID = 0;

        # endregion

        # region FormLoad Events

        public CreateDocTypeForm()
        {
            InitializeComponent();
            m_oDocTypesList = new List<DocType>();
            m_oDocTypesList = Definitions.GetDocTypesList();
            bAddDocType = false;
        }

        private void CreateDocTypeForm_Load(object sender, EventArgs e)
        {
          
            cmbDeterminantType.DataSource = Definitions.GetTypesList();
            cmbDeterminantType.ValueMember = "Value";
            cmbDeterminantType.DisplayMember = "Key";
            m_NewDocType = new DocType();
        }

        # endregion

        # region Button Click Events
        private void btnAddDocType_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDocType.Text))
            {
                m_DoctypeID = m_NewDocType.Add(txtDocType.Text);

                m_oDocTypesList = new List<DocType>();
                m_oDocTypesList = Definitions.GetDocTypesList();

                bAddDocType = true;
                AddDeterminants();
                
                this.m_bCancelled = false;
                m_xName = this.txtDocType.Text;
            }
            else
            {
                MessageBox.Show("Please enter a new DocType", "Add DocType", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.m_bCancelled = true;

            this.Close();
        }


        # endregion

        # region Private Methods

        //Method to add the new Determinant
        private void AddDeterminants()
        {
            bool bDeterminantSave = false;
            int iDeterminantType = 0;
            string xDeterminantKey = string.Empty;
            string xDeterminantValue = string.Empty;
            bool bDeterminantValue = true;
            int iDeterminantId = 0;


            if (cmbDeterminantType.SelectedIndex > -1)
            {

                if (m_DoctypeID != 0)
                {
                    iDeterminantType = (int)cmbDeterminantType.SelectedValue;
                    if (!string.IsNullOrEmpty(txtDeterminantKey.Text))
                    {
                        xDeterminantKey = txtDeterminantKey.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(txtDeterminantValue.Text))
                    {
                        xDeterminantValue = txtDeterminantValue.Text.Trim();
                    }

                    m_NewDocType = new DocType(m_DoctypeID);
                    bDeterminantSave = m_NewDocType.UpdateDeterminant(iDeterminantType, xDeterminantKey, xDeterminantValue, iDeterminantId, bDeterminantValue);
                    this.Close();
                }
                else
                {
                    ShowDocTypesForm();
                }

            }
            this.Close();
        }

        //Method to show the DocTypes Form if not found in the List
        private void ShowDocTypesForm()
        {
            int iDocTypeID = 0;

            DocTypesForm oForm = new DocTypesForm("Select Document Type", "&Select Document Type:");
            oForm.ShowDialog();

            iDocTypeID = oForm.m_SelectDocType;
            if (iDocTypeID > -1)
            {
                m_NewDocType = new DocType(iDocTypeID);
                txtDocType.Text = m_NewDocType.Name;
                m_DoctypeID = iDocTypeID;
                AddDeterminants();
            }

        }

        # endregion

        private void cmbDeterminantType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbDeterminantType.Text == "Template" || this.cmbDeterminantType.Text == "Text")
            {
                this.txtDeterminantKey.Text = "";
                this.txtDeterminantKey.Enabled = false;
            }
            else
            {
                this.txtDeterminantKey.Enabled = true;
            }
        }

       
    }
}