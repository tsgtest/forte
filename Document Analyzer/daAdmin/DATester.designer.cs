namespace LMP.DocAnalyzer.Admin
{
    partial class DATester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMCValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMCName = new System.Windows.Forms.TextBox();
            this.cbMCDocObjType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRDName = new System.Windows.Forms.TextBox();
            this.cbRDUnit = new System.Windows.Forms.ComboBox();
            this.cbRDDocObjType = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbRPPosition2 = new System.Windows.Forms.ComboBox();
            this.txtRPValue2 = new System.Windows.Forms.TextBox();
            this.txtRPName2 = new System.Windows.Forms.TextBox();
            this.cbRPOperator2 = new System.Windows.Forms.ComboBox();
            this.cbRPDocObjType2 = new System.Windows.Forms.ComboBox();
            this.cbRPSection2 = new System.Windows.Forms.ComboBox();
            this.cbRangeParameter2 = new System.Windows.Forms.ComboBox();
            this.Position = new System.Windows.Forms.Label();
            this.Value = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbRPPosition = new System.Windows.Forms.ComboBox();
            this.txtRPValue = new System.Windows.Forms.TextBox();
            this.txtRPName = new System.Windows.Forms.TextBox();
            this.cbRPOperator = new System.Windows.Forms.ComboBox();
            this.cbRPDocObjType = new System.Windows.Forms.ComboBox();
            this.cbRPSection = new System.Windows.Forms.ComboBox();
            this.cbRangeParameter = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Element = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtElementName = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbStory = new System.Windows.Forms.ComboBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.btnAnalyze = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Element.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMCValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtMCName);
            this.groupBox1.Controls.Add(this.cbMCDocObjType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 296);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(937, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Match Criteria";
            // 
            // txtMCValue
            // 
            this.txtMCValue.Location = new System.Drawing.Point(337, 52);
            this.txtMCValue.Name = "txtMCValue";
            this.txtMCValue.Size = new System.Drawing.Size(100, 20);
            this.txtMCValue.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(347, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Value";
            // 
            // txtMCName
            // 
            this.txtMCName.Location = new System.Drawing.Point(187, 49);
            this.txtMCName.Name = "txtMCName";
            this.txtMCName.Size = new System.Drawing.Size(100, 20);
            this.txtMCName.TabIndex = 3;
            // 
            // cbMCDocObjType
            // 
            this.cbMCDocObjType.FormattingEnabled = true;
            this.cbMCDocObjType.Location = new System.Drawing.Point(10, 52);
            this.cbMCDocObjType.Name = "cbMCDocObjType";
            this.cbMCDocObjType.Size = new System.Drawing.Size(121, 21);
            this.cbMCDocObjType.TabIndex = 2;
            this.cbMCDocObjType.SelectedIndexChanged += new System.EventHandler(this.cbMCDocObjType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(184, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DocObj Type";
            // 
            // btnTest
            // 
            this.btnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTest.Location = new System.Drawing.Point(224, 551);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 1;
            this.btnTest.Text = "&Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(419, 551);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtRDName);
            this.groupBox2.Controls.Add(this.cbRDUnit);
            this.groupBox2.Controls.Add(this.cbRDDocObjType);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 419);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(937, 100);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Return Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(337, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "List Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(187, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Measurement Unit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "DocObj Type";
            // 
            // txtRDName
            // 
            this.txtRDName.Location = new System.Drawing.Point(337, 58);
            this.txtRDName.Name = "txtRDName";
            this.txtRDName.Size = new System.Drawing.Size(100, 20);
            this.txtRDName.TabIndex = 2;
            // 
            // cbRDUnit
            // 
            this.cbRDUnit.FormattingEnabled = true;
            this.cbRDUnit.Location = new System.Drawing.Point(187, 58);
            this.cbRDUnit.Name = "cbRDUnit";
            this.cbRDUnit.Size = new System.Drawing.Size(121, 21);
            this.cbRDUnit.TabIndex = 1;
            // 
            // cbRDDocObjType
            // 
            this.cbRDDocObjType.FormattingEnabled = true;
            this.cbRDDocObjType.Location = new System.Drawing.Point(10, 58);
            this.cbRDDocObjType.Name = "cbRDDocObjType";
            this.cbRDDocObjType.Size = new System.Drawing.Size(121, 21);
            this.cbRDDocObjType.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbRPPosition2);
            this.groupBox3.Controls.Add(this.txtRPValue2);
            this.groupBox3.Controls.Add(this.txtRPName2);
            this.groupBox3.Controls.Add(this.cbRPOperator2);
            this.groupBox3.Controls.Add(this.cbRPDocObjType2);
            this.groupBox3.Controls.Add(this.cbRPSection2);
            this.groupBox3.Controls.Add(this.cbRangeParameter2);
            this.groupBox3.Controls.Add(this.Position);
            this.groupBox3.Controls.Add(this.Value);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.cbRPPosition);
            this.groupBox3.Controls.Add(this.txtRPValue);
            this.groupBox3.Controls.Add(this.txtRPName);
            this.groupBox3.Controls.Add(this.cbRPOperator);
            this.groupBox3.Controls.Add(this.cbRPDocObjType);
            this.groupBox3.Controls.Add(this.cbRPSection);
            this.groupBox3.Controls.Add(this.cbRangeParameter);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 97);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(943, 163);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Location Range";
            // 
            // cbRPPosition2
            // 
            this.cbRPPosition2.FormattingEnabled = true;
            this.cbRPPosition2.Location = new System.Drawing.Point(816, 112);
            this.cbRPPosition2.Name = "cbRPPosition2";
            this.cbRPPosition2.Size = new System.Drawing.Size(121, 21);
            this.cbRPPosition2.TabIndex = 20;
            // 
            // txtRPValue2
            // 
            this.txtRPValue2.Location = new System.Drawing.Point(694, 112);
            this.txtRPValue2.Name = "txtRPValue2";
            this.txtRPValue2.Size = new System.Drawing.Size(100, 20);
            this.txtRPValue2.TabIndex = 19;
            // 
            // txtRPName2
            // 
            this.txtRPName2.Location = new System.Drawing.Point(572, 112);
            this.txtRPName2.Name = "txtRPName2";
            this.txtRPName2.Size = new System.Drawing.Size(100, 20);
            this.txtRPName2.TabIndex = 18;
            // 
            // cbRPOperator2
            // 
            this.cbRPOperator2.FormattingEnabled = true;
            this.cbRPOperator2.Location = new System.Drawing.Point(432, 113);
            this.cbRPOperator2.Name = "cbRPOperator2";
            this.cbRPOperator2.Size = new System.Drawing.Size(121, 21);
            this.cbRPOperator2.TabIndex = 17;
            this.cbRPOperator2.SelectedIndexChanged += new System.EventHandler(this.cbRPOperator2_SelectedIndexChanged);
            // 
            // cbRPDocObjType2
            // 
            this.cbRPDocObjType2.FormattingEnabled = true;
            this.cbRPDocObjType2.Location = new System.Drawing.Point(291, 112);
            this.cbRPDocObjType2.Name = "cbRPDocObjType2";
            this.cbRPDocObjType2.Size = new System.Drawing.Size(121, 21);
            this.cbRPDocObjType2.TabIndex = 16;
            this.cbRPDocObjType2.SelectedIndexChanged += new System.EventHandler(this.cbRPDocObjType2_SelectedIndexChanged);
            // 
            // cbRPSection2
            // 
            this.cbRPSection2.FormattingEnabled = true;
            this.cbRPSection2.Location = new System.Drawing.Point(150, 112);
            this.cbRPSection2.Name = "cbRPSection2";
            this.cbRPSection2.Size = new System.Drawing.Size(121, 21);
            this.cbRPSection2.TabIndex = 15;
            // 
            // cbRangeParameter2
            // 
            this.cbRangeParameter2.FormattingEnabled = true;
            this.cbRangeParameter2.Location = new System.Drawing.Point(6, 113);
            this.cbRangeParameter2.Name = "cbRangeParameter2";
            this.cbRangeParameter2.Size = new System.Drawing.Size(121, 21);
            this.cbRangeParameter2.TabIndex = 14;
            // 
            // Position
            // 
            this.Position.AutoSize = true;
            this.Position.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Position.Location = new System.Drawing.Point(842, 29);
            this.Position.Name = "Position";
            this.Position.Size = new System.Drawing.Size(52, 13);
            this.Position.TabIndex = 13;
            this.Position.Text = "Position";
            // 
            // Value
            // 
            this.Value.AutoSize = true;
            this.Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Value.Location = new System.Drawing.Point(707, 29);
            this.Value.Name = "Value";
            this.Value.Size = new System.Drawing.Size(39, 13);
            this.Value.TabIndex = 12;
            this.Value.Text = "Value";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(586, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(450, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Operator";
            // 
            // cbRPPosition
            // 
            this.cbRPPosition.FormattingEnabled = true;
            this.cbRPPosition.Location = new System.Drawing.Point(816, 63);
            this.cbRPPosition.Name = "cbRPPosition";
            this.cbRPPosition.Size = new System.Drawing.Size(121, 21);
            this.cbRPPosition.TabIndex = 9;
            // 
            // txtRPValue
            // 
            this.txtRPValue.Location = new System.Drawing.Point(694, 63);
            this.txtRPValue.Name = "txtRPValue";
            this.txtRPValue.Size = new System.Drawing.Size(100, 20);
            this.txtRPValue.TabIndex = 8;
            // 
            // txtRPName
            // 
            this.txtRPName.Location = new System.Drawing.Point(572, 63);
            this.txtRPName.Name = "txtRPName";
            this.txtRPName.Size = new System.Drawing.Size(100, 20);
            this.txtRPName.TabIndex = 7;
            // 
            // cbRPOperator
            // 
            this.cbRPOperator.FormattingEnabled = true;
            this.cbRPOperator.Location = new System.Drawing.Point(432, 61);
            this.cbRPOperator.Name = "cbRPOperator";
            this.cbRPOperator.Size = new System.Drawing.Size(121, 21);
            this.cbRPOperator.TabIndex = 6;
            this.cbRPOperator.SelectedIndexChanged += new System.EventHandler(this.cbRPOperator_SelectedIndexChanged);
            // 
            // cbRPDocObjType
            // 
            this.cbRPDocObjType.FormattingEnabled = true;
            this.cbRPDocObjType.Location = new System.Drawing.Point(291, 61);
            this.cbRPDocObjType.Name = "cbRPDocObjType";
            this.cbRPDocObjType.Size = new System.Drawing.Size(121, 21);
            this.cbRPDocObjType.TabIndex = 5;
            this.cbRPDocObjType.SelectedIndexChanged += new System.EventHandler(this.cbRPDocObjType_SelectedIndexChanged);
            // 
            // cbRPSection
            // 
            this.cbRPSection.FormattingEnabled = true;
            this.cbRPSection.Location = new System.Drawing.Point(150, 62);
            this.cbRPSection.Name = "cbRPSection";
            this.cbRPSection.Size = new System.Drawing.Size(121, 21);
            this.cbRPSection.TabIndex = 4;
            // 
            // cbRangeParameter
            // 
            this.cbRangeParameter.FormattingEnabled = true;
            this.cbRangeParameter.Location = new System.Drawing.Point(6, 62);
            this.cbRangeParameter.Name = "cbRangeParameter";
            this.cbRangeParameter.Size = new System.Drawing.Size(121, 21);
            this.cbRangeParameter.TabIndex = 3;
            this.cbRangeParameter.SelectedIndexChanged += new System.EventHandler(this.cbRangeParameter_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(317, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "DocObj Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(193, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Section";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Range Parameter";
            // 
            // Element
            // 
            this.Element.Controls.Add(this.label14);
            this.Element.Controls.Add(this.txtElementName);
            this.Element.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Element.Location = new System.Drawing.Point(12, 13);
            this.Element.Name = "Element";
            this.Element.Size = new System.Drawing.Size(482, 78);
            this.Element.TabIndex = 5;
            this.Element.TabStop = false;
            this.Element.Text = "Element";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Name";
            // 
            // txtElementName
            // 
            this.txtElementName.Location = new System.Drawing.Point(126, 20);
            this.txtElementName.Name = "txtElementName";
            this.txtElementName.Size = new System.Drawing.Size(100, 20);
            this.txtElementName.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbStory);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(510, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(439, 78);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Story";
            // 
            // cbStory
            // 
            this.cbStory.FormattingEnabled = true;
            this.cbStory.Location = new System.Drawing.Point(27, 26);
            this.cbStory.Name = "cbStory";
            this.cbStory.Size = new System.Drawing.Size(121, 21);
            this.cbStory.TabIndex = 0;
            // 
            // btnAnalyze
            // 
            this.btnAnalyze.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalyze.Location = new System.Drawing.Point(323, 551);
            this.btnAnalyze.Name = "btnAnalyze";
            this.btnAnalyze.Size = new System.Drawing.Size(75, 23);
            this.btnAnalyze.TabIndex = 11;
            this.btnAnalyze.Text = "Analyze";
            this.btnAnalyze.UseVisualStyleBackColor = true;
            this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
            // 
            // DATester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 607);
            this.Controls.Add(this.btnAnalyze);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.Element);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.groupBox1);
            this.Name = "DATester";
            this.Text = "DATester";
            this.Load += new System.EventHandler(this.DATester_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Element.ResumeLayout(false);
            this.Element.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMCValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMCName;
        private System.Windows.Forms.ComboBox cbMCDocObjType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRDName;
        private System.Windows.Forms.ComboBox cbRDUnit;
        private System.Windows.Forms.ComboBox cbRDDocObjType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtRPName;
        private System.Windows.Forms.ComboBox cbRPOperator;
        private System.Windows.Forms.ComboBox cbRPDocObjType;
        private System.Windows.Forms.ComboBox cbRPSection;
        private System.Windows.Forms.ComboBox cbRangeParameter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbRPPosition2;
        private System.Windows.Forms.TextBox txtRPValue2;
        private System.Windows.Forms.TextBox txtRPName2;
        private System.Windows.Forms.ComboBox cbRPOperator2;
        private System.Windows.Forms.ComboBox cbRPDocObjType2;
        private System.Windows.Forms.ComboBox cbRPSection2;
        private System.Windows.Forms.ComboBox cbRangeParameter2;
        private System.Windows.Forms.Label Position;
        private System.Windows.Forms.Label Value;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbRPPosition;
        private System.Windows.Forms.TextBox txtRPValue;
        private System.Windows.Forms.GroupBox Element;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtElementName;
        private System.Windows.Forms.ComboBox cbStory;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.Button btnAnalyze;
    }
}