Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Namespace LMP.DocumentAnalyzerWordMethods
    Friend Class GlobalMethods

        Public Shared g_xMatch As String 'need this for testing parsing object.
        Public Shared g_oMatchRange As Word.Range 'need this for testing parsing object.
        Public Shared g_oElements As Collection
        Public Shared g_bInitialized As Boolean
        Public Shared g_oCursor As Word.Range
        Public Shared g_oDocTypes As Collection
        Public Shared g_oDeterminants As Collection
        Public Shared g_iSeed As Integer
        Public Shared g_iOldSeed As Integer
        Private Shared m_xConnectString As String
        Private Shared m_xQuery As String
        Private Shared m_bInitialized As Boolean
        Private Shared g_oCurWordApp As Word.Application

        Public Const REGISTRY_ROOT = "Software\The Sackett Group\Document Analyzer"

        Public Shared Sub Initialize()

            On Error GoTo ProcError
            m_bInitialized = True
            g_oElements = New Collection
            g_oDocTypes = New Collection
            g_oDeterminants = New Collection

            g_iSeed = cEncrypt.iGetSeed()
            g_iOldSeed = cEncrypt.iGetSeed("OldSeed")

            g_bInitialized = True
            Exit Sub
ProcError:
            GlobalMethods.RaiseError("LMP.GlobalMethods.Initialize")
            Exit Sub
        End Sub

        Public Function CurrentWordApplication() As Word.Application
            On Error Resume Next
            'return null if word is not open
            CurrentWordApplication = g_oCurWordApp
        End Function

        Public Shared Property CurWordApp As Word.Application
            Get
                CurWordApp = g_oCurWordApp
            End Get
            Set(value As Word.Application)
                g_oCurWordApp = value
            End Set
        End Property
        Public Shared Function IsArrayInitialized(TheValue As Object) _
          As Boolean

            Dim v As Object
            IsArrayInitialized = False

            On Error GoTo ProcError

            If Not IsArray(TheValue) Then Exit Function
            On Error Resume Next
            v = TheValue(LBound(TheValue))
            IsArrayInitialized = Err.Number = 0

            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlGlobals.IsArrayInitialized")
            Exit Function
        End Function
        Public Shared Function IsAlphaNumeric(xChr As String) As Boolean
            On Error GoTo ProcError

            IsAlphaNumeric = xChr Like "[0-9A-Za-z]"

            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlGlobals.IsAlphaNumeric")
            Exit Function
        End Function

        Public Shared Function RemoveNonAlphaNumeric(xSource As String) As String
            Dim xChar As String
            Dim xNewString As String
            Dim i As Integer

            xNewString = ""

            For i = 1 To Len(xSource)
                xChar = Mid(xSource, i, 1)
                If IsAlphaNumeric(xChar) Then
                    xNewString = xNewString & xChar
                End If
            Next

            RemoveNonAlphaNumeric = xNewString

        End Function
        Public Shared Function ReplaceIllegalChars(xSource As String) As String
            Dim xChar As String
            Dim xNewString As String
            Dim i As Integer

            xNewString = ""

            For i = 1 To Len(xSource)
                xChar = Mid(xSource, i, 1)
                If IsAlphaNumeric(xChar) Then
                    xNewString = xNewString & xChar
                Else
                    Select Case xChar
                        Case "&"
                            xNewString = xNewString & "&#38;"
                            '            Case "_"
                            '                xNewString = xNewString & "&#95;"
                            '            Case Chr(32)
                            '                xNewString = xNewString & "&#160;"
                        Case Chr(148), Chr(34)
                            xNewString = xNewString & "&#34;"
                        Case Chr(146), Chr(39)
                            xNewString = xNewString & "&#39;"
                        Case Chr(60)
                            xNewString = xNewString & "&#60;"
                        Case Chr(62)
                            xNewString = xNewString & "&#62;"
                        Case Else
                            xNewString = xNewString & xChar
                    End Select
                End If
            Next

            ReplaceIllegalChars = xNewString
        End Function

        Public Shared Function StyleExists(oDoc As Word.Document, xStyleName As String) As Boolean

            Dim oStyle As Word.Style
            Dim bStyleExists As Boolean

            On Error GoTo ProcError

            For Each oStyle In oDoc.Styles
                If oStyle.InUse And oStyle.NameLocal = xStyleName Then
                    bStyleExists = True
                    Exit For
                End If
            Next
            StyleExists = bStyleExists

            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlGlobal.StyleExists")
            Exit Function
        End Function


        Public Shared Function StoryExists(oDoc As Word.Document, lStoryType As Long) As Boolean

            Dim oStory As Word.Range
            Dim bStoryExists As Boolean

            On Error GoTo ProcError

            For Each oStory In oDoc.StoryRanges
                If oStory.StoryType = lStoryType Then
                    bStoryExists = True
                    Exit For
                End If
            Next

            StoryExists = bStoryExists

            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlGlobals.StoryExists")
            Exit Function
        End Function

        Public Shared Function DocVarExists(oDoc As Word.Document, xDocVarName As String) As Boolean

            Dim oDocVar As Object
            Dim bDocVarExists As Boolean

            For Each oDocVar In oDoc.Variables

                If LCase(oDocVar.Name) = LCase(xDocVarName) Then
                    bDocVarExists = True
                    Exit For
                End If
            Next

            DocVarExists = bDocVarExists


        End Function
        Public Shared Function TemplateVarExists(oDoc As Word.Document, xTemplateVarName As String) As Boolean

            Dim oTemplateVar As Object
            Dim bTemplateExists As Boolean

            For Each oTemplateVar In oDoc.Variables

                If LCase(oTemplateVar.Name) = LCase(xTemplateVarName) Then
                    bTemplateExists = True
                    Exit For
                End If
            Next

            TemplateVarExists = bTemplateExists


        End Function


        Public Shared Function TrimBlanks(xSource As String)

            Dim xChar, xNewString As String
            Dim i As Integer
            Dim iAsc As Integer
            xNewString = Nothing

            For i = 1 To Len(xSource)
                xChar = Mid(xSource, i, 1)
                iAsc = Asc(xChar)
                Select Case iAsc
                    'Case vbCr, vbTab, vbVerticalTab, vbLf, vbNullChar, vbCrLf, vbNewLine, vbNullString, vbFormFeed
                    '
                    Case 13, 11, 9, 8, 12, 10, 7
                        'MsgBox ("got one")
                    Case Else
                        xNewString = Mid(xSource, i, Len(xSource) - (i - 1))
                        Exit For
                End Select
            Next

            For i = Len(xNewString) To 1 Step -1
                xChar = Mid(xNewString, i, 1)
                iAsc = Asc(xChar)
                Select Case iAsc
                    'Case vbCr, vbTab, vbVerticalTab, vbLf, vbNullChar, vbCrLf, vbNewLine, vbNullString, vbFormFeed
                    '
                    Case 13, 11, 9, 8, 12, 10, 7
                        'MsgBox ("got one")
                    Case 21
                        'this is a checkbox
                        'run a function to get the value
                        MsgBox("got a checkbox")
                        'have to have range to deal with this.
                    Case Else
                        xNewString = Mid(xNewString, 1, i)
                        Exit For
                End Select
            Next
            TrimBlanks = xNewString

        End Function


        Public Shared Function GetMinimum(lNumbers() As Long, iIndex As Integer, _
                                    xSeparators() As String) As Long

            Dim iLen, i As Integer
            Dim lMin As Long

            iLen = UBound(lNumbers)

            'minimum must be higher than zero
            For i = 0 To iLen
                If lNumbers(i) > 0 Then
                    lMin = lNumbers(i)
                    iIndex = i
                    Exit For
                End If
            Next

            'lMin = lNumbers(0)
            For i = 0 To iLen
                If lNumbers(i) < lMin And lNumbers(i) > 0 Then
                    lMin = lNumbers(i)
                    iIndex = i
                End If
            Next

            'if the strings have identical starting characters,
            'must choose longest string.
            For i = 0 To iLen
                If lNumbers(i) = lMin Then
                    If Len(xSeparators(iIndex)) < Len(xSeparators(i)) Then
                        lMin = lNumbers(i)
                        iIndex = i
                    End If
                End If
            Next

            '    'original loop
            '    lMin = lNumbers(0)
            '    For i = 0 To iLen
            '        If lNumbers(i) < lMin Then
            '            lMin = lNumbers(i)
            '            iIndex = i
            '        End If
            '    Next


            GetMinimum = lMin

        End Function
        Public Shared Function ConvertCheckBoxes(oRange As Word.Range) As String

            Dim oFF As Word.FormField
            Dim xVal As String
            xVal = "False"

            For Each oFF In oRange.FormFields
                If oFF.Type = WdFieldType.wdFieldFormCheckBox Then
                    If oFF.CheckBox.Value = True Then
                        xVal = "True"
                    Else
                        xVal = "False"
                    End If
                End If
            Next
            ConvertCheckBoxes = xVal

        End Function
        Public Shared Function GetKeywordList(xListName As String, _
                                        bGotList As Boolean) As Object()

            Dim vKeywordList() As Object
            Dim oConn As ADODB.Connection
            Dim oRS As ADODB.Recordset
            Dim xQuery As String
            Dim xOtherQuery As String
            Dim xOtherConnection As String
            Dim iKeywordListID As Integer

            GetKeywordList = Nothing

            On Error GoTo ProcError

            'connect to database
            oConn = New ADODB.Connection
            oConn.ConnectionString = m_xConnectString
            oConn.Open()

            'set recordset
            oRS = New ADODB.Recordset

            'query to see if we need to use a different database
            'to get keywords
            '    xQuery = "SELECT fldConnection, fldSQL " & _
            '        "FROM tblLists WHERE fldID = " & _
            '        iKeywordListID & ";"

            xQuery = "SELECT fldConnection, fldSQL, fldID " & _
                "FROM tblLists WHERE fldName = '" & _
                xListName & "';"

            oRS.Open(xQuery, oConn)

            If oRS.EOF Then
                bGotList = False
                Exit Function
            Else
                iKeywordListID = Int(oRS("fldID"))
            End If

            If oRS("fldConnection").Value <> "" _
                And (oRS("fldConnection").Value Is Nothing) = False Then
                'keywords are in another database.
                'store connection string & query in variables
                xOtherConnection = oRS("fldConnection").ToString
                xOtherQuery = oRS("fldSQL").ToString
                oConn.Close()
                'oRS.Close
                'open new connection
                oConn.ConnectionString = xOtherConnection
                oConn.Open()
                'query the other database
                oRS.Open(xOtherQuery, oConn)
            Else
                'keywords are in daInstructionSets.mdb,
                'so run the normal query
                xQuery = "SELECT [fldName] From tblListItems " & _
                    "WHERE fldListID=" & _
                    iKeywordListID & ";"
                oRS.Close()
                oRS.Open(xQuery, oConn)
            End If

            If oRS.EOF Then
                bGotList = False
                Exit Function
            End If

            'store keywords from recordset in an array
            vKeywordList = oRS.GetRows()
            bGotList = True

            'clean up
            oConn.Close()
            oConn = Nothing
            oRS = Nothing

            'return the array of keywords
            GetKeywordList = vKeywordList

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CDefinitions.GetKeywordList")
            Exit Function
        End Function
        Public Shared Function CreatePadString(ByVal iLength As Short, ByVal xChar As String) As String
            If iLength < 1 Then
                iLength = 0
            End If

            Return New String(xChar, iLength)
        End Function


        '        Public Shared Sub SendToDebug(ByVal xMessage As String, ByVal xSource As String, Optional ByVal iMsgType As mpDebugMessageTypes = mpDebugMessageTypes.mpDebugMessageType_Info)
        '            'sends the supplied message to debug if ci is in debug mode
        '            On Error GoTo ProcError
        '            'If DebugMode = True Then
        '            '    'send message to debug
        '            '    OutputDebugString(iMsgType & "  " & xSource & "  " & xMessage)
        '            'End If
        '            Exit Sub
        'ProcError:
        '            GlobalMethods.RaiseError("CIO.CError.SendToDebug")
        '            Exit Sub
        '        End Sub

        Public Shared Sub RaiseError(ByVal xNewSource As String)
            'raises the current error, appending the source
            With Err()
                If InStr(.Source, ".") Then
                    '           an originating source has already been specified - keep it
                    .Raise(.Number, .Source, .Description)
                Else
                    '           no originating source has been specified - use the one supplied
                    .Raise(.Number, xNewSource, .Description)
                End If
            End With
        End Sub

        Public Shared Sub ShowError()
            Dim lNum As Long
            Dim xDesc As String
            Dim xSource As String

            With Err()
                lNum = .Number
                xDesc = .Description
                xSource = .Source
            End With

            'SendToDebug("ERROR #" & lNum & " - " & xDesc, _
            '    xSource, mpDebugMessageTypes.mpDebugMessageType_Error)

            MsgBox("The following unexpected error occurred: " & vbCrLf & vbCrLf & _
                "Number:  " & lNum & vbCr & _
                "Description:  " & xDesc & vbCr & _
                "Source:  " & xSource, vbExclamation, My.Application.Info.Title)
        End Sub
    End Class
End Namespace

