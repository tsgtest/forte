Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.DocumentAnalyzerWord
    Public Class cError

        Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

        Public Enum ciErrs
            ciErr_MissingOrInvalidINIKey = vbError + 512 + 1
            ciErr_InvalidUNID
            ciErr_InvalidFilterField
            ciErr_NotImplemented
            ciErr_BackendLogonCancelled
            ciErr_CouldNotSetFilterField
            ciErr_MissingFile
            ciErr_InvalidListing
            ciErr_InvalidUserINI
            ciErr_invalidAddress
            ciErr_CouldNotAddToCollection
            ciErr_InvalidContactNumberType
            ciErr_InvalidContactDetailFormatString
            ciErr_DefaultFolderDoesNotExist
            ciErr_NoConnectedBackends
            ciErr_InvalidColumnParameter
            ciErr_ODBCError
            ciErr_CouldNotConnectToBackend
            ciErr_CouldNotExecuteSQL
            ciErr_CouldNotBootBackend
            ciErr_CouldNotReadRegistry
        End Enum

        Public Enum mpDebugMessageTypes
            mpDebugMessageType_Info
            mpDebugMessageType_Warning
            mpDebugMessageType_Error
        End Enum

        Private m_iDebugMode As Integer

Public Property Get DebugMode() As Boolean
    On Error GoTo ProcError
        Const DEBUG_ON As Byte = 1
        Const DEBUG_OFF As Byte = 2

        '    Dim oIni As CIO.CIni
        '
        '    If m_iDebugMode = Empty Then
        '        'get whether we're in debug mode or not
        '        Set oIni = New CIO.CIni
        '        m_iDebugMode = IIf(Trim$(UCase$(oIni.GetIni("CIApplication", "Debug"))) = _
        '            "TRUE", DEBUG_ON, DEBUG_OFF)
        '        Set oIni = Nothing
        '    End If
        '
    DebugMode = (m_iDebugMode = DEBUG_ON)
    Exit Property
ProcError:
    RaiseError "CIO.CError.DebugMode"
    Exit Property
End Property

        Public Sub SendToDebug(ByVal xMessage As String, ByVal xSource As String, Optional ByVal iMsgType As mpDebugMessageTypes = mpDebugMessageType_Info)
            'sends the supplied message to debug if ci is in debug mode
            On Error GoTo ProcError
            If DebugMode = True Then
                'send message to debug
                OutputDebugString(iMsgType & "  " & xSource & "  " & xMessage)
            End If
            Exit Sub
ProcError:
            RaiseError("CIO.CError.SendToDebug")
            Exit Sub
        End Sub

        Public Sub RaiseError(ByVal xNewSource As String)
            'raises the current error, appending the source
            With Err()
                If InStr(.Source, ".") Then
                    '           an originating source has already been specified - keep it
                    .Raise.Number, .Source, .Description
                Else
                    '           no originating source has been specified - use the one supplied
                    .Raise.Number, xNewSource, .Description
                End If
            End With
        End Sub

        Sub ShowError()
            Dim lNum As Long
            Dim xDesc As String
            Dim xSource As String

            With Err()
                lNum = .Number
                xDesc = .Description
                xSource = .Source
            End With

            SendToDebug("ERROR #" & lNum & " - " & xDesc, _
                xSource, mpDebugMessageType_Error)

            MsgBox("The following unexpected error occurred: " & vbCrLf & vbCrLf & _
                "Number:  " & lNum & vbCr & _
                "Description:  " & xDesc & vbCr & _
                "Source:  " & xSource, vbExclamation, App.Title)
        End Sub
    End Class
End Namespace
