Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.DocumentAnalyzer.MSWord
    Public Class DeterminantHelper

        Public Enum daDeterminantType
            Template = 1
            DocumentVariable = 2
            DocumentProperty = 3
            Bookmark = 4
            Text = 5
            Style = 6
        End Enum

        Public Function GetDeterminantValue(ByVal oDoc As Word.Document, ByVal iType As daDeterminantType, ByVal xParam As String) As String
            'returns the value for the specified determinant
            Dim xValue As String

            xValue = ""

            Select Case iType
                Case daDeterminantType.Template
                    xValue = GetTemplateValue(oDoc)
                Case daDeterminantType.DocumentVariable
                    xValue = GetDocVarValue(oDoc, xParam)
                Case daDeterminantType.DocumentProperty
                    xValue = GetDocPropValue(oDoc, xParam)
                Case daDeterminantType.Bookmark
                    xValue = BookmarkExists(oDoc, xParam)
                Case daDeterminantType.Text
                    xValue = TextExists(oDoc, xParam)
                Case daDeterminantType.Style
                    xValue = GlobalMethods.StyleExists(oDoc, xParam)

            End Select
            GlobalMethods.g_iSeed = Encryption.iGetSeed()
            If GlobalMethods.g_iSeed Then
                xValue = Encryption.DecryptText(xValue)
            End If

            If LCase(xValue) = "false" Then
                xValue = ""
            End If

            GetDeterminantValue = xValue
        End Function

        'Get the Document Property Value name if the DocObjType is DocumentProperty
        Private Function GetDocPropValue(oDoc As Word.Document, xDocPropName As String) As String

            Dim oProp As Object
            Dim xDocPropValue As String
            Dim bFoundMatch As Boolean

            xDocPropValue = ""

            For Each oProp In oDoc.BuiltInDocumentProperties
                If oProp.Name = xDocPropName Then
                    xDocPropValue = oProp.Value
                    bFoundMatch = True
                    Exit For
                End If
            Next
            If bFoundMatch = False Then
                For Each oProp In oDoc.CustomDocumentProperties
                    If oProp.Name = xDocPropName Then
                        xDocPropValue = oProp.Value
                        bFoundMatch = True
                        Exit For
                    End If
                Next
            End If

            GetDocPropValue = xDocPropValue

        End Function
        'Get the Document Variable Value name if the DocObjType is DocumentVariable
        Private Function GetDocVarValue(oDoc As Word.Document, xDocVarName As String) As String
            Dim xDocVarValue As String
            If GlobalMethods.DocVarExists(oDoc, xDocVarName) Then
                'get DocVar text from the doc.
                xDocVarValue = oDoc.Variables(xDocVarName).Value

            Else
                xDocVarValue = ""
            End If

            GetDocVarValue = xDocVarValue
        End Function


        'Get the template name if the DocObjType is AttachedTemplate
        Private Function GetTemplateValue(oDoc As Word.Document) As String

            Dim xTemplateName As String
            'GLOG 8504: Can't cast Template object to string
            xTemplateName = oDoc.AttachedTemplate.Name

            GetTemplateValue = xTemplateName

        End Function

        'Find whether the Bookmark Exists if the DocObjType is BookMark
        Private Function BookmarkExists(oDoc As Word.Document, xBookmarkName As String) As Boolean
            Dim m_xBookmarkValueExists As Boolean
            'Dim m_xBookmarkValue As String

            If oDoc.Bookmarks.Exists(xBookmarkName) Then
                'm_xBookmarkValue = oDoc.Bookmarks(xBookmarkName).Range.Text
                m_xBookmarkValueExists = True
            Else
                'm_xBookmarkValue = ""
                m_xBookmarkValueExists = False
            End If

            BookmarkExists = m_xBookmarkValueExists
            'BookmarkExists = m_xBookmarkValue


        End Function

        'Find whether the Text Exists if the DocObjType is Text
        Private Function TextExists(oDoc As Word.Document, xText As String) As Boolean

            Dim bFoundText As Boolean
            Dim oRange As Word.Range

            oRange = oDoc.Range
            With oRange.Find
                .ClearAllFuzzyOptions()
                .ClearFormatting()
                .Text = xText
                .Execute()
                If .Found Then
                    ' m_bFoundTextValue = xText
                    bFoundText = True
                Else
                    'm_bFoundTextValue = ""
                    bFoundText = False
                End If
            End With

            TextExists = bFoundText

        End Function

    End Class
End Namespace