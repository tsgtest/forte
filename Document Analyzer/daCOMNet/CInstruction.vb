Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports ADODB

Namespace LMP.DocumentAnalyzerWordMethods
    Public Enum daMatchRangeType
        None = 0
        Whole = 1
        StartAndEnd = 2
    End Enum

    Public Enum daStory
        None = 0
        Main = 1
        Header = 2
        Footer = 3
        All = 4
        PrimaryHeader = 5
        EvenPagesHeader = 6
        FirstPageHeader = 7
        PrimaryFooter = 8
        EvenPagesFooter = 9
        FirstPageFooter = 10
    End Enum

    Public Enum daRDDocObjType
        Bookmark = 0
        FirstInstanceOfText = 1
        LastInstanceOfText = 2
        Style = 3
        Paragraph = 4
        Table = 5
        Row = 6
        Cell = 7
        Keyword = 8
        Element = 9
    End Enum

    Public Enum daOrder
        daOrder_First = 1
        daOrder_Last = -1
    End Enum

    Public Enum daRDOperator
        WhoseNameIs = 0
        ThatIs = 1
        ThatContainsTheText = 2
        WhoseIndexIs = 3
        ThatContainsTheStyle = 4
    End Enum

    Public Enum daRDPosition
        Starting = 0
        Ending = 1
        Whole = 2
    End Enum

    Public Enum daMatchCriteria
        None = 0
        Bookmark = 1
        DocVar = 2
        DocProp = 3
        FirstInstanceOfStyle = 4
        LastInstanceOfStyle = 5
        AllInstancesOfStyle = 6
        FirstInstanceOfText = 7
        LastInstanceOfText = 8
        ParagraphContainingText = 9
        ParagraphWhoseIndexIs = 10
        TableContainingText = 11
        TableContainingStyle = 12
        CellContainingText = 13
        CellContainingStyle = 14
        Keyword = 15
        Selection = 16
    End Enum

    Public Enum daReturnDataType
        TheValue = 0
        None = 1
        IsTrue = 2
        IsFalse = 3
        FromStartOfXToMatchedText = 4
        FromMatchedTextToEndOfX = 5
        FromEndOfMatchedTextToEndOfX = 6
        NextX = 7
        PreviousX = 8
        CurrentX = 9
        MappedValue = 10
    End Enum

    Public Enum daReturnDataUnit
        Character = 0
        Word = 1
        Line = 2
        Sentence = 3
        Paragraph = 4
        Table = 5
        Cell = 6
        Row = 7
        None = 8
    End Enum

    'Public Enum daSeparatorType
    '    daSeparatorType_None = 0
    '    daSeparatorType_Paragraph = 1
    '    daSeparatorType_DoubleParagraph = 2
    '    daSeparatorType_SoftReturn = 3
    '    daSeparatorType_String = 4
    '    daSeparatorType_MultipleStrings = 5
    '    daSeparatorType_Cell = 6
    '    daSeparatorType_Row = 7
    '    daSeparatorType_Column = 8
    '    daSeparatorType_FieldList = 9
    'End Enum

    Public Enum daFieldListDef
        None = 0
        HeaderRow = 1
        DocVar = 2 'specify name
        Prefix = 3 'specify value
        IsString = 4 'specify value
    End Enum

    'Public Enum daNameSource
    '    daNameSource_Default = 0 '(record name + 1 for fields, element name for record)
    '    daNameSource_Label = 1
    '    daNameSource_HardCodedSingle = 2
    '    daNameSource_HardCodedMultiple = 3
    '    daNameSource_FirstRecord = 4
    '    daNameSource_DocVar = 5
    'End Enum

    Public Enum daAscii
        Tab = 0
        SoftReturn = 1
        ParagraphMark = 2
    End Enum

    Public Enum daWildCardStart
        Tab = 0
        Line = 1
        Paragraph = 2
        Cell = 3
        Row = 4
        ParentUnit = 5
        None = 6
    End Enum

    Public Structure RangeDef
        Dim iDocObjType As Integer
        Dim xDocObjName As String
        Dim iDocObjOperator As String
        Dim xDocObjValue As String
        Dim iSection As Integer
        Dim iPosition As Integer
    End Structure

    Public Structure MatchCriteriaDef
        Dim iDocObjType As String
        Dim xDocObjName As String
        Dim xDocObjValue As String
    End Structure

    Public Structure ReturnDataDef
        Dim iType As Integer
        Dim iUnit As Integer
        Dim xName As String
    End Structure
    Public Class cInstruction
        Private m_uStartRange As RangeDef
        Private m_uEndRange As RangeDef
        Private m_uWholeRange As RangeDef
        Private m_uMatchCriteria As MatchCriteriaDef
        Private m_uReturnData As ReturnDataDef
        Private m_iElementID As Integer
        Private m_iExecutionIndex As Integer
        Private m_iStory As Integer
        Private m_iReturnDataType
        Private m_iReturnDataUnit
        Private m_xText As String
        Private m_oRange As Word.Range
        Private m_iMatchRangeType As Integer
        Private m_oWordDocument As Word.Document
        Private m_bEncrypted As Boolean

        Private m_oRanges() As Word.Range

        Public WriteOnly Property WordDocument As Word.Document
            Set(value As Word.Document)
                m_oWordDocument = value
            End Set
        End Property

        Public Property Text As String
            Set(value As String)
                m_xText = value
            End Set
            Get
                Text = m_xText
            End Get
        End Property


        Public Property Range As Word.Range
            Set(value As Word.Range)
                m_oRange = value
            End Set
            Get
                Range = m_oRange
            End Get
        End Property
        Public Property Story As Integer
            Get
                Story = m_iStory
            End Get
            Set(value As Integer)
                m_iStory = value
            End Set
        End Property
        Public Property MatchRangeType As Integer
            Set(value As Integer)
                m_iMatchRangeType = value
            End Set
            Get
                MatchRangeType = m_iMatchRangeType
            End Get
        End Property
        Public Property StartRangeType As Integer
            Set(value As Integer)
                m_uStartRange.iDocObjType = value
            End Set
            Get
                StartRangeType = m_uStartRange.iDocObjType
            End Get
        End Property
        Public Property StartRangePosition As Integer
            Set(value As Integer)
                m_uStartRange.iPosition = value
            End Set
            Get
                StartRangePosition = m_uStartRange.iPosition
            End Get
        End Property
        Public Property StartRangeSection As Integer
            Set(value As Integer)
                m_uStartRange.iSection = value
            End Set
            Get
                StartRangeSection = m_uStartRange.iSection
            End Get
        End Property
        Public Property StartRangeName As String
            Set(value As String)
                m_uStartRange.xDocObjName = value
            End Set
            Get
                StartRangeName = m_uStartRange.xDocObjName
            End Get
        End Property
        Public Property StartRangeOperator As Integer
            Set(value As Integer)
                m_uStartRange.iDocObjOperator = value
            End Set
            Get
                StartRangeOperator = m_uStartRange.iDocObjOperator
            End Get
        End Property
        'text for table, index for table, id for element
        Public Property StartRangeValue As String
            Set(value As String)
                m_uStartRange.xDocObjValue = value
            End Set
            Get
                StartRangeValue = m_uStartRange.xDocObjValue
            End Get
        End Property
        Public Property EndRangeType As Integer
            Set(value As Integer)
                m_uEndRange.iDocObjType = value
            End Set
            Get
                EndRangeType = m_uEndRange.iDocObjType
            End Get
        End Property
        Public Property EndRangePosition As Integer
            Set(value As Integer)
                m_uEndRange.iPosition = value
            End Set
            Get
                EndRangePosition = m_uEndRange.iPosition
            End Get
        End Property

        Public Property EndRangeSection As Integer
            Set(value As Integer)
                m_uEndRange.iSection = value
            End Set
            Get
                EndRangeSection = m_uEndRange.iSection
            End Get
        End Property
        Public Property EndRangeName As String
            Set(value As String)
                m_uEndRange.xDocObjName = value
            End Set
            Get
                EndRangeName = m_uEndRange.xDocObjName
            End Get
        End Property

        Public Property EndRangeOperator As Integer
            Set(value As Integer)
                m_uEndRange.iDocObjOperator = value
            End Set
            Get
                EndRangeOperator = m_uEndRange.iDocObjOperator
            End Get
        End Property
        Public Property EndRangeValue As String
            Set(value As String)
                m_uEndRange.xDocObjValue = value
            End Set
            Get
                EndRangeValue = m_uEndRange.xDocObjValue
            End Get
        End Property
        Public Property WholeRangeType As Integer
            Set(value As Integer)
                m_uWholeRange.iDocObjType = value
            End Set
            Get
                WholeRangeType = m_uWholeRange.iDocObjType
            End Get
        End Property
        Public Property WholeRangePosition As Integer
            Set(value As Integer)
                m_uWholeRange.iPosition = value
            End Set
            Get
                WholeRangePosition = m_uWholeRange.iPosition
            End Get
        End Property
        Public Property WholeRangeSection As Integer
            Set(value As Integer)
                m_uWholeRange.iSection = value
            End Set
            Get
                WholeRangeSection = m_uWholeRange.iSection
            End Get
        End Property
        Public Property WholeRangeName As String
            Set(value As String)
                m_uWholeRange.xDocObjName = value
            End Set
            Get
                WholeRangeName = m_uWholeRange.xDocObjName
            End Get
        End Property
        Public Property WholeRangeOperator As Integer
            Set(value As Integer)
                m_uWholeRange.iDocObjOperator = value
            End Set
            Get
                WholeRangeOperator = m_uWholeRange.iDocObjOperator
            End Get
        End Property
        'text for table, index for table, id for element
        Public Property WholeRangeValue As String
            Set(value As String)
                m_uWholeRange.xDocObjValue = value
            End Set
            Get
                WholeRangeValue = m_uWholeRange.xDocObjValue
            End Get
        End Property
        Public Property MatchCriteriaType As Integer
            Set(value As Integer)
                m_uMatchCriteria.iDocObjType = value
            End Set
            Get
                MatchCriteriaType = m_uMatchCriteria.iDocObjType
            End Get
        End Property
        Public Property MatchCriteriaName As String
            Set(value As String)
                m_uMatchCriteria.xDocObjName = value
            End Set
            Get
                MatchCriteriaName = m_uMatchCriteria.xDocObjName
            End Get
        End Property
        Public Property MatchCriteriaValue As String
            Set(value As String)
                m_uMatchCriteria.xDocObjValue = value
            End Set
            Get
                MatchCriteriaValue = m_uMatchCriteria.xDocObjValue
            End Get
        End Property
        Public Property ElementID As Integer
            Get
                ElementID = m_iElementID
            End Get
            Set(value As Integer)
                m_iElementID = value
            End Set
        End Property
        Public Property ExecutionIndex As Integer
            Set(value As Integer)
                m_iExecutionIndex = value
            End Set
            Get
                ExecutionIndex = m_iExecutionIndex
            End Get
        End Property

        Public Property ReturnDataType As Integer
            Set(value As Integer)
                m_uReturnData.iType = value
            End Set
            Get
                ReturnDataType = m_uReturnData.iType
            End Get
        End Property

        Public Property ReturnDataUnit As Integer
            Set(value As Integer)
                m_uReturnData.iUnit = value
            End Set
            Get
                ReturnDataUnit = m_uReturnData.iUnit
            End Get
        End Property
        Public Property ReturnDataName As String
            Set(value As String)
                m_uReturnData.xName = value
            End Set
            Get
                ReturnDataName = m_uReturnData.xName
            End Get
        End Property

        Public ReadOnly Property Encrypted() As Boolean
            Get
                Encrypted = m_bEncrypted
            End Get
        End Property
        Public ReadOnly Property RangeArray() As Word.Range()
            Get
                RangeArray = m_oRanges
            End Get
        End Property

        Public Function GetRawValue() As Boolean

            Dim bGotMatch As Boolean
            Dim bGotStoryMatch As Boolean
            Dim oMatchRange As Word.Range
            Dim xMatch As String
            Dim bGotReturnVal As Boolean
            Dim bGotSearchRange As Boolean
            Dim iStories(2) As Integer
            Dim i, y As Integer
            Dim xPrompt As String
            Dim oRanges() As Word.Range
            Dim iCount As Integer
            Dim xHeaderFooterMatch As String
            Dim oHeaderFooterMatchRange As Word.Range

            xMatch = ""
            xHeaderFooterMatch = ""
            oRanges = Nothing
            oHeaderFooterMatchRange = Nothing
            oMatchRange = Nothing

            On Error GoTo ProcError
            If GlobalMethods.g_bInitialized = False Then
                GlobalMethods.Initialize()
            End If
            Select Case Me.Story
                'if story is header or footer, there are 3 story
                'possibilities, have to loop through them all
                Case daStory.Header
                    iCount = 0
                    iStories(0) = daStory.PrimaryHeader
                    iStories(1) = daStory.FirstPageHeader
                    iStories(2) = daStory.EvenPagesHeader
                    For i = 0 To 2
                        Me.Story = iStories(i)
                        'set location range for match
                        oMatchRange = GetMatchRange()
                        'xMatch is match text, oMatchRange is match range
                        bGotMatch = GetMatch(xMatch, oMatchRange)
                        If bGotMatch Then
                            For y = 0 To UBound(m_oRanges)
                                ReDim Preserve oRanges(iCount)
                                oRanges(iCount) = m_oRanges(y)
                                iCount = iCount + 1
                            Next
                            bGotStoryMatch = True
                            xHeaderFooterMatch = xMatch
                            oHeaderFooterMatchRange = GetRangeCopy(oMatchRange)
                        End If
                    Next
                    m_oRanges = oRanges
                Case daStory.Footer
                    iStories(0) = daStory.PrimaryFooter
                    iStories(1) = daStory.FirstPageFooter
                    iStories(2) = daStory.EvenPagesFooter
                    iCount = 0

                    For i = 0 To 2
                        Me.Story = iStories(i)
                        'set location range for match
                        oMatchRange = GetMatchRange()
                        'xMatch is match text, oMatchRange is match range
                        bGotMatch = GetMatch(xMatch, oMatchRange)
                        If bGotMatch Then
                            For y = 0 To UBound(m_oRanges)
                                ReDim Preserve oRanges(iCount)
                                oRanges(iCount) = m_oRanges(y)
                                iCount = iCount + 1
                            Next
                            bGotStoryMatch = True
                            xHeaderFooterMatch = xMatch
                            oHeaderFooterMatchRange = GetRangeCopy(oMatchRange)
                        End If
                    Next
                    m_oRanges = oRanges
                Case Else
                    'set location range for match
                    oMatchRange = GetMatchRange()
                    bGotMatch = GetMatch(xMatch, oMatchRange)
            End Select
            If bGotStoryMatch Then
                bGotMatch = True
                xMatch = xHeaderFooterMatch
                oMatchRange = oHeaderFooterMatchRange
            End If
            'If match found, get the return value
            If bGotMatch Then
                '        If Not (oMatchRange Is Nothing) Then
                '           oMatchRange.Select 'DEBUG Note - this only works with in doc.
                '        End If
                If GlobalMethods.g_iSeed Then
                    xMatch = cEncrypt.DecryptText(xMatch)
                End If
                bGotReturnVal = GetReturnValue(xMatch, oMatchRange)
            End If

            'If return value found, set text and range properties
            If bGotReturnVal Then
                Me.Text = xMatch
                Me.Range = oMatchRange
            End If
            'put cursor back in original position, in case it was moved.
            If Not GlobalMethods.g_oCursor Is Nothing Then
                GlobalMethods.g_oCursor.Select()
            End If
            GetRawValue = bGotReturnVal

            Exit Function
ProcError:
            GlobalMethods.ShowError()
            Exit Function
        End Function
        Private Function GetMatchRange() As Word.Range

            Dim oWholeRange As Word.Range
            Dim oStoryAndSection As Word.Range
            Dim oStartRange As Word.Range
            Dim oEndRange As Word.Range
            Dim iStartPos As Long
            Dim iEndPos As Integer
            Dim bGotStartRange As Boolean

            GetMatchRange = Nothing

            On Error GoTo ProcError

            Select Case Me.MatchRangeType
                Case daMatchRangeType.Whole
                    'set initial match range, according to story and section
                    oWholeRange = GetStoryAndSectionRange(m_uWholeRange.iSection)
                    GetMatchRange = oWholeRange
                Case daMatchRangeType.StartAndEnd
                    'set initial match range, according to story and section
                    oStoryAndSection = GetStoryAndSectionRange(m_uStartRange.iSection)
                    'get start range for match
                    oStartRange = GetRangeParameters(m_uStartRange, oStoryAndSection)
                    'set match range for end position according to story and section,
                    'starting at the end of oStartRange, unless end range is determined
                    'by table index.
                    If m_uEndRange.iDocObjOperator = daRDOperator.WhoseIndexIs Then
                        oStoryAndSection = GetStoryAndSectionRange(m_uEndRange.iSection)
                    Else
                        oStoryAndSection = GetStoryAndSectionRange(m_uEndRange.iSection, oStartRange)
                    End If
                    'get end range for match
                    oEndRange = GetRangeParameters(m_uEndRange, oStoryAndSection)
                    If Not (oStartRange Is Nothing) And Not (oEndRange Is Nothing) Then
                        iStartPos = oStartRange.Start
                        iEndPos = oEndRange.Start
                        'make sure end range is below start range
                        If iEndPos > iStartPos Then

                            GetMatchRange = oStoryAndSection
                            GetMatchRange.SetRange(Start:=iStartPos, End:=iEndPos)
                        End If
                    End If
                Case Else
                    'set match range, according to story only -- no section.
                    GetMatchRange = GetStoryAndSectionRange(0)
                    'MsgBox GetMatchRange
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetMatchRange")
            Exit Function
        End Function

        Private Function GetStoryAndSectionRange(iSection As Integer, _
                                            Optional oStartRange As Word.Range = Nothing) _
                                            As Word.Range
            Dim oRange As Word.Range
            Dim bGotStartRange As Boolean
            Dim oStory As Word.Range
            Dim bStoryExists As Boolean

            oRange = Nothing

            On Error GoTo ProcError

            If Not (oStartRange Is Nothing) Then
                bGotStartRange = True
            End If

            Select Case m_iStory
                Case daStory.All, daStory.Main
                    If m_iStory = daStory.All Then
                        oRange = m_oWordDocument.Range
                    Else
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdMainTextStory)

                    End If
                    If bGotStartRange Then
                        'set match range for end parameter
                        oRange = m_oWordDocument.Range(Start:=oStartRange.End, _
                            End:=oRange.End)
                    End If
                    Select Case iSection
                        Case 0
                            'no section specified
                        Case -1 'last section
                            oRange = m_oWordDocument.Sections.Last.Range
                            If bGotStartRange Then
                                'set match range for end parameter
                                oRange = m_oWordDocument.Range(Start:=oStartRange.End, _
                                    End:=oRange.End)
                            End If
                        Case Else
                            'does section exist?
                            If iSection <= m_oWordDocument.Sections.Count Then
                                oRange = m_oWordDocument.Sections(iSection).Range
                                If bGotStartRange Then
                                    'set match range for end parameter
                                    oRange = m_oWordDocument.Range(Start:=oStartRange.End, _
                                        End:=oRange.End)
                                End If
                            Else
                                'section doesn't exist
                                oRange = Nothing
                            End If
                    End Select
                Case daStory.PrimaryHeader
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdPrimaryHeaderStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdPrimaryHeaderStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
                Case daStory.PrimaryFooter
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdPrimaryFooterStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdPrimaryFooterStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
                Case daStory.EvenPagesHeader
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdEvenPagesHeaderStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdEvenPagesHeaderStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
                Case daStory.EvenPagesFooter
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdEvenPagesFooterStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdEvenPagesFooterStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
                Case daStory.FirstPageHeader
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdFirstPageHeaderStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdFirstPageHeaderStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
                Case daStory.FirstPageFooter
                    If GlobalMethods.StoryExists(m_oWordDocument, WdStoryType.wdFirstPageFooterStory) Then
                        oRange = m_oWordDocument.StoryRanges(WdStoryType.wdFirstPageFooterStory)
                        If bGotStartRange Then
                            'set match range for end parameter
                            oRange.SetRange(Start:=oStartRange.End, End:=oRange.End)
                        End If
                    End If
            End Select

            GetStoryAndSectionRange = oRange
            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetStoryAndSectionRange")
            Exit Function

        End Function

        Private Function GetRangeParameters(uRangeData As RangeDef, _
                                            ByVal oRange As Word.Range) _
                                            As Word.Range

            On Error GoTo ProcError

            Select Case uRangeData.iDocObjType
                Case daRDDocObjType.Bookmark
                    GetRangeParameters = GetBookmarkRange(uRangeData, oRange)
                Case daRDDocObjType.FirstInstanceOfText
                    GetRangeParameters = GetTextRange(uRangeData, oRange, daOrder.daOrder_First)
                Case daRDDocObjType.LastInstanceOfText
                    GetRangeParameters = GetTextRange(uRangeData, oRange, daOrder.daOrder_Last)
                Case daRDDocObjType.Style
                    GetRangeParameters = GetStyleRange(uRangeData, oRange)
                Case daRDDocObjType.Paragraph
                    GetRangeParameters = GetParagraphRange(uRangeData, oRange)
                Case daRDDocObjType.Table
                    GetRangeParameters = GetTableRange(uRangeData, oRange)
                Case daRDDocObjType.Row
                    GetRangeParameters = GetRowRange(uRangeData, oRange)
                Case daRDDocObjType.Cell
                    GetRangeParameters = GetCellRange(uRangeData, oRange)
                Case daRDDocObjType.Keyword
                    GetRangeParameters = GetKeywordRange(uRangeData, oRange)
                Case daRDDocObjType.Element
                    GetRangeParameters = GetElementRange(uRangeData)
                Case Else
                    GetRangeParameters = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetRangeParameters")
            Exit Function
        End Function
        Private Function GetBookmarkRange(uRangeData As RangeDef, _
                                            ByVal oRange As Word.Range) _
                                            As Word.Range

            Dim xBookmarkName As String
            Dim iOperator As Integer
            Dim iPosition As Integer
            Dim iSection As Integer

            On Error GoTo ProcError

            'need a range for this -- make sure range is valid
            If oRange Is Nothing Then
                GetBookmarkRange = Nothing
                Exit Function
            End If
            'get the bookmark name, operator type
            xBookmarkName = uRangeData.xDocObjName
            iOperator = uRangeData.iDocObjOperator
            iPosition = uRangeData.iPosition
            iSection = uRangeData.iSection

            'make sure DocObjOperator is appropriate
            Select Case iOperator
                Case daRDOperator.WhoseNameIs
                    'search for bookmark
                    If oRange.Bookmarks.Exists(xBookmarkName) Then
                        'get bookmark range
                        oRange = m_oWordDocument.Bookmarks(xBookmarkName).Range
                        'collapse range to start or end
                        Select Case iPosition
                            Case daRDPosition.Starting
                                oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                            Case daRDPosition.Ending
                                oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                        End Select
                        GetBookmarkRange = oRange
                    Else
                        GetBookmarkRange = Nothing
                    End If
                Case Else
                    GetBookmarkRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetBookmarkRange")
            Exit Function
        End Function

        Private Function GetTextRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range, iOrder As Integer) _
                                        As Word.Range

            Dim xText As String
            Dim iOperator As Integer
            Dim iSection As Integer
            Dim iPosition As Integer

            On Error GoTo ProcError

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                GetTextRange = Nothing
                Exit Function
            End If

            'get text value, the operator type, section
            xText = uRangeData.xDocObjValue
            iOperator = uRangeData.iDocObjOperator
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition

            'make sure operator is appropriate
            Select Case iOperator
                Case daRDOperator.ThatIs
                    'search for first instance of text
                    With oRange.Find
                        .ClearAllFuzzyOptions()
                        .ClearFormatting()
                        .Text = xText
                        '.MatchCase = True
                        If iOrder = daOrder.daOrder_First Then
                            .Forward = True
                        Else
                            .Forward = False
                        End If
                        .Execute()
                        If .Found Then
                            Select Case iPosition
                                Case daRDPosition.Starting
                                    oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                Case daRDPosition.Ending
                                    oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                            End Select
                            GetTextRange = oRange
                        Else
                            GetTextRange = Nothing
                        End If
                    End With
                Case Else
                    GetTextRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetTextRange")
            Exit Function
        End Function
        Private Function GetStyleRange(uRangeData As RangeDef, _
                                        oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim xStyleName As String
            Dim xText As String
            Dim iSection As Integer
            Dim iPosition As Integer

            On Error GoTo ProcError

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                GetStyleRange = Nothing
                Exit Function
            End If

            'get style name, the operator type and section
            iOperator = uRangeData.iDocObjOperator
            xStyleName = uRangeData.xDocObjName
            'xText = uRangeData.xDocObjValue
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition

            'set range according to operator
            Select Case iOperator
                Case daRDOperator.WhoseNameIs
                    If GlobalMethods.StyleExists(m_oWordDocument, xStyleName) = False Then
                        GetStyleRange = Nothing
                        Exit Function
                    End If
                    'search for first instance of style
                    With oRange.Find
                        .ClearAllFuzzyOptions()
                        .ClearFormatting()
                        .Wrap = WdFindWrap.wdFindContinue
                        .Style = m_oWordDocument.Styles(xStyleName)
                        '.Text = "" 'xText
                        '.MatchCase = True
                        .Execute()
                        If .Found Then

                            Select Case iPosition
                                Case daRDPosition.Starting
                                    oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                Case daRDPosition.Ending
                                    oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                            End Select
                            GetStyleRange = oRange
                        Else
                            GetStyleRange = Nothing
                        End If
                    End With
                Case Else
                    GetStyleRange = Nothing
            End Select



            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetStyleRange")
            Exit Function
        End Function
        Private Function GetParagraphRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim xStyleName As String
            Dim xText As String
            Dim iSection As Integer
            Dim iPosition As Integer
            Dim oParagraph As Word.Paragraph

            On Error GoTo ProcError
            GetParagraphRange = Nothing

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                Exit Function
            End If

            'get style name, the operator type and section
            iOperator = uRangeData.iDocObjOperator
            xStyleName = uRangeData.xDocObjName
            xText = uRangeData.xDocObjValue
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition

            'set range according to operator
            Select Case iOperator
                Case daRDOperator.ThatContainsTheText
                    xText = uRangeData.xDocObjValue
                    'Search for 1st para that contains the text
                    For Each oParagraph In oRange.Paragraphs
                        With oParagraph.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            '.Text = xText
                            '.MatchCase = True
                            .Execute()
                            If .Found Then
                                oRange = oParagraph.Range
                                Select Case iPosition
                                    Case daRDPosition.Starting
                                        oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                    Case daRDPosition.Ending
                                        oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                End Select
                                GetParagraphRange = oRange
                                Exit For
                            End If
                        End With
                    Next
                Case daRDOperator.ThatContainsTheStyle
                    xStyleName = uRangeData.xDocObjName
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text
                    For Each oParagraph In oRange.Paragraphs
                        With oParagraph.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Style = m_oWordDocument.Styles(xStyleName)
                            '.Text = xText
                            '.MatchCase = True
                            .Execute()
                            If .Found Then
                                oRange = oParagraph.Range
                                Select Case iPosition
                                    Case daRDPosition.Starting
                                        oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                    Case daRDPosition.Ending
                                        oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                End Select
                                GetParagraphRange = oRange
                                Exit For
                            End If
                        End With
                    Next
                Case daRDOperator.WhoseIndexIs
                    Dim iIndex As Integer
                    iIndex = Int(uRangeData.xDocObjValue)
                    If iIndex <= oRange.Paragraphs.Count Then
                        oRange = oRange.Paragraphs(iIndex).Range
                        Select Case iPosition
                            Case daRDPosition.Starting
                                oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                            Case daRDPosition.Ending
                                oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                        End Select
                        GetParagraphRange = oRange
                    End If
                Case Else
                    GetParagraphRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetParagraphRange")
            Exit Function
        End Function

        Private Function GetTableRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim iSection As Integer
            Dim oTable As Word.Table
            Dim bFoundText As Boolean
            Dim iPosition As Integer
            Dim xText As String

            On Error GoTo ProcError
            GetTableRange = Nothing

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                Exit Function
            End If

            'get the operator type and section
            iOperator = uRangeData.iDocObjOperator
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition

            Select Case iOperator
                Case daRDOperator.ThatContainsTheText
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text

                    For Each oTable In oRange.Tables
                        With oTable.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Text = xText
                            '.MatchCase = True
                            .Execute()
                            If .Found Then
                                oRange = oTable.Range
                                Select Case iPosition
                                    Case daRDPosition.Starting
                                        oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                    Case daRDPosition.Ending
                                        oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                End Select
                                GetTableRange = oRange
                                Exit For
                            End If
                        End With
                    Next
                Case daRDOperator.ThatContainsTheStyle
                    Dim xStyleName As String
                    xStyleName = uRangeData.xDocObjName
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text
                    For Each oTable In oRange.Tables
                        With oTable.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Style = m_oWordDocument.Styles(xStyleName)
                            '.Text = xText
                            '.MatchCase = True
                            .Execute()
                            If .Found Then
                                oRange = oTable.Range
                                Select Case iPosition
                                    Case daRDPosition.Starting
                                        oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                    Case daRDPosition.Ending
                                        oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                End Select
                                GetTableRange = oRange
                                Exit For
                            End If
                        End With
                    Next

                Case daRDOperator.WhoseIndexIs
                    Dim iIndex As Integer
                    iIndex = Int(uRangeData.xDocObjValue)
                    If iIndex <= oRange.Tables.Count Then
                        oRange = oRange.Tables(iIndex).Range
                        Select Case iPosition
                            Case daRDPosition.Starting
                                oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                            Case daRDPosition.Ending
                                oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                        End Select
                        GetTableRange = oRange
                    Else
                        GetTableRange = Nothing
                    End If
                Case Else
                    GetTableRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetTableRange")
            Exit Function
        End Function
        Private Function GetRowRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim iSection As Integer
            Dim oTable As Word.Table
            Dim bFoundText As Boolean
            Dim iPosition As Integer
            Dim oRow As Word.Row
            Dim lStartRange As Long
            Dim xText As String

            On Error GoTo ProcError
            GetRowRange = Nothing

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                Exit Function
            End If

            'get the operator type and section
            iOperator = uRangeData.iDocObjOperator
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition
            lStartRange = oRange.Start

            Select Case iOperator
                Case daRDOperator.ThatContainsTheText
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text
                    For Each oTable In oRange.Tables
                        'beginning of 1st table in range might
                        'be before start of range.
                        'have to make sure found cell is
                        'within range.
                        For Each oRow In oTable.Range.Rows
                            If oRow.Range.Start >= lStartRange Then
                                With oRow.Range.Find
                                    .ClearAllFuzzyOptions()
                                    .ClearFormatting()
                                    .Text = xText
                                    '.MatchCase = True
                                    .Execute()
                                    If .Found Then
                                        oRange = oRow.Range
                                        Select Case iPosition
                                            Case daRDPosition.Starting
                                                oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                            Case daRDPosition.Ending
                                                oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                        End Select
                                        GetRowRange = oRange
                                        Exit Function
                                    End If
                                End With
                            End If
                        Next
                    Next
                Case daRDOperator.ThatContainsTheStyle
                    Dim xStyleName As String
                    xStyleName = uRangeData.xDocObjName
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text
                    For Each oTable In oRange.Tables
                        For Each oRow In oTable.Range.Rows
                            With oRow.Range.Find
                                .ClearAllFuzzyOptions()
                                .ClearFormatting()
                                .Style = m_oWordDocument.Styles(xStyleName)
                                '.Text = xText
                                '.MatchCase = True
                                .Execute()
                                If .Found Then
                                    oRange.Select()
                                    oRange = oRow.Range
                                    Select Case iPosition
                                        Case daRDPosition.Starting
                                            oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                        Case daRDPosition.Ending
                                            oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                    End Select
                                    GetRowRange = oRange
                                    Exit Function
                                End If
                            End With
                        Next
                    Next

                Case Else
                    GetRowRange = Nothing
            End Select

            'GetRowRange.Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetRowRange")
            Exit Function
        End Function

        Private Function GetCellRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim iSection As Integer
            Dim oTable As Word.Table
            Dim bFoundText As Boolean
            Dim iPosition As Integer
            Dim oCell As Word.Cell
            Dim lStartRange As Long
            Dim xText As String

            On Error GoTo ProcError
            GetCellRange = Nothing

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                Exit Function
            End If

            'get the operator type and section
            iOperator = uRangeData.iDocObjOperator
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition
            lStartRange = oRange.Start

            Select Case iOperator
                Case daRDOperator.ThatContainsTheText
                    xText = uRangeData.xDocObjValue
                    'Search for 1st table that contains the text
                    For Each oTable In oRange.Tables
                        For Each oCell In oTable.Range.Cells
                            'beginning of 1st table in range might
                            'be before start of range.
                            'have to make sure found cell is
                            'within range.
                            If oCell.Range.Start >= lStartRange Then
                                With oCell.Range.Find
                                    .ClearAllFuzzyOptions()
                                    .ClearFormatting()
                                    .Text = xText
                                    '.MatchCase = True
                                    .Execute()
                                    If .Found Then
                                        oRange = oCell.Range
                                        Select Case iPosition
                                            Case daRDPosition.Starting
                                                oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                            Case daRDPosition.Ending
                                                oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                        End Select
                                        GetCellRange = oRange
                                        Exit Function
                                    End If
                                End With
                            End If
                        Next
                    Next
                Case daRDOperator.ThatContainsTheStyle
                    Dim xStyleName As String
                    xStyleName = uRangeData.xDocObjName
                    If GlobalMethods.StyleExists(m_oWordDocument, xStyleName) Then
                        xText = uRangeData.xDocObjValue
                        'Search for 1st table that contains the text
                        For Each oTable In oRange.Tables
                            For Each oCell In oTable.Range.Cells
                                'beginning of 1st table in range might
                                'be before start of range.
                                'have to make sure found cell is
                                'within range.
                                If oCell.Range.Start >= lStartRange Then
                                    With oCell.Range.Find
                                        .ClearAllFuzzyOptions()
                                        .ClearFormatting()
                                        .Style = m_oWordDocument.Styles(xStyleName)
                                        '.Text = xText
                                        '.MatchCase = True
                                        .Execute()
                                        If .Found Then
                                            oCell.Select()
                                            oRange.Select()
                                            oRange = oCell.Range
                                            Select Case iPosition
                                                Case daRDPosition.Starting
                                                    oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                                Case daRDPosition.Ending
                                                    oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                            End Select
                                            GetCellRange = oRange
                                            Exit Function
                                        End If
                                    End With
                                End If
                            Next
                        Next
                    End If
                Case Else
                    GetCellRange = Nothing
            End Select

            'GetCellRange.Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetCellRange")
            Exit Function
        End Function
        Private Function GetKeywordRange(uRangeData As RangeDef, _
                                        ByVal oRange As Word.Range) _
                                        As Word.Range

            Dim iOperator As Integer
            Dim xKeyWordListName As String
            'Dim iKeywordListID As Integer
            Dim vKeywordList() As Object
            Dim iSection As Integer
            Dim i As Integer
            Dim vKeyword As Object
            Dim iPosition As Integer
            Dim bGotList As Boolean

            On Error GoTo ProcError
            GetKeywordRange = Nothing

            'need range for this, make sure oRange is valid
            If oRange Is Nothing Then
                Exit Function
            End If

            'get the operator type and section
            iOperator = uRangeData.iDocObjOperator
            iSection = uRangeData.iSection
            iPosition = uRangeData.iPosition '
            xKeyWordListName = uRangeData.xDocObjName

            Select Case iOperator
                Case daRDOperator.WhoseNameIs
                    'populate an array of keyword.  xKeywordName is name of keyword list.
                    vKeywordList = GlobalMethods.GetKeywordList(xKeyWordListName, bGotList)
                    If bGotList = True Then
                        For Each vKeyword In vKeywordList
                            With oRange.Find
                                .ClearAllFuzzyOptions()
                                .ClearFormatting()
                                .Text = vKeyword
                                .Execute()
                                If .Found Then
                                    Select Case iPosition
                                        Case daRDPosition.Starting
                                            oRange.Collapse(WdCollapseDirection.wdCollapseStart)
                                        Case daRDPosition.Ending
                                            oRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                                    End Select
                                    GetKeywordRange = oRange
                                    Exit For
                                End If
                            End With
                        Next
                    End If
                Case Else
                    GetKeywordRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetKeywordRange")
            Exit Function
        End Function

        Private Function GetElementRange(uRangeData As RangeDef) _
                                        As Word.Range

            'Choices:
            '1. get element from collection using name as Key - name might not be unique.
            '2. get element from collection using id as key
            '3. send name to CDefinitions to get value from database. - why? already in collection.

            'Dim oElement As CElement
            Dim oElement
            Dim xID As String
            Dim iOperator As Integer
            Dim xName As String
            Dim iPosition As Integer
            Dim oRange As Word.Range

            On Error GoTo ProcError

            GetElementRange = Nothing

            xID = uRangeData.xDocObjValue
            iOperator = uRangeData.iDocObjOperator
            iPosition = uRangeData.iPosition
            xName = uRangeData.xDocObjName

            Select Case uRangeData.iDocObjOperator
                Case daRDOperator.WhoseIndexIs
                    oElement = GlobalMethods.g_oElements.Item(xID)
                    If oElement.EndRange <= m_oWordDocument.Range.End Then
                        Select Case iPosition
                            Case daRDPosition.Starting
                                oRange = m_oWordDocument.Range(Start:=oElement.StartRange)
                            Case daRDPosition.Ending
                                oRange = m_oWordDocument.Range(Start:=oElement.EndRange)
                            Case daRDPosition.Whole
                                oRange = m_oWordDocument.Range(Start:=oElement.StartRange, _
                                End:=oElement.EndRange)
                            Case Else
                                oRange = Nothing
                        End Select
                        GetElementRange = oRange
                    Else
                        GetElementRange = Nothing
                    End If
                Case daRDOperator.WhoseNameIs
                    For Each oElement In GlobalMethods.g_oElements
                        If oElement.Name = xName Then
                            If oElement.EndRange <= m_oWordDocument.Range.End Then
                                Select Case iPosition
                                    Case daRDPosition.Starting
                                        oRange = m_oWordDocument.Range(Start:=oElement.StartRange)
                                    Case daRDPosition.Ending
                                        oRange = m_oWordDocument.Range(Start:=oElement.EndRange)
                                    Case daRDPosition.Whole
                                        oRange = m_oWordDocument.Range(Start:=oElement.StartRange, _
                                        End:=oElement.EndRange)
                                    Case Else
                                        oRange = Nothing
                                End Select
                                GetElementRange = oRange
                                Exit For
                            End If
                        End If
                    Next
                Case Else
                    GetElementRange = Nothing
            End Select

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetElementRange")
            Exit Function
        End Function


        Public Function GetMatch(ByRef xMatch As String, ByRef oMatchRange As Word.Range) As Boolean
            Dim oRange As Word.Range
            Dim bFoundMatch As Boolean
            Dim xObjName As String
            Dim iObjType As Integer
            Dim xObjValue As String
            Dim iObjId As Integer
            Dim bGotKeywordList As Boolean
            Dim oTable As Word.Table
            Dim oCell As Word.Cell
            Dim oParagraph As Word.Paragraph
            Dim oArrayRange As Word.Range
            Dim iCount As Integer
            Dim iStart As Integer
            Dim iEnd As Integer
            Dim iLastStart As Integer

            iCount = 0
            iLastStart = -1
            oArrayRange = Nothing

            On Error GoTo ProcError

            xObjName = m_uMatchCriteria.xDocObjName
            iObjType = m_uMatchCriteria.iDocObjType
            xObjValue = m_uMatchCriteria.xDocObjValue

            If oMatchRange Is Nothing Then
                Select Case iObjType
                    Case daMatchCriteria.DocVar, daMatchCriteria.DocProp
                        'don't need a range, proceed with function
                    Case Else
                        'need a range and don't have one.
                        oMatchRange = GetMatchRange()

                        If oMatchRange Is Nothing Then
                            GetMatch = False
                            Exit Function
                        Else
                            oArrayRange = GetRangeCopy(oMatchRange)
                        End If
                End Select
            Else
                oArrayRange = GetRangeCopy(oMatchRange)
            End If


            'search for match within range
            Select Case iObjType

                Case daMatchCriteria.None
                    'get text of current range
                    xMatch = oMatchRange.Text
                    bFoundMatch = True

                Case daMatchCriteria.Bookmark
                    If oMatchRange.Bookmarks.Exists(xObjName) Then
                        'get bookmark text and range.
                        oMatchRange = oMatchRange.Bookmarks(xObjName).Range
                        xMatch = oMatchRange.Text
                        bFoundMatch = True
                    Else
                        bFoundMatch = False
                    End If

                Case daMatchCriteria.DocVar
                    'MsgBox "daMatchCriteria_DocVar"
                    'If m_oWordDocument.Variables(xObjName).Value <> "" Then
                    If GlobalMethods.DocVarExists(m_oWordDocument, xObjName) Then
                        'get DocVar text
                        xMatch = m_oWordDocument.Variables(xObjName).Value
                        oMatchRange = Nothing
                        bFoundMatch = True
                    Else
                        bFoundMatch = False
                    End If

                Case daMatchCriteria.DocProp
                    'return DocProp value.
                    'NOTE: can't declare oProp as
                    'DocumentProperty, get a compile error.
                    Dim oProp As Object
                    For Each oProp In m_oWordDocument.BuiltInDocumentProperties
                        If oProp.Name = xObjName Then
                            xMatch = oProp.Value
                            bFoundMatch = True
                            Exit For
                        End If
                    Next
                    If bFoundMatch = False Then
                        For Each oProp In m_oWordDocument.CustomDocumentProperties
                            If oProp.Name = xObjName Then
                                xMatch = Str(oProp.Value)
                                bFoundMatch = True
                                Exit For
                            End If
                        Next
                    End If

                Case daMatchCriteria.FirstInstanceOfStyle, daMatchCriteria.LastInstanceOfStyle, daMatchCriteria.AllInstancesOfStyle
                    With oMatchRange.Find
                        If GlobalMethods.StyleExists(m_oWordDocument, xObjName) Then
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Wrap = WdFindWrap.wdFindContinue
                            .Style = m_oWordDocument.Styles(xObjName)
                            '.Text = xObjValue
                            .Execute()
                            If .Found Then
                                xMatch = oMatchRange.Text
                                bFoundMatch = True
                            Else
                                bFoundMatch = False
                            End If
                        Else
                            bFoundMatch = False
                        End If
                    End With

                    If bFoundMatch Then
                        Dim oPara As Word.Paragraph
                        For Each oPara In oArrayRange.Paragraphs
                            If oPara.Style = m_oWordDocument.Styles(xObjName) Then
                                'add to range array
                                ReDim Preserve m_oRanges(iCount)
                                m_oRanges(iCount) = GetRangeCopy(oMatchRange)
                                m_oRanges(iCount).SetRange(Start:=oPara.Range.Start, End:=oPara.Range.End)
                                If iObjType = daMatchCriteria.LastInstanceOfStyle Then
                                    xMatch = xMatch & m_oRanges(iCount).Text
                                End If
                                If iObjType = daMatchCriteria.AllInstancesOfStyle Then
                                    If iCount > 0 Then
                                        xMatch = xMatch & m_oRanges(iCount).Text
                                    End If
                                End If

                                iCount = iCount + 1
                            End If
                        Next
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) And iObjType <> daMatchCriteria.AllInstancesOfStyle Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                    End If


                Case daMatchCriteria.FirstInstanceOfText
                    With oMatchRange.Find
                        .ClearAllFuzzyOptions()
                        .ClearFormatting()
                        .Text = xObjValue
                        .Execute()
                        If .Found Then
                            xMatch = oMatchRange.Text
                            bFoundMatch = True
                        Else
                            bFoundMatch = False
                        End If
                    End With
                Case daMatchCriteria.LastInstanceOfText
                    With oMatchRange.Find
                        .ClearAllFuzzyOptions()
                        .ClearFormatting()
                        .Forward = False
                        .Text = xObjValue
                        .Execute()
                        If .Found Then
                            xMatch = oMatchRange.Text
                            bFoundMatch = True
                        Else
                            bFoundMatch = False
                        End If
                    End With

                Case daMatchCriteria.ParagraphContainingText
                    'Search for 1st para that contains the text
                    For Each oParagraph In oMatchRange.Paragraphs
                        With oParagraph.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Text = xObjValue
                            .Execute()
                            If .Found Then
                                xMatch = oParagraph.Range.Text
                                oMatchRange = oParagraph.Range
                                bFoundMatch = True
                                Exit For
                            End If
                        End With
                    Next

                Case daMatchCriteria.ParagraphWhoseIndexIs
                    Dim iObjVal As Integer
                    iObjVal = Int(Trim(xObjValue))
                    If iObjVal <= oMatchRange.Paragraphs.Count Then
                        oParagraph = oMatchRange.Paragraphs(iObjVal)
                        oMatchRange = oParagraph.Range
                        xMatch = oMatchRange.Text
                        bFoundMatch = True
                    End If

                Case daMatchCriteria.TableContainingText
                    'Search for 1st table that contains the text

                    For Each oTable In oMatchRange.Tables
                        With oTable.Range.Find
                            .ClearAllFuzzyOptions()
                            .ClearFormatting()
                            .Text = xObjValue
                            .Execute()
                            If .Found Then
                                xMatch = oTable.Range.Text
                                oMatchRange = oTable.Range
                                bFoundMatch = True
                                Exit For
                            End If
                        End With
                    Next

                Case daMatchCriteria.TableContainingStyle
                    'Search for 1st table that contains the style
                    If GlobalMethods.StyleExists(m_oWordDocument, xObjName) Then
                        For Each oTable In oMatchRange.Tables
                            With oTable.Range.Find
                                .ClearAllFuzzyOptions()
                                .ClearFormatting()
                                '.Text = xObjValue
                                .Style = m_oWordDocument.Styles(xObjName)
                                .Execute()
                                If .Found Then
                                    xMatch = oTable.Range.Text
                                    oMatchRange = oTable.Range
                                    bFoundMatch = True
                                    Exit For
                                End If
                            End With
                        Next
                    Else
                        bFoundMatch = False
                    End If

                Case daMatchCriteria.CellContainingText
                    'Search for 1st table that contains the text
                    For Each oTable In oMatchRange.Tables
                        For Each oCell In oTable.Range.Cells
                            oCell.Select()
                            With oCell.Range.Find
                                .ClearAllFuzzyOptions()
                                .ClearFormatting()
                                .Text = xObjValue
                                .Execute()
                                If .Found Then
                                    oCell.Range.Select()
                                    xMatch = oCell.Range.Text
                                    oMatchRange = oCell.Range
                                    GetMatch = True
                                    Exit Function
                                End If
                            End With
                        Next
                    Next


                Case daMatchCriteria.CellContainingStyle
                    If GlobalMethods.StyleExists(m_oWordDocument, xObjName) Then
                        If oMatchRange.Tables.Count > 0 Then
                            For Each oTable In oMatchRange.Tables
                                For Each oCell In oTable.Range.Cells
                                    With oCell.Range.Find
                                        .ClearAllFuzzyOptions()
                                        .ClearFormatting()
                                        '.Text = xObjValue
                                        .Style = m_oWordDocument.Styles(xObjName)
                                        .Execute()
                                        If .Found Then
                                            oCell.Range.Select()
                                            oMatchRange.Select()
                                            xMatch = oCell.Range.Text
                                            oMatchRange = oCell.Range
                                            GetMatch = True
                                            Exit Function
                                        End If
                                    End With
                                Next
                            Next
                        End If
                    Else
                        bFoundMatch = False
                    End If


                Case daMatchCriteria.Keyword
                    '            Dim vKeywordList() As Variant
                    '            Dim vKeyword As Variant
                    '            'iObjId = CInt(xObjValue)
                    '            vKeywordList() = g_oDefinition.GetKeywordList(xObjName, bGotKeywordList)
                    '            If bGotKeywordList Then
                    '                For Each vKeyword In vKeywordList
                    '                    With oMatchRange.Find
                    '                        .ClearAllFuzzyOptions
                    '                        .ClearFormatting
                    '                        .Text = vKeyword
                    '                        .Execute
                    '                        If .Found Then
                    '                            xMatch = oRange.Text
                    '                            bFoundMatch = True
                    '                            Exit For
                    '                        End If
                    '                    End With
                    '                Next
                    '            Else
                    '                bFoundMatch = False
                    '            End If
                Case daMatchCriteria.Selection
                    Dim xColText As String
                    Dim i, j As Integer
                    xColText = ""
                    With GlobalMethods.CurWordApp.Selection
                        On Error Resume Next
                        If .Tables.Count Then
                            If .Type = WdSelectionType.wdSelectionColumn Then
                                For i = .Information(Word.WdInformation.wdStartOfRangeRowNumber) To _
                                    .Information(Word.WdInformation.wdEndOfRangeRowNumber)
                                    For j = .Information(Word.WdInformation.wdStartOfRangeColumnNumber) To _
                                        .Information(Word.WdInformation.wdEndOfRangeColumnNumber)
                                        xColText = xColText & _
                                            .Tables(1).Cell(i, j).Range.Text
                                    Next j
                                Next i

                            End If
                        End If
                        On Error GoTo ProcError
                    End With

                    If xColText = "" Then
                        xMatch = Me.Range.Text
                    Else
                        xMatch = xColText
                    End If

                    bFoundMatch = True
                Case Else

            End Select

            GetMatch = bFoundMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetMatch")
            Exit Function

        End Function

        Private Function GetReturnValue(ByRef xMatch As String, _
                                     oMatchRange As Word.Range) _
                                    As Boolean

            Dim bGotReturnValue As Boolean

            On Error GoTo ProcError

            Select Case m_uReturnData.iType
                Case daReturnDataType.TheValue
                    bGotReturnValue = True
                Case daReturnDataType.None
                    'Dan, give me an example
                    xMatch = ""
                    oMatchRange = Nothing
                    bGotReturnValue = True
                Case daReturnDataType.IsTrue
                    xMatch = "True"
                    bGotReturnValue = True
                Case daReturnDataType.IsFalse
                    xMatch = "False"
                    bGotReturnValue = True
                Case daReturnDataType.FromStartOfXToMatchedText
                    bGotReturnValue = _
                        GetFromStartOfXToMatchedText(xMatch, oMatchRange)
                Case daReturnDataType.FromMatchedTextToEndOfX
                    bGotReturnValue = _
                       GetFromMatchedTextToEndOfX(xMatch, oMatchRange)
                Case daReturnDataType.FromEndOfMatchedTextToEndOfX
                    bGotReturnValue = _
                        GetFromEndOfMatchedTextToEndOfX(xMatch, oMatchRange)
                Case daReturnDataType.NextX
                    bGotReturnValue = _
                       GetNextX(xMatch, oMatchRange)
                Case daReturnDataType.PreviousX
                    bGotReturnValue = _
                       GetPreviousX(xMatch, oMatchRange)
                Case daReturnDataType.CurrentX
                    bGotReturnValue = _
                       GetCurrentX(xMatch, oMatchRange)
                Case daReturnDataType.MappedValue
                    bGotReturnValue = GetMappedValue(xMatch, oMatchRange)
            End Select

            GetReturnValue = bGotReturnValue

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetReturnValue")
            Exit Function

        End Function

        Private Function GetFromStartOfXToMatchedText(ByRef xMatch As String, _
                                            ByVal oMatchRange As Word.Range) _
                                            As Boolean

            Dim bGotMatch As Boolean
            Dim lEndPos As Long
            Dim lStartPos As Long
            Dim oSearchRange As Word.Range
            Dim oTable As Word.Table
            Dim oRow As Word.Row

            On Error GoTo ProcError

            lEndPos = oMatchRange.Start

            Select Case m_uReturnData.iUnit
                Case daReturnDataUnit.Character
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)
                    If oMatchRange.Move(Word.WdUnits.wdCharacter, Count:=-1) <> 0 Then
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Word
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)
                    If oMatchRange.Move(Word.WdUnits.wdWord, Count:=-1) <> 0 Then
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    oMatchRange.Select()
                    If GlobalMethods.CurWordApp.Selection.Move(Word.WdUnits.wdLine, Count:=-1) <> 0 Then
                        oMatchRange = GlobalMethods.CurWordApp.Selection.Range
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Sentence
                    'oMatchRange.Collapse Direction:=wdCollapseStart
                    If oMatchRange.Move(Word.WdUnits.wdSentence, Count:=-1) <> 0 Then
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Paragraph
                    'oMatchRange.Collapse Direction:=wdCollapseStart
                    If oMatchRange.Move(Word.WdUnits.wdParagraph, Count:=-1) <> 0 Then
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Table
                    If oMatchRange.Move(Word.WdUnits.wdTable, Count:=-1) <> 0 Then
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Cell
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        oMatchRange.Move(Word.WdUnits.wdCell, Count:=-1)
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        oSearchRange = m_oWordDocument.Range
                        oSearchRange.End = oMatchRange.Start
                        oTable = oSearchRange.Tables(oSearchRange.Tables.Count)
                        oRow = oTable.Rows(oTable.Rows.Count)
                        If Not (oRow Is Nothing) Then
                            oMatchRange.Start = oRow.Range.Start
                            oMatchRange.End = lEndPos
                            xMatch = oMatchRange.Text
                            bGotMatch = True
                        Else
                            bGotMatch = False
                        End If
                    End If
                Case daReturnDataUnit.Row
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        oMatchRange.Move(Word.WdUnits.wdRow, Count:=-1)
                        oMatchRange.End = lEndPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        oSearchRange = m_oWordDocument.Range
                        oSearchRange.End = oMatchRange.Start
                        oTable = oSearchRange.Tables(oSearchRange.Tables.Count)
                        oRow = oTable.Rows(oTable.Rows.Count)
                        If Not (oRow Is Nothing) Then
                            oMatchRange.Start = oRow.Range.Start
                            oMatchRange.End = lEndPos
                            xMatch = oMatchRange.Text
                            bGotMatch = True
                        Else
                            bGotMatch = False
                        End If
                    End If
            End Select

            'oMatchRange.Select 'DEBUG
            GetFromStartOfXToMatchedText = bGotMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetFromStartOfXToMatchedText")
            Exit Function

        End Function
        Private Function GetFromMatchedTextToEndOfX(ByRef xMatch As String, _
                                            ByVal oMatchRange As Word.Range) _
                                            As Boolean

            Dim bGotMatch As Boolean
            Dim lEndPos As Long
            Dim lStartPos As Long
            Dim lMaxPos As Long
            Dim oSearchRange As Word.Range
            Dim oTable As Word.Table
            Dim oRow As Word.Row
            Dim lLen As Long
            Dim i As Long
            Dim xChar As String

            On Error GoTo ProcError

            lStartPos = oMatchRange.Start
            lEndPos = oMatchRange.End

            Select Case m_uReturnData.iUnit
                Case daReturnDataUnit.Character
                    'nonsensical, return false, with original range?
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                    If oMatchRange.Move(Word.WdUnits.wdCharacter, Count:=1) <> 0 Then
                        'If oMatchRange.Characters.Count
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Word
                    'goes to end of word.
                    If oMatchRange.Expand(Word.WdUnits.wdWord) > 0 Then
                        lEndPos = oMatchRange.End
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    'goes to end of line.
                    oMatchRange.Select()
                    If GlobalMethods.CurWordApp.Selection.Expand(Word.WdUnits.wdLine) > 0 Then
                        lEndPos = GlobalMethods.CurWordApp.Selection.Range.End
                        If GlobalMethods.CurWordApp.Selection.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If GlobalMethods.CurWordApp.Selection.Range.Cells.Count = 1 Then
                                lMaxPos = GlobalMethods.CurWordApp.Selection.Range.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Sentence
                    'goes to end of current sentence.
                    'If oMatchRange.Sentences.Count <= 1 Then
                    If oMatchRange.Expand(Word.WdUnits.wdSentence) > 0 Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If oMatchRange.End = lMaxPos Then
                                    oMatchRange.End = lMaxPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Paragraph
                    If oMatchRange.Expand(Word.WdUnits.wdParagraph) > 0 Then
                        lEndPos = oMatchRange.End
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Table
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        oMatchRange.End = oMatchRange.Tables(1).Range.End
                        xMatch = oMatchRange.Text
                        '            Else
                        '                Set oSearchRange = m_oWordDocument.Range
                        '                oSearchRange.Start = oMatchRange.End
                        '                Set oTable = oSearchRange.Tables(1)
                        '                If Not (oTable Is Nothing) Then
                        '                    oMatchRange.End = oTable.Range.End
                        '                    xMatch = oMatchRange.Text
                        '                    bGotMatch = True
                        '                Else
                        '                    bGotMatch = False
                        '                End If
                    End If
                Case daReturnDataUnit.Cell
                    'If oMatchRange.Cells.Count <= 1 Then
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        'end of cell minus one, so end of cell marker
                        'not in range.
                        oMatchRange.End = oMatchRange.Cells(1).Range.End - 1
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        '                    Set oSearchRange = m_oWordDocument.Range
                        '                    oSearchRange.Start = oMatchRange.Start
                        '                    Set oTable = oSearchRange.Tables(1)
                        '                    Set oRow = oTable.Rows(1)
                        '                    If Not (oRow Is Nothing) Then
                        '                        oMatchRange.End = oRow.Range.End
                        '                        oMatchRange.Start = lStartPos
                        '                        xMatch = oMatchRange.Text
                        '                        bGotMatch = True
                        '                    Else
                        bGotMatch = False
                        '                    End If
                    End If
                    'End If
                Case daReturnDataUnit.Row
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        'end of cell minus one, so end of cell marker
                        'not in range.
                        oMatchRange.End = oMatchRange.Rows(1).Range.End - 1
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        'this only makes sense if range is already in table.
                        '                Set oSearchRange = m_oWordDocument.Range
                        '                oSearchRange.Start = oMatchRange.Start
                        '                Set oTable = oSearchRange.Tables(1)
                        '                Set oRow = oTable.Rows(1)
                        '                If Not (oRow Is Nothing) Then
                        '                    oMatchRange.End = oRow.Range.End
                        '                    oMatchRange.Start = lStartPos
                        '                    xMatch = oMatchRange.Text
                        '                    bGotMatch = True
                        '                Else
                        bGotMatch = False
                        '                End If
                    End If
            End Select

            'oMatchRange.Select 'DEBUG

            GetFromMatchedTextToEndOfX = bGotMatch
            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetFromMatchedTextToEndOfX")
            Exit Function
        End Function
        Private Function GetFromEndOfMatchedTextToEndOfX(ByRef xMatch As String, _
                                            ByVal oMatchRange As Word.Range) _
                                            As Boolean

            Dim bGotMatch As Boolean
            Dim lEndPos As Long
            Dim lStartPos As Long
            Dim lMaxPos As Long
            Dim oSearchRange As Word.Range
            Dim oTable As Word.Table
            Dim oRow As Word.Row
            Dim lLen As Long
            Dim i As Long
            Dim xChar As String

            On Error GoTo ProcError

            'lStartPos = oMatchRange.Start
            lStartPos = oMatchRange.End

            Select Case m_uReturnData.iUnit
                Case daReturnDataUnit.Character
                    'nonsensical, return false, with original range?
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                    If oMatchRange.Move(Word.WdUnits.wdCharacter, Count:=1) <> 0 Then
                        'If oMatchRange.Characters.Count
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                    bGotMatch = False
                Case daReturnDataUnit.Word
                    'goes to end of word.
                    If oMatchRange.Expand(Word.WdUnits.wdWord) > 0 Then
                        lEndPos = oMatchRange.End
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If

                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    'goes to end of line.
                    oMatchRange.Select()
                    If GlobalMethods.CurWordApp.Selection.Expand(Word.WdUnits.wdLine) > 0 Then
                        lEndPos = GlobalMethods.CurWordApp.Selection.Range.End
                        If GlobalMethods.CurWordApp.Selection.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If GlobalMethods.CurWordApp.Selection.Range.Cells.Count = 1 Then
                                lMaxPos = GlobalMethods.CurWordApp.Selection.Range.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Sentence
                    'goes to end of current sentence.
                    'If oMatchRange.Sentences.Count <= 1 Then
                    If oMatchRange.Expand(Word.WdUnits.wdSentence) > 0 Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If oMatchRange.End = lMaxPos Then
                                    oMatchRange.End = lMaxPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                    'End If
                Case daReturnDataUnit.Paragraph
                    If oMatchRange.Expand(Word.WdUnits.wdParagraph) > 0 Then
                        lEndPos = oMatchRange.End
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last sentence in the cell,
                            'expanding it will expand to the entire cell
                            'have to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If lEndPos = lMaxPos Then
                                    lEndPos = lEndPos - 1
                                End If
                            End If
                        End If
                        oMatchRange.End = lEndPos
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Table
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        oMatchRange.End = oMatchRange.Tables(1).Range.End
                        xMatch = oMatchRange.Text
                        '            Else
                        '                Set oSearchRange = m_oWordDocument.Range
                        '                oSearchRange.Start = oMatchRange.End
                        '                Set oTable = oSearchRange.Tables(1)
                        '                If Not (oTable Is Nothing) Then
                        '                    oMatchRange.End = oTable.Range.End
                        '                    xMatch = oMatchRange.Text
                        '                    bGotMatch = True
                        '                Else
                        '                    bGotMatch = False
                        '                End If
                    End If
                Case daReturnDataUnit.Cell
                    'If oMatchRange.Cells.Count <= 1 Then
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        'end of cell minus one, so end of cell marker
                        'not in range.
                        oMatchRange.End = oMatchRange.Cells(1).Range.End - 1
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        '                    Set oSearchRange = m_oWordDocument.Range
                        '                    oSearchRange.Start = oMatchRange.Start
                        '                    Set oTable = oSearchRange.Tables(1)
                        '                    Set oRow = oTable.Rows(1)
                        '                    If Not (oRow Is Nothing) Then
                        '                        oMatchRange.End = oRow.Range.End
                        '                        oMatchRange.Start = lStartPos
                        '                        xMatch = oMatchRange.Text
                        '                        bGotMatch = True
                        '                    Else
                        bGotMatch = False
                        '                    End If
                    End If
                    'End If
                Case daReturnDataUnit.Row
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        'end of cell minus one, so end of cell marker
                        'not in range.
                        oMatchRange.End = oMatchRange.Rows(1).Range.End - 1
                        oMatchRange.Start = lStartPos
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        'this only makes sense if range is already in table.
                        '                Set oSearchRange = m_oWordDocument.Range
                        '                oSearchRange.Start = oMatchRange.Start
                        '                Set oTable = oSearchRange.Tables(1)
                        '                Set oRow = oTable.Rows(1)
                        '                If Not (oRow Is Nothing) Then
                        '                    oMatchRange.End = oRow.Range.End
                        '                    oMatchRange.Start = lStartPos
                        '                    xMatch = oMatchRange.Text
                        '                    bGotMatch = True
                        '                Else
                        bGotMatch = False
                        '                End If
                    End If
            End Select

            'oMatchRange.Select 'DEBUG

            GetFromEndOfMatchedTextToEndOfX = bGotMatch
            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetFromMatchedTextToEndOfX")
            Exit Function
        End Function
        Private Function GetNextX(ByRef xMatch As String, _
                                    ByVal oMatchRange As Word.Range) _
                                    As Boolean

            Dim bGotMatch As Boolean
            Dim lLen As Long
            Dim i As Long
            Dim lEndPos As Long
            Dim lMaxPos As Long

            On Error GoTo ProcError

            Select Case m_uReturnData.iUnit

                Case daReturnDataUnit.Character
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdCharacter)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    Else
                        bGotMatch = False
                    End If
                Case daReturnDataUnit.Word
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                    oMatchRange.Select() 'DEBUG
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdWord)
                    oMatchRange.Select() 'DEBUG
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        lLen = Len(xMatch)
                        'make sure range moved to valid word.
                        For i = 1 To lLen
                            If GlobalMethods.IsAlphaNumeric(Mid(xMatch, i, 1)) Then
                                bGotMatch = True
                                Exit For
                            End If
                        Next
                        If bGotMatch = False Then
                            oMatchRange.Select()
                            bGotMatch = GetNextX(xMatch, oMatchRange)
                        End If
                    End If
                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    oMatchRange.Select()
                    oMatchRange = GlobalMethods.CurWordApp.Selection.Next(Word.WdUnits.wdLine)
                    If Not (oMatchRange Is Nothing) Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last line in cell,
                            'have to reselect to exclude end of cell marker.
                            If oMatchRange.Cells.Count = 1 Then
                                oMatchRange.End = oMatchRange.End - 1
                                '                        oMatchRange.End = (lMaxPos - 1)
                                oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                                oMatchRange.Select()
                                GlobalMethods.CurWordApp.Selection.HomeKey(Word.WdUnits.wdLine, WdMovementType.wdExtend)
                                oMatchRange = GlobalMethods.CurWordApp.Selection.Range
                            End If
                        End If
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Sentence
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdSentence)
                    oMatchRange.Select()
                    If Not (oMatchRange Is Nothing) Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) = True Then
                            If oMatchRange.Cells.Count = 1 Then
                                lEndPos = oMatchRange.End - 1
                                oMatchRange.Move(Word.WdUnits.wdSentence, Count:=-1)
                                oMatchRange.End = lEndPos
                            End If
                        End If
                        xMatch = oMatchRange.Text
                        lLen = Len(xMatch)
                        'make sure range moved to valid word.
                        For i = 1 To lLen
                            If GlobalMethods.IsAlphaNumeric(Mid(xMatch, i, 1)) Then
                                bGotMatch = True
                                Exit For
                            End If
                        Next
                        If bGotMatch = False Then
                            bGotMatch = GetNextX(xMatch, oMatchRange)
                        End If
                    End If
                Case daReturnDataUnit.Paragraph
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdParagraph)
                    If Not (oMatchRange Is Nothing) Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            If oMatchRange.Cells.Count = 1 Then
                                lEndPos = oMatchRange.End - 1
                                oMatchRange.Move(Word.WdUnits.wdParagraph, Count:=-1)
                                oMatchRange.End = lEndPos
                            End If
                        End If

                        xMatch = oMatchRange.Text
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Table
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdTable)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Cell
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdCell)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Row
                    oMatchRange = oMatchRange.Next(Word.WdUnits.wdRow)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
            End Select

            'oMatchRange.Select 'DEBUG
            GetNextX = bGotMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetNextX")
            Exit Function
        End Function
        Private Function GetPreviousX(ByRef xMatch As String, _
                                        ByVal oMatchRange As Word.Range) _
                                        As Boolean

            Dim bGotMatch As Boolean
            Dim lLen As Long
            Dim i As Long

            On Error GoTo ProcError

            Select Case m_uReturnData.iUnit

                Case daReturnDataUnit.Character
                    'don't know what action to perform here.  dan?
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdCharacter)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Word
                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseStart)
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdWord)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        lLen = Len(xMatch)
                        'make sure range moved to valid word.
                        For i = 1 To lLen
                            If GlobalMethods.IsAlphaNumeric(Mid(xMatch, i, 1)) Then
                                bGotMatch = True
                                Exit For
                            End If
                        Next
                        If bGotMatch = False Then
                            bGotMatch = GetPreviousX(xMatch, oMatchRange)
                        End If
                    End If
                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    oMatchRange.Select()
                    oMatchRange = GlobalMethods.CurWordApp.Selection.Previous(Word.WdUnits.wdLine)
                    If Not (oMatchRange Is Nothing) Then
                        'Set oMatchRange = Selection.Range
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Sentence
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdSentence)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Paragraph
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdParagraph)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Table
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdTable)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Cell
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdCell)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Row
                    oMatchRange = oMatchRange.Previous(Word.WdUnits.wdRow)
                    If Not (oMatchRange Is Nothing) Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
            End Select

            GetPreviousX = bGotMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetPreviousX")
            Exit Function
        End Function

        Private Function GetCurrentX(ByRef xMatch As String, _
                                    ByVal oMatchRange As Word.Range) _
                                    As Boolean

            Dim bGotMatch As Boolean
            Dim lMaxPos As Long
            Dim lEndPos As Long

            On Error GoTo ProcError

            Select Case m_uReturnData.iUnit
                Case daReturnDataUnit.Character
                    'not relevant
                    'don't know what action to perform here.  dan?
                    '            If oMatchRange.Expand(Unit:=wdCharacter) <> 0 Then
                    '                xMatch = oMatchRange.Text
                    '                bGotMatch = True
                    '            Else
                    '                bGotMatch = False
                    '            End If
                Case daReturnDataUnit.Word
                    If oMatchRange.Expand(Word.WdUnits.wdWord) <> 0 Then
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Line
                    'have to use Selection instead of range,
                    'or we'll get an error
                    'if this is last line in cell, whole cell is selected.
                    oMatchRange.Select()
                    If GlobalMethods.CurWordApp.Selection.Expand(Word.WdUnits.wdLine) <> 0 Then
                        If GlobalMethods.CurWordApp.Selection.Information(Word.WdInformation.wdWithInTable) Then
                            'if it's the last line in cell,
                            'have to reselect to exclude end of cell marker.
                            If GlobalMethods.CurWordApp.Selection.Cells.Count = 1 Then
                                lMaxPos = GlobalMethods.CurWordApp.Selection.Cells(1).Range.End
                                If GlobalMethods.CurWordApp.Selection.Range.End = lMaxPos Then
                                    lMaxPos = lMaxPos - 1
                                    oMatchRange.End = lMaxPos
                                    oMatchRange.Collapse(Direction:=WdCollapseDirection.wdCollapseEnd)
                                    oMatchRange.Select()
                                    GlobalMethods.CurWordApp.Selection.HomeKey(Word.WdUnits.wdLine, WdMovementType.wdExtend)
                                End If
                            End If
                        End If
                        oMatchRange = GlobalMethods.CurWordApp.Selection.Range
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Sentence
                    If oMatchRange.Expand(Word.WdUnits.wdSentence) <> 0 Then
                        If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                            If oMatchRange.Cells.Count = 1 Then
                                lMaxPos = oMatchRange.Cells(1).Range.End
                                If oMatchRange.End = lMaxPos Then
                                    lMaxPos = lMaxPos - 1
                                    oMatchRange.End = lMaxPos
                                    oMatchRange.Move(Word.WdUnits.wdSentence, Count:=-1)
                                    oMatchRange.End = lMaxPos
                                End If
                            End If
                        End If
                        xMatch = oMatchRange.Text
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Paragraph
                    If oMatchRange.Expand(Word.WdUnits.wdParagraph) <> 0 Then
                        xMatch = oMatchRange.Text
                        'trim ending paragraph mark, if it exists
                        If Right(xMatch, 1) = Chr(13) Then
                            xMatch = Left(xMatch, (Len(xMatch) - 1))
                        End If
                        bGotMatch = True
                    End If
                Case daReturnDataUnit.Table
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        If oMatchRange.Expand(Word.WdUnits.wdTable) <> 0 Then
                            xMatch = oMatchRange.Text
                            bGotMatch = True
                        End If
                    End If
                Case daReturnDataUnit.Cell
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        If oMatchRange.Expand(Word.WdUnits.wdCell) <> 0 Then
                            xMatch = oMatchRange.Text
                            bGotMatch = True
                        End If
                    End If
                Case daReturnDataUnit.Row
                    If oMatchRange.Information(Word.WdInformation.wdWithInTable) Then
                        If oMatchRange.Expand(Word.WdUnits.wdRow) <> 0 Then
                            xMatch = oMatchRange.Text
                            bGotMatch = True
                        End If
                    End If
            End Select

            GetCurrentX = bGotMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetCurrentX")
            Exit Function
        End Function
        Private Function GetMappedValue(ByRef xMatch As String, _
                                            ByVal oMatchRange As Word.Range) _
                                            As Boolean
            Dim bGotMatch As Boolean
            Dim oConn As ADODB.Connection
            Dim oRS As ADODB.Recordset
            Dim xQuery As String
            Dim xDataPath As String

            On Error GoTo ProcError

            'On Error GoTo Manual

            ''connect to database
            'oConn = New ADODB.Connection()

            ''get data directory - 
            'xDataPath = LMP.Registry.GetLocalMachineValue(
            '   GlobalMethods.REGISTRY_ROOT, "DataDirectory")

            ''set connection string
            'oConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & _
            'xDataPath & "daInstructionSets.mdb'; User Id=admin; Password="

            ''open connection
            'oConn.Open()

            ''set recordset
            'oRS = New ADODB.Recordset

            'xQuery = "SELECT fldValue " & _
            '    "FROM tblMappedValues WHERE fldName = '" & _
            '    xMatch & "';"

            'oRS.Open(xQuery, oConn)

            'If oRS.EOF Then
            '    oMatchRange = Nothing
            '    bGotMatch = True
            'Else
            '    xMatch = oRS("fldValue").ToString()
            '    oMatchRange = Nothing
            '    bGotMatch = True
            'End If

            ''clean up
            'oConn.Close()
            'GetMappedValue = bGotMatch
            'Exit Function
            'Manual:
            On Error GoTo ProcError
            'could not connect
            Select Case xMatch
                Case "0"
                    xMatch = "False"
                Case "1"
                    xMatch = "True"
                Case "-1"
                    xMatch = "True"
                Case "-Field-"
                    xMatch = "MMMM d, yyyy /F"
                Case "mmmm d, yyyy"
                    xMatch = "MMMM d, yyyy"
                Case Else
                    If InStr(xMatch, "0?") Then
                        xMatch = False
                    End If
                    If InStr(xMatch, "1?") Then
                        xMatch = True
                    End If
            End Select
            oMatchRange = Nothing
            bGotMatch = True

            oConn = Nothing
            oRS = Nothing

            GetMappedValue = bGotMatch

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.GetMappedValue")
            Exit Function
        End Function
        Private Function EvaluateEnvironmentVariables(xText As String) As String
            Dim iPos As Integer
            Dim iPos2 As Integer
            Dim xEnvVar As String

            'evaluate path for environment variables
            iPos = InStr(xText, "%")

            While (iPos > 0)
                iPos2 = InStr(iPos + 1, xText, "%")

                If iPos2 = 0 Then
                    'something's wrong
                    Err.Raise(-20019, , "Could not evaluate the string '" & xText & "'.")
                End If

                xEnvVar = Mid$(xText, iPos + 1, iPos2 - iPos - 1)

                'evaluate
                xEnvVar = Environ(xEnvVar)

                xText = Left$(xText, iPos - 1) + xEnvVar + Mid$(xText, iPos2 + 1)
                iPos = InStr(iPos2 + 1, xText, "%")
            End While

            EvaluateEnvironmentVariables = xText

            Exit Function
ProcError:
            GlobalMethods.RaiseError("CInstruction.EvaluateEnvironmentVariables")
        End Function
        Private Function GetRangeCopy(oRange As Word.Range) As Word.Range

            Dim oRangeCopy As Word.Range

            GetRangeCopy = Nothing
            oRangeCopy = Nothing

            If oRange Is Nothing Then
                Exit Function
            End If

            Select Case m_iStory
                Case daStory.All, daStory.Main
                    If m_iStory = daStory.All Then
                        oRangeCopy = m_oWordDocument.Range
                    Else
                        oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdMainTextStory)
                    End If
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.PrimaryHeader
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdPrimaryHeaderStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.PrimaryFooter
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdPrimaryFooterStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.EvenPagesHeader
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdEvenPagesHeaderStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.EvenPagesFooter
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdEvenPagesFooterStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.FirstPageHeader
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdFirstPageHeaderStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
                Case daStory.FirstPageFooter
                    oRangeCopy = m_oWordDocument.StoryRanges(WdStoryType.wdFirstPageFooterStory)
                    oRangeCopy.SetRange(Start:=oRange.Start, End:=oRange.End)
            End Select


            GetRangeCopy = oRangeCopy


        End Function
    End Class
End Namespace
