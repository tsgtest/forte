Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.DocumentAnalyzer.MSWord

    Public Structure ParsingTemplateUnit
        Dim xName As String
        Dim xChar As String
    End Structure

    'parsing units contain
    'info for writing xml
    'and for parsing child units
    Public Structure ParsingUnit
        Dim xValue As String
        Dim oRange As Word.Range
        Dim xName As String
        Dim xNewValue As String
    End Structure

    Public Class ParsingInstructionHelper

        Private m_iElementID As Integer
        Private m_iExecutionIndex As Integer
        Private m_bDoParsing As Boolean
        Private m_bParsed As Boolean
        Private m_iTemplateCounter As Integer


        Private Function GetRangeSeparatedValues(ByVal xParentElement As String, ByRef uSourceUnit As ParsingUnit, ByRef uTemplateUnit() As ParsingTemplateUnit, _
                                                            iNumParsed As Integer, Optional ByVal iIndex As Integer = 0) As ParsingUnit()

            Dim bUseRange As Boolean
            Dim oParagraph As Word.Paragraph
            Dim uParsingUnit As ParsingUnit
            Dim uParsingUnits() As ParsingUnit
            Dim i, y As Integer
            Dim iCounter As Integer
            Dim xMatch As String
            Dim oMatchRange As Word.Range
            Dim lEnd, lStart, lLen As Long
            Dim k As Integer
            Dim bValidity As Boolean
            Dim iElementCount As Integer
            Dim iFindRawValueChar As Integer
            Dim bCharReplace As Boolean
            On Error GoTo ProcError

            With uSourceUnit
                xMatch = .xValue
                oMatchRange = .oRange
            End With

            uParsingUnit = Nothing
            uParsingUnits = Nothing
            iElementCount = 0
            iCounter = 0
            i = 0
            lEnd = 0
            lStart = 1
            lLen = 0
            k = 1


            If Not (oMatchRange Is Nothing) Then
                bUseRange = True
            End If

            If bUseRange Then

                For Each oParagraph In oMatchRange.Paragraphs


                    With uParsingUnit
                        .xValue = oParagraph.Range.Text
                        iFindRawValueChar = FindChar(.xValue, uTemplateUnit(k).xChar)
                        If (iFindRawValueChar > 0) Then
                            If (GlobalMethods.ReplaceIllegalChars(GlobalMethods.TrimBlanks(.xValue)) <> "") Then
                                bCharReplace = False
                                'name after parent
                                If (Len(.xValue) > 0) Then
                                    .xName = uTemplateUnit(k).xName
                                    bValidity = True
                                Else
                                    .xName = xParentElement & iElementCount
                                    bValidity = False

                                End If

                            Else
                                bCharReplace = True

                                If (GlobalMethods.ReplaceIllegalChars(GlobalMethods.TrimBlanks(.xValue)) = "") Then

                                    iElementCount = iElementCount + 1
                                    '.xName = xParentElement & iElementCount
                                    .xName = ""
                                    .xValue = ""
                                    .xNewValue = "New"
                                    If (k = 1) Then
                                        k = 0
                                    End If
                                End If
                                bValidity = True
                                If (k = UBound(uTemplateUnit)) Then
                                    k = 1
                                Else
                                    If (bValidity = True) Then
                                        k = k + 1
                                    End If
                                End If
                            End If
                        End If
                        .oRange = oParagraph.Range
                    End With
                    ReDim Preserve uParsingUnits(iCounter)
                    uParsingUnits(iCounter) = uParsingUnit
                    iNumParsed = iCounter
                    iCounter = iCounter + 1
                    i = i + 1
                    If (bCharReplace = False) Then
                        If (k = UBound(uTemplateUnit)) Then
                            k = 1
                        Else
                            If (bValidity = True) Then
                                k = k + 1

                            End If
                        End If
                    End If

                Next
            End If

            GetRangeSeparatedValues = uParsingUnits

            Exit Function
ProcError:
            GlobalMethods.RaiseError("ParsingInstructionHelper.GetRangeSeparatedUnits")
            Exit Function
        End Function
        Public Function GetParsingInstructionValue(ByVal xTemplate As String, ByVal xParentElement As String, ByVal oMatchRange As Word.Range) As String

            Dim i As Integer
            Dim iRecordCount As Integer
            Dim uElement As ParsingUnit
            Dim uRecords() As ParsingUnit
            Dim xRecordValue As String
            Dim bValidRecord As Boolean
            Dim xRecordName As String
            Dim xXML As String
            Dim uTemplateElement() As ParsingTemplateUnit
            Dim xClosedElement As String
            Dim l As Integer
            Dim xNewRecordValue As String

            On Error GoTo ProcError

            iRecordCount = -1
            bValidRecord = False
            uElement = Nothing
            xXML = ""

            uTemplateElement = GetRangeParsingValue(xTemplate)
            With uElement
                .oRange = oMatchRange
                .xValue = ""
            End With

            'Get the Records and store it in Array
            uRecords = GetRecordValues(xParentElement, uElement, uTemplateElement, iRecordCount)
            If iRecordCount > -1 Then

                For i = 0 To iRecordCount
                    'Trim any unwanted characters
                    xRecordValue = GlobalMethods.ReplaceIllegalChars(GlobalMethods.TrimBlanks(uRecords(i).xValue))
                    xNewRecordValue = uRecords(i).xNewValue

                    If Len(xRecordValue) > 0 Then
                        xRecordName = GlobalMethods.RemoveNonAlphaNumeric(uRecords(i).xName)
                        bValidRecord = True
                    Else
                        xRecordName = uRecords(i).xName
                        bValidRecord = False
                        If (xNewRecordValue = "New") Then
                            xXML = xXML & vbNewLine
                            '                 xXML = xXML & "<" & xParentElement & ">"
                        End If
                    End If

                    'write xml.
                    If bValidRecord Then
                        xXML = xXML & "<" & xRecordName & ">"
                    Else
                        If (xRecordName <> "") Then

                            If (xRecordValue = "") Then
                                xClosedElement = xRecordName
                                'xXML = xXML & "<" & xRecordName & ">"
                            End If
                        End If

                    End If
                    If bValidRecord Then

                        xXML = xXML & xRecordValue
                        'End Writing Child Elements
                        xXML = xXML & "</" & xRecordName & ">"
                        xXML = xXML & vbNewLine
                    End If
                    If (i = iRecordCount - 1) Then
                        Exit For
                    End If

                Next



            End If

            GetParsingInstructionValue = xXML

            Exit Function
ProcError:
            GlobalMethods.ShowError()
            Exit Function
        End Function

        Private Function GetRecordValues(ByVal xElementName As String, ByRef uElement As ParsingUnit, ByRef uTemplateUnit() As ParsingTemplateUnit, _
                                    iRecordCount As Integer) As ParsingUnit()

            Dim uRecords() As ParsingUnit

            On Error GoTo ProcError

            uRecords = GetRangeSeparatedValues(xElementName, uElement, uTemplateUnit, iRecordCount)
            GetRecordValues = uRecords

            Exit Function
ProcError:
            GlobalMethods.RaiseError("ParsingInstructionHelper.GetRecordValues")
            Exit Function
        End Function
        Private Function GetRangeParsingValue(ByVal Template As String) As ParsingTemplateUnit()

            Dim iCounter As Integer
            Dim xParseParentElementName As String
            Dim iTemplateStartIndex As Integer
            Dim iTemplateEndIndex As Integer
            Dim iTemplateNextIndex As Integer
            Dim iTemplateLength As Integer
            Dim xChildElement As String
            Dim iStartRawValueIndex As Integer
            Dim iEndRawValueIndex As Integer
            Dim iRawValueLength As Integer
            Dim iLastIndex As Integer
            Dim bNextIndex As Boolean
            Dim xFoundChr As String
            Dim xRawChildValue As String
            Dim xParseError As String
            Dim xResult As String

            Dim iCharCount As Integer
            Dim i As Integer
            Dim uParsingTemplateUnit As ParsingTemplateUnit
            Dim uParsingTemplateUnits() As ParsingTemplateUnit



            iCounter = 1
            xParseParentElementName = ""
            iTemplateStartIndex = 0
            iTemplateEndIndex = -1
            iTemplateNextIndex = -1
            iTemplateLength = 0
            xChildElement = ""
            iStartRawValueIndex = 1
            iEndRawValueIndex = 1
            iRawValueLength = 0
            iLastIndex = -1
            bNextIndex = False
            'xFindChr = ""
            xRawChildValue = ""
            xParseError = ""
            xResult = ""
            uParsingTemplateUnits = Nothing

            iTemplateStartIndex = InStr(Template, "[")
            iTemplateEndIndex = InStr(Template, "]")
            If (iTemplateEndIndex > 0) Then
                iTemplateNextIndex = InStr(iTemplateEndIndex, Template, "[")
            Else
                iTemplateNextIndex = InStr(Template, "[")
            End If

            iTemplateLength = iTemplateEndIndex - iTemplateStartIndex

            For i = 1 To Len(Template)

                xChildElement = Mid(Template, iTemplateStartIndex + 1, iTemplateLength - 1)
                If (iTemplateNextIndex > iTemplateEndIndex) Then
                    xFoundChr = Mid(Template, iTemplateEndIndex + 1, (iTemplateNextIndex - (iTemplateEndIndex + 1)))
                Else
                    xFoundChr = Mid(Template, iTemplateEndIndex + 1)
                End If
                iCharCount = InStr(xChildElement, "_")
                If (iCharCount > 0) Then
                    xChildElement = Mid(xChildElement, iCharCount + 1)
                End If

                uParsingTemplateUnit.xName = xChildElement
                uParsingTemplateUnit.xChar = xFoundChr

                ReDim Preserve uParsingTemplateUnits(iCounter)
                uParsingTemplateUnits(iCounter) = uParsingTemplateUnit
                iCounter = iCounter + 1

                i = i + 1
                iTemplateStartIndex = iTemplateEndIndex + 1
                iTemplateEndIndex = InStr(iTemplateStartIndex, Template, "]")
                If (iTemplateEndIndex > 0) Then
                    iTemplateNextIndex = InStr(iTemplateEndIndex, Template, "[")
                    iTemplateLength = iTemplateEndIndex - iTemplateStartIndex
                End If
                If (iTemplateLength < 0) Then
                    Exit For
                ElseIf (iTemplateEndIndex = 0) Then
                    Exit For

                End If

            Next
            m_iTemplateCounter = iCounter

            GetRangeParsingValue = uParsingTemplateUnits

        End Function


        Private Function FindChar(ByVal Text As String, _
         ByVal Search As String, _
         Optional ByVal Position As Long = 0, _
         Optional CaseSensitive As Boolean = False, _
         Optional ByVal Up As Boolean = False) _
         As Long

            Dim lPos As Long
            Dim lFind As Long
            Dim lBoolFind As Boolean
            Dim xChar As String
            Dim xNewString As String
            Dim i As Integer
            Dim j As Integer
            Dim k As Integer
            Dim iAsc As Integer
            Dim iAsc1 As Integer
            Dim iAsc2 As Integer
            Dim xSearchChar As String

            xNewString = ""
            FindChar = ""

            'vbCr, vbTab, vbVerticalTab, vbLf, vbNullChar, vbCrLf, vbNewLine, vbNullString, vbFormFeed
            If Text <> "" Then
                If Position < 1 Then Position = 1
                If Position > Len(Text) Then Position = Len(Text)
                If Up Then
                    lPos = Position - 1
                    While lPos > 0
                        lFind = InStr(lPos, Text, Search, Math.Abs(Not Val(CaseSensitive)))
                        If lFind = lPos Then
                            lPos = 0
                        Else
                            lFind = 0
                            lPos = lPos - 1
                        End If
                    End While
                    FindChar = lFind
                Else
                    FindChar = InStr(Position + 1, Text, Search, _
                      Math.Abs(Not Val(CaseSensitive)))
                    ' Search = ""

                    If (FindChar = 0) Then
                        For k = 1 To Len(Search)
                            xSearchChar = Mid(Search, k, 1)
                            If GlobalMethods.IsAlphaNumeric(xSearchChar) = False Then

                                iAsc1 = -1
                            Else
                                iAsc1 = Search
                            End If
                        Next

                        For i = 1 To Len(Text)
                            xChar = Mid(Text, i, 1)

                            If (iAsc1 > -1) Then
                                iAsc = Asc(xChar)
                                If (iAsc = iAsc1) Then
                                    j = i
                                    lBoolFind = True
                                    xNewString = xNewString + xChar
                                End If
                            ElseIf (iAsc1 = -1) Then

                                If (xChar = Search) Then
                                    j = i
                                    lBoolFind = True
                                    xNewString = xNewString + xChar
                                Else
                                    iAsc = Asc(xChar)
                                    iAsc2 = Asc(Search)
                                    If (iAsc2 = 7) Then
                                        If (iAsc = 13 Or iAsc = 7) Then
                                            j = i
                                            lBoolFind = True
                                            xNewString = xNewString + xChar
                                        End If

                                    End If

                                End If

                            End If
                        Next
                        If (lBoolFind = True) Then
                            FindChar = j
                        End If
                    End If
                End If


            End If


        End Function

    End Class
End Namespace