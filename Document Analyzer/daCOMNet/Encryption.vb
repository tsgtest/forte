Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.DocumentAnalyzer.MSWord
    Public Class Encryption

        Public Shared Function DecryptText(ByVal vText As Object)
            Dim i As Integer
            Dim iChar As Integer
            Dim vScram As Object
            Dim iSeed As Integer
            Dim iRnd As Integer

            On Error GoTo ProcError
            vScram = Nothing

            'this function will decrypt string
            'encrypted by EncryptText function

            '   first 2 characters are encryption markers
            If Left(vText, 2) = "~}" Then
                '       3rd character is variable seed
                iRnd = Asc(Mid(vText, 3, 1))
                iSeed = iRnd - 144

                '       stri((iChar - g_iSeed) - (i Mod 5)) + iSeedng to decrypt starts at 5th character
                vText = Mid(vText, 5)
                For i = 1 To Len(vText)
                    On Error Resume Next
                    If Asc(Mid(vText, i, 1)) <> AscW(Mid(vText, i, 1)) Then
                        ' Use AscW because encrypted character may be beyond ASCII 255 limit
                        ' However, it shouldn't be higher than
                        ' ((255 + g_iSeed) + (i Mod 5)) - iSeed
                        ' So use regular Asc function for those characters (Unicode)
                        If AscW(Mid(vText, i, 1)) <= ((255 + IIf(GlobalMethods.g_iSeed <> 0, GlobalMethods.g_iSeed, GlobalMethods.g_iOldSeed)) + (i Mod 5)) - iSeed Then
                            iChar = AscW(Mid(vText, i, 1))
                        Else
                            iChar = Asc(Mid(vText, i, 1))
                        End If
                    Else
                        iChar = Asc(Mid(vText, i, 1))
                    End If
                    On Error GoTo 0
                    vScram = vScram & Chr(((iChar - IIf(GlobalMethods.g_iSeed <> 0, GlobalMethods.g_iSeed, GlobalMethods.g_iOldSeed)) - (i Mod 5)) + iSeed)
                Next i
            Else
                vScram = vText
            End If


            DecryptText = vScram
            Debug.Print(DecryptText)
            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlEncrypt.DecryptText")
            Exit Function
        End Function

        Public Shared Function iGetSeed(Optional xSeedKey As String = "Seed") As Integer   'noseed
            Dim iSeedLen As Integer
            Dim xChar As String
            Dim iDigit As Integer
            Dim iTemp As Integer
            Dim xSeed As String

            On Error GoTo ProcError

            If xSeedKey = "OldSeed" Then
                xSeed = UCase(LMP.Registry.GetLocalMachineValue("MacPacEncryptionOldSeed"))
            Else
                xSeed = UCase(LMP.Registry.GetLocalMachineValue("MacPacEncryptionSeed"))
            End If

            '   get length
            iSeedLen = Len(xSeed)

            If iSeedLen > 0 Then
                '       cycle through seed string and turn into number 'iDigit'
                While Len(xSeed)
                    xChar = Mid(xSeed, 1, 1)
                    iDigit = iDigit + Asc(xChar)
                    xSeed = Mid(xSeed, 2)
                End While
                iTemp = iDigit / iSeedLen
            Else
                iTemp = iSeedLen
            End If

            '   return seed number
            iGetSeed = iTemp
            Exit Function
ProcError:
            GlobalMethods.RaiseError("mdlEncrypt.iGetSeed")
            Exit Function

        End Function
    End Class
End Namespace

