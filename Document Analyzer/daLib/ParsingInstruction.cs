using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
namespace LMP.DocAnalyzer
{
    public class ParsingInstruction
    {
        # region *********************************fields*********************************
        string m_xfldcodeTemplate;
        Definitions.daParseSource m_ofldParseType;
        public string m_xElement;
        # endregion
        # region *********************************properties*********************************
        /// <summary>
        /// Gets/sets the field code template for the ParseInstruction
        /// </summary>
        public string Template
        {
            get
            {
                return m_xfldcodeTemplate;
            }
            set
            {
                m_xfldcodeTemplate = value;
            }
        }

        /// <summary>
        /// Gets/sets whether we�re going to parse the Instruction.Text or the Instruction.MatchRange.
        /// </summary>
        public Definitions.daParseSource ParseSource
        {
            get
            {
                return m_ofldParseType;
            }
            set
            {
                m_ofldParseType = value;
            }
        }

        # endregion
        # region *********************************methods*********************************
        /// <summary>
        /// Sets the properties value for parsing instruction.
        /// </summary>
        public ParsingInstruction(string xTemplate, Definitions.daParseSource iParseSource)
        {
            this.Template = xTemplate;
            this.ParseSource = iParseSource;
        }
        /// <summary>
        /// returns a string that replaces all [CHAR]
        /// field codes with the appropriate characters
        /// /// </summary>
        /// <param name="xExp"></param>
        /// <returns></returns>
        public static string EvaluateCharFieldCodes(string xExpression)
        {

            string xMod = xExpression;
            //exit if expression is empty
            if (xMod == null || xMod == "")
                return xMod;
            //find all [CHAR] field codes
            MatchCollection oMatches = Regex.Matches(xMod, @"(?i)\[CHAR_.*?\]");

            foreach (Match oMatch in oMatches)
            {
                //get the key code of the field code
                string xKeyCode = oMatch.Value.Substring(6);
                xKeyCode = xKeyCode.TrimEnd(']');
                int iKeyCode = 0;
                //convert to int - alert if not convertible
                try
                {
                    iKeyCode = System.Convert.ToInt32(xKeyCode);
                }
                catch (System.Exception oE)
                {
                    throw new System.Exception(
                        "Could not evaluate character field codes while parsing: " + xExpression, oE);

                }
                //replace the field code with the character having the specified key code
                xMod = xMod.Replace(oMatch.Value, ((char)iKeyCode).ToString());

            }
            return xMod;

        }
        /// <summary>
        /// returns the parsed Xml string 
        /// Executes the parse instruction based on the Template
        /// </summary>
        /// <param name="xData"></param>
        /// <returns>string</returns>
        public string Execute(string xData, bool bIsRecipients)
        {
            StringBuilder oSB = new StringBuilder();

            XmlTextWriter oXMLWriter = new XmlTextWriter(
                new MemoryStream(), System.Text.UTF8Encoding.Unicode);

            string xTemplate = this.Template;
            string xEntityDelimiter = "";
            int iTemplateTempFieldStartPos;
            int iNextFieldStartPos = -1;
            int iTemplateTempFieldEndPos;
            string xFieldDelimiter = "";
            string xEntity = string.Empty;
            bool bIsStandardRecipientTemplateLetter = false;
            string xStandardRecipientTemplateLetter = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_TITLE]\r\n[Field_COMPANY]\r\n[Field_COREADDRESS]";
            bool bIsStandardRecipientTemplateServiceList = false;
            string xStandardRecipientTemplateServiceList = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COREADDRESS]|[Field_PHONE]|" +
                                                           "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";

            //evaluate all char field codes in template -
            xTemplate = EvaluateCharFieldCodes(xTemplate);

            //get entity delimiter from template - this will be
            //the text that follows the last [Field_X] code
            int iPos = xTemplate.LastIndexOf("[Field_");
            int iPosEnd = -1;
            bool bHasEntityDelimiter = false;

            if (iPos == -1)
                //template is invalid
                throw new LMP.Exceptions.ParameterException(
                    LMP.Resources.GetLangString("Error_InvalidParsingTemplate") + xTemplate);
            else
            {
                //get end of field code
                iPosEnd = xTemplate.IndexOf("]", iPos);
                if (iPosEnd == -1)

                    //template is invalid
                    throw new LMP.Exceptions.ParameterException(
                        LMP.Resources.GetLangString("Error_InvalidParsingTemplate") + xTemplate);

                //get entity delimiter from end of field
                xEntityDelimiter = xTemplate.Substring(iPosEnd + 1);

                //entity delimiter is identical to a field delimiter (like Fax).
                if (xTemplate.IndexOf(xEntityDelimiter) < iPos)
                    bHasEntityDelimiter = false;
                else
                    bHasEntityDelimiter = true;

            }

            string[] aEntities = null;

            if (bHasEntityDelimiter)
                //split data into entities - then parse each entity
                aEntities = xData.Split(
                   new string[] { xEntityDelimiter }, StringSplitOptions.None);
            else
            {
                //there's no entity delimiter, so build entities
                //using template fields to parse the data string.

                //make sure field and sep arrays are being built correctly.

                string xTemplateFields = string.Empty;
                string xSeparators = string.Empty;
                
                //create an entity separator
                string xSEP = "|SEP|";

                iTemplateTempFieldStartPos = xTemplate.IndexOf("[Field_");

                //get array of the parsing template separators
                while (iTemplateTempFieldStartPos > -1)
                {

                    //find end of field
                    iTemplateTempFieldEndPos = xTemplate.IndexOf("]", iTemplateTempFieldStartPos);

                    //find start of next field
                    iNextFieldStartPos = xTemplate.IndexOf("[", iTemplateTempFieldEndPos);

                    //if there is a next field,parse delimiter
                    if (iNextFieldStartPos > -1)
                    {
                        //get delimiter
                        xFieldDelimiter = xTemplate.Substring(
                            iTemplateTempFieldEndPos + 1, iNextFieldStartPos -
                            iTemplateTempFieldEndPos - 1);

                        xSeparators += xFieldDelimiter + xSEP;

                    }

                    iTemplateTempFieldStartPos = iNextFieldStartPos;
                }

                //add last separator
                //jsw -- is this necessary?
                xSeparators += xEntityDelimiter;

                string[] aSeps = xSeparators.Split(new string[] { xSEP }, StringSplitOptions.None);
                int iLength = aSeps.Length;
                int iStartPos = 0;
                int iNextPos = 0;
                string xEntities = string.Empty;
                bool bDone = false;

                while (!bDone)
                {
                    for (int i = 0; i < iLength; i++)
                    {
                        iStartPos = xData.IndexOf(aSeps[i], iStartPos) + aSeps[i].Length;

                    }
                    if (iStartPos > iNextPos)
                    {   
                        //remove field separator from end of entity if it exists
                        xEntity = xData.Substring(iNextPos, (iStartPos - iNextPos));
                        if (xEntity.Substring(xEntity.Length - xFieldDelimiter.Length, xFieldDelimiter.Length) == xFieldDelimiter)
                            xEntity = xEntity.Substring(0, xEntity.Length - 1);
                       ///xEntity = xEntity.Trim();

                        xEntities += xEntity + xSEP;
                    }
                    else
                    {
                        //remove field separator from end of entity if it exists
                        xEntity = xData.Substring(iNextPos);
                        if (xEntity.Substring(xEntity.Length - xFieldDelimiter.Length, xFieldDelimiter.Length) == xFieldDelimiter)
                            xEntity = xEntity.Substring(0, xEntity.Length - 1);

                       // xEntity = xEntity.Trim();

                        xEntities += xEntity + xSEP;
                        bDone = true;
                    }

                    iNextPos = iStartPos;
                }

                //remove last separator
                xEntities = xEntities.Substring(0, xEntities.Length - xSEP.Length);
                xEntities = xEntities.Trim();

                //create entity array, splitting at our entity separator
                aEntities = xEntities.Split(
                   new string[] { xSEP }, StringSplitOptions.None);

            }

            //remove entity delimiter from template, as we're through using it
            xTemplate = xTemplate.Substring(0, iPosEnd + 1);

            if (xTemplate == xStandardRecipientTemplateLetter)
                bIsStandardRecipientTemplateLetter = true;

            if (xTemplate == xStandardRecipientTemplateServiceList)
                bIsStandardRecipientTemplateServiceList = true;

            // if (aEntities.GetUpperBound
            //set index counter
            int iIndex = 0;
            
            //cycle through entities, parsing each
            foreach (string xEntityData in aEntities)
            {
                if(string.IsNullOrEmpty(xEntityData))
                    continue;

                //make sure Entity is valid
                if (!bEntityIsValild(xEntityData, xTemplate))
                    break;


                //GLOG : 8137 : JSW
                //modify parsing template if address only 2 delimitors
                //because it indicates that there is no title or company name
                if (bIsRecipients && bIsStandardRecipientTemplateLetter)
                {
                    //reset parsing template to default
                    if (bIsStandardRecipientTemplateLetter)
                        xTemplate = xStandardRecipientTemplateLetter;

                    //get field separator
                    string[] aEnts = xEntityData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    if (aEnts.GetUpperBound(0) == 2 || aEnts.GetUpperBound(0) == 3)
                    {
                        if (bContainsNumber(aEnts[1]))
                        {
                            //2nd line in must be a street address
                            //remove title and company field from parsing template
                            //if it is the standard template for recipients
                            xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COREADDRESS]";
                        }
                        else
                        {
                            //check 3rd line
                            if (bContainsNumber(aEnts[2]))
                            {
                                //2nd line is probably company name, and 3rd line is street address, so remove title field
                                xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COMPANY]\r\n[Field_COREADDRESS]";
                            }
                            else
                                //2nd line is probably title and 3rd line is company name
                                xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_TITLE]\r\n[Field_COMPANY]";
                        }
                    }
                }
                //GLOG : 8137 : JSW
                if (bIsRecipients && bIsStandardRecipientTemplateServiceList)
                {
                   
                    int iPipe = -1;
                    string xAddress = xEntityData;

                    //reset parsing template to default
                    if (bIsStandardRecipientTemplateServiceList)
                        xTemplate = xStandardRecipientTemplateServiceList;
                    
                    //get address portion of value
                    if (xEntityData.Contains("|"))
                    {
                        iPipe = xEntityData.IndexOf("|");
                        xAddress = xAddress.Substring(0, iPipe);
                    }
                    
                    //get field separator
                    string[] aEnts = xAddress.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    int iUBound = aEnts.GetUpperBound(0);
                    if (iUBound > 3)
                    {
                        //title, company and street address
                        xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_TITLE]\r\n[Field_COMPANY]\r\n[Field_COREADDRESS]|[Field_PHONE]|" +
                                    "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";

                    }
                    if (iUBound == 2 || iUBound == 3)
                    {
                        if (bContainsNumber(aEnts[1]))
                        {
                            //2nd line in must be a street address
                            //remove title and company field from parsing template
                            //if it is the standard template for recipients
                            xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COREADDRESS]|[Field_PHONE]|" +
                                        "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";
                        }
                        else
                        {
                            //check 3rd line
                            if (bContainsNumber(aEnts[2]))
                            {

                                //if (iUBound==2)
                                //2nd line is probably company name, and 3rd line is street address, so remove title field
                                xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COMPANY]\r\n[Field_COREADDRESS]|[Field_PHONE]|" +
                                            "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";

                            }
                            else
                                //2nd line is probably title and 3rd line is company name
                                if (iUBound == 2)
                                {
                                    //no street address
                                    xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_TITLE]\r\n[Field_COMPANY]|[Field_PHONE]|" +
                                                "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";
                                }
                                else
                                {
                                    //title, company and street address
                                    xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX]\r\n[Field_COMPANY]\r\n[Field_TITLE]\r\n[Field_COREADDRESS]|[Field_PHONE]|" +
                                                "[Field_FAX]|[Field_EMAILADDRESS]|[Field_PARTYTITLE]|[Field_PARTYNAME]|[Field_DPHRASE]";
                                }
                        }
                    }
                }

                iIndex++;

                string xTemplateTemp = xTemplate;
                string xEntityDataTemp = xEntityData;

                ////write entity opening tag
                //oSB.AppendFormat("<{0}>", this.m_xElement);

                //cycle through fields in template
                iTemplateTempFieldStartPos = xTemplateTemp.IndexOf("[Field_");

                try
                {

                    while (iTemplateTempFieldStartPos > -1)
                    {

                        string xValue = "";

                        int iDataTempPos = -1;

                        //find end of field
                        iTemplateTempFieldEndPos = xTemplateTemp.IndexOf("]");

                        //get field code
                        string xFieldCode = xTemplateTemp.Substring(0, iTemplateTempFieldEndPos + 1);

                        //find start of next field
                        iNextFieldStartPos = xTemplateTemp.IndexOf("[", iTemplateTempFieldEndPos);

                        //if there is a next field,parse delimiter
                        if (iNextFieldStartPos > -1)
                        {
                            //get delimiter
                            xFieldDelimiter = xTemplateTemp.Substring(
                                iTemplateTempFieldEndPos + 1, iNextFieldStartPos -
                                iTemplateTempFieldEndPos - 1);

                            //search for first instance of
                            //delimiter in the remaining data text
                            iDataTempPos = xEntityDataTemp.IndexOf(xFieldDelimiter);

                            //get text from start of data text to
                            //to start of delimiter text
                            if (iDataTempPos > -1)
                                xValue = xEntityDataTemp.Substring(0, iDataTempPos);
                            else
                                xValue = xEntityDataTemp.ToString();
                            //remove "used" string from template
                            xTemplateTemp = xTemplateTemp.Substring(iNextFieldStartPos);

                            //remove "used" data from entity detail
                            
                            if (iDataTempPos > -1)
                                 xEntityDataTemp = xEntityDataTemp.Substring(iDataTempPos + xFieldDelimiter.Length);

                        }

                        else
                        {

                            //indicate that we've analyzed the template
                            xTemplateTemp = "";

                            //remaining text is value for the field
                            xValue = xEntityDataTemp;

                        }

                        //parse name from corresponding field code
                        string xElementName = xFieldCode.Substring(7, xFieldCode.Length - 8);

                        //write appropriate inner xml - in form of
                        //<fieldname>ParsedText</fieldname>
                        if (iDataTempPos > -1)
                            iTemplateTempFieldStartPos = iNextFieldStartPos;
                        else
                        {
                            //run out of data, force this to -1 so we can exit loop
                            iTemplateTempFieldStartPos = -1;
                        }

                        xValue = xValue.Replace("\v", "\r\n");
                        xValue = xValue.Replace("\a", "");
                        //GLOG 15968: Replace Word's non-breaking hyphen char with valid unicode equivalent
                        xValue = xValue.Replace((char)30, (char)8209);

                        //JTS 7/26/10: Make sure the Detail XML doesn't contain reserved XML characters within the Value
                        oSB.AppendFormat(@"<{0} Index='{1}'>{2}</{0}>", xElementName, iIndex, LMP.String.ReplaceXMLChars(xValue.Trim()));
                    }
                }
                //parsing error, return string as it is
                catch { }
            }

            return oSB.ToString();
        }
        
        /// <summary>
        /// returns the parsed Xml string  
        /// Executes the parse instruction based on the Template 
        /// </summary>
        /// <param name="xData"></param>
        /// <returns>string</returns>
        //public string Execute(string xData)
        //{
        //    StringBuilder oSB = new StringBuilder();

        //    XmlTextWriter oXMLWriter = new XmlTextWriter(
        //        new MemoryStream(), System.Text.UTF8Encoding.Unicode);

        //    string xTemplate = this.Template;
        //    string xEntityDelimiter = "";

        //    //evaluate all char field codes in template -
        //    xTemplate = EvaluateCharFieldCodes(xTemplate);
            
        //    //get entity delimiter from template - this will be
        //    //the text that follows the last [Field_X] code
        //    int iPos = xTemplate.LastIndexOf("[Field_");
        //    int iPosEnd = -1;

        //    if (iPos == -1)
        //        //template is invalid
        //        throw new LMP.Exceptions.ParameterException(
        //            LMP.Resources.GetLangString("Error_InvalidParsingTemplate") + xTemplate);
        //    else
        //    {
        //        //get end of field code
        //        iPosEnd = xTemplate.IndexOf("]", iPos);

        //        if (iPosEnd == -1)
        //            //template is invalid
        //            throw new LMP.Exceptions.ParameterException(
        //                LMP.Resources.GetLangString("Error_InvalidParsingTemplate") + xTemplate);

        //        //get entity delimiter from end of field
        //        xEntityDelimiter = xTemplate.Substring(iPosEnd + 1);
        //    }

        //    //split data into entities - then parse each entity
        //    string[] aEntities = xData.Split(
        //        new string[] { xEntityDelimiter },StringSplitOptions.None);

        //    //remove entity delimiter from template, as we're through using it
        //    xTemplate = xTemplate.Substring(0, iPosEnd + 1);

        //    //cycle through entities, parsing each
        //    foreach (string xEntityData in aEntities)
        //    {
        //        string xTemplateTemp = xTemplate;
        //        string xEntityDataTemp = xEntityData;

        //        //write entity opening tag
        //        oSB.AppendFormat("<{0}>", this.m_xElement);

        //        //cycle through fields in template
        //        int iTemplateTempFieldStartPos = xTemplateTemp.IndexOf("[Field_");

        //        while (iTemplateTempFieldStartPos > -1)
        //        {
        //            string xFieldDelimiter = "";
        //            string xValue = "";

        //            //find end of field
        //            int iTemplateTempFieldEndPos = xTemplateTemp.IndexOf("]");

        //            //get field code
        //            string xFieldCode = xTemplateTemp.Substring(0, iTemplateTempFieldEndPos + 1);

        //            //find start of next field
        //            int iNextFieldStartPos = xTemplateTemp.IndexOf("[", iTemplateTempFieldEndPos);

        //            //if there is a next field,parse delimiter
        //            if (iNextFieldStartPos > -1)
        //            {
        //                //get delimiter
        //                xFieldDelimiter = xTemplateTemp.Substring(
        //                    iTemplateTempFieldEndPos + 1, iNextFieldStartPos - 
        //                    iTemplateTempFieldEndPos - 1);

        //                //search for first instance of
        //                //delimiter in the remaining data text
        //                int iDataTempPos = xEntityDataTemp.IndexOf(xFieldDelimiter);

        //                //get text from start of data text to
        //                //to start of delimiter text
        //                xValue = xEntityDataTemp.Substring(0, iDataTempPos);

        //                //remove "used" string from template
        //                xTemplateTemp = xTemplateTemp.Substring(iNextFieldStartPos);
        //            }
        //            else
        //            {
        //                //indicate that we've analyzed the template
        //                xTemplateTemp = "";

        //                //remaining text is value for the field
        //                xValue = xEntityDataTemp;
        //            }
                    
        //            //parse name from corresponding field code
        //            string xElementName = xFieldCode.Substring(7, xFieldCode.Length - 8);

        //            //write appropriate inner xml - in form of 
        //            //<fieldname>ParsedText</fieldname>

        //            iTemplateTempFieldStartPos = iNextFieldStartPos;

        //            oSB.AppendFormat(@"<{0}>{1}<\{0}>", xElementName, xValue);
        //        }

        //        //write entity closing tag
        //        oSB.AppendFormat(@"<\{0}>", this.m_xElement);

        //    }

        //    return oSB.ToString();
        //}
        //public string ExecuteOld(string xData)
        //{
        //    XmlTextWriter xt = new XmlTextWriter(new MemoryStream(), System.Text.UTF8Encoding.Unicode);
        //    int iCounter = 1;
        //    string xParseParentElementName = string.Empty;
        //    int iTemplateStartIndex = 0;
        //    int iTemplateEndIndex = -1;
        //    int iTemplateNextIndex = -1;
        //    int iTemplateLength = 0;
        //    string xEndRow = "\r\a";
        //    string xNewRow = "\a";
        //    string xChildElement = string.Empty;
        //    int iStartRawValueIndex = 0;
        //    int iEndRawValueIndex = 0;
        //    int iRawValueLength = 0;
        //    int iLastIndex = -1;
        //    bool xNextIndex = false;
        //    bool bFindIndex = false;
        //    bool bParenttagClose;
        //    bool bEndTemplate = false;
        //    bool bFindText = false;
        //    string xFindChr = string.Empty;
        //    string xRawChildValue = string.Empty;
        //    string xParseError = string.Empty;
        //    string xResult = string.Empty;


        //        xData = xData.TrimEnd();
        //        //Checking to see if the Template has a Plain Text or [Field_X]
        //        iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //        if (iTemplateEndIndex > -1)
        //        {
        //            iTemplateNextIndex = Template.IndexOf("[", iTemplateEndIndex);
        //            bFindText = true;
        //            if (iTemplateNextIndex < 0)
        //            {
        //                bFindText = false;
        //            }
        //        }
        //        else
        //        {
        //            bFindText = false;
        //        }

        //        if (bFindText)
        //        {
        //            //Loop until it reaches end of the raw value.
        //            do
        //            {
        //                iTemplateStartIndex = 0;
        //                if (bEndTemplate)
        //                {
        //                    break;
        //                }
        //                iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //                iTemplateNextIndex = Template.IndexOf("[", iTemplateEndIndex);
        //                if (xData.Length > iEndRawValueIndex)
        //                {
        //                    //if Rawvalue still has values to be parsed
        //                    //if [Field_X] code does not exist 
        //                    //return a error
        //                    if (iTemplateEndIndex < 0)
        //                    {
        //                        xParseError = "Problem with template." + "\n" + m_xElement + " cannot be parsed";
        //                        return xParseError;
        //                    }
        //                }
        //                iTemplateLength = 0;
        //                xParseParentElementName = m_xElement + iCounter;
        //                if (iCounter > 1)
        //                {
        //                    xt.WriteRaw("\n");
        //                }

        //                //Start forming XML
        //                xt.WriteStartElement(xParseParentElementName);
        //                bParenttagClose = true;
        //                bFindIndex = true;
        //                //Loop until it reaches end of the Template.
        //                do
        //                {
        //                    if (iTemplateEndIndex > iTemplateStartIndex)
        //                    {
        //                        iTemplateLength = iTemplateEndIndex - iTemplateStartIndex;
        //                        if (iTemplateLength < 0)
        //                        {
        //                            break;
        //                        }
        //                    }
        //                    if (iTemplateEndIndex < 0)
        //                    {
        //                        break;
        //                    }
        //                    if (Template.Length != iTemplateEndIndex + 1)
        //                    {
        //                        //Find the string next to [Field_X]
        //                        if (iTemplateNextIndex > -1)
        //                        {
        //                            xFindChr = Template.Substring(iTemplateEndIndex + 1, iTemplateNextIndex - (iTemplateEndIndex + 1));
        //                            xFindChr = Definitions.TrimBlanks(xFindChr);
        //                            xNextIndex = true;
        //                        }
        //                        else
        //                        {
        //                            xFindChr = Template.Substring(iTemplateEndIndex + 1);
        //                            xFindChr = Definitions.TrimBlanks(xFindChr);
        //                            if (iTemplateEndIndex < 0)
        //                            {
        //                                break;
        //                            }

        //                            xNextIndex = false;
        //                        }
        //                    }

        //                    //If it is not a Plain Text but contains [Field_X] code                                                              
        //                    if (iTemplateLength > 0)
        //                    {

        //                        xChildElement = Template.Substring(iTemplateStartIndex + 1, iTemplateLength - 1);
        //                        xChildElement = xChildElement.Substring(xChildElement.IndexOf("_") + 1);
        //                        xt.WriteRaw("\n");
        //                        xt.WriteStartElement(xChildElement);

        //                        if (!string.IsNullOrEmpty(xFindChr))
        //                        {
        //                            //Check whether the string found in template exists in raw value
        //                            //if exists write it to xml
        //                            iEndRawValueIndex = xData.IndexOf(xFindChr, iStartRawValueIndex);
        //                            if (iEndRawValueIndex < 0)
        //                            {
        //                                xParseError = "Problem with template." + "\n" + m_xElement + " cannot be parsed";
        //                                return xParseError;
        //                            }
        //                            if (iEndRawValueIndex > iStartRawValueIndex)
        //                            {
        //                                iRawValueLength = iEndRawValueIndex - iStartRawValueIndex;
        //                                xRawChildValue = xData.Substring(iStartRawValueIndex, iRawValueLength);
        //                                xRawChildValue = Definitions.TrimBlanks(xRawChildValue);
        //                                xt.WriteRaw(xRawChildValue);
        //                            }
        //                            else if (iEndRawValueIndex == iStartRawValueIndex)
        //                            {
        //                                if (bFindIndex)
        //                                {
        //                                    xRawChildValue = xData.Substring(iStartRawValueIndex).Trim();
        //                                    int iFoundIndex = xRawChildValue.IndexOf(xFindChr);

        //                                    //Get the Index of next occurence of the found character
        //                                    iEndRawValueIndex = xData.IndexOf(xFindChr, iStartRawValueIndex + iFoundIndex);
        //                                    if (iEndRawValueIndex > iStartRawValueIndex)
        //                                    {
        //                                        iRawValueLength = iEndRawValueIndex - iStartRawValueIndex;
        //                                    }
        //                                    //Get the value starting from the found index
        //                                    xRawChildValue = xData.Substring(iStartRawValueIndex, iRawValueLength).Trim();
        //                                    xRawChildValue = Definitions.TrimBlanks(xRawChildValue);
        //                                    xt.WriteRaw(xRawChildValue);

        //                                }

        //                            }

        //                            //Move to next substring in the rawvalue
        //                            if (iEndRawValueIndex > -1)
        //                            {
        //                                iStartRawValueIndex = iEndRawValueIndex + xFindChr.Length;
        //                            }
        //                            iEndRawValueIndex = xData.IndexOf(xFindChr, iStartRawValueIndex);

        //                            if (iEndRawValueIndex < 0)
        //                            {
        //                                xt.WriteEndElement();
        //                                if (!xNextIndex)
        //                                {
        //                                    xt.WriteRaw("\n");
        //                                    xt.WriteEndElement();
        //                                }
        //                                break;
        //                            }
        //                            else if (iStartRawValueIndex == iEndRawValueIndex)
        //                            {
        //                                xt.WriteEndElement();
        //                                if (bParenttagClose)
        //                                {
        //                                    xt.WriteRaw("\n");
        //                                    xt.WriteEndElement();
        //                                    bParenttagClose = false;
        //                                }
        //                                break;
        //                            }
        //                        }
        //                        xt.WriteEndElement();
        //                    }
        //                    //Move to next [Field_x]
        //                    if (iTemplateEndIndex < 0)
        //                    {
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        iTemplateStartIndex = iTemplateEndIndex + 1;
        //                        iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //                        if (iTemplateEndIndex > 0)
        //                        {
        //                            bFindIndex = false;
        //                            iTemplateNextIndex = Template.IndexOf("[", iTemplateEndIndex);
        //                        }
        //                        else if (iTemplateEndIndex < 0)
        //                        {
        //                            break;
        //                        }

        //                    }

        //                } while (Template.Length > iTemplateEndIndex); //while (Template.Length != iTemplateStartIndex);


        //                //If there is one more [Field_x] string in the Template 
        //                //Populate the value for the [Field_x] code
        //                if (iEndRawValueIndex < 0)
        //                {
        //                    if (bEndTemplate)
        //                    {
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                        break;
        //                    }
        //                    //For the last  element 
        //                    if (Template.Length > iTemplateEndIndex)
        //                    {

        //                        iTemplateStartIndex = iTemplateEndIndex + 1;
        //                        xChildElement = Template.Substring(iTemplateStartIndex);
        //                        xChildElement = xChildElement.Substring(xChildElement.IndexOf("_") + 1);
        //                        iLastIndex = xChildElement.IndexOf("]");
        //                        if (iLastIndex > -1)
        //                        {
        //                            //Loop through the process until you reach the end of the template
        //                            do
        //                            {
        //                                //Get the Start and End  index of the [Field_X] 
        //                                iTemplateStartIndex = iTemplateEndIndex + 1;
        //                                iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //                                if (iTemplateEndIndex > 0)
        //                                {
        //                                    iTemplateNextIndex = Template.IndexOf("[", iTemplateEndIndex);
        //                                }
        //                                //If you still have the [Field_X] find the [Char_X] next to [Field_X]
        //                                if (iTemplateNextIndex > -1)
        //                                {
        //                                    xFindChr = Template.Substring(iTemplateEndIndex + 1, iTemplateNextIndex - (iTemplateEndIndex + 1));
        //                                }
        //                                else
        //                                {
        //                                    if (iTemplateEndIndex < 0)
        //                                    {
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        xFindChr = Template.Substring(iTemplateEndIndex + 1);
        //                                    }
        //                                }

        //                                if (iTemplateEndIndex > iTemplateStartIndex)
        //                                {
        //                                    iTemplateLength = iTemplateEndIndex - iTemplateStartIndex;
        //                                }
        //                                xChildElement = Template.Substring(iTemplateStartIndex + 1, iTemplateLength - 1);
        //                                xChildElement = xChildElement.Substring(xChildElement.IndexOf("_") + 1);
        //                                xChildElement = xChildElement.Replace("]", "").Trim();

        //                                //Check whether the [Char_x] found  in template exists in raw value
        //                                //if exists write it to xml                                                                                        
        //                                iEndRawValueIndex = xData.IndexOf(xFindChr, iStartRawValueIndex);
        //                                if (iEndRawValueIndex > 0)
        //                                {
        //                                    iStartRawValueIndex = iEndRawValueIndex + xFindChr.Length;
        //                                    xRawChildValue = xData.Substring(iStartRawValueIndex);
        //                                }
        //                                else if (iEndRawValueIndex < 0)
        //                                {
        //                                    xRawChildValue = xData.Substring(iStartRawValueIndex);

        //                                }
        //                                else if (iEndRawValueIndex > iStartRawValueIndex)
        //                                {
        //                                    iRawValueLength = iEndRawValueIndex - iStartRawValueIndex;
        //                                    xRawChildValue = xData.Substring(iStartRawValueIndex, iRawValueLength);
        //                                }

        //                                xt.WriteRaw("\n");
        //                                xt.WriteStartElement(xChildElement);
        //                                xt.WriteRaw(xRawChildValue);
        //                                xt.WriteEndElement();
        //                                if (iStartRawValueIndex == iRawValueLength - 1)
        //                                {
        //                                    break;
        //                                }
        //                            } while (Template.Length > iTemplateStartIndex);
        //                        }

        //                    }
        //                    if (iLastIndex > -1)
        //                    {
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                        break;
        //                    }
        //                }
        //                if (iLastIndex > -1)
        //                {
        //                    //Close the Parent Tag
        //                    if (xNextIndex)
        //                    {
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                        bParenttagClose = false;
        //                    }
        //                }
        //                else
        //                {
        //                    if (xNextIndex && bParenttagClose)
        //                    {
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                        bParenttagClose = false;
        //                    }

        //                }

        //                if ((xData.Substring(iStartRawValueIndex) == xEndRow.ToString()) || (xData.Substring(iStartRawValueIndex) == xNewRow.ToString()))
        //                {
        //                    bEndTemplate = true;
        //                    if (bParenttagClose)
        //                    {
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                    }
        //                    bParenttagClose = false;
        //                    break;
        //                }

        //                iCounter++;
                       
        //            } while (xData.Length > iEndRawValueIndex);
        //        }

        //        //If Template has only Plain Text
        //        else
        //        {
        //            //Split the raw value by checking if it has any [Char_x]
        //            string[] xSplit = xData.Split(new Char[] { '\r' });
        //            int iNextElement = 0;

        //            //Loop until you reach end of raw value
        //            do
        //            {
        //                for (int j = iNextElement; j < xSplit.Length; j++)
        //                {
        //                    //Check whether the raw value has any paragraph string
        //                    if (xData.IndexOf("\r\n\r\n") > -1)
        //                    {
        //                        if (iCounter > 1)
        //                        {
        //                            xParseParentElementName = m_xElement + iCounter;
        //                            xt.WriteRaw("\n");
        //                        }
        //                        else
        //                        {
        //                            xParseParentElementName = m_xElement;
        //                        }
        //                        xParseParentElementName = m_xElement + iCounter;
        //                        xt.WriteStartElement(xParseParentElementName);
        //                        //Get the Child Element name from the Template
        //                        xt.WriteRaw("\n");
        //                        iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //                        if (iTemplateEndIndex > iTemplateStartIndex)
        //                        {
        //                            iTemplateLength = iTemplateEndIndex - iTemplateStartIndex;
        //                        }
        //                        if (iTemplateLength > 0)
        //                        {
        //                            xChildElement = Template.Substring(iTemplateStartIndex + 1, iTemplateLength - 1);
        //                            xChildElement = xChildElement.Substring(xChildElement.IndexOf("_") + 1);
        //                            //xt.WriteRaw("\n");
        //                            xt.WriteStartElement(xChildElement);
        //                        }
        //                        //xt.WriteStartElement(Template.Substring(Template.IndexOf("_") + 1));

        //                        //Loop until you find Paragraph string
        //                        do
        //                        {
        //                            iNextElement = j + 1;
        //                            if (!string.IsNullOrEmpty(xSplit[j]))
        //                            {
        //                                xt.WriteRaw(xSplit[j]);
        //                            }
        //                            //do the process until you reach end of raw value
        //                            if (iNextElement == xSplit.Length)
        //                            {
        //                                break;
        //                            }
        //                            j++;
        //                        } while (xSplit[j] != "\n");

        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                        iCounter++;
        //                        xt.WriteRaw("\n");
        //                        xt.WriteEndElement();
        //                    }
        //                    else
        //                    {
        //                        //If the raw value doesn't have any paragraph string
        //                        //If the raw value is a Plain Text get the first instance of text
        //                        //put it as the value for the child element.
        //                        xParseParentElementName = m_xElement;
                               
        //                        iTemplateEndIndex = Template.IndexOf("]", iTemplateStartIndex);
        //                        if (iTemplateEndIndex > iTemplateStartIndex)
        //                        {
        //                            iTemplateLength = iTemplateEndIndex - iTemplateStartIndex;
        //                        }

        //                        if (iTemplateLength > 0)
        //                        {
        //                            xt.WriteStartElement(xParseParentElementName);
        //                            xt.WriteRaw("\n");
        //                            xChildElement = Template.Substring(iTemplateStartIndex + 1, iTemplateLength - 1);
        //                            xChildElement = xChildElement.Substring(xChildElement.IndexOf("_") + 1);
        //                            //xt.WriteRaw("\n");
        //                            xt.WriteStartElement(xChildElement);
        //                            iNextElement = j + 1;
        //                            if (!string.IsNullOrEmpty(xSplit[j]))
        //                            {
        //                                xt.WriteRaw(xSplit[j]);
        //                                xt.WriteRaw("\n");
        //                                xt.WriteEndElement();
        //                            }
        //                            xt.WriteRaw("\n");
        //                            xt.WriteEndElement();
        //                        }

        //                        else
        //                        {
        //                            iNextElement = j + 1;
        //                            if (iTemplateEndIndex < 0)
        //                            {
        //                                break;
        //                            }
        //                        }
                               
                               

        //                    }
        //                }


        //            } while (iNextElement != xSplit.Length);

        //        }

        //        xt.Flush();
        //        xt.BaseStream.Position = 0;

        //        return new StreamReader(xt.BaseStream).ReadToEnd();
        //}

        private bool bEntityIsValild(string xEntity, string xTemplate)
        {
            if (xEntity == string.Empty)
                return false;
            return true;
        }
        //GLOG : 8137 : JSW
        //checks recipients entity for number
        private bool bContainsNumber(string xEntity)
        {
            if (xEntity.Contains("0") || xEntity.Contains("1") || xEntity.Contains("2") || xEntity.Contains("3")
                || xEntity.Contains("4") || xEntity.Contains("5") || xEntity.Contains("6")
                || xEntity.Contains("7") || xEntity.Contains("8") || xEntity.Contains("9"))
                return true;
            else
                return false;
        }
        #endregion
    }
}
