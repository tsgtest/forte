using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using LMP.DocumentAnalyzer.MSWord;

namespace LMP.DocAnalyzer
{
    public class Element
    {
        # region Declarations

        private int m_iID;
        private string m_xName;
        private string m_xText;
        private long m_lStartRange;
        private long m_lEndRange;
        private int m_iNoMatch;
        private int m_iSortOrder;
        private bool m_bVisible;
        private string m_xDisplayName;
        private List<Instruction> m_oInstructions;
        public Word.Document m_oWordDoc;
        private ParsingInstruction m_ofldParse;
        private bool m_bEncrypted;

        # endregion

        public enum daNoMatch
        {
            Nothing = 1,
            True = 2,
            False = 3
        }

        # region Properties

        public List<Instruction> Instructions
        {
            get
            {
                if (m_oInstructions == null)
                {
                    m_oInstructions = GetInstructions();
                    Instructions = m_oInstructions;
                }

                return m_oInstructions;
            }
            set
            {
                m_oInstructions = value;
            }

        }

        public int ID
        {
            get { return m_iID; }
            set
            {
                m_iID = value;
            }
        }

        public int SortOrder
        {
            get { return m_iSortOrder; }
            set
            {
                m_iSortOrder = value;
            }
        }

        public bool Visible
        {
            get { return m_bVisible; }
            set
            {
                m_bVisible = value;
            }
        }

        public string Name
        {
            get { return m_xName; }
            set
            {
                m_xName = value;
            }
        }

        public string DisplayName
        {
            get { return m_xDisplayName; }
            set
            {
                m_xDisplayName = value;
            }
        }

        public string Text
        {
            get { return m_xText; }
            set
            {
                m_xText = value;
            }
        }

        public long StartRange
        {
            get { return m_lStartRange; }
            set
            {
                m_lStartRange = value;
            }
        }

        public long EndRange
        {
            get { return m_lEndRange; }
            set
            {
                m_lEndRange = value;
            }
        }


        public int NoMatch
        {
            get { return m_iNoMatch; }
            set
            {
                m_iNoMatch = value;
            }
        }

        public ParsingInstruction ParsingInstructions
        {
            get
            {
                return m_ofldParse;
            }
            set
            {
                m_ofldParse = value;
            }
        }
        public bool Encrypted
        {
            get
            {
                return m_bEncrypted;
            }
            set
            {
                m_bEncrypted = value;
            }
        }

        # endregion

        # region Constructors
        public Element()
        {

        }
        public Element(int iID)
        {
            ID = iID;
        }

        public Element(int iID, string xName, string xText, long lStartRange, long lEndRange, int iMatch)
        {
            ID = iID;
            Name = xName;
            Text = xName;
            StartRange = lStartRange;
            EndRange = lEndRange;
            NoMatch = iMatch;
            Instructions = m_oInstructions;
        }


        public Element(int iID, string xName, int iDocTypeID, int iMatch)
        {
            ID = iID;
            Name = xName;
            NoMatch = iMatch;
            Instructions = m_oInstructions;
        }


        public Element(int iID, string xName, int iDocTypeID, int iMatch, int iParseSource, string xTemplate)
        {
            ID = iID;
            Name = xName;
            NoMatch = iMatch;
            Instructions = m_oInstructions;
            ParsingInstructions = new ParsingInstruction(xTemplate, (Definitions.daParseSource)iParseSource);
        }
        public Element(int iID, string xName, int iDocTypeID, int iMatch, Word.Document oDoc, int iParseSource, 
            string xTemplate, string xDisplayName, int iSortOrder, bool bVisible)
        {
            ID = iID;
            Name = xName;
            SortOrder = iSortOrder;
            Visible = bVisible;
            DisplayName = xDisplayName;
            NoMatch = iMatch;
            Instructions = m_oInstructions;
            m_oWordDoc = oDoc;
            ParsingInstructions = new ParsingInstruction(xTemplate, (Definitions.daParseSource)iParseSource);
        }

        # endregion

        # region Methods


        /// <summary>
        ///Get the list of Instructions based on the ElementId
        /// </summary>
        public List<Instruction> GetInstructions()
        {
            try
            {
                m_oInstructions = Connection.GetInstructionsList(this.ID);
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return m_oInstructions;
        }

        /// <summary>
        /// Get the unparsed value of the Element from the document
        /// </summary>
        public string GetUnParsedValue()
        {
            string xElementValue = string.Empty;
            string xParsedValue = string.Empty;
            string xTemplate = string.Empty;
            string xParentElement = string.Empty;
            string xChildElement = string.Empty;
            string xParseError = string.Empty;        
            try
            {
                GetInstructions();
                if (m_oInstructions != null)
                {
                    //To Find a match, loop through  the instructionslist for the corresponding element id 
                    for (int i = 0; i < m_oInstructions.Count; i++)
                    {
                        if (this.ID == m_oInstructions[i].ElementID)
                        {
                            bool bGotValue = m_oInstructions[i].GetRawValue();
                            if (bGotValue)
                            {
                                if (m_oInstructions[i].Text == null)
                                    break;
                                xElementValue = m_oInstructions[i].Text;
                                if (xElementValue.StartsWith("~}"))
                                    this.Encrypted = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return xElementValue;
        }

        /// <summary>
        /// Get the parsed value of the Element from the document
        /// </summary>
        public string GetParsedValue()
        {

            //GLOG : 8137 : JSW
            //pass bIsRecipients parameter 
            bool bIsRecipients = false;

            string xParsedValue = "";
            if (!string.IsNullOrEmpty(ParsingInstructions.Template))
            {
                if (!string.IsNullOrEmpty(this.ParsingInstructions.Template))
                {
                    if (this.Name == "Recipients")
                        bIsRecipients = true;

                    ParsingInstructions.m_xElement = this.Name;
                    xParsedValue = ParsingInstructions.Execute(this.Text, bIsRecipients);
                    return xParsedValue;
                }
             }
            
            return this.Text;
        }
        /// <summary>
        /// Get the parsed value of the Element from the document
        /// </summary>
        public Word.Range[] GetRanges()
        {
            //Word.Range oRange = null;
            Word.Range[] oRanges = null;

            try
            {
                GetInstructions();
                if (m_oInstructions != null)
                {
                    //To Find a match, loop through  the instructionslist for the corresponding element id 
                    for (int i = 0; i < m_oInstructions.Count; i++)
                    {
                        if (this.ID == m_oInstructions[i].ElementID)
                        {
                            bool bGotValue = m_oInstructions[i].GetRawValue();
                            if (bGotValue)
                            {
                                if (m_oInstructions[i].Text == null)
                                    break;
                                
                                //oRange = m_oInstructions[i].Range;
                                oRanges = (Word.Range[])m_oInstructions[i].RangeArray;
                                break;
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return oRanges;
        }
        /// <summary>
        ///saves the changes to the Instruction.
        /// Inserting or updating the instruction
        /// </summary>
        /// <param name="iStory"></param>
        /// <param name="iExecutionIndex"></param>
        /// <param name="xWholeRange"></param>
        ///  <param name="xStartRange"></param>
        /// <param name="xEndRange"></param>
        /// <param name="xMatchCriteria"></param>
        /// <param name="xReturnData"></param>
        /// <param name="iNewInstruction"></param>
        /// <returns></returns>
        public bool SaveInstruction(int iStory, int iExecutionIndex, string xWholeRange, string xStartRange, string xEndRange, string xMatchCriteria, string xReturnData, bool iNewInstruction, int iMatchRangeType)
        {
            bool bSucessAdd = false;
            try
            {
                bSucessAdd = Connection.SaveInstruction(this.ID, iStory, iExecutionIndex, xWholeRange, xStartRange, xEndRange, xMatchCriteria, xReturnData, iNewInstruction, iMatchRangeType);
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return bSucessAdd;
        }

        /// <summary>
        /// Deleting the instruction 
        /// </summary>
        /// <param name="iExecutionIndex"></param>
        /// <returns></returns>
        public bool DeleteInstruction(int iExecutionIndex)
        {
            bool bSucessDelete = false;
            try
            {
                bSucessDelete = Connection.DeleteInstruction(this.ID, iExecutionIndex);
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return bSucessDelete;
        }
        # endregion

      
    }
}
