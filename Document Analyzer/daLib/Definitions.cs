using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.DocAnalyzer
{
    public class Definitions
    {
        public static bool m_bDocOpen = false;

        #region *********************properties*********************
        //public bool DocOpen
        //{
        //    get {return m_bDocOpen; }
        //    set
        //    {
        //        m_bDocOpen = value;
        //    }
        //}
        #endregion *********************properties*********************
        /// <summary>
        /// Provides values indicating whether we�re going 
        /// to parse the Instruction.Text or the Instruction.MatchRange.
        /// </summary>
        public enum daParseSource
        {
            daElementValue = 1,
            daElementMatchRange = 2
        }

        /// <summary>
        /// Method to Get the List of Document Types
        /// </summary>
        public static List<DocType> GetDocTypesList()
        {
            List<DocType> oDocTypesList = new List<DocType>();
            try
            {
                oDocTypesList = Connection.GetDocTypeList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oDocTypesList;
        }

        /// <summary>
        ///Method to Get the types of document
        /// </summary>      
        public static object GetTypesList()
        {
            List<KeyValuePair<string, int>> oTypeList = new List<KeyValuePair<string, int>>();
            oTypeList.Add(new KeyValuePair<string, int>("Template", 1));
            oTypeList.Add(new KeyValuePair<string, int>("Document Variable", 2));
            oTypeList.Add(new KeyValuePair<string, int>("Document Property", 3));
            oTypeList.Add(new KeyValuePair<string, int>("Bookmark", 4));
            oTypeList.Add(new KeyValuePair<string, int>("Text", 5));
            oTypeList.Add(new KeyValuePair<string,int>("Style", 6));

            return oTypeList;
        }

        /// <summary>
        ///Method to Trim blanks in the string
        /// </summary> 
        public static string TrimBlanks(string xSource)
        {
            char xChar;
            string xNewString=string.Empty;
            int iAsc;
            char xCr = (char)13;
            string xNewCell = "\r\a";
            string xNewLine = "\r\n";
            char xVerticalTab = (char)11;
            switch (xSource)
            {
                case "\\r":
                    xSource = xSource.Replace("\\r", xCr.ToString());
                    break;
                case "\\v":
                    xSource = xSource.Replace("\\v", xVerticalTab.ToString());
                    break;
                case "\\r\\a":
                    xSource = xSource.Replace("\\r\\a", xNewCell.ToString());
                    break;
                case "\\r\\n":
                    xSource = xSource.Replace("\\r\\n", xNewLine.ToString());
                    break;

            }
            for (int i = 0; i < xSource.Length; i++)
            {
                xChar = Convert.ToChar(xSource.Substring(i, 1));
                iAsc = (int)xChar;
                
                switch (iAsc)
                {
                    case 13:
                    case 11:
                    case 9:
                    case 8:
                    case 12:
                    case 10:                   
                        xNewString = xSource;
                        break;
                    case 7:
                        xSource = xSource.Replace("\a", "").Trim();
                        break;                   
                    
                }
                if (string.IsNullOrEmpty(xNewString))
                {
                    xNewString = xSource;
                }
               
            }
            return xNewString;
        }
    }
}
