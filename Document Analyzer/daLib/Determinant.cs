using System;
using System.Collections.Generic;
using System.Text;
using LMP.DocumentAnalyzer.MSWord;
namespace LMP.DocAnalyzer
{
    public class Determinant
    {
        #region***********************fields****************************
        private int m_iID;
        private DeterminantHelper.daDeterminantType m_iType;
        private string m_xValue;
        private string m_xKey;
        private int m_iDocTypeID;
        #endregion
        #region***********************constructors****************************
        public Determinant(int iID, DeterminantHelper.daDeterminantType iType, string xKey, string xValue, int iDocTypeID)
        {
            m_iID = iID;
            m_iType = iType;
            m_xValue = xValue;
            m_xKey = xKey;
            m_iDocTypeID = iDocTypeID;
        }
        public Determinant()
        {

        }
        #endregion
        #region***********************properties****************************
        public int ID
        {
            get { return m_iID; }
            set
            {
                m_iID = value;
            }
        }
        public DeterminantHelper.daDeterminantType Type
        {
            get { return m_iType; }
            set
            {
                m_iType = value;
            }
        }
        public string Value
        {
            get { return m_xValue; }
            set
            {
                m_xValue = value;
            }
        }
        public string Key
        {
            get { return m_xKey; }
            set
            {
                m_xKey = value;
            }
        }
        public int DocTypeID
        {
            get { return m_iDocTypeID; }
            set
            {
                m_iDocTypeID = value;
            }
        }
        #endregion
    }
}
