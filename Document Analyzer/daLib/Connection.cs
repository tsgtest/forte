using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using LMP.DocAnalyzer;
using System.Reflection;
using LMP.DocumentAnalyzer.MSWord;
using Word = Microsoft.Office.Interop.Word;
using System.Data;
using System.Windows.Forms;

namespace LMP.DocAnalyzer
{
    internal static class Connection
    {
        #region***********************fields****************************
        private static OleDbConnection m_oCnn = null;
        private static string m_xConnectionProvider = null; //GLOG : 8500 : jsw
        # endregion
        #region***********************constructors****************************
        static Connection()
        {
            try
            {
                ConnectIfNecessary();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region***********************methods****************************
        /// <summary>
        /// returns the doc type name of the doc type with the specified ID
        /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <returns></returns>
        static internal string GetDocTypeName(int iDocTypeID)
        {
            ConnectIfNecessary();
            string xDocTypeName = string.Empty;
            OleDbParameter oParam = new OleDbParameter();
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);

                    oCmd.CommandText = "EXECUTE qryGetDocTypes";

                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                xDocTypeName = (string)oReader.GetValue(1);
                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xDocTypeName;
        }
        /// <summary>
        /// returns the doc type name of the doc type with the specified ID
        /// </summary>
        /// <param name="xDocTypeName"></param>
        /// <returns>int</returns>
        static internal int GetDocTypeID(string xDocTypeName)
        {
            ConnectIfNecessary();
            OleDbParameter oParam = new OleDbParameter();
            int iDocTypeID = 0;
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("Name", xDocTypeName);
                    oCmd.Parameters.Add(oParam);

                    oCmd.CommandText = "EXECUTE qryGetDocTypesFromName";
                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                iDocTypeID = Convert.ToInt32(oReader.GetValue(0).ToString());
                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iDocTypeID;
        }
        /// <summary>
        /// returns the doctype id of the determinant that
        /// matches the specified parameters
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        internal static int GetMatchingDocTypeID(DeterminantHelper.daDeterminantType iType, string xKey, string xValue)
        {
            ConnectIfNecessary();

            using (OleDbCommand oCmd = m_oCnn.CreateCommand())
            {
                oCmd.CommandType = CommandType.StoredProcedure;

                if (iType == DeterminantHelper.daDeterminantType.Template || iType == DeterminantHelper.daDeterminantType.Text)
                {   
                    oCmd.Parameters.AddRange(new OleDbParameter[] 
                    {new OleDbParameter("DocObjType", iType),
                    new OleDbParameter("DocObjValue", xValue)});
                    oCmd.CommandText = "qryGetDocTypeFromTemplateOrTextDeterminant";
               
                }
                else
                {
                    oCmd.Parameters.AddRange(new OleDbParameter[] 
                    {new OleDbParameter("DocObjType", iType),
                    new OleDbParameter("DocObjName", xKey),
                    new OleDbParameter("DocObjValue", xValue)});
                    oCmd.CommandText = "qryGetDocTypeFromDeterminant";
                }
                object oRet = oCmd.ExecuteScalar();
                if (oRet == null)
                    return 0;
                else
                    return (int)oRet;

            }
        }
        /// <summary>
        /// returns the list of elements for the doc type with the specified ID
        /// </summary>
        /// <param name="DocTypeId"></param>
        /// <returns></returns>
        static internal List<Element> GetElementsList(int iDocTypeID, Word.Document oDoc)
        {
            ConnectIfNecessary();
            List<Element> oElementsList = new List<Element>();
            OleDbParameter oParam = new OleDbParameter();
            string xElementName = string.Empty;
            string xTemplate = string.Empty;
            int iSource=-1;
            string xDisplayName = string.Empty;
            int iSortOrder = 0;
            bool bVisible = false;

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    //"ID" is name of the Parameter the qryGetElements
                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);
                    oCmd.CommandText = "EXECUTE qryGetElements";
                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(1)))
                                {
                                    xElementName = (string)oReader.GetValue(1);
                                }
                                else
                                {
                                    xElementName = string.Empty;
                                }
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(5).ToString()))
                                {
                                    xTemplate = (string)oReader.GetValue(5);
                                }
                                else
                                {
                                    xTemplate = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(oReader.GetValue(4).ToString()))
                                {
                                    iSource = (int)oReader.GetValue(4);
                                }
                                else
                                {
                                    iSource = 0;
                                }
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(6).ToString()))
                                {
                                    xDisplayName = (string)oReader.GetValue(6);
                                }
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(7).ToString()))
                                {
                                    iSortOrder = (int)oReader.GetValue(7);
                                }
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(8).ToString()))
                                {
                                    bVisible = (bool)oReader.GetValue(8);
                                }

                                oElementsList.Add(new Element((int)oReader.GetValue(0),
                                   xElementName, (int)oReader.GetValue(2),
                                   (int)oReader.GetValue(3), oDoc, iSource, xTemplate, xDisplayName, iSortOrder, bVisible));

                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oElementsList;
        }

        /// <summary>
        /// returns the list of elements for the doc type with the specified ID
        /// </summary>
        /// <param name="DocTypeId"></param>
        /// <returns></returns>
        static internal Element GetElement(int iDocTypeID, int iElementID)
        {
            ConnectIfNecessary();
            OleDbParameter oParam = new OleDbParameter();
            string xElementName = string.Empty;
            string xTemplate = string.Empty;
            int iSource = -1;
            Element oElement = null;
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    //"ID" is name of the Parameter the qryGetElements
                    oParam = new OleDbParameter("DocTypeID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);
                    oParam = new OleDbParameter("ElementID", iElementID);
                    oCmd.Parameters.Add(oParam);
                    oCmd.CommandText = "EXECUTE qryGetElement";
                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(1)))
                                {
                                    xElementName = (string)oReader.GetValue(1);
                                }
                                else
                                {
                                    xElementName = string.Empty;
                                }
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(5).ToString()))
                                {
                                    xTemplate = (string)oReader.GetValue(5);
                                }
                                else
                                {
                                    xTemplate = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(oReader.GetValue(4).ToString()))
                                {
                                    iSource = (int)oReader.GetValue(4);
                                }
                                else
                                {
                                    iSource = 0;
                                }
                                oElement = new Element((int)oReader.GetValue(0),
                                   xElementName, (int)oReader.GetValue(2),
                                   (int)oReader.GetValue(3), iSource, xTemplate);

                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oElement;
        }
        /// <summary>
        /// returns the list of defined determinants
        /// </summary>
        /// <returns></returns>
        static internal List<Determinant> GetDeterminantsList()
        {
            ConnectIfNecessary();
            List<Determinant> oDeterList = new List<Determinant>();
            OleDbDataReader oReader = null;
            string xKey = string.Empty;
            string xValue = string.Empty;
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oCmd.CommandText = "EXECUTE qryGetDeterminantsList";
                    using (oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                if (!string.IsNullOrEmpty(oReader.GetValue(2).ToString()))
                                    xKey = (string)oReader.GetValue(2);
                                else
                                    xKey = string.Empty;

                                if (!string.IsNullOrEmpty(oReader.GetValue(3).ToString()))
                                    xValue = (string)oReader.GetValue(3);
                                else
                                    xValue = string.Empty;

                                Determinant oD = new Determinant((int)oReader.GetValue(0), (DeterminantHelper.daDeterminantType)oReader.GetValue(1), xKey, xValue, (int)oReader.GetValue(4));
                                oDeterList.Add(oD);
                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oDeterList;
        }

        /// <summary>
        /// returns the instruction list for the 
        /// element with the specified ID
        /// </summary>
        /// <param name="ElementID"></param>
        /// <returns></returns>
        static internal List<Instruction> GetInstructionsList(int iElementID)
        {
            ConnectIfNecessary();
            List<Instruction> oInstructionsList = new List<Instruction>();
            OleDbParameter oParam = new OleDbParameter();
            string xStartValue = "";
            string xEndValue = "";
            string xWholeValue = "";
            try
            {
                if (iElementID == 0)
                {
                    Instruction oInstruction = new Instruction();
                    oInstruction.ElementID = 0;
                    oInstruction.EndRangeName = "0";
                    oInstruction.EndRangeOperator = 0;
                    oInstruction.EndRangePosition = 0;
                    oInstruction.EndRangeSection = 0;
                    oInstruction.EndRangeType = 0;
                    oInstruction.EndRangeValue = "0";
                    oInstruction.ExecutionIndex = 1;
                    oInstruction.MatchCriteriaName = "Recipients";
                    oInstruction.MatchCriteriaType = 16;
                    oInstruction.MatchCriteriaValue = "";
                    oInstruction.MatchRangeType = 0;
                    oInstruction.ReturnDataType = 0;
                    oInstruction.ReturnDataUnit = 8;
                    oInstruction.Story = 1;
                    oInstruction.StartRangeType = 0;
                    oInstruction.StartRangeName = "";
                    oInstruction.StartRangeOperator = 0;
                    oInstruction.StartRangeValue = "";
                    oInstruction.StartRangeSection = 0;
                    oInstruction.StartRangePosition = 0;
                    oInstruction.WholeRangeType = 0;
                    oInstruction.WholeRangeName = "";
                    oInstruction.WholeRangeOperator = 0;
                    oInstruction.WholeRangeValue = "";
                    oInstruction.WholeRangeSection = 0;
                    oInstruction.WholeRangePosition = 0;
                    oInstruction.Range = GetSelectionRange();
                    oInstruction.WordDocument = GetWordDoc();

                    oInstructionsList.Add(oInstruction);
                }
                
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oParam = new OleDbParameter("ID", iElementID);
                    oCmd.Parameters.Add(oParam);
                    oCmd.CommandText = "EXECUTE qryGetInstructions";
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                        //Assign retrieved values from the query to the Instruction
                        while (oReader.Read())
                        {
                            Instruction oInstruction = new Instruction();

                            # region Initialize

                            oInstruction.StartRangeType = 0;
                            oInstruction.StartRangeName = "";
                            oInstruction.StartRangeOperator = 0;
                            oInstruction.StartRangeValue = "";
                            oInstruction.StartRangeSection = 0;
                            oInstruction.StartRangePosition = 0;
                            oInstruction.EndRangeType = 0;
                            oInstruction.EndRangeName = "";
                            oInstruction.EndRangeOperator = 0;
                            oInstruction.EndRangeValue = "";
                            oInstruction.EndRangeSection = 0;
                            oInstruction.EndRangePosition = 0;

                            # endregion
                            //Assign the fldElementID
                            oInstruction.ElementID = (short)(Convert.ToInt32(oReader.GetValue(0)));
                            //Assign the ExecutionIndex
                            oInstruction.ExecutionIndex = (short)(Convert.ToInt32(oReader.GetValue(1)));
                            //Assign the Story
                            oInstruction.Story = (short)(Convert.ToInt32(oReader.GetValue(2)));

                            oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(8)));
                            # region WholeRange Instructions

                            //Find the occurences of the Char "�" in the fldWholeRange
                            int[] iWholeRangeVal = new int[oReader.GetValue(3).ToString().Length];
                            int iWholeRangeValCount = 0;

                            for (int i = 1; i < oReader.GetValue(3).ToString().Length; i++)
                            {

                                if (oReader.GetValue(3).ToString().Substring(i, 1) == "�")
                                {
                                    iWholeRangeVal[iWholeRangeValCount] = i;
                                    iWholeRangeValCount++;
                                }
                            }
                            //Based on the No. of occurences of the char "�" in the string assign the value to the WholeRange Instructions
                            switch (iWholeRangeValCount)
                            {
                                case 0:
                                    oInstruction.WholeRangeType = 0;
                                    oInstruction.WholeRangeName = "";
                                    oInstruction.WholeRangeOperator = 0;
                                    oInstruction.WholeRangeValue = "";
                                    oInstruction.WholeRangeSection = 0;
                                    oInstruction.WholeRangePosition = 0;
                                    break;
                                case 1:
                                    //oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().Substring(0, 1)));
                                    oInstruction.WholeRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeName = oReader.GetValue(3).ToString().Substring(2, (oReader.GetValue(3).ToString().Length - 3));
                                    break;
                                case 2:
                                    //oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeName = oReader.GetValue(3).ToString().Substring(2, (iWholeRangeVal[1] - 2));
                                    oInstruction.WholeRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().Substring(iWholeRangeVal[1] + 1, 1)));
                                    break;
                                case 3:
                                    //oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeName = oReader.GetValue(3).ToString().Substring(2, (iWholeRangeVal[1] - 2));
                                    oInstruction.WholeRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().Substring(iWholeRangeVal[1] + 1, 1)));
                                    xWholeValue = oReader.GetValue(3).ToString().Substring(iWholeRangeVal[2] + 1, oReader.GetValue(3).ToString().Length - (iWholeRangeVal[2] + 1));
                                    iWholeRangeValCount = 0;
                                    iWholeRangeVal = new int[xWholeValue.ToString().Length];
                                    for (int j = 1; j < xWholeValue.Length - 1; j++)
                                    {
                                        if (xWholeValue.Substring(j, 1) == "�")
                                        {
                                            iWholeRangeVal[iWholeRangeValCount] = j;
                                            iWholeRangeValCount++;
                                        }
                                    }
                                    if (iWholeRangeVal[0] <= xWholeValue.Length)
                                    {
                                        oInstruction.WholeRangeValue = xWholeValue.Substring(0, (xWholeValue.Length - (xWholeValue.Length - (iWholeRangeVal[0]))));
                                    }
                                    break;
                                case 4:
                                    //  oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeName = oReader.GetValue(3).ToString().Substring(2, (iWholeRangeVal[1] - 2));
                                    oInstruction.WholeRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().Substring(iWholeRangeVal[1] + 1, 1)));
                                    xWholeValue = oReader.GetValue(3).ToString().Substring(iWholeRangeVal[2] + 1, oReader.GetValue(3).ToString().Length - (iWholeRangeVal[2] + 1));
                                    iWholeRangeValCount = 0;
                                    iWholeRangeVal = new int[xWholeValue.ToString().Length];
                                    for (int j = 1; j < xWholeValue.Length - 1; j++)
                                    {
                                        if (xWholeValue.Substring(j, 1) == "�")
                                        {
                                            iWholeRangeVal[iWholeRangeValCount] = j;
                                            iWholeRangeValCount++;
                                        }
                                    }
                                    if (iWholeRangeVal[0] <= xWholeValue.Length)
                                    {
                                        oInstruction.WholeRangeValue = xWholeValue.Substring(0, (xWholeValue.Length - (xWholeValue.Length - (iWholeRangeVal[0]))));
                                        oInstruction.WholeRangeSection = (short)(Convert.ToInt32(xWholeValue.Substring(xWholeValue[0] + 1, 1)));
                                    }
                                    break;
                                case 5:
                                    //  oInstruction.MatchRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeType = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.WholeRangeName = oReader.GetValue(3).ToString().Substring(2, (iWholeRangeVal[1] - 2));
                                    oInstruction.WholeRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(3).ToString().Substring(iWholeRangeVal[1] + 1, 1)));
                                    xWholeValue = oReader.GetValue(3).ToString().Substring(iWholeRangeVal[2] + 1, oReader.GetValue(3).ToString().Length - (iWholeRangeVal[2] + 1));
                                    iWholeRangeValCount = 0;
                                    iWholeRangeVal = new int[xWholeValue.ToString().Length];
                                    for (int j = 1; j < xWholeValue.Length - 1; j++)
                                    {
                                        if (xWholeValue.Substring(j, 1) == "�")
                                        {
                                            iWholeRangeVal[iWholeRangeValCount] = j;
                                            iWholeRangeValCount++;
                                        }
                                    }
                                    if (iWholeRangeVal[0] <= xWholeValue.Length)
                                    {
                                        oInstruction.WholeRangeValue = xWholeValue.Substring(0, (xWholeValue.Length - (xWholeValue.Length - (iWholeRangeVal[0]))));

                                        oInstruction.WholeRangeSection = (short)(Convert.ToInt32(xWholeValue.Substring(iWholeRangeVal[0] + 1, 1)));
                                        if (iWholeRangeValCount > 1)
                                        {
                                            oInstruction.WholeRangePosition = (short)(Convert.ToInt32(xWholeValue.Substring(iWholeRangeVal[1] + 1, 1)));
                                        }
                                    }

                                    break;

                            }

                            # endregion

                            if (!string.IsNullOrEmpty(oInstruction.WholeRangeName))
                            {
                                oInstruction.StartRangeType = 0;
                                oInstruction.StartRangeName = "";
                                oInstruction.StartRangeOperator = 0;
                                oInstruction.StartRangeValue = "";
                                oInstruction.StartRangeSection = 0;
                                oInstruction.StartRangePosition = 0;
                                oInstruction.EndRangeType = 0;
                                oInstruction.EndRangeName = "";
                                oInstruction.EndRangeOperator = 0;
                                oInstruction.EndRangeValue = "";
                                oInstruction.EndRangeSection = 0;
                                oInstruction.EndRangePosition = 0;
                                //oInstruction.MatchRangeType = 1;
                            }
                            else if (string.IsNullOrEmpty(oInstruction.WholeRangeName))
                            {
                                # region StartRange Instructions
                                //oInstruction.MatchRangeType = 0;
                                int[] iStartRangeVal = new int[((string)oReader.GetValue(4)).Length];
                                int iStartRangeValCount = 0;

                                for (int j = 1; j < oReader.GetValue(4).ToString().Length - 1; j++)
                                {
                                    if (oReader.GetValue(4).ToString().Substring(j, 1) == "�")
                                    {
                                        iStartRangeVal[iStartRangeValCount] = j;
                                        iStartRangeValCount++;
                                    }
                                }

                                switch (iStartRangeValCount)
                                {
                                    case 0:
                                        oInstruction.StartRangeType = 0;
                                        oInstruction.StartRangeName = "";
                                        oInstruction.StartRangeOperator = 0;
                                        oInstruction.StartRangeValue = "";
                                        oInstruction.StartRangeSection = 0;
                                        oInstruction.StartRangePosition = 0;
                                        break;
                                    case 1:
                                        oInstruction.StartRangeType = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.StartRangeName = oReader.GetValue(4).ToString().Substring(2, (oReader.GetValue(4).ToString().Length - 3));
                                        break;
                                    case 2:
                                        oInstruction.StartRangeType = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.StartRangeName = oReader.GetValue(4).ToString().Substring(2, (iStartRangeVal[1]-2));
                                        oInstruction.StartRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().Substring(iStartRangeVal[1] + 1, 1)));
                                        break;
                                    case 3:
                                        oInstruction.StartRangeType = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.StartRangeName = oReader.GetValue(4).ToString().Substring(2, (iStartRangeVal[1] - 2));
                                        oInstruction.StartRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().Substring(iStartRangeVal[1] + 1, 1)));
                                        xStartValue = oReader.GetValue(4).ToString().Substring(iStartRangeVal[2] + 1, oReader.GetValue(4).ToString().Length - (iStartRangeVal[2] + 1));
                                        iStartRangeValCount = 0;
                                        iStartRangeVal = new int[xStartValue.ToString().Length];
                                        for (int j = 1; j < xStartValue.Length - 1; j++)
                                        {
                                            if (xStartValue.Substring(j, 1) == "�")
                                            {
                                                iStartRangeVal[iStartRangeValCount] = j;
                                                iStartRangeValCount++;
                                            }
                                        }
                                        if (iStartRangeVal[0] <= xStartValue.Length)
                                        {
                                            oInstruction.StartRangeValue = xStartValue.Substring(0, (xStartValue.Length - (xStartValue.Length - (iStartRangeVal[0]))));
                                        }
                                        break;
                                    case 4:
                                        oInstruction.StartRangeType = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.StartRangeName = oReader.GetValue(4).ToString().Substring(2, (iStartRangeVal[1] - 2));
                                        oInstruction.StartRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().Substring(iStartRangeVal[1] + 1, 1)));
                                        xStartValue = oReader.GetValue(4).ToString().Substring(iStartRangeVal[2] + 1, oReader.GetValue(4).ToString().Length - (iStartRangeVal[2] + 1));
                                        iStartRangeValCount = 0;
                                        iStartRangeVal = new int[xStartValue.ToString().Length];
                                        for (int j = 1; j < xStartValue.Length - 1; j++)
                                        {
                                            if (xStartValue.Substring(j, 1) == "�")
                                            {
                                                iStartRangeVal[iStartRangeValCount] = j;
                                                iStartRangeValCount++;
                                            }
                                        }                                       
                                        if (iStartRangeVal[0] <= xStartValue.Length)
                                        {
                                            oInstruction.StartRangeValue = xStartValue.Substring(0, (xStartValue.Length-(xStartValue.Length - (iStartRangeVal[0]))));
                                            oInstruction.StartRangeSection = (short)(Convert.ToInt32(xStartValue.Substring(iStartRangeVal[0] + 1, 1)));
                                        }
                                        break;
                                    case 5:
                                        oInstruction.StartRangeType = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.StartRangeName = oReader.GetValue(4).ToString().Substring(2, (iStartRangeVal[1] - 2));
                                        oInstruction.StartRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(4).ToString().Substring(iStartRangeVal[1] + 1, 1)));
                                        xStartValue = oReader.GetValue(4).ToString().Substring(iStartRangeVal[2] + 1, oReader.GetValue(4).ToString().Length - (iStartRangeVal[2] + 1));
                                        iStartRangeValCount = 0;
                                        iStartRangeVal = new int[xStartValue.ToString().Length];
                                        for (int j = 1; j < xStartValue.Length - 1; j++)
                                        {
                                            if (xStartValue.Substring(j, 1) == "�")
                                            {
                                                iStartRangeVal[iStartRangeValCount] = j;
                                                iStartRangeValCount++;
                                            }
                                        }                                       
                                        if (iStartRangeVal[0] <= xStartValue.Length)
                                        {
                                            oInstruction.StartRangeValue = xStartValue.Substring(0, (xStartValue.Length-(xStartValue.Length - iStartRangeVal[0])));
                                            oInstruction.StartRangeSection = (short)(Convert.ToInt32(xStartValue.Substring(iStartRangeVal[0] + 1, 1)));
                                            if (iStartRangeValCount > 1)
                                            {
                                                oInstruction.StartRangePosition = (short)(Convert.ToInt32(xStartValue.Substring(iStartRangeVal[1] + 1, 1)));
                                            }
                                        }
                                        break;


                                }

                                # endregion

                                # region EndRange Instructions

                                int[] iEndRangeVals = new int[((string)oReader.GetValue(5)).Length];
                                int iEndRangeValCount = 0;

                                for (int k = 1; k < oReader.GetValue(5).ToString().Length; k++)
                                {
                                    if (oReader.GetValue(5).ToString().Substring(k, 1) == "�")
                                    {
                                        iEndRangeVals[iEndRangeValCount] = k;
                                        iEndRangeValCount++;
                                    }
                                }

                                switch (iEndRangeValCount)
                                {
                                    case 0:
                                        oInstruction.EndRangeType = 0;
                                        oInstruction.EndRangeName = "";
                                        oInstruction.EndRangeOperator = 0;
                                        oInstruction.EndRangeValue = "";
                                        oInstruction.EndRangeSection = 0;
                                        oInstruction.EndRangePosition = 0;
                                        break;
                                    case 1:
                                        oInstruction.EndRangeType = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.EndRangeName = oReader.GetValue(5).ToString().Substring(2, (oReader.GetValue(5).ToString().Length - 3));
                                        break;
                                    case 2:
                                        oInstruction.EndRangeType = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.EndRangeName = oReader.GetValue(5).ToString().Substring(2, (iEndRangeVals[1] - 2));
                                        oInstruction.EndRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().Substring(iEndRangeVals[1] + 1, 1)));
                                        break;
                                    case 3:
                                        oInstruction.EndRangeType = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.EndRangeName = oReader.GetValue(5).ToString().Substring(2, (iEndRangeVals[1] - 2));
                                        oInstruction.EndRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().Substring(iEndRangeVals[1] + 1, 1)));
                                        xEndValue = oReader.GetValue(5).ToString().Substring(iEndRangeVals[2] + 1, oReader.GetValue(5).ToString().Length - (iEndRangeVals[2] + 1));
                                        iEndRangeValCount = 0;
                                        iEndRangeVals = new int[xEndValue.ToString().Length];
                                        for (int j = 1; j < xEndValue.Length - 1; j++)
                                        {
                                            if (xEndValue.Substring(j, 1) == "�")
                                            {
                                                iEndRangeVals[iEndRangeValCount] = j;
                                                iEndRangeValCount++;
                                            }
                                        }
                                        if (iEndRangeVals[0] <= xEndValue.Length)
                                        {
                                            oInstruction.EndRangeValue = xEndValue.Substring(0, (xEndValue.Length - (xEndValue.Length - (iEndRangeVals[0]))));
                                        }
                                        break;
                                    case 4:
                                        oInstruction.EndRangeType = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.EndRangeName = oReader.GetValue(5).ToString().Substring(2, (iEndRangeVals[1] - 2));
                                        oInstruction.EndRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().Substring(iEndRangeVals[1] + 1, 1)));
                                        oInstruction.EndRangeValue = oReader.GetValue(5).ToString().Substring(iEndRangeVals[2] + 1, oReader.GetValue(5).ToString().Length - (iEndRangeVals[2] + 1));
                                        xEndValue = oReader.GetValue(5).ToString().Substring(iEndRangeVals[2] + 1, oReader.GetValue(5).ToString().Length - (iEndRangeVals[2] + 1));
                                        iEndRangeValCount = 0;
                                        iEndRangeVals = new int[xEndValue.ToString().Length];
                                        for (int j = 1; j < xEndValue.Length - 1; j++)
                                        {
                                            if (xEndValue.Substring(j, 1) == "�")
                                            {
                                                iEndRangeVals[iEndRangeValCount] = j;
                                                iEndRangeValCount++;
                                            }
                                        }
                                        if (iEndRangeVals[0] <= xEndValue.Length)
                                        {
                                            oInstruction.EndRangeValue = xEndValue.Substring(0, (xEndValue.Length - (xEndValue.Length - (iEndRangeVals[0]))));
                                            oInstruction.EndRangeSection = (short)(Convert.ToInt32(xEndValue.Substring(iEndRangeVals[0] + 1, 1)));
                                        }
                                        break;
                                    case 5:
                                        oInstruction.EndRangeType = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().TrimStart().Substring(0, 1)));
                                        oInstruction.EndRangeName = oReader.GetValue(5).ToString().Substring(2, (iEndRangeVals[1] - 2));
                                        oInstruction.EndRangeOperator = (short)(Convert.ToInt32(oReader.GetValue(5).ToString().Substring(iEndRangeVals[1] + 1, 1)));
                                        xEndValue = oReader.GetValue(5).ToString().Substring(iEndRangeVals[2] + 1, oReader.GetValue(5).ToString().Length - (iEndRangeVals[2] + 1));
                                        iEndRangeValCount = 0;
                                        iEndRangeVals = new int[xEndValue.ToString().Length];
                                        for (int j = 1; j < xEndValue.Length - 1; j++)
                                        {
                                            if (xEndValue.Substring(j, 1) == "�")
                                            {
                                                iEndRangeVals[iEndRangeValCount] = j;
                                                iEndRangeValCount++;
                                            }
                                        }
                                        if (iEndRangeVals[0] <= xEndValue.Length)
                                        {
                                            oInstruction.EndRangeValue = xEndValue.Substring(0, (xEndValue.Length - (xEndValue.Length - (iEndRangeVals[0]))));

                                            oInstruction.EndRangeSection = (short)(Convert.ToInt32(xEndValue.Substring(iEndRangeVals[0] + 1, 1)));
                                            if (iEndRangeValCount > 1)
                                            {
                                                oInstruction.EndRangePosition = (short)(Convert.ToInt32(xEndValue.Substring(iEndRangeVals[1] + 1, 1)));
                                            }
                                        }

                                        break;
                                }

                                # endregion
                            }

                            # region Match Instructions

                            int iMatchCriteriaLength = 0;
                            int iMatchFirstLocation = 0;
                            int iMatchNextLocation = 0;
                            if (string.IsNullOrEmpty(oReader.GetValue(6).ToString()))
                            {
                                oInstruction.MatchCriteriaName = "";
                                oInstruction.MatchCriteriaType = 0;
                                oInstruction.MatchCriteriaValue = "";
                            }
                            else
                            {
                                iMatchCriteriaLength = oReader.GetValue(6).ToString().Length;
                                string xMatchCriteria = oReader.GetValue(6).ToString().TrimStart();
                                if (iMatchCriteriaLength > 0)
                                {
                                   iMatchFirstLocation = xMatchCriteria.IndexOf("�");
                                   iMatchNextLocation = xMatchCriteria.LastIndexOf("�");
                                   if (iMatchFirstLocation > -1)
                                   {
                                        oInstruction.MatchCriteriaType = (short)(Convert.ToInt32(xMatchCriteria.Substring(0, iMatchFirstLocation)));
                                        if (!xMatchCriteria.Substring(iMatchFirstLocation + 1).StartsWith("�"))
                                        {
                                            if (iMatchNextLocation > iMatchFirstLocation)
                                                oInstruction.MatchCriteriaName = xMatchCriteria.Substring(iMatchFirstLocation + 1, iMatchNextLocation - iMatchFirstLocation);
                                            else
                                                oInstruction.MatchCriteriaName = xMatchCriteria.Substring(iMatchFirstLocation + 1);
                                        }
                                        else
                                        {
                                            oInstruction.MatchCriteriaName = "";
                                        }
                                        if (iMatchFirstLocation == iMatchNextLocation)
                                        {
                                            oInstruction.MatchCriteriaValue = "";
                                        }
                                        else
                                        {
                                            oInstruction.MatchCriteriaValue = xMatchCriteria.Substring(iMatchNextLocation + 1);
                                        }
                                    }
                                    else
                                    {
                                       oInstruction.MatchCriteriaType = (short)(Convert.ToInt32(xMatchCriteria));
                                       oInstruction.MatchCriteriaName = "";
                                       oInstruction.MatchCriteriaValue = "";
                                       
                                    }
                                }
                                else
                                {
                                    iMatchFirstLocation = 0;
                                    oInstruction.MatchCriteriaType = 0;
                                    oInstruction.MatchCriteriaName = "";
                                    oInstruction.MatchCriteriaValue = "";
                                }
                            }

                            # endregion

                            # region Range Instructions
                            string xReturnUnit = string.Empty;
                            if (string.IsNullOrEmpty(oReader.GetValue(7).ToString()))
                            {
                                oInstruction.ReturnDataType = 0;
                                oInstruction.ReturnDataUnit = 0;
                            }
                            else
                            {

                                int iRangeIndex = oReader.GetValue(7).ToString().IndexOf("�");
                                if (iRangeIndex > 0)
                                {
                                    oInstruction.ReturnDataType = (short)(Convert.ToInt32(oReader.GetValue(7).ToString().TrimStart().Substring(0, iRangeIndex)));
                                    oInstruction.ReturnDataUnit = (short)(Convert.ToInt32(oReader.GetValue(7).ToString().TrimStart().Substring(iRangeIndex + 1, 1)));
                                }
                                else
                                {
                                    oInstruction.ReturnDataType = (short)(Convert.ToInt32(oReader.GetValue(7).ToString().TrimStart().Substring(0, 1)));
                                    oInstruction.ReturnDataUnit = 0;
                                }
                            # endregion
                            }

                            # region Assign Document
                            if (Definitions.m_bDocOpen)
                            {
                                //SetWordDocProperty(oInstruction);
                                oInstruction.WordDocument = GetWordDoc();
                                oInstruction.Range = GetSelectionRange();
                            }

                            # endregion

                            //# region Instructions List

                            oInstructionsList.Add(oInstruction);

                            //# endregion
                        }

                        oReader.Close();
                    oCmd.Dispose();
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oInstructionsList;

        }

        static internal void SetWordDocProperty(Instruction oInstruction)
        {
            oInstruction.WordDocument = Application.oWord.ActiveDocument;
        }
        static internal Word.Document GetWordDoc()
        {
            return Application.oWord.ActiveDocument;
        }
        static internal Word.Range GetSelectionRange()
        {
            return Application.oWord.Selection.Range;
        }
        /// <summary>
        /// returns the DocType list 
        /// </summary>
        /// <returns></returns>
        static internal List<DocType> GetDocTypeList()
        {
            ConnectIfNecessary();

            List<DocType> oDocTypesList = new List<DocType>();
            string xDocType = string.Empty;
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oCmd.CommandText = "EXECUTE qryGetDocTypesList";
                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                if (!string.IsNullOrEmpty((string)oReader.GetValue(1)))
                                {
                                    xDocType = (string)oReader.GetValue(1);
                                }
                                else
                                {
                                    xDocType = string.Empty;
                                }

                                oDocTypesList.Add(new DocType((int)oReader.GetValue(0), xDocType));

                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oDocTypesList;
        }

        /// <summary>
        /// returns the Determinant list for the 
        /// specified DocTypeID
        /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <returns></returns>
        static internal List<Determinant> GetDeterminants(int iDocTypeID)
        {
            ConnectIfNecessary();
            List<Determinant> oDeterminantsList = new List<Determinant>();
            OleDbParameter oParam = new OleDbParameter();
            string xDeterminantKey = string.Empty;
            string xDeterminantValue = string.Empty;
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);
                    oCmd.CommandText = "EXECUTE qryGetDeterminants";
                    using (OleDbDataReader oReader = oCmd.ExecuteReader())
                    {
                        try
                        {
                            while (oReader.Read())
                            {
                                if (!string.IsNullOrEmpty(oReader.GetValue(2).ToString()))
                                {
                                    xDeterminantKey = (string)oReader.GetValue(2);
                                }
                                else
                                {
                                    xDeterminantKey = string.Empty;
                                }
                                if (!string.IsNullOrEmpty(oReader.GetValue(3).ToString()))
                                {
                                    xDeterminantValue = (string)oReader.GetValue(3);
                                }
                                else
                                {
                                    xDeterminantValue = string.Empty;
                                }
                                oDeterminantsList.Add(new Determinant((int)oReader.GetValue(0), (DeterminantHelper.daDeterminantType)oReader.GetValue(1),
                                   xDeterminantKey, xDeterminantValue, (int)oReader.GetValue(4)));

                            }
                        }
                        finally
                        {
                            oReader.Close();
                        }
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return oDeterminantsList;
        }

        /// <summary>
        /// Adds the New DocType 
        /// </summary>
        /// <param name="iDocTypeName"></param>
        /// <returns></returns>
        static internal int AddDocType(string iDocTypeName)
        {
            bool bInsertDocType = false;
            int iInsertedDocTypeId = 0;
            ConnectIfNecessary();
            OleDbParameter oParam = new OleDbParameter();
            OleDbCommand oCmd = new OleDbCommand();

            try
            {
                using (OleDbCommand oCmd1 = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("Name", iDocTypeName);
                    oCmd1.Parameters.Add(oParam);

                    oCmd1.CommandText = "EXECUTE qryInsertDocTypes";
                    OleDbDataReader oReader = oCmd1.ExecuteReader();
                    bInsertDocType = true;
                    oReader.Close();
                    if (bInsertDocType)
                    {
                        oCmd = new OleDbCommand("SELECT @@IDENTITY", m_oCnn);
                        iInsertedDocTypeId = (int)oCmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            finally
            {
                oCmd.Dispose();
            }
            return iInsertedDocTypeId;
        }

        /// <summary>
        /// Deletes DocType and associated Elements,Instructions,Determinants
        /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <returns></returns>
        static internal void DeleteDocType(int iDocTypeID)
        {
            ConnectIfNecessary();
            OleDbParameter oParam = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);
 
                    oCmd.CommandText = "EXECUTE qryDeleteAssociatedDocTypeInstructions";
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                    oReader.Close();
                    oCmd.CommandText = "EXECUTE qryDeleteElement";
                    oReader = oCmd.ExecuteReader();
                    oReader.Close();
                    oCmd.CommandText = "EXECUTE qryDeleteDocTypes";
                    oReader = oCmd.ExecuteReader();
                    oReader.Close();
                    oCmd.CommandText = "EXECUTE qryDeleteDeterminants";
                    oReader = oCmd.ExecuteReader();
                    oReader.Close();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Updates Elements when newly added,Existing updated or when element deleted        /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <param name="iElementName"></param>
        /// <param name="nmatch"></param>
        /// <param name="elementId"></param>
        /// <param name="newElement"></param>
        /// <returns></returns>
        //static internal int UpdateElements(int xDocTypeID, string xElementName, bool xDoParsing, int xNMatch, int xElementID, bool xNewElement)
        static internal int UpdateElements(int iDocTypeID, string xElementName, int iNMatch, int iElementID, bool bNewElement,
            int iElementSource,string xTemplate, string xDisplayName, int iSortOrder, bool bVisible)
        {
            int iInsertedElementID = 0;
            ConnectIfNecessary();
            bool bSuccess = false;
            OleDbParameter oParam = new OleDbParameter();
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("Name", xElementName);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("NMatch", iNMatch);
                    oCmd.Parameters.Add(oParam);


                    oParam = new OleDbParameter("NParseSource", iElementSource);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("NParseTemplate", xTemplate);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("DisplayName", xDisplayName);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("SortOrder", iSortOrder);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("Visible", bVisible);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ElementId", iElementID);
                    oCmd.Parameters.Add(oParam);


                    if (bNewElement)
                    {
                        oCmd.CommandText = "EXECUTE qryInsertElements";
                        OleDbDataReader oReader = oCmd.ExecuteReader();
                        bSuccess = true;
                        oReader.Close();
                        if (bSuccess)
                        {
                            OleDbCommand oCmd1 = new OleDbCommand("SELECT @@IDENTITY", m_oCnn);
                            iInsertedElementID = (int)oCmd1.ExecuteScalar();
                        }
                    }
                    else
                    {
                     
                        oCmd.CommandText = "EXECUTE qryUpdateElements";
                        OleDbDataReader oReader = oCmd.ExecuteReader();
                        bSuccess = true;
                        oReader.Close();
                      
                    }

                    oCmd.Dispose();
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
       
            return iInsertedElementID;
        }

        /// <summary>
        /// Deletes Elements from the table with the specified elementID        /// </summary>
        ///<param name="ideletedelementId"></param>
        /// <returns></returns>
        static internal bool DeleteElements(int iDeletedElementId)
        {
            ConnectIfNecessary();
            bool bSuccess = false;

            OleDbParameter oParam = new OleDbParameter();
            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("DeletedElementId", iDeletedElementId);
                    oCmd.Parameters.Add(oParam);

                    oCmd.CommandText = "EXECUTE qryDeleteAssociatedElementInstructions";
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                    oReader.Close();
                   // paramDeleteElementId.Value = iDeletedElementId;
                    oCmd.CommandText = "EXECUTE qryElementsDelete";
                    oReader = oCmd.ExecuteReader();
                    oReader.Close();
                    bSuccess = true;

                    oCmd.Dispose();
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }


        /// <summary>
        /// Updates Determinants when newly added,Existing updated or when Determinants deleted        /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <param name="iDeterminantType"></param>
        ///<param name="iDeterminantKey"></param>
        /// <param name="iDeterminantValue"></param>
        /// <param name="iDeterminantId"></param>
        /// <param name="newDeterminant"></param>
        ///<param name="ideletedDeterminantId"></param>
        /// <returns></returns>
        /// 
        static internal bool UpdateDeterminants(int iDocTypeID, int iDeterminantType, string iDeterminantKey, string xDeterminantValue, int iDeterminantId, bool bNewDeterminant)
        {
            ConnectIfNecessary();
            bool bSuccess = false;
            OleDbParameter oParam = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oParam = new OleDbParameter("Type", iDeterminantType);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("Key", iDeterminantKey);
                    oCmd.Parameters.Add(oParam);


                    oParam = new OleDbParameter("Value", xDeterminantValue);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ID", iDocTypeID);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("DeterminantID", iDeterminantId);
                    oCmd.Parameters.Add(oParam);

                    if (bNewDeterminant)
                    {
                        oCmd.CommandText = "EXECUTE qryInsertDeterminants";
                    }
                    else
                    {
                        oCmd.CommandText = "EXECUTE qryUpdateDeterminants";
                    }
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                    bSuccess = true;
                    oReader.Close();
                    
                    oCmd.Dispose();

                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }

        /// <summary>
        /// Deletes Determinants from the table with the specified Determinant        /// </summary>
        ///<param name="iDeleteDeterminantId"></param>
        /// <returns></returns>
        static internal bool DeleteDeterminants(int iDeleteDeterminantId)
        {
            ConnectIfNecessary();
            bool bSuccess = false;

            OleDbParameter oParam = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oParam = new OleDbParameter("DeletedDeterminantId", iDeleteDeterminantId);
                    oCmd.Parameters.Add(oParam);

                    oCmd.CommandText = "EXECUTE qryDeterminantsDelete";
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                    oReader.Close();
                    bSuccess = true;

                    oCmd.Dispose();
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }

        /// <summary>
        /// connect to the db if necessary
        /// </summary>
        private static void ConnectIfNecessary()
        {
            try
            {
                if (m_oCnn == null)
                    m_oCnn = new OleDbConnection();

                //GLOG : 8500 : jsw
                if (m_xConnectionProvider == null)
                    m_xConnectionProvider = SetConnectionProvider();

                if ((m_oCnn.State & ConnectionState.Open) != ConnectionState.Open)
                {
                    //get data directory - 
                    string xDataDir = LMP.Registry.GetLocalMachineValue(
                        LMP.DocAnalyzer.Application.REGISTRY_ROOT, "DataDirectory");

                    //GLOG : 8500 : jsw
                    if (string.IsNullOrEmpty(xDataDir))
                    {
                        //look in 64-bit portion of registry
                        xDataDir = LMP.Registry.GetLocalMachineValue64Bit(
                        LMP.DocAnalyzer.Application.REGISTRY_ROOT, "DataDirectory");
                    }

                    if (string.IsNullOrEmpty(xDataDir))
                    {
                        //expect data in the default doc analyzer data directory
                        string xBinDir = Assembly.GetExecutingAssembly().Location;
                        xDataDir = xBinDir.Replace("bin\\daLib.dll", "data");
                    }
                    else
                    {
                        xDataDir = LMP.OS.EvaluateEnvironmentVariables(xDataDir);
                    }

                    //GLOG : 8905 : jsw
                    xDataDir = xDataDir.TrimEnd('\\');

                    //set connection string
                    m_oCnn.ConnectionString = @"Provider=" + m_xConnectionProvider + ";Data Source='" +
                        xDataDir + "\\daInstructionSets.mdb'; User Id=admin; Password=";

                    //open connection
                    m_oCnn.Open();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException("Could not connect to the database.", oE);
            }
        }
        
        //GLOG : 8500 : jsw
        static internal string SetConnectionProvider()
        {
            //GLOG : 8669 : jsw
            if (Registry.Is64Bit())
                return "Microsoft.ACE.OLEDB.12.0";
            else
                return "Microsoft.Jet.OLEDB.4.0";

        }        


        /// <summary>
        /// Updates Instructions when newly added,Existing updated       /// </summary>
        /// <param name="iElementID"></param>
        /// <param name="iStory"></param>
        /// <param name="iExecutionIndex"></param>
        /// <param name="xWholeRange"></param>
        /// <param name="xStartRange"></param>
        /// <param name="xEndRange"></param>
        /// <param name="xMatchCriteria"></param>
        /// <param name="xReturnData"></param>
        /// <param name="bNewInstruction"</param>
        /// <returns></returns>
        static internal bool SaveInstruction(int iElementID, int iStory, int iExecutionIndex, string xWholeRange, string xStartRange, string xEndRange, string xMatchCriteria, string xReturnData, bool bNewInstruction, int iMatchRangeType)
        {
            ConnectIfNecessary();

            bool bSuccess = false;
            OleDbParameter oParam = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    oParam = new OleDbParameter("Story", iStory);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("WholeRange", xWholeRange);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("StartRange", xStartRange);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("EndRange", xEndRange);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("MatchCriteria", xMatchCriteria);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ReturnData", xReturnData);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("MatchRangeType", iMatchRangeType);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ElementId", iElementID);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ExecutionIndex", iExecutionIndex);
                    oCmd.Parameters.Add(oParam);

                    if (bNewInstruction)
                    {

                        oCmd.CommandText = "EXECUTE qryInsertInstructions";
                        OleDbDataReader oReader = oCmd.ExecuteReader();
                        bSuccess = true;
                        oReader.Close();
                    }
                    else
                    {
                        oCmd.CommandText = "EXECUTE qryUpdateInstructions";
                        OleDbDataReader oReader = oCmd.ExecuteReader();
                        bSuccess = true;
                        oReader.Close();
                    }

                    oCmd.Dispose();

                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }

        /// <summary>
        /// Deleted Instructions      /// </summary>
        /// <param name="ElementID"></param>
        ///<param name="xExecutionIndex"></param>
        /// <returns>bool</returns>
        static internal bool DeleteInstruction(int iElementID, int iExecutionIndex)
        {
            ConnectIfNecessary();
            bool bSuccess = false;
            OleDbParameter oParam = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {

                    oParam = new OleDbParameter("ElementId", iElementID);
                    oCmd.Parameters.Add(oParam);

                    oParam = new OleDbParameter("ExecutionIndex", iExecutionIndex);
                    oCmd.Parameters.Add(oParam);

                    oCmd.CommandText = "EXECUTE qryDeleteInstruction";
                    OleDbDataReader oReader = oCmd.ExecuteReader();
                    bSuccess = true;
                    oReader.Close();

                    oCmd.Dispose();
                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }

        /// <summary>
        /// Inserts Determinants when newly added,      /// </summary>
        /// <param name="iDocTypeID"></param>
        /// <param name="iDeterminantType"></param>
        ///<param name="iDeterminantKey"></param>
        /// <param name="iDeterminantValue"></param>
        /// <param name="newDeterminant"></param>
        ///<param name="ideletedDeterminantId"></param>
        /// <returns></returns>
        /// 
        static internal bool InsertDeterminants(int iDocTypeID, int[] iDeterminantTypes, string[] xDeterminantKeys, string[] xDeterminantValues, bool bNewDeterminant, int iCount)
        {
            ConnectIfNecessary();
            bool bSuccess = false;
            OleDbParameter paramType = new OleDbParameter();
            OleDbParameter paramKey = new OleDbParameter();
            OleDbParameter paramValue = new OleDbParameter();
            OleDbParameter paramId = new OleDbParameter();

            try
            {
                using (OleDbCommand oCmd = m_oCnn.CreateCommand())
                {
                    //paramType.ParameterName = "Type";
                    paramType.OleDbType = OleDbType.Integer;
                    paramType.Size = 50;
                    oCmd.Parameters.Add(paramType);

                    //paramKey.ParameterName = "Key";
                    paramKey.OleDbType = OleDbType.VarChar;
                    paramType.Size = 250;
                    oCmd.Parameters.Add(paramKey);

                    //paramValue.ParameterName = "Value";
                    paramValue.OleDbType = OleDbType.VarChar;
                    paramValue.Size = 250;
                    oCmd.Parameters.Add(paramValue);

                    //paramId.ParameterName = "ID";
                    paramId.OleDbType = OleDbType.Integer;
                    paramId.Size = 4;
                    oCmd.Parameters.Add(paramId);


                    for (int k = 0; k < iCount; k++)
                    {
                        paramType.Value = iDeterminantTypes[k];
                        paramKey.Value = xDeterminantKeys[k];
                        paramValue.Value = xDeterminantValues[k];
                        paramId.Value = iDocTypeID;

                        if (bNewDeterminant)
                        {
                            oCmd.CommandText = "EXECUTE qryInsertDeterminants";
                        }


                        OleDbDataReader oReader = oCmd.ExecuteReader();
                        bSuccess = true;
                        oReader.Close();

                    }

                    oCmd.Dispose();

                }
            }
            catch (Exception oE)
            {
                throw oE;
            }
            return bSuccess;
        }

        # endregion
    }
}
