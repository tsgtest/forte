using System;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Word;
using System.Collections.Generic;
using System.Windows.Forms;
using LMP.DocumentAnalyzer.MSWord;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.DocAnalyzer
{
    public static class Application
    {
        #region***********************fields****************************
        public const string REGISTRY_ROOT = @"Software\The Sackett Group\Document Analyzer";
        public static Word.Global oWord = new Word.Global();
        private static List<Determinant> m_oDeterminants;
        # endregion
        #region***********************properties****************************
        public static List<Determinant> Determinants
        {
            get
            {
                if (m_oDeterminants == null)
                    //retrieve determinants
                    m_oDeterminants = Connection.GetDeterminantsList();

                return m_oDeterminants;
            }
        }
        #endregion
        #region***********************methods****************************

        /// <summary>
        /// returns the doctype id of the determinant that
        /// matches the specified parameters
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xKey"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private static int GetMatchingDocTypeID(DeterminantHelper.daDeterminantType iType, string xKey, string xValue)
        {
            return Connection.GetMatchingDocTypeID(iType, xKey, xValue);
        }
        /// <summary>
        /// returns the doctype of the specified document -
        /// returns null if no doctype can be associated with document
        /// </summary>
        /// <param name="oWordDoc"></param>
        /// <returns></returns>
        public static DocType GetDocType(Word.Document oWordDoc)
        {
            DeterminantHelper oDeterHelper = new DeterminantHelper();
            Definitions.m_bDocOpen = true;
            int iMatchingID = 0;
            string xValue = "";

            //get bpfile docvar value
            xValue = oDeterHelper.GetDeterminantValue(
                oWordDoc, DeterminantHelper.daDeterminantType.DocumentVariable, "bpfile");

            if(!string.IsNullOrEmpty(xValue))
            {
                //check for match
                iMatchingID = GetMatchingDocTypeID(
                     DeterminantHelper.daDeterminantType.DocumentVariable, "bpfile", xValue);
            }

            if (iMatchingID == 0)
            {
                //no bpfile match, get attached template
                xValue = oDeterHelper.GetDeterminantValue(
                    oWordDoc, DeterminantHelper.daDeterminantType.Template, string.Empty);

                if (!string.IsNullOrEmpty(xValue))
                {
                    //check for match
                    iMatchingID = GetMatchingDocTypeID(
                       DeterminantHelper.daDeterminantType.Template, string.Empty, xValue);
                }
            }

            if (iMatchingID == 0)
            {
                //still no match, cycle through determinants, executing each
                foreach (Determinant oD in Application.Determinants)
                {
                    //skip over attached template and bpfile determinants,
                    //as we've already checked for them
                    if (oD.Type != DeterminantHelper.daDeterminantType.Template &&
                        !(oD.Type == DeterminantHelper.daDeterminantType.DocumentVariable && oD.Key == "bpfile"))
                    {
                        //get value
                        xValue = oDeterHelper.GetDeterminantValue(
                            oWordDoc, oD.Type, oD.Key != "" ? oD.Key : oD.Value);

                        if (!string.IsNullOrEmpty(xValue))
                        {
                            //check for match
                            //GLOG : 8880 : jsw
                            iMatchingID = GetMatchingDocTypeID(
                                oD.Type, oD.Key, xValue); 
                        }

                        if (iMatchingID > 0)
                            //we have a match
                            break;
                    }
                }
            }

            iMatchingID = ShowDocTypesForm(iMatchingID);
            
            if (iMatchingID > 0)
            {
                //create and return associated doc type
                DocType oDocType = new DocType(iMatchingID, oWordDoc);
                return oDocType;
            }
            else
                return null;
        }
        /// <summary>
        /// displays document type selection form
        /// </summary>
        /// <param name="iFoundDocTypeID"></param>
        /// <returns></returns>
        private static int ShowDocTypesForm(int iFoundDocTypeID)
        {
            int iDocTypeID = 0;
            DocTypesForm oForm = new DocTypesForm("Select Document Type", "&Document Types:");
            oForm.m_SelectDocType = iFoundDocTypeID;
            oForm.ShowDialog();
            iDocTypeID = oForm.m_SelectDocType;
            return iDocTypeID;
        }
        /// <summary>
        /// analyzes the active document - returns a string of data
        /// in the specified format.  returns null if analysis failed
        /// or was canceled
        /// </summary>
        /// <param name="iFormat"></param>
        /// <param name="bShowSelectionForm"></param>
        public static string AnalyzeCurrentDocument(DocType.OutputFormats iFormat, 
            bool bShowSelectionForm, bool bShowOutputMessage)
        {
            Word.Document oWordDoc = null;
            try
            {
                oWordDoc = Application.oWord.ActiveDocument;
            }
            catch {}
            
            if (oWordDoc == null)
            { 
                //message user - no doc is open
                MessageBox.Show("There is no document to analyze.  Open a Word Document and run the Document Analyzer.",
                    "Document Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            DocType oDocType = GetDocType(oWordDoc);

            if (oDocType != null)
            {
                string xData = oDocType.AnalyzeDocument(iFormat, bShowSelectionForm, bShowOutputMessage);
                if (xData != string.Empty && xData != null)
                    return oDocType.Name + "�" + xData;
                else
                    return null;
            }
            else
                return null;
        }
        /// <summary>
        /// returns an ArrayList of Element Name and its matching array of Word ranges 
        /// </summary>        
        public static ArrayList GetElementRanges(Word.Document oDoc, string[] xElementNames, string xDocTypeName)
        {

            ArrayList aElementRanges = new ArrayList();

            Word.Range[] oRanges = null;

            Element oElement = new Element();

            //get the DocType object for
            DocType oDocType = new DocType(xDocTypeName, oDoc);

            //loop through element collection and get match
            for (int i = 0; i < oDocType.Elements.Count; i++)
            {
                oElement = oDocType.Elements[i];

                foreach (string xElementName in xElementNames)
                {
                    if (oElement.Name == xElementName)
                    {
                        oRanges = oElement.GetRanges();
                        aElementRanges.Add(new object[] { xElementName, oRanges });
                    }
                }
            }

            return aElementRanges;

        }
        
        # endregion
    }
}
