using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Data;
using System.Windows.Forms;
using LMP.DocAnalyzer;
using LMP.DocumentAnalyzer.MSWord;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.DocAnalyzer
{
    public class DocType
    {


        #region***********************enumerations*************************
        public enum OutputFormats
        {
            DelimitedString = 1,
            XML = 2
        }
        #endregion
        #region***********************fields*******************************
        //Element element = new Element();
        private int m_iID;
        private string m_xName;
        private List<Element> m_oElements;
        private List<Determinant> m_oDeterminants;
        private bool m_bInitialized;
        private Word.Document m_oWordDocument;
        private int m_iParentCourtLevel0;
        private int m_iParentCourtLevel1;
        private int m_iParentCourtLevel2;
        private int m_iParentCourtLevel3;
        private Element m_oElement;
        //GLOG : 7961 : jsw
        private bool m_bCantUnencrypt;

        #endregion
        #region***********************constants****************************
        //these are same separators as used in LMP.Architect.Prefill
        private const char mpPrefillFieldDelimiter = '�';		// ASCII 222
        private const char mpPrefillValueDelimiter = '�';		// ASCII 164
        private const char mpPrefillAuthorsSeparator = '|';
        #endregion
        #region***********************properties****************************
        public int ID
        {
            get { return m_iID; }
            set
            {
                m_iID = value;
            }
        }
        public string Name
        {
            get { return m_xName; }
            set
            {
                m_xName = value;
            }
        }
        public List<Element> Elements
        {
            get
            {
                if (!m_bInitialized)
                {
                    Initialize();
                    Elements = m_oElements;
                }
                return m_oElements;
            }
            set
            {
                if (!m_bInitialized)
                {
                    Initialize();
                }
                m_oElements = value;
            }
        }
        public Word.Document WordDocument
        {
            get { return m_oWordDocument; }
        }
        ////public Word.Selection Selection
        ////{
        ////    get { return m_oWordSelection; }
        ////}
        public List<Determinant> Determinants
        {
            get
            {
                if (!m_bInitialized)
                {
                    Determinants = m_oDeterminants;
                }
                return m_oDeterminants;
            }
            set
            {
                if (!m_bInitialized)
                {
                    Initialize();
                }
                m_oDeterminants = value;
            }
        }
        # endregion
        #region***********************methods****************************

        //Constructors
        public DocType()
        {
        }

        /// <summary>
        ///Constructors
        /// </summary>   
        public DocType(int iID)
        {
            Initialize();
            this.ID = iID;
            this.Name = Connection.GetDocTypeName(this.ID);
            m_oElements = GetElements();
            m_oDeterminants = GetDeterminants();
        }

        public DocType(string xName)
        {
            Initialize();
            this.Name = xName;
            this.ID = Connection.GetDocTypeID(this.Name);
            m_oElements = GetElements();
            m_oDeterminants = GetDeterminants();
        }

        public DocType(int iID, string xName)
        {
            this.ID = iID;
            this.Name = xName;

        }

        public DocType(int iID, Word.Document oDoc)
        {
            Initialize();
            this.ID = iID;
            DocAnalyzer.Definitions.m_bDocOpen = true;
            this.m_oWordDocument = oDoc;
            this.Name = Connection.GetDocTypeName(this.ID);
            m_oElements = GetElements();
            m_oDeterminants = GetDeterminants();
        }

        public DocType(string xName, Word.Document oDoc)
        {
            Initialize();
            this.Name = xName;
            DocAnalyzer.Definitions.m_bDocOpen = true;
            this.m_oWordDocument = oDoc;
            this.ID = Connection.GetDocTypeID(this.Name);
            m_oElements = GetElements();
            m_oDeterminants = GetDeterminants();
        }

        public DocType(string xName, Word.Selection oSelection)
        {
            Initialize();
            this.Name = xName;
            DocAnalyzer.Definitions.m_bDocOpen = true;
            //this.m_oWordDocument = oDoc;
            if (this.Name == "RecipientList")
            {
                string xTemplate = "[Field_FULLNAMEWITHPREFIXANDSUFFIX][Char_13][Field_TITLE][Char_13][Field_COMPANY][Char_13][Field_COREADDRESS][Char_13][Char_13]";
                
                m_oElements.Add(new Element(0, "Recipients", 228, 1, null, 1, xTemplate, "Recipients/List", 0, true));
                m_oDeterminants.Add(new Determinant(228, (LMP.DocumentAnalyzer.MSWord.DeterminantHelper.daDeterminantType)4, "", "Recipients", 239));
            }
            else
            {
                this.ID = Connection.GetDocTypeID(this.Name);
                m_oElements = GetElements();
                m_oDeterminants = GetDeterminants();
            }
        }
        /// <summary>
        ///Initialize all the List
        /// </summary>   
        private void Initialize()
        {
            m_oElements = new List<Element>();
            m_oDeterminants = new List<Determinant>();
            m_bInitialized = true;
        }

        /// <summary>
        ///Get the list of Elements based on the DocTypeId
        /// </summary>        
        public List<Element> GetElements()
        {
            try
            {
                m_oElements = Connection.GetElementsList(this.ID, this.m_oWordDocument);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return m_oElements;
        }

        /// <summary>
        ///Get the list of Elements based on the DocTypeId
        /// </summary>        
        public Element GetElement(string xName)
        {
            try
            {
                m_oElements = Connection.GetElementsList(this.ID, this.m_oWordDocument);

                foreach (Element oElement in m_oElements)
                {
                    if (oElement.Name == xName)
                    {
                        return oElement;
                    }
                }
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }

            return null;
        }

        /// <summary>
        /// Get the list of Determinants based on the DocTypeId 
        /// </summary>   
        private List<Determinant> GetDeterminants()
        {
            try
            {
                m_oDeterminants = Connection.GetDeterminants(this.ID);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return m_oDeterminants;
        }

        /// <summary>
        /// returns the XML string after analyzing the document - 
        /// returns null if analysis was canceled by the user
        /// </summary>        
        public string AnalyzeDocument(OutputFormats iFormat, bool bShowReviewForm, bool bShowOutputMessage)
        {
            //Analyze the document and get the analyzed value in the form of XML
            string xXML = "";
            string xOutput = "";
            bool bDoParsing;

            //if not reviewing should get parsed value of element from Analyze Doc
            //show the review form if indicated
            //User is allowed to check/uncheck the Elements
            if (bShowReviewForm)
            {
                //Get the raw element values
                //to display to user, do not parse.
                //Parsing happens after the user has reviewed.
                bDoParsing = false;
                xXML = AnalyzeToXML(bDoParsing);
                //GLOG : 7961 : jsw
                if (this.m_bCantUnencrypt == true)
                {
                    m_bCantUnencrypt = false;
                    return null;
                }
                //remove Authors node, if it exists.  We want to preserve the data,
                //but don't want to display in the review form.

                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(xXML);

                XmlNode oNode = null;
                XmlNode oAuthorNode = null;

                for (int i = 0; i < oXML.DocumentElement.ChildNodes.Count; i++)
                {
                    oNode = oXML.DocumentElement.ChildNodes[i];
                    if (oNode.Attributes["Name"].Value == "Authors")
                    {
                        oAuthorNode = oNode;

                        try
                        {
                            oXML.DocumentElement.RemoveChild(oNode);
                        }
                        catch { }
                        break;
                    }
                }
                ElementReviewForm oForm = new ElementReviewForm(oXML.InnerXml, oAuthorNode);
                DialogResult iRes = oForm.ShowDialog();
                
                if (iRes == DialogResult.Cancel)
                    //user canceled - return null
                    return null;
                else
                {    
                    //get modified xml
                    //the form methods return parsed Element values
                    if (iFormat == OutputFormats.XML)
                        xOutput = oForm.ReviewedXML;
                    else if (iFormat == OutputFormats.DelimitedString)
                        xOutput = oForm.ReviewedDelimitedString;
                }
            }
            else
            {
                //get the parsed values for the elements,
                //user is not reviewing the element values.  
                bDoParsing = true;
                xXML = AnalyzeToXML(bDoParsing);

                //format text according to specified output format
                if (iFormat == OutputFormats.DelimitedString)
                    xOutput = GetDelimitedStringOutput();
                else if (iFormat == OutputFormats.XML)
                    xOutput = xXML;
            }

            if(bShowOutputMessage)
                MessageBox.Show(xOutput);

            return xOutput;
        }
        /// <summary>
        /// parses m_oElements collection, returns delimited string with parsed values
        /// </summary>
        /// <returns></returns>
        private string GetDelimitedStringOutput()
        {
            StringBuilder oSB = new StringBuilder();
            for (int j = 0; j < m_oElements.Count; j++)
            {
                //construct delimited string
                oSB.AppendFormat("{0}" + mpPrefillValueDelimiter.ToString() +
                    "{1}" + mpPrefillFieldDelimiter.ToString(), m_oElements[j].Name, m_oElements[j].Text);
            }
            return oSB.ToString();
        }
        /// <summary>
        /// cycle through m_oElements list, return well-formed XML
        /// </summary>
        /// <returns></returns>
        private string AnalyzeToXML(bool bDoParsing)
        {
            XmlTextWriter xWriter = new XmlTextWriter(
                new MemoryStream(), System.Text.UTF8Encoding.Unicode);
            xWriter.Formatting = Formatting.Indented;
            xWriter.WriteStartElement("DocType");
            xWriter.WriteAttributeString("id", this.ID.ToString());
            string xElementValue = string.Empty;
            bool bEncrypted = false;

            for (int j = 0; j < m_oElements.Count; j++)
            {

                m_oElement = m_oElements[j];
                if (m_oElement.Visible.ToString().ToLower() == "false")
                    //skip this element
                    continue;

                //get unparsed value and massage as necessary, reassign
                xElementValue = m_oElements[j].Text = m_oElement.GetUnParsedValue();

                if (m_oElement.Encrypted && bEncrypted == false)
                    bEncrypted = true;
                xElementValue = GetMassagedElementValue(xElementValue, m_oElement.Name);

                //not necessary to convert chars, this is done automatically when converted to XmlDocument.
                //xElementValue = String.ReplaceXMLChars(xElementValue);

                //GLOG 15874: Replace Page break with returns
                m_oElement.Text = xElementValue.Replace("\f", "\r\r");

                //To Find a match in the instructionslist for the corresponding element id            
                if (bDoParsing == true)
                    m_oElement.Text = m_oElement.GetParsedValue();

                //if there is no element value,
                //return value according to NoMatch property
                if (xElementValue == "" && m_oElement.NoMatch != 1)
                {
                    if (m_oElement.NoMatch == 2)
                        xElementValue = "True";
                    else
                        xElementValue = "False";
                }

                xWriter.WriteStartElement("Element");
                //new
                //xWriter.WriteAttributeString("Include", m_oElements[j].Visible.ToString());
                xWriter.WriteAttributeString("Name", m_oElement.Name);
                xWriter.WriteAttributeString("DisplayName", m_oElement.DisplayName);

                //replace "True" and "False" with "Yes" and "No" for display only
                if (bDoParsing == false)
                {
                    //if return data type is 10 for "mapped value", then show
                    //"yes" or "no" in display if appropriate.
                    //to do -- create constants for instruction types
                    if (m_oElement.Instructions[0].ReturnDataType == 10)
                    {
                        if (xElementValue == "True" || xElementValue == "1" || xElementValue == "-1")
                        {
                            //if (m_oElements[j].Name != "L0")
                            xElementValue = "Yes";
                        }
                        if (xElementValue == "False" || xElementValue == "0")
                            xElementValue = "No";
                    }
                }

                xWriter.WriteAttributeString("Value", xElementValue);

                //this value will be set to true or false
                //according to whether it is parsed successfully.
                if (!string.IsNullOrEmpty(m_oElement.ParsingInstructions.Template))
                    xWriter.WriteAttributeString("DParsing", "Parsed");
                else
                    xWriter.WriteAttributeString("DParsing", "");

                xWriter.WriteAttributeString("ID", m_oElement.ID.ToString());

                xWriter.WriteEndElement();
            }
            xWriter.WriteEndElement();
            xWriter.Flush();
            xWriter.BaseStream.Position = 0;

            if (bEncrypted == true)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_LegacyDocEncrypted"),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                //GLOG : 7961 : jsw
                this.m_bCantUnencrypt = true;
            }

            return new StreamReader(xWriter.BaseStream).ReadToEnd();
        }
        private string GetMassagedElementValue(string xElementValue, string xElementName)
        {
            //sometimes doc legacy elements contain the string |vbcr|.
            //instead of "\r\n".  Replace now so this does not display 
            //in Review Data form

            xElementValue = xElementValue.Replace("|vbcr|\n", "\r\n");
            xElementValue = xElementValue.Replace("|vbcr|", "\r\n");
            
            if (xElementValue.EndsWith("^^-1"))
                xElementValue = xElementValue.Replace("^^-1", "");

            if (xElementName == "DateTimeInformation")
            {
                //remove pipe symbols from element value
                xElementValue = xElementValue.Replace("|", "");
            }
            if (xElementName == "Recipients")
            {

                if (this.Name.Contains("POS") || this.Name.Contains("COS"))
                {
                    xElementValue = GetServiceRecipients(xElementValue);
                }
                if (this.Name == "RecipientList" || this.Name.StartsWith("Label") || this.Name.StartsWith("Envelope"))
                {
                    xElementValue = GetMassagedRecipientListData(xElementValue);
                }
                //GLOG : 8139 : JSW
                if (this.Name.Contains("Fax") && !(this.Name.Contains("COS") || this.Name.Contains("POS")))
                {
                    xElementValue = GetFaxRecipients(xElementValue);
                }
            }
            if (xElementName == "Gender")
            {
                //map numeric values to keywords
                switch (xElementValue)
                {
                    case "0":
                        xElementValue = "Indefinite";
                        break;
                    case "1":
                        xElementValue = "Female";
                        break;
                    case "2":
                        xElementValue = "Male";
                        break;
                    case "3":
                        xElementValue = "Plural";
                        break;
                    case "4":
                        xElementValue = "Unknown";
                        break;
                    default:
                        xElementValue = "Indefinite";
                        break;
                }
            }
            // GLOG : 3108 : JSW
            if (xElementName == "L0" || xElementName == "L1" || xElementName == "L2" || xElementName == "L3" || xElementName == "L4")
            {
                
                int iCourtID = 0;
                int iLevel = Convert.ToInt32(xElementName.Substring(1, 1));

                switch (iLevel)
                {
                    case 0:
                        iCourtID = 1;
                        m_iParentCourtLevel0 = 1;
                        break;
                    case 1:
                        m_iParentCourtLevel0 = 1;
                        iCourtID = GetCourtID(iLevel, m_iParentCourtLevel0, xElementValue);
                        m_iParentCourtLevel1 = iCourtID;
                        break;
                    case 2:
                        iCourtID = GetCourtID(iLevel, m_iParentCourtLevel1, xElementValue);
                        m_iParentCourtLevel2 = iCourtID;
                        break;
                    case 3:
                        iCourtID = GetCourtID(iLevel, m_iParentCourtLevel2, xElementValue);
                        m_iParentCourtLevel3 = iCourtID;
                        break;
                    case 4:
                        iCourtID = GetCourtID(iLevel, m_iParentCourtLevel3, xElementValue);
                        //m_iParentCourtLevel3 = iCourtID;
                        break;
                    default:
                        //xElementValue = "Indefinite";
                        break;
                } 
                xElementValue = iCourtID.ToString();

            }

            //GLOG 5767: Replace Line break with single return only
            xElementValue = xElementValue.Replace("\v", "\r\n");
            //replace tabs with blanks
            //GLOG : 8140 : JSW
            if (xElementName != "DateTimeInformation")
                xElementValue = xElementValue.Replace("\t", "");
            
            return xElementValue;
        }
        private string GetServiceRecipients(string xElementValue)
        {
            /// GLOG : JSW : 4797
            //there are 2 different formats for recipients depending on the version of 9.x.
            //pre 9.6.1 has 4 pipes separating the element fields, post 9.6.0 has 7 pipes.
            //have to determine 9.x version by document variable name for recpients

            int iPipeCount = 4;
            object oVar1 = "ServiceList_1_Recipients";
            object oVar2 = "ServiceList_1_Recipients961";
            int iSepLen = "|".Length;
            int iPos = xElementValue.IndexOf("|") + iSepLen;
            int iStartPos = 0;
            int iEndPos = 0;
            int iCount = 1;
            string xNewElementValue = "";
            string xRecip = "";

            try
            {
                if (m_oWordDocument.Variables.get_Item(ref oVar1).Value != "")
                    iPipeCount = 4;
            }
            catch { }
            try
            {
                if (m_oWordDocument.Variables.get_Item(ref oVar2).Value != "")
                    iPipeCount = 7;
            }
            catch { }

            try
            {
                while (iPos != -1)
                {
                    iPos = xElementValue.IndexOf("|", iPos);
                    if (iPos != -1)
                        iPos = iPos + iSepLen;
                    iCount++;
                    if ((iCount % iPipeCount) == 0)
                    {
                        if (iPos == -1)
                            iEndPos = xElementValue.Length;
                        else
                        {
                            if (iPos < xElementValue.Length && xElementValue.Substring(iPos, iSepLen) == "|")
                            {
                                iPos = iPos + 1;
                                iEndPos = iPos;
                            }
                            else
                                iEndPos = iPos;
                        }
                        xRecip = xElementValue.Substring(iStartPos, iEndPos - iStartPos);
                        while (xRecip.LastIndexOf("|") > xRecip.Length - 4)
                        {
                            //trim empty values
                            xRecip = xRecip.Substring(0, xRecip.Length - 1);
                        }
                        //GLOG : 8139 : JSW
                        //even if the entry is missing the delimiter for name and title
                        //it may still be valid--if so, add "\r\n" delimiter as first element
                        //so the rest of it will parse
                        if (!xRecip.Contains("\r\n"))
                        {
                            xRecip = " \r\n" + xRecip;
                        }
                        xNewElementValue = xNewElementValue + xRecip + "|||||";
                        iStartPos = iEndPos;
                    }
                }
                if (xNewElementValue.LastIndexOf("|||||") == xNewElementValue.Length - 5)
                    xNewElementValue = xNewElementValue.Substring(0, xNewElementValue.Length - 5);
                xElementValue = xNewElementValue;

            }
            // if there is any kind of error, go with the value we have
            catch
            {
                if (xNewElementValue != "")
                    xElementValue = xNewElementValue;
            }
            return xElementValue;
      
        }
        private string GetMassagedRecipientListData(string xElementValue)
        {
            //GLOG 5811: 
            //remove all types of end of row markers if present
            xElementValue = xElementValue.Replace("\r\r\a\r\a", "\r\r");
            xElementValue = xElementValue.Replace("\r\a\r\a", "\r\r");
            xElementValue = xElementValue.Replace("\r\r\a", "\r\r");
            xElementValue = xElementValue.Replace("\r\a\a", "\r\r");
            xElementValue = xElementValue.Replace("\r\a", "\r\r");
            xElementValue = xElementValue.Replace("\a\a\a", "\r\r");
            xElementValue = xElementValue.Replace("\a\r\a", "\r\r");
            xElementValue = xElementValue.Replace("\a\r\r", "\r\r");
            xElementValue = xElementValue.Replace("\a\a", "\r\r");

            //remove double sets of returns
            xElementValue = xElementValue.Replace("\r\r\r\r", "\r\r");
            xElementValue = xElementValue.Replace("\r\n\r\r", "\r\r");

            //remove unrecognized characters
            xElementValue = xElementValue.Replace("", "");

            //GLOG 5811
            //if these characters are present, recipients may be separated by 
            //single return (\r) or a single end of cell marker (\a),
            //which have to be replaced with double returns 
            if (xElementValue.Contains("\n") || xElementValue.Contains("\a") || xElementValue.Contains("\v"))
            {
                xElementValue = SplitRecipientListTableData(xElementValue, "\r");
                xElementValue = SplitRecipientListTableData(xElementValue, "\a");
                //clean up any remaining cell markers
                xElementValue = xElementValue.Replace("\a", "");
            }

            //GLOG 5767: 
            //now that table rows and cells have been processed, 
            //replace Line breaks with single return only -- use "\r", not "\r\n" 
            //to march parsing template
            xElementValue = xElementValue.Replace("\v", "\r");
 
            return xElementValue;
        }
        private string GetFaxRecipients(string xElementValue)
        {
            //replace pipe symbols with carriage returns for readability
            xElementValue = xElementValue.Replace("|", "\r\n");

            //if this is a text range from a doc, instead of a doc var,
            //replace end of cell and end of row markers with returns.
            xElementValue = xElementValue.Replace("\r\r", "\r\n");
            xElementValue = xElementValue.Replace("\r\a", "\r\n");

            //if there are two carriage returns together,
            //put in a space to designate missing value 
            //rather than entity delimiter.
            xElementValue = xElementValue.Replace("\r\n\r\n", "\r\n \r\n");

            //find every 4th carriage return
            //insert 2nd carriage return to create
            //entity delimiter
            int iSepLen = "\r\n".Length;
            int iPos = xElementValue.IndexOf("\r\n") + iSepLen;
            int iPosForDocRange = -1;
            int iStartPos = 0;
            int iEndPos = 0;
            int iCount = 1;
            bool bFromDocVar = false;
            string xNewElementValue = "";
            try
            {
                while (iPos != -1)
                {
                    iPos = xElementValue.IndexOf("\r\n", iPos);
                    if (iPos != -1)
                        iPos = iPos + iSepLen;
                    iCount++;
                    if ((iCount % 4) == 0)
                    {
                        if (iPos == -1)
                            iEndPos = xElementValue.Length;
                        else
                        {
                            //GLOG : 5761 : JSW
                            //if this is from a doc range rather than a doc var, there are
                            //probably end of row markers or double returns that separate recipients
                            //these have to be removed for recipients to parse correctly
                            for (int i = 0; i < m_oElement.Instructions.Count; i++)
                            {
                                //at some point need to create constants for instruction sets
                                //2 designates doc variable type.
                                if (m_oElement.Instructions[i].MatchCriteriaType == 2)
                                {
                                    bFromDocVar = true;
                                    break;
                                }
                            }
                            if (!bFromDocVar)
                            {
                                //
                                iPosForDocRange = xElementValue.IndexOf(" \r\n", iPos);
                                if (iPosForDocRange == iPos)
                                    iPos = iPos + " \r\n".Length;
                            }
                            iEndPos = iPos;
                        }
                        xNewElementValue = xNewElementValue + xElementValue.Substring(iStartPos, iEndPos - iStartPos) + "\r\n";
                        iStartPos = iEndPos;
                    }
                }
                xElementValue = xNewElementValue;
            }
            // if there is any kind of error, go with the value we have
            catch
            {
                if (xNewElementValue != "")
                    xElementValue = xNewElementValue;
            }
            return xElementValue;
        }
        
        private int GetCourtID(int iLevel, int iParentID, string xCourtName)
        {
            string xSQL = "SELECT ID FROM Jurisdictions" + iLevel + " WHERE Parent=" + iParentID + " AND Name = \""  + xCourtName + "\"";
            object oRet = LMP.Data.SimpleDataCollection.GetScalar(xSQL);
            if (oRet != null)
            {
                return (int)oRet;
            }
            else
                return 0;
        }

        private string SplitRecipientListTableData(string xElementValue, string xChar)
        {

            //find single instances of xChar and replace with \r\r 
            string xTemp = "";
            int iSepLen = xChar.Length;
            int iPos = xElementValue.IndexOf(xChar);
            int iStartPos = 0;
            int iStartRecipPos = 0;
            bool bReplaceElementVal = false;
            string xPostChar = "";
            string xPreChar = "";

            while (iPos != -1 && iPos + iSepLen <= xElementValue.Length)
            {

                if (iPos + iSepLen < xElementValue.Length)
                    xPostChar = xElementValue.Substring(iPos + iSepLen, 1);
                else
                    xPostChar = "";
                if (iPos > 0)
                    xPreChar = xElementValue.Substring(iPos - 1, 1);
                else
                    xPreChar = xElementValue.Substring(iPos, 1);

                if (xPostChar == "\r" || xPostChar == "\a" || xPostChar == "\n" || xPreChar == "\r" || xPreChar == "\a" || xPreChar == "\n")
                {
                    //skip

                }
                else
                {
                    //needs to be replaced
                    xTemp = xTemp + xElementValue.Substring(iStartRecipPos, iPos - iStartRecipPos) + "\r\r";
                    iStartRecipPos = iPos + iSepLen;
                    bReplaceElementVal = true;
                }

                iStartPos = iPos + iSepLen;

                iPos = xElementValue.IndexOf(xChar, iPos + iSepLen);
            }

            if (bReplaceElementVal == true)
            {    //append last bit of string
                if (iPos == -1)
                    xTemp = xTemp + xElementValue.Substring(iStartRecipPos);

                xElementValue = xTemp;
            }
            //xElementValue = xElementValue.Replace("\a", "\r\r");
            return xElementValue;
        }

        /// <summary>
        ///Method to add a DocType
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public int Add(string xName)
        {
            int iInsertedDocTypeID = 0;
            try
            {
                iInsertedDocTypeID = Connection.AddDocType(xName);
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return iInsertedDocTypeID;
        }

        /// <summary>
        ///Method to delete a DocType
        /// </summary>
        /// <param name="iDocTypeId"></param>
        /// <returns></returns>
        public void Delete(int iDocTypeId)
        {
            try
            {
                Connection.DeleteDocType(iDocTypeId);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
        }

        /// <summary>
        ///Method to Update the Elements
        /// </summary>
        /// <param name="xName"></param>
        ///  <param name="xNMatch"></param>
        ///  <param name="iElementId"></param>
        /// <param name="iNewElement"></param>
        ///  <param name="iElementSource"></param>
        ///  <param name="xTemplate"></param>
        /// <returns></returns>
        public int UpdateElement(string xName, int iNMatch, int iElementId, bool bNewElement, int iElementSource,
            string xTemplate, string xDisplayName, int iSortOrder, bool bVisible)
        {
            int iInsertedElementID = 0;

            try
            {
                iInsertedElementID = Connection.UpdateElements(this.ID, xName,
                    iNMatch, iElementId, bNewElement, iElementSource, xTemplate, xDisplayName, iSortOrder, bVisible);

            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return iInsertedElementID;
        }

        /// <summary>
        ///Method to Delete the Element
        /// </summary>
        /// <param name="oDeleteElementId"></param>
        /// <returns></returns>
        public bool DeleteElement(int iDeleteElementId)
        {
            bool bSucessDelete = false;
            try
            {
                bSucessDelete = Connection.DeleteElements(iDeleteElementId);
            }
            catch (Exception ex)
            {
                Error.ShowInEnglish(ex);
            }
            return bSucessDelete;
        }

        /// <summary>
        ///Method to Update Determinant
        /// </summary>
        /// <param name="iDeterminantType"></param>
        ///  <param name="xDeterminantKey"></param>
        ///  <param name="xDeterminantValue"></param>
        /// <param name="iDeterminantId"></param>
        ///  <param name="iNewDeterminant"></param>
        /// <returns></returns>       
        public bool UpdateDeterminant(int iDeterminantType, string xDeterminantKey, string xDeterminantValue, int iDeterminantId, bool bNewDeterminant)
        {
            bool bSuccessUpdate = false;
            try
            {
                bSuccessUpdate = Connection.UpdateDeterminants(this.ID, iDeterminantType, xDeterminantKey, xDeterminantValue, iDeterminantId, bNewDeterminant);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return bSuccessUpdate;
        }

        /// <summary>
        ///Delete Determinant
        /// </summary>
        /// <param name="iDeleteDeterminantId"></param>
        /// <returns></returns>    
        public bool DeleteDeterminant(int iDeleteDeterminantId)
        {
            bool bSucessDelete = false;
            try
            {
                bSucessDelete = Connection.DeleteDeterminants(iDeleteDeterminantId);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return bSucessDelete;
        }

        /// <summary>
        ///Method to Insert Determinant
        /// </summary>
        /// <param name="iDeterminantType"></param>
        ///  <param name="xDeterminantKey"></param>
        ///  <param name="xDeterminantValue"></param>
        /// <param name="iNewDeterminant"></param>
        ///  <param name="iCount"></param>
        /// <returns></returns> 
        public bool InsertDeterminants(int[] iDeterminantTypes, string[] xDeterminantKeys, string[] xDeterminantValues, bool bNewDeterminant, int iCount)
        {
            bool bSuccessInsert = false;
            try
            {
                bSuccessInsert = Connection.InsertDeterminants(
                    this.ID, iDeterminantTypes, xDeterminantKeys,
                    xDeterminantValues, bNewDeterminant, iCount);
            }
            catch (Exception oE)
            {
                Error.ShowInEnglish(oE);
            }
            return bSuccessInsert;
        }
        /// <summary>
        /// Method to display the selected Elements as XML string
        /// </summary>
        /// <param name="idoctypeID"></param>
        /// <returns></returns>
        private string GetXMLOutput(string xInputXML, int iDocTypeID)
        {
            string xElementName = string.Empty;
            string xElementValue = string.Empty;
            bool bStartElement = false;

            XmlDataDocument oXMLDoc = new XmlDataDocument();
            oXMLDoc.DataSet.ReadXml(new StringReader(xInputXML));

            DataTable oDT = oXMLDoc.DataSet.Tables[1];

            //Start creating XML
            XmlTextWriter oWriter = new XmlTextWriter(new MemoryStream(), 
                System.Text.UTF8Encoding.Unicode);

            oWriter.Formatting = Formatting.Indented;
            oWriter.WriteStartElement("DocType");
            oWriter.WriteAttributeString("id", iDocTypeID.ToString());

            for (int j = 0; j < oDT.Rows.Count; j++)
            {
                DataRow oRow = oDT.Rows[j];

                xElementName = oRow.ItemArray[1].ToString();
                xElementValue = oRow.ItemArray[2].ToString();

                bStartElement = true;
                oWriter.WriteStartElement("Element");
                oWriter.WriteAttributeString("Name", xElementName);

                if (oRow.ItemArray[3].ToString() == "Parsed")
                {
                    //Check to see Whether the corresponding Element is parsed                           
                    oWriter.WriteAttributeString("Value", "");
                    oWriter.WriteAttributeString("Parsed", "true");
                    string xParsedValue = oRow.ItemArray[2].ToString();
                    string[] xSplitParsedValue = xParsedValue.Split(new Char[] { '\r', '\n', '\t', '|' });
                    int iFindIndex = xParsedValue.IndexOf("\t\t");
                    for (int i = 0; i < xSplitParsedValue.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(xSplitParsedValue[i]))
                        {
                            oWriter.WriteRaw("\n\t");
                            xSplitParsedValue[i] = xSplitParsedValue[i].Trim();
                            //If the parsed Element does not have a start tag 
                            if (xSplitParsedValue[i].Substring(0, 1) != "<")
                            {
                                xSplitParsedValue[i] = xSplitParsedValue[i].Insert(0, "\t");
                            }
                            else
                            {
                                //If the parsed element has both the start and End Tags.
                                int iStartIndex = xSplitParsedValue[i].IndexOf("<");
                                int iEndIndex = xSplitParsedValue[i].IndexOf("</");

                                if (iStartIndex > -1 && iEndIndex > -1)
                                    if (iStartIndex != iEndIndex)
                                        xSplitParsedValue[i] = xSplitParsedValue[i].Insert(0, "\t");
                            }
                            
                            oWriter.WriteRaw(xSplitParsedValue[i]);
                        }
                    }
                }
                else
                {
                    oWriter.WriteAttributeString("Value", xElementValue);
                    oWriter.WriteAttributeString("Parsed", "false");
                }
                oWriter.WriteRaw("\n");
                oWriter.WriteEndElement();
                }

            if (bStartElement)
                    oWriter.WriteRaw("\n");
                
                oWriter.WriteEndElement();
                oWriter.Flush();
                oWriter.BaseStream.Position = 0;
                return new StreamReader(oWriter.BaseStream).ReadToEnd();
        }

    # endregion


    }
}
