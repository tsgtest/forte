namespace LMP.DocAnalyzer
{
    partial class DocTypesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstDocTypeList = new System.Windows.Forms.ListBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblDocTypes = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstDocTypeList
            // 
            this.lstDocTypeList.FormattingEnabled = true;
            this.lstDocTypeList.Location = new System.Drawing.Point(8, 26);
            this.lstDocTypeList.Name = "lstDocTypeList";
            this.lstDocTypeList.Size = new System.Drawing.Size(253, 199);
            this.lstDocTypeList.TabIndex = 1;
            this.lstDocTypeList.DoubleClick += new System.EventHandler(this.lstDocTypeList_DoubleClick);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(105, 233);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lblDocTypes
            // 
            this.lblDocTypes.AutoSize = true;
            this.lblDocTypes.BackColor = System.Drawing.Color.Transparent;
            this.lblDocTypes.Location = new System.Drawing.Point(7, 10);
            this.lblDocTypes.Name = "lblDocTypes";
            this.lblDocTypes.Size = new System.Drawing.Size(91, 13);
            this.lblDocTypes.TabIndex = 0;
            this.lblDocTypes.Text = "&Document Types:";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(186, 233);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // DocTypesForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(271, 266);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lstDocTypeList);
            this.Controls.Add(this.lblDocTypes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DocTypesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select";
            this.Load += new System.EventHandler(this.DocTypeList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstDocTypeList;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblDocTypes;
        private System.Windows.Forms.Button btnCancel;
    }
}