using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

namespace LMP.DocAnalyzer
{
    public partial class ElementReviewForm: System.Windows.Forms.Form
    {
        #region********************constants***********************
        //these are same separators as used in LMP.Architect.Prefill
        private const char mpPrefillFieldDelimiter = '�';		// ASCII 222
        private const char mpPrefillValueDelimiter = '�';		// ASCII 164
        private const char mpPrefillAuthorsSeparator = '|';
        #endregion
        #region********************fields***********************
        private string m_xInitialXML;
        private int m_iDocTypeID;
        private bool m_bCancelled;
        private XmlNode m_oNode;
        # endregion
        #region********************constructors***********************
        public ElementReviewForm(string xXML, System.Xml.XmlNode oNode)
        {
            InitializeComponent();
            m_xInitialXML = xXML;
            m_oNode = oNode;

            try
            {
                //parse out doc type id from xml
                int iPos1 = m_xInitialXML.IndexOf("<DocType");

                iPos1 = m_xInitialXML.IndexOf("id=\"", iPos1 + 7) + 4;
                int iPos2 = m_xInitialXML.IndexOf("\"", iPos1 + 1);

                string xDocTypeID = m_xInitialXML.Substring(iPos1, iPos2 - iPos1);
                m_iDocTypeID = int.Parse(xDocTypeID);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException("Could not retrieve DocType ID from XML.", oE);
            }
        }
        # endregion
        #region********************properties***********************

        public bool Cancelled
        {
            get { return m_bCancelled; }
        }
        #endregion********************properties***********************
        #region********************event handlers***********************
        /// <summary>
        ///Method during the form Load
        /// </summary>   
        private void ElementReviewForm_Load(object sender, EventArgs e)
        {
            //Set the Styles of the cell
            SetCellStyles();

            XmlDataDocument oXMLDoc = new XmlDataDocument();
            oXMLDoc.DataSet.ReadXml(new StringReader(m_xInitialXML));

            //Bind the grid with the Datatable as Datasource
            //columns 1 and 2 contain name and value of element.
            grdDisplay.DataSource = oXMLDoc.DataSet.Tables[1];

            //put check in each row that has non-empty values
            for (int j = 0; j < grdDisplay.Rows.Count; j++)
            {
                grdDisplay.Rows[j].Cells[0].Value = true;
            }
            
            grdDisplay.Refresh();
 
            ////column contains Parsed true or false value
            grdDisplay.Columns["colElementName"].Visible = false;
            ////column contains Id
            grdDisplay.Columns["ID"].Visible = false;

            grdDisplay.Columns["DParsing"].Visible = false;

            //DataGridViewColumn oColumn = new DataGridViewColumn();
            //oColumn = this.grdDisplay.Columns["DisplayName"];

            //grdDisplay.Sort(oColumn, ListSortDirection.Ascending);
        }
        # endregion
        #region********************properties***********************
        /// <summary>
        /// gets/set storage for XML string; built if outputformat
        /// set to XML
        /// </summary>
        public string ReviewedXML
        {
            get { return GetReviewedXML(m_iDocTypeID); }
        }
        /// <summary>
        /// gets/set storage for delimited string; built if outputformat
        /// set to DelimitedString
        /// </summary>
        public string ReviewedDelimitedString
        {
            get { return GetReviewedDelimitedString(m_iDocTypeID); }
        }
        #endregion
        #region********************methods***********************
        /// <summary>
        ///Method to Set Wrap Mode property for the grid
        /// </summary>       
        private void SetCellStyles()
        {
            grdDisplay.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
            cellStyle.WrapMode = DataGridViewTriState.True;
            grdDisplay.Columns[2].DefaultCellStyle = cellStyle;
        }

        /// <summary>
        /// Method to display the selected Elements as XML string
        /// </summary>
        /// <param name="idoctypeID"></param>
        /// <returns></returns>
        private string GetReviewedXML(int iDocTypeID)
        {
            string xElementName = string.Empty;
            string xElementValue = string.Empty;
            int iID; 
            bool bStartElement = false;
            bool bUseAuthorName = false;
            bool bIsRecipients = false;

            //Start creating XML
            XmlTextWriter xt = new XmlTextWriter(new MemoryStream(), System.Text.UTF8Encoding.Unicode);
            xt.Formatting = Formatting.Indented;
            xt.WriteStartElement("DocType");
            xt.WriteAttributeString("id", iDocTypeID.ToString());

            for (int j = 0; j < grdDisplay.Rows.Count; j++)
            {

                xElementName = grdDisplay.Rows[j].Cells["colElementName"].FormattedValue.ToString();
                if (xElementName == "AuthorName")
                {
                    if (grdDisplay.Rows[j].Cells[0].FormattedValue.ToString() == "True")
                    {
                        bUseAuthorName = true;
                    }
                    break;
                }
            }

            if (m_oNode != null && bUseAuthorName == true)
            {
                xElementName = m_oNode.Attributes["Name"].Value;
                xElementValue = m_oNode.Attributes["Value"].Value;

                xt.WriteStartElement("Element");
                xt.WriteAttributeString("Name", xElementName);
                xt.WriteAttributeString("Value", xElementValue);
                xt.WriteAttributeString("Parsed", "false");
                xt.WriteRaw("\n");
                xt.WriteEndElement();
            }

            for (int j = 0; j < grdDisplay.Rows.Count; j++)
            {
                if (!string.IsNullOrEmpty(grdDisplay.Rows[j].Cells[0].FormattedValue.ToString()))
                {
                    //Check to see whether the Element is checked.
                    //Only if checked take that Element
                    if (grdDisplay.Rows[j].Cells[0].FormattedValue.ToString() == "True")
                    {
                        
                        xElementName = grdDisplay.Rows[j].Cells["colElementName"].FormattedValue.ToString();
                        xElementValue = grdDisplay.Rows[j].Cells["colElementValue"].FormattedValue.ToString();

                        if (xElementName == "Recipients")
                            bIsRecipients = true;
                        else
                            bIsRecipients = false;

                        iID = Convert.ToInt32(grdDisplay.Rows[j].Cells["ID"].FormattedValue.ToString());

                        bStartElement = true;
                        xt.WriteStartElement("Element");
                        xt.WriteAttributeString("Name", xElementName);
                        if (grdDisplay.Rows[j].Cells["DParsing"].FormattedValue.ToString() == "Parsed")
                        {   
                            //get parsed value now
                            Element oElement = Connection.GetElement(iDocTypeID, iID);
                            
                            //replace reserved chars so element value will parse correctly
                            xElementValue = String.ReplaceXMLChars(xElementValue);

                            //GLOG : 8137 : JSW
                            //pass bIsRecipients paramater
                            xElementValue = oElement.ParsingInstructions.Execute(xElementValue, bIsRecipients);

                            //Check to see Whether the corresponding Element is parsed                           
                            xt.WriteAttributeString("Value", xElementValue);
                            xt.WriteAttributeString("Parsed", "true");
                            string xParsedValue = xElementValue;

                        }
                        else
                        {
                            xt.WriteAttributeString("Value", xElementValue);
                            xt.WriteAttributeString("Parsed", "false");
                        }
                        xt.WriteRaw("\n");
                        xt.WriteEndElement();
                    }
                    else
                    {
                        //If the parsed Element does not have a start tag and it is not selected 
                        bStartElement = false;
                    }
                    if (bStartElement)
                    {
                        xt.WriteRaw("\n");
                    }
                }
            }
            xt.WriteEndElement();
            xt.Flush();
            xt.BaseStream.Position = 0;
            return new StreamReader(xt.BaseStream).ReadToEnd();
        }
        /// <summary>
        /// Method to display the selected Elements as Delimited string
        /// </summary>
        /// <param name="idoctypeID"></param>
        /// <returns></returns>
        private string GetReviewedDelimitedString(int iDocTypeID)
        {
            string xElementName = string.Empty;
            string xElementValue = string.Empty;
            StringBuilder oSB = new StringBuilder();

            //GetReviewedXML returns data that is parsed and verified    
            //convert this xml into delimited string
            string xXML = GetReviewedXML(iDocTypeID);

            XmlDataDocument oXMLDoc = new XmlDataDocument();
            oXMLDoc.DataSet.ReadXml(new StringReader(xXML));

            for (int i = 0; i < oXMLDoc.ChildNodes[0].ChildNodes[0].ChildNodes.Count; i++)
            {
                XmlNode oNode = oXMLDoc.ChildNodes[0].ChildNodes[0].ChildNodes[i];
                xElementName = oNode.Attributes["Name"].Value;
                xElementValue = oNode.Attributes["Value"].Value;


                if (xElementValue.ToLower() == "yes")
                    xElementValue = "True";
                if (xElementValue.ToLower() == "no")
                    xElementValue = "False";

                //construct delimited string
                oSB.AppendFormat("{0}" + mpPrefillValueDelimiter.ToString() +
                    "{1}" + mpPrefillFieldDelimiter.ToString(), xElementName, xElementValue);
            }
            
            return oSB.ToString();
        }
        # endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.m_bCancelled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.m_bCancelled = true;
        }

        private void grdDisplay_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (grdDisplay.CurrentRow.Cells[1].Value.ToString() == "AuthorName" && grdDisplay.CurrentCell.OwningColumn.Index == 3)
            {
                grdDisplay.CancelEdit();
            }
        }
    }
}