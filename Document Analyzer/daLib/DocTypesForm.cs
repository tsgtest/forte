using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.DocAnalyzer
{
    public partial class DocTypesForm : Form
    {
      
        private List<DocType> m_oDocTypesList;
        private bool m_bCancelled;
        public int m_SelectDocType;

        public bool Cancelled
        {
            get { return m_bCancelled; }
        }
        
        public DocTypesForm(string xTitle, string xLabel)
        {
            InitializeComponent();

            //Get DocType List and populate the listBox
            m_oDocTypesList = Definitions.GetDocTypesList();
            if (!string.IsNullOrEmpty(xTitle))
                this.Text = xTitle;

            if (!string.IsNullOrEmpty(xLabel))
                this.lblDocTypes.Text = xLabel;
        }

        private void DocTypeList_Load(object sender, EventArgs e)
        {
            lstDocTypeList.DataSource = null;    
            lstDocTypeList.DataSource = m_oDocTypesList;
            lstDocTypeList.DisplayMember = "Name";
            lstDocTypeList.ValueMember = "ID";
            if (m_SelectDocType > -1)
            {
                lstDocTypeList.SelectedValue = m_SelectDocType;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (lstDocTypeList.SelectedIndex != -1)
            {
                m_SelectDocType = Convert.ToInt32((int)lstDocTypeList.SelectedValue);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select a Document type from the list");
                lstDocTypeList.Focus();
            }
            this.m_bCancelled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_SelectDocType = -1;
            this.m_bCancelled = true;
            this.Close();
        }

        private void lstDocTypeList_DoubleClick(object sender, EventArgs e)
        {
            if (lstDocTypeList.SelectedIndex != -1)
            {
                m_SelectDocType = Convert.ToInt32((int)lstDocTypeList.SelectedValue);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select a Document type from the list");
                lstDocTypeList.Focus();
            }
        }

        

      
    }
}