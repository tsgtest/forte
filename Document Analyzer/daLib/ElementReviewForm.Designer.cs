namespace LMP.DocAnalyzer
{
    partial class ElementReviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdDisplay = new System.Windows.Forms.DataGridView();
            this.ColInclude = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colElementName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colElementValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DParsing = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // grdDisplay
            // 
            this.grdDisplay.AllowUserToAddRows = false;
            this.grdDisplay.AllowUserToDeleteRows = false;
            this.grdDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDisplay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.grdDisplay.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.grdDisplay.BackgroundColor = System.Drawing.Color.White;
            this.grdDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColInclude,
            this.DisplayName,
            this.colElementName,
            this.colElementValue,
            this.DParsing,
            this.ID});
            this.grdDisplay.Location = new System.Drawing.Point(0, 0);
            this.grdDisplay.Name = "grdDisplay";
            this.grdDisplay.RowHeadersWidth = 20;
            this.grdDisplay.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdDisplay.Size = new System.Drawing.Size(676, 397);
            this.grdDisplay.TabIndex = 0;
            this.grdDisplay.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdDisplay_CellValidating);
            // 
            // ColInclude
            // 
            this.ColInclude.DataPropertyName = "Include";
            this.ColInclude.HeaderText = "Include";
            this.ColInclude.Name = "ColInclude";
            this.ColInclude.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColInclude.TrueValue = "";
            this.ColInclude.Width = 46;
            // 
            // DisplayName
            // 
            this.DisplayName.DataPropertyName = "DisplayName";
            this.DisplayName.HeaderText = "Display Name";
            this.DisplayName.Name = "DisplayName";
            this.DisplayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DisplayName.Width = 76;
            // 
            // colElementName
            // 
            this.colElementName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colElementName.DataPropertyName = "name";
            this.colElementName.HeaderText = "Name";
            this.colElementName.Name = "colElementName";
            this.colElementName.ReadOnly = true;
            this.colElementName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colElementName.Visible = false;
            // 
            // colElementValue
            // 
            this.colElementValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colElementValue.DataPropertyName = "value";
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.colElementValue.DefaultCellStyle = dataGridViewCellStyle1;
            this.colElementValue.FillWeight = 300F;
            this.colElementValue.HeaderText = "Value";
            this.colElementValue.Name = "colElementValue";
            this.colElementValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DParsing
            // 
            this.DParsing.DataPropertyName = "DParsing";
            this.DParsing.HeaderText = "DParsing";
            this.DParsing.Name = "DParsing";
            this.DParsing.Width = 73;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 41;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(506, 415);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(589, 415);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ElementReviewForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(676, 452);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.grdDisplay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ElementReviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Review Data";
            this.Load += new System.EventHandler(this.ElementReviewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdDisplay;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColInclude;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colElementName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colElementValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn DParsing;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}